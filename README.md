# SiteManager 3 

Sito Istituzionale di un Comune Italiano, conforme alle linee guida di [Design Italia](https://designers.italia.it/progetti/siti-web-comuni/).

## Descrizione

E' un modello di portale web avanzato, adatto a promuovere i contenuti di un ente pubblico in modalità multi-canale.
Può essere usato dai team per creare, modificare, organizzare e pubblicare contenuti. 

Conforme alle Linee Guida di design per i servizi web della PA e sviluppato utilizzando i KIT 
di Designers Italia, il sistema guida i redattori a creare e pubblicare contenuti strutturati, che vengono inseriti una sola volta nel sistema 
e possono essere rappresentati automaticamente. 

In base ai permessi, il contenuto diventa disponibile agli utenti. 
Esso risulta essere uno strumento ideale per le redazioni distribuite: ciascun redattore detiene permessi e ruoli in base alla competenza; 
ovvero in base al proprio ruolo, gli utenti ricevono specifici privilegi e responsabilità.

## Altri riferimenti

Per maggiori informazioni è possibile consultare: 

 * [Demo]
 * [Manuale utente]

## API 

 * [API della Demo]
 * [Documentazione API]


## Project Status

Il prodotto è *stabile*. 
Lo sviluppo avviene in modo costante su iniziativa autonoma del _maintainer_.

### Come provare SiteManager 3

Per attivare un ambiente demo e' necessario installare sul proprio sistema alcuni tools.

#### Pre requisiti

* Visual Studio 2022 Community scaricabile dal seguente indirizzo [https://visualstudio.microsoft.com/it/vs/community/](https://visualstudio.microsoft.com/it/vs/community/).   
* .NET Framework 4.7.2 [https://dotnet.microsoft.com/download/dotnet-framework-runtime/net472](https://dotnet.microsoft.com/download/dotnet-framework-runtime/net472).
* MySQL Server 5.7 
* Tool di gestione come MySQL Workbench, HeidiSQL etc.

#### Setup ambiente
  
Clonare il repository ed eseguire i seguenti step:

	1. Posizionarsi nella cartella `SiteManager3\WebSite`
	2. Dulicare la cartella `App_Data_Init` e rinominarla in `App_Data`  
	3. Dulicare la cartella `configs_Init` e rinominarla in `configs`.
	4. Duplicare il file `Web.Template.config` e rinominarlo in `web.config`


#### Configurazione database:  

Accedere alla shell di MySql ed edeguire i seguenti comandi, sostituendo opportunamente i placeholder `nomeutentescelto`, `passwordscelta` e `nomedatabase`:
    
```sql
CREATE USER 'nomeutentescelto'@'%' IDENTIFIED BY 'passwordscelta';

CREATE DATABASE `nomedatabase` CHARACTER SET utf8 COLLATE utf8_general_ci;

GRANT SELECT, INSERT, UPDATE, EXECUTE, DELETE, CREATE, ALTER, DROP, INDEX,CREATE VIEW, SHOW VIEW, LOCK TABLES, CREATE TEMPORARY TABLES, CREATE ROUTINE, ALTER ROUTINE, REFERENCES, TRIGGER, EVENT ON `nomedatabase`.* TO 'nomeutentescelto'@'%';
  
FLUSH PRIVILEGES;
```

Eseguire il restore del dump `cms3_db_start.sql` presente nella cartella  `SiteManager3\WebSite\Setup\Configs` sul database precedentemente creato.

#### Configurazione web.config
  
Aprire con un editor a scelta il file `web.config` presente nella cartella `SiteManager3\WebSite` e modificare i seguenti punti:

* cercare la stringa di connessione al database `connectionStrings` e modificare opportunamente i valori inserendo nome database, indirizzo host e nome utente e password scelti al punto precente.
* cercare la stringa `{#**AbsoluteLogsFolderPath**#}` e sostituirla con il percorso assoluto della cartella che conterrà i file logs del cms; 
* cercare la string `{#**AbsoluteCMSFolderPath**#}` e sostituirla con il percorso assoluto della cartella `SiteManager3\WebSite`;
* cercare il commento `<!-- Email Settings-->` e sostituire opportunamente i placeholder riportati a seguire relativi alla configurazione del servizio di invio email:
 * `{#**SmtpHost**#}` inserire l'host del server email
 * `{#**SmtpPort**#}` inserire la porta tcp 
 * `{#**SmtpUser**#}` inserire lo username associato all'email
 * `{#**SmtpPw**#}` inserire la password
 * `{#**SmtpSsl**#}` inserire true o false se il provider email richiede il protocollo SSL
 * `{#**PortalEmail**#}` inserire l'indirizzo email che il portale userà per l'invio email

#### Configurazione config.xml

Aprire con un editor a scelta il file `config.xml` presente nella cartella `SiteManager3\WebSite\configs\` e modificare i seguenti punti:

* cercare la stringa `{#**PortalName**#}` e sostituire tutte le istanze col nome del portale;
* cercare la stringa `{#**PortalEmail**#}` e sostituire tutte le istanze con l'indirizzo email che il portale userà per l'invio email;
* cercare il tag `<servermail>` e modificare opportunamente i placeholeder di seguito indicati:
  * `{#**SmtpHost**#}` inserire l'host del server email
  * `{#**SmtpPort**#}` inserire la porta tcp 
  * `{#**SmtpUser**#}` inserire lo username associato all'email
  * `{#**SmtpPw**#}` inserire la password
  * `{#**SmtpSsl**#}` inserire true o false se il provider email richiede il protocollo SSL


#### Primo avvio
  
Aprire il progetto su Visual Studio e eseguire un Run sul progetto d'uscita WebSite.

E' possibile fare accesso come amministratore (inserendo in coda all'Url `/cms`) usando l'account:
* utente:  `Administrator`
* password: `change_password`


### Come creare un ambiente di sviluppo locale

Per avere un ambiente completo di sviluppo clonare
il repository e dalla directory principale seguire i punti sopra elencati in `Come provare SiteManager 3`.


## Informazioni tecniche

L'applicazione è sviluppata sul piattaforma Microsoft .Net Framework 4.7.2, 
il dump del database contenente una installazione pienamente funzionante
dell'applicativo e i file di configurazione necessari per l'avvio dello stesso.

### Struttura del Repository

Il repository contiene direttamente il codice applicativo, strutturato in folder.
Il progetto d'uscita è il `WebSite`, le funzionalità base si trovano nella folder `Core`.
Invece nella folder `SiteManager3\Extensions` sono presenti le funzionalità aggiuntive, installabili direttamente dall'Installer presente da backoffice.


### Requisiti

Lo stack minimo di SiteManager 3 è il seguente:
  * database MySQL 5.7
  * .Net Framework 4.7.2



## Copyright (C)

Il titolare del software è il [Comune di Misterbianco](https://www.comune.misterbianco.ct.it/).

Il software è rilasciato con licenza aperta ai sensi dell'art. 69 comma 1 del [Codice dell’Amministrazione Digitale](https://cad.readthedocs.io/)

### Maintainer

[Net Service S.r.l.](), è responsabile della progettazione, realizzazione e manutenzione tecnica di SiteManager 3.

