﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace NetCms.GUI
{
    public class DetailsSheetImageRow : WebControl
    {
        protected string Url
        {
            get
            {
                return _Url;
            }
            set { _Url = value; }
        }
        private string _Url;

        protected string Title
        {
            get
            {
                return _Title;
            }
            set { _Title = value; }
        }
        private string _Title;

        protected string ImageUrl
        {
            get
            {
                return _ImageUrl;
            }
            set { _ImageUrl = value; }
        }
        private string _ImageUrl;

        public string LinkLeft
        {
            get
            {
                return _LinkLeft;
            }
            set { _LinkLeft = value; }
        }
        private string _LinkLeft;

        public string LinkRight
        {
            get
            {
                return _LinkRight;
            }
            set { _LinkRight = value; }
        }
        private string _LinkRight;

        public string CssClassLeft
        {
            get
            {
                return _CssClassLeft;
            }
            set { _CssClassLeft = value; }
        }
        private string _CssClassLeft;

        public string CssClassRight
        {
            get
            {
                return _CssClassRight;
            }
            set { _CssClassRight = value; }
        }
        private string _CssClassRight;


        public string CssClassImage
        {
            get
            {
                return _CssClassImage;
            }
            set { _CssClassImage = value; }
        }
        private string _CssClassImage;

        private HtmlTextWriterTag _Tag;

        public List<Control> ControlsToAppendRight
        {
            get
            {
                if (_ControlsToAppendRight == null)
                    _ControlsToAppendRight = new List<Control>();
                return _ControlsToAppendRight;
            }
        }
        private List<Control> _ControlsToAppendRight;

        public bool PrintNbsp { get; set; }


        public bool Alternate
        {
            get { return _Alternate; }
            set { _Alternate = value; }
        }
        private bool _Alternate;

        public DetailsSheetImageRow(string title, HtmlTextWriterTag tag = HtmlTextWriterTag.Tr)
            : base(tag)
        {
            _Tag = tag;
            this.Title = title;
            this.Url = "";
            this.PrintNbsp = false;
        }
        public DetailsSheetImageRow(string title, string url, string cssimage, HtmlTextWriterTag tag = HtmlTextWriterTag.Tr)
            : base(tag)
        {
            _Tag = tag;
            this.Title = title;
            this.Url = url;
            this.PrintNbsp = false;
            this.CssClassImage = cssimage;
        }

        public DetailsSheetImageRow(string title, string url, bool printNbsp, string cssimage, HtmlTextWriterTag tag = HtmlTextWriterTag.Tr)
            : base(tag)
        {
            _Tag = tag;
            this.Title = title;
            this.Url = url;
            this.PrintNbsp = printNbsp;
            this.CssClassImage = cssimage;
        }

        public DetailsSheetImageRow(string title, string url, string cssclass, string cssimage, HtmlTextWriterTag tag = HtmlTextWriterTag.Tr)
            : base(tag)
        {
            _Tag = tag;
            this.CssClass = cssclass;
            this.Title = title;
            this.Url = url;
            this.PrintNbsp = false;
            this.CssClassImage = cssimage;
        }

        public DetailsSheetImageRow(string title, string url, bool printNbsp, string cssclass, string cssimage, HtmlTextWriterTag tag = HtmlTextWriterTag.Tr)
            : base(tag)
        {
            _Tag = tag;
            this.CssClass = cssclass;
            this.Title = title;
            this.Url = url;
            this.PrintNbsp = printNbsp;
            this.CssClassImage = cssimage;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.CssClass += " DetailsRow DetailsRow" + (this.Alternate ? "Alternate" : "Normal");
            this.CssClass.Trim();

            WebControl ctr;
            WebControl spanLabel;
            WebControl spanValue;

            switch (_Tag)
            {
                case HtmlTextWriterTag.Tr:
                    #region Rendering in modalità Table (TD)
                    ctr = new WebControl(HtmlTextWriterTag.Td);
                    ctr.CssClass = "DetailsLabel";

                    if (!string.IsNullOrEmpty(this.ImageUrl))
                        ctr.CssClass += " " + this.CssClassLeft;

                    if (!string.IsNullOrEmpty(this.CssClassLeft))
                        ctr.CssClass += " " + this.CssClassLeft;


                    ctr.CssClass = ctr.CssClass.Trim();


                    WebControl a;

                    bool printLeft = !string.IsNullOrEmpty(this.Title);
                    if (PrintNbsp && string.IsNullOrEmpty(this.Url))
                        this.Url = "&nbsp;";
                    bool printRight = !string.IsNullOrEmpty(this.Url) || ControlsToAppendRight.Count > 0;

                    if (printLeft)
                    {
                        this.Controls.Add(ctr);
                        if (!printRight) ctr.Attributes.Add("colspan", "2");
                        if (!string.IsNullOrEmpty(this.LinkLeft))
                        {
                            a = new WebControl(HtmlTextWriterTag.A);
                            a.Attributes.Add("href", this.LinkLeft);
                        }
                        else
                            a = new WebControl(HtmlTextWriterTag.Strong);

                        ctr.Controls.Add(a);
                        a.Controls.Add(new LiteralControl(this.Title + ":"));
                    }

                    if (printRight)
                    {

                        ctr = new WebControl(HtmlTextWriterTag.Td);
                        if (!printLeft) ctr.Attributes.Add("colspan", "2");

                        this.Controls.Add(ctr);

                        if (!string.IsNullOrEmpty(this.LinkRight))
                        {
                            a = new WebControl(HtmlTextWriterTag.A);
                            ctr.Controls.Add(a);
                            a.Attributes.Add("href", this.LinkRight);
                            ctr = a;
                        }

                        WebControl img = new WebControl(HtmlTextWriterTag.Img);
                        img.CssClass = this.CssClassImage;

                        img.Attributes.Add("src", this.Url);

                        ctr.Controls.Add(img);
                        foreach (Control myctr in this.ControlsToAppendRight)
                            ctr.Controls.Add(myctr);
                    }
                    #endregion

                    break;
                case HtmlTextWriterTag.P:
                    #region Rendering in modalità Paragrafo (P)
                    ctr = new WebControl(HtmlTextWriterTag.P);
                    ctr.CssClass = "DetailsRow DetailsRow" + this.Title.Replace(" ", "_");
                    ctr.CssClass.Trim();

                    spanLabel = new WebControl(HtmlTextWriterTag.Span);
                    spanLabel.CssClass = "DetailsRowLabel";
                    spanLabel.Controls.Add(new LiteralControl(this.Title + ": "));
                    ctr.Controls.Add(spanLabel);

                    spanValue = new WebControl(HtmlTextWriterTag.Span);
                    spanValue.CssClass = "DetailsRowValue";

                    WebControl imgspan = new WebControl(HtmlTextWriterTag.Img);
                    imgspan.CssClass = this.CssClassImage;
                    imgspan.Attributes.Add("src", this.Url);

                    spanValue.Controls.Add(imgspan);
                    ctr.Controls.Add(spanValue);

                    this.Controls.Add(ctr);

                    #endregion
                    break;
                case HtmlTextWriterTag.Li:
                    #region Rendering in modalità Lista (UL->LI)
                    ctr = new WebControl(HtmlTextWriterTag.Li);
                    ctr.CssClass = "DetailsRow DetailsRow" + this.Title.Replace(" ", "_");
                    ctr.CssClass.Trim();

                    spanLabel = new WebControl(HtmlTextWriterTag.Span);
                    spanLabel.CssClass = "DetailsRowLabel";
                    spanLabel.Controls.Add(new LiteralControl(this.Title + ":"));
                    ctr.Controls.Add(spanLabel);

                    spanValue = new WebControl(HtmlTextWriterTag.Span);
                    spanValue.CssClass = "DetailsRowValue";

                    WebControl imgli = new WebControl(HtmlTextWriterTag.Img);
                    imgli.CssClass = this.CssClassImage;
                    imgli.Attributes.Add("src", this.Url);

                    spanValue.Controls.Add(imgli);
                    ctr.Controls.Add(spanValue);

                    this.Controls.Add(ctr);

                    #endregion
                    break;
            }
        }
    }
}