using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI
{
    public class DetailsSheet : WebControl
    {
        /// <summary>
        /// RenderType
        /// </summary>
        public enum RenderType
        {
            /// <summary>
            /// Table Tag
            /// </summary>
            Table,
            /// <summary>
            /// Ul Tag 
            /// </summary>
            List,
            /// <summary>
            /// P tag
            /// </summary>
            Paragraph,
            /// <summary>
            /// Dl tag
            /// </summary>
            DescriptionList,
            /// <summary>
            /// Dl horizontal tag 
            /// </summary>
            DescriptionListHorizontal
        }

        /// <summary>
        /// Type Render
        /// </summary>
        public RenderType Render
        {
            get { return _Render; }
            set { _Render = value; }
        }
        private RenderType _Render = RenderType.Table;

        
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
        private string _Title;

        public string Description
        {
            get { return _Description; }
            private set { _Description = value; }
        }
        private string _Description;


        /// <summary>
        /// Used on Frontend
        /// </summary>
        public bool UsedOnFrontend
        {
            get { return _UsedOnFrontend; }
            set { _UsedOnFrontend = value; }
        }
        private bool _UsedOnFrontend = false;


        /// <summary>
        /// Permette di visualizzare il pannello aperto al primo avvio
        /// </summary>
        public bool CollapseOpen
        {
            get { return _CollapseOpen; }
            set { _CollapseOpen = value; }
        }
        private bool _CollapseOpen = false;


        /// <summary>
        /// Detail Sheet Constuctor
        /// </summary>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="render"></param>
        public DetailsSheet(string title, string description, RenderType render = RenderType.Table):base(HtmlTextWriterTag.Div)
        {
            Render = render;
            this.Title = title;
            this.Description = description;
        }

        /// <summary>
        /// Detail Sheet Constructor
        /// </summary>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="render"></param>
        /// <param name="isOnFrondend"></param>
        public DetailsSheet(string title, string description, RenderType render , bool isOnFrondend = false) : base(HtmlTextWriterTag.Div)
        {
            Render = render;
            this.Title = title;
            this.Description = description;
            UsedOnFrontend = isOnFrondend;            
        }


        /// <summary>
        /// AddTitleTextRow Simplified Constructor for DetailSheetRow
        /// </summary>
        /// <param name="Title">Title key</param>
        /// <param name="Text">Text value</param>
        public void AddTitleTextRow(string Title, string Text)
        {
            var row = new DetailsSheetRow(Title, Text, true);
            AddRow(row);
        }


        /// <summary>
        /// AddTitleTextRow Simplified Constructor for DetailSheetImageRow
        /// </summary>
        /// <param name="Title">Title key</param>
        /// <param name="Url">Url image value</param>
        public void AddTitleImageRow(string Title, string Url)
        {
            var row = new DetailsSheetImageRow(Title, Url, true, "");
            AddRow(row);
        }


        public void AddRow(DetailsSheetRow row)
        {
            row.Alternate = Alternate;
            Content.Controls.Add(row);
            Alternate = !Alternate;
        }

        public void AddRow(DetailsSheetImageRow row)
        {
            row.Alternate = Alternate;
            Content.Controls.Add(row);
            Alternate = !Alternate;
        }

        public void AddControl(WebControl control)
        { 
            Content.Controls.Add(control);           
        }


        /// <summary>
        /// Add Webcontrol into DetailSheetContainerElement
        /// </summary>
        /// <param name="control"></param>
        public void AddContainerControl(Control control)
        {
            ContainerContent.Controls.Add(control);
        }


        /// <summary>
        /// Add separator and create a new DetailsSheet
        /// </summary>
        /// <remarks>Adatto per i cicli foreach, da inserire alla fine del singolo ciclo</remarks>
        public void AddSeparatorElement()
        {        
            ListDetailsSheet.Add(ContainerContent);
            _Content = null;
            _ContainerContent = null;
        }

         
        /// <summary>
        /// List DetailsSheet
        /// </summary>
        public IList<Control> ListDetailsSheet
        {
            get
            {
                if (_ListDetailsSheet == null)
                {
                    _ListDetailsSheet = new List<Control>();
                }
                return _ListDetailsSheet;
            }
        }
        private List<Control> _ListDetailsSheet;

        //public List<Control> ControlsToAppend
        //{
        //    get
        //    {
        //        if (_ControlsToAppend == null)
        //        {
        //            _ControlsToAppend = new List<Control>();
        //        }
        //        return _ControlsToAppend;
        //    }
        //}
        //private List<Control> _ControlsToAppend;


        public bool Alternate
        {
            get { return _Alternate; }
            private set { _Alternate = value; }
        }
        private bool _Alternate = true;
          

        /// <summary>
        /// Ename MultiEntiy or Multi Control Rendering
        /// </summary>
        /// <remarks>Abilita il supporto alla gestione di pi�</remarks>
        public bool MultipleEntityOrControl
        {
            get { return _MultipleEntityOrControl; }
            set { _MultipleEntityOrControl = value; }
        }
        private bool _MultipleEntityOrControl = false;


        /// <summary>
        /// Enable Collapsable Panel Rendering
        /// </summary>
        public bool EnableCollapseRendering
        {
            get { return _EnableCollapseRendering; }
            set { _EnableCollapseRendering = value; }
        }
        private bool _EnableCollapseRendering = false;

        private WebControl ContainerContent
        {
            get
            {
                if (_ContainerContent == null)
                {
                    _ContainerContent = new WebControl(HtmlTextWriterTag.Div);
                    _ContainerContent.CssClass = "DetailSheetContainerElement" + (UsedOnFrontend ? " col-md-12 " : "" );
                    _ContainerContent.Controls.Add(Content);
                }
                return _ContainerContent;
            }
            set { _ContainerContent = value; }
        }

        private WebControl _ContainerContent;

        private WebControl Content
        {
            get
            {
                if (_Content == null)
                {
                    switch (Render)
                    {
                        case RenderType.Table:
                            _Content = new WebControl(HtmlTextWriterTag.Table);
                            _Content.CssClass = UsedOnFrontend ? "table DetailsContainer": " DetailsContainer";
                            //_Content.Attributes.Add("cellpadding", "0");
                            //_Content.Attributes.Add("cellspacing", "0");
                            break;
                        case RenderType.List:
                            _Content = new WebControl(HtmlTextWriterTag.Ul);
                            _Content.CssClass += " DetailsContainer";
                            break;
                        case RenderType.Paragraph:
                            _Content = new WebControl(HtmlTextWriterTag.Div);
                            _Content.CssClass += " DetailsContainer";
                            break;
                        case RenderType.DescriptionList:
                            _Content = new WebControl(HtmlTextWriterTag.Dl);
                            _Content.CssClass += " DetailsContainer";
                            break;
                        case RenderType.DescriptionListHorizontal:
                            _Content = new WebControl(HtmlTextWriterTag.Dl);
                            _Content.CssClass = "dl-horizontal DetailsContainer";
                            break;
                        default:
                            _Content = new WebControl(HtmlTextWriterTag.Table);
                            _Content.CssClass = UsedOnFrontend ? "table DetailsContainer" : " DetailsContainer";
                            //_Content.Attributes.Add("cellpadding", "0");
                            //_Content.Attributes.Add("cellspacing", "0");
                            break;
                    }
                    
                }
                return _Content;
            }
        }
        private WebControl _Content;


        private static Random random = new Random();
        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!EnableCollapseRendering)
            {
                HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
                this.CssClass = "Details";

                HtmlGenericControl legend = new HtmlGenericControl("legend");
                legend.InnerHtml = this.Title;
                fieldset.Controls.Add(legend);

                if (!string.IsNullOrEmpty(this.Description))
                {
                    HtmlGenericControl text = new HtmlGenericControl("div");
                    text.Attributes["class"] = "DetailsText";
                    text.InnerHtml = this.Description;
                    fieldset.Controls.Add(text);
                }


                if (!MultipleEntityOrControl)
                    fieldset.Controls.Add(Content);
                else
                {
                    if (ListDetailsSheet.Count > 0)
                    {
                        foreach (var item in ListDetailsSheet)
                            fieldset.Controls.Add(item);
                    }
                    else
                        fieldset.Controls.Add(ContainerContent);
                }

                this.Controls.Add(fieldset);
            }
            else
            {
                string key = RandomString(3);
                this.CssClass = "DetailSheetController";
                
                WebControl DescriptionContainer = new WebControl(HtmlTextWriterTag.Div);
                DescriptionContainer.CssClass = "panel panel-default";

                WebControl DescriptionHeading = new WebControl(HtmlTextWriterTag.Div);
                DescriptionHeading.CssClass = "panel-heading";
                DescriptionHeading.Attributes.Add("role", "tab");
                DescriptionHeading.ID = "DetailsSheet-Heading-" + key ;

                WebControl DescriptionTitle = new WebControl(HtmlTextWriterTag.H4);
                DescriptionTitle.CssClass = "panel-title";

                WebControl aDescriptionTitleLink = new WebControl(HtmlTextWriterTag.A);
                aDescriptionTitleLink.Attributes["href"] = "#DetailsSheet-Panel-" + key ;
                aDescriptionTitleLink.Attributes.Add("role", "button");
                aDescriptionTitleLink.Attributes.Add("data-toggle", "collapse");
                aDescriptionTitleLink.Attributes.Add("aria-controls", "DetailsSheet-Panel-" + key);
                aDescriptionTitleLink.Attributes.Add("aria-expanded", "false");
                aDescriptionTitleLink.Controls.Add(new LiteralControl(Title));
                DescriptionTitle.Controls.Add(aDescriptionTitleLink);

                DescriptionHeading.Controls.Add(DescriptionTitle);
                DescriptionContainer.Controls.Add(DescriptionHeading);


                WebControl DescriptionCollapseBox = new WebControl(HtmlTextWriterTag.Div);
                DescriptionCollapseBox.ID = "DetailsSheet-Panel-" + key;
                DescriptionCollapseBox.CssClass = "panel-collapse collapse " + (CollapseOpen ? " in": "");
                DescriptionCollapseBox.Attributes.Add("role", "tabpanel");
                DescriptionCollapseBox.Attributes.Add("aria-labelledby", "DetailsSheet-Heading-" + key);

                WebControl DescriptionBody = new WebControl(HtmlTextWriterTag.Div);
                DescriptionBody.CssClass = "panel-body";

                if (!string.IsNullOrEmpty(this.Description))
                {
                    HtmlGenericControl text = new HtmlGenericControl("div");
                    text.Attributes["class"] = "DetailsText";
                    text.InnerHtml = this.Description;
                    DescriptionBody.Controls.Add(text);
                }

                if (!MultipleEntityOrControl)
                    DescriptionBody.Controls.Add(Content);
                else
                {
                    if (ListDetailsSheet.Count > 0)
                    {
                        foreach (var item in ListDetailsSheet)
                            DescriptionBody.Controls.Add(item);
                    }
                    else
                        DescriptionBody.Controls.Add(ContainerContent);
                }


                DescriptionCollapseBox.Controls.Add(DescriptionBody);
                DescriptionContainer.Controls.Add(DescriptionCollapseBox);

                this.Controls.Add(DescriptionContainer);
            }

        }

        public void AddTitleTextRow(string v)
        {
            throw new NotImplementedException();
        }

        public void AddSection(string title, string cssClass)
        {
            var row = new DetailsSheetRow("", title, false, cssClass);
            AddRow(row);
        }
    }
}