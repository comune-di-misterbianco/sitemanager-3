﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace NetCms.Vertical
{
    public class BreadcrumbBlock
    {
        public BreadcrumbBlock(string label,string url = null, string cssClass = null) 
        {
            _Label = label;
            _Url = url;
            _CssClass = cssClass;
        }

        private string _Label;
        public string Label
        {
            get { return _Label; }            
        }

        private string _Url;
        public string Url
        {
            get { return _Url; }            
        }

        private string _CssClass;
        public string CssClass
        {
            get { return _CssClass; }            
        }

        public WebControl GetControl() 
        {
            WebControl li = new WebControl(HtmlTextWriterTag.Li);
            if (!string.IsNullOrEmpty(CssClass))
                li.CssClass = CssClass;

            WebControl link = new WebControl(System.Web.UI.HtmlTextWriterTag.A);
            link.Controls.Add(new LiteralControl(Label));

            link.Attributes["href"] = Url;
            link.Attributes["title"] = "Torna al " + Label;

            li.Controls.Add(link);

            return li;
        }

        public WebControl GetControlSimple() 
        {
            //WebControl span = new WebControl(HtmlTextWriterTag.Span);
            WebControl li = new WebControl(HtmlTextWriterTag.Li);

            li.Controls.Add(new LiteralControl(Label));
            if (!string.IsNullOrEmpty(CssClass))
                li.CssClass = CssClass;
            return li;
        }
    }
}
