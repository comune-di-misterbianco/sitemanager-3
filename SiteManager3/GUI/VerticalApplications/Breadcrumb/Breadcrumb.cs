﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace NetCms.Vertical
{
    public class Breadcrumb : WebControl
    {
        private List<BreadcrumbBlock> _BreadcrumbBlocks;
        public List<BreadcrumbBlock> BreadcrumbBlocks
        {
            get
            {
                if (_BreadcrumbBlocks == null)
                    _BreadcrumbBlocks = new List<BreadcrumbBlock>();
                return _BreadcrumbBlocks;
            }
            set
            {
                _BreadcrumbBlocks = value;   
            }
        }

        public Breadcrumb() 
            :base ( HtmlTextWriterTag.Ol)
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnInit(e);
            if (BreadcrumbBlocks != null && BreadcrumbBlocks.Count > 0)
            {
                int i = 1;
                foreach (BreadcrumbBlock block in BreadcrumbBlocks)
                {
                    if (i < BreadcrumbBlocks.Count)
                    {
                        this.Controls.Add(block.GetControl());
                        //this.Controls.Add(new LiteralControl("&raquo;"));
                    }
                    else
                        this.Controls.Add(block.GetControlSimple());
                    i++;
                }
            }
        }

        //public WebControl GetControl() 
        //{
        //    WebControl control = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
        //    control.CssClass = "Breadcrumb";

        //    if (BreadcrumbBlocks != null && BreadcrumbBlocks.Count > 0)
        //    {
        //        int i = 1;
        //        foreach (BreadcrumbBlock block in BreadcrumbBlocks)
        //        {
        //            if (i < BreadcrumbBlocks.Count)
        //            {
        //                control.Controls.Add(block.GetControl());
        //                control.Controls.Add(new LiteralControl("&raquo;"));
        //            }
        //            else
        //                control.Controls.Add(block.GetControlSimple());
        //            i++;
        //        }
        //    }

        //    return control;
        //}
    }
}
