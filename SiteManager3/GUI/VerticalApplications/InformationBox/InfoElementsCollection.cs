using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace NetCms.GUI.Info
{
    /// <summary>
    /// Rappresenta una collezione di NetCms.GUI.ToolbarButton
    /// </summary>
    public class InfoElementsCollection : IEnumerable
    {
        private Dictionary<string,object> coll;
        public int Count { get { return coll.Count; } }
        
        public InfoElementsCollection()
        {
            coll = new Dictionary<string,object>();
        }

        public void Add(InfoElement InfoElement, string key)
        {
            coll.Add(key, InfoElement);
            OnItemAdded(EventArgs.Empty, InfoElement);
        }

        public void Add(InfoElement InfoElement)
        {
            coll.Add(InfoElement.GetHashCode().ToString(), InfoElement);
            OnItemAdded(EventArgs.Empty,InfoElement);
        }

        public event EventHandler ItemAdded;
        public delegate void EventHandler(object sender, EventArgs e, object value);

        protected virtual void OnItemAdded(EventArgs e, object value)
        {
            if (ItemAdded != null)
                ItemAdded(this, e, value);
        }

        public void Remove(string key)
        {
            coll.Remove(key);
        }

        public void Remove(InfoElement InfoElement)
        {
            coll.Remove(InfoElement.GetHashCode().ToString());
        }

        
        public void Clear()
        {
            coll.Clear();
        }

        public InfoElement this[int i]
        {
            get
            {
                InfoElement str = (InfoElement)coll[coll.Keys.ElementAt(i)];
                return str;
            }
        }

        /*public int IndexOf(string key)
        {
            var ks = coll.Keys;
            int firstItem = ks.Select((item, index) => new
            {
                ItemName = item,
                Position = index
            }).Where(x => x.ToString() == key)
.First()
.Position;

            return firstItem;
        }*/

        public InfoElement this[string key]
        {
            get
            {
                InfoElement val = (InfoElement)coll[key];
                return val;
            }
        }
        
        public bool Contains(string key)
        {
            return coll[key] != null;
        }


        public bool Contains(InfoElement InfoElement)
        {
            return coll[InfoElement.GetHashCode().ToString()] != null;
        }

        #region Enumerator
   
        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private InfoElementsCollection Collection;
            public CollectionEnumerator(InfoElementsCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}