﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace NetCms.GUI.Info
{
    /// <summary>
    /// La classe GenericToolbar implementa la gestione della Toolbar generiche da utilizzare nella pagina dell'applicazione.
    /// </summary>
    public class GenericInfoBox : InfoBox
    {
        /// <summary>
        /// Inizializza una nuova istanza di NetCms.GUI.GenericInfoBox 
        /// </summary>
        /// <example>
        /// L'esempio descrive come istanziare un oggetto NetCms.GUI.InfoBox
        /// <code>
        /// InfoBox myGenericInfoBox = new GenericInfoBox();
        /// </code>
        /// </example>
        public GenericInfoBox()
        {
        }


        /// <summary>
        /// Ritorna il nome tag html, relativo all'oggetto NetCms.GUI.GenericToolbar
        /// </summary>
        protected override string TagName
        {
            get
            {
                return "DIV";
            }
        }

        /// <summary>
        /// Renering html dell'oggetto NetCms.GUI.GenericInfoBox
        /// </summary>
        /// <returns>
        /// Ritorna il controllo HtmlGenericControl del NetCms.GUI.GenericInfoBox
        /// </returns>
        protected override HtmlGenericControl RenderRow(InfoElement element)
        {
            NetCms.GUI.Icons.IconsIndexer indexer = new NetCms.GUI.Icons.IconsIndexer(this.Page);

            HtmlGenericControl li = new HtmlGenericControl("li");
            
            li.Attributes["class"] = "InfoBoxItem";

            if (element.CssClass != null && CssClass.Length > 0)
                li.Attributes["class"] += " " + CssClass;

            li.InnerHtml += "<span style=\"background-image:url(" + indexer.GetImageURL(element.Icon) + ")\" >";
            li.InnerHtml += "<strong>" + element.Text + "</strong>";
            li.InnerHtml += "</span>";

            return li;
        }
    }
}
