﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.GUI.Info
{
    /// <summary>
    /// Rappresenta la classe astratta per la gestione del Box Informazioni NetCms.GUI.Info.InfoBox
    /// </summary>
    public abstract class InfoBox:WebControl
    {
        public const string PostBackMessageSessionKey = "PostBackMessageSessionKey";

        /// <summary>
        /// Ottiene la collezione NetCms.GUI.Info.InfoElementsCollection di oggetti NetCms.GUI.InfoElement
        /// </summary>
        public InfoElementsCollection Infos
        {
            get
            {
                return _Infos;
            }
        }
        private InfoElementsCollection _Infos;

        /// <summary>
        /// Inizializza una nuova istanza di NetCms.GUI.Info.InfoBox 
        /// </summary> 
        public InfoBox()
            : base(HtmlTextWriterTag.Ul)
        {
            _Infos = new InfoElementsCollection();

        }

        private void InitSessionMessages()
        {
            List<SessionPostbackMessage> list = GetSessionMessagesList();

            foreach (var p in list)
                this.AddPostbackMessage(p.Message, p.PostBackMessagesType);

            HttpContext.Current.Session.Remove(PostBackMessageSessionKey);
        }

        /// <summary>
        /// Metodo astratto da implementare nella classe derivata per il rendering di una riga di info
        /// </summary>
        /// <param name="button">InfoElement</param>
        /// <returns>Ritorna il controllo web relativo all'InfoElement</returns>
        protected abstract HtmlGenericControl RenderRow(InfoElement element);

        /// <summary>
        /// Genera l'evento PreRender
        /// </summary>
        /// <param name="e">Evento</param>
        /// <remarks>
        /// Questo metodo notifica al controllo server di effettuare tutte le operazioni preliminari al rendering prima di salvare lo stato di visualizzazione e di eseguire il rendering del contenuto.
        /// </remarks>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            InitSessionMessages();

            foreach (InfoElement element in this.Infos)
            {
                this.Controls.Add(RenderRow(element));
            }
        }
        public enum PostBackMessagesType { Normal, Success, Notify, Error }

        public void AddPostbackMessage(string str) { AddPostbackMessage(str, PostBackMessagesType.Normal); }

        public void AddPostbackMessage(string msg, PostBackMessagesType postBackMessagesType)
        {
            this.Infos.Add(new NetCms.GUI.Info.InfoElement(msg,postBackMessagesType.ToString()));
        }
        public static void AddSessionPostbackMessage(string msg, PostBackMessagesType postBackMessagesType)
        {
            AddSessionPostbackMessage(msg, postBackMessagesType, HttpContext.Current.Request.Url.ToString());
        }
        public static void AddSessionPostbackMessage(string msg, PostBackMessagesType postBackMessagesType, string redirectToUrl)
        {
            List<SessionPostbackMessage> list = GetSessionMessagesList();
            list.Add(new SessionPostbackMessage() { Message = msg, PostBackMessagesType = postBackMessagesType });
            HttpContext.Current.Response.Redirect(redirectToUrl);
        }

        public static List<SessionPostbackMessage> GetSessionMessagesList()
        { 
            List<SessionPostbackMessage> list = null;

            if(HttpContext.Current.Session[PostBackMessageSessionKey] == null)
                HttpContext.Current.Session[PostBackMessageSessionKey] = list = new List<SessionPostbackMessage>();
            else
                list = HttpContext.Current.Session[PostBackMessageSessionKey] as List<SessionPostbackMessage>;

            return list;        
        }

        public struct SessionPostbackMessage
        {
            public string Message { get; set; }
            public PostBackMessagesType PostBackMessagesType { get; set; } 
        }
    }
}
