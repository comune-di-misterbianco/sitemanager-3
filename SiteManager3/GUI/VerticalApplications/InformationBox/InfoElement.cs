using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms;


namespace NetCms.GUI.Info
{
    /// <summary>
    /// Rappresenta una istanza di un oggetto NetCms.Gui.Info.InfoElement
    /// </summary>
    /// <remarks>
    ///  La Classe InfoElement permette di definire un riga per esporre informazioni. 
    /// </remarks>
    public class InfoElement
    {        
        private string _CssClass;
        /// <summary>
        /// Ottiene la classe css associata
        /// </summary>
        public string CssClass
        {
            get { return _CssClass; }
            private set { _CssClass = value; }
        }       

        /// <summary>
        /// Ottiene l'etichetta associata
        /// </summary>
        public string Text
        {
            get { return _Text; }
            private set { _Text = value; }
        }
        private string _Text;

        /// <summary>
        /// Ottiene l'icona associata
        /// </summary>
        public NetCms.GUI.Icons.Icons Icon
        {
            get { return _Icon; }
            private set { _Icon = value; }
        }
        private NetCms.GUI.Icons.Icons _Icon;

        /// <summary>
        ///  La Classe InfoElement permette di definire un riga per esporre informazioni. 
        /// </summary>
        /// <param name="text">Il Testo della riga Informativa</param>
        /// <example>
        /// Per istanziare una nuova riga usare la seguente sintassi:
        /// <code>
        /// InfoElement myelement = new InfoElement("Testo testo");
        /// </code>
        /// </example>
        public InfoElement(string text, string cssClass = "")
        {
            this.CssClass = cssClass;
            this.Icon = NetCms.GUI.Icons.Icons.Information;
            this.Text = text;
        }

        /// <summary>
        ///  La Classe InfoElement permette di definire un riga per esporre informazioni. 
        /// </summary>
        /// <param name="text">L'etichetta da associare al bottone</param>
        /// <param name="icon">Un icona da associare alla riga</param>
        /// <example>
        /// Per istanziare una nuova riga usare la seguente sintassi:
        /// <code>
        /// InfoElement myelement = new InfoElement("Testo testo");
        /// </code>
        /// </example>
        public InfoElement(string text, NetCms.GUI.Icons.Icons icon)
        {
            this.Icon = icon;
            this.Text = text;
        }
        
    }
}