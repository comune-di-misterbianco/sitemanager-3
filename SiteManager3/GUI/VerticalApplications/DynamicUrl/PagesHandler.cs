﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.Vertical;

namespace DynamicUrl
{
    public class PageHandler
    {
        public static bool GeneratePages
        {
            get
            {
                if (_GeneratePages == null)
                {
                    _GeneratePages = System.Web.Configuration.WebConfigurationManager.AppSettings["dinamicGeneratePhysicalPages"];
                }
                return _GeneratePages == "true";
            }
        }
        private static string _GeneratePages;

        public static bool DebugMode
        {
            get
            {
                if (_DebugMode == null)
                {
                    _DebugMode = System.Web.Configuration.WebConfigurationManager.AppSettings["debugMode"];
                }
                return _DebugMode == "true";
            }
        }
        private static string _DebugMode;

        public List<System.Reflection.Assembly> Assemblies
        {
            get
            {
                if (_Assemblies == null)
                {
                    _Assemblies = new List<System.Reflection.Assembly>();
                }
                return _Assemblies;
            }
        }
        private List<System.Reflection.Assembly> _Assemblies;

        public Dictionary<string,System.Type> Pages
        {
            get
            {
                if (_Pages == null)
                {
                    _Pages = new Dictionary<string, Type>();
                }
                return _Pages;
            }
        }
        private Dictionary<string,System.Type> _Pages;

        public string AbsoluteWebRoot
        {
            get
            {
                return PagesHandler.AbsoluteWebRoot;
            }
        }
        private string _AbsoluteWebRoot;

        public PageHandler()
        {
        }

        public void InitPages()
        {
            this.Pages.Clear();
            foreach (var assembly in this.Assemblies)
            {
                var types = assembly.GetTypes();
                foreach (System.Type t in types)
                {
                    System.Attribute[] attrs = System.Attribute.GetCustomAttributes(t);  // reflection

                    foreach (System.Attribute attr in attrs)
                    {
                        if (attr is PageControl)
                        {
                            PageControl pageAddress = (PageControl)attr;
                            if(DebugMode || !pageAddress.OnlyForDebugPurpose)
                                this.Pages.Add(pageAddress.Address, t);
                        }
                    }

                }
            }            
        }

        public string[,] GetPagesForVerticalSetup()
        {
            var pages = new string[this.Pages.Count, 2];

            int i = 0;
            foreach (var page in this.Pages)
            {
                pages[i, 0] = page.Key;
                pages[i++, 1] = page.Value.FullName;
            }

            return pages;
        }

        public NetCms.Vertical.ApplicationPage GetCurrentPage(System.Web.UI.Page webpage, StateBag viewState)
        {
            return GetCurrentPage(webpage ,viewState,new NetCms.GUI.GenericToolbar());
        }

        public NetCms.Vertical.ApplicationPage GetCurrentPage(System.Web.UI.Page webpage, StateBag viewState,  NetCms.GUI.Toolbar toolbar)
        {
            NetCms.Vertical.ApplicationPage page = null;

            string currentUrl = HttpContext.Current.Request.Url.ToString().ToLower();

            string splitKey = this.AbsoluteWebRoot.ToLower().Length == 0 ? "/" : this.AbsoluteWebRoot.ToLower();

            string[] parts = currentUrl.Split(new string[] { splitKey, "?", "#" }, StringSplitOptions.RemoveEmptyEntries);

            //Modificato per gestire il caso in cui AbsoluteWebRoot è una stringa vuota es: http://<ip>/default.aspx
            currentUrl = parts[1];
            if (this.AbsoluteWebRoot.ToLower().Length == 0)
                if (parts.Length > 2)
                    currentUrl = "/" + parts[2];

            if (this.Pages.ContainsKey(currentUrl))
            {
                object[] pars = { viewState, webpage.IsPostBack, toolbar, new NetCms.GUI.Info.GenericInfoBox(), null };
                return (NetCms.Vertical.ApplicationPage)System.Activator.CreateInstance(this.Pages[currentUrl], pars);
            }

            return page;
        }
    }
}
