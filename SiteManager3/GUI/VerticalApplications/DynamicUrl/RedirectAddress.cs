﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicUrl
{
    public class RedirectAddress
    {
        public RedirectAddress(string address)
        {
            this.Address = address;
        }

        public string Address { get; private set; }

        public void GoTo()
        {
            System.Web.HttpContext.Current.Response.Redirect(Address);
        }
    }

}
