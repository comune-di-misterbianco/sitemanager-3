﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicUrl
{
    [System.AttributeUsage(System.AttributeTargets.Class)]
    public class PageControl : System.Attribute
    {
        /// <summary>
        /// Indirizzo di reperimento della pagina
        /// Es: /default.aspx , /users/default.aspx , /users/edit/edit.aspx
        /// </summary>
        /// <param name="address">Stringa contenente l'indirizzo</param>
        public PageControl(string address):this(address,false)
        {
        }
        public PageControl(string address, bool onlyForDebugPurpose)
        {
            this.ApplicationID = 0;
            this.Address = address;
            this.OnlyForDebugPurpose = onlyForDebugPurpose;
        }

        public string Address
        {
            get
            {
                return _Address;
            }
            private set
            {
                _Address = value;
            }
        }
        private string _Address;

        public bool OnlyForDebugPurpose
        {
            get
            {
                return _OnlyForDebugPurpose;
            }
            private set
            {
                _OnlyForDebugPurpose = value;
            }
        }
        private bool _OnlyForDebugPurpose;

        public int ApplicationID
        {
            get
            {
                return _ApplicationID;
            }
            set
            {
                _ApplicationID = value;
            }
        }
        private int _ApplicationID;
    }
}
