﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DynamicUrl
{
    public class RedirectManager
    {
        public static void GoTo(System.Type type, string absoluteWebRoot)
        {
            System.Attribute[] attrs = System.Attribute.GetCustomAttributes(type);  // reflection

            foreach (System.Attribute attr in attrs)
            {
                if (attr is PageControl)
                {
                    PageControl pageAddress = (PageControl)attr;
                    
                    //Permette di gestire il caso di pagine richiamate attraverso VerticalApplicationPackageApp. 
                    //Restituisce l'applicazione contenuta, nel caso di package, stringa vuota nei casi standard.
                    string prefixPage = GetVerticalApplicationPageURL(pageAddress.Address, absoluteWebRoot);
                    
                    System.Web.HttpContext.Current.Response.Redirect(absoluteWebRoot + prefixPage + pageAddress.Address);
                }
            }
        }

        public static string GetPageAddress(System.Type type, string absoluteWebRoot)
        {
            System.Attribute[] attrs = System.Attribute.GetCustomAttributes(type);  // reflection
            
            foreach (System.Attribute attr in attrs)
            {
                if (attr is PageControl)
                {
                    PageControl pageAddress = (PageControl)attr;

                    
                    //Permette di gestire il caso di pagine richiamate attraverso VerticalApplicationPackageApp. 
                    //Restituisce l'applicazione contenuta, nel caso di package, stringa vuota nei casi standard.
                    string prefixPage = GetVerticalApplicationPageURL(pageAddress.Address, absoluteWebRoot);
                    
                    return absoluteWebRoot + prefixPage + pageAddress.Address;
                }
            }
            return null;
        }


        private static string GetVerticalApplicationPageURL(string pageAddress, string absoluteWebRoot)
        {
            string url = HttpContext.Current.Request.Url.PathAndQuery.ToLower().Split('?')[0];

            if (url.Contains(absoluteWebRoot + "/"))
            {

                url = url.Replace(absoluteWebRoot + "/", "");

                string[] splittedUrl = new string[] { url };

                if (url.Contains("/"))
                    splittedUrl = SplitAtFirstSlash(url);

                string prefixToReturn = splittedUrl[0];

                //Restituisco il prefisso solo se non contiene un indirizzo di pagina con estensione e se non è già contenuto nel pageAddress
                if (!prefixToReturn.Contains(".") && !pageAddress.Contains(prefixToReturn))
                    return "/" + prefixToReturn;
                return "";
            }
            return "";
        }
        private static string[] SplitAtFirstSlash(string path)
        {
            string[] value = new string[2];
            int IndexOfSlash = path.IndexOf('/');
            value[0] = path.Substring(
                                        0,
                                        IndexOfSlash
                                     );
            value[1] = path.Substring(
                                        IndexOfSlash + 1,
                                        (path.Length - IndexOfSlash) - 1
                                     );
            return value;
        }
    }
}
