﻿//using System;
//using System.Net.Mail;

//namespace NetService.Utility.Common
//{
//    /// <summary>
//    /// La classe MailSender consente di inviare messaggi di posta elettronica attraverso un server SMTP.
//    /// </summary>
//    public class MailSender
//    {
//        public string SmtpHost { get; set; }
//        public int SmtpPort { get; set; }
//        public string SmtpUser { get; set; }
//        public string SmtpPassword { get; set; }
//        public bool EnableSsl { get; set; }

//        public string EmailTo { get; private set; }
//        public string EmailCC { get; private set; }
//        public string Subject { get; private set; }
//        public string Body { get; private set; }
//        public string EmailFrom { get; private set; }
//        public string LblMittente { get; private set; }
//        public bool SendAsHtml { get; set; }

//        /// <summary>
//        /// Inizializzazione di una nuova istanza
//        /// </summary>
//        /// <param name="emailTo">Indirizzo di posta elettronica a cui spedire il messaggio</param>
//        /// <param name="subject">Oggette del messaggio</param>
//        /// <param name="body">Corpo del messaggio in fomato: accetta anche testo formattato in html</param>
//        /// <param name="emailFrom">Indirizzo di posta elettronica usato per l'invio del messaggio</param>
//        /// <param name="lblMittente">Etichetta testuale visibile nel intestazione del messaggio</param>
//        public MailSender(string emailTo, string subject, string body, string emailFrom, string lblMittente)
//        {
//            EmailTo = emailTo;
//            Subject = subject;
//            Body = body;
//            EmailFrom = emailFrom;
//            LblMittente = lblMittente;
//        }

//        /// <summary>
//        /// Inizializzazione di una nuova istanza
//        /// </summary>
//        /// <param name="emailTo">Indirizzo di posta elettronica a cui spedire il messaggio</param>
//        /// <param name="subject">Oggette del messaggio</param>
//        /// <param name="body">Corpo del messaggio in fomato: accetta anche testo formattato in html</param>
//        /// <param name="emailFrom">Indirizzo di posta elettronica usato per l'invio del messaggio</param>
//        /// <param name="lblMittente">Etichetta testuale visibile nel intestazione del messaggio</param>
//        public MailSender(string emailTo, string emailCC, string subject, string body, string emailFrom, string lblMittente)
//            : this(emailTo, subject, body, emailFrom, lblMittente)
//        {
//            EmailCC = emailCC;
//        }

//        /// <summary>
//        /// Metodo per l'invio del messaggio
//        /// </summary>
//        public void Send()
//        {
//            try
//            {
//                MailMessage messaggio = new MailMessage();

//                messaggio.From = new MailAddress(EmailFrom, LblMittente);
//                messaggio.To.Add(new MailAddress(EmailTo));
//                if (EmailCC != null) messaggio.CC.Add(new MailAddress(EmailCC));
//                messaggio.Subject = Subject;
//                messaggio.SubjectEncoding = System.Text.Encoding.UTF8;
//                messaggio.Body = Body;
//                messaggio.BodyEncoding = System.Text.Encoding.UTF8;
//                messaggio.IsBodyHtml = SendAsHtml;

//                SmtpClient smtpClient = new SmtpClient();
//                smtpClient.Host = this.SmtpHost;
//                smtpClient.Port = this.SmtpPort;
//                smtpClient.EnableSsl = this.EnableSsl;
                
//                if (SmtpUser != null && SmtpPassword != null && SmtpUser.Length > 0 && SmtpPassword.Length > 0)
//                {
//                    smtpClient.UseDefaultCredentials = false;
//                    smtpClient.Credentials = new System.Net.NetworkCredential(SmtpUser, SmtpPassword);
//                }

//                smtpClient.Send(messaggio);

//                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Info, string.Format("Spedita email con oggetto: \n'{0}'\n all'indirizzo '{1}' \n con testo: \n'{2}'", Subject, EmailTo, Body), "", "", callerObject: this);
//            }
//            catch (Exception ex)
//            {
//                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile inviare la mail " + ex.Message, "", "", "63", callerObject: this);
//            }
//        }

//    }
//}