﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using System.Collections.Generic;

namespace NetCms.Vertical
{
    /// <summary>
    /// La classe astratta ApplicationPage espone le proprietà di base per la creazione e la creazione e gestione di pagine per la costruzione di applicazioni verticali.
    /// </summary>
    public abstract class ApplicationPage
    {
        public abstract string PageTitle
        {
            get;
        }

        public virtual Breadcrumb Breadcrumb
        {
            get{ return null;}
        }

        public virtual Toolbar FrontToolbar
        {
            get { return null; }
        }

        public virtual NetCms.GUI.Icons.Icons PageIcon
        {
            get
            {
                return GUI.Icons.Icons.Page;
            }
        }

        /// <summary>
        /// Proprietà da implementare nella classe derivata per ottenere il Box Informazioni NetCms.GUI.Info.InfoBox della pagina.
        /// </summary>
        public abstract NetCms.GUI.Info.InfoBox InformationBox{ get; }
        
        /// <summary>
        /// Proprietà implementabile nella classe derivata per far si che il sistema di gestione della sicurezza controlli che 
        /// si abbiano i permessi per il criterio specificato.
        /// </summary>
        public virtual string Criteria
        {
            get { return null; }
        }

        public virtual List<string> OuterScripts
        {
            get
            {
                return _OuterScripts ?? (_OuterScripts = new List<string>());
            }
        }
        private List<string> _OuterScripts;

        public enum PageAvailableFor { Front, Back, Both }
        public enum PageLayouts { Default, Lite }

        public PageLayouts PageLayout { get; protected set; } 

        public PageAvailableFor PageAvailability
        {
            get
            {
                return _PageAvailability;
            }
            protected set
            {
                _PageAvailability = value;
            }
        }
        private PageAvailableFor _PageAvailability; 
       
        public bool DisableContent
        {
            get
            {
                return _DisableContent;
            }
            protected set
            {
                _DisableContent = value;
            }
        }
        private bool _DisableContent;

        /// <summary>
        /// Proprietà da implementare nella classe derivata per ottenere la Toolbar NetCms.GUI.Toolbar della pagina.
        /// Per evitare di utilizzarla e necessario restituire NULL
        /// </summary>
        public abstract Toolbar Toolbar { get; }

        /// <summary>
        /// Proprietà da implementare nella classe derivata per ottentere il WebControl da aggiungere al corpo pagina
        /// </summary>        
        public abstract WebControl Control { get; }

        protected ApplicationPage()
        {
            PageLayout = PageLayouts.Default;
        }

        public bool _AddScriptsToHead = true;
        public bool AddScriptsToHead
        {
            get { return _AddScriptsToHead; }
            set { _AddScriptsToHead = value; }
        }

        /// <summary>
        /// property AddScriptManager
        /// </summary>
        /// La proprietà booleana permette di appendere l'oggetto ScriptManager alla pagina
        public bool AddScriptManager { get; protected set; }
        /// <summary>
        /// property ScriptManager
        /// </summary>
        /// La proprietà definisce di default l'oggetto ScriptManager.
        /// E' possibile definire tramite set un custom ScriptManager.
        ///
        private ScriptManager _ScriptManager;
        public ScriptManager ScriptManager
        {
            get
            {
                if (_ScriptManager == null)
                {
                    _ScriptManager = new ScriptManager();
                }
                return _ScriptManager;
            } 
            protected set
            {
                _ScriptManager = value;
            }  
        }


        private bool _AddPingSessionControl = false;
        public virtual bool AddPingSessionControl
        {
            get { return _AddPingSessionControl; }
        }

        
    }

}
