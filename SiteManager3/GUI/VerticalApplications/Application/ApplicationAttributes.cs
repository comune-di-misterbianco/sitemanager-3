﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Vertical
{
    [System.AttributeUsage(System.AttributeTargets.Class)]
    public class ApplicationAttributes : System.Attribute
    {
        /// <summary>
        /// Indirizzo di reperimento della pagina
        /// Es: /default.aspx , /users/default.aspx , /users/edit/edit.aspx
        /// </summary>
        /// <param name="address">Stringa contenente l'indirizzo</param>
        public ApplicationAttributes(string systemname, int id)
        {
            this.SystemName = systemname;
            this.ID = id;
        }

        public string SystemName
        {
            get
            {
                return _SystemName;
            }
            private set
            {
                _SystemName = value;
            }
        }
        private string _SystemName; 

        public int ID
        {
            get
            {
                return _ID;
            }
            private set
            {
                _ID = value;
            }
        }
        private int _ID; 
    }

}
