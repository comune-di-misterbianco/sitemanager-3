﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Vertical
{
    public abstract class ApplicationPageStatusValidator
    {
        public abstract string ErrorMessage { get; }
        public abstract string ErrorTitle { get; }

        public abstract bool Validate();
    }
}
