﻿using LanguageManager.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Vertical
{
    public class PagesHandler
    {
        public static DynamicUrl.PageHandler DynamicUrlHandler
        {
            get
            {
                if (_DynamicUrlHandler == null)
                {
                    _DynamicUrlHandler = new DynamicUrl.PageHandler();
                }
                return _DynamicUrlHandler;
            }
        }
        private static DynamicUrl.PageHandler _DynamicUrlHandler;

        public NetCms.Vertical.ApplicationPage GetCurrentPage(System.Web.UI.Page page, StateBag viewState)
        {
            return DynamicUrlHandler.GetCurrentPage(page, viewState);
        }

        private static string _AbsoluteRoot;
        public static string AbsoluteRoot
        {
            get
            {
                if (_AbsoluteRoot == null)
                {
                    _AbsoluteRoot = string.Empty;
                    string RootPathValue = System.Web.Configuration.WebConfigurationManager.AppSettings["RootPath"].ToLower();
                    if (RootPathValue.Length > 0)
                        _AbsoluteRoot += RootPathValue.ToLower();
                }
                return _AbsoluteRoot;
            }
        }

        public static string AbsoluteWebRoot
        {
            get
            {
                return HttpContext.Current != null &&
                       HttpContext.Current.Items != null &&
                       HttpContext.Current.Items["VerticalAppCurrentAbsoluteWebRoot"] != null?
                       HttpContext.Current.Items["VerticalAppCurrentAbsoluteWebRoot"].ToString():
                       null;
            }
        }
        private static string _AbsolutWebRoot;

        public static void GoTo(System.Type type)
        {
            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                if (LanguageBusinessLogic.CurrentLanguage.Default_Lang > 0)
                    DynamicUrl.RedirectManager.GoTo(type, AbsoluteWebRoot);
                else
                    DynamicUrl.RedirectManager.GoTo(type, "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (AbsoluteWebRoot.StartsWith("/") ? "" : "/") + AbsoluteWebRoot);
            }
            else
                DynamicUrl.RedirectManager.GoTo(type, AbsoluteWebRoot);
        }

        public static string GetPageAddress(System.Type type)
        {
            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                if (LanguageBusinessLogic.CurrentLanguage.Default_Lang > 0)
                    return DynamicUrl.RedirectManager.GetPageAddress(type, AbsoluteWebRoot);
                else
                    return DynamicUrl.RedirectManager.GetPageAddress(type, "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (AbsoluteWebRoot.StartsWith("/")? "":"/") + AbsoluteWebRoot);
                
            }
            else
                return DynamicUrl.RedirectManager.GetPageAddress(type, AbsoluteWebRoot);



        }
    }

    public class Handler : IHttpHandlerFactory
    {
        public IHttpHandler GetHandler(HttpContext context, string requestType, string url, string pathTranslated)
        {
            var AbsoluteWebRoot = System.Web.Configuration.WebConfigurationManager.AppSettings["RootPath"].ToLower();
            string Url = AbsoluteWebRoot + "/default.aspx";
            HttpContext.Current.Items["VerticalAppCurrentAbsoluteWebRoot"] = AbsoluteWebRoot;
            return PageParser.GetCompiledPageInstance(Url, context.Server.MapPath(Url), context);
        }

        public void ReleaseHandler(IHttpHandler handler)
        {

        }

    }
}
