﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.GUI
{
    /// <summary>
    /// Rappresenta la classe astratta per la gestione delle NetCms.GUI.Toolbar
    /// </summary>
    public abstract class Toolbar:WebControl
    {
        private ToolbarButtonsCollection _Buttons;

        /// <summary>
        /// Ottiene la collezione NetCms.GUI.ToolbarButtonsCollection di oggetti NetCms.GUI.ToolbarButton
        /// </summary>
        public ToolbarButtonsCollection Buttons
        {
            get
            {
                return _Buttons;
            }
        }

        /// <summary>
        /// Inizializza una nuova istanza di NetCms.GUI.Toolbar. 
        /// </summary> 
        public Toolbar()
            : base(HtmlTextWriterTag.Ul)
        {
            _Buttons = new ToolbarButtonsCollection(); 
        }
		
        /// <summary>
        /// Metodo astratto da implementare nella classe derivata per il rendering del bottone
        /// </summary>
        /// <param name="button">Bottone</param>
        /// <returns>Ritorna il controllo web relativo al bottone</returns>
	    protected abstract WebControl RenderButton(ToolbarButton button);

        /// <summary>
        /// Genera l'evento PreRender
        /// </summary>
        /// <param name="e">Evento</param>
        /// <remarks>
        /// Questo metodo notifica al controllo server di effettuare tutte le operazioni preliminari al rendering prima di salvare lo stato di visualizzazione e di eseguire il rendering del contenuto.
        /// </remarks>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            foreach (ToolbarButton button in this.Buttons)
                this.Controls.Add(RenderButton(button));
        }
    }
}
