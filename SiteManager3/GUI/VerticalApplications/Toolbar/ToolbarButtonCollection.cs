using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace NetCms.GUI
{
    /// <summary>
    /// Rappresenta una collezione di NetCms.GUI.ToolbarButton
    /// </summary>
    public class ToolbarButtonsCollection : IEnumerable
    {
        private ToolbarButton ParentToolbarButton;
        private Dictionary<string, object> coll;
        public int Count { get { return coll.Count; } }
        /// <summary>
        /// Inizializza un nuova NetCms.GUI.ToolbarButton
        /// </summary>
        public ToolbarButtonsCollection()
        {
            coll = new Dictionary<string, object>();
        }

        /// <summary>
        /// Inizializza un nuova NetCms.GUI.ToolbarButton
        /// </summary>
        public ToolbarButtonsCollection(ToolbarButton parentToolbarButton)
        {
            coll = new Dictionary<string, object>();
            ParentToolbarButton = parentToolbarButton;
        }
        /// <summary>
        /// Aggiunge un oggetto NetCms.GUI.ToolbarButton alla NetCms.GUI.ToolbarButtonsCollection
        /// </summary>
        /// <param name="button">Oggetto NetCms.GUI.ToolbarButton</param>
        /// <param name="key">Chiave univoca d'identificazione</param>
        public void Add(ToolbarButton button, string key)
        {
            coll.Add(key, button);
            if (this.ParentToolbarButton != null) button.ParentButton = ParentToolbarButton;
        }
        /// <summary>
        /// Aggiunge un oggetto NetCms.GUI.ToolbarButton alla NetCms.GUI.ToolbarButtonsCollection
        /// </summary>
        /// <param name="button">Oggetto NetCms.GUI.ToolbarButton</param>
        public void Add(ToolbarButton button)
        {
            coll.Add(button.GetHashCode().ToString(), button);
            if (this.ParentToolbarButton != null) button.ParentButton = ParentToolbarButton;
        }
        /// <summary>
        /// Rimuove l'oggetto NetCms.GUI.ToolbarButton 
        /// </summary>
        /// <param name="key">Chiave d'identificazione</param>
        public void Remove(string key)
        {
            coll.Remove(key);
        }
        /// <summary>
        /// Rimuove l'oggetto NetCms.GUI.ToolbarButton
        /// </summary>
        /// <param name="button">NetCms.GUI.ToolbarButton da rimuovere</param>
        public void Remove(ToolbarButton button)
        {
            coll.Remove(button.GetHashCode().ToString());
        }

        /// <summary>
        /// Elimina tutti gli elementi contenuti nella NetCms.GUI.ToolbarButtonsCollection
        /// </summary>
        public void Clear()
        {
            coll.Clear();
        }
        /// <summary>
        /// Ottiene l'oggetto NetCms.GUI.ToolbarButton
        /// </summary>
        /// <param name="i">Indice della NetCms.GUI.ToolbarButtonsCollection</param>
        /// <returns>Il bottone NetCms.GUI.ToolbarButton</returns>
        public ToolbarButton this[int i]
        {
            get
            {
                ToolbarButton str = (ToolbarButton)coll[coll.Keys.ElementAt(i)];
                return str;
            }
        }
        
        /// <summary>
        /// Ottiene l'oggetto NetCms.GUI.ToolbarButton
        /// </summary>
        /// <param name="key">Chiave d'identificazione</param>
        /// <returns>Il bottone NetCms.GUI.ToolbarButton</returns>
        public ToolbarButton this[string key]
        {
            get
            {
                ToolbarButton val = (ToolbarButton)coll[key];
                return val;
            }
        }
        /// <summary>
        /// Verifica se la collezione NetCms.GUI.ToolbarButtonsCollection contiene l'oggetto identificato dalla chiave key
        /// </summary>
        /// <param name="key">Chiave d'identificazione</param>
        /// <returns>Ritorna un Booleano che indica se l'elemento identificato dalla key � presente nella collezione</returns>
        public bool Contains(string key)
        {
            return coll[key] != null;
        }

        /// <summary>
        /// Verifica se la collezione NetCms.GUI.ToolbarButtonsCollection contiene l'istanza dell'oggetto NetCms.GUI.ToolbarButton
        /// </summary>
        /// <param name="key">Chiave d'identificazione</param>
        /// <returns>Ritorna un Booleano che indica se l'oggetto NetCms.GUI.ToolbarButton � presente nella collezione</returns>
        public bool Contains(ToolbarButton button)
        {
            return coll[button.GetHashCode().ToString()] != null;
        }

        #region Enumerator
   
        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private ToolbarButtonsCollection Collection;
            public CollectionEnumerator(ToolbarButtonsCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}