using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms;
using System.Collections.Generic;
using LanguageManager.BusinessLogic;

namespace NetCms.GUI
{
    /// <summary>
    /// Rappresenta una istanza di un oggetto NetCms.Gui.ToolbarButton
    /// </summary>
    /// <remarks>
    ///  La Classe ToolbarButton permette di definire un bottone da utilizzare come collegamento ipertestuale. 
    /// </remarks>
    public class ToolbarButton
    {        
        /// <summary>
        /// Ottiene la classe css associata bottone
        /// </summary>
        public string CssClass
        {
            get { return _CssClass; }
            /*private*/ set { _CssClass = value; }
        }
        private string _CssClass;

        /// <summary>
        /// Ottiene la collezione NetCms.GUI.ToolbarButtonsCollection di oggetti NetCms.GUI.ToolbarButton
        /// </summary>
        public ToolbarButtonsCollection ChildButtons
        {
            get
            {
                if (_ChildButtons == null)
                    _ChildButtons = new ToolbarButtonsCollection(this);
                return _ChildButtons;
            }
        }
        private ToolbarButtonsCollection _ChildButtons;        

        public int Depth
        {
            get 
            { 
                return this.ParentButton != null ? this.ParentButton.Depth + 1 : 0 ;                 
            }  
           
        }

        public virtual bool Opened
        {
            get
            {
                if (OpenAlways) return true;
                else
                {
                    int index = this.Href.IndexOf('?');
                    string compHref = index > 0 ? this.Href.Substring(0, index) : this.Href;

                    bool opened = HttpContext.Current.Request.Url.LocalPath.ToLower().Contains(compHref.ToLower());

                    //check IndirectHref
                    if (!opened)
                    {
                        foreach (string indirectHerfItem in IndirectHref)
                        {
                            index = indirectHerfItem.IndexOf('?');
                            compHref = index > 0 ? indirectHerfItem.Substring(0, index) : indirectHerfItem;
                            opened = HttpContext.Current.Request.Url.LocalPath.ToLower().Contains(compHref.ToLower());
                            if (opened)
                                break;
                        }
                    }

                    if (!opened)
                        foreach (ToolbarButton button in this.ChildButtons)
                            if (button.Opened)
                            {
                                opened = true;
                                break;
                            }
                    return opened;
                }
            }
        }


        /// <summary>
        /// Verifica il percorso compreso di querystring
        /// </summary>
        public virtual bool OpenedWithQueryString
        {
            get
            {
                if (OpenAlways) return true;
                else
                {                                                                                                          
                    bool opened = HttpContext.Current.Request.Url.ToString().ToLower().Contains(System.Web.HttpUtility.UrlDecode(this.Href.ToLower()));

                    //check IndirectHref
                    if (!opened)
                    {
                        foreach (string indirectHerfItem in IndirectHref)
                        {                                                                                                  
                            opened = HttpContext.Current.Request.Url.ToString().ToLower().Contains(System.Web.HttpUtility.UrlDecode(indirectHerfItem.ToLower()));
                            if (opened)
                                break;
                        }
                    }

                    if (!opened)
                        foreach (ToolbarButton button in this.ChildButtons)
                            if (button.OpenedWithQueryString)
                            {
                                opened = true;
                                break;
                            }
                    return opened;
                }
            }
        }

        public bool OpenAlways { get; set; } 

        /// <summary>
        /// Ottiene il collegamento web da raggiungere alla pressione del bottone
        /// </summary>
        public string Href
        {
            get { return _Href; }
            protected set { _Href = value; }
        }
        private string _Href;

        /// <summary>
        /// Serve per legare altri collegamenti a pagine , non direttamente raggiungibili dal bottone ma
        /// che se visualizzati devono tenerere il bottone aperto
        /// </summary>
        public List<string> IndirectHref
        {
            get
            {
                if (_IndirectHref == null)
                    _IndirectHref = new List<string>();
                return _IndirectHref; 
            }
            protected set { _IndirectHref = value; }
        }
        private List<string> _IndirectHref;

        /// <summary>
        /// Se true, il collegamento verr� aperto in una nuova finestra se la toolbar gestisce tale funzionalit�
        /// </summary>
        public bool OpenLinkInNewTab
        { get; set; }
       
        /// <summary>
        /// Ottiene l'etichetta associata al bottone
        /// </summary>
        public string Label
        {
            get { return _Label; }
            private set { _Label = value; }
        }
        private string _Label;

        /// <summary>
        /// Ottiene l'icona associata al bottone
        /// </summary>
        public NetCms.GUI.Icons.Icons Icon
        {
            get { return _Icon; }
            private set { _Icon = value; }
        }
        private NetCms.GUI.Icons.Icons _Icon;
        
        /// <summary>
        /// Ottiene l'icona associata al bottone
        /// </summary>
        public string IconUrl
        {
            get { return _IconUrl; }
            private set { _IconUrl = value; }
        }
        private string _IconUrl;
        
        
        public ToolbarButton ParentButton
        {
            get { return _ParentButton; }
            set { _ParentButton = value; }
        }
        private ToolbarButton _ParentButton;

        /// <summary>
        /// La Classe ToolbarButton permette di definire un bottone da utilizzare come collegamento ipertestuale. 
        /// </summary>
        /// <param name="href">Il collegamento web da raggiungere alla pressione del bottone.
        /// Per utilizzare la funzionalit� ChildButtons � necessario passare percorsi completi, es: http://www.miosito.com/miacartella/miofile.aspx
        /// </param>
        /// <param name="icon">L'icona da associare al bottone</param>
        /// <param name="label">L'etichetta da associare al bottone</param>
        /// <example>
        /// Per istanziare un nuovo bottone usare la seguente sintassi:
        /// <code>
        /// ToolbarButton mybutton = new ToolbarButton("add.aspx", NetCms.GUI.Icons.Icons.Add, "Aggiungi");
        /// </code>
        /// </example>
        public ToolbarButton(string href, NetCms.GUI.Icons.Icons icon, string label)
        {
            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                if (LanguageBusinessLogic.CurrentLanguage.Default_Lang > 0)
                    this.Href = href;
                else
                {
                    if (LanguageBusinessLogic.UrlHasLanguageInfo(href))
                        this.Href = href;
                    else
                        this.Href = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (href.StartsWith("/") ? "" : "/") + href;
                }
               
            }
            else
                this.Href = href;
                        
            this.Icon = icon;
            this.Label = label;
        }
        /// <summary>
        /// La Classe ToolbarButton permette di definire un bottone da utilizzare come collegamento ipertestuale. 
        /// </summary>
        /// <param name="key">Inutile, � usato solo per retrocompatibilit�
        /// <param name="href">Il collegamento web da raggiungere alla pressione del bottone.
        /// Per utilizzare la funzionalit� ChildButtons � necessario passare percorsi completi, es: http://www.miosito.com/miacartella/miofile.aspx
        /// </param>
        /// <param name="icon">L'icona da associare al bottone</param>
        /// <param name="label">L'etichetta da associare al bottone</param>
        /// <example>
        /// Per istanziare un nuovo bottone usare la seguente sintassi:
        /// <code>
        /// ToolbarButton mybutton = new ToolbarButton("add.aspx", NetCms.GUI.Icons.Icons.Add, "Aggiungi");
        /// </code>
        /// </example>
        public ToolbarButton(string key,string href, NetCms.GUI.Icons.Icons icon, string label)
        {
            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                if (LanguageBusinessLogic.CurrentLanguage.Default_Lang > 0)
                    this.Href = href;
                else
                {
                    if (LanguageBusinessLogic.UrlHasLanguageInfo(href))
                        this.Href = href;
                    else
                        this.Href = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (href.StartsWith("/") ? "" : "/") + href;
                }

            }
            else
                this.Href = href;
            this.Icon = icon;
            this.Label = label;
        }
        /// <summary>
        /// La Classe ToolbarButton permette di definire un bottone da utilizzare come collegamento ipertestuale. 
        /// </summary>
        /// <param name="href">Il collegamento web da raggiungere alla pressione del bottone.</param>
        /// <param name="icon">L'icona da associare al bottone</param>
        /// <param name="label">L'etichetta da associare al bottone</param>
        /// <example>
        /// Per istanziare un nuovo bottone usare la seguente sintassi:
        /// <code>
        /// ToolbarButton mybutton = new ToolbarButton("add.aspx", NetCms.GUI.Icons.Icons.Add, "Aggiungi");
        /// </code>
        /// </example>
        public ToolbarButton(string href, string iconUrl, string label)
        {
            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                if (LanguageBusinessLogic.CurrentLanguage.Default_Lang > 0)
                    this.Href = href;
                else
                {
                    if (LanguageBusinessLogic.UrlHasLanguageInfo(href))
                        this.Href = href;
                    else
                        this.Href = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (href.StartsWith("/") ? "" : "/") + href;
                }

            }
            else
                this.Href = href;
            this.IconUrl = iconUrl;
            this.Label = label;
        }
        /// <summary>
        /// La Classe ToolbarButton permette di definire un bottone da utilizzare come collegamento ipertestuale. 
        /// </summary>
        /// <param name="key">Inutile, � usato solo per retrocompatibilit�
        /// <param name="href">Il collegamento web da raggiungere alla pressione del bottone.</param>
        /// <param name="icon">L'icona da associare al bottone</param>
        /// <param name="label">L'etichetta da associare al bottone</param>
        /// <example>
        /// Per istanziare un nuovo bottone usare la seguente sintassi:
        /// <code>
        /// ToolbarButton mybutton = new ToolbarButton("add.aspx", NetCms.GUI.Icons.Icons.Add, "Aggiungi");
        /// </code>
        /// </example>
        public ToolbarButton(string key, string href, string iconUrl, string label)
        {
            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                if (LanguageBusinessLogic.CurrentLanguage.Default_Lang > 0)
                    this.Href = href;
                else
                {
                    if (LanguageBusinessLogic.UrlHasLanguageInfo(href))
                        this.Href = href;
                    else
                        this.Href = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (href.StartsWith("/") ? "" : "/") + href;
                }

            }
            else
                this.Href = href;
            this.IconUrl = iconUrl;
            this.Label = label;
        }

        /// <summary>
        /// La Classe ToolbarButton permette di definire un bottone da utilizzare come collegamento ipertestuale. 
        /// </summary>
        /// <param name="icon">L'icona da associare al bottone</param>
        /// <param name="label">L'etichetta da associare al bottone</param>
        /// <example>
        /// Per istanziare un nuovo bottone usare la seguente sintassi:
        /// <code>
        /// ToolbarButton mybutton = new ToolbarButton("add.aspx", NetCms.GUI.Icons.Icons.Add, "Aggiungi");
        /// </code>
        /// </example>
        public ToolbarButton(NetCms.GUI.Icons.Icons icon, string label)
        {
            this.Icon = icon;
            this.Label = label;
        }

        /// <summary>
        /// La Classe ToolbarButton permette di definire un bottone da utilizzare come collegamento ipertestuale. 
        /// </summary>
         /// <param name="icon">L'icona da associare al bottone</param>
        /// <param name="label">L'etichetta da associare al bottone</param>
        /// <example>
        /// Per istanziare un nuovo bottone usare la seguente sintassi:
        /// <code>
        /// ToolbarButton mybutton = new ToolbarButton("add.aspx", NetCms.GUI.Icons.Icons.Add, "Aggiungi");
        /// </code>
        /// </example>
        public ToolbarButton(string iconUrl, string label)
        {
            this.IconUrl = iconUrl;
            this.Label = label;
        }

       
    }
}