﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.GUI
{
    /// <summary>
    /// La classe GenericToolbar implementa la gestione della Toolbar generiche da utilizzare nella pagina dell'applicazione.
    /// </summary>
    public class GenericToolbar : Toolbar
    {
        /// <summary>
        /// Inizializza una nuova istanza di NetCms.GUI.GenericToolbar. 
        /// </summary>
        /// <example>
        /// L'esempio descrive come istanziare un oggetto NetCms.GUI.GenericToolbar.
        /// <code>
        /// GenericToolbar myGenericToolbar = new GenericToolbar();
        /// </code>
        /// </example>
        public GenericToolbar()
        {
            //this.TagName = "ul";
        }

        /// <summary>
        /// Ritorna il nome tag html, relativo all'oggetto NetCms.GUI.GenericToolbar
        /// </summary>
        protected override string TagName
        {
            get
            {
                return "DIV";
            }
        }

        /// <summary>
        /// Rendering html dell'oggetto NetCms.GUI.ToolbarButton
        /// </summary>
        /// <returns>
        /// Ritorna il controllo HtmlGenericControl del bottone NetCms.GUI.ToolbarButton
        /// </returns>
        protected override WebControl RenderButton(ToolbarButton button)
        {
            WebControl li = new WebControl(System.Web.UI.HtmlTextWriterTag.Li);
            
            li.CssClass = "SideToolbarButton";

            if (button.CssClass != null && CssClass.Length > 0)
                li.CssClass += " " + CssClass;

            WebControl a;

            if (!string.IsNullOrEmpty(button.Href))
            {
                a = new WebControl(System.Web.UI.HtmlTextWriterTag.A);
                a.Attributes.Add("title", button.Label);
                a.Attributes.Add("href", button.Href);
                if (button.OpenLinkInNewTab)
                    a.Attributes.Add("target", "_blank");
            }
            else
                a = new WebControl(System.Web.UI.HtmlTextWriterTag.Span);

            string aText = "";
            if (button.IconUrl != null)
                aText += "<span style=\"background-image:url(" + button.IconUrl + ")\" >";
            else
            {
                NetCms.GUI.Icons.IconsIndexer indexer = new NetCms.GUI.Icons.IconsIndexer(this.Page);
                aText += "<span style=\"background-image:url(" + indexer.GetImageURL(button.Icon) + ")\" >";
            }
            aText += "<span class=\"SideToolbarButton_Label\">" + button.Label + "</span>";
            aText += "</span>";

            a.Controls.Add(new LiteralControl(aText));
            li.Controls.Add(a);

            if(button.ChildButtons!=null && button.ChildButtons.Count>0)
            {
                WebControl ul = new WebControl(System.Web.UI.HtmlTextWriterTag.Ul);
                foreach (ToolbarButton childButton in button.ChildButtons)  
                    ul.Controls.Add(RenderButton(childButton));
                li.Controls.Add(ul);
            }

            return li;
        }
    }
}
