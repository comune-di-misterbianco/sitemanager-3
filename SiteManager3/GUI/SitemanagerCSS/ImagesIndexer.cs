using System;
using System.Web;
using System.Data;
using System.Web.SessionState;
using System.Web.UI;

namespace NetCms.GUI.Css
{
    public class ImagesIndexer
    {
        public enum Images
        {
            JqueryTreeview_Ajax_Loader,
            JqueryTreeview_Plus,
            JqueryTreeview_Minus,
            JqueryTreeview_File,
            JqueryTreeview_Folder,
            JqueryTreeview_FolderClosed,
            JqueryTreeview_Black_Line,
            JqueryTreeview_Black,
            JqueryTreeview_Default,
            JqueryTreeview_Default_Line,
            JqueryTreeview_FamFamFam,
            JqueryTreeview_FamFamFam_Line,
            JqueryTreeview_Gray,
            JqueryTreeview_Gray_Line,
            JqueryTreeview_Red,
            JqueryTreeview_Red_Line,
            Header,
            LoadingAnimation,
            JQueryUI_001,
            JQueryUI_002,
            JQueryUI_003,
            JQueryUI_004,
            JQueryUI_005,
            JQueryUI_006,
            JQueryUI_007,
            JQueryUI_008,
            JQueryUI_009,
            JQueryUI_010,
            JQueryUI_011,
            JQueryUI_012,
            JQueryUI_013,
            Accept,
            Add,
            AlboPretorio,
            Alert,
            Arrowclose_Gif,
            Arrowclose_Png,
            Arrowopen,
            Arrow_Down,
            Arrow_Gif,
            Arrow_Gray_Gif,
            Arrow_Gray_Png,
            Arrow_Jpg,
            Arrow_Png,
            Arrow_Switch_Red,
            Arrow_Up,
            Arrow_Updown,
            AppzInstaller,
            Attach,
            Attach_Search,
            Atti,
            AttiAmministrativi,
            Bacheca,
            Bacheca2,
            Bacheca_Search,
            Back,
            BackHover,
            Backrow,
            Back_Disabled,
            Back_Search,
            Banca_Tempo,
            Bandi,
            Bandi_Search,
            Banners,
            Border_Header_Top,
            Brick,
            Building,
            Building_Add,
            Button,
            ButtonOn,
            Cancel,
            Cat,
            Cat_New,
            Chart_Pie,
            Check,
            Cms,
            Cms_Internet,
            Cms_Intranet,
            Contacts,
            Crm,
            Customforms,
            Dark_Table_Head,
            Date,
            Del_Jpg,
            Del_Png,
            Details,
            Details_Hover,
            Disabled,
            Dropback,
            Email_Groups,
            Email_Important,
            Email_In,
            Email_Lowp,
            Email_Open_Image,
            Email_Out,
            Error_Search,
            Eventi,
            Eventi_Search,
            Faq,
            Faq_Search,
            Film,
            Flash,
            Folderback,
            Folderopen,
            Folder_Add,
            Folder_Delete,
            Folder_Edit,
            Folder_Gif,
            Folder_Jpg,
            Folder_Move,
            Folder_Negate,
            Folder_New,
            Folder_Popup,
            Folder_Search,
            Folder_System,
            Forum,
            Forum_Search,
            Frontlink,
            Frontusers,
            Frontuser_Search,
            Fsi_Text,
            Galleria_Search,
            GestioneIstituti,
            Go,
            Grants,
            Grants_Search,
            Green,
            Green_Table_Head,
            Group,
            Groups,
            Gruppi_Search,
            Help_Icon,
            Home,
            HomeHover,
            Homepage_Gif,
            Homepage_Jpg,
            Home_Search,
            Hr,
            Ici_Jpg,
            Ici_Png,
            Ici_Search,
            Image,
            Immagine,
            Inherits,
            Inserthtml,
            Internet,
            Importer,
            Intranet,
            Intsx,
            InputBG,
            Join,
            Joinbottom,
            Key,
            Left_Search,
            Line,
            Link,
            Link_Search,
            ListeUtenti,
            Lock,
            Log,
            Login,
            Log_Search,
            Mail_Search,
            Messenger,
            Messaggistica,
            Meta_Search,
            Minus,
            MinusBottom,
            Modello,
            Modify,
            Modulistica_Search,
            Monitoring,
            Money,
            Netkey,
            NetService,
            News,
            Networks,
            Newsletter,
            Newspaper_Go,
            News_Popup,
            News_Search,
            Norecordgrant,
            Note,
            Offices,
            Office_Search,
            Overtitle,
            Page,
            Page_Gear,
            Phone,
            Phone_Edit,
            Phone_Send,
            Pictures,
            Plus,
            PlusBottom,
            Preview,
            Prop,
            Prop_Search,
            Publish,
            Questionari,
            Questionari_Search,
            Rassegna,
            Red,
            Redfolder,
            Redfolderopen,
            Right_Search,
            Root,
            Search,
            Searchbox_Bkg,
            Search_Search,
            Sf,
            Sideback_Gif,
            Sideback_Jpg,
            Sidebuttonback,
            Sideelem,
            Sideelem_Notice,
            Sidehideback,
            Sideline,
            Sidetitle,
            SideTitleBackDark,
            Sidetoolbarback,
            Sidetoolbarback2,
            Sidetoolbarbackhid,
            Side_Close,
            Side_Open,
            Side_Close_Dark,
            Side_Open_Dark,
            Sms,
            Sms_Search,
            Stampa,
            Stampa_Search,
            Star_Search,
            Stop,
            Tableback,
            Tdimg_Modifica,
            Thbg,
            Thbg2,
            Toolbarback,
            Toolbar_Arrow_Updown,
            Toolbar_Coldx_Add,
            Toolbar_Coldx_Del,
            Toolbar_Folder_Add,
            Toolbar_Folder_Delete,
            Toolbar_Folder_Edit,
            Toolbar_Footer_Add,
            Toolbar_Footer_Del,
            Toolbar_Mailq,
            Toolbar_Meta_Add,
            Toolbar_Services,
            Topback,
            Topsmenuback,
            Topsxmenu,
            Tp1,
            Tp2,
            Tp3,
            Tp4,
            Tp5,
            Trash,
            Trash_Search,
            Uncheck,
            User,
            Users,
            Usersctrselect_Gif,
            Usersctrselect_Jpg,
            Usersearch,
            Usersearchback,
            Usersearchbt,
            Usersearchbt_Hover,
            Usersearchleft,
            Usersearchreset,
            Usersearchreset_Hover,
            User_Suit,
            Utenti_Search,
            Voidgrant,
            Webcam,
            Webdoc,
            Webdoc_Search,
            Yellow,
            OverlayClose,
            OverlayPetrol,
            OverlayTransparent,
            OverlayWhite
        }

        private System.Web.UI.Page _SystemPage;
        public System.Web.UI.Page SystemPage
        {
            get
            {
                return _SystemPage;
            }
            private set { _SystemPage = value; }
        }

        public ImagesIndexer(System.Web.UI.Page page)
        {
            SystemPage = page;
        }

        public string GetImageURL(Images image)
        {
            switch (image)
            {
                case Images.JqueryTreeview_Ajax_Loader: return GetImageRealURL("images/jquerytreeview/ajax-loader.gif");
                case Images.JqueryTreeview_Plus: return GetImageRealURL("images/jquerytreeview/plus.gif");
                case Images.JqueryTreeview_Minus: return GetImageRealURL("images/jquerytreeview/minus.gif");
                case Images.JqueryTreeview_FolderClosed: return GetImageRealURL("images/jquerytreeview/folderclosed.gif");
                case Images.JqueryTreeview_Folder: return GetImageRealURL("images/jquerytreeview/folder.gif");
                case Images.JqueryTreeview_File: return GetImageRealURL("images/jquerytreeview/file.gif");
                case Images.JqueryTreeview_Black: return GetImageRealURL("images/jquerytreeview/treeview-black.gif");
                case Images.JqueryTreeview_Black_Line: return GetImageRealURL("images/jquerytreeview/treeview-black-line.gif");
                case Images.JqueryTreeview_Default: return GetImageRealURL("images/jquerytreeview/treeview-default.gif");
                case Images.JqueryTreeview_Default_Line: return GetImageRealURL("images/jquerytreeview/treeview-default-line.gif");
                case Images.JqueryTreeview_Gray: return GetImageRealURL("images/jquerytreeview/treeview-gray.gif");
                case Images.JqueryTreeview_Gray_Line: return GetImageRealURL("images/jquerytreeview/treeview-gray-line.gif");
                case Images.JqueryTreeview_Red: return GetImageRealURL("images/jquerytreeview/treeview-red.gif");
                case Images.JqueryTreeview_Red_Line: return GetImageRealURL("images/jquerytreeview/treeview-red-line.gif");
                case Images.JqueryTreeview_FamFamFam: return GetImageRealURL("images/jquerytreeview/treeview-famfamfam.gif");
                case Images.JqueryTreeview_FamFamFam_Line: return GetImageRealURL("images/jquerytreeview/treeview-famfamfam-line.gif");

                case Images.JQueryUI_001: return GetImageRealURL("images/jquery/ui001.png");
                case Images.JQueryUI_002: return GetImageRealURL("images/jquery/ui002.png");
                case Images.JQueryUI_003: return GetImageRealURL("images/jquery/ui003.png");
                case Images.JQueryUI_004: return GetImageRealURL("images/jquery/ui004.png");
                case Images.JQueryUI_005: return GetImageRealURL("images/jquery/ui005.png");
                case Images.JQueryUI_006: return GetImageRealURL("images/jquery/ui006.png");
                case Images.JQueryUI_007: return GetImageRealURL("images/jquery/ui007.png");
                case Images.JQueryUI_008: return GetImageRealURL("images/jquery/ui008.png");
                case Images.JQueryUI_009: return GetImageRealURL("images/jquery/ui009.png");
                case Images.JQueryUI_010: return GetImageRealURL("images/jquery/ui010.png");
                case Images.JQueryUI_011: return GetImageRealURL("images/jquery/ui011.png");
                case Images.JQueryUI_012: return GetImageRealURL("images/jquery/ui012.png");
                case Images.JQueryUI_013: return GetImageRealURL("images/jquery/ui013icons_cd0a0a_256x240.png");

                case Images.SideTitleBackDark: return GetImageRealURL("images/collapsable.sidetitlebackdark.png");
                case Images.Header: return GetImageRealURL("images/header.png");
                case Images.LoadingAnimation: return GetImageRealURL("images/loadingAnimation.gif");
                case Images.Accept: return GetImageRealURL("images/accept.png");
                case Images.Add: return GetImageRealURL("images/add.png");
                case Images.AlboPretorio: return GetImageRealURL("images/albopretorio.png");
                case Images.Alert: return GetImageRealURL("images/alert.png");
                case Images.Arrowclose_Gif: return GetImageRealURL("images/arrowclose_gif.gif");
                case Images.Arrowclose_Png: return GetImageRealURL("images/arrowclose_png.png");
                case Images.Arrowopen: return GetImageRealURL("images/arrowopen.gif");
                case Images.Arrow_Down: return GetImageRealURL("images/arrow_down.gif");
                case Images.Arrow_Gif: return GetImageRealURL("images/arrow_gif.gif");
                case Images.Arrow_Gray_Gif: return GetImageRealURL("images/arrow_gray_gif.gif");
                case Images.Arrow_Gray_Png: return GetImageRealURL("images/arrow_gray_png.png");
                case Images.Arrow_Jpg: return GetImageRealURL("images/arrow_jpg.jpg");
                case Images.Arrow_Png: return GetImageRealURL("images/arrow_png.png");
                case Images.Arrow_Switch_Red: return GetImageRealURL("images/arrow_switch_red.png");
                case Images.Arrow_Up: return GetImageRealURL("images/arrow_up.gif");
                case Images.Arrow_Updown: return GetImageRealURL("images/arrow_updown.png");
                case Images.Attach: return GetImageRealURL("images/attach.jpg");
                case Images.AppzInstaller: return GetImageRealURL("images/appzinstaller.png");
                case Images.Attach_Search: return GetImageRealURL("images/attach_search.jpg");
                case Images.Atti: return GetImageRealURL("images/atti.jpg");
                case Images.AttiAmministrativi: return GetImageRealURL("images/attiamministrativi.png");
                case Images.Bacheca: return GetImageRealURL("images/bacheca.jpg");
                case Images.Bacheca2: return GetImageRealURL("images/bacheca2.jpg");
                case Images.Bacheca_Search: return GetImageRealURL("images/bacheca_search.jpg");
                case Images.Back: return GetImageRealURL("images/back.png");
                case Images.Backrow: return GetImageRealURL("images/backrow.png");
                case Images.Back_Disabled: return GetImageRealURL("images/back_disabled.jpg");
                case Images.Back_Search: return GetImageRealURL("images/back_search.jpg");
                case Images.Banca_Tempo: return GetImageRealURL("images/banca_tempo.jpg");
                case Images.Bandi: return GetImageRealURL("images/bandi.jpg");
                case Images.Bandi_Search: return GetImageRealURL("images/bandi_search.jpg");
                case Images.Banners: return GetImageRealURL("images/banners.jpg");
                case Images.Border_Header_Top: return GetImageRealURL("images/border_header_top.gif");
                case Images.Brick: return GetImageRealURL("images/brick.png");
                case Images.Building: return GetImageRealURL("images/building.gif");
                case Images.Building_Add: return GetImageRealURL("images/building_add.gif");
                case Images.Button: return GetImageRealURL("images/button.png");
                case Images.ButtonOn: return GetImageRealURL("images/button_on.png");
                case Images.Cancel: return GetImageRealURL("images/cancel.png");
                case Images.Cat: return GetImageRealURL("images/cat.png");
                case Images.Cat_New: return GetImageRealURL("images/cat_new.png");
                case Images.Chart_Pie: return GetImageRealURL("images/chart_pie.gif");
                case Images.Check: return GetImageRealURL("images/check.png");
                case Images.Cms: return GetImageRealURL("images/cms.jpg");
                case Images.Cms_Internet: return GetImageRealURL("images/cms_internet.png");
                case Images.Cms_Intranet: return GetImageRealURL("images/cms_intranet.jpg");
                case Images.Contacts: return GetImageRealURL("images/contacts.jpg");
                case Images.Crm: return GetImageRealURL("images/crm.jpg");
                case Images.Customforms: return GetImageRealURL("images/customforms.jpg");
                case Images.Date: return GetImageRealURL("images/date.gif");
                case Images.Dark_Table_Head: return GetImageRealURL("images/dark_table_head.gif");
                case Images.Del_Jpg: return GetImageRealURL("images/del_jpg.jpg");
                case Images.Del_Png: return GetImageRealURL("images/del_png.png");
                case Images.Details: return GetImageRealURL("images/details.jpg");
                case Images.Details_Hover: return GetImageRealURL("images/details_hover.jpg");
                case Images.Disabled: return GetImageRealURL("images/disabled.jpg");
                case Images.Dropback: return GetImageRealURL("images/dropback.jpg");
                case Images.Email_Groups: return GetImageRealURL("images/email_groups.png");
                case Images.Email_Important: return GetImageRealURL("images/email_important.png");
                case Images.Email_In: return GetImageRealURL("images/email_in.png");
                case Images.Email_Lowp: return GetImageRealURL("images/email_lowp.png");
                case Images.Email_Open_Image: return GetImageRealURL("images/email_open_image.png");
                case Images.Email_Out: return GetImageRealURL("images/email_out.png");
                case Images.Error_Search: return GetImageRealURL("images/error_search.jpg");
                case Images.Eventi: return GetImageRealURL("images/eventi.jpg");
                case Images.Eventi_Search: return GetImageRealURL("images/eventi_search.jpg");
                case Images.Faq: return GetImageRealURL("images/faq.jpg");
                case Images.Faq_Search: return GetImageRealURL("images/faq_search.jpg");
                case Images.Film: return GetImageRealURL("images/film.gif");
                case Images.Flash: return GetImageRealURL("images/flash.png");
                case Images.Folderback: return GetImageRealURL("images/folderback.jpg");
                case Images.Folderopen: return GetImageRealURL("images/folderopen.gif");
                case Images.Folder_Add: return GetImageRealURL("images/folder_add.gif");
                case Images.Folder_Delete: return GetImageRealURL("images/folder_delete.gif");
                case Images.Folder_Edit: return GetImageRealURL("images/folder_edit.gif");
                case Images.Folder_Gif: return GetImageRealURL("images/folder_gif.gif");
                case Images.Folder_Jpg: return GetImageRealURL("images/folder_jpg.jpg");
                case Images.Folder_Move: return GetImageRealURL("images/folder_move.gif");
                case Images.Folder_Negate: return GetImageRealURL("images/folder_negate.gif");
                case Images.Folder_New: return GetImageRealURL("images/folder_new.gif");
                case Images.Folder_Popup: return GetImageRealURL("images/folder_popup.jpg");
                case Images.Folder_Search: return GetImageRealURL("images/folder_search.jpg");
                case Images.Folder_System: return GetImageRealURL("images/folder_system.gif");
                case Images.Forum: return GetImageRealURL("images/forum.jpg");
                case Images.Forum_Search: return GetImageRealURL("images/forum_search.jpg");
                case Images.Frontlink: return GetImageRealURL("images/frontlink.jpg");
                case Images.Frontusers: return GetImageRealURL("images/frontusers.png");
                case Images.Frontuser_Search: return GetImageRealURL("images/frontuser_search.jpg");
                case Images.Fsi_Text: return GetImageRealURL("images/fsi_text.png");
                case Images.Galleria_Search: return GetImageRealURL("images/galleria_search.jpg");
                case Images.GestioneIstituti: return GetImageRealURL("images/gestioneistituti.png");
                case Images.Go: return GetImageRealURL("images/go.png");
                case Images.Grants: return GetImageRealURL("images/grants.png");
                case Images.Grants_Search: return GetImageRealURL("images/grants_search.jpg");
                case Images.Green: return GetImageRealURL("images/Green.png");
                case Images.Green_Table_Head: return GetImageRealURL("images/green_table_head.gif");
                case Images.Group: return GetImageRealURL("images/group.jpg");
                case Images.Groups: return GetImageRealURL("images/groups.png");
                case Images.Gruppi_Search: return GetImageRealURL("images/gruppi_search.jpg");
                case Images.Help_Icon: return GetImageRealURL("images/help_icon.gif");
                case Images.Homepage_Gif: return GetImageRealURL("images/homepage_gif.gif");
                case Images.Homepage_Jpg: return GetImageRealURL("images/homepage_jpg.jpg");
                case Images.Home_Search: return GetImageRealURL("images/home_search.jpg");
                case Images.Hr: return GetImageRealURL("images/hr.jpg");
                case Images.Ici_Jpg: return GetImageRealURL("images/ici_jpg.jpg");
                case Images.Ici_Png: return GetImageRealURL("images/ici_png.png");
                case Images.Ici_Search: return GetImageRealURL("images/ici_search.jpg");
                case Images.Image: return GetImageRealURL("images/image.jpg");
                case Images.Immagine: return GetImageRealURL("images/immagine.png");
                case Images.Importer: return GetImageRealURL("images/importer.png");
                case Images.Inherits: return GetImageRealURL("images/inherits.png");
                case Images.InputBG: return GetImageRealURL("images/input-bg.gif");
                case Images.Inserthtml: return GetImageRealURL("images/inserthtml.gif");
                case Images.Internet: return GetImageRealURL("images/internet.jpg");
                case Images.Intranet: return GetImageRealURL("images/intranet.jpg");
                case Images.Intsx: return GetImageRealURL("images/intsx.jpg");
                case Images.Join: return GetImageRealURL("images/join.gif");
                case Images.Joinbottom: return GetImageRealURL("images/joinbottom.gif");
                case Images.Key: return GetImageRealURL("images/key.png");
                case Images.Left_Search: return GetImageRealURL("images/left_search.jpg");
                case Images.Line: return GetImageRealURL("images/line.gif");
                case Images.Link: return GetImageRealURL("images/link.jpg");
                case Images.Link_Search: return GetImageRealURL("images/link_search.jpg");
                case Images.Lock: return GetImageRealURL("images/lock.jpg");
                case Images.ListeUtenti: return GetImageRealURL("images/listeutenti.png");
                case Images.Log: return GetImageRealURL("images/log.png");
                case Images.Login: return GetImageRealURL("images/login.png");
                case Images.Log_Search: return GetImageRealURL("images/log_search.jpg");
                case Images.Mail_Search: return GetImageRealURL("images/mail_search.jpg");
                case Images.Messenger: return GetImageRealURL("images/messenger.jpg");
                case Images.Messaggistica: return GetImageRealURL("images/messaggistica.png");
                case Images.Meta_Search: return GetImageRealURL("images/meta_search.jpg");
                case Images.Minus: return GetImageRealURL("images/minus.gif");
                case Images.MinusBottom: return GetImageRealURL("images/minusbottom.gif");
                case Images.Modello: return GetImageRealURL("images/modello.jpg");
                case Images.Modify: return GetImageRealURL("images/modify.png");
                case Images.Modulistica_Search: return GetImageRealURL("images/modulistica_search.jpg");
                case Images.Monitoring: return GetImageRealURL("images/monitoring.png");
                case Images.Money: return GetImageRealURL("images/money.gif");
                case Images.Netkey: return GetImageRealURL("images/netkey.png");
                case Images.NetService: return GetImageRealURL("images/netservice.jpg");
                case Images.News: return GetImageRealURL("images/news.jpg");
                case Images.Newsletter: return GetImageRealURL("images/newsletter.jpg");
                case Images.Newspaper_Go: return GetImageRealURL("images/newspaper_go.png");
                case Images.News_Popup: return GetImageRealURL("images/news_popup.jpg");
                case Images.News_Search: return GetImageRealURL("images/news_search.jpg");
                case Images.Networks: return GetImageRealURL("images/networks.png");
                case Images.Norecordgrant: return GetImageRealURL("images/norecordgrant.png");
                case Images.Note: return GetImageRealURL("images/note.gif");
                case Images.Offices: return GetImageRealURL("images/offices.jpg");
                case Images.Office_Search: return GetImageRealURL("images/office_search.jpg");
                case Images.Overtitle: return GetImageRealURL("images/overtitle.png");
                case Images.Page: return GetImageRealURL("images/page.gif");
                case Images.Page_Gear: return GetImageRealURL("images/page_gear.gif");
                case Images.Phone: return GetImageRealURL("images/phone.png");
                case Images.Phone_Edit: return GetImageRealURL("images/phone_edit.png");
                case Images.Phone_Send: return GetImageRealURL("images/phone_send.png");
                case Images.Pictures: return GetImageRealURL("images/pictures.gif");
                case Images.Plus: return GetImageRealURL("images/plus.gif");
                case Images.PlusBottom: return GetImageRealURL("images/plusbottom.gif");
                case Images.Preview: return GetImageRealURL("images/preview.png");
                case Images.Prop: return GetImageRealURL("images/prop.png");
                case Images.Prop_Search: return GetImageRealURL("images/prop_search.jpg");
                case Images.Publish: return GetImageRealURL("images/publish.png");
                case Images.Questionari: return GetImageRealURL("images/questionari.jpg");
                case Images.Questionari_Search: return GetImageRealURL("images/questionari_search.jpg");
                case Images.Rassegna: return GetImageRealURL("images/rassegna.jpg");
                case Images.Red: return GetImageRealURL("images/Red.png");
                case Images.Redfolder: return GetImageRealURL("images/redfolder.gif");
                case Images.Redfolderopen: return GetImageRealURL("images/redfolderopen.gif");
                case Images.Right_Search: return GetImageRealURL("images/right_search.jpg");
                case Images.Root: return GetImageRealURL("images/root.gif");
                case Images.Search: return GetImageRealURL("images/search.jpg");
                case Images.Searchbox_Bkg: return GetImageRealURL("images/searchbox_bkg.gif");
                case Images.Search_Search: return GetImageRealURL("images/search_search.jpg");
                case Images.Sf: return GetImageRealURL("images/sf.jpg");
                case Images.Sideback_Gif: return GetImageRealURL("images/sideback_gif.gif");
                case Images.Sideback_Jpg: return GetImageRealURL("images/sideback_jpg.jpg");
                case Images.Sidebuttonback: return GetImageRealURL("images/sidebuttonback.png");
                case Images.Sideelem: return GetImageRealURL("images/sideelem.jpg");
                case Images.Sideelem_Notice: return GetImageRealURL("images/sideelem_notice.jpg");
                case Images.Sidehideback: return GetImageRealURL("images/sidehideback.jpg");
                case Images.Sideline: return GetImageRealURL("images/sideline.jpg");
                case Images.Sidetitle: return GetImageRealURL("images/sidetitle.jpg");
                case Images.Sidetoolbarback: return GetImageRealURL("images/sidetoolbarback.jpg");
                case Images.Sidetoolbarback2: return GetImageRealURL("images/sidetoolbarback2.jpg");
                case Images.Sidetoolbarbackhid: return GetImageRealURL("images/sidetoolbarbackhid.jpg");
                case Images.Side_Close: return GetImageRealURL("images/side_close.gif");
                case Images.Side_Open: return GetImageRealURL("images/side_open.gif");
                case Images.Side_Close_Dark: return GetImageRealURL("images/side_close_dark.gif");
                case Images.Side_Open_Dark: return GetImageRealURL("images/side_open_dark.gif");
                case Images.Sms: return GetImageRealURL("images/sms.jpg");
                case Images.Sms_Search: return GetImageRealURL("images/sms_search.jpg");
                case Images.Stampa: return GetImageRealURL("images/stampa.jpg");
                case Images.Stampa_Search: return GetImageRealURL("images/stampa_search.jpg");
                case Images.Star_Search: return GetImageRealURL("images/star_search.jpg");
                case Images.Stop: return GetImageRealURL("images/stop.png");
                case Images.Tableback: return GetImageRealURL("images/tableback.jpg");
                case Images.Tdimg_Modifica: return GetImageRealURL("images/tdimg_modifica.gif");
                case Images.Thbg: return GetImageRealURL("images/thbg.gif");
                case Images.Thbg2: return GetImageRealURL("images/thbg2.gif");
                case Images.Toolbarback: return GetImageRealURL("images/toolbarback.png");
                case Images.Toolbar_Arrow_Updown: return GetImageRealURL("images/toolbar_arrow_updown.png");
                case Images.Toolbar_Coldx_Add: return GetImageRealURL("images/toolbar_coldx_add.png");
                case Images.Toolbar_Coldx_Del: return GetImageRealURL("images/toolbar_coldx_del.png");
                case Images.Toolbar_Folder_Add: return GetImageRealURL("images/toolbar_folder_add.png");
                case Images.Toolbar_Folder_Delete: return GetImageRealURL("images/toolbar_folder_delete.png");
                case Images.Toolbar_Folder_Edit: return GetImageRealURL("images/toolbar_folder_edit.png");
                case Images.Toolbar_Footer_Add: return GetImageRealURL("images/toolbar_footer_add.png");
                case Images.Toolbar_Footer_Del: return GetImageRealURL("images/toolbar_footer_del.png");
                case Images.Toolbar_Mailq: return GetImageRealURL("images/toolbar_mailq.png");
                case Images.Toolbar_Meta_Add: return GetImageRealURL("images/toolbar_meta_add.png");
                case Images.Toolbar_Services: return GetImageRealURL("images/toolbar_services.png");
                case Images.Topback: return GetImageRealURL("images/topback.jpg");
                case Images.Topsmenuback: return GetImageRealURL("images/topsmenuback.jpg");
                case Images.Topsxmenu: return GetImageRealURL("images/topsxmenu.jpg");
                case Images.Tp1: return GetImageRealURL("images/tp1.jpg");
                case Images.Tp2: return GetImageRealURL("images/tp2.jpg");
                case Images.Tp3: return GetImageRealURL("images/tp3.jpg");
                case Images.Tp4: return GetImageRealURL("images/tp4.jpg");
                case Images.Tp5: return GetImageRealURL("images/tp5.jpg");
                case Images.Trash: return GetImageRealURL("images/trash.gif");
                case Images.Trash_Search: return GetImageRealURL("images/trash_search.jpg");
                case Images.Uncheck: return GetImageRealURL("images/uncheck.png");
                case Images.User: return GetImageRealURL("images/user.jpg");
                case Images.Users: return GetImageRealURL("images/users.png");
                case Images.Usersctrselect_Gif: return GetImageRealURL("images/usersctrselect_gif.gif");
                case Images.Usersctrselect_Jpg: return GetImageRealURL("images/usersctrselect_jpg.jpg");
                case Images.Usersearch: return GetImageRealURL("images/usersearch.jpg");
                case Images.Usersearchback: return GetImageRealURL("images/usersearchback.jpg");
                case Images.Usersearchbt: return GetImageRealURL("images/usersearchbt.jpg");
                case Images.Usersearchbt_Hover: return GetImageRealURL("images/usersearchbt_hover.jpg");
                case Images.Usersearchleft: return GetImageRealURL("images/usersearchleft.jpg");
                case Images.Usersearchreset: return GetImageRealURL("images/usersearchreset.jpg");
                case Images.Usersearchreset_Hover: return GetImageRealURL("images/usersearchreset_hover.jpg");
                case Images.User_Suit: return GetImageRealURL("images/user_suit.gif");
                case Images.Utenti_Search: return GetImageRealURL("images/utenti_search.jpg");
                case Images.Voidgrant: return GetImageRealURL("images/voidgrant.png");
                case Images.Webcam: return GetImageRealURL("images/webcam.gif");
                case Images.Webdoc: return GetImageRealURL("images/webdoc.jpg");
                case Images.Webdoc_Search: return GetImageRealURL("images/webdoc_search.jpg");
                case Images.Yellow: return GetImageRealURL("images/Yellow.png");

                //overlay

                case Images.OverlayClose: return GetImageRealURL("images/overlay.close.png");
                case Images.OverlayPetrol: return GetImageRealURL("images/overlay.petrol.png");
                case Images.OverlayTransparent: return GetImageRealURL("images/overlay.transparent.png");
                case Images.OverlayWhite: return GetImageRealURL("images/overlay.white.png");

            }

            return string.Empty;
        }

        public string GetImageRealURL(string image)
        {
            return Configurations.Paths.BackOfficeSkinPath + image.ToLower();
        }
    }

}