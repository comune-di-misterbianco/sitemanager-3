using System;
using System.Web;
using System.Data;
using System.Web.SessionState;
using System.Web.UI;


[assembly: WebResource("SitemanagerScripts.scripts.clock.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.codaslider.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.dtree.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.floating_popup.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.freetextjsintegration.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.homepage.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.jquery.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.jquery_ui.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.jquery_boot.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.jquery_cookie.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.jquery_treeview.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.jquery_treeview_async.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.jquery.treeview.edit.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.jquery.treeview.sortable.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.jquery_localscroll.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.jquery_scrollto.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.jquery_serialscroll.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.jscripts.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.static_popup.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.xtreeview.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.thickbox.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.jquery_tools.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.htmlcode.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.jquery.capSlide.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.jquery.mCustomScrollbar.js", "text/javascript")]
[assembly: WebResource("SitemanagerScripts.scripts.jquery.mousewheel.min.js", "text/javascript")]

namespace NetCms.GUI.Scripts
{
    public class ScriptsIndexer
    {
        public enum Scripts
        {
            JScripts,
            Clock,
            CodaSlider,
            Floating_Popup,
            Homepage,
            Static_Popup,
            FreeTextJsIntegration,
            DTree,
            xTreeView,
            Jquery,
            Jquery_UI,
            Jquery_Boot,
            Jquery_Tools,
            Jquery_Treeview,
            Jquery_Treeview_Async,
            Jquery_Treeview_Edit,
            Jquery_Treeview_Sortable,
            Jquery_LocalScroll,
            Jquery_ScrollTo,
            Jquery_SerialScroll,
            Jquery_Cookie,
            Thickbox,
            HtmlCode,
            Jquery_CapSlide,
            Jquery_mCustomScrollbar,
            Jquery_MouseWheel_min
        }

        private System.Web.UI.Page _SystemPage;
        public System.Web.UI.Page SystemPage
        {
            get
            {
                return _SystemPage;
            }
            private set { _SystemPage = value; }
        }

        public ScriptsIndexer(System.Web.UI.Page page)
        {
            SystemPage = page;
        }

        public string GetScriptURL(Scripts script)
        {            
            if (script != Scripts.Jquery)
                return Configurations.Paths.AbsoluteRoot + "/scripts/admin/" + script.ToString().ToLower() + ".js";
            else
            {
                return Configurations.Scripts.JQuery.Path;                               
            }
        }

    }

}