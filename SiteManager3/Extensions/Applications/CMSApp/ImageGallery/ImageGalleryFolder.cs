﻿using System;
using System.IO;
using System.Data;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using System.Xml;
using NetForms;
using NetCms.Structure.WebFS;
using NetCms.Grants;
using NetCms.Structure.Applications.Homepage;

namespace NetCms.Structure.Applications.ImageGallery
{
    public partial class ImageGalleryFolder : StructureFolder
    {
        private NetCms.Structure.Applications.Application _RelativeApplication;
        public override NetCms.Structure.Applications.Application RelativeApplication
        {
            get
            {
                if (_RelativeApplication == null)
                    _RelativeApplication = Applications.ApplicationsPool.ActiveAssemblies[ImageGalleryApplication.ApplicationID.ToString()];
                return _RelativeApplication;
            }
        }

        //public bool AllowMoveLoaded;
        //public bool _AllowMove;
        public override bool AllowMove
        {
            get
            {   
                return RelativeApplication.EnableMove;
            }
        }

        public override ToolbarButtonsCollection ToolbarButtons
        {
            get
            {
                ToolbarButtonsCollection _ToolbarButtons = base.ToolbarButtons;

                if (this.Criteria[CriteriaTypes.Create].Allowed)
                    _ToolbarButtons.Add(new NetCms.Structure.Applications.ApplicationToolbarButton("gallery_new.aspx?folder=" + ID, NetCms.GUI.Icons.Icons.Image_Add, "Importa nuova immagine", this.StructureNetwork, this.RelativeApplication));
                return _ToolbarButtons;

            }
        }

        public override bool ReviewEnabled
        {
            get
            {
                return this.RelativeApplication.EnableReviews;
            }
        }

        public ImageGalleryFolder()
            : base()
        { }
        public ImageGalleryFolder(Networks.NetworkKey network)
            : base(network) { }

        public override void addSearchResult(StructureTable table, StructureFolder.StandardSearchFields pars)
        {
            base.addSearchResult(table, pars);

        }


        protected override void getTableDocsView(StructureTable table)
        {
            if (PageData.OuterScripts.Length == 0)
            {
                string str = "\n   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + PageData.RootDist + "css/titlepop.css\" />\n";
                str += "   <script type=\"text/javascript\" src=\"" + PageData.RootDist + "scripts/titlepop.js\" ></script>\n";
                PageData.OuterScripts = str;
            }
            base.getTableDocsView(table);
        }




        //protected override int CreateDocContent(DataRow docRecord, DataSet sourceDB, string sourceFileName, params NetCms.Importer.Importer[] imp)
        //{
        //    string insertInto = "INSERT INTO " + this.Network.SystemName + "_docs_imagegallery ";
        //    string insertIntoFields = "";
        //    string insertIntoValues = "";
        //    foreach (DataColumn col in docRecord.Table.Columns)
        //    {
        //        if (col.ColumnName.ToLower().EndsWith("_image") && !col.ColumnName.ToLower().StartsWith("id_"))
        //        {
        //            string comma = insertIntoFields.Length > 0 ? "," : "";

        //            insertIntoFields += comma + col.ColumnName;

        //            string value = docRecord[col.ColumnName].ToString();

        //            if (value != "NULL")
        //                insertIntoValues += comma + "'" + value.Replace("'", "''") + "'";
        //            else
        //                insertIntoValues += comma + value.Replace("'", "''");
        //            //insertIntoValues += comma + "'" + value.Replace("'", "''") + "'";
        //            //break;
        //        }
        //    }
        //    insertInto += "(" + insertIntoFields + ") VALUES (" + insertIntoValues + ")";
        //    int result = this.Network.Connection.ExecuteInsert(insertInto);
        //    if (result==-1)
        //        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile inserire i dati dell'immagine durante l'importazione", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "105");
        //    return result;
        //}

        public override bool DeleteModuloApplicativo(NHibernate.ISession session)
        {
            return HomePageBusinessLogic.DeleteModuliApplicativiByFolder(this.ID, session);
        }

        public override bool DeleteSomething(NHibernate.ISession session)
        {
            return true;
        }
    }
}
