﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using System.IO;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NHibernate;
using NetService.Utility.ValidatedFields;
using System.Collections.Generic;
using System.Linq;

namespace NetCms.Structure.Applications.ImageGallery
{
    public class ImageGalleryReview : NetCms.Structure.WebFS.DocumentReviewPage
    {
        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Bell_Add; }
        }
        public override string PageTitle
        {
            get { return "Nuova Revisione '" + this.CurrentDocument.Nome + "'"; }
        }

        private int ImageID
        {
            get
            {
                return this.CurrentDocument.Content;
            }
        }

        public ImageGalleryReview()
        {
            if (this.CurrentDocument == null)
            {
                //Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Immagine richiesta dall'utente non trovata", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                //HttpContext.Current.NetCms.Diagnostics.Diagnostics.Redirect("gallery_error.aspx");
            }
        }

        //private int SmallThumbSize
        //{
        //    get
        //    {
        //        string attribute = Configurations.XmlConfig.GetAttribute("/Portal/configs/Networks/" + this.CurrentFolder.StructureNetwork.SystemName + "/applications/" + this.CurrentFolder.RelativeApplication.SystemName + "/thumbsize", "small");
        //        int.TryParse(attribute, out _SmallThumbSize);
        //        return _SmallThumbSize;
        //    }
        //}
        //private int _SmallThumbSize;

        //private int BigThumbSize
        //{
        //    get
        //    {
        //        string attribute = Configurations.XmlConfig.GetAttribute("/Portal/configs/Networks/" + this.CurrentFolder.StructureNetwork.SystemName + "/applications/" + this.CurrentFolder.RelativeApplication.SystemName + "/thumbsize", "big");
        //        int.TryParse(attribute, out _BigThumbSize);
        //        return _BigThumbSize;
        //    }
        //}
        //private int _BigThumbSize;

        //private Uploadify.Uploadify uploadField;
        //public Uploadify.Uploadify UploadField
        //{
        //    get
        //    {
        //        if (uploadField == null)
        //        {
        //            uploadField = new Uploadify.Uploadify("FileUp_" + this.ImageID,
        //                "*.jpg; *.png; *.bmp; *.gif; *.tif; *.jpeg",
        //                "Allegati trasferibili: *.jpg; *.png; *.bmp; *.gif; *.tif; *.jpeg",
        //                "Allega",
        //                NetCms.Configurations.Paths.AbsoluteRoot + "/charts/uploader.ashx",
        //                NetCms.Configurations.Paths.AbsoluteRoot + "/charts/deleteFile.ashx",
        //                NetCms.Configurations.Paths.AbsoluteRoot + "/repository/imagegallery/tmp/" + NetCms.Users.AccountManager.CurrentAccount.UserName,
        //                NetCms.Configurations.Scripts.SwfObject.Path,
        //                NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/shared/uploadify/",
        //                false,
        //                true);
        //            uploadField.ID = "FileUpload";
        //            uploadField.UploadLimit = 1;
        //            uploadField.QueueSizeLimit = 1;

        //        }
        //        return uploadField;
        //    }
        //}

        private List<FileUploadedV2> _FileUploaded;
        private List<FileUploadedV2> FileUploaded
        {
            get
            {
                if (_FileUploaded == null)
                {
                    _FileUploaded = new List<FileUploadedV2>();
                }

                return _FileUploaded;

            }
        }

        private string tmpFolderImagesRepository;
        public string TmpFolderImagesRepository
        {
            get
            {
                if (tmpFolderImagesRepository == null)
                {
                    string tmpPath = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/imagegallery/tmp/" + NetCms.Users.AccountManager.CurrentAccount.ID;
                    tmpFolderImagesRepository = tmpPath;

                    tmpPath = HttpContext.Current.Server.MapPath(tmpPath);
                    if (!Directory.Exists(tmpPath))
                        Directory.CreateDirectory(tmpPath);
                }
                return tmpFolderImagesRepository;
            }
        }

        private VfMultiUpload2 uploadField;
        public VfMultiUpload2 UploadField
        {
            get
            {
                if (uploadField == null)
                {
                    uploadField = new VfMultiUpload2("allegati",
                                                    "Documenti allegati",
                                                    TmpFolderImagesRepository,
                                                    null,
                                                    new String[] { "jpg", "Jpg", "png", "gif", "jpeg", "svg" },
                                                    1,
                                                    false,
                                                    FileUploaded
                                                    );

                    uploadField.Mode = VfMultiUpload2.FieldMode.FileSystemWithJavascript;
                    uploadField.Required = false;
                    uploadField.BindField = false;
                    uploadField.DefaultValue = FileUploaded;
                    uploadField.DisplayFileList = false;
                    // uploadField.delete.Click += AllegatoDelete_Click;
                    //uploadField.delete.Attributes["type"] = "button";
                    uploadField.delete.UseSubmitBehavior = false;


                    uploadField.AllowDelete = true; //?


                }
                return uploadField;
            }
        }

        private NetForm _Form;
        public NetForm Form
        {
            get
            {
                if (_Form == null)
                {
                    _Form = new NetForm();
                    _Form.CssClass = "imageform";
                    _Form.SubmitLabel = "Salva Revisione";
                }
                return _Form;
            }
        }

        private NetFormTable _ContentTable;
        public NetFormTable ContentTable
        {
            get
            {
                if (_ContentTable == null)
                {
                    _ContentTable = ImageGalleryDocument.DocsContentFormTable(PageData);
                    _ContentTable.CssClass = "netformtable_contenttable";
                }
                return _ContentTable;
            }
        }

        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "container-fluid";

            NetExternalField content;

            NetFormTable ImageForm;

            if (ReviewID == 0)
                ImageForm = new NetFormTable("imagegallery_docs_contents", "id_Image", this.CurrentFolder.StructureNetwork.Connection);
            else
                ImageForm = new NetFormTable("imagegallery_docs_contents", "id_Image", this.CurrentFolder.StructureNetwork.Connection, this.ContentID);

            ImageForm.CssClass = "imagegallery_contentfield";

            Form.CssClass = "imagegalleryform";

            Form.AddTable(ContentTable);
            Form.AddTable(ImageForm);

            content = new NetExternalField("docs_contents", "id_Image");
            ImageForm.ExternalFields.Add(content);

            

            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            div.Controls.Add(fieldset);

            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerText = "Nuova revisione";
            fieldset.Controls.Add(legend);

            //fieldset.Controls.Add(errors);

            HtmlGenericControl contentDiv = new HtmlGenericControl("div");
            contentDiv.Attributes["class"] = "contentForm imagegalleryform reviewform col-md-12";
            contentDiv.Controls.Add(ContentTable.getForm());
            contentDiv.Controls.Add(ImageForm.getForm());
            contentDiv.Controls.Add(UploadField);

            HtmlGenericControl pButtons = new HtmlGenericControl("p");
            pButtons.Attributes["class"] = "buttonscontainer";

            Button salva_btn = new Button();
            salva_btn.Text = "Salva Revisione";
            salva_btn.ID = "send";
            salva_btn.CssClass = "button";
            salva_btn.Click += Salva_btn_Click;

            pButtons.Controls.Add(salva_btn);

            contentDiv.Controls.Add(pButtons);

            WebControl row = new WebControl(HtmlTextWriterTag.Div);
            row.CssClass = "row";

            row.Controls.Add(contentDiv);            

            fieldset.Controls.Add(row);

            div.Controls.Add(fieldset);

            return div;
        }

        private void Salva_btn_Click(object sender, EventArgs e)
        {
            string errors = "";
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {

                    errors = Form.serializedInsert(HttpContext.Current.Request.Form);

                    if (Form.LastOperation == NetForm.LastOperationValues.OK)
                    {
                        ImageGalleryDocContent imageDocContent = ImageGalleryBusinessLogic.GetById(ContentTable.LastInsert, sess);
                        DocumentLang doc_lang = DocumentBusinessLogic.GetDocLangById(DocLang, sess);

                        DocumentOverview.insertReview(doc_lang, imageDocContent, this.ContentID, PageData, sess);


                        // aggiunta di ulteriore allegati trasferiti 
                        IList<FileUploadedV2> fileList = (IList<FileUploadedV2>)UploadField.PostbackValueObject;
                        if (fileList != null && fileList.Any())
                        {
                            foreach (FileUploadedV2 allegato in fileList)
                            {
                                string fileNameCorrect = NetCms.Connections.Connection.FilterString(allegato.FileName);
                                fileNameCorrect = fileNameCorrect.Replace("''", "-").Replace(" ", "-");

                                string source = TmpFolderImagesRepository;
                                source = System.Web.HttpContext.Current.Server.MapPath(source);

                                if (File.Exists(source + "\\" + allegato.FileName))
                                {
                                    NetUtility.ImageFilters image = new NetUtility.ImageFilters(source + "\\" + allegato.FileName);

                                    string destination = NetCms.Configurations.Paths.AbsoluteRoot + "/" + Networks.NetworksManager.CurrentActiveNetwork.Paths.Network.RootPath + CurrentDocument.Folder.Path + "/";
                                    destination = System.Web.HttpContext.Current.Server.MapPath(destination);
                                    //string path = Networks.NetworksManager.CurrentActiveNetwork.Paths.Network.RootPath + this.CurrentDocument.Folder.Path + "/";

                                    if (File.Exists(destination + fileNameCorrect))
                                    {
                                        string file_rinominato = "";

                                        FileInfo file_info = new FileInfo(destination + fileNameCorrect);
                                        string ext = file_info.Extension;
                                        string nome_file = file_info.Name;

                                        nome_file = nome_file.Replace(ext, "");
                                        ext = ext.Replace(".", "");

                                        int tab = DocumentBusinessLogic.GetNumberOfInstanceFile(nome_file, ext, ImageGalleryApplication.ApplicationID, sess); // ??
                                        int valore = (tab + 1);

                                        if (tab != 0)
                                        {
                                            valore = tab + 1;
                                            file_rinominato = nome_file + "(" + valore.ToString() + ")" + "." + ext;//nome[dim - 1];
                                        }
                                        else
                                        {
                                            file_rinominato = nome_file + "." + ext;
                                        }

                                        while (File.Exists(destination + file_rinominato))
                                        {
                                            valore++;
                                            file_rinominato = nome_file + "(" + valore.ToString() + ")" + "." + ext;//nome[1]
                                        }


                                        File.Copy(source + "\\" + allegato.FileName, destination + file_rinominato);
                                        // associazione nuovo file al docContent
                                        imageDocContent.FileName_Image = file_rinominato;
                                        imageDocContent.Height_Image = image.Height;
                                        imageDocContent.Width_Image = image.Width;
                                        imageDocContent.Type_Image = file_rinominato.Substring(file_rinominato.LastIndexOf('.') + 1, 3);

                                        ImageGalleryBusinessLogic.SaveOrUpdateImageDocsContent(imageDocContent, sess);
                                    }
                                    else
                                    {
                                        File.Copy(source + "\\" +allegato.FileName, destination + fileNameCorrect); // file move mi da un errore di blocco da altro processo
                                        
                                        // associazione nuovo file al docContent
                                        imageDocContent.FileName_Image = fileNameCorrect;
                                        imageDocContent.Height_Image = image.Height;
                                        imageDocContent.Width_Image = image.Width;
                                        imageDocContent.Type_Image = fileNameCorrect.Substring(fileNameCorrect.LastIndexOf('.') + 1, 3);

                                        ImageGalleryBusinessLogic.SaveOrUpdateImageDocsContent(imageDocContent, sess);

                                    }

                                    this.CurrentFolder.Invalidate();
                                    //File.Delete(source + "\\" + allegato.FileName);
                                }

                            }

                        }

                        tx.Commit();
                        string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha salvato una nuova revisione di rassegna stampa'" + this.CurrentDocument.Nome + "' relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    


                    }
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    NetCms.GUI.PopupBox.AddMessage("Non è stato possibile salvare questa revisione a causa di un problema tecnico. Riprovare ad effettuare l'inserimento, se il problema persiste contattare l'assistenza tecnica.", NetCms.GUI.PostBackMessagesType.Error);
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile creare la revisione dell'immagine di gallerie", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "50", ex);
                }
                NetCms.GUI.PopupBox.AddSessionMessage(errors, NetCms.GUI.PostBackMessagesType.Normal, Form.LastOperation == NetForm.LastOperationValues.OK ? true : false);
            }
        }

        
        //funzione che controlla se il file con quel nome è gia presente tra i doc nel database e se è così lo cambia per evitare di sovrascrivere il file. 
        //private string FileNameController(string savepath, string justfilename, string fileext)
        //{
        //    string file_rinominato = "";
        //    using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
        //    using (ITransaction tx = sess.BeginTransaction())
        //    {
        //        try
        //        {
        //            FileInfo file_info = new FileInfo(savepath + justfilename + "." + fileext);
        //            string fileNameCorrect = justfilename + "." + fileext;
        //            if (file_info.Exists)
        //            {
        //                string[] nome = fileNameCorrect.Split('.');
        //                int dim = nome.Length;


        //                for (int indice = 0; indice <= dim - 2; indice++)
        //                {
        //                    if (indice == (dim - 2))
        //                        file_rinominato += nome[indice];
        //                    else
        //                        file_rinominato += nome[indice] + ".";
        //                }

        //                int tab = DocumentBusinessLogic.GetNumberOfInstanceFile(file_rinominato, nome[dim - 1], ImageGalleryApplication.ApplicationID, sess);
        //                tx.Commit();
        //                string valore = (tab + 1).ToString();
        //                file_rinominato = file_rinominato + "(" + valore + ")" + "." + nome[dim - 1];

        //                //return savepath + file_rinominato;
        //            }
        //            else
        //            {
        //                file_rinominato = justfilename + "." + fileext;
        //                //return savepath + justfilename + "." + fileext;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            tx.Rollback();
        //            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
        //        }

        //    }
        //    return savepath + file_rinominato;
        //}

        //private void Trasferisci_Click(object sender, EventArgs e)
        //{
        //    //NetUtility.RequestVariable Descrizione = new NetUtility.RequestVariable("Descrizione", HttpContext.Current.Request.Form);
        //    //NetUtility.RequestVariable Titolo = new NetUtility.RequestVariable("titolo", HttpContext.Current.Request.Form);

        //    string lista_allegati = ""; //PageData.Request["file_list_" + UploadField.ID];

        //    if (!string.IsNullOrEmpty(lista_allegati))
        //    {

        //        char split_simbol = ';';
        //        string[] allegati = lista_allegati.Split(split_simbol);

        //        string FileName;
        //        string JustFileName = "";
        //        string FileExt = ""; ;

        //        //string UploadedFile = uploadform.PostedFile.FileName.Replace(" ", "_"); ;
        //        //int ExtractPos = UploadedFile.LastIndexOf("\\") + 1;

        //        //to retrieve only Filename from the complete path
        //        //FileName = UploadedFile.Substring(ExtractPos, UploadedFile.Length - ExtractPos).ToLower();

        //        //metto allegati di 0 perchè posso fare l'upload di un solo file visto che la revisione la si fa di una sola immagine per volta.
        //        FileName = allegati[0];

        //        JustFileName = FileName.Substring(0, FileName.LastIndexOf('.'));
        //        FileExt = FileName.Substring(FileName.LastIndexOf('.') + 1, (FileName.Length) - (FileName.LastIndexOf('.') + 1));

        //        if (IsImageFile(FileExt))
        //        {
        //            string SavePath = this.CurrentFolder.StructureNetwork.Paths.AbsoluteFrontRoot + this.CurrentFolder.Path + "/";
        //            SavePath = HttpContext.Current.Server.MapPath(SavePath);
        //            string TargetPath = "";//SavePath + FileName;                    

        //            string path = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/imagegallery/tmp/" + NetCms.Users.AccountManager.CurrentAccount.UserName + "/" + FileName;
        //            path = System.Web.HttpContext.Current.Server.MapPath(path);

        //            TargetPath = FileNameController(SavePath, JustFileName, FileExt);
        //            FileName = TargetPath.Substring(TargetPath.LastIndexOf('\\') + 1);
        //            //if (!file.Exists)
        //            //{
        //            //uploadform.SaveAs(TargetPath);
        //            File.Move(path, TargetPath);
        //            NetUtility.ImageFilters image = new NetUtility.ImageFilters(TargetPath);
        //            //Prima l'anteprima piccola 
        //            //image.NameOffset = "tmb";
        //            //if (image.Height > SmallThumbSize || image.Width > SmallThumbSize)
        //            //    image.CreateThumbnail(SmallThumbSize);
        //            //else
        //            //    image.Save();
        //            //string ThumbName = image.ThumbName;
        //            ////Dopo  l'anteprima grande
        //            //image.NameOffset = "preview";
        //            //if (image.Height > BigThumbSize || image.Width > BigThumbSize)
        //            //    image.CreateThumbnail(BigThumbSize);
        //            //else
        //            //    image.Save();
        //            //string PreviewName = image.ThumbName;

        //            Document document = new ImageGalleryDocument();
        //            ImageGalleryDocContent docFile = new ImageGalleryDocContent();

        //            #region Creo il contenuto

        //            docFile.FileName_Image = FileName;
        //            docFile.Height_Image = image.Height;
        //            docFile.Width_Image = image.Width;
        //            docFile.Type_Image = FileExt;
        //            //docFile.ThumbFile_Image = ThumbName;
        //            //docFile.PreviewFile_Image = PreviewName;

        //            #endregion
        //            //document = DocumentBusinessLogic.SaveOrUpdateDocument(document);//sess
        //            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
        //            using (ITransaction tx = sess.BeginTransaction())
        //            {
        //                try
        //                {
        //                    ImageGalleryDocContent imagecontent = ImageGalleryBusinessLogic.SaveOrUpdateImageDocsContent(docFile, sess);//, sess
        //                    this.CurrentFolder.Invalidate();

        //                    if (this.CurrentFolder.EnableRss)
        //                        this.CurrentFolder.RssManager.SaveXML();

        //                    if (this.CurrentFolder.ReviewEnabled)
        //                    {
        //                        DocumentLang doc_lang = DocumentBusinessLogic.GetDocLangById(DocLang, sess);
        //                        //ImageGalleryDocContent imagecontent = ImageGalleryBusinessLogic.GetById(imageid);
        //                        DocumentOverview.insertReview(doc_lang, imagecontent, this.ContentID, PageData, sess);

        //                        this.CurrentFolder.Invalidate();

        //                    }
        //                    tx.Commit();
        //                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
        //                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha creato una nuova revisione dell'immagine '" + this.CurrentDocument.Folder.Path + this.CurrentDocument.Nome + "' eseguendo l'upload dell'immagine '" + FileName + "', relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

        //                }
        //                catch (Exception ex)
        //                {
        //                    tx.Rollback();
        //                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
        //                }

        //                NetCms.GUI.PopupBox.AddSessionMessage("Immagine Salvata", NetCms.GUI.PostBackMessagesType.Success, true);
        //            }
        //        }
        //        else
        //        {
        //            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Il file scelto per la revisione non è un'immagine valida", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "107");
        //            NetCms.GUI.PopupBox.AddMessage("Errore! Il file scelto non è un'immagine valida", NetCms.GUI.PostBackMessagesType.Error);
        //        }
        //    }
        //    else
        //    {
        //        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore nel trasferimento dell'immagine per la revisione: file non valido, nome o descrizione non inseriti o non validi", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "108");
        //        NetCms.GUI.PopupBox.AddMessage("<strong>Errore nel trasferimento </strong><br />File non valido, nome o descrizione non inseriti o non validi", NetCms.GUI.PostBackMessagesType.Error);

        //    }

        //}

        //private bool IsImageFile(string ext)
        //{
        //    string[] ExtArray = { "png", "jpg", "bmp", "gif", "jpeg" };
        //    for (int i = 0; i < ExtArray.Length; i++)
        //        if (ExtArray[i] == ext)
        //            return true;
        //    return false;
        //}
    }
}