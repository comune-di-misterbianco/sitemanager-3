﻿using System;
using System.Data;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using NetCms.Connections;
using NetForms;
using NetCms.Structure.WebFS;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.Structure.Applications.Homepage;

namespace NetCms.Structure.Applications.ImageGallery
{
    [NetCms.Homepages.HomepageModuleDefiner(ImageGalleryApplication.ApplicationID, "Galleria immagini", ImageGalleryApplication.ApplicationID, NetCms.Homepages.HomepageModuleDefiner.ModuleType.CmsApp)]
    public partial class Modulo : Homepage.HomepageModule
    {
        private bool _ModuloError;
        public bool ModuloError
        {
            get { return _ModuloError; }
            set { _ModuloError = value; }
        }

        protected override string LocalCssClass
        {
            get { return " HomepageModulo_ImageGallery"; }
        }
        public override string Title
        {
            get { return "Modulo Image Gallery"; }
        }

        private bool SubFolders;
        private int Records;
        private bool ShowDesc;
        private string Template;

        private ImageGalleryFolder Folder;

        public Modulo(Homepage.Model.Modulo modulo)
            : base(modulo)
        {
            try
            {            
                if (modulo != null)
                {
                    ModuloApplicativo moduloApplicativo = NetCms.Structure.Applications.Homepage.HomepageModulesBusinessLogic<ModuloApplicativo>.GetModuloById(modulo.ID);

                    int ID = moduloApplicativo.FolderId;

                    SubFolders = moduloApplicativo.Subfolders == 1;
                    Records = moduloApplicativo.Records;
                    ShowDesc = moduloApplicativo.ShowDesc == 1;
                    Template = moduloApplicativo.TemplateMode;              
                    Folder = (ImageGalleryFolder)NetCms.Networks.NetworksManager.CurrentActiveNetwork.RootFolder.FindFolderByID(ID);
                    ModuloError = false;
                }
                else
                {                 
                    ModuloError = true;
                    throw new Exception("La query di selezione del modulo applicativo di tipo Rassegna dell'homepage non ha prodotto alcun risultato");
                }
            }
            catch (Exception ex)
            {              
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "37");
            }
        }

        protected override HtmlGenericControl GetAdminContent()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            if (ModuloError == false)
            {
                //HtmlGenericControl h3 = new HtmlGenericControl("h3");
                //h3.InnerHtml = "Rassegna Stampa: '" + Folder.Label + "'";
                //div.Controls.Add(h3);
                HtmlGenericControl strong = new HtmlGenericControl("strong");

                strong.InnerHtml = "Image Gallery : '" + Folder.Label + "'";
                div.Controls.Add(strong);

                HtmlGenericControl p = new HtmlGenericControl("p");
                p.InnerHtml = "Codice Modulo : #" + NetUtility.TreeUtility.FormatNumber(this.ID.ToString(), 4) + " <br />";
                p.InnerHtml = "Template: <strong>" + Template + "</strong> <br />";
                p.InnerHtml += "Mostra le ultime " + Records + " immagini della Galleria Immagini";
                p.InnerHtml += " presi dalla cartella '" + Folder.Label + "'";
                p.InnerHtml += SubFolders ? " e dalle sue sottocartelle" : "";
                div.Controls.Add(p);
            }
            return div;
        }

    }
}
namespace NetCms.Structure.Applications.ImageGallery
{
    [NetCms.Homepages.HomepageModuleDefiner(ImageGalleryApplication.ApplicationID, "Galleria immagini", ImageGalleryApplication.ApplicationID, Homepages.HomepageModuleDefiner.ModuleType.CmsApp)]
    public class ModuloForm : Homepage.HomepageModuleForm
    {
        public ModuloForm(int id)
            : base(id)
        {
        }
        public ModuloForm()
            : base()
        {
        }
        public override NetForms.NetFormTable GetFormTable()
        {
            NetFormTable modulo;
            if (ID != 0)
            {
                int id_modulo_applicativo = 0;
                DataTable table = this.Conn.SqlQuery("SELECT * FROM homepages_moduli_applicativi WHERE Modulo_ModuloApplicativo = " + ID);
                try
                {
                    if (table.Rows.Count > 0 && int.TryParse(table.Rows[0]["Modulo_ModuloApplicativo"].ToString(), out id_modulo_applicativo)) ;
                    else throw new Exception("Errore tabella vuota oppure conversione Modulo_ModuloApplicativo errata nella classe Modulo di Rassegna");
                }
                catch (Exception ex)
                {
                    /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                    ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "38");
                    //HttpContext.Current.NetCms.Diagnostics.Diagnostics.Redirect("rassegna_error.aspx");
                }

                modulo = new NetFormTable("homepages_moduli_applicativi", "Modulo_ModuloApplicativo", this.Conn, id_modulo_applicativo);
            }
            else
            {
                modulo = new NetFormTable("homepages_moduli_applicativi", "Modulo_ModuloApplicativo", this.Conn);
                NetExternalField mod = new NetExternalField("homepages_moduli", "Modulo_ModuloApplicativo");
                modulo.addExternalField(mod);
            }

            NetCheckBox showmorelink = new NetCheckBox("Mostra link 'Mostra tutti'", "ShowMoreLink_ModuloApplicativo");
            modulo.addField(showmorelink);

            NetCheckBox showdesc = new NetCheckBox("Mostra descrizione", "ShowDesc_ModuloApplicativo");
            modulo.addField(showdesc);

            //NetTextBox desclen = new NetTextBox("Lunghezza Descrizione(se attiva)", "DescLength_ModuloApplicativo");
            NetTextBox desclen = new NetTextBox("Lunghezza Descrizione", "DescLength_ModuloApplicativo");
            desclen.Numeric = true;
            desclen.Required = true;
            //desclen.Value = "50";
            modulo.addField(desclen);

            NetDropDownList records = new NetDropDownList("Numero di record da visualizzare", "Records_ModuloApplicativo");
            records.Required = true;
            for (int i = 1; i <= 16; i++)
                records.addItem(i.ToString(), i.ToString());
            modulo.addField(records);

            NetDropDownList showdepth = new NetDropDownList("Profondità di visualizzazione", "Subfolders_ModuloApplicativo");
            showdepth.Required = true;
            showdepth.addItem("Mostra solo Immagini di questa cartella", "0");
            showdepth.addItem("Mostra Immagini di questa cartella e delle sue sottocartelle", "1");
            modulo.addField(showdepth);


            if (ID == 0)
            {
                StructureFolderBinder binder = new StructureFolderBinder(PageData.CurrentReference);
                TreeNode root = binder.TreeFromStructure((StructureFolder)NetCms.Networks.NetworksManager.CurrentActiveNetwork.RootFolder, ImageGalleryApplication.ApplicationID);
                NetTreeView folders = new NetTreeView("Seleziona una cartella", "Folder_ModuloApplicativo", root);
                folders.NotAllowedMsg = "Attenzione! La cartella selezionata non è di tipo Image Gallery o non si possiedono i permessi di accesso";
                folders.RootDist = Configurations.Paths.AbsoluteRoot;
                folders.CheckAllows = true;
                folders.Required = true;
                modulo.addField(folders);
            }

            return modulo;
        }

       // public override bool UseTemplate { get { return true; } }
        //public override bool UseExendedTemplate
        //{
        //    get { return true; }
        //}

        public override TemplateOptionEnum TemplateOption
        {
            get { return TemplateOptionEnum.ExendedTemplate; }
        }
    }
}

