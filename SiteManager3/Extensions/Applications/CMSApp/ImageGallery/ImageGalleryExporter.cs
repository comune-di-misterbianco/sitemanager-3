﻿using System;
using System.IO;
using System.Xml;
using System.Web;
using System.Data;
using NetCms.Networks;
using NetCms.Networks.WebFS;
using NetCms.Exporter;

namespace NetCms.Structure.Applications.WebDocs
{
    public class ImageGalleryExporter : ApplicationExporter
    {
        private DataTable _DocsTable;
        public override DataTable DocsTable 
        {
            get
            {
                if (_DocsTable == null)
                {
                    _DocsTable = new DataTable();
                    //this.FolderExporter.Exporter.Connection.FillDataTable(string.Format(ApplicationExporter.Network_Docs, "INNER JOIN docs_imagegallery ON id_Image = Content_DocLang", this.FolderID, ""), _DocsTable);
                    this.FolderExporter.Exporter.Conn.FillDataTable(string.Format(ApplicationExporter.Network_Docs, "INNER JOIN revisioni ON DocLang_Revisione=id_DocLang INNER JOIN imagegallery_docs_contents ON id_Image = Contenuto_Revisione", this.FolderID, "", ImageGallery.ImageGalleryApplication.ApplicationID.ToString()), _DocsTable);
                    _DocsTable.TableName = "docs";
                }
                return _DocsTable;
            }
        }


        public override DataTable[] Tables
        {
            get
            {
                return null;
            }
        }

        public override bool HasFiles { get { return false; } }


        public ImageGalleryExporter(string folderID, FolderExporter folderExporter)
            : base(folderID, folderExporter)
        {
        }
    }
}
