﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Data;

namespace NetCms.Structure.Applications.ImageGallery
{
    public class ImageGalleryApplication : NetCms.Structure.Applications.Application
    {
        public const string ApplicationContextFrontLabel = "ImageGallery.Frontend";
        public const string ApplicationContextBackLabel = "ImageGallery.Backoffice";
        public const int ApplicationID = 11;

        public override string[] AllowedSubfolders
        {
            get { return null; }
        }
       
        public override bool EnableNewsLetter 
        { get { return true; } }
        public override bool EnableMove
        { get { return true; } }
        public override bool EnableModule
        { get { return false; } }
        public override bool EnableReviews
        {
            get { return true; }
        }
        public override string Namespace
        { get { return "NetCms.Structure.Applications.ImageGallery"; } }
        public override string SystemName
        { get { return "imagegallery"; } }
        public override string Label
        { get { return "Gallerie Immagini"; } }
        public override string LabelSingolare
        { get { return "Galleria Immagini"; } }
        public override int ID
        { get { return ApplicationID; } }
        public override bool IsSystemApplication { get { return false; } }


        public override NetCms.Structure.Applications.Setup Installer
        {
            get
            { 
                if (_Installer == null)
                    _Installer = new NetCms.Structure.Applications.ImageGallery.Setup(this);
                return _Installer;
            }
        }
        private NetCms.Structure.Applications.Setup _Installer;

        public ImageGalleryApplication()
            : base(null)
        {
            base.Assembly = Assembly.GetAssembly(this.GetType());
        }
        public ImageGalleryApplication(Assembly assembly)
            : base(assembly)
        {
        }
        /*
        protected override List<ApplicationValidation> LocalCheck()
        {
            List<ApplicationValidation> validationArray = new List<ApplicationValidation>();
           
            foreach (DataRow row in this.Installer.NetworksTable.Select(this.Installer.FilterID))
            {
                NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);
                if (network.CmsStatus != NetCms.Networks.NetworkCmsStates.NotCreated)
                {
                    DataTable check = Connection.SqlQuery("SHOW TABLES LIKE '" + network.SystemName + "_docs_imagegallery'");
                    if (check.Rows.Count != 1)
                        validationArray.Add(new ApplicationValidation("Tabella Documenti Galleria Immagini nella rete: '" + network.SystemName + "'", false, "Tabella dei documenti Galleria Immagini non presente per la rete: '" + network.SystemName + "'"));
                    else validationArray.Add(new ApplicationValidation("Tabella Documenti Galleria Immagini nella rete: '" + network.SystemName + "'", true, ""));
                }
            }
            return validationArray;
        }

        protected override bool LocalRepair()
        {
            return true;
        }*/
       
    }

}
