﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.Applications.ImageGallery
{
    public static class Config
    {
        private static NetCms.Vertical.Configuration.Configuration Configuration
        {
            get
            {
                return _Configuration ?? (_Configuration = new NetCms.Vertical.Configuration.Configuration("imagegallery"));
            }
        }
        private static NetCms.Vertical.Configuration.Configuration _Configuration;

        private static string pattern = "{0}?preset={1}";

        public static bool UseAshxHandler
        {
            get
            {
                bool useAshxHandler = false;
                try
                {
                    if (Configuration != null && Configuration["UseAshxHandler"] != null &&  Configuration["UseAshxHandler"].ToString() == "true")
                        return true;
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Chiave ThumbPreset mancante, verificare il web.config.");
                }

                return useAshxHandler;
            }
        }

        public static string _ThumbPreset;
        public static string ThumbPreset
        { 
            get{
                if (string.IsNullOrEmpty(_ThumbPreset))
                {                    
                    try
                    {
                        if (Configuration != null && Configuration["ThumbPreset"] != null)
                        {
                            _ThumbPreset = Configuration["ThumbPreset"].ToString();
                            _ThumbPreset = (UseAshxHandler) ? string.Format(pattern, ".ashx", _ThumbPreset) : string.Format(pattern, "", _ThumbPreset);                            
                        }
                    }
                    catch (Exception ex)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Chiave ThumbPreset mancante, verificare il web.config.");
                    }
                }
                return _ThumbPreset;
            }
        }
        
        private static string _PreviewPreset;
        public static string PreviewPreset
        {
            get
            {
                if (string.IsNullOrEmpty(_PreviewPreset))
                {
                    try
                    {
                        if (Configuration != null && Configuration["PreviewPreset"] != null)
                        {
                            _PreviewPreset = Configuration["PreviewPreset"].ToString();                            
                            _PreviewPreset = (UseAshxHandler) ? string.Format(pattern, ".ashx", _PreviewPreset) : string.Format(pattern, "", _PreviewPreset);
                        }
                    }
                    catch (Exception ex)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Chiave PreviewPreset mancante, verificare il web.config.");
                    }
                }
                return _PreviewPreset;
            }
        }

        private static string _ModuloPreset;
        public static string ModuloImgGalleryPreset
        {
            get
            {
                if (string.IsNullOrEmpty(_ModuloPreset))
                {
                    try
                    {
                        if (Configuration != null && Configuration["ModuloImgGalleryPreset"] != null)
                        {
                            _ModuloPreset = Configuration["ModuloImgGalleryPreset"].ToString();
                            _ModuloPreset = (UseAshxHandler) ? string.Format(pattern, ".ashx", _ModuloPreset) : string.Format(pattern, "", _ModuloPreset);
                        }
                    }
                    catch (Exception ex)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Chiave ModuloPreset mancante, verificare il web.config.");
                    }
                }
                return _ModuloPreset;
            }
        }

        private static string AppRepository
        {
            get
            {
                string appRepository = "repository";
                try
                {
                    if (Configuration != null && Configuration["AppRepository"] != null)
                        appRepository = Configuration["AppRepository"].ToString();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Chiave AppRepository mancante, verificare il web.config.");
                }
                return appRepository;
            }
        }

        private static string TmpFolderUploadPath
        {
            get
            {
                string tmpFolderUploadPath = "";
                try
                {
                    if (Configuration != null && Configuration["TmpFolderUploadPath"] != null)
                        tmpFolderUploadPath = Configuration["TmpFolderUploadPath"].ToString();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Chiave TmpFolderUploadPath mancante, verificare il web.config.");
                }
                return tmpFolderUploadPath;
            }
        }

        private static string UploaderHandler
        {
            get
            {
                string uploaderHandler = "";
                try
                {
                    if (Configuration != null && Configuration["UploaderHandler"] != null)
                        uploaderHandler = Configuration["UploaderHandler"].ToString();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Chiave UploaderHandler mancante, verificare il web.config.");
                }
                return uploaderHandler;
            }
        }
    }
}
