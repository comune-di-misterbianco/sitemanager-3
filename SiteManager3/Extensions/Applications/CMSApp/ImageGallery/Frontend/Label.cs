﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;


namespace NetCms.Structure.Applications.ImageGallery
{
    public class ImageGalleryLabels : LabelsManager.Labels
    {
        //public const String ApplicationKey = "GalleryLabelsCacheObj";

        //public ImageGalleryLabels()
        //    : base(NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteConfigRoot)
        //{
        //}
        public const String ApplicationKey = "gallery";

        public ImageGalleryLabels()
            : base(ApplicationKey)
        {
        }

        public enum GalleryLabelsList
        {
            MostraImmaginiSezione,
            MostraImmaginiSottosezioni,
            Risoluzione,
            AvvisoPubblicazione,
            Avviso,
            NoRecord,
            ImmagineSuccessiva,
            ImmaginePrecedente,
            Description
        }

        //public string this[GalleryLabelsList label]
        //{
        //    get
        //    {
        //        if (this.LabelsCache.Contains(label.ToString()))
        //        {
        //            return this.FormatLabel(this.LabelsCache[label.ToString()]);
        //        }
        //        else
        //        {
        //            XmlNodeList oNodeList = this.XmlLabelsRoot.SelectNodes("/labels/gallery/" + label.ToString());
        //            if (oNodeList != null && oNodeList.Count > 0)
        //            {
        //                string value = oNodeList[0].InnerXml;
        //                LabelsCache.Add(value, label.ToString());
        //                return this.FormatLabel(value);
        //            }
        //        }
        //        return null;
        //    }
        //}
    }
}
