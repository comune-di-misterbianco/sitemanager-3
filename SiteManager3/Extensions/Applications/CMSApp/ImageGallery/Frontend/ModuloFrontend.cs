﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms;
using NetFrontend;
using System.Collections.Generic;
using System.Globalization;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.ImageGallery
{
    [NetCms.Homepages.HomepageModuleDefiner(ImageGalleryApplication.ApplicationID, "Galleria Immagini", ImageGalleryApplication.ApplicationID, NetCms.Homepages.HomepageModuleDefiner.ModuleType.CmsApp)]
    public class LayoutModuloImageGalleryBlock : GenericApplicationModulo
    {
        protected override string BaseSqlQuery
        {
            get
            {
                return "Application_Doc = " + ImageGalleryApplication.ApplicationID;
            }
        }

        protected override string LocalTypeCssClass
        {
            get { return "modulo_imagegallery"; }
        }

        public override string Title
        {
            get { return this.Label; }
        }

        public LayoutModuloImageGalleryBlock(NetCms.Structure.Applications.Homepage.Model.Modulo modulo)
            : base(modulo)
        {
        }

        public override DataRow GetRecord(string sqlConditions)
        {
            return NetFrontend.DAL.ImageGalleryDAL.GetDocumentRecord(sqlConditions);
        }

        public override DataRow[] SelectRecords(string sqlConditions, int page = 0, int recordCount = 0, string order = null)
        {
            return NetFrontend.DAL.ImageGalleryDAL.ListDocumentRecords(sqlConditions, page, recordCount, order);
        }
        public override int CountRecords(string sqlConditions)
        {
            return NetFrontend.DAL.ImageGalleryDAL.CountDocumentRecords(sqlConditions);
        }


        protected override HtmlGenericControl BlockContent()
        {
            var Records = InitRecords(RecordsToShow);
            string awr = NetCms.Front.FrontendNetwork.GetAbsoluteWebRoot();
            HtmlGenericControl div = new HtmlGenericControl("div");
            

            #region OldCode
            //WebControl link_slider = new WebControl(HtmlTextWriterTag.Link);
            //link_slider.Attributes["rel"] = "stylesheet";
            //link_slider.Attributes["type"] = "text/css";
            //link_slider.Attributes["href"] = NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/shared/ResponsiveSlides" + "/responsiveslides.css";

            //WebControl script_slider = new WebControl(HtmlTextWriterTag.Script);
            //script_slider.Attributes["type"] = "text/javascript";
            //script_slider.Attributes["src"] =  NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/shared/ResponsiveSlides" + "/responsiveslides.min.js";

            //            string script_code = @"<script>
            //                                            $(function() {
            //                                            $('.rslides').responsiveSlides({
            //                                                            auto : true,
            //                                                            pager: true,                                                           
            //                                                            nav: true,                                                            
            //                                                            namespace: 'centered-btns',
            //                                                            before: function () {
            //                                                                                //$('.events').append('<li>before event fired.</li>');
            //                                                                                 },
            //                                                            after: function () {
            //                                                                                //$('.events').append('<li>after event fired.</li>');
            //                                                                               }  
            //                                                            })
            //                                            });
            //                                    </script>";




            //string script_code = @"<script type=""text/javascript"">
            //                                $(function() {
            //                                $('.rslides').responsiveSlides({
            //                                                auto : true,
            //                                                pager: true,                                                           
            //                                                nav: true,                                                            
            //                                                namespace: 'centered-btns',
            //                                                before: function () {},
            //                                                after: function () {}  
            //                                                })
            //                                });
            //                        </script>";

            //div.Controls.Add(link_slider);
            //div.Controls.Add(script_slider);


            //div.Controls.Add(new LiteralControl(script_code)); 
            #endregion

            #region NoTemplateCode
            if (string.IsNullOrEmpty(base.Template) || base.Template == TemplateType.No_Template.ToString())
            {

                
                div.Attributes["class"] = "panel panel-default rslides_container";

                WebControl panelHeading = new WebControl(HtmlTextWriterTag.Div);
                panelHeading.CssClass = "panel-heading";

                

                WebControl title = new WebControl(HtmlTextWriterTag.H4);
                title.Controls.Add(new LiteralControl("<a href=\"" + awr + FolderPath + "/default.aspx" + "\">" + this.Label + "</a>"));

                panelHeading.Controls.Add(title);
                div.Controls.Add(panelHeading);

                WebControl panelbody = new WebControl(HtmlTextWriterTag.Div);
                panelbody.CssClass = "panel-body";

                int rnd = new Random().Next(0, 9999);

                WebControl contentModulo = new WebControl(HtmlTextWriterTag.Div);
                contentModulo.CssClass = "carousel slide";
                contentModulo.Attributes.Add("data-ride", "carousel");
                contentModulo.ID = "carousel-" + rnd.ToString();

                WebControl ol = new WebControl(HtmlTextWriterTag.Ol);
                ol.CssClass = "carousel-indicators";

                WebControl thumbs = new WebControl(HtmlTextWriterTag.Div);
                thumbs.CssClass = "carousel-inner";
                thumbs.Attributes.Add("role", "listbox");

                for (int i = 0; i < (Records.Length <= RecordsToShow ? Records.Length : RecordsToShow); i++)
                {
                    ImageGalleryDocContent image = ImageGalleryBusinessLogic.GetById(int.Parse(Records[i]["id_Image"].ToString()));

                    WebControl li = new WebControl(HtmlTextWriterTag.Li);
                    li.Attributes.Add("data-target", "#carousel-" + rnd.ToString());
                    li.Attributes.Add("data-slide-to", i.ToString());

                    if (i == 0)
                        li.CssClass = "active";

                    ol.Controls.Add(li);

                    WebControl item = new WebControl(HtmlTextWriterTag.Div);
                    item.CssClass = "item " + (i == 0 ? "active" : "");

                    WebControl img = new WebControl(HtmlTextWriterTag.Img);
                    img.Attributes.Add("src", awr + Records[i]["Path_Folder"] + "/" + image.FileName_Image + Config.ModuloImgGalleryPreset);
                    img.Attributes.Add("alt", Records[i]["Label_DocLang"].ToString());

                    item.Controls.Add(img);

                    WebControl caption = new WebControl(HtmlTextWriterTag.Div);
                    caption.CssClass = "carousel-caption";

                    if (ShowDesc)
                    {
                        string desc = Records[i]["Desc_DocLang"].ToString();
                        if (desc.Length > DescLength)
                            desc = desc.Remove(DescLength) + "...";

                        caption.Controls.Add(new LiteralControl(desc));
                    }

                    item.Controls.Add(caption);

                    thumbs.Controls.Add(item);
                }

                contentModulo.Controls.Add(ol);
                contentModulo.Controls.Add(thumbs);

                WebControl left = new WebControl(HtmlTextWriterTag.A);
                left.CssClass = "left carousel-control";
                left.Attributes.Add("href", "#carousel-" + rnd.ToString());
                left.Attributes.Add("role", "button");
                left.Attributes.Add("data-slide", "prev");

                left.Controls.Add(new LiteralControl("<span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>"));
                left.Controls.Add(new LiteralControl("<span class=\"sr-only\">Previous</span>"));

                contentModulo.Controls.Add(left);

                WebControl right = new WebControl(HtmlTextWriterTag.A);
                right.CssClass = "right carousel-control";
                right.Attributes.Add("href", "#carousel-" + rnd.ToString());
                right.Attributes.Add("role", "button");
                right.Attributes.Add("data-slide", "next");

                right.Controls.Add(new LiteralControl("<span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>"));
                right.Controls.Add(new LiteralControl("<span class=\"sr-only\">Next</span>"));

                contentModulo.Controls.Add(right);

                div.Controls.Add(contentModulo);

                // link mostra tutti
                if (this.ShowMoreLink)
                {
                    HtmlGenericControl divShowAll = new HtmlGenericControl("div");

                    divShowAll.Attributes["class"] = "panel-footer text-center showAll";
                    divShowAll.InnerHtml = "<a class=\"btn btn-primary\" href =\"" + awr + FolderPath + "/default.aspx" + "\">Mostra tutte</a>";
                    div.Controls.Add(divShowAll);
                }
            }
            #endregion
            else
            {
                LiteralControl cSrc = GetTemplateContent(awr, Records);
                //div.Attributes["class"] = "row";
                div.Controls.Add(cSrc);
            }
            return div;
        }

        public LiteralControl GetTemplateContent(string awr, DataRow[] Records)
        {

            var currentmodulo = new { label = this.Label, url = awr + FolderPath + "/default.aspx", itemshowdesc = this.ShowDesc, itemshowmore = this.ShowMoreLink, showmorelabel = this.FrontCommonLabels[CommonLabels.CommonLabelsList.LabelAccediSezione], };

            Dictionary<string, string> labels = new Dictionary<string, string>();
            List<Object> items = new List<Object>();

            for (int i = 0; i < (Records.Length <= RecordsToShow ? Records.Length : RecordsToShow); i++)
            {
                ImageGalleryDocContent image = ImageGalleryBusinessLogic.GetById(int.Parse(Records[i]["id_Image"].ToString()));

                string titolo = "";
                string descrizione = Records[i]["Desc_DocLang"].ToString();
                DateTime date = DateTime.Parse(Records[i]["Data_Doc"].ToString());
                string date_ok = date.Day.ToString() + " " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month) + " " + date.Year.ToString();

                string itemurl = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetUrlWithoutLanguage(awr) + Records[i]["Path_Folder"] + "/" + image.FileName_Image + Config.ModuloImgGalleryPreset;

                Object img = new { imgsrc = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetUrlWithoutLanguage(awr) + Records[i]["Path_Folder"] + "/" + image.FileName_Image + Config.ModuloImgGalleryPreset,
                    imgalt = descrizione.Replace("\"", ""),
                    imgalign = "false",
                    imgname = Records[i]["Label_DocLang"].ToString() };

                Object item = new {
                    titolo = (!string.IsNullOrEmpty(titolo) ? titolo : "false"),
                    descrizione = descrizione,
                    date = date_ok,
                    itemurl = itemurl,
                    img = img };
                items.Add(item);
            }

            Object dati = new { modulo = currentmodulo, items = items };

            //string base_path = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + TplPath + "/content/homepage/");            
            string tpl_path = GetModuleTemplatePath(base.Template, "");

            string src = string.Empty;
            src = base.RenderTemplate(dati, tpl_path, labels);
            LiteralControl srcCntr = new LiteralControl(src);
            return srcCntr;
        }
    }
}