﻿using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Xml;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using NetCms;
using NetUtility;
using HtmlGenericControls;
using NetFrontend;
using G2Core.AdvancedPagination;
using NetService.Utility.RecordsFinder;
using System.Collections.Generic;
using NetService.Utility.Controls;
using GenericDAO.DAO.Utility;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Diagnostics;
using Frontend.Entities;
using AutoMapper;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Structure.Applications.ImageGallery
{
    public class ImageGalleryContentsHandler : NetFrontend.ApplicationContentsHandler
    {
        private int _ReferValue;
        private int ReferValue
        {
            get
            {
                if (_ReferValue == 0 && PageData.XmlConfigs != null)
                {
                    XmlNodeList oNodeList = Configurations.XmlConfig.GetNodes("/Portal/configs/Networks/" + this.PageData.Network.SystemName + "/applications/imagegallery/thumbsize");
                    if (oNodeList != null && oNodeList.Count > 0)
                        int.TryParse(oNodeList[0].Attributes["small"].Value, out _ReferValue);
                }
                return _ReferValue;
            }
        }

        protected override string ApplicationCSSClass
        {
            get { return "Gallery"; }
        }
        protected override string QueryStringKey
        {
            get { return "detail"; }
        }

        private string _FrontType;
        private string FrontType
        {
            get
            {
                if (_FrontType == null)
                {
                    _FrontType = "table";
                    XmlNode nodeConf = null;
                    if (FrontConfigs != null && FrontConfigs.ChildNodes.Count > 0)
                    {
                        nodeConf = FrontConfigs.SelectSingleNode("frontend");
                        if (nodeConf != null && nodeConf.Attributes["type"] != null)
                            _FrontType = nodeConf.Attributes["type"].Value;
                    }
                }
                return _FrontType;
            }
        }

        private XmlNode _FrontConfigs;
        private XmlNode FrontConfigs
        {
            get
            {
                if (_FrontConfigs == null)
                {

                    _FrontConfigs = this.PageData.XmlConfigs.SelectSingleNode("/Portal/configs/Networks/" + this.PageData.Network.SystemName + "/applications/imagegallery");
                }
                return _FrontConfigs;
            }
        }

        /* Template */
        private Dictionary<string, string> _TemplateLabels;
        public Dictionary<string, string> TemplateLabels
        {
            get
            {
                if (_TemplateLabels == null)
                {
                    _TemplateLabels = new Dictionary<string, string>();
                    foreach (ImageGalleryLabels.GalleryLabelsList label in Enum.GetValues(typeof(ImageGalleryLabels.GalleryLabelsList)))
                        _TemplateLabels.Add(label.ToString().ToLower(), GalleryLabels[label].ToString());
                }
                return _TemplateLabels;
            }
        }

        private ImageGalleryDocument DocRecord;
        private LabelsManager.Labels _GalleryLabels;
        private LabelsManager.Labels GalleryLabels
        {
            get
            {
                if (_GalleryLabels == null)
                {
                    _GalleryLabels = LabelsManager.LabelsCache.GetTypedLabels(typeof(ImageGalleryLabels)) as LabelsManager.Labels;
                }
                return _GalleryLabels;
            }
        }

        public ImageGalleryContentsHandler(NetFrontend.PageData pagedata)
            : base(pagedata)
        {
            currentFolder = PageData.Folder;
            //ApplicationContext.SetCurrentComponent(ImageGalleryApplication.ApplicationContextFrontLabel);
            //if (this.HasCurrentDocument)
            //   DocRecord = NetFrontend.DAL.ImageGalleryDAL.GetDocumentRecord(CurrentDocumentID);
            //_GalleryLabels = new ImageGalleryLabels();
            //DocRecord = ImageGalleryBusinessLogic.GetDocumentPreviewRecord(this.ContentID, this.PageData.Folder.ID);
            DocRecord = ImageGalleryBusinessLogic.GetDocumentRecord(this.CurrentDocumentID);
          
        }

        public override Control Control
        {
            get
            {
                // GetDetailView() funziona solo quando si crea un link ad in'immagine e cliccando sopra viene richiamata la funzione..per portarlo al funzionamento precedente levare la class=thichbox al link a_image.
                //la stringa href dentro la funzione listview va cambiata con quella che contiene la querystring "?detail"
                return (this.CurrentDocumentID > 0) ?
                        ((this.FrontType == "template") ? GetImageDetail() : GetDetailView()) :
                        ((this.FrontType == "template") ?GetImagesList() : GetListView());
                //return GetListView();
            }
        }

        private FrontendFolder currentFolder;

        public HtmlGenericControl GetListView()
        {
            List<SearchParameter> folder = new List<SearchParameter>();
            List<SearchParameter> searchparameters = new List<SearchParameter>();
            List<SearchParameter> document = new List<SearchParameter>();
            List<SearchParameter> doclang = new List<SearchParameter>();
            List<SearchParameter> doccontent = new List<SearchParameter>();

            HtmlGenericControl divblock = new HtmlGenericControl("div");
            divblock.Attributes["class"] = "row " + ApplicationCSSClass + CssClass;
            divblock.ID = "GalleryTable";  //???
                       
            HtmlGenericControl generale = new HtmlGenericControl("div");
            generale.Attributes["class"] = "col-md-12";

            #region Title

            HtmlGenericControl pageTitle = new HtmlGenericControl("div");
            pageTitle.Attributes["class"] = "row";

            HtmlGenericControl colTitle = new HtmlGenericControl("div");
            colTitle.Attributes["class"] = "col-md-12";

            HtmlGenericControl pageheader = new HtmlGenericControl("div");
            pageheader.Attributes["class"] = "page-header";

            HtmlGenericControl h1 = new HtmlGenericControl("h1");
            h1.InnerHtml = PageData.Folder.Label;
            pageheader.Controls.Add(h1);

            colTitle.Controls.Add(pageheader);
               
            StructureFolder currfolder = FolderBusinessLogic.GetById(this.PageData.Folder.ID);
            if (currfolder.HomeType == -2)
            {
                HtmlGenericControl desk = new HtmlGenericControl("div");
                desk.Attributes["class"] = "folderDesc";
                desk.InnerHtml = "<p>" + currfolder.Descrizione + "</p>";
                colTitle.Controls.Add(desk);
            }
                           
            WebControl all = new WebControl(HtmlTextWriterTag.P);
            all.Attributes.Add("class", "gallery_barra");

            if (this.Show == Views.ListAll && this.PageData.Folder.HasSubFolders)
            {
                //sql = ChildsFolders();
                //images_raw = NetFrontend.DAL.ImageGalleryDAL.ListDocumentRecords(sql, order: this.PrintOrder);
                WebControl a = new WebControl(HtmlTextWriterTag.A);
                a.Attributes.Add("href", PageData.PageName + "?show=level");
                a.Controls.Add(new LiteralControl(this.GalleryLabels[ImageGalleryLabels.GalleryLabelsList.MostraImmaginiSezione]));
                all.Controls.Add(a);
                //all.InnerHtml = "<a href=\"" + PageData.PageName + "?show=level\">" + this.GalleryLabels[ImageGalleryLabels.GalleryLabelsList.MostraImmaginiSezione] + "</a>";
            }
            else
            {
                //sql = "Folder_Doc  = " + PageData.Folder.ID;
                //images_raw = NetFrontend.DAL.ImageGalleryDAL.ListDocumentRecords(sql, order: this.PrintOrder);
                WebControl a = new WebControl(HtmlTextWriterTag.A);
                a.Attributes.Add("href", PageData.PageName + "?show=all");
                a.Controls.Add(new LiteralControl(this.GalleryLabels[ImageGalleryLabels.GalleryLabelsList.MostraImmaginiSottosezioni]));
                all.Controls.Add(a);
                //all.InnerHtml = "<a href=\"" + PageData.PageName + "?show=all\">" + this.GalleryLabels[ImageGalleryLabels.GalleryLabelsList.MostraImmaginiSottosezioni] + "</a>";
            }

            colTitle.Controls.Add(all);
                    
            pageTitle.Controls.Add(colTitle);

            generale.Controls.Add(pageTitle);

            #endregion


            HtmlGenericControl GalleryDiv = new HtmlGenericControl("div");
            GalleryDiv.Attributes["class"] = "row";

            HtmlGenericControl colGallery = new HtmlGenericControl("div");
            colGallery.Attributes["class"] = "col-md-12";
                       

            NetUtility.RequestVariable show = new NetUtility.RequestVariable("show", NetUtility.RequestVariable.RequestType.QueryString);
            if (show.StringValue.ToLower() == "all")
            {

                StructureFolder mytree = FolderBusinessLogic.GetFolderRecordByPath(PageData.Folder.Path, PageData.Folder.NetworkID);
                if (mytree != null)
                {
                    folder.Add(new SearchParameter(null, "Tree", mytree.Tree + "%", Finder.ComparisonCriteria.Like, false));
                    folder.Add(new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.LessThan, false));
                }
            }
            else
            {
                folder.Add(new SearchParameter(null, "ID", PageData.Folder.ID, Finder.ComparisonCriteria.Equals, false));
            }

            int rpp = 12;

            document.Add(new SearchParameter(null, "Application", ImageGalleryApplication.ApplicationID, Finder.ComparisonCriteria.Equals, false));
            document.Add(new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.LessThan, false));
            document.Add(new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false));
            //document.Add(new SearchParameter(null, "Folder.ID", this.PageData.Folder.ID, Finder.ComparisonCriteria.Equals, false));

            SearchParameter foldSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, folder.ToArray<SearchParameter>());
            document.Add(foldSP);

            SearchParameter documentSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, document.ToArray());
            doclang.Add(documentSP);

            SearchParameter contentSP = new SearchParameter(null, "Content", SearchParameter.RelationTypes.OneToMany, doccontent.ToArray());
            doclang.Add(contentSP);

            SearchParameter langSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToMany, doclang.ToArray());
            searchparameters.Add(langSP);
            
            //Paginazione 
            PaginationHandler pagination = new PaginationHandler(rpp, ImageGalleryBusinessLogic.GetImageGalleryPagedCount(searchparameters.ToArray()),"page",1);
            pagination.CssClass = "pagination";
            pagination.BaseLink = PageData.PageName + "?" + pagination.PaginationKey + "={0}";

            var immagini = ImageGalleryBusinessLogic.GetImageGalleryPaged(pagination.CurrentPage,
                                            pagination.PageSize,
                                            searchparameters.ToArray());

            if (immagini != null && immagini.Count > 0)
            {

                // init colorbox
                //this.PageData.HtmlHead.Controls.Add(new LiteralControl(NetCms.Configurations.Scripts.ColorBox.HtmlHeadCode));
                this.PageData.HtmlHead.Controls.Add(new LiteralControl(@"<link rel=""stylesheet"" href=""/scripts/front/lightbox/ekko-lightbox.css""></link>"));
                this.PageData.HtmlHead.Controls.Add(new LiteralControl(@"<script type=""text/javascript"" src=""/scripts/front/lightbox/ekko-lightbox.js""></script>"));
                

                foreach (ImageGalleryDocContent image in immagini)
                {
                    //try
                    //{
                    string Label = image.DocLang.Label;
                    //int Height = image.Height_Image;
                    //int Width = image.Width_Image;

                    //if (Width > Height)
                    //    Height = Height * ReferValue / Width;
                    //else
                    //    Height = 100;
                    //int paddingtop = (ReferValue - Height + 20) / 2;

                    string href = PageData.AbsoluteWebRoot + image.DocLang.Document.Folder.Path + "/" + image.FileName_Image;// +PageData.PageName + "?detail=" + image.DocLang.Document.ID + "\"";
                    //string href = PageData.AbsoluteWebRoot + image.DocLang.Document.Folder.Path + "/"  + PageData.PageName + "?detail=" + (image.DocLang.Content as ImageGalleryDocContent).ID;// +PageData.PageName + "?detail=" + image.DocLang.Document.ID + "\"";
                    string data = image.DocLang.Document.Data.ToString();
                    if (data.Length > 0)
                        data = data.Split(' ')[0];
                    //data = data.Remove(10);


                    WebControl Image = new WebControl(HtmlTextWriterTag.Div);
                    Image.CssClass = "col-lg-4 col-md-4 col-xs-12 thumb";
                      
                    HtmlGenericControl thumbnail = new HtmlGenericControl("figure");
                    thumbnail.Attributes["class"] = "figure figure-imagegallery thumbnail";
                                    
                    WebControl a_image = new WebControl(HtmlTextWriterTag.A);
                    a_image.Attributes.Add("href", href);
                    //a_image.Attributes.Add("class", "thickbox");
                    a_image.Attributes.Add("class", "img-group");
                    a_image.Attributes.Add("title", image.DocLang.Descrizione);
                    a_image.Attributes.Add("data-toggle", "lightbox");
                    a_image.Attributes.Add("data-gallery", "#GalleryTable");
                   // a_image.Attributes.Add("data-title", Label);


                    WebControl img = new WebControl(HtmlTextWriterTag.Img);
                    img.CssClass = "figure-img img-fluid rounded img-responsive";
                    img.Attributes.Add("alt", image.DocLang.Label);
                    img.Attributes.Add("src", PageData.AbsoluteWebRoot + image.DocLang.Document.Folder.Path + "/" + image.ThumbFile_Image);

                    a_image.Controls.Add(img);

                    thumbnail.Controls.Add(a_image);

                    HtmlGenericControl divCaption = new HtmlGenericControl("figcaption");
                    divCaption.Attributes["class"] = "figure-caption figure-caption-imagegallery caption";

                    WebControl titleimg = new WebControl(HtmlTextWriterTag.H5);
                    titleimg.Controls.Add(new LiteralControl(Label));
                    
                    divCaption.Controls.Add(titleimg);

                    WebControl dataP = new WebControl(HtmlTextWriterTag.P);
                    dataP.CssClass = "dataimg";
                    dataP.Controls.Add(new LiteralControl("<span class=\"glyphicon glyphicon-calendar\"></span> " + data));

                    divCaption.Controls.Add(dataP);
                           
                    thumbnail.Controls.Add(divCaption);
                              
                    Image.Controls.Add(thumbnail);

                    colGallery.Controls.Add(Image);
                  
                }
            }                                        
            
            GalleryDiv.Controls.Add(colGallery);

            generale.Controls.Add(GalleryDiv);

            HtmlGenericControl paginationRow = new HtmlGenericControl("div");
            paginationRow.Attributes["class"] = "row";

            HtmlGenericControl colPagination = new HtmlGenericControl("div");
            colPagination.Attributes["class"] = "col-md-12 text-center";

            colPagination.Controls.Add(pagination);

            paginationRow.Controls.Add(colPagination);   

            generale.Controls.Add(paginationRow);


            divblock.Controls.Add(generale);
                  
            string Script = @"<script type=""text/javascript"">
                            $(document).on('click', '[data-toggle=""lightbox""]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox({
    always_show_close: false,
    loadingMessage: '<div class=""ekko-lightbox-loader""><div><div></div><div></div></div></div>',
    leftArrow: '<i class=""fa fa-chevron-left""></i>',
    rightArrow: '<i class=""fa fa-chevron-right""></i>',

});
        });</script>";

            divblock.Controls.Add(new LiteralControl(Script));

            return divblock;
        }

        public override Control PreviewControl
        {
            get
            {
                DocRecord = ImageGalleryBusinessLogic.GetDocumentPreviewRecord(this.ContentID, this.PageData.Folder.ID);//NetFrontend.DAL.ImageGalleryDAL.GetDocumentPreviewRecord("id_Image = " + this.ContentID + " AND Folder_Doc=" + this.PageData.Folder.ID);
                if (DocRecord != null)
                    return GetDetailPreview();
                else
                    // throw new NetCms.Exceptions.PageNotFound404Exception();
                    throw new System.Web.HttpException(404, "File Not Found");
            }
        }
                
        public HtmlGenericControl GetDetailPreview()
        {
            #region vecchio modo di estrazione dei dati
            //if (DocRecord == null) new NetCms.Exceptions.PageNotFound404Exception();

            //HtmlGenericControls.Div div = new HtmlGenericControls.Div();
            //div.Class = ApplicationCSSClass + CssClass;

            //ImageGalleryDocContent ImageContent;
            //if (!DocRecord.DocLangs.ElementAt(0).Revisioni.ElementAt(0).Pubblicata)
            //    ImageContent = (DocRecord.DocLangs.ElementAt(0).Revisioni.ElementAt(0).Content as ImageGalleryDocContent);
            //else
            //    ImageContent = (DocRecord.DocLangs.ElementAt(0).Content as ImageGalleryDocContent);

            //string href = "href=\"" + PageData.AbsoluteWebRoot + PageData.Folder.Path + "/" + ImageContent.FileName_Image/*DocRecord["FileName_Image"].ToString()*/ + "?detail=" + DocRecord.ID/*DocRecord["id_Doc"]*/ + "\"";

            //PageData.HtmlHead.Title = /*DocRecord["Label_DocLang"]*/DocRecord.DocLangs.ElementAt(0).Label + " » " + PageData.HtmlHead.Title;

            //string data = DocRecord.Date;//DocRecord["Data_Doc"].ToString();
            //data = data.Length > 10 ? data.Remove(10) : data;

            //DateTime date = DateTime.Parse(data);
            //string date_ok = date.Day.ToString() + " " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month) + " " + date.Year.ToString();

            //div.InnerHtml += "<h1>" + DocRecord.DocLangs.ElementAt(0).Label/*DocRecord["Label_DocLang"]*/ + "</h1><div class=\"detail\">";
            //div.InnerHtml += "<div class=\"img_preview\"><a " + href + " class=\"thickbox\" >";
            //div.InnerHtml += "<img alt=\"" + DocRecord.DocLangs.ElementAt(0).Label/*DocRecord["Label_DocLang"].ToString()*/ + "\" src=\"" + ImageContent.PreviewFile_Image/*DocRecord["PreviewFile_Image"].ToString()*/ + "\"  />";
            //div.InnerHtml += "</a></div>";
            //div.InnerHtml += "<div class=\"img_data\"><p class=\"link\"><a " + href + " title=\"" + DocRecord.DocLangs.ElementAt(0).Label/*DocRecord["Label_DocLang"].ToString()*/ + "\" class=\"thickbox\">" + DocRecord.DocLangs.ElementAt(0).Label + "</a></p>";
            //div.InnerHtml += "<p class=\"data\">" + date_ok + "</p>";
            //div.InnerHtml += "<p class=\"ris\">" + this.GalleryLabels[ImageGalleryLabels.GalleryLabelsList.Risoluzione] + ": " + ImageContent.Width_Image/*DocRecord["Width_Image"].ToString()*/ + "*" + ImageContent.Height_Image/*DocRecord["Height_Image"].ToString()*/ + "</p>";
            //div.InnerHtml += "<p class=\"desc\">" + DocRecord.DocLangs.ElementAt(0).Descrizione + "</p></div>";//DocRecord["Desc_DocLang"] 
            //div.InnerHtml += "</div>";

            //return div; 
            #endregion

            HtmlGenericControl divblock = new HtmlGenericControl("div");
            divblock.Attributes["class"] = "row " + ApplicationCSSClass + CssClass;

            HtmlGenericControl generale = new HtmlGenericControl("div");
            generale.Attributes["class"] = "col-md-12";
                
            if (DocRecord == null)
                DocRecord = ImageGalleryBusinessLogic.GetDocumentRecord(CurrentDocument.ID); //NetFrontend.DAL.WebDocsDAL.GetDocumentRecord(CurrentDocument.ID);
            if (DocRecord != null)
            {
                ImageGalleryDocContent ImageContent = ImageGalleryBusinessLogic.GetById(this.ContentID);

                if (ImageContent != null)
                {
                    string detail = "";
                    string data = "";
                    string label = "";
                    string descrizione = "";
                    string href = "";

                    if (ImageContent.Revisione != null)
                    {
                        detail = ImageContent.Revisione.Document.ID.ToString();
                        label = ImageContent.Revisione.DocLang.Label.Replace("<%= WebRoot %>", PageData.AbsoluteWebRoot); ;
                        descrizione = ImageContent.Revisione.DocLang.Descrizione.Replace("<%= WebRoot %>", PageData.AbsoluteWebRoot); ;

                        href = "href=\"" + PageData.AbsoluteWebRoot + PageData.Folder.Path + "/" + ImageContent.FileName_Image + "?detail=" + ImageContent.Revisione.Document.ID + "\"";
                        PageData.HtmlHead.Title = ImageContent.Revisione.DocLang.Label  + " » " + PageData.HtmlHead.Title;

                        data = ImageContent.Revisione.Data.ToString();//DocRecord["Data_Doc"].ToString();
                        //data = data.Length > 10 ? data.Remove(10) : data;                       
                        data = data.Split(' ')[0];
                    }
                    else
                    {
                        detail = ImageContent.DocLang.Document.ID.ToString().Replace("<%= WebRoot %>", PageData.AbsoluteWebRoot); ;
                        label = ImageContent.DocLang.Label.Replace("<%= WebRoot %>", PageData.AbsoluteWebRoot); ;
                        descrizione = ImageContent.DocLang.Descrizione.Replace("<%= WebRoot %>", PageData.AbsoluteWebRoot); ;
                        
                        href = "href=\"" + PageData.AbsoluteWebRoot + PageData.Folder.Path + "/" + ImageContent.FileName_Image + "?detail=" + ImageContent.DocLang.Document.ID + "\"";
                        PageData.HtmlHead.Title = ImageContent.DocLang.Label + " » " + PageData.HtmlHead.Title;

                        data = ImageContent.DocLang.Document.Date;//DocRecord["Data_Doc"].ToString();
                        //data = data.Length > 10 ? data.Remove(10) : data;                       
                        data = data.Split(' ')[0];
                    }
                    DateTime date = DateTime.Parse(data);
                    string date_ok = date.Day.ToString() + " " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month) + " " + date.Year.ToString();    

                    #region Title

                    HtmlGenericControl pageTitle = new HtmlGenericControl("div");
                    pageTitle.Attributes["class"] = "row";

                    HtmlGenericControl colTitle = new HtmlGenericControl("div");
                    colTitle.Attributes["class"] = "col-md-12";

                    HtmlGenericControl pageheader = new HtmlGenericControl("div");
                    pageheader.Attributes["class"] = "page-header";

                    HtmlGenericControl h1 = new HtmlGenericControl("h1");
                    h1.InnerHtml = label;
                    pageheader.Controls.Add(h1);

                    colTitle.Controls.Add(pageheader);
                    pageTitle.Controls.Add(colTitle);

                    generale.Controls.Add(pageTitle);

                    #endregion

                    HtmlGenericControl divImage = new HtmlGenericControl("div");
                    divImage.Attributes["class"] = "row";

                    HtmlGenericControl colImage = new HtmlGenericControl("div");
                    colImage.Attributes["class"] = "col-md-12 thumb";

                    HtmlGenericControl thumbnail = new HtmlGenericControl("figure");
                    thumbnail.Attributes["class"] = "figure figure-imagegallery thumbnail";

                    WebControl a_image = new WebControl(HtmlTextWriterTag.A);
                    a_image.Attributes.Add("href", href);
                    a_image.Attributes.Add("class", "img-group");
                    a_image.Attributes.Add("title", label);
                    a_image.Attributes.Add("data-toggle", "lightbox");
                        
                    WebControl img = new WebControl(HtmlTextWriterTag.Img);
                    img.CssClass = "figure-img img-fluid rounded img-responsive";
                    img.Attributes.Add("alt", label);
                    img.Attributes.Add("src", ImageContent.PreviewFile_Image);

                    a_image.Controls.Add(img);

                    thumbnail.Controls.Add(a_image);

                    HtmlGenericControl divCaption = new HtmlGenericControl("figcaption");
                    divCaption.Attributes["class"] = "figure-caption figure-caption-imagegallery caption";

                    WebControl titleimg = new WebControl(HtmlTextWriterTag.H5);
                    titleimg.Controls.Add(new LiteralControl(label));

                    divCaption.Controls.Add(titleimg);
                       
                    WebControl dlInfoImage = new WebControl(HtmlTextWriterTag.Dl);
                    dlInfoImage.CssClass = "dl-horizontal";

                    WebControl dtData = new WebControl(HtmlTextWriterTag.Dt);
                    dtData.Controls.Add(new LiteralControl("<span class=\"glyphicon glyphicon-calendar\"></span>"));
                    dlInfoImage.Controls.Add(dtData);

                    WebControl ddData = new WebControl(HtmlTextWriterTag.Dd);
                    ddData.Controls.Add(new LiteralControl(date_ok));
                    dlInfoImage.Controls.Add(ddData);
                            
                    WebControl dtResolution = new WebControl(HtmlTextWriterTag.Dt);
                    dtResolution.Controls.Add(new LiteralControl(GalleryLabels[ImageGalleryLabels.GalleryLabelsList.Risoluzione]));
                    dlInfoImage.Controls.Add(dtResolution);

                    WebControl ddResolution = new WebControl(HtmlTextWriterTag.Dd);
                    ddResolution.Controls.Add(new LiteralControl(ImageContent.Width_Image + "*" + ImageContent.Height_Image));
                    dlInfoImage.Controls.Add(ddResolution);

                    WebControl dtDescription = new WebControl(HtmlTextWriterTag.Dt);
                    dtDescription.Controls.Add(new LiteralControl(GalleryLabels[ImageGalleryLabels.GalleryLabelsList.Description]));
                    dlInfoImage.Controls.Add(dtDescription);

                    WebControl ddDescription = new WebControl(HtmlTextWriterTag.Dd);
                    ddDescription.Controls.Add(new LiteralControl(descrizione));
                    dlInfoImage.Controls.Add(ddDescription);
                           
                    divCaption.Controls.Add(dlInfoImage);
                           
                    thumbnail.Controls.Add(divCaption);

                    colImage.Controls.Add(thumbnail);

                    divImage.Controls.Add(colImage);
                    generale.Controls.Add(divImage);
                }
                else
                {
                    generale.Controls.Add(new LiteralControl("<h1>" + "Errore nella preview della Galleria Immagini" + "</h1>"));
                }        
            }
            else
                throw new NetCms.Exceptions.PageNotFound404Exception(HttpContext.Current.Request.Url.ToString());


            divblock.Controls.Add(generale);
            return divblock; 
        }

        public NhRequestObject<ImageGalleryDocContent> ImageSelezionata { get; private set; }
        public HtmlGenericControl GetDetailView()
        {            
            if (DocRecord == null)
                //throw new NetCms.Exceptions.PageNotFound404Exception();
                throw new System.Web.HttpException(404, "File Not Found");

            this.PageData.HtmlHead.Controls.Add(new LiteralControl(@"<link rel=""stylesheet"" href=""/scripts/front/lightbox/ekko-lightbox.css""></link>"));
            this.PageData.HtmlHead.Controls.Add(new LiteralControl(@"<script type=""text/javascript"" src=""/scripts/front/lightbox/ekko-lightbox.js""></script>"));

            string href = "href=\"" + PageData.AbsoluteWebRoot + PageData.Folder.Path + "/" + (DocRecord.ContentRecord as ImageGalleryDocContent).FileName_Image /*+ "?detail=" + ImageSelezionata.Instance.ID*/ + "\"";
            PageData.HtmlHead.Title = (DocRecord.ContentRecord as ImageGalleryDocContent).Revisione.DocLang.Label + " » " + PageData.HtmlHead.Title;

            string data = (DocRecord.ContentRecord as ImageGalleryDocContent).Revisione.Data.ToString();//DocLang.Document.Date;
            //data = data.Length > 10 ? data.Remove(10) : data;
            data = data.Split(' ')[0];

            DateTime date = DateTime.Parse(data);
            string date_ok = date.Day.ToString() + " " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month) + " " + date.Year.ToString();
                 
            HtmlGenericControl divblock = new HtmlGenericControl("div");
            divblock.Attributes["class"] = "row " + ApplicationCSSClass + CssClass;

            HtmlGenericControl generale = new HtmlGenericControl("div");
            generale.Attributes["class"] = "col-md-12";
               
            #region Title

            HtmlGenericControl pageTitle = new HtmlGenericControl("div");
            pageTitle.Attributes["class"] = "row";

            HtmlGenericControl colTitle = new HtmlGenericControl("div");
            colTitle.Attributes["class"] = "col-md-12";

            HtmlGenericControl pageheader = new HtmlGenericControl("div");
            pageheader.Attributes["class"] = "page-header";

            HtmlGenericControl h1 = new HtmlGenericControl("h1");
            h1.InnerHtml = (DocRecord.ContentRecord as ImageGalleryDocContent).Revisione.DocLang.Label;
            pageheader.Controls.Add(h1);

            colTitle.Controls.Add(pageheader);
            pageTitle.Controls.Add(colTitle);

            generale.Controls.Add(pageTitle);

            #endregion

            HtmlGenericControl divImage = new HtmlGenericControl("div");
            divImage.Attributes["class"] = "row";

            HtmlGenericControl colImage = new HtmlGenericControl("div");
            colImage.Attributes["class"] = "col-md-12 thumb";

            HtmlGenericControl thumbnail = new HtmlGenericControl("figure");
            thumbnail.Attributes["class"] = "figure figure-imagegallery thumbnail";

            WebControl a_image = new WebControl(HtmlTextWriterTag.A);
            a_image.Attributes.Add("href", href);
            a_image.Attributes.Add("class", "img-group");
            a_image.Attributes.Add("title", (DocRecord.ContentRecord as ImageGalleryDocContent).Revisione.DocLang.Label);
            a_image.Attributes.Add("data-toggle", "lightbox");
               
            WebControl img = new WebControl(HtmlTextWriterTag.Img);
            img.CssClass = "figure-img img-fluid rounded img-responsive";
            img.Attributes.Add("alt", (DocRecord.ContentRecord as ImageGalleryDocContent).Revisione.DocLang.Label);
            img.Attributes.Add("src", (DocRecord.ContentRecord as ImageGalleryDocContent).PreviewFile_Image);

            a_image.Controls.Add(img);

            thumbnail.Controls.Add(a_image);       

            HtmlGenericControl divCaption = new HtmlGenericControl("figcaption");
            divCaption.Attributes["class"] = "figure-caption figure-caption-imagegallery caption";

            WebControl titleimg = new WebControl(HtmlTextWriterTag.H5);
            titleimg.Controls.Add(new LiteralControl((DocRecord.ContentRecord as ImageGalleryDocContent).Revisione.DocLang.Label));

            divCaption.Controls.Add(titleimg);


            WebControl dlInfoImage = new WebControl(HtmlTextWriterTag.Dl);
            dlInfoImage.CssClass = "dl-horizontal";

            WebControl dtData = new WebControl(HtmlTextWriterTag.Dt);
            dtData.Controls.Add(new LiteralControl("<span class=\"glyphicon glyphicon-calendar\"></span>"));
            dlInfoImage.Controls.Add(dtData);

            WebControl ddData = new WebControl(HtmlTextWriterTag.Dd);
            ddData.Controls.Add(new LiteralControl(data));
            dlInfoImage.Controls.Add(ddData);


            WebControl dtResolution = new WebControl(HtmlTextWriterTag.Dt);
            dtResolution.Controls.Add(new LiteralControl(GalleryLabels[ImageGalleryLabels.GalleryLabelsList.Risoluzione]));
            dlInfoImage.Controls.Add(dtResolution);

            WebControl ddResolution = new WebControl(HtmlTextWriterTag.Dd);
            ddResolution.Controls.Add(new LiteralControl((DocRecord.ContentRecord as ImageGalleryDocContent).Width_Image + "*" + (DocRecord.ContentRecord as ImageGalleryDocContent).Height_Image));
            dlInfoImage.Controls.Add(ddResolution);

            WebControl dtDescription = new WebControl(HtmlTextWriterTag.Dt);
            dtDescription.Controls.Add(new LiteralControl(GalleryLabels[ImageGalleryLabels.GalleryLabelsList.Description]));
            dlInfoImage.Controls.Add(dtDescription);

            WebControl ddDescription = new WebControl(HtmlTextWriterTag.Dd);
            ddDescription.Controls.Add(new LiteralControl((DocRecord.ContentRecord as ImageGalleryDocContent).Revisione.DocLang.Descrizione));
            dlInfoImage.Controls.Add(ddDescription);
                         
            divCaption.Controls.Add(dlInfoImage);
                   
            thumbnail.Controls.Add(divCaption);
                     
            colImage.Controls.Add(thumbnail);

            divImage.Controls.Add(colImage);
            generale.Controls.Add(divImage);

            divblock.Controls.Add(generale);
            return divblock;
        }

        private IDictionary<string, bool> _ReportOrder;
        private IDictionary<string, bool> ReportOrder
        {
            get
            {
                if (_ReportOrder == null)
                {

                    _ReportOrder = new Dictionary<string, bool>();

                    XmlNode nodeConf = null;
                    if (FrontConfigs != null && FrontConfigs.ChildNodes.Count > 0)
                    {
                        nodeConf = FrontConfigs.SelectSingleNode("frontend");
                        if (nodeConf != null && nodeConf.Attributes["reportorder"] != null)
                        {
                            string reportorder = nodeConf.Attributes["reportorder"].Value;

                            string[] arrayReporToOrder = reportorder.Split(';');

                            foreach (string subOrder in arrayReporToOrder)
                            {
                                string[] subArrayOrder = subOrder.Replace("{", "").Replace("}", "").Split(',');
                                string key = subArrayOrder[0].ToString();
                                bool desc = (subArrayOrder[1].ToString().ToLower() == "desc") ? false : true;
                                _ReportOrder.Add(key, desc);
                            }
                        }
                    }
                }
                return _ReportOrder;
            }
        }

        public Control GetImagesList()
        {
            WebControl page = new WebControl(HtmlTextWriterTag.Div);
            page.CssClass = "wrapper";

            IEnumerable<FrontendFolder> frontendFolders = PageData.Folder.SubFolders.Where(x => x.Type == 11 && (x.StateValue == 0 || x.StateValue == 4)).OrderByDescending(y => y.Order).ToList();

            var folders = (from front_folder in frontendFolders
                           select new
                           {
                               ID = front_folder.ID,
                               Titolo = front_folder.Label,
                               Descrizione = front_folder.Descrizione,
                               Order = front_folder.Order,
                               url = front_folder.FrontendUrl,
                               image = ((front_folder.Documents != null && front_folder.Documents.Any()) ? new
                               {
                                   url = front_folder.Documents.FirstOrDefault().FrontendUrl,
                                   desc = front_folder.Documents.FirstOrDefault().Descrizione
                               } : null)
                           }
                            );


            #region SearchParameters
            List<SearchParameter> folder = new List<SearchParameter>();
            List<SearchParameter> searchparameters = new List<SearchParameter>();
            List<SearchParameter> document = new List<SearchParameter>();
            List<SearchParameter> doclang = new List<SearchParameter>();
            List<SearchParameter> doccontent = new List<SearchParameter>();

            NetUtility.RequestVariable show = new NetUtility.RequestVariable("show", NetUtility.RequestVariable.RequestType.QueryString);
            if (show.StringValue.ToLower() == "all")
            {

                StructureFolder mytree = FolderBusinessLogic.GetFolderRecordByPath(PageData.Folder.Path, PageData.Folder.NetworkID);
                if (mytree != null)
                {
                    folder.Add(new SearchParameter(null, "Tree", mytree.Tree + "%", Finder.ComparisonCriteria.Like, false));
                    folder.Add(new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.LessThan, false));
                }
            }
            else
            {
                folder.Add(new SearchParameter(null, "ID", PageData.Folder.ID, Finder.ComparisonCriteria.Equals, false));
            }

            

            document.Add(new SearchParameter(null, "Application", ImageGalleryApplication.ApplicationID, Finder.ComparisonCriteria.Equals, false));
            document.Add(new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.LessThan, false));
            document.Add(new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false));
            //document.Add(new SearchParameter(null, "Folder.ID", this.PageData.Folder.ID, Finder.ComparisonCriteria.Equals, false));

            SearchParameter foldSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, folder.ToArray<SearchParameter>());
            document.Add(foldSP);

            SearchParameter documentSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, ReportOrder, document.ToArray());
            doclang.Add(documentSP);

            SearchParameter contentSP = new SearchParameter(null, "Content", SearchParameter.RelationTypes.OneToMany, doccontent.ToArray());
            doclang.Add(contentSP);

            SearchParameter langSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToMany, doclang.ToArray());
            searchparameters.Add(langSP);

            #endregion

            int rpp = 12;

            int numRecord = ImageGalleryBusinessLogic.GetImageGalleryPagedCount(searchparameters.ToArray());

            PaginationHandler pager = new PaginationHandler(rpp, numRecord, "page", 1);          

            // TODO: implementare codice per ordinamento record o da config.xml o da cartella

            var immagini = ImageGalleryBusinessLogic.GetImageGalleryPaged(pager.CurrentPage,
                                            pager.PageSize,
                                            searchparameters.ToArray());

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ImageGalleryDocContent, ImageGalleryDTO>()
                .ForMember(x => x.ID, o => o.MapFrom(y => y.DocLang.Document.ID))
                .ForMember(x => x.Titolo, o => o.MapFrom(y => y.DocLang.Label))
                .ForMember(x => x.Descrizione, o => o.MapFrom(y => y.DocLang.Descrizione))
                .ForMember(x => x.Data, o => o.MapFrom(y => y.DocLang.Document.Data))
                .ForMember(x => x.Filename, o => o.MapFrom(y => y.FileName_Image))
                .ForMember(x => x.Content, o => o.MapFrom(y => y.PreviewFile_Image));
            });

            config.AssertConfigurationIsValid();

            var mapper = config.CreateMapper();
            IEnumerable<ImageGalleryDTO> documents = mapper.Map<IEnumerable<ImageGalleryDocContent>, IEnumerable<ImageGalleryDTO>>(immagini);

            var sezione = new {
                titolo = this.currentFolder.Label,
                descrizione = this.currentFolder.Descrizione,
                url = this.currentFolder.FrontendUrl,
                childfolder = ((folders != null && folders.Any()) ? folders : null), 
                pagename = "default.aspx"
            };

            var paging = new {
                pages = pager.PagesCount,
                pagekey = pager.PaginationKey,
                current = pager.CurrentPage
            };

            string tpl_path = base.GetContentTemplatePath("list", "imagegallery");

            var result = NetCms.Themes.TemplateEngine.RenderTpl(documents, sezione, tpl_path, TemplateLabels, TemplateCommonLabels, paging);
            page.Controls.Add(new LiteralControl(result));

            return page;
        }

        public Control GetImageDetail()
        {
            WebControl page = new WebControl(HtmlTextWriterTag.Div);
            page.CssClass = "wrapper";

            return page;
        }

    }
}