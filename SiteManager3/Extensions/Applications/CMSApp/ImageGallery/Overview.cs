﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using NetTable;


namespace NetCms.Structure.Applications.ImageGallery
{
    public class ImageGalleryOverview : NetCms.Structure.WebFS.DocumentOverview
    {
        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Bell; }
        }

        public ImageGalleryOverview()
        {
        }
        protected override string PreviewJS
        {
            get
            {
                return "javascript: Preview('" + this.CurrentFolder.FrontendHyperlink + "',{0},'" + this.CurrentFolder.Path + "')";
            }
        }
        public override Control getView()
        {
            if (this.CurrentFolder.Tipo != this.CurrentFolder.RelativeApplication.ID ||
               this.CurrentFolder.Criteria["modify"].Value == NetCms.Grants.GrantsValues.Deny ||
               this.CurrentFolder == this.CurrentFolder.StructureNetwork.RootFolder
               )
            {
                return this.InvalidGrantsControl;
            }
            return base.getView();
        }

       
    }
}
