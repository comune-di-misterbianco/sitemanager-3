﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using System.IO;
using System.Collections.Generic;

namespace NetCms.Structure.Applications.ImageGallery
{
    public class ImageGalleryDocument : NetCms.Structure.WebFS.Document
    {
        private string _ThumbnailName;
        private string ThumbnailName { 
            get
            {
                if (string.IsNullOrEmpty( _ThumbnailName))
                    _ThumbnailName = (ContentRecord as ImageGalleryDocContent).ThumbFile_Image.ToString();
                        
                return _ThumbnailName;
            }
        }

        private string _PreviewName;
        private string PreviewName {
            get {
                if (string.IsNullOrEmpty(_PreviewName))
                    _PreviewName = (ContentRecord as ImageGalleryDocContent).PreviewFile_Image.ToString();
                return _PreviewName;
            }
        }

        private string _Width;
        private string Width
        {
            get 
            { 
                if (string.IsNullOrEmpty(_Width))
                    _Width = (ContentRecord as ImageGalleryDocContent).Width_Image.ToString();
                return _Width;    
            }
        }
        
        private string _Height;
        private string Height{
            get{
                if (string.IsNullOrEmpty(_Height))
                    _Height = (ContentRecord as ImageGalleryDocContent).Height_Image.ToString();

                return _Height;
            }
        }


        public override bool EnableCssEdit
        {
            get
            {
                return false;
            }
        }
        public override bool EnableCustomTitleEdit
        {
            get
            {
                return false;
            }
        }
        public override bool EnableCustomLayoutEdit
        {
            get
            {
                return false;
            }
        }

        private bool MultilanguageLoaded;
        private bool _Multilanguage;
        private bool Multilanguage
        {
            get
            {   
                return false;
            }
        }

        public override string FrontendLink
        {
            get { return (this.Folder as StructureFolder).FrontendHyperlink + this.PhysicalName; }//
            //get { return (this.Folder as StructureFolder).FrontendHyperlink + "default.aspx?detail=" + this.ID; }//this.PhysicalName
        }

        public override string Icon
        {
            get { return "icon_Gallery"; }
        }
        
        private IList<ImageGalleryDocContent> _ApplicationRecords;
        public virtual IList<ImageGalleryDocContent> ApplicationRecords
        {
            get
            {
                if (_ApplicationRecords == null)
                {
                    _ApplicationRecords = ImageGalleryBusinessLogic.FindAllApplicationRecords();
                }
                return _ApplicationRecords;
            }
        }

   
        public override DocContent ContentRecord
        {
            get
            {
                if (_ContentRecord == null)
                {
                    _ContentRecord = ImageGalleryBusinessLogic.GetById(this.Content);
                }
                return _ContentRecord;
            }
        }

   
        public override string Link
        {
            get { return FrontendLink; }
        }

        public ImageGalleryDocument()
        { 
        }
        
        public ImageGalleryDocument( Networks.NetworkKey network)
            : base( network)
        {
            //PreviewName = (ContentRecord as ImageGalleryDocContent).PreviewFile_Image.ToString();
            //ThumbnailName = (ContentRecord as ImageGalleryDocContent).ThumbFile_Image.ToString();
            //Width = (ContentRecord as ImageGalleryDocContent).Width_Image.ToString();
            //Height = (ContentRecord as ImageGalleryDocContent).Height_Image.ToString();
            //ThumbnailName = ContentRecord["ThumbFile_Image"].ToString();
            //Width = ContentRecord["Width_Image"].ToString();
            //Height = ContentRecord["Height_Image"].ToString();
        }

        public override HtmlGenericControl getRowView()
        {
            return getRowView(false);
        }

        public override HtmlGenericControl getRowView(bool SearchView)
        {
            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina della Galleria Immagini del portale "+NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");

            HtmlGenericControl tr = new HtmlGenericControl("tr");
            HtmlGenericControl td = new HtmlGenericControl("td");

            #region Icona
            /************************/
            td.InnerHtml = "&nbsp;";
            td.Attributes["class"] = "FS_Icon " + Icon;
            tr.Controls.Add(td);
            /************************/
            #endregion

            #region Nome & Link Alla Scheda
            /************************/
            string Link;

            if (!this.Grant)
                Link = this.Nome;
            else
            {//Link = "<a href=\""+PageData.ApplicationsRoot+"Gallery/Gallery_scheda.aspx?id=" + this.ID + "\">" + Label + "</a>";
                string info = "<strong>" + this.Nome +"</strong>";//Label.Replace("'", "\\\'")
                info += "<p><em>" + this.Descrizione.Replace(@"
", " ") + "</em></p>";//ATTENZIONE L'accapo soprastante non è un errore di battitura ma una necessità in quanto rappresenta il carattere accapo

                //info += "<p>Dimensione: " + (this.ContentRecord as ImageGalleryDocContent).Width_Image + "*" + (this.ContentRecord as ImageGalleryDocContent).Height_Image;//Height;

                info += "<br />Data Creazione: " + this.Date + "</p>";
                
                //Link = "<a onmouseover=\"Over('" + PageData.Paths.AbsoluteWebRoot + this.Folder.Path.Replace("'", "\\\'") + "/" + this.PhysicalName.Replace("'", "\\\'") + "','" + info + "')\" onmouseout=\"Out()\" href=\"" + SiteManagerLink + "\">" + this.Nome + "</a>";
                Link = "<a href=\"" + SiteManagerLink + "\">" + this.Nome + "</a>";
            }
            td = new HtmlGenericControl("td");
            td.InnerHtml = Link;
            tr.Controls.Add(td);
            /************************/
            #endregion

            #region Cartella di apparteneza
            //*************************************
            if (SearchView)
            {
                td = new HtmlGenericControl("td");
                td.InnerHtml = (this.Folder as StructureFolder).LabelLinkPath;
                tr.Controls.Add(td);
            }
            //*************************************
            #endregion

            #region Data Oggetto
            //*************************************
            td = new HtmlGenericControl("td");
            td.InnerHtml = this.Date.ToString();
            if (td.InnerHtml.Length > 10) td.InnerHtml = td.InnerHtml.Remove(10);
            tr.Controls.Add(td);
            //*************************************
            #endregion

            #region Tipo Dell'oggetto
            //*************************************
            td = new HtmlGenericControl("td");
            td.InnerHtml = "Immagine";
            tr.Controls.Add(td);
            //*************************************
            #endregion

            if (!SearchView)
            {
                #region Stato
                //*************************************
                td = new HtmlGenericControl("td");
                td.InnerHtml += this.StatusLabel;
                if ((this.Folder as StructureFolder).ReviewEnabled)
                            td.InnerHtml += " - " + (this.Content > 0 ? "Pubblicato" : "Non Pubblicato");
                tr.Controls.Add(td);
                //*************************************
                #endregion

                NetUtility.HtmlGenericControlCollection options = this.OptionsControls("gallery");
                if (options != null)
                    foreach (HtmlGenericControl option in options)
                        tr.Controls.Add(option);
            }
            //else
            //{
            //    #region Data
            //    //*************************************
            //    td = new HtmlGenericControl("td");
            //    td.InnerHtml = this.Date;
            //    tr.Controls.Add(td);
            //    //*************************************
            //    #endregion
            //}

            return tr;
        }

        public override bool MoveDocPhysical(string TargetFolderPath, PageData ExternalData)
        {
            //Immagine
            string path = ExternalData.Paths.AbsoluteWebRoot + this.Folder.Path + "/" + this.PhysicalName;
            path = HttpContext.Current.Server.MapPath(path);
            System.IO.FileInfo file = new FileInfo(path);
            if (file.Exists)
            {
                path = ExternalData.Paths.AbsoluteWebRoot + TargetFolderPath + "/" + this.PhysicalName;
                path = HttpContext.Current.Server.MapPath(path);
                file.MoveTo(path);
            }
            else
            {
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile spostare l'immagine in quanto il file non esiste", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "102");
                return false;
            }
            //Thumbnail
            path = ExternalData.Paths.AbsoluteWebRoot + this.Folder.Path + "/" + ThumbnailName;
            path = HttpContext.Current.Server.MapPath(path);
            file = new FileInfo(path);
            if (file.Exists)
            {
                path = ExternalData.Paths.AbsoluteWebRoot + TargetFolderPath + "/" + ThumbnailName;
                path = HttpContext.Current.Server.MapPath(path);
                file.MoveTo(path);
            }
            else
            {
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile spostare l'immagine in quanto la thumbnail non esiste", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "103");
                return false;
            }
            //Anteprima
            path = ExternalData.Paths.AbsoluteWebRoot + this.Folder.Path + "/" + PreviewName;
            path = HttpContext.Current.Server.MapPath(path);
            file = new FileInfo(path);
            if (file.Exists)
            {
                path = ExternalData.Paths.AbsoluteWebRoot + TargetFolderPath + "/" + PreviewName;
                path = HttpContext.Current.Server.MapPath(path);
                file.MoveTo(path);
            }
            else
            {
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile spostare l'immagine in quanto l'anteprima non esiste", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "104");
                return false;
            }
            return true;
        }
    }
}
