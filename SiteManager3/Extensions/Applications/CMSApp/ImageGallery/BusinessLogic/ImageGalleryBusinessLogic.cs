﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using NetCms.Structure.WebFS;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using NetCms.DBSessionProvider;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;

namespace NetCms.Structure.Applications.ImageGallery
{
    public static class ImageGalleryBusinessLogic
    {

        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static IStatelessSession GetStatelessSession()
        {
            return FluentSessionProvider.Instance.OpenStatelessSession();
        }


        public static ImageGalleryDocContent GetById(int idImageDoc, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();
            IGenericDAO<ImageGalleryDocContent> dao = new CriteriaNhibernateDAO<ImageGalleryDocContent>(innerSession);
            return dao.GetById(idImageDoc);
        }

        public static IList<ImageGalleryDocContent> FindAllApplicationRecords(ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();
            IGenericDAO<ImageGalleryDocContent> dao = new CriteriaNhibernateDAO<ImageGalleryDocContent>(innerSession);
            return dao.FindAll();
        }

        public static DocumentLang GetDocLangById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();
            IGenericDAO<DocumentLang> dao = new CriteriaNhibernateDAO<DocumentLang>(innerSession);
            return dao.GetById(id);
        }

        #region Metodi di frontend


        public static SearchParameter SearchContentByDataDoc(object data, Finder.ComparisonCriteria criterio)
        {
            SearchParameter dataSP = new SearchParameter(null, "Data", data, criterio, false);
            SearchParameter docSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, dataSP);
            SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, docSP);

            return docLangSP;
        }

        public static SearchParameter SearchContentByDescDocLang(string descrizione)
        {
            SearchParameter descSP = new SearchParameter(null, "Descrizione", "%" + descrizione + "%", Finder.ComparisonCriteria.Like, false);
            SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, descSP);

            return docLangSP;
        }

        public static SearchParameter SearchContentByTitoloDocLang(string titolo)
        {
            SearchParameter descSP = new SearchParameter(null, "Titolo", "%" + titolo + "%", Finder.ComparisonCriteria.Like, false);
            SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, descSP);

            return docLangSP;
        }

        public static ImageGalleryDocContent SaveOrUpdateImageDocsContent(ImageGalleryDocContent content, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();
            IGenericDAO<ImageGalleryDocContent> dao = new CriteriaNhibernateDAO<ImageGalleryDocContent>(innerSession);
            return dao.SaveOrUpdate(content);
        }
        public static IList<SearchParameter> ChildsFolders(string Path, int NetworkID)
        {
            List<SearchParameter> lista = new List<SearchParameter>();

            StructureFolder mytree = FolderBusinessLogic.GetFolderRecordByPath(Path, NetworkID);
            if (mytree != null)
            {
                //folders = "(Stato_Folder < 2 AND Tree_Folder LIKE '" + mytree.Tree + "%')";
                SearchParameter treeSP = new SearchParameter(null, "Tree", mytree.Tree + "%", Finder.ComparisonCriteria.Like, false);
                SearchParameter statoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.LessThan, false);
                SearchParameter foldSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { treeSP, statoSP });
                SearchParameter docSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, foldSP);
                SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, docSP);
                lista.Add(docLangSP);
            }

            return lista;
        }

        public static IList<SearchParameter> SearchInThisFolder(int FolderID)
        {
            List<SearchParameter> lista = new List<SearchParameter>();

            SearchParameter idSP = new SearchParameter(null, "ID", FolderID, Finder.ComparisonCriteria.Equals, false);
            SearchParameter foldSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, idSP);
            SearchParameter docSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, foldSP);
            SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, docSP);
            lista.Add(docLangSP);

            return lista;
        }



        //    //SELECT * FROM Folders INNER JOIN  Docs ON id_Folder = Folder_Doc 
        //    //INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang 
        //    //INNER JOIN Docs_News ON id_News = Content_DocLang 
        //    //WHERE id_Doc = id AND Stato_Doc<>2 AND Trash_Doc = 0

        public static ImageGalleryDocument GetDocumentRecord(int idDoc)
        {
            IGenericDAO<ImageGalleryDocument> dao = new CriteriaNhibernateDAO<ImageGalleryDocument>(GetCurrentSession());

            SearchParameter applicationSP = new SearchParameter(null, "Application", ImageGalleryApplication.ApplicationID, Finder.ComparisonCriteria.Equals, false);

            SearchParameter statoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.Not, false);

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter idSP = new SearchParameter(null, "ID", idDoc, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { applicationSP, statoSP, trashSP, idSP });

        }




        //    //SELECT * FROM docs_news 
        //    //    INNER JOIN revisioni ON id_News=Contenuto_Revisione 
        //    //    INNER JOIN docs_lang ON DocLang_Revisione=id_DocLang 
        //    //    INNER JOIN docs ON Doc_DocLang=id_Doc WHERE true
        //    //        and id_News = " + this.ContentID + " AND Folder_Doc=" + this.PageData.Folder.ID
        public static ImageGalleryDocument GetDocumentPreviewRecord(int idImageDoc, int idFolder)
        {
            IGenericDAO<ImageGalleryDocument> dao = new CriteriaNhibernateDAO<ImageGalleryDocument>(GetCurrentSession());

            SearchParameter applicationSP = new SearchParameter(null, "Application", ImageGalleryApplication.ApplicationID, Finder.ComparisonCriteria.Equals, false);

            SearchParameter statoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.Not, false);

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter folderSP = new SearchParameter(null, "Folder.ID", idFolder, Finder.ComparisonCriteria.Equals, false);

            SearchParameter idSP = new SearchParameter(null, "ID", idImageDoc, Finder.ComparisonCriteria.Equals, false);
            SearchParameter ContentSP = new SearchParameter(null, "Content", SearchParameter.RelationTypes.OneToOne, idSP);
            SearchParameter RevisioneSP = new SearchParameter(null, "Revisioni", SearchParameter.RelationTypes.OneToMany, ContentSP);
            SearchParameter docsLangSP = new SearchParameter(null, "DocLangs", SearchParameter.RelationTypes.OneToMany, RevisioneSP);

            return dao.GetByCriteria(new SearchParameter[] { applicationSP, statoSP, trashSP, folderSP, docsLangSP });

        }



        //public static IList<NewsDocContent> GetPagedRecordsWithSearch(string[] searchFields, string folderSqlQuery, int rpp, int pageStart)
        //IDictionary<string, bool> order,
        public static IList<ImageGalleryDocContent> GetImageGalleryPaged(int currentPage, int pageSize, IList<SearchParameter> searchParameters)
        {
            GenericDAO.DAO.CriteriaNhibernateDAO<ImageGalleryDocContent> dao = new GenericDAO.DAO.CriteriaNhibernateDAO<ImageGalleryDocContent>(GetCurrentSession());

            IList<ImageGalleryDocContent> images = dao.FindByCriteria(((currentPage - 1) * pageSize),
                                            pageSize,
                                            searchParameters.ToArray());

            return images;
        }

        public static IList<ImageGalleryDocContent> GetImageGalleryBySearchParameters(IList<SearchParameter> searchParameters)
        {
            GenericDAO.DAO.CriteriaNhibernateDAO<ImageGalleryDocContent> dao = new GenericDAO.DAO.CriteriaNhibernateDAO<ImageGalleryDocContent>(GetCurrentSession());

            var news = dao.FindByCriteria(searchParameters.ToArray());

            return news;
        }

        public static int GetImageGalleryPagedCount(IList<SearchParameter> searchParameters)
        {
            int count = -1;

            GenericDAO.DAO.CriteriaNhibernateDAO<ImageGalleryDocContent> dao = new GenericDAO.DAO.CriteriaNhibernateDAO<ImageGalleryDocContent>(GetCurrentSession());

            count = dao.FindByCriteriaCount(searchParameters.ToArray());

            return count;
        }


        public static int GetNumRecordsWithSearch(IList<SearchParameter> searchFields)
        {


            IGenericDAO<ImageGalleryDocContent> dao = new CriteriaNhibernateDAO<ImageGalleryDocContent>(GetCurrentSession());

            return dao.FindByCriteriaCount(searchFields.ToArray<SearchParameter>());

        }
        #endregion

    }
}


