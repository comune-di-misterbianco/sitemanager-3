﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using System.IO;
using NetCms.Structure.WebFS;
using NetService.Utility.ValidatedFields;
using NetCms.Grants;
using System.Collections.Generic;

namespace NetCms.Structure.Applications.ImageGallery.Pages
{
    public class ImageGalleryAdd : NetCms.GUI.NetworkPage
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Creazione Nuova Galleria"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Image_Add; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        private int SmallThumbSize
        {
            get
            {
                string attribute = Configurations.XmlConfig.GetAttribute("/Portal/configs/Networks/" + this.CurrentFolder.StructureNetwork.SystemName + "/applications/" + this.CurrentFolder.RelativeApplication.SystemName + "/thumbsize", "small");
                int.TryParse(attribute, out _SmallThumbSize);
                return _SmallThumbSize;
            }
        }
        private int _SmallThumbSize;

        private int BigThumbSize
        {
            get
            {
                string attribute = Configurations.XmlConfig.GetAttribute("/Portal/configs/Networks/" + this.CurrentFolder.StructureNetwork.SystemName + "/applications/" + this.CurrentFolder.RelativeApplication.SystemName + "/thumbsize", "big");
                int.TryParse(attribute, out _BigThumbSize);
                return _BigThumbSize;
            }
        }
        private int _BigThumbSize;

        public ImageGalleryAdd()
        {
            ApplicationContext.SetCurrentComponent(ImageGalleryApplication.ApplicationContextBackLabel);
            G2Core.Common.RequestVariable click = new G2Core.Common.RequestVariable("Avanti", G2Core.Common.RequestVariable.RequestType.Form_QueryString);
            
            if (this.CurrentFolder.Criteria[CriteriaTypes.Create].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina di aggiunta di una nuova immagine", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else if (!IsPostBack && !click.IsPostBack)
                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha effettuato l'accesso alla prima pagina di creazione di una nuova Immagine nel portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
        }

        private string _folderTempPath;
        private string FolderTempPath
        {
            get
            {
                if( string.IsNullOrEmpty(_folderTempPath))
                {
                    _folderTempPath = TmpFolderImagesRepository;
                    _folderTempPath = _folderTempPath.Replace(CurrentNetwork.SystemName + "/", "");
                    _folderTempPath = System.Web.HttpContext.Current.Server.MapPath(_folderTempPath);
                    
                }
                return _folderTempPath;
            }
            set
            {
                _folderTempPath = value;
            }
        }
                
        private void Avanti_Click(object sender, EventArgs e)
        {
            //folderTempPath = CurrentFolder.PageData.Paths.AbsoluteWebRoot + "/repository/imagegallery/tmp/" + NetCms.Users.AccountManager.CurrentAccount.UserName;
            //folderTempPath = folderTempPath.Replace(CurrentNetwork.SystemName + "/", "");
            //folderTempPath = System.Web.HttpContext.Current.Server.MapPath(folderTempPath);
            DirectoryInfo folderTemp = new DirectoryInfo(FolderTempPath);
            string lista_allegati = PageData.Request["file_list_FileUpload"];
            if (!string.IsNullOrEmpty(lista_allegati) || folderTemp.GetFiles().Length >0 )
            {               
                PageData.Redirect("gallery_steptwo.aspx?folder=" + this.CurrentFolder.ID);                
            }
            else
            {
                NetCms.GUI.PopupBox.AddMessage("Allegare almeno un file prima di andare avanti!!", NetCms.GUI.PostBackMessagesType.Notify);
            }
                       

        }
        
        private HtmlGenericControl errors = new HtmlGenericControl("div");

        private string tmpFolderImagesRepository;
        public string TmpFolderImagesRepository
        {
            get
            {
                if (tmpFolderImagesRepository == null )
                {
                    string tmpPath = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/imagegallery/tmp/" + NetCms.Users.AccountManager.CurrentAccount.ID;
                    tmpFolderImagesRepository = tmpPath;

                    tmpPath = HttpContext.Current.Server.MapPath(tmpPath);
                    if (!Directory.Exists(tmpPath))
                        Directory.CreateDirectory(tmpPath);                
                }
                return tmpFolderImagesRepository;             
            }
        }

        //private Uploadify.Uploadify uploadField;
        //public Uploadify.Uploadify UploadField
        //{
        //    get
        //    {
        //        if (uploadField == null)
        //        {
        //            uploadField = new Uploadify.Uploadify("FileUpload", "",
        //                "*.jpg; *.png; *.bmp; *.gif; *.tif; *.jpeg",
        //                "Allegati trasferibili: *.jpg; *.png; *.bmp; *.gif; *.tif; *.jpeg",
        //                "Allega",
        //                NetCms.Configurations.Paths.AbsoluteRoot + "/charts/uploader.ashx",
        //                NetCms.Configurations.Paths.AbsoluteRoot + "/charts/deleteFile.ashx",
        //                TmpFolderImagesRepository,
        //                NetCms.Configurations.Scripts.SwfObject.Path,
        //                NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/shared/uploadify/",
        //                true,
        //                true);
        //            uploadField.ID = "FileUpload";                   
        //        }
        //        return uploadField;
        //    }
        //}

        private List<FileUploadedV2> _FileUploaded;
        private List<FileUploadedV2> FileUploaded
        {
            get
            {
                if (_FileUploaded == null)
                {
                    _FileUploaded = new List<FileUploadedV2>();
                }

                return _FileUploaded;

            }
        }


        private VfMultiUpload2 UploadField
        {
            get
            {
                if (uploadField == null)
                {
                    uploadField = new VfMultiUpload2("FileUpload",
                                                "Immagini da importare",
                                                TmpFolderImagesRepository,
                                                null,
                                                new String[] {"jpg", "Jpg", "png", "gif", "jpeg", "svg"},
                                                0,
                                                false,
                                                FileUploaded
                                                );
                    uploadField.Mode = VfMultiUpload2.FieldMode.Hybrid;
                    uploadField.Required = true;
                    uploadField.BindField = false;
                    uploadField.DisplayFileList = true;
                    uploadField.AllowDelete = false;
                }
                return uploadField;
            }
        }
        private VfMultiUpload2 uploadField;


        //private VfUploadify UploadField
        //{
        //    get
        //    {
        //        if (uploadField == null)
        //        {
        //            uploadField = new VfUploadify("FileUpload",
        //                                        "Immagini da importare",
        //                                       "*.jpg; *.png; *.bmp; *.gif; *.tif; *.jpeg",
        //                                        "Allegati trasferibili: *.jpg; *.png; *.bmp; *.gif; *.tif; *.jpeg",
        //                                        "Allega",
        //                                        NetCms.Configurations.Paths.AbsoluteRoot + "/charts/uploader.ashx",
        //                NetCms.Configurations.Paths.AbsoluteRoot + "/charts/deleteFile.ashx",
        //                TmpFolderImagesRepository,
        //                NetCms.Configurations.Scripts.SwfObject.Path,
        //                NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/shared/uploadify/",
        //                true,
        //                true);



        //        }
        //        return uploadField;
        //    }
        //}
        //private VfUploadify uploadField;

        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "galleriaImageApp";

            WebControl div_upload = new WebControl(HtmlTextWriterTag.Div);            
            div_upload.ID = "div_upload";

            
            WebControl label = new WebControl(HtmlTextWriterTag.P);
            label.CssClass = "DetailsText";
            string infoDetail = "";

            DirectoryInfo folderTemp = new DirectoryInfo(FolderTempPath);
            
            if (folderTemp != null)
            {
                if (folderTemp.GetFiles().Length > 0)
                {
                    div_upload.Attributes.Add("style", "display:none");
                    infoDetail = @"La cartella di importazione contiene immagini per l'importazione. <br />
                    Se si desidera aggiungerne altre selezionare il bottone ""Trasferisci"", caricare le immagini dal proprio computer e cliccare ""Avanti"", altrimenti procedere direttamente con l'importazione cliccando il tasto ""Avanti"".";
                }
                else
                {
                    div_upload.Attributes.Add("style", "display:block");
                    infoDetail = "La cartella di importazione non contiene immagini  per l'importazione. <br />Selezionare il bottone \"Trasferisci\" e trasferire le immagini e cliccare \"Avanti\".";
                }

                label.Controls.Add(new LiteralControl(infoDetail));
            }
            
            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");            
            div.Controls.Add(fieldset);
            
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            fieldset.Controls.Add(legend);
            legend.InnerHtml = "<strong>Caricamento dati immagine - Passo 1/2</strong>";
            fieldset.Controls.Add(label);

            HtmlGenericControl p = new HtmlGenericControl("p");            
            //fieldset.Controls.Add(p);

            WebControl trasferisci = new WebControl(HtmlTextWriterTag.Div);
            trasferisci.ID = "button_trasferisci";
            trasferisci.Controls.Add(new LiteralControl("<a href=\"#\"><span>Trasferisci</span></a>"));
            fieldset.Controls.Add(trasferisci);

            WebControl script = new WebControl(HtmlTextWriterTag.Script);
            script.Controls.Add(new LiteralControl(@"$('#button_trasferisci a').click(function () {
                                                        $('#div_upload').toggle('slow');
                                                    });"));
            fieldset.Controls.Add(script);                   
        
            div_upload.Controls.Add(UploadField);
            fieldset.Controls.Add(div_upload);
            
            Button Avanti = new Button();
            Avanti.Text = "Avanti";
            Avanti.ID = "Avanti";
            Avanti.Click += Avanti_Click;

            div.Controls.Add(Avanti);
            
            return div;
        }
    }
}
