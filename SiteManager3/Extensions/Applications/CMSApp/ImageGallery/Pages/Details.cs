﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using NetCms.Grants;

namespace NetCms.Structure.Applications.ImageGallery.Pages
{
    public class Details : NetCms.Structure.WebFS.DocumentDetailsPage
    {
        protected override ToolbarButton ReserveForNewsletter
        {
            get
            {
                return null;
            }
        }

        protected override ToolbarButton ModifyButton
        {
            get
            {
                return null;
            }
        }



        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Dettaglio immagine: '" + this.CurrentDocument.Nome + "'"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Image_Add; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        public Details()
        {
            ApplicationContext.SetCurrentComponent(ImageGalleryApplication.ApplicationContextBackLabel);
            if (this.CurrentDocument.Criteria[CriteriaTypes.Show].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina dei dettagli di un'immagine", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina di dettaglio dell'immagine " + this.CurrentDocument.Nome/*Label*/ + " del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
        }
        
        protected WebControl FormatDetailRow(string Label, string Content, bool alternate)
        {
            WebControl tr = new WebControl(HtmlTextWriterTag.Tr);
            WebControl td = new WebControl(HtmlTextWriterTag.Td);

            td.Controls.Add(new LiteralControl(Label ));
            tr.Controls.Add(td);

            td = new WebControl(HtmlTextWriterTag.Td);
            td.Controls.Add(new LiteralControl(Content));
            tr.Controls.Add(td);
            return tr;
        }
        
        protected override WebControl BuildControls()
        {
            WebControl container = new WebControl(HtmlTextWriterTag.Div);
            container.CssClass = "container";

            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "card mt-3";

            string scheda = "<div class=\"DetailFrameError\"><h3>Nessuna revisione attiva per questo documento<h3></div>";
            
            if (this.CurrentDocument.Content > 0)
            {
                ImageGalleryDocContent imageContent = ImageGalleryBusinessLogic.GetById(this.CurrentDocument.Content);

                if (imageContent != null)
                {
                    WebControl image = new WebControl(HtmlTextWriterTag.Img);
                    image.CssClass = "card-image-top";
                    image.Attributes.Add("border", "0");
                    image.Attributes.Add("alt", imageContent.DocLang.Label);
                    image.Attributes.Add("src", this.CurrentFolder.Network.Paths.AbsoluteFrontRoot + this.CurrentFolder.Path + "/" + imageContent.PreviewFile_Image);

                    WebControl a = new WebControl(HtmlTextWriterTag.A);
                    a.Attributes.Add("href", this.CurrentFolder.Network.Paths.AbsoluteFrontRoot + this.CurrentFolder.Path + "/" + imageContent.FileName_Image.ToString());
                    a.Attributes.Add("target", "_blank");
                    a.Controls.Add(image);
                    div.Controls.Add(a);

                    WebControl title = new WebControl(HtmlTextWriterTag.H5);
                    title.CssClass = "card-title mt-3";
                    title.Controls.Add(new LiteralControl(PageTitle));
                    div.Controls.Add(title);                                        

                    WebControl table = new WebControl(HtmlTextWriterTag.Table);
                    table.CssClass = "table text-left";
                    table.Attributes.Add("cellpadding", "0");
                    table.Attributes.Add("cellspacing", "0");

                    table.Controls.Add(FormatDetailRow("<strong>Data Creazione</strong>: ", this.CurrentDocument.Data.ToString(),false));
                    
                    string stato = this.CurrentDocument.Stato.ToString() == "1" ? "Attivo" : "Non Attivo";
                    table.Controls.Add(FormatDetailRow("<strong>Stato</strong>: ", stato, false));

                    table.Controls.Add(FormatDetailRow("<strong>Dimensione</strong>: ", imageContent.Width_Image +" x "+imageContent.Height_Image, false));

                    if (!string.IsNullOrEmpty(imageContent.DocLang.Descrizione))
                        table.Controls.Add(FormatDetailRow("<strong>Descrizione</strong>: ", imageContent.DocLang.Descrizione, false));
                    
                    div.Controls.Add(table);
                }
            }
            else
                div.Controls.Add(new LiteralControl(scheda));

            container.Controls.Add(div);

            return container;
        }
    }
}
