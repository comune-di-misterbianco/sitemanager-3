﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using System.IO;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS.Reviews;
using NHibernate;
using NetCms.Grants;
using NetCms.Structure.Applications.Files;


namespace NetCms.Structure.Applications.ImageGallery.Pages
{
    public class ImageGalleryAddStepTwo : NetCms.GUI.NetworkPage
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Creazione Nuova Galleria"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Image_Add; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        private string _mainPageApplicationPath;
        private string mainPageApplicationPath
        {
            get
            {
                return _mainPageApplicationPath;
            }
            set
            {
                _mainPageApplicationPath = value;
            }
        }

        private void InitToolBar()
        {
            mainPageApplicationPath = this.CurrentFolder.Network.Paths.AbsoluteAdminRoot + "/cms/" + "default.aspx?folder=" + this.CurrentFolder.ID;
            this.Toolbar.Buttons.Add(new ToolbarButton(mainPageApplicationPath, NetCms.GUI.Icons.Icons.Arrow_Left, "Torna alla galleria immagini"));
            this.Toolbar.Buttons.Add(new ToolbarButton("gallery_new.aspx?folder=" + this.CurrentFolder.ID, NetCms.GUI.Icons.Icons.Arrow_Left, "Torna allo step 1"));
        }

        //private int SmallThumbSize
        //{
        //    get
        //    {
        //        string attribute = Configurations.XmlConfig.GetAttribute("/Portal/configs/Networks/" + this.CurrentFolder.Network.SystemName + "/applications/" + this.CurrentFolder.RelativeApplication.SystemName + "/thumbsize", "small");
        //        int.TryParse(attribute, out _SmallThumbSize);
        //        return _SmallThumbSize;
        //    }
        //}
        //private int _SmallThumbSize;

        //private int BigThumbSize
        //{
        //    get
        //    {
        //        string attribute = Configurations.XmlConfig.GetAttribute("/Portal/configs/Networks/" + this.CurrentFolder.Network.SystemName + "/applications/" + this.CurrentFolder.RelativeApplication.SystemName + "/thumbsize", "big");
        //        int.TryParse(attribute, out _BigThumbSize);
        //        return _BigThumbSize;
        //    }
        //}
        //private int _BigThumbSize;

        public ImageGalleryAddStepTwo()
        {

            ApplicationContext.SetCurrentComponent(ImageGalleryApplication.ApplicationContextBackLabel);
            G2Core.Common.RequestVariable click = new G2Core.Common.RequestVariable("Trasferisci", G2Core.Common.RequestVariable.RequestType.Form_QueryString);

            if (this.CurrentFolder.Criteria[CriteriaTypes.Create].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina di aggiunta di una nuova immagine", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else if (!IsPostBack && !click.IsPostBack)
                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha effettuato l'accesso alla pagina di creazione di una nuova Immagine nel portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
        }

        private HtmlGenericControl errors = new HtmlGenericControl("div");

        private CheckBox CK;
        private WebControl td;
        private TextBox filename;
        private TextBox titolo;
        private TextBox descrizione;
        private CheckBox ck_file;
        //private Button button_delete;

        private int contatore = 1;//prima era 1

        private string id_ck_pubblica = "ckpubblica";
        private string id_ck_file = "ckfile_";
        private string id_tx_file = "txfile_";
        private string id_tx_titolo = "txtitolo_";
        private string id_tx_desc = "txdesc_";
        //private string id_button = "button_";

        private string _SavePath;
        private string SavePath
        {
            get
            {
                if (_SavePath == null)
                {
                    _SavePath = this.CurrentFolder.Network.Paths.AbsoluteFrontRoot + this.CurrentFolder.Path + "/";
                    _SavePath = HttpContext.Current.Server.MapPath(_SavePath);
                }
                return _SavePath;
            }
        }

        private string tmpFolderImagesRepository;
        private string TmpFolderImagesRepository
        {
            get
            {
                if (tmpFolderImagesRepository == null)
                {
                    tmpFolderImagesRepository = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/imagegallery/tmp/" + NetCms.Users.AccountManager.CurrentAccount.ID + "/";
                    tmpFolderImagesRepository = HttpContext.Current.Server.MapPath(tmpFolderImagesRepository);
                }
                return tmpFolderImagesRepository;
            }
        }

        private LabelsManager.Labels _GalleryLabels;
        private LabelsManager.Labels GalleryLabels
        {
            get
            {
                if (_GalleryLabels == null)
                {
                    _GalleryLabels = LabelsManager.LabelsCache.GetTypedLabels(typeof(ImageGalleryLabels)) as LabelsManager.Labels;
                }
                return _GalleryLabels;
            }
        }

        protected void OnPreRender()
        {

        }

        protected override WebControl BuildControls()
        {

            InitToolBar();
            WebControl body = new WebControl(HtmlTextWriterTag.Body);
            WebControl div = new WebControl(HtmlTextWriterTag.Div);

            WebControl script = new WebControl(HtmlTextWriterTag.Script);
            script.Controls.Add(new LiteralControl(@"$(function() {
                                                        $('input[id*=ckfile_]').addClass('ckglobal');
                                                        enable_cb();
                                                        $('#ckglobal').click(enable_cb);
                                                    });

                                                    function enable_cb() {
                                                                        if (this.checked) {
                                                                                            $('input.ckglobal').attr('checked', true);
                                                                                           
                                                                                          } else {
                                                                                             $('input.ckglobal').removeAttr('checked');
                                                                                          }
                                                                          }   "));


            
            div.Controls.Add(script);
            WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
            div.Controls.Add(fieldset);
            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            fieldset.Controls.Add(legend);
            legend.Controls.Add(new LiteralControl("<strong>Caricamento dati immagine - Passo 2/2</strong>"));

            DirectoryInfo dirinfo = new DirectoryInfo(TmpFolderImagesRepository);
            FileInfo[] fileinfo = dirinfo.GetFiles();

            if (fileinfo != null && fileinfo.Length > 0)
            {
                WebControl par_delete = new WebControl(HtmlTextWriterTag.P);               

                Button button_elimina = new Button();
                button_elimina.Text = "Rimuovi le immagini";
                button_elimina.ID = "delete";
                button_elimina.CssClass = "button";
                button_elimina.OnClientClick = "javascript: setSubmitSource('delete')";
                par_delete.Controls.Add(button_elimina);

                fieldset.Controls.Add(par_delete);

                WebControl table = new WebControl(HtmlTextWriterTag.Table);
                table.CssClass = "tab";
                table.Attributes.Add("border", "0");
                table.Attributes.Add("cellspacing", "0");
                table.Attributes.Add("cellpadding", "0");

                WebControl tr = new WebControl(HtmlTextWriterTag.Tr);
                WebControl th = new WebControl(HtmlTextWriterTag.Th);

                CK = new CheckBox();
                CK.ID = "ckglobal";
                CK.ToolTip = "Seleziona/Deseleziona tutto";

                th.Controls.Add(CK);
                tr.Controls.Add(th);

                th = new WebControl(HtmlTextWriterTag.Th);
                th.Controls.Add(new LiteralControl("Nome del file"));
                tr.Controls.Add(th);

                th = new WebControl(HtmlTextWriterTag.Th);
                th.Controls.Add(new LiteralControl("Titolo"));
                tr.Controls.Add(th);

                th = new WebControl(HtmlTextWriterTag.Th);
                th.Controls.Add(new LiteralControl("Descrizione"));
                tr.Controls.Add(th);                

                table.Controls.Add(tr);

                contatore = 1;

                bool alternate = false;
                foreach (FileInfo file in fileinfo)
                {
                    tr = new WebControl(HtmlTextWriterTag.Tr);
                    if (alternate)
                    {
                        tr.Attributes["class"] += " alternate";
                        tr.Attributes["class"] = tr.Attributes["class"].Trim();
                    }
                    alternate = !alternate;


                    ck_file = new CheckBox();
                    ck_file.ID = id_ck_file + contatore;
                    ck_file.Attributes.Add("class", "ck_file");
                    //ck_file.Attributes["class"] = CK.ID;
                    ck_file.CheckedChanged += new EventHandler(Checked_Click);
                    td = new WebControl(HtmlTextWriterTag.Td);
                    td.Controls.Add(ck_file);
                    tr.Controls.Add(td);

                    filename = new TextBox();
                    filename.Width = 250;
                    filename.ID = id_tx_file + contatore;
                    filename.ReadOnly = true;
                    filename.Attributes.Add("style", "background:transparent; ");
                    filename.Text = file.Name;
                    td = new WebControl(HtmlTextWriterTag.Td);
                    td.Controls.Add(filename);
                    tr.Controls.Add(td);

                    titolo = new TextBox();//WebControl(HtmlTextWriterTag.Input);    
                    titolo.Width = 250;
                    titolo.ID = id_tx_titolo + contatore;
                    td = new WebControl(HtmlTextWriterTag.Td);
                    td.Controls.Add(titolo);

                    tr.Controls.Add(td);

                    descrizione = new TextBox();
                    descrizione.ID = id_tx_desc + contatore;
                    descrizione.TextMode = TextBoxMode.MultiLine;
                    descrizione.Rows = 2;
                    td = new WebControl(HtmlTextWriterTag.Td);
                    td.Controls.Add(descrizione);
                    tr.Controls.Add(td);     

                    table.Controls.Add(tr);
                    contatore++;
                }

                fieldset.Controls.Add(table);

                WebControl par_pubblicazione = new WebControl(HtmlTextWriterTag.P);

                CheckBox pubblicazione = new CheckBox();
                pubblicazione.ID = id_ck_pubblica;
                par_pubblicazione.Controls.Add(pubblicazione);

                WebControl lbl_pubblicazione = new WebControl(HtmlTextWriterTag.Label);
                lbl_pubblicazione.Attributes["for"] = id_ck_pubblica;
                lbl_pubblicazione.Controls.Add(new LiteralControl("Pubblica contestualmente la revisione per ogni immagine"));
                par_pubblicazione.Controls.Add(lbl_pubblicazione);

                fieldset.Controls.Add(par_pubblicazione);

                Button button = new Button();
                button.Text = "Importa";
                button.ID = "send";
                button.CssClass = "button";
                button.OnClientClick = "javascript: setSubmitSource('send')";

                fieldset.Controls.Add(button);               
            }
            else
            {
                WebControl p = new WebControl(HtmlTextWriterTag.P);
                p.Controls.Add(new LiteralControl(this.GalleryLabels[ImageGalleryLabels.GalleryLabelsList.NoRecord]));
                fieldset.Controls.Add(p);
            }


            # region postback
            if (PageData.IsPostBack)
            {
                //bool isOk = true;
                int image_not_selected = 0;
                if (PageData.SubmitSource == "send")
                {
                    try
                    {
                        for (int i = 1; i < contatore; i++)
                        {
                            NetUtility.RequestVariable ck = new NetUtility.RequestVariable(id_ck_file + i, NetUtility.RequestVariable.RequestType.Form);
                            if (string.IsNullOrEmpty(ck.StringValue) && ck.StringValue.Equals("on"))
                            {
                                //isOk = isOk && true; 
                                image_not_selected++;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                    }
                    if (image_not_selected == (contatore - 1))
                        NetCms.GUI.PopupBox.AddMessage("Selezionare almeno un file prima di effettuare l'importazione!!", NetCms.GUI.PostBackMessagesType.Notify);
                }

            }


            //nel postback della pagina controllo che ad essere cliccato sia stato il bottone elimina
            //per ogni ceckbox che è stata cliccata elimino il corrispondente file nella cartella temporanea di upload    

            if (PageData.IsPostBack)
            {
                if (PageData.SubmitSource == "delete")
                {
                    bool isOk = true;

                    try
                    {
                        for (int i = 1; i <= contatore - 1; i++)
                        {
                            NetUtility.RequestVariable ck = new NetUtility.RequestVariable(id_ck_file + i, NetUtility.RequestVariable.RequestType.Form);
                            if (!string.IsNullOrEmpty(ck.StringValue) && ck.StringValue.Equals("on"))
                            {
                                NetUtility.RequestVariable file = new NetUtility.RequestVariable(id_tx_file + i, NetUtility.RequestVariable.RequestType.Form);
                                string TargetPath = "";
                                if (!string.IsNullOrEmpty(file.StringValue))
                                {
                                    TargetPath = TmpFolderImagesRepository + file.StringValue;
                                    if (File.Exists(TargetPath))
                                    {
                                        File.Delete(TargetPath);
                                        isOk = isOk && true;
                                    }
                                }

                            }
                        }
                        //PageData.Redirect(mainPageApplicationPath);//PageData.PageName + "?folder=" + this.CurrentFolder.ID
                    }
                    catch (Exception ex)
                    {
                        isOk = isOk && false;
                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                    }

                    if (isOk)
                        NetCms.GUI.PopupBox.AddSessionMessage("File rimossi con successo!", NetCms.GUI.PostBackMessagesType.Success, mainPageApplicationPath);
                    else
                        NetCms.GUI.PopupBox.AddMessage("Errore durante la cancellazione dei file. Selezionare almeno un file da cancellare!", NetCms.GUI.PostBackMessagesType.Error);

                }

            }
            #endregion

            return div;
        }

        private bool IsImageFile(string ext)
        {
            string[] ExtArray = { "png", "jpg", "bmp", "gif", "jpeg" };
            for (int i = 0; i < ExtArray.Length; i++)
                if (ExtArray[i] == ext)
                    return true;
            return false;
        }

        //funzione che controlla se il file con quel nome è gia presente tra i doc nel database e se è così lo cambia per evitare di sovrascrivere il file. 
        private string FileNameController(string savepath, string justfilename, string fileext)
        {
            string file_rinominato = "";
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    FileInfo file_info = new FileInfo(savepath + justfilename + "." + fileext);
                    string fileNameCorrect = justfilename + "." + fileext;


                    if (file_info.Exists)
                    {
                        string ext = file_info.Extension;
                        string nome_file = file_info.Name;

                        nome_file = nome_file.Replace(ext, "");
                        ext = ext.Replace(".", "");

                        //string[] nome = fileNameCorrect.Split('.');
                        //int dim = nome.Length;


                        //for (int indice = 0; indice <= dim - 2; indice++)
                        //{
                        //    if (indice == (dim - 2))
                        //        file_rinominato += nome[indice];
                        //    else
                        //        file_rinominato += nome[indice] + ".";
                        //}

                        //int tab = DocumentBusinessLogic.GetNumberOfInstanceFile(file_rinominato, nome[dim - 1], FilesApplication.ApplicationID, sess);
                        int tab = DocumentBusinessLogic.GetNumberOfInstanceFile(nome_file, ext, ImageGalleryApplication.ApplicationID, sess);
                        tx.Commit();

                        int valore = 0;
                        if (tab != 0)
                        {
                            valore = tab + 1;
                            file_rinominato = nome_file + "(" + valore.ToString() + ")" + "." + ext;//nome[dim - 1];
                        }
                        else
                        {
                            file_rinominato = nome_file + "." + ext;
                        }

                        while (File.Exists(savepath + file_rinominato))
                        {
                            valore++;
                            file_rinominato = nome_file + "(" + valore.ToString() + ")" + "." + ext;//nome[1]
                        }

                        //return savepath + file_rinominato;
                    }
                    else
                    {
                        file_rinominato = justfilename + "." + fileext;
                        //return savepath + justfilename + "." + fileext;
                    }
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                }

            }
            return savepath + file_rinominato;
        }

        public virtual void Checked_Click(object sender, EventArgs e)
        {

            if (((CheckBox)sender).Checked)
            {
                string FileName;
                string JustFileName = "";
                string FileExt = "";


                string id_checkbox = ((CheckBox)sender).ID; //id della checkbox che è stato cliccato
                string[] stringa_separazione = id_checkbox.Split('_');
                int dim_stringa = stringa_separazione.Length;
                int checkbox_selezionata = int.Parse(stringa_separazione[dim_stringa - 1]);//estrazione del numero dall'id della checkbox

                //string url = HttpContext.Current.Request.RawUrl;//url corrente della pagina

                NetUtility.RequestVariable file = new NetUtility.RequestVariable(id_tx_file + checkbox_selezionata, NetUtility.RequestVariable.RequestType.Form);
                NetUtility.RequestVariable title = new NetUtility.RequestVariable(id_tx_titolo + checkbox_selezionata, NetUtility.RequestVariable.RequestType.Form);
                NetUtility.RequestVariable desc = new NetUtility.RequestVariable(id_tx_desc + checkbox_selezionata, NetUtility.RequestVariable.RequestType.Form);
                NetUtility.RequestVariable ck_pubb = new NetUtility.RequestVariable(id_ck_pubblica, NetUtility.RequestVariable.RequestType.Form);

                FileName = file.StringValue;

                JustFileName = FileName.Substring(0, FileName.LastIndexOf('.'));
                FileExt = FileName.Substring(FileName.LastIndexOf('.') + 1, (FileName.Length) - (FileName.LastIndexOf('.') + 1)).ToLower();
                //controllo inserito per fare in modo che il salvataggio avvenga solo al click del bottone importa!    
                if (PageData.SubmitSource == "send")
                {
                    if (IsImageFile(FileExt))
                    {

                        string TargetPath = "";// SavePath + FileName;
                        //FileInfo file_info = new FileInfo(TargetPath);

                        string complete_path = TmpFolderImagesRepository + FileName;
                        TargetPath = FileNameController(SavePath, JustFileName, FileExt);
                        FileName = TargetPath.Substring(TargetPath.LastIndexOf('\\') + 1);

                        File.Move(complete_path, TargetPath);

                        NetUtility.ImageFilters image = new NetUtility.ImageFilters(TargetPath);

                        ////Prima l'anteprima piccola 
                        //image.NameOffset = "tmb";
                        //if (image.Height > SmallThumbSize || image.Width > SmallThumbSize)
                        //    image.CreateThumbnail(SmallThumbSize);
                        //else
                        //    image.Save();
                        //string ThumbName = image.ThumbName;
                        ////Dopo  l'anteprima grande
                        //image.NameOffset = "preview";
                        //if (image.Height > BigThumbSize || image.Width > BigThumbSize)
                        //    image.CreateThumbnail(BigThumbSize);
                        //else
                        //    image.Save();
                        //string PreviewName = image.ThumbName;

                        Document document = new ImageGalleryDocument();
                        ImageGalleryDocContent docFile = new ImageGalleryDocContent();
                        DocumentLang docLang = new DocumentLang();
                        bool success = false;

                        using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                        using (ITransaction tx = sess.BeginTransaction())
                        {
                            try
                            {
                                #region Creo il Doc
                                //this.CurrentFolder.NetworkKey
                                document.Application = ImageGalleryApplication.ApplicationID;
                                document.PhysicalName = FileName;
                                document.Create = DateTime.Now;
                                document.Hide = 0;
                                document.Data = DateTime.Now;
                                document.Stato = 1;
                                document.Folder = this.CurrentFolder;//this.CurrentFolder---this.CurrentFolder.FindFolderByName("Allegati")
                                //Da VERIFICARE CHE SALVI L'ASSOCIAZIONE
                                this.CurrentFolder.Documents.Add(document);
                                document.Autore = Users.AccountManager.CurrentAccount.ID;//this.Account.ID;                                

                                #endregion

                                #region Creo il contenuto

                                docFile.FileName_Image = FileName;
                                docFile.Height_Image = image.Height;
                                docFile.Width_Image = image.Width;
                                docFile.Type_Image = FileExt;                                

                                #endregion

                                #region Creo la lingua

                                docLang.Document = document;
                                document.DocLangs.Add(docLang);

                                if (!string.IsNullOrEmpty(ck_pubb.StringValue) && ck_pubb.StringValue.Equals("on"))
                                {
                                    docLang.Content = docFile;//non li creo perchè vanno inseriti quando il documento viene revisionato!!
                                    docFile.DocLang = docLang;
                                }


                                //se il campo titolo è pieno mette il titolo se no mette il nome del file
                                if (!string.IsNullOrEmpty(title.StringValue))
                                    docLang.Label = title.StringValue;
                                else
                                    docLang.Label = FileName;

                                docLang.Lang = this.CurrentFolder.Network.DefaultLang;

                                //se il campo descrizione è pieno...
                                if (!string.IsNullOrEmpty(desc.StringValue))
                                    docLang.Descrizione = desc.StringValue;
                                else
                                    docLang.Descrizione = "";
                                #endregion

                                document = DocumentBusinessLogic.SaveOrUpdateDocument(document, sess);//sess
                                ImageGalleryBusinessLogic.SaveOrUpdateImageDocsContent(docFile, sess);//, sess
                                DocumentBusinessLogic.SaveOrUpdateDocumentLang(docLang, sess);

                                this.CurrentFolder.Invalidate();

                                if (this.CurrentFolder.EnableRss)
                                    this.CurrentFolder.RssManager.SaveXML();

                                if (this.CurrentFolder.ReviewEnabled)
                                {


                                    Review revisione = new Review();
                                    revisione.DocLang = docLang;
                                    revisione.Content = docFile;

                                    DateTime ora = DateTime.Now;
                                    string date = ora.Year + "-" + ora.Month + "-" + ora.Day + " " + ora.Hour + ":" + ora.Minute + ":" + ora.Second;
                                    DateTime data = DateTime.Parse(date);

                                    revisione.Data = data;
                                    revisione.Autore = Users.AccountManager.CurrentAccount.ID;
                                    revisione.Origine = 0;
                                    revisione.Approvata = 1;

                                    if (!string.IsNullOrEmpty(ck_pubb.StringValue) && ck_pubb.StringValue.Equals("on"))
                                    {
                                        revisione.Published = 1;
                                    }
                                    else
                                        revisione.Published = 0;

                                    ReviewBusinessLogic.SaveOrUpdateReview(revisione, sess);
                                    this.CurrentFolder.Invalidate();
                                    tx.Commit();

                                    Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha creato con successo un'immagine nel portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");

                                    success = true;
                                }

                            }
                            catch (Exception ex)
                            {
                                tx.Rollback();
                                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                            }
                        }



                        if (success)
                            NetCms.GUI.PopupBox.AddSessionMessage("Immagine '" + FileName + "' Salvata", NetCms.GUI.PostBackMessagesType.Success);

                        
                    }
                    else
                    {
                        NetCms.GUI.PopupBox.AddMessage("Errore! Il file '" + FileName + "' scelto non è un immagine valida", NetCms.GUI.PostBackMessagesType.Notify);
                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Il file scelto per l'inserimento non è un'immagine valida", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "110");
                    }
                }
            }
        }



    }
}