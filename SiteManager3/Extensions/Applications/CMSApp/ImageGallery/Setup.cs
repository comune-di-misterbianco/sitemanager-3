﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using System.Reflection;
using System.Linq;
using NetCms.Structure.WebFS;
using NetCms.DBSessionProvider;
using NHibernate;


namespace NetCms.Structure.Applications.ImageGallery
{
    public class Setup : NetCms.Structure.Applications.Setup
    {
        #region AddTableSql
//        private const string AddTableSql = @"
//CREATE TABLE IF NOT EXISTS `{0}_docs_imagegallery` (
//  `id_Image` INTEGER(11) NOT NULL AUTO_INCREMENT,
//  `FileName_Image` VARCHAR(256) COLLATE latin1_swedish_ci DEFAULT NULL,
//  `Width_Image` INTEGER(11) DEFAULT NULL,
//  `Height_Image` INTEGER(11) DEFAULT NULL,
//  `Type_Image` VARCHAR(3) COLLATE latin1_swedish_ci DEFAULT NULL,
//  `ThumbFile_Image` VARCHAR(256) COLLATE latin1_swedish_ci DEFAULT NULL,
//  `PreviewFile_Image` VARCHAR(256) COLLATE latin1_swedish_ci DEFAULT NULL,
//  PRIMARY KEY (`id_Image`)
//) ENGINE=InnoDB DEFAULT CHARSET=latin1;
//";
        #endregion

        public Setup(NetCms.Structure.Applications.Application baseApp)
            : base(baseApp)
        {
        }

        public override string[,] Pages
        {
            get
            {
                string[,] pages = {
                                  { "gallery_new.aspx", "NetCms.Structure.Applications.ImageGallery.Pages.ImageGalleryAdd" },
                                  { "gallery_steptwo.aspx", "NetCms.Structure.Applications.ImageGallery.Pages.ImageGalleryAddStepTwo" }
                                  };
                return pages;
            }
        }

        protected override bool LocalInstall(bool isInsidePackage = false)
        {
            bool done = true;
            try
            {
                FluentSessionProvider.Instance.UpdateSessionFactoryAdd(Assembly.GetAssembly(this.GetType()));
            }
            //if (done) done = AddRecords();
            catch (Exception ex)
            {
                this.RollBack();
                done = false;
            }
            

            return done;
        }
      

        private void RollBack()
        {
            bool done = false;
            if (!(NetworkID > 0))
            {
                using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = session.BeginTransaction())
                {
                    try
                    {
                        Application dbApp = ApplicationBusinessLogic.GetApplicationById(this.ApplicationBaseData.ID, session);
                        ApplicationBusinessLogic.DeleteApplication(dbApp, session);
                        tx.Commit();
                        done = true;
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                    }
                }

                if (done == false)
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile rimuovere l'Applicazione ImageGallery dal database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "113");
            }

            //foreach (BasicStructureNetwork network in this.NetworksTable.Where(x => !FilterID || (FilterID && x.ID == NetworkID)))
            //{
            //    //NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);
            //    string sql = string.Format("drop table if exists {0}_docs_imagegallery", network.SystemName);
            //    Connection.Execute(sql);
            //}
        }

        public override void Uninstall()
        {
            base.Uninstall();


            //foreach (BasicStructureNetwork network in this.NetworksTable.Where(x => !FilterID || (FilterID && x.ID == NetworkID)))
            //{
            //    //NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);
            //    string sql = string.Format("drop table if exists {0}_docs_imagegallery", network.SystemName);
            //    Connection.Execute(sql);
            //}

            FluentSessionProvider.Instance.UpdateSessionFactoryRemove(Assembly.GetAssembly(this.GetType()));
        }
    }
}
