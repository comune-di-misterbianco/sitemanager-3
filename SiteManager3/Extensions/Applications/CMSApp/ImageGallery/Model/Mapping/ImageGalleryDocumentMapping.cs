﻿using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.ImageGallery.Mapping
{
    class ImageGalleryDocumentMapping : SubclassMap<ImageGalleryDocument>
    {
        public ImageGalleryDocumentMapping()
        {
            Table("imagegallery_docs");
            KeyColumn("id_docs");
        }
    }
}
