﻿using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.ImageGallery.Mapping
{
    class ImageGalleryFolderMapping : SubclassMap<ImageGalleryFolder>
    {
        public ImageGalleryFolderMapping()
        {
            Table("imagegallery_folders");
            KeyColumn("id_folders");
        }

    }
}
