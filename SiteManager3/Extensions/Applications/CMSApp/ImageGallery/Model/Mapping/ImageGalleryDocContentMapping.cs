﻿using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.ImageGallery.Mapping
{
    class ImageGalleryDocContentMapping : SubclassMap<ImageGalleryDocContent>
    {
        public ImageGalleryDocContentMapping()
        {
            Table("imagegallery_docs_contents");
            KeyColumn("id_Image");
            Map(x => x.FileName_Image);
            Map(x => x.Width_Image);
            Map(x => x.Height_Image);
            Map(x => x.Type_Image); //.CustomSqlType("Varchar(4)")
            //Map(x => x.ThumbFile_Image);
            //Map(x => x.PreviewFile_Image);
        }
    }
}
