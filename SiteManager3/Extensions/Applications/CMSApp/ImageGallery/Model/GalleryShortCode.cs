﻿using NetCms.CmsApp.ShortCodes;
using NetCms.Structure.Applications.ImageGallery;
using NetService.Utility.RecordsFinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageGallery.Model
{
    public class GalleryShortCode : ShortCode
    {
        public GalleryShortCode() { }

        public GalleryShortCode(string rawValue, string appRif, int rifObj, EnumObjectType objectType, NetCms.Themes.Model.Theme currentTheme, int itemLimit = -1) : base(rawValue, appRif, rifObj, objectType, currentTheme, itemLimit)
        { }

        public override string GetHtml()
        {
            string source = string.Empty;

            string tpl_path = base.GetTemplatePath(); //System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + "/templates/content/imagegallery/shortcode.tpl");

            var template = Scriban.Template.Parse(System.IO.File.ReadAllText(tpl_path), tpl_path);
            try
            {
                if (template.HasErrors)
                {
                    if (NetCms.Configurations.Debug.DebugLoginEnabled)
                        foreach (var error in template.Messages)
                            source += error.Message + System.Environment.NewLine;
                    else
                        foreach (var error in template.Messages)
                            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, error.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                }
                else
                {
                    var context = new Scriban.TemplateContext();
                    context.PushCulture(System.Globalization.CultureInfo.CurrentCulture);

                    var scriptObj = new Scriban.Runtime.ScriptObject();
                    scriptObj.Add("items", GetItems());
                    scriptObj.Add("section", Section);

                    context.PushGlobal(scriptObj);

                    source += template.Render(context);

                }
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Template parsing error", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "", ex);
            }

            return source;
        }

        private object GetItems()
        {
            object documents = null;
            if (base.ObjectType == EnumObjectType.folder)
            {
                int id = base.RifObj;

                #region SearchParameters
                List<SearchParameter> searchparameters = new List<SearchParameter>();
                List<SearchParameter> folder = new List<SearchParameter>();                
                List<SearchParameter> document = new List<SearchParameter>();
                List<SearchParameter> doclang = new List<SearchParameter>();
                List<SearchParameter> doccontent = new List<SearchParameter>();

                
                folder.Add(new SearchParameter(null, "ID", base.RifObj, Finder.ComparisonCriteria.Equals, false));
                
                document.Add(new SearchParameter(null, "Application", ImageGalleryApplication.ApplicationID, Finder.ComparisonCriteria.Equals, false));
                document.Add(new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.LessThan, false));
                document.Add(new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false));                

                SearchParameter foldSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, folder.ToArray<SearchParameter>());
                document.Add(foldSP);

                SearchParameter documentSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, document.ToArray());
                doclang.Add(documentSP);

                SearchParameter contentSP = new SearchParameter(null, "Content", SearchParameter.RelationTypes.OneToMany, doccontent.ToArray());
                doclang.Add(contentSP);

                SearchParameter langSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToMany, doclang.ToArray());
                searchparameters.Add(langSP);

                #endregion
                // ritorna tutti i file pubblicati nella folder con id RifObj
                IList<ImageGalleryDocContent> items = ImageGalleryBusinessLogic.GetImageGalleryBySearchParameters(searchparameters.ToArray());

                if (base.ItemLimit != -1)
                    items = items.Take(ItemLimit).ToList();

                documents = (from item in items                             
                             select new
                             {
                                 titolo = item.DocLang.Label,
                                 descrizione = item.DocLang.Descrizione,
                                 url = item.DocLang.Document.GetFolderPath,
                                 data = item.DocLang.Document.Data,
                                 content = item.PreviewFile_Image,                                 
                             });
            }
            return documents;

        }

        private object _Section;
        private object Section
        {
            get
            {
                if (_Section == null)
                {

                    NetCms.Structure.WebFS.StructureFolder currentFolder = NetCms.Structure.WebFS.FileSystemBusinessLogic.FolderBusinessLogic.GetById(base.RifObj);
                    _Section = new { titolo = currentFolder.Label, descrizione = currentFolder.Descrizione, link = currentFolder.FrontendHyperlink, itemlimit = base.ItemLimit };

                }
                return _Section;
            }
        }

    }
}
