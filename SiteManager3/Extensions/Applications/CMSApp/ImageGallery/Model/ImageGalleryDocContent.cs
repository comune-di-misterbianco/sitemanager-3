﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Structure.WebFS;
using System.Data;
using NHibernate;
using NetCms.Networks;
using System.Xml.Linq;
using NetCms.Structure.Search.Models;


namespace NetCms.Structure.Applications.ImageGallery
{
    public class ImageGalleryDocContent : DocContent
    {
        public ImageGalleryDocContent()
            :base()
        { 
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string FileName_Image
        {
            get;
            set;
        }

        public virtual int Width_Image
        {
            get;
            set;
        }

        public virtual int Height_Image
        {
            get;
            set;
        }

        public virtual string Type_Image
        {
            get;
            set;
        }

        public virtual string ThumbFile_Image
        {
            get { 

                
                string thumbfile_image = FileName_Image + Config.ThumbPreset;
                
                return thumbfile_image;
            }
            
        }

        public virtual string PreviewFile_Image
        {
            get { 
                string previewfile_image = FileName_Image + Config.PreviewPreset;
                return previewfile_image;
            }
        }

        public enum Column
        {
            ID,
            FileName_Image,
            Width_Image,
            Height_Image,
            Type_Image,
            ThumbFile_Image,
            PreviewFile_Image
        }
       

        public override DocContent ImportDocContent(DataRow docRecord, ISession session, DataSet sourceDB, NetworkKey netKey, string sourceFolderPath, ref Document newDoc)
        {
            //ID = int.Parse(docRecord["id_Image"].ToString());
            FileName_Image = docRecord["FileName_Image"].ToString();

            Width_Image = int.Parse(docRecord["Width_Image"].ToString());
            Height_Image = int.Parse(docRecord["Height_Image"].ToString());

            Type_Image = docRecord["Type_Image"].ToString();            

            return this;
        }

        public override DocContent ImportDocContent(Dictionary<string, string> docRecord, ISession session)
        {
            throw new NotImplementedException();
        }

        public override bool EnableIndexContent
        {
            get
            {
                return true;
            }
        }

        public override CmsConfigs.Model.IndexDocContent GetContentToIndex(ISession session)
        {
            string frontUrl = (string.IsNullOrEmpty(this.DocLang.Document.Network.Paths.AbsoluteFrontRoot) ? "" : this.DocLang.Document.Network.Paths.AbsoluteFrontRoot)
                           + this.DocLang.Document.Folder.Path
                           + this.DocLang.Document.PhysicalName;

            CmsConfigs.Model.IndexDocContent indexContent = new CmsConfigs.Model.IndexDocContent();

            indexContent.ID = this.ID;
            indexContent.Fields.Add(CmsDocument.Fields.document_title.ToString(), this.DocLang.Label);
            indexContent.Fields.Add(CmsDocument.Fields.document_description.ToString(), this.DocLang.Descrizione);
            indexContent.Fields.Add(CmsDocument.Fields.document_content.ToString(), this.DocLang.Document.PhysicalName);
            indexContent.Fields.Add(CmsDocument.Fields.document_fronturl.ToString(), frontUrl);
            indexContent.Fields.Add(CmsDocument.Fields.document_backurl.ToString(), this.DocLang.Document.SiteManagerLink);
            indexContent.Fields.Add(CmsDocument.Fields.document_type.ToString(), this.DocLang.Document.Application.ToString());

            return indexContent;

        }

    }
}
