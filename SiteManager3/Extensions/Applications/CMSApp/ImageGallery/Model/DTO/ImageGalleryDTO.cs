﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.Applications.ImageGallery
{
    public class ImageGalleryDTO
    {
        public ImageGalleryDTO() { }

        public int ID { get; set; }
        public string Titolo { get; set; }
        public string Descrizione { get; set; }
        public DateTime Data { get; set; }
        public string Content { get; set; }

        public string Filename {get; set; }
    }
}
