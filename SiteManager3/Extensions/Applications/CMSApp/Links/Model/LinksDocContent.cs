﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Structure.WebFS;
using System.Data;
using NHibernate;
using NetCms.Networks;

namespace NetCms.Structure.Applications.Links
{
    public class LinksDocContent : DocContent
    {
        public LinksDocContent()
            : base()
        { 
        
        }

        public virtual string Href
        {
            get;
            set;
        }

        public virtual int Clicks
        {
            get;
            set;
        }

        public override DocContent ImportDocContent(DataRow docRecord, ISession session, DataSet sourceDB, NetworkKey netKey, string sourceFolderPath, ref Document newDoc)
        {
            string filteredContent = NetCms.Structure.Utility.ImportContentFilter.ParseImportedCleanUrls(docRecord["Href_Link"].ToString());
            Href = filteredContent;
            Clicks = int.Parse(docRecord["Clicks_Link"].ToString());
            return this;
        }
        public override DocContent ImportDocContent(Dictionary<string, string> docRecord, ISession session)
        {
            Href = NetCms.Structure.Utility.ImportContentFilter.ParseImportedCleanUrls(docRecord["Href_Link"].ToString());
            Clicks = int.Parse(docRecord["clicks"].ToString());

            return this;
        }


        public override bool EnableIndexContent
        {
            get
            {
                return false;
            }
        }

        public override CmsConfigs.Model.IndexDocContent GetContentToIndex(ISession session)
        {
            return null;
        }
    }
}
