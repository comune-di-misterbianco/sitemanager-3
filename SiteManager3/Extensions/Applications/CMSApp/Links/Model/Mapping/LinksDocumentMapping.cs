﻿using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.Links.Mapping
{
    public class LinksDocumentMapping : SubclassMap<LinksDocument>
    {
        public LinksDocumentMapping()
        {
            Table("links_docs");
            KeyColumn("id_docs");
        }
    }
}
