﻿using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.Links.Mapping
{
    public class LinksFolderMapping : SubclassMap<LinksFolder>
    {
        public LinksFolderMapping()
        {
            Table("links_folders");
            KeyColumn("id_folders");
        }
    }
}
