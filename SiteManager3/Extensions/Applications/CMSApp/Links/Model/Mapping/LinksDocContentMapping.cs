﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Structure.WebFS;

namespace NetCms.Structure.Applications.Links.Mapping
{
    public class LinksDocContentMapping : SubclassMap<LinksDocContent>
    {
        public LinksDocContentMapping()
        {
            Table("links_docs_contents");
            KeyColumn("id_Link");
            Map(x => x.Href).Column("Href_Link").CustomSqlType("mediumtext");
            Map(x => x.Clicks).Column("Clicks_Link");
        }
    }
}