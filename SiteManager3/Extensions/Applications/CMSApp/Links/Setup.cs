﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using System.Reflection;
using NetCms.DBSessionProvider;
using NetCms.Structure.WebFS;
using System.Linq;
using NHibernate;

namespace NetCms.Structure.Applications.Links
{
    public class Setup : NetCms.Structure.Applications.Setup
    {
//        #region AddTableSql
//        private const string AddTableSql = @"
//CREATE TABLE IF NOT EXISTS `{0}_docs_link` (
//  `id_Link` int(11) NOT NULL auto_increment,
//  `Href_Link` longtext,
//  `Clicks_Link` int(11),
//  PRIMARY KEY  (`id_Link`)
//) ENGINE=InnoDB DEFAULT CHARSET=latin1;
//";
//        #endregion

        public Setup(NetCms.Structure.Applications.Application baseApp)
            : base(baseApp)
        {
        }

        public override string[,] Pages
        {
            get
            {
                string[,] pages = {
                                  { "links_new.aspx", "NetCms.Structure.Applications.Links.Pages.LinksAdd" }
                                  };
                return pages;
            }
        }

        protected override bool LocalInstall(bool isInsidePackage = false)
        {
            bool done = true;
            try
            {
                FluentSessionProvider.Instance.UpdateSessionFactoryAdd(Assembly.GetAssembly(this.GetType()));
                //if (done) done = AddRecords();
                //if (done) done = AddTables();

            }
            catch (Exception ex)
            {
                this.RollBack();
                done = false;
            }
            return done;
        }
        /*
        private bool AddRecords()
        {
            bool done = false;
            try
            {
                string sql = string.Format(NetCms.Structure.Applications.Setup.AddApplicationSql, this.ApplicationBaseData.ID, this.ApplicationBaseData.SystemName, this.ApplicationBaseData.Label, this.ApplicationBaseData.LabelSingolare, this.ApplicationBaseData.EnableReviews, 1, this.ApplicationBaseData.EnableMove, this.ApplicationBaseData.EnableNewsLetter, this.ApplicationBaseData.EnableModule);
                done = Connection.Execute(sql);

                if (done)
                {
                    sql = string.Format(NetCms.Structure.Applications.Setup.AddApplicationPageSql, "links_new.aspx", "NetCms.Structure.Applications.Links.Pages.LinksAdd", this.ApplicationBaseData.ID);
                    done = Connection.Execute(sql);
                    if (!done)
                        this.AddErrorString("Non è stato possibile creare il record della pagina " + "links_new.aspx" + ".");
                }
                else
                    this.AddErrorString("Non è stato possibile creare il record dell'Applicazione, verificare se esiste già nella tabella 'Applicazioni' del database.");

            }
            catch (Exception ex)
            {
                //Trace
                this.AddErrorString("Non è stato possibile creare il record dell'Applicazione, verificare se esiste già nella tabella 'Applicazioni' del database.");
                NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);
            }

            return done;
        }*/
        //private bool AddTables()
        //{
        //    bool done = false;
        //    try
        //    {
        //        foreach (DataRow row in this.NetworksTable.Select(FilterID))
        //        {
        //            NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);
        //            string sql = string.Format(AddTableSql, network.SystemName);
        //            done = Connection.Execute(sql);
        //            if (done==false) throw new Exception();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //Trace
        //        /*this.AddErrorString("Non è stato possibile creare una o più tabelle dell'Applicazione nel database.");
        //        NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
        //        ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
        //        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile creare una o più tabelle dell'Applicazione Links nel database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "27");
        //    }
        //    return done;
        //}

        private void RollBack()
        {
            bool done = false;
            if (!(NetworkID > 0))
            {
                using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = session.BeginTransaction())
                {
                    try
                    {
                        Application dbApp = ApplicationBusinessLogic.GetApplicationById(this.ApplicationBaseData.ID, session);
                        ApplicationBusinessLogic.DeleteApplication(dbApp, session);
                        tx.Commit();
                        done = true;
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                    }
                }

                if (done == false)
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile rimuovere l'Applicazione Links dal database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "28");
            }
            //foreach (DataRow row in this.NetworksTable.Select(FilterID))
            //{
            //    NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);
            //    string sql = string.Format("drop table if exists {0}_docs_link", network.SystemName);
            //    Connection.Execute(sql);
            //}

        }
        public override void Uninstall()
        {
            base.Uninstall();

            //foreach (DataRow row in this.NetworksTable.Select(FilterID))
            foreach (BasicStructureNetwork network in this.NetworksTable.Where(x => !FilterID || (FilterID && x.ID == NetworkID)))
            {
                //NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);
                //string sql = string.Format("drop table if exists {0}_docs_link", network.SystemName);
                //Connection.Execute(sql);

                string sql = string.Format(sql = "DELETE homepages_moduli FROM folders INNER JOIN homepages_moduli_applicativi ON id_Folder = Folder_ModuloApplicativo INNER JOIN homepages_moduli ON id_Modulo = Modulo_ModuloApplicativo WHERE Tipo_Folder = " + this.ApplicationBaseData.ID + " AND Network_Folder = {0}", network.ID);
                Connection.Execute(sql);

                sql = string.Format(sql = "DELETE FROM folders WHERE Tipo_Folder = " + this.ApplicationBaseData.ID + " AND Network_Folder = {0}", network.ID);
                Connection.Execute(sql);
            }

            FluentSessionProvider.Instance.UpdateSessionFactoryRemove(Assembly.GetAssembly(this.GetType()));
        }
    }
}
