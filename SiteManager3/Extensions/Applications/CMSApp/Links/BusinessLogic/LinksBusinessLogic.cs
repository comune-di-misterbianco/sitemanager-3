﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using GenericDAO.DAO;
using NHibernate.Transform;
using NHibernate.Criterion;
using NetService.Utility.RecordsFinder;
using NetCms.DBSessionProvider;

namespace NetCms.Structure.Applications.Links
{
    public static class LinksBusinessLogic
    {

        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static LinksDocContent GetById(int id)
        {
            IGenericDAO<LinksDocContent> dao = new CriteriaNhibernateDAO<LinksDocContent>(GetCurrentSession());
            return dao.GetById(id);
        }

        public static IList<LinksDocContent> FindAllApplicationRecords()
        {
            IGenericDAO<LinksDocContent> dao = new CriteriaNhibernateDAO<LinksDocContent>(GetCurrentSession());
            return dao.FindAll();
        }

        public static LinksDocContent SaveOrUpdateLinksDocContent(LinksDocContent doc)
        {
            IGenericDAO<LinksDocContent> dao = new CriteriaNhibernateDAO<LinksDocContent>(GetCurrentSession());
            return dao.SaveOrUpdate(doc);
        }

        public static IList<LinksDocContent> FindLinksInFolders(List<int> folders,string sortField,bool descending)
        {
            IGenericDAO<LinksDocContent> dao = new CriteriaNhibernateDAO<LinksDocContent>(GetCurrentSession());

            SearchParameter idFolderSP = new SearchParameter(null,"ID",folders, Finder.ComparisonCriteria.In,false);
            SearchParameter foldersSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, idFolderSP);

            SearchParameter StatoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.Not, false);
            SearchParameter TrashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter documentSP = new SearchParameter(null,"Document", SearchParameter.RelationTypes.OneToMany,new SearchParameter[] {StatoSP,TrashSP,foldersSP });
            SearchParameter docLangSP = new SearchParameter(null,"DocLang", SearchParameter.RelationTypes.OneToOne,documentSP);

            //return dao.FindByCriteria(sortField, descending, new SearchParameter[] { docLangSP });
            return dao.FindByCriteria(new SearchParameter[] { docLangSP });
        }

        public static IList<LinksDocContent> FindLinksInFoldersWithLang(List<int> folders, string sortField, bool descending, int lang)
        {
            IGenericDAO<LinksDocContent> dao = new CriteriaNhibernateDAO<LinksDocContent>(GetCurrentSession());

            SearchParameter idFolderSP = new SearchParameter(null, "ID", folders, Finder.ComparisonCriteria.In, false);
            SearchParameter foldersSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, idFolderSP);

            SearchParameter StatoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.Not, false);
            SearchParameter TrashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter documentSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { StatoSP, TrashSP, foldersSP });

            SearchParameter spLang = new SearchParameter(null, "Lang", lang, Finder.ComparisonCriteria.Equals, false);
            
            SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, new SearchParameter[] { documentSP, spLang });

            //return dao.FindByCriteria(sortField, descending, new SearchParameter[] { docLangSP });
            return dao.FindByCriteria(new SearchParameter[] { docLangSP });
        }

        #region Metodi di Frontend

        public static LinksDocument GetDocumentRecord(int idLinkDoc)
        {
            IGenericDAO<LinksDocument> dao = new CriteriaNhibernateDAO<LinksDocument>(GetCurrentSession());

            SearchParameter applicationSP = new SearchParameter(null, "Application", 9, Finder.ComparisonCriteria.Equals, false);

            SearchParameter statoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.Not, false);

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter idSP = new SearchParameter(null, "ID", idLinkDoc, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { applicationSP, statoSP, trashSP, idSP });

        }

        public static IList<LinksDocContent> ListDocumentPreviewRecordByIdAndFolder(int idLink, int idFolder)
        {
            IGenericDAO<LinksDocContent> dao = new CriteriaNhibernateDAO<LinksDocContent>(GetCurrentSession());

            SearchParameter idFolderSP = new SearchParameter(null, "ID", idFolder, Finder.ComparisonCriteria.Equals, false);
            SearchParameter foldersSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, idFolderSP);


            SearchParameter documentSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { foldersSP });
            SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, documentSP);
            SearchParameter revisioneSP = new SearchParameter(null, "Revisione", SearchParameter.RelationTypes.OneToOne, docLangSP);

            SearchParameter idSP = new SearchParameter(null, "ID", idLink, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[] { revisioneSP, idSP });
        }

        public static IList<LinksDocument> ListDocumentRecords(int idFolder)
        {
            IGenericDAO<LinksDocument> dao = new CriteriaNhibernateDAO<LinksDocument>(GetCurrentSession());

            SearchParameter applicationSP = new SearchParameter(null, "Application", 9, Finder.ComparisonCriteria.Equals, false);

            SearchParameter statoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.Not, false);

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter folderSP = new SearchParameter(null, "Folder.ID", idFolder, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[] { applicationSP, statoSP, trashSP, folderSP });

        }

        #endregion
    }
}