using System.Web.UI;
using NetCms.Structure.Applications.Links;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace NetFrontend
{
    /// <summary>
    /// Links Content Handler
    /// </summary>
    public class LinksContentsHandler : ContentsHandler
    {
        private LinksDocument DocRecord;
        private FrontendFolder currentFolder;

        protected string ApplicationCSSClass
        {
            get { return "Links"; }
        }
        
        public LinksContentsHandler(PageData pagedata):base(pagedata)
        {
            PageData = pagedata;
            currentFolder = PageData.Folder;
            if (PageData.Network.HasCurrentDocument)
                DocRecord = LinksBusinessLogic.GetDocumentRecord(PageData.Network.CurrentDocument.ID);//NetFrontend.DAL.LinksDAL.GetDocumentRecordByID(PageData.Network.CurrentDocument.ID);
        }

        public override Control PreviewControl
        {
            get
            {
                #region old
                //    HtmlGenericControl div = new HtmlGenericControl("div");
                //    div.Attributes["class"] = ApplicationCSSClass + currentFolder.CssClass;

                //    div.InnerHtml = "<h1>" + currentFolder.Label + "</h1>";
                //    div.InnerHtml += "<p>" + currentFolder.Descrizione + "</p>";

                // var rows = LinksBusinessLogic.ListDocumentPreviewRecordByIdAndFolder(this.ContentID, this.PageData.Folder.ID);
                //NetFrontend.DAL.LinksDAL.ListDocumentPreviewRecordBySqlConditions("id_Link = " + this.ContentID + " AND Folder_Doc=" + this.PageData.Folder.ID);

                //    div.InnerHtml += "<p><ul>";

                //    foreach (var link in rows)
                //        div.InnerHtml += "<li class=\"contentLink\"><a href=\"" + link.Href.Replace("<%= WebRoot %>", PageData.AbsoluteWebRoot) + "\"><strong>" + (link.Revisione != null ? link.Revisione.Document.Nome : link.DocLang.Document.Nome) + "</strong></a><br /><em>" + (link.Revisione != null ? link.Revisione.Document.Descrizione : link.DocLang.Document.Descrizione) + "</em></li>";

                //    div.InnerHtml += "</ul></p>";
                //    return div;

                #endregion

                WebControl divBlock = new WebControl(HtmlTextWriterTag.Div);
                divBlock.CssClass = "row " + ApplicationCSSClass + "_" + currentFolder.CssClass;

                WebControl rowTitle = new WebControl(HtmlTextWriterTag.Div);
                rowTitle.CssClass = "row";

                WebControl colTitle = new WebControl(HtmlTextWriterTag.Div);
                colTitle.CssClass = "col-lg-12";
                WebControl phblock = new WebControl(HtmlTextWriterTag.Div);
                phblock.CssClass = "page-header";

                WebControl title = new WebControl(HtmlTextWriterTag.H1);
                title.Controls.Add(new LiteralControl(currentFolder.Label));


                phblock.Controls.Add(title);

                WebControl descriptions = new WebControl(HtmlTextWriterTag.P);
                descriptions.Controls.Add(new LiteralControl(currentFolder.Descrizione));
                                     
                colTitle.Controls.Add(phblock);

                colTitle.Controls.Add(descriptions);


                rowTitle.Controls.Add(colTitle);

                divBlock.Controls.Add(rowTitle);

                WebControl divBlockLink = new WebControl(HtmlTextWriterTag.Div);
                divBlockLink.CssClass = "row";

                WebControl colBlockLink = new WebControl(HtmlTextWriterTag.Div);
                colBlockLink.CssClass = "col-lg-12";

                WebControl divLinkGroup = new WebControl(HtmlTextWriterTag.Div);
                divLinkGroup.CssClass = "list-group";

                var rows = LinksBusinessLogic.ListDocumentPreviewRecordByIdAndFolder(this.ContentID, this.PageData.Folder.ID);
                foreach (var link in rows)
                {
                    WebControl a = new WebControl(HtmlTextWriterTag.A);
                    a.Attributes.Add("href", link.Href.Replace(" <%= WebRoot %> ", PageData.AbsoluteWebRoot));
                    a.CssClass = "list-group-item";

                    WebControl titleLink = new WebControl(HtmlTextWriterTag.H4);
                    titleLink.CssClass = "list-group-item-heading";
                    titleLink.Controls.Add(new LiteralControl(link.Revisione != null ? link.Revisione.Document.Nome : link.DocLang.Document.Nome));

                    a.Controls.Add(titleLink);

                    WebControl DescriptionLink = new WebControl(HtmlTextWriterTag.P);
                    DescriptionLink.CssClass = "list-group-item-text";
                    DescriptionLink.Controls.Add(new LiteralControl(link.Revisione != null ? link.Revisione.Document.Descrizione : link.DocLang.Document.Descrizione));

                    a.Controls.Add(DescriptionLink);


                    divLinkGroup.Controls.Add(a);
                }   
                              
                colBlockLink.Controls.Add(divLinkGroup);
                divBlockLink.Controls.Add(colBlockLink);


                divBlock.Controls.Add(divBlockLink);

                return divBlock;

            }
        }
        public override Control Control
        {
            get
            {
                WebControl divBlock = new WebControl(HtmlTextWriterTag.Div);
                divBlock.CssClass = "container " + ApplicationCSSClass + "_" + currentFolder.CssClass;

                WebControl rowTitle = new WebControl(HtmlTextWriterTag.Div);
                rowTitle.CssClass = "row";

                WebControl colTitle = new WebControl(HtmlTextWriterTag.Div);
                colTitle.CssClass = "col-lg-12";

                WebControl phblock = new WebControl(HtmlTextWriterTag.Div);
                phblock.CssClass = "page-header";

                WebControl title = new WebControl(HtmlTextWriterTag.H1);
                title.Controls.Add(new LiteralControl(currentFolder.Label));
                
                phblock.Controls.Add(title);

                WebControl descriptions = new WebControl(HtmlTextWriterTag.P);
                descriptions.Controls.Add(new LiteralControl(currentFolder.Descrizione));

                colTitle.Controls.Add(phblock);

                colTitle.Controls.Add(descriptions);

                rowTitle.Controls.Add(colTitle);

                divBlock.Controls.Add(rowTitle);

                WebControl divBlockLink = new WebControl(HtmlTextWriterTag.Div);
                divBlockLink.CssClass = "row";

                WebControl colBlockLink = new WebControl(HtmlTextWriterTag.Div);
                colBlockLink.CssClass = "col-lg-12";

                //WebControl divLinkGroup = new WebControl(HtmlTextWriterTag.Div);
                //divLinkGroup.CssClass = "list-group";

                IList<LinksDocument> links = LinksBusinessLogic.ListDocumentRecords(PageData.Folder.ID);

                foreach (LinksDocument link in links)               
                {
                    if (link.ContentRecord != null)
                    {
                        string href = (link.ContentRecord as LinksDocContent).Href.Replace("<%= WebRoot %>", PageData.AbsoluteWebRoot);
                        string css = href.ToLower().StartsWith("http://") ? " extenal" : "";
                        
                        //WebControl titleLink = new WebControl(HtmlTextWriterTag.H4);
                        //titleLink.CssClass = "list-group-item-heading";

                        WebControl ul = new WebControl(HtmlTextWriterTag.Ul);
                        ul.CssClass = "list-group";


                        WebControl li = new WebControl(HtmlTextWriterTag.Li);
                        li.CssClass = "list-group-item";

                        WebControl a = new WebControl(HtmlTextWriterTag.A);
                        a.Attributes.Add("href", href);
                        //a.CssClass = "list-group-item " + css;
                        a.Controls.Add(new LiteralControl(link.Nome));

                        WebControl DescriptionLink = new WebControl(HtmlTextWriterTag.P);
                        //DescriptionLink.CssClass = "list-group-item-text";
                        DescriptionLink.Controls.Add(new LiteralControl(link.Descrizione));

                        li.Controls.Add(a);
                        li.Controls.Add(DescriptionLink);

                        colBlockLink.Controls.Add(li);

                       

                      //  divLinkGroup.Controls.Add(DescriptionLink);

                      //  divLinkGroup.Controls.Add(a);
                    }
                }

               // colBlockLink.Controls.Add(colBlockLink);
                divBlockLink.Controls.Add(colBlockLink);


                divBlock.Controls.Add(divBlockLink);

                return divBlock;
            }
        }

    }   
}