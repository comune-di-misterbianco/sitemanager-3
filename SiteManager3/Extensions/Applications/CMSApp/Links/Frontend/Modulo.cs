using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using NetFrontend.Homepages;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.Structure.Applications.Homepage;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS;
using System.Collections.Generic;
using NetCms.Structure.Applications.Links;

/// <summary>
/// Summary description for LayoutModuloBlock
/// </summary>
/// 
namespace NetFrontend
{
    [NetCms.Homepages.HomepageModuleDefiner(LinksApplication.ApplicationID, "Collegamenti Ipertestuali", LinksApplication.ApplicationID, NetCms.Homepages.HomepageModuleDefiner.ModuleType.CmsApp)]
    public class LayoutModuloLinkBlock : LayoutModuloBlock
    {
        protected override string LocalTypeCssClass
        {
            get { return "modulo_link"; }
        }
        
        public override string Nome
        {
            get
            {
                return Folder.Label;
            }
        }

        public override string Title
        {
            get { return Nome; }
        }

        public override bool HasModuli
        {
            get { return false; }
        }

        private string OrderString
        {
            get
            {
                string output = "";
                switch (Order)
                {
                    case 0:
                        output = " Data";
                        break;
                    case 1:
                        output = " Clicks";
                        break;
                    case 2:
                        output = " Label";
                        break;
                }
                return output;
            }
        }
        private int Order;
        private bool SubFolders;
        private int Records;
        private bool ShowDesc;
        private int DescLength;

        private StructureFolder Folder;
        private int FolderID;
        private String FolderTree;

        public LayoutModuloLinkBlock(NetCms.Structure.Applications.Homepage.Model.Modulo modulo)
            : base(modulo)
        {
            ModuloApplicativo moduloApp = HomePageBusinessLogic.GetModuloById(modulo.ID) as ModuloApplicativo;
            int id = moduloApp.FolderId;

            Order = moduloApp.Order; 
            SubFolders = moduloApp.Subfolders == 1; 
            Records = moduloApp.Records; 
            ShowDesc = moduloApp.ShowDesc == 1; 
            DescLength = moduloApp.DescLength; 

            Folder = FolderBusinessLogic.GetById(id); 
            FolderID = id;
            FolderTree = Folder.Tree; 

        }

        //private List<int> BuildSqlQuery()
        //{
            //if (SubFolders)
            //{
            //    DataRow[] folders = NetFrontend.DAL.FileSystemDAL.ListFoldersRecordsBySqlConditions("Tree_Folder LIKE '%." + FolderTree + ".%' AND id_Folder <> " + FolderID);
            //    return string.Join(" OR ", "Folder_Doc = " +folders.Select(x=>x["id_Folder"].ToString()));
            //}
            //else return "Folder_Doc = " + FolderID;
        //}

        public override HtmlGenericControl GetLayoutBlock(PageData PageData)
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "panel panel-default " + CssClass;

            HtmlGenericControl panelHeading = new HtmlGenericControl("div");
            panelHeading.Attributes["class"] = "panel-heading";
                         
            HtmlGenericControl h4 = new HtmlGenericControl("h4");
            h4.InnerHtml = Nome;
            h4.Attributes["class"] = "link-title";
            panelHeading.Controls.Add(h4);
  
            div.Controls.Add(panelHeading);

            HtmlGenericControl contentModulo = new HtmlGenericControl("div");
            List<int> fIds = new List<int>();
            fIds.Add(FolderID);

            if (SubFolders)
            {
                List<StructureFolder> subfolders = Folder.SubFolders.ToList();
                foreach (StructureFolder folder in subfolders.Where(x => x.Trash == 0))
                {
                    fIds.Add(folder.ID);
                }
            }

            List<LinksDocContent> Links;
            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                Links = LinksBusinessLogic.FindLinksInFoldersWithLang(fIds, OrderString, Order == 1, LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.ID).ToList();
            }
            else
            {
                Links = LinksBusinessLogic.FindLinksInFolders(fIds, OrderString, Order == 1).ToList();
            }
            switch (Order)
            {
                case 0:
                    Links = Links.OrderBy(x=>x.DocLang.Document.Data).ToList();
                    break;
                case 1:
                    Links = Links.OrderBy(x => x.Clicks).ToList();
                    break;
                case 2:
                    Links = Links.OrderBy(x => x.DocLang.Label).ToList();
                    break;
            }
            //DataRow[] Links = NetFrontend.DAL.LinksDAL.ListDocumentRecordsBySqlConditions(sql, order: OrderString);


            #region old
            //foreach(LinksDocContent link in Links)
            //{
            //    string href = link.Href.Replace("<%= WebRoot %>", "");
            //    string css = href.ToLower().StartsWith("http://") ? "class=\"extenal\"" : "";

            //    contentModulo.InnerHtml += "<li><a " + css + " href=\"" + href + "\"><span>" + link.DocLang.Label + "</span></a>";
            //    if (ShowDesc)
            //    {
            //        string desc = link.DocLang.Descrizione;
            //        if (desc.Length > DescLength)
            //            desc = desc.Remove(DescLength) + "...";
            //        contentModulo.InnerHtml += "<p>" + desc + "</p>";
            //    }
            //    contentModulo.InnerHtml += "</li>";
            //}
            //if (contentModulo.InnerHtml.Length != 0)
            //{
            //    contentModulo.InnerHtml = "<ul>" + contentModulo.InnerHtml + "</ul>";
            //}
            //HtmlGenericControl h3 = new HtmlGenericControl("h3");
            //h3.InnerHtml = Nome;
            //div.Controls.Add(h3);
            //div.Controls.Add(contentModulo);

            //return div;
            #endregion

            WebControl divList = new WebControl(HtmlTextWriterTag.Div);
            divList.CssClass = "list-group";

            foreach (LinksDocContent link in Links)
            {
                string href = link.Href.Replace("<%= WebRoot %>", "");
                string css = href.ToLower().StartsWith("http://") ? "class=\"extenal\"" : "";

                //WebControl li = new WebControl(HtmlTextWriterTag.Li);
                //li.CssClass = "list-group-item";
                   
                WebControl a = new WebControl(HtmlTextWriterTag.A);
                a.Attributes.Add("href", href);
                a.CssClass = "list-group-item " + css;

                WebControl titleLink = new WebControl(HtmlTextWriterTag.H4);
                titleLink.CssClass = "list-group-item-heading";
                titleLink.Controls.Add(new LiteralControl(link.DocLang.Label));

                a.Controls.Add(titleLink);

                if (ShowDesc)
                {
                    string desc = link.DocLang.Descrizione;
                    if (desc.Length > DescLength)
                        desc = desc.Remove(DescLength) + "...";
                  
                    WebControl DescriptionLink = new WebControl(HtmlTextWriterTag.P);
                    DescriptionLink.CssClass = "list-group-item-text";
                    DescriptionLink.Controls.Add(new LiteralControl(desc));

                    a.Controls.Add(DescriptionLink);
                }
                        
                divList.Controls.Add(a);
            }

            div.Controls.Add(divList);

            return div;
        }
    }
}
