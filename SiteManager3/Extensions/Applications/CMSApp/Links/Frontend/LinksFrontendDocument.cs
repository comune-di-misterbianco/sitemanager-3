using System.Data;
using NetFrontend;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Reflection;

/// <summary>
/// Questa classe viene chiamata tramite Reflection
/// </summary>
namespace NetCms.Structure.Applications.Links
{
    [FrontendDocumentsDefiner(9, "Links")]
    public class LinksFrontendDocument : FrontendDocument
    {
        public override string FrontendUrl
        {
            get
            {
                //DataRow Link = NetFrontend.DAL.LinksDAL.GetDocumentRecordByID(this.ID);
                LinksDocument Link = DocumentBusinessLogic.GetById(this.ID) as LinksDocument;
                
                if (Link != null)
                {
                    return (Link.ContentRecord as LinksDocContent).Href.Replace("<%= WebRoot %>", "");
                }
                else
                    return string.Empty;
            }
        }

        public LinksFrontendDocument(Document doc, FrontendFolder folder):base(doc, folder) {}
        public LinksFrontendDocument(int docID, FrontendFolder folder) : base(docID, folder) { }
    }
}