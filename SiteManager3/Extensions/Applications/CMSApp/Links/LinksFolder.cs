﻿using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using NetForms;
using System.Reflection;
using NetCms.Grants;
using NetCms.Structure.Applications.Homepage;

namespace NetCms.Structure.Applications.Links
{
    public partial class LinksFolder : StructureFolder
    {
        private NetCms.Structure.Applications.Application _RelativeApplication;
        public override NetCms.Structure.Applications.Application RelativeApplication
        {
            get
            {
                if (_RelativeApplication == null)
                    _RelativeApplication = Applications.ApplicationsPool.ActiveAssemblies[LinksApplication.ApplicationID.ToString()];
                return _RelativeApplication;
            }
        }
 
        public override bool ReviewEnabled
        {
            get
            {
                return this.RelativeApplication.EnableReviews;
            }
        }
        
        public override bool AllowMove
        {
            get
            {  
                return RelativeApplication.EnableMove;
            }
        }

        public override ToolbarButtonsCollection ToolbarButtons
        {
            get
            {
                ToolbarButtonsCollection _ToolbarButtons = base.ToolbarButtons;
                    if (this.Criteria[CriteriaTypes.Create].Allowed)
                        _ToolbarButtons.Add(new NetCms.Structure.Applications.ApplicationToolbarButton("Links_new.aspx?folder=" + ID, NetCms.GUI.Icons.Icons.Link_Add, "Nuovo Collegamento Ipertestuale", this.StructureNetwork, this.RelativeApplication));
                return _ToolbarButtons;

            }
        }

        public LinksFolder()
            : base()
        { }

        public LinksFolder(Networks.NetworkKey network)
                    : base(network) 
        {
        
        }

        public override void addSearchResult(StructureTable table, StructureFolder.StandardSearchFields pars)
        {
               base.addSearchResult(table, pars);
        }


        //protected override int CreateDocContent(DataRow docRecord, DataSet sourceDB, string sourceFileName, params NetCms.Importer.Importer[] imp)
        //{
        //    string insertInto = "INSERT INTO " + this.Network.SystemName + "_docs_link ";
        //    string insertIntoFields = "";
        //    string insertIntoValues = "";
            
        //    foreach (DataColumn col in docRecord.Table.Columns)
        //    {
        //        if (col.ColumnName.ToLower().EndsWith("_link") && !col.ColumnName.ToLower().StartsWith("id_") && !col.ColumnName.ToLower().Contains("target_link"))
        //        {
        //            string comma = insertIntoFields.Length > 0 ? "," : "";

        //            insertIntoFields += comma + col.ColumnName;

        //            string value = docRecord[col.ColumnName].ToString();

        //            if (value != "NULL")
        //                insertIntoValues += comma + "'" + value.Replace("'", "''") + "'";
        //            else
        //                insertIntoValues += comma + value.Replace("'", "''");
        //            //insertIntoValues += comma + "'" + value + "'";
        //            //break;
        //        }
        //    }

        //    insertInto += "(" + insertIntoFields + ") VALUES (" + insertIntoValues + ")";
        //    int result = this.Network.Connection.ExecuteInsert(insertInto);
        //    if (result == -1)
        //        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile inserire i dati del Link durante l'importazione", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "29");
        //    return result;
        //}

        public override bool DeleteModuloApplicativo(NHibernate.ISession session)
        {
            return HomePageBusinessLogic.DeleteModuliApplicativiByFolder(this.ID, session);
        }

        public override bool DeleteSomething(NHibernate.ISession session)
        {
            return true;
        }
    }
}
