﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Data;

namespace NetCms.Structure.Applications.Links
{
    public class LinksApplication : NetCms.Structure.Applications.Application
    {
        public const string ApplicationContextFrontLabel = "Links.Frontend";
        public const string ApplicationContextBackLabel = "Links.Backoffice";
        public const int ApplicationID = 9;

        public override string[] AllowedSubfolders
        {
            get { return new[] { "files" }; }
        }
        
        public override bool EnableNewsLetter
        { get { return false; } }
        public override bool EnableMove
        { get { return true; } }
        public override bool EnableModule
        { get { return true; } }
        public override bool EnableReviews
        {
            get { return true; }
        }
        public override string Namespace
        { get { return "NetCms.Structure.Applications.Links"; } }
        public override string SystemName
        { get { return "links"; } }
        public override string Label
        { get { return "Collegamenti Ipertestuali"; } }
        public override string LabelSingolare
        { get { return "Collegamento Ipertestuale"; } }
        public override int ID
        { get { return ApplicationID; } }
        public override bool IsSystemApplication { get { return true; } }

        public override NetCms.Structure.Applications.Setup Installer
        {
            get
            {
                if (_Installer == null)
                    _Installer = new NetCms.Structure.Applications.Links.Setup(this);
                return _Installer;
                
            }
        }

        private NetCms.Structure.Applications.Setup _Installer;
        
        public LinksApplication():base(null)
        {
            base.Assembly = Assembly.GetAssembly(this.GetType());
        }
        public LinksApplication(Assembly assembly)
            : base(assembly)
        {
        }
        /*
        protected override List<ApplicationValidation> LocalCheck()
        {
            List<ApplicationValidation> validationArray = new List<ApplicationValidation>();
            
            foreach (DataRow row in this.Installer.NetworksTable.Select(this.Installer.FilterID))
            {
                NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);
                if (network.CmsStatus != NetCms.Networks.NetworkCmsStates.NotCreated)
                {
                    DataTable check = Connection.SqlQuery("SHOW TABLES LIKE '" + network.SystemName + "_docs_link'");
                    if (check.Rows.Count != 1)
                        validationArray.Add(new ApplicationValidation("Tabella Documenti Link nella rete: '" + network.SystemName + "'", false, "Tabella dei documenti Link non presente per la rete: '" + network.SystemName + "'"));
                    else validationArray.Add(new ApplicationValidation("Tabella Documenti Link nella rete: '" + network.SystemName + "'", true, ""));
                }
            }
            return validationArray;
        }

        protected override bool LocalRepair()
        {
            return true;
        }
    */
    }


}
