﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;

namespace NetCms.Structure.Applications.Links
{
    public class LinksReview : NetCms.Structure.WebFS.DocumentReviewPage
    {
        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Link_Add; }
        }
        public override string PageTitle
        {
            get { return "Nuova Revisione Link '" + this.CurrentDocument.Nome + "'"; }
        }

        public LinksReview() { }

        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "container-fluid";

            PageData.CurentObjectInfoColtrol = new SideCurentObjectData("CurentDocSideInfo", this.CurrentDocument, PageData).getControl();

            NetExternalField content;
            NetFormTable contentTable = LinksDocument.DocsContentFormTable(PageData);
            contentTable.CssClass = "netformtable_contenttable";

            NetFormTable Link;
            if (ReviewID == 0)
                Link = new NetFormTable("links_docs_contents", "id_Link", this.CurrentFolder.StructureNetwork.Connection);
            else
                Link = new NetFormTable("links_docs_contents", "id_Link", this.CurrentFolder.StructureNetwork.Connection, this.ContentID);

            Link.CssClass = "link_contentField";

            #region Campi
            /*
            if (this.Account.GUI.Editor == NetCms.Users.SitemanagerUserGUI.RichTextEditors.FreeTextBox)
            {
                NetRichTextBoxCMS Testo = new NetRichTextBoxCMS("Testo", "Href_Link", PageData);
                Link.addField(Testo);
            }
            if (this.Account.GUI.Editor == NetCms.Users.SitemanagerUserGUI.RichTextEditors.TinyMCE)
            {
                NetTinyMCE Testo = new NetTinyMCE("Testo", "Href_Link", PageData);
                Link.addField(Testo);
            }*/
            NetTextBoxPlus Testo = new NetTextBoxPlus("Indirizzo", "Href_Link", NetTextBoxPlus.ContentTypes.NormalText);
            Testo.Required = true;
            Link.addField(Testo);

            NetHiddenField Clicks = new NetHiddenField("Clicks_Link");
            Clicks.Value = "0";
            Link.addField(Clicks);
            #endregion

            NetForm form = new NetForm();
            form.CssClass = "linkform";

            form.AddTable(contentTable);
            form.AddTable(Link);
            form.SubmitLabel = "Salva Revisione";

            content = new NetExternalField("docs_contents", "id_Link");
            Link.ExternalFields.Add(content);

            #region PostBack

            string errors = "";
            if (PageData.IsPostBack)
            {
                if (PageData.SubmitSource == "send")
                {
                    errors = form.serializedInsert(HttpContext.Current.Request.Form);

                    if (form.LastOperation == NetForm.LastOperationValues.OK)
                    {
                        DocumentOverview.insertReview(DocumentBusinessLogic.GetDocLangById(DocLang), LinksBusinessLogic.GetById(contentTable.LastInsert), this.ContentID, PageData);
                        string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha salvato una nuova revisione del link '" + this.CurrentDocument.Nome + "' relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    }

                    if (form.LastOperation == NetForm.LastOperationValues.Error)
                    {
                        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante la revisione del link " + this.CurrentDocument.Nome + " del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "547");
                    }

                    //SISTEMARE CON CACHE 2° LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                    this.CurrentFolder.Invalidate();

                    NetCms.GUI.PopupBox.AddSessionMessage(errors, NetCms.GUI.PostBackMessagesType.Normal, form.LastOperation == NetForm.LastOperationValues.OK ? true : false);

                }
            }
            else
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di creazione di una nuova revisione del link '" + this.CurrentDocument.Nome + "' relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            }
            #endregion


            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");

            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerText = "Nuova revisione";
            fieldset.Controls.Add(legend);

            HtmlGenericControl contentDiv = new HtmlGenericControl("div");
            contentDiv.Attributes["class"] = "contentForm newsform reviewform col-md-12";
            contentDiv.Controls.Add(contentTable.getForm());
            contentDiv.Controls.Add(Link.getForm());            

            HtmlGenericControl pButtons = new HtmlGenericControl("p");
            pButtons.Attributes["class"] = "buttonscontainer";

            Button bt = new Button();
            bt.Text = "Salva Revisione";
            bt.ID = "send";
            bt.CssClass = "button";
            bt.OnClientClick = "javascript: setSubmitSource('send')";
            pButtons.Controls.Add(bt);

            contentDiv.Controls.Add(pButtons);

            WebControl row = new WebControl(HtmlTextWriterTag.Div);
            row.CssClass = "row";

            row.Controls.Add(contentDiv);            

            fieldset.Controls.Add(row);

            div.Controls.Add(fieldset);


            //div.Controls.Add(form.getForms("Nuova Revisione"));


            return div;
        }
    }
}

