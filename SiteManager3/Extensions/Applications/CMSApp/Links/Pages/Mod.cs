﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.Structure.Grants;
using NetCms.Structure.WebFS;
using System.Linq;
using NetCms.Grants;
using NetCms.GUI;

namespace NetCms.Structure.Applications.Links.Pages
{
    public class Mod : NetCms.GUI.NetworkPage
    {
        /*private int LinkID
        {
            get
            {
                DataTable Link = this.CurrentFolder.Network.Connection.SqlQuery("SELECT * FROM Docs INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang INNER JOIN Docs_Link ON id_Link = Content_DocLang WHERE Lang_DocLang = " + PageData.SelectedLang + " AND id_Doc = " + Doc.ID);
                if (Link.Rows.Count > 0)
                    return int.Parse(Link.Rows[0]["id_Link"].ToString());
                return 0;
            }
        }*/
        
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }

        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Modifica Collegamento Ipertestuale"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Link_Edit; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        private NetCms.Structure.WebFS.Document _Doc;
        public NetCms.Structure.WebFS.Document Doc
        {
            get
            {
                if (_Doc == null)
                {
                    NetUtility.RequestVariable docvar = new NetUtility.RequestVariable("id", HttpContext.Current.Request.QueryString);
                    if (docvar.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                        _Doc = this.CurrentFolder.Documents.Where(x => x.ID == docvar.IntValue) as LinksDocument;
                        //_Doc = this.CurrentFolder.Documents[docvar.IntValue.ToString()];
                }
                try
                {
                    if (_Doc == null || !_Doc.Grant || _Doc.Criteria[CriteriaTypes.Modify].Value == GrantsValues.Deny)
                        throw new Exception("_Doc uguale a null oppure permessi negati nella classe Mod di Links");
                }
                catch (Exception ex)
                {
                    /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                    ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "33");
                }
                return _Doc;
            }
        }


        public Mod()
        {
            ApplicationContext.SetCurrentComponent(LinksApplication.ApplicationContextBackLabel);
            if (this.CurrentDocument.Criteria[CriteriaTypes.Modify].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", NetCms.GUI.PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina di modifica di un link", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else if (!IsPostBack)
                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha effettuato l'accesso a alla pagina di modifica del link " + this.CurrentDocument.Nome + " del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
        }

        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);


            PageData.CurentObjectInfoColtrol = new SideCurentObjectData("CurentDocSideInfo", Doc, PageData).getControl();
            NetFormTable Link = new NetFormTable("links_docs_contents", "id_Link", this.CurrentFolder.StructureNetwork.Connection, this.CurrentDocument.Content);
            #region Campi

            CMSTextBoxPlus href = new CMSTextBoxPlus("Collegamento Ipertestuale", "Href_Link", PageData);
            href.Size = 100;
            href.MaxLenght = 1000;
            href.Required = true;
            href.CurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID;
            Link.addField(href);

            #endregion

            NetForm form = new NetForm();
            form.AddTable(Link);
            form.SubmitLabel = "Salva Modifiche";

            #region PostBack

            if (PageData.IsPostBack)
            {
                if (PageData.SubmitSource == "send")
                {
                    string message = form.serializedUpdate(HttpContext.Current.Request.Form);
                    
                    
                    if (form.LastOperation == NetForm.LastOperationValues.OK)
                        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha modificato con successo il link " + this.CurrentDocument.Nome + " del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                    
                    if(form.LastOperation == NetForm.LastOperationValues.Error)
                        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante la modifica del link " + this.CurrentDocument.Nome + " del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "546");
                    
                    //NetUtility.RequestVariable Href_Link = new NetUtility.RequestVariable("Href_Link",PageData.Request.Form);
                    //SISTEMARE CON CACHE 2° LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                    this.CurrentFolder.Invalidate();
                    //if(Href_Link.IsValid()&&Href_Link.StringValue!=null)
                    //     PageData.Conn.Execute("UPDATE Docs_Link SET Href_Link='" + Href_Link.StringValue.Trim().ToLower() + "' WHERE id_Link = "+LinkID);                                    

                    if (this.CurrentFolder.EnableRss)
                        this.CurrentFolder.RssManager.SaveXML();

                    G2Core.Common.RequestVariable tags = new G2Core.Common.RequestVariable("Tags");
                    Tags.AddTagsForDoc(tags.StringValue, this.CurrentDocument.ID.ToString(), this.CurrentFolder.StructureNetwork.Connection);

                    NetCms.GUI.PopupBox.AddSessionMessage(message, PostBackMessagesType.Notify, form.LastOperation == NetForm.LastOperationValues.OK ? true : false);
                }
            }
            #endregion

            //return form.getForms(PageData.InfoToolbar.Title);
            div.Controls.Add(form.getForms(PageData.InfoToolbar.Title));
            return div;
        }
    }
}
