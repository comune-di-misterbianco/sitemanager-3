﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Linq;
using NetCms.Grants;

namespace NetCms.Structure.Applications.Links.Pages
{
    public class LinksAdd : NetCms.GUI.NetworkPage
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }

        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Creazione Nuovo Collegamento Ipertestuale"; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Link_Add; }
        }

        public LinksAdd()
        {
            ApplicationContext.SetCurrentComponent(LinksApplication.ApplicationContextBackLabel);
            if (this.CurrentFolder.Criteria[CriteriaTypes.Create].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina di aggiunta di un nuovo link", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else if (!IsPostBack)
                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha effettuato l'accesso alla pagina di creazione di un nuovo Link nel portale "+NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");

        }

        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "container-fluid";

            NetFormTable netDocs = LinksDocument.NetworkDocsFormTable(PageData);
            netDocs.CssClass = "netformtable_netDocs";

            NetFormTable docs = LinksDocument.DocsFormTable(PageData, 9);
            docs.CssClass = "netformtable_docs";

            NetFormTable langs = LinksDocument.LanguageFormTable(PageData);
            langs.CssClass = "netformtable_langs";

            NetExternalField docsToNetDocs;
            NetExternalField docsToLinkDoc;
            NetExternalField contentForDocLang;
            NetExternalField content;

            NetFormTable contentTable = LinksDocument.DocsContentFormTable(PageData);
            contentTable.CssClass = "netformtable_contenttable";

            NetFormTable Link = new NetFormTable("links_docs_contents", "id_Link", this.CurrentFolder.StructureNetwork.Connection);
            Link.CssClass = "link_contentfield";

            NetFormTable LinkDoc = new NetFormTable("links_docs", "id_docs", this.CurrentFolder.StructureNetwork.Connection);


            CMSTextBoxPlus href = new CMSTextBoxPlus("Collegamento Ipertestuale (se l'indirizzo è esterno al CMS, deve iniziare con http://)", "Href_Link", PageData);
            href.Size = 100;
            href.MaxLenght = 1000;
            href.Required = true;
            href.CurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID; 
            Link.addField(href);

            NetForm form = new NetForm();

            form.AddTable(contentTable);
            form.AddTable(Link);

            form.AddTable(netDocs);
            form.AddTable(docs);
            form.AddTable(LinkDoc);
            
            form.AddTable(langs);
            form.SubmitLabel = "Crea";

            if (!this.CurrentFolder.ReviewEnabled && !this.CurrentFolder.IsRootFolder)
            {
                contentForDocLang = new NetExternalField("docs_contents", "Content_DocLang");
                langs.ExternalFields.Add(contentForDocLang);
            }

            content = new NetExternalField("docs_contents", "id_Link");
            Link.ExternalFields.Add(content);

            docsToNetDocs = new NetExternalField("network_docs", "id_Doc");
            docs.ExternalFields.Add(docsToNetDocs);

            docsToLinkDoc = new NetExternalField("network_docs", "id_docs");
            LinkDoc.ExternalFields.Add(docsToLinkDoc);

            #region PostBack

            string errors = "";
            if (PageData.IsPostBack)
            {
                if (PageData.SubmitSource == "send")
                {
                    errors = form.serializedInsert(HttpContext.Current.Request.Form);
                    NetUtility.RequestVariable name = new NetUtility.RequestVariable("Name_Doc", NetUtility.RequestVariable.RequestType.Form);

                    string Href_Link = HttpContext.Current.Request.Form["Href_Link"];
                    if (netDocs.LastInsert > 0 && name.IsValid())
                    {
                        bool done = false;
                        try
                        {
                            NetCms.Structure.WebFS.Document document = DocumentBusinessLogic.GetById(netDocs.LastInsert);
                            document.PhysicalName = name.OriginalValue;//name.StringValue;
                            document.DocLangs.Where(x => x.Lang == PageData.DefaultLang).First().Label = name.OriginalValue; //name.StringValue;
                            DocumentBusinessLogic.SaveOrUpdateDocument(document);
                            //done = this.CurrentFolder.Network.Connection.Execute("UPDATE Docs SET Name_Doc='" + name.StringValue + "' WHERE id_Doc = " + docs.LastInsert);
                            //done = done & this.CurrentFolder.Network.Connection.Execute("UPDATE Docs_Lang SET Label_DocLang='" + name.StringValue + "' WHERE Lang_DocLang = " + PageData.DefaultLang.ToString() + " AND Doc_DocLang = " + docs.LastInsert);
                            if (Href_Link != null)
                            {
                                Href_Link = Href_Link.Replace(NetCms.Structure.Utility.ImportContentFilter.WebRoot, "");
                                if (PageData.Paths.AbsoluteWebRoot.Length > 0)
                                    Href_Link = Href_Link.Replace(PageData.Paths.AbsoluteWebRoot, "");
                                Href_Link = Href_Link.ToLower();
                                Href_Link = NetCms.Structure.Utility.ImportContentFilter.WebRoot + Href_Link;

                                LinksDocContent contenuto = LinksBusinessLogic.GetById(contentTable.LastInsert);
                                contenuto.Href = Href_Link.Trim();
                                contenuto.Clicks = 0;
                                LinksBusinessLogic.SaveOrUpdateLinksDocContent(contenuto);
                                //this.CurrentFolder.Network.Connection.Execute("UPDATE Docs_Link SET Href_Link='" + Href_Link.Trim() + "', Clicks_Link='0' WHERE id_Link = " + Link.LastInsert);
                            }
                            if (this.CurrentFolder.ReviewEnabled || this.CurrentFolder.Tipo == Homepage.HomepageApplication.ApplicationID) //|| this.CurrentFolder.IsRootFolder)
                            {
                                DocumentOverview.insertReview(DocumentBusinessLogic.GetDocLangById(langs.LastInsert), LinksBusinessLogic.GetById(contentTable.LastInsert), 0, PageData);

                                //SISTEMARE CON CACHE 2° LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                                this.CurrentFolder.Invalidate();
                            }
                            G2Core.Common.RequestVariable tags = new G2Core.Common.RequestVariable("Tags");
                            Tags.AddTagsForDoc(tags.StringValue, netDocs.LastInsert.ToString(), this.CurrentFolder.StructureNetwork.Connection);

                            done = true;

                            if (this.CurrentFolder.EnableRss)
                                this.CurrentFolder.RssManager.SaveXML();

                            this.CurrentFolder.Invalidate();
                            
                            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha creato con successo un nuovo link nel portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");                                                       
                            NetCms.GUI.PopupBox.AddSessionMessage(errors, NetCms.GUI.PostBackMessagesType.Success);
                        }
                        catch (Exception ex)
                        {
                            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                        }

                        if (done == false)
                            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile inserire i dati del link nel database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "32");
                        
                            
                    }
                    else
                    {
                        NetCms.GUI.PopupBox.AddMessage(errors, NetCms.GUI.PostBackMessagesType.Error);
                    }
                }
            }
            #endregion

            #region Creazione dei controlli

            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");

            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerText = "Nuovo Collegamento Ipertestuale";
            fieldset.Controls.Add(legend);

            HtmlGenericControl contentDiv = new HtmlGenericControl("div");
            contentDiv.Attributes["class"] = "contentForm newsform col-md-12";

            contentDiv.Controls.Add(docs.getForm());
            contentDiv.Controls.Add(langs.getForm());
            contentDiv.Controls.Add(Link.getForm());

            //PageData.InfoToolbar.Title = "Nuovo Collegamento Ipertestuale";
            //div.Controls.Add(fieldset);
            HtmlGenericControl pButtons = new HtmlGenericControl("p");
            pButtons.Attributes["class"] = "buttonscontainer";

            Button bt = new Button();
            bt.Text = "Invia";
            bt.ID = "send";
            bt.CssClass = "button";
            bt.OnClientClick = "javascript: setSubmitSource('send')";
            pButtons.Controls.Add(bt);

            contentDiv.Controls.Add(pButtons);

            WebControl row = new WebControl(HtmlTextWriterTag.Div);
            row.CssClass = "row";

            row.Controls.Add(contentDiv);

            fieldset.Controls.Add(row);

            div.Controls.Add(fieldset);

            #endregion

            return div;
        }
    }
}
