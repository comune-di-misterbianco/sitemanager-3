﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using NetCms.Grants;

namespace NetCms.Structure.Applications.Links.Pages
{
    public class Details : NetCms.Structure.WebFS.DocumentDetailsPage
    {

        protected override ToolbarButton ReserveForNewsletter
        {
            get
            {
                return new ApplicationToolbarButton("links_scheda.aspx?doc=" + CurrentDocument.ID + "&amp;folder=" + CurrentFolder.ID + "&amp;nlreserve=" + CurrentDocument.ID, NetCms.GUI.Icons.Icons.Email_Go, "Prenota per Newsletter",this.CurrentFolder.StructureNetwork, this.CurrentFolder.RelativeApplication);
            }
        }

        protected override ToolbarButton ModifyButton
        {
            get
            {
                if (this.CurrentFolder.ReviewEnabled)
                    return new ApplicationToolbarButton("links_overview.aspx?doc=" + this.CurrentDocument.ID, NetCms.GUI.Icons.Icons.Pencil, "Revisioni",this.CurrentFolder.StructureNetwork, this.CurrentFolder.RelativeApplication);
                else
                    return new ApplicationToolbarButton("links_mod.aspx?doc=" + CurrentDocument.ID + "&amp;folder=" + CurrentFolder.ID, NetCms.GUI.Icons.Icons.Pencil, "Modifica Link",this.CurrentFolder.StructureNetwork, this.CurrentFolder.RelativeApplication);
            }
        }



        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Dettaglio Link: '" + this.CurrentDocument.Nome + "'"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Newspaper_Add; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }


        private int SelectedLang
        {
            get
            {   
                 return PageData.DefaultLang;
                //return _SelectedLang;
            }
        }

        public Details()
        {
            ApplicationContext.SetCurrentComponent(LinksApplication.ApplicationContextBackLabel);
            if (this.CurrentDocument.Criteria[CriteriaTypes.Show].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina dei dettagli di un link", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina di dettaglio del link " + this.CurrentDocument.Nome + " del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
        }

        protected override WebControl BuildControls()
        {
            string stato = (this.CurrentDocument.Stato.ToString() == "0" || this.CurrentDocument.Stato.ToString() == "1") ? "Attivo" : "Non Attivo";


            WebControl container = new WebControl(HtmlTextWriterTag.Div);
            container.CssClass = "container";

            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "card mt-3";

            HtmlGenericControl scheda = new HtmlGenericControl("div");
            scheda.Attributes["class"] = "scheda card-body";
           
            div.Controls.Add(scheda);

            scheda.InnerHtml += "<p class=\"card-text\">"
                             + "<span>Data Creazione: </span>" + this.CurrentDocument.Date + "<br />"
                             + "<span>Data Ultima Modifica: </span>" + this.CurrentDocument.LastModify + "<br />"
                             + " <span>Stato: </span>" + stato + "</p>";
            
            if (CurrentDocument.Content > 0)
            {                
                scheda.InnerHtml += "<p class=\"card-text\">" 
                                 + "<strong>Indirizzo: </strong>" 
                                 + "<a href =\"" + (CurrentDocument.ContentRecord as LinksDocContent).Href.Replace(NetCms.Structure.Utility.ImportContentFilter.WebRoot, CurrentFolder.PageData.Paths.AbsoluteWebRoot) + "\">"+
                  (CurrentDocument.ContentRecord as LinksDocContent).Href.Replace(NetCms.Structure.Utility.ImportContentFilter.WebRoot, CurrentFolder.PageData.Paths.AbsoluteWebRoot) +
                 "</a>" +
                "<br /><strong>Clicks: </strong>" + (CurrentDocument.ContentRecord as LinksDocContent).Clicks +
                "</p>";
            }
            else
                scheda.InnerHtml += "<p>Nessuna Revisione Attiva</p>";

            container.Controls.Add(div);

            return container;


        }
    }
}
