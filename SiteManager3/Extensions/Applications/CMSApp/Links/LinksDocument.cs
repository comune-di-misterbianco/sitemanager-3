﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using NetCms.Structure.WebFS;
using System.Configuration;
using System.Web.Security;
using System.IO;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;

namespace NetCms.Structure.Applications.Links
{
    public class LinksDocument : NetCms.Structure.WebFS.Document
    {
        public override string FrontendLink
        {
            get { return (this.Folder as StructureFolder).FrontendHyperlink + "?extlink=" + this.ID; }
        }

        public override string Icon
        {
            get { return "icon_link"; }
        }


        private IList<LinksDocContent> _ApplicationRecords;
        public virtual IList<LinksDocContent> ApplicationRecords
        {
            get
            {
                if (_ApplicationRecords == null)
                {
                    _ApplicationRecords = LinksBusinessLogic.FindAllApplicationRecords();
                }
                return _ApplicationRecords;
            }
        }

        //private DataTable _ApplicationRecords;
        //public DataTable ApplicationRecords
        //{
        //    get
        //    {
        //        if (_ApplicationRecords == null)
        //        {
        //            _ApplicationRecords = this.Network.Connection.SqlQuery("SELECT * FROM Docs_Link");
        //        }
        //        return _ApplicationRecords;
        //    }
        //}

        //public override DataRow ContentRecord
        //{
        //    get
        //    {
        //        if (_ContentRecord == null)
        //        {
        //            DataRow[] records = ApplicationRecords.Select("id_Link = " + this.Content);
        //            if (records.Length > 0)
        //                _ContentRecord = records[0];
        //        }
        //        return _ContentRecord;
        //    }
        //}

        public override DocContent ContentRecord
        {
            get
            {
                if (_ContentRecord == null)
                {
                    _ContentRecord = LinksBusinessLogic.GetById(this.Content);
                }
                return _ContentRecord;
            }
        }

        #region IsMultilanguage

        private bool MultilanguageLoaded;
        private bool _Multilanguage;
        private bool Multilanguage
        {
            get
            {   
                return false;
            }
        }

        #endregion

        public override string Link
        {
            get { return "/applications/Link/detail.aspx?id=" + ID; }
        }

        public LinksDocument()
            : base()
        { }

        public LinksDocument(Networks.NetworkKey network)
                    : base(network)
                {
                }

        public override HtmlGenericControl getRowView()
        {
            return getRowView(false);
        }

        public override HtmlGenericControl getRowView(bool SearchView)
        {
            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina di elenco Link del portale "+NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");

            HtmlGenericControl tr = new HtmlGenericControl("tr");
            HtmlGenericControl td = new HtmlGenericControl("td");

            #region Icona
            /************************/
            td.InnerHtml = "&nbsp;";
            td.Attributes["class"] = "FS_Icon " + Icon;
            tr.Controls.Add(td);
            /************************/
            #endregion

            #region Nome & Link Alla Scheda
            /************************/
            string Link;

            if (!this.Grant)
                Link = this.Nome;
            else
                Link = BuildLink();

            td = new HtmlGenericControl("td");
            td.InnerHtml = Link;
            tr.Controls.Add(td);
            /************************/
            #endregion

            #region Cartella di apparteneza
            //*************************************
            if (SearchView)
            {
                td = new HtmlGenericControl("td");
                td.InnerHtml = (this.Folder as StructureFolder).LabelLinkPath;
                tr.Controls.Add(td);
            }
            //*************************************
            #endregion

            #region Data Oggetto
            //*************************************
            td = new HtmlGenericControl("td");
            td.InnerHtml = this.Date.ToString();
            if (td.InnerHtml.Length > 10) td.InnerHtml = td.InnerHtml.Remove(10);
            tr.Controls.Add(td);
            //*************************************
            #endregion

            #region Tipo Dell'oggetto
            //*************************************
            td = new HtmlGenericControl("td");
            td.InnerHtml = "Link";
            tr.Controls.Add(td);
            //*************************************
            #endregion

            if (!SearchView)
            {
                #region Stato
                //*************************************
                td = new HtmlGenericControl("td");
                td.InnerHtml += this.StatusLabel;
                if ((this.Folder as StructureFolder).ReviewEnabled)
                    td.InnerHtml += " - " + (this.Content > 0 ? "Pubblicato" : "Non Pubblicato");
                tr.Controls.Add(td);
                //*************************************
                #endregion

                NetUtility.HtmlGenericControlCollection options = this.OptionsControls("link");
                if (options != null)
                    foreach (HtmlGenericControl option in options)
                        tr.Controls.Add(option);            
            }
            //else
            //{
            //    #region Data
            //    //*************************************
            //    //td = new HtmlGenericControl("td");
            //    //td.InnerHtml = this.Date;
            //    //tr.Controls.Add(td);
            //    //*************************************
            //    #endregion
            //}

            return tr;
        }

        private string BuildLink()
        {
            //

//            string details = "<strong>" + this.Nome + "</strong><br />";
//            details += "<p><em>" + this.Descrizione.Replace(@"
//", " ") + "</em></p>";//ATTENZIONE L'accapo soprastante non è un errore di battitura ma una necessità in quanto rappresenta il carattere accapo


//            string data = this.Date.ToString(); ;
//            if (data.Length > 10) data = data.Remove(10);
//            details += "Data Creazione: " + data + "<br />";

//            string ultimamodifica = this.LastModify.ToString();
//            if (ultimamodifica.Length > 10) ultimamodifica = ultimamodifica.Remove(10);
//            details += "Data Ultima Modifica: " + ultimamodifica + "<br /><br />";

//            if (ContentRecord != null)
//            {
//                string url = this.LastModify.ToString();
//                details += "URL: " + (ContentRecord as LinksDocContent).Href.Replace("<%= WebRoot %>", "") + "<br /><br />";
//                //details += "URL: " + ContentRecord["Href_Link"] + "<br /><br />";
//            }

//            details = details.Replace("'", "\\\'");
//            details = details.Replace("\"", "&quot;");

//            string image = "";
//            if (NetCms.Users.AccountManager.CurrentAccount.GUI.PreviewType == NetCms.Users.SitemanagerUserGUI.PreviewTypes.Static)
//                image = NetCms.GUI.GuiUtility.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Link);

            //string Link = "<a onmouseover=\"Over('" + image + "','" + details + "')\" onmouseout=\"Out()\"";
            string Link = "<a href=\"" + SiteManagerLink + "\">" + this.Nome + "</a>";

            return Link;
        }

    }
}
