﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.Applications.Files
{
    public class FilesLabels : LabelsManager.Labels
    {
        public const String ApplicationKey = "files";

        public FilesLabels() : base(ApplicationKey)
        {
        }

        public enum FilesLabelsList
        {
            AllDocs,
            AllDocsTitle,
        }
    }
}
