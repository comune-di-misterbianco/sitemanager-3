﻿using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using NetCms.Grants;

namespace NetCms.Structure.Applications.Files
{
    public partial class FilesFolder : StructureFolder
    {
        public override NetCms.Structure.Applications.Application RelativeApplication
        {
            get
            {
                if (_RelativeApplication == null)
                    _RelativeApplication = Applications.ApplicationsPool.ActiveAssemblies[FilesApplication.ApplicationID.ToString()];
                return _RelativeApplication;
            }
        }

        private NetCms.Structure.Applications.Application _RelativeApplication;

        public override bool ReviewEnabled
        {
            get
            {
                return this.RelativeApplication.EnableReviews;
            }
        }

        public override bool AllowMove
        {
            get
            {
                return RelativeApplication.EnableMove;
            }
        }

        public override ToolbarButtonsCollection ToolbarButtons
        {
            get
            {
                ToolbarButtonsCollection _ToolbarButtons = base.ToolbarButtons;

                if (this.Criteria[CriteriaTypes.Create].Allowed)
                {
                    if (NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies.Contains("9"))
                        _ToolbarButtons.Add(new NetCms.GUI.ToolbarButton(this.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/applications/links/links_new.aspx?folder=" + ID, NetCms.GUI.Icons.Icons.Link_Add, "Nuovo Collegamento Ipertestuale"));
                    _ToolbarButtons.Add(new NetCms.Structure.Applications.ApplicationToolbarButton("files_new.aspx?folder=" + ID, NetCms.GUI.Icons.Icons.Page_White_Get, "Upload di un nuovo File", this.StructureNetwork, this.RelativeApplication));
                    _ToolbarButtons.Add(new NetCms.Structure.Applications.ApplicationToolbarButton("multi_upload.aspx?folder=" + ID, NetCms.GUI.Icons.Icons.Page_White_Get, "Trasferisci documenti", this.StructureNetwork, this.RelativeApplication));
                }
                return _ToolbarButtons;
            }
        }

        public FilesFolder()
            : base()
        {
            //if (this.Criteria[NetCms.Structure.Grants.CriteriaTypes.Create])
            //    base.ToolbarButtons.Add(new NetCms.Structure.Applications.ApplicationToolbarButton("files_new.aspx?folder=" + ID, NetCms.GUI.Icons.Icons.Page_White_Get, "Upload di un nuovo File", this.Network, this.RelativeApplication));
        }

        public FilesFolder(Networks.NetworkKey network)
            : base(network) 
        {
            //if (this.Criteria[NetCms.Structure.Grants.CriteriaTypes.Create])
            //    base.ToolbarButtons.Add(new NetCms.Structure.Applications.ApplicationToolbarButton("files_new.aspx?folder=" + ID, NetCms.GUI.Icons.Icons.Page_White_Get, "Upload di un nuovo File", this.Network, this.RelativeApplication));
        }

        public override void addSearchResult(StructureTable table, StructureFolder.StandardSearchFields pars)
        {
            base.addSearchResult(table, pars);

        }

        //protected override int CreateDocContent(DataRow docRecord, DataSet sourceDB, string sourceFileName, params NetCms.Importer.Importer[] imp)
        //{
        //    string sql = "";
        //    sql = "INSERT INTO Docs_Files ";
        //    sql += "(Link_DocFiles)";
        //    //Controllo aggiunto per prevedere il caso in cui non sia presente la tabella docs_files come accadeva nel vecchio cms, in questo caso prendo in considerazione il Name_Doc
        //    if (docRecord.Table.Columns.Contains("Link_DocFiles"))
        //        sql += " Values ('" + docRecord["Link_DocFiles"].ToString() + "')";
        //    else
        //        sql += " Values ('" + docRecord["Name_Doc"].ToString().Replace("'","''") + "')";
        //    int docFileID = this.Network.Connection.ExecuteInsert(sql);
        //    if (docFileID == -1)
        //        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile inserire i dati del File durante l'importazione", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "24");

        //    return docFileID;
        //}

        public override bool DeleteModuloApplicativo(NHibernate.ISession session)
        {
            return true;
        }

        public override bool DeleteSomething(NHibernate.ISession session)
        {
            return true;
        }
    }
}
