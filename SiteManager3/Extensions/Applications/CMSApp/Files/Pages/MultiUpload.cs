﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using NHibernate;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS.Reviews;
using System.Web;
using NetCms.GUI;
using NetUpload;
using NetUpload.Model;

namespace NetCms.Structure.Applications.Files.Pages
{
    public class MultiUpload : NetCms.GUI.NetworkPage
    {

        public MultiUpload() 
        {
            FilesUpload.SaveClickHandler += save_Click;
        }

        private List<FileFilter> _Filters;
        protected List<FileFilter> Filters{
            get{
                if (_Filters == null)
                {
                    _Filters = new List<FileFilter>();
                    _Filters.Add(new FileFilter("Image files", "jpg,jpeg,gif,png,svg"));
                    _Filters.Add(new FileFilter("Document files", "doc,docx,odt,ods,odp,odf,odp,odb,csv,xml,txt,ppt,pptx,rtf,xls,xlsx,xlsm,pdf,pdf/a,p7m"));
                    _Filters.Add(new FileFilter("Media", "mp3,mp4,wmv,asf"));
                    _Filters.Add(new FileFilter("Zip files", "zip,rar"));
                }
                return _Filters;
            }

        }

        private NetUpload.MultiUpload _FilesUpload;
        protected NetUpload.MultiUpload FilesUpload 
        {
            get {
                if (_FilesUpload == null)
                {
                    _FilesUpload = new NetUpload.MultiUpload(
                    "multiupload",
                    "/scripts/shared/plupload/js",
                    this.CurrentFolder.ID,
                    Filters,
                    NetCms.Configurations.PortalData.UploadFileSizeLimit/1000,
                    "/repository/plupload/tmp/" + this.Account.ID.ToString() + "/",
                   NetUpload.MultiUpload.CtrlRuntimes.All
                );
                }
                return _FilesUpload;
            }
            set {
                _FilesUpload = value;
            }
        }

        protected override System.Web.UI.WebControls.WebControl BuildControls()
        {

            #region old
            //string headCssImport = @"<link rel=""stylesheet"" type=""text/css"" href=""" + NetCms.Configurations.Paths.AbsoluteRoot + @"/scripts/shared/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css"" /><!-- jquery.plupload.css -->";
            //headCssImport += @"<link rel=""stylesheet"" type=""text/css"" href=""" + NetCms.Configurations.Paths.AbsoluteRoot + @"/scripts/shared/plupload/js/custom/fileupload.css"" /><!-- fileupload.css -->";

            //base.Page.Header.Controls.Add(new LiteralControl(headCssImport));

            //string headJsImport = @"<script type=""text/javascript"" src=""" + NetCms.Configurations.Paths.AbsoluteRoot + @"/scripts/shared/plupload/js/plupload.full.min.js""></script>";
            //headJsImport += @"<script type=""text/javascript"" src=""" + NetCms.Configurations.Paths.AbsoluteRoot + @"/scripts/shared/plupload/js/jquery.plupload.queue/jquery.plupload.queue.js""></script>";
            //// localizzazione
            //headJsImport += @"<script type=""text/javascript"" src=""" + NetCms.Configurations.Paths.AbsoluteRoot + @"/scripts/shared/plupload/js/i18n/it.js""></script>";

            //headJsImport += @"<script type=""text/javascript"" src=""" + NetCms.Configurations.Paths.AbsoluteRoot + @"/scripts/shared/plupload/js/custom/fileupload.js""></script>";

            //base.Page.Header.Controls.Add(new LiteralControl(headJsImport));

            //// Imposto i settagi di upload da passare all'handler
            //PlUpload.Model.UploadSetting uploadSetting = new PlUpload.Model.UploadSetting("/repository/plupload/tmp/" + this.Account.ID + "/" , 50000000, ".doc,.docx,.xls,.xlsx,.pdf,.jpg,.jpeg,.gif,.png,.zip,.rar,.p7f");
            //HttpContext.Current.Session["UploadSettings"] = uploadSetting; 
            #endregion


            WebControl div = new WebControl(HtmlTextWriterTag.Div);

            #region old_code
            //// box per esposizione msg errore
            //WebControl console = new WebControl(HtmlTextWriterTag.Div);
            //console.ID = "console";
            //console.CssClass = "consoleArea hide";

            //WebControl console_ul = new WebControl(HtmlTextWriterTag.Ul);
            //console.Controls.Add(console_ul);

            //div.Controls.Add(console);

            //WebControl uploadedfiles = new WebControl(HtmlTextWriterTag.Div);
            //uploadedfiles.ID = "files-selected-container";
            //uploadedfiles.CssClass = "files-selected-container hide";            
            //uploadedfiles.Controls.Add(new LiteralControl("<ul class=\"files-selected-list\"></ul>"));
            //div.Controls.Add(uploadedfiles);

            //WebControl DropArea = new WebControl(HtmlTextWriterTag.Div);
            //DropArea.ID = "DropArea";
            //DropArea.CssClass = "dropArea";

            //WebControl plupload = new WebControl(HtmlTextWriterTag.Div);
            //plupload.ID = "plupload";
            //plupload.Controls.Add(new LiteralControl("<p>You browser doesn't have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.</p>"));
            //DropArea.Controls.Add(plupload);

            ////WebControl uploadedfiles = new WebControl(HtmlTextWriterTag.Div);
            ////uploadedfiles.ID = "filesuploaded";
            ////uploadedfiles.Controls.Add(new LiteralControl("<ul class=\"files-uploaded-list\"></ul>"));                                   
            ////DropArea.Controls.Add(uploadedfiles);

            //WebControl uploadUI = new WebControl(HtmlTextWriterTag.Div);
            //uploadUI.CssClass = "uploadui";
            //uploadUI.Controls.Add(new LiteralControl("<h3 class=\"upload-instruction-msg\">Trascina qui tutti i file da trasferire</h3>"));
            //uploadUI.Controls.Add(new LiteralControl("<p class=\"upload-instruction-msg2\">oppure</p>"));

            //#region Buttons
            //WebControl buttons = new WebControl(HtmlTextWriterTag.Div);
            //buttons.CssClass = "btns";
            //buttons.ID = "container";

            //Button picker = new Button();
            //picker.Text = "Seleziona i file";
            //picker.Attributes["type"] = "button";
            //picker.ID = "pickfiles";
            //buttons.Controls.Add(picker);

            ////Button submit = new Button();
            ////submit.Text = "Trasferisci";
            ////submit.Attributes["type"] = "button";
            ////submit.ID = "uploadfiles";
            ////submit.Attributes["disabled"] = "disabled";
            ////buttons.Controls.Add(submit);           

            //uploadUI.Controls.Add(buttons);


            //HiddenField fileUploaded = new HiddenField();
            //fileUploaded.ID = "fileUploaded";
            //DropArea.Controls.Add(fileUploaded);

            //WebControl buttons_confirm = new WebControl(HtmlTextWriterTag.Div);
            //buttons_confirm.CssClass = "btns_confirm";

            //Button save = new Button();
            //save.Text = "Salva e completa";
            //save.ID = "save";
            //save.CssClass = "btnHide";
            //save.Click += save_Click;
            //buttons_confirm.Controls.Add(save);

            //uploadUI.Controls.Add(buttons_confirm);
            //#endregion

            //DropArea.Controls.Add(uploadUI);

            //div.Controls.Add(DropArea);

            //HiddenField currentFolder = new HiddenField();
            //currentFolder.ID = "currentFolder";
            //currentFolder.Value = this.CurrentFolder.ID.ToString();
            //div.Controls.Add(currentFolder);


            /****************************************/

            //List<FileFilter> filters = new List<FileFilter>();
            //filters.Add(new FileFilter("Image files","jpg,jpeg,gif,png"));
            //filters.Add(new FileFilter("Document files","doc,docx,xls,xlsx,pdf,p7f"));
            //filters.Add(new FileFilter("Zip files","zip,rar"));

            //NetUpload.MultiUpload multiUpload = new NetUpload.MultiUpload(
            //    "multiupload", 
            //    "/scripts/shared/plupload/js",                  
            //    this.CurrentFolder.ID, 
            //    filters, 
            //    10,
            //    "/repository/plupload/tmp/" + this.Account.ID.ToString() + "/", 
            //   NetUpload.MultiUpload.CtrlRuntimes.All
            //);
            //multiUpload.Save.Click += save_Click; 
            #endregion

            div.Controls.Add(FilesUpload);
            
            return div;
        }

        void save_Click(object sender, EventArgs e)
        {
            NetUtility.RequestVariable fileTrasfered = new NetUtility.RequestVariable("fileUploaded_" + FilesUpload.ClientID, NetUtility.RequestVariable.RequestType.Form);
            if (fileTrasfered.IsValidString)
            {
                string[] files = fileTrasfered.StringValue.Split(',');

                bool errore = false;

                foreach(string file in files)
                {
                    string pathOrigine = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/plupload/tmp/" + this.Account.ID + "/";
                    string pathDestinazione = this.CurrentFolder.StructureNetwork.Paths.AbsoluteFrontRoot + this.CurrentFolder.Path + "/";

                    string filenameOriginale = file;
                    string filenameParsed = NetCms.Connections.Connection.FilterString(filenameOriginale);                   

                    using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                    using (ITransaction tx = sess.BeginTransaction())
                    {
                        NetCms.Structure.WebFS.Document document = null;
                        
                        try
                        {
                            // Spostamento file nella cartella di destinazione
                            pathOrigine = System.Web.HttpContext.Current.Server.MapPath(pathOrigine + filenameOriginale);
                            pathDestinazione = System.Web.HttpContext.Current.Server.MapPath(pathDestinazione + filenameParsed);
                            System.IO.File.Copy(pathOrigine, pathDestinazione);
                            System.IO.File.Delete(pathOrigine);

                            // Salvataggio su db
                            document = new FilesDocument(this.CurrentFolder.NetworkKey);
                            document.Application = 4;
                            document.PhysicalName = filenameParsed;
                            document.Create = DateTime.Now;
                            document.Modify = DateTime.Now;
                            document.Data = DateTime.Today;
                            document.Stato = 1;
                            document.Folder = this.CurrentFolder;

                            this.CurrentFolder.Documents.Add(document);
                            document.Autore = this.Account.ID;

                            FilesDocContent docFile = new FilesDocContent();
                            docFile.Link = filenameParsed;
                          
                            docFile.Size = SharedUtilities.IO.FileUtility.GetSizeInKB(pathDestinazione);
                            docFile.ContentType = MimeMapping.MimeUtility.GetMimeMapping(pathDestinazione);
                            docFile.Extension = System.IO.Path.GetExtension(pathDestinazione);

                            DocumentLang docLang = new DocumentLang();
                            docLang.Document = document;
                            document.DocLangs.Add(docLang);
                            docLang.Content = docFile;
                            docFile.DocLang = docLang;
                            docLang.Label = filenameParsed;
                            docLang.Lang = this.CurrentFolder.StructureNetwork.DefaultLang;
                            docLang.Descrizione = filenameParsed;

                            document = DocumentBusinessLogic.SaveOrUpdateDocument(document, sess);
                            FilesBusinessLogic.SaveOrUpdateFilesDocContent(docFile, sess);
                            DocumentBusinessLogic.SaveOrUpdateDocumentLang(docLang, sess);
                            this.CurrentFolder.Invalidate();

                            if (this.CurrentFolder.ReviewEnabled)
                            {
                                //revisione

                                Review revisione = new Review(Users.AccountManager.CurrentAccount);
                                revisione.DocLang = docLang;
                                revisione.Content = docFile;
                                revisione.Data = DateTime.Now;
                                revisione.Autore = Users.AccountManager.CurrentAccount.ID;
                                revisione.Origine = 0;
                                revisione.Published = 1;

                                docLang.Revisioni.Add(revisione);
                                docFile.Revisione = revisione;

                                ReviewBusinessLogic.SaveOrUpdateReview(revisione, sess);

                            }
                            tx.Commit();


                        }
                        catch (Exception ex)
                        {
                            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore nel salvataggio dei file.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, ex);
                            tx.Rollback();
                            errore = true;
                        }
                       
                    }
                }

                if (!errore)
                {
                    Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha creato con successo un Allegato nel portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                    NetCms.GUI.PopupBox.AddSessionMessage("File trasferiti correttamente.", PostBackMessagesType.Success, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                }
                else
                {
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore nel salvataggio dei file dopo il trasferimento.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "25");
                    NetCms.GUI.PopupBox.AddMessage("<strong>Errore nel trasferimento </strong><br />E' avvenuto un errore durante il salvataggio del file.", NetCms.GUI.PostBackMessagesType.Error);                    
                }

            }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Page_White_Get; }
        }

        public override string PageTitle
        {
            get { return "Trasferimento file"; }
        }
    }
}
