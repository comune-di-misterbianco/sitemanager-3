﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using NetCms.GUI;
using NetForms;
using System.Collections.Generic;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS.Reviews;
using NetCms.Grants;
using NHibernate;

namespace NetCms.Structure.Applications.Files.Pages
{
    public class FilesPopupAdd : NetCms.GUI.NetworkPage
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Creazione Nuovo Allegato"; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Page_White_Get; }
        }

        private string _TargetControlID;
        public string TargetControlID
        {
            get
            {
                if (_TargetControlID == null)
                {
                    //try
                    //{
                    NetUtility.RequestVariable qs = new NetUtility.RequestVariable("ctrid", HttpContext.Current.Request.QueryString);
                    if (qs.IsValid())
                        _TargetControlID = qs.StringValue;
                    /*    else throw new Exception("variabile ctrid non valida");
                    }
                    catch (Exception ex)
                    { 
                        NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                        ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);
                    }*/
                }
                return _TargetControlID;
            }
        }

        private FileUpload uploadform = new FileUpload();
        private HtmlGenericControl errors = new HtmlGenericControl("div");

        public FilesPopupAdd()
        {            
            G2Core.Common.RequestVariable click = new G2Core.Common.RequestVariable("Trasferisci", G2Core.Common.RequestVariable.RequestType.Form_QueryString);
            
            if (this.CurrentFolder.Criteria[CriteriaTypes.Create].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina di aggiunta di una nuovo File", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else if (!IsPostBack && !click.IsPostBack)
                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha effettuato l'accesso alla pagina di creazione di un nuovo Allegato nel portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
        }

        public bool ClosePopup
        {
            get { return _ClosePopup; }
            private set { _ClosePopup = value; }
        }
        private bool _ClosePopup;

        public WebControl CloserScriptContainer
        {
            get
            {
                if (_CloserScriptContainer == null)
                    _CloserScriptContainer = new WebControl(HtmlTextWriterTag.Span);
                return _CloserScriptContainer;
            }
        }
        private WebControl _CloserScriptContainer;

        private void Trasferisci_Click(object sender, EventArgs e)
        {
            NetUtility.RequestVariable Nome = new NetUtility.RequestVariable("Nome");
            NetUtility.RequestVariable Descrizione = new NetUtility.RequestVariable("Descrizione");
            NetUtility.RequestVariable target = new NetUtility.RequestVariable("ctrid", NetUtility.RequestVariable.RequestType.QueryString);
            if (uploadform.HasFile && Descrizione.IsValidString)
            {
                int fileSize = uploadform.PostedFile.ContentLength/1000;
                //Uncomment this line to Save the uploaded file
                if (fileSize <= NetCms.Configurations.PortalData.UploadFileSizeLimit)
                {
                    string SavePath = this.CurrentFolder.StructureNetwork.Paths.AbsoluteFrontRoot + this.CurrentFolder.Path + "/";
                    SavePath = HttpContext.Current.Server.MapPath(SavePath);

                    string FileName = uploadform.FileName.Replace(" ", "_").ToLower();
                    string FileExt = FileName.Substring(FileName.LastIndexOf('.') + 1, (FileName.Length) - (FileName.LastIndexOf('.') + 1));

                    string name = "";
                    //string[] ext = FileName.ToString().Split('.');
                    //for (int i = 0; i < ext.Length - 1; i++)
                    //    name += ext[i] + ".";

                    name = FileName.Replace(FileExt, "");
                    name = NetForms.NetTextBoxPlus.ClearFileSystemName(name);
                    FileName = name + "." + FileExt;

                    FileInfo finfo = new FileInfo(SavePath + FileName);

                    if (!finfo.Exists)
                    {
                        string nome = Nome.IsValidString ? Nome.StringValue : FileName.Replace("'", "''");

                        IList<FilesDocContent> checkExistingFiles = FilesBusinessLogic.GetFilesInFolderByLabelOrNameOrLink(this.CurrentFolder.ID, FileName.Replace("'", "''"), nome);

                        ////DataTable rows = this.Connection.SqlQuery("SELECT * FROM Docs INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang INNER JOIN revisioni ON DocLang_Revisione=id_DocLang INNER JOIN docs_files ON id_DocFiles=Contenuto_Revisione WHERE Link_DocFiles = '" + FileName.Replace("'", "''") + "'");

                        //if (rows.Rows.Count == 0)
                        if (checkExistingFiles.Count == 0)
                        {

                            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                            using (ITransaction tx = sess.BeginTransaction())
                            {
                                NetCms.Structure.WebFS.Document document = null;
                                bool errore = false;
                                try
                                {
                                    uploadform.SaveAs(SavePath + FileName);

                                    document = new FilesDocument(this.CurrentFolder.NetworkKey);
                                    document.Application = 4;
                                    document.PhysicalName = FileName.Replace("'", "''");
                                    document.Create = DateTime.Now;
                                    document.Modify = DateTime.Now;
                                    document.Data = DateTime.Today;
                                    document.Stato = 1;
                                    document.Folder = this.CurrentFolder;
                                    this.CurrentFolder.Documents.Add(document);
                                    document.Autore = this.Account.ID;

                                    FilesDocContent docFile = new FilesDocContent();
                                    docFile.Link = FileName.Replace("'", "''");

                                    DocumentLang docLang = new DocumentLang();
                                    docLang.Document = document;
                                    document.DocLangs.Add(docLang);
                                    docLang.Content = docFile;
                                    docFile.DocLang = docLang;
                                    docLang.Label = nome;
                                    docLang.Lang = this.CurrentFolder.StructureNetwork.DefaultLang;
                                    docLang.Descrizione = Descrizione.StringValue;

                                    document = DocumentBusinessLogic.SaveOrUpdateDocument(document,sess);
                                    FilesBusinessLogic.SaveOrUpdateFilesDocContent(docFile,sess);
                                    DocumentBusinessLogic.SaveOrUpdateDocumentLang(docLang,sess);


                                    //SISTEMARE CON CACHE 2° LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                                    this.CurrentFolder.Invalidate();

                                    if (this.CurrentFolder.ReviewEnabled)
                                    {
                                        //revisione

                                        Review revisione = new Review(Users.AccountManager.CurrentAccount);
                                        revisione.DocLang = docLang;
                                        revisione.Content = docFile;
                                        revisione.Data = DateTime.Now;
                                        revisione.Autore = Users.AccountManager.CurrentAccount.ID;
                                        revisione.Origine = 0;
                                        revisione.Published = 1;

                                        docLang.Revisioni.Add(revisione);
                                        docFile.Revisione = revisione;

                                        ReviewBusinessLogic.SaveOrUpdateReview(revisione, sess);

                                    }
                                    tx.Commit();


                                }
                                catch (Exception ex)
                                {
                                    tx.Rollback();
                                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante il salvataggio dei record su db relativi al file trasferito: ", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "25",ex);
                                    errore = true;
                                }


                                #region vecchio codice

                                ////DataTable rows = this.Connection.SqlQuery("SELECT Name_Doc FROM Docs INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang WHERE Folder_Doc = " + this.CurrentFolder.ID + " AND (Label_DocLang LIKE '" + nome + "' OR Name_Doc LIKE '" + FileName.Replace("'", "''") + "') ");
                                //DataTable rows = this.Connection.SqlQuery("SELECT * FROM Docs INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang INNER JOIN revisioni ON DocLang_Revisione=id_DocLang INNER JOIN docs_files ON id_DocFiles=Contenuto_Revisione WHERE Link_DocFiles = '" + FileName.Replace("'", "''") + "'");
                                //if (rows.Rows.Count == 0)
                                //{
                                //    uploadform.SaveAs(SavePath + FileName);
                                //    string sql = "";
                                //    sql += "INSERT INTO Docs ";
                                //    sql += "(Application_Doc,Name_Doc,Data_Doc,Stato_Doc,Folder_Doc,Autore_Doc)";
                                //    sql += " Values (4,'" + FileName.Replace("'", "''") + "'," + this.Connection.FilterDate(DateTime.Today.ToString()) + ",1," + this.CurrentFolder.ID + "," + this.Account.ID + ")";
                                //    int docid = this.Connection.ExecuteInsert(sql);

                                //    sql = "INSERT INTO Docs_Files ";
                                //    sql += "(Link_DocFiles)";
                                //    sql += " Values ('" + FileName.Replace("'", "''") + "')";
                                //    int docFileID = this.Connection.ExecuteInsert(sql);

                                //    sql = "INSERT INTO Docs_Lang ";
                                //    sql += "(Doc_DocLang,Content_DocLang,Label_DocLang,Lang_DocLang,Desc_DocLang)";
                                //    sql += " Values (" + docid + "," + docFileID + ",'" + nome + "'," + this.CurrentFolder.Network.DefaultLang;
                                //    sql += ",'" + Descrizione.StringValue + "')";
                                //    int docLangID = this.CurrentFolder.Network.Connection.ExecuteInsert(sql);

                                //SISTEMARE CON CACHE 2° LIVELLO
                                //    this.CurrentFolder.Invalidate();

                                //    if (this.CurrentFolder.ReviewEnabled)
                                //    {
                                //        //revisione
                                //        DateTime ora = DateTime.Now;
                                //        string date = ora.Year + "-" + ora.Month + "-" + ora.Day + " " + ora.Hour + "." + ora.Minute + "." + ora.Second;

                                //        string sqlrev = "INSERT INTO Revisioni ";
                                //        sqlrev += " (DocLang_Revisione,Contenuto_Revisione,Data_Revisione,Autore_Revisione,Origine_Revisione,Published_Revisione) VALUES ";
                                //        sqlrev += " (" + docLangID + "," + docFileID + ",'" + date + "'," + Users.AccountManager.CurrentAccount.ID + ",0,1) ";
                                //        this.CurrentFolder.Network.Connection.Execute(sqlrev);
                                //    }
                                #endregion
                                if (!errore)
                                {
                                    this.CloserScriptContainer.Controls.Add(new LiteralControl(@"
                                    <script type=""text/javascript"">
    
                                        function closeAndUpdateParent()
                                        {
                                            window.parent.FileChoser_AddOption('" + target + @"','" + FileName + @"','" + document.ID + @"');
                                            window.parent.CloseTB();
                                        }
                                        closeAndUpdateParent();
                                    </script>
                                    "));
                                    Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha creato con successo un nuovo Allegato nel portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                                    /*if (TargetControlID != null && TargetControlID.Length > 0 && HttpContext.Current.Request.Form["Lite"] != null && HttpContext.Current.Request.Form["Lite"] == "true")
                                        NetCms.PageData.Redirect("end.aspx?docname=" + FileName + "&ctrid=" + TargetControlID + "&docid=" + docid);
                                    else
                                        NetCms.GUI.PopupBox.AddSessionMessage("File Salvato", PostBackMessagesType.Success, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                                    */
                                    //NetCms.GUI.PopupBox.AddMessage("File Salvato", NetCms.GUI.PostBackMessagesType.Success);
                                }
                                else
                                {
                                    NetCms.GUI.PopupBox.AddMessage("<strong>Errore nel trasferimento </strong><br />E' avvenuto un errore durante il salvataggio del file.", NetCms.GUI.PostBackMessagesType.Error);
                                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante il salvataggio dei record su db relativi al file trasferito: ", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "25");
                                }
                            }
                        }
                        else
                        {
                            NetCms.GUI.PopupBox.AddMessage("<strong>Errore nel trasferimento </strong><br />Esiste già un file con questo nome.", NetCms.GUI.PostBackMessagesType.Error);
                            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore nel trasferimento durante l'inserimento: esiste già un file con quel nome", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "25");
                        }
                    }
                    else
                    {
                        NetCms.GUI.PopupBox.AddMessage("<strong>Errore nel trasferimento </strong><br />Esiste già un file con questo nome.", NetCms.GUI.PostBackMessagesType.Error);
                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore nel trasferimento durante l'inserimento: esiste già un file con quel nome", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "25");
                    }
                }
                else 
                {
                    NetCms.GUI.PopupBox.AddMessage("<strong>Errore nel trasferimento </strong><br />Superata la dimensione massima di " + (NetCms.Configurations.PortalData.UploadFileSizeLimit / 1000).ToString() + " MB accettata dal sistema per il file che si desidera trasferire", NetCms.GUI.PostBackMessagesType.Error);
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "Superata la dimensione massima di " + (NetCms.Configurations.PortalData.UploadFileSizeLimit / 1000).ToString() + " MB accettata dal sistema per il file che si desidera trasferire", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "152");
                }
            }
            else
            {

                NetCms.GUI.PopupBox.AddMessage("<strong>Errore nel trasferimento </strong><br />File non valido o descrizione non inserita", NetCms.GUI.PostBackMessagesType.Error);
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore nel trasferimento durante l'inserimento: file non valido o descrizione non inserita", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "26");
            }

        }

        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "fileUpload";

            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            div.Controls.Add(fieldset);
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            fieldset.Controls.Add(legend);
            legend.InnerHtml = "<strong>Trasferimento File</strong>";

            fieldset.Controls.Add(errors);

            HtmlGenericControl p = new HtmlGenericControl("p");

            fieldset.Controls.Add(p);

            Label label = new Label();
            p.Controls.Add(label);
            label.Style.Add("display", "block");
            label.Text = "File* (Max "+(NetCms.Configurations.PortalData.UploadFileSizeLimit/1000).ToString()+"MB)";
            label.AssociatedControlID = "FileUpload";

            uploadform.ID = "FileUpload";            
            p.Controls.Add(uploadform);

            label = new Label();
            p.Controls.Add(label);
            label.Style.Add("display", "block");
            label.Text = "Nome (Puoi lasciare vuoto questo campo, in questo caso verrà utilizzato il nome del file)";
            label.AssociatedControlID = "Nome";

            TextBox nome = new TextBox();
            p.Controls.Add(nome);
            nome.ID = "Nome";
            nome.Columns = 50;

            label = new Label();
            p.Controls.Add(label);
            label.Style.Add("display", "block");
            label.Text = "Descrizione*";
            label.AssociatedControlID = "FileUpload";

            TextBox Descrizione = new TextBox();
            p.Controls.Add(Descrizione);
            Descrizione.ID = "Descrizione";
            Descrizione.Rows = 5;
            Descrizione.TextMode = TextBoxMode.MultiLine;
            Descrizione.Columns = 50;
            label.AssociatedControlID = "Descrizione";


            p = new HtmlGenericControl("p");
            fieldset.Controls.Add(p);

            Button Trasferisci = new Button();
            Trasferisci.Text = "Trasferisci";
            Trasferisci.ID = "Trasferisci";
            Trasferisci.Click += Trasferisci_Click;
            p.Controls.Add(Trasferisci);

            fieldset.Controls.Add(CloserScriptContainer);

            return div;
        }

        protected override void OnInit(EventArgs e)
        {
            //base.OnInit(e);

            this.Page.Form.Method = "post";
            this.Page.Form.Name = "PageForm";
            this.Page.Form.ID = "PageForm";

            this.AddCssSimpleToHeader();
            this.AddScriptsToHeader();
            this.Page.Header.Title = Configurations.PortalData.TitoloCms + " - " + this.PageTitle;
            this.Controls.Add(this.ContentBody);
        }
    }
}
