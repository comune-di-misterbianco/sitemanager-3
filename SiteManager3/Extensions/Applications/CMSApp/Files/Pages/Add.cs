﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using NetCms.GUI;
using NetForms;
using System.Collections.Generic;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.Reviews;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NHibernate;
using NetCms.Grants;

namespace NetCms.Structure.Applications.Files.Pages
{
    public class FilesAdd : NetCms.GUI.NetworkPage
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Creazione Nuovo Allegato"; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Page_White_Get; }
        }

        private string _TargetControlID;
        public string TargetControlID
        {
            get
            {
                if (_TargetControlID == null)
                {
                    //try
                    //{
                    NetUtility.RequestVariable qs = new NetUtility.RequestVariable("ctrid", HttpContext.Current.Request.QueryString);
                    if (qs.IsValid())
                        _TargetControlID = qs.StringValue;
                    /*    else throw new Exception("variabile ctrid non valida");
                    }
                    catch (Exception ex)
                    { 
                        NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                        ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);
                    }*/
                }
                return _TargetControlID;
            }
        }

        private FileUpload uploadform = new FileUpload();
        private HtmlGenericControl errors = new HtmlGenericControl("div");

        public FilesAdd()
        {
            G2Core.Common.RequestVariable click = new G2Core.Common.RequestVariable("Trasferisci", G2Core.Common.RequestVariable.RequestType.Form_QueryString);
            
            if (this.CurrentFolder.Criteria[CriteriaTypes.Create].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina di aggiunta di una nuovo File", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else if (!IsPostBack && !click.IsPostBack) 
                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha effettuato l'accesso alla pagina di creazione di un nuovo Allegato nel portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
        }

        private void Trasferisci_Click(object sender, EventArgs e)
        {
            NetUtility.RequestVariable Nome = new NetUtility.RequestVariable("Nome");
            NetUtility.RequestVariable Descrizione = new NetUtility.RequestVariable("Descrizione");
            if (uploadform.HasFile && Descrizione.IsValidString)
            {
                int fileSize = uploadform.PostedFile.ContentLength/1000;
                //Uncomment this line to Save the uploaded file
                if (fileSize <= NetCms.Configurations.PortalData.UploadFileSizeLimit)
                {
                    string SavePath = this.CurrentFolder.StructureNetwork.Paths.AbsoluteFrontRoot + this.CurrentFolder.Path + "/";
                    SavePath = HttpContext.Current.Server.MapPath(SavePath);

                    string FileName = uploadform.FileName.Replace(" ", "_").ToLower();
                    string[] ext = FileName.ToString().Split('.');
                    string name = "";
                    for (int i = 0; i < ext.Length - 1; i++)
                        name += ext[i] + ".";
                    name = NetForms.NetTextBoxPlus.ClearFileSystemName(name);
                    FileName = name + "." + ext[ext.Length - 1];
                    FileInfo finfo = new FileInfo(SavePath + FileName);

                    if (!finfo.Exists)
                    {
                        string fileNameCorrect = NetCms.Connections.Connection.FilterString(FileName);
                        //string nome = Nome.IsValidString ? Nome.StringValue : fileNameCorrect;
                        string nome = Nome.IsValidString ? Nome.OriginalValue : fileNameCorrect;
                        
                        //string sqlCheck = "SELECT Name_Doc FROM Docs INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang";
                        //sqlCheck += " WHERE Folder_Doc = " + this.CurrentFolder.ID;
                        //sqlCheck += " AND (Label_DocLang LIKE '" + nome + "' OR Name_Doc LIKE '" + fileNameCorrect + "' OR Link_DocFiles LIKE '" + fileNameCorrect + "' )";
                        
                        //DataTable rows = this.Connection.SqlQuery(sqlCheck);

                        IList<FilesDocContent> checkExistingFiles = FilesBusinessLogic.GetFilesInFolderByLabelOrNameOrLink(this.CurrentFolder.ID, fileNameCorrect, nome);

                        ////DataTable rows = this.Connection.SqlQuery("SELECT * FROM Docs INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang INNER JOIN revisioni ON DocLang_Revisione=id_DocLang INNER JOIN docs_files ON id_DocFiles=Contenuto_Revisione WHERE Link_DocFiles = '" + FileName.Replace("'", "''") + "'");
                        
                        //if (rows.Rows.Count == 0)
                        if (checkExistingFiles.Count == 0)
                        {
                            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                            using (ITransaction tx = sess.BeginTransaction())
                            {
                                NetCms.Structure.WebFS.Document document = null;
                                bool errore = false;
                                try
                                {
                                    uploadform.SaveAs(SavePath + FileName);
                                    //string sql = "";
                                    //sql += "INSERT INTO Docs ";
                                    //sql += "(Application_Doc,Name_Doc,Data_Doc,Stato_Doc,Folder_Doc,Autore_Doc)";
                                    //sql += " Values (4,'" + fileNameCorrect + "'," + this.Connection.FilterDate(DateTime.Today.ToString()) + ",1," + this.CurrentFolder.ID + "," + this.Account.ID + ")";
                                    //int docid = this.Connection.ExecuteInsert(sql);

                                    document = new FilesDocument(this.CurrentFolder.NetworkKey);
                                    document.Application = 4;
                                    document.PhysicalName = fileNameCorrect;
                                    document.Create = DateTime.Now;
                                    document.Modify = DateTime.Now;
                                    document.Data = DateTime.Today;
                                    document.Stato = 1;
                                    document.Folder = this.CurrentFolder;
                                    //Da VERIFICARE CHE SALVI L'ASSOCIAZIONE
                                    this.CurrentFolder.Documents.Add(document);
                                    document.Autore = this.Account.ID;

                                    //sql = "INSERT INTO Docs_Files ";
                                    //sql += "(Link_DocFiles)";
                                    //sql += " Values ('" + fileNameCorrect + "')";
                                    //int docFileID = this.Connection.ExecuteInsert(sql);

                                    FilesDocContent docFile = new FilesDocContent();
                                    docFile.Link = fileNameCorrect;
                                    docFile.Size = SharedUtilities.IO.FileUtility.GetSizeInKB(SavePath + FileName);
                                    docFile.ContentType = MimeMapping.MimeUtility.GetMimeMapping(SavePath + FileName);
                                    docFile.Extension = System.IO.Path.GetExtension(SavePath + FileName);

                                    DocumentLang docLang = new DocumentLang();
                                    docLang.Document = document;
                                    document.DocLangs.Add(docLang);
                                    docLang.Content = docFile;
                                    docFile.DocLang = docLang;
                                    docLang.Label = nome;
                                    docLang.Lang = this.CurrentFolder.StructureNetwork.DefaultLang;
                                    docLang.Descrizione = Descrizione.StringValue;

                                    document = DocumentBusinessLogic.SaveOrUpdateDocument(document,sess);
                                    FilesBusinessLogic.SaveOrUpdateFilesDocContent(docFile,sess);
                                    DocumentBusinessLogic.SaveOrUpdateDocumentLang(docLang,sess);
                                    //sql = "INSERT INTO Docs_Lang ";
                                    //sql += "(Doc_DocLang,Content_DocLang,Label_DocLang,Lang_DocLang,Desc_DocLang)";
                                    //sql += " Values (" + docid + "," + docFileID + ",'" + nome + "'," + this.CurrentFolder.Network.DefaultLang;
                                    //sql += ",'" + Descrizione.StringValue + "')";
                                    //int docLangID = this.CurrentFolder.Network.Connection.ExecuteInsert(sql);

                                    //SISTEMARE CON CACHE 2° LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                                    this.CurrentFolder.Invalidate();

                                    if (this.CurrentFolder.ReviewEnabled)
                                    {
                                        //revisione

                                        Review revisione = new Review(Users.AccountManager.CurrentAccount);
                                        revisione.DocLang = docLang;
                                        revisione.Content = docFile;
                                        revisione.Data = DateTime.Now;
                                        revisione.Autore = Users.AccountManager.CurrentAccount.ID;
                                        revisione.Origine = 0;
                                        revisione.Published = 1;

                                        docLang.Revisioni.Add(revisione);
                                        docFile.Revisione = revisione;

                                        ReviewBusinessLogic.SaveOrUpdateReview(revisione,sess);
                                        //DateTime ora = DateTime.Now;
                                        //string date = ora.Year + "-" + ora.Month + "-" + ora.Day + " " + ora.Hour + "." + ora.Minute + "." + ora.Second;

                                        //string sqlrev = "INSERT INTO Revisioni ";
                                        //sqlrev += " (DocLang_Revisione,Contenuto_Revisione,Data_Revisione,Autore_Revisione,Origine_Revisione,Published_Revisione) VALUES ";
                                        //sqlrev += " (" + docLangID + "," + docFileID + ",'" + date + "'," + Users.AccountManager.CurrentAccount.ID + ",0,1) ";
                                        //this.CurrentFolder.Network.Connection.Execute(sqlrev);
                                    }
                                    tx.Commit();
                                    

                                }
                                catch (Exception ex)
                                {
                                    tx.Rollback();
                                    errore = true;
                                }
                                if (!errore)
                                {
                                    if (TargetControlID != null && TargetControlID.Length > 0 && HttpContext.Current.Request.Form["Lite"] != null && HttpContext.Current.Request.Form["Lite"] == "true")
                                        NetCms.PageData.Redirect("end.aspx?docname=" + FileName + "&ctrid=" + TargetControlID + "&docid=" + document.ID);
                                    else
                                    {
                                        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha creato con successo un Allegato nel portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                                        NetCms.GUI.PopupBox.AddSessionMessage("File Salvato", PostBackMessagesType.Success, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                                        //NetCms.GUI.PopupBox.AddMessage("File Salvato", NetCms.GUI.PostBackMessagesType.Success);
                                    }
                                }
                                else
                                {
                                    NetCms.GUI.PopupBox.AddMessage("<strong>Errore nel trasferimento </strong><br />E' avvenuto un errore durante il salvataggio del file.", NetCms.GUI.PostBackMessagesType.Error);
                                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore nel trasferimento durante l'inserimento: ", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "25");
                                }
                            }
                        }
                        else
                        {
                            NetCms.GUI.PopupBox.AddMessage("<strong>Errore nel trasferimento </strong><br />Esiste già un file con questo nome.", NetCms.GUI.PostBackMessagesType.Error);
                            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore nel trasferimento durante l'inserimento: esiste già un file con quel nome", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "25");
                        }
                    }
                    else
                    {
                        NetCms.GUI.PopupBox.AddMessage("<strong>Errore nel trasferimento </strong><br />Esiste già un file con questo nome.", NetCms.GUI.PostBackMessagesType.Error);
                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore nel trasferimento durante l'inserimento: esiste già un file con quel nome", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "25");
                    }
                }
                else 
                {
                    NetCms.GUI.PopupBox.AddMessage("<strong>Errore nel trasferimento </strong><br />Superata la dimensione massima di " + (NetCms.Configurations.PortalData.UploadFileSizeLimit / 1000).ToString() + " MB accettata dal sistema per il file che si desidera trasferire", NetCms.GUI.PostBackMessagesType.Error);
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Superata la dimensione massima di " + (NetCms.Configurations.PortalData.UploadFileSizeLimit / 1000).ToString() + " MB accettata dal sistema per il file che si desidera trasferire", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "152");
                }
            }
            else
            {

                NetCms.GUI.PopupBox.AddMessage("<strong>Errore nel trasferimento </strong><br />File non valido o descrizione non inserita", NetCms.GUI.PostBackMessagesType.Error);
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore nel trasferimento durante l'inserimento: file non valido o descrizione non inserita", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "26");
            }

        }

        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);

            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            div.Controls.Add(fieldset);
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            fieldset.Controls.Add(legend);
            legend.InnerHtml = "<strong>Trasferimento File</strong>";

            fieldset.Controls.Add(errors);

            HtmlGenericControl p = new HtmlGenericControl("p");

            fieldset.Controls.Add(p);

            Label label = new Label();
            p.Controls.Add(label);
            label.Style.Add("display", "block");
            label.Text = "File* (Max " + (NetCms.Configurations.PortalData.UploadFileSizeLimit/1000).ToString() + "MB)";
            label.AssociatedControlID = "FileUpload";

            uploadform.ID = "FileUpload";            
            p.Controls.Add(uploadform);

            label = new Label();
            p.Controls.Add(label);
            label.Style.Add("display", "block");
            label.Text = "Nome (Puoi lasciare vuoto questo campo, in questo caso verrà utilizzato il nome del file)";
            label.AssociatedControlID = "Nome";

            TextBox nome = new TextBox();
            p.Controls.Add(nome);
            nome.ID = "Nome";
            nome.Columns = 50;

            label = new Label();
            p.Controls.Add(label);
            label.Style.Add("display", "block");
            label.Text = "Descrizione*";
            label.AssociatedControlID = "FileUpload";

            TextBox Descrizione = new TextBox();
            p.Controls.Add(Descrizione);
            Descrizione.ID = "Descrizione";
            Descrizione.Rows = 5;
            Descrizione.TextMode = TextBoxMode.MultiLine;
            Descrizione.Columns = 50;
            label.AssociatedControlID = "Descrizione";


            p = new HtmlGenericControl("p");
            fieldset.Controls.Add(p);

            Button Trasferisci = new Button();
            Trasferisci.Text = "Trasferisci";
            Trasferisci.ID = "Trasferisci";
            Trasferisci.Click += Trasferisci_Click;
            p.Controls.Add(Trasferisci);

            return div;
        }

        /* public HtmlGenericControl getViewLite()
         {
             div = new HtmlGenericControl("div");

             if (PageData.IsPostBack && PageData.Request.Form["Lite"] != null && PageData.Request.Form["Lite"] == "true")
             {
                 error = new HtmlGenericControl();
                 div.Controls.Add(error);
                 error.InnerHtml = "<strong>Errore nel trasferimento </strong><br />File non valido o descrizione non inserita";
             }

             HtmlGenericControl fieldset = new HtmlGenericControl("div");
             div.Controls.Add(fieldset);
             HtmlGenericControl legend = new HtmlGenericControl("p");
             fieldset.Controls.Add(legend);
             legend.InnerHtml = "<strong>Trasferimento File</strong>";

             fieldset.Controls.Add(errors);

             HtmlGenericControl p = new HtmlGenericControl("p");
             fieldset.Controls.Add(p);

             Label label = new Label();
             p.Controls.Add(label);
             label.Style.Add("display", "block");
             label.Text = "File*";
             label.AssociatedControlID = "FileUpload";

             uploadform.ID = "FileUpload";
             p.Controls.Add(uploadform);

             label = new Label();
             p.Controls.Add(label);
             label.Style.Add("display", "block");
             label.Text = "Descrizione*";
             label.AssociatedControlID = "FileUpload";

             TextBox Descrizione = new TextBox();
             p.Controls.Add(Descrizione);
             Descrizione.ID = "Descrizione";
             Descrizione.Rows = 5;
             Descrizione.TextMode = TextBoxMode.MultiLine;
             Descrizione.Columns = 40;
             label.AssociatedControlID = "Descrizione";


             p = new HtmlGenericControl("p");
             fieldset.Controls.Add(p);

             Button Trasferisci = new Button();
             Trasferisci.Text = "Trasferisci";
             Trasferisci.ID = "Trasferisci";
             Trasferisci.Click += Trasferisci_Click;
             p.Controls.Add(Trasferisci);

             HiddenField hidden = new HiddenField();
             hidden.ID = "Lite";
             hidden.Value = "true";
             div.Controls.Add(hidden);

             return div;
         }*/
    }
}
