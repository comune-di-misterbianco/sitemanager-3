﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using System.IO;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using System.Collections.Generic;
using System.Linq;
using NetCms.Structure.WebFS.Reviews;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NHibernate;

namespace NetCms.Structure.Applications.Files
{
    public class FilesReview : NetCms.Structure.WebFS.DocumentReviewPage
    {
        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Bell_Add; }
        }
        public override string PageTitle
        {
            get { return "Nuova Revisione '" + this.CurrentDocument.Nome + "'"; }
        }

        private int FilesID
        {
            get
            {
                return this.CurrentDocument.Content;
            }
        }

        public FilesReview()
        {
            if (this.CurrentDocument == null)
            {
                //Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "File richiesto dall'utente non trovato", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                //HttpContext.Current.NetCms.Diagnostics.Diagnostics.Redirect("files_error.aspx");
            }
        }

        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            /*
            PageData.CurentObjectInfoColtrol = new SideCurentObjectData("CurentDocSideInfo", this.CurrentDocument, PageData).getControl();

            NetFormTable Files;
            if (FilesID == 0)
                Files = new NetFormTable("Docs", "id_Doc", this.CurrentFolder.Network.Connection);
            else
                Files = new NetFormTable("Docs", "id_Doc", this.CurrentFolder.Network.Connection, FilesID);
            
            #region Campi
            
            NetDateField inizio = new NetDateField("Data Inizio", "Inizio_Eventi", this.CurrentFolder.Network.Connection);
            inizio.Required = true;
            Eventi.addField(inizio);

            NetDateField Fine = new NetDateField("Data Fine", "Fine_Eventi", this.CurrentFolder.Network.Connection);
            Fine.Required = true;
            Eventi.addField(Fine);
            
            if (this.Account.GUI.Editor == NetCms.Users.SitemanagerUserGUI.RichTextEditors.FreeTextBox)
            {
                NetRichTextBoxCMS testo = new NetRichTextBoxCMS("Testo", "Testo_Eventi", PageData);
                testo.ToolbarType = NetRichTextBoxCMS.PredefinedToobars.Full;
                Eventi.addField(testo);
            }
            if (this.Account.GUI.Editor == NetCms.Users.SitemanagerUserGUI.RichTextEditors.TinyMCE)
            {
                NetTinyMCE Testo = new NetTinyMCE("Testo", "Testo_Eventi", PageData);
                Testo.ToolbarType = NetTinyMCE.PredefinedToobars.Full;
                Eventi.addField(Testo);
            }
            #endregion
            
            NetForm form = new NetForm();
            form.AddTable(Files);
            form.SubmitLabel = "Salva Revisione";

            #region PostBack

            string errors = "";
            if (PageData.IsPostBack)
            {
                if (PageData.SubmitSource == "send")
                {
                    errors = form.serializedInsert(HttpContext.Current.Request.Form);
                    DocumentOverview.insertReview(DocLang, Files.LastInsert, FilesID, PageData);
                    this.CurrentFolder.Invalidate();
                    NetCms.GUI.PopupBox.AddMessage(errors, NetCms.GUI.PostBackMessagesType.Normal);
                }
            }
            #endregion

            div.Controls.Add(form.getForms("Nuova Revisione"));
            */

            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            div.Controls.Add(fieldset);
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            fieldset.Controls.Add(legend);
            legend.InnerHtml = "<strong>Trasferimento File</strong>";

            fieldset.Controls.Add(errors);

            HtmlGenericControl p = new HtmlGenericControl("p");

            //string logText = "L'utente " + this.Account.Label +"vuole fare l'upload di un nuovo file nella cartella " + this.CurrentFolder.Nome + " (" + this.CurrentFolder.ID + ")";
            //this.GuiLog.SaveLogRecord(logText, false);
            //Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, logText, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            if (!PageData.IsPostBack)
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di creazione di una nuova revisione del file '" + (this.CurrentDocument.Folder as StructureFolder).LabeledPath + this.CurrentDocument.Nome + "', relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            }
            fieldset.Controls.Add(p);

            Label label = new Label();
            p.Controls.Add(label);
            label.Style.Add("display", "block");
            label.Text = "File*";
            label.AssociatedControlID = "FileUpload";

            uploadform.ID = "FileUpload";
            p.Controls.Add(uploadform);

            //label = new Label();
            //p.Controls.Add(label);
            //label.Style.Add("display", "block");
            //label.Text = "Nome (Puoi lasciare vuoto questo campo, in questo caso verrà utilizzato il nome del file)";
            //label.AssociatedControlID = "Nome";

            //TextBox nome = new TextBox();
            //p.Controls.Add(nome);
            //nome.ID = "Nome";
            //nome.Columns = 50;

            //label = new Label();
            //p.Controls.Add(label);
            //label.Style.Add("display", "block");
            //label.Text = "Descrizione*";
            //label.AssociatedControlID = "FileUpload";

            //TextBox Descrizione = new TextBox();
            //p.Controls.Add(Descrizione);
            //Descrizione.ID = "Descrizione";
            //Descrizione.Rows = 5;
            //Descrizione.TextMode = TextBoxMode.MultiLine;
            //Descrizione.Columns = 50;
            //label.AssociatedControlID = "Descrizione";


            p = new HtmlGenericControl("p");
            fieldset.Controls.Add(p);

            Button Trasferisci = new Button();
            Trasferisci.Text = "Trasferisci";
            Trasferisci.ID = "Trasferisci";
            Trasferisci.Click += Trasferisci_Click;
            p.Controls.Add(Trasferisci);

            return div;
        }


        private FileUpload uploadform = new FileUpload();
        private HtmlGenericControl errors = new HtmlGenericControl("div");

        private void Trasferisci_Click(object sender, EventArgs e)
        {
            //NetUtility.RequestVariable Nome = new NetUtility.RequestVariable("Nome");
            //NetUtility.RequestVariable Descrizione = new NetUtility.RequestVariable("Descrizione");
            int id_docLang = this.CurrentDocument.DocLangs.ElementAt(0).ID;
            bool isOk = false;
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {

                    if (uploadform.HasFile /*&& Descrizione.IsValidString*/)
                    {
                        //Uncomment this line to Save the uploaded file
                        string SavePath = this.CurrentFolder.Network.Paths.AbsoluteFrontRoot + this.CurrentFolder.Path + "/";
                        SavePath = HttpContext.Current.Server.MapPath(SavePath);

                        string FileName = uploadform.FileName.Replace(" ", "_").ToLower();
                        string[] ext = FileName.ToString().Split('.');
                        string name = "";
                        for (int i = 0; i < ext.Length - 1; i++)
                            name += ext[i] + ".";
                        name = NetForms.NetTextBoxPlus.ClearFileSystemName(name);
                        FileName = name + "." + ext[ext.Length - 1];
                        FileInfo finfo = new FileInfo(SavePath + FileName);
                        if (!finfo.Exists)
                        {
                            //string nome = Nome.IsValidString ? Nome.StringValue : FileName.Replace("'", "''");

                            ////DataTable rows = this.Connection.SqlQuery("SELECT Name_Doc FROM Docs INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang WHERE Folder_Doc = " + this.CurrentFolder.ID + " AND (Label_DocLang LIKE '" + nome + "' OR Name_Doc LIKE '" + FileName.Replace("'", "''") + "') ");
                            //DataTable rows = this.Connection.SqlQuery("SELECT * FROM Docs INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang INNER JOIN revisioni ON DocLang_Revisione=id_DocLang INNER JOIN docs_files ON id_DocFiles=Contenuto_Revisione WHERE Link_DocFiles = '" + FileName.Replace("'", "''") + "'");
                            IList<FilesDocContent> existFile = FilesBusinessLogic.GetFolderFilesByLink(this.CurrentFolder.ID, FileName.Replace("'", "''"), sess);
                            //if (rows.Rows.Count == 0)
                            if (existFile.Count == 0)
                            {
                                uploadform.SaveAs(SavePath + FileName);
                                //string sql = "";


                                //sql = "INSERT INTO Docs_Files ";
                                //sql += "(Link_DocFiles)";
                                //sql += " Values ('" + FileName.Replace("'", "''") + "')";
                                //int docFileID = this.Connection.ExecuteInsert(sql);

                                //calcolo docLang dentro la sessione interna perchè se no appena salvo alla riga 248 mi darà un illegal attemp to access an object with two open session
                                DocumentLang docLang = DocumentBusinessLogic.GetDocLangById(id_docLang, sess);
                                FilesDocContent content = new FilesDocContent();
                                content.Link = FileName.Replace("'", "''");

                                //SISTEMARE CON CACHE 2° LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                                this.CurrentFolder.Invalidate();

                                if (this.CurrentFolder.ReviewEnabled)
                                {
                                    //this.CurrentDocument.DocLangs.ElementAt(0)
                                    Review revisione = DocumentOverview.insertReview(docLang, content, this.ContentID, PageData, sess);
                                    content.Revisione = revisione;
                                    this.CurrentDocument.DocLangs.ElementAt(0).Revisioni.Add(revisione);
                                    //SISTEMARE CON CACHE 2° LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                                    this.CurrentFolder.Invalidate();

                                }
                                else
                                {
                                    content.DocLang = docLang;//this.CurrentDocument.DocLangs.ElementAt(0);
                                    docLang.Content = content;//this.CurrentDocument.DocLangs.ElementAt(0)
                                }
                                FilesBusinessLogic.SaveOrUpdateFilesDocContent(content, sess);
                                DocumentBusinessLogic.SaveOrUpdateDocumentLang(docLang, sess);//this.CurrentDocument.DocLangs.ElementAt(0)


                                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha creato una nuova revisione del documento '" + (this.CurrentDocument.Folder as StructureFolder).LabeledPath + this.CurrentDocument.Nome + "' eseguendo l'upload del file '" + FileName + "', relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                                tx.Commit();

                                isOk = true;
                            }
                            else
                            {
                                NetCms.GUI.PopupBox.AddMessage("<strong>Errore nel trasferimento </strong><br />Esiste già un file con questo nome.", NetCms.GUI.PostBackMessagesType.Error);
                                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore nel trasferimento durante la revisione: esiste già un file con questo nome", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "22");
                            }
                        }
                        else
                        {
                            NetCms.GUI.PopupBox.AddMessage("<strong>Errore nel trasferimento </strong><br />Esiste già un file con questo nome.", NetCms.GUI.PostBackMessagesType.Error);
                            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore nel trasferimento durante la revisione: esiste già un file con questo nome", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "22");
                        }
                    }
                    else
                    {

                        NetCms.GUI.PopupBox.AddMessage("<strong>Errore nel trasferimento </strong><br />File non valido.", NetCms.GUI.PostBackMessagesType.Error);
                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore nel trasferimento durante la revisione: file non valido o descrizione non inserita", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "23");
                    }
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }

                if (isOk)
                    NetCms.GUI.PopupBox.AddSessionMessage("File Salvato", NetCms.GUI.PostBackMessagesType.Success, true);

            }//chiude gli using
        }
    }
}