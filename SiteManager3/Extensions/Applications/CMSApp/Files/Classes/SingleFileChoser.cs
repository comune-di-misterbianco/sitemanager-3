﻿//using System;
//using System.IO;
//using System.Data;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.HtmlControls;
//using NetCms.Structure.WebFS;
//using System.Linq;

//namespace NetCms.Structure.Applications.Files
//{
//    public class NetSingleFileChoser : NetForms.NetField
//    {
//        private DropDownList _FileList;
//        private DropDownList FileList
//        {
//            get
//            {
//                if (_FileList == null)
//                {
//                    _FileList = new DropDownList();
//                    _FileList.ID = _FieldName;
//                    _FileList.Attributes["onclick"] = "FileChoser_Preview('" + _FileList.ID + "')";
//                }
//                return _FileList;
//            }
//        }
//        public override string Value
//        {
//            set
//            {
//                _Value = value; this.DataBind();
//            }
//            get { return _Value; }
//        }
//        private bool _ImageField;
//        public bool ImageField
//        {
//            get { return _ImageField; }
//            set { _ImageField = value; }
//        }

//        private NetCms.PageData PageData;

//        public NetSingleFileChoser(string label, string fieldname, StructureFolder folder, NetCms.PageData pageData)
//            : base(label, fieldname)
//        {
//            PageData = pageData;
//            Required = false;
//            Folder = folder;
//        }

//        private StructureFolder Folder;

//        public void DataBind()
//        {
//            if (!ImageField)
//            {
//                try
//                {
//                    if (Folder != null)
//                    {
//                        ListItem item;
//                        FileList.Items.Clear();
//                        if (!this.Required)
//                        {
//                            item = new ListItem("Nessuna", "");
//                            FileList.Items.Add(item);
//                        }
//                        for (int i = 0; i < Folder.Documents.Count; i++)
//                        {
//                            item = new ListItem(Folder.Documents.ElementAt(i).Nome, Folder.Documents.ElementAt(i).ID.ToString());
//                            if (Folder.Documents.ElementAt(i).ID.ToString() == this.Value)
//                                item.Selected = true;
//                            FileList.Items.Add(item);
//                        }
//                    }
//                    else throw new Exception("Folder uguale a null");
//                }
//                catch (Exception ex)
//                {
//                    /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
//                    ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
//                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "20");
//                }
//            }
//            else ImagesDataBind();
//        }
//        private void ImagesDataBind()
//        {
//            try
//            {
//                if (Folder != null)
//                {
//                    ListItem item;

//                    FileList.Items.Clear();
//                    if (!this.Required)
//                    {
//                        item = new ListItem("Nessuna", "");
//                        FileList.Items.Add(item);
//                    }

//                    for (int i = 0; i < Folder.Documents.Count; i++)
//                    {
//                        if ((Folder.Documents.ElementAt(i) as FilesDocument).Name.Contains(".jpg") ||
//                           (Folder.Documents.ElementAt(i) as FilesDocument).Name.Contains(".png") ||
//                           (Folder.Documents.ElementAt(i) as FilesDocument).Name.Contains(".gif"))
//                        {
//                            item = new ListItem(Folder.Documents.ElementAt(i).Nome, Folder.Documents.ElementAt(i).ID.ToString());
//                            if (Folder.Documents.ElementAt(i).ID.ToString() == this.Value)
//                                item.Selected = true;
//                            FileList.Items.Add(item);
//                        }
//                    }
//                }
//                else throw new Exception("Folder uguale a null");
//            }
//            catch (Exception ex)
//            {
//                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
//                ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
//                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "21");
//            }
//        }
//        public void addSourceItem(string label, string value)
//        {
//            FileList.Items.Add(new ListItem(label, value));
//        }

//        public override string validateInput(string input)
//        {
//            string errors = "";

//            if (Required && input.Length == 0)
//            {
//                errors += "<li>";
//                errors += "Il campo '" + _Label + "' è obbligatorio";
//                errors += "</li>";
//            }

//            return errors;
//        }
//        public override string validateValue(string value)
//        {
//            string val = value;

//            int i = 0;
//            if (!int.TryParse(val, out i))
//                val = "'" + val + "'";

//            return val;
//        }
//        public override string getFilter(string value)
//        {
//            string filter = " ";
//            if (value.Length > 0)
//            {
//                filter += _FieldName;

//                filter += " = ";
//                filter += value;
//                filter += " ";
//            }
//            return filter;
//        }

//        private HtmlGenericControl FileListControl()
//        {
//            HtmlGenericControl par = new HtmlGenericControl("p");

//            Label lb = new Label();
//            lb.AssociatedControlID = _FieldName;
//            if (Required)
//                lb.Text = Label + "*";
//            else
//                lb.Text = Label;
//            lb.Style.Add("display", "block");
//            par.Controls.Add(lb);

//            par.Controls.Add(FileList);
//            HtmlGenericControl button = new HtmlGenericControl("div");

//            button.InnerHtml += "<input id=\"" + FieldName + "_Transfer" + "\" name=\"" + FieldName + "_ADD" + "\" type=\"button\" onclick=\"javascript: TransferDialog('" + PageData.Paths.AbsoluteNetworkRoot + "/','" + Folder.ID + "')\" value=\"Trasferisci\" />";

//            par.Controls.Add(button);

//            return par;
//        }
//        private HtmlGenericControl PreviewControl()
//        {
//            HtmlGenericControl iframe = new HtmlGenericControl("iframe");
//            iframe.ID = FieldName + "_PreviewBox";
//            iframe.Attributes["class"] = "SingleFileChoserPreviewBox";
//            return iframe;
//        }

//        public override HtmlGenericControl getControl()
//        {
//            HtmlGenericControl p = new HtmlGenericControl("p");

//            HtmlGenericControl table = new HtmlGenericControl("table");
//            table.Attributes["class"] = "SingleFileChoserTable";
//            table.Attributes["cellpadding"] = "0";
//            table.Attributes["cellspacing"] = "0";
//            HtmlGenericControl tr = new HtmlGenericControl("tr");
//            HtmlGenericControl td;

//            table.Controls.Add(tr);


//            td = new HtmlGenericControl("td");
//            td.Attributes["class"] = "SingleFileChoserDDL";
//            td.Controls.Add(FileListControl());
//            tr.Controls.Add(td);

//            tr = new HtmlGenericControl("tr");
//            table.Controls.Add(tr);
//            td = new HtmlGenericControl("td");
//            td.Attributes["colspan"] = "3";
//            td.Attributes["class"] = "SingleFileChoserPreview";
//            td.Controls.Add(PreviewControl());
//            tr.Controls.Add(td);

//            p.Controls.Add(table);


//            HtmlGenericControl script = new HtmlGenericControl("script");
//            script.Attributes["type"] = "text/javascript";
//            script.InnerHtml = @"FileChoser_Preview('" + FileList.ID + "')";

//            script.InnerHtml += @"
//
//    function FileChoser_Preview(Source)
//    {
//        oPreview = document.getElementById('" + FieldName + @"_PreviewBox'); 
//        var previewLoaded = false;
//        if(Source)
//        {
//            oSource = document.getElementById(Source); 
//               
//            if(oSource != null && oSource.options.length > 0)
//            {
//                var index = oSource.options.selectedIndex
//                if(index!=null && index<0)
//                    index = 0;
//                oOption = oSource.options[index];
//                
//                if(oOption != null)            
//                {
//                    var extsplit = oOption.text.split('.');
//                    var ext = extsplit[extsplit.length-1];
//                    if(ext!=null && ( ext == ""jpg"" || ext == ""gif"" || ext == ""png"" ))
//                    {
//                        oPreview.src = """ + PageData.Paths.AbsoluteWebRoot + Folder.Path + @"/""+oOption.text;
//                        oPreview.style.display = ""block"";
//                        previewLoaded = true;
//                    }
//                }
//            } 
//        }
//        if(!previewLoaded)
//            FileChoser_HidePreview();
//    }
//
//    function FileChoser_AddOption(target,text,value)
//    {
//        oTarget = document.getElementById(target);
//        oOption = oSource.options[oTarget.options.length]; 
//        oTarget.options[oTarget.options.length] = new Option(text,value);
//    }
//
//
//    function FileChoser_HidePreview()
//    {
//        oPreview.src = """";
//        oPreview.style.display = ""none"";
//    }
//    function MoveItemBetweenListBox(Source,Dest,Pool,Max)
//    {   
//        oDest = document.getElementById(Dest);
//        if(Max && Max > 0 && Max-1 < oDest.options.length)
//            alert(""Il numero massimo di file selezionabili è ""+Max);            
//        else
//        {
//            oPool = document.getElementById(Pool);
//            oSource = document.getElementById(Source);
//            
//            if(oSource.options.selectedIndex !=null &&
//            oSource.options.selectedIndex >= 0 &&
//            oSource.options.selectedIndex <= oSource.options.length
//            )
//            {    
//                oOption = oSource.options[oSource.options.selectedIndex];
//                oSource.remove(oSource.options.selectedIndex);        
//                oDest.options[oDest.options.length] = new Option(oOption.text,oOption.value);
//                if(oPool.value.match("",""+oOption.value)!=null)
//                    oPool.value = oPool.value.replace("",""+oOption.value,"""");
//                else
//                    oPool.value+="",""+oOption.value;
//            }
//            /*if(Max==1 && oDest.options.length==1)
//                FileChoser_Preview(Dest);
//            else
//                FileChoser_Preview();*/
//        }
//    }
//    
//" + TransferDialogJS(FieldName);

//            p.Controls.Add(script);

//            return p;
//        }

//        public static string TransferDialogJS(string fieldname)
//        {
//            return @" 
//            function TransferDialog(address,folder)
//            {   //     alert(address)
//	            window.open(address+""files/transfer.aspx?folder=""+folder+""&ctrid=" + fieldname + @""","""",""height=318,width=400,status=yes,toolbar=no,menubar=no,location=no"");//""1,0,0,250,200,0,0,0,0,1,0,100,150"");	    
//            }
//            ";

//        }
//    }

//}
