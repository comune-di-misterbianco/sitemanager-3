﻿//using System;
//using System.IO;
//using System.Data;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.HtmlControls;
//using NetCms.Structure.WebFS;
//using System.Linq;

//namespace NetCms.Structure.Applications.Files
//{
//    public class NetFileChoser : NetForms.NetField
//    {
//        private string _SelectedFilesQuery;
//        public string SelectedFilesQuery
//        {
//            get { return _SelectedFilesQuery; }
//            set { _SelectedFilesQuery = value; }
//        }

//        private ListBox _SelectedList;
//        private ListBox SelectedList
//        {
//            get
//            {
//                if (_SelectedList == null)
//                {
//                    _SelectedList = new ListBox();
//                    _SelectedList.Rows = 15;
//                    _SelectedList.ID = _FieldName + "_Selected";
//                    _SelectedList.Attributes["onclick"] = "FileChoser_Preview('" + _SelectedList.ID + "')";
//                }
//                return _SelectedList;
//            }
//        }

//        private int _MaxSelected;
//        public int MaxSelected
//        {
//            get { return _MaxSelected; }
//            set { _MaxSelected = value; }
//        }


//        private ListBox _FileList;
//        private ListBox FileList
//        {
//            get
//            {
//                if (_FileList == null)
//                {
//                    _FileList = new ListBox();
//                    _FileList.Rows = 15;
//                    _FileList.ID = _FieldName + "_Pool";
//                    _FileList.Attributes["onclick"] = "FileChoser_Preview('" + _FileList.ID + "')";
//                }
//                return _FileList;
//            }
//        }

//        private NetCms.PageData PageData;

//        public NetFileChoser(string label, string fieldname, StructureFolder folder, NetCms.PageData pageData)
//            : base(label, fieldname)
//        {
//            _MaxSelected = 0;
//            PageData = pageData;
//            Required = false;
//            Folder = folder;
//        }

//        private StructureFolder Folder;

//        public void DataBind()
//        {
//            try
//            {
//                if (Folder != null)
//                {
//                    ListItem item;
//                    for (int i = 0; i < Folder.Documents.Count; i++)
//                    {
//                        item = new ListItem(Folder.Documents.ElementAt(i).Nome, Folder.Documents.ElementAt(i).ID.ToString());
//                        FileList.Items.Add(item);
//                    }
//                }
//                else throw new Exception("Folder uguale a null");
//            }
//            catch (Exception ex) 
//            {
//                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
//                ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
//                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "16");
//            }
//        }
//        public void ImagesDataBind()
//        {
//            try
//            {
//                if (Folder != null)
//                {
//                    for (int i = 0; i < Folder.Documents.Count; i++)
//                    {
//                        if ((Folder.Documents.ElementAt(i) as FilesDocument).Name.Contains(".jpg") ||
//                           (Folder.Documents.ElementAt(i) as FilesDocument).Name.Contains(".png") ||
//                           (Folder.Documents.ElementAt(i) as FilesDocument).Name.Contains(".gif"))

//                            FileList.Items.Add(new ListItem(Folder.Documents.ElementAt(i).Nome, Folder.Documents.ElementAt(i).ID.ToString()));
//                    }
//                }
//                else throw new Exception("Folder uguale a null");
//            }
//            catch (Exception ex)
//            {
//                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
//                ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
//                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "17");
//            }
//        }
//        public void addSourceItem(string label, string value)
//        {
//            FileList.Items.Add(new ListItem(label, value));
//        }

//        public override string validateInput(string input)
//        {
//            string errors = "";

//            if (Required && input.Length == 0)
//            {
//                errors += "<li>";
//                errors += "Il campo '" + _Label + "' è obbligatorio";
//                errors += "</li>";
//            }

//            return errors;
//        }
//        public override string validateValue(string value)
//        {
//            string val = value;

//            //nel caso in cui si selezioni solo un allegato come impostazione
//            //faccio in modo da far uscire solo un id
//            if (MaxSelected == 1)
//            {
//                int first = val.IndexOf(',');
//                int last = val.LastIndexOf(',');
//                if (first == last)
//                    val = val.Replace(",", "");
//            }
//            int i = 0;
//            if (!int.TryParse(val, out i))
//                val = "'" + val + "'";
//            return val;
//        }
//        public override string getFilter(string value)
//        {
//            string filter = " ";
//            if (value.Length > 0)
//            {
//                filter += _FieldName;

//                filter += " = ";
//                filter += value;
//                filter += " ";
//            }
//            return filter;
//        }

//        private HtmlGenericControl SelectButtons()
//        {
//            HtmlGenericControl par = new HtmlGenericControl("p");


//            par.InnerHtml += "<div><input id=\"" + FieldName + "_Transfer" + "\" name=\"" + FieldName + "_ADD" + "\" type=\"button\" onclick=\"javascript: TransferDialog('" + PageData.Paths.AbsoluteNetworkRoot + "/','" + Folder.ID + "')\" value=\"Trasferisci\" /></div>";
//            par.InnerHtml += "<div><input id=\"" + FieldName + "_ADD" + "\" name=\"" + FieldName + "_ADD" + "\" type=\"button\" onclick=\"javascript: MoveItemBetweenListBox('" + FieldName + "_Pool','" + FieldName + "_Selected','" + FieldName + "'," + MaxSelected + ")\" value=\">>\" /></div>";
//            par.InnerHtml += "<div><input id=\"" + FieldName + "_REMOVE" + "\" name=\"" + FieldName + "_REMOVE" + "\" type=\"button\" onclick=\"javascript: MoveItemBetweenListBox('" + FieldName + "_Selected','" + FieldName + "_Pool','" + FieldName + "')\" value=\"<<\" /></div>";


//            return par;
//        }
//        private HtmlGenericControl FileListControl()
//        {
//            HtmlGenericControl par = new HtmlGenericControl("p");

//            Label lb = new Label();
//            lb.AssociatedControlID = _FieldName + "_Pool";
//            if (Required)
//                lb.Text = "File disponibili";
//            else
//                lb.Text = "File disponibili";
//            lb.Style.Add("display", "block");
//            par.Controls.Add(lb);

//            par.Controls.Add(FileList);

//            return par;
//        }
//        private HtmlGenericControl SelectedListControl()
//        {
//            HtmlGenericControl par = new HtmlGenericControl("p");
//            Label lb = new Label();
//            lb.AssociatedControlID = _FieldName + "_Selected";
//            lb.Text = "File selezionat" + (MaxSelected > 1 ? "i" : "o");
//            lb.Style.Add("display", "block");
//            par.Controls.Add(lb);

//            par.Controls.Add(SelectedList);
//            //Non trovo il punto in cui viene settato SelectedFilesQuery...fà qualcosa questo codice?
//            if (this.SelectedFilesQuery != null && this.SelectedFilesQuery.Length > 0)
//            {
//                DataTable dt = PageData.Conn.SqlQuery(this.SelectedFilesQuery);
//                ListItem item;
//                foreach (DataRow row in dt.Rows)
//                {
//                    try
//                    {
//                        //NetCms.Structure.WebFS.Document doc = PageData.CurrentFolder.FindDoc(row[0].ToString());
//                        //Suppongo sia una ricerca per ID
//                        NetCms.Structure.WebFS.Document doc = PageData.CurrentFolder.FindDocByID(int.Parse(row[0].ToString())) as FilesDocument;
//                        if (doc != null)
//                        {
//                            item = new ListItem(doc.Nome, doc.ID.ToString());

//                            item = FileList.Items.FindByValue(doc.ID.ToString());
//                            if (item != null)
//                                FileList.Items.Remove(item);

//                            SelectedList.Items.Add(item);
//                        }
//                        else throw new Exception("Documento non trovato");
//                    }
//                    catch (Exception ex)
//                    {
//                        /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
//                        ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
//                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "18");
//                    }
//                }
//            }
//            else
//            {
//                if (Value.Length > 0)
//                {
//                    try
//                    {
//                        //NetCms.Structure.WebFS.Document doc = Folder.FindDoc(Value);
//                        NetCms.Structure.WebFS.Document doc = Folder.FindDocByID(int.Parse(Value)) as FilesDocument;
//                        if (doc != null)
//                        {
//                            SelectedList.Items.Add(new ListItem(doc.Name, doc.ID.ToString()));
//                            ListItem item;
//                            item = FileList.Items.FindByValue(doc.ID.ToString());
//                            if (item != null)
//                                FileList.Items.Remove(item);
//                        }
//                        else throw new Exception("Documento non trovato");
//                    }
//                    catch (Exception ex)
//                    {
//                        /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
//                        ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
//                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "19");
//                    }
//                }
//            }
//            return par;
//        }
//        private HtmlGenericControl PreviewControl()
//        {
//            HtmlGenericControl iframe = new HtmlGenericControl("iframe");
//            iframe.ID = FieldName + "_PreviewBox";
//            iframe.Attributes["class"] = "FileChoserPreviewBox";
//            return iframe;
//        }

//        public override HtmlGenericControl getControl()
//        {
//            HtmlGenericControl p = new HtmlGenericControl("p");
//            HtmlGenericControl FileChoserTitle = new HtmlGenericControl("span");
//            FileChoserTitle.Attributes["class"] = "FileChoserTitle";
//            FileChoserTitle.InnerHtml = Label;
//            HtmlGenericControl table = new HtmlGenericControl("table");
//            table.Attributes["class"] = "FileChoserTable";
//            table.Attributes["cellpadding"] = "0";
//            table.Attributes["cellspacing"] = "0";
//            HtmlGenericControl tr = new HtmlGenericControl("tr");
//            HtmlGenericControl td;

//            table.Controls.Add(tr);


//            td = new HtmlGenericControl("td");
//            td.Attributes["class"] = "FileChoserSX";
//            td.Controls.Add(FileListControl());
//            tr.Controls.Add(td);

//            td = new HtmlGenericControl("td");
//            td.Attributes["class"] = "FileChoserCX";
//            td.Controls.Add(SelectButtons());
//            tr.Controls.Add(td);

//            td = new HtmlGenericControl("td");
//            td.Attributes["class"] = "FileChoserDX";
//            td.Controls.Add(SelectedListControl());
//            tr.Controls.Add(td);

//            tr = new HtmlGenericControl("tr");
//            table.Controls.Add(tr);
//            td = new HtmlGenericControl("td");
//            td.Attributes["colspan"] = "3";
//            td.Attributes["class"] = "FileChoserPreview";
//            td.Controls.Add(PreviewControl());
//            tr.Controls.Add(td);

//            p.Controls.Add(FileChoserTitle);
//            p.Controls.Add(table);

//            HtmlInputHidden attachsStr = new HtmlInputHidden();
//            attachsStr.ID = _FieldName;
//            attachsStr.Name = _FieldName;
//            attachsStr.Value = "";
//            foreach (ListItem item in this.SelectedList.Items)
//            {
//                attachsStr.Value += "," + item.Value;
//            }
//            p.Controls.Add(attachsStr);

//            HtmlGenericControl script = new HtmlGenericControl("script");
//            script.Attributes["type"] = "text/javascript";
//            script.InnerHtml = @"FileChoser_Preview('" + SelectedList.ID + "')";

//            script.InnerHtml += @"
//
//    function FileChoser_Preview(Source)
//    {
//        oPreview = document.getElementById('" + FieldName + @"_PreviewBox'); 
//        var previewLoaded = false;
//        if(Source)
//        {
//            oSource = document.getElementById(Source); 
//               
//            if(oSource != null && oSource.options.length > 0)
//            {
//                var index = oSource.options.selectedIndex
//                if(index!=null && index<0)
//                    index = 0;
//                oOption = oSource.options[index];
//                
//                if(oOption != null)            
//                {
//                    var extsplit = oOption.text.split('.');
//                    var ext = extsplit[extsplit.length-1];
//                    if(ext!=null && ( ext == ""jpg"" || ext == ""gif"" || ext == ""png"" ))
//                    {
//                        oPreview.src = """ + PageData.Paths.AbsoluteWebRoot + Folder.Path + @"/""+oOption.text;
//                        oPreview.style.display = ""block"";
//                        previewLoaded = true;
//                    }
//                }
//            } 
//        }
//        if(!previewLoaded)
//            FileChoser_HidePreview();
//    }
//
//    function FileChoser_AddOption(target,text,value)
//    {
//        oTarget = document.getElementById(target);
//        oOption = oSource.options[oTarget.options.length]; 
//        oTarget.options[oTarget.options.length] = new Option(text,value);
//    }
//
//
//    function FileChoser_HidePreview()
//    {
//        oPreview.src = """";
//        oPreview.style.display = ""none"";
//    }
//    function MoveItemBetweenListBox(Source,Dest,Pool,Max)
//    {   
//        oDest = document.getElementById(Dest);
//        if(Max && Max > 0 && Max-1 < oDest.options.length)
//            alert(""Il numero massimo di file selezionabili è ""+Max);            
//        else
//        {
//            oPool = document.getElementById(Pool);
//            oSource = document.getElementById(Source);
//            
//            if(oSource.options.selectedIndex !=null &&
//            oSource.options.selectedIndex >= 0 &&
//            oSource.options.selectedIndex <= oSource.options.length
//            )
//            {    
//                oOption = oSource.options[oSource.options.selectedIndex];
//                oSource.remove(oSource.options.selectedIndex);        
//                oDest.options[oDest.options.length] = new Option(oOption.text,oOption.value);
//                if(oPool.value.match("",""+oOption.value)!=null)
//                    oPool.value = oPool.value.replace("",""+oOption.value,"""");
//                else
//                    oPool.value+="",""+oOption.value;
//            }
//            /*if(Max==1 && oDest.options.length==1)
//                FileChoser_Preview(Dest);
//            else
//                FileChoser_Preview();*/
//        }
//    }
//    
//" + TransferDialogJS(FieldName);

//            p.Controls.Add(script);

//            return p;
//        }

//        public static string TransferDialogJS(string fieldname)
//        {
//            return @" 
//            function TransferDialog(address,folder)
//            {   //     alert(address)
//	            window.open(address+""files/transfer.aspx?folder=""+folder+""&ctrid=" + fieldname + "_Pool" + @""","""",""height=318,width=400,status=yes,toolbar=no,menubar=no,location=no"");//""1,0,0,250,200,0,0,0,0,1,0,100,150"");	    
//            }
//            ";

//        }
//    }
//}
