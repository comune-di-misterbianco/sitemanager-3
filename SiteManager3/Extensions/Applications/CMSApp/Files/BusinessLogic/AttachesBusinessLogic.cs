﻿using GenericDAO.DAO;
using NetCms.Networks;
using NetCms.Networks.WebFS;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS.Reviews;
using NetService.Utility.ValidatedFields;
using NHibernate;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCms.Structure.Applications.Files
{
    public static class AttachesBusinessLogic
    {

        private const string tmpPathPattern = "/repository/contents/{year}/{month}/{day}";
   
        private static string TmpFolderPath
        {
            get
            {
                string tmpPath = NetCms.Configurations.Paths.AbsoluteRoot + tmpPathPattern;
                tmpPath = tmpPath.Replace("{year}", DateTime.Today.Year.ToString())
                                   .Replace("{month}", DateTime.Today.Month.ToString())
                                   .Replace("{day}", DateTime.Today.Day.ToString());
                return tmpPath;
            }
        }
        public static NHibernate.ISession GetCurrentSession()
        {
            return DBSessionProvider.FluentSessionProvider.Instance.GetSession();
        }

        public static void SaveAttaches(int idDoc, 
            int docContentId, 
            DocContent prevDocContent, 
            VfGeneric uploadField, 
            List<string> attachesToKeep,
            int networkID, 
            int accountID, 
            ISession sess)
        {
            Document cmsdocument = DocumentBusinessLogic.GetById(idDoc, sess);

            StructureFolder destinationAttachFolder = GetFolderByDate(networkID); 

            DocContent currentDocument = DocumentBusinessLogic.GetDocContentById(docContentId, sess);

            if (prevDocContent != null && prevDocContent.Attaches.Any()) // TODO: attenzio
                                               // ne a eventuali duplicati - in caso vanno skippati
            {
                foreach (DocumentAttach a in prevDocContent.Attaches) 
                {
                    if (attachesToKeep.Contains(a.FileDocument.PhysicalName))
                    {
                        DocumentAttach documentAttach = new DocumentAttach();
                        documentAttach.Content = currentDocument;
                        documentAttach.FileDocument = a.FileDocument;
                        currentDocument.Attaches.Add(documentAttach);
                    }
                }
            }

            IList<FileUploadedV2> fileList = (IList<FileUploadedV2>)uploadField.PostbackValueObject;
            if (fileList != null && fileList.Any())
            {
                foreach (FileUploadedV2 allegato in fileList)
                {
                    string fileNameCorrect = NetCms.Connections.Connection.FilterString(allegato.FileName);
                    fileNameCorrect = fileNameCorrect.Replace("''", "-").Replace(" ", "-");

                    string source = NetCms.Configurations.Paths.AbsoluteRoot + "/" + TmpFolderPath + "/";
                    source = System.Web.HttpContext.Current.Server.MapPath(source);


                    string destination = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + "/" + destinationAttachFolder.Path + "/");

                    if (File.Exists(destination + fileNameCorrect))
                    {
                        string file_rinominato = "";

                        FileInfo file_info = new FileInfo(destination + fileNameCorrect);
                        string ext = file_info.Extension;
                        string nome_file = file_info.Name;

                        nome_file = nome_file.Replace(ext, "");
                        ext = ext.Replace(".", "");
                        // TODO: fix da effettuare - far restituire le istanze nella cartella corrente
                        int tab = DocumentBusinessLogic.GetNumberOfInstanceFile(nome_file, ext, FilesApplication.ApplicationID, sess);

                        int valore = (tab + 1);

                        if (tab != 0)
                        {
                            valore = tab + 1;
                            file_rinominato = nome_file + "(" + valore.ToString() + ")" + "." + ext;
                        }
                        else
                        {
                            file_rinominato = nome_file + "." + ext;
                        }

                        while (File.Exists(destination + file_rinominato))
                        {
                            valore++;
                            file_rinominato = nome_file + "(" + valore.ToString() + ")" + "." + ext;
                        }

                        File.Move(source + allegato.FileName, destination + file_rinominato);



                        InsertAttach(cmsdocument, currentDocument, file_rinominato, networkID, accountID, destination, destinationAttachFolder, sess);
                    }
                    else
                    {
                        File.Move(source + allegato.FileName, destination + fileNameCorrect);


                        InsertAttach(cmsdocument, currentDocument, fileNameCorrect, networkID, accountID, destination, destinationAttachFolder, sess);
                    }

                }
            }
        }


        /// <summary>
        /// NB il metodo è chiamato da la produra di importazione massiva via reflection  prestare attenzione se si applicano modifiche ai parametri
        /// e ritestare il flusso completo. Vedi CreateDocumentFromImport in NetCms.Structure.WebFS.StructureFolder
        /// </summary>
        /// <param name="cmsDocument"></param>
        /// <param name="currentDocument"></param>
        /// <param name="nomefile"></param>
        /// <param name="networkID"></param>
        /// <param name="accountID"></param>
        /// <param name="absoluteFolderPath"></param>
        /// <param name="sess"></param>
        public static void InsertAttach(Document cmsDocument,     
                                      DocContent currentDocument,
                                      string nomefile,  
                                      int networkID,
                                      int accountID,
                                      string absoluteFolderPath,
                                      StructureFolder destinationAttachFolder,
                                      NHibernate.ISession sess)
        {
            

            FilesDocContent docFile = null;
            DocumentLang docLang = null;
                      

            //StructureFolder currentAttachFolder = AttachsFolder(networkID);

            Document document = new FilesDocument(destinationAttachFolder.NetworkKey);
            document.Application = 4;
            document.PhysicalName = nomefile;
            document.Create = DateTime.Today;
            document.Hide = 1; //TODO: implementare una visualizzazione per l'utente admin per la manutenzione
            document.Data = DateTime.Today;
            document.Stato = 1;
            document.Folder = destinationAttachFolder;
            document.Autore = accountID;

            destinationAttachFolder.Documents.Add(document);

            docFile = new FilesDocContent();
            docFile.Link = nomefile;
            if (!string.IsNullOrEmpty(absoluteFolderPath))
            {
                string filepath = absoluteFolderPath + nomefile;

                docFile.Size = SharedUtilities.IO.FileUtility.GetSizeInKB(filepath);
                docFile.ContentType = File.Exists(filepath) ? MimeMapping.MimeUtility.GetMimeMapping(filepath) : "";
                docFile.Extension = File.Exists(filepath) ? System.IO.Path.GetExtension(filepath) : "";
            }

            docLang = new DocumentLang();
            docLang.Document = document;
            document.DocLangs.Add(docLang);
            docLang.Content = docFile;
            docFile.DocLang = docLang;
            docLang.Label = nomefile;
            docLang.Lang = destinationAttachFolder.StructureNetwork.DefaultLang;
            docLang.Descrizione = "File allegato";

            cmsDocument.Allegati.Add(document);

            document = DocumentBusinessLogic.SaveOrUpdateDocument(document, sess);
            FilesBusinessLogic.SaveOrUpdateFilesDocContent(docFile, sess);
            DocumentBusinessLogic.SaveOrUpdateDocumentLang(docLang, sess);

            if (document != null && docFile != null && docLang != null)
            {
                DocumentBusinessLogic.SaveOrUpdateDocument(cmsDocument, sess);
            }

            DocumentAttach documentAttach = new DocumentAttach();
            documentAttach.FileDocument = document;
            documentAttach.Content = currentDocument;

            if (currentDocument.Attaches == null)
                currentDocument.Attaches = new List<DocumentAttach>();
            currentDocument.Attaches.Add(documentAttach);
            
            // NB con questo salvataggio dovrebbe salvare in cascata anche il docContent
            cmsDocument = DocumentBusinessLogic.SaveOrUpdateDocument(cmsDocument, sess);

            Review revisione = new Review(Users.AccountManager.CurrentAccount);
            revisione.DocLang = docLang;
            revisione.Content = docFile;
            revisione.Data = DateTime.Now;
            revisione.Autore = Users.AccountManager.CurrentAccount.ID;
            revisione.Origine = 0;
            revisione.Published = 1;

            docLang.Revisioni.Add(revisione);
            docFile.Revisione = revisione;

            ReviewBusinessLogic.SaveOrUpdateReview(revisione, sess);
        }

        public static StructureFolder GetFolderByDate(int networkID, DateTime? data = null)
        {
            if (data == null)
                data = DateTime.Today;

            StructureFolder contentsFolder = NetworkContentsFolder(networkID);
            
            string folderYearName = data.Value.Year.ToString();

            StructureFolder yearFolder = contentsFolder.FindFolderByName(folderYearName) as StructureFolder;
            if (yearFolder == null)            
                yearFolder = contentsFolder.CreateFolder(folderYearName, folderYearName, "Cms attaches folders "+ folderYearName + " for network {0}", FilesApplication.ApplicationID);                        
            
            string folderMontName = data.Value.Month.ToString();

            StructureFolder monthFolder = yearFolder.FindFolderByName(folderMontName) as StructureFolder;
            if (monthFolder == null)
                monthFolder = yearFolder.CreateFolder(folderMontName, folderMontName, "Cms attaches folders " + folderMontName + " for network {0}", FilesApplication.ApplicationID);

            string folderDayName = data.Value.Day.ToString();

            StructureFolder dayFolder = monthFolder.FindFolderByName(folderDayName) as StructureFolder;
            if (dayFolder == null)
                dayFolder = monthFolder.CreateFolder(folderDayName, folderDayName, "Cms attaches folders " + folderDayName + " for network {0}", FilesApplication.ApplicationID);


            return dayFolder;
        }



        public static StructureFolder NetworkContentsFolder(int networkID)
        {                     
            StructureFolder rootFolder = FolderBusinessLogic.FindRootFoldersByNetwork(networkID);
            var folders = rootFolder.SubFolders.Where(x=>x.Nome.ToLower() == "contents");
            
            StructureFolder contentsFolder = (folders.Count() > 0) ? folders.First() : null;
            if (contentsFolder == null)
            {
                contentsFolder = rootFolder.CreateFolder("contents", "contents", "Cms attaches folders for network {0}", FilesApplication.ApplicationID);
            }

            return contentsFolder;
        }      
        //public static StructureFolder AttachsFolder(int networkID)
        //{
           
        //        StructureFolder currentfolder = NetworkContentsFolder(networkID);
        //        StructureFolder attachsFolder;

        //        var folders = currentfolder.SubFolders.Where(x => x.Nome.ToLower() == "allegati");
        //        attachsFolder = (folders.Count() > 0) ? folders.First() : null;

        //        if (attachsFolder == null)                    
        //            attachsFolder = currentfolder.CreateFolder("Allegati", "allegati", "Contenitore degli allegati", FilesApplication.ApplicationID);

        //        return attachsFolder;
            
        //}
    
        public static object GetAttachesByDocContent(int docContentID, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            DocContent docContent = DocumentBusinessLogic.GetDocContentById(docContentID, innerSession);

            ICollection<DocumentAttach> allegati = docContent.Revisione.Content.Attaches;
                   
            List<object> attachedFiles = new List<object>();
           
            if (allegati != null && allegati.Any())
            {
                foreach (DocumentAttach allegato in allegati)
                {
                    DocContent document = innerSession.GetSessionImplementation().PersistenceContext.Unproxy(DocumentBusinessLogic.GetDocContentById(allegato.FileDocument.Content, innerSession)) as DocContent;
                    FilesDocContent fileDocContent = (FilesDocContent)document;

                    var attachedFile = new
                    {
                        label = allegato.FileDocument.Nome,
                        url = allegato.FileDocument.FrontendLink,
                        size = fileDocContent.Size,
                        extension = fileDocContent.Extension,
                        contenttype = fileDocContent.ContentType
                    };

                    attachedFiles.Add(attachedFile);
                }
            }

            return attachedFiles;
        }       
    }
}
