﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using GenericDAO.DAO;
using NHibernate.Transform;
using NHibernate.Criterion;
using NetService.Utility.RecordsFinder;
using NetCms.DBSessionProvider;

namespace NetCms.Structure.Applications.Files
{
    public static class FilesBusinessLogic
    {

        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static FilesDocContent SaveOrUpdateFilesDocContent(FilesDocContent doc, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<FilesDocContent> dao = new CriteriaNhibernateDAO<FilesDocContent>(innerSession);
            return dao.SaveOrUpdate(doc);
        }

        public static FilesDocContent GetPublishedLinkByDocLang(int DocLang, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<FilesDocContent> dao = new CriteriaNhibernateDAO<FilesDocContent>(innerSession);

            SearchParameter docLangSP = new SearchParameter(null, "ID", DocLang, Finder.ComparisonCriteria.Equals, false);
            SearchParameter docLangRevSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToMany, docLangSP);
            SearchParameter publishedSP = new SearchParameter(null, "Published", 1, Finder.ComparisonCriteria.Equals, false);
            SearchParameter revisioneSP = new SearchParameter(null, "Revisione", SearchParameter.RelationTypes.OneToOne, new SearchParameter[] { publishedSP, docLangRevSP });
            IList<FilesDocContent> list = dao.FindByCriteria(new SearchParameter[] { revisioneSP });
            if (list.Count > 0)
                return list.First();
            return null;
        }

        public static IList<FilesDocContent> GetFolderFilesByLink(int folderID, string link, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<FilesDocContent> dao = new CriteriaNhibernateDAO<FilesDocContent>(innerSession);

            SearchParameter idFolderSP = new SearchParameter(null, "ID", folderID, Finder.ComparisonCriteria.Equals, false);
            SearchParameter folderSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, idFolderSP);
            SearchParameter docSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, folderSP);
            SearchParameter docLangRevSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToMany, docSP);
            SearchParameter revisioneSP = new SearchParameter(null, "Revisione", SearchParameter.RelationTypes.OneToOne, new SearchParameter[] { docLangRevSP });

            SearchParameter linkSP = new SearchParameter(null, "Link", link, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[] { revisioneSP, linkSP });

        }

        //DA TESTARE IL FUNZIONAMENTO
        public static IList<FilesDocContent> GetFilesInFolderByLabelOrNameOrLink(int folderID, string link, string name, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            var files = innerSession.CreateCriteria<FilesDocContent>()
                .CreateAlias("Revisione", "rev")
                .CreateAlias("rev.DocLang", "dL")
                .CreateAlias("dL.Document", "doc")
                .CreateAlias("doc.Folder", "fold")
                .Add(Expression.Eq("fold.ID", folderID))
                .Add(Restrictions.Disjunction()
                    .Add(Expression.Like("dL.Label", name))
                    .Add(Expression.Like("doc.PhysicalName", link))
                    .Add(Expression.Like("Link", link))
                ).List<FilesDocContent>();
            return files;
        }

        #region Metodi di Frontend

        public static IList<FilesDocContent> ListDocumentRecords(int idFile, int idFolder)
        {
            IGenericDAO<FilesDocContent> dao = new CriteriaNhibernateDAO<FilesDocContent>(GetCurrentSession());

            SearchParameter idFolderSP = new SearchParameter(null, "ID", idFolder, Finder.ComparisonCriteria.Equals, false);
            SearchParameter foldersSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, idFolderSP);


            SearchParameter documentSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { foldersSP });
            SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, documentSP);
            SearchParameter revisioneSP = new SearchParameter(null, "Revisione", SearchParameter.RelationTypes.OneToOne, docLangSP);

            SearchParameter idSP = new SearchParameter(null, "ID", idFile, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[] { revisioneSP, idSP });
        }

        public static IList<FilesDocContent> ListDocumentRecordsPublishedByFolder(int idFolder)
        {
            IGenericDAO<FilesDocContent> dao = new CriteriaNhibernateDAO<FilesDocContent>(GetCurrentSession());

            SearchParameter idFolderSP = new SearchParameter(null, "ID", idFolder, Finder.ComparisonCriteria.Equals, false);
            SearchParameter foldersSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, idFolderSP);

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            Dictionary<string, bool> orders = new Dictionary<string, bool>();
            orders.Add("Order", false);

            SearchParameter documentSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, orders, new SearchParameter[] { foldersSP, trashSP });
            SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, documentSP);

            SearchParameter publishedSP = new SearchParameter(null, "Published", 1, Finder.ComparisonCriteria.Equals, false);

            SearchParameter revisioneSP = new SearchParameter(null, "Revisione", SearchParameter.RelationTypes.OneToOne, new SearchParameter[] { publishedSP, docLangSP });

            return dao.FindByCriteria(new SearchParameter[] { revisioneSP });
        }

        #endregion

    }
}