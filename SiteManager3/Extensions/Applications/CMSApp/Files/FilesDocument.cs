﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using NetCms.Structure.WebFS;
using System.Configuration;
using System.Web.Security;
using System.IO;
using System.Web.UI.WebControls.WebParts;
using System.Linq;
using NHibernate;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;

namespace NetCms.Structure.Applications.Files
{
    public class FilesDocument : NetCms.Structure.WebFS.Document
    {
        public override string FrontendLink
        {
            get 
            {               
                //Aggiunto il recupero tramite sessione interna per risolvere un problema di sessione mancante quando si richiama il metodo da web api
                //Verificare che resti invariato e funzioni il comportamento in altri casi.
                using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                {
                    FilesDocContent contenutoPubblicato = FilesBusinessLogic.GetPublishedLinkByDocLang(this.DocLangs.ElementAt(0).ID,sess);
                    if (contenutoPubblicato != null)
                    {
                        StructureFolder folder = FolderBusinessLogic.GetById(sess, this.Folder.ID);
                        return folder.FrontendHyperlink + contenutoPubblicato.Link; //nome.Rows[0]["Link_DocFiles"].ToString();
                    }
                    else return string.Empty;
                }
            }

        }

        public override string Icon
        {
            get { return "icon_file icon_" + this.Extension.ToLower(); }
        }
        
        public override string Link
        {
            get { return FullName; }
        }       

        public FilesDocument()
            : base()
        { }

        public FilesDocument(Networks.NetworkKey network)
            : base(network)
        {
        }

        public override HtmlGenericControl getRowView()
        {
            return getRowView(false);
        }

        public override HtmlGenericControl getRowView(bool SearchView)
        {
            //Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina di elenco Allegati del portale "+NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");

            HtmlGenericControl tr = new HtmlGenericControl("tr");

            #region Icona

            HtmlGenericControl td = new HtmlGenericControl("td");
            td.InnerHtml = "&nbsp;";
            td.Attributes["class"] = "FS_Icon " + Icon;
            tr.Controls.Add(td);

            #endregion

            #region Nome & Link

            string Link;

            if (!this.Grant)
                Link = this.Nome;
            else
                Link = BuildLink();

            td = new HtmlGenericControl("td");
            td.InnerHtml = Link;
            tr.Controls.Add(td);

            #endregion

            #region Cartella di apparteneza
            //*************************************
            if (SearchView)
            {
                td = new HtmlGenericControl("td");
                td.InnerHtml = (this.Folder as StructureFolder).LabelLinkPath;
                tr.Controls.Add(td);
            }
            //*************************************
            #endregion

            #region Data Oggetto
            //*************************************
            td = new HtmlGenericControl("td");
            td.InnerHtml = this.Date.ToString();
            if (td.InnerHtml.Length > 10) td.InnerHtml = td.InnerHtml.Remove(10);
            tr.Controls.Add(td);
            //*************************************
            #endregion

            #region Tipo

            td = new HtmlGenericControl("td");
            td.InnerHtml = "&nbsp;" + this.Extension;
            tr.Controls.Add(td);

            #endregion
            
            #region Stato
            //*************************************
            td = new HtmlGenericControl("td");
            td.InnerHtml = this.StatusLabel;
            tr.Controls.Add(td);
            //*************************************
            #endregion

            NetUtility.HtmlGenericControlCollection options = this.OptionsControls("files");
            if (options != null)
                foreach (HtmlGenericControl option in options)
                    tr.Controls.Add(option);           

            return tr;
        }

        public override DocContent ContentRecord
        {
            get
            {
                return null;
            }
        }

        private string _Extension;
        public override string Extension
        {
            get
            {
                if (_Extension == null)
                {
                    _Extension = "";
                    string[] arrayNomeFile = this.PhysicalName.Split('.');
                    if (arrayNomeFile[arrayNomeFile.Length - 1] != null)
                        _Extension = arrayNomeFile[arrayNomeFile.Length - 1];
                }
                return _Extension;
            }
        }

        private int _IsImage;
        public virtual bool IsImage
        {
            get
            {
                if (_IsImage == 0)
                {
                    _IsImage--;
                    string[] splitname = this.PhysicalName.Split('.');
                    if (splitname.Length > 0)
                    {
                        string ext = splitname[splitname.Length - 1];
                        if (ext == "gif" ||
                            ext == "png" ||
                            ext == "jpg")
                            _IsImage = 1;
                    }

                }
                return _IsImage == 1;
            }
        }        

        private string BuildLink()
        {
            //

            //string details = "<strong>" + this.Name + "</strong><br />"; VERIFICARE SE E' GIUSTO NAME, SOSTITUITO CON NOME
            //            string details = "<strong>" + this.Nome + "</strong><br />";
            //            details += "<p><em>" + this.Descrizione.Replace(@"
            //", " ") + "</em></p>";//ATTENZIONE L'accapo soprastante non è un errore di battitura ma una necessità in quanto rappresenta il carattere accapo


            //            string data = this.Date.ToString(); ;
            //            if (data.Length > 10) data = data.Remove(10);
            //            details += "Data Creazione: " + data + "<br />";

            //            details = details.Replace("'", "\\\'");
            //            details = details.Replace("\"", "&quot;");

            //            string image = "";

            //            if (NetCms.Users.AccountManager.CurrentAccount.GUI.PreviewType == NetCms.Users.SitemanagerUserGUI.PreviewTypes.Static)
            //                if (this.IsImage)
            //                    image = NetCms.GUI.GuiUtility.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Image);
            //                else
            //                    image = NetCms.GUI.GuiUtility.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Attach);

            //            string Link = "<a onmouseover=\"Over('" + image + "','" + details + "')\" onmouseout=\"Out()\"";
            string Link = "<a href=\"" + this.FrontendLink + "\">" + this.Nome + "</a>";

            return Link;
        }

        public virtual HtmlGenericControl PrintDocCriteria()
        {
            FilesDocument doc = this;
            HtmlGenericControl output = new HtmlGenericControl("div");
            /*HtmlGenericControl nome = new HtmlGenericControl("div");
            output.Controls.Add(nome);
            nome.InnerHtml = doc.Name;
            nome = new HtmlGenericControl("div");
            for (int i = 0; i < doc.Criteria.Count; i++)
            {
                nome.InnerHtml += doc.Criteria[i].Key + " - " + doc.Criteria[i].Value + "<br>";
            }
            output.Controls.Add(nome);*/
            return output;
        }

        public override bool MoveDocPhysical(string TargetFolderPath, PageData ExternalData)
        {
            string path = ExternalData.Paths.AbsoluteWebRoot + this.FullName;
            path = HttpContext.Current.Server.MapPath(path);
            System.IO.FileInfo pagefile = new FileInfo(path);
            if (pagefile.Exists)
            {
                path = ExternalData.Paths.AbsoluteWebRoot + TargetFolderPath + "/" + this.PhysicalName;
                path = HttpContext.Current.Server.MapPath(path);
                pagefile.MoveTo(path);
                return true;
            }
            else
                return false;
        }       
    }
}