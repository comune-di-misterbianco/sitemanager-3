﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Data;

namespace NetCms.Structure.Applications.Files
{
    public class FilesApplication : NetCms.Structure.Applications.Application
    {
        public const int ApplicationID = 4;
        public override string[] AllowedSubfolders
        {
            get { return null; }
        }
        
        public override bool EnableNewsLetter
        { get { return false; } }
        public override bool EnableMove
        { get { return true; } }
        public override bool EnableModule
        { get { return false; } }
        public override bool EnableReviews
        {
            get { return true; }
        }
        public override string Namespace
        { get { return "NetCms.Structure.Applications.Files"; } }
        public override string SystemName
        { get { return "files"; } }
        public override string Label
        { get { return "Allegati"; } }
        public override string LabelSingolare
        { get { return "Allegato"; } }
        public override int ID
        { get { return ApplicationID; } }
        public override bool IsSystemApplication { get { return true; } }

        public override NetCms.Structure.Applications.Setup Installer
        {
            get
            {
                if (_Installer == null)
                    _Installer = new NetCms.Structure.Applications.Files.Setup(this);
                return _Installer;
            }
        }

        private NetCms.Structure.Applications.Setup _Installer;
        
        public FilesApplication():base(null)
        {
            base.Assembly = Assembly.GetAssembly(this.GetType());
        }
        public FilesApplication(Assembly assembly)
            : base(assembly)
        {
        }
        /*
        protected override List<ApplicationValidation> LocalCheck()
        {
            List<ApplicationValidation> validationArray = new List<ApplicationValidation>();
            
            foreach (DataRow row in this.Installer.NetworksTable.Select(this.Installer.FilterID))
            {
                NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);
                if (network.CmsStatus != NetCms.Networks.NetworkCmsStates.NotCreated)
                {
                    DataTable check = Connection.SqlQuery("SHOW TABLES LIKE '" + network.SystemName + "_docs_files'");
                    if (check.Rows.Count != 1)
                        validationArray.Add(new ApplicationValidation("Tabella Documenti Files nella rete: '" + network.SystemName + "'", false, "Tabella dei documenti Files non presente per la rete: '" + network.SystemName + "'"));
                    else validationArray.Add(new ApplicationValidation("Tabella Documenti Files nella rete: '" + network.SystemName + "'", true, ""));
                }                
            }
            return validationArray;
        }

        protected override bool LocalRepair()
        {
            return true;
        }
    */
    }


}
