﻿using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS;
using NetCms.Structure.Applications.Files;
using System.Web.UI.WebControls;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System;
using NetCms.Front;
using Frontend.Entities;
using AutoMapper;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    public class FilesContentsHandler : ContentsHandler
    {

        private LabelsManager.Labels _FilesLabels;
        private LabelsManager.Labels FLabels
        {
            get
            {
                if (_FilesLabels == null)
                {
                    _FilesLabels = LabelsManager.LabelsCache.GetTypedLabels(typeof(FilesLabels)) as LabelsManager.Labels;
                }
                return _FilesLabels;
            }
        }

        private Document DocRecord;
        private FrontendFolder currentFolder;

        private XmlNode _FrontConfigs;
        private XmlNode FrontConfigs
        {
            get
            {
                if (_FrontConfigs == null)
                {

                    _FrontConfigs = this.PageData.XmlConfigs.SelectSingleNode("/Portal/configs/Networks/" + this.PageData.Network.SystemName + "/applications/files");
                }
                return _FrontConfigs;
            }
        }

        private string _FrontType;
        private string FrontType
        {
            get
            {
                if (_FrontType == null)
                {
                    _FrontType = "list";
                    XmlNode nodeConf = null;
                    if (FrontConfigs != null && FrontConfigs.ChildNodes.Count > 0)
                    {
                        nodeConf = FrontConfigs.SelectSingleNode("frontend");
                        if (nodeConf != null && nodeConf.Attributes["type"] != null)
                            _FrontType = nodeConf.Attributes["type"].Value;
                    }
                }
                return _FrontType;
            }
        }


        protected string ApplicationCSSClass
        {
            get { return "Files"; }
        }

        public FilesContentsHandler(PageData pagedata)
            : base(pagedata)
        {
            PageData = pagedata;
            currentFolder = PageData.Folder;

            if (PageData.Network.HasCurrentDocument)
                DocRecord = DocumentBusinessLogic.GetDocumentRecordById(PageData.Network.CurrentDocument.ID); //NetFrontend.DAL.FileSystemDAL.GetDocumentRecordByID(PageData.Network.CurrentDocument.ID);
        }

        public override Control PreviewControl
        {
            get
            {
                #region old
                //HtmlGenericControl div = new HtmlGenericControl("div");
                //div.Attributes["class"] = ApplicationCSSClass + currentFolder.CssClass;

                //div.InnerHtml = "<h1>" + currentFolder.Label + "</h1>";
                //div.InnerHtml += "<p>" + currentFolder.Descrizione + "</p>";

                //var files = FilesBusinessLogic.ListDocumentRecords(this.ContentID, this.PageData.Folder.ID); //NetFrontend.DAL.FilesDAL.ListDocumentRecords("id_DocFiles = " + this.ContentID + " AND Folder_Doc=" + this.PageData.Folder.ID);
                //div.InnerHtml += "<p><ul>";

                //foreach (FilesDocContent file in files)
                //{
                //    string[] ext = file.Link.Split('.');
                //    div.InnerHtml += "<li class=\"contentLink\"><a class=\"" + ext[ext.Length - 1] + "\" href=\"" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + currentFolder.Path + "/" + file.Link + "\"><span class=\"nomefile\">" + (file.Revisione != null ? file.Revisione.DocLang.Document.Nome : file.DocLang.Document.Nome) + "</span></a><br /><span class=\"descrizione\">" + (file.Revisione != null ? file.Revisione.DocLang.Document.Descrizione : file.DocLang.Document.Descrizione) + "</span></li>";
                //}

                //div.InnerHtml += "</ul></p>";
                //return div;
                #endregion

                WebControl divBlock = new WebControl(HtmlTextWriterTag.Div);
                divBlock.CssClass =  ApplicationCSSClass + "_" + currentFolder.CssClass;

                WebControl rowTitle = new WebControl(HtmlTextWriterTag.Div);
                rowTitle.CssClass = "row";

                WebControl colTitle = new WebControl(HtmlTextWriterTag.Div);
                colTitle.CssClass = "col-md-12";
                WebControl phblock = new WebControl(HtmlTextWriterTag.Div);
                phblock.CssClass = "page-header";

                WebControl title = new WebControl(HtmlTextWriterTag.H1);
                title.Controls.Add(new LiteralControl(currentFolder.Label));


                phblock.Controls.Add(title);

                WebControl descriptions = new WebControl(HtmlTextWriterTag.P);
                descriptions.Controls.Add(new LiteralControl(currentFolder.Descrizione));

                colTitle.Controls.Add(phblock);

                colTitle.Controls.Add(descriptions);


                rowTitle.Controls.Add(colTitle);

                divBlock.Controls.Add(rowTitle);

                WebControl divBlockLink = new WebControl(HtmlTextWriterTag.Div);
                divBlockLink.CssClass = "row";

                WebControl colBlockLink = new WebControl(HtmlTextWriterTag.Div);
                colBlockLink.CssClass = "col-md-12";

                WebControl divLinkGroup = new WebControl(HtmlTextWriterTag.Div);
                divLinkGroup.CssClass = "list-group";

                var files = FilesBusinessLogic.ListDocumentRecords(this.ContentID, this.PageData.Folder.ID);
                foreach (FilesDocContent file in files)
                {

                    string[] ext = file.Link.Split('.');

                    WebControl a = new WebControl(HtmlTextWriterTag.A);
                    a.Attributes.Add("href", NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + currentFolder.Path + "/" + file.Link);
                    a.CssClass = "list-group-item" + ext[ext.Length - 1];

                    WebControl titleLink = new WebControl(HtmlTextWriterTag.H4);
                    titleLink.CssClass = "list-group-item-heading";
                    titleLink.Controls.Add(new LiteralControl(file.Revisione != null ? file.Revisione.DocLang.Document.Nome : file.DocLang.Document.Nome));

                    a.Controls.Add(titleLink);

                    WebControl DescriptionLink = new WebControl(HtmlTextWriterTag.P);
                    DescriptionLink.CssClass = "list-group-item-text";
                    DescriptionLink.Controls.Add(new LiteralControl(file.Revisione != null ? file.Revisione.DocLang.Document.Descrizione : file.DocLang.Document.Descrizione));

                    a.Controls.Add(DescriptionLink);


                    divLinkGroup.Controls.Add(a);
                }

                colBlockLink.Controls.Add(divLinkGroup);
                divBlockLink.Controls.Add(colBlockLink);


                divBlock.Controls.Add(divBlockLink);


                return divBlock;
            }
        }

        //public override Control Control
        //{
        //    get
        //    {

        //        #region old
        //        //HtmlGenericControl div = new HtmlGenericControl("div");

        //        //if (currentFolder.StateValue < 2)
        //        //{                    
        //        //    div.Attributes["class"] = ApplicationCSSClass + currentFolder.CssClass;

        //        //    div.InnerHtml = "<h1>" + currentFolder.Label + "</h1>";
        //        //    div.InnerHtml += "<p>" + currentFolder.Descrizione + "</p>";

        //        //    var files = FilesBusinessLogic.ListDocumentRecordsPublishedByFolder(this.PageData.Folder.ID); //NetFrontend.DAL.FilesDAL.ListDocumentRecords("Published_Revisione=1 AND Folder_Doc=" + this.PageData.Folder.ID);
        //        //    div.InnerHtml += "<div class=\"files_list\">"
        //        //                   + "<ul>";

        //        //    foreach (FilesDocContent file in files)
        //        //    {
        //        //        string[] ext = file.Link.Split('.');
        //        //        div.InnerHtml += "<li class=\"contentLink\"><a class=\"" + ext[ext.Length - 1] + "\" href=\"" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + currentFolder.Path + "/" + file.Link + "\"><span class=\"nomefile\">" + (file.Revisione != null ? file.Revisione.DocLang.Document.Nome : file.DocLang.Document.Nome) + "</span></a><br /><span class=\"descrizione\">" + (file.Revisione != null ? file.Revisione.DocLang.Document.Descrizione : file.DocLang.Document.Descrizione) + "</span></li>";
        //        //    }

        //        //    div.InnerHtml += "</ul>"
        //        //                   + "</div>";
        //        //}
        //        //else 
        //        //{                   
        //        //    throw new NetCms.Exceptions.PageNotFound404Exception(HttpContext.Current.Request.Url.ToString());                  
        //        //}
        //        //return div;

        //        #endregion
        //        WebControl divBlock = new WebControl(HtmlTextWriterTag.Div);
        //        divBlock.CssClass = " " + ApplicationCSSClass + "_" + currentFolder.CssClass;

        //        if (currentFolder.StateValue < 2)
        //        {
        //            WebControl rowTitle = new WebControl(HtmlTextWriterTag.Div);
        //            rowTitle.CssClass = "row";

        //            WebControl colTitle = new WebControl(HtmlTextWriterTag.Div);
        //            colTitle.CssClass = "col-md-12";

        //            WebControl phblock = new WebControl(HtmlTextWriterTag.Div);
        //            phblock.CssClass = "page-header";

        //            WebControl title = new WebControl(HtmlTextWriterTag.H1);
        //            title.Controls.Add(new LiteralControl(currentFolder.Label));

        //            phblock.Controls.Add(title);

        //            WebControl descriptions = new WebControl(HtmlTextWriterTag.P);
        //            descriptions.Controls.Add(new LiteralControl(currentFolder.Descrizione));

        //            colTitle.Controls.Add(phblock);

        //            colTitle.Controls.Add(descriptions);

        //            rowTitle.Controls.Add(colTitle);

        //            divBlock.Controls.Add(rowTitle);

        //            WebControl divBlockLink = new WebControl(HtmlTextWriterTag.Div);
        //            divBlockLink.CssClass = "row";

        //            WebControl colBlockLink = new WebControl(HtmlTextWriterTag.Div);
        //            colBlockLink.CssClass = "col-md-12";

        //            WebControl divLinkGroup = new WebControl(HtmlTextWriterTag.Div);
        //            divLinkGroup.CssClass = "list-group";

        //            var files = FilesBusinessLogic.ListDocumentRecordsPublishedByFolder(this.PageData.Folder.ID);

        //            foreach (FilesDocContent file in files)
        //            {
        //                string[] ext = file.Link.Split('.');

        //                WebControl a = new WebControl(HtmlTextWriterTag.A);
        //                a.Attributes.Add("href", NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + currentFolder.Path + "/" + file.Link);
        //                a.CssClass = "list-group-item " + ext[ext.Length - 1];

        //                WebControl titleLink = new WebControl(HtmlTextWriterTag.H4);
        //                titleLink.CssClass = "list-group-item-heading";
        //                titleLink.Controls.Add(new LiteralControl(file.Revisione != null ? file.Revisione.DocLang.Document.Nome : file.DocLang.Document.Nome));

        //                a.Controls.Add(titleLink);

        //                WebControl DescriptionLink = new WebControl(HtmlTextWriterTag.P);
        //                DescriptionLink.CssClass = "list-group-item-text";
        //                DescriptionLink.Controls.Add(new LiteralControl(file.Revisione != null ? file.Revisione.DocLang.Document.Descrizione : file.DocLang.Document.Descrizione));

        //                a.Controls.Add(DescriptionLink);

        //                divLinkGroup.Controls.Add(a);

        //            }
        //            colBlockLink.Controls.Add(divLinkGroup);
        //            divBlockLink.Controls.Add(colBlockLink);


        //            divBlock.Controls.Add(divBlockLink);

        //        }
        //        else
        //        {
        //            throw new NetCms.Exceptions.PageNotFound404Exception(HttpContext.Current.Request.Url.ToString());

        //        }
        //        return divBlock;
        //    }
        //}


        private Dictionary<string, string> _TemplateLabels;
        public Dictionary<string, string> TemplateLabels
        {
            get
            {
                if (_TemplateLabels == null)
                {
                    _TemplateLabels = new Dictionary<string, string>();
                    foreach (FilesLabels.FilesLabelsList label in Enum.GetValues(typeof(FilesLabels.FilesLabelsList)))
                        _TemplateLabels.Add(label.ToString().ToLower(), FLabels[label].ToString());
                }
                return _TemplateLabels;
            }
        }

        public WebControl GetFilesList()
        {
            WebControl divBlock = new WebControl(HtmlTextWriterTag.Div);
            divBlock.CssClass = " " + ApplicationCSSClass + "_" + currentFolder.CssClass;

            WebControl rowTitle = new WebControl(HtmlTextWriterTag.Div);
            rowTitle.CssClass = "row";

            WebControl colTitle = new WebControl(HtmlTextWriterTag.Div);
            colTitle.CssClass = "col-md-12";

            WebControl phblock = new WebControl(HtmlTextWriterTag.Div);
            phblock.CssClass = "page-header";

            WebControl title = new WebControl(HtmlTextWriterTag.H1);
            title.Controls.Add(new LiteralControl(currentFolder.Label));

            phblock.Controls.Add(title);

            WebControl descriptions = new WebControl(HtmlTextWriterTag.P);
            descriptions.Controls.Add(new LiteralControl(currentFolder.Descrizione));

            colTitle.Controls.Add(phblock);

            colTitle.Controls.Add(descriptions);

            rowTitle.Controls.Add(colTitle);

            divBlock.Controls.Add(rowTitle);

            WebControl divBlockLink = new WebControl(HtmlTextWriterTag.Div);
            divBlockLink.CssClass = "row";

            WebControl colBlockLink = new WebControl(HtmlTextWriterTag.Div);
            colBlockLink.CssClass = "col-md-12";

            WebControl divLinkGroup = new WebControl(HtmlTextWriterTag.Div);
            divLinkGroup.CssClass = "list-group";

            var files = FilesBusinessLogic.ListDocumentRecordsPublishedByFolder(this.PageData.Folder.ID);

            foreach (FilesDocContent file in files)
            {
                string[] ext = file.Link.Split('.');

                WebControl a = new WebControl(HtmlTextWriterTag.A);
                a.Attributes.Add("href", NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + currentFolder.Path + "/" + file.Link);
                a.CssClass = "list-group-item " + ext[ext.Length - 1];

                WebControl titleLink = new WebControl(HtmlTextWriterTag.H4);
                titleLink.CssClass = "list-group-item-heading";
                titleLink.Controls.Add(new LiteralControl(file.Revisione != null ? file.Revisione.DocLang.Document.Nome : file.DocLang.Document.Nome));

                a.Controls.Add(titleLink);

                WebControl DescriptionLink = new WebControl(HtmlTextWriterTag.P);
                DescriptionLink.CssClass = "list-group-item-text";
                DescriptionLink.Controls.Add(new LiteralControl(file.Revisione != null ? file.Revisione.DocLang.Document.Descrizione : file.DocLang.Document.Descrizione));

                a.Controls.Add(DescriptionLink);

                divLinkGroup.Controls.Add(a);

            }
            colBlockLink.Controls.Add(divLinkGroup);
            divBlockLink.Controls.Add(colBlockLink);


            divBlock.Controls.Add(divBlockLink);

            return divBlock;
        }

        public Control GetTemplateFilesList()
        {
            WebControl page = new WebControl(HtmlTextWriterTag.Div);
            page.CssClass = "wrapper";


            IEnumerable<FolderDTO> folders = null;
            string sectionmenu = currentFolder.ShowMenu.ToString();

            if (currentFolder.ShowMenu)
            {
                IList<FrontendFolder> frontendFolders = PageData.Folder.SubFolders.Where(x => x.Type == 4 && (x.StateValue == 0 || x.StateValue == 4)).OrderByDescending(y => y.Order).ToList();

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<FrontendFolder, FolderDTO>()
                    .ForMember(x => x.ID, o => o.MapFrom(y => y.ID))
                    .ForMember(x => x.Titolo, o => o.MapFrom(y => y.Label))
                    .ForMember(x => x.Descrizione, o => o.MapFrom(y => y.Descrizione))
                    .ForMember(x => x.Url, o => o.MapFrom(y => y.FrontendUrl))
                    .ForMember(x => x.Riffapp, o => o.MapFrom(y => y.Type))
                    .ForMember(x => x.Childfolder, o => o.MapFrom(y => y.SubFolders.Where(x => x.Type == 4 && (x.StateValue == 0 || x.StateValue == 4)).OrderByDescending(k => k.Order)))
                    .ForMember(x => x.Order, o => o.MapFrom(y => y.Order))
                    .ForMember(x => x.Documents, o => o.MapFrom(y => y.Documents.Where(q => q.Name != "default.aspx").OrderByDescending(q=>q.Order)));
                    
                    cfg.CreateMap<FrontendDocument, DocumentDTO>()
                           .ForMember(x => x.ID, o => o.MapFrom(y => y.ID))
                           .ForMember(x => x.Titolo, o => o.MapFrom(y => y.Label))
                           .ForMember(x => x.Descrizione, o => o.MapFrom(y => y.Descrizione))
                           .ForMember(x => x.Url, o => o.MapFrom(y => y.FrontendUrl));
                });
                
                config.AssertConfigurationIsValid();

                var mapper = config.CreateMapper();

                folders = mapper.Map<IEnumerable<FrontendFolder>, IEnumerable<FolderDTO>>(frontendFolders);
            }

            IList<FilesDocContent> files = FilesBusinessLogic.ListDocumentRecordsPublishedByFolder(this.PageData.Folder.ID);

            var documents = (from file in files                                                         
                             select new 
                            {
                             titolo = file.DocLang.Label,
                             descrizione = file.DocLang.Descrizione,
                             url = file.Link,
                             itemtype = "file"
                            });

            IList<NetCms.Structure.Applications.Links.LinksDocContent> links = NetCms.Structure.Applications.Links.LinksBusinessLogic.FindLinksInFolders(new List<int> { this.PageData.Folder.ID }, "", false);

            var items = (from link in links
                         select new
                         {
                             titolo = link.DocLang.Label,
                             descrizione = link.DocLang.Descrizione,
                             url = link.Href.Replace("<%= WebRoot %>", ""),
                             itemtype = ((link.Href.Contains("http") || link.Href.Contains("https")) ? "extlink" : "link")
                        });

            if (items != null && items.Any())
                documents = documents.Concat(items);

            var sezione = new
            {
                titolo = this.currentFolder.Label,
                descrizione = this.currentFolder.Descrizione,
                url = this.currentFolder.FrontendUrl,
                menu = sectionmenu,
                folders = (folders != null && folders.Any() ? folders : null), //folders.OrderByDescending(x => x.Order)
                pagename = "default.aspx"                
            };

            //string tpl_path = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + PageData.CurrentConfig.CurrentTheme.TemplateFolder + "content/files/list.tpl");
            string tpl_path = base.GetContentTemplatePath("list", "files");
            var result = NetCms.Themes.TemplateEngine.RenderTpl(documents, sezione, tpl_path, TemplateLabels, TemplateCommonLabels);

            page.Controls.Add(new LiteralControl(result));

            #region MyRegion
            //var template = Scriban.Template.Parse(System.IO.File.ReadAllText(tpl_path), tpl_path);
            //try
            //{
            //    if (template.HasErrors)
            //    {
            //        if (NetCms.Configurations.Debug.DebugLoginEnabled)
            //            foreach (var error in template.Messages)
            //                page.Controls.Add(new LiteralControl(error.Message + System.Environment.NewLine));
            //        else
            //            foreach (var error in template.Messages)
            //                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, error.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            //    }
            //    else
            //    {                    
            //        var context = new Scriban.TemplateContext();
            //        context.PushCulture(System.Globalization.CultureInfo.CurrentCulture);

            //        var scriptObj = new Scriban.Runtime.ScriptObject();
            //        scriptObj.Add("documents", documents);
            //        scriptObj.Add("sezione", sezione);
            //        scriptObj.Add("labels", TemplateLabels);
            //        scriptObj.Add("commonlabels", TemplateCommonLabels);

            //        context.PushGlobal(scriptObj);

            //        var result = template.Render(context);

            //        page.Controls.Add(new LiteralControl(result));
            //    }
            //}
            //catch (Exception ex)
            //{
            //    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Template parsing error", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "", ex);
            //} 
            #endregion

            return page;
        }


        public override Control Control
        {
            get
            {
                if (currentFolder.StateValue < 2)
                {
                    // TODO: rimodulare la selezione del FrontType - spostando il settaggio dal file config.xml all'applicazione di cms (DB) considerando anche di diversificarlo per network
                    return (this.FrontType == "template" ? GetTemplateFilesList() : GetFilesList());
                }
                else
                {
                    throw new NetCms.Exceptions.PageNotFound404Exception(HttpContext.Current.Request.Url.ToString());

                }
            }
        }
    }
}