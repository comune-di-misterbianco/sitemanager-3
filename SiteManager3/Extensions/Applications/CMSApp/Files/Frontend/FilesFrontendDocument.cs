using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Xml;
using System.Text;
using NetCms.Networks.WebFS;
using NetFrontend;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;


/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Structure.Applications.Files
{
    [FrontendDocumentsDefiner(4, "Allegati")]
    public class FilesFrontendDocument : FrontendDocument
    {
        public override string FrontendUrl
        {
            get
            {

                FilesDocument doc = DocumentBusinessLogic.GetById(this.ID) as FilesDocument; //NetFrontend.DAL.FilesDAL.GetDocumentRecord(this.ID);
                if (doc != null && doc.ContentRecord != null)
                    return this.Folder.FrontendUrl + "/" + (doc.ContentRecord as FilesDocContent).Link;//["Link_DocFiles"].ToString();
                else if (!string.IsNullOrEmpty(doc.Link))
                    return doc.Link;
                else return string.Empty;
            }
        }

        public FilesFrontendDocument(Document doc, FrontendFolder folder):base(doc, folder) {}
        public FilesFrontendDocument(int docID, FrontendFolder folder) : base(docID, folder) { }
    }
}