﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using NetTable;
using NetCms.Grants;


namespace NetCms.Structure.Applications.Files
{
    public class FilesOverview : NetCms.Structure.WebFS.DocumentOverview
    {
        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Bell; }
        }

        public FilesOverview()
        {
        }
        protected override string PreviewJS
        {
            get
            {
                return "javascript: Preview('" + this.CurrentFolder.FrontendHyperlink + "',{0},'" + this.CurrentFolder.Path + "')";
            }
        }
        public override Control getView()
        {
            if (this.CurrentFolder.Tipo != this.CurrentFolder.RelativeApplication.ID ||
               this.CurrentFolder.Criteria[CriteriaTypes.Modify].Value == GrantsValues.Deny ||
               this.CurrentFolder == this.CurrentFolder.StructureNetwork.RootFolder
               )
            {
                return this.InvalidGrantsControl;
            }
            return base.getView();
        }

        //public override void PostBackPublica()
        //{
        //    base.PostBackPublica();

        //    string idrev = HttpContext.Current.Request.Form["publish"];
        //    string sqlRev = "SELECT * FROM revisioni WHERE id_Revisione=" + idrev;
        //    DataTable revisione = this.Connection.SqlQuery(sqlRev);
        //    string idDoc = revisione.Rows[0]["Contenuto_Revisione"].ToString();
        //    string sqlDoc = "SELECT * FROM docs WHERE id_Doc =" + idDoc;
        //    DataTable documento = this.Connection.SqlQuery(sqlDoc);


        //    string sql = "UPDATE Docs_Lang ";
        //    sql += "SET Doc_DocLang=" + idDoc + ", Label_DocLang='" + documento.Rows[0]["Name_Doc"].ToString() + "'";
        //    sql += " WHERE id_DocLang=" + this.CurrentDocument.Lang;
        //    this.Connection.Execute(sql);

        //    this.CurrentFolder.Invalidate();
        //}
    }
}
