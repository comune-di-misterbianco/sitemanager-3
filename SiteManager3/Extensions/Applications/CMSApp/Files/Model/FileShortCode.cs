﻿using NetCms.CmsApp.ShortCodes;
using NetCms.Structure.Applications.Files;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace Files.Model
{
    public class FileShortCode : ShortCode
    {
        public FileShortCode()
        {
        }
        public FileShortCode(string rawValue, string appRif, int rifObj, EnumObjectType objectType, NetCms.Themes.Model.Theme currentTheme, int itemLimit = -1) : base (rawValue,appRif,rifObj,objectType, currentTheme, itemLimit)
        {

        }

        public override string GetHtml()
        {
            string source = string.Empty;

            string tpl_path = base.GetTemplatePath();// System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + "/templates/content/files/shortcode.tpl");

            var template = Scriban.Template.Parse(System.IO.File.ReadAllText(tpl_path), tpl_path);
            try
            {
                if (template.HasErrors)
                {
                    if (NetCms.Configurations.Debug.DebugLoginEnabled)
                        foreach (var error in template.Messages)
                            source += error.Message + System.Environment.NewLine;
                    else
                        foreach (var error in template.Messages)
                            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, error.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                }
                else
                {

                    // var result = template.Render(new { documents = documents, sezione = sezione, labels = TemplateLabels, pager = paging });

                    var context = new Scriban.TemplateContext();
                    context.PushCulture(System.Globalization.CultureInfo.CurrentCulture);

                    var scriptObj = new Scriban.Runtime.ScriptObject();
                    scriptObj.Add("items", GetItems());
                    scriptObj.Add("section", Section);
                    context.PushGlobal(scriptObj);

                    source += template.Render(context);
                    
                }
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Template parsing error", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "", ex);
            }

            return source;
        }

       
        private object GetItems()
        {
          object documents = null;
          if (base.ObjectType == EnumObjectType.folder)
          {
             int id = base.RifObj;
             // ritorna tutti i file pubblicati nella folder con id RifObj
             IList<FilesDocContent> files = FilesBusinessLogic.ListDocumentRecordsPublishedByFolder(base.RifObj);

                if (base.ItemLimit != -1)
                    files = files.Take(ItemLimit).ToList();

                documents = (from file in files
                              select new
                              {
                                titolo = file.DocLang.Label,
                                descrizione = file.DocLang.Descrizione,
                                url = file.Revisione.Document.FrontendLink,
                              });
          }
            return documents;

        }

        private object _Section;
        private object Section
        {
            get
            {
                if (_Section == null) {

                    NetCms.Structure.WebFS.StructureFolder currentFolder = NetCms.Structure.WebFS.FileSystemBusinessLogic.FolderBusinessLogic.GetById(base.RifObj);
                    _Section = new { titolo = currentFolder.Label, descrizione = currentFolder.Descrizione, link = currentFolder.FrontendHyperlink, itemlimit = base.ItemLimit };

                }
                return _Section;
            }
        }
    }
}
