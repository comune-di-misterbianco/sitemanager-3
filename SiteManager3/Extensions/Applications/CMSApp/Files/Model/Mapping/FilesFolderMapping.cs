﻿using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.Files.Mapping
{
    public class FilesFolderMapping : SubclassMap<FilesFolder>
    {
        public FilesFolderMapping()
        {
            Table("files_folders");
            KeyColumn("id_folders");
        }
    }
}
