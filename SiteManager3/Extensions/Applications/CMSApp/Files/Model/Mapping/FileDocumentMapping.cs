﻿using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.Files.Mapping
{
    public class FilesDocumentMapping : SubclassMap<FilesDocument>
    {
        public FilesDocumentMapping()
        {
            Table("files_docs");
            KeyColumn("id_docs");
        }
    }
}
