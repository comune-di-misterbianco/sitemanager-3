﻿using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.Files.Mapping
{
    public class FilesDocContentMapping : SubclassMap<FilesDocContent>
    {
        public FilesDocContentMapping()
        {
            Table("files_docs_contents");
            KeyColumn("id_DocFiles");
            Map(x => x.Link).Column("Link_DocFiles").CustomSqlType("mediumtext");
            Map(x => x.ContentType).Column("ContentType_DocFiles");
            Map(x => x.Extension).Column("Extension_DocFiles");
            Map(x => x.Size).Column("Size_DocFiles");

        }
    }
}