﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Structure.WebFS;
using System.Data;
using NHibernate;
using NetCms.Networks;
using NetCms.Structure.Search.Models;

namespace NetCms.Structure.Applications.Files
{
    public class FilesDocContent : DocContent
    {
        public FilesDocContent()
            : base()
        { 
        
        }

        public virtual string Link
        {
            get;
            set;
        }

        public virtual string Extension
        {
            get;
            set;
        }

        public virtual int Size
        {
            get;
            set;
        }

        public virtual string ContentType
        {
            get;
            set;
        }


        public override DocContent ImportDocContent(DataRow docRecord, ISession session, DataSet sourceDB, NetworkKey netKey, string sourceFolderPath, ref Document newDoc)
        {
            if (docRecord.Table.Columns.Contains("Link_DocFiles"))
                Link = docRecord["Link_DocFiles"].ToString();
            else
                Link = docRecord["Name_Doc"].ToString();
            return this;
        }

        public override DocContent ImportDocContent(Dictionary<string, string> docRecord, ISession session)
        {
            throw new NotImplementedException();
        }


        public override bool EnableIndexContent
        {
            get
            {
                return true;
            }
        }

        public override CmsConfigs.Model.IndexDocContent GetContentToIndex(ISession session)
        {
            string frontUrl = (string.IsNullOrEmpty(this.DocLang.Document.Network.Paths.AbsoluteFrontRoot) ? "" : this.DocLang.Document.Network.Paths.AbsoluteFrontRoot)
                           + this.DocLang.Document.Folder.Path + "/"
                           + this.DocLang.Document.PhysicalName;

            CmsConfigs.Model.IndexDocContent indexContent = new CmsConfigs.Model.IndexDocContent();

            indexContent.ID = this.ID;
            indexContent.Fields.Add(CmsDocument.Fields.document_title.ToString(), this.DocLang.Label);
            indexContent.Fields.Add(CmsDocument.Fields.document_description.ToString(), this.DocLang.Descrizione);
            indexContent.Fields.Add(CmsDocument.Fields.document_content.ToString(), this.DocLang.Document.PhysicalName);
            indexContent.Fields.Add(CmsDocument.Fields.document_fronturl.ToString(), frontUrl);
            indexContent.Fields.Add(CmsDocument.Fields.document_backurl.ToString(), frontUrl);
            indexContent.Fields.Add(CmsDocument.Fields.document_type.ToString(), this.DocLang.Document.Application.ToString());

            return indexContent;

        }
    }
}
