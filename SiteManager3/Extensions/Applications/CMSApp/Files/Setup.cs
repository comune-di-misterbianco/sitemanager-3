﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using System.Reflection;
using NetCms.DBSessionProvider;
using System.Linq;
using NHibernate;

namespace NetCms.Structure.Applications.Files
{
    public class Setup : NetCms.Structure.Applications.Setup
    {
       
//        private string[] AddTablesSql = {@"
//CREATE TABLE IF NOT EXISTS `{0}_docs_files` (
//  `id_DocFiles` INTEGER(11) NOT NULL AUTO_INCREMENT,
//  `Link_DocFiles` LONGTEXT,
//  PRIMARY KEY (`id_DocFiles`)
//)ENGINE=InnoDB DEFAULT CHARSET=latin1;
//"};

        public Setup(NetCms.Structure.Applications.Application baseApp)
            : base(baseApp)
        {
        }

        public override string[,] Pages
        {
            get
            {
                string[,] pages = {
                                  { "files_new.aspx", "NetCms.Structure.Applications.Files.Pages.FilesAdd" },
                                  { "transfer.aspx", "NetCms.Structure.Applications.Files.Pages.FilesPopupAdd" },
                                  { "multi_upload.aspx", "NetCms.Structure.Applications.Files.Pages.MultiUpload"},
                                  };
                return pages;
            }
        }

        protected override bool LocalInstall(bool isInsidePackage = false)
        {
            bool done = true;
            try
            {
                FluentSessionProvider.Instance.UpdateSessionFactoryAdd(Assembly.GetAssembly(this.GetType()));
            }
            catch (Exception ex)
            {
                this.RollBack();
                done = false;
            }
            return done;
        }

        //private bool AddTables()
        //{
        //    bool done = false;
        //    try
        //    {
        //        foreach (DataRow row in this.NetworksTable.Select(FilterID))
        //        {
        //            foreach (string sqlModel in this.AddTablesSql)
        //            {
        //                NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);
        //                string sql = string.Format(sqlModel, network.SystemName);
        //                done = Connection.Execute(sql);
        //                if (done == false) throw new Exception();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //Trace
        //        /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
        //        ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);
        //        this.AddErrorString("Non è stato possibile creare una o più tabelle dell'Applicazione nel database.");*/
        //        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile creare una o più tabelle dell'Applicazione Files nel database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "14");
        //    }
        //    return done;
        //}

        /*
        private bool AddRecords()
        {
            bool done = false;
            try
            {
                string sql = string.Format(NetCms.Structure.Applications.Setup.AddApplicationSql, this.ApplicationBaseData.ID, this.ApplicationBaseData.SystemName, this.ApplicationBaseData.Label, this.ApplicationBaseData.LabelSingolare, this.ApplicationBaseData.EnableReviews, 1, this.ApplicationBaseData.EnableMove, this.ApplicationBaseData.EnableNewsLetter, this.ApplicationBaseData.EnableModule);
                done = Connection.Execute(sql);

                if (done)
                {
                    sql = string.Format(NetCms.Structure.Applications.Setup.AddApplicationPageSql, "files_new.aspx", "NetCms.Structure.Applications.Files.Pages.FilesAdd", this.ApplicationBaseData.ID);
                    done = Connection.Execute(sql);
                    if (!done)
                        this.AddErrorString("Non è stato possibile creare il record della pagina " + "files_new.aspx" + ".");
                }
                else
                    this.AddErrorString("Non è stato possibile creare il record dell'Applicazione, verificare se esiste già nella tabella 'Applicazioni' del database.");

            }
            catch (Exception ex)
            {
                //Trace
                this.AddErrorString("Non è stato possibile creare il record dell'Applicazione, verificare se esiste già nella tabella 'Applicazioni' del database.");
                NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);
            }

            return done;
        }*/
        

        private void RollBack()
        {
            bool done = false;
            if (!(NetworkID > 0))
            {
                using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = session.BeginTransaction())
                {
                    try
                    {
                        Application dbApp = ApplicationBusinessLogic.GetApplicationById(this.ApplicationBaseData.ID, session);
                        ApplicationBusinessLogic.DeleteApplication(dbApp, session);
                        tx.Commit();
                        done = true;
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                    }
                }

                if (done == false)
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile rimuovere l'Applicazione Files dal database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "15");

            }
        }
        public override void Uninstall()
        {
            base.Uninstall();
            FluentSessionProvider.Instance.UpdateSessionFactoryRemove(Assembly.GetAssembly(this.GetType()));
        }
    }
}
