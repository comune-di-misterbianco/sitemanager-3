﻿using System;
using System.IO;
using System.Xml;
using System.Web;
using System.Data;
using NetCms.Networks;
using NetCms.Networks.WebFS;
using NetCms.Exporter;

namespace NetCms.Structure.Applications.Files
{
    public class FilesExporter:ApplicationExporter
    {
        private DataTable _DocsTable;
        public override DataTable DocsTable
        {
            get
            {
                if (_DocsTable == null)
                {
                    _DocsTable = new DataTable();
                    //this.FolderExporter.Exporter.Connection.FillDataTable(string.Format(ApplicationExporter.Network_Docs, "INNER JOIN revisioni ON DocLang_Revisione=id_DocLang INNER JOIN docs_files ON id_DocFiles=Contenuto_Revisione", this.FolderID, "Published_Revisione=1 AND"), _DocsTable);
                    this.FolderExporter.Exporter.Conn.FillDataTable(string.Format(ApplicationExporter.Network_Docs, "INNER JOIN revisioni ON DocLang_Revisione=id_DocLang INNER JOIN files_docs_contents ON id_DocFiles=Contenuto_Revisione", this.FolderID, "",FilesApplication.ApplicationID.ToString()), _DocsTable);
                    _DocsTable.TableName = "docs";
                }
                return _DocsTable;
            }
        }
        public override DataTable[] Tables 
        {
            get
            {
                return null;
            }
        }

        public override bool HasFiles { get { return false; } }


        public FilesExporter(string folderID, FolderExporter folderExporter)
            : base(folderID, folderExporter)
        {
        }
    }
}
