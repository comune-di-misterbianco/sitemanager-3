﻿//using System;
//using System.Data;
//using System.Configuration;
//using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
//using NetForms;

///// <summary>
///// Summary description for NewsFolder_NEW_
///// </summary>
//namespace NetCms.Structure.Applications.WebDocs
//{
//    public partial class WebDocsArticoloNewsLetter : NetCms.Newsletter2.Admin.ApplicationArticoloNewsLetter
//    {
//        public override int Tipo
//        {
//            get
//            {
//                return NetCms.Structure.Applications.WebDocs.WebDocsApplication.ApplicationID;
//            }
//        }

//        private string _Url;
//        public override string Url
//        {
//            get
//            {
//                if (_Url == null && Record != null)
//                {
//                    _Url = "http://" +
//                         HttpContext.Current.Request.Url.DnsSafeHost +
//                        this.CurrentNetwork.Paths.AbsoluteFrontRoot +
//                        Record["Path_Folder"] + "/default.aspx?Eventi=" + Record["id_Doc"];
//                }
//                return _Url;
//            }
//        }

//        public WebDocsArticoloNewsLetter(int id)
//            : base(id)
//        {
//        }

//        private string htmlSource;
//        public override string HtmlSource
//        {
//            get
//            {
//                if (htmlSource == null)
//                {
//                    htmlSource = "<div class=\"articolo\">";

//                    htmlSource += "<h4>" + base.Titolo + "</h4>";
//                    htmlSource += "<p>" + base.Descrizione + "</p>";
//                    if (Url != null && Url.Length > 0)
//                    {
//                        htmlSource += "<p class=\"link\"><a href=\"" + Url + "\" title=\"" + base.Titolo + "\">Leggi l'articolo completo</a></p>";
//                    }
//                    htmlSource += "</div>";
//                }
//                return htmlSource;
//            }
//        }
//    }
//}