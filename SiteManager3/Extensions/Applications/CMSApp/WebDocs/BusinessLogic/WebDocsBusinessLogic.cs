﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using GenericDAO.DAO;
using NHibernate.Transform;
using NHibernate.Criterion;
using NetService.Utility.RecordsFinder;
using NetCms.DBSessionProvider;

namespace NetCms.Structure.Applications.WebDocs
{
    public static class WebDocsBusinessLogic
    {

        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static IStatelessSession GetStatelessSession()
        {
            return FluentSessionProvider.Instance.OpenStatelessSession();
        }

        public static WebDocsDocContent GetById(int id)
        {
            IGenericDAO<WebDocsDocContent> dao = new CriteriaNhibernateDAO<WebDocsDocContent>(GetCurrentSession());
            return dao.GetById(id);
        }

        public static WebDocsDocContent GetById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<WebDocsDocContent> dao = new CriteriaNhibernateDAO<WebDocsDocContent>(innerSession);
            return dao.GetById(id);
        }

        public static IList<WebDocsDocContent> FindAllApplicationRecords()
        {
            IGenericDAO<WebDocsDocContent> dao = new CriteriaNhibernateDAO<WebDocsDocContent>(GetCurrentSession());
            return dao.FindAll();
        }

        public static WebDocsDocContent SaveOrUpdateWebDocsContent(WebDocsDocContent content, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<WebDocsDocContent> dao = new CriteriaNhibernateDAO<WebDocsDocContent>(innerSession);
            return dao.SaveOrUpdate(content);
        }

        
        #region Metodi di Frontend

        public static WebDocsDocument GetDocumentPreviewRecord(int idWebDoc, int idFolder)
        {
            IGenericDAO<WebDocsDocument> dao = new CriteriaNhibernateDAO<WebDocsDocument>(GetCurrentSession());

            SearchParameter applicationSP = new SearchParameter(null,"Application",2, Finder.ComparisonCriteria.Equals,false);

            //SearchParameter statoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.Not, false);

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter folderSP = new SearchParameter(null,"Folder.ID",idFolder, Finder.ComparisonCriteria.Equals,false);

            SearchParameter idSP = new SearchParameter(null, "ID", idWebDoc, Finder.ComparisonCriteria.Equals, false);
            SearchParameter ContentSP = new SearchParameter(null,"Content", SearchParameter.RelationTypes.OneToOne,idSP);
            SearchParameter RevisioneSP = new SearchParameter(null,"Revisioni", SearchParameter.RelationTypes.OneToMany,ContentSP);
            SearchParameter docsLangSP = new SearchParameter(null, "DocLangs", SearchParameter.RelationTypes.OneToMany, RevisioneSP);
            //statoSP,
            return dao.GetByCriteria(new SearchParameter[] { applicationSP, trashSP, folderSP, docsLangSP });

        }


        public static WebDocsDocument GetDocumentRecord(int idDoc)
        {
            IGenericDAO<WebDocsDocument> dao = new CriteriaNhibernateDAO<WebDocsDocument>(GetCurrentSession());

            SearchParameter applicationSP = new SearchParameter(null, "Application", 2, Finder.ComparisonCriteria.Equals, false);

            SearchParameter statoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.Not, false);

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter idSP = new SearchParameter(null, "ID", idDoc, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { applicationSP, statoSP, trashSP, idSP });

        }

        #endregion


        //public static void DeleteAllApplicationRecords()
        //{
        //    IStatelessSession session = GetStatelessSession();
        //    using (var tx = session.BeginTransaction())
        //    {
                
                

        //        tx.Commit();
        //    }
        //    FluentSessionProvider.Instance.CloseStatelessSession(session);
        //}
    }
}
