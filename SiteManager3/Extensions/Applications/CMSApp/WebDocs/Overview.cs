﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using NetTable;
using NetCms.Grants;

/// <summary>
/// Summary description for NewsFolder_NEW_
/// </summary>
namespace NetCms.Structure.Applications.WebDocs
{
    public class WebDocsOverview : NetCms.Structure.WebFS.DocumentOverview
    {
        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Page_White; }
        }

        public WebDocsOverview()
        {
        }
        protected override string PreviewJS
        {
            get
            {
                return "javascript: Preview('" + this.CurrentFolder.FrontendHyperlink + "',{0},'" + this.CurrentFolder.Path + "')";
            }
        }
        public override Control getView()
        {
            if (this.CurrentFolder.Tipo != this.CurrentFolder.RelativeApplication.ID ||
               this.CurrentFolder.Criteria[CriteriaTypes.Modify].Allowed ||
               this.CurrentFolder == this.CurrentFolder.StructureNetwork.RootFolder
               )
            {
                return this.InvalidGrantsControl;
            }
            return base.getView();
        }
    }
}