﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Data;

namespace NetCms.Structure.Applications.WebDocs
{
    public class WebDocsApplication : NetCms.Structure.Applications.Application
    {
        public const string ApplicationContextLabel = "WebDocs.Backoffice";

        public const int ApplicationID = 2;

        public override string[] AllowedSubfolders
        {
            get { return new[] { "all" }; }
        }

        public override bool EnableNewsLetter { get { return true; } }
        public override bool EnableMove { get { return true; } }
        public override bool EnableModule { get { return false; } }
        public override bool EnableReviews { get { return true; } }
        public override string Namespace { get { return "NetCms.Structure.Applications.WebDocs"; } }
        public override string SystemName { get { return "webdocs"; } }
        public override string Label { get { return "Pagine Web"; } }
        public override string LabelSingolare { get { return "Pagina Web"; } }
        public override int ID { get { return ApplicationID; } }
        public override bool IsSystemApplication { get { return true; } }

        public override NetCms.Structure.Applications.Setup Installer
        {
            get
            {
                if (_Installer == null)
                    _Installer = new NetCms.Structure.Applications.WebDocs.Setup(this);
                return _Installer;
            }
        }
        private NetCms.Structure.Applications.Setup _Installer;
        
        public WebDocsApplication():base(null)
        {
            base.Assembly = Assembly.GetAssembly(this.GetType());
        }
        public WebDocsApplication(Assembly assembly)
            : base(assembly)
        {
        }
        /*
        protected override List<ApplicationValidation> LocalCheck()
        {
            List<ApplicationValidation> validationArray = new List<ApplicationValidation>();
            
            foreach (DataRow row in this.Installer.NetworksTable.Select(this.Installer.FilterID))
            {
                NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);
                if (network.CmsStatus != NetCms.Networks.NetworkCmsStates.NotCreated)
                {
                    DataTable check = Connection.SqlQuery("SHOW TABLES LIKE '" + network.SystemName + "_docs_webdocs'");
                    if (check.Rows.Count != 1)
                        validationArray.Add(new ApplicationValidation("Tabella Documenti WebDocs nella rete: '" + network.SystemName + "'", false, "Tabella dei documenti WebDocs non presente per la rete: '" + network.SystemName + "'"));
                    else validationArray.Add(new ApplicationValidation("Tabella Documenti WebDocs nella rete: '" + network.SystemName + "'", true, ""));
                }
            }
            return validationArray;
        }

        protected override bool LocalRepair()
        {
            return true;
            //throw new NotImplementedException();
        }*/
    }
}
