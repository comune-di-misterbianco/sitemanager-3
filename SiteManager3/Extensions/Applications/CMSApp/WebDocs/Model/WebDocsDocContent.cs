﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Structure.WebFS;
using System.Data;
using NHibernate;
using NetCms.Networks;
using NetCms.Structure.Search.Models;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;

namespace NetCms.Structure.Applications.WebDocs
{
    public class WebDocsDocContent : DocContent
    {
        public WebDocsDocContent()
            : base()
        { 
        
        }

        public virtual string Testo
        {
            get;
            set;
        }

        public override DocContent ImportDocContent(DataRow docRecord, ISession session, DataSet sourceDB, NetworkKey netKey, string sourceFolderPath, ref Document newDoc)
        {
            //Sostituire con un parsing dei contenuti href e src (ecc) e fare un replace dei percorsi secondo le nuove regole del nome cartella nel cms (vedi ModuloApplicativo in homepage)
            string filteredContent = NetCms.Structure.Utility.ImportContentFilter.ParseImportedContentUrls(docRecord["Testo_WebDocs"].ToString());
            Testo = filteredContent;
            return this;
        }

        public override DocContent ImportDocContent(Dictionary<string, string> docRecord, ISession session)
        {
            throw new NotImplementedException();
        }


        public override bool EnableIndexContent
        {
            get
            {
                return true;
            }
        }

        public override CmsConfigs.Model.IndexDocContent GetContentToIndex(ISession session)
        {
            CmsConfigs.Model.IndexDocContent indexContent = new CmsConfigs.Model.IndexDocContent();

            string currentDocPath = this.DocLang.Document.Folder.Path;

            string docLabel = string.Empty;

            if (this.DocLang.Label.ToLower() == "Pagina Indice".ToLower())
            {
                StructureFolder folder = FolderBusinessLogic.GetById(session, this.DocLang.Document.Folder.ID);
                if (folder != null)
                    docLabel = folder.Label;
            }
            else
            {
                docLabel = this.DocLang.Label;
            }

            indexContent.ID = this.ID;
            indexContent.Fields.Add(CmsDocument.Fields.document_title.ToString(), docLabel);            
            indexContent.Fields.Add(CmsDocument.Fields.document_description.ToString(), this.DocLang.Descrizione);
            indexContent.Fields.Add(CmsDocument.Fields.document_content.ToString(), this.Testo);
            indexContent.Fields.Add(CmsDocument.Fields.document_fronturl.ToString(), currentDocPath + "/" + this.DocLang.Document.PhysicalName); // manca il path relativo al documento 
            indexContent.Fields.Add(CmsDocument.Fields.document_backurl.ToString(), this.DocLang.Document.SiteManagerLink);
            indexContent.Fields.Add(CmsDocument.Fields.document_type.ToString(), this.DocLang.Document.Application.ToString());

            return indexContent;
        }
    }
}
