﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Structure.WebFS;

namespace NetCms.Structure.Applications.WebDocs.Mapping
{
    public class WebDocsDocContentMapping : SubclassMap<WebDocsDocContent>
    {
        public WebDocsDocContentMapping()
        {
            Table("webdocs_docs_contents");
            KeyColumn("id_WebDocs");
            Map(x => x.Testo).Column("Testo_WebDocs").CustomSqlType("mediumtext");
        }
    }
}
