﻿using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.WebDocs.Mapping
{
    public class WebDocsDocumentMapping : SubclassMap<WebDocsDocument>
    {
        public WebDocsDocumentMapping()
        {
            Table("webdocs_docs");
            KeyColumn("id_docs");
        }
    }
}
