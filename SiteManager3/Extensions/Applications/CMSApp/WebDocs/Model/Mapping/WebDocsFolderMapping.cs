﻿using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.WebDocs.Mapping
{
    public class WebDocsFolderMapping : SubclassMap<WebDocsFolder>
    {
        public WebDocsFolderMapping()
        {
            Table("webdocs_folders");
            KeyColumn("id_folders");
        }
    }
}
