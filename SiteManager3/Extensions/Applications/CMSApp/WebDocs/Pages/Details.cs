﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using NetCms.Grants;
using System.Reflection;

namespace NetCms.Structure.Applications.WebDocs.Pages
{
    public class Details : NetCms.Structure.WebFS.DocumentDetailsPage
    {
        protected override ToolbarButton ReserveForNewsletter
        {
            get
            {
                return new ToolbarButton("details.aspx?doc=" + CurrentDocument.ID + "&folder=" + PageData.CurrentFolder.ID + "&nlreserve=" + CurrentDocument.ID, NetCms.GUI.Icons.Icons.Email_Go, "Prenota per Newsletter");                
            }
        }

        protected override ToolbarButton ModifyButton
        {
            get
            {
                if (this.CurrentFolder.ReviewEnabled)
                    return new ApplicationToolbarButton("webdocs_overview.aspx?doc=" + this.CurrentDocument.ID, NetCms.GUI.Icons.Icons.Pencil, "Revisioni", this.CurrentFolder.StructureNetwork, this.CurrentFolder.RelativeApplication);
                else
                    return new ApplicationToolbarButton("webdocs_mod.aspx?doc=" + CurrentDocument.ID + "&amp;folder=" + CurrentFolder.ID, NetCms.GUI.Icons.Icons.Pencil, "Modifica Pagina Web", this.CurrentFolder.StructureNetwork, this.CurrentFolder.RelativeApplication);
            }
        }
        
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Dettaglio Pagina Web: '" + this.CurrentDocument.Nome + "'"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Newspaper_Add; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        public Details()
            : base()
        {

            ApplicationContext.SetCurrentComponent(WebDocsApplication.ApplicationContextLabel);
            if (this.CurrentDocument.Criteria[CriteriaTypes.Show].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina dei dettagli di una pagina web", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else if (!IsPostBack)
            {
                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina di dettaglio della pagina web " + this.CurrentDocument.Nome + " del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", callerObject: this);
            }
        }

        private bool NewsletterIsInstalled
        {
            get
            {
                return NetCms.Vertical.VerticalApplicationsPool.ActiveAssemblies.Contains(2);
            }
        }

        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);

            #region NewsletterButton

            //NetCms.Configurations.PortalData.NewsLetterEnabled
            if (NewsletterIsInstalled && this.CurrentDocument.Content > 0)
            {                
                Assembly assemblyNL = NetCms.Vertical.VerticalApplicationsPool.GetByID(2).Assembly;
                Type typePrenotazioneBL = assemblyNL.GetType("Newsletter.BusinessLogic.PrenotazioniBusinessLogic");

                var objPrenotazioneBL = Activator.CreateInstance(typePrenotazioneBL);
                MethodInfo mi = objPrenotazioneBL.GetType().GetMethod("CheckPrenotazione");                                
                
                // verifico se il documento è prenotato x la newsletter
                bool prenotato = (bool)mi.Invoke(objPrenotazioneBL, new object[] { this.CurrentDocument, null });

                if (!prenotato)
                {
                    bool insert = false;
                    NetUtility.RequestVariable NewsLetterReserve = new NetUtility.RequestVariable("nlreserve", PageData.Request.QueryString);
                    if (NewsLetterReserve.IsValid(NetUtility.RequestVariable.VariableType.Integer) && this.CurrentDocument.ID == NewsLetterReserve.IntValue)
                    {
                        Type typePrenotazione = assemblyNL.GetType("Newsletter.Model.Prenotazioni");

                        Object[] args = { this.CurrentDocument, NetCms.Users.AccountManager.CurrentAccount, 0 };
                        var objPrenotazione = Activator.CreateInstance(typePrenotazione, args);

                        mi = objPrenotazioneBL.GetType().GetMethod("SaveOrUpdatePrenotazioni");
                        insert = (bool)mi.Invoke(objPrenotazioneBL, new object[] { objPrenotazione, null });

                        if (insert)
                            NetCms.GUI.PopupBox.AddSessionMessage("Prenotazione eseguita con successo", PostBackMessagesType.Success);
                        else
                            NetCms.GUI.PopupBox.AddSessionMessage("Errore durante la prenotazione del documento", PostBackMessagesType.Error);

                    }
                    
                    if (!insert && this.CurrentDocument.Content > 0)
                        this.Toolbar.Buttons.Add(ReserveForNewsletter);
                }
                
                    
                
                //if (this.Connection.SqlQuery("SELECT * FROM Newsletter_Prenotazioni WHERE Doc_Prenotazione = " + this.CurrentDocument.ID).Rows.Count == 0)
                //{
                //    NetUtility.RequestVariable NewsLetterReserve = new NetUtility.RequestVariable("nlreserve", PageData.Request.QueryString);

                //    if (NewsLetterReserve.IsValid(NetUtility.RequestVariable.VariableType.Integer) && this.CurrentDocument.ID == NewsLetterReserve.IntValue)
                //    {
                //        //string sql = "INSERT INTO Newsletter_Prenotazioni";
                //        //sql += " (Utente_Prenotazione,Doc_Prenotazione,Stato_Prenotazione,Data_Prenotazione)";
                //        //sql += " Values (";
                //        //sql += NetCms.Users.AccountManager.CurrentAccount.ID + ",";//Utente_MailingList_Prenotazione
                //        //sql += this.CurrentDocument.ID + ",";//Doc_MailingList_Prenotazione
                //        //sql += "0,";//Stato_Prenotazione
                //        //sql += "" + this.Connection.FilterDate(DateTime.Now.Date.ToString()) + "";//Data_Prenotazione
                //        //sql += ")";
                //        //bool done = this.Connection.Execute(sql);
                //        //if (done == false)
                //        //    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile inserire la prenotazione per la pagina web nel database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "100");

                //        NetCms.GUI.PopupBox.AddSessionMessage("Prenotazione eseguita con successo", PostBackMessagesType.Success);
                //    }
                //    else
                //    {
                //        if (ReserveForNewsletter != null)
                //            this.Toolbar.Buttons.Add(ReserveForNewsletter);
                //    }
                //}

            }
            #endregion

            string scheda = "<div class=\"DetailFrameError\"><h3>Nessuna revisione attiva per questo documento<h3></div>";



            if (this.CurrentDocument.Content > 0)
            {
                //WebDocsDocContent webDocContent = WebDocsBusinessLogic.GetById(this.CurrentDocument.Content);
                //if (webDocContent != null)
                //{
                //    NetCms.Structure.CmsPathCodes coder = new NetCms.Structure.CmsPathCodes(this.CurrentFolder.StructureNetwork);
                //    string preview = webDocContent.Testo;
                //    preview = coder.DecodeWebRootReference(preview);
                //    HtmlGenericControl cx = new HtmlGenericControl("div");
                //    cx.ID = "PreviewBox";
                //    scheda = preview;
                //}




                //DataTable table = this.CurrentFolder.Network.Connection.SqlQuery("SELECT * FROM Docs_WebDocs WHERE id_WebDocs = " + this.CurrentDocument.Content);
                //if (table.Rows.Count > 0)
                //{
                //    NetCms.Structure.CmsPathCodes coder = new NetCms.Structure.CmsPathCodes(this.CurrentFolder.Network);
                //    string preview = table.Rows[0]["Testo_WebDocs"].ToString();
                //    preview = coder.DecodeWebRootReference(preview);
                //    HtmlGenericControl cx = new HtmlGenericControl("div");
                //    cx.ID = "PreviewBox";
                //    scheda = preview;
                //}

                WebControl iframe = new WebControl(HtmlTextWriterTag.Iframe);
                iframe.Attributes["src"] = this.CurrentFolder.FrontendHyperlink + "/preview.aspx?id=" + this.CurrentDocument.Content;
                iframe.Attributes["width"] = "100%";
                iframe.Attributes["height"] = "700px";

                div.Controls.Add(iframe);
            }
            else
            {
                div.Controls.Add(new LiteralControl(scheda));
            }           

            return div;
        }
    }
}
