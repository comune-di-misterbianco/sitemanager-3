﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Linq;
using NetCms.Structure.WebFS.Reviews;
using NetCms.Grants;

namespace NetCms.Structure.Applications.WebDocs.Pages
{
    public class WebDocsAdd : NetCms.GUI.NetworkPage
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override bool AddPingSessionControl
        {
            get
            {
                return true;
            }
        }

        public override string PageTitle
        {
            get { return "Creazione Nuova Pagina Web"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Page_White_Add; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        public WebDocsAdd()
        {
            ApplicationContext.SetCurrentComponent(WebDocsApplication.ApplicationContextLabel);
            
           

            if (this.CurrentFolder.Criteria[CriteriaTypes.Create].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina di aggiunta di una nuova pagina web", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else if (!IsPostBack)
                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina di creazione di una pagina web del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", callerObject: this);
        }
       
        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "container-fluid";

            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");

            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerText = "Nuova Pagina Web";
            fieldset.Controls.Add(legend);

            HtmlGenericControl contentDiv = new HtmlGenericControl("div");
            contentDiv.Attributes["class"] = "contentForm webdocform col-md-12";

            NetFormTable netDocs = WebDocsDocument.NetworkDocsFormTable(PageData);
            netDocs.CssClass = "netformtable_netDocs";

            NetFormTable docs = WebDocsDocument.DocsFormTable(PageData, 2);
            docs.CssClass = "netformtable_docs";

            NetFormTable langs = WebDocsDocument.LanguageFormTable(PageData);
            langs.CssClass = "netformtable_langs";


            NetExternalField docsToNetDocs;
            NetExternalField docsToWebDocsDoc;
            NetExternalField contentForDocLang;
            NetExternalField content;

            NetFormTable contentTable = WebDocsDocument.DocsContentFormTable(PageData);
            contentTable.CssClass = "netformtable_contentTable";

            NetFormTable webdoc = new NetFormTable("webdocs_docs_contents", "id_Webdocs", this.CurrentFolder.StructureNetwork.Connection);
            webdoc.CssClass = "netformtable_webdoc";

            NetFormTable webdocDoc = new NetFormTable("webdocs_docs", "id_docs", this.CurrentFolder.StructureNetwork.Connection);
            webdocDoc.CssClass = "netformtable_webdocDoc";

            //if (this.Account.GUI.Editor == NetCms.Users.SitemanagerUserGUI.RichTextEditors.FreeTextBox)
            //{
            //    NetRichTextBoxCMS Testo = new NetRichTextBoxCMS("Testo", "Testo_Webdocs", PageData);
            //    Testo.EditorWidth = NetRichTextBoxCMS.WebEditorWidth.Full;
            //    Testo.Value = "<p> <br /> </p>";
            //    Testo.Required = true;
            //    webdoc.addField(Testo);
            //}
            //if (this.Account.GUI.Editor == NetCms.Users.SitemanagerUserGUI.RichTextEditors.TinyMCE)
            //{
            //    NetTinyMCE Testo = new NetTinyMCE("Testo", "Testo_Webdocs", PageData);
            //    Testo.EditorWidth = NetTinyMCE.WebEditorWidth.Full;
            //    Testo.Required = true;
            //    webdoc.addField(Testo);
            //}

            RichTextBox TestoCKEditor = new RichTextBox("Testo", "Testo_Webdocs");
            TestoCKEditor.Value = "<p> <br /> </p>";
            TestoCKEditor.Required = true;
            TestoCKEditor.CurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID;
            webdoc.addField(TestoCKEditor);

            NetForm form = new NetForm();

            form.AddTable(contentTable);
            form.AddTable(webdoc);

            form.AddTable(netDocs);
            form.AddTable(docs);
            form.AddTable(webdocDoc);

            form.AddTable(langs);

            NetFormTable review = WebDocsDocument.ReviewFormTable(PageData);
            review.CssClass = "netformtable_review";
            if (!this.CurrentFolder.ReviewEnabled)
            {
                contentForDocLang = new NetExternalField("docs_contents", "Content_DocLang");
                langs.ExternalFields.Add(contentForDocLang);
            }
            else
            {
                contentForDocLang = new NetExternalField("docs_contents", "Contenuto_Revisione");
                review.ExternalFields.Add(contentForDocLang);
                form.AddTable(review);
            }

            content = new NetExternalField("docs_contents", "id_WebDocs");
            webdoc.ExternalFields.Add(content);

            docsToNetDocs = new NetExternalField("network_docs", "id_Doc");
            docs.ExternalFields.Add(docsToNetDocs);

            docsToWebDocsDoc = new NetExternalField("network_docs", "id_docs");
            webdocDoc.ExternalFields.Add(docsToWebDocsDoc);

            #region PostBack

            string errors = "";
            if (PageData.IsPostBack)
            {
                if (PageData.SubmitSource == "send")
                {
                    NetField namefield = docs.Fields["Name_Doc"];
                    string name = namefield.RequestVariable.OriginalValue;
                    name = name.Replace(".aspx", "");
                    
                    string filename = NetTextBoxPlus.ClearFileSystemName(namefield.RequestVariable.StringValue);

                    WebDocsDocument docExist = this.CurrentFolder.FindDocByPhysicalName(filename + ".aspx") as WebDocsDocument;
                    //DataTable row = this.CurrentFolder.Network.Connection.SqlQuery("SELECT id_Doc FROM Docs WHERE Folder_Doc = " + this.CurrentFolder.ID + " AND Name_Doc = '" + filename + ".aspx'");
                    //if (row.Rows.Count == 0)
                    if (docExist == null)
                    {
                        errors = form.serializedInsert(HttpContext.Current.Request.Form);
                        if (form.LastOperation == NetForm.LastOperationValues.OK)
                        {
                            WebDocsDocument insertedDoc = null;
                            try
                            {
                                insertedDoc = DocumentBusinessLogic.GetById(netDocs.LastInsert) as WebDocsDocument;
                                insertedDoc.PhysicalName = filename + ".aspx";
                                //VERIFICARE CHE AGGIORNI LA LABEL CORRETTA
                                insertedDoc.DocLangs.Where(x => x.Lang == PageData.DefaultLang).First().Label = name;

                                DocumentBusinessLogic.SaveOrUpdateDocument(insertedDoc);
                                //this.CurrentFolder.Network.Connection.Execute("UPDATE Docs SET Name_Doc='" + filename + ".aspx' WHERE id_Doc = " + docs.LastInsert);
                                //this.CurrentFolder.Network.Connection.Execute("UPDATE Docs_Lang SET Label_DocLang='" + name + "' WHERE Lang_DocLang = " + docs.DefaultLangID.ToString() + " AND Doc_DocLang = " + docs.LastInsert);

                                if (this.CurrentFolder.ReviewEnabled)
                                {
                                    DateTime ora = DateTime.Now;
                                    string date = ora.Year + "-" + ora.Month + "-" + ora.Day + " " + ora.Hour + "." + ora.Minute + "." + ora.Second;

                                    Review revisione = ReviewBusinessLogic.GetReviewById(review.LastInsert);
                                    revisione.Data = DateTime.Now;
                                    ReviewBusinessLogic.SaveOrUpdateReview(revisione);
                                }
                                //this.CurrentFolder.Network.Connection.Execute("UPDATE revisioni SET Data_Revisione='" + date + "' WHERE id_Revisione = " + review.LastInsert);

                                //SISTEMARE CON CACHE DI 2° LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                                this.CurrentFolder.Invalidate();
                                //this.CurrentFolder.InvalidateDocs();


                                //NetCms.Structure.WebFS.Document doc = this.CurrentFolder.Documents[docs.LastInsert.ToString()];

                                if (this.CurrentFolder.EnableRss)
                                    this.CurrentFolder.RssManager.SaveXML();

                                G2Core.Common.RequestVariable tags = new G2Core.Common.RequestVariable("Tags");
                                Tags.AddTagsForDoc(tags.StringValue, docs.LastInsert.ToString(), this.CurrentFolder.StructureNetwork.Connection);

                                
                            }
                            catch (Exception ex)
                            {
                                DocumentBusinessLogic.DeleteDocument(insertedDoc);
                                //this.CurrentFolder.Network.Connection.Execute("DELETE FROM Docs_Lang WHERE Doc_DocLang = " + docs.LastInsert);
                                //this.CurrentFolder.Network.Connection.Execute("DELETE FROM Docs WHERE id_Doc = " + docs.LastInsert);
                                NetCms.GUI.PopupBox.AddMessage("Non è stato possibile creare questo documento a causa di un problema tecnico. Riprovare ad effettuare l'inserimento, se il problema persiste contattare l'assistenza tecnica.", NetCms.GUI.PostBackMessagesType.Error);
                                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile creare la pagina web", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "50");
                            }
                            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha creato con successo una pagina web del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", callerObject: this);
                            NetCms.GUI.PopupBox.AddSessionMessage(errors, NetCms.GUI.PostBackMessagesType.Success, true);
                        }
                        else
                        {
                            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante la creazione di una pagina web del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "554");
                            NetCms.GUI.PopupBox.AddMessage(errors, NetCms.GUI.PostBackMessagesType.Error);
                        }
                    }
                    else
                    {
                        NetCms.GUI.PopupBox.AddMessage("Esiste già un documento con questo nome in questa cartella o nel cestino.", NetCms.GUI.PostBackMessagesType.Error);
                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Esiste già una pagina web con questo nome nella cartella o nel cestino", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "98");
                    }
                }
            }
            #endregion

            #region Creazione dei controlli


            //HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            //HtmlGenericControl legend = new HtmlGenericControl("legend");

            //legend.InnerHtml = "Nuova Pagina Web";

            //fieldset.Controls.Add(legend);
            contentDiv.Controls.Add(docs.getForm());
            contentDiv.Controls.Add(langs.getForm());
            contentDiv.Controls.Add(webdoc.getForm());
            if (this.CurrentFolder.ReviewEnabled)
                contentDiv.Controls.Add(review.getForm());
            
            
            //div.Controls.Add(fieldset);
            HtmlGenericControl pButtons = new HtmlGenericControl("p");
            pButtons.Attributes["class"] = "buttonscontainer";

            Button bt = new Button();
            bt.Text = "Crea Pagina Web";
            bt.ID = "send";
            bt.CssClass = "button";
            bt.OnClientClick = "javascript: setSubmitSource('send')";
            //fieldset.Controls.Add(bt);                    
            pButtons.Controls.Add(bt);

            contentDiv.Controls.Add(pButtons);

            WebControl row = new WebControl(HtmlTextWriterTag.Div);
            row.CssClass = "row";

            row.Controls.Add(contentDiv);

            fieldset.Controls.Add(row);

            div.Controls.Add(fieldset);
            #endregion

            return div;
        }
    }
}
