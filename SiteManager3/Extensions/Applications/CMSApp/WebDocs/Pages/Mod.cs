﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.Structure.Grants;
using NetCms.Structure.WebFS;
using NetCms.Grants;

namespace NetCms.Structure.Applications.WebDocs.Pages
{
    public class Mod : NetCms.GUI.NetworkPage
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Modifica Pagina Web"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Page_White_Edit; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }



        public Mod()
        {
            ApplicationContext.SetCurrentComponent(WebDocsApplication.ApplicationContextLabel);

            if (this.CurrentDocument.Criteria[CriteriaTypes.Modify].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", NetCms.GUI.PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina di modifica di una rassegna", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else if (!IsPostBack)
            {
                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina di modifica della pagina web " + this.CurrentDocument.Nome + " del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", callerObject: this);
            }
        }
        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);

            NetFormTable docs = WebDocsDocument.DocsFormTable(PageData,2, this.CurrentDocument);

            NetFormTable WebDocs = new NetFormTable("webdocs_docs_contents"/*"Docs_WebDocs"*/, "id_WebDocs", this.CurrentFolder.StructureNetwork.Connection, this.CurrentDocument.Content);
            #region Campi

            //if (this.Account.GUI.Editor == NetCms.Users.SitemanagerUserGUI.RichTextEditors.FreeTextBox)
            //{
            //    NetRichTextBoxCMS Testo = new NetRichTextBoxCMS("Testo", "Testo_Webdocs", PageData);
            //    Testo.EditorWidth = NetRichTextBoxCMS.WebEditorWidth.Full;
            //    Testo.ToolbarType = NetRichTextBoxCMS.PredefinedToobars.Full;
            //    Testo.Required = true;
            //    WebDocs.addField(Testo);
            //}
            //if (this.Account.GUI.Editor == NetCms.Users.SitemanagerUserGUI.RichTextEditors.TinyMCE)
            //{
            //    NetTinyMCE Testo = new NetTinyMCE("Testo", "Testo_Webdocs", PageData);
            //    Testo.EditorWidth = NetTinyMCE.WebEditorWidth.Full;
            //    Testo.ToolbarType = NetTinyMCE.PredefinedToobars.Full;
            //    Testo.Required = true;
            //    WebDocs.addField(Testo);
            //}

            RichTextBox TestoCKEditor = new RichTextBox("Testo", "Testo_Webdocs");
            //TestoCKEditor.Value = "<p> <br /> </p>";
            TestoCKEditor.Required = true;
            TestoCKEditor.CurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID;
            WebDocs.addField(TestoCKEditor);

            //CodeTextBox Testo = new CodeTextBox("Testo", "Testo_Webdocs", PageData);
            //WebDocs.addField(Testo);

            //NetTinyMCE Testo = new NetTinyMCE("Testo", "Testo_Webdocs", PageData);
            //Testo.EditorWidth = NetTinyMCE.WebEditorWidth.Full;
            //Testo.ToolbarType = NetTinyMCE.PredefinedToobars.Full;
            //Testo.Required = true;
            //WebDocs.addField(Testo);
            
            #endregion
            
            
            PageData.CurentObjectInfoColtrol = new SideCurentObjectData("CurentDocSideInfo", this.CurrentDocument, PageData).getControl();

            NetForm form = new NetForm();
            form.AddTable(WebDocs);
            form.AddTable(docs);

            #region PostBack

            string errors = "";
            if (PageData.IsPostBack)
            {
                if (PageData.SubmitSource == "send")
                {
                    errors = form.serializedUpdate(HttpContext.Current.Request.Form);
                    if (form.LastOperation == NetForm.LastOperationValues.OK)
                    {
                        this.CurrentDocument.PublishContent();
                        //SISTEMARE CON CACHE 2° LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                        this.CurrentFolder.Invalidate();

                        if (this.CurrentFolder.EnableRss)
                            this.CurrentFolder.RssManager.SaveXML();

                        G2Core.Common.RequestVariable tags = new G2Core.Common.RequestVariable("Tags");
                        Tags.AddTagsForDoc(tags.StringValue, this.CurrentDocument.ID.ToString(), this.CurrentFolder.StructureNetwork.Connection);

                        
                        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha modificato con successo la pagina web " + this.CurrentDocument.Nome + " del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", callerObject: this);
                        NetCms.GUI.PopupBox.AddSessionMessage(errors, NetCms.GUI.PostBackMessagesType.Success, true);
                    }
                    else
                    {
                        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante la modifica della pagina web " + this.CurrentDocument.Nome + " del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "555");
                        NetCms.GUI.PopupBox.AddMessage(errors, NetCms.GUI.PostBackMessagesType.Error);
                    }
                }
            }
            #endregion

            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerText = "Modifica Pagina Web";
            
            fieldset.Controls.Add(legend);
            fieldset.Controls.Add(docs.getForm());
            fieldset.Controls.Add(WebDocs.getForm());

            Button bt = new Button();
            bt.Text = "Salva Modifiche";
            bt.ID = "send";
            bt.CssClass = "button";
            bt.OnClientClick = "javascript: setSubmitSource('send')";
            fieldset.Controls.Add(bt);

            div.Controls.Add(fieldset);

            return div;
        }
    }
}
