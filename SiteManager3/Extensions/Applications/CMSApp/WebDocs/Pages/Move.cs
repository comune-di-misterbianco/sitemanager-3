﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.Structure.WebFS;
using NetCms.Grants;

/// <summary>
/// Summary description for WebDocsFolder_NEW_
/// </summary>
namespace NetCms.Structure.Applications.WebDocs
{
    public class DocMove : NetCms.GUI.NetworkPage
    {

        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Spostamento Pagina Web"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Page_White_Go; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        private NetCms.Structure.WebFS.Document _Doc;
        public NetCms.Structure.WebFS.Document Doc
        {
            get
            {
                if (_Doc == null)
                {

                    NetUtility.RequestVariable docvar = new NetUtility.RequestVariable("id", HttpContext.Current.Request.QueryString);
                    if (docvar.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                        _Doc = this.CurrentFolder.FindDocByID(docvar.IntValue) as WebDocsDocument;

                }
                try
                {
                    if (_Doc == null || !_Doc.Grant || _Doc.Criteria[CriteriaTypes.Advanced].Value == GrantsValues.Deny)
                        throw new Exception("Variabile _Doc in " + this.CurrentFolder.StructureNetwork.Paths.AbsoluteFrontRoot + " contenuta nella classe Move di WebDocs non valida o permessi negati");//PageData.GoToErrorPage(_Doc);
                }
                catch (Exception ex)
                {
                    /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                    ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "101");

                }
                return _Doc;
            }
        }




        public DocMove()
        {
            ApplicationContext.SetCurrentComponent(WebDocsApplication.ApplicationContextLabel);
            if (this.CurrentFolder.Criteria[CriteriaTypes.Advanced].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", NetCms.GUI.PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina di spostamento di una pagina web", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            if (this.CurrentDocument == null)
            {
                //Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Pagina web richiesta dall'utente non trovata", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                //HttpContext.Current.NetCms.Diagnostics.Diagnostics.Redirect("webdocs_error.aspx");
            }
        }
        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.Controls.Add(Doc.MoveDocView(PageData));

            return div;
        }
    }
}