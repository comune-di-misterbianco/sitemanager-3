﻿using System;
using System.IO;
using System.Xml;
using System.Web;
using System.Data;
using NetCms.Networks;
using NetCms.Networks.WebFS;
using NetCms.Exporter;

namespace NetCms.Structure.Applications.WebDocs
{
    public class WebDocsExporter:ApplicationExporter
    {
        private DataTable _DocsTable;
        public override DataTable DocsTable
        {
            get
            {
                if (_DocsTable == null)
                {
                    //_DocsTable = this.FolderExporter.Exporter.Connection.SqlQuery(string.Format(ApplicationExporter.Network_Docs, "INNER JOIN docs_webdocs ON id_WebDocs = Content_DocLang", this.FolderID, ""));

                    _DocsTable = this.FolderExporter.Exporter.Conn.SqlQuery(string.Format(ApplicationExporter.Network_Docs, "INNER JOIN revisioni ON DocLang_Revisione=id_DocLang INNER JOIN webdocs_docs_contents ON id_WebDocs = Contenuto_Revisione", this.FolderID, "",WebDocsApplication.ApplicationID.ToString()));
                    //_DocsTable = this.FolderExporter.Exporter.Connection.SqlQuery(string.Format(ApplicationExporter.Network_Docs, "INNER JOIN revisioni ON DocLang_Revisione=id_DocLang INNER JOIN docs_webdocs ON id_WebDocs = Contenuto_Revisione", this.FolderID, ""));
                    _DocsTable.TableName = "docs";
                }
                return _DocsTable;
            }
        }

        public override DataTable[] Tables 
        {
            get
            {
                return null;
            }
        }

        public override bool HasFiles { get { return false; } }


        public WebDocsExporter(string folderID, FolderExporter folderExporter)
            : base(folderID, folderExporter)
        {
        }
    }
}
