﻿using System;
using System.IO;
using System.Data;
using System.Reflection;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.WebFS;
using NetCms.Networks.WebFS;
using NetCms.Front;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS.Reviews;
using NHibernate;
using NetCms.Grants;
using NetCms.Structure.Applications.Homepage;
using SharedUtilities;
using System.Collections.Generic;

namespace NetCms.Structure.Applications.WebDocs
{
    public partial class WebDocsFolder : StructureFolder
    {
        public override NetCms.Structure.Applications.Application RelativeApplication
        {
            get
            {
                if (_RelativeApplication == null)
                    _RelativeApplication = Applications.ApplicationsPool.ActiveAssemblies[WebDocsApplication.ApplicationID.ToString()];
                return _RelativeApplication;
            }
        }
        private NetCms.Structure.Applications.Application _RelativeApplication;

        public override bool AllowMove
        {
            get
            {   
                return RelativeApplication.EnableMove;
            }
        }

        public override bool ReviewEnabled
        {
            get
            {
                return this.RelativeApplication.EnableReviews;
            }
        }

        public override ToolbarButtonsCollection ToolbarButtons
        {
            get
            {
                ToolbarButtonsCollection _ToolbarButtons = base.ToolbarButtons;


                if (this.Criteria[CriteriaTypes.Create].Allowed)
                {
                    if (NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies.Contains("9"))
                        _ToolbarButtons.Add(new NetCms.GUI.ToolbarButton(this.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/applications/links/links_new.aspx?folder=" + ID, NetCms.GUI.Icons.Icons.Link_Add, "Nuovo Collegamento Ipertestuale"));
                    _ToolbarButtons.Add(new NetCms.Structure.Applications.ApplicationToolbarButton("webdocs_new.aspx?folder=" + ID, NetCms.GUI.Icons.Icons.Page_White_Add, "Nuova Pagina Web", this.StructureNetwork, this.RelativeApplication));                    
                }
                return _ToolbarButtons;

            }
        }

        public WebDocsFolder()
            : base()
        { }

        public WebDocsFolder(Networks.NetworkKey network)
            : base(network)
        {

        }
        
        public enum HomepageTypes
        {
            TitleAndDesc,
            ContentList,
            CustomPage,
            LandingPage,
            SectionPage,
        }

        private HomepageTypes _HomeTypeEnum;
        public virtual HomepageTypes HomeTypeEnum
        {
            get
            {
                switch (HomeType)
                {
                    case 0:
                        _HomeTypeEnum = HomepageTypes.TitleAndDesc;
                        break;
                    case -1:
                        _HomeTypeEnum = HomepageTypes.ContentList;
                        break;
                    case 2:
                        _HomeTypeEnum = HomepageTypes.LandingPage;
                        break;
                    case 3:
                        _HomeTypeEnum = HomepageTypes.SectionPage;
                        break;
                    case 1:
                        _HomeTypeEnum = HomepageTypes.CustomPage;
                        break;
                    default:
                        _HomeTypeEnum = HomepageTypes.CustomPage;
                        break;
                }
                
                return _HomeTypeEnum;
            }
        }      

        public virtual int CreateDefaultDoc(bool visible, ISession innerSession = null)
        {

            StructureFolder folderToMod = FolderBusinessLogic.GetById(innerSession, this.ID);

            string now = folderToMod.StructureNetwork.Connection.FilterDate(DateTime.Now.Date.ToString());

            WebDocsDocument newDefaultDoc = new WebDocsDocument(folderToMod.NetworkKey);
            newDefaultDoc.PhysicalName = "default.aspx";
            newDefaultDoc.Folder = folderToMod;
            newDefaultDoc.Application = folderToMod.Tipo;
            newDefaultDoc.Stato = (visible ? 1 : 2);
            newDefaultDoc.Autore = folderToMod.Account.ID;
            newDefaultDoc.Data = DateTime.Now;
            newDefaultDoc.Modify = DateTime.Now;
            newDefaultDoc.Create = DateTime.Now;

            newDefaultDoc = DocumentBusinessLogic.SaveOrUpdateDocument(newDefaultDoc, innerSession) as WebDocsDocument;
            
            int docid = newDefaultDoc.ID;

            DocumentLang newLang = new DocumentLang();
            newLang.Label = "Pagina Indice";
            newLang.Lang = folderToMod.StructureNetwork.DefaultLang;
            newLang.Descrizione = "Pagina Indice della cartella";
            newLang.Document = newDefaultDoc;
            newDefaultDoc.DocLangs.Add(newLang);

            newLang = DocumentBusinessLogic.SaveOrUpdateDocumentLang(newLang, innerSession);

            WebDocsDocContent content = new WebDocsDocContent();
            content.Testo = "";
            WebDocsBusinessLogic.SaveOrUpdateWebDocsContent(content, innerSession);
            
            //Se non sono attive le revisioni associo il contenuto al doclang e viceversa
            if (!folderToMod.RelativeApplication.EnableReviews)
            {
                content.DocLang = newLang;
                newLang.Content = content;
            }
            else
            {
                //Se sono attive le revisioni associo al contenuto la revisione e viceversa, e associo la revisione al doclang, nel quale il contenuto sarà null (oppure quello pubblicato)
                Review revisione = new Review(Users.AccountManager.CurrentAccount);
                revisione.DocLang = newLang;
                revisione.Content = content;
                revisione.Data = DateTime.Now;
                revisione.Autore = Users.AccountManager.CurrentAccount.ID;
                revisione.Origine = 0;
                revisione.Published = 1;
                ReviewBusinessLogic.SaveOrUpdateReview(revisione, innerSession);

                newLang.Content = content;
                content.Revisione = revisione;
                WebDocsBusinessLogic.SaveOrUpdateWebDocsContent(content, innerSession);

                newLang.Revisioni.Add(revisione);
            }
            //SISTEMARE CON CACHE 2° LIVELLO
            //this.InvalidateDocs();

            #region invalido la struttura
            //Controllare l'invalidazione. Verificare se va invalidata tutta la cartella o solo 1 documento - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
            folderToMod.Invalidate();

            #endregion

            DocumentBusinessLogic.SaveOrUpdateDocumentLang(newLang, innerSession);
            folderToMod.Documents.Add(newDefaultDoc);
            return newDefaultDoc.ID;
        }


        public override HtmlGenericControl getModFolderView()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "container-fluid";

            #region Toolbar
            PageData.InfoToolbar.Title = "Modifica Proprietà Cartella '" + this.Nome + "'";
            PageData.InfoToolbar.Icon = "Folder";
            PageData.InfoToolbar.ShowFolderPath = true;
            #endregion
           
            NetFormTable lang = new NetFormTable("FoldersLang", "id_FolderLang", this.StructureNetwork.Connection, this.FolderLangs.ElementAt(0).ID);
            lang.CssClass = "netformtable_lang";

            NetFormTable table = new NetFormTable("Folders", "id_Folder", this.StructureNetwork.Connection, this.ID);
            table.CssClass = "netformtable_folder";

            NetTextBoxPlus nome = new NetTextBoxPlus("Nome", "Label_FolderLang");
            nome.Required = true;
            nome.ContentType = NetTextBoxPlus.ContentTypes.Folder;
            lang.addField(nome);

            NetDropDownList tipo = new NetDropDownList("Tipo", "Tipo_Folder");
            NetDropDownList Stato = new NetDropDownList("Stato", "Stato_Folder");
            
            foreach (StructureFolder.FolderStates stato in Enum.GetValues(typeof(StructureFolder.FolderStates)))
            {
                int k = (int)Enum.Parse(typeof(StructureFolder.FolderStates), stato.ToString());
                Stato.addItem(EnumUtilities.GetEnumDescription(stato), k.ToString());
            }  
            Stato.Required = true;
            table.addField(Stato);

            NetDropDownList restricted = new NetDropDownList("Area Riservata", "Restricted_Folder");
            restricted.ClearList();
            if ((this.Parent as StructureFolder).Restricted && (this.Parent as StructureFolder).RestrictedArea > 0)
            {
                NetHiddenField restrictedforced = new NetHiddenField("Restricted_Folder");
                restrictedforced.Value = "0";
                table.addField(restrictedforced);
            }
            else
            {
                if (!(this.Parent as StructureFolder).Restricted)
                {
                    restricted.addItem("NO (Cartella accessibile a tutto il Pubblico)", "0");
                    restricted.addItem("Generale (Cartella accessibile da tutto il pubblico autenticato)", "-1");
                }
                else
                {
                    restricted.addItem("Eredita (Conserva le impostazioni della cartella genitore)", "0");
                }
            
                restricted.Required = true;
                table.addField(restricted);
            }
            bool errore = false;
            int AbsHome = 0;           

            NetUtility.RequestVariable CurrentHomeType = new NetUtility.RequestVariable("HomeType_Folder", NetUtility.RequestVariable.RequestType.Form);
            int currentHomeType = CurrentHomeType.IntValue;

            if (currentHomeType == 1 || currentHomeType == 3 || HomeType < -1) // il terzo controllo serve per gestire le pagine custom con la precedente 
                                                                               // modalità e mantenere la compatibilità con i dati vecchi
            {
                WebDocsDocument doc = (WebDocsDocument)this.FindDocByPhysicalName("default.aspx");
                if (doc == null)
                {
                    using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                    using (ITransaction tx = sess.BeginTransaction())
                    {
                        try
                        {
                            AbsHome = CreateDefaultDoc(false, sess);
                            tx.Commit();
                            //Se si arriva a questo punto il commit non ha generato eccezioni e l'inserimento di document, doclang e revisione
                            //è avvenuto con successo. Allora è possibile aggiungere il documento creato nella folder.
                            WebDocsDocument newDefaultDoc = (WebDocsDocument) DocumentBusinessLogic.GetById(AbsHome);
                            if (this.Documents.Count(x=>x.ID == newDefaultDoc.ID) == 0)
                                this.Documents.Add(newDefaultDoc);
                        }
                        catch (Exception ex)
                        {
                            tx.Rollback();
                            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile inserire la WebDocs di default nel database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "48",ex);
                            AbsHome = 0;
                            errore = true;                            
                        }
                    }
                }
            }
            if (!errore)
            {
                #region Campi
                NetDropDownList TipoHome = new NetDropDownList("Tipo pagina principale", "HomeType_Folder");
                TipoHome.addItem("Nome e Descrizione", "0");
                TipoHome.addItem("Indice contenuti", "-1"); 
                TipoHome.addItem("Pagina Personalizzata", "1");
                TipoHome.addItem("Landing Page", "2");
                TipoHome.addItem("Section Page", "3");
                TipoHome.Required = true;

                table.addField(TipoHome);

                NetDropDownList ShowMenu = new NetDropDownList("Mostra menu contestuale", "ShowMenu_Folder");
                ShowMenu.ClearList();
                ShowMenu.addItem("No", "0");
                ShowMenu.addItem("Si", "1");
                //ShowMenu.Required = true;
                table.addField(ShowMenu);

                NetDropDownList ShowInTopMenu = new NetDropDownList("Mostra nel topmenu", "ShowInTopMenu_Folder");
                ShowInTopMenu.ClearList();
                ShowInTopMenu.addItem("Si", "1");
                ShowInTopMenu.addItem("No", "0");
                table.addField(ShowInTopMenu);

                NetImagePicker coverImageStart = new NetImagePicker("Immagine di copertina", "CoverImage_Folder", this.PageData);
                coverImageStart.CurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID;
                table.addField(coverImageStart);

                NetTextBoxPlus Descrizione = new NetTextBoxPlus("Descrizione", "Desc_FolderLang");
                Descrizione.Required = true;
                Descrizione.MaxLenght = 2000;
                Descrizione.Rows = 5;
                lang.addField(Descrizione);

                NetDropDownList TitleOffset = new NetDropDownList("Titolo del Frontend", "TitleOffset_Folder");
                TitleOffset.ClearList();
                TitleOffset.addItem("Mostra solo il titolo", "0");
                TitleOffset.addItem("Aggiungi path in coda al titolo", "-1");
                TitleOffset.addItem("Aggiungi KEYWORDS in coda al titolo", "1");
                TitleOffset.addItem("Mostra solo KEYWORDS", "2");
                TitleOffset.Required = true;

                NetTextBoxPlus cssclass = new NetTextBoxPlus("Classe Css", "CssClass_Folder");
                cssclass.ContentType = NetTextBoxPlus.ContentTypes.NormalText;

                if (this.Criteria[CriteriaTypes.Advanced].Allowed)
                {
                    table.addField(TitleOffset);
                    table.addField(cssclass);
                }

                if (this.StructureNetwork.CurrentFolder.RssValue != RssStatus.SystemError)
                {
                    NetDropDownList rss = new NetDropDownList("Rss", "Rss_Folder");
                    rss.addItem("Eredita", "0");
                    rss.addItem("Abilitati", "1");
                    rss.addItem("Disabilitati", "2");
                    rss.Required = true;

                    table.addField(rss);
                }
                if (this.FrontLayout != FrontLayouts.SystemError && this.Tipo != 1)
                {
                    NetDropDownList layout = new NetDropDownList("Layout di colonne e Piè di pagina (Attenzione la visualizzazione finale del layout dipende comunque da come sono state impostate le colonne nella homepage relativa a questa cartella.)", "Layout_Folder");
                    layout.ClearList();

                    layout.addItem(FrontLayouts.Inherits.ToDescriptionString(), ((int)FrontLayouts.Inherits).ToString());
                    layout.addItem(FrontLayouts.All.ToDescriptionString(), ((int)FrontLayouts.All).ToString());
                    layout.addItem(FrontLayouts.None.ToDescriptionString(), ((int)FrontLayouts.None).ToString());
                    layout.addItem(FrontLayouts.LeftRight.ToDescriptionString(), ((int)FrontLayouts.LeftRight).ToString());
                    layout.addItem(FrontLayouts.LeftFooter.ToDescriptionString(), ((int)FrontLayouts.LeftFooter).ToString());
                    layout.addItem(FrontLayouts.RightFooter.ToDescriptionString(), ((int)FrontLayouts.RightFooter).ToString());
                    layout.addItem(FrontLayouts.Left.ToDescriptionString(), ((int)FrontLayouts.Left).ToString());
                    layout.addItem(FrontLayouts.Right.ToDescriptionString(), ((int)FrontLayouts.Right).ToString());
                    layout.addItem(FrontLayouts.Footer.ToDescriptionString(), ((int)FrontLayouts.Footer).ToString());                 
                    layout.Required = true;

                    table.addField(layout);
                }

                NetForm form = new NetForm();
                form.CssClass = "modfolderform";

                form.AddTable(lang);


                //Inserimento codice per il supporto multilingua
                #region Supporto multilingua
                WebControl langContols = new WebControl(HtmlTextWriterTag.Div);

                if (LanguageManager.Utility.Config.EnableMultiLanguage)
                {
                    IList<LanguageManager.Model.Language> languages = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages();
                    if (languages != null && languages.Count > 0)
                    {
                        string insertAfterId = "FoldersLang";
                        foreach (LanguageManager.Model.Language l in languages.Where(x => x.Default_Lang == 0))
                        {
                            #region Lang
                            Collapsable colTrad = new Collapsable("Traduzione " + l.Nome_Lang);
                            colTrad.ID = "FoldersLang#" + l.LangTwoLetterCode;
                            colTrad.StartCollapsed = true;
                                                        

                            NetTextBoxPlus nomeTrad = new NetTextBoxPlus("Nome " + l.Nome_Lang, "Label_FolderLang_" + l.LangTwoLetterCode);
                            nomeTrad.Required = false;
                            nomeTrad.MaxLenght = this.StructureNetwork.FolderNameMaxLenght;
                            nomeTrad.ContentType = NetTextBoxPlus.ContentTypes.Folder;

                            

                            NetTextBoxPlus DescrizioneTrad = new NetTextBoxPlus("Descrizione " + l.Nome_Lang, "Desc_FolderLang_" + l.LangTwoLetterCode);
                            DescrizioneTrad.Required = false;
                            DescrizioneTrad.MaxLenght = 2000;
                            DescrizioneTrad.Rows = 5;

                            if (this.FolderLangs.Count(x => x.Lang == l.ID) > 0)
                            {
                                FolderLang curLang = this.FolderLangs.Where(x => x.Lang == l.ID).First();
                                nomeTrad.Value = curLang.Label;
                                DescrizioneTrad.Value = curLang.Descrizione;
                            }

                            colTrad.AppendControl(nomeTrad.getControl());
                            colTrad.AppendControl(DescrizioneTrad.getControl());

                            langContols.Controls.Add(colTrad);

                            #endregion

                            form.ControlToInsertAfter.Add(new KeyValuePair<string, WebControl>(insertAfterId, colTrad));
                            insertAfterId = colTrad.ID;
                        }
                    }
                }
                #endregion

                form.AddTable(table);
                #endregion

                string errors = "";

                #region Postback
                if (PageData.IsPostBack && PageData.SubmitSource == "send")
               
                {                    
                    bool languageError = false;
                    if (LanguageManager.Utility.Config.EnableMultiLanguage)
                    {
                        IList<LanguageManager.Model.Language> languages = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages();
                        if (languages != null && languages.Count > 0)
                        {
                            foreach (LanguageManager.Model.Language l in languages.Where(x => x.Default_Lang == 0))
                            {
                                string nameTrad = HttpContext.Current.Request.Form["Label_FolderLang_" + l.LangTwoLetterCode].ToString();
                                string descTrad = HttpContext.Current.Request.Form["Desc_FolderLang_" + l.LangTwoLetterCode].ToString();

                                if (!string.IsNullOrEmpty(nameTrad) && !string.IsNullOrEmpty(descTrad))
                                {
                                    //Entrambi popolati, posso inserire la traduzione
                                    //Se Esiste modifico il record
                                    if (this.FolderLangs.Count(x => x.Lang == l.ID) > 0)
                                    {
                                        FolderLang langToModify = this.FolderLangs.First(x => x.Lang == l.ID);
                                        langToModify.Label = nameTrad;
                                        langToModify.Descrizione = descTrad;

                                        FolderBusinessLogic.SaveOrUpdateFolderLang(langToModify);
                                    }
                                    else
                                    {
                                        //Se non esiste lo creo
                                        FolderLang langToCreate = new FolderLang();
                                        langToCreate.Label = nameTrad;
                                        langToCreate.Descrizione = descTrad;
                                        langToCreate.Folder = this;
                                        langToCreate.Lang = l.ID;
                                        this.FolderLangs.Add(langToCreate);

                                        FolderBusinessLogic.SaveOrUpdateFolder(this);
                                    }
                                }
                                else
                                {
                                    //Se solo uno è popolato mostro un messaggio di errore affinchè vengano valorizzati entrambi
                                    if (!string.IsNullOrEmpty(nameTrad) || !string.IsNullOrEmpty(descTrad))
                                    {
                                        string messaggio = "I campi '<strong>Nome</strong>' e '<strong>Descrizione</strong>' devono essere compilati entrambi o nessuno per la lingua " + l.Nome_Lang;
                                        if (errors != null && errors.Length > 0)
                                            errors = errors.Replace("<ul>", "<ul><li>" + messaggio + "</li>");
                                        else
                                            errors = "<div class=\"form-error\"><ul><li>" + messaggio + "</li></ul></div>";
                                        languageError = true;
                                    }
                                    else
                                    {
                                        //Nessuno dei due è popolato, rimuovo la traduzione
                                        if (this.FolderLangs.Count(x => x.Lang == l.ID) > 0)
                                        {
                                            FolderLang langToDelete = this.FolderLangs.First(x => x.Lang == l.ID);
                                            this.FolderLangs.Remove(langToDelete);
                                            langToDelete.Folder = null;
                                            FolderBusinessLogic.DeleteFolderLang(langToDelete);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    
                        errors += form.serializedValidation();

                    if (string.IsNullOrEmpty(errors))
                    {
                        form.serializedUpdate();
                        //Controllo che non esistano documenti nella cartella genitore con lo stesso nome nuovo
                        bool notValidName = (this.Parent as StructureFolder).SubFolders
                                            .Count(folder => folder.Label.ToLower().Trim() == nome.RequestVariable.StringValue.ToLower().Trim() && folder.ID != this.ID) > 0;
                        if (notValidName)
                        {
                            string messaggio = "Non è possibile creare una cartella con questo nome in quanto esiste già un altra cartella con questo nome.";
                            if (errors != null && errors.Length > 0)
                                errors = errors.Replace("<ul>", "<ul><li>" + messaggio + "</li>");
                            else
                                errors = "<div class=\"form-error\"><ul><li>" + messaggio + "</li></ul></div>";
                        }
                    }
                    //NetUtility.RequestVariable label = new NetUtility.RequestVariable("Label_FolderLang", NetUtility.RequestVariable.RequestType.Form);
                    NetUtility.RequestVariable tipoHome = new NetUtility.RequestVariable("HomeType_Folder", NetUtility.RequestVariable.RequestType.Form);
                    if (tipoHome.IsValidInteger && //label.IsValid() && 
                        form.LastOperation == NetForm.LastOperationValues.OK && string.IsNullOrEmpty(errors))
                    {

                        if (HttpContext.Current.Request.Form["Stato_Folder"] != null && HttpContext.Current.Request.Form["Stato_Folder"].ToString() != "")
                            this.Stato = int.Parse(HttpContext.Current.Request.Form["Stato_Folder"].ToString());

                        NetUtility.RequestVariable rss = new NetUtility.RequestVariable("Rss_Folder", NetUtility.RequestVariable.RequestType.Form);

                        NetUtility.RequestVariable layout = new NetUtility.RequestVariable("Layout_Folder", NetUtility.RequestVariable.RequestType.Form);
                        if (!rss.IsValidInteger
                            || rss.IntValue == 2
                            || (rss.IntValue == 0 && this.StructureNetwork.CurrentFolder.Parent != null && (this.StructureNetwork.CurrentFolder.Parent as StructureFolder).EnableRss)
                            )
                        {
                            FileInfo rssFile = new FileInfo(HttpContext.Current.Server.MapPath(this.StructureNetwork.Paths.AbsoluteFrontRoot + this.StructureNetwork.CurrentFolder.Path + "/rss.xml"));
                            if (rssFile.Exists) rssFile.Delete();
                        }
                        else this.BuildRssRecursive();

                        if (this.StructureNetwork.CurrentFolder.RssValue != RssStatus.SystemError && rss.IsValidInteger)

                            this.Rss = rss.IntValue;
                        if (this.StructureNetwork.CurrentFolder.FrontLayout != FrontLayouts.SystemError && layout.IsValidInteger)
                            this.Layout = layout.IntValue;

                        if (this.Criteria[CriteriaTypes.Advanced].Allowed)
                        {
                            if (TitleOffset.RequestVariable.IsValidInteger)
                                this.TitleOffset = TitleOffset.RequestVariable.IntValue;
                            if (cssclass.RequestVariable.IsValidString)
                                this.CssClass = cssclass.RequestVariable.StringValue;
                        }
                        this.RestrictedInt = int.Parse(HttpContext.Current.Request["Restricted_Folder"].ToString());
                        this.HomeType = tipoHome.IntValue;

                        if (HttpContext.Current.Request.Form["ShowMenu_Folder"] != null)
                            this.ShowMenu = int.Parse(HttpContext.Current.Request.Form["ShowMenu_Folder"].ToString());
                        if (HttpContext.Current.Request.Form["ShowInTopMenu_Folder"] != null)
                            this.ShowInTopMenu = int.Parse(HttpContext.Current.Request.Form["ShowInTopMenu_Folder"].ToString());

                        FolderBusinessLogic.SaveOrUpdateFolder(this);

                        errors = "Aggiornamento Effettuato";

                        int docstatus = 0;                       

                        if (tipoHome.IntValue != 1 && tipoHome.IntValue != 3 )
                        {

                            switch (tipoHome.IntValue)
                            {
                                case 0:
                                    _HomeTypeEnum = HomepageTypes.TitleAndDesc;
                                    break;
                                case -1:
                                    _HomeTypeEnum = HomepageTypes.ContentList;
                                    break;
                                case 2:
                                    _HomeTypeEnum = HomepageTypes.LandingPage;
                                    break;
                                //case 3:
                                //    _HomeTypeEnum = HomepageTypes.SectionPage;
                                //    break;
                            }

                            this.CreateDefaultPage(WebDocsBusinessLogic.GetCurrentSession());

                            docstatus = 1;//Nascosto al sistema
                        }
                        else if (tipoHome.IntValue == 1 || tipoHome.IntValue == 3) // pagina personalizzata
                        {

                            switch (tipoHome.IntValue)
                            {
                                case 1:
                                    _HomeTypeEnum = HomepageTypes.CustomPage;
                                    break;
                                case 3:
                                    _HomeTypeEnum = HomepageTypes.SectionPage;
                                    break;
                            }

                            //NetCms.Structure.WebFS.Document homedoc = this.FindDocByID(tipoHome.IntValue) as WebDocsDocument;
                            //Document homedoc = this.FindDocByPhysicalName("default.aspx") as WebDocsDocument;
                            Document homedoc = NetCms.Structure.WebFS.FileSystemBusinessLogic.DocumentBusinessLogic.GetDocumentByPhysicalNameAndFolder("default.aspx", this);
                            if (homedoc != null)
                                homedoc.PublishContent();
                            else
                                // SE NON ESISTE IL DOCUMENTO DI DEFAULT PER LA CARTELLA CORRENTE
                                // LO CREO FORZATAMENTE IN MODO DA RISOLVERE IL BUG PAGINA PERSONALIZZATA 
                                this.CreateDefaultPage(WebDocsBusinessLogic.GetCurrentSession());

                            docstatus = 0;//Visibile al sistema
                        }

                        //Document docToUpdate = this.FindDocByPhysicalName("default.aspx") as Document;
                        Document docToUpdate = NetCms.Structure.WebFS.FileSystemBusinessLogic.DocumentBusinessLogic.GetDocumentByPhysicalNameAndFolder("default.aspx", this);

                        // ***************************************************************************************************************************
                        /* 
                         * Il codice sotto funziona solo quando il valore di AbsHome è settato con l'id di un documento valido e presente all'interno 
                           della cartella corrente.
                           Nel caso di default quando ho appena inserito il record ha come valore quello definito dal campo HomeType_Folder e settato a 0,1, o -1:
                           ipotizzando invece che la cartella possa contenere un documento col nome "default.aspx" - si esegue un ricerca al fine di settarne lo stato.
                        */
                        // ***************************************************************************************************************************

                        //NetCms.Structure.WebFS.Document docToUpdate = DocumentBusinessLogic.FindDocument(this, AbsHome.ToString(), DocumentBusinessLogic.SearchFor.ID);

                        //E' STATO INSERITO QUESTO CONTROLLO PERCHE' NEL CMS 2 LA QUERY DI AGGIORNAMENTO NON FACEVA ALCUN CONTROLLO SULL'ID
                        //SE SI SCEGLIE COME TIPO DI CARTELLA "PAGINA PERSONALIZZATA" O "INDICE E CONTENUTI" ABSHOME SARA' SEMPRE 1
                        //QUINDI IL DOCUMENTO TROVATO DALLA BUSINESSLOGIC SARA' NULL O IL DOCUMENTO DI ROOT
                        if (docToUpdate != null)
                        {
                            docToUpdate.Stato = 1;
                            docToUpdate.Hide = docstatus;
                            DocumentBusinessLogic.SaveOrUpdateDocument(docToUpdate);
                        }
                                               

                        #region invalido la struttura
                        //SISTEMARE CON CACHE 2° LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                        //Controllare l'invalidazione. Verificare se va invalidata tutta la cartella o solo 1 documento
                        //this.StructureNetwork.CurrentFolder.Invalidate();
                        this.Invalidate();
                        if (this.Parent != null)
                            ((StructureFolder)this.Parent).Invalidate();
                        #endregion


                        //NetCms.Front.NetworkMenu.

                    }
                    if (form.LastOperation == NetForm.LastOperationValues.OK && !languageError)
                        NetCms.GUI.PopupBox.AddSessionMessage(errors, PostBackMessagesType.Success, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                    else
                        NetCms.GUI.PopupBox.AddSessionMessage(errors, PostBackMessagesType.Error);

                }
                #endregion
                

                HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");

                HtmlGenericControl legend = new HtmlGenericControl("legend");
                legend.InnerText = "Proprietà Cartella ";
                fieldset.Controls.Add(legend);

                HtmlGenericControl contentDiv = new HtmlGenericControl("div");
                contentDiv.Attributes["class"] = "contentForm foldermod col-md-12";

                contentDiv.Controls.Add(lang.getForm());
                if (LanguageManager.Utility.Config.EnableMultiLanguage)
                {
                    contentDiv.Controls.Add(langContols);
                }

                contentDiv.Controls.Add(table.getForm());              

                HtmlGenericControl pButtons = new HtmlGenericControl("p");
                pButtons.Attributes["class"] = "buttonscontainer";

                Button bt = new Button();
                bt.Text = "Salva";
                bt.ID = "send";
                bt.CssClass = "button";
                bt.OnClientClick = "javascript: setSubmitSource('send')";
                pButtons.Controls.Add(bt);

                contentDiv.Controls.Add(pButtons);

                WebControl row = new WebControl(HtmlTextWriterTag.Div);
                row.CssClass = "row";

                row.Controls.Add(contentDiv);

                fieldset.Controls.Add(row);

                div.Controls.Add(fieldset);


            }
            else
                NetCms.GUI.PopupBox.AddSessionMessage("Creazione del documento di default fallita", PostBackMessagesType.Error);

            return div;
        }

        public override void CreateDefaultPage(ISession innerSession = null)
        {
            base.CreateDefaultPage();

            if (HomeTypeEnum == HomepageTypes.CustomPage || HomeTypeEnum == HomepageTypes.SectionPage)
            {
                int AbsHome = 0;

                Document tDoc =  NetCms.Structure.WebFS.FileSystemBusinessLogic.DocumentBusinessLogic.GetDocumentByPhysicalNameAndFolder("default.aspx", this, innerSession);

                //WebDocsDocument doc = (WebDocsDocument)this.FindDocByPhysicalName("default.aspx");
                if (tDoc != null)
                {
                    
                    AbsHome = tDoc.ID;
                }
                else
                {
                    AbsHome = CreateDefaultDoc(false, innerSession);
                }

                WebFS.Document docCreated = DocumentBusinessLogic.FindDocument(this,AbsHome.ToString(), DocumentBusinessLogic.SearchFor.ID);
                docCreated.Stato = 1;
                docCreated.Hide = 0;

                docCreated = DocumentBusinessLogic.SaveOrUpdateDocument(docCreated, innerSession);

                //string sql = "";
                //sql += "UPDATE Docs SET ";
                //sql += " Stato_Doc = 1";
                //sql += ", Hide_Doc = 0";
                //sql += " WHERE id_Doc = " + AbsHome + " AND Folder_Doc =  " + this.ID;
                //bool done = this.Network.Connection.Execute(sql);
                //if (done==false)
                if (docCreated == null)
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile inserire la WebDocs di default nel database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "48");
            
            }
        }

        //protected override int CreateDocContent(DataRow docRecord, DataSet sourceDB, string sourceFileName, params NetCms.Importer.Importer[] imp)
        //{
        //    string insertInto = "INSERT INTO " + this.Network.SystemName + "_docs_webdocs ";
        //    string insertIntoFields = "";
        //    string insertIntoValues = "";

        //    foreach (DataColumn col in docRecord.Table.Columns)
        //    {
        //        if (col.ColumnName.ToLower().EndsWith("_webdocs") && !col.ColumnName.ToLower().StartsWith("id_"))
        //        {
        //            string comma = insertIntoFields.Length > 0 ? "," : "";

        //            insertIntoFields += comma + col.ColumnName;

        //            string value = docRecord[col.ColumnName].ToString();

        //            if (value != "NULL")
        //                insertIntoValues += comma + "'" + value.Replace("'", "''") + "'";
        //            else
        //                insertIntoValues += comma + value.Replace("'", "''");
        //            //insertIntoValues += comma + "'" + value.Replace("'","''") + "'";
        //            //break;
        //        }
        //    }

        //    insertInto += "(" + insertIntoFields + ") VALUES (" + insertIntoValues + ")";
        //    int result = this.Network.Connection.ExecuteInsert(insertInto);
        //    if (result == -1)
        //        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile inserire nel database i dati della pagina Web", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "49");
        //    return result;
        //}

        public override bool DeleteModuloApplicativo(ISession session)
        {
            return HomePageBusinessLogic.DeleteModuliApplicativiByFolder(this.ID, session);
        }

        public override bool DeleteSomething(NHibernate.ISession session)
        {
            return true;
        }
    }
}
