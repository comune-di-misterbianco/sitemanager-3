﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.Applications.WebDocs
{
   public class WebDocsLabels :LabelsManager.Labels
    {
        public const String ApplicationKey = "webdocs";

        public WebDocsLabels() : base (ApplicationKey)
        {
        }
        public enum WebDocsLabelsList
        {
            Data,
            Titolo,
            Descrizione,
            Testo,
        }
    }
}
