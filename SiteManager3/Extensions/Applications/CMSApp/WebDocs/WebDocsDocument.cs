﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using NetForms;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using System.Collections.Generic;
using System.Linq;

namespace NetCms.Structure.Applications.WebDocs
{
    public class WebDocsDocument : NetCms.Structure.WebFS.Document
    {
        public override string FrontendLink
        {
            get { return (StructureFolder != null) ? StructureFolder.FrontendHyperlink + PhysicalName : ""; }
        }

        public override string Icon
        {
            get { return "icon_WebDocs"; }
        }

        private IList<WebDocsDocContent> _ApplicationRecords;
        public virtual IList<WebDocsDocContent> ApplicationRecords
        {
            get
            {
                if (_ApplicationRecords == null)
                {

                    _ApplicationRecords = WebDocsBusinessLogic.FindAllApplicationRecords();
                }
                return _ApplicationRecords;
            }
        }

        //private DataTable _ApplicationRecords;
        //public DataTable ApplicationRecords
        //{
        //    get
        //    {
        //        if (_ApplicationRecords == null)
        //        {

        //            _ApplicationRecords = this.Folder.Network.Connection.SqlQuery("SELECT * FROM Docs_WebDocs");
        //        }
        //        return _ApplicationRecords;
        //    }
        //}

        public virtual bool IsIndexPage
        {
            get
            {
                return this.PhysicalName == ("default.aspx");
            }
        }

        public override bool AddDoc
        {
            get
            {
                var folder = (this.Folder as WebDocs.WebDocsFolder);
                bool showWebDoc = folder.HomeTypeEnum == WebDocs.WebDocsFolder.HomepageTypes.CustomPage || folder.HomeTypeEnum == WebDocs.WebDocsFolder.HomepageTypes.SectionPage || !this.IsIndexPage;

                return base.AddDoc && showWebDoc;
            }
        }

        public override string Link
        {
            get { return FrontendLink; }
        }

        public WebDocsDocument()
            : base()
        { }

        public WebDocsDocument(Networks.NetworkKey network)
            : base(network)
        {
        }

        private bool MultilanguageLoaded;
        private bool _Multilanguage;
        private bool Multilanguage
        {
            get
            {
                return false;
            }
        }

        public override HtmlGenericControl getRowView()
        {
            return getRowView(false);
        }
        public override HtmlGenericControl getRowView(bool SearchView)
        {
            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina di elenco delle pagine web del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", callerObject: this);

            HtmlGenericControl tr = new HtmlGenericControl("tr");
            HtmlGenericControl td = new HtmlGenericControl("td");

            #region Icona
            /************************/
            td.InnerHtml = "&nbsp;";
            td.Attributes["class"] = "FS_Icon " + Icon;
            tr.Controls.Add(td);
            /************************/
            #endregion

            #region Nome & Link Alla Scheda
            /************************/
            string Link;

            if (!this.Grant)
                Link = this.Nome;
            else
                Link = BuildLink();

            td = new HtmlGenericControl("td");
            td.InnerHtml = Link;
            tr.Controls.Add(td);
            /************************/
            #endregion

            #region Cartella di apparteneza
            //*************************************
            if (SearchView)
            {
                td = new HtmlGenericControl("td");
                td.InnerHtml = (this.Folder as StructureFolder).LabelLinkPath;
                tr.Controls.Add(td);
            }
            //*************************************
            #endregion

            #region Data Oggetto
            //*************************************
            td = new HtmlGenericControl("td");
            td.InnerHtml = this.Date.ToString();
            if (td.InnerHtml.Length > 10) td.InnerHtml = td.InnerHtml.Remove(10);
            tr.Controls.Add(td);
            //*************************************
            #endregion

            #region Tipo Dell'oggetto
            //*************************************
            td = new HtmlGenericControl("td");
            td.InnerHtml = "WebDocs";
            tr.Controls.Add(td);
            //*************************************
            #endregion

            if (!SearchView)
            {
                #region Stato
                //*************************************
                td = new HtmlGenericControl("td");
                td.InnerHtml = this.StatusLabel;
                if ((this.Folder as StructureFolder).ReviewEnabled)
                    td.InnerHtml += " - " + (this.Content > 0 ? "Pubblicato" : "Non Pubblicato");
                tr.Controls.Add(td);
                //*************************************
                #endregion

                NetUtility.HtmlGenericControlCollection options = this.OptionsControls("webdocs");
                if (options != null)
                    foreach (HtmlGenericControl option in options)
                        tr.Controls.Add(option);
            }
            //else
            //{
                //#region Data
                ////*************************************
                //td = new HtmlGenericControl("td");
                //td.InnerHtml = this.Date;
                //tr.Controls.Add(td);
                ////*************************************
                //#endregion
            //}

            return tr;
        }

        public override DocContent ContentRecord
        {
            get
            {
                if (_ContentRecord == null)
                {

                    _ContentRecord = WebDocsBusinessLogic.GetById(this.Content);
                }
                return _ContentRecord;
            }
        }

        private string BuildLink()
        {
            //
//            string details = "<strong>" + this.Nome + "</strong><br />";
//            details += "<p><em>" + this.Descrizione.Replace(@"
//", " ") + "</em></p>";//ATTENZIONE L'accapo soprastante non è un errore di battitura ma una necessità in quanto rappresenta il carattere accapo


            //string data = this.Date.ToString(); ;
            //if (data.Length > 10) data = data.Remove(10);
            //details += "Data Creazione: " + data + "<br />";

            //string ultimamodifica = this.LastModify.ToString();
            //if (ultimamodifica.Length > 10) ultimamodifica = ultimamodifica.Remove(10);
            //details += "Data Ultima Modifica: " + ultimamodifica + "<br /><br />";

            //details = details.Replace("'", "\\\'");
            //details = details.Replace("\"", "&quot;");

            //string image = "";


            //if (NetCms.Users.AccountManager.CurrentAccount.GUI.PreviewType == NetCms.Users.SitemanagerUserGUI.PreviewTypes.Static)
            //   image = NetCms.GUI.GuiUtility.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Webdoc); //da vedere
         
            //string Link = "<a onmouseover=\"Over('" + image + "','" + details + "')\" onmouseout=\"Out()\"";
            string Link = "<a href=\"" + SiteManagerLink + "\">" + this.Nome + "</a>";

            return Link;
        }



        /* Metodi aggiunti */
        public override bool PublishContent(int ContentID)
        {
            return true;
        }

        public override HtmlGenericControl MoveDocView(PageData ExternalData)
        {
            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerHtml = "Spostamento del Documento";
            fieldset.Controls.Add(legend);

            StructureFolderBinder binder = new StructureFolderBinder(ExternalData);
            TreeNode root = binder.TreeFromStructure(ExternalData.RootFolder, 2);
            NetTreeView folders = new NetTreeView("Seleziona la cartella di destinazione", "Folder_ModuloApplicativo", root);
            folders.NotAllowedMsg = "Attenzione! La cartella selezionata non è di tipo Pagine Web o non si possiedono i permessi di acceso";
            folders.RootDist = ExternalData.RootDist;
            folders.CheckAllows = true;
            folders.Required = true;
            fieldset.Controls.Add(folders.getControl());

            Button move = new Button();
            move.ID = "Move";
            move.Text = "Sposta";
            fieldset.Controls.Add(move);

            if (ExternalData.IsPostBack)
            {
                NetUtility.RequestVariable MoveRV = new NetUtility.RequestVariable(move.ID, NetUtility.RequestVariable.RequestType.Form);
                if (MoveRV.IsValid() && MoveRV.StringValue == move.Text)
                {
                    NetUtility.RequestVariable FolderRV = new NetUtility.RequestVariable(folders.FieldName, NetUtility.RequestVariable.RequestType.Form);
                    if (FolderRV.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                    {
                        StructureFolder targetFolder = ExternalData.RootFolder.FindFolderByID(FolderRV.IntValue) as StructureFolder;
                        if (targetFolder != null)
                        {

                            bool moved = MoveDocDB(targetFolder) && MoveDocPhysical(targetFolder.Path, ExternalData);
                            if (moved)
                                PageData.MsgBox.InnerHtml = "Documento Spostato";
                            else
                            {
                                PageData.MsgBox.InnerHtml = "Si è verificato un errore: non sarà possibile spostare il documento. ";
                                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile spostare il documento di tipo WebDocs", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "46");
                            }
                        }
                        else
                        {
                            PageData.MsgBox.InnerHtml = "Si è verificato un errore: non sarà possibile spostare il documento.";
                            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile individuare la cartella di destinazione per spostare il documento di tipo WebDocs", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "47");
                        }
                    }
                    else
                    {
                        PageData.MsgBox.InnerHtml = "Si è verificato un errore: non sarà possibile spostare il documento.";
                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile individuare la cartella di destinazione per spostare il documento di tipo WebDocs", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "47");
                    }
                }
            }

            return fieldset;
        }


        public override bool MoveDocPhysical(string TargetFolderPath, PageData ExternalData)
        {
            string path = ExternalData.Paths.AbsoluteWebRoot + this.FullName;
            path = PageData.Server.MapPath(path);
            System.IO.FileInfo pagefile = new FileInfo(path);
            if (pagefile.Exists)
            {
                path = ExternalData.Paths.AbsoluteWebRoot + TargetFolderPath + "/" + this.PhysicalName;
                path = PageData.Server.MapPath(path);
                System.IO.FileInfo destfile = new FileInfo(path);
                if (!destfile.Exists)
                    pagefile.MoveTo(path);
            }
            else return false;

            path = ExternalData.Paths.AbsoluteWebRoot + this.FullName + ".cs";
            path = PageData.Server.MapPath(path);
            System.IO.FileInfo codefile = new FileInfo(path);
            if (codefile.Exists)
            {
                path = ExternalData.Paths.AbsoluteWebRoot + TargetFolderPath + "/" + this.PhysicalName + ".cs";
                path = PageData.Server.MapPath(path);
                codefile.MoveTo(path);
            }
            else return false;

            return true;
        }
    }
}
