﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using System.Reflection;
using NetCms.DBSessionProvider;
using NHibernate;


namespace NetCms.Structure.Applications.WebDocs
{
    public class Setup : NetCms.Structure.Applications.Setup
    {
        public Setup(NetCms.Structure.Applications.Application baseApp)
            : base(baseApp)
        {
        }

        public override string[,] Pages
        {
            get
            {
                string[,] pages = {
                                  { "webdocs_new.aspx", "NetCms.Structure.Applications.WebDocs.Pages.WebDocsAdd" },
                                  { "webdocs_move.aspx", "NetCms.Structure.Applications.WebDocs.DocMove" }
                                  };
                return pages;
            }
        }

        protected override bool LocalInstall(bool isInsidePackage = false)
        {
            bool done = true;
            try
            {
                FluentSessionProvider.Instance.UpdateSessionFactoryAdd(Assembly.GetAssembly(this.GetType()));
            }
            catch (Exception ex) 
            {
                this.RollBack();
                done = false;
            }
         
            return done;
        }
        
        private void RollBack()
        {
            bool done = false;
            if (!(NetworkID > 0))
            {
                using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = session.BeginTransaction())
                {
                    try
                    {
                        Application dbApp = ApplicationBusinessLogic.GetApplicationById(this.ApplicationBaseData.ID, session);
                        ApplicationBusinessLogic.DeleteApplication(dbApp, session);
                        tx.Commit();
                        done = true;
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                    }
                }
                
                if (done == false)
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile rimuovere l'Applicazione WebDocs dal database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "45");
            }

            //Da sostituire con meccanismo di scelta se eliminare o meno tutti i record dell'applicazione (usando stateless session)
            //foreach (DataRow row in this.NetworksTable.Select(FilterID))
            //{
            //    NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);
            //    string sql = string.Format("drop table if exists {0}_docs_webdocs", network.SystemName);
            //    Connection.Execute(sql);
            //}
            
        }
        public override void Uninstall()
        {
            base.Uninstall();

            //Da sostituire con meccanismo di scelta se eliminare o meno tutti i record dell'applicazione (usando stateless session)
            //foreach (DataRow row in this.NetworksTable.Select(FilterID))
            //{
            //    NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);
            //    string sql = string.Format("drop table if exists {0}_docs_webdocs", network.SystemName);
            //    Connection.Execute(sql);
            //}

            FluentSessionProvider.Instance.UpdateSessionFactoryRemove(Assembly.GetAssembly(this.GetType()));
        }
    }
}
