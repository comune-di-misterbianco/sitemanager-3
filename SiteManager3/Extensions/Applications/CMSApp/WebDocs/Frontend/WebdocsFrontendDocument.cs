using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Xml;
using System.Text;
using NetCms.Networks.WebFS;
using NetFrontend;
using NetCms.Structure.WebFS;


/// <summary>
/// Questa classe viene chiamata tramite Reflection
/// </summary>
namespace NetCms.Structure.Applications.WebDocs
{
    [FrontendDocumentsDefiner(2, "Pagine Web")]
    public class WebDocsFrontendDocument : FrontendDocument
    {
        public override string FrontendUrl
        {
            get { return this.Folder.FrontendUrl + this.Name; }
        }

        public WebDocsFrontendDocument(Document doc, FrontendFolder folder):base(doc, folder) {}
        public WebDocsFrontendDocument(int docID, FrontendFolder folder) : base(docID, folder) { }
    }
}