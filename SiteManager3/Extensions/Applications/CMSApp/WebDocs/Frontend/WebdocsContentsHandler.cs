using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using NetCms.Front;
using NetCms.Structure.Applications.WebDocs;
using System.Web.UI.WebControls;
using NetCms.Structure.WebFS;
using NetCms.Diagnostics;
using System;
using System.Xml;
using System.Linq;
using System.Collections.Generic;
using Frontend.Entities;
using LabelsManager;
using NetCms.Themes.Model;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;

/// <summary>
/// Questa classe viene chiamata tramite Reflection
/// </summary>
namespace NetFrontend
{
    public class WebdocsContentsHandler : ContentsHandler
    {
        private bool IsMainpage
        {
            get
            {
                return PageData.PageName.ToLower() == "default.aspx";
            }
        }      

        private WebDocsDocument DocRecord;
        private FrontendFolder currentFolder;

        private XmlNode _FrontConfigs;
        private XmlNode FrontConfigs
        {
            get
            {
                if (_FrontConfigs == null)
                {

                    _FrontConfigs = this.PageData.XmlConfigs.SelectSingleNode("/Portal/configs/Networks/" + this.PageData.Network.SystemName + "/applications/webdocs");
                }
                return _FrontConfigs;
            }
        }
        
        
        // TODO: ThemeEngine - Rimodulare il codice affinche legga la variabile globale anziche quella codificata nel config.xml per network???
        private FrontModeEnum _FrontMode;
        public FrontModeEnum FrontMode
        {
            get {
                if (FrontConfigs != null && FrontConfigs.ChildNodes.Count > 0)
                {
                    XmlNode nodeConf = FrontConfigs.SelectSingleNode("frontend");
                    if (nodeConf != null && nodeConf.Attributes["type"] != null)
                        _FrontMode = (nodeConf.Attributes["type"].Value == FrontModeEnum.Template.ToString().ToLower()) ? FrontModeEnum.Template : FrontModeEnum.Normal;
                }
                else
                {
                    _FrontMode = FrontModeEnum.Normal;
                }

                return _FrontMode;
            }
        }      

        public WebdocsContentsHandler(PageData pagedata) : base(pagedata)
        {
            currentFolder = PageData.Folder;
            if (CurrentDocumentID > 0)
                DocRecord = WebDocsBusinessLogic.GetDocumentRecord(CurrentDocumentID);
        }

        public override Control Control
        {
            get
            {
                if (IsMainpage)
                {
                    //string description =  currentFolder.Descrizione;
                    //this.PageData.HtmlHead.Controls.Add(new LiteralControl(" <meta content=\"" + description + "\" name=\"description\"/>"));

                    if (FrontMode == FrontModeEnum.Normal)
                    {
                        
                        if (currentFolder.Mainpage == FrontendFolder.MainpageTypes.Custom)
                        {
                            if (this.CurrentDocument != null)
                                return WebDocControl;
                            else
                                //throw new NetCms.Exceptions.PageNotFound404Exception(HttpContext.Current.Request.Url.ToString());
                                throw new HttpException(404, "File Not Found");
                        }
                        else
                            return TitleAndListControl;
                    }
                    else
                    {
                        return GetTemplateView();
                    }
                }
                else
                {

                    //string description = (string.IsNullOrEmpty(DocRecord.Descrizione)) ? DocRecord.Descrizione : currentFolder.Descrizione;
                    //this.PageData.HtmlHead.Controls.Add(new LiteralControl(" <meta content=\""+ description + "\" name=\"description\"/>"));

                    if (FrontMode == FrontModeEnum.Template)
                        return GetTemplateDetail();
                    else
                        return WebDocControl;
                }
            }
        }
        public override Control PreviewControl
        {
            get
            {
                DocRecord = WebDocsBusinessLogic.GetDocumentPreviewRecord(this.ContentID, this.PageData.Folder.ID); //NetFrontend.DAL.WebDocsDAL.GetDocumentPreviewRecord("id_WebDocs = " + this.ContentID + " AND Folder_Doc=" + this.PageData.Folder.ID);
                if (DocRecord != null)
                    return PreviewWebDocControl;

                //throw new NetCms.Exceptions.PageNotFound404Exception(HttpContext.Current.Request.Url.ToString());
                throw new HttpException(404, "File Not Found");
            }
        }

        private HtmlGenericControl TitleAndListControl
        {
            get
            {
                HtmlGenericControl divBlock = new HtmlGenericControl("div");
                divBlock.Attributes["class"] = "row docContent";

                WebControl col = new WebControl(HtmlTextWriterTag.Div);
                col.CssClass = "col-md-12";

                WebControl phblock = new WebControl(HtmlTextWriterTag.Div);
                phblock.CssClass = "page-header";

                WebControl title = new WebControl(HtmlTextWriterTag.H1);
                title.Controls.Add(new LiteralControl(currentFolder.Label));

                phblock.Controls.Add(title);

                WebControl descriptions = new WebControl(HtmlTextWriterTag.P);
                descriptions.Controls.Add(new LiteralControl(currentFolder.Descrizione));

                col.Controls.Add(phblock);
                col.Controls.Add(descriptions);

                if (currentFolder.Mainpage == FrontendFolder.MainpageTypes.List)
                {

                    WebControl divBlockLink = new WebControl(HtmlTextWriterTag.Div);
                    divBlockLink.CssClass = "row ContentMenuTree";

                    WebControl colBlockLink = new WebControl(HtmlTextWriterTag.Div);
                    colBlockLink.CssClass = "col-md-12";

                    ModelMenu topMenuOBJ = new NetworkMenu();
                    topMenuOBJ.ListType = ModelMenu.ListTypes.Div;
                    topMenuOBJ.FirstLevelAllwaysVisible = false;
                    topMenuOBJ.PrintDescriptions = true;

                    colBlockLink.Controls.Add(topMenuOBJ.GetInsideWebDocMenu("ColCX", currentFolder.Depth, 0));

                    divBlockLink.Controls.Add(colBlockLink);

                    col.Controls.Add(divBlockLink);
                }

                divBlock.Controls.Add(col);

                return divBlock;
            }
        }

        private HtmlGenericControl WebDocControl
        {
            get
            {
                HtmlGenericControl article = new HtmlGenericControl("article");
                article.Attributes["class"] = "article-webdoc";

                HtmlGenericControl divBlock = new HtmlGenericControl("div");
                divBlock.Attributes["class"] = "row docContent";

                WebControl col = new WebControl(HtmlTextWriterTag.Div);
                col.CssClass = "col-md-12";


                if (DocRecord == null && CurrentDocument != null)
                    DocRecord = WebDocsBusinessLogic.GetDocumentRecord(CurrentDocument.ID); //NetFrontend.DAL.WebDocsDAL.GetDocumentRecord(CurrentDocument.ID);
                if (DocRecord != null)
                {
                    WebDocsDocContent currentWebDocContent = DocRecord.ContentRecord as WebDocsDocContent;

                    //Nel caso in cui non � stato pubblicato il contenuto per la lingua scelta, prendo di default il contenuto della lingua predefinita, ad esempio l'italiano.
                    if (DocRecord.ContentRecord == null)
                    {
                        DocContent content = DocRecord.DocLangs.First(x => x.Lang == LanguageManager.BusinessLogic.LanguageBusinessLogic.GetDefaultLanguage().ID).Content;
                        currentWebDocContent = WebDocsBusinessLogic.GetById(content.ID);
                    }

                    if (!DocRecord.IsIndexPage)
                    {
                        // Consente l'inserimento del titolo del documento nelle briciole di pane
                        WhereBlock part = new WhereBlock(currentWebDocContent.DocLang.Label, "");
                        PageData.Where.Blocks.Add(part);
                    }
                    col.Controls.Add(new LiteralControl(currentWebDocContent.Testo.Replace("<%= WebRoot %>", PageData.AbsoluteWebRoot)));

                    if (NetCms.Configurations.Generics.EnableFrontContentDisclosure)
                    {
                        col.Controls.Add(Frontend.Common.Disclosure.ContentDisclosure.GetControl(CurrentDocument.ID, DocRecord.ID));
                    }

                }
                else
                    //throw new NetCms.Exceptions.PageNotFound404Exception(HttpContext.Current.Request.Url.ToString());
                    throw new HttpException(404, "File Not Found");

                divBlock.Controls.Add(col);
                article.Controls.Add(divBlock);
                return article;
            }
        }

        private Control PreviewWebDocControl
        {
            
            get
            {
                WebControl article = null;

                if (FrontMode == FrontModeEnum.Normal)
                {
                    //if (currentFolder.Mainpage == FrontendFolder.MainpageTypes.Custom)
                    //{

                        article = new WebControl(HtmlTextWriterTag.Div);
                        article.CssClass = "article-webdoc";

                        HtmlGenericControl divBlock = new HtmlGenericControl("div");
                        divBlock.Attributes["class"] = "row";

                        WebControl col = new WebControl(HtmlTextWriterTag.Div);
                        col.CssClass = "col-md-12";

                        WebDocsDocContent docContent = WebDocsBusinessLogic.GetById(this.ContentID);

                        if (docContent != null)
                            col.Controls.Add(new LiteralControl(docContent.Testo.Replace("<%= WebRoot %>", PageData.AbsoluteWebRoot)));
                        else
                            //throw new NetCms.Exceptions.PageNotFound404Exception(HttpContext.Current.Request.Url.ToString());
                            throw new HttpException(404, "File Not Found");

                        divBlock.Controls.Add(col);

                        article.Controls.Add(divBlock);
                        
                    //}
                }
                else
                    article = (DocRecord.IsIndexPage) ?  GetTemplateView(true) as WebControl :  GetTemplateDetail(true) as WebControl;

                return article;
            }
        }

        private Labels _WebDocsLabels;
        private Labels WebDocsLabels
        {
            get
            {
                if (_WebDocsLabels == null)
                {
                    _WebDocsLabels = LabelsCache.GetTypedLabels(typeof(WebDocsLabels)) as Labels;
                }
                return _WebDocsLabels;
            }
        }

        private Dictionary<string, string> _TemplateLabels;
        public Dictionary<string, string> TemplateLabels
        {
            get
            {
                if (_TemplateLabels == null)
                {
                    _TemplateLabels = new Dictionary<string, string>();
                    foreach (WebDocsLabels.WebDocsLabelsList label in Enum.GetValues(typeof(WebDocsLabels.WebDocsLabelsList)))
                        _TemplateLabels.Add(label.ToString().ToLower(), WebDocsLabels[label].ToString());
                }
                return _TemplateLabels;
            }
        }

        public Control GetTemplateView(bool isForPreview = false)
        {
            WebControl page = new WebControl(HtmlTextWriterTag.Div);
            page.CssClass = "wrapper";

            string result = string.Empty;
            
            Object sezione = null;

            string sectionmenu = "false";
            string srcsharecontent = "false";


            // TODO: da includere solo in AGID 2018
            if (currentFolder.ShowMenu)
            {
                ModelMenu constext_menu = new NetworkMenu();
                constext_menu.FirstLevelAllwaysVisible = false;
                sectionmenu = ControltoHtml(constext_menu.GetContextMenu("column", 
                                                                         currentFolder.Depth, 
                                                                         currentFolder.Depth + 1, 
                                                                         true, 
                                                                         "Linklist Linklist--padded u-layout-prose u-text-r-xs"));            
            }


            object sidebar = new { sidebarsx = this.PageData.Homepage.ShowColSX, sidebardx = this.PageData.Homepage.ShowColDX };         

            switch (currentFolder.Mainpage)
            {
                case FrontendFolder.MainpageTypes.Simple:
                    {

                        sezione = new { titolo = this.currentFolder.Label,
                                        descrizione = this.currentFolder.Descrizione, 
                                        url = this.currentFolder.FrontendUrl, 
                                        pagename = "default.aspx", 
                                        sidebar = sidebar  
                        };
                    }
                    break;
                case FrontendFolder.MainpageTypes.List:
                    {
                        ModelMenu topMenuOBJ = new NetworkMenu();
                        topMenuOBJ.ListType = ModelMenu.ListTypes.Div;
                        topMenuOBJ.FirstLevelAllwaysVisible = false;
                        topMenuOBJ.PrintDescriptions = true;

                        string items = ControltoHtml(topMenuOBJ.GetInsideWebDocMenu("ColCX", currentFolder.Depth, 0));   

                        sezione = new { titolo = this.currentFolder.Label, descrizione = this.currentFolder.Descrizione, items = items, url = this.currentFolder.FrontendUrl, pagename = "default.aspx", sidebar = sidebar };
                    }
                    break;
                case FrontendFolder.MainpageTypes.Custom:
                    {
                        if (DocRecord == null && CurrentDocument != null)
                        {
                            DocRecord = WebDocsBusinessLogic.GetDocumentRecord(CurrentDocument.ID);
                        }
                        if (DocRecord != null)
                        {
                            WebDocsDocContent currentWebDocContent = null;
                            if (!isForPreview)
                                currentWebDocContent = DocRecord.ContentRecord as WebDocsDocContent;
                            else
                                currentWebDocContent = WebDocsBusinessLogic.GetById(this.ContentID);

                            //Nel caso in cui non � stato pubblicato il contenuto per la lingua scelta, prendo di default il contenuto della lingua predefinita, ad esempio l'italiano.
                            if (DocRecord.ContentRecord == null)
                            {
                                DocContent content = DocRecord.DocLangs.First(x => x.Lang == LanguageManager.BusinessLogic.LanguageBusinessLogic.GetDefaultLanguage().ID).Content;
                                currentWebDocContent = WebDocsBusinessLogic.GetById(content.ID);
                            }

                            NetCms.Structure.CmsPathCodes coder = new NetCms.Structure.CmsPathCodes(PageData.AbsoluteWebRoot);

                            string testo = coder.DecodeWebRootReference(currentWebDocContent.Testo);
                            testo = ReplaceShortCode(testo);

                            // datadoc
                            string data_doc = "";
                            string date_rev = "";
                            string datadoc = (currentWebDocContent.DocLang != null) ? currentWebDocContent.DocLang.Document.Data.ToString() : "";
                            if (!string.IsNullOrEmpty(datadoc))
                            {
                                datadoc = datadoc.Split(' ')[0];
                                DateTime date_doc = DateTime.Parse(datadoc);
                                data_doc = date_doc.Day.ToString() + " " + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date_doc.Month) + " " + date_doc.Year.ToString();

                                // datarev
                                string data = currentWebDocContent.Revisione.Data.ToString();
                                data = data.Split(' ')[0];
                                DateTime date = DateTime.Parse(data);
                                date_rev = date.Day.ToString() + " " + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month) + " " + date.Year.ToString();
                            }


                        sezione = new { titolo = this.currentFolder.Label,
                                            descrizione = this.currentFolder.Descrizione,
                                            testo = testo,
                                            data = data_doc, 
                                            datarev = date_rev, 
                                            url = this.currentFolder.FrontendUrl,                                           
                                            tags = DocRecord.DocumentTags,
                                            pagename = "default.aspx",                                            
                                            sidebar = sidebar,
                                            menu = sectionmenu };


                            srcsharecontent = GetShareContentBlock(System.Web.HttpContext.Current.Server.UrlEncode(this.currentFolder.Label), this.currentFolder.FrontendUrl, System.Web.HttpContext.Current.Server.UrlEncode(this.currentFolder.Descrizione));
                        }
                        else                           
                            throw new HttpException(404, "File Not Found");
                    }
                    break;
                case FrontendFolder.MainpageTypes.Section:
                    {                         
                        if (DocRecord == null && CurrentDocument != null)                        
                            DocRecord = WebDocsBusinessLogic.GetDocumentRecord(CurrentDocument.ID);

                        if (DocRecord != null)
                        {
                            WebDocsDocContent currentWebDocContent = DocRecord.ContentRecord as WebDocsDocContent;
                            //Nel caso in cui non � stato pubblicato il contenuto per la lingua scelta, prendo di default il contenuto della lingua predefinita, ad esempio l'italiano.
                            if (DocRecord.ContentRecord == null)
                            {
                                DocContent content = DocRecord.DocLangs.First(x => x.Lang == LanguageManager.BusinessLogic.LanguageBusinessLogic.GetDefaultLanguage().ID).Content;
                                currentWebDocContent = WebDocsBusinessLogic.GetById(content.ID);
                            }

                            NetCms.Structure.CmsPathCodes coder = new NetCms.Structure.CmsPathCodes(PageData.AbsoluteWebRoot);

                            string testo = coder.DecodeWebRootReference(currentWebDocContent.Testo);                           
                            testo = ReplaceShortCode(testo);

                            // datadoc
                            string datadoc = currentWebDocContent.DocLang.Document.Data.ToString();
                            datadoc = datadoc.Split(' ')[0];
                            DateTime date_doc = DateTime.Parse(datadoc);
                            string data_doc = date_doc.Day.ToString() + " " + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date_doc.Month) + " " + date_doc.Year.ToString();

                            // datarev
                            string data = currentWebDocContent.Revisione.Data.ToString();
                            data = data.Split(' ')[0];
                            DateTime date = DateTime.Parse(data);
                            string date_rev = date.Day.ToString() + " " + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month) + " " + date.Year.ToString();

                            object doctags;

                            if (DocRecord.DocTags != null && DocRecord.DocTags.Any())
                            {
                                doctags = from tag in DocRecord.DocTags
                                          select new
                                          {
                                              label = tag.RifTag.Label,
                                              key = tag.RifTag.Key
                                          };
                            }
                            else
                            {
                                doctags = new { };
                            }
                      
                            sezione = new
                            {
                                titolo = this.currentFolder.Label,
                                descrizione = this.currentFolder.Descrizione,
                                testo = testo,
                                data = datadoc,
                                datarev = date_rev,
                                url = this.currentFolder.FrontendUrl,
                                menu = sectionmenu,                             
                                tags = doctags,
                                sidebar = sidebar,
                                pagename = "default.aspx"
                            };

                            srcsharecontent = GetShareContentBlock(System.Web.HttpContext.Current.Server.UrlEncode(this.currentFolder.Label),this.currentFolder.FrontendUrl, System.Web.HttpContext.Current.Server.UrlEncode(this.currentFolder.Descrizione));
                        }
                        else
                        {                         
                            Diagnostics.TraceMessage(TraceLevel.Error, "Rilevato un webdocs settato a null nella cartella " + currentFolder.FrontendUrl);
                            throw new HttpException(404, "File Not Found");
                        }
                    }
                    break;
                case FrontendFolder.MainpageTypes.Landing:
                    {
                        IEnumerable<FolderDTO> folders = null;

                        if (PageData.Folder.HasSubFolders)
                        {
                            StructureFolder currentFolders = FolderBusinessLogic.GetById(PageData.Folder.ID);

                            folders = (from folder in currentFolder.SubFolders
                                     where (folder.Type != 4 && (folder.StateValue == 0 || folder.StateValue == 4))
                                     orderby folder.Order descending
                                     select new FolderDTO
                                     {
                                         ID = folder.ID,
                                         Titolo = folder.Label,
                                         Descrizione = folder.Descrizione,
                                         Url = folder.FrontendUrl,
                                         Riffapp = folder.Type,
                                         Coverimage = folder.CoverImage,
                                         Cssclass = folder.CssClass,
                                         Childfolder = (from subfolder in folder.SubFolders
                                                        where (subfolder.Type != 4 && (subfolder.StateValue == 0 || subfolder.StateValue == 4))
                                                        orderby subfolder.Order descending
                                                        select new FolderDTO
                                                        {
                                                            ID = subfolder.ID,
                                                            Titolo = subfolder.Label,
                                                            Descrizione = subfolder.Descrizione,
                                                            Url = subfolder.FrontendUrl,
                                                            Riffapp = subfolder.Type,
                                                            Coverimage = subfolder.CoverImage,
                                                            Childfolder = null
                                                        }).ToList(),
                                         Order = folder.Order,
                                         Documents = (from doc in folder.Documents
                                                      where doc.Name != "default.aspx" && doc.Stato == 0
                                                      orderby doc.Order descending
                                                      select new DocumentDTO
                                                      {
                                                          ID = doc.ID,
                                                          Titolo = doc.Label,
                                                          Descrizione = doc.Descrizione,
                                                          Url = doc.FrontendUrl                                                         
                                                      }).ToList()
                                     }).ToList();

                        }

                        // lista dei documenti di front filtrati su webdocs (escluso la pagina indice) e links con stato Visibile in tutti i menu (0) e menu contestuale (4)
                        IEnumerable<FrontendDocument> frontDocument = this.currentFolder.Documents.Where(x => (x.Type == 2 || x.Type == 9) && (x.Stato == 0 || x.Stato == 4)).OrderByDescending(x => x.Order);

                        var docs = from doc in frontDocument
                                   select new
                                   {
                                       url = doc.FrontendUrl,
                                       titolo = doc.Label,
                                       descrizione = doc.Descrizione
                                   };


                        string CoverImage = "false";
                        if (currentFolder.CoverImage != null && !string.IsNullOrEmpty(currentFolder.CoverImage))
                            CoverImage = currentFolder.CoverImage;
                        
                        sezione = new {
                            titolo = this.currentFolder.Label,
                            descrizione = this.currentFolder.Descrizione,
                            url = this.currentFolder.FrontendUrl,                            
                            pagename = "default.aspx",                            
                            sidebar = sidebar,
                            folders = (folders!=null && folders.Any() ? folders:null),                            
                            coverimage = currentFolder.CoverImage,
                            pages = docs
                        };
                    }
                    break;
                default:
                    {
                        sezione = new { titolo = this.currentFolder.Label,
                                        descrizione = this.currentFolder.Descrizione,
                                        url = this.currentFolder.FrontendUrl,
                                        pagename = "default.aspx",
                                        sidebar = sidebar };
                    }
                    break;
            }
                    
            string tpl_path = base.GetContentTemplatePath(currentFolder.Mainpage.ToString(), "webdocs");

            result = NetCms.Themes.TemplateEngine.RenderTpl(sezione, srcsharecontent, tpl_path, TemplateLabels, CommonTemplateLabels);

            page.Controls.Add(new LiteralControl(result));

            return page;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="isForPreview">Indica se isForPreview = true viene usato per espore l'anteprima del documento dal backoffice</param>
        /// <returns></returns>
        public Control GetTemplateDetail(bool isForPreview = false)
        {
            WebDocsDocContent currentWebDoc;

            WebControl page = new WebControl(HtmlTextWriterTag.Div);
            page.CssClass = "wrapper";

            if (DocRecord == null)
            {            
                throw new HttpException(404, "File Not Found");
            }
            else if (isForPreview)
                currentWebDoc = WebDocsBusinessLogic.GetById(this.ContentID);
            else
            {
                currentWebDoc = DocRecord.ContentRecord as WebDocsDocContent;

                //Nel caso in cui non � stato pubblicato il contenuto per la lingua scelta, prendo di default il contenuto della lingua predefinita, ad esempio l'italiano.
                if (DocRecord.ContentRecord == null)
                {
                    DocContent content = DocRecord.DocLangs.First(x => x.Lang == LanguageManager.BusinessLogic.LanguageBusinessLogic.GetDefaultLanguage().ID).Content;
                    currentWebDoc = WebDocsBusinessLogic.GetById(content.ID);
                }
            }

            // Consente l'inserimento del titolo della news nelle briciole di pane
            WhereBlock part = new WhereBlock(currentWebDoc.Revisione.DocLang.Label, "");
            PageData.Where.Blocks.Add(part);

            string srcsharecontent = "false";

            NetCms.Structure.CmsPathCodes coder = new NetCms.Structure.CmsPathCodes(PageData.AbsoluteWebRoot);

            string testo = coder.DecodeWebRootReference(currentWebDoc.Testo);
            testo = ReplaceShortCode(testo); 

            string dataRev = currentWebDoc.Revisione.Data.ToString();            
            dataRev = dataRev.Split(' ')[0];

            DateTime dateParsed = DateTime.Parse(dataRev);
            string dateFormatted = dateParsed.Day.ToString() + " " + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dateParsed.Month) + " " + dateParsed.Year.ToString();

            object doctags;
            if (DocRecord.DocTags != null && DocRecord.DocTags.Any())
            {
                doctags = from tag in DocRecord.DocTags
                          select new
                          {
                              label = tag.RifTag.Label,
                              key = tag.RifTag.Key
                          };
            }
            else
            {
                doctags = new { };
            }

            var document = new {
                titolo = currentWebDoc.Revisione.DocLang.Label,
                testo = testo,
                data = dateFormatted,
                descrizione = currentWebDoc.Revisione.DocLang.Descrizione,        
                tags = doctags
            };

            srcsharecontent = GetShareContentBlock(System.Web.HttpContext.Current.Server.UrlEncode(this.currentFolder.Label), currentWebDoc.Revisione.DocLang.Document.FrontendLink, System.Web.HttpContext.Current.Server.UrlEncode(currentWebDoc.Revisione.DocLang.Descrizione));

            PageData.HtmlHead.Title = currentWebDoc.Revisione.DocLang.Label + " � " + PageData.HtmlHead.Title;
            
            var sezione = new { titolo = this.currentFolder.Label, url = this.currentFolder.FrontendUrl, pagename = "default.aspx" };            
            string tpl_path = base.GetContentTemplatePath("Dettaglio", "webdocs");
            
            var result = NetCms.Themes.TemplateEngine.RenderTpl(document, sezione, srcsharecontent, tpl_path, TemplateLabels, TemplateCommonLabels);
            
            page.Controls.Add(new LiteralControl(result));
            
            return page;
        }

        private ICollection<NetCms.Structure.WebFS.Documents.Tags.DocTag> GetDocTags()
        {
            return null;
        }
    }   
}