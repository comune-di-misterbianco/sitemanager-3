﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NHibernate;

namespace NetCms.Structure.Applications.WebDocs
{
    public class WebDocsReview : NetCms.Structure.WebFS.DocumentReviewPage
    {
        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Page_White_Add; }
        }
        public override string PageTitle
        {
            get { return "Nuova Revisione Pagina Web '" + this.CurrentDocument.Nome + "'"; }
        }

        public override bool AddPingSessionControl
        {
            get
            {
                return true;
            }
        }

        public WebDocsReview()
        {
            if (this.CurrentDocument == null)
            {
                //Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Pagina Web richiesta dall'utente non trovata", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                //HttpContext.Current.NetCms.Diagnostics.Diagnostics.Redirect("webdocs_error.aspx");
            }
        } 

        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "container-fluid";

            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");

            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerText = "Nuova Revisione";
            fieldset.Controls.Add(legend);

            HtmlGenericControl contentDiv = new HtmlGenericControl("div");
            contentDiv.Attributes["class"] = "contentForm webdocreviewform col-md-12";

            NetExternalField content;
            NetFormTable contentTable = WebDocsDocument.DocsContentFormTable(PageData);
            contentTable.CssClass = "netformtable_docs";
            NetFormTable WebDocs;
           
            
            PageData.CurentObjectInfoColtrol = new SideCurentObjectData("CurentDocSideInfo", this.CurrentDocument, PageData).getControl();
            if (ReviewID == 0)
                WebDocs = new NetFormTable("webdocs_docs_contents", "id_WebDocs", this.CurrentFolder.StructureNetwork.Connection);
            else
                WebDocs = new NetFormTable("webdocs_docs_contents", "id_WebDocs", this.CurrentFolder.StructureNetwork.Connection, this.ContentID);

            WebDocs.CssClass = "netformtable_webdocform";

            #region Campi
            //if (this.Account.GUI.Editor == NetCms.Users.SitemanagerUserGUI.RichTextEditors.FreeTextBox)
            //{
            //    NetRichTextBoxCMS Testo = new NetRichTextBoxCMS("Testo", "Testo_Webdocs", PageData);
            //    Testo.EditorWidth = NetRichTextBoxCMS.WebEditorWidth.Full;                        
            //    WebDocs.addField(Testo);
            //}
            //if (this.Account.GUI.Editor == NetCms.Users.SitemanagerUserGUI.RichTextEditors.TinyMCE)
            //{
            //    NetTinyMCE Testo = new NetTinyMCE("Testo", "Testo_Webdocs", PageData);
            //    Testo.EditorWidth = NetTinyMCE.WebEditorWidth.Full;
            //    Testo.ToolbarType = NetTinyMCE.PredefinedToobars.Full;
            //    WebDocs.addField(Testo);
            //}

            RichTextBox TestoCKEditor = new RichTextBox("Testo", "Testo_Webdocs");
            //TestoCKEditor.Value = "<p> <br /> </p>";
            TestoCKEditor.Required = true;
            TestoCKEditor.CurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID;
            WebDocs.addField(TestoCKEditor);
            #endregion

           
            PageData.CurentObjectInfoColtrol = new SideCurentObjectData("CurentDocSideInfo", this.CurrentDocument, PageData).getControl();

            NetForm form = new NetForm();
            form.AddTable(contentTable);
            form.AddTable(WebDocs);
            form.SubmitLabel = "Salva Revisione";

            content = new NetExternalField("docs_contents", "id_WebDocs");
            WebDocs.ExternalFields.Add(content);

            #region PostBack

            string errors = "";
            if (PageData.IsPostBack)
            {
                if (PageData.SubmitSource == "send")
                {
                    errors = form.serializedInsert(HttpContext.Current.Request.Form);
                    if (form.LastOperation == NetForm.LastOperationValues.OK)
                    {

                        using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                        using (ITransaction tx = sess.BeginTransaction())
                        {
                            try
                            {
                                DocumentOverview.insertReview(DocumentBusinessLogic.GetDocLangById(DocLang, sess), WebDocsBusinessLogic.GetById(contentTable.LastInsert, sess), this.ContentID, PageData, sess);
                                tx.Commit();
                            }
                            catch(Exception ex)
                            {
                                tx.Rollback();
                                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                            }
                        }
                    
                        string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha salvato una nuova revisione della Pagina Web '" + this.CurrentDocument.Nome + "' relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName, callerObject: this);
                    }

                    if (form.LastOperation == NetForm.LastOperationValues.Error)
                        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante la revisione della pagina web " + this.CurrentDocument.Nome + " del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "556");

                    //SISTEMARE CON CACHE 2° LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                    this.CurrentFolder.Invalidate();
                    NetCms.GUI.PopupBox.AddSessionMessage(errors, NetCms.GUI.PostBackMessagesType.Normal,true);
                }
            }
            else {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di creazione di una nuova revisione della Pagina Web '" + this.CurrentDocument.Nome + "' relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName, callerObject: this);
            }
            #endregion

            //div.Controls.Add(form.getForms("Nuova Revisione"));


            contentDiv.Controls.Add(contentTable.getForm());
            contentDiv.Controls.Add(WebDocs.getForm());

            HtmlGenericControl pButtons = new HtmlGenericControl("p");
            pButtons.Attributes["class"] = "buttonscontainer";

            Button bt = new Button();
            bt.Text = "Salva Revisione";
            bt.ID = "send";
            bt.CssClass = "button";
            bt.OnClientClick = "javascript: setSubmitSource('send')";
            pButtons.Controls.Add(bt);

            contentDiv.Controls.Add(pButtons);

            WebControl row = new WebControl(HtmlTextWriterTag.Div);
            row.CssClass = "row";

            row.Controls.Add(contentDiv);

            fieldset.Controls.Add(row);

            div.Controls.Add(fieldset);


            return div;

        }
    }
}
