﻿using System;
using System.Web;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace NetCms.Homepages
{
    public static class ModuliHomepageClassesPool
    {
        private const string ModuloFrontApplicationKey = "NetCms.Homepages.ModuliHomepageClassesPool.ModuloFrontApplicationKey";
        private const string ModuloCmsApplicationKey = "NetCms.Homepages.ModuliHomepageClassesPool.ModuloCmsAssembliesApplicationKey";
        private const string ModuloCmsFormApplicationKey = "NetCms.Homepages.ModuliHomepageClassesPool.ModuloCmsFormApplicationKey";

        public static Dictionary<string,Type> FrontendModuleClasses
        {
            get
            {
                if (_FrontendModuleClasses == null)
                {
                    if (HttpContext.Current.Application[ModuloFrontApplicationKey] != null)
                        _FrontendModuleClasses = (Dictionary<string, Type>)HttpContext.Current.Application[ModuloFrontApplicationKey];
                    else
                        HttpContext.Current.Application[ModuloFrontApplicationKey] = _FrontendModuleClasses = new Dictionary<string, Type>();
                }

                return _FrontendModuleClasses;
            }
        }
        private static Dictionary<string, Type> _FrontendModuleClasses;

        //public static Dictionary<string, Type> CmsModuleClasses
        //{
        //    get
        //    {
        //        if (_CmsModuleClasses == null)
        //        {
        //            if (HttpContext.Current.Application[ModuloCmsApplicationKey] != null)
        //                _CmsModuleClasses = (Dictionary<string, Type>)HttpContext.Current.Application[ModuloCmsApplicationKey];
        //            else
        //                HttpContext.Current.Application[ModuloCmsApplicationKey] = _CmsModuleClasses = new Dictionary<string, Type>();
        //        }

        //        return _CmsModuleClasses;
        //    }
        //}
        //private static Dictionary<string, Type> _CmsModuleClasses;


        public static Dictionary<string, ModuleTypeInfo> CmsModuleClasses
        {
            get
            {
                if (_CmsModuleClasses == null)
                {
                    if (HttpContext.Current.Application[ModuloCmsApplicationKey] != null)
                        _CmsModuleClasses = (Dictionary<string, ModuleTypeInfo>)HttpContext.Current.Application[ModuloCmsApplicationKey];
                    else
                        HttpContext.Current.Application[ModuloCmsApplicationKey] = _CmsModuleClasses = new Dictionary<string, ModuleTypeInfo>();
                }

                return _CmsModuleClasses;
            }

        }
        private static Dictionary<string, ModuleTypeInfo> _CmsModuleClasses;



        private static Dictionary<string, Type> _ActiveModules;
        public static Dictionary<string, Type> ActiveModules
        {
            get
            {
                if (_ActiveModules == null)
                {
                    _ActiveModules = new Dictionary<string, Type>();

                    var vApps = NetCms.Vertical.VerticalApplicationsPool.ActiveAssemblies.Cast<NetCms.Vertical.VerticalApplication>()
                               .Where(x => !x.IsSystemApplication && x.InstallState == Vertical.VerticalApplication.InstallStates.Installed)
                               .Select(g => new { AppID = g.ID, AppName = g.SystemName }).ToDictionary(item => item.AppID, item => item.AppName);

                    var cmsApp = NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies.Cast<NetCms.Structure.Applications.Application>()
                                .Where(x => x.InstallState == Structure.Applications.Application.InstallStates.Installed)
                                .Select(g => new { AppId = g.ID, AppName = g.SystemName }).ToDictionary(item => item.AppId, item => item.AppName);

                    var apps = vApps.Union(cmsApp).ToList().OrderBy(x => x.Key);

                    foreach (var item in CmsModuleClasses)
                    {
                        if (item.Value.ModuleType == HomepageModuleDefiner.ModuleType.Static)
                            _ActiveModules.Add(item.Key, item.Value.Types);
                        else
                            if (apps.Any(x => x.Key == item.Value.IdApp))
                            {
                                _ActiveModules.Add(item.Key, item.Value.Types);
                            }
                    }
                }
                return _ActiveModules;

            }

        }

        public static Dictionary<string, Type> CmsModuleFormsClasses
        {
            get
            {
                if (_CmsModuleFormsClasses == null)
                {
                    if (HttpContext.Current.Application[ModuloCmsFormApplicationKey] != null)
                        _CmsModuleFormsClasses = (Dictionary<string, Type>)HttpContext.Current.Application[ModuloCmsFormApplicationKey];
                    else
                        HttpContext.Current.Application[ModuloCmsFormApplicationKey] = _CmsModuleFormsClasses = new Dictionary<string, Type>();
                }

                return _CmsModuleFormsClasses;
            }
        }
        private static Dictionary<string, Type> _CmsModuleFormsClasses;

        public static void ParseType(Type type)
        {
            HomepageModuleDefiner attribute = System.Attribute.GetCustomAttributes(type).FirstOrDefault(x => x is HomepageModuleDefiner) as HomepageModuleDefiner;
            if (attribute != null)
            {
                if (type.HinheritsFrom("NetFrontend.Homepages.LayoutBlock"))
                    FrontendModuleClasses.Add(attribute.ID.ToString(), type);

                if (type.HinheritsFrom("NetCms.Structure.Applications.Homepage.HomepageModule"))
                {
                    //CmsModuleClasses.Add(attribute.ID.ToString(), type);

                    CmsModuleClasses.Add(attribute.ID.ToString(), new ModuleTypeInfo(type, attribute.RifIDApp, attribute.Type));
                }
                if (type.HinheritsFrom("NetCms.Structure.Applications.Homepage.HomepageModuleForm"))
                    CmsModuleFormsClasses.Add(attribute.ID.ToString(), type);
            }
        }

        public static bool HinheritsFrom(this Type childType, string parentType)
        {
            var type = childType;

            while (type != null)
            {
                if (type.FullName == parentType) return true;
                type = type.BaseType;
            }

            return false;
        }
    }

    public class ModuleTypeInfo
    {
        public ModuleTypeInfo(Type type, int appid, HomepageModuleDefiner.ModuleType moduleType)
        {
           Types = type;
           ModuleType = moduleType;
           IdApp = appid;
        }

        public Type Types
        {
            get;
            protected set;
        }

        public HomepageModuleDefiner.ModuleType  ModuleType
        {
            get;
            protected set;
        }

        public int IdApp
        {
            get;
            protected set;
        }
    }
   
    
    [System.AttributeUsage(System.AttributeTargets.Class)]    
    public class HomepageModuleDefiner : System.Attribute
    {
        /// <summary>
        /// Attributo per identificazione del modulo di homepage
        /// </summary>
        /// <param name="id">Identificativo univoco modulo</param>
        /// <param name="name">Etichetta modulo</param>
        /// <param name="rifAppid">Identificativo dell'applicazione correlata. Inserire -1 se il modulo non ha rifiermenti con applicazioni di cms o vapp.</param>
        /// <param name="type">Correlazione del modulo ad applicazione del framework. Impostare static se il modulo non ha rifiermenti con applicazioni di cms o vapp.</param>
        public HomepageModuleDefiner(int id, string name, int rifAppid, ModuleType type)
        {
            ID = id;
            Name = name;
            Type = type;
            RifIDApp = rifAppid;
        }

        public int ID { get; private set; }
        public string Name { get; private set; }
        public ModuleType Type { get; private set; }
        public int RifIDApp { get; private set; }

        public enum ModuleType 
        {
            Static,
            CmsApp,
            VerticalApp
        }
    }
}