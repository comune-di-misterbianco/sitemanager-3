﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace NetCms.Structure.Applications.Homepage
{
    public class HomepageApplication : NetCms.Structure.Applications.Application
    {
        public const string ApplicationContextFrontLabel = "Homepage.Frontend";
        public const string ApplicationContextBackLabel = "Homepage.Backoffice";

        public override string[] AllowedSubfolders
        {
            get { return new[] { "all" }; }
        }
        
        public const string InputFieldIDDelete = "HomepageHFDeleteModulo";
        public const string InputFieldIDDeleteContainer = "HomepageHFDeleteContainer";
        public const string InputFieldIDNewrow = "HomepageHFNewrow";
        public const string InputFieldIDSource = "HomepageHFSource";
        public const string InputFieldIDTarget = "HomepageHFTarget";
        public const string InputFieldIDAction = "HomepageHFAction";
        public const string InputFieldIDColumnStatus = "HomepageHFColumnStatus";

        public const int ApplicationID = 1;

        public override bool EnableNewsLetter
        { get { return true; } }
        public override bool EnableMove
        { get { return true; } }
        public override bool EnableModule
        { get { return false; } }
        public override bool EnableReviews
        {
            get { return false; }
        }
        public override string Namespace
        { get { return "NetCms.Structure.Applications.Homepage"; } }
        public override string SystemName
        { get { return "homepage"; } }
        public override string Label
        { get { return "Homepage"; } }
        public override string LabelSingolare
        { get { return "Homepage"; } }
        public override int ID
        { get { return ApplicationID; } }
        public override bool IsSystemApplication { get { return true; } }

        /*
        public override string FolderClassName
        {
            get { return "NetCms.Structure.Applications.Homepage.Folder"; }
        }
        public override string DetailsClassName
        {
            get { return "NetCms.Structure.Applications.Homepage.Manager"; }
        }
        public override string DocumentClassName
        {
            get { return "NetCms.Structure.Applications.Homepage.Document"; }
        }
        public override string HomepageModuleClassName
        {
            get { return null; }
        }
        public override string HomepageModuleFormClassName
        {
            get { return null; }
        }
        public override string OverviewClassName
        {
            get { return null; }
        }
        public override string ReviewClassName
        {
            get { return null; }
        }
        */
        public override NetCms.Structure.Applications.Setup Installer
        {
            get
            {
                if (_Installer == null)
                    _Installer = new Setup(this);
                return _Installer;
            }
        }
        private NetCms.Structure.Applications.Setup _Installer;

        public HomepageApplication()
            : base(null)
        {
            base.Assembly = Assembly.GetAssembly(this.GetType());
        }
        public HomepageApplication(Assembly assembly)
            : base(assembly)
        {
            this.ApplicationValidator.Validators.Add(new HomepageApplicationValidator(this));
        }



    }
}