﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Applications.Validation;

namespace NetCms.Structure.Applications.Homepage
{
    public class HomepageApplicationValidator : NetCms.Structure.Applications.Validation.ApplicationValidation
    {
        #region Sql

        private const string SelectFolderSql = @"SELECT * FROM folders INNER JOIN network_folders WHERE Parent_Folder = null
                                                    AND Path_Folder = ''
                                                    AND Tipo_Folder = 1
                                                    AND Stato_Folder = 0
                                                    AND Name_Folder = 'root'
                                                    AND Depth_Folder = 1
                                                    AND Trash_Folder = 0
                                                    AND Restricted_Folder = 0
                                                    AND Network_Folder = '{0}'
                                                    AND HomeType_Folder = 0
                                                    AND System_Folder = 0
                                                    AND Disable_Folder = 0
                                                    AND TitleOffset_Folder = 0
                                                    AND Order_Folder = 0
                                                    AND Rss_Folder = 1
                                                    AND Layout_Folder = 0";

        private const string SelectFolderLangSql = @"SELECT * FROM folderslang WHERE Lang_FolderLang = {0}
                                                     AND Folder_FolderLang = {1}
                                                     AND Label_FolderLang = 'Radice sito {2}'
                                                     AND Desc_Folderlang = 'Radice'";

        private const string SelectImgFolderSql = @"SELECT * FROM folders INNER JOIN network_folders WHERE Path_Folder = '/{0}'
                                                    AND Tipo_Folder = 4
                                                    AND Stato_Folder = 0
                                                    AND Depth_Folder = 2
                                                    AND Restricted_Folder = 0
                                                    AND Network_Folder = '{1}'
                                                    AND HomeType_Folder = 0
                                                    AND System_Folder = 0
                                                    AND TitleOffset_Folder = 0
                                                    AND CssClass_Folder = ''
                                                    AND Tree_Folder = '{2}'
                                                   ";

        private const string SelectImgFolderLangSql = @"SELECT * FROM folderslang WHERE Lang_FolderLang = {0}
                                                        AND Folder_FolderLang = {1}
                                                        AND Label_FolderLang = 'ImgHome'
                                                        AND Desc_Folderlang = 'Cartella immagini homepage'";

        private const string SelectHomeModel = @"SELECT * FROM homepages_docs_contents WHERE Nome_Homepage = 'Homepage {0}'
                                                 AND MetaDescription_homepage = ''
                                                 AND Keywords_homepage = ''
                                                 AND MsgManutenzione_homepage = 'Sezione attualmente in fase di manutenzione. Riprovare in seguito.'
                                                 AND Stato_homepage = 1";

        private const string SelectDocSql = @"SELECT * FROM docs INNER JOIN network_docs WHERE Name_Doc = 'default.aspx'
                                              AND Stato_Doc = 1
                                              AND Folder_Doc = {0}
                                              AND Descrizione_Doc = 'Homepage'
                                              AND Application_Doc = 1
                                              AND Autore_Doc = 1
                                              AND Trash_Doc = 0
                                              AND Hide_Doc = 0
                                              AND TitleOffset_Doc = 0
                                              AND CssClass_Doc = ''
                                              AND DefaultLang_Doc = {1}
                                              AND LangPickMode_Doc = 0
                                              AND Order_Doc = 0
                                              AND Layout_Doc = 0 ";

        private const string SelectDocLangSql = @"SELECT * FROM docs_lang WHERE Doc_DocLang = {0}
                                                  AND Label_DocLang = 'Homepage'
                                                  AND Desc_DocLang = 'HomePage del Portale {1}'
                                                  AND Lang_DocLang = {2}
                                                  AND Content_DocLang = {3} ";

        private const string SelectContainerModelSql = @"SELECT * FROM homepages_contenitori WHERE nome_contenitore = '{0}'
                                                         AND parent_contenitore = '{1}'
                                                         AND stato_contenitore = 1
                                                         AND homepage_contenitore = {2}
                                                         AND posizione_contenitore = 0
                                                         AND depth_contenitore = {3}
                                                         AND cssclass_contenitore = ''";
        #endregion

        public HomepageApplicationValidator(Application app)
            : base(app)
        {

        }

        public override bool Validate()
        {
            LocalCheck();
            foreach (var validation in this.ValidationResults)
            {
                if (validation.IsOk != ApplicationValidatorResult.ResultsTypes.Error) return false;
            }

            return true;
        }

        protected void LocalCheck()
        {
            foreach (var networkChached in Networks.NetworksManager.GlobalIndexer.CmsEnabled)
            {
                NetCms.Networks.BasicNetwork network = networkChached.Value;
                DataTable check;
               
                #region Cartella Homepage

                check = Application.Conn.SqlQuery(String.Format(SelectFolderSql, network.ID));

                if (check.Rows.Count != 1)
                    this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Cartella Homepage nella rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Cartella dell'HomePage non presente per la rete: '" + network.SystemName + "'"));
                else
                {
                    this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Cartella Homepage nella rete: '" + network.SystemName + "'", true, ""));

                    int folderID = Int32.Parse(check.Rows[0]["id_Folder"].ToString());
                    if (check.Rows[0]["Tree_Folder"].ToString() == "." + G2Core.Common.Utility.FormatNumber(folderID.ToString()) + ".")
                        this.ValidationResults.Add(new ApplicationValidatorResult("Correttezza Posizione Cartella Homepage nella rete: '" + network.SystemName + "'", true, ""));
                    else this.ValidationResults.Add(new ApplicationValidatorResult("Correttezza Posizione Cartella Homepage nella rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Cartella dell'HomePage non posizionata correttamente per la rete: '" + network.SystemName + "'"));

                    check = Application.Conn.SqlQuery(String.Format(SelectFolderLangSql, network.DefaultLang, folderID, network.Label));
                    if (check.Rows.Count != 1)
                        this.ValidationResults.Add(new ApplicationValidatorResult("Lingua Cartella Homepage nella rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Lingua per la Cartella dell'Homepage non presente o errata per la rete: '" + network.SystemName + "'"));
                    else this.ValidationResults.Add(new ApplicationValidatorResult("Lingua Cartella Homepage nella rete: '" + network.SystemName + "'", true, ""));

                #endregion

                    #region imghome

                    string FileSystemName = "imghome";
                    string path = "\\" + FileSystemName;

                    DirectoryInfo folder;
                    string phisical_path = HttpContext.Current.Server.MapPath(network.Paths.AbsoluteFrontRoot + path);
                    folder = new DirectoryInfo(phisical_path);

                    string adaptedAbsoluteWebRoot = network.Paths.AbsoluteFrontRoot.Length > 0 ? network.Paths.AbsoluteFrontRoot : "/";
                    string phisical_AbsoluteWebRoot = HttpContext.Current.Server.MapPath(adaptedAbsoluteWebRoot);

                    string[] paths = path.Split('/');

                    folder = new DirectoryInfo(phisical_AbsoluteWebRoot + path);
                    if (!folder.Exists || !File.Exists(phisical_path + "\\default.aspx"))
                        this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Cartella imghome su disco per la rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Cartella delle immagini dell'HomePage non presente correttamente su disco per la rete: '" + network.SystemName + "'"));
                    else
                    {
                        this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Cartella imghome su disco per la rete: '" + network.SystemName + "'", true, ""));

                        string sql = "SELECT * FROM folders INNER JOIN network_folders WHERE Name_Folder = '" + FileSystemName + "' AND Parent_Folder = " + folderID;
                        check = Application.Conn.SqlQuery(sql);

                        if (check.Rows.Count != 1)
                            this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Cartella imghome su database per la rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Cartella delle immagini dell'HomePage non presente su database per la rete: '" + network.SystemName + "'"));
                        else
                        {
                            this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Cartella imghome su database per la rete: '" + network.SystemName + "'", true, ""));

                            int newFolderID = Int32.Parse(check.Rows[0]["id_Folder"].ToString());

                            check = Application.Conn.SqlQuery(String.Format(SelectImgFolderSql, FileSystemName, network.ID, "." + NetUtility.TreeUtility.FormatNumber(folderID.ToString()) + "." + NetUtility.TreeUtility.FormatNumber(newFolderID.ToString()) + "."));
                            if (check.Rows.Count != 1)
                                this.ValidationResults.Add(new ApplicationValidatorResult("Correttezza Cartella imghome su database per la rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Cartella delle immagini dell'HomePage errata su database per la rete: '" + network.SystemName + "'"));
                            else
                            {
                                this.ValidationResults.Add(new ApplicationValidatorResult("Correttezza Cartella imghome su database per la rete: '" + network.SystemName + "'", true, ""));

                                check = this.Application.Conn.SqlQuery(String.Format(SelectImgFolderLangSql, network.DefaultLang, newFolderID));
                                if (check.Rows.Count != 1)
                                    this.ValidationResults.Add(new ApplicationValidatorResult("Lingua Cartella imghome per la rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Lingua per la Cartella delle immagini dell'Homepage non presente o errata per la rete: '" + network.SystemName + "'"));
                                else this.ValidationResults.Add(new ApplicationValidatorResult("Lingua Cartella imghome per la rete: '" + network.SystemName + "'", true, ""));
                            }
                        }
                    }

                    #endregion

                    #region Contenuto Homepage

                    check = Application.Conn.SqlQuery(String.Format(SelectHomeModel, network.SystemName));
                    if (check.Rows.Count != 1)
                        this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Contenuto Homepage per la rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning , "Contenuto Homepage non presente o errato per la rete: '" + network.SystemName + "'"));
                    else
                    {
                        this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Contenuto Homepage per la rete: '" + network.SystemName + "'", true, ""));

                        int homepageID = Int32.Parse(check.Rows[0]["ID_homepage"].ToString());
                        string sql = "SELECT Nome_homepage FROM homepages_docs_contents WHERE ID_homepage = " + homepageID;
                        DataTable nomeHomepage = Application.Conn.SqlQuery(sql);
                        string nomeHome = nomeHomepage.Rows[0]["Nome_homepage"].ToString();

                        check = Application.Conn.SqlQuery(String.Format(SelectDocSql, folderID, network.DefaultLang));
                        if (check.Rows.Count != 1)
                            this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Documento Homepage per la rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Documento Homepage non presente o errato per la rete: '" + network.SystemName + "'"));
                        else
                        {
                            this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Documento Homepage per la rete: '" + network.SystemName + "'", true, ""));

                            int docID = Int32.Parse(check.Rows[0]["id_Doc"].ToString());

                            check = Application.Conn.SqlQuery(String.Format(SelectDocLangSql, docID, network.Label, network.DefaultLang, homepageID));
                            if (check.Rows.Count != 1)
                                this.ValidationResults.Add(new ApplicationValidatorResult("Lingua Documento per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Lingua Documento Homepage non presente o errata per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'"));
                            else this.ValidationResults.Add(new ApplicationValidatorResult("Lingua Documento per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", true, ""));

                            check = Application.Conn.SqlQuery(String.Format(SelectContainerModelSql, "radice", "0", "0", homepageID, "0"));
                            if (check.Rows.Count != 1)
                                this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Root Container per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Root Container non presente o errato per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'"));
                            else
                            {
                                this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Root Container per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", true, ""));

                                int rootContainer = Int32.Parse(check.Rows[0]["ID_contenitore"].ToString());
                                string treeRoot = "." + G2Core.Common.Utility.FormatNumber(rootContainer.ToString()) + ".";

                                sql = "SELECT * FROM homepages_contenitori WHERE tree_contenitore = '" + treeRoot + "' AND ID_contenitore = " + rootContainer;
                                check = Application.Conn.SqlQuery(sql);

                                if (check.Rows.Count != 1)
                                    this.ValidationResults.Add(new ApplicationValidatorResult("Tree per il Root Container dell'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Tree per il Root Container dell'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "' non presente o errato"));
                                else this.ValidationResults.Add(new ApplicationValidatorResult("Tree per il Root Container dell'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", true, ""));

                                check = Application.Conn.SqlQuery(String.Format(SelectContainerModelSql, "Colsx", rootContainer, homepageID, "1"));
                                if (check.Rows.Count != 1)
                                    this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Colonna sinistra per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Colonna sinistra non presente o errata per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'"));
                                else
                                {
                                    this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Colonna sinistra per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", true, ""));

                                    int colsx = Int32.Parse(check.Rows[0]["ID_contenitore"].ToString());

                                    sql = "SELECT * FROM homepages_contenitori WHERE tree_contenitore = '" + treeRoot + G2Core.Common.Utility.FormatNumber(colsx.ToString()) + ".' AND ID_contenitore = " + colsx;
                                    check = Application.Conn.SqlQuery(sql);

                                    if (check.Rows.Count != 1)
                                        this.ValidationResults.Add(new ApplicationValidatorResult("Tree per la colonna sinistra dell'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Tree per la colonna sinistra dell'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "' non presente o errato"));
                                    else this.ValidationResults.Add(new ApplicationValidatorResult("Tree per la colonna sinistra dell'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", true, ""));
                                }

                                check = Application.Conn.SqlQuery(String.Format(SelectContainerModelSql, "Colcx", rootContainer, homepageID, "1"));
                                if (check.Rows.Count != 1)
                                    this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Colonna centrale per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Colonna centrale non presente o errata per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'"));
                                else
                                {
                                    this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Colonna centrale per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", true, ""));

                                    int colcx = Int32.Parse(check.Rows[0]["ID_contenitore"].ToString());

                                    sql = "SELECT * FROM homepages_contenitori WHERE tree_contenitore = '" + treeRoot + G2Core.Common.Utility.FormatNumber(colcx.ToString()) + ".' AND ID_contenitore = " + colcx;
                                    check = Application.Conn.SqlQuery(sql);

                                    if (check.Rows.Count != 1)
                                        this.ValidationResults.Add(new ApplicationValidatorResult("Tree per la colonna centrale dell'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Tree per la colonna centrale dell'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "' non presente o errato"));
                                    else this.ValidationResults.Add(new ApplicationValidatorResult("Tree per la colonna centrale dell'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", true, ""));
                                }

                                check = Application.Conn.SqlQuery(String.Format(SelectContainerModelSql, "Coldx", rootContainer, homepageID, "1"));
                                if (check.Rows.Count != 1)
                                    this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Colonna destra per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Colonna destra non presente o errata per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'"));
                                else
                                {
                                    this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Colonna destra per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", true, ""));

                                    int coldx = Int32.Parse(check.Rows[0]["ID_contenitore"].ToString());

                                    sql = "SELECT * FROM homepages_contenitori WHERE tree_contenitore = '" + treeRoot + G2Core.Common.Utility.FormatNumber(coldx.ToString()) + ".' AND ID_contenitore = " + coldx;
                                    check = Application.Conn.SqlQuery(sql);

                                    if (check.Rows.Count != 1)
                                        this.ValidationResults.Add(new ApplicationValidatorResult("Tree per la colonna destra dell'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Tree per la colonna destra dell'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "' non presente o errato"));
                                    else this.ValidationResults.Add(new ApplicationValidatorResult("Tree per la colonna destra dell'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", true, ""));
                                }

                                check = Application.Conn.SqlQuery(String.Format(SelectContainerModelSql, "Footer", rootContainer, homepageID, "1"));
                                if (check.Rows.Count != 1)
                                    this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Footer per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Footer non presente o errato per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'"));
                                else
                                {
                                    this.ValidationResults.Add(new ApplicationValidatorResult("Presenza Footer per l'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", true, ""));

                                    int footer = Int32.Parse(check.Rows[0]["ID_contenitore"].ToString());

                                    sql = "SELECT * FROM homepages_contenitori WHERE tree_contenitore = '" + treeRoot + G2Core.Common.Utility.FormatNumber(footer.ToString()) + ".' AND ID_contenitore = " + footer;
                                    check = Application.Conn.SqlQuery(sql);

                                    if (check.Rows.Count != 1)
                                        this.ValidationResults.Add(new ApplicationValidatorResult("Tree per il Footer dell'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", ApplicationValidatorResult.ResultsTypes.Warning, "Tree il Footer dell'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "' non presente o errato"));
                                    else this.ValidationResults.Add(new ApplicationValidatorResult("Tree per il Footer dell'Homepage: '" + nomeHome + "' della rete: '" + network.SystemName + "'", true, ""));
                                }
                            }
                        }
                    }
                    #endregion

                }
            }
        }
    }
}