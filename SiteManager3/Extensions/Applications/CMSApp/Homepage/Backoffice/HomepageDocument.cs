using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using NetCms.Structure.WebFS;
using NetCms.Structure.Applications.Homepage.Model;
using System.Collections.Generic;
using NetCms.Grants;

/// <summary>
/// Summary description for FilesDocument
/// </summary>
namespace NetCms.Structure.Applications.Homepage
{
    public class HomepageDocument : NetCms.Structure.WebFS.Document
    {
        private bool Multilanguage
        {
            get
            {
                return false;
            }
        }

        public override string FrontendLink
        {
            get { return (this.Folder as StructureFolder).FrontendHyperlink; }
        }

        public override bool AllowDelete
        {
            get {
                if (NetCms.Configurations.Debug.GenericEnabled || NetCms.Users.AccountManager.CurrentAccount.God == 1)
                    return true;
                return false;             
            }
        }
        public override string Icon
        {
            get { return "icon_homepage"; }
        }

        private IList<HomePageDocContent> _ApplicationRecords;
        public virtual IList<HomePageDocContent> ApplicationRecords
        {
            get
            {
                if (_ApplicationRecords == null)
                {
                    _ApplicationRecords =  HomePageBusinessLogic.FindAllApplicationRecords();
                }
                return _ApplicationRecords;
            }
        }      

        public override string Link
        {
            get { return this.Folder.Path + "/default.aspx?news=" + ID; }
        }

        public HomepageDocument()
            : base()
        { }

        public HomepageDocument(Networks.NetworkKey network)
            : base(network)
        {
        }

        public override HtmlGenericControl getRowView()
        {
            return getRowView(false);
        }

        public override HtmlGenericControl getRowView(bool SearchView)
        {
            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla radice del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");

            HtmlGenericControl tr = new HtmlGenericControl("tr");
            HtmlGenericControl td = new HtmlGenericControl("td");

            #region Icona
            /************************/
            td.InnerHtml = "&nbsp;";
            td.Attributes["class"] = "FS_Icon " + Icon;
            tr.Controls.Add(td);
            /************************/
            #endregion

            #region Nome & Link Alla Scheda
            /************************/
            string Link;

            if (this.Grant && Criteria[CriteriaTypes.Modify].Allowed)
                Link = BuildLink(); 
            else 
                Link = this.Nome;                

            td = new HtmlGenericControl("td");
            td.InnerHtml = Link;
            tr.Controls.Add(td);
            /************************/
            #endregion

            #region Cartella di apparteneza
            //*************************************
            if (SearchView)
            {
                td = new HtmlGenericControl("td");
                td.InnerHtml = (this.Folder as StructureFolder).LabelLinkPath;
                tr.Controls.Add(td);
            }
            //*************************************
            #endregion

            #region Data Oggetto
            //*************************************
            td = new HtmlGenericControl("td");
            td.InnerHtml = this.Date.ToString();
            if (td.InnerHtml.Length > 10) td.InnerHtml = td.InnerHtml.Remove(10);
            tr.Controls.Add(td);
            //*************************************
            #endregion

            #region Tipo Dell'oggetto
            //*************************************
            td = new HtmlGenericControl("td");
            td.InnerHtml = "Homepage";
            tr.Controls.Add(td);
            //*************************************
            #endregion

            if (!SearchView)
            {
                #region Stato
                //*************************************
                td = new HtmlGenericControl("td");
                td.InnerHtml = this.StatusLabel;
                tr.Controls.Add(td);
                //*************************************
                #endregion

                NetUtility.HtmlGenericControlCollection options = this.OptionsControls("homepage", "gest_homepage");
                if (options != null)
                    foreach (HtmlGenericControl option in options)
                        tr.Controls.Add(option);
            }
            //else
            //{
                //#region Data
                //*************************************
                //td = new HtmlGenericControl("td");
                //td.InnerHtml = this.Date;
                //tr.Controls.Add(td);
                //*************************************
            //    #endregion
            //}

            return tr;
        }       

        public override DocContent ContentRecord
        {
            get
            {
                if (_ContentRecord == null)
                {
                    _ContentRecord = HomePageBusinessLogic.GetHomepageContentById(this.Content);
                }
                return _ContentRecord;
            }
        }

        private string BuildLink()
        {
            string Link = "<a href=\"" + PageData.Paths.AbsoluteNetworkRoot + "/cms/applications/" + (this.Folder as StructureFolder).RelativeApplication.SystemName + "/modify.aspx?folder=" + this.Folder.ID + "&amp;doc=" + this.ID + "\">" + this.Nome + "</a>";
            return Link;
        }
    }
}