using System;
using System.Xml;
using System.IO;
using NetCms.Structure.WebFS;
using NHibernate;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Structure.Applications.Homepage
{
    public class RssManager : NetCms.RssManager
    {
        public RssManager(string link, string title, string desc, StructureFolder folder)
            : base(link, title, desc, folder)
        {
        }
        public override void AddCustomContents(RssItem item, NetCms.Structure.WebFS.Document doc, ISession session = null)
        {
            item.Category = "News";
            item.PubDate = doc.Date;
        }
        //public override RssDocument InitDocument()
        //{
        //    RssDocument newDoc = new RssDocument(Link, Title, Desc);
        //    newDoc.Category = "News";
        //    newDoc.PubDate = DateTime.Now.ToShortDateString();
        //    newDoc.Copyright = Folder.PageData.Network.Label;
        //    newDoc.WebMaster = Folder.PageData.Network.eMail;
        //    newDoc.LastBuildDate = DateTime.Now.ToShortDateString();

        //    return newDoc;
        //}
    }
}