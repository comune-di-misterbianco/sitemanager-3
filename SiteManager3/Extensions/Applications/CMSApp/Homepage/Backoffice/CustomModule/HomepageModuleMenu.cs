using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Connections;
using NetForms;
using NetCms.Structure.Applications.Homepage.Model;

/// <summary>
/// Summary description for LayoutModuloBlock
/// </summary>
/// 
namespace NetCms.Structure.Applications.Homepage
{
    [NetCms.Homepages.HomepageModuleDefiner(2, "Menu", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuleMenu : NetCms.Structure.Applications.Homepage.HomepageModule
    {
        protected override string LocalCssClass
        {
            get { return " HomepageModulo_Menu"; }
        }
        public override string Title
        {
            get { return "Modulo Menu"; }
        }

        private int Starting;
        private int Ending;
        private int Formato;

        public HomepageModuleMenu(Modulo module)
            : base(module)
        {
            ModuloMenu modulo = HomePageBusinessLogic.GetModuloMenuById(module.ID);

            Starting = modulo.Starting;
            Ending = modulo.Ending;
            Formato = modulo.Type;
        }

        public HomepageModuleMenu(ModuloMenu modulo)
            : base(modulo)
        {
            //DataTable table = Connection.SqlQuery("SELECT * FROM Homepages_Moduli_Menu WHERE Modulo_ModuloMenu =" + row["id_Modulo"].ToString());
            //Starting = int.Parse(table.Rows[0]["Starting_ModuloMenu"].ToString());
            //Ending = int.Parse(table.Rows[0]["Ending_ModuloMenu"].ToString());
            //Formato = int.Parse(table.Rows[0]["Type_ModuloMenu"].ToString());

            Starting = modulo.Starting;
            Ending = modulo.Ending;
            Formato = modulo.Type;
        }

        protected override HtmlGenericControl GetAdminContent()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");

            HtmlGenericControl h3 = new HtmlGenericControl("h3");

            h3.InnerHtml = "Menu";
            div.Controls.Add(h3);

            HtmlGenericControl p = new HtmlGenericControl("p");
            p.InnerHtml = "Codice Modulo : #" + NetUtility.TreeUtility.FormatNumber(this.ID.ToString(), 4) + " <br />";
            p.InnerHtml += "Livello partenza: '" + Starting + "' <br />";
            p.InnerHtml += " Livello massimo: '" + (Ending > 0 ? Ending.ToString() : "Infinito") + "'<br />";
            p.InnerHtml += " Formato: '" + (Formato == 0 ? "Elastico" : (Formato == 1 ? "Esploso" : "Agid")) + "'";
            div.Controls.Add(p);

            return div;


        }

    }
    [NetCms.Homepages.HomepageModuleDefiner(2, "Menu", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuleMenuForm : NetCms.Structure.Applications.Homepage.HomepageModuleForm
    {
        public HomepageModuleMenuForm(int id)
            : base(id)
        {
        }
        public HomepageModuleMenuForm()
            : base()
        {
        }
        public override NetForms.NetFormTable GetFormTable()
        {
            NetFormTable table2;
            DataRow row = null;
            if (ID != 0)
            {
                int id_ModuloMenu = 0;
                DataTable table = this.Conn.SqlQuery("SELECT * FROM homepages_moduli_menu WHERE Modulo_ModuloMenu = " + ID);
                if (table.Rows.Count > 0 && int.TryParse(table.Rows[0]["Modulo_ModuloMenu"].ToString(), out id_ModuloMenu))
                    row = table.Rows[0];
                //else PageData.GoBack();
                //Trace

                table2 = new NetFormTable("homepages_moduli_menu", "Modulo_ModuloMenu", this.Conn, id_ModuloMenu);
            }
            else
            {
                table2 = new NetFormTable("homepages_moduli_menu", "Modulo_ModuloMenu", this.Conn);
                NetExternalField mod = new NetExternalField("homepages_moduli", "Modulo_ModuloMenu");
                table2.addExternalField(mod);
            }


            int selectedModel = 3;
            if (!table2.IsPostBack && row != null)
            {
                selectedModel = 1;
                int RetrievedStart = int.Parse(row["Starting_ModuloMenu"].ToString());
                int RetrievedEnd = int.Parse(row["Ending_ModuloMenu"].ToString());
                int RetrievedFirst = int.Parse(row["FirstLevel_ModuloMenu"].ToString());

                if (RetrievedStart == 1 && RetrievedEnd == 0 && RetrievedFirst == 0)
                    selectedModel = 2;
                if (RetrievedStart == 2 && RetrievedEnd == 0 && RetrievedFirst == 0)
                    selectedModel = 3;
                if (RetrievedStart == 1 && RetrievedEnd == 1 && RetrievedFirst == 1)
                    selectedModel = 4;
            }

            NetDropDownList Model = new NetDropDownList("Modello", "Modello");
            Model.AutoPostBack = true;
            Model.Required = true;
            Model.ClearList();
            Model.addItem("A partire dal Secondo Livello (Men� laterale in caso di aree tematiche)", "3", selectedModel == 3);
            Model.addItem("Solo Primo Livello (Aree Tematiche)", "4", selectedModel == 4);
            Model.addItem("A partire dal Primo Livello (Men� Completo)", "2", selectedModel == 2);
            Model.addItem("Personalizzato (Per utenti esperti)", "1", selectedModel == 1);
            Model.JumpField = true;
            table2.addField(Model);



            if (table2.IsPostBack && Model.RequestVariable.IsValidInteger)
            {
                if (Model.RequestVariable.IntValue == 1)
                    selectedModel = 1;
                else
                    if (Model.RequestVariable.IntValue > 1)
                        selectedModel = Model.RequestVariable.IntValue;
            }

            if (selectedModel == 1)
            {
                NetTextBox starting = new NetTextBox("Livello di inizio del men�(radice = 1): ", "Starting_ModuloMenu");
                starting.Required = true;
                starting.Numeric = true;
                table2.addField(starting);

                NetTextBox ending = new NetTextBox("Livello di fine del men�(radice = 1,infinito=0,mostra un solo livello = 'stesso numero di livello di inizio'): ", "Ending_ModuloMenu");
                ending.Required = true;
                ending.Numeric = true;
                table2.addField(ending);

                NetCheckBox first = new NetCheckBox("Mostra sempre il primo livello", "FirstLevel_ModuloMenu");
                table2.addField(first);
            }
            else
                if (selectedModel > 1)
                {
                    int startValue = 1;
                    int endValue = 0;
                    int firstValue = 0;

                    switch (selectedModel)
                    {
                        case 2:
                            startValue = 1;
                            endValue = 0;
                            firstValue = 0;
                            break;
                        case 3:
                            startValue = 2;
                            endValue = 0;
                            firstValue = 0;
                            break;
                        case 4:
                            startValue = 1;
                            endValue = 1;
                            firstValue = 1;
                            break;
                    }

                    NetHiddenField Starting;
                    NetHiddenField Ending;
                    NetHiddenField First;

                    Starting = new NetHiddenField("Starting_ModuloMenu");
                    Starting.Value = startValue.ToString();
                    table2.addField(Starting);

                    Ending = new NetHiddenField("Ending_ModuloMenu");
                    Ending.Value = endValue.ToString();
                    table2.addField(Ending);

                    First = new NetHiddenField("FirstLevel_ModuloMenu");
                    First.Value = firstValue.ToString();
                    table2.addField(First);
                }


            NetDropDownList Type = new NetDropDownList("Tipo", "Type_ModuloMenu");
            Type.Required = true;
            Type.ClearList();
            Type.addItem("Elastico (I rami si aprono con la navigazione)", "0");
            Type.addItem("Esploso (Tutti i rami gi� aperti)", "1");
            Type.addItem("TreeView (Tutti i rami gi� aperti)", "2");
            Type.addItem("Sezione (Solo i rami locali)", "3");
            table2.addField(Type);

            return table2;
        }
    }
}