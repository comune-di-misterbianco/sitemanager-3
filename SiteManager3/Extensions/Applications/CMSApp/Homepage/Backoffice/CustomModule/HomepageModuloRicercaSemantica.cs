using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using NetForms;
using NetCms.Structure.Applications.Homepage.Model;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for LayoutModuloBlock
/// </summary>
/// 
namespace NetCms.Structure.Applications.Homepage
{
    [NetCms.Homepages.HomepageModuleDefiner(66, "Ricerca Semantica", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuloRicercaSemantica : NetCms.Structure.Applications.Homepage.HomepageModule
    {
        protected override string LocalCssClass
        {
            get { return " HomepageModulo_RicercaSemantica"; }
        }
        public override string Title
        {
            get { return "Modulo Ricerca Semantica"; }
        }

        public bool ShowTitle;
        public int MaxDocs;

        public HomepageModuloRicercaSemantica(Modulo module)
            : base(module)
        {
            ModuloRicercaSemantica modulo = HomePageBusinessLogic.GetModuloRicercaSemaById(module.ID);
            ShowTitle = modulo.ShowTitle;
            MaxDocs = modulo.MaxDocument;
        }

        public HomepageModuloRicercaSemantica(ModuloRicercaSemantica modulo):base(modulo)
        {
            ShowTitle = modulo.ShowTitle;
            MaxDocs = modulo.MaxDocument;
        }

        protected override HtmlGenericControl GetAdminContent()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");


            HtmlGenericControl h3 = new HtmlGenericControl("h3");

            h3.InnerHtml = "Modulo Ricerca Semantica";
            div.Controls.Add(h3);

            HtmlGenericControl p = new HtmlGenericControl("p");
            p.InnerHtml = "Codice Modulo : #" + NetUtility.TreeUtility.FormatNumber(this.ID.ToString(), 4) + " <br />";
            div.Controls.Add(p);

            return div;


        }

    }
    [NetCms.Homepages.HomepageModuleDefiner(66, "Ricerca Semantica", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuleRicercaSemanticaForm : NetCms.Structure.Applications.Homepage.HomepageModuleForm
    {
        public HomepageModuleRicercaSemanticaForm(int id)
            : base(id)
        {
        }
        public HomepageModuleRicercaSemanticaForm()
            : base()
        {
        }
        public override NetForms.NetFormTable GetFormTable()
        {

            NetFormTable modulo;
            if (ID != 0)
            {
                int id_modulo_notifiche = 0;
                DataTable table = this.Conn.SqlQuery("SELECT * FROM homepages_moduli_ricerca_semantica WHERE Modulo_RicercaSemantica = " + this.ID);
                if (table.Rows.Count > 0 && int.TryParse(table.Rows[0]["Modulo_RicercaSemantica"].ToString(), out id_modulo_notifiche)) ;
                //else PageData.GoBack();
                //Trace

                modulo = new NetFormTable("homepages_moduli_ricerca_semantica", "Modulo_RicercaSemantica", this.Conn, id_modulo_notifiche);
            }
            else
            {
                modulo = new NetFormTable("homepages_moduli_ricerca_semantica", "Modulo_RicercaSemantica", this.Conn);
                NetExternalField mod = new NetExternalField("homepages_moduli", "Modulo_RicercaSemantica");
                modulo.addExternalField(mod);
            }

            NetCheckBox showTitle = new NetCheckBox("Mostra titolo modulo", "ShowTitle_RicercaSemantica");
            modulo.addField(showTitle);

            NetDropDownList maxDocs = new NetDropDownList("Numero di record da mostrare", "MaxDocs_RicercaSemantica");
            maxDocs.Required = true;
            for (int i = 1; i <= 10; i++)
                maxDocs.addItem(i.ToString(), i.ToString());
            modulo.addField(maxDocs);

            return modulo;

            /*
             NetFormTable modulo;
             if (ID != 0)
             {
                 int id_modulo_rss = 0;
                 DataTable table = this.Connection.SqlQuery("SELECT * FROM homepages_moduli_applicativi WHERE Modulo_ModuloApplicativo = " + this.ID);
                 if (table.Rows.Count > 0 && int.TryParse(table.Rows[0]["Modulo_ModuloRss"].ToString(), out id_modulo_rss)) ;
                 else PageData.Redirect(PageData.BackAddress);

                 modulo = new NetFormTable("homepages_moduli_applicativi", "Modulo_ModuloCircolare", this.Connection, id_modulo_rss);
             }
             else
             {
                 modulo = new NetFormTable("homepages_moduli_rss", "Modulo_ModuloCircolare", this.Connection);
                 NetExternalField mod = new NetExternalField("homepages_moduli", "Modulo_ModuloApplicativo");
                 modulo.addExternalField(mod);
             }
             NetTextBoxPlus xml = new NetTextBoxPlus("Indirizzo file XML", "Link_ModuloRss");
             xml.ContentType = NetTextBoxPlus.ContentTypes.NormalText;
             xml.Required = true;
             xml.MaxLenght = 1000;
             xml.Size = 70;
             modulo.addField(xml);

             NetCheckBox showdesc = new NetCheckBox("Mostra descrizione", "Descrizione_ModuloRss");
             modulo.addField(showdesc);

             NetTextBox desclen = new NetTextBox("Lunghezza Descrizione(se attiva)", "DescLen_ModuloRss");
             desclen.Numeric = true;
             desclen.Required = true;
             modulo.addField(desclen);

             NetDropDownList records = new NetDropDownList("Numero di record da visualizare", "Records_ModuloRss");
             records.Required = true;
             for (int i = 3; i <= 7; i++)
                 records.addItem(i.ToString(), i.ToString());
             modulo.addField(records);

             return modulo;*/
        }
    }

}