﻿using System.Data;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.Homepage
{
    [NetCms.Homepages.HomepageModuleDefiner(55, "Notifiche", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuleNotifiche : NetCms.Structure.Applications.Homepage.HomepageModule
    {
        protected override string LocalCssClass
        {
            get { return " HomepageModulo_Notifiche"; }
        }
        public override string Title
        {
            get { return "Modulo Notifiche"; }
        }

        public bool ShowTitle;

        public HomepageModuleNotifiche(Modulo modulo)
            : base(modulo)
        {
            ModuloNotifiche moduloNotifiche = HomePageBusinessLogic.GetModuloNotificheById(modulo.ID);
            ShowTitle = moduloNotifiche.ShowTitle;
        }

        public HomepageModuleNotifiche(ModuloNotifiche modulo)
            : base(modulo)
        {
            ShowTitle = modulo.ShowTitle;
        }

        protected override HtmlGenericControl GetAdminContent()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");


            HtmlGenericControl h3 = new HtmlGenericControl("h3");

            h3.InnerHtml = "Modulo Notifiche";
            div.Controls.Add(h3);

            HtmlGenericControl p = new HtmlGenericControl("p");
            p.InnerHtml = "Codice Modulo : #" + NetUtility.TreeUtility.FormatNumber(this.ID.ToString(), 4) + " <br />";
            div.Controls.Add(p);

            return div;


        }

    }
    [NetCms.Homepages.HomepageModuleDefiner(55, "Notifiche", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuleNotificheForm : NetCms.Structure.Applications.Homepage.HomepageModuleForm
    {
        public HomepageModuleNotificheForm(int id)
            : base(id)
        {
        }
        public HomepageModuleNotificheForm()
            : base()
        {
        }
        public override NetForms.NetFormTable GetFormTable()
        {
            NetFormTable modulo;
            if (ID != 0)
            {
                int id_modulo_notifiche = 0;
                DataTable table = this.Conn.SqlQuery("SELECT * FROM homepages_moduli_notifiche WHERE Modulo_ModuloNotifiche = " + this.ID);
                if (table.Rows.Count > 0 && int.TryParse(table.Rows[0]["Modulo_ModuloNotifiche"].ToString(), out id_modulo_notifiche)) ;
                //else PageData.GoBack();
                //Trace

                modulo = new NetFormTable("homepages_moduli_notifiche", "Modulo_ModuloNotifiche", this.Conn, id_modulo_notifiche);
            }
            else
            {
                modulo = new NetFormTable("homepages_moduli_notifiche", "Modulo_ModuloNotifiche", this.Conn);
                NetExternalField mod = new NetExternalField("homepages_moduli", "Modulo_ModuloNotifiche");
                modulo.addExternalField(mod);
            }

            NetCheckBox showTitle = new NetCheckBox("Mostra titolo modulo", "ShowTitle_ModuloNotifiche");
            modulo.addField(showTitle);

            return modulo;
        }
    }

}