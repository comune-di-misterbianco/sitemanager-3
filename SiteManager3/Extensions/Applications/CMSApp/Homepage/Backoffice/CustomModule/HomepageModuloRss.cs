using System.Data;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.Structure.Applications.Homepage.Model;

/// <summary>
/// Summary description for LayoutModuloBlock
/// </summary>
/// 
namespace NetCms.Structure.Applications.Homepage
{
    [NetCms.Homepages.HomepageModuleDefiner(4, "Rss", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuleRss : NetCms.Structure.Applications.Homepage.HomepageModule
    {
        protected override string LocalCssClass
        {
            get { return " HomepageModulo_Rss"; }
        }
        public override string Title
        {
            get { return "Modulo Rss"; }
        }

        private string Href;
        private int Records;
        private bool ShowDesc;
        private int DescLen;

        public HomepageModuleRss(Modulo module)
            : base(module)
        {
            ModuloRSS modulo = HomePageBusinessLogic.GetModuloRSSById(module.ID);

            Records = modulo.Records;
            Href = modulo.Link;
            ShowDesc = modulo.Descrizione == 1;
            DescLen = modulo.DescLen;
        }

        public HomepageModuleRss(ModuloRSS modulo)
            : base(modulo)
        {
            //DataTable table = Connection.SqlQuery("SELECT * FROM Homepages_Moduli_Rss WHERE Modulo_ModuloRss =" + row["id_Modulo"].ToString());

            //Records = int.Parse(table.Rows[0]["Records_ModuloRss"].ToString());
            //Href = table.Rows[0]["Link_ModuloRss"].ToString();
            //ShowDesc = table.Rows[0]["Descrizione_ModuloRss"].ToString() == "1";
            //DescLen = int.Parse(table.Rows[0]["DescLen_ModuloRss"].ToString());

            Records = modulo.Records;
            Href = modulo.Link;
            ShowDesc = modulo.Descrizione == 1;
            DescLen = modulo.DescLen;
        }

        protected override HtmlGenericControl GetAdminContent()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");


            HtmlGenericControl h3 = new HtmlGenericControl("h3");

            h3.InnerHtml = "RSS: '" + Href.Remove(15) + "&hellip;'";
            div.Controls.Add(h3);

            HtmlGenericControl p = new HtmlGenericControl("p");
            p.InnerHtml = "Codice Modulo : #" + NetUtility.TreeUtility.FormatNumber(this.ID.ToString(), 4) + " <br />";
            p.InnerHtml += "Mostra le ultime " + Records + " News";
            p.InnerHtml += " prese dal Feed";
            div.Controls.Add(p);

            return div;


        }

    }
    [NetCms.Homepages.HomepageModuleDefiner(4, "Rss", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuleRssForm : NetCms.Structure.Applications.Homepage.HomepageModuleForm
    {
        public HomepageModuleRssForm(int id)
            : base(id)
        {
        }
        public HomepageModuleRssForm()
            : base()
        {
        }
        public override NetForms.NetFormTable GetFormTable()
        {
            NetFormTable modulo;
            if (ID != 0)
            {
                int id_modulo_rss = 0;
                DataTable table = this.Conn.SqlQuery("SELECT * FROM homepages_moduli_rss WHERE Modulo_ModuloRss = " + this.ID);
                if (table.Rows.Count > 0 && int.TryParse(table.Rows[0]["Modulo_ModuloRss"].ToString(), out id_modulo_rss)) ;
                //else PageData.GoBack();
                //Trace

                modulo = new NetFormTable("homepages_moduli_rss", "Modulo_ModuloRss", this.Conn, id_modulo_rss);
            }
            else
            {
                modulo = new NetFormTable("homepages_moduli_rss", "Modulo_ModuloRss", this.Conn);
                NetExternalField mod = new NetExternalField("homepages_moduli", "Modulo_ModuloRss");
                modulo.addExternalField(mod);
            }
            NetTextBoxPlus xml = new NetTextBoxPlus("Indirizzo file XML", "Link_ModuloRss");
            xml.ContentType = NetTextBoxPlus.ContentTypes.NormalText;
            xml.Required = true;
            xml.MaxLenght = 1000;
            xml.Size = 70;
            modulo.addField(xml);

            NetCheckBox showdesc = new NetCheckBox("Mostra descrizione", "Descrizione_ModuloRss");
            modulo.addField(showdesc);

            //NetTextBox desclen = new NetTextBox("Lunghezza Descrizione(se attiva)", "DescLen_ModuloRss");
            NetTextBox desclen = new NetTextBox("Lunghezza Descrizione", "DescLen_ModuloRss");
            desclen.Numeric = true;
            desclen.Required = true;
            //desclen.Value = "50";
            modulo.addField(desclen);

            NetDropDownList records = new NetDropDownList("Numero di record da visualizzare", "Records_ModuloRss");
            records.Required = true;
            for (int i = 3; i <= 7; i++)
                records.addItem(i.ToString(), i.ToString());
            modulo.addField(records);

            return modulo;
        }
    }
}