using System.Data;
using System.Web.UI.HtmlControls;
using NetForms;
using System.Collections.Generic;
using System.Linq;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;

/// <summary>
/// Summary description for LayoutModuloBlock
/// </summary>
/// 
namespace NetCms.Structure.Applications.Homepage
{
    [NetCms.Homepages.HomepageModuleDefiner(20, "Circolare", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public class HomepageModuleCircular : NetCms.Structure.Applications.Homepage.HomepageModule
    {
        protected override string LocalCssClass
        {
            get { return " HomepageModulo_Circular"; }
        }
        public override string Title
        {
            get { return "Modulo Circolare"; }
        }


        private HomepageModule ReferedModulo;

        public HomepageModuleCircular(Modulo module)
            : base(module)
        {
            ModuloCircolare modulo = HomePageBusinessLogic.GetModuloCircolareById(module.ID);

            ReferedModulo = NetCms.Structure.Applications.Homepage.HomepageModule.ModuleFactory(modulo.ModuloCollegato);
        }

        public HomepageModuleCircular(ModuloCircolare modulo)
            : base(modulo)
        {
            //DataTable table = this.Connection.SqlQuery("SELECT * FROM Homepages_Moduli_Circolari WHERE Modulo_ModuloCircolare =" + row["id_Modulo"].ToString());

            ReferedModulo = NetCms.Structure.Applications.Homepage.HomepageModule.ModuleFactory(modulo.ModuloCollegato);
        }

        protected override HtmlGenericControl GetAdminContent()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");

            HtmlGenericControl h3 = new HtmlGenericControl("h3");

            h3.InnerHtml = Title;
            div.Controls.Add(h3);

            HtmlGenericControl p = new HtmlGenericControl("p");
            p.InnerHtml = "Codice Modulo : " + this.LabelCode + " <br />";
            p.InnerHtml += "Codice Modulo Referenziato: '" + ReferedModulo.LabelCode + "' <br />";
            p.InnerHtml += "Tipo Modulo Referenziato: '" + ReferedModulo.Title + "' <br />";
            p.InnerHtml += "Homepage Modulo Referenziato: '" + (ReferedModulo.HomepageDocument.Folder as StructureFolder).LabeledPath + "' <br />";
            div.Controls.Add(p);

            return div;
        }
    }

    [NetCms.Homepages.HomepageModuleDefiner(20, "Circolare", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public class HomepageModuleCircularForm : NetCms.Structure.Applications.Homepage.HomepageModuleForm
    {
        public HomepageModuleCircularForm(int id)
            : base(id)
        {
        }
        public HomepageModuleCircularForm()
            : base()
        {
        }
        public override NetForms.NetFormTable GetFormTable()
        {
            NetFormTable table2;
            if (ID != 0)
            {
                int id_ModuloCircolare = 0;
                DataTable table = this.Conn.SqlQuery("SELECT * FROM homepages_moduli_circolari WHERE Modulo_ModuloCircolare = " + ID);
                if (table.Rows.Count > 0 && int.TryParse(table.Rows[0]["Modulo_ModuloCircolare"].ToString(), out id_ModuloCircolare)) ;
                //else PageData.GoBack();
                //Trace
                table2 = new NetFormTable("homepages_moduli_circolari", "Modulo_ModuloCircolare", this.Conn, id_ModuloCircolare);
            }
            else
            {
                table2 = new NetFormTable("homepages_moduli_circolari", "Modulo_ModuloCircolare", this.Conn);
                NetExternalField mod = new NetExternalField("homepages_moduli", "Modulo_ModuloCircolare");
                table2.addExternalField(mod);
            }



            NetDropDownList Model = new NetDropDownList("Modulo Collegato", "ModuloCollegato_ModuloCircolare");
            //Model.AutoPostBack = true;
            Model.Required = true;
            Model.ClearList();

            //string sql = "SELECT * FROM Folders ";
            //sql += " INNER JOIN Docs ON id_Folder = Folder_Doc ";
            //sql += " INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang ";
            //sql += " INNER JOIN Homepages ON id_Homepage = Content_DocLang ";
            //sql += " INNER JOIN Homepages_Contenitori ON Homepage_Contenitore = id_Homepage ";
            //sql += " INNER JOIN Homepages_Moduli ON Contenitore_Modulo = id_Contenitore ";
            //sql += " WHERE Tipo_Modulo <> 2 AND Application_Doc = 1 AND Network_Folder = " + Networks.NetworksManager.CurrentActiveNetwork.ID;
            //sql += NetCms.Structure.Applications.ApplicationsPool.ActivesAppzSqlFilter("Tipo_Folder");
            //DataTable moduli = this.Connection.SqlQuery(sql);
            //foreach (DataRow row in moduli.Rows)
            //{
            //    NetCms.Structure.Applications.Homepage.HomepageModule tempModulo = NetCms.Structure.Applications.Homepage.HomepageModule.ModuleFactory(row);
            //    string itemLabel = "";
            //    if (tempModulo.HomepageDocument.Folder.Path.Length == 0)
            //        itemLabel += "Homepage Principale";
            //    else
            //        itemLabel += "Homepage nella cartella: " + tempModulo.HomepageDocument.Folder.LabeledPath;
            //    itemLabel += " | Modulo Codice: " + tempModulo.LabelCode;
            //    itemLabel += " | Tipo Modulo : " + tempModulo.Title;
            //    Model.addItem(itemLabel, row["id_Modulo"].ToString());
            //}

            ICollection<Modulo> moduli = HomePageBusinessLogic.GetModuliHomepageByNetwork(Networks.NetworksManager.CurrentActiveNetwork.ID, ApplicationsPool.ActiveAssemblies.Cast<Application>().Select(x => x.ID).ToList<int>());
            foreach (Modulo modulo in moduli)
            {
                HomepageModule tempModulo = HomepageModule.ModuleFactory(modulo);
                string itemLabel = "";
                StructureFolder folder = FolderBusinessLogic.GetById(tempModulo.HomepageDocument.Folder.ID);
                if (folder.Path.Length == 0)
                    itemLabel += "Homepage Principale";
                else
                    itemLabel += "Homepage nella cartella: " + folder.LabeledPath;
                itemLabel += " | Modulo Codice: " + tempModulo.LabelCode;
                itemLabel += " | Tipo Modulo : " + tempModulo.Title;
                Model.addItem(itemLabel, modulo.ID.ToString());
            }

            //Model.JumpField = true;
            table2.addField(Model);

            return table2;
        }
    }
}