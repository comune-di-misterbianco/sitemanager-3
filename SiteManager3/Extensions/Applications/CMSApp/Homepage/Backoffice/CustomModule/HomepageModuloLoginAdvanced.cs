﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using NetForms;
using NetCms.Structure.Applications.Homepage.Model;
using System.Web.UI.HtmlControls;

namespace NetCms.Structure.Applications.Homepage
{
    [NetCms.Homepages.HomepageModuleDefiner(29, "LoginAdvanced", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuloLoginAdvanced : NetCms.Structure.Applications.Homepage.HomepageModule
    {
        protected override string LocalCssClass
        {
            get { return " HomepageModulo_Login"; }
        }
        public override string Title
        {
            get { return "Modulo Login"; }
        }

        //private bool ShowReg;
        //private bool ShowRecPass;
        //private bool ShowOnlyADCtrl;
        //private string SSOPageUrl;

        public HomepageModuloLoginAdvanced(Modulo module)
            : base(module)
        {

            ModuloLoginAdvanced modulo = HomePageBusinessLogic.GetModuloLoginAdvById(module.ID);

            //ShowReg = modulo.MostraLinkRegistrazione;
            //ShowRecPass = modulo.MostraRecuperaPassword;
            //ShowOnlyADCtrl = modulo.MostraSoloControlliAD;
            //SSOPageUrl = modulo.SSOPageUrl;

        }

        public HomepageModuloLoginAdvanced(ModuloLoginAdvanced modulo)
            : base(modulo)
        {
            //ShowReg = modulo.MostraLinkRegistrazione;
            //ShowRecPass = modulo.MostraRecuperaPassword;
            //ShowOnlyADCtrl = modulo.MostraSoloControlliAD;
            //SSOPageUrl = modulo.SSOPageUrl;
        }

        protected override HtmlGenericControl GetAdminContent()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");


            HtmlGenericControl h3 = new HtmlGenericControl("h3");

            h3.InnerHtml = "Modulo Login Advanced";
            div.Controls.Add(h3);

            HtmlGenericControl p = new HtmlGenericControl("p");
            p.InnerHtml = "Codice Modulo : #" + NetUtility.TreeUtility.FormatNumber(this.ID.ToString(), 4) + " <br />";
            //p.InnerHtml += "Link Registrazione: " + (ShowReg ? "Abilitato" : "Disabilitato") + " <br />";
            //p.InnerHtml += "Link Recupera Password: " + (ShowRecPass ? "Abilitato" : "Disabilitato");
            //p.InnerHtml += "Modstra solo controlli AD: " + (ShowOnlyADCtrl ? "Abilitato" : "Disabilitato");

            div.Controls.Add(p);

            return div;
        }
    }

    [NetCms.Homepages.HomepageModuleDefiner(29, "LoginAdvanced", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuleLoginAdvancedForm : NetCms.Structure.Applications.Homepage.HomepageModuleForm
    {
        public HomepageModuleLoginAdvancedForm(int id)
            : base(id)
        {
        }
        public HomepageModuleLoginAdvancedForm()
            : base()
        {
        }
        public override NetForms.NetFormTable GetFormTable()
        {
            NetFormTable modulo;
            if (ID != 0)
            {
                int id_modulo_login = 0;
                DataTable table = this.Conn.SqlQuery("SELECT * FROM homepages_moduli_login_advanced WHERE Modulo_ModuloLoginAdv = " + this.ID);
                if (table.Rows.Count > 0 && int.TryParse(table.Rows[0]["Modulo_ModuloLoginAdv"].ToString(), out id_modulo_login)) ;

                modulo = new NetFormTable("homepages_moduli_login_advanced", "Modulo_ModuloLoginAdv", this.Conn, id_modulo_login);
            }
            else
            {
                modulo = new NetFormTable("homepages_moduli_login_advanced", "Modulo_ModuloLoginAdv", this.Conn);
                NetExternalField mod = new NetExternalField("homepages_moduli", "Modulo_ModuloLoginAdv");
                modulo.addExternalField(mod);
            }


            //NetCheckBox showReg = new NetCheckBox("Mostra link Registrazione", "LinkRegistrazione_ModuloLoginAdv");
            //modulo.addField(showReg);

            //NetCheckBox showRecPass = new NetCheckBox("Mostra link Recupero Password", "RecuperaPassword_ModuloLoginAdv");
            //modulo.addField(showRecPass);

            //NetCheckBox showOnlyADCtrl = new NetCheckBox("Mostra solo controlli Activie directory", "ShowOnlyADCtrl_ModuloLoginAdv");
            //modulo.addField(showOnlyADCtrl);

            //NetTextBox ssoPageUrl = new NetTextBox("Indirizzo pagina di autenticazione", "SSOPageUrl_ModuloLoginAdv");
            //modulo.addField(ssoPageUrl);

            return modulo;
        }
    }
}
