using System;
using System.Data;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using NetCms.Connections;
using NetCms.Structure.WebFS;
using NetForms;
using NetCms.Structure.Applications.Homepage.Model;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for LayoutModuloBlock
/// </summary>
/// 
namespace NetCms.Structure.Applications.Homepage
{
    [NetCms.Homepages.HomepageModuleDefiner(1, "Statico", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuleStatico : NetCms.Structure.Applications.Homepage.HomepageModule
    {
        protected override string LocalCssClass
        {
            get { return " HomepageModulo_Statico"; }
        }
        public override string Title
        {
            get { return "Modulo Statico"; }
        }
        private ModuloStaticoData Data;

        public struct ModuloStaticoData
        {
            private string TitoloValue;
            private string TestoValue;
            private string LinkValue;
            private string TitleValue;
            private string ImgValue;
            private string ImgAltValue;
            private string ImgAlignValue;
            private string CssClassValue;
            private string TemplateValue;

            public string Titolo
            {
                get
                {
                    return TitoloValue;
                }
                set
                {
                    TitoloValue = value;
                }
            }
            public string Testo
            {
                get
                {
                    return TestoValue;
                }
                set
                {
                    TestoValue = value;
                }
            }
            public string Link
            {
                get
                {
                    return LinkValue;
                }
                set
                {
                    LinkValue = value;
                }
            }
            public string Title
            {
                get
                {
                    return TitleValue;
                }
                set
                {
                    TitleValue = value;
                }
            }
            public string Img
            {
                get
                {
                    return ImgValue;
                }
                set
                {
                    ImgValue = value;
                }
            }
            public string ImgAlt
            {
                get
                {
                    return ImgAltValue;
                }
                set
                {
                    ImgAltValue = value;
                }
            }
            public string ImgAlign
            {
                get
                {
                    return ImgAlignValue;
                }
                set
                {
                    ImgAlignValue = value;
                }
            }
            public string CssClass
            {
                get
                {
                    return CssClassValue;
                }
                set
                {
                    CssClassValue = value;
                }
            }

            public string Template
            {
                get { return TemplateValue; }
                set { TemplateValue = value; }
            }

        }

        public HomepageModuleStatico(Modulo module)
            : base(module)
        {
            //string sql = "SELECT * FROM homepages_moduli_statici WHERE rif_modulo_modulo_statico = " + row["ID_modulo"].ToString();

            //DataTable table = Connection.SqlQuery(sql);
            //if (table.Rows.Count > 0)
            //{
            //NomeValue = table.Rows[0]["nome_modulo_statico"].ToString();

            ModuloStatico modulo = HomePageBusinessLogic.GetModuloStaticoById(module.ID);

            Data.Titolo = modulo.Titolo;
            Data.Testo = modulo.Testo;
            Data.Link = modulo.Link;
            Data.Title = modulo.Title;
            Data.Template = modulo.TemplateMode;


            string id_img = modulo.Img;
            if (id_img != null)
            {
                if (id_img.Length > 0 && id_img != "")
                {
                    //Data.Img = pagedata.GUIBuilder.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Immagine);
                    Data.ImgAlt = modulo.ImgAlt;
                    Data.ImgAlign = modulo.ImgAll;
                }
            }

            Data.CssClass = "modulo";


            //}
        }


        public HomepageModuleStatico(ModuloStatico modulo)
            : base(modulo)
        {
            //string sql = "SELECT * FROM homepages_moduli_statici WHERE rif_modulo_modulo_statico = " + row["ID_modulo"].ToString();

            //DataTable table = Connection.SqlQuery(sql);
            //if (table.Rows.Count > 0)
            //{
            //NomeValue = table.Rows[0]["nome_modulo_statico"].ToString();

            Data.Titolo = modulo.Titolo;
            Data.Testo = modulo.Testo;
            Data.Link = modulo.Link;
            Data.Title = modulo.Title;
            Data.Template = modulo.TemplateMode;


            string id_img = modulo.Img;
            if (id_img != null)
            {
                if (id_img.Length > 0 && id_img != "")
                {
                    //Data.Img = pagedata.GUIBuilder.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Immagine);
                    Data.ImgAlt = modulo.ImgAlt;
                    Data.ImgAlign = modulo.ImgAll;
                }
            }

            Data.CssClass = "modulo";


            //}
        }

        protected override HtmlGenericControl GetAdminContent()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");

            if (Data.Titolo != null)
            {
                HtmlGenericControl h3 = new HtmlGenericControl("h3");
                if (Data.Link != null && Data.Link != "")
                {
                    HtmlGenericControl a = new HtmlGenericControl("a");
                    a.Attributes["href"] = Data.Link.Replace(NetCms.Structure.Utility.ImportContentFilter.WebRoot, NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot);
                    if (Data.Title != null && Data.Link != "")
                    {
                        a.Attributes["title"] = Data.Title;
                    }
                    a.InnerHtml = Data.Titolo;
                    h3.Controls.Add(a);
                    div.Controls.Add(h3);
                }
                else
                {
                    h3.InnerHtml = Data.Titolo;
                    div.Controls.Add(h3);
                }
            }

            HtmlGenericControl p = new HtmlGenericControl("p");
            p.InnerHtml = "Codice Modulo : #" + NetUtility.TreeUtility.FormatNumber(this.ID.ToString(), 4) + " <br />"
                        + "Template: <strong>" + Data.Template + "</strong> <br />";
            div.Controls.Add(p);

            if (Data.Img != null && Data.Img != "")
            {

                HtmlGenericControl img = new HtmlGenericControl("img");
                img.Attributes["src"] = Data.Img;

                if (Data.ImgAlt != null && Data.ImgAlt != "")
                {
                    img.Attributes["alt"] = Data.ImgAlt;
                }

                #region allineamento img
                if (Data.ImgAlign != null && Data.ImgAlign != "")
                {
                    if (Data.ImgAlign == "0")
                    {
                        img.Attributes["class"] = "HomepageModulo_Left";
                        div.Controls.Add(img);
                    }
                    if (Data.ImgAlign == "1")
                    {
                        HtmlGenericControl divimg = new HtmlGenericControl("div");
                        divimg.Attributes["class"] = "HomepageModulo_Center";
                        divimg.Controls.Add(img);
                        div.Controls.Add(divimg);
                    }
                    if (Data.ImgAlign == "2")
                    {
                        img.Attributes["class"] = "HomepageModulo_Right";
                        div.Controls.Add(img);
                    }
                }
                #endregion
            }
            return div;
        }
    }

    [NetCms.Homepages.HomepageModuleDefiner(1, "Statico", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuleStaticoForm : NetCms.Structure.Applications.Homepage.HomepageModuleForm
    {
        public HomepageModuleStaticoForm(int id)
            : base(id)
        {
        }
        public HomepageModuleStaticoForm()
            : base()
        {
        }
        public override NetForms.NetFormTable GetFormTable()
        {
            NetFormTable table2;
            if (ID != 0)
            {
                int id_modulo_statico = 0;
                DataTable table = this.Conn.SqlQuery("SELECT * FROM homepages_moduli_statici WHERE rif_modulo_modulo_statico = " + ID);
                if (table.Rows.Count > 0 && int.TryParse(table.Rows[0]["rif_modulo_modulo_statico"].ToString(), out id_modulo_statico)) ;
                //else PageData.GoBack();
                //Trace

                table2 = new NetFormTable("homepages_moduli_statici", "rif_modulo_modulo_statico", this.Conn, id_modulo_statico);
            }
            else
            {
                table2 = new NetFormTable("homepages_moduli_statici", "rif_modulo_modulo_statico", this.Conn);
                NetExternalField mod = new NetExternalField("homepages_moduli", "rif_modulo_modulo_statico");
                table2.addExternalField(mod);
            }

            NetTextBoxPlus titolo_modulo_statico = new NetTextBoxPlus("Titolo: ", "titolo_modulo_statico");
            titolo_modulo_statico.ContentType = NetTextBoxPlus.ContentTypes.NormalText;
            titolo_modulo_statico.Required = false;
            table2.addField(titolo_modulo_statico);

            
            RichTextBox testo_modulo_statico = new RichTextBox("Testo (Max 100000 Caratteri): ", "testo_modulo_statico", "full");
            testo_modulo_statico.MaxLenght = 100000;
            testo_modulo_statico.Required = false;
            testo_modulo_statico.CurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID;
            table2.addField(testo_modulo_statico);

            CMSTextBoxPlus link_modulo_statico = new CMSTextBoxPlus("Collegamento: ", "link_modulo_statico", PageData.CurrentReference);
            link_modulo_statico.CurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID;
            table2.addField(link_modulo_statico);

            NetTextBox title_modulo_statico = new NetTextBox("Descrizione collegamento: ", "title_modulo_statico");
            table2.addField(title_modulo_statico);
            
            //StructureFolder folder = (StructureFolder)Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.FindFolderByName("ImgHome");
            StructureFolder currentfolder = Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder as StructureFolder;
            var folders = currentfolder.SubFolders.Where(x => x.Nome.ToLower() == "ImgHome".ToLower());

            StructureFolder folder = null;

            if (folders != null && folders.Count() > 0)
                folder = folders.First();
            
            if (folder != null)
            {
                NetCms.NetSingleFileChoser img_modulo_statico = new NetCms.NetSingleFileChoser("Immagine: ", "img_modulo_statico", folder, PageData.CurrentReference);
                img_modulo_statico.ImageField = true;
                img_modulo_statico.DataBind();
                table2.addField(img_modulo_statico);

                NetTextBox imgAlt_modulo_statico = new NetTextBox("Descrizione immagine: ", "imgAlt_modulo_statico");
                table2.addField(imgAlt_modulo_statico);

                NetDropDownList imgAll_modulo_statico = new NetDropDownList("Allineamento immagine: ", "imgAll_modulo_statico");
                imgAll_modulo_statico.ClearList();
                imgAll_modulo_statico.addItem("Sinistra", "0");
                imgAll_modulo_statico.addItem("Centrata", "1");
                imgAll_modulo_statico.addItem("Destra", "2");
                table2.addField(imgAll_modulo_statico);
            }
            return table2;
        }

        //public override bool UseTemplate { get { return true; } }

        public override TemplateOptionEnum TemplateOption { get { return TemplateOptionEnum.Simple; } }

        /// <summary>
        /// Aggiunto per l'inserimento/modifica delle lingue aggiuntive di un modulo statico
        /// </summary>
        /// <returns></returns>
        public NetForms.NetFormTable GetLangFormTable(int idLang, string nomeLang, string twoLetterLang)
        {
            NetFormTable table2 = new NetFormTable("homepages_moduli_statici_lang", "id_ModuloStatico_lang");
            
            NetTextBoxPlus titolo_modulo_statico = new NetTextBoxPlus("Titolo " + nomeLang + ": ", "titolo_modulo_statico_" + twoLetterLang);
            titolo_modulo_statico.ContentType = NetTextBoxPlus.ContentTypes.NormalText;
            titolo_modulo_statico.Required = false;
            table2.addField(titolo_modulo_statico);


            RichTextBox testo_modulo_statico = new RichTextBox("Testo " + nomeLang + " (Max 1600 Caratteri): ", "testo_modulo_statico_" + twoLetterLang, "full");
            testo_modulo_statico.MaxLenght = 10000;
            testo_modulo_statico.Required = false;
            testo_modulo_statico.CurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID;
            table2.addField(testo_modulo_statico);

            CMSTextBoxPlus link_modulo_statico = new CMSTextBoxPlus("Collegamento " + nomeLang + ": ", "link_modulo_statico_" + twoLetterLang, PageData.CurrentReference);
            link_modulo_statico.CurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID;
            table2.addField(link_modulo_statico);

            NetTextBox title_modulo_statico = new NetTextBox("Descrizione collegamento " + nomeLang + ": ", "title_modulo_statico_" + twoLetterLang);
            table2.addField(title_modulo_statico);

            //StructureFolder folder = (StructureFolder)Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.FindFolderByName("ImgHome");
            StructureFolder currentfolder = Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder as StructureFolder;
            var folders = currentfolder.SubFolders.Where(x => x.Nome.ToLower() == "ImgHome".ToLower());

            StructureFolder folder = null;

            if (folders != null && folders.Count() > 0)
                folder = folders.First();

            NetCms.NetSingleFileChoser img_modulo_statico = null;
            NetTextBox imgAlt_modulo_statico = null;
            NetDropDownList imgAll_modulo_statico = null;
            if (folder != null)
            {
                img_modulo_statico = new NetCms.NetSingleFileChoser("Immagine " + nomeLang + ": ", "img_modulo_statico_" + twoLetterLang, folder, PageData.CurrentReference);
                img_modulo_statico.ImageField = true;
                img_modulo_statico.DataBind();
                table2.addField(img_modulo_statico);

                imgAlt_modulo_statico = new NetTextBox("Descrizione immagine " + nomeLang + ": ", "imgAlt_modulo_statico_" + twoLetterLang);
                table2.addField(imgAlt_modulo_statico);

                imgAll_modulo_statico = new NetDropDownList("Allineamento immagine " + nomeLang + ": ", "imgAll_modulo_statico_" + twoLetterLang);
                imgAll_modulo_statico.ClearList();
                imgAll_modulo_statico.addItem("Sinistra", "0");
                imgAll_modulo_statico.addItem("Centrata", "1");
                imgAll_modulo_statico.addItem("Destra", "2");
                table2.addField(imgAll_modulo_statico);
            }

            //Popolamento dati dal database
            if (idLang != 0)
            {
                int id_modulo_statico_lang = 0;
                DataTable table = this.Conn.SqlQuery("SELECT * FROM homepages_moduli_statici_lang WHERE id_ModuloStatico_lang = " + idLang);
                if (table.Rows.Count > 0 && int.TryParse(table.Rows[0]["id_ModuloStatico_lang"].ToString(), out id_modulo_statico_lang))
                {
                    titolo_modulo_statico.Value = table.Rows[0]["titolo_modulo_statico"].ToString();

                    testo_modulo_statico.Value = table.Rows[0]["testo_modulo_statico"].ToString();
                    link_modulo_statico.Value = table.Rows[0]["link_modulo_statico"].ToString();
                    title_modulo_statico.Value = table.Rows[0]["title_modulo_statico"].ToString();
                    if (folder != null)
                    {
                        img_modulo_statico.Value = table.Rows[0]["img_modulo_statico"].ToString();
                        img_modulo_statico.DataBind();

                        imgAlt_modulo_statico.Value = table.Rows[0]["imgAlt_modulo_statico"].ToString();
                        imgAll_modulo_statico.Value = table.Rows[0]["imgAll_modulo_statico"].ToString();
                    }
                    
                }

            }

            return table2;
        }
    }
}