﻿using System.Data;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.Homepage
{
    [NetCms.Homepages.HomepageModuleDefiner(45, "Servizi", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuleServizi : NetCms.Structure.Applications.Homepage.HomepageModule
    {
        protected override string LocalCssClass
        {
            get { return " HomepageModulo_Servizi"; }
        }
        public override string Title
        {
            get { return "Modulo Servizi"; }
        }

        public HomepageModuleServizi(Modulo modulo)
            : base(modulo)
        {
        }

        public HomepageModuleServizi(ModuloServizi modulo)
            : base(modulo)
        {
        }

        protected override HtmlGenericControl GetAdminContent()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");


            HtmlGenericControl h3 = new HtmlGenericControl("h3");

            h3.InnerHtml = "Modulo Servizi";
            div.Controls.Add(h3);

            HtmlGenericControl p = new HtmlGenericControl("p");
            p.InnerHtml = "Codice Modulo : #" + NetUtility.TreeUtility.FormatNumber(this.ID.ToString(), 4) + " <br />";
            div.Controls.Add(p);

            return div;


        }

    }
    [NetCms.Homepages.HomepageModuleDefiner(45, "Servizi", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuleServiziForm : NetCms.Structure.Applications.Homepage.HomepageModuleForm
    {
        public HomepageModuleServiziForm(int id)
            : base(id)
        {
        }
        public HomepageModuleServiziForm()
            : base()
        {
        }
        public override NetForms.NetFormTable GetFormTable()
        {
            NetFormTable modulo;
            if (ID != 0)
            {
                int id_modulo_servizi = 0;
                DataTable table = this.Conn.SqlQuery("SELECT * FROM homepages_moduli_servizi WHERE Modulo_ModuloServizi = " + this.ID);
                if (table.Rows.Count > 0 && int.TryParse(table.Rows[0]["Modulo_ModuloServizi"].ToString(), out id_modulo_servizi)) ;
                //else PageData.GoBack();
                //Trace

                modulo = new NetFormTable("homepages_moduli_servizi", "Modulo_ModuloServizi", this.Conn, id_modulo_servizi);
            }
            else
            {
                modulo = new NetFormTable("homepages_moduli_servizi", "Modulo_ModuloServizi", this.Conn);
                NetExternalField mod = new NetExternalField("homepages_moduli", "Modulo_ModuloServizi");
                modulo.addExternalField(mod);
            }

            return modulo;
        }
    }

}