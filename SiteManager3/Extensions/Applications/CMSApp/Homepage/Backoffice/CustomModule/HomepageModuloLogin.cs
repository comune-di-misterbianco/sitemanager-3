using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Connections;
using NetForms;
using NetCms.Structure.Applications.Homepage.Model;

/// <summary>
/// Summary description for LayoutModuloBlock
/// </summary>
/// 
namespace NetCms.Structure.Applications.Homepage
{
    [NetCms.Homepages.HomepageModuleDefiner(21, "Login", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuleLogin : NetCms.Structure.Applications.Homepage.HomepageModule
    {
        protected override string LocalCssClass
        {
            get { return " HomepageModulo_Login"; }
        }
        public override string Title
        {
            get { return "Modulo Login"; }
        }

        private bool ShowReg;
        private bool ShowRecPass;

        public HomepageModuleLogin(Modulo module)
            : base(module)
        {

            ModuloLogin modulo = HomePageBusinessLogic.GetModuloLoginById(module.ID);

            ShowReg = modulo.MostraLinkRegistrazione;
            ShowRecPass = modulo.MostraRecuperaPassword;
        }

        public HomepageModuleLogin(ModuloLogin modulo)
            : base(modulo)
        {
            ShowReg = modulo.MostraLinkRegistrazione;
            ShowRecPass = modulo.MostraRecuperaPassword;
        }

        protected override HtmlGenericControl GetAdminContent()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");


            HtmlGenericControl h3 = new HtmlGenericControl("h3");

            h3.InnerHtml = "Modulo Login";
            div.Controls.Add(h3);

            HtmlGenericControl p = new HtmlGenericControl("p");
            p.InnerHtml = "Codice Modulo : #" + NetUtility.TreeUtility.FormatNumber(this.ID.ToString(), 4) + " <br />";
            p.InnerHtml += "Link Registrazione: " + (ShowReg ? "Abilitato" : "Disabilitato") + " <br />";
            p.InnerHtml += "Link Recupera Password: " + (ShowRecPass ? "Abilitato" : "Disabilitato");

            div.Controls.Add(p);

            return div;


        }

    }
    [NetCms.Homepages.HomepageModuleDefiner(21, "Login", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuleLoginForm : NetCms.Structure.Applications.Homepage.HomepageModuleForm
    {
        public HomepageModuleLoginForm( int id)
            : base(id)
        {
        }
        public HomepageModuleLoginForm()
            : base()
        {
        }
        public override NetForms.NetFormTable GetFormTable()
        {
            NetFormTable modulo;
            if (ID != 0)
            {
                int id_modulo_login = 0;
                DataTable table = this.Conn.SqlQuery("SELECT * FROM homepages_moduli_login WHERE Modulo_ModuloLogin = " + this.ID);
                if (table.Rows.Count > 0 && int.TryParse(table.Rows[0]["Modulo_ModuloLogin"].ToString(), out id_modulo_login)) ;
                //else PageData.GoBack();
                //Trace

                modulo = new NetFormTable("homepages_moduli_login", "Modulo_ModuloLogin", this.Conn, id_modulo_login);
            }
            else
            {
                modulo = new NetFormTable("homepages_moduli_login", "Modulo_ModuloLogin", this.Conn);
                NetExternalField mod = new NetExternalField("homepages_moduli", "Modulo_ModuloLogin");
                modulo.addExternalField(mod);
            }


            NetCheckBox showReg = new NetCheckBox("Mostra link Registrazione", "LinkRegistrazione_ModuloLogin");
            modulo.addField(showReg);

            NetCheckBox showRecPass = new NetCheckBox("Mostra link Recupero Password", "RecuperaPassword_ModuloLogin");
            modulo.addField(showRecPass);

            return modulo;
        }
    }

}