﻿using NetCms.Structure.Applications.Homepage.Model;
using NetForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.HtmlControls;

namespace NetCms.Structure.Applications.Homepage
{
    [NetCms.Homepages.HomepageModuleDefiner(88, "Tags", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public class HomepageModuloTags : NetCms.Structure.Applications.Homepage.HomepageModule
    {
        protected override string LocalCssClass
        {
            get { return " HomepageModulo_Tags"; }
        }
        public override string Title
        {
            get { return "Modulo Tags"; }
        }
 
        private int Records;
        private bool ShowDesc;
        private int DescLen;

        public HomepageModuloTags(Modulo module)
            : base(module)
        {
            ModuloTags modulo = HomepageModulesBusinessLogic<ModuloTags>.GetModuloById(module.ID);

            Records = modulo.Records;                     
           
        }
        protected override HtmlGenericControl GetAdminContent()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            HtmlGenericControl h3 = new HtmlGenericControl("h3");

            h3.InnerHtml = "Modulo Tags";
            div.Controls.Add(h3);

            HtmlGenericControl p = new HtmlGenericControl("p");
            p.InnerHtml = "Codice Modulo : #" + NetUtility.TreeUtility.FormatNumber(this.ID.ToString(), 4) + " <br />";
            p.InnerHtml += "Mostra " + Records + " tags";
            p.InnerHtml += " recuperati dalla network corrente";
            div.Controls.Add(p);

            return div;
        }
    }

    [NetCms.Homepages.HomepageModuleDefiner(88, "Tags", -1, Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public partial class HomepageModuleTagsForm : NetCms.Structure.Applications.Homepage.HomepageModuleForm
    {
        public HomepageModuleTagsForm(int id)
            : base(id)
        {
        }

        public HomepageModuleTagsForm()
          : base()
        {
        }

        public override NetForms.NetFormTable GetFormTable()
        {
            NetFormTable modulo;
            if (ID != 0)
            {
                int id_modulo = 0;
                DataTable table = this.Conn.SqlQuery("SELECT * FROM homepages_moduli_tags WHERE Modulo_ModuloTags = " + this.ID);
                if (table.Rows.Count > 0 && int.TryParse(table.Rows[0]["Modulo_ModuloTags"].ToString(), out id_modulo)) ;
                    
                modulo = new NetFormTable("homepages_moduli_tags", "Modulo_ModuloTags", this.Conn, id_modulo);
            }
            else
            {
                modulo = new NetFormTable("homepages_moduli_tags", "Modulo_ModuloTags", this.Conn);
                NetExternalField mod = new NetExternalField("homepages_moduli", "Modulo_ModuloTags");
                modulo.addExternalField(mod);
            }
                     
            NetDropDownList records = new NetDropDownList("Numero di record da visualizzare", "Records_ModuloTags");
            records.Required = true;
            for (int i = 3; i <= 15; i++)
                records.addItem(i.ToString(), i.ToString());
            modulo.addField(records);

            NetHiddenField networkid = new NetHiddenField("RifNetwork_ModuloTags");
            networkid.Value = NetCms.Networks.NetworksManager.CurrentActiveNetwork.ID.ToString();
            modulo.addField(networkid);

            return modulo;
        }
    }
}
