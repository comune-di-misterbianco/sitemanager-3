using System;
using System.IO;
using System.Collections;
using System.Data;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using System.Linq;
using System.Xml.Linq;
using NetCms.Networks.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.Applications.Homepage.Model;
using NHibernate;
using NetCms.Grants;

/// <summary>
/// Summary description for NewsFolder
/// </summary>
namespace NetCms.Structure.Applications.Homepage
{
    public class HomepageFolder : StructureFolder
    {
        public override NetCms.Structure.Applications.Application RelativeApplication
        {
            get
            {
                if(_RelativeApplication == null)
                    _RelativeApplication = Applications.ApplicationsPool.ActiveAssemblies[HomepageApplication.ApplicationID.ToString()];
                return _RelativeApplication;
            }
        }
        private NetCms.Structure.Applications.Application _RelativeApplication;

        public override bool AllowMove
        {
            get
            {   
                return RelativeApplication.EnableMove;
            }
        }

        public override bool ReviewEnabled
        {
            get
            {
                return this.RelativeApplication.EnableReviews;
            }
        }

        public override ToolbarButtonsCollection ToolbarButtons
        {
            get
            {
                ToolbarButtonsCollection _ToolbarButtons = base.ToolbarButtons;

                if (this.Criteria[CriteriaTypes.Create].Allowed)
                {
                    if (NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies.Contains("9"))
                        _ToolbarButtons.Add(new NetCms.GUI.ToolbarButton(this.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/applications/links/links_new.aspx?folder=" + ID, NetCms.GUI.Icons.Icons.Link_Add, "Nuovo Collegamento Ipertestuale"));
                    //if (NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies.Contains("3"))
                    //    _ToolbarButtons.Add(new NetCms.GUI.ToolbarButton(this.Network.Paths.AbsoluteAdminRoot + "/cms/applications/webdocs/webdocs_new.aspx?folder=" + ID, NetCms.GUI.Icons.Icons.Page_White_Add, "Nuova Pagina Web"));
                }

                return _ToolbarButtons;

            }
        }

        public HomepageFolder()
            : base()
        { }

        public HomepageFolder(Networks.NetworkKey network)
            : base(network)
        {            
        }

        protected override NetCms.RssManager BuildRssManager()
        {
            return new NetCms.Structure.Applications.Homepage.RssManager("http://" + PageData.Request.Url.Authority + this.FrontendHyperlink, this.Label, this.Descrizione, this);

        }

        public override void CreateDefaultPage(ISession innerSession=null)
        {
            base.CreateDefaultPage();
            CreateNewHomepage(innerSession);

            //TODO: Commentato in quanto genera problemi con il salvataggio degli oggetti folder (commit della sessione in cascata) - da ri-vedere e ripristinare.
            //  CreateFolder("Immagini della Homepage", "ImgHome", "Contenitore delle immagini della Homepage", 4, 1, "", 0, 1);

        }

        //private DataSet sourceDataBase;

        //private string SourceFolderPath;

        private NetCms.Importer.Importer importer;

        //protected override int CreateDocContent(DataRow docRecord, DataSet sourceDB, string sourceFileName,params NetCms.Importer.Importer[] imp)
        //{
        //    sourceDataBase = sourceDB;
        //    importer = imp[0];
        //    SourceFolderPath = imp[0].SourceFolderPath;
        //    #region Sql Inserimento Homepage
        //    string sqlHome = @"INSERT INTO  " + this.Network.SystemName + @"_homepages  (Nome_homepage,MetaDescription_homepage,Keywords_homepage,MsgManutenzione_homepage,Stato_homepage) VALUES (";
        //    if (docRecord.Table.Columns.Contains("Nome_homepage"))
        //        sqlHome += "'" + docRecord["Nome_homepage"].ToString().Replace("'", "''") + @"',";
        //    else
        //        sqlHome += @"NULL,";
        //    if (docRecord.Table.Columns.Contains("MetaDescription_homepage"))
        //        sqlHome += "'" + docRecord["MetaDescription_homepage"].ToString().Replace("'", "''") + @"',";
        //    else
        //        sqlHome += @"NULL,";
        //    if (docRecord.Table.Columns.Contains("Keywords_homepage"))
        //        sqlHome += "'" + docRecord["Keywords_homepage"].ToString().Replace("'", "''") + @"',";
        //    else
        //        sqlHome += @"NULL,";
        //    if (docRecord.Table.Columns.Contains("MsgManutenzione_homepage"))
        //        sqlHome += "'" + docRecord["MsgManutenzione_homepage"].ToString().Replace("'", "''") + @"',";
        //    else
        //        sqlHome += @"NULL,";
        //    if (docRecord.Table.Columns.Contains("Stato_homepage"))
        //        sqlHome += docRecord["Stato_homepage"].ToString().Replace("'", "''") + @")";
        //    else
        //        sqlHome += @"NULL)";
        //                                                               //+ docRecord["MetaDescription_homepage"].ToString().Replace("'", "''") + @"','"
        //                                                               //+ docRecord["Keywords_homepage"].ToString().Replace("'", "''") + @"','"
        //                                                               //+ docRecord["MsgManutenzione_homepage"].ToString().Replace("'", "''") + @"',"
        //                                                               //+ docRecord["Stato_homepage"].ToString() + @")";
        //    #endregion
        //    int homeID = this.Network.Connection.ExecuteInsert(sqlHome);

        //    //foreach (DataTable table in sourceDB.Tables) 
        //    //{ 
        //        //if (table.TableName.StartsWith("select *"))
        //        //{
        //    var table = sourceDB.Tables["homepages_contenitori"];
        //    DataRow[] cont = table.Select("homepage_contenitore = '" + docRecord["ID_homepage"].ToString() + "'");
        //    DataTable tableContenitori = sourceDB.Tables["homepages_contenitori"].Clone();
        //    foreach (DataRow row in cont)
        //        tableContenitori.ImportRow(row);
            
        //    Hashtable contenitori = ImportTable(tableContenitori,homeID);

        //    DataTable tableModuli = sourceDB.Tables["homepages_moduli"];
        //    Hashtable moduli = ImportTable(tableModuli, homeID, contenitori);

        //    DataTable tableModApplicativi = sourceDB.Tables["homepages_moduli_applicativi"];
        //    if (tableModApplicativi!=null)
        //        ImportTable(tableModApplicativi, homeID, moduli);

        //    DataTable tableModDocumenti = sourceDB.Tables["homepages_moduli_documenti"];
        //    if (tableModDocumenti!=null)
        //        ImportTable(tableModDocumenti, homeID, moduli);

        //    DataTable tableModMenu = sourceDB.Tables["homepages_moduli_menu"];
        //    if (tableModMenu!=null)
        //        ImportTable(tableModMenu, homeID, moduli);

        //    DataTable tableModRss = sourceDB.Tables["homepages_moduli_rss"];
        //    if (tableModRss!=null)
        //        ImportTable(tableModRss, homeID, moduli);

        //    DataTable tableModStatici = sourceDB.Tables["homepages_moduli_statici"];
        //    if (tableModStatici!=null)
        //        ImportTable(tableModStatici, homeID, moduli);

        //    DataTable tableModCircolari = sourceDB.Tables["homepages_moduli_circolari"];
        //    if (tableModCircolari != null)
        //        ImportTable(tableModCircolari, homeID, moduli);


        //        //}
        //    //}
        //    if (homeID == -1)
        //        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non � stato possibile inserire i dati dell'Homepage durante l'importazione", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "135");

        //    return homeID;
        //}

        public virtual void CreateNewHomepage(ISession sess = null)
        {                        
            //using (ISession sess = ((innerSession == null) ? DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession(): innerSession ))
            //using (ITransaction tx = sess.BeginTransaction())
            //{
            //    try
            //    {
                    //Inserisco il doc
                    NetCms.Structure.WebFS.Document doc = new HomepageDocument(this.NetworkKey);
                    doc.PhysicalName = "default.aspx";
                    doc.Application = this.RelativeApplication.ID;
                    doc.Stato = 1;
                    doc.Autore = this.Account.ID;
                    doc.Data = DateTime.Now;
                    doc.Modify = DateTime.Now;
                    doc.Create = DateTime.Now;
                    doc.Folder = this;

                    this.Documents.Add(doc);


                    FolderBusinessLogic.SaveOrUpdateFolder(this, sess);                                    

                    //Inserisco la home                    
                    HomePageDocContent homepageContent = new HomePageDocContent();
                    homepageContent.Stato = 1;
                    HomePageBusinessLogic.SaveOrUpdateHomepage(homepageContent, sess);

                    #region Inserisco i contenitori                    

                    Contenitore contenitoreRoot = new Contenitore();
                    contenitoreRoot.Name = "radice";
                    contenitoreRoot.Depth = 0;
                    contenitoreRoot.Stato = 1;
                    contenitoreRoot.Posizione = 1;
                    contenitoreRoot.Homepage = homepageContent;
                    homepageContent.Contenitori.Add(contenitoreRoot);

                    contenitoreRoot = HomePageBusinessLogic.SaveOrUpdateContenitore(contenitoreRoot, sess);

                    contenitoreRoot.Tree = "." + NetUtility.TreeUtility.FormatNumber(contenitoreRoot.ID.ToString()) + ".";
            //TODO: in questa sezione vanno aggiunti eventuali nuovi contenitori per migrare il contenuto del parts.xml (int e footer)
                    string[] containers = { "Colsx", "Colcx", "Coldx", "Footer" };

                    for (int i = 0; i < containers.Length; i++)
                    {

                        Contenitore child = new Contenitore();
                        child.Name = containers[i];
                        child.Depth = 1;
                        child.Stato = 1;
                        child.Posizione = (i + 1);
                        child.Parent = contenitoreRoot;
                        contenitoreRoot.ChildsBlocks.Add(child);
                        child.Homepage = homepageContent;
                        homepageContent.Contenitori.Add(child);

                        child = HomePageBusinessLogic.SaveOrUpdateContenitore(child, sess);
                        child.Tree = "." + NetUtility.TreeUtility.FormatNumber(contenitoreRoot.ID.ToString()) + "." + NetUtility.TreeUtility.FormatNumber(child.ID.ToString()) + ".";
                    }

                    #endregion

                    // Inserisco la lingua                    
                    DocumentLang docLang = new DocumentLang();
                    docLang.Lang = this.StructureNetwork.DefaultLang;
                    docLang.Label = "Homepage";
                    docLang.Document = doc;
                    docLang.Content = homepageContent;
                    doc.DocLangs.Add(docLang);
                    homepageContent.DocLang = docLang;

                    DocumentBusinessLogic.SaveOrUpdateDocument(doc, sess);                    

                //    tx.Commit();
                //}
                //catch (Exception ex)
                //{
                //   tx.Rollback();
                //}
            //}
        }


        //private Hashtable ImportTable(DataTable table,int homeId, params Hashtable[] parentTable) 
        //{
        //    Hashtable map = new Hashtable();
        //    bool insert = true;    
        //    string[] arrayNome = table.TableName.Trim().Split('_');
        //    string nomeTab = "";
        //    if (arrayNome.Length>2)
        //        nomeTab = "_" + arrayNome[arrayNome.Length - 3] + "_" + arrayNome[arrayNome.Length - 2] + "_" + arrayNome[arrayNome.Length - 1].Trim();
        //    else
        //        nomeTab = "_" + arrayNome[arrayNome.Length - 2] + "_" + arrayNome[arrayNome.Length - 1].Trim();
        //    bool processedRoot = false;
        //    string treeRoot = "";
        //    foreach (DataRow row in table.Rows)
        //    {
        //        string insertInto = "INSERT INTO " + this.Network.SystemName + nomeTab;
        //        string insertIntoFields = "";
        //        string insertIntoValues = "";
        //        insert = true;
        //        if (table.Columns.Contains("nome_contenitore") && row["nome_contenitore"].ToString() == "radice")
        //            processedRoot = false;

        //        string idAssociatedModule = "";
        //        foreach (DataColumn col in table.Columns)
        //        {
        //            if (/*col.ColumnName.ToLower().EndsWith("_" + arrayNome[arrayNome.Length - 1]) &&*/ !col.ColumnName.ToLower().StartsWith("id_"))
        //            {
        //                string comma = insertIntoFields.Length > 0 ? "," : "";

        //                insertIntoFields += comma + col.ColumnName;

        //                string value = "";
        //                switch (col.ColumnName)
        //                {
        //                    case "homepage_contenitore": value = homeId.ToString();
        //                        break;
        //                    case "parent_contenitore": if (!processedRoot) value = "0";
        //                        else
        //                        {

        //                            int old_parent = int.Parse(row[col.ColumnName].ToString());
        //                            value = map[old_parent].ToString();
        //                        }
        //                        break;
        //                    case "Folder_ModuloApplicativo":
        //                        string id = row[col.ColumnName].ToString();
        //                        //DataRow folder = null;

        //                        //if (id == "100")
        //                        //{

        //                        //    folder = sourceDataBase.Tables["folders"].Rows[39];
        //                        //}
        //                        //else
        //                        //    folder = sourceDataBase.Tables["folders"].Select("id_Folder=" + id)[0];

        //                        var all = XDocument.Load(SourceFolderPath + "\\" + "data.xml").Descendants("NewDataSet");
        //                        var folder = from fold in all.Descendants("folders")
        //                                     where fold.Element("id_Folder").Value == id
        //                                     select new
        //                                     {
        //                                         Name = fold.Element("Name_Folder").Value,
        //                                         Path = fold.Element("Path_Folder").Value
        //                                     };
        //                        var selectedFOlder = folder.FirstOrDefault();

        //                        //importer.InitSourceDataBase();
        //                        //DataTable tab1 = importer.SourceDatabase.Tables["folders"];
        //                        //DataRow[] rowstest = importer.SourceDatabase.Tables["folders"].Select();
        //                        //folder = importer.SourceDatabase.Tables["folders"].Select("id_Folder=" + id)[0];
        //                        if (selectedFOlder != null)
        //                        {
        //                            DataTable newfolder = this.Network.Connection.SqlQuery("select * from " + this.Network.SystemName + "_folders where Name_Folder='" + folder.First().Name + "' AND Path_Folder='" + folder.First().Path + "'");
        //                            if (newfolder != null && newfolder.Rows.Count > 0)
        //                                value = newfolder.Rows[0]["id_Folder"].ToString();
        //                            else
        //                            {
        //                                value = "NULL";
        //                                insert = false;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            value = "NULL";
        //                            insert = false;
        //                        }
        //                        break;
        //                    case "Contenitore_Modulo":
        //                    case "Modulo_ModuloApplicativo":
        //                    case "ModuloCollegato_ModuloCircolare":
        //                    case "Modulo_ModuloCircolare":
        //                    case "modulo_modulodocumento":
        //                    case "Modulo_ModuloMenu":
        //                    case "Modulo_ModuloRss":
        //                    case "rif_modulo_modulo_statico":

        //                        int old_id = int.Parse(row[col.ColumnName].ToString());
        //                        if (parentTable[0] != null && parentTable[0][old_id] != null)
        //                            value = idAssociatedModule = parentTable[0][old_id].ToString();
        //                        else
        //                            insert = false;
        //                        break;
        //                    case "img_modulo_statico":
        //                        string oldImgID = row[col.ColumnName].ToString();
        //                        if (oldImgID.Length > 0)
        //                        {
        //                            var allData = XDocument.Load(SourceFolderPath + "\\" + "data.xml").Descendants("NewDataSet");
        //                            var folders = from fold in allData.Descendants("folders")
        //                                          where fold.Element("Tipo_Folder").Value == "4"
        //                                          select fold;
        //                            string filename = "";
        //                            foreach (var itemFold in folders)
        //                            {
        //                                if (sourceDataBase.Tables["docs-" + itemFold.Element("id_Folder").Value] != null)
        //                                {
        //                                    DataRow[] files = sourceDataBase.Tables["docs-" + itemFold.Element("id_Folder").Value].Select("id_Doc = '" + oldImgID + "'");
        //                                    if (files.Length > 0)
        //                                    {
        //                                        filename = files[0]["Name_Doc"].ToString();
        //                                        break;
        //                                    }
        //                                }
        //                            }
        //                            NetworkDocument doc = this.FindDocByName(filename);
        //                            string ID = doc != null ? doc.ID.ToString() : "";
        //                            if (ID.Length > 0)
        //                                value = ID;
        //                            else
        //                                value = "";
        //                        }
        //                        else
        //                            value = "";
        //                        break;
        //                    default: value = row[col.ColumnName].ToString();
        //                        break;
        //                };
        //                if (value != "NULL")
        //                    insertIntoValues += comma + "'" + value.Replace("'", "''") + "'";
        //                else
        //                    insertIntoValues += comma + value.Replace("'", "''");
        //            }
        //        }
        //        if (!insert)
        //        {
        //            if (idAssociatedModule.Length > 0)
        //            {
        //                string deleteModuleSQL = "DELETE FROM " + this.Network.SystemName + "_homepages_moduli WHERE id_Modulo = " + idAssociatedModule;
        //                this.Network.Connection.Execute(deleteModuleSQL);
        //                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Eliminato modulo con id = " + idAssociatedModule, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
        //            }
        //            continue;
        //        }
        //        insertInto += "(" + insertIntoFields + ") VALUES (" + insertIntoValues + ")";
        //        int id_new = this.Network.Connection.ExecuteInsert(insertInto);
        //        if (id_new <= 0)
        //            throw new Exception();
        //        map.Add(int.Parse(row[0].ToString()), id_new);
        //        if (arrayNome[arrayNome.Length - 1].Trim() == "contenitori")
        //        {
        //            string nodeRoot = "";
        //            if (!processedRoot)
        //            {
        //                treeRoot = "." + G2Core.Common.Utility.FormatNumber(id_new.ToString()) + ".";
        //                nodeRoot = treeRoot;
        //            }
        //            else
        //            {
        //                var all = XDocument.Load(SourceFolderPath + "\\" + "data.xml").Descendants("NewDataSet");
        //                var record = from rec in all.Descendants(table.TableName)
        //                             where rec.Element("ID_contenitore").Value == row["ID_contenitore"].ToString()
        //                             select new
        //                             {
        //                                 id = rec.Element("ID_contenitore").Value,
        //                                 parent = rec.Element("parent_contenitore").Value
        //                             };
        //                //DataRow[] temp = table.Select("ID_contenitore = " + row["ID_contenitore"].ToString());
        //                while (record.First().parent != "0")
        //                {
        //                    nodeRoot = "." + G2Core.Common.Utility.FormatNumber(map[int.Parse(record.First().id)].ToString()) + nodeRoot;
        //                    //temp = table.Select("ID_contenitore = " + temp[0]["parent_contenitore"].ToString());
        //                    var temp = record;
        //                    var recordtemp = from rec in all.Descendants(table.TableName)
        //                                     where rec.Element("ID_contenitore").Value == temp.First().parent
        //                                     select new
        //                                     {
        //                                         id = rec.Element("ID_contenitore").Value,
        //                                         parent = rec.Element("parent_contenitore").Value
        //                                     };
        //                    record = recordtemp;
        //                }
        //                nodeRoot = treeRoot.Remove(treeRoot.Length - 1, 1) + nodeRoot + ".";
        //            }

        //            processedRoot = true;
        //            string sql = "UPDATE " + this.Network.SystemName + "_homepages_contenitori SET tree_contenitore = '" + nodeRoot + "' WHERE ID_contenitore = " + id_new;
        //            this.Network.Connection.Execute(sql);
        //        }  
        //    }
        //    return map;

        //}

        public override bool DeleteModuloApplicativo(ISession session)
        {
            return true;
        }

        public override bool DeleteSomething(NHibernate.ISession session)
        {
            return true;
        }
    } 
}