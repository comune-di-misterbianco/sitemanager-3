//using System;
//using System.Data;
//using System.Configuration;
//using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;


///// <summary>
///// Summary description for xNodeCollection
///// </summary>

//namespace NetCms.Structure.Applications.Homepage
//{

//    public class HomepageModuleCollection
//    {
//        private NetCollection coll;

//        public int Count { get { return coll.Count; } }

//        private HomepageBlock Owner;

//        public HomepageModuleCollection(HomepageBlock owner)
//        {
//            coll = new NetCollection();
//            Owner = owner;
//        }

//        public void Add(HomepageModule module)
//        {
//            coll.Add(module.UniqueName, module);
//            module.Container = Owner;
//        }

//        public HomepageModule this[int i]
//        {
//            get
//            {
//                HomepageModule value = (HomepageModule)coll[coll.Keys[i]];
//                return value;
//            }
//        }

//        public HomepageModule this[string key]
//        {
//            get
//            {
//                HomepageModule field = (HomepageModule)coll[key];
//                return field;
//            }
//        }
//    }
//}