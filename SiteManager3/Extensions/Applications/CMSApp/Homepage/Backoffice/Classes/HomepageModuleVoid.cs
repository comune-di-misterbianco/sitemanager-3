using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Applications.Homepage.Model;


namespace NetCms.Structure.Applications.Homepage
{
    public partial class Homepage
    {
        public class HomepageModuleVoid : HomepageModule
        {
            protected override string LocalCssClass
            {
                get { return " HomepageModulo_Void"; }
            }
            public override string Title
            {
                get { return "Modulo Vuoto #" + this.ID; }
            }
            #region Costruttori

            public HomepageModuleVoid(Modulo modulo)
                : base(modulo)
            {
            }

            #endregion

            protected override HtmlGenericControl GetAdminContent()
            {

                HtmlGenericControls.Div div = new HtmlGenericControls.Div();

                div.InnerHtml = "Questo � temp. un modulo vuoto";

                return div;
            }
        }
    }
}