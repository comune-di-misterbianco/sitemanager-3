using System;
using System.Data;
using System.Web.UI.HtmlControls;
using NetCms.Homepages;
using System.Linq;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.Connections;
using System.Collections.Generic;

/// <summary>
/// Summary description for homepage
/// </summary>
/// 
namespace NetCms.Structure.Applications.Homepage
{
    public abstract class HomepageModule
    {      
        private HomepageDocument _HomepageDocument;
        public HomepageDocument HomepageDocument
        {
            get
            {
                return _HomepageDocument;
            }
        }

        protected abstract string LocalCssClass
        {
            get;
        }
        public abstract string Title
        {
            get;
        }

        public string LabelCode
        {
            get
            {
                return "#" + NetUtility.TreeUtility.FormatNumber(this.ID.ToString(), 4);
            }
        }

        public int _Tipo;
        public int Tipo
        {
            get
            {
                return _Tipo;
            }
        }

        public string UniqueName
        {
            get
            {
                return "Module" + ID;
            }
        }

        private string _CustomCssClass;
        public string CustomCssClass
        {
            get
            {
                return _CustomCssClass;
            }
        }

        private bool _Stato;
        public bool Stato
        {
            get
            {
                return _Stato;
            }
        }

        private int _ID;
        public int ID
        {
            get
            {
                return _ID;
            }
        }

        private HomepageBlock _Container;
        public HomepageBlock Container
        {
            get
            {
                return _Container;
            }
            set
            {
                _Container = value;
            }
        }

        #region Costruttori
      
        public HomepageModule(Modulo modulo)
        {
            _ID = modulo.ID;
            _Stato = modulo.Stato == 1;
            _Tipo = modulo.Tipo;
            _CustomCssClass = (modulo.CssClass != null && modulo.CssClass.Length > 0) ? " " + modulo.CssClass : "";
            
            this._HomepageDocument = HomePageBusinessLogic.GetHomepageDocumentByContainer(NetCms.Networks.NetworksManager.CurrentActiveNetwork.ID, ApplicationsPool.ActiveAssemblies.Cast<Application>().Select(x => x.ID).ToList<int>(), modulo.Contenitore.ID);

            _Container = new HomepageBlock(modulo.Contenitore, _HomepageDocument, _HomepageDocument.PageData);
        }

        #endregion

        public HtmlGenericControl GetControl()
        {
            HtmlGenericControls.Div div = new HtmlGenericControls.Div();

            div.ID = UniqueName;
            div.Class = "HomepageModulo" + LocalCssClass + CustomCssClass;

            HtmlGenericControls.Div shield = new HtmlGenericControls.Div();
            shield.Class = "HomepageModulo_Shield";
            div.Controls.Add(shield);

            shield.Controls.Add(GetModuleHead());

            HtmlGenericControl content = new HtmlGenericControl("div");
            content.Controls.Add(GetAdminContent());
            content.Attributes["class"] = "HomepageModulo_Content";
            shield.Controls.Add(content);

            shield.Attributes["onmouseover"] = "Homepage_ModuloOver('" + UniqueName + "');";
            shield.Attributes["onmouseout"] = "Homepage_ModuloOut('" + UniqueName + "');";
            shield.Attributes["onclick"] = "Homepage_BlockSelected('" + UniqueName + "','Modulo');";

            return div;
        }

        public HtmlGenericControl GetModuleHead()
        {
            HtmlGenericControls.Div div = new HtmlGenericControls.Div();
            div.Class = "HomepageModulo_Head";
            if (!this.Stato)
                div.Class += " HomepageModulo_HeadRed";

            HtmlGenericControl module_bar = new HtmlGenericControl("div");
            module_bar.Attributes["class"] = "HomepageModulo_Bar";

            div.Controls.Add(module_bar);

            string confirmmsg = "Sei sicuro di voler eliminare questo Modulo?";

            module_bar.InnerHtml = "";
            module_bar.InnerHtml += "<a class=\"HomepageModulo_BarButton HomepageModulo_ButtonEdit\" title=\"Modifica Modulo\" href=\"gest_modulo.aspx?hid=" + this.Container.HomepageID + "&amp;folder=" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID + "&amp;doc=" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.CurrentDocument.ID + "&amp;mid=" + ID + "\"><span>Modifica Modulo</span></a>";
            module_bar.InnerHtml += "<a class=\"HomepageModulo_BarButton HomepageModulo_ButtonDelete\" title=\"Elimina Modulo\" href=\"javascript: setFormValueConfirm('" + this.ID + "','" + HomepageApplication.InputFieldIDDelete + "',1,'" + confirmmsg + "')\"><span>Elimina Modulo</span></a>";
            module_bar.InnerHtml += "<a class=\"HomepageModulo_BarButton HomepageModulo_ButtonSwitch\" title=\"Scambia Posizione\" href=\"javascript: Homepage_Start('" + UniqueName + "','switch');\"><span>Scambia Posizione</span></a>";
            module_bar.InnerHtml += "<a class=\"HomepageModulo_BarButton HomepageModulo_ButtonMove\" title=\"Sposta Modulo\" href=\"javascript: Homepage_Start('" + UniqueName + "','move');\"><span>Sposta Modulo</span></a>";

            if (LanguageManager.Utility.Config.EnableMultiLanguage && Tipo == 1)
            {
                module_bar.InnerHtml += "<a class=\"HomepageModulo_BarButton HomepageModulo_ButtonTranslate\" title=\"Traduci Modulo\" href=\"lang_modulo.aspx?hid=" + this.Container.HomepageID + "&amp;folder=" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID + "&amp;doc=" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.CurrentDocument.ID + "&amp;mid=" + ID + "\"><span>Traduci Modulo</span></a>";
            }


            HtmlGenericControls.Div titleCtr = new HtmlGenericControls.Div();
            titleCtr.Class = "HomepageModulo_Title";
            titleCtr.InnerHtml = this.Title;
            if (!this.Stato)
            {
                titleCtr.Class += " HomepageModulo_TitleRed";
                titleCtr.InnerHtml += " - Nascosto al pubblico";
            }

            div.Controls.Add(titleCtr);

            return div;
        }

        protected abstract HtmlGenericControl GetAdminContent();

        //public static HomepageModule ModuleFactory(int id)
        //{
        //    DataTable table = NetCms.Connections.ConnectionsManager.CurrentNetworkConnection.SqlQuery("SELECT * FROM Homepages_Moduli WHERE id_Modulo = " + id);
        //    return ModuleFactory(table.Rows[0]);
        //}

        //public static HomepageModule ModuleFactory(DataRow row)
        //{
        //    string TypeID = row["Tipo_Modulo"].ToString();
        //    if (ModuliHomepageClassesPool.CmsModuleClasses.ContainsKey(TypeID))
        //    {
        //        object[] pars = { row };
        //        var type = ModuliHomepageClassesPool.CmsModuleClasses[TypeID];
        //        return System.Activator.CreateInstance(type, pars) as HomepageModule;
        //    }

        //    Exception ex = new Exception("Tipo del modulo non riconoscibile");
        //    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "132");

        //    return null;
        //}

        public static HomepageModule ModuleFactory(Modulo modulo)
        {
            string TypeID = modulo.Tipo.ToString();
            if (ModuliHomepageClassesPool.CmsModuleClasses.ContainsKey(TypeID))
            {
                object[] pars = new object[] { modulo };               

                Type type = ModuliHomepageClassesPool.CmsModuleClasses[TypeID].Types;
                return System.Activator.CreateInstance(type, pars) as HomepageModule;
            }

            Exception ex = new Exception("Tipo del modulo non riconoscibile");
            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "132");

            return null;
        }

      
    }

    public abstract class HomepageModuleForm
    {
        private Connection _Conn;
        public Connection Conn
        {
            get
            {
                //if (_Conn == null)
                //    _Conn = NetCms.Connections.ConnectionsManager.CurrentNetworkConnection;
                //return _Conn;
                ConnectionsManager ConnManager = new ConnectionsManager();
                return ConnManager.CommonConnection;
            }
        }

        private int _ID;
        protected int ID
        {
            get { return _ID; }
        }

        public HomepageModuleForm(int id)
        {
            if (id > 0)
                _ID = id;
        }
        public HomepageModuleForm()
        {
        }

        public abstract NetForms.NetFormTable GetFormTable();

        public static HomepageModuleForm ModuleFactory(int type, PageData PageData)
        {
            return ModuleFactory(type, 0);
        }
        public static HomepageModuleForm ModuleFactory(int type, int id)
        {
            if (ModuliHomepageClassesPool.CmsModuleFormsClasses.ContainsKey(type.ToString()))
            {
                object[] pars = { id };
                var typeClass = ModuliHomepageClassesPool.CmsModuleFormsClasses[type.ToString()];
                return System.Activator.CreateInstance(typeClass, pars) as HomepageModuleForm;
            }

            //Trace
            Exception ex = new Exception("Tipo del modulo non riconoscibile");
            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "134");

            return null;
        }

        public virtual TemplateOptionEnum TemplateOption
        {
            get
            {
                return TemplateOptionEnum.None; 
            }
        }

        public enum TemplateOptionEnum
        {
            None,
            Base,
            Simple,
            ExendedTemplate,
        }

        /*  public virtual List<TemplateType> AvailableTemplate
          {
              get
              {
                  List<TemplateType> availableTemplate = new List<TemplateType>();

                  switch (this.TemplateOption)
                  {
                      case HomepageModuleForm.TemplateOptionEnum.Base:
                          availableTemplate.Add(TemplateType.No_Template);
                          availableTemplate.Add(TemplateType.Simple);
                          break;
                      case HomepageModuleForm.TemplateOptionEnum.Simple:
                          availableTemplate.Add(TemplateType.No_Template);
                          availableTemplate.Add(TemplateType.Standard);
                          availableTemplate.Add(TemplateType.Agid);
                          availableTemplate.Add(TemplateType.Box);
                          break;
                      case HomepageModuleForm.TemplateOptionEnum.ExendedTemplate:
                          availableTemplate.Add(TemplateType.No_Template);
                          availableTemplate.Add(TemplateType.Default_Style);
                          availableTemplate.Add(TemplateType.Focus);
                          availableTemplate.Add(TemplateType.Gallery);
                          availableTemplate.Add(TemplateType.Grid);
                          availableTemplate.Add(TemplateType.Masonry);
                          availableTemplate.Add(TemplateType.News);
                          availableTemplate.Add(TemplateType.Carousel);
                          availableTemplate.Add(TemplateType.Simple);
                          availableTemplate.Add(TemplateType.List);
                          break;
                  }

                  return availableTemplate;
              }
          }*/

        public virtual List<string> AvailableTemplate(PageData pagedata)
        {

            List<string> availableTemplate = new List<string>();
            List<Themes.Model.TemplateElement> templates = new List<Themes.Model.TemplateElement>();

            availableTemplate.Add("No_Template");
            switch (this.TemplateOption)
            {
                case HomepageModuleForm.TemplateOptionEnum.Base:
                    //templates = pagedata.CurrentConfig.CurrentTheme.GetTemplateElements(Themes.Model.TemplateElement.TplTypes.modulo, true);
                    //availableTemplate.AddRange((from Themes.Model.TemplateElement tpl in templates
                    //                     select tpl.Label).ToList());

                    availableTemplate = CmsConfigs.Model.GlobalConfig.CurrentConfig.CurrentTheme.GetTplElement(new List<Themes.Model.TemplateElement.TplTypes>() { Themes.Model.TemplateElement.TplTypes.modulo }, "");


                    break;
                case HomepageModuleForm.TemplateOptionEnum.Simple:
                    //templates = pagedata.CurrentConfig.CurrentTheme.GetTemplateElements(Themes.Model.TemplateElement.TplTypes.modulo);
                    //availableTemplate.AddRange((from Themes.Model.TemplateElement tpl in templates
                    //                     select tpl.Label).ToList());
                    availableTemplate = CmsConfigs.Model.GlobalConfig.CurrentConfig.CurrentTheme.GetTplElement(new List<Themes.Model.TemplateElement.TplTypes>() { Themes.Model.TemplateElement.TplTypes.modulo }, "");
                    break;
                case HomepageModuleForm.TemplateOptionEnum.ExendedTemplate:
                    //templates = pagedata.CurrentConfig.CurrentTheme.GetTemplateElements(Themes.Model.TemplateElement.TplTypes.moduloapplicativo);
                    //availableTemplate.AddRange((from Themes.Model.TemplateElement tpl in templates
                    //                     select tpl.Label).ToList());

                    availableTemplate = CmsConfigs.Model.GlobalConfig.CurrentConfig.CurrentTheme.GetTplElement(new List<Themes.Model.TemplateElement.TplTypes>() { Themes.Model.TemplateElement.TplTypes.moduloapplicativo }, "");
                    break;
            }
            return availableTemplate;
        }       
    }

}
