//using System;
//using System.Data;
//using System.Configuration;
//using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;


///// <summary>
///// Summary description for xNodeCollection
///// </summary>

//namespace NetCms.Structure.Applications.Homepage
//{
//    public class HomepageBlockCollection
//    {
//        private NetCollection coll;

//        public int Count { get { return coll.Count; } }

//        private HomepageBlock Owner;

//        public HomepageBlockCollection(HomepageBlock owner)
//        {
//            coll = new NetCollection();
//            Owner = owner;
//        }

//        public void Add(HomepageBlock layoutblock)
//        {
//            coll.Add(layoutblock.UniqueName, layoutblock);
//            layoutblock.Parent = Owner;
//        }

//        public int IndexOf(string key)
//        {
//            for (int i = 0; i < coll.Keys.Count; i++)
//            {
//                if (coll.Keys[i] == key)
//                    return i;
//            }
//            return -1;
//        }

//        public HomepageBlock this[int i]
//        {
//            get
//            {
//                HomepageBlock value = (HomepageBlock)coll[coll.Keys[i]];
//                return value;
//            }
//        }

//        public HomepageBlock this[string key]
//        {
//            get
//            {
//                HomepageBlock field = (HomepageBlock)coll[key];
//                return field;
//            }
//        }
//        public HomepageBlock Find(string key)
//        {
//            HomepageBlock node = null;

//            if (key == Owner.UniqueName) /* cambiare con nome */
//                node = Owner;

//            if (node == null)
//                node = this[key];

//            int i = 0;
//            while (node == null && i < this.Count)
//                node = this[i++].ChildsBlocks.Find(key);

//            return node;
//        }

//        public HomepageBlock Find(int dbId)
//        {
//            HomepageBlock node = null;

//            if (dbId == Owner.ID) /* cambiare con nome */
//                node = Owner;

//            if (node == null)
//                for (int i = 0; i < this.Count && node == null; i++)
//                {
//                    node = this[i].ID == dbId ? this[i] : null;
//                }

//            int j = 0;
//            while (node == null && j < this.Count)
//                node = this[j++].ChildsBlocks.Find(dbId);

//            return node;
//        }
//        public HomepageBlock FindByName(string key)
//        {
//            HomepageBlock node = null;

//            if (key.ToLower() == Owner.Name.ToLower())
//                node = Owner;

//            int i = 0;
//            for (i = 0; node == null && i < this.Count; i++)
//                if (key.ToLower() == this[i].Name.ToLower())
//                    node = this[i];
//            i = 0;
//            while (node == null && i < this.Count)
//                node = this[i++].ChildsBlocks.FindByName(key);

//            return node;
//        }
//    }
//}