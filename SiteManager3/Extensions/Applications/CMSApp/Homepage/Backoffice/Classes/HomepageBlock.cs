using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using NetCms.Connections;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using System.Collections.Generic;
using System.Linq;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.Diagnostics;
/// <summary>
/// Summary description for homepage
/// </summary>
/// 
namespace NetCms.Structure.Applications.Homepage
{
    public class HomepageBlock
    {
        private PageData PageData;
        //private DataRow DbData;

        public string UniqueName
        {
            get
            {
                return "Block" + ID;
            }
        }

        private int _ID;
        public int ID
        {
            get
            {
                return _ID;
            }
        }

        private int _HomepageID;
        public int HomepageID
        {
            get
            {
                return _HomepageID;
            }
        }

        private string _Name;
        public string Name
        {
            get
            {
                return _Name;
            }
        }

        public string SystemName
        {
            get
            {
                return "Row" + ID;
            }
        }

        private bool _Stato;
        public bool Stato
        {
            get
            {
                return _Stato;
            }
        }

        private string _Tree;
        public string Tree
        {
            get
            {
                return _Tree;
            }
        }

        private int _Depth;
        public int Depth
        {
            get
            {
                return _Depth;
            }
        }

        public bool IsSystemBlock
        {
            get
            {
                return Depth < 2;
            }
        }

        // AGGIUNTA
        private int _Posizione;
        public int Posizione
        {
            get
            {
               return _Posizione;
            }
        }

        private int _Order;
        public int Order { 
            get 
            {
                return _Order;
            }
        }

        //private HomepageBlock _Parent;
        //public HomepageBlock Parent
        //{
        //    get
        //    {
        //        return _Parent;
        //    }
        //    set
        //    {
        //        _Parent = value;
        //        _Depth = Parent.Depth + 1;
        //    }
        //}

        private HomepageBlock _Parent;
        public HomepageBlock Parent
        {
            get
            {
                if (_Parent == null)
                {
                    _Parent = new HomepageBlock(Contenitore.Parent, this.CurrentDocument, this.PageData);
                }
                return _Parent;
            }
        }

        //private HomepageBlockCollection _ChildsBlocks;
        //public HomepageBlockCollection ChildsBlocks
        //{
        //    get
        //    {
        //        if (_ChildsBlocks == null)
        //        {
        //            _ChildsBlocks = new HomepageBlockCollection(this);
        //            DataTable tab = PageData.Conn.SqlQuery("SELECT * FROM Homepages_Contenitori");
        //            DataRow[] Rows = tab.Select("(Stato_Contenitore = 1 OR Depth_Contenitore>1) AND Depth_Contenitore = " + (this.Depth + 1) + " AND Tree_Contenitore LIKE '" + Tree + "%'", "Tree_Contenitore");
        //            foreach (DataRow row in Rows)
        //            {
        //                _ChildsBlocks.Add(new HomepageBlock(row, this.CurrentDocument, PageData));
        //            }
        //        }
        //        return _ChildsBlocks;
        //    }
        //}

        private ICollection<HomepageBlock> _ChildsBlocks;
        public ICollection<HomepageBlock> ChildsBlocks
        {
            get
            {
                if (_ChildsBlocks == null)
                {
                    _ChildsBlocks = new List<HomepageBlock>();
                    var item2Add = Contenitore.ChildsBlocks.Where(x => (x.Stato == 1 || x.Depth > 1) && x.Depth == (this.Depth + 1) && Contenitore.Tree.Contains(Tree)).OrderBy(x => x.Tree);
                    foreach (Contenitore cont in item2Add)
                        _ChildsBlocks.Add(new HomepageBlock(cont, this.CurrentDocument, this.PageData));
                }
                return _ChildsBlocks;
            }
        }

        //private HomepageModuleCollection _Modules;
        //public HomepageModuleCollection Modules
        //{
        //    get
        //    {
        //        if (_Modules == null)
        //        {
        //            _Modules = new HomepageModuleCollection(this);
        //            DataTable tab = PageData.Conn.SqlQuery("SELECT * FROM Homepages_Moduli");
        //            DataRow[] Rows = tab.Select("Contenitore_Modulo = " + ID, "Posizione_Modulo");
        //            foreach (DataRow row in Rows)
        //            {
        //                _Modules.Add(HomepageModule.ModuleFactory(row));
        //            }
        //        }
        //        return _Modules;
        //    }
        //}

        private ICollection<Modulo> _Modules;
        public ICollection<Modulo> Modules
        {
            get
            {
                return _Modules.OrderBy(x=>x.Posizione).ToList<Modulo>();
            }
        }

        public Contenitore Contenitore
        { get; private set; }
        

        public NetCms.Structure.WebFS.Document CurrentDocument
        {
            get { return _CurrentDocument; }
            private set { _CurrentDocument = value; }
        }
        private NetCms.Structure.WebFS.Document _CurrentDocument;

        #region Costruttori

        //public HomepageBlock(DataRow row, NetCms.Structure.WebFS.Document doc, PageData pagedata)
        //{
        //    CurrentDocument = doc;
        //    this.Init(row, pagedata);
        //}

        public HomepageBlock(Contenitore contenitore, NetCms.Structure.WebFS.Document doc, PageData pagedata)
        {
            Contenitore = contenitore;
            CurrentDocument = doc;
            this.Init(contenitore, pagedata);
        }

        //public HomepageBlock(int id, Document doc, PageData pagedata)
        //{
        //    CurrentDocument = doc;
        //    DataRow[] rows = pagedata.Conn.SqlQuery("SELECT * FROM Homepages_Contenitori WHERE id_Contenitore = " + id).Select();
        //    if (rows.Length > 0)
        //    {
        //        this.Init(rows[0], pagedata);
        //    }
        //}

        //private void Init(DataRow row, PageData pagedata)
        //{
        //    DbData = row;
        //    PageData = pagedata;

        //    _Name = row["Nome_Contenitore"].ToString();
        //    _ID = int.Parse(row["id_Contenitore"].ToString());
        //    _HomepageID = int.Parse(row["Homepage_Contenitore"].ToString());
        //    _Stato = row["Stato_Contenitore"].ToString() == "1";
        //    _Tree = row["Tree_Contenitore"].ToString();
        //}

        private void Init(Contenitore contenitore, PageData pagedata)
        {
            PageData = pagedata;
            _Name = contenitore.Name;
            _ID = contenitore.ID;
            _HomepageID = contenitore.Homepage.ID;
            _Stato = contenitore.Stato.ToString() == "1";
            _Tree = contenitore.Tree;
            _Modules = contenitore.Modules;
            //_ChildsBlocks = contenitore.ChildsBlocks;
            //_Parent = contenitore.Parent;
            _Posizione = contenitore.Posizione;
            _Order = contenitore.Order;
            _Depth = contenitore.Depth;
        }

        #endregion

        public HtmlGenericControl GetControl()
        {
            HtmlGenericControls.Div div = new HtmlGenericControls.Div();

            div.ID = UniqueName;

            div.Controls.Add(GetContainerHead());
            
            if (Depth > 0 && this.Name.ToLower() != "colcx")
            {
                string full = (this.Modules.Count >= 4 && this.Name.ToLower() != "colsx" && this.Name.ToLower() != "coldx").ToString().ToLower();

                div.Attributes["onmouseover"] = "Homepage_ContainerOver('" + UniqueName + "','" + full + "');";
                div.Attributes["onmouseout"] = "Homepage_ContainerOut('" + UniqueName + "','" + full + "');";
                div.Attributes["onclick"] = "Homepage_BlockSelected('" + UniqueName + "','Container','" + full + "');";
            }

            //if (Depth == 1 && this.Name.ToLower() == "colcx")
            //{
            //    string code = @"<input type=""hidden"" name=""containerOrder"" id=""containerOrder"" value="""" />"
            //                + @"<input type=""button"" name=""sortRows""  id=""sortRows"" value=""Ordina righe"" class=""btn btn-primary btn-sm"" />"
            //                + @"<input type=""submit"" name=""abortSortRows""  id=""abortSortRows"" value=""Annulla"" class=""btn btn-secondary btn-sm"" disable=""disable"" />"
            //                + @"<input type=""submit"" name=""saveSortRows""  id=""saveSortRows"" value=""Salva"" class=""btn btn-primary btn-sm"" disable=""disable"" />";

            //    Button saveSettings = new Button

            //    div.Controls.Add(new LiteralControl(code));
            //}

            div.Class = "HomepageContainer HomepageContainer_" + Name + "";

            if (ChildsBlocks.Count > 0)
            {
                //for (int i = 0; i < ChildsBlocks.Count; i++)
                //{
                //    HomepageBlock currentBlock = new HomepageBlock(ChildsBlocks.ElementAt(i).Contenitore, CurrentDocument, PageData);
                //    div.Controls.Add(currentBlock.GetControl());
                //}

                foreach (HomepageBlock block in ChildsBlocks.OrderBy(x => x.Order))                    
                {
                    div.Controls.Add(block.GetControl());
                }
                    
            }
            else
            {
                HtmlGenericControl modules = new HtmlGenericControl("div");
                modules.Attributes["class"] = "HomepageContainer_Content HomepageContainer_" + Modules.Count;
                div.Controls.Add(modules);

                for (int i = 0; i < Modules.Count; i++)
                    modules.Controls.Add(HomepageModule.ModuleFactory(Modules.ElementAt(i)).GetControl());

                HtmlGenericControls.Div clear = new HtmlGenericControls.Div();
                clear.Class = "HomepageClear";
                modules.Controls.Add(clear);
            }
            
            return div;
        }

        public HtmlGenericControl GetContainerHead()
        {
            HtmlGenericControls.Div div = new HtmlGenericControls.Div();
            div.Class = "HomepageContainer_Head";
            if (!this.Stato)
                div.Class += " HomepageContainer_HeadRed";

            if (!IsSystemBlock)
            {
                HtmlGenericControl module_bar = new HtmlGenericControl("div");
                module_bar.Attributes["class"] = "HomepageContainer_Bar";
                if (!this.Stato)
                    module_bar.Attributes["class"] += " HomepageContainer_BarRed";

                string confirmmsg = "Sei sicuro di voler eliminare questo contenitore?\nL\\\'operazione non sar� reversibile e verranno eliminati anche tutti i moduli in esso contenuti";

                module_bar.InnerHtml = "";
                module_bar.InnerHtml += "<a class=\"HomepageContainer_BarButton HomepageContainer_ButtonEdit\" title=\"Modifica Contenitore\" href=\"gest_contenitore.aspx?folder=" + this.PageData.CurrentFolder.ID + "&amp;doc=" + this.CurrentDocument.ID + "&amp;cid=" + ID + "\"><span>Modifica Contenitore</span></a>";
                module_bar.InnerHtml += "<a class=\"HomepageContainer_BarButton HomepageContainer_ButtonDelete\" title=\"Elimina contenitore\" href=\"javascript: setFormValueConfirm('" + this.ID + "','" + HomepageApplication.InputFieldIDDeleteContainer + "',1,'" + confirmmsg + "')\"><span>Ilimina Contenitore</span></a>";
                module_bar.InnerHtml += "<a class=\"HomepageContainer_BarButton HomepageContainer_ButtonSwitch\" title=\"Scambia Posizione Contenitore\" href=\"javascript: Homepage_Start('" + UniqueName + "','switchcontainer');\"><span>Scambia Posizione Contenitore</span></a>";
                if (this.Modules.Count < 4 || (this.Posizione == 1 && this.Modules.Count < 8)) // abilito l'aggiunta di ulteriori moduli se il contenitore � a tabs
                    module_bar.InnerHtml += "<a class=\"HomepageContainer_BarButton HomepageContainer_ButtonAdd\" title=\"Crea nuovo modulo in questo contenitore\" href=\"ins_modulo.aspx?folder=" + PageData.CurrentFolder.ID + "&amp;cid=" + ID + "&amp;doc=" + this.CurrentDocument.ID + "\"><span>Nuovo Modulo</span></a>";

                div.Controls.Add(module_bar);
            }

            if (this.Depth > 0)
            {
                HtmlGenericControls.Div titleCtr = new HtmlGenericControls.Div();
                if (this.Depth == 1)
                {
                    string setEredita = "";
                    bool ered = this.CurrentDocument.Folder.Parent == null; 
                    
                    string addcontainerbutton = "<a class=\"HomepageContainer_ContainerButton HomepageContainer_ContainerButtonAdd\" title=\"Crea nuova riga\" href=\"javascript: setFormValue('1','" + HomepageApplication.InputFieldIDNewrow + "',1);\"><span>Nuova Riga</span></a>";
                    string addmodulebutton = "<a class=\"HomepageContainer_ContainerButton HomepageContainer_ButtonAdd\" title=\"Crea nuovo modulo in questo contenitore\" href=\"ins_modulo.aspx?folder=" + PageData.CurrentFolder.ID + "&amp;cid=" + ID + "&amp;doc=" + this.CurrentDocument.ID + "\"><span>Nuovo Modulo</span></a>";
                    //string title = "Disabilita Eredita da Homepage Padre";
                                         
                        //setEredita = "HomepageContainer_ContainerButton HomepageContainer_ButtonDelete";
                   
                        //setEredita = "HomepageContainer_ContainerButton HomepageContainer_ButtonSwitch";
                   
                    titleCtr.Class = "HomepageContainer_SystemTitle";
                   // string setEredita = "<a class=\"HomepageContainer_ContainerButton HomepageContainer_ButtonSwitch\" title=\""+ title +" href=\" " + PageData.CurrentFolder.ID + "\"><span>Eredita</span></a>";
                    //string setEredita2 = "<a class=\"HomepageContainer_ContainerButton HomepageContainer_ButtonSwitch\" title=\"Eredita da Homepage padre\" href=\""  + "\"><span>Nuovo Modulo</span></a>";
                    titleCtr.Class = "HomepageContainer_SystemTitle";
                    string eredita = "";
                    string title = "";
                  
                    if (this.Contenitore.Eredita)
                    {
                        setEredita = "HomepageContainer_ContainerButton HomepageContainer_ApplicationCascadeDelete";
                        title = "Disattiva Eredita da Homepage";
                    }
                    else
                    {
                        setEredita = "HomepageContainer_ContainerButton HomepageContainer_ApplicationCascade";
                        title = "Attiva Eredita da Homepage";
                    }
            
                    switch (this.Name.ToLower())
                    {
                        case "colsx":
                            //string eredita = "<a id=\"eredita_button_"+this.Name.ToLower() + "\"" + " class =\"HomepageContainer_ContainerButton HomepageContainer_ButtonSwitch\" ></a>";
                            //eredita = "<input type=\"submit\" id=\"eredita_button_" + this.Name.ToLower() + "\"" + " value=" + setEredita + ">";
                            if (!ered)
                            {
                                
                                
                                eredita = "<a id=\"eredita_button_" + this.Name.ToLower() + "\"" + " class =\"" + setEredita + "\" title=\"" + title +"\" href=\"javascript: openModal('colsx');\"></a>";
                                titleCtr.InnerHtml += addmodulebutton + eredita + "<span>Colonna Sinistra</span>";
                            }
                            else
                            {
                                titleCtr.InnerHtml += addmodulebutton  + "<span>Colonna Sinistra</span>";
                            }
                            break;
                        case "coldx":
                           // eredita = "<input type=\"submit\" id=\"eredita_button_" + this.Name.ToLower() + "\"" + " value=" + setEredita + ">";
                            if(!ered)
                            {

                                eredita = "<a id=\"eredita_button_" + this.Name.ToLower() + "\"" + " class =\"" + setEredita + "\" title=\"" + title + "\" href=\"javascript: openModal('coldx');\"></a>";
                                titleCtr.InnerHtml += addmodulebutton + eredita + "<span>Colonna Destra</span>";
                           
                            }
                            else
                            {
                                titleCtr.InnerHtml += addmodulebutton + "<span>Colonna Destra</span>";
                            }
                            
                            
                            break;
                        case "colcx":
                            // eredita = "<a id=\"eredita_button_" + this.Name.ToLower() + "\"" + " class =\"" + setEredita + "\" href=\"javascript: openModal();\"></a>";                           
                               titleCtr.InnerHtml += addcontainerbutton + "<span>Colonna Centrale</span>";                            
                            break;
                        case "footer":
                            //eredita = "<input type=\"submit\" id=\"eredita_button_" + this.Name.ToLower() + "\"" + " value=" + setEredita + ">";
                            if (!ered)
                            {
                                eredita = "<a id=\"eredita_button_" + this.Name.ToLower() + "\"" + " class =\"" + setEredita + "\" title=\"" + title + "\" href=\"javascript: openModal('footer');\"></a>";
                                if (this.Modules.Count < 4)
                                    titleCtr.InnerHtml += addmodulebutton + eredita + "<span>Pi� di Pagina</span>";
                                else
                                    titleCtr.InnerHtml += eredita + "<span>Pi� di Pagina</span>";
                            }
                            else
                            {   
                                if (this.Modules.Count < 4)
                                    titleCtr.InnerHtml += addmodulebutton + "<span>Pi� di Pagina</span>";
                                else 
                                    titleCtr.InnerHtml += "<span>Pi� di Pagina</span>";
                            }
                            break;
                    }
                }
                else
                {
                    titleCtr.Class = "HomepageContainer_Title";

                    //VERIFICARE

                    //titleCtr.InnerHtml = "Riga " + (this.Parent.ChildsBlocks.IndexOf(this.UniqueName) + 1);

                    //titleCtr.InnerHtml = "Riga " + (this.Parent.ChildsBlocks.ToList().IndexOf(this.Parent.ChildsBlocks.First(x=>x.UniqueName == this.UniqueName)) + 1);//this.Posizione + 1;

                    var order = (this.Order == 0) ? (this.Parent.ChildsBlocks.ToList().IndexOf(this.Parent.ChildsBlocks.First(x => x.UniqueName == this.UniqueName)) + 1) : this.Order;

                    titleCtr.InnerHtml = "Riga" + order;

                    if (this.SystemName != Name)
                        titleCtr.InnerHtml += " - " + Name;
                    if (!this.Stato)
                    {
                        titleCtr.Class += " HomepageContainer_TitleRed";
                        titleCtr.InnerHtml += " - Nascosta al publico";
                    }

                    WebControl selettoreSortable = new WebControl(HtmlTextWriterTag.Div);
                    selettoreSortable.CssClass = "handle_container";
                    selettoreSortable.Controls.Add(new LiteralControl(@"<i class=""fas fa-arrows-alt-v fa-2x""></i>"));                    

                    titleCtr.Controls.Add(selettoreSortable);


                }
                div.Controls.Add(titleCtr);
            }
           
            return div;
        }

        public WebControl BuildEreditaControl()
        {
            //HtmlGenericControl divModale = new HtmlGenericControls.Div();
            WebControl divMod = new WebControl(HtmlTextWriterTag.Div);
            divMod.ID = "dialogEredita";
            Contenitore c;//divMod.CssClass = "set_eredita";
            //divModale.ID = "dialogEredita";
            divMod.Style.Add(HtmlTextWriterStyle.Display, "none");
            divMod.Controls.Add(new LiteralControl("Cambiare il parametro 'Eredita contenuto da homepage padre' di questo contenitore?"));
            divMod.Controls.Add(new LiteralControl("</br><strong>Attenzione,se si eredita da homepage i moduli inseriti nel contenitore verranno ignorati</strong>"));
            //divModale.Style.Add(HtmlTextWriterStyle.Display, "none");
            //Button toggle = new Button();
            //toggle.Style.Add(HtmlTextWriterStyle.Display, "none");
            //toggle.ID = "submitEredita";
            //toggle.Click += new EventHandler(CambiaEredita_Click);
            //toggle.Text = this.Contenitore.ID.ToString();
            Button toggleFooter = new Button();
            toggleFooter.Style.Add(HtmlTextWriterStyle.Display, "none");
            toggleFooter.ID = "submitFooter";
            toggleFooter.Click += new EventHandler(CambiaEredita_Click);
            c = (this.Contenitore.ChildsBlocks.Where(x => x.Name == "Footer")).FirstOrDefault();
            toggleFooter.Text = c.ID.ToString();
            Button toggleColDx = new Button();
            toggleColDx.Style.Add(HtmlTextWriterStyle.Display, "none");
            toggleColDx.ID = "submitColDx";
            toggleColDx.Click += new EventHandler(CambiaEredita_Click);
             
            c = (this.Contenitore.ChildsBlocks.Where(x => x.Name == "Coldx")).FirstOrDefault();
            toggleColDx.Text = c.ID.ToString();
            Button toggleColSx = new Button();
            toggleColSx.Style.Add(HtmlTextWriterStyle.Display, "none");
            toggleColSx.ID = "submitColSx";
            toggleColSx.Click += new EventHandler(CambiaEredita_Click);
            c = (this.Contenitore.ChildsBlocks.Where(x => x.Name == "Colsx")).FirstOrDefault();
            toggleColSx.Text = c.ID.ToString();
            divMod.Controls.Add(toggleFooter);
            divMod.Controls.Add(toggleColDx);
            divMod.Controls.Add(toggleColSx);
            //divMod.Controls.Add(toggle);

            return divMod;

        }
        
        public void CambiaEredita_Click(object sender,EventArgs e)
        {
           
           // string  id = ((Button)sender).ID.ToString();
            int containerID = Int32.Parse(((Button)sender).Text);
            
            
            bool result = HomePageBusinessLogic.CambiaEredita(containerID);
            if (result)
            {

                List<HomePageDocContent> allHome = HomePageBusinessLogic.GetAllHomePageDocContent().ToList();
                G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentDocument.NetworkSystemName, this.HomepageID.ToString());
                foreach(HomePageDocContent h in allHome)
                {
                   // G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage",this.CurrentDocument.NetworkSystemName, this.HomepageID.ToString());
                    G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentDocument.NetworkSystemName, h.ID.ToString());
                }
                
                PopupBox.AddSessionMessage("Cambiamento effettuato con successo", PostBackMessagesType.Success,true);
                
            }
            else
            {
                PopupBox.AddSessionMessage("Cambia Eredita non riuscito", PostBackMessagesType.Error);
            }
            // richiama metodo businessLogic che cambia Eredita da true a false e viceversa
            

        }
    }
}