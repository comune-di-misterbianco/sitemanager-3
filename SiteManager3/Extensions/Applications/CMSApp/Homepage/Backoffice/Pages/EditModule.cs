using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.Grants;
using System.Collections.Generic;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.Structure.Applications.Homepage.Pages
{
    public class EditModule : BasePage
    {
        #region Oggetti da contratto NetworkPage

        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Modifica modulo della Homepage della cartella '" + this.CurrentFolder.Label + "'"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.House; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        #endregion

        public NetUtility.QueryStringRequestVariable ModuloId
        {
            get
            {
                if (_ModuloId == null)
                    _ModuloId = new NetUtility.QueryStringRequestVariable("mid");
                return _ModuloId;
            }
        }
        private NetUtility.QueryStringRequestVariable _ModuloId;

        public EditModule()
        {
            if (this.CurrentDocument.Criteria[CriteriaTypes.Modify].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina di aggiunta di un nuovo modulo all'HomePage", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else if (!IsPostBack && ModuloId.IsValidInteger)
                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina di modifica del modulo con id " + ModuloId.IntValue + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
        }
        
        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "container-fluid";

            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");

            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerText = "Modifica modulo";
            fieldset.Controls.Add(legend);

            HtmlGenericControl contentDiv = new HtmlGenericControl("div");
            contentDiv.Attributes["class"] = "contentForm homepagemoduloform col-md-12";

            if (!ModuloId.IsValidInteger)
            {
                PageData.GoBack();
            }
            else
            {
                //DataTable tab = PageData.Conn.SqlQuery("SELECT * FROM homepages_moduli WHERE id_Modulo = " + Modulo.IntValue);
                Modulo modulo = HomePageBusinessLogic.GetModuloById(ModuloId.IntValue);

                if (modulo!=null)
                {
                    HtmlGenericControl Toolbar = new HtmlGenericControl("div");
                    PageData.InfoToolbar.Icon = "Home";
                    PageData.InfoToolbar.Title = "Aggiornamento Modulo";

                    NetFormTable table = new NetFormTable("homepages_moduli", "id_modulo", PageData.Conn, ModuloId.IntValue);
                    table.CssClass = "netformtable_homepagemoduli";

                    NetForm form = new NetForm();
                    form.CssClass = "formhomepagemoduli";
                    form.SubmitLabel = "Aggiorna Modulo";
                    form.AddTable(table);

                    if (this.CurrentDocument.Criteria[CriteriaTypes.Advanced].Allowed)
                    {
                        NetTextBox cssClass_modulo = new NetTextBox("Classe CSS: ", "cssclass_modulo");
                        table.addField(cssClass_modulo);
                    }

                    NetDropDownList stato = new NetDropDownList("Stato:", "stato_modulo");
                    stato.Required = true;
                    stato.ClearList();
                    stato.addItem("Nascosto al pubblico", "0");
                    stato.addItem("Visibile al pubblico", "1");
                    stato.addItem("Visibile solo al pubblico autenticato", "2");
                    stato.addItem("Nascosto al pubblico autenticato", "3");                    
                    table.addField(stato);

                    HomepageModuleForm module = HomepageModuleForm.ModuleFactory(modulo.Tipo, ModuloId.IntValue);
                    
                    if (module.TemplateOption != HomepageModuleForm.TemplateOptionEnum.None)
                    {
                        NetDropDownList template = new NetDropDownList("Template", "TemplateMode_Modulo");
                        template.Required = true;
                        template.ClearList();
                       
                        foreach (string tpl in module.AvailableTemplate(PageData))
                        {
                            template.addItem(tpl.Replace("_", " "), tpl.ToString());
                        }
               
                        table.addField(template);
                    }

                    contentDiv.Controls.Add(table.getForm());

                    NetForms.NetFormTable formTable = module.GetFormTable();
                    if (formTable != null)
                    {
                        form.AddTable(formTable);
                        contentDiv.Controls.Add(formTable.getForm());
                    }

                    if (PageData.IsPostBack)
                    {
                        #region Postback
                        if (PageData.SubmitSource == form.SubmitSourceID)
                        {
                            string errors = form.serializedUpdate();
                            if (form.LastOperation == NetForm.LastOperationValues.OK)
                            {
                                //G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, this.HomepageID.ToString());
                                List<HomePageDocContent> allHome = HomePageBusinessLogic.GetAllHomePageDocContent();
                             
                                if (allHome != null)
                                {

                                    foreach (HomePageDocContent h in allHome)
                                    {
                                        // G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage",this.CurrentDocument.NetworkSystemName, this.HomepageID.ToString());
                                        G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, h.ID.ToString());
                                        
                                    }
                                    G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, this.HomepageID.ToString());
                                }
                                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha modificato con successo il modulo con id " + ModuloId.IntValue + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                                NetCms.GUI.PopupBox.AddSessionMessage(errors, PostBackMessagesType.Success, true);
                            }
                            else
                            {
                                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante la modifica del modulo con id " + ModuloId.IntValue + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "545");
                                NetCms.GUI.PopupBox.AddSessionMessage(errors, PostBackMessagesType.Error);
                            }
                        }
                        #endregion

                        NetUtility.RequestVariable back = new NetUtility.RequestVariable("back");
                        if (back.IsValidString && back.StringValue == "Torna indietro")
                            PageData.GoBack();

                    }

                    //div.Controls.Add(Toolbar);
                    //div.Controls.Add(form.getForms("Modifica modulo"));

                    HtmlGenericControl pButtons = new HtmlGenericControl("p");
                    pButtons.Attributes["class"] = "buttonscontainer";

                    Button bt = new Button();
                    bt.Text = "Aggiorna Modulo";
                    bt.ID = "send";
                    bt.CssClass = "button";
                    bt.OnClientClick = "javascript: setSubmitSource('send')";
                    pButtons.Controls.Add(bt);

                    contentDiv.Controls.Add(pButtons);

                    WebControl row = new WebControl(HtmlTextWriterTag.Div);
                    row.CssClass = "row";

                    row.Controls.Add(contentDiv);                    

                    fieldset.Controls.Add(row);

                    div.Controls.Add(fieldset);

                }
            }
            return div;
        }
    }
}