using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using System.Collections.Generic;
using System.Linq;
using NetCms.Structure.Applications.Homepage.Model;
using NHibernate;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.Structure.Applications.Homepage.Pages
{
    public abstract class BasePage : NetCms.GUI.NetworkPage
    {
        protected int HomepageID
        {
            get
            {
                return this.CurrentDocument.Content;
            }
        }

        private HomepageBlock _Root;
        protected HomepageBlock Root
        {
            get
            {
                if (_Root == null)
                {
                    //DataTable tab = PageData.Conn.SqlQuery("SELECT * FROM Homepages_Contenitori");
                    //DataRow[] rows = tab.Select("Depth_Contenitore = 0 AND Homepage_Contenitore = " + this.CurrentDocument.Content);
                    //if (rows.Length > 0)
                    //{
                    //    _Root = new HomepageBlock(rows[0],this.CurrentDocument, PageData);
                    //}

                    Contenitore contenitore = HomePageBusinessLogic.GetContenitoreByHomepage(this.CurrentDocument.ContentRecord as HomePageDocContent);
                    if (contenitore != null)
                        _Root = new HomepageBlock(contenitore, this.CurrentDocument, PageData);

                }
                return _Root;
            }
        }

        private HomepageBlock _ColSx;
        protected HomepageBlock ColSx
        {
            get
            {
                if (_ColSx == null)
                //_ColSx = Root.ChildsBlocks.FindByName("Colsx");
                {
                    //Contenitore contenitoreRoot = HomePageBusinessLogic.GetContenitoreByHomepage(this.CurrentDocument.ContentRecord as HomePageDocContent);
                    //Contenitore contenitoreColSx = HomePageBusinessLogic.FindBlock(contenitoreRoot, "Colsx", HomePageBusinessLogic.SearchFor.Name);
                    //_ColSx = new HomepageBlock(contenitoreColSx, this.CurrentDocument, PageData);
                    var res = Root.ChildsBlocks.Where(x => x.Name == "Colsx");
                    if (res.Count() > 0)
                        _ColSx = res.First();
                } 
                return _ColSx;
            }
        }

        private HomepageBlock _ColDx;
        protected HomepageBlock ColDx
        {
            get
            {
                if (_ColDx == null)
                //_ColDx = Root.ChildsBlocks.FindByName("Coldx");
                {
                    //Contenitore contenitoreRoot = HomePageBusinessLogic.GetContenitoreByHomepage(this.CurrentDocument.ContentRecord as HomePageDocContent);
                    //Contenitore contenitoreColDx = HomePageBusinessLogic.FindBlock(contenitoreRoot, "Coldx", HomePageBusinessLogic.SearchFor.Name);
                    var res = Root.ChildsBlocks.Where(x => x.Name == "Coldx");
                    if (res.Count() > 0)
                        _ColDx = res.First(); //new HomepageBlock(contenitoreColDx, this.CurrentDocument, PageData);
                }
                return _ColDx;
            }
        }

        private HomepageBlock _Footer;
        protected HomepageBlock Footer
        {
            get
            {
                if (_Footer == null)
                //_Footer = Root.ChildsBlocks.FindByName("Footer");
                {
                    //Contenitore contenitoreRoot = HomePageBusinessLogic.GetContenitoreByHomepage(this.CurrentDocument.ContentRecord as HomePageDocContent);
                    //Contenitore contenitoreFooter = HomePageBusinessLogic.FindBlock(contenitoreRoot, "Footer", HomePageBusinessLogic.SearchFor.Name);
                    //_Footer = new HomepageBlock(contenitoreFooter, this.CurrentDocument, PageData);
                    var res = Root.ChildsBlocks.Where(x => x.Name == "Footer");
                    if (res.Count() > 0)
                        _Footer = res.First();
                } 
                return _Footer;
            }
        }

        //private DataTable _ModuliDT;
        //public DataTable ModuliDT
        //{
        //    get
        //    {
        //        if (_ModuliDT == null)
        //        {
        //            _ModuliDT = PageData.Conn.SqlQuery("SELECT * FROM Homepages_Moduli");
        //        }
        //        return _ModuliDT;
        //    }
        //}

        //private DataTable _ContainersDT;
        //protected DataTable ContainersDT
        //{
        //    get
        //    {
        //        if (_ContainersDT == null)
        //        {
        //            _ContainersDT = PageData.Conn.SqlQuery("SELECT * FROM Homepages_Contenitori");
        //        }
        //        return _ContainersDT;
        //    }
        //}

        protected HtmlGenericControl HiddenFields
        {
            get
            {
                HtmlGenericControl ctr = new HtmlGenericControl("span");

                NetUtility.RequestVariable sourcePost = new NetUtility.RequestVariable(HomepageApplication.InputFieldIDSource);
                NetUtility.RequestVariable targetPost = new NetUtility.RequestVariable(HomepageApplication.InputFieldIDTarget);
                NetUtility.RequestVariable deleteContainerPost = new NetUtility.RequestVariable(HomepageApplication.InputFieldIDDeleteContainer);
                NetUtility.RequestVariable deletePost = new NetUtility.RequestVariable(HomepageApplication.InputFieldIDDelete);
                NetUtility.RequestVariable newrowPost = new NetUtility.RequestVariable(HomepageApplication.InputFieldIDNewrow);
                NetUtility.RequestVariable ActionPost = new NetUtility.RequestVariable(HomepageApplication.InputFieldIDAction);
                NetUtility.RequestVariable ColStatPost = new NetUtility.RequestVariable(HomepageApplication.InputFieldIDColumnStatus);

                string SourceValue = "";
                string TargetValue = "";

                string inputfield = "";
                inputfield += "<input";
                inputfield += " type=\"hidden\"";
                inputfield += " name=\"{0}\"";
                inputfield += " id=\"{0}\"";
                inputfield += " value=\"{1}\"";
                inputfield += " />";

                ctr.InnerHtml = "";

                ctr.InnerHtml += string.Format(inputfield, HomepageApplication.InputFieldIDSource, SourceValue);
                ctr.InnerHtml += string.Format(inputfield, HomepageApplication.InputFieldIDTarget, TargetValue);
                ctr.InnerHtml += string.Format(inputfield, HomepageApplication.InputFieldIDDelete, "");
                ctr.InnerHtml += string.Format(inputfield, HomepageApplication.InputFieldIDDeleteContainer, "");
                ctr.InnerHtml += string.Format(inputfield, HomepageApplication.InputFieldIDNewrow, "");
                ctr.InnerHtml += string.Format(inputfield, HomepageApplication.InputFieldIDAction, "");
                ctr.InnerHtml += string.Format(inputfield, HomepageApplication.InputFieldIDColumnStatus, "");


                #region Switch Modulo PostBack
                if (PageData.IsPostBack && sourcePost.IsValid() && targetPost.IsValid() && ActionPost.IsValid() && ActionPost.StringValue == "switch")
                {
                    int SourceID, TargetID;
                    if (int.TryParse(sourcePost.StringValue.Replace("Module", ""), out SourceID) && int.TryParse(targetPost.StringValue.Replace("Module", ""), out TargetID))
                    {
                        using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                        using (ITransaction tx = session.BeginTransaction())
                        {
                            try
                            {
                                string SourceOrder = null;
                                string SourceColumn = null;

                                //DataRow[] rows = ModuliDT.Select("id_Modulo = " + SourceID);
                                //if (rows.Length > 0)
                                //{
                                //    SourceOrder = rows[0]["Posizione_Modulo"].ToString();
                                //    SourceColumn = rows[0]["Contenitore_Modulo"].ToString();
                                //}

                                Modulo moduloSource = HomePageBusinessLogic.GetModuloById(SourceID, session);
                                if (moduloSource != null)
                                {
                                    SourceOrder = moduloSource.Posizione.ToString();
                                    SourceColumn = moduloSource.Contenitore.ID.ToString();                                    
                                }

                                string TargetOrder = null;
                                string TargetColumn = null;


                                //rows = ModuliDT.Select("id_Modulo = " + TargetID);
                                //if (rows.Length > 0)
                                //{
                                //    TargetOrder = rows[0]["Posizione_Modulo"].ToString();
                                //    TargetColumn = rows[0]["Contenitore_Modulo"].ToString();
                                //}

                                Modulo moduloTarget = HomePageBusinessLogic.GetModuloById(TargetID, session);
                                if (moduloTarget != null)
                                {
                                    TargetOrder = moduloTarget.Posizione.ToString();
                                    TargetColumn = moduloTarget.Contenitore.ID.ToString();
                                }

                                if (TargetOrder != null && SourceOrder != null && SourceColumn != null && TargetColumn != null)
                                {
                                    //    bool done = false;
                                    //    string sql = "UPDATE Homepages_Moduli SET Contenitore_Modulo = " + TargetColumn + ",Posizione_Modulo = " + TargetOrder + " WHERE id_Modulo = " + SourceID;
                                    //    done = PageData.Conn.Execute(sql);
                                    //    sql = "UPDATE Homepages_Moduli SET Contenitore_Modulo = " + SourceColumn + ",Posizione_Modulo = " + SourceOrder + " WHERE id_Modulo = " + TargetID;
                                    //    done = done & PageData.Conn.Execute(sql);
                                    //    if (done == false)
                                    //        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non � stato possibile aggiornare contenitore e/o posizione del modulo dell'homepage", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "136");
                                    //    else
                                    //    {
                                    //        G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.Network.SystemName, this.HomepageID.ToString());
                                    //        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha aggiornato con successo contenitore e/o posizione del modulo con id " + SourceID + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                                    //    }

                                    bool done = false;

                                    moduloSource.Contenitore = HomePageBusinessLogic.GetContenitoreById(moduloTarget.Contenitore.ID, session);
                                    moduloSource.Posizione = int.Parse(TargetOrder);
                                    done = HomePageBusinessLogic.SaveOrUpdateModulo(moduloSource, session) != null;
                                    
                                    moduloTarget.Contenitore = HomePageBusinessLogic.GetContenitoreById(int.Parse(SourceColumn), session);
                                    moduloTarget.Posizione = int.Parse(SourceOrder);
                                    done = done & HomePageBusinessLogic.SaveOrUpdateModulo(moduloTarget,session) != null;

                                    tx.Commit();

                                    if (done == false)
                                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non � stato possibile aggiornare contenitore e/o posizione del modulo dell'homepage", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "136");
                                    else
                                    {
                                        //G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, this.HomepageID.ToString());
                                        List<HomePageDocContent> allHome = HomePageBusinessLogic.GetAllHomePageDocContent().ToList();
                                        //G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentDocument.NetworkSystemName, this.HomepageID.ToString());
                                        foreach (HomePageDocContent h in allHome)
                                        {
                                            // G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage",this.CurrentDocument.NetworkSystemName, this.HomepageID.ToString());
                                            G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentDocument.NetworkSystemName, h.ID.ToString());
                                        }
                
                                        
                                        
                                        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha aggiornato con successo contenitore e/o posizione del modulo con id " + SourceID + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, ex.Message, "", "");
                                tx.Rollback();
                            }
                        }
                    }
                }
                #endregion

                #region Switch Contenitore PostBack
                if (PageData.IsPostBack && sourcePost.IsValid() && targetPost.IsValid() && ActionPost.IsValid() && ActionPost.StringValue == "switchcontainer")
                {
                    int SourceID, TargetID;
                    if (int.TryParse(sourcePost.StringValue.Replace("Block", ""), out SourceID) && int.TryParse(targetPost.StringValue.Replace("Block", ""), out TargetID))
                    {
                        #region Source Container Data

                        string SourceTree = null;
                        string SourcePos = null;
                        string SourceParent = null;
                        int SourceDepth = -1;

                        //DataRow[] rows = this.ContainersDT.Select("id_Contenitore = " + SourceID);
                        //if (rows.Length > 0)
                        //{
                        //    SourceTree = rows[0]["Tree_Contenitore"].ToString();
                        //    SourcePos = rows[0]["Posizione_Contenitore"].ToString();
                        //    SourceParent = rows[0]["Parent_Contenitore"].ToString();
                        //    SourceDepth = int.Parse(rows[0]["Depth_Contenitore"].ToString());
                        //}

                        Contenitore contenitoreSource = HomePageBusinessLogic.GetContenitoreById(SourceID);
                        if (contenitoreSource != null)
                        {
                            SourceTree = contenitoreSource.Tree;
                            SourcePos = contenitoreSource.Posizione.ToString();
                            SourceParent = contenitoreSource.Parent.ID.ToString();
                            SourceDepth = contenitoreSource.Depth;
                        }

                        #endregion

                        #region Target Container Data

                        string TargetTree = null;
                        string TargetPos = null;
                        string TargetParent = null;
                        int TargetDepth = -1;

                        //rows = ContainersDT.Select("id_Contenitore = " + TargetID);
                        //if (rows.Length > 0)
                        //{
                        //    TargetTree = rows[0]["Tree_Contenitore"].ToString();
                        //    TargetPos = rows[0]["Posizione_Contenitore"].ToString();
                        //    TargetParent = rows[0]["Parent_Contenitore"].ToString();
                        //    TargetDepth = int.Parse(rows[0]["Depth_Contenitore"].ToString());
                        //}

                        Contenitore contenitoreTarget = HomePageBusinessLogic.GetContenitoreById(TargetID);
                        if (contenitoreTarget != null)
                        {
                            TargetTree = contenitoreTarget.Tree;
                            TargetPos = contenitoreTarget.Posizione.ToString();
                            TargetParent = contenitoreTarget.Parent.ID.ToString();
                            TargetDepth = contenitoreTarget.Depth;
                        }

                        #endregion

                        if (TargetDepth == SourceDepth && TargetDepth > -1 &&
                            TargetTree != null && TargetPos != null
                            && SourcePos != null && SourceTree != null)
                        {
                            bool done = false;
                            //string sql = "UPDATE Homepages_Contenitori SET Parent_Contenitore = {0},Tree_Contenitore = '{1}',Posizione_Contenitore = {2} WHERE id_Contenitore = {3}";
                            //done = PageData.Conn.Execute(String.Format(sql, TargetParent, TargetTree, TargetPos, SourceID));
                            //done = done & PageData.Conn.Execute(String.Format(sql, SourceParent, SourceTree, SourcePos, TargetID));
                            //if (done == false)
                            //    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Impossibile switchare un contenitore per l'homepage", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "137");
                            //else
                            //{
                            //    G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.Network.SystemName, this.HomepageID.ToString());
                            //    Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha effettuato lo switch del contenitore con id " + SourceID + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                            //}

                            contenitoreSource.Tree = TargetTree;
                            contenitoreSource.Posizione = int.Parse(TargetPos);
                            contenitoreSource.Parent = HomePageBusinessLogic.GetContenitoreById(int.Parse(TargetParent));
                            done = HomePageBusinessLogic.SaveOrUpdateContenitore(contenitoreSource) != null;

                            contenitoreTarget.Tree = SourceTree;
                            contenitoreTarget.Posizione = int.Parse(SourcePos);
                            contenitoreTarget.Parent = HomePageBusinessLogic.GetContenitoreById(int.Parse(SourceParent));
                            done = done & HomePageBusinessLogic.SaveOrUpdateContenitore(contenitoreTarget) != null;

                            if (done == false)
                                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Impossibile switchare un contenitore per l'homepage", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "137");
                            else
                            {
                                G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, this.HomepageID.ToString());
                                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha effettuato lo switch del contenitore con id " + SourceID + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                            }
                        }
                    }
                }
                #endregion

                #region Newrow Postback

                if (PageData.IsPostBack && newrowPost.IsValidInteger)
                {
                    //DataRow[] rows = this.ContainersDT.Select("Depth_Contenitore = 1 AND Nome_Contenitore = 'Colcx' AND Homepage_Contenitore = " + this.CurrentDocument.Content, "Tree_Contenitore DESC");
                    //if (rows.Length > 0)
                    //{
                    //    string Tree = rows[0]["Tree_Contenitore"].ToString();

                    //    string Parent = rows[0]["id_Contenitore"].ToString();
                    //    string Homepage = rows[0]["Homepage_Contenitore"].ToString();
                    //    int Depth = int.Parse(rows[0]["Depth_Contenitore"].ToString()) + 1;

                    //    bool done = true;
                    //    string sql = "INSERT INTO Homepages_Contenitori(HaveModuli_Contenitore,Tree_Contenitore,Parent_Contenitore,Stato_Contenitore,Homepage_Contenitore,Posizione_Contenitore,CssClass_Contenitore,Depth_Contenitore)Values(0,'{0}',{1},1,{2},0,'',{3})";
                    //    int LastID = PageData.Conn.ExecuteInsert(String.Format(sql, Tree, Parent, Homepage, Depth));
                    //    if (LastID == -1)
                    //        done = false;
                    //    sql = "UPDATE Homepages_Contenitori SET Nome_Contenitore = 'Row" + LastID + "' ,Tree_Contenitore = '" + Tree + "" + NetUtility.TreeUtility.FormatNumber(LastID.ToString()) + ".'WHERE id_Contenitore = " + LastID;
                    //    done = done & PageData.Conn.Execute(sql);
                    //    if (done == false)
                    //        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non � stato possibile aggiungere un nuovo contenitore all'homepage", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "138");
                    //    else
                    //    {
                    //        G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.Network.SystemName, this.HomepageID.ToString());
                    //        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha aggiunto con successo un contenitore all'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                    //    }

                    //}

                    ICollection<Contenitore> contenitori = HomePageBusinessLogic.FindContenitoriColcxByHomepage(this.CurrentDocument.ContentRecord as HomePageDocContent);
                    if (contenitori != null && contenitori.Count > 0)
                    {
                        using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                        using (ITransaction tx = sess.BeginTransaction())
                        {
                            try
                            {

                                string Tree = contenitori.ElementAt(0).Tree;

                                //string Parent = contenitori.ElementAt(0).ID.ToString();
                                //string Homepage = contenitori.ElementAt(0).Homepage.ID.ToString();
                                int Depth = contenitori.ElementAt(0).Depth + 1;

                                bool done = true;

                                Contenitore nuovo = new Contenitore();
                                //nuovo.HasModuli = false;
                                nuovo.Tree = Tree;
                                nuovo.Parent = contenitori.ElementAt(0);
                                nuovo.Stato = 1;
                                nuovo.Homepage = contenitori.ElementAt(0).Homepage;
                                nuovo.Posizione = 0;
                                nuovo.CssClass = "";
                                nuovo.Depth = Depth;

                                nuovo = HomePageBusinessLogic.SaveOrUpdateContenitore(nuovo, sess);
                                if (nuovo == null)
                                    done = false;

                                nuovo.Name = "Row" + nuovo.ID;
                                nuovo.Tree = Tree + "" + NetUtility.TreeUtility.FormatNumber(nuovo.ID.ToString()) + ".";
                                done = done & HomePageBusinessLogic.SaveOrUpdateContenitore(nuovo, sess) != null;

                                if (done == false)
                                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non � stato possibile aggiungere un nuovo contenitore all'homepage", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "138");
                                else
                                {
                                    G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, this.HomepageID.ToString());
                                    Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha aggiunto con successo un contenitore all'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                                }
                                tx.Commit();
                            }
                            catch (Exception ex)
                            {
                                tx.Rollback();
                            }
                        }
                    }
                }

                #endregion

                #region Delete Postback

                if (PageData.IsPostBack && deletePost.IsValidInteger)
                {
                    //DataTable moduli = PageData.Conn.SqlQuery("SELECT * FROM Homepages_Moduli WHERE id_Modulo = " + deletePost.IntValue);
                    //if (moduli.Rows.Count > 0)
                    //{
                    //    bool ok = true;
                    //    int cont = int.Parse(moduli.Rows[0]["Contenitore_Modulo"].ToString());
                    //    string sql;
                    //    moduli = PageData.Conn.SqlQuery("SELECT * FROM Homepages_Moduli_Circolari INNER JOIN Homepages_Moduli ON id_Modulo = Modulo_ModuloCircolare WHERE ModuloCollegato_ModuloCircolare = " + deletePost.IntValue);
                    //    if (moduli.Rows.Count > 0)
                    //    {
                    //        sql = "DELETE FROM Homepages_Moduli WHERE id_Modulo = " + moduli.Rows[0]["id_Modulo"].ToString();
                    //        ok&=PageData.Conn.Execute(sql);
                    //    }

                    //    sql = "DELETE FROM Homepages_Moduli WHERE id_Modulo = " + deletePost.IntValue;
                    //    ok &= PageData.Conn.Execute(sql);

                    //    moduli = PageData.Conn.SqlQuery("SELECT * FROM Homepages_Moduli WHERE Contenitore_Modulo = " + cont);
                    //    if (moduli.Rows.Count == 0)
                    //    {
                    //        sql = "UPDATE Homepages_Contenitori SET Havemoduli_Contenitore = 0 WHERE id_Contenitore = " + cont;
                    //        ok &= PageData.Conn.Execute(sql);
                    //    }
                    //    if (ok)
                    //    {
                    //        G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.Network.SystemName, this.HomepageID.ToString());
                    //        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha eliminato con successo il modulo con id " + deletePost.IntValue + " dall'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                    //    }
                    //    else
                    //        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante l'eliminazione del modulo con id " + deletePost.IntValue + " dall'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "540");
                    
                    //}

                    Modulo modulo = HomePageBusinessLogic.GetModuloById(deletePost.IntValue);
                    if (modulo != null)
                    {
                        bool ok = true;
                        Contenitore contenitoreModulo = modulo.Contenitore;

                        // rimuovo l'eventuale modulo circolare associato
                        ModuloCircolare moduloCircolare = HomePageBusinessLogic.GetModuloCircolareByModuloCollegato(modulo);
                        if (moduloCircolare != null)
                        {
                            ok &= HomePageBusinessLogic.DeleteModulo(moduloCircolare);
                        }

                        modulo.Contenitore.Modules.Remove(modulo);

                        ok &= HomePageBusinessLogic.DeleteModulo(modulo);
                        List<HomePageDocContent> allHome = HomePageBusinessLogic.GetAllHomePageDocContent().ToList();
                        foreach (HomePageDocContent h in allHome)
                        {
                            // G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage",this.CurrentDocument.NetworkSystemName, this.HomepageID.ToString());
                            G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, h.ID.ToString());
                        }
                        //G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, this.HomepageID.ToString());
                    }
                }

                if (PageData.IsPostBack && deleteContainerPost.IsValidInteger)
                {
                    //DataTable blocks = PageData.Conn.SqlQuery("SELECT * FROM Homepages_Contenitori WHERE id_Contenitore = " + deleteContainerPost.IntValue);
                    //if (blocks.Rows.Count == 1)
                    //{
                    //    string sql = "DELETE FROM Homepages_Contenitori WHERE Depth_Contenitore>1 AND Tree_Contenitore LIKE '" + blocks.Rows[0]["Tree_Contenitore"].ToString() + "%'";
                    //    if (PageData.Conn.Execute(sql))
                    //    {
                    //        G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.Network.SystemName, this.HomepageID.ToString());
                    //        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha eliminato con successo il contenitore con id " + deleteContainerPost.IntValue + " dall'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                    //    }
                    //    else Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante l'eliminazione del contenitore con id " + deleteContainerPost.IntValue + " dall'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "541"); 
                    //}

                    Contenitore contenitore = HomePageBusinessLogic.GetContenitoreById(deleteContainerPost.IntValue);

                    if (contenitore != null)
                    {
                        bool ok = true;
                        foreach (Contenitore conten in HomePageBusinessLogic.FindContenitoriWithDepthMoreThan1ByTreeStart(contenitore.Tree))
                        {
                            conten.Parent.ChildsBlocks.Remove(conten);
                            ok &= HomePageBusinessLogic.DeleteContenitore(conten);
                        }

                        if (ok)
                        {
                            G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, this.HomepageID.ToString());
                            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha eliminato con successo il contenitore con id " + deleteContainerPost.IntValue + " dall'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                        }
                        else Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante l'eliminazione del contenitore con id " + deleteContainerPost.IntValue + " dall'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "541"); 
                    }

                }
                #endregion

                #region Move PostBack
                if (PageData.IsPostBack && sourcePost.IsValid() && targetPost.IsValid() && ActionPost.IsValid() && ActionPost.StringValue == "move")
                {
                    //int SourceID, TargetID;
                    //bool ok = true;
                    //if (int.TryParse(sourcePost.StringValue.Replace("Module", ""), out SourceID))
                    //{
                    //    if (int.TryParse(targetPost.StringValue.Replace("Block", ""), out TargetID))
                    //    {
                    //        /*string SourceOrder = null;
                    //        string SourceColumn = null;
                    //        DataRow[] rows = this.ModuliDT.Select("id_Modulo = " + SourceID);
                    //        if (rows.Length > 0)
                    //        {
                    //            SourceOrder = rows[0]["Posizione_Modulo"].ToString();
                    //            SourceColumn = rows[0]["Contenitore_Modulo"].ToString();
                    //        }
                    //        */
                    //        int TargetOrder = 0;

                    //        DataRow[] rows = this.ModuliDT.Select("Contenitore_Modulo = " + TargetID, "Posizione_Modulo DESC");
                    //        if (rows.Length > 0)
                    //        {
                    //            TargetOrder = int.Parse(rows[0]["Posizione_Modulo"].ToString()) + 1;
                    //        }
                    //        string sql = "";
                    //        DataTable moduli = PageData.Conn.SqlQuery("SELECT * FROM Homepages_Moduli WHERE id_Modulo = " + SourceID);

                    //        if (moduli.Rows.Count > 0)
                    //        {
                    //            int cont = int.Parse(moduli.Rows[0]["Contenitore_Modulo"].ToString());
                    //            sql = "DELETE FROM Homepages_Moduli WHERE id_Modulo = " + deletePost.IntValue;
                    //            ok &= PageData.Conn.Execute(sql);

                    //            moduli = PageData.Conn.SqlQuery("SELECT * FROM Homepages_Moduli WHERE Contenitore_Modulo = " + cont);
                    //            if (moduli.Rows.Count == 1)
                    //            {
                    //                sql = "UPDATE Homepages_Contenitori SET Havemoduli_Contenitore = 0 WHERE id_Contenitore = " + cont;
                    //                ok &= PageData.Conn.Execute(sql);
                    //            }
                    //        }
                    //        sql = "UPDATE Homepages_Contenitori SET Havemoduli_Contenitore = 1 WHERE id_Contenitore = " + TargetID;
                    //        ok &= PageData.Conn.Execute(sql);

                    //        sql = "UPDATE Homepages_Moduli SET Contenitore_Modulo = " + TargetID + ",Posizione_Modulo = " + TargetOrder + " WHERE id_Modulo = " + SourceID;
                    //        ok &= PageData.Conn.Execute(sql);

                    //        /*
                    //        rows = this.ModuliDT.Select("Contenitore_Modulo = " + TargetColumn + " AND Posizione_Modulo >= " + TargetOrder + " AND id_Modulo <> " + SourceID);
                    //        for (int i = 0; i < rows.Length; i++)
                    //        {
                    //            int Neworder;
                    //            if (int.TryParse(rows[i]["Posizione_Modulo"].ToString(), out Neworder))
                    //            {
                    //                Neworder++;
                    //                string sql = "UPDATE Homepages_Moduli SET Posizione_Modulo = " + Neworder + " WHERE id_Modulo = " + rows[i]["id_Modulo"].ToString();
                    //                this.PageData.Conn.Execute(sql);
                    //            }
                    //        }*/



                    //    }
                    //    else ok = false;
                    //}
                    //else ok = false;

                    //if (ok)
                    //{
                    //    G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.Network.SystemName, this.HomepageID.ToString());
                    //    Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha spostato correttamente il modulo con id " + SourceID + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                    //}
                    //else Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante lo spostamento del modulo con id " + SourceID + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "542");

                    int SourceID, TargetID;
                    bool ok = true;
                    if (int.TryParse(sourcePost.StringValue.Replace("Module", ""), out SourceID))
                    {

                        if (int.TryParse(targetPost.StringValue.Replace("Block", ""), out TargetID))
                        {
                            int TargetOrder = 0;

                            ICollection<Modulo> moduliDelContenitoreTarget = HomePageBusinessLogic.FindModuliByContenitoreOrderByPositionDesc(TargetID);

                            if (moduliDelContenitoreTarget.Count > 0)
                            {
                                TargetOrder = moduliDelContenitoreTarget.ElementAt(0).Posizione + 1;
                            }

                            Modulo moduloSource = HomePageBusinessLogic.GetModuloById(SourceID);

                            if (moduloSource != null)
                            {
                                moduloSource.Posizione = TargetOrder;
                                moduloSource.Contenitore = HomePageBusinessLogic.GetContenitoreById(TargetID);
                                ok &= HomePageBusinessLogic.SaveOrUpdateModulo(moduloSource) != null;

                            }
                        }
                        if (ok)
                        {
                            G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, this.HomepageID.ToString());
                            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha spostato correttamente il modulo con id " + SourceID + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                            Diagnostics.Diagnostics.Redirect(HttpContext.Current.Request.Url.ToString());
                        }
                        else Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante lo spostamento del modulo con id " + SourceID + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "542");
                    }
                }
                #endregion

                #region Gestione Colonne

                if (ColStatPost.IsValidString)
                {
                    //string sql;
                    //if (ColStatPost.IsValidInteger)
                    //    sql = "UPDATE homepages_contenitori SET stato_contenitore = 0 WHERE ID_contenitore = " + ColStatPost;
                    //else
                    //    sql = "UPDATE homepages_contenitori SET stato_contenitore = 1 WHERE homepage_contenitore = " + this.HomepageID + " AND nome_contenitore = '" + ColStatPost + "'";
                    //PageData.Conn.Execute(sql);

                    if (ColStatPost.IsValidInteger)
                    {
                        Contenitore cont = HomePageBusinessLogic.GetContenitoreById(ColStatPost.IntValue);
                        cont.Stato = 0;
                        HomePageBusinessLogic.SaveOrUpdateContenitore(cont);
                    }
                    else
                    {
                        Contenitore cont = HomePageBusinessLogic.GetContenitoreByHomePageAndName(this.HomepageID, ColStatPost.StringValue);
                        cont.Stato = 1;
                        HomePageBusinessLogic.SaveOrUpdateContenitore(cont);
                    }

                    G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, this.HomepageID.ToString());
                    //PopupBox.AddMessage("Colonna " + (ColStatPost.IsValidInteger?"dis":"") + "abilitata con successo.", PostBackMessagesType.Success);
                }

                #endregion

                return ctr;
            }
        }
        public BasePage()
        {
            ApplicationContext.SetCurrentComponent(HomepageApplication.ApplicationContextBackLabel);
            /*if (this.CurrentDocument.Content == null)
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "HomePage richiesta dall'utente non trovata", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");*/
            //redirect
        }
    }
}