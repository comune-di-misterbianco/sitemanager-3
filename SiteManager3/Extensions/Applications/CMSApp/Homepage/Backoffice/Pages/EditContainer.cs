using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.Grants;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.Structure.Applications.Homepage.Pages
{
    public class EditContainer : BasePage
    {
        #region Oggetti da contratto NetworkPage

        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Aggiornamento Contenitore"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.House; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        #endregion

        public NetUtility.QueryStringRequestVariable Container
        {
            get
            {
                if (_Container == null)
                    _Container = new NetUtility.QueryStringRequestVariable("cid");
                return _Container;
            }
        }
        private NetUtility.QueryStringRequestVariable _Container;

        public EditContainer()
        {
            if (this.CurrentDocument.Criteria[CriteriaTypes.Modify].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina di aggiornamento di un contenitore", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else if (!IsPostBack && Container.IsValidInteger)
                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina di modifica del contenitore con id " + Container.IntValue + " dell'homepage del portale " +NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
        }
        
        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "container-fluid";

            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");

            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerText = "Modifica contenitore";
            fieldset.Controls.Add(legend);

            HtmlGenericControl contentDiv = new HtmlGenericControl("div");
            contentDiv.Attributes["class"] = "contentForm containerform col-md-12";

            if (!Container.IsValidInteger)
            {
                PageData.GoBack();
            }
            else
            {
                //DataTable sqltab = PageData.Conn.SqlQuery("SELECT * FROM homepages_contenitori");
                //DataRow[] rows = sqltab.Select("id_Contenitore = " + Container.IntValue);

                Contenitore contenitore = HomePageBusinessLogic.GetContenitoreById(Container.IntValue);
                if (contenitore != null)
                {
                    string ContainerID = contenitore.ID.ToString();
                    string ContainerName = contenitore.Name;
                    string ContainerSystemName = "Row" + ContainerID;

                    NetFormTable table = new NetFormTable("homepages_contenitori", "id_Contenitore", PageData.Conn, Container.IntValue);
                    table.CssClass = "netformtable_homepagecontenitore";

                    NetTextBoxPlus Nome = new NetTextBoxPlus("Nome: ", "Nome_Contenitore");
                    Nome.LoadValue = ContainerName != ContainerSystemName;
                    table.addField(Nome);

                    if (this.CurrentDocument.Criteria[CriteriaTypes.Advanced].Allowed)
                    {
                        NetTextBoxPlus wrapperCssClass = new NetTextBoxPlus("Class CSS Wrapper:", "wrappercssclass_contenitore");
                        table.addField(wrapperCssClass);

                        NetTextBox cssClass = new NetTextBox("Classe CSS: ", "CssClass_Contenitore");
                        table.addField(cssClass);
                    }

                    NetDropDownList container = new NetDropDownList("Modalit� contenitore", "mode_contenitore");
                    container.Required = true;
                    container.addItem("Container", "container");
                    container.addItem("Container Fluid", "container-fluid");
                    container.addItem("Personalizzato", "custom");
                    table.addField(container);

                    NetDropDownList visualizzazione = new NetDropDownList("Visualizzazione Moduli:", "Posizione_Contenitore");
                    visualizzazione.Required = true;
                    visualizzazione.ClearList();
                    visualizzazione.addItem("Affiancati", "0");
                    visualizzazione.addItem("Come Schede", "1");
                    table.addField(visualizzazione);

                    NetDropDownList stato = new NetDropDownList("Stato:", "Stato_Contenitore");
                    stato.Required = true;
                    stato.addItem("Visibile al pubblico", "1");
                    stato.addItem("Nascosto al pubblico", "0");
                    table.addField(stato);

                    NetForm form = new NetForm();
                    form.SubmitLabel = "Aggiorna Contenitore";
                    form.AddTable(table);

                    contentDiv.Controls.Add(table.getForm());

                    if (PageData.IsPostBack)
                    {
                        #region Postback
                        if (PageData.SubmitSource == form.SubmitSourceID)
                        {
                            //reupero precente valore posizione container
        //                    Contenitore preContainer = HomePageBusinessLogic.GetContenitoreById(Container.IntValue);
        //                    int posizionePre = preContainer.Posizione;

                            bool ok = true;
                            if (Nome.RequestVariable.StringValue.Length == 0)
                            {
                                Nome.JumpField = true;
                                //PageData.Conn.Execute("UPDATE Homepages_Contenitori SET Nome_Contenitore = '" + ContainerSystemName + "' WHERE id_Contenitore = " + ContainerID);
                                contenitore.Name = ContainerSystemName;
                                HomePageBusinessLogic.SaveOrUpdateContenitore(contenitore);
                            }
                            else
                            {
                                //rows = sqltab.Select("id_Contenitore <> " + Container.IntValue + " AND Nome_Contenitore LIKE '" + Nome.RequestVariable.StringValue + "' AND Homepage_Contenitore = " + this.HomepageID);
                                //ok = rows.Length == 0;
                                var contenitori = HomePageBusinessLogic.FindContenitoryByHomepageAndNameDifferentId(Container.IntValue, this.HomepageID, Nome.RequestVariable.StringValue);
                                ok = contenitori.Count == 0;
                            }
                            if (ok)
                            {
                                //PageData.MsgBox.InnerHtml = form.serializedUpdate();
                                string errors = form.serializedUpdate();
                                if (form.LastOperation == NetForm.LastOperationValues.OK)
                                {
   //                                 Contenitore currentContainer = HomePageBusinessLogic.GetContenitoreById(Container.IntValue);
    //                                currentContainer.Posizione = posizionePre;
    //                                HomePageBusinessLogic.SaveOrUpdateContenitore(currentContainer);

                                    G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, this.HomepageID.ToString());

                                    Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha modificato con successo il contenitore con id " + ContainerID + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                                    NetCms.GUI.PopupBox.AddSessionMessage("Contenitore modificato con successo.", NetCms.GUI.PostBackMessagesType.Success, true);
                                }
                                if (form.LastOperation == NetForm.LastOperationValues.Error)
                                {
                                    Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante la modifica del contenitore con id " + ContainerID + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "543");
                                    NetCms.GUI.PopupBox.AddMessage("Errore durante la modifica del contenitore a causa di uno dei seguenti errori: " + errors, NetCms.GUI.PostBackMessagesType.Error);
                                }
                            }
                            else
                            {
                                PageData.MsgBox.InnerHtml = "Esiste un altro contenitore con questo nome, specificare un nome diverso";
                                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "L'utente ha inserito un nome gi� presente per un contenitore di un'homepage", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                            }
                        }
                        #endregion

                        NetUtility.RequestVariable back = new NetUtility.RequestVariable("back");
                        if (back.IsValidString && back.StringValue == "Torna indietro")
                            PageData.GoBack();

                    }

                    //div.Controls.Add(Toolbar);
                    //div.Controls.Add(form.getForms(PageData.InfoToolbar.Title));

                    HtmlGenericControl pButtons = new HtmlGenericControl("p");
                    pButtons.Attributes["class"] = "buttonscontainer";

                    Button bt = new Button();
                    bt.Text = "Aggiorna Contenitore";
                    bt.ID = "send";
                    bt.CssClass = "button";
                    bt.OnClientClick = "javascript: setSubmitSource('send')";
                    pButtons.Controls.Add(bt);

                    contentDiv.Controls.Add(pButtons);

                    WebControl row = new WebControl(HtmlTextWriterTag.Div);
                    row.CssClass = "row";

                    row.Controls.Add(contentDiv);

                    fieldset.Controls.Add(row);

                    div.Controls.Add(fieldset);
                }
            }
            return div;
        }
    }
}