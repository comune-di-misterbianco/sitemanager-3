using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.Applications.Homepage.Model;
using System.Collections.Generic;
using NetCms.Grants;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.Structure.Applications.Homepage.Pages
{
    public class AddModule : BasePage
    {
        #region Oggetti da contratto NetworkPage

        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Aggiunta modulo alla Homepage della cartella '" + this.CurrentFolder.Label + "'"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.House; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        #endregion

        public AddModule()
        {
            ApplicationContext.SetCurrentComponent(HomepageApplication.ApplicationContextBackLabel);
            if (this.CurrentDocument.Criteria[CriteriaTypes.Create].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina di aggiunta di un nuovo modulo nella HomePage", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else if (!IsPostBack)
                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina di creazione di un modulo homepage nel portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
        }

        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "container-fluid";

            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");

            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerText = "Inserimento modulo";
            fieldset.Controls.Add(legend);

            NetForm form = new NetForm();
            form.CssClass = "formhomepagemoduli";
            form.SubmitLabel = "Crea Modulo";

            HtmlGenericControl contentDiv = new HtmlGenericControl("div");
            contentDiv.Attributes["class"] = "contentForm homepagemoduloform col-md-12";

            NetUtility.QueryStringRequestVariable idcont = new NetUtility.QueryStringRequestVariable("cid");
            if (!idcont.IsValidInteger || HomePageBusinessLogic.GetContenitoreById(idcont.IntValue) == null)
            {
                PageData.GoBack();
            }
            else
            {
                //HtmlGenericControl Toolbar = new HtmlGenericControl("div");
                PageData.InfoToolbar.Icon = "Home";
                PageData.InfoToolbar.Title = "Creazione nuovo modulo nella homepage '" + this.CurrentDocument.Nome + "'";

                NetFormTable table = new NetFormTable("homepages_moduli", "id_modulo", PageData.Conn);
                table.CssClass = "netformtable_homepagemoduli";

                NetHiddenField id_contenitore = new NetHiddenField("contenitore_modulo");
                id_contenitore.Value = idcont.StringValue;

                table.addField(id_contenitore);

                /* recupero la posizione dell'ultimo modulo */
                ICollection<Modulo> maxPos = HomePageBusinessLogic.FindModuliByContenitore(idcont.IntValue);
                int posizione_mod = 1;
                if (maxPos != null && maxPos.Count > 0)
                {
                    //Il valore deve essere incrementato per puntare alla prossima posizione da occupare
                    //posizione_mod = maxPos.ElementAt(0).Posizione + 1;
                    posizione_mod = maxPos.Count + 1;
                }

                
                form.AddTable(table);
                

                NetHiddenField posizione_modulo = new NetHiddenField("posizione_modulo");
                posizione_modulo.Value = posizione_mod.ToString();
                table.addField(posizione_modulo);

                NetDropDownList tipo = new NetDropDownList("Tipo:", "tipo_modulo");
                foreach (var entry in NetCms.Homepages.ModuliHomepageClassesPool.ActiveModules.OrderBy(x => x.Key))
                {
                    var attribute = System.Attribute.GetCustomAttributes(entry.Value).FirstOrDefault(x => x is NetCms.Homepages.HomepageModuleDefiner) as NetCms.Homepages.HomepageModuleDefiner;
                    if (attribute != null)
                        tipo.addItem("Modulo " + attribute.Name, attribute.ID.ToString());
                }
                tipo.AutoPostBack = true;
                table.addField(tipo);

                if (!PageData.IsPostBack)
                    contentDiv.Controls.Add(table.getForm());

                if (PageData.IsPostBack && tipo.RequestVariable.IsValidInteger)
                {
                    // tipo.CssClass = "hide"; a cosa serve ?

                    if (this.CurrentDocument.Criteria[CriteriaTypes.Advanced].Allowed)
                    {
                        NetTextBox cssClass_modulo = new NetTextBox("Classe CSS: ", "cssclass_modulo");
                        table.addField(cssClass_modulo);
                    }

                    NetDropDownList stato = new NetDropDownList("Stato:", "stato_modulo");
                    stato.Required = true;
                    stato.ClearList();
                    stato.addItem("Nascosto al pubblico", "0");
                    stato.addItem("Visibile al pubblico", "1");
                    stato.addItem("Visibile solo al pubblico autenticato", "2");
                    stato.addItem("Nascosto al pubblico autenticato", "3");
                    table.addField(stato);

                    HomepageModuleForm module = HomepageModuleForm.ModuleFactory(tipo.RequestVariable.IntValue, PageData);

                    // if (module.UseTemplate)
                    if (module.TemplateOption != HomepageModuleForm.TemplateOptionEnum.None)
                    {
                        NetDropDownList template = new NetDropDownList("Template", "TemplateMode_Modulo");
                        template.Required = true;
                        template.ClearList();

                        //switch (module.TemplateOption)
                        //{
                        //    case HomepageModuleForm.TemplateOptionEnum.Base:
                        //        template.addItem(nameof(Modulo.TemplateType.No_Template).Replace("_", " "), nameof(Modulo.TemplateType.No_Template));
                        //        template.addItem(nameof(Modulo.TemplateType.Simple), nameof(Modulo.TemplateType.Simple));
                        //        break;
                        //    case HomepageModuleForm.TemplateOptionEnum.Simple:
                        //        template.addItem(nameof(Modulo.TemplateType.No_Template).Replace("_", " "), nameof(Modulo.TemplateType.No_Template));
                        //        template.addItem(nameof(Modulo.TemplateType.Standard), nameof(Modulo.TemplateType.Standard));
                        //        template.addItem(nameof(Modulo.TemplateType.Agid), nameof(Modulo.TemplateType.Agid));
                        //        template.addItem(nameof(Modulo.TemplateType.Box), nameof(Modulo.TemplateType.Box));
                        //        break;
                        //    case HomepageModuleForm.TemplateOptionEnum.ExendedTemplate:
                        //        template.addItem(nameof(Modulo.TemplateType.No_Template).Replace("_", " "), nameof(Modulo.TemplateType.No_Template));
                        //        template.addItem(nameof(Modulo.TemplateType.Default_Style).Replace("_", " "), nameof(Modulo.TemplateType.Default_Style));
                        //        template.addItem(nameof(Modulo.TemplateType.Focus), nameof(Modulo.TemplateType.Focus));
                        //        template.addItem(nameof(Modulo.TemplateType.Gallery), nameof(Modulo.TemplateType.Gallery));
                        //        template.addItem(nameof(Modulo.TemplateType.Grid), nameof(Modulo.TemplateType.Grid));
                        //        template.addItem(nameof(Modulo.TemplateType.Masonry), nameof(Modulo.TemplateType.Masonry));
                        //        template.addItem(nameof(Modulo.TemplateType.News), nameof(Modulo.TemplateType.News));
                        //        template.addItem(nameof(Modulo.TemplateType.Carousel), nameof(Modulo.TemplateType.Carousel));
                        //        template.addItem(nameof(Modulo.TemplateType.Simple), nameof(Modulo.TemplateType.Simple));
                        //        template.addItem(nameof(Modulo.TemplateType.List), nameof(Modulo.TemplateType.List));
                        //        break;
                        //}

                        //foreach(TemplateType tpl in module.AvailableTemplate)
                        //{
                        //    template.addItem(tpl.ToString().Replace("_", " "), tpl.ToString());
                        //}
                        foreach (string tpl in module.AvailableTemplate(PageData))
                        {
                            template.addItem(tpl.Replace("_", " "), tpl.ToString());
                        }






                        table.addField(template);
                    }

                    NetForms.NetFormTable formTable = module.GetFormTable();
                    if (formTable != null)
                    {
                        
                        form.AddTable(formTable);
                        contentDiv.Controls.Add(table.getForm());
                        contentDiv.Controls.Add(formTable.getForm());
                    }

                    if (PageData.IsPostBack)
                    {
                        #region Postback
                        if (PageData.SubmitSource == form.SubmitSourceID)
                        {
                            string errors = form.serializedInsert(PageData.Request.Form);
                            if (form.LastOperation == NetForm.LastOperationValues.OK)
                            {
                                //if (PageData.Conn.Execute("UPDATE homepages_contenitori SET havemoduli_contenitore = 1 WHERE id_contenitore = " + id_contenitore.Value))
                                //{
                                //    G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.Network.SystemName, this.HomepageID.ToString());
                                //    Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha inserito con successo un nuovo modulo homepage nel portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                                //    NetCms.GUI.PopupBox.AddSessionMessage("Il modulo � stato creato con successo.", NetCms.GUI.PostBackMessagesType.Success, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                                //}
                                List<HomePageDocContent> allHome = HomePageBusinessLogic.GetAllHomePageDocContent().ToList();
                                foreach (HomePageDocContent h in allHome)
                                {
                                    // G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage",this.CurrentDocument.NetworkSystemName, this.HomepageID.ToString());
                                    G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, h.ID.ToString());
                                }
                                //G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, this.HomepageID.ToString());
                                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha inserito con successo un nuovo modulo homepage nel portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                                NetCms.GUI.PopupBox.AddSessionMessage("Il modulo � stato creato con successo.", NetCms.GUI.PostBackMessagesType.Success, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                            }

                            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante la creazione di un modulo nella homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "539");
                            NetCms.GUI.PopupBox.AddMessage("Non � stato possibile creare il modulo a causa di uno dei seguenti errori: " + errors, NetCms.GUI.PostBackMessagesType.Error);

                        }
                        #endregion
                    }
                }
                else
                    form.AddSubmitButton = false;

                //div.Controls.Add(Toolbar);
                //div.Controls.Add(form.getForms("Inserimento modulo"));

                HtmlGenericControl pButtons = new HtmlGenericControl("p");
                pButtons.Attributes["class"] = "buttonscontainer";

                Button bt = new Button();
                bt.Text = "Crea Modulo";
                bt.ID = "send";
                bt.CssClass = "button";
                bt.OnClientClick = "javascript: setSubmitSource('send')";
                pButtons.Controls.Add(bt);

                contentDiv.Controls.Add(pButtons);

                WebControl row = new WebControl(HtmlTextWriterTag.Div);
                row.CssClass = "row";

                row.Controls.Add(contentDiv);

                fieldset.Controls.Add(row);

                div.Controls.Add(fieldset);
            }
            return div;
        }
    }
}