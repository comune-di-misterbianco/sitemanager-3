﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.Grants;
using System.Collections.Generic;
using LanguageManager.Model;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.Structure.Applications.Homepage.Pages
{
    public class TranslateModule : BasePage
    {
        #region Oggetti da contratto NetworkPage

        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Traduzione modulo della Homepage della cartella '" + this.CurrentFolder.Label + "'"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.House; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        #endregion

        public NetUtility.QueryStringRequestVariable ModuloId
        {
            get
            {
                if (_ModuloId == null)
                    _ModuloId = new NetUtility.QueryStringRequestVariable("mid");
                return _ModuloId;
            }
        }
        private NetUtility.QueryStringRequestVariable _ModuloId;

        public TranslateModule()
        {
            if (this.CurrentDocument.Criteria[CriteriaTypes.Modify].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina di traduzione di un modulo all'HomePage", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else if (!IsPostBack && ModuloId.IsValidInteger)
                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina di traduzione del modulo con id " + ModuloId.IntValue + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
        }

        protected override WebControl BuildControls()
        {
            WebControl tuttalaform = new WebControl(HtmlTextWriterTag.Div);

            if (!ModuloId.IsValidInteger)
            {
                PageData.GoBack();
            }
            else
            {
                //DataTable tab = PageData.Conn.SqlQuery("SELECT * FROM homepages_moduli WHERE id_Modulo = " + Modulo.IntValue);
                ModuloStatico modulo = HomePageBusinessLogic.GetModuloStaticoById(ModuloId.IntValue);

                if (modulo != null)
                {
                    HtmlGenericControl Toolbar = new HtmlGenericControl("div");
                    PageData.InfoToolbar.Icon = "Home";
                    PageData.InfoToolbar.Title = "Traduzione Modulo";


                    tuttalaform.Controls.Add(Toolbar);


                    HomepageModuleStaticoForm module = (HomepageModuleStaticoForm)HomepageModuleForm.ModuleFactory(modulo.Tipo, ModuloId.IntValue);

                    IList<LanguageManager.Model.Language> languages = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages();
                    if (languages != null && languages.Count > 0)
                    {
                        foreach (LanguageManager.Model.Language l in languages.Where(x => x.Default_Lang == 0))
                        {
                            Collapsable colTrad = new Collapsable("Traduzione " + l.Nome_Lang);
                            colTrad.ID = "Modulo_Lang#" + l.LangTwoLetterCode;
                            colTrad.StartCollapsed = true;

                            NetForm form = new NetForm();
                            form.SubmitSourceID = "Send_" + l.LangTwoLetterCode;
                            form.SubmitLabel = "Aggiorna traduzione";

                            int idModuloLang = 0;
                            //Se esiste il record riempio la form
                            if (modulo.ModuloLangs.Count(x => x.Lang == l.ID) > 0)
                            {
                                ModuloStaticoLang moduloLang = modulo.ModuloLangs.First(x => x.Lang == l.ID);
                                idModuloLang = moduloLang.ID;
                            }

                            NetForms.NetFormTable formTable = module.GetLangFormTable(idModuloLang, l.Nome_Lang,l.LangTwoLetterCode);
                            if (formTable != null)
                                form.AddTable(formTable);
                            
                            colTrad.AppendControl(form.getForms("Traduzione in " + l.Nome_Lang + " del modulo"));
                            tuttalaform.Controls.Add(colTrad);
                        }
                    }


                    if (PageData.IsPostBack)
                    {
                        string submitSource = PageData.SubmitSource;
                        string submitLang = submitSource.Split('_').Last();
                        #region Postback
                        try
                        {
                            NetForm form = new NetForm();
                            string errors = form.serializedValidation(HttpContext.Current.Request.Form);
                            if (string.IsNullOrEmpty(errors))
                            {
                                Language languageToAdd = languages.First(x => x.LangTwoLetterCode == submitLang);

                                //string Nome = HttpContext.Current.Request.Form["Label_DocLang_" + submitLang].ToString();
                                string Titolo = HttpContext.Current.Request.Form["titolo_modulo_statico_" + submitLang].ToString();
                                string Testo = HttpContext.Current.Request.Form["testo_modulo_statico_" + submitLang].ToString();
                                string Link = HttpContext.Current.Request.Form["link_modulo_statico_" + submitLang].ToString();
                                string Title = HttpContext.Current.Request.Form["title_modulo_statico_" + submitLang].ToString();
                                string Img = HttpContext.Current.Request.Form["img_modulo_statico_" + submitLang].ToString();
                                string ImgAlt = HttpContext.Current.Request.Form["imgAlt_modulo_statico_" + submitLang].ToString();
                                string ImgAll = HttpContext.Current.Request.Form["imgAll_modulo_statico_" + submitLang].ToString();

                                ModuloStaticoLang moduloLang = null;
                                //Se esiste il record lo modifico
                                if (modulo.ModuloLangs.Count(x => x.Lang == languageToAdd.ID) > 0)
                                {
                                    moduloLang = modulo.ModuloLangs.First(x => x.Lang == languageToAdd.ID);
                                }
                                else
                                {
                                    //Se non esiste lo creo
                                    moduloLang = new ModuloStaticoLang();
                                    moduloLang.Lang = languageToAdd.ID;
                                    moduloLang.ModuloParent = modulo;
                                    modulo.ModuloLangs.Add(moduloLang);
                                }

                                moduloLang.Titolo = Titolo;
                                moduloLang.Testo = Testo;
                                moduloLang.Link = Link;
                                moduloLang.Title = Title;
                                moduloLang.Img = Img;
                                moduloLang.ImgAlt = ImgAlt;
                                moduloLang.ImgAll = ImgAll;

                                HomePageBusinessLogic.SaveOrUpdateModulo(modulo);
                                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha modificato con successo la traduzione '" + languageToAdd.Nome_Lang + "' per il modulo con id " + ModuloId.IntValue + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                                NetCms.GUI.PopupBox.AddSessionMessage("Traduzione '" + languageToAdd.Nome_Lang + "' salvata con successo!", PostBackMessagesType.Success, false);
                            }
                            else
                            {
                                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante la traduzione '" + submitLang + "' del modulo con id " + ModuloId.IntValue + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "545");
                                NetCms.GUI.PopupBox.AddSessionMessage(errors, PostBackMessagesType.Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante la traduzione '" + submitLang + "' del modulo con id " + ModuloId.IntValue + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "545");
                            NetCms.GUI.PopupBox.AddSessionMessage("Si è verificato un errore durante il salvataggio delle modifiche alla traduzione per il modulo corrente!", PostBackMessagesType.Error);
                        }
                        //if (PageData.SubmitSource == form.SubmitSourceID)
                        //{
                        //    string errors = form.serializedUpdate();
                        //    if (form.LastOperation == NetForm.LastOperationValues.OK)
                        //    {
                        //        //G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, this.HomepageID.ToString());
                        //        List<HomePageDocContent> allHome = HomePageBusinessLogic.GetAllHomePageDocContent();

                        //        if (allHome != null)
                        //        {

                        //            foreach (HomePageDocContent h in allHome)
                        //            {
                        //                // G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage",this.CurrentDocument.NetworkSystemName, this.HomepageID.ToString());
                        //                G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, h.ID.ToString());

                        //            }
                        //            G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, this.HomepageID.ToString());
                        //        }
                        //        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha modificato con successo il modulo con id " + ModuloId.IntValue + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                        //        NetCms.GUI.PopupBox.AddSessionMessage(errors, PostBackMessagesType.Success, true);
                        //    }
                        //    else
                        //    {
                        //        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante la modifica del modulo con id " + ModuloId.IntValue + " dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "545");
                        //        NetCms.GUI.PopupBox.AddSessionMessage(errors, PostBackMessagesType.Error);
                        //    }
                        //}
                        #endregion

                        NetUtility.RequestVariable back = new NetUtility.RequestVariable("back");
                        if (back.IsValidString && back.StringValue == "Torna indietro")
                            PageData.GoBack();

                    }
                                                            
                }
            }
            return tuttalaform;
        }
    }
}