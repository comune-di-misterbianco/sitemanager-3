using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Grants;
using NetCms.Structure.WebFS;
using System.Linq;
using System.Collections.Generic;
using NetCms.Structure.Applications.Homepage.Model;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.Structure.Applications.Homepage.Pages
{
    public class HomepageManager : NetCms.Structure.Applications.Homepage.Pages.BasePage
    {
        #region Oggetti da contratto NetworkPage

        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Gestione Homepage della cartella '" + this.CurrentFolder.Label + "'"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.House; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        #endregion

        public HomepageManager()
        {
            if (this.CurrentDocument.Criteria[CriteriaTypes.Modify].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina di gestione dell'HomePage", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }           

            StructureFolder currentfolder = Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder as StructureFolder;
            var folders = currentfolder.SubFolders.Where(x => x.Nome.ToLower() == "ImgHome".ToLower());

            StructureFolder folder = null;
            
            if (folders != null && folders.Count() > 0)
                folder = folders.First();
                        
            if (folder == null)            
                this.CurrentFolder.CreateFolder("Immagini della Homepage", "ImgHome", "Contenitore delle immagini della Homepage", 4, 1, "", 0, 1);

            this.JqueryUiEnable = false;

            OuterScripts.Add("<script type=\"text/javascript\" src=\"" + NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/shared/sortable/html5sortable.min.js\"></script>\n");
        }

        private void SetToolbars()
        {
            /*******************************************************/
            /* Aggiungere i bottoni relativi alle colonne con i 
             * controlli associati allo stato delle colonne 
            /*******************************************************/

            if (ColSx != null && ColSx.Stato)
                Toolbar.Buttons.Add(new ToolbarButton("javascript:setFormValue(" + ColSx.ID.ToString() + ", '" + HomepageApplication.InputFieldIDColumnStatus + "',1);", NetCms.GUI.Icons.Icons.Layout_Delete, "Disattiva colonna sinistra"));
            else
                Toolbar.Buttons.Add(new ToolbarButton("javascript:setFormValue('Colsx','" + HomepageApplication.InputFieldIDColumnStatus + "', 1);", NetCms.GUI.Icons.Icons.Layout_Add, "Attiva colonna sinistra"));

            if (ColDx != null && ColDx.Stato)
                Toolbar.Buttons.Add(new ToolbarButton("javascript:setFormValue(" + ColDx.ID.ToString() + ",'" + HomepageApplication.InputFieldIDColumnStatus + "',1);", this.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Toolbar_Coldx_Del), "Disattiva colonna destra"));
            else
                Toolbar.Buttons.Add(new ToolbarButton("javascript:setFormValue('Coldx','" + HomepageApplication.InputFieldIDColumnStatus + "',1);", this.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Toolbar_Coldx_Add), "Attiva colonna destra"));

            if (Footer != null && Footer.Stato)
                Toolbar.Buttons.Add(new ToolbarButton("javascript:setFormValue(" + Footer.ID.ToString() + ",'" + HomepageApplication.InputFieldIDColumnStatus + "',1);", this.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Toolbar_Footer_Del), "Disattiva Pi� di pagina"));
            else
                Toolbar.Buttons.Add(new ToolbarButton("javascript:setFormValue('Footer','" + HomepageApplication.InputFieldIDColumnStatus + "',1);", this.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Toolbar_Footer_Add), "Attiva Pi� di pagina"));
            //if (Footer != null && Footer.Stato && Footer.Contenitore.Eredita)
            //    Toolbar.Buttons.Add(new ToolbarButton("", this.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Toolbar_Footer_Add),"Disattiva Eredita da HomePage Padre"));
            //else
            //Toolbar.Buttons.Add(new ToolbarButton("", this.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Toolbar_Footer_Add), "Aggiorna Cache"));
           
           
            if (this.CurrentDocument.Criteria[CriteriaTypes.Advanced].Allowed)
            {
                Toolbar.Buttons.Add(new ToolbarButton(this.CurrentNetwork.Paths.AbsoluteAdminRoot + "/docs/docs_meta.aspx?doc=" + CurrentDocument.ID, NetCms.GUI.Icons.Icons.Page_White_Code, "Meta"));
                Toolbar.Buttons.Add(new ToolbarButton(this.CurrentNetwork.Paths.AbsoluteAdminRoot + "/docs/docs_mod.aspx?folder=" + CurrentDocument.Folder.ID + "&doc=" + CurrentDocument.ID, NetCms.GUI.Icons.Icons.Cog_Edit, "Propriet� Homepage"));
            }
            //Toolbar.Controls.Add(BuildCacheControl());
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            SetToolbars();
            
        }

        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);

            string Utente = PageData.Account.NomeCompleto + " (" + PageData.Account.UserName + ")";
            string logText = "L'utente " + Utente + "vuole gestire i contenuti dell' Homepage ";
            GuiLog.SaveLogRecord(logText);

            string errore = "<fieldset>"
                              + "<legend>Errore</legend>"
                              + "<p>Errore: l'homepage selezionata non � stata trovata!</p>"
                              + "</fieldset>";
            div.Controls.Add(HiddenFields);
            if (Root != null)
            {

                WebControl btnContainer = new WebControl(HtmlTextWriterTag.Div);
                btnContainer.CssClass = "btn-container";

                string code = @"<input type=""hidden"" name=""containerOrder"" id=""containerOrder"" value="""" />"
                           + @"<input type=""button"" name=""sortRows""  id=""sortRows"" value=""Ordina righe"" class=""btn btn-primary btn-sm"" />"
                           + @"<input type=""submit"" name=""abortSortRows""  id=""abortSortRows"" value=""Annulla"" class=""btn btn-secondary btn-sm ml-2"" disabled=""disabled"" />";


                btnContainer.Controls.Add(new LiteralControl(code));

                Button saveSettings = new Button();
                saveSettings.ID = "saveSortRows";
                saveSettings.Attributes["name"] = "saveSortRows";
                saveSettings.Text = "Salva";
                saveSettings.CssClass = "btn btn-primary btn-sm";
                saveSettings.Enabled = false;
                saveSettings.Click += SaveSettings_Click    ;

                
                btnContainer.Controls.Add(saveSettings);
                div.Controls.Add(btnContainer);

                HtmlGenericControl rootControl = Root.GetControl();
                div.Controls.Add(Root.BuildEreditaControl());
                
                div.Controls.Add(rootControl);
                
                if (ColSx == null && ColDx != null)
                    ((HtmlGenericControl)rootControl.Controls[1]).Attributes["class"] += " HomepageContainer_ColcxHalf";

                if (ColDx == null && ColSx != null)
                    ((HtmlGenericControl)rootControl.Controls[2]).Attributes["class"] += " HomepageContainer_ColcxHalf";

                if (ColDx == null && ColSx == null)
                    ((HtmlGenericControl)rootControl.Controls[1]).Attributes["class"] += " HomepageContainer_ColcxBig";

                if (ColDx != null && ColSx != null)
                    ((HtmlGenericControl)rootControl.Controls[2]).Attributes["class"] += " HomepageContainer_ColcxSmall";
               // decommentare la riga sottostante nel qual caso si voglia aggiungere il div in fondo alla pagina con il pulsante per aggiornare la cache dei moduli
               if (NetCms.Users.AccountManager.CurrentAccount != null && NetCms.Users.AccountManager.CurrentAccount.IsAdministrator)
                div.Controls.Add(BuildCacheControl());
            }
            else
            {
                NetCms.GUI.PopupBox.AddMessage(errore, NetCms.GUI.PostBackMessagesType.Error);
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non � stato possibile trovare l'homepage selezionata", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "139");
            }
            return div;
        }

        private void SaveSettings_Click(object sender, EventArgs e)
        {
            // implementare logica di recupero e salvataggio posizione rows (contenitori)
            NetUtility.RequestVariable reqRowsOrder = new NetUtility.RequestVariable("containerOrder", NetUtility.RequestVariable.RequestType.Form);
            if (reqRowsOrder.IsPostBack && reqRowsOrder.IsValidString)
            {
                string strRowsOrder = reqRowsOrder.StringValue.Replace("Block","");
                var rowsOrder = strRowsOrder.Split(',');

                Dictionary<string, string> rows = new Dictionary<string, string>();

                foreach(string row in rowsOrder)
                {
                    var strRow = row.Split('|');
                    rows.Add(strRow[0], strRow[1]);
                }
                
                HomePageBusinessLogic.SaveRowsOrder(rows);

                // invalidazione homepage corrente
                G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentDocument.NetworkSystemName, this.HomepageID.ToString());

                // ricaricamento pagina
                NetCms.GUI.PopupBox.AddSessionMessage("Aggiornamento effettuato", NetCms.GUI.PostBackMessagesType.Success, "modify.aspx?folder="+CurrentFolder.ID+"&doc="+this.CurrentDocument.ID);

                //2478|1,2480|2,2479|3,2481|4,2482|5,2483|6,2484|7,2485|8,1951|9,1952|10,2081|11,2411|12,2477|13,2319|14
                //Console.WriteLine(rowsOrder);

            }
        }

        public void refreshCache_ButtonClick(object sender,EventArgs e)
        {
            List<HomePageDocContent> allHome = HomePageBusinessLogic.GetAllHomePageDocContent().ToList();

            //G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentDocument.NetworkSystemName, this.HomepageID.ToString());
            foreach (HomePageDocContent h in allHome)
            {
                // G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage",this.CurrentDocument.NetworkSystemName, this.HomepageID.ToString());
                G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentDocument.NetworkSystemName, h.ID.ToString());
            }
        }

        protected WebControl BuildCacheControl()
        {
            WebControl divCache = new WebControl(HtmlTextWriterTag.Div);
            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
            legend.Controls.Add(new LiteralControl("Aggiorna Cache"));
            fieldset.Controls.Add(legend);

            Button refresh = new Button();
            refresh.ID = "submitCache_buttton";
            refresh.Text = "Aggiorna Cache Moduli";
            refresh.CssClass = "refresh_cache";
            refresh.Click += new EventHandler(refreshCache_ButtonClick);

            fieldset.Controls.Add(new LiteralControl("Aggiorna la cache dei moduli nelle homepage</br></br>"));
            fieldset.Controls.Add(refresh);
            divCache.Controls.Add(fieldset);

            //WebControl a = new WebControl(HtmlTextWriterTag.A);
            // a.Controls.Add(refresh);
            //WebControl span = new WebControl(HtmlTextWriterTag.Span);
            //WebControl span2 = new WebControl(HtmlTextWriterTag.Span);

            //span.Attributes.Add("style", "background-image:url(/cms/css/admin/images/icons/arrow_refresh.png)");
            //span2.Controls.Add(refresh);
            //span.Controls.Add(span2);
            //a.Controls.Add(span);
            //WebControl li = new WebControl(HtmlTextWriterTag.Li);
            //li.Attributes.Add("class", "SideToolbarButton");
            //li.Controls.Add(span);

            return divCache;

        }
    }
}