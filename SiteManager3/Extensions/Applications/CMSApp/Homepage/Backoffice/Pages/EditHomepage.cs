using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.Grants;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.Structure.Applications.Homepage.Pages
{
    public class EditHomepage : NetCms.Structure.Applications.Homepage.Pages.BasePage
    {
        #region Oggetti da contratto NetworkPage

        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Aggiornamento dati Homepage della cartella " + this.CurrentFolder.Label + "'"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.House; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        #endregion

        public EditHomepage()
        {
            if (this.CurrentDocument.Criteria[CriteriaTypes.Modify].Value == GrantsValues.Deny)
            {
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina di aggiornamento dell'HomePage", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
            }
            else if (!IsPostBack && HomepageID > 0)
                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina di modifica dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
        }
        protected override WebControl BuildControls()
        {
            WebControl tuttalaform = new WebControl(HtmlTextWriterTag.Div);
            if (HomepageID <= 0 || !CurrentDocument.Grant)
            {
                PageData.GoToErrorPage(CurrentDocument);
            }
            else
            {
                //DataTable tab = this.Connection.SqlQuery("SELECT * FROM homepages WHERE id_Homepage = " + this.CurrentDocument.Content);
                HomePageDocContent homepageDocContent = HomePageBusinessLogic.GetHomepageContentById(this.CurrentDocument.Content);

                if (homepageDocContent != null)
                {
                    HtmlGenericControl Toolbar = new HtmlGenericControl("div");
                    PageData.InfoToolbar.Icon = "Home";
                    PageData.InfoToolbar.Title = "Aggiornamento Dati Homepage";

                    NetFormTable table = new NetFormTable("homepages_docs_contents", "id_Homepage", this.Conn, this.HomepageID);

                    NetTextBox Msgmanu = new NetTextBox("Messaggio Manutenzione: ", "MsgManutenzione_homepage");
                    Msgmanu.Required = false;
                    Msgmanu.MaxLenght = 0;
                    table.addField(Msgmanu);

                    NetDropDownList stato = new NetDropDownList("Stato:", "Stato_Homepage");
                    stato.Required = true;
                    stato.addItem("Visibile al pubblico", "0");
                    stato.addItem("In Manutenzione", "1");
                    table.addField(stato);

                    NetForm form = new NetForm();
                    form.SubmitLabel = "Aggiorna Homepage";
                    form.AddTable(table);

                    if (PageData.IsPostBack)
                    {
                        #region Postback
                        if (PageData.SubmitSource == form.SubmitSourceID)
                        {
                            PageData.MsgBox.InnerHtml = form.serializedUpdate();
                            
                            G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", this.CurrentFolder.StructureNetwork.SystemName, this.HomepageID.ToString());
                            if (form.LastOperation == NetForm.LastOperationValues.OK)
                                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha modificato con successo l'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
                            if (form.LastOperation == NetForm.LastOperationValues.Error)
                                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error,"Errore durante la modifica dell'homepage del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "","544");
                        }
                        #endregion

                        NetUtility.RequestVariable back = new NetUtility.RequestVariable("back");
                        if (back.IsValidString && back.StringValue == "Torna indietro")
                            PageData.GoBack();

                    }

                    tuttalaform.Controls.Add(Toolbar);
                    tuttalaform.Controls.Add(form.getForms(PageData.InfoToolbar.Title));
                }
            }
            return tuttalaform;
        }
    }
}