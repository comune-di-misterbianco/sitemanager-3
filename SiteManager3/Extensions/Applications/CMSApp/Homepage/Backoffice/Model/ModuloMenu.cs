﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NetCms.Structure.Applications.Homepage.Model
{
    public class ModuloMenu : Modulo
    {
        public virtual int Starting
        {
            get;
            set;
        }

        public virtual int Ending
        {
            get;
            set;
        }

        public virtual int FirstLevel
        {
            get;
            set;
        }

        public virtual int Type
        {
            get;
            set;
        }

        public ModuloMenu() : base() { }

        public virtual void FillModuloInfoFromImport(DataRow baseInfo, DataRow moduloInfo, Contenitore container)
        {
            base.FillBaseInfoFromImport(baseInfo, container);

            if (moduloInfo.Table.Columns.Contains("Starting_ModuloMenu") && moduloInfo["Starting_ModuloMenu"].ToString().Length > 0)
                this.Starting = int.Parse(moduloInfo["Starting_ModuloMenu"].ToString());
            if (moduloInfo.Table.Columns.Contains("Ending_ModuloMenu") && moduloInfo["Ending_ModuloMenu"].ToString().Length > 0)
                this.Ending = int.Parse(moduloInfo["Ending_ModuloMenu"].ToString());
            if (moduloInfo.Table.Columns.Contains("FirstLevel_ModuloMenu") && moduloInfo["FirstLevel_ModuloMenu"].ToString().Length > 0)
                this.FirstLevel = int.Parse(moduloInfo["FirstLevel_ModuloMenu"].ToString());
            if (moduloInfo.Table.Columns.Contains("Type_ModuloMenu") && moduloInfo["Type_ModuloMenu"].ToString().Length > 0)
                this.Type = int.Parse(moduloInfo["Type_ModuloMenu"].ToString());
        }
    }
}
