﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCms.Structure.Applications.Homepage.Model
{
    public class ModuloTags : Modulo
    {
        public virtual int NetworkID
        {
            get;
            set;
        }

        public virtual int Records
        {
            get;
            set;
        }

        public ModuloTags() : base() { }

        public virtual void FillModuloInfoFromImport(DataRow baseInfo, DataRow moduloInfo, Contenitore container)
        {
            base.FillBaseInfoFromImport(baseInfo, container);
                 
            if (moduloInfo.Table.Columns.Contains("Records_ModuloTags") && moduloInfo["Records_ModuloTags"].ToString().Length > 0)
                this.Records = int.Parse(moduloInfo["Records_ModuloTags"].ToString());
        }
    }
}
