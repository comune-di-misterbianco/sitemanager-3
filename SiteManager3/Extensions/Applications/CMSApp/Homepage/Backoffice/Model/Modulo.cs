﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NetCms.Structure.Applications.Homepage.Model
{
    public abstract class Modulo 
    {
        public Modulo()
        {
            ModuliCircolari = new HashSet<ModuloCircolare>();
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual Contenitore Contenitore
        {
            get;
            set;
        }

        public virtual int Posizione
        {
            get;
            set;
        }

        public virtual int Tipo
        {
            get;
            set;
        }

        public virtual int Stato
        {
            get;
            set;
        }

        public virtual string CssClass
        {
            get;
            set;
        }

        public virtual  string ShowOnlyInUrl 
        {
            get;
            set; 
        }

        public virtual ICollection<ModuloCircolare> ModuliCircolari
        {
            get;
            set;
        }                

        public virtual string TemplateMode
        {
            get;
            set;
        }

        /// <summary>
        /// Indica al sistema quali enum usare TemplateType oppure usare l'enumeratore per i moduli applicativi ApplicativoTemplateType        
        /// </summary>
        public virtual bool UseExendedTemplate
        {
            get { return false; }
        }

        

        protected virtual void FillBaseInfoFromImport(DataRow baseInfo,Contenitore container)
        {
            this.Contenitore = container;
            if (baseInfo.Table.Columns.Contains("Posizione_Modulo") && baseInfo["Posizione_Modulo"].ToString().Length > 0)
                this.Posizione = int.Parse(baseInfo["Posizione_Modulo"].ToString());
            if (baseInfo.Table.Columns.Contains("Tipo_Modulo") && baseInfo["Tipo_Modulo"].ToString().Length > 0)
                this.Tipo = int.Parse(baseInfo["Tipo_Modulo"].ToString());
            if (baseInfo.Table.Columns.Contains("Stato_Modulo") && baseInfo["Stato_Modulo"].ToString().Length > 0)
                this.Stato = int.Parse(baseInfo["Stato_Modulo"].ToString());
            if (baseInfo.Table.Columns.Contains("CssClass_modulo"))
                this.CssClass = baseInfo["CssClass_modulo"].ToString();
        }

    }
}
