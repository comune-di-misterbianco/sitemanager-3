﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml.Linq;
using NetCms.Structure.WebFS;
using System.ComponentModel;

namespace NetCms.Structure.Applications.Homepage.Model
{
    public class ModuloApplicativo : Modulo
    {
        public ModuloApplicativo() : base() { }

        public virtual int Subfolders
        {
            get;
            set;
        }

        public virtual int Records
        {
            get;
            set;
        }

        public virtual int ShowDesc
        {
            get;
            set;
        }

        public virtual int FolderId
        {
            get;
            set;
        }

        public virtual int DescLength
        {
            get;
            set;
        }

        public virtual int Order
        {
            get;
            set;
        }

        public virtual bool ShowMoreLink
        {
            get;
            set;
        }

        public virtual bool HideImages
        {
            get;
            set;
        }

        public override bool UseExendedTemplate
        {
            get { return true; }
        }

        public virtual void FillModuloInfoFromImport(DataRow baseInfo, DataRow moduloInfo, Contenitore container, int networkId, string sourceFolderPath)
        {
            base.FillBaseInfoFromImport(baseInfo,container);

            if (moduloInfo.Table.Columns.Contains("Subfolders_ModuloApplicativo"))
            {
                int res;
                if (int.TryParse(moduloInfo["Subfolders_ModuloApplicativo"].ToString(), out res))
                    this.Subfolders = res;
                else
                    this.Subfolders = 0;
            }
            if (moduloInfo.Table.Columns.Contains("Records_ModuloApplicativo"))
                this.Records = int.Parse(moduloInfo["Records_ModuloApplicativo"].ToString());
            if (moduloInfo.Table.Columns.Contains("ShowDesc_ModuloApplicativo"))
                this.ShowDesc = int.Parse(moduloInfo["ShowDesc_ModuloApplicativo"].ToString());

            #region FolderId

            string id = moduloInfo["Folder_ModuloApplicativo"].ToString();

            var all = XDocument.Load(sourceFolderPath + "\\" + "data.xml").Descendants("NewDataSet");
            var folder = from fold in all.Descendants("folders")
                         where fold.Element("id_Folder").Value == id
                         select new
                         {
                             Name = fold.Element("Name_Folder").Value,
                             Path = fold.Element("Path_Folder").Value
                         };
            var selectedFOlder = folder.FirstOrDefault();

            if (selectedFOlder != null)
            {
                string newPath = "";
                string[] pathSplitted = folder.First().Path.Split('/');
                foreach(string s in pathSplitted)
                {
                    if (s.Length > 0 && s != "/")
                        newPath += "/" + FsFolderName(s);
                }


                StructureFolder folderInserted = NetCms.Structure.WebFS.FileSystemBusinessLogic.FolderBusinessLogic.GetFolderRecordByNameAndPath(FsFolderName(folder.First().Name), newPath, networkId);
                //DataTable newfolder = this.Network.Connection.SqlQuery("select * from " + this.Network.SystemName + "_folders where Name_Folder='" + folder.First().Name + "' AND Path_Folder='" + folder.First().Path + "'");
                if (folderInserted != null)
                    this.FolderId = folderInserted.ID;
                
            }

            #endregion

            if (moduloInfo.Table.Columns.Contains("DescLength_ModuloApplicativo") && moduloInfo["DescLength_ModuloApplicativo"].ToString().Length > 0)
                this.DescLength = int.Parse(moduloInfo["DescLength_ModuloApplicativo"].ToString());
            if (moduloInfo.Table.Columns.Contains("Order_ModuloApplicativo") && moduloInfo["Order_ModuloApplicativo"].ToString().Length > 0)
                this.Order = int.Parse(moduloInfo["Order_ModuloApplicativo"].ToString());
        }

        private string FsFolderName(string folderName)
        {
            string FileSystemName = folderName;
            FileSystemName = FileSystemName.Trim();
            FileSystemName = FileSystemName.ToLower();

            for (int i = 0; i < FileSystemName.Length; i++)
            {
                int integerValue;
                int convertedPw = Convert.ToInt32(FileSystemName[i]);
                if (!int.TryParse(FileSystemName[i].ToString(), out integerValue) && (convertedPw < 97 || convertedPw > 122))
                    FileSystemName = FileSystemName.Replace(FileSystemName[i], '-');
            }
            //Rimuovo i doppi trattini
            while (FileSystemName.Contains("--"))
                FileSystemName = FileSystemName.Replace("--", "-");

            //Rimuovo i trattini alla fine del nome
            while (FileSystemName.EndsWith("-"))
                FileSystemName = FileSystemName.Remove(FileSystemName.Length - 1);

            //Rimuovo i trattini all inizio del nome
            while (FileSystemName.StartsWith("-"))
                FileSystemName = FileSystemName.Substring(1, FileSystemName.Length - 1);

            //Rimuovo i punti alla fine del nome
            while (FileSystemName.EndsWith("."))
                FileSystemName = FileSystemName.Remove(FileSystemName.Length - 1);

            //Taglio il nome della cartella se è più lungo di 60 caratteri
            if (FileSystemName.Length > 60)
                FileSystemName = FileSystemName.Remove(60);

            while (FileSystemName.EndsWith("-"))
                FileSystemName = FileSystemName.Remove(FileSystemName.Length - 1);
            return FileSystemName;
        }     
        
    }

    //public class ModuloApplicativoNews : ModuloApplicativo
    //{
    //    public ModuloApplicativoNews() : base() { }
    //    public virtual int ShowCustomNews { get; set; }
    //    public enum ShowCustom
    //    {
    //        [Description("Tutte le news")]
    //        Tutte = 1,
    //        [Description("Solo le news in evidenza")]
    //        InEvidenza = 2,
    //        [Description("Solo le news non in evidenza")]
    //        NonInEvidenza = 3
    //    }
    //}

}
