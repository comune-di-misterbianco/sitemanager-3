﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NetCms.Structure.Applications.Homepage.Model
{
    public class ModuloCircolare : Modulo
    {
        public ModuloCircolare() : base() { }

        public virtual Modulo ModuloCollegato
        {
            get;
            set;
        }

        public virtual void FillModuloInfoFromImport(DataRow baseInfo, DataRow moduloInfo, Contenitore container)
        {
            base.FillBaseInfoFromImport(baseInfo, container);


        }
    }
}
