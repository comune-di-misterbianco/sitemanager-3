﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Structure.WebFS;
using System.Data;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NHibernate;
using NetCms.Networks;
using System.Collections;

namespace NetCms.Structure.Applications.Homepage.Model
{
    public class HomePageDocContent : DocContent
    {
        public HomePageDocContent()
        {
            Contenitori = new HashSet<Contenitore>();
        }

        public virtual string Nome
        {
            get;
            set;
        }

        public virtual string MetaDescription
        {
            get;
            set;
        }

        public virtual string Keywords
        {
            get;
            set;
        }

        public virtual string MsgManutenzione
        {
            get;
            set;
        }

        public virtual int Stato
        {
            get;
            set;
        }

        public virtual ICollection<Contenitore> Contenitori
        {
            get;
            set;
        }

        public override DocContent ImportDocContent(DataRow docRecord, ISession session, DataSet sourceDB, NetworkKey netKey, string sourceFolderPath, ref Document newDoc)
        {
            
            HomePageDocContent homepageContent = new HomePageDocContent();
            homepageContent.Stato = int.Parse(docRecord["Stato_homepage"].ToString().Replace("'", "''"));
            if (docRecord.Table.Columns.Contains("Nome_homepage"))
                homepageContent.Nome = docRecord["Nome_homepage"].ToString().Replace("'", "''");
            if (docRecord.Table.Columns.Contains("MetaDescription_homepage"))
                homepageContent.MetaDescription = docRecord["MetaDescription_homepage"].ToString().Replace("'", "''");
            if (docRecord.Table.Columns.Contains("Keywords_homepage"))
                homepageContent.Keywords = docRecord["Keywords_homepage"].ToString().Replace("'", "''");
            if (docRecord.Table.Columns.Contains("MsgManutenzione_homepage"))
                homepageContent.MsgManutenzione = docRecord["MsgManutenzione_homepage"].ToString().Replace("'", "''");

            HomePageBusinessLogic.SaveOrUpdateHomepage(homepageContent, session);
            
            #region Inserimento contenitori

            var table = sourceDB.Tables["homepages_contenitori"];
            DataRow[] cont = table.Select("homepage_contenitore = '" + docRecord["ID_homepage"].ToString() + "'");
            DataTable tableContenitori = sourceDB.Tables["homepages_contenitori"].Clone();
            foreach (DataRow row in cont)
                tableContenitori.ImportRow(row);

            Hashtable hashContenitori = new Hashtable();
            
            foreach(DataRow contenitore in tableContenitori.Rows)
            {
                Contenitore container = new Contenitore();
                container.Name = contenitore["nome_contenitore"].ToString();
                container.Depth = int.Parse(contenitore["depth_contenitore"].ToString());
                container.Stato = int.Parse(contenitore["stato_contenitore"].ToString());
                container.Posizione = int.Parse(contenitore["posizione_contenitore"].ToString());
                if (hashContenitori.Count > 0 && hashContenitori[contenitore["parent_contenitore"].ToString()] != null)
                    (hashContenitori[contenitore["parent_contenitore"].ToString()] as Contenitore).ChildsBlocks.Add(container);
                container.Homepage = homepageContent;
                homepageContent.Contenitori.Add(container);
                if (hashContenitori.Count > 0 && hashContenitori[contenitore["parent_contenitore"].ToString()] != null)
                    container.Parent = hashContenitori[contenitore["parent_contenitore"].ToString()] as Contenitore;

                container = HomePageBusinessLogic.SaveOrUpdateContenitore(container, session);

                if (hashContenitori.Count > 0 && hashContenitori[contenitore["parent_contenitore"].ToString()] != null)
                    container.Tree = container.Parent.Tree + NetUtility.TreeUtility.FormatNumber(container.ID.ToString()) + ".";
                else
                    container.Tree = "." + NetUtility.TreeUtility.FormatNumber(container.ID.ToString()) + ".";

                if (container.Name.StartsWith("Row"))
                {
                    container.Name = "Row" + container.ID;
                }

                hashContenitori.Add(contenitore["ID_contenitore"].ToString(), container);
            }

            #endregion

            #region Inserimento moduli

            DataTable tableModuli = sourceDB.Tables["homepages_moduli"];
            
            DataTable tableModApplicativi = sourceDB.Tables["homepages_moduli_applicativi"];
            if (tableModApplicativi != null)
                ImportHomepageModules(ModuleType.ModuliApplicativi, session, sourceDB, tableModuli, tableModApplicativi, hashContenitori, netKey, sourceFolderPath);

            //ATTENZIONE: Questi moduli non sono più presenti!!!
            //DataTable tableModDocumenti = sourceDB.Tables["homepages_moduli_documenti"];
            //if (tableModDocumenti != null)
            //    ImportHomepageModules(ModuleType.ModuliDocumenti, session, tableModuli, tableModDocumenti, hashContenitori);

            DataTable tableModMenu = sourceDB.Tables["homepages_moduli_menu"];
            if (tableModMenu != null)
                ImportHomepageModules(ModuleType.ModuliMenu, session, sourceDB, tableModuli, tableModMenu, hashContenitori, netKey);

            DataTable tableModRss = sourceDB.Tables["homepages_moduli_rss"];
            if (tableModRss != null)
                ImportHomepageModules(ModuleType.ModuliRss, session, sourceDB, tableModuli, tableModRss, hashContenitori, netKey);

            DataTable tableModStatici = sourceDB.Tables["homepages_moduli_statici"];
            if (tableModStatici != null)
                ImportHomepageModules(ModuleType.ModuliStatici, session, sourceDB, tableModuli, tableModStatici, hashContenitori, netKey,sourceFolderPath);

            DataTable tableModCircolari = sourceDB.Tables["homepages_moduli_circolari"];
            if (tableModCircolari != null)
                ImportHomepageModules(ModuleType.ModuliCircolari, session, sourceDB, tableModuli, tableModCircolari, hashContenitori, netKey);

            #endregion

            return homepageContent;
        }

        public override DocContent ImportDocContent(Dictionary<string, string> docRecord, ISession session)
        {
            throw new NotImplementedException();
        }

        private enum ModuleType { ModuliApplicativi,ModuliLogin,ModuliMenu,ModuliRss,ModuliStatici,ModuliCircolari };

        private void ImportHomepageModules(ModuleType tipoModuli, ISession session, DataSet sourceDB, DataTable tableModuliBase, DataTable tableModuliSpecific, Hashtable contenitoriTable, NetworkKey netKey, string sourceFolderPath = "")
        {
            foreach (DataRow SpecificModule in tableModuliSpecific.Rows)
            {
                
                DataRow[] baseInfos;    
                switch (tipoModuli)
                {
                    case ModuleType.ModuliApplicativi:
                        baseInfos = tableModuliBase.Select("id_Modulo = '" + SpecificModule["Modulo_ModuloApplicativo"].ToString() + "'");
                        if (baseInfos.Length > 0)
                        {
                            DataRow baseModuleInfo = baseInfos[0];
                            Contenitore container = contenitoriTable[baseModuleInfo["Contenitore_Modulo"].ToString()] as Contenitore;
                            if (container != null)
                            {
                                ModuloApplicativo moduloApp = new ModuloApplicativo();
                                moduloApp.FillModuloInfoFromImport(baseModuleInfo, SpecificModule, container, netKey.NetworkID, sourceFolderPath);
                                if (moduloApp.FolderId > 0)
                                {
                                    container.Modules.Add(moduloApp);
                                    HomePageBusinessLogic.SaveOrUpdateModulo(moduloApp, session);
                                }
                            }
                        }
                        break;
                    case ModuleType.ModuliCircolari:
                        baseInfos = tableModuliBase.Select("id_Modulo = '" + SpecificModule["Modulo_ModuloCircolare"].ToString() + "'");
                        if (baseInfos.Length > 0)
                        {
                            DataRow baseModuleInfo = baseInfos[0];
                            Contenitore container = contenitoriTable[baseModuleInfo["Contenitore_Modulo"].ToString()] as Contenitore;
                            if (container != null)
                            {
                                ModuloCircolare moduloCrc = new ModuloCircolare();
                                moduloCrc.FillModuloInfoFromImport(baseModuleInfo, SpecificModule, container);
                                container.Modules.Add(moduloCrc);
                                HomePageBusinessLogic.SaveOrUpdateModulo(moduloCrc, session);
                            }
                        }
                        break;
                    case ModuleType.ModuliLogin:
                        baseInfos = tableModuliBase.Select("id_Modulo = '" + SpecificModule["Modulo_ModuloLogin"].ToString() + "'");
                        if (baseInfos.Length > 0)
                        {
                            DataRow baseModuleInfo = baseInfos[0];
                            Contenitore container = contenitoriTable[baseModuleInfo["Contenitore_Modulo"].ToString()] as Contenitore;
                            if (container != null)
                            {
                                ModuloLogin moduloLog = new ModuloLogin();
                                moduloLog.FillModuloInfoFromImport(baseModuleInfo, SpecificModule, container);
                                container.Modules.Add(moduloLog);
                                HomePageBusinessLogic.SaveOrUpdateModulo(moduloLog, session);
                            }
                        }
                        break;
                    case ModuleType.ModuliMenu:
                        baseInfos = tableModuliBase.Select("id_Modulo = '" + SpecificModule["Modulo_ModuloMenu"].ToString() + "'");
                        if (baseInfos.Length > 0)
                        {
                            DataRow baseModuleInfo = baseInfos[0];
                            Contenitore container = contenitoriTable[baseModuleInfo["Contenitore_Modulo"].ToString()] as Contenitore;
                            if (container != null)
                            {
                                ModuloMenu moduloMenu = new ModuloMenu();
                                moduloMenu.FillModuloInfoFromImport(baseModuleInfo, SpecificModule, container);
                                container.Modules.Add(moduloMenu);
                                HomePageBusinessLogic.SaveOrUpdateModulo(moduloMenu, session);
                            }
                        }
                        break;
                    case ModuleType.ModuliRss:
                        baseInfos = tableModuliBase.Select("id_Modulo = '" + SpecificModule["Modulo_ModuloRss"].ToString() + "'");
                        if (baseInfos.Length > 0)
                        {
                            DataRow baseModuleInfo = baseInfos[0];
                            Contenitore container = contenitoriTable[baseModuleInfo["Contenitore_Modulo"].ToString()] as Contenitore;
                            if (container != null)
                            {
                                ModuloRSS moduloRSS = new ModuloRSS();
                                moduloRSS.FillModuloInfoFromImport(baseModuleInfo, SpecificModule, container);
                                container.Modules.Add(moduloRSS);
                                HomePageBusinessLogic.SaveOrUpdateModulo(moduloRSS, session);
                            }
                        }
                        break;
                    case ModuleType.ModuliStatici:
                        baseInfos = tableModuliBase.Select("id_Modulo = '" + SpecificModule["rif_modulo_modulo_statico"].ToString() + "'");
                        if (baseInfos.Length > 0)
                        {
                            DataRow baseModuleInfo = baseInfos[0];
                            Contenitore container = contenitoriTable[baseModuleInfo["Contenitore_Modulo"].ToString()] as Contenitore;
                            if (container != null)
                            {
                                ModuloStatico moduloStatico = new ModuloStatico();
                                moduloStatico.FillModuloInfoFromImport(baseModuleInfo, SpecificModule, container,sourceDB,sourceFolderPath,netKey.NetworkID);
                                container.Modules.Add(moduloStatico);
                                HomePageBusinessLogic.SaveOrUpdateModulo(moduloStatico, session);
                            }
                        }
                        break;
                }
            }
        }

        public override bool EnableIndexContent
        {
            get
            {
                return false;
            }
        }

        public override CmsConfigs.Model.IndexDocContent GetContentToIndex(ISession session)
        {
            return null;
        }
    }
}
