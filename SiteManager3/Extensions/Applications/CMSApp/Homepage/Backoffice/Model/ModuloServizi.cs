﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NetCms.Structure.Applications.Homepage.Model
{
    public class ModuloServizi : Modulo
    {
        public ModuloServizi() : base() { }

        public virtual void FillModuloInfoFromImport(DataRow baseInfo, DataRow moduloInfo, Contenitore container)
        {
            base.FillBaseInfoFromImport(baseInfo, container);


        }
    }
}
