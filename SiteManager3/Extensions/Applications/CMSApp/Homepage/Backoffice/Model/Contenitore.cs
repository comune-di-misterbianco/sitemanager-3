using System.Collections.Generic;
/// <summary>
/// Summary description for homepage
/// </summary>
/// 
namespace NetCms.Structure.Applications.Homepage.Model
{
    public class Contenitore
    {
        public Contenitore()
        {
            ChildsBlocks = new HashSet<Contenitore>();
            Modules = new HashSet<Modulo>();
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Name
        {
            get;
            set;
        }

        public virtual string Tree
        {
            get;
            set;
        }

        public virtual int Depth
        {
            get;
            set;
        }

        public virtual string CssClass
        {
            get;
            set;
        }

        public virtual string WrapperCssClass
        {
            get;
            set;
        }

        //public virtual bool HasModuli
        //{
        //    get;
        //    set;
        //}

        // trasformata in proprietÓ transiente
        public virtual bool HasModuli
        {
            get
            {
                return Modules != null && Modules.Count > 0;
            }
        }
        public virtual bool Eredita
        {
            get;
            set;
        }
        public virtual int Stato
        {
            get;
            set;
        }

        /// <summary>
        /// Vecchia trasposizione che indica il layout inline o tabs
        /// </summary>
        public virtual int Posizione
        {
            get;
            set;
        }

        /// <summary>
        /// Indica la posizione occupata del contenitore a livello di depth 
        /// </summary>
        public virtual int Order
        {
            get;
            set;
        }


        public virtual string Mode
        {
            get;
            set;
        }

        public virtual Contenitore Parent
        {
            get;
            set;
        }

        public virtual HomePageDocContent Homepage
        {
            get;
            set;
        }

        public virtual ICollection<Contenitore> ChildsBlocks
        {
            get;
            set;
        }

        public virtual ICollection<Modulo> Modules
        {
            get;
            set;
        }

        //private HomepageBlockCollection _ChildsBlocks;
        //public HomepageBlockCollection ChildsBlocks
        //{
        //    get
        //    {
        //        if (_ChildsBlocks == null)
        //        {
        //            _ChildsBlocks = new HomepageBlockCollection(this);
        //            DataTable tab = PageData.Conn.SqlQuery("SELECT * FROM Homepages_Contenitori");
        //            DataRow[] Rows = tab.Select("(Stato_Contenitore = 1 OR Depth_Contenitore>1) AND Depth_Contenitore = " + (this.Depth + 1) + " AND Tree_Contenitore LIKE '" + Tree + "%'", "Tree_Contenitore");
        //            foreach (DataRow row in Rows)
        //            {
        //                _ChildsBlocks.Add(new HomepageBlock(row,this.CurrentDocument, PageData));
        //            }
        //        }
        //        return _ChildsBlocks;
        //    }
        //}

        //private HomepageModuleCollection _Modules;
        //public HomepageModuleCollection Modules
        //{
        //    get
        //    {
        //        if (_Modules == null)
        //        {
        //            _Modules = new HomepageModuleCollection(this);
        //            DataTable tab = PageData.Conn.SqlQuery("SELECT * FROM Homepages_Moduli");
        //            DataRow[] Rows = tab.Select("Contenitore_Modulo = " + ID, "Posizione_Modulo");
        //            foreach (DataRow row in Rows)
        //            {
        //                _Modules.Add(HomepageModule.ModuleFactory(row));
        //            }
        //        }
        //        return _Modules;
        //    }
        //}
    }
}