﻿using FluentNHibernate.Mapping;
using NetCms.Structure.Applications.Homepage.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class ModuloStaticoLangMapping : ClassMap<ModuloStaticoLang>
    {
        public ModuloStaticoLangMapping()
        {
            Table("homepages_moduli_statici_lang");
            Id(x => x.ID).Column("id_ModuloStatico_lang");
            Map(x => x.Nome).Column("nome_modulo_statico");
            Map(x => x.Titolo).Column("titolo_modulo_statico");
            Map(x => x.Testo).Column("testo_modulo_statico").CustomSqlType("mediumtext");
            Map(x => x.Link).Column("link_modulo_statico");
            Map(x => x.Title).Column("title_modulo_statico");
            Map(x => x.Img).Column("img_modulo_statico");
            Map(x => x.ImgAlt).Column("imgAlt_modulo_statico");
            Map(x => x.ImgAll).Column("imgAll_modulo_statico");
            Map(x => x.Lang).Column("modulo_statico_lang");

            References(x => x.ModuloParent)
                 .ForeignKey("lang_modulo")
                 .Column("Modulo_ModuloLang")
                 .Fetch.Select();

        }
    }
}
