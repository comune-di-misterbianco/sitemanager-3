﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using NHibernate.SqlTypes;
//using NHibernate.UserTypes;
//using System.Data;


//namespace NetCms.Structure.Applications.Homepage.Mapping
//{
//    [Serializable]
//    public class TemplateTypeMapping : IUserType
//    {
//        new public bool Equals(object x, object y)
//        {
//            return object.Equals(x, y);
//        }

//        public int GetHashCode(object x)
//        {
//            return x.GetHashCode();
//        }

//        public object NullSafeGet(IDataReader rs, string[] names, object owner)
//        {
//            object r = rs[names[0]];
//            var value = (string)r;

//            if (string.IsNullOrEmpty(value))
//                throw new Exception("Invalid Status");

//            switch (value)
//            {
//                case "Custom":
//                    return NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType.Custom;
//                case "Default_Style":
//                    return NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType.Default_Style;
//                case "Focus":
//                    return NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType.Focus;
//                case "Gallery":
//                    return NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType.Gallery;
//                case "Grid":
//                    return NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType.Grid;
//                case "Masonry":
//                    return NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType.Masonry;
//                case "News":
//                    return NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType.News;
//                case "No_Template":
//                    return NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType.No_Template;
//                default:
//                    throw new Exception("Invalid NewsStato Type");

//            }
//        }

//        public void NullSafeSet(IDbCommand cmd, object value, int index)
//        {
//            object paramVal = 0;

//            switch ((NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType)value)
//            {

//                case NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType.Custom: paramVal = "Custom"; break;
//                case NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType.Default_Style: paramVal = "Default_Style"; break;
//                case NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType.Focus: paramVal = "Focus"; break;
//                case NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType.Gallery: paramVal = "Gallery"; break;
//                case NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType.Grid: paramVal = "Grid"; break;
//                case NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType.Masonry: paramVal = "Masonry"; break;
//                case NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType.News: paramVal = "News"; break;
//                case NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType.No_Template: paramVal = "No_Template"; break;
//                default:
//                    throw new Exception("Invalid NewsStato Type");
//            }
//            var parameter = (IDataParameter)cmd.Parameters[index];
//            parameter.Value = paramVal;
//        }

//        public object DeepCopy(object value)
//        {
//            return value;
//        }

//        public object Replace(object original, object target, object owner)
//        {
//            return original;
//        }

//        public object Assemble(object cached, object owner)
//        {
//            return cached;
//        }

//        public object Disassemble(object value)
//        {
//            return value;
//        }

//        public SqlType[] SqlTypes
//        {
//            get { return new SqlType[] { new StringSqlType() }; }
//        }

//        public Type ReturnedType
//        {
//            get { return typeof(NetCms.Structure.Applications.Homepage.Model.ModuloApplicativo.TemplateType); }
//        }

//        public bool IsMutable
//        {
//            get { return false; }
//        }
//    }
//}
