﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class ModuloTagsMapping : SubclassMap<ModuloTags>
    {
        public ModuloTagsMapping()
        {
            Table("homepages_moduli_tags");
            KeyColumn("Modulo_ModuloTags");
            Map(x => x.NetworkID).Column("RifNetwork_ModuloTags");        
            Map(x => x.Records).Column("Records_ModuloTags");        
        }
    }
}
