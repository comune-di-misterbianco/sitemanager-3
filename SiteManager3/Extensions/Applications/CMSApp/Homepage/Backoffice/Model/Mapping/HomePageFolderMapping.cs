﻿using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class HomePageFolderMapping : SubclassMap<HomepageFolder>
    {
        public HomePageFolderMapping()
        {
            Table("homepages_folders");
            KeyColumn("id_folders");
        }
    }
}
