﻿using FluentNHibernate.Mapping;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class ModuloNotificheMapping : SubclassMap<ModuloNotifiche>
    {
        public ModuloNotificheMapping()
        {
            Table("homepages_moduli_notifiche");
            KeyColumn("Modulo_ModuloNotifiche");
            Map(x => x.ShowTitle).Column("ShowTitle_ModuloNotifiche");
        }
    }
}
