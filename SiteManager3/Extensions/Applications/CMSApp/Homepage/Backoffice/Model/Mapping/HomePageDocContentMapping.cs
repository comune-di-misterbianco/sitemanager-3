﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class HomePageDocContentMapping : SubclassMap<HomePageDocContent>
    {
        public HomePageDocContentMapping()
        {
            Table("homepages_docs_contents");
            KeyColumn("id_Homepage");
            Map(x => x.Nome).Column("Nome_homepage");
            Map(x => x.MetaDescription).Column("MetaDescription_homepage").CustomSqlType("mediumtext");
            Map(x => x.Keywords).Column("Keywords_homepage").CustomSqlType("mediumtext");
            Map(x => x.MsgManutenzione).Column("MsgManutenzione_homepage").CustomSqlType("mediumtext");
            Map(x => x.Stato).Column("Stato_homepage");
            HasMany(x => x.Contenitori)
                .KeyColumn("homepage_contenitore")
                .ForeignKeyConstraintName("homepages_contenitori_homepages_docs_contents")
                .Fetch.Select().AsSet()
                .Cascade.All();
        }
    }
}
