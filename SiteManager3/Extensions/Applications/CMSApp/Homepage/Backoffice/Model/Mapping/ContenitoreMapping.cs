﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Structure.Applications.Homepage.Model;
using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.Homepage.Backoffice.Model.Mapping
{
    public class ContenitoreMapping : ClassMap<Contenitore>
    {
        public ContenitoreMapping()
        {
            Table("homepages_contenitori");
            Id(x => x.ID).Column("ID_contenitore");
            Map(x => x.Name).Column("nome_contenitore");
            Map(x => x.Tree).Column("tree_contenitore");
            Map(x => x.Stato).Column("stato_contenitore");
            Map(x => x.Eredita).Column("eredita_contenitore");
            Map(x => x.Posizione).Column("posizione_contenitore");
            Map(x => x.Order).Column("order_contenitore").Default("0");
            Map(x => x.Depth).Column("depth_contenitore");
            Map(x => x.CssClass).Column("cssclass_contenitore");
            Map(x => x.WrapperCssClass).Column("wrappercssclass_contenitore");
            Map(x => x.Mode).Column("mode_contenitore");

            References(x => x.Homepage)
                .ForeignKey("homepages_contenitori_homepages_docs_contents")
                .Column("homepage_contenitore")
                .Fetch.Select();
            References(x => x.Parent)
                .ForeignKey("homepages_contenitori_parent")
                 .Column("parent_contenitore")
                 .Nullable()
                 .Fetch.Select();
            HasMany(x => x.ChildsBlocks)
                .KeyColumn("parent_contenitore")
               .Fetch.Select().AsSet()
               .Cascade.All();
            HasMany(x => x.Modules)
                .KeyColumn("Contenitore_Modulo")
                .ForeignKeyConstraintName("homepages_moduli_homepages_contenitori")
                .Fetch.Select().AsSet()
                .Cascade.All();
        }
    }
}
