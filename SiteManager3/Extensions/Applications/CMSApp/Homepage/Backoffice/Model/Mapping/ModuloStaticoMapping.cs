﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class ModuloStaticoMapping : SubclassMap<ModuloStatico>
    {
        public ModuloStaticoMapping()
        {
            Table("homepages_moduli_statici");
            KeyColumn("rif_modulo_modulo_statico");
            Map(x => x.Nome).Column("nome_modulo_statico");
            Map(x => x.Titolo).Column("titolo_modulo_statico");
            Map(x => x.Testo).Column("testo_modulo_statico").CustomSqlType("mediumtext");
            Map(x => x.Link).Column("link_modulo_statico");
            Map(x => x.Title).Column("title_modulo_statico");
            Map(x => x.Img).Column("img_modulo_statico");
            Map(x => x.ImgAlt).Column("imgAlt_modulo_statico");
            Map(x => x.ImgAll).Column("imgAll_modulo_statico");
            //Map(x => x.CssClass).Column("cssClass_modulo_statico");

            HasMany(x => x.ModuloLangs)
                .KeyColumn("Modulo_ModuloLang")
                .Fetch.Select().AsSet()
                .Cascade.AllDeleteOrphan();
        }
    }
}
