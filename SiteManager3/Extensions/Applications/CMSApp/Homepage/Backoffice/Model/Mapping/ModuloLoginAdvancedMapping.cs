﻿using FluentNHibernate.Mapping;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class ModuloLoginAdvancedMapping : SubclassMap<ModuloLoginAdvanced>
    {
        public ModuloLoginAdvancedMapping()
        {
            Table("homepages_moduli_login_advanced");
            KeyColumn("Modulo_ModuloLoginAdv");
            //Map(x => x.MostraLinkRegistrazione).Column("LinkRegistrazione_ModuloLoginAdv");
            //Map(x => x.MostraRecuperaPassword).Column("RecuperaPassword_ModuloLoginAdv");
            //Map(x => x.MostraSoloControlliAD).Column("ShowOnlyADCtrl_ModuloLoginAdv");
            //Map(x => x.SSOPageUrl).Column("SSOPageUrl_ModuloLoginAdv");
        }
    }
}