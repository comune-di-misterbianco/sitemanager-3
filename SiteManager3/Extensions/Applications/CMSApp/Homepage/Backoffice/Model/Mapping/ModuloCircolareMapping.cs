﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class ModuloCircolareMapping : SubclassMap<ModuloCircolare>
    {
        public ModuloCircolareMapping()
        {
            Table("homepages_moduli_circolari");
            KeyColumn("Modulo_ModuloCircolare");
            References(x => x.ModuloCollegato)
                .Column("ModuloCollegato_ModuloCircolare")
                .Fetch.Select();
        }
    }
}
