﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class ModuloMapping : ClassMap<Modulo>
    {
        public ModuloMapping()
        {
            Table("homepages_moduli");
            Id(x => x.ID).Column("id_Modulo");
            Map(x => x.Posizione).Column("Posizione_Modulo");
            Map(x => x.Tipo).Column("Tipo_Modulo");
            Map(x => x.Stato).Column("Stato_Modulo");
            Map(x => x.CssClass).Column("CssClass_Modulo").Default("");            
            Map(x => x.TemplateMode).Column("TemplateMode_Modulo");
            Map(x => x.ShowOnlyInUrl).Column("ShowOnlyInUrl_Modulo");

            References(x => x.Contenitore)
                .ForeignKey("homepages_moduli_homepages_contenitori")
                .Column("Contenitore_Modulo")
                .Fetch.Select();
            HasMany(x => x.ModuliCircolari)
                .KeyColumn("ModuloCollegato_ModuloCircolare")
                .Fetch.Select().AsSet()
                .Cascade.AllDeleteOrphan();
        }
    }
}
