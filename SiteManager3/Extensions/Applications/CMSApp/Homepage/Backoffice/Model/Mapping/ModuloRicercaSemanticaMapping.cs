﻿using FluentNHibernate.Mapping;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class ModuloRicercaSemanticaMapping : SubclassMap<ModuloRicercaSemantica>
    {
        public ModuloRicercaSemanticaMapping()
        {
            Table("homepages_moduli_ricerca_semantica");
            KeyColumn("Modulo_RicercaSemantica");
            Map(x => x.ShowTitle).Column("ShowTitle_RicercaSemantica");
            Map(x => x.MaxDocument).Column("MaxDocs_RicercaSemantica");
        }
    }
}
