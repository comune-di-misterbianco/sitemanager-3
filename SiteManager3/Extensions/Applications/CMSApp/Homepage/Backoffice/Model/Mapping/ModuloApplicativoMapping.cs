﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class ModuloApplicativoMapping : SubclassMap<ModuloApplicativo>
    {
        public ModuloApplicativoMapping()
        {
            Table("homepages_moduli_applicativi");
            KeyColumn("Modulo_ModuloApplicativo");
            Map(x => x.Subfolders).Column("Subfolders_ModuloApplicativo");
            Map(x => x.Records).Column("Records_ModuloApplicativo");
            Map(x => x.ShowDesc).Column("ShowDesc_ModuloApplicativo");
            Map(x => x.DescLength).Column("DescLength_ModuloApplicativo");
            Map(x => x.Order).Column("Order_ModuloApplicativo");
            Map(x => x.FolderId).Column("Folder_ModuloApplicativo");
            Map(x => x.ShowMoreLink).Column("ShowMoreLink_ModuloApplicativo").Default("0").Not.Nullable();
            Map(x => x.HideImages).Column("HideImage_ModuloApplicativo").Default("0").Not.Nullable();
        }
    }

    //public class ModuloApplicativoNewsMapping : SubclassMap<ModuloApplicativoNews>
    //{
    //    public ModuloApplicativoNewsMapping()
    //    {
    //        Table("homepages_moduli_applicativi_news");
    //        KeyColumn("Modulo_ModuloApplicativoNews");
    //        Map(x => x.ShowCustomNews).Column("ShowCustomNews_ModuloApplicativoNews").Default(((int)(ModuloApplicativoNews.ShowCustom.Tutte)).ToString()).Not.Nullable();
    //    }
    //}
}
