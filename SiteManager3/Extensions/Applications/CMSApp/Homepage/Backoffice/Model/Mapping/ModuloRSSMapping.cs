﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class ModuloRSSMapping : SubclassMap<ModuloRSS>
    {
        public ModuloRSSMapping()
        {
            Table("homepages_moduli_rss");
            KeyColumn("Modulo_ModuloRss");
            Map(x => x.Descrizione).Column("Descrizione_ModuloRss");
            Map(x => x.DescLen).Column("DescLen_ModuloRss");
            Map(x => x.Records).Column("Records_ModuloRss");
            Map(x => x.Link).Column("Link_ModuloRss").CustomSqlType("text");
        }
    }
}
