﻿using FluentNHibernate.Mapping;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class ModuloLoginMapping : SubclassMap<ModuloLogin>
    {
        public ModuloLoginMapping()
        {
            Table("homepages_moduli_login");
            KeyColumn("Modulo_ModuloLogin");
            Map(x => x.MostraLinkRegistrazione).Column("LinkRegistrazione_ModuloLogin");
            Map(x => x.MostraRecuperaPassword).Column("RecuperaPassword_ModuloLogin");
        }
    }
}
