﻿using FluentNHibernate.Mapping;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class ModuloServiziMapping : SubclassMap<ModuloServizi>
    {
        public ModuloServiziMapping()
        {
            Table("homepages_moduli_servizi");
            KeyColumn("Modulo_ModuloServizi");
        }
    }
}
