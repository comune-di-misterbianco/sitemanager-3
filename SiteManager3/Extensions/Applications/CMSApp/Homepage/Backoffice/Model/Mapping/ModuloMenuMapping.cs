﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class ModuloMenuMapping : SubclassMap<ModuloMenu>
    {
        public ModuloMenuMapping()
        {
            Table("homepages_moduli_menu");
            KeyColumn("Modulo_ModuloMenu");
            Map(x => x.Starting).Column("Starting_ModuloMenu");
            Map(x => x.Ending).Column("Ending_ModuloMenu");
            Map(x => x.FirstLevel).Column("FirstLevel_ModuloMenu");
            Map(x => x.Type).Column("Type_ModuloMenu");
        }
    }
}
