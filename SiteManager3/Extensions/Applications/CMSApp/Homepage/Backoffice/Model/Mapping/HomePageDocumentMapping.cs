﻿using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class HomePageDocumentMapping : SubclassMap<HomepageDocument>
    {
        public HomePageDocumentMapping()
        {
            Table("homepages_docs");
            KeyColumn("id_docs");
        }
    }
}
