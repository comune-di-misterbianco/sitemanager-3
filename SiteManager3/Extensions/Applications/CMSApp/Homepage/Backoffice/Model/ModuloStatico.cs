﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml.Linq;
using NetCms.Networks.WebFS;
using NetCms.Structure.WebFS;
using NHibernate;

namespace NetCms.Structure.Applications.Homepage.Model
{
    public class ModuloStatico : Modulo
    {
        public ModuloStatico() : base() { }

        public virtual string Nome
        {
            get;
            set;
        }

        public virtual string Titolo
        {
            get;
            set;
        }

        public virtual string Testo
        {
            get;
            set;
        }

        public virtual string Link
        {
            get;
            set;
        }

        public virtual string Title
        {
            get;
            set;
        }

        public virtual string Img
        {
            get;
            set;
        }

        public virtual string ImgAlt
        {
            get;
            set;
        }

        public virtual string ImgAll
        {
            get;
            set;
        }

        //public virtual string CssClass
        //{
        //    get;
        //    set;
        //}

        public virtual ICollection<ModuloStaticoLang> ModuloLangs
        {
            get;
            set;
        }

        public virtual void FillModuloInfoFromImport(DataRow baseInfo, DataRow moduloInfo, Contenitore container, DataSet sourceDataBase, string SourceFolderPath, int netId)
        {
            base.FillBaseInfoFromImport(baseInfo, container);

            if (moduloInfo.Table.Columns.Contains("nome_modulo_statico"))
                this.Nome = moduloInfo["nome_modulo_statico"].ToString();
            if (moduloInfo.Table.Columns.Contains("titolo_modulo_statico"))
                this.Titolo = moduloInfo["titolo_modulo_statico"].ToString();
            if (moduloInfo.Table.Columns.Contains("testo_modulo_statico"))
            {
                string filteredContent = NetCms.Structure.Utility.ImportContentFilter.ParseImportedContentUrls(moduloInfo["testo_modulo_statico"].ToString());
                this.Testo = filteredContent;
            }
            if (moduloInfo.Table.Columns.Contains("link_modulo_statico"))
            {
                string filteredContent = NetCms.Structure.Utility.ImportContentFilter.ParseImportedCleanUrls(moduloInfo["link_modulo_statico"].ToString());
                this.Link = filteredContent;
            }
            if (moduloInfo.Table.Columns.Contains("title_modulo_statico"))
                this.Title = moduloInfo["title_modulo_statico"].ToString();

            #region ImgModuloStatico

            string oldImgID = moduloInfo["img_modulo_statico"].ToString();
            if (oldImgID.Length > 0)
            {
                var allData = XDocument.Load(SourceFolderPath + "\\" + "data.xml").Descendants("NewDataSet");
                var folders = from fold in allData.Descendants("folders")
                              where fold.Element("Tipo_Folder").Value == "4"
                              select fold;
                string filename = "";
                string path = "";
                foreach (var itemFold in folders)
                {
                    if (sourceDataBase.Tables["docs-" + itemFold.Element("id_Folder").Value] != null)
                    {
                        DataRow[] files = sourceDataBase.Tables["docs-" + itemFold.Element("id_Folder").Value].Select("id_Doc = '" + oldImgID + "'");
                        if (files.Length > 0)
                        {
                            filename = files[0]["Name_Doc"].ToString();
                            path = NetCms.Structure.Utility.ImportContentFilter.ParseImportedCleanUrls(itemFold.Element("Path_Folder").Value).Replace(Utility.ImportContentFilter.WebRoot,"");
                            break;
                        }
                    }
                }
                using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                {
                    IList<Document> docs = NetCms.Structure.WebFS.FileSystemBusinessLogic.DocumentBusinessLogic.GetDocumentByName(filename, netId, path,sess);
                    if (docs.Count > 0)
                    {
                        var orderedDocs = from d in docs
                                          orderby d.Data descending
                                          select d;

                        string ID = orderedDocs.First().ID.ToString();
                        if (ID.Length > 0)
                            this.Img = ID;
                        else
                            this.Img = "";
                    }
                    else
                        this.Img = "";
                }
            }
            else
                this.Img = "";

            #endregion

            if (moduloInfo.Table.Columns.Contains("imgAlt_modulo_statico"))
                this.ImgAlt = moduloInfo["imgAlt_modulo_statico"].ToString();
            if (moduloInfo.Table.Columns.Contains("imgAll_modulo_statico"))
                this.ImgAll = moduloInfo["imgAll_modulo_statico"].ToString();
            //if (moduloInfo.Table.Columns.Contains("cssClass_modulo_statico"))
            //    this.CssClass = moduloInfo["cssClass_modulo_statico"].ToString();
        }
    }
}
