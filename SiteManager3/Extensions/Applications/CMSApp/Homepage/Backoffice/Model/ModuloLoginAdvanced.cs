﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NetCms.Structure.Applications.Homepage.Model
{
    public class ModuloLoginAdvanced : Modulo
    {
        public ModuloLoginAdvanced() : base() { }

        //public virtual bool MostraLinkRegistrazione
        //{
        //    get;
        //    set;
        //}

        //public virtual bool MostraRecuperaPassword
        //{
        //    get;
        //    set;
        //}

        //// aggiungere ulteriori campi per AD

        //public virtual bool MostraSoloControlliAD
        //{
        //    get;
        //    set;
        //}

        //public virtual string SSOPageUrl
        //{
        //    get;
        //    set;
        //}

        public virtual void FillModuloInfoFromImport(DataRow baseInfo, DataRow moduloInfo, Contenitore container)
        {
            base.FillBaseInfoFromImport(baseInfo, container);
        }
    }
}
