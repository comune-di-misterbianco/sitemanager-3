﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NetCms.Structure.Applications.Homepage.Model
{
    public class ModuloLogin : Modulo
    {
        public ModuloLogin() : base() { }

        public virtual bool MostraLinkRegistrazione
        {
            get;
            set;
        }

        public virtual bool MostraRecuperaPassword
        { 
            get; 
            set; 
        }

        public virtual void FillModuloInfoFromImport(DataRow baseInfo, DataRow moduloInfo, Contenitore container)
        {
            base.FillBaseInfoFromImport(baseInfo, container);


        }
    }
}
