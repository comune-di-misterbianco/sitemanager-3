﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NetCms.Structure.Applications.Homepage.Model
{
    public class ModuloRSS : Modulo
    {
        public virtual string Link
        {
            get;
            set;
        }

        public virtual int Descrizione
        {
            get;
            set;
        }

        public virtual int DescLen
        {
            get;
            set;
        }

        public virtual int Records
        {
            get;
            set;
        }

        public ModuloRSS() : base() { }

        public virtual void FillModuloInfoFromImport(DataRow baseInfo, DataRow moduloInfo, Contenitore container)
        {
            base.FillBaseInfoFromImport(baseInfo, container);

            if (moduloInfo.Table.Columns.Contains("Link_ModuloRss"))
            {
                string filteredContent = NetCms.Structure.Utility.ImportContentFilter.ParseImportedCleanUrls(moduloInfo["Link_ModuloRss"].ToString());
                this.Link = filteredContent;
            }
            if (moduloInfo.Table.Columns.Contains("Descrizione_ModuloRss") && moduloInfo["Descrizione_ModuloRss"].ToString().Length > 0)
                this.Descrizione = int.Parse(moduloInfo["Descrizione_ModuloRss"].ToString());
            if (moduloInfo.Table.Columns.Contains("DescLen_ModuloRss") && moduloInfo["DescLen_ModuloRss"].ToString().Length > 0)
                this.DescLen = int.Parse(moduloInfo["DescLen_ModuloRss"].ToString());
            if (moduloInfo.Table.Columns.Contains("Records_ModuloRss") && moduloInfo["Records_ModuloRss"].ToString().Length > 0)
                this.Records = int.Parse(moduloInfo["Records_ModuloRss"].ToString());
        }
    }
}
