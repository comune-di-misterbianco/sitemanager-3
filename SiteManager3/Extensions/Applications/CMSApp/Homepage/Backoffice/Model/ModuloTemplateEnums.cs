﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.Applications.Homepage.Model
{
    public enum TemplateType
    {
        No_Template, // nessun template - il sistema userà il codice html predefinito embeddato
        Standard, // template con codice html basato su bootstrap
        Simple, // lista semplificata di record - con tema agid
        Agid,  // altra modalità per modulo statico
        Box,   // altra modalità per modulo statico
        Focus,   // mostra tre blocchi disposti su altrettante colonne https://italia.github.io/design-web-toolkit/components/detail/layout--focus.html
        Gallery, // mostra 3x3 blocchi  https://italia.github.io/design-web-toolkit/components/detail/layout--gallery.html   
        Grid,    // https://italia.github.io/design-web-toolkit/components/detail/layout--grid.html
        Masonry, // https://italia.github.io/design-web-toolkit/components/detail/layout--masonry.html
        News,    // https://italia.github.io/design-web-toolkit/components/detail/layout--news.html
        Default_Style, // https://italia.github.io/design-web-toolkit/components/detail/layout--default.html
        Carousel, // https://italia.github.io/design-web-toolkit/components/detail/carousel.html
        List,    // template per modulo applicativo con esposizione a list (ul>li) per vecchia modalità
        Custom,
        Hero,
        MiniCards,
    }
}
