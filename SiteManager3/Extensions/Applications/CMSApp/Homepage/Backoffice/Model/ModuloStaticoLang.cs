﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.Applications.Homepage.Model
{
    public class ModuloStaticoLang
    {
        public ModuloStaticoLang() { }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Nome
        {
            get;
            set;
        }

        public virtual string Titolo
        {
            get;
            set;
        }

        public virtual string Testo
        {
            get;
            set;
        }

        public virtual string Link
        {
            get;
            set;
        }

        public virtual string Title
        {
            get;
            set;
        }

        public virtual string Img
        {
            get;
            set;
        }

        public virtual string ImgAlt
        {
            get;
            set;
        }

        public virtual string ImgAll
        {
            get;
            set;
        }

        public virtual ModuloStatico ModuloParent
        {
            get;
            set;
        }

        public virtual int Lang
        {
            get;
            set;
        }
    }
}
