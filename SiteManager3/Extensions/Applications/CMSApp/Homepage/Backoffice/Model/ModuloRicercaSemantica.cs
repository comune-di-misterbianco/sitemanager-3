﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NetCms.Structure.Applications.Homepage.Model
{
    public class ModuloRicercaSemantica : Modulo
    {
        public ModuloRicercaSemantica() : base() { }

        public virtual bool ShowTitle
        {
            get;
            set;
        }

        public virtual int MaxDocument
        {
            get;
            set;
        }

        public virtual void FillModuloInfoFromImport(DataRow baseInfo, DataRow moduloInfo, Contenitore container)
        {
            base.FillBaseInfoFromImport(baseInfo, container);
        }
    }
}
