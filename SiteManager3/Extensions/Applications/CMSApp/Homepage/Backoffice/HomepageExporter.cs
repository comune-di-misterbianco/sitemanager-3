﻿using System;
using System.IO;
using System.Xml;
using System.Web;
using System.Data;
using NetCms.Networks;
using NetCms.Networks.WebFS;
using NetCms.Exporter;
using System.Collections.Generic;

namespace NetCms.Structure.Applications.Homepage
{
    public class HomepageExporter : ApplicationExporter
    {
        private DataTable _DocsTable;
        public override DataTable DocsTable
        {
            get
            {
                if (_DocsTable == null)
                {
                    _DocsTable = new DataTable();
                    this.FolderExporter.Exporter.Conn.FillDataTable(string.Format(ApplicationExporter.Network_Docs, "INNER JOIN homepages_docs_contents ON id_homepage = Content_DocLang", this.FolderID, "",HomepageApplication.ApplicationID.ToString()), _DocsTable);
                    _DocsTable.TableName = "docs";
                }
                return _DocsTable;
            }
        }
        private DataTable[] _Tables;

        public override DataTable[] Tables
        {
            get
            {
                if (_Tables == null) 
                {
                    var homePageList = from row in this.DocsTable.AsEnumerable()
                                       select new
                                        {
                                            id = row["id_homepage"].ToString()
                                        };

                    string homePageIds = "";
                    foreach (var item in homePageList)
                    {
                        if (homePageIds.Length > 0)
                            homePageIds += "," + item.id;
                        else
                            homePageIds += item.id;
                    }

                    _Tables = new DataTable[7];
                    _Tables[0] = new DataTable();
                    this.FolderExporter.Exporter.Conn.FillDataTable("SELECT * FROM homepages_contenitori WHERE homepage_contenitore IN (" + homePageIds + ")", _Tables[0]);
                    _Tables[0].TableName = "homepages_contenitori";

                    var contenitoriList = from row in _Tables[0].AsEnumerable()
                                       select new
                                       {
                                           id = row["ID_contenitore"].ToString()
                                       };

                    string contenitoriIds = "";
                    foreach (var item in contenitoriList)
                    {
                        if (contenitoriIds.Length > 0)
                            contenitoriIds += "," + item.id;
                        else
                            contenitoriIds += item.id;
                    }


                    _Tables[1] = new DataTable();
                    this.FolderExporter.Exporter.Conn.FillDataTable("SELECT * FROM homepages_moduli WHERE Contenitore_Modulo IN (" + contenitoriIds + ")", _Tables[1]);
                    _Tables[1].TableName = "homepages_moduli";

                    var moduliList = from row in _Tables[1].AsEnumerable()
                                          select new
                                          {
                                              id = row["id_Modulo"].ToString()
                                          };

                    string moduliIds = "";
                    foreach (var item in moduliList)
                    {
                        if (moduliIds.Length > 0)
                            moduliIds += "," + item.id;
                        else
                            moduliIds += item.id;
                    }

                    if (!string.IsNullOrEmpty(moduliIds))
                    {
                        _Tables[2] = new DataTable();
                        this.FolderExporter.Exporter.Conn.FillDataTable("SELECT * FROM homepages_moduli_applicativi WHERE Modulo_ModuloApplicativo IN (" + moduliIds + ")", _Tables[2]);
                        _Tables[2].TableName = "homepages_moduli_applicativi";

                        _Tables[3] = new DataTable();
                        this.FolderExporter.Exporter.Conn.FillDataTable("SELECT * FROM homepages_moduli_circolari WHERE Modulo_ModuloCircolare IN (" + moduliIds + ")", _Tables[3]);
                        _Tables[3].TableName = "homepages_moduli_circolari";

                        //_Tables[4] = new DataTable();
                        //this.FolderExporter.Exporter.Connection.FillDataTable("SELECT * FROM homepages_moduli_documenti", _Tables[4]);
                        //_Tables[4].TableName = "homepages_moduli_documenti";

                        _Tables[4] = new DataTable();
                        this.FolderExporter.Exporter.Conn.FillDataTable("SELECT * FROM homepages_moduli_menu WHERE Modulo_ModuloMenu IN (" + moduliIds + ")", _Tables[4]);
                        _Tables[4].TableName = "homepages_moduli_menu";

                        _Tables[5] = new DataTable();
                        this.FolderExporter.Exporter.Conn.FillDataTable("SELECT * FROM homepages_moduli_rss WHERE Modulo_ModuloRss IN (" + moduliIds + ")", _Tables[5]);
                        _Tables[5].TableName = "homepages_moduli_rss";

                        _Tables[6] = new DataTable();
                        this.FolderExporter.Exporter.Conn.FillDataTable("SELECT * FROM homepages_moduli_statici WHERE rif_modulo_modulo_statico IN (" + moduliIds + ")", _Tables[6]);
                        _Tables[6].TableName = "homepages_moduli_statici";
                    }
                }
                return _Tables;
            }
        }

        public override bool HasFiles { get { return false; } }


        public HomepageExporter(string folderID, FolderExporter folderExporter)
            : base(folderID, folderExporter)
        {
        }
    }
}