﻿using GenericDAO.DAO;
using NetCms.DBSessionProvider;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCms.Structure.Applications.Homepage
{
    public static class HomepageModulesBusinessLogic<T>
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static T GetModuloById(int idModulo, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            CriteriaNhibernateDAO<T> dao = new CriteriaNhibernateDAO<T>(innerSession);

            return dao.GetById(idModulo);
        }

        public static T SaveOrUpdateModulo(T modulo, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<T> dao = new CriteriaNhibernateDAO<T>(innerSession);
            return dao.SaveOrUpdate(modulo);
        }
    }
}
