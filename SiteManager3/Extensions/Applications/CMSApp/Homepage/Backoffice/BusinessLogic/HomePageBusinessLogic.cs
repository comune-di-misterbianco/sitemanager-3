﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using NHibernate.Transform;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.DBSessionProvider;
using NetCms.Structure.WebFS;
using NetCms.Diagnostics;

namespace NetCms.Structure.Applications.Homepage
{
    public static class HomePageBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static HomepageDocument GetHomepageDocumentByContainer(int networkId, ICollection<int> tipi, int containerId)
        {
            ICriteria documentCriteria = GetCurrentSession().CreateCriteria(typeof(HomepageDocument)).Add(Restrictions.Eq("Application", 1));

            documentCriteria.CreateCriteria("Folder").Add(Restrictions.Eq("Network.ID", networkId)).Add(Restrictions.In("Tipo",tipi.ToArray()));

            DetachedCriteria homePageContentCriteria = DetachedCriteria.For<HomePageDocContent>().CreateCriteria("Contenitori").Add(Restrictions.Eq("ID",containerId)).SetProjection(Projections.Property("ID"));

            documentCriteria.CreateCriteria("DocLangs").CreateCriteria("Content").Add(Subqueries.PropertyIn("ID", homePageContentCriteria));

            return documentCriteria.UniqueResult() as HomepageDocument;
            
        }

        public static IList<HomePageDocContent> FindAllApplicationRecords()
        {
            IGenericDAO<HomePageDocContent> dao = new CriteriaNhibernateDAO<HomePageDocContent>(GetCurrentSession());
            return dao.FindAll();
        }

        public static ICollection<Modulo> GetModuliHomepageByNetwork(int networkId, ICollection<int> tipi)
        {
            //IGenericDAO<Modulo> dao = new CriteriaNhibernateDAO<Modulo>(GetCurrentSession());

            //SearchParameter tipiFolderSP = new SearchParameter(null,"Tipo",tipi, Finder.ComparisonCriteria.In,false);
            //SearchParameter networkFolderSP = new SearchParameter(null,"Network.ID",networkId, Finder.ComparisonCriteria.Equals,false);
            //SearchParameter folderSP = new SearchParameter(null,"Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[]{tipiFolderSP,networkFolderSP});

            //SearchParameter applicationDocSP = new SearchParameter(null,"Application",1, Finder.ComparisonCriteria.Equals,false);
            //SearchParameter docSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, new SearchParameter[]{applicationDocSP,folderSP});
            
            //SearchParameter docLangSP = new SearchParameter(null,"DocLang", SearchParameter.RelationTypes.OneToMany, new SearchParameter[]{docSP});
            
            //SearchParameter homepageSP = new SearchParameter(null,"Homepage", SearchParameter.RelationTypes.OneToMany, new SearchParameter[]{docLangSP});

            //SearchParameter contenitoreSP = new SearchParameter(null,"Contenitore", SearchParameter.RelationTypes.OneToMany,new SearchParameter[]{homepageSP});

            //SearchParameter tipoModuloSP = new SearchParameter(null,"Tipo",2, Finder.ComparisonCriteria.Not,false);

            //return dao.FindByCriteria(new SearchParameter[]{tipoModuloSP,contenitoreSP});

            ICriteria moduloCriteria = GetCurrentSession().CreateCriteria(typeof(Modulo)).Add(Restrictions.Not(Restrictions.Eq("Tipo", 2)));

            //ICriteria documentCriteria = moduloCriteria.CreateCriteria("Contenitore", NHibernate.SqlCommand.JoinType.InnerJoin).CreateCriteria("Homepage", NHibernate.SqlCommand.JoinType.InnerJoin).CreateCriteria("DocLang", NHibernate.SqlCommand.JoinType.InnerJoin).CreateCriteria("Document","doc", NHibernate.SqlCommand.JoinType.InnerJoin).Add(Restrictions.Eq("doc.Application", 1));

            //documentCriteria.CreateCriteria("doc.Folder", "fold", NHibernate.SqlCommand.JoinType.InnerJoin).Add(Restrictions.Eq("fold.Network.ID", networkId)).Add(Restrictions.In("fold.Tipo", tipi.ToArray()));

            DetachedCriteria docCriteria = DetachedCriteria.For(typeof(Document)).SetProjection(Projections.Property("ID")).Add(Restrictions.Eq("Application", 1));
            docCriteria.CreateCriteria("Folder").Add(Restrictions.Eq("Network.ID", networkId)).Add(Restrictions.In("Tipo", tipi.ToArray()));

            moduloCriteria.CreateCriteria("Contenitore", NHibernate.SqlCommand.JoinType.LeftOuterJoin).CreateCriteria("Homepage", NHibernate.SqlCommand.JoinType.LeftOuterJoin).CreateCriteria("DocLang", NHibernate.SqlCommand.JoinType.LeftOuterJoin).CreateCriteria("Document","doc", NHibernate.SqlCommand.JoinType.LeftOuterJoin);
            moduloCriteria.Add(Subqueries.PropertyIn("doc.ID", docCriteria));

            return moduloCriteria.List<Modulo>();
        }

        public static Contenitore GetContenitoreByHomepage(HomePageDocContent homepage)
        {
            IGenericDAO<Contenitore> dao = new CriteriaNhibernateDAO<Contenitore>(GetCurrentSession());

            SearchParameter depthContenitoreSP = new SearchParameter(null, "Depth", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter homepageSP = new SearchParameter(null, "Homepage", homepage, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { depthContenitoreSP, homepageSP });
        }

        public static HomePageDocContent GetHomepageContentById(int id)
        {
            IGenericDAO<HomePageDocContent> dao = new CriteriaNhibernateDAO<HomePageDocContent>(GetCurrentSession());
            return dao.GetById(id);
        }

        public static Modulo GetModuloById(int idModulo, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Modulo> dao = new CriteriaNhibernateDAO<Modulo>(innerSession);

            return dao.GetById(idModulo);
        }

        public static ModuloStatico GetModuloStaticoById(int idModulo, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ModuloStatico> dao = new CriteriaNhibernateDAO<ModuloStatico>(innerSession);

            return dao.GetById(idModulo);
        }

        public static ModuloRSS GetModuloRSSById(int idModulo, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ModuloRSS> dao = new CriteriaNhibernateDAO<ModuloRSS>(innerSession);

            return dao.GetById(idModulo);
        }

        public static ModuloLogin GetModuloLoginById(int idModulo, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ModuloLogin> dao = new CriteriaNhibernateDAO<ModuloLogin>(innerSession);

            return dao.GetById(idModulo);
        }

        public static ModuloLoginAdvanced GetModuloLoginAdvById(int idModulo, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ModuloLoginAdvanced> dao = new CriteriaNhibernateDAO<ModuloLoginAdvanced>(innerSession);

            return dao.GetById(idModulo);
        }

        public static ModuloRicercaSemantica GetModuloRicercaSemaById(int idModulo, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ModuloRicercaSemantica> dao = new CriteriaNhibernateDAO<ModuloRicercaSemantica>(innerSession);

            return dao.GetById(idModulo);
        }

        public static ModuloServizi GetModuloServiziById(int idModulo, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ModuloServizi> dao = new CriteriaNhibernateDAO<ModuloServizi>(innerSession);

            return dao.GetById(idModulo);
        }

        public static ModuloNotifiche GetModuloNotificheById(int idModulo, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ModuloNotifiche> dao = new CriteriaNhibernateDAO<ModuloNotifiche>(innerSession);

            return dao.GetById(idModulo);
        }

        public static ModuloMenu GetModuloMenuById(int idModulo, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ModuloMenu> dao = new CriteriaNhibernateDAO<ModuloMenu>(innerSession);

            return dao.GetById(idModulo);
        }

        public static ModuloCircolare GetModuloCircolareById(int idModulo, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ModuloCircolare> dao = new CriteriaNhibernateDAO<ModuloCircolare>(innerSession);

            return dao.GetById(idModulo);
        }

        public static Contenitore GetContenitoreById(int idContenitore, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Contenitore> dao = new CriteriaNhibernateDAO<Contenitore>(innerSession);

            return dao.GetById(idContenitore);
        }

        public static Modulo SaveOrUpdateModulo(Modulo modulo, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Modulo> dao = new CriteriaNhibernateDAO<Modulo>(innerSession);
            return dao.SaveOrUpdate(modulo);
        }

        public static Contenitore SaveOrUpdateContenitore(Contenitore contenitore, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Contenitore> dao = new CriteriaNhibernateDAO<Contenitore>(innerSession);
            return dao.SaveOrUpdate(contenitore);
        }

        public static void SaveRowsOrder(Dictionary<string,string> rowsorder, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Contenitore> dao = new CriteriaNhibernateDAO<Contenitore>(innerSession);

            foreach (KeyValuePair<string, string> roworder in rowsorder)
            {
                var contID = int.Parse(roworder.Key.ToString());
                var order = int.Parse(roworder.Value);
                Contenitore contenitore = GetContenitoreById(contID,innerSession);
                contenitore.Order = order;
                dao.SaveOrUpdate(contenitore);
            }
        }

        public static HomePageDocContent SaveOrUpdateHomepage(HomePageDocContent homepage, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<HomePageDocContent> dao = new CriteriaNhibernateDAO<HomePageDocContent>(innerSession);
            return dao.SaveOrUpdate(homepage);
        }

        public enum SearchFor
        {
            ID,
            Name
        }

        public static Contenitore FindBlock(Contenitore startBlock, string key, SearchFor searchFor)
        {
            Contenitore foundNode = null;

            switch (searchFor)
            {
                case SearchFor.ID:
                    {
                        foundNode = FindBlockByID(startBlock, key);
                        break;
                    }

                case SearchFor.Name:
                    {
                        foundNode = FindBlockByName(startBlock, key);
                        break;
                    }
            }

            if (foundNode != null)
                return foundNode;

            IList<Contenitore> list = GetCurrentSession().CreateCriteria(typeof(Contenitore)).Add(Expression.Eq("Parent", startBlock)).SetFetchMode("ChildsBlocks", FetchMode.Join)
                .SetResultTransformer(new DistinctRootEntityResultTransformer())
                .List<Contenitore>();

            return TraverseBlockTree(list, key, searchFor);
        }


        private static Contenitore TraverseBlockTree(IList<Contenitore> list, string key, SearchFor searchFor)
        {
            Contenitore foundNode = null;

            foreach (Contenitore contenitore in list)
            {
                switch (searchFor)
                {
                    case SearchFor.ID:
                        {
                            foundNode = FindBlockByID(contenitore, key);
                            break;
                        }

                    case SearchFor.Name:
                        {
                            foundNode = FindBlockByName(contenitore, key);
                            break;
                        }
                }

                if (foundNode != null)
                    return foundNode;

                Contenitore result = null;
                if (contenitore.ChildsBlocks.Count > 0)
                    result = TraverseBlockTree(contenitore.ChildsBlocks.Cast<Contenitore>().ToList(), key, searchFor);

                if (result != null)
                    return result;
            }
            return null;
        }

        private static Contenitore FindBlockByID(Contenitore contenitore, string key)
        {
            if (contenitore.ID == int.Parse(key)) return contenitore;
            return null;
        }

        private static Contenitore FindBlockByName(Contenitore contenitore, string key)
        {
            if (contenitore.Name == key) return contenitore;
            return null;
        }

        public static ICollection<Contenitore> FindContenitoriColcxByHomepage(HomePageDocContent homepage)
        {
            IGenericDAO<Contenitore> dao = new CriteriaNhibernateDAO<Contenitore>(GetCurrentSession());

            SearchParameter depthSP = new SearchParameter(null, "Depth", 1, Finder.ComparisonCriteria.Equals, false);
            SearchParameter nomeSP = new SearchParameter(null, "Name", "Colcx", Finder.ComparisonCriteria.Equals, false);
            SearchParameter homepageSP = new SearchParameter(null, "Homepage", homepage, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria("Tree", true, new SearchParameter[] { depthSP, nomeSP, homepageSP });
        }

        public static ModuloCircolare GetModuloCircolareByModuloCollegato(Modulo moduloCollegato)
        {
            IGenericDAO<ModuloCircolare> dao = new CriteriaNhibernateDAO<ModuloCircolare>(GetCurrentSession());

            SearchParameter moduloAllegatoSP = new SearchParameter(null, "ModuloCollegato", moduloCollegato, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { moduloAllegatoSP });
        }

        public static bool DeleteModulo(Modulo modulo)
        {
            try
            {
                IGenericDAO<Modulo> dao = new CriteriaNhibernateDAO<Modulo>(GetCurrentSession());
                dao.Delete(modulo);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool DeleteContenitore(Contenitore contenitore)
        {
            try
            {
                IGenericDAO<Contenitore> dao = new CriteriaNhibernateDAO<Contenitore>(GetCurrentSession());
                dao.Delete(contenitore);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static ICollection<Contenitore> FindContenitoriWithDepthMoreThan1ByTreeStart(string treeStart)
        {
            IGenericDAO<Contenitore> dao = new CriteriaNhibernateDAO<Contenitore>(GetCurrentSession());

            SearchParameter depthSP = new SearchParameter(null, "Depth", 1, Finder.ComparisonCriteria.GreaterThan, false);
            SearchParameter treeStartSP = new SearchParameter(null, "Tree", treeStart, Finder.ComparisonCriteria.LikeStart, false);

            return dao.FindByCriteria(new SearchParameter[] { depthSP, treeStartSP });
        }

        public static ICollection<Modulo> FindModuliByContenitoreOrderByPositionDesc(int idContenitore)
        {
            IGenericDAO<Modulo> dao = new CriteriaNhibernateDAO<Modulo>(GetCurrentSession());

            SearchParameter contenitoreSP = new SearchParameter(null, "Contenitore.ID", idContenitore, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria("Posizione", true, new SearchParameter[] { contenitoreSP });
            
        }
        public static ICollection<Modulo> FindModuliByContenitore(int idContenitore)
        {
            IGenericDAO<Modulo> dao = new CriteriaNhibernateDAO<Modulo>(GetCurrentSession());

            SearchParameter contenitoreSP = new SearchParameter(null, "Contenitore.ID", idContenitore, Finder.ComparisonCriteria.Equals, false);

            var results = dao.FindByCriteria(new SearchParameter[] { contenitoreSP });

            return results;
        }


        public static Contenitore GetContenitoreByHomePageAndName(int idHomepage, string nome)
        {
            IGenericDAO<Contenitore> dao = new CriteriaNhibernateDAO<Contenitore>(GetCurrentSession());

            SearchParameter nomeSP = new SearchParameter(null, "Name", nome, Finder.ComparisonCriteria.Equals, false);
            SearchParameter idHomepageSP = new SearchParameter(null, "Homepage.ID", idHomepage, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { nomeSP, idHomepageSP });
        }

        public static ICollection<Contenitore> FindContenitoryByHomepageAndNameDifferentId(int idContainer, int idHomepage, string nome)
        {
            IGenericDAO<Contenitore> dao = new CriteriaNhibernateDAO<Contenitore>(GetCurrentSession());

            SearchParameter nomeSP = new SearchParameter(null, "Name", nome, Finder.ComparisonCriteria.Like, false);
            SearchParameter idContainerSP = new SearchParameter(null, "ID", idContainer, Finder.ComparisonCriteria.Not, false);
            SearchParameter homepageSP = new SearchParameter(null,"Homepage.ID",idHomepage, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[]{nomeSP,idContainerSP,homepageSP});
        }

        public static ICollection<Contenitore> FindContenitoriByHomePageAndStato(int idHomepage, int stato)
        {
            IGenericDAO<Contenitore> dao = new CriteriaNhibernateDAO<Contenitore>(GetCurrentSession());

            SearchParameter statoSP = new SearchParameter(null, "Stato", stato, Finder.ComparisonCriteria.Equals, false);
            SearchParameter idHomepageSP = new SearchParameter(null, "Homepage.ID", idHomepage, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[] { statoSP, idHomepageSP });
        }

        public static ICollection<Modulo> FindModuliByHomepageOrderByPosition(int idHomepage)
        {
            IGenericDAO<Modulo> dao = new CriteriaNhibernateDAO<Modulo>(GetCurrentSession());

            SearchParameter homepageID = new SearchParameter(null,"ID",idHomepage, Finder.ComparisonCriteria.Equals,false);
            SearchParameter homepageSP = new SearchParameter(null, "Homepage", SearchParameter.RelationTypes.OneToMany, homepageID);
            SearchParameter homepageContenitoreSP = new SearchParameter(null, "Contenitore", SearchParameter.RelationTypes.OneToMany,homepageSP);

            return dao.FindByCriteria("Posizione", false, new SearchParameter[] { homepageContenitoreSP });

        }

        public static ICollection<Contenitore> FindContenitoriByHomepage(int idHomepage,int depth)
        {
            IGenericDAO<Contenitore> dao = new CriteriaNhibernateDAO<Contenitore>(GetCurrentSession());

            SearchParameter depthSP = new SearchParameter(null, "Depth", 1, Finder.ComparisonCriteria.Equals, false);
            SearchParameter homepageSP = new SearchParameter(null, "Homepage.ID", idHomepage, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[] { depthSP, homepageSP });
        }

        public static bool DeleteModuliApplicativiByFolder(int idFolder, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();
            try
            {
                IGenericDAO<ModuloApplicativo> dao = new CriteriaNhibernateDAO<ModuloApplicativo>(innerSession);
                SearchParameter folderSP = new SearchParameter(null, "FolderId", idFolder, Finder.ComparisonCriteria.Equals, false);

                IList<ModuloApplicativo> moduli = dao.FindByCriteria(new SearchParameter[] { folderSP });
                foreach (ModuloApplicativo modulo in moduli)
                {
                    modulo.Contenitore.Modules.Remove(modulo);
                    modulo.ModuliCircolari.Clear();
                    dao.Delete(modulo);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool CambiaEredita(int idContainer)
        {
            bool result = false;
            using(ISession session = FluentSessionProvider.Instance.OpenInnerSession())
            using(ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    Contenitore container = GetContenitoreById(idContainer, session);
                    if (container.Eredita)
                        container.Eredita = false;
                    else
                        container.Eredita = true;

                    IGenericDAO<Contenitore> dao = new CriteriaNhibernateDAO<Contenitore>(session);

                    dao.SaveOrUpdate(container);
                    tx.Commit();
                    result = true;

                }
                catch(Exception ex)
                {
                    result = false;
                    Diagnostics.Diagnostics.TraceMessage(TraceLevel.Error, ex.ToString());


                }
            }
            return result;

        
        }

        public static List<HomePageDocContent> GetAllHomePageDocContent(ISession innerSession = null)
        {

            if (innerSession == null)
                innerSession = GetCurrentSession();

            List<HomePageDocContent> homepages = null;
            
            try
            {
                IGenericDAO<HomePageDocContent> dao = new CriteriaNhibernateDAO<HomePageDocContent>(innerSession);
                SearchParameter spStato = new SearchParameter("", "Stato", 1, Finder.ComparisonCriteria.Equals, false);
                //var results = dao.FindAll();
                var results = dao.FindByCriteria(new SearchParameter[] { spStato }, true);
                if (results != null)
                    homepages = results.ToList();

            }
            catch(Exception ex)
            {
                Diagnostics.Diagnostics.TraceMessage(TraceLevel.Error, ex.ToString());
            }

            return homepages;            
        }

        public static IList<HomePageDocContent> GetAllHomePageByModuloTipo(int tipo, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IList<HomePageDocContent> homepages = null;
            try
            {
                IGenericDAO<HomePageDocContent> dao = new CriteriaNhibernateDAO<HomePageDocContent>(innerSession);
                               
                SearchParameter homepageModuloTipoSP = new SearchParameter(null, "Tipo", tipo, Finder.ComparisonCriteria.Equals, false);
                SearchParameter homepageModuliSP = new SearchParameter(null, "Modules", SearchParameter.RelationTypes.OneToMany, homepageModuloTipoSP);
                SearchParameter homepageContenitoreSP = new SearchParameter(null, "Contenitori", SearchParameter.RelationTypes.OneToMany, homepageModuliSP);
                

                homepages = dao.FindByCriteria(new SearchParameter[] { homepageContenitoreSP });

            }
            catch(Exception ex)
            {
                Diagnostics.Diagnostics.TraceMessage(TraceLevel.Error, ex.ToString());
            }

            return homepages;
        }
    }
}
