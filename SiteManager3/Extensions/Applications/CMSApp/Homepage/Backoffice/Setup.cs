﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using NetCms;
using System.IO;
using NetCms.GUI;
using NetCms.Structure;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Reflection;
using NetCms.Structure.WebFS;
using NetCms.Networks;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.DBSessionProvider;
using NHibernate;
using System.Linq;

namespace NetCms.Structure.Applications.Homepage
{
    public class Setup : NetCms.Structure.Applications.Setup
    {

        public const bool Debug = true;
        public Setup(NetCms.Structure.Applications.Application baseApp)
            : base(baseApp)
        {
        }

        public override string[,] Pages
        {
            get
            {
                string[,] pages = {
                                  { "default.aspx", "NetCms.Structure.Applications.Homepage.Pages.HomepageManager" },
                                  { "gest_contenitore.aspx", "NetCms.Structure.Applications.Homepage.Pages.EditContainer" },
                                  { "gest_modulo.aspx", "NetCms.Structure.Applications.Homepage.Pages.EditModule" },
                                  { "ins_modulo.aspx", "NetCms.Structure.Applications.Homepage.Pages.AddModule" },
                                  { "modify.aspx", "NetCms.Structure.Applications.Homepage.Pages.HomepageManager" },
                                  { "prop_homepage.aspx", "NetCms.Structure.Applications.Homepage.Pages.EditHomepage" },
                                  { "lang_modulo.aspx", "NetCms.Structure.Applications.Homepage.Pages.TranslateModule" }
                                  };
                return pages;
            }
        }

        protected override bool LocalInstall(bool isInsidePackage = false)
        {
            bool done = true;
            
            FluentSessionProvider.Instance.UpdateSessionFactoryAdd(Assembly.GetAssembly(this.GetType()));

            //if (done) done = AddTables();
            if (done) done = AddRecords();
            //if (done) done = AddPermission();
            if (!done) this.RollBack(); ;

            return done;
        }
        
        /*private bool AddDocument()
        {
            bool done = false;
            try
            {
                foreach (DataRow row in this.NetworksTable.Select(FilterID))
                {
                    NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork("internet");


                    done = true;
                }
            }
            catch (Exception ex)
            {
                //Trace
                this.AddErrorString("Non è stato possibile creare uno o più record dell'Applicazione.");
                NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);
            }

            return done;
        } */
       			
        private bool AddRecords()
        {
            bool done = false;
            ISession session = FluentSessionProvider.Instance.OpenInnerSession();
            ITransaction tx = session.BeginTransaction();
            try
            {

                //                string sqlNewHomeModel = "INSERT INTO {0}_homepages (Nome_Homepage,MetaDescription_homepage,Keywords_homepage,MsgManutenzione_homepage,Stato_homepage) VALUES('Homepage {0}','','','Sezione attualmente in fase di manutenzione. Riprovare in seguito.',1)";
                //                string sqlNewContainerModel = @"INSERT INTO {5}_homepages_contenitori
                //                                                (
                //                                                    nome_contenitore,
                //                                                    parent_contenitore,
                //                                                    stato_contenitore,
                //                                                    havemoduli_contenitore,
                //                                                    homepage_contenitore,
                //                                                    posizione_contenitore,
                //                                                    depth_contenitore,
                //                                                    cssclass_contenitore
                //                                                ) 
                //                                                VALUES(
                //                                                    '{0}',
                //                                                    {1},
                //                                                    1,
                //                                                    {2},
                //                                                    {3},
                //                                                    0,
                //                                                    {4},
                //                                                    ''
                //                                                ) 
                //                ";

                //DA TESTARE
                //bool filter = false;
                //var networks = this.NetworksTable.Where(x => !filter || (filter && x.ID == NetworkID));
                foreach (BasicStructureNetwork network in this.NetworksTable.Where(x=> !FilterID || (FilterID && x.ID == NetworkID)))
                {
                    //NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);

                    //Creo la cartella dell'homepage
                    #region Sql Inserimento Cartella
                    //                    string sqlFolder = @"INSERT INTO  " + network.SystemName + @"_folders (
                    //                                                            Parent_Folder,                                                            
                    //                                                            Path_Folder,
                    //                                                            Tipo_Folder,
                    //                                                            Stato_Folder,
                    //                                                            Name_Folder,
                    //                                                            Depth_Folder,
                    //                                                            Trash_Folder,
                    //                                                            Restricted_Folder,
                    //                                                            Network_Folder,
                    //                                                            HomeType_Folder,
                    //                                                            System_Folder,
                    //                                                            Disable_Folder,
                    //                                                            TitleOffset_Folder,
                    //                                                            Review_Folder,
                    //                                                            Order_Folder,
                    //                                                            Rss_Folder,
                    //                                                            Layout_Folder
                    //                                                         )
                    //                                                            VALUES (
                    //                                                            0,
                    //                                                            '',                                                            
                    //                                                            1,
                    //                                                            0,
                    //                                                            'root',
                    //                                                            1,
                    //                                                            0,
                    //                                                            0,
                    //                                                            " + network.ID + @",
                    //                                                            0,
                    //                                                            0,
                    //                                                            0,
                    //                                                            0,
                    //                                                            0,
                    //                                                            0,
                    //                                                            1,
                    //                                                            0
                    //                                                          )";

                    //                    int folderID = Connection.ExecuteInsert(sqlFolder);

                    //string tree_Folder = G2Core.Common.Utility.FormatNumber(folderID.ToString());

                    //string sql = "UPDATE " + network.SystemName + "_folders SET Tree_Folder = '." + tree_Folder + ".' WHERE id_Folder = " + folderID;
                    //this.Connection.Execute(sql);

                    NetworkKey nKey = new Networks.NetworkKey(network.ID);

                    HomepageFolder folder = new HomepageFolder(nKey);

                    folder.Tipo = 1;
                    folder.Nome = "root";
                    folder.Network = NetCms.Networks.NetworksManager.GlobalIndexer.GetNetworkByID(network.ID);

                    folder = FolderBusinessLogic.SaveOrUpdateFolder(folder, session) as HomepageFolder;

                    string tree_Folder = "." + G2Core.Common.Utility.FormatNumber(folder.ID.ToString()) + ".";
                    folder.Tree = tree_Folder;
                    folder.Parent = null;
                    folder.Path = "";
                    folder.Stato = 0;
                    folder.Depth = 1;
                    folder.Trash = 0;
                    folder.RestrictedInt = 0;
                    folder.HomeType = 0;
                    folder.Sistema = 0;
                    folder.Disable = 0;
                    folder.TitleOffset = 0;
                    folder.Order = 0;
                    folder.Rss = 1;
                    folder.Layout = 0;
                    //folder = FolderBusinessLogic.SaveOrUpdateFolder(folder) as HomepageFolder;

                    #endregion

                    #region Sql Inserimento Lingua Cartella
                    //                    string sqlFolderLang = @"INSERT INTO  " + network.SystemName + @"_folderslang (
                    //                                                            Lang_FolderLang,
                    //                                                            Folder_FolderLang,
                    //                                                            Label_FolderLang,
                    //                                                            Desc_Folderlang
                    //                                                         )
                    //                                                            VALUES (
                    //                                                            " + network.DefaultLang + @",
                    //                                                            " + folderID + @",
                    //                                                            'Radice sito " + network.Label + @"',
                    //                                                            'Radice'
                    //                                                         )";

                    //                    int folderLangID = Connection.ExecuteInsert(sqlFolderLang);

                    FolderLang folderLang = new FolderLang();
                    folderLang.Lang = network.DefaultLang;
                    folderLang.Label = "Radice sito " + network.Label;
                    folderLang.Descrizione = "Radice";
                    folderLang.Folder = folder;
                    folder.FolderLangs.Add(folderLang);

                    //FolderBusinessLogic.SaveOrUpdateFolderLang(folderLang);
                    folder = FolderBusinessLogic.SaveOrUpdateFolder(folder, session) as HomepageFolder;

                    #endregion

                    if (CreateRecord)
                    {
                        #region Creazione cartella imghome

                        string FileSystemName = "imghome";

                        if (this.CreatePhysicalFolder("\\" + FileSystemName, network))
                        {

                            #region Creo la folder

                            //sql = "INSERT INTO Folders";
                            //sql += " (Name_Folder) Values ('" + FileSystemName + "') ";

                            //int NewFolderID = network.Connection.ExecuteInsert(sql);

                            object[] parameters = { nKey };
                            Application application = ApplicationsPool.ActiveAssemblies["4"];
                            Type ApplicationFolderType = application.Assembly.GetType(application.Classes.Folder);
                            StructureFolder physicalFolder = (StructureFolder)System.Activator.CreateInstance(ApplicationFolderType, parameters);

                            //HomepageFolder physicalFolder = new HomepageFolder(nKey);

                            physicalFolder.Nome = FileSystemName;
                            physicalFolder.Tipo = 4;
                            physicalFolder.Network = NetCms.Networks.NetworksManager.GlobalIndexer.GetNetworkByID(network.ID);

                            physicalFolder = FolderBusinessLogic.SaveOrUpdateFolder(physicalFolder, session);

                            #endregion

                            #region Aggiorno i dati della folder aggiungendo i dati del Tree e del path

                            //sql = "UPDATE Folders SET ";
                            //sql += " Tree_Folder = '." + NetUtility.TreeUtility.FormatNumber(folderID.ToString()) + "." + NetUtility.TreeUtility.FormatNumber(NewFolderID.ToString()) + ".' ";
                            //sql += ", Parent_Folder = " + folderID + " ";
                            //sql += ", Depth_Folder = 2 ";
                            //sql += ", Stato_Folder =  0 ";
                            //sql += ", CssClass_Folder = '' ";
                            //sql += ", Tipo_Folder = 4 ";
                            //sql += ", TitleOffset_Folder = 0 ";
                            //sql += ", Path_Folder = '/" + FileSystemName + "' ";
                            //sql += ", System_Folder = 0 ";
                            //sql += ", Network_Folder = " + network.ID + " ";
                            //sql += ", HomeType_Folder = 0 ";
                            //sql += ", Restricted_Folder = 0 ";
                            //sql += "WHERE id_Folder = " + NewFolderID + " ";

                            //network.Connection.Open();
                            //network.Connection.Execute(sql);
                            //network.Connection.Close();

                            physicalFolder.Tree = "." + NetUtility.TreeUtility.FormatNumber(folder.ID.ToString()) + "." + NetUtility.TreeUtility.FormatNumber(physicalFolder.ID.ToString()) + ".";
                            physicalFolder.Depth = 2;
                            physicalFolder.Stato = 1;
                            physicalFolder.CssClass = "";
                            physicalFolder.TitleOffset = 0;
                            physicalFolder.Path = "/" + FileSystemName;
                            physicalFolder.Sistema = 0;
                            physicalFolder.HomeType = 0;
                            physicalFolder.RestrictedInt = 0;
                            physicalFolder.Parent = folder;
                            //folder.Folders.Add(physicalFolder);
                            //physicalFolder = FolderBusinessLogic.SaveOrUpdateFolder(physicalFolder);

                            #endregion

                            #region Creo la Lingua

                            //sql = "INSERT INTO FoldersLang";
                            //sql += " (Label_FolderLang,Lang_FolderLang,Desc_FolderLang,Folder_FolderLang)";
                            //sql += " Values";
                            //sql += " (";
                            //sql += " 'ImgHome'";//Nome
                            //sql += ", " + network.DefaultLang + "";//Lingua
                            //sql += ", 'Cartella immagini homepage'";//Descrizione
                            //sql += ", " + NewFolderID + "";//Folder
                            //sql += " )";

                            //network.Connection.Open();
                            //network.Connection.Execute(sql);
                            //network.Connection.Close();

                            FolderLang physicalFolderLang = new FolderLang();
                            physicalFolderLang.Label = "ImgHome";
                            physicalFolderLang.Lang = network.DefaultLang;
                            physicalFolderLang.Descrizione = "Cartella immagini homepage";

                            physicalFolderLang.Folder = physicalFolder;
                            physicalFolder.FolderLangs.Add(physicalFolderLang);

                            //FolderBusinessLogic.SaveOrUpdateFolderLang(physicalFolderLang);
                            physicalFolder = FolderBusinessLogic.SaveOrUpdateFolder(physicalFolder, session);

                            //folder = FolderBusinessLogic.SaveOrUpdateFolder(folder) as HomepageFolder;

                            #endregion

                        }


                        ////DataTable tab = this.Network.Connection.SqlQuery("SELECT * FROM Folders INNER JOIN FoldersLang ON id_Folder = Folder_FolderLang WHERE id_Folder = " + NewFolderID);
                        ////if (tab.Rows.Count > 0)
                        ////{
                        ////    StructureFolder folder = StructureFolder.Fabricate(tab.Rows[0], this.NetworkKey);
                        ////    if (CreateDefaultPage) folder.CreateDefaultPage();
                        ////}

                        #endregion



                        //Creo il contenuto della homepage
                        //sql = string.Format(sqlNewHomeModel, network.SystemName);
                        //int HomepageID = this.Connection.ExecuteInsert(sql);

                        HomePageDocContent homepage = new HomePageDocContent();
                        homepage.Nome = "Homepage " + network.SystemName;
                        homepage.MetaDescription = "";
                        homepage.Keywords = "";
                        homepage.MsgManutenzione = "Sezione attualmente in fase di manutenzione. Riprovare in seguito.";
                        homepage.Stato = 1;
                        //homepage = HomePageBusinessLogic.SaveOrUpdateHomepage(homepage);
                        homepage = HomePageBusinessLogic.SaveOrUpdateHomepage(homepage, session);
                        #region Sql Inserimento Documento
                        //                        string sqlDoc = @"INSERT INTO  " + network.SystemName + @"_docs  (
                        //                                                            Name_Doc,
                        //                                                            Stato_Doc,
                        //                                                            Data_Doc,
                        //                                                            Modify_Doc,
                        //                                                            Folder_Doc,
                        //                                                            Descrizione_Doc,
                        //                                                            Application_Doc,
                        //                                                            Autore_Doc,
                        //                                                            Trash_Doc,
                        //                                                            Create_Doc,
                        //                                                            Hide_Doc,
                        //                                                            TitleOffset_Doc,
                        //                                                            CssClass_Doc,
                        //                                                            DefaultLang_Doc,
                        //                                                            LangPickMode_Doc,
                        //                                                            Order_Doc,
                        //                                                            Layout_Doc
                        //                                                         ) 
                        //                                                            VALUES (
                        //                                                            'default.aspx',
                        //                                                            1,
                        //                                                            " + Connection.FilterDate(DateTime.Now) + @",
                        //                                                            " + Connection.FilterDate(DateTime.Now) + @",
                        //                                                            " + folderID + @",
                        //                                                            'Homepage',
                        //                                                            " + HomepageApplication.ApplicationID + @",
                        //                                                            1,
                        //                                                            0,
                        //                                                            " + Connection.FilterDate(DateTime.Now) + @",
                        //                                                            0,
                        //                                                            0,
                        //                                                            '',
                        //                                                            " + network.DefaultLang + @",
                        //                                                            0,
                        //                                                            0,
                        //                                                            0
                        //                                                         )";

                        //                        int docID = Connection.ExecuteInsert(sqlDoc);

                        NetCms.Structure.WebFS.Document doc = new HomepageDocument(nKey);
                        doc.PhysicalName = "default.aspx";
                        doc.Stato = 1;
                        doc.Data = DateTime.Now;
                        doc.Modify = DateTime.Now;
                        doc.Application = HomepageApplication.ApplicationID;
                        doc.Autore = 1;
                        doc.Trash = 0;
                        doc.Create = DateTime.Now;
                        doc.Hide = 0;
                        doc.TitleOffset = 0;
                        doc.CssClass = "";
                        doc.LangPickMode = 0;
                        doc.Order = 0;
                        doc.Layout = 0;
                        doc.Folder = folder;

                        folder.Documents.Add(doc);

                        //folder = FolderBusinessLogic.SaveOrUpdateFolder(folder) as HomepageFolder;

                        #endregion

                        #region Sql Inserimento Lingua Documento

                        //                        string sqlDocLang = @"INSERT INTO  " + network.SystemName + @"_docs_lang  (
                        //                                                            Doc_DocLang,
                        //                                                            Label_DocLang,
                        //                                                            Desc_DocLang,
                        //                                                            Lang_DocLang,
                        //                                                            Content_DocLang
                        //                                                         ) 
                        //                                                            VALUES (
                        //                                                            " + docID + @",
                        //                                                            'Homepage',
                        //                                                            'HomePage del Portale " + network.Label + @"',
                        //                                                            " + network.DefaultLang + @",
                        //                                                            " + HomepageID + @"
                        //                                                         )";

                        //                        this.Connection.Execute(sqlDocLang);

                        DocumentLang doclang = new DocumentLang();
                        doclang.Label = "Homepage";
                        doclang.Descrizione = "HomePage del Portale " + network.Label;
                        doclang.Lang = network.DefaultLang;
                        doclang.Content = homepage;
                        doclang.Document = doc;

                        homepage.DocLang = doclang;
                        doc.DocLangs.Add(doclang);

                        DocumentBusinessLogic.SaveOrUpdateDocument(doc, session);

                        #endregion

                        ////Genero il root container
                        //sql = string.Format(sqlNewContainerModel, "radice", "0", "0", HomepageID, "0", network.SystemName);
                        //int rootContainer = this.Connection.ExecuteInsert(sql);

                        //string treeRoot = "." + G2Core.Common.Utility.FormatNumber(rootContainer.ToString()) + ".";

                        //sql = "UPDATE " + network.SystemName + "_homepages_contenitori SET tree_contenitore = '" + treeRoot + "' WHERE ID_contenitore = " + rootContainer;
                        //this.Connection.Execute(sql);

                        ////Genero la colonna sinistra
                        //sql = string.Format(sqlNewContainerModel, "Colsx", rootContainer, "1", HomepageID, "1", network.SystemName);
                        //int colsx = this.Connection.ExecuteInsert(sql);

                        //sql = "UPDATE " + network.SystemName + "_homepages_contenitori SET tree_contenitore = '" + treeRoot + G2Core.Common.Utility.FormatNumber(colsx.ToString()) + ".' WHERE ID_contenitore = " + colsx;
                        //this.Connection.Execute(sql);

                        ////Genero la colonna centrale
                        //sql = string.Format(sqlNewContainerModel, "Colcx", rootContainer, "1", HomepageID, "1", network.SystemName);
                        //int colcx = this.Connection.ExecuteInsert(sql);

                        //sql = "UPDATE " + network.SystemName + "_homepages_contenitori SET tree_contenitore = '" + treeRoot + G2Core.Common.Utility.FormatNumber(colcx.ToString()) + ".' WHERE ID_contenitore = " + colcx;
                        //this.Connection.Execute(sql);

                        ////Genero la colonna destra
                        //sql = string.Format(sqlNewContainerModel, "Coldx", rootContainer, "1", HomepageID, "1", network.SystemName);
                        //int coldx = this.Connection.ExecuteInsert(sql);

                        //sql = "UPDATE " + network.SystemName + "_homepages_contenitori SET tree_contenitore = '" + treeRoot + G2Core.Common.Utility.FormatNumber(coldx.ToString()) + ".' WHERE ID_contenitore = " + coldx;
                        //this.Connection.Execute(sql);

                        ////Genero il footer
                        //sql = string.Format(sqlNewContainerModel, "Footer", rootContainer, "1", HomepageID, "1", network.SystemName);
                        //int footer = this.Connection.ExecuteInsert(sql);

                        //sql = "UPDATE " + network.SystemName + "_homepages_contenitori SET tree_contenitore = '" + treeRoot + G2Core.Common.Utility.FormatNumber(footer.ToString()) + ".' WHERE ID_contenitore = " + footer;
                        //this.Connection.Execute(sql);

                        //Genero il root container
                        Contenitore rootContainer = new Contenitore();
                        rootContainer.Name = "radice";
                        rootContainer.Stato = 1;
                        rootContainer.Posizione = 0;
                        rootContainer.Depth = 0;
                        rootContainer.CssClass = network.SystemName;
                        rootContainer.Homepage = homepage;
                        homepage.Contenitori.Add(rootContainer);

                        rootContainer = HomePageBusinessLogic.SaveOrUpdateContenitore(rootContainer, session);

                        string treeRoot = "." + G2Core.Common.Utility.FormatNumber(rootContainer.ID.ToString()) + ".";

                        rootContainer.Tree = treeRoot;


                        //Genero la colonna sinistra
                        Contenitore colSxContainer = new Contenitore();
                        colSxContainer.Name = "Colsx";
                        colSxContainer.Stato = 1;
                        colSxContainer.Parent = rootContainer;
                        colSxContainer.Posizione = 0;
                        colSxContainer.Depth = 1;
                        colSxContainer.CssClass = network.SystemName;
                        rootContainer.ChildsBlocks.Add(colSxContainer);
                        homepage.Contenitori.Add(colSxContainer);
                        colSxContainer.Homepage = homepage;

                        colSxContainer = HomePageBusinessLogic.SaveOrUpdateContenitore(colSxContainer, session);


                        colSxContainer.Tree = treeRoot + G2Core.Common.Utility.FormatNumber(colSxContainer.ID.ToString()) + ".";

                        //Genero la colonna centrale
                        Contenitore colCxContainer = new Contenitore();
                        colCxContainer.Name = "Colcx";
                        colCxContainer.Stato = 1;
                        colCxContainer.Parent = rootContainer;
                        colCxContainer.Posizione = 0;
                        colCxContainer.Depth = 1;
                        colCxContainer.CssClass = network.SystemName;
                        rootContainer.ChildsBlocks.Add(colCxContainer);
                        homepage.Contenitori.Add(colCxContainer);
                        colCxContainer.Homepage = homepage;

                        colCxContainer = HomePageBusinessLogic.SaveOrUpdateContenitore(colCxContainer, session);


                        colCxContainer.Tree = treeRoot + G2Core.Common.Utility.FormatNumber(colCxContainer.ID.ToString()) + ".";

                        //Genero la colonna destra
                        Contenitore colDxContainer = new Contenitore();
                        colDxContainer.Name = "Coldx";
                        colDxContainer.Stato = 1;
                        colDxContainer.Parent = rootContainer;
                        colDxContainer.Posizione = 0;
                        colDxContainer.Depth = 1;
                        colDxContainer.CssClass = network.SystemName;
                        rootContainer.ChildsBlocks.Add(colDxContainer);
                        homepage.Contenitori.Add(colDxContainer);
                        colDxContainer.Homepage = homepage;
                        colDxContainer = HomePageBusinessLogic.SaveOrUpdateContenitore(colDxContainer, session);


                        colDxContainer.Tree = treeRoot + G2Core.Common.Utility.FormatNumber(colDxContainer.ID.ToString()) + ".";

                        //Genero il footer
                        Contenitore footerContainer = new Contenitore();
                        footerContainer.Name = "Footer";
                        footerContainer.Stato = 1;
                        footerContainer.Parent = rootContainer;
                        footerContainer.Posizione = 0;
                        footerContainer.Depth = 1;
                        footerContainer.CssClass = network.SystemName;
                        rootContainer.ChildsBlocks.Add(footerContainer);
                        homepage.Contenitori.Add(footerContainer);
                        footerContainer.Homepage = homepage;
                        footerContainer = HomePageBusinessLogic.SaveOrUpdateContenitore(footerContainer, session);

                        footerContainer.Tree = treeRoot + G2Core.Common.Utility.FormatNumber(footerContainer.ID.ToString()) + ".";

                        //HomePageBusinessLogic.SaveOrUpdateContenitore(colSxContainer);
                        //HomePageBusinessLogic.SaveOrUpdateContenitore(colCxContainer);
                        //HomePageBusinessLogic.SaveOrUpdateContenitore(colDxContainer);
                        //HomePageBusinessLogic.SaveOrUpdateContenitore(footerContainer);

                        HomePageBusinessLogic.SaveOrUpdateContenitore(rootContainer, session);

                        FolderBusinessLogic.SaveOrUpdateFolder(folder,session);
                    }
                }
                tx.Commit();
                done = true;
            }
            catch (Exception ex)
            {
                //Trace
                /*this.AddErrorString("Non è stato possibile creare uno o più record dell'Applicazione.");
                NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/

                tx.Rollback();
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile creare uno o più record dell'Applicazione HomePage", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "129");
                if (Debug) throw;
            }
            finally
            {
                session.Close();
            }

            return done;
        }
        //private bool AddTables()
        //{
        //    bool done = false;
        //    string sql = "";
        //    try
        //    {
        //        foreach (DataRow row in this.NetworksTable.Select(FilterID))
        //        {
        //            foreach (string sqlModel in this.AddTablesSql)
        //            {
        //                NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);
        //                sql = string.Format(sqlModel, network.SystemName);
        //                done = Connection.Execute(sql);
        //                if (done == false) throw new Exception();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile creare una o più tabelle dell'Applicazione HomePage nel database, query: " + sql, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "130");
        //        if (Debug) throw ex;
        //    }
        //    return done;
        //}

        //private bool AddPermission()
        //{
        //    foreach (DataRow row in this.NetworksTable.Select(FilterID))
        //    {
        //        string UserID =  NetCms.Users.AccountManager.CurrentAccount.ID.ToString();
        //        string SystemName = row["Nome_Network"].ToString();
        //        DataRow user = Connection.SqlQuery("SELECT Profile_User FROM Users WHERE id_User = " + UserID).Rows[0];
        //        DataRow root = Connection.SqlQuery("SELECT id_folder FROM " + SystemName + "_folders WHERE Depth_Folder = 1").Rows[0];

        //        string users_networks = "INSERT INTO users_networks (User_UserNetwork,Network_UserNetwork)";
        //        users_networks += " VALUES (" + UserID + "," + row["id_Network"] + ")";

        //        Connection.ExecuteInsert(users_networks);

        //        string folderprofiles = "INSERT INTO " + SystemName + "_folderprofiles (Folder_FolderProfile,Profile_FolderProfile,Creator_FolderProfile)";
        //        folderprofiles += " VALUES (" + root[0].ToString() + "," + user[0].ToString() + "," + UserID + ")";

        //        int folderProfileID = Connection.ExecuteInsert(folderprofiles);

        //        string folderprofilesCriteria = "INSERT INTO " + SystemName + "_folderprofilescriteria (FolderProfile_FolderProfileCriteria,Criteria_FolderProfileCriteria,Value_FolderProfileCriteria,Application_FolderProfileCriteria,Updater_FolderProfileCriteria)";
        //        folderprofilesCriteria += " VALUES (" + folderProfileID + ",{0},1,0," + UserID + ")";
        //        for (int i = 1; i <= 5; i++)
        //            Connection.Execute(string.Format(folderprofilesCriteria, i));
        //    }
        //    return true;
        //}

        private void RollBack()
        {
            bool done = false;
            if (this.NetworkID == 0)
            {
                using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = session.BeginTransaction())
                {
                    try
                    {
                        Application dbApp = ApplicationBusinessLogic.GetApplicationById(this.ApplicationBaseData.ID, session);
                        ApplicationBusinessLogic.DeleteApplication(dbApp, session);
                        tx.Commit();
                        done = true;
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                    }
                }

                if (done == false)
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile rimuovere l'Applicazione HomePage dal database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "131");
            }

            //Da sostituire con meccanismo di scelta se eliminare o meno tutti i record dell'applicazione (usando stateless session)
            //foreach (DataRow row in this.NetworksTable.Select(FilterID))
            //{

            //    NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);

            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_moduli_applicativi", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_moduli_banner", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_moduli_circolari", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_moduli_rss", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_moduli_documenti", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_moduli_menu", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_moduli_statici", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_moduli", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_contenitori", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages", network.SystemName));
            //}
        }
        public override void Uninstall()
        {
            base.Uninstall();

            //Da sostituire con meccanismo di scelta se eliminare o meno tutti i record dell'applicazione (usando stateless session)
            //foreach (DataRow row in this.NetworksTable.Select(FilterID))
            //{
            //    NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);

            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_moduli_applicativi", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_moduli_banner", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_moduli_circolari", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_moduli_rss", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_moduli_documenti", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_moduli_menu", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_moduli_statici", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_moduli", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages_contenitori", network.SystemName));
            //    Connection.Execute(string.Format("drop table if exists {0}_homepages", network.SystemName));
            //}

            FluentSessionProvider.Instance.UpdateSessionFactoryRemove(Assembly.GetAssembly(this.GetType()));
        }

        private bool CreatePhysicalFolder(string path, NetCms.Structure.WebFS.BasicStructureNetwork Network)
        {
            DirectoryInfo folder;
            string phisical_path;

            phisical_path = HttpContext.Current.Server.MapPath(Network.Paths.AbsoluteFrontRoot + path);
            folder = new DirectoryInfo(phisical_path);

            string adaptedAbsoluteWebRoot = Network.Paths.AbsoluteFrontRoot.Length > 0 ? Network.Paths.AbsoluteFrontRoot : "/";
            string phisical_AbsoluteWebRoot = HttpContext.Current.Server.MapPath(adaptedAbsoluteWebRoot);

            string[] paths = path.Split('/');
            string progressive_path = "\\";

            try
            {
                folder = new DirectoryInfo(phisical_AbsoluteWebRoot + path);
                if (!folder.Exists) folder.Create();

                if (!File.Exists(phisical_path + "\\default.aspx"))
                    File.Create(phisical_path + "\\default.aspx");
            }
            catch (Exception ex) { }

            return true;
        }
    }
}
