using NetCms.Connections;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;

namespace NetCms.Structure.Applications.Homepage
{
    class HomepageProp
    {
        PageData _pagedata;
        private Connection _Conn;
        private bool _ispostback;

        public HomepageProp(PageData pagedata)
        {
            _Conn = pagedata.Conn;
            _ispostback = pagedata.IsPostBack;
            _pagedata = pagedata;
        }


        public HtmlGenericControl getView()
        {

            string errors = "";

            HtmlGenericControl _toolbar = new HtmlGenericControl("div");
            _pagedata.InfoToolbar.Title = "ProprietÓ homepage";

            string id_doc = _pagedata.checkNumericGetExist("id", PageData.BackAddress).ToString();

            int id_homepage = 0;

            if (id_doc != null && id_doc != "0")
            {
                //string sql_id_homepage = "SELECT Content_DocLang FROM docs_lang WHERE Doc_DocLang = " + id_doc;
                //DataTable dt = _Conn.SqlQuery(sql_id_homepage);
                //if (dt.Rows.Count > 0)
                //{
                //    id_homepage = int.Parse(dt.Rows[0]["Content_DocLang"].ToString());
                //}
                DocumentLang docLang = DocumentBusinessLogic.GetDocLangByIdDoc(int.Parse(id_doc));
                if (docLang != null)
                {
                    id_homepage = docLang.Content.ID;
                }
            }

            NetForm form = new NetForm();

            #region ControlliForm
            if (id_homepage != 0)
            {

                NetFormTable table = new NetFormTable("homepages_docs_contents", "id_homepage", _Conn, id_homepage);
                form.AddTable(table);

                NetTextBox MetaDescription = new NetTextBox("MetaDescription", "MetaDescription_homepage");
                table.addField(MetaDescription);

                NetTextBox keywords = new NetTextBox("Keywords: ", "Keywords_homepage");
                table.addField(keywords);

                NetTextBox MsgManutenzione = new NetTextBox("Messaggio manutenzione: ", "MsgManutenzione_homepage");
                table.addField(MsgManutenzione);


                NetDropDownList Stato = new NetDropDownList("Stato", "Stato_homepage");
                Stato.addItem("Visibile al pubblico", "1");
                Stato.addItem("Nascosto al pubblico", "0");
                table.addField(Stato);

            }
            #endregion

            if (_ispostback)
            {
                errors = form.serializedUpdate(_pagedata.Request.Form);
                
                G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendHomepage", _pagedata.CurrentFolder.StructureNetwork.SystemName, ((this._pagedata.CurrentFolder.CurrentDocument) as HomepageDocument).Content.ToString());
            }

            HtmlGenericControl tuttalaform = new HtmlGenericControl("div");

            tuttalaform.Controls.Add(_toolbar);
            tuttalaform.Controls.Add(form.getForms("ProprietÓ homepage", errors));

            return tuttalaform;
        }
    }

}
