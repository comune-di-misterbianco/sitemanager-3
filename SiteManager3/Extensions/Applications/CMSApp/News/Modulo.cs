using System;
using System.Data;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using NetCms.Connections;
using NetForms;
using NetCms.Structure.WebFS;
using NetCms.Structure.Applications.Homepage.Model;

/// <summary>
/// Summary description for LayoutModuloBlock
/// </summary>
/// 
namespace NetCms.Structure.Applications.News
{
    [NetCms.Homepages.HomepageModuleDefiner(NewsApplication.ApplicationID, "News", NewsApplication.ApplicationID, NetCms.Homepages.HomepageModuleDefiner.ModuleType.CmsApp)]
    public partial class Modulo : Homepage.HomepageModule
    {
        private bool _ModuloError;
        public bool ModuloError
        {
            get { return _ModuloError; }
            set { _ModuloError = value; }
        }

        protected override string LocalCssClass
        {
            get { return " HomepageModulo_News"; }
        }
        public override string Title
        {
            get { return "Modulo News"; }
        }

        protected bool SubFolders;
        protected int Records;
        private bool ShowDesc;
        protected NewsFolder Folder;
        protected string Template;

        protected int ShowCustomNews;

        public Modulo(Homepage.Model.Modulo modulo)
            : base(modulo)
        {
            try
            {          
                if (modulo != null)
                {
                    ModuloApplicativoNews moduloApplicativo = 
                        NetCms.Structure.Applications.Homepage.HomepageModulesBusinessLogic<ModuloApplicativoNews>.GetModuloById(modulo.ID);

                    int ID = moduloApplicativo.FolderId;

                    ShowCustomNews = moduloApplicativo.ShowCustomNews;
                    SubFolders = moduloApplicativo.Subfolders == 1;
                    Records = moduloApplicativo.Records;
                    ShowDesc = moduloApplicativo.ShowDesc == 1;
                    Template = moduloApplicativo.TemplateMode;                
                    Folder = (NewsFolder)NetCms.Networks.NetworksManager.CurrentActiveNetwork.RootFolder.FindFolderByID(ID);
                    ModuloError = false;
                }
                else
                {
                    ModuloError = true;
                    throw new Exception("La query di selezione del modulo applicativo di tipo News dell'homepage non ha prodotto alcun risultato");
                }
            }
            catch (Exception ex)
            {               
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "117");
            }
        }

        protected override HtmlGenericControl GetAdminContent()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            if (ModuloError == false)
            {
                HtmlGenericControl strong = new HtmlGenericControl("strong");

                strong.InnerHtml = "News: '" + Folder.Label + "'";
                div.Controls.Add(strong);

                ModuloApplicativoNews.ShowCustom enumKey = (ModuloApplicativoNews.ShowCustom)ShowCustomNews;

                HtmlGenericControl p = new HtmlGenericControl("p");
                p.InnerHtml = "Codice Modulo : #" + NetUtility.TreeUtility.FormatNumber(this.ID.ToString(), 4) + " <br />";
                p.InnerHtml += "Template: <strong>" + Template + "</strong> <br />";
                p.InnerHtml += "Mostra " + SharedUtilities.EnumUtilities.GetEnumDescription(enumKey) + " <br />";
                p.InnerHtml += "Le ultime " + Records + " News";                
                p.InnerHtml += " prese dalla cartella '" + Folder.Label + "'";
                p.InnerHtml += SubFolders ? " e dalle sue sottocartelle" : "";
                div.Controls.Add(p);
            }

            return div;
        }
    }
}

namespace NetCms.Structure.Applications.News
{
    [NetCms.Homepages.HomepageModuleDefiner(NewsApplication.ApplicationID, "News", NewsApplication.ApplicationID, NetCms.Homepages.HomepageModuleDefiner.ModuleType.CmsApp)]
    public class ModuloForm : Homepage.HomepageModuleForm
    {
        public ModuloForm(int id)
            : base(id)
        {
        }
        public ModuloForm()
            : base()
        {
        }
        public override NetForms.NetFormTable GetFormTable()
        {
            string parentKeyId = "Modulo_ModuloApplicativo";
            string parentTableName = "homepages_moduli_applicativi";

            string keyId = "Modulo_ModuloApplicativoNews";
            string tableName = "homepages_moduli_applicativi_news";

            NetFormTable moduloApplicativo;
            NetFormTable moduloNews;

            string ShowCustomNews_ModuloApplicativoNews = "";
            if (ID != 0)
            {
                int id_modulo_applicativo_news = 0;

                string sqlQuery = 
                    "SELECT * " +
                    "FROM " + parentTableName + " " +
                    "INNER JOIN " + tableName + " " +
                    "ON " + parentTableName + "." + parentKeyId + "=" + tableName + "." + keyId + " " +
                    "WHERE " + parentTableName + "." + parentKeyId + "=" + ID 
                    ;

                DataTable table = this.Conn.SqlQuery(sqlQuery) ;
                try
                {
                    if (table.Rows.Count > 0 && int.TryParse(table.Rows[0][parentKeyId].ToString(), out id_modulo_applicativo_news)) ;
                    else throw new Exception("Errore tabella vuota oppure conversione Modulo_ModuloApplicativo errata nella classe Modulo di News");
                    ShowCustomNews_ModuloApplicativoNews = table.Rows[0]["ShowCustomNews_ModuloApplicativoNews"].ToString();
                }
                catch (Exception ex)
                {
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "118");
                }
                moduloApplicativo = new NetFormTable(parentTableName, parentKeyId, this.Conn, id_modulo_applicativo_news);
                moduloNews = new NetFormTable(tableName, keyId, this.Conn, id_modulo_applicativo_news);
            }
            else
            {
                moduloApplicativo = new NetFormTable(parentTableName, parentKeyId, this.Conn);
                NetExternalField mod = new NetExternalField("homepages_moduli", parentKeyId);
                moduloApplicativo.addExternalField(mod);

                moduloNews = new NetFormTable(tableName, keyId, this.Conn);
            }

            NetCheckBox showmorelink = new NetCheckBox("Mostra link 'Mostra tutti'", "ShowMoreLink_ModuloApplicativo");
            moduloApplicativo.addField(showmorelink);

            NetCheckBox showdesc = new NetCheckBox("Mostra descrizione", "ShowDesc_ModuloApplicativo");
            moduloApplicativo.addField(showdesc);

            NetTextBox desclen = new NetTextBox("Lunghezza Descrizione", "DescLength_ModuloApplicativo");
            desclen.Numeric = true;
            desclen.Required = true;
            moduloApplicativo.addField(desclen);

            NetCheckBox hideImage = new NetCheckBox("Non esporre le immagini", "HideImage_ModuloApplicativo");
            moduloApplicativo.addField(hideImage);

            NetDropDownList records = new NetDropDownList("Numero di record da visualizzare", "Records_ModuloApplicativo");
            records.Required = true;
            for (int i = 1; i <= 15; i++)
                records.addItem(i.ToString(), i.ToString());
            moduloApplicativo.addField(records);

            NetDropDownList showdepth = new NetDropDownList("Profondit� di visualizzazione", "Subfolders_ModuloApplicativo");
            showdepth.Required = true;
            showdepth.addItem("Mostra solo news di questa cartella", "0");
            showdepth.addItem("Mostra news di questa cartella e delle sue sottocartelle", "1");
            moduloApplicativo.addField(showdepth);


            #region MODULO NEWS
            if(ID == 0)
            {
                /// Necessario in fase di INSERT
                /// per settare correttamente
                /// la foreign key
                /// della tabella figlia
                /// 
                NetTextBox foreignkey = new NetTextBox("", "Modulo_ModuloApplicativoNews");            
                moduloNews.addField(foreignkey);
            }            
            NetDropDownList showNewsItems = new NetDropDownList
                ("Mostra", "ShowCustomNews_ModuloApplicativoNews");            
            foreach (ModuloApplicativoNews.ShowCustom enumKey in (ModuloApplicativoNews.ShowCustom[])Enum.GetValues(typeof(ModuloApplicativoNews.ShowCustom)))
                showNewsItems.addItem(SharedUtilities.EnumUtilities.GetEnumDescription(enumKey), ((int)(enumKey)).ToString() );                     
            moduloNews.addField(showNewsItems);
            #endregion

            NetDropDownList showItems = new NetDropDownList
                ("Mostra", "ShowCustomNews_ModuloApplicativoNews");
            showItems.Required = true;
            foreach (ModuloApplicativoNews.ShowCustom enumKey in (ModuloApplicativoNews.ShowCustom[])Enum.GetValues(typeof(ModuloApplicativoNews.ShowCustom)))
                showItems.addItem(SharedUtilities.EnumUtilities.GetEnumDescription(enumKey), ((int)(enumKey)).ToString());
            showItems.Value = ShowCustomNews_ModuloApplicativoNews;
            showItems.JumpField = true;
            moduloApplicativo.addField(showItems);

            showNewsItems.Value = showItems.Value;
            moduloApplicativo.SetInnerJoinTables(new NetFormTable[] { moduloNews });

            if (ID == 0)
            {
                StructureFolderBinder binder = new StructureFolderBinder(PageData.CurrentReference);
                TreeNode root = binder.TreeFromStructure((StructureFolder)NetCms.Networks.NetworksManager.CurrentActiveNetwork.RootFolder, NewsApplication.ApplicationID);
                NetTreeView folders = new NetTreeView("Seleziona una cartella", "Folder_ModuloApplicativo", root);
                folders.NotAllowedMsg = "Attenzione! La cartella selezionata non � di tipo news o non si possiedono i permessi di accesso";
                folders.RootDist = Configurations.Paths.AbsoluteRoot;
                folders.CheckAllows = true;
                folders.Required = true;               
                moduloApplicativo.addField(folders);
            }

            return  moduloApplicativo ;
        }
        public override TemplateOptionEnum TemplateOption
        {
            get { return TemplateOptionEnum.ExendedTemplate; }
        }
    }
}
