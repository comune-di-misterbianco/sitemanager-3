using System;
using System.Xml;
using System.IO;
using NetCms.Structure.WebFS;
using System.Web;
using NHibernate;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Structure.Applications.News
{
    public class RssManager : NetCms.RssManager
    {
        public RssManager(string link, string title, string desc, StructureFolder folder)
            : base(link, title, desc, folder)
        {
        }
        public override void AddCustomContents(RssItem item, NetCms.Structure.WebFS.Document doc, ISession session = null)
        {     
            NetCms.Structure.WebFS.Document document = NetCms.Structure.WebFS.FileSystemBusinessLogic.DocumentBusinessLogic.GetById(doc.ID,session);
           ///NewsDocument newsDoc = NewsBusinessLogic.GetDocumentRecord(doc.ID,session);
            
            NewsDocContent currentNews = NewsBusinessLogic.GetById(document.Content,session);

            string imgStr = "";
            if (currentNews.ImgInEvidenza != null && currentNews.ImgInEvidenza.RifDoc > 0)
            {
                string imgPattern = @"<img src=""{0}"" alt=""{1}"" />";
                NewsHighLigthImage img = currentNews.ImgInEvidenza;
                string urlImg = img.GetSrc(NewsHighLigthImage.Presets.None);
                string imgPath = Folder.StructureNetwork.Paths.AbsoluteFrontRoot + urlImg;
                string absoluteImgURL = base.BaseUrl + Folder.StructureNetwork.Paths.AbsoluteFrontRoot + urlImg;
                imgStr = string.Format(imgPattern, absoluteImgURL, img.Description);

                var imgType = string.Empty;
                switch (Path.GetExtension(HttpContext.Current.Server.MapPath(imgPath)).ToLower())
                {
                    case ".jpg":
                        imgType = "image/jpeg";
                        break;
                    case ".gif":
                        imgType = "image/gif";
                        break;
                    case ".png":
                        imgType = "image/png";
                        break;
                }

                item.Media = new Media() { Url = absoluteImgURL, Medium = "image", Type = imgType, Width = 280, Height = 140 };
            }

            item.Title = currentNews.DocLang.Label;
            item.PubDate = NetCms.Utility.Rfc822Date.GetRFC822Date(currentNews.Revisione.Data);
            item.Category = "News";    
            item.Description = doc.Descrizione + " " + imgStr;
            item.Guid = base.BaseUrl + Folder.StructureNetwork.Paths.AbsoluteFrontRoot + doc.FrontendLink + "&rss=1";
            item.Link = base.BaseUrl + Folder.StructureNetwork.Paths.AbsoluteFrontRoot + doc.FrontendLink;            
        }
        //public override RssDocument InitDocument()
        //{
        //    RssDocument newDoc = new RssDocument(Link, Title, Desc);
        //    newDoc.Category = "News";
        //    newDoc.PubDate = NetCms.Utility.Rfc822Date.GetRFC822Date(DateTime.Now);
        //    newDoc.Copyright = Folder.PageData.Network.Label;
        //    newDoc.WebMaster = Folder.PageData.Network.eMail;
        //    newDoc.LastBuildDate = NetCms.Utility.Rfc822Date.GetRFC822Date(DateTime.Now);

        //    return newDoc;
        //}
    }
}