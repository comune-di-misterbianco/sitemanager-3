﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Structure.Applications.News;

namespace NetCms.Structure.Applications.News.Configs
{
    public static class Config
    {
        public static bool ArchivioStorico
        {
            get
            {
                if (NewsApplication.Configuration["ArchivioStorico"] != null
                    && NewsApplication.Configuration["ArchivioStorico"].ToString() == "true")
                    return true;
                return false;
            }
        }

        public static bool DataPubblicazione
        {
            get
            {
                if (NewsApplication.Configuration["DataPubblicazione"] != null
                    && NewsApplication.Configuration["DataPubblicazione"].ToString() == "true")
                    return true;
                return false;
            }
        }

        public static bool DataScadenza
        {
            get
            {
                if (NewsApplication.Configuration["DataScadenza"] != null
                    && NewsApplication.Configuration["DataScadenza"].ToString() == "true")
                    return true;
                return false;
            }
        }

        public static bool UseAshxHandler
        {
            get
            {
                bool useAshxHandler = false;
                try
                {
                    if (NewsApplication.Configuration["UseAshxHandler"] != null && NewsApplication.Configuration["UseAshxHandler"].ToString() == "true")
                        return true;
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Chiave UseAshxHandler mancante, verificare il web.config.");
                }

                return useAshxHandler;
            }
        }
    }
}
