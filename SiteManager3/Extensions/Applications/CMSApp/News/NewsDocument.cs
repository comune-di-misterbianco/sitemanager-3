using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using System.Collections.Generic;

/// <summary>
/// Summary description for FilesDocument
/// </summary>
namespace NetCms.Structure.Applications.News
{
    public class NewsDocument : NetCms.Structure.WebFS.Document
    {
        public override string FrontendLink
        {
            get
            {
                string frontendlink = string.Empty;

                StructureFolder currentFolder = null;

                currentFolder = this.Folder as StructureFolder;

                if (currentFolder != null && !string.IsNullOrEmpty(currentFolder.FrontendHyperlink))
                    frontendlink = currentFolder.FrontendHyperlink + "default.aspx?news=" + this.ID;
                else
                {
                    currentFolder = NetCms.Structure.WebFS.FileSystemBusinessLogic.FolderBusinessLogic.GetById(this.Folder.ID);
                    if (currentFolder != null)
                        frontendlink = currentFolder.FrontendHyperlink + "default.aspx?news=" + this.ID;
                }
                return frontendlink;
            }
        }


        public override string Icon
        {
            get { return "icon_news"; }
        }

        public override DocContent ContentRecord
        {
            get
            {
                if (_ContentRecord == null)
                {
                    _ContentRecord = NewsBusinessLogic.GetById(this.Content);
                }
                return _ContentRecord;
            }
        }


        #region IsMultilanguage

        private bool MultilanguageLoaded;
        private bool _Multilanguage;
        private bool Multilanguage
        {
            get
            {
                return false;
            }
        }

        #endregion

        public override string Link
        {
            get { return "/applications/News/detail.aspx?id=" + ID; }
        }

        public NewsDocument()
            : base()
        { }

        public NewsDocument(Networks.NetworkKey network)
            : base(network)
        {
        }

        public override HtmlGenericControl getRowView()
        {
            return getRowView(false);
        }
        public override HtmlGenericControl getRowView(bool SearchView)
        {
            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina di elenco News del portale "+NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");

            HtmlGenericControl tr = new HtmlGenericControl("tr");
            HtmlGenericControl td = new HtmlGenericControl("td");

            #region Icona
            /************************/
            td.InnerHtml = "&nbsp;";
            td.Attributes["class"] = "FS_Icon " + Icon;
            tr.Controls.Add(td);
            /************************/
            #endregion

            #region Nome & Link Alla Scheda
            /************************/
            string Link;

            if (!this.Grant)
                Link = this.Nome;
            else
                Link = BuildLink();

            td = new HtmlGenericControl("td");
            td.InnerHtml = Link;
            tr.Controls.Add(td);
            /************************/
            #endregion

            #region Cartella di apparteneza
            // *************************************
            if (SearchView)
            {
                td = new HtmlGenericControl("td");              
                td.InnerHtml = (this.Folder as StructureFolder).LabelLinkPath;
                tr.Controls.Add(td);
            }
            //*************************************
            #endregion

            #region Data Oggetto
            //*************************************
            td = new HtmlGenericControl("td");
            td.InnerHtml = this.Date.ToString();
            if (td.InnerHtml.Length > 10) td.InnerHtml = td.InnerHtml.Remove(10);
            tr.Controls.Add(td);
            //*************************************
            #endregion

            #region Tipo Dell'oggetto
            //*************************************
            td = new HtmlGenericControl("td");
            td.InnerHtml = "News";
            tr.Controls.Add(td);
            //*************************************
            #endregion

            if (!SearchView)
            {
                #region Stato
                //*************************************
                td = new HtmlGenericControl("td");
                td.InnerHtml = this.StatusLabel;
                if ((this.Folder as StructureFolder).ReviewEnabled)
                    td.InnerHtml += " - " + (this.Content > 0 ? "Pubblicato" : "Non Pubblicato");

                tr.Controls.Add(td);
                //*************************************
                #endregion

                NetUtility.HtmlGenericControlCollection options = this.OptionsControls("news");
                if (options != null)
                    foreach (HtmlGenericControl option in options)
                        tr.Controls.Add(option);
            }
         

            return tr;
        }

        private string BuildLink()
        {
          
            string Link = "<a href=\"" + SiteManagerLink + "\">" + this.Nome + "</a>";

            return Link;
        }
    }
}