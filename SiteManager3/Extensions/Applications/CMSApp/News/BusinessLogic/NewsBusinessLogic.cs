﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using NetCms.Structure.WebFS;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using NetCms.DBSessionProvider;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Users;
using NHibernate.Transform;

namespace NetCms.Structure.Applications.News
{
    public static class NewsBusinessLogic
    {

        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static NewsDocContent GetById(int idNewsDoc, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<NewsDocContent> dao = new CriteriaNhibernateDAO<NewsDocContent>(innerSession);
            return dao.GetById(idNewsDoc);
        }

        public static IList<NewsDocContent> FindAllApplicationRecords(ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();
            IGenericDAO<NewsDocContent> dao = new CriteriaNhibernateDAO<NewsDocContent>(innerSession);
            return dao.FindAll();
        }


        /// <summary>
        /// Restituisce una lista di NewsDocContent contenuti in una cartella 
        /// </summary>
        /// <param name="folderId">Id della cartella</param>
        /// <param name="innerSession">Eventuale sessione esterna se disponibile</param>
        /// <returns></returns>
        public static IList<NewsDocContent> GetApplicationRecordsByFolder(int folderId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<NewsDocContent> dao = new CriteriaNhibernateDAO<NewsDocContent>(innerSession);

            List<SearchParameter> folder = new List<SearchParameter>();
            List<SearchParameter> document = new List<SearchParameter>();
            List<SearchParameter> doclang = new List<SearchParameter>();
            List<SearchParameter> content = new List<SearchParameter>();
            
            folder.Add(new SearchParameter(null,"ID",folderId, Finder.ComparisonCriteria.Equals,false));
            document.Add(new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false)); 

            SearchParameter foldSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, folder.ToArray<SearchParameter>());
            document.Add(foldSP);

            SearchParameter docSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, document.ToArray<SearchParameter>());
            doclang.Add(docSP);

            SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, doclang.ToArray<SearchParameter>());
            content.Add(docLangSP);
           
            IList<NewsDocContent> results = dao.FindByCriteria(content.ToArray()).ToList();

            return results;
        }


        public static DocumentLang GetDocLangById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();
            IGenericDAO<DocumentLang> dao = new CriteriaNhibernateDAO<DocumentLang>(innerSession);
            return dao.GetById(id);
        }

        // logica di recupero e salvataggio highlightimage
        // getbyid
        public static NewsHighLigthImage GetHighLigthImageByID(int id, ISession innerSession = null)
        { 
            if (innerSession == null)
               innerSession = GetCurrentSession();

            CriteriaNhibernateDAO<NewsHighLigthImage> dao = new CriteriaNhibernateDAO<NewsHighLigthImage>(innerSession);
            return dao.GetById(id);
        }
        
        // save-update highlightimages
        public static void SaveHighLightImageContent(int idDoc, NewsHighLigthImage newsHighLigthImage, ISession innerSession = null) 
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            CriteriaNhibernateDAO<NewsDocContent> dao = new CriteriaNhibernateDAO<NewsDocContent>(innerSession);
            NewsDocContent docContent = NewsBusinessLogic.GetById(idDoc,innerSession);
            docContent.ImgInEvidenza = newsHighLigthImage;
            dao.SaveOrUpdate(docContent);
        }

        public static void RemoveHighLightImageContent(int idDoc, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            CriteriaNhibernateDAO<NewsDocContent> dao = new CriteriaNhibernateDAO<NewsDocContent>(innerSession);
            NewsDocContent docContent = NewsBusinessLogic.GetById(idDoc, innerSession);
            
            if (docContent.ImgInEvidenza != null)
            {
                docContent.ImgInEvidenza = null;
                dao.SaveOrUpdate(docContent);
            }
        }

        public static NewsDocContent SaveOrUpdateNewsDocsContent(NewsDocContent content, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();
            IGenericDAO<NewsDocContent> dao = new CriteriaNhibernateDAO<NewsDocContent>(innerSession);
            return dao.SaveOrUpdate(content);
        }

        #region Metodi di frontend


        public static SearchParameter SearchContentByDataDoc(object data, Finder.ComparisonCriteria criterio)
        {
            SearchParameter dataSP = new SearchParameter(null, "Data", data, criterio, false);
            SearchParameter docSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, dataSP);
            SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, docSP);

            return docLangSP;
        }

        public static SearchParameter SearchContentByDescDocLang(string descrizione)
        {
            SearchParameter descSP = new SearchParameter(null, "Descrizione", "%" + descrizione + "%", Finder.ComparisonCriteria.Like, false);
            SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, descSP);

            return docLangSP;
        }

        public static SearchParameter SearchContentByTitoloDocLang(string titolo)
        {
            SearchParameter descSP = new SearchParameter(null, "Titolo", "%" + titolo + "%", Finder.ComparisonCriteria.Like, false);
            SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, descSP);

            return docLangSP;
        }

        public static IList<SearchParameter> ChildsFolders(string Path, int NetworkID)
        {
            List<SearchParameter> lista = new List<SearchParameter>();

            StructureFolder mytree = FolderBusinessLogic.GetFolderRecordByPath(Path, NetworkID);
            if (mytree != null)
            {
                //folders = "(Stato_Folder < 2 AND Tree_Folder LIKE '" + mytree.Tree + "%')";
                SearchParameter treeSP = new SearchParameter(null, "Tree", mytree.Tree + "%", Finder.ComparisonCriteria.Like, false);
                SearchParameter statoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.LessThan, false);
                SearchParameter foldSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { treeSP, statoSP });
                SearchParameter docSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, foldSP);
                SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, docSP);
                lista.Add(docLangSP);
            }

            return lista;
        }

        public static IList<SearchParameter> SearchInThisFolder(int FolderID)
        {
            List<SearchParameter> lista = new List<SearchParameter>();

            SearchParameter idSP = new SearchParameter(null, "ID", FolderID, Finder.ComparisonCriteria.Equals, false);
            SearchParameter foldSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, idSP);
            SearchParameter docSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, foldSP);
            SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, docSP);
            lista.Add(docLangSP);

            return lista;
        }



        //    //SELECT * FROM Folders INNER JOIN  Docs ON id_Folder = Folder_Doc 
        //    //INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang 
        //    //INNER JOIN Docs_News ON id_News = Content_DocLang 
        //    //WHERE id_Doc = id AND Stato_Doc<>2 AND Trash_Doc = 0

        public static NewsDocument GetDocumentRecord(int idDoc, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<NewsDocument> dao = new CriteriaNhibernateDAO<NewsDocument>(innerSession);

            SearchParameter applicationSP = new SearchParameter(null, "Application", 3, Finder.ComparisonCriteria.Equals, false);

            SearchParameter statoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.Not, false);

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter idSP = new SearchParameter(null, "ID", idDoc, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { applicationSP, statoSP, trashSP, idSP });

        }




        //    //SELECT * FROM docs_news 
        //    //    INNER JOIN revisioni ON id_News=Contenuto_Revisione 
        //    //    INNER JOIN docs_lang ON DocLang_Revisione=id_DocLang 
        //    //    INNER JOIN docs ON Doc_DocLang=id_Doc WHERE true
        //    //        and id_News = " + this.ContentID + " AND Folder_Doc=" + this.PageData.Folder.ID
        public static NewsDocument GetDocumentPreviewRecord(int idNewsDoc, int idFolder, ISession innerSession = null)
        {

            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<NewsDocument> dao = new CriteriaNhibernateDAO<NewsDocument>(innerSession);

            SearchParameter applicationSP = new SearchParameter(null, "Application", 3, Finder.ComparisonCriteria.Equals, false);

           // SearchParameter statoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.Not, false);

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter folderSP = new SearchParameter(null, "Folder.ID", idFolder, Finder.ComparisonCriteria.Equals, false);

            SearchParameter idSP = new SearchParameter(null, "ID", idNewsDoc, Finder.ComparisonCriteria.Equals, false);
            SearchParameter ContentSP = new SearchParameter(null, "Content", SearchParameter.RelationTypes.OneToOne, idSP);
            SearchParameter RevisioneSP = new SearchParameter(null, "Revisioni", SearchParameter.RelationTypes.OneToMany, ContentSP);
            SearchParameter docsLangSP = new SearchParameter(null, "DocLangs", SearchParameter.RelationTypes.OneToMany, RevisioneSP);
            
            //statoSP,
            return dao.GetByCriteria(new SearchParameter[] { applicationSP, trashSP, folderSP, docsLangSP });

        }

        public static IList<NewsDocContent> GetNewsPaged(int currentPage, int pageSize, IList<SearchParameter> searchParameters, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            try
            {
                CriteriaNhibernateDAO<NewsDocContent> dao = new CriteriaNhibernateDAO<NewsDocContent>(innerSession);

                ICriteria query = dao.BuildCriteria(searchParameters.ToArray());

                if (NetCms.Structure.Applications.News.Configs.Config.DataPubblicazione)
                query.Add(Restrictions.Or(Restrictions.Le("DataPubblicazione", DateTime.Today), Restrictions.IsNull("DataPubblicazione")));

                if (NetCms.Structure.Applications.News.Configs.Config.DataScadenza)
                query.Add(Restrictions.Or(Restrictions.Ge("DataScadenza", DateTime.Today), Restrictions.IsNull("DataScadenza")));

                IList<NewsDocContent> news = query.SetFirstResult(((currentPage - 1) * pageSize)).SetMaxResults(pageSize).List<NewsDocContent>();


                //IList<NewsDocContent> news = dao.FindByCriteria(((currentPage - 1) * pageSize),
                //                                pageSize,
                //                                searchParameters.ToArray());

                return news;
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(NewsDocContent) + ". " + ex.Message);
                return null;
            }
        }

        public static IList<NewsDocContent> GetArchiviedNewsPaged(int currentPage, int pageSize, IList<SearchParameter> searchParameters, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            try
            {
                CriteriaNhibernateDAO<NewsDocContent> dao = new CriteriaNhibernateDAO<NewsDocContent>(innerSession);

                ICriteria query = dao.BuildCriteria(searchParameters.ToArray());
                
                if (NetCms.Structure.Applications.News.Configs.Config.DataPubblicazione)
                    query.Add(Restrictions.Or(Restrictions.Le("DataPubblicazione", DateTime.Today), Restrictions.IsNull("DataPubblicazione")));

                if (NetCms.Structure.Applications.News.Configs.Config.DataPubblicazione)
                    query.Add(Restrictions.Le("DataScadenza", DateTime.Today));

                IList<NewsDocContent> news = query.SetFirstResult(((currentPage - 1) * pageSize)).SetMaxResults(pageSize).List<NewsDocContent>();

                return news;
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(NewsDocContent) + ". " + ex.Message);
                return null;
            }
        }


        public static IList<NewsDocContent> GetNewsBySearchParameters(IList<SearchParameter> searchParameters, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            GenericDAO.DAO.CriteriaNhibernateDAO<NewsDocContent> dao = new GenericDAO.DAO.CriteriaNhibernateDAO<NewsDocContent>(innerSession);

            var news = dao.FindByCriteria(searchParameters.ToArray());

            return news;
        }

        //public static IList<NewsDocContent> GetPagedRecordsWithSearch(IList<SearchParameter> searchFields, int rpp, int pageStart)
        //{
        //    //SELECT * FROM Folders 
        //    //INNER JOIN  Docs ON id_Folder = Folder_Doc 
        //    //INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang 
        //    //INNER JOIN Docs_News ON id_News = Content_DocLang 
        //    //WHERE Application_Doc = 3 AND Stato_Doc<>2 AND Trash_Doc = 0 //Da prendere dentro document
        //    //AND Stato_Folder < 2
        //    //AND Tree_Folder LIKE '" + mytree.Tree + "%'
        //    //AND ( Testo_News LIKE '%" + text.StringValue + "%' OR
        //    //Desc_DocLang LIKE '%" + text.StringValue + "%' OR
        //    //Label_DocLang LIKE '%" + text.StringValue + "%' OR
        //    //Name_Doc LIKE '%" + text.StringValue + "%')
        //    //AND Data_Doc > data // nel document

        //    //searchFIelds[0]: testo da cercare in testo(newsDocCOntent), descrizione(doclang), label(doclang), name(newsDocument) [CON OR]
        //    //searchFIelds[1]: compararatore da usare per la data
        //    //searchFIelds[2]: data di riferimento

        //    //folderSqlQuery:  Enterà questa stringa (Stato_Folder < 2 AND Tree_Folder LIKE '" + mytree.Tree + "%') //da prendere dentro la folder
        //    //folderSqlQuery:  Oppure questa stringa Folder_Doc  = xxx //da prendere dentro il document
        //    //string parametro ="";
        //    //if (folderSqlQuery.Length > 20)
        //    //{
        //    //    System.Diagnostics.Debug.WriteLine(folderSqlQuery);
        //    //    int like = folderSqlQuery.LastIndexOf("LIKE '");
        //    //    System.Diagnostics.Debug.WriteLine(like);
        //    //    int end = folderSqlQuery.LastIndexOf("%'");
        //    //    System.Diagnostics.Debug.WriteLine(end);
        //    //    //prendere anche la percentuale
        //    //    parametro = folderSqlQuery.Substring(like + 6, (end - like-5));
        //    //}
        //    //else
        //    //{
        //    //    int start = 14 + folderSqlQuery.IndexOf("Folder_Doc  = ");
        //    //    parametro = folderSqlQuery.Substring(start, folderSqlQuery.Length - start);
        //    //}



        //    //IGenericDAO<NewsDocContent> dao = new CriteriaNhibernateDAO<NewsDocContent>(GetCurrentSession());


        //    ////Condizioni da effettuare su NewsDocContent
        //    //Conjunction and = new Conjunction();
        //    //Disjunction or = new Disjunction();

        //    ////Verifico se è stato inserito del testo nella casella ricerca e faccio le query con OR
        //    //if (searchFields[0] != null)
        //    //{
        //    //    //Condizioni su NewsDocContent
        //    //    or.Add(Restrictions.Like("Testo", "%" + searchFields[0] + "%"));
        //    //    //Condizione da effettuare su DocLang
        //    //    or.Add(Restrictions.Like("dLang.Descrizione", "%" + searchFields[0]+"%"));
        //    //    or.Add(Restrictions.Like("dLang.Label", "%" + searchFields[0] + "%"));
        //    //    //Condizione su NewsDocument
        //    //    or.Add(Restrictions.Like("doc.Name", "%" + searchFields[0] + "%"));
        //    //}
        //    //if(or.ToString().Length > 2)    
        //    //    and.Add(or);
        //    ////Verifico se ho una comparazione di data e la eseguo
        //    //if (searchFields[2] != null)
        //    //{
        //    //    //parsing data
        //    //    DateTime tempo;
        //    //    DateTime.TryParse(searchFields[2], out tempo);
        //    //    //parsing comparatore
        //    //    if (searchFields[1].CompareTo(">=") == 0)
        //    //        and.Add(Restrictions.Ge("doc.Data", tempo));
        //    //    if (searchFields[1].CompareTo("<=") == 0)
        //    //        and.Add(Restrictions.Le("doc.Data", tempo));
        //    //    if (searchFields[1].CompareTo("==") == 0)
        //    //        and.Add(Restrictions.Eq("doc.Data", tempo));
        //    //}

        //    //and.Add(Restrictions.Eq("doc.Application", 3));
        //    //and.Add(Restrictions.Not(Restrictions.Eq("doc.Stato", 2)));
        //    //and.Add(Restrictions.Eq("doc.Trash", 0));
        //    //if (folderSqlQuery.Length > 20)
        //    //    and.Add(Restrictions.Like("fold.Tree", parametro));
        //    //else
        //    //    and.Add(Restrictions.Eq("fold.ID", int.Parse(parametro)));
        //    //and.Add(Restrictions.Lt("fold.Stato", 2));


        //    ////inserire paginazione
        //    //return GetCurrentSession().CreateCriteria<NewsDocContent>()
        //    //    .CreateAlias("DocLang", "dLang")
        //    //    .CreateAlias("dLang.Document", "doc")
        //    //    .CreateAlias("doc.Folder", "fold")
        //    //    .Add(and)
        //    //    .SetFirstResult((pageStart-1)*rpp)
        //    //    .SetMaxResults(rpp)
        //    //    .List<NewsDocContent>();


        //    IGenericDAO<NewsDocContent> dao = new CriteriaNhibernateDAO<NewsDocContent>(GetCurrentSession());

        //    return dao.FindByCriteria((pageStart-1)*rpp,rpp,searchFields.ToArray<SearchParameter>());

        //}


        public static int GetNumRecordsWithSearch(IList<SearchParameter> searchFields, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            try
            {
                CriteriaNhibernateDAO<NewsDocContent> dao = new CriteriaNhibernateDAO<NewsDocContent>(innerSession);

                ICriteria query = dao.BuildCriteria(searchFields.ToArray<SearchParameter>());

                if (NetCms.Structure.Applications.News.Configs.Config.DataPubblicazione)
                    query.Add(Restrictions.Or(Restrictions.Le("DataPubblicazione", DateTime.Today), Restrictions.IsNull("DataPubblicazione")));

                if (NetCms.Structure.Applications.News.Configs.Config.DataScadenza)
                    query.Add(Restrictions.Or(Restrictions.Ge("DataScadenza", DateTime.Today), Restrictions.IsNull("DataScadenza")));
                    

                ICriteria countCriteria = CriteriaTransformer.TransformToRowCount(query);

                return countCriteria.UniqueResult<int>();
                //  return dao.FindByCriteriaCount(searchFields.ToArray<SearchParameter>());
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(NewsDocContent) + ". " + ex.Message);
                return -1;
            }
        }

        public static int GetNumRecordsWithSearchForArchive(IList<SearchParameter> searchFields,ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            try
            {
                CriteriaNhibernateDAO<NewsDocContent> dao = new CriteriaNhibernateDAO<NewsDocContent>(innerSession);

                ICriteria query = dao.BuildCriteria(searchFields.ToArray<SearchParameter>());

                if (NetCms.Structure.Applications.News.Configs.Config.DataPubblicazione)
                    query.Add(Restrictions.Le("DataPubblicazione", DateTime.Today));

                if (NetCms.Structure.Applications.News.Configs.Config.DataScadenza)
                    query.Add(Restrictions.Le("DataScadenza", DateTime.Today));

                ICriteria countCriteria = CriteriaTransformer.TransformToRowCount(query);

                return countCriteria.UniqueResult<int>();

                //  return dao.FindByCriteriaCount(searchFields.ToArray<SearchParameter>());
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(NewsDocContent) + ". " + ex.Message);
                return -1;
            }
        }

        public static NewsFolder GetNewsFolder(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            try
            {
                IGenericDAO<NewsFolder> dao = new CriteriaNhibernateDAO<NewsFolder>(innerSession);
                return dao.GetById(id);
            }
            catch(Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(NewsDocContent) + ". " + ex.Message);
                return null;
            }
        }

        #endregion



        #region Import/Export


        /// <summary>
        /// Metodo per il salvataggio di una news 
        /// </summary>
        /// <param name="folderID">ID della folder</param>
        /// <param name="titolo">Titolo della news</param>
        /// <param name="descrizione">Descrizione della news</param>
        /// <param name="htmlContent">Contenuto in formato html</param>
        /// <param name="data">Data inserimento</param>
        /// <param name="idLang">Rif all'id della lingua</param>
        /// <param name="appID">Rif all'id dell'applicazione di cms</param>
        /// <param name="sess">Sessione nhibernate</param>
        //public static void SaveNews(int folderID, string titolo, string descrizione, string htmlContent, DateTime data, int idLang, int appID, ISession sess = null)
        //{
        //    try
        //    {
        //        CriteriaNhibernateDAO<NewsDocContent> dao = new CriteriaNhibernateDAO<NewsDocContent>(sess);
        //        //
        //        StructureFolder containerFolder = FolderBusinessLogic.GetById(sess, folderID);

        //        //
        //        User currentUser = UsersBusinessLogic.GetById(Users.AccountManager.CurrentAccount.ID, sess);

        //        //
        //        Application application = ApplicationsPool.ActiveAssemblies[appID.ToString()];

        //        // Document
        //        Document newDocument = new NewsDocument();
        //        newDocument.PhysicalName = NetCms.Structure.Utility.ImportContentFilter.ClearFileSystemName(titolo);
        //        newDocument.Stato = 1;
        //        newDocument.Create = data;
        //        newDocument.Data = data;
        //        newDocument.Modify = data;
        //        newDocument.Autore = currentUser.ID;
        //        newDocument.Hide = 0;
        //        newDocument.CssClass = "";
        //        newDocument.Application = appID;
        //        newDocument.TitleOffset = 0;
        //        newDocument.Trash = 0;
        //        newDocument.LangPickMode = 0;
        //        newDocument.Order = 0;
        //        newDocument.Layout = 0;
        //        newDocument.Folder = containerFolder;
        //        containerFolder.Documents.Add(newDocument);


        //        // utilizzare la reflection
        //        NewsDocContent docContent = new NewsDocContent();
        //        docContent.Testo = htmlContent;
        //        docContent.Stato = NewsDocContent.NewsStatus.Normale;


        //        DocumentLang docLang = new DocumentLang();
        //        docLang.Document = newDocument;
        //        newDocument.DocLangs.Add(docLang);
        //        docLang.Label = titolo;
        //        docLang.Descrizione = descrizione;
        //        docLang.Lang = idLang;


        //        newDocument = DocumentBusinessLogic.SaveOrUpdateDocument(newDocument, sess);

        //        DocumentBusinessLogic.SaveOrUpdateDocumentLang(docLang, sess);

        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        #endregion



        public static IList<YearMonth> GetArchivioStorico(int idFolder, ISession innerSession = null)//int idTipologia,
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            NHibernate.ICriteria criteria = innerSession.CreateCriteria(typeof(NewsDocument));

            var dateFromYearProj = Projections.SqlFunction("year", NHibernateUtil.Int32, Projections.Property("Data"));
            var dateFromMonthProj = Projections.SqlFunction("month", NHibernateUtil.Int32, Projections.Property("Data"));
            var dateFromDayProj = Projections.SqlFunction("day", NHibernateUtil.Int32, Projections.Property("Data"));

            criteria.Add(Expression.Eq("Folder.ID", idFolder));

            criteria.SetProjection(Projections.ProjectionList()
                .Add(dateFromYearProj, "Year")
                .Add(dateFromMonthProj, "Month")
                .Add(dateFromDayProj, "Day")
                .Add(Projections.GroupProperty(dateFromYearProj))
                .Add(Projections.GroupProperty(dateFromMonthProj))
                .Add(Projections.GroupProperty(dateFromDayProj))
                );

            criteria
                .AddOrder(Order.Asc(dateFromYearProj))
                .AddOrder(Order.Asc(dateFromMonthProj))
                .AddOrder(Order.Asc(dateFromDayProj)
                );

            criteria.SetResultTransformer(Transformers.AliasToBean(typeof(YearMonth)));

            var result = criteria.List<YearMonth>() as List<YearMonth>;

            return result;

        }

        public static IList<YearMonth> GetArchivioStorico(string folderTree, ISession innerSession = null)//int idTipologia,
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            NHibernate.ICriteria criteria = innerSession.CreateCriteria(typeof(NewsDocument));

            var dateFromYearProj = Projections.SqlFunction("year", NHibernateUtil.Int32, Projections.Property("Data"));
            var dateFromMonthProj = Projections.SqlFunction("month", NHibernateUtil.Int32, Projections.Property("Data"));
            var dateFromDayProj = Projections.SqlFunction("day", NHibernateUtil.Int32, Projections.Property("Data"));

            criteria.CreateAlias("Folder", "folder");
            criteria.Add(Expression.Like("folder.Tree", folderTree + "%"));

            criteria.SetProjection(Projections.ProjectionList()
                .Add(dateFromYearProj, "Year")
                .Add(dateFromMonthProj, "Month")
                .Add(dateFromDayProj, "Day")
                .Add(Projections.GroupProperty(dateFromYearProj))
                .Add(Projections.GroupProperty(dateFromMonthProj))
                .Add(Projections.GroupProperty(dateFromDayProj))
                );

            criteria
                .AddOrder(Order.Asc(dateFromYearProj))
                .AddOrder(Order.Asc(dateFromMonthProj))
                .AddOrder(Order.Asc(dateFromDayProj)
                );

            criteria.SetResultTransformer(Transformers.AliasToBean(typeof(YearMonth)));

            var result = criteria.List<YearMonth>() as List<YearMonth>;

            return result;

        }
    }
}
