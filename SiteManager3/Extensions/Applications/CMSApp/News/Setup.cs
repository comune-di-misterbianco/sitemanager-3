﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using System.Reflection;
using NetCms.DBSessionProvider;
using NHibernate;


namespace NetCms.Structure.Applications.News
{
    public class Setup : NetCms.Structure.Applications.Setup
    {
        public Setup(NetCms.Structure.Applications.Application baseApp)
            : base(baseApp)
        {
        }

        public override string[,] Pages
        {
            get
            {
                string[,] pages = {
                                  { "news_new.aspx", "NetCms.Structure.Applications.News.Pages.NewsAdd" },
                                  { "modify.aspx", "NetCms.Structure.Applications.News.Pages.NewsModify" }
                                  };
                return pages;
            }
        }

        protected override bool LocalInstall(bool isInsidePackage = false)
        {
            bool done = true;
            try
            {
                FluentSessionProvider.Instance.UpdateSessionFactoryAdd(Assembly.GetAssembly(this.GetType()));           
            }
            catch (Exception ex)
            {
                this.RollBack();
                done = false;
            }
            return done;
        }
   
        private void RollBack()
        {
            bool done = false;
            if (!(NetworkID > 0))
            {
                using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = session.BeginTransaction())
                {
                    try
                    {
                        Application dbApp = ApplicationBusinessLogic.GetApplicationById(this.ApplicationBaseData.ID, session);
                        ApplicationBusinessLogic.DeleteApplication(dbApp, session);
                        tx.Commit();
                        done = true;
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                    }
                }

                if (done == false)
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile rimuovere l'Applicazione News dal database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "115");
            }          
        }

        public override void Uninstall()
        {
            base.Uninstall();

            FluentSessionProvider.Instance.UpdateSessionFactoryRemove(Assembly.GetAssembly(this.GetType()));
        }
    }
}
