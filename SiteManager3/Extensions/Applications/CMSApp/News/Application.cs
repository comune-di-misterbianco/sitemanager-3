﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Data;

namespace NetCms.Structure.Applications.News
{
    public class NewsApplication : NetCms.Structure.Applications.Application
    {
        public const string ApplicationContextFrontLabel = "News.Frontend";
        public const string ApplicationContextBackLabel = "News.Backoffice";
        public const int ApplicationID = 3;

        public override string[] AllowedSubfolders
        {
            get { return new[] { "files" }; }
        }
        
        public override bool EnableNewsLetter 
        { get { return true; } }
      
        public override bool EnableMove 
        { get { return true; } }
        
        public override bool EnableModule 
        { get { return true; } }
      
        public override bool EnableReviews
        {
            get { return true; }
        }

        public override string Namespace
        { get { return "NetCms.Structure.Applications.News"; } }
       
        public override string SystemName 
        { get { return "news"; } }

        public static string SystemNameKey
        { get { return "news"; } }

        public override string Label
        { get { return "News"; } }
       
        public override string LabelSingolare 
        { get { return "News"; } }
       
        public override int ID
        { get { return ApplicationID; } }
       
        public override bool IsSystemApplication { get { return false; } }

        public override NetCms.Structure.Applications.Setup Installer
        {
            get
            {
                if (_Installer == null)                  
                    _Installer = new NetCms.Structure.Applications.News.Setup(this);
                return _Installer;
            }
        }
      
        private NetCms.Structure.Applications.Setup _Installer;
        
        public NewsApplication():base(null)
        {
            base.Assembly = Assembly.GetAssembly(this.GetType());
        }
       
        public NewsApplication(Assembly assembly)
            : base(assembly)
        {
        }


        public static NetCms.CmsApp.Configuration.Configuration Configuration
        {
            get
            {
                if (_Configuration == null)
                {
                    var type = typeof(NewsApplication);
                    if (!Configs.ContainsKey(type.FullName))
                        Configs.Add(type.FullName, new NetCms.CmsApp.Configuration.Configuration(SystemNameKey));
                    _Configuration = (NetCms.CmsApp.Configuration.Configuration)Configs[type.FullName];
                }
                return _Configuration;
            }
        }
        private static NetCms.CmsApp.Configuration.Configuration _Configuration;

        /*
        protected override List<ApplicationValidation> LocalCheck()
        {
            List<ApplicationValidation> validationArray = new List<ApplicationValidation>();
            
            foreach (DataRow row in this.Installer.NetworksTable.Select(this.Installer.FilterID))
            {
                NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(row);
                if (network.CmsStatus != NetCms.Networks.NetworkCmsStates.NotCreated)
                {
                    DataTable check = Connection.SqlQuery("SHOW TABLES LIKE '" + network.SystemName + "_docs_news'");
                    if (check.Rows.Count != 1)
                        validationArray.Add(new ApplicationValidation("Tabella Documenti News nella rete: '" + network.SystemName + "'", false, "Tabella dei documenti News non presente per la rete: '" + network.SystemName + "'", ApplicationValidator.RepairTypes.Custom));
                    else validationArray.Add(new ApplicationValidation("Tabella Documenti News nella rete: '" + network.SystemName + "'", true, "", ApplicationValidator.RepairTypes.Custom));
                }
            }
            return validationArray;
        }

        protected override bool LocalRepair()
        {
            return true;
        }*/
    }
}