﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using NetCms.Structure.Applications.News;
using NHibernate;
using NetCms.WebAPI;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;

namespace NetCms.Structure.Applications.NewsExtended.API
{
    public class NewsController : AbstractBaseController
    {
        public override IEnumerable<BaseInfo> GetAll(string net)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                IList<NewsDocContent> contents = NewsBusinessLogic.FindAllApplicationRecords(sess);
                var result = from cont in contents
                             where cont.Revisione.Pubblicata && cont.Revisione.Document.NetworkSystemName == net
                             orderby cont.Revisione.Document.Data descending
                             let folder = FolderBusinessLogic.GetById(sess, cont.DocLang.Document.Folder.ID)
                             select new BaseInfo()
                             {
                                 Data = cont.DocLang.Document.Data.ToShortDateString() + " " + cont.DocLang.Document.Data.ToShortTimeString(),
                                 Titolo = cont.DocLang.Label,
                                 Descrizione = cont.DocLang.Descrizione,
                                 Link = folder.Network.Paths.AbsoluteFrontRoot +  folder.Path + "/default.aspx?news=" + cont.DocLang.Document.ID
                             };
                return result.Take(5).ToList();
            }
        }

        public override IEnumerable<BaseInfo> GetAllByFolder(string net, int id)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {               
                IList<NewsDocContent> contents = NewsBusinessLogic.GetApplicationRecordsByFolder(id, sess);

                var result = from cont in contents.OrderByDescending(x=>x.Revisione.Document.Data)
                             where cont.Revisione.Pubblicata //&& cont.DocLang.Document.Folder.ID == id
                             //let folder = FolderBusinessLogic.GetById(sess, cont.DocLang.Document.Folder.ID)
                             select new BaseInfo()
                             {
                                 Data = cont.DocLang.Document.Data.ToShortDateString() + " " + cont.DocLang.Document.Data.ToShortTimeString(),
                                 Titolo = cont.DocLang.Label,
                                 Descrizione = cont.DocLang.Descrizione,
                                 //Link = folder.Network.Paths.AbsoluteFrontRoot + folder.Path + "/default.aspx?news=" + cont.DocLang.Document.ID
                                 Link = cont.DocLang.Document.Folder.Network.Paths.AbsoluteFrontRoot + cont.DocLang.Document.Folder.Path + "/default.aspx?news=" + cont.DocLang.Document.ID
                             };
                return result.Take(5).ToList();
            }
        }
    }
}


