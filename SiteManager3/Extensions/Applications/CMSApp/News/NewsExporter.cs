﻿using System;
using System.IO;
using System.Xml;
using System.Web;
using System.Data;
using NetCms.Networks;
using NetCms.Networks.WebFS;
using NetCms.Exporter;

namespace NetCms.Structure.Applications.News
{
    public class NewsExporter:ApplicationExporter
    {
        private DataTable _DocsTable;
        public override DataTable DocsTable
        {
            get
            {
                if (_DocsTable == null)
                {
                    _DocsTable = new DataTable();                    
                    this.FolderExporter.Exporter.Conn.FillDataTable(string.Format(ApplicationExporter.Network_Docs, "INNER JOIN revisioni ON DocLang_Revisione=id_DocLang INNER JOIN news_docs_contents ON id_News = Contenuto_Revisione", this.FolderID, "",NewsApplication.ApplicationID.ToString()), _DocsTable);
                    _DocsTable.TableName = "docs";
                }
                return _DocsTable;
            }
        }
        public override DataTable[] Tables 
        {
            get
            {
                return null;
            }
        }

        public override bool HasFiles { get { return false; } }


        public NewsExporter(string folderID,FolderExporter folderExporter)
            : base(folderID, folderExporter)
        {
        }
    }
}
