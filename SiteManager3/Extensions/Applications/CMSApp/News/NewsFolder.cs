using System;
using System.IO;
using System.Data;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using NetCms.Grants;
using NetCms.Structure.Applications.Homepage;

/// <summary>
/// Summary description for NewsFolder
/// </summary>
namespace NetCms.Structure.Applications.News
{
    public partial class NewsFolder : StructureFolder
    {
        public override NetCms.Structure.Applications.Application RelativeApplication
        {
            get
            {
                if (_RelativeApplication == null)
                    _RelativeApplication = Applications.ApplicationsPool.ActiveAssemblies[NewsApplication.ApplicationID.ToString()];
                return _RelativeApplication;
            }
        }
        private NetCms.Structure.Applications.Application _RelativeApplication;

        public override bool AllowMove
        {
            get
            {
                return RelativeApplication.EnableMove;
            }
        }

        public override bool ReviewEnabled
        {
            get
            {
                return this.RelativeApplication.EnableReviews;
            }
        }

        public override ToolbarButtonsCollection ToolbarButtons
        {
            get
            {
                    ToolbarButtonsCollection _ToolbarButtons = base.ToolbarButtons;
                    if (this.Criteria[CriteriaTypes.Create].Allowed)
                        _ToolbarButtons.Add(new NetCms.Structure.Applications.ApplicationToolbarButton("news_new.aspx?folder=" + ID, NetCms.GUI.Icons.Icons.Newspaper_Add, "Nuova News", this.StructureNetwork, this.RelativeApplication));
                return _ToolbarButtons;
            }
        }
      

        public virtual bool ShowChild { get; set; }

        public NewsFolder()
            : base()
        { }       

        public NewsFolder(Networks.NetworkKey network)
            : base(network) { }

        protected override NetCms.RssManager BuildRssManager()
        {
            return new NetCms.Structure.Applications.News.RssManager("http://" + PageData.Request.Url.Authority + this.FrontendHyperlink, this.Label, this.Descrizione, this);

        }

        //Questo deve essere rimosso???
        //protected override int CreateDocContent(DataRow docRecord, DataSet sourceDB, string sourceFileName, params NetCms.Importer.Importer[] imp)
        //{
        //    string insertInto = "INSERT INTO " + this.Network.SystemName + "_docs_news ";
        //    string insertIntoFields = "";
        //    string insertIntoValues = "";

        //    foreach (DataColumn col in docRecord.Table.Columns)
        //    {
        //        if (col.ColumnName.ToLower().EndsWith("_news") && !col.ColumnName.ToLower().StartsWith("id_"))
        //        {
        //            string comma = insertIntoFields.Length > 0 ? "," : "";

        //            insertIntoFields += comma + col.ColumnName;

        //            string value = docRecord[col.ColumnName].ToString();

        //            if (value != "NULL")
        //                insertIntoValues += comma + "'" + value.Replace("'", "''") + "'";
        //            else
        //                insertIntoValues += comma + value.Replace("'", "''");
        //            //insertIntoValues += comma + "'" + value.Replace("'", "''") + "'";
        //            //break;
        //        }
        //    }

        //    insertInto += "(" + insertIntoFields + ") VALUES (" + insertIntoValues + ")";
        //    int result = this.Network.Connection.ExecuteInsert(insertInto);
        //    if (result == -1)
        //        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non � stato possibile inserire i dati della news durante l'importazione", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "116");
        //    return result;
        //}

        public override bool DeleteModuloApplicativo(NHibernate.ISession session)
        {
            return HomePageBusinessLogic.DeleteModuliApplicativiByFolder(this.ID, session);
        }
        public override bool DeleteSomething(NHibernate.ISession session)
        {
            return true;
        }
    }
}