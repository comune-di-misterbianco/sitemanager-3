﻿using NetCms.Structure.Applications.Files;
using NetCms.Structure.Applications.News.Configs;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.Applications.News
{
    public class NewsHighLigthImage : NetCms.HighLightImage.HighLightImage
    {
        public virtual string GetSrc(Presets preset)
        {
            string src = string.Empty;

            try
            {
                FilesDocument doc = DocumentBusinessLogic.GetById(base.RifDoc) as FilesDocument;
                if (doc != null)
                {
                    string strPreset = string.Empty;
                    switch (preset)
                    {
                        case Presets.Thumb:
                            strPreset = "newsthumb";
                            break;
                        case Presets.List:
                            strPreset = "newslist";
                            break;
                        case Presets.Detail:
                            strPreset = "newsdetail";
                            break;
                        case Presets.Social:
                            strPreset = "social";
                            break;
                        case Presets.None:
                            break;
                    }

                    src =doc.FrontendLink + ((Config.UseAshxHandler) ? ".ashx" : "") + (string.IsNullOrEmpty(strPreset) ? "" : "?preset=" + strPreset);
                }
            }
            catch (Exception ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", this);
            }
            return src;
        }


        public enum Presets
        {
            Thumb,
            List,
            Detail,
            Social,
            None
        }
      
    }
}
