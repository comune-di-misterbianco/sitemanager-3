﻿using System;
using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.News.Mapping
{
    public class NewsHighLigthImageMapping : ClassMap<NewsHighLigthImage>
    {
        public NewsHighLigthImageMapping() 
        {
            Table("news_highlightimages");
            Id(x => x.ID);
            Map(x => x.RifDoc);
            Map(x => x.Align);
            Map(x => x.Description);
            Map(x => x.ShowInDetail);
            Map(x => x.ShowInModulo);
            Map(x => x.ShowInTeaser);            
        }
    }
}
