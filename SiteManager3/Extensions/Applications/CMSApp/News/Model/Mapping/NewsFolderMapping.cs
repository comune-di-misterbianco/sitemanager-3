﻿using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.News.Mapping
{
    public class NewsFolderMapping : SubclassMap<NewsFolder>
    {
        public NewsFolderMapping()
        {
            Table("news_folders");
            KeyColumn("id_folders");

            Map(x => x.ShowChild).Default("true");
        }
    }
}
