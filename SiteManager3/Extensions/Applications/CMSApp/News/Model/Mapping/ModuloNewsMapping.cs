﻿using FluentNHibernate.Mapping;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetCms.Structure.Applications.Homepage.Mapping
{
    public class ModuloApplicativoNewsMapping : SubclassMap<ModuloApplicativoNews>
    {
        public ModuloApplicativoNewsMapping()
        {
            Table("homepages_moduli_applicativi_news");
            KeyColumn("Modulo_ModuloApplicativoNews");
            Map(x => x.ShowCustomNews).Column("ShowCustomNews_ModuloApplicativoNews").Default(((int)(ModuloApplicativoNews.ShowCustom.Tutte)).ToString()).Not.Nullable();
        }
    }
}