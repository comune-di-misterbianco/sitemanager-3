﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;
using System.Data;


namespace NetCms.Structure.Applications.News.Mapping
{
    [Serializable]
    public class NewsStatusMapping : IUserType
    {
        new public bool Equals(object x, object y)
       {
           return object.Equals(x, y);
       }
 
       public int GetHashCode(object x)
       {
           return x.GetHashCode();
       }
 
       public object NullSafeGet(IDataReader rs, string[] names, object owner)
       {
           object r = rs[names[0]];
           var value = (string)r;
 
           if (string.IsNullOrEmpty(value))
               throw new Exception("Invalid Status");
 
           switch (value)
           {
               case "Normale":
                   return NetCms.Structure.Applications.News.NewsDocContent.NewsStatus.Normale;
               case "In_Evidenza":
                   return NetCms.Structure.Applications.News.NewsDocContent.NewsStatus.In_Evidenza;
                
               default:
                  throw new Exception("Invalid NewsStato Type");
 
           }
       }
 
       public void NullSafeSet(IDbCommand cmd, object value, int index)
       {
           object paramVal = 0;
           switch ((NetCms.Structure.Applications.News.NewsDocContent.NewsStatus)value)
           {
               
               case NetCms.Structure.Applications.News.NewsDocContent.NewsStatus.Normale: paramVal = "Normale"; break;
               case NetCms.Structure.Applications.News.NewsDocContent.NewsStatus.In_Evidenza: paramVal = "In_Evidenza"; break;
               default:
                   throw new Exception("Invalid NewsStato Type");
           }
           var parameter = (IDataParameter)cmd.Parameters[index];
           parameter.Value = paramVal;
       }
 
       public object DeepCopy(object value)
       {
           return value;
       }
 
       public object Replace(object original, object target, object owner)
       {
           return original;
       }
 
       public object Assemble(object cached, object owner)
       {
           return cached;
       }
 
       public object Disassemble(object value)
       {
           return value;
       }
 
       public SqlType[] SqlTypes
       {
           get { return new SqlType[] { new StringSqlType() }; }
       }
 
       public Type ReturnedType
       {
           get { return typeof(NetCms.Structure.Applications.News.NewsDocContent.NewsStatus); }
       }
 
       public bool IsMutable
       {
           get { return false; }
       }
   
    }
}
