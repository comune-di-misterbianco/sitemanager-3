﻿using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.News.Mapping
{
    public class NewsDocumentMapping : SubclassMap<NewsDocument>
    {
        public NewsDocumentMapping()
        {
            Table("news_docs");
            KeyColumn("id_docs");
        }
    }
}
