﻿using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications.News.Mapping
{
    class NewsDocContentMapping : SubclassMap<NewsDocContent>
    {
        public NewsDocContentMapping()
        {
            Table("news_docs_contents");
            KeyColumn("id_News");
            Map(x => x.Testo).Column("Testo_News").CustomSqlType("mediumtext");
            Map(x => x.Stato).Column("Stato_News").CustomType<NewsStatusMapping>().Default("'" + NewsDocContent.NewsStatus.Normale.ToString() + "'").Not.Nullable();
            Map(x => x.DataPubblicazione).Column("DataPubblicazione_News");
            Map(x => x.DataScadenza).Column("DataScadenza_News");
            //Map(x => x.ImgInEvidenza).Column("ImgInEvidenza_News").Default("-1").Nullable();

            References(x => x.ImgInEvidenza, "HighlightImages_News")
              .Nullable().Cascade.All();  
        }
    }
}
