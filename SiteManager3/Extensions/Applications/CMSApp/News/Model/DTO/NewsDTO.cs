﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.Applications.News
{
    public class NewsDTO
    {
        public NewsDTO()
        {

        }

        public int ID { get; set; }
        public string Titolo { get; set; }
        public string Descrizione { get; set; }
        public DateTime Data { get; set; }
        public string Content { get; set; }

        public string Categoria { get; set; }
        public string Folderpath { get; set; }
             
        public NewsImageDTO Imgevidenza { get; set; }

    }
}
