﻿using NetCms.Structure.Applications.Files;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.Applications.News
{
    public class NewsImageDTO
    {
        public NewsImageDTO() { }

        public int Iddoc { get; set; }
        //public string Src { get; set; }
        public string Description { get; set; }
        public string Cssclass { get; set; }
        public bool Show { get; set; }
        public string Src { get; set; }

    }
}
