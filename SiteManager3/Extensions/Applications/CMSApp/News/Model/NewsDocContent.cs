﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Structure.WebFS;
using System.Data;
using NHibernate;
using NetCms.Networks;
using NetCms.Structure.Search.Models;

namespace NetCms.Structure.Applications.News
{
    public class NewsDocContent : DocContent
    {
        public NewsDocContent()
            : base()
        {

        }

        public virtual string Testo
        {
            get;
            set;
        }


        public virtual NewsStatus Stato
        {
            get;
            set;
        }

        public virtual DateTime? DataPubblicazione
        {
            get;
            set;
        }

        public virtual DateTime? DataScadenza
        {
            get;
            set;
        }

        //public virtual int ImgInEvidenza
        //{
        //    get;
        //    set;
        //}

        public virtual NewsHighLigthImage ImgInEvidenza
        {
            get;
            set;
        }

        public enum NewsStatus 
        {
            Normale,
            In_Evidenza,
            Archiviata
        }

        public override DocContent ImportDocContent(DataRow docRecord, ISession session, DataSet sourceDB, NetworkKey netKey, string sourceFolderPath, ref Document newDoc)
        {
            string filteredContent = NetCms.Structure.Utility.ImportContentFilter.ParseImportedContentUrls(docRecord["Testo_News"].ToString());
            Testo = filteredContent;
            return this;
        }

        public override DocContent ImportDocContent(Dictionary<string,string> docRecord, ISession session)
        {
            if (docRecord.ContainsKey("Testo"))
                this.Testo = docRecord["Testo"].ToString();
            this.Stato = NewsStatus.Normale;            
            return this;

        }

        public override bool EnableIndexContent
        {
            get
            {
                return true;
            }
        }

        public override CmsConfigs.Model.IndexDocContent GetContentToIndex(ISession session)
        {
            string frontUrl = (string.IsNullOrEmpty(this.DocLang.Document.Network.Paths.AbsoluteFrontRoot) ? "" : this.DocLang.Document.Network.Paths.AbsoluteFrontRoot)
                            + this.DocLang.Document.Folder.Path
                            + "/default.aspx?news=" + this.ID;

            CmsConfigs.Model.IndexDocContent indexContent = new CmsConfigs.Model.IndexDocContent();

            indexContent.ID = this.ID;
            indexContent.Fields.Add(CmsDocument.Fields.document_title.ToString(), this.DocLang.Label);
            indexContent.Fields.Add(CmsDocument.Fields.document_description.ToString(), this.DocLang.Descrizione);
            indexContent.Fields.Add(CmsDocument.Fields.document_content.ToString(), this.Testo);
            indexContent.Fields.Add(CmsDocument.Fields.document_fronturl.ToString(), frontUrl);
            indexContent.Fields.Add(CmsDocument.Fields.document_backurl.ToString(), this.DocLang.Document.SiteManagerLink);
            indexContent.Fields.Add(CmsDocument.Fields.document_type.ToString(), this.DocLang.Document.Application.ToString());

            return indexContent;

        }

    }

    
}
