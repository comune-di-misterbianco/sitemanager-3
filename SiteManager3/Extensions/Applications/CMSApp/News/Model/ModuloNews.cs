﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Web.UI.HtmlControls;
//using System.Web.UI.WebControls;
//using NetCms.Structure.Applications.Homepage.Model;
//using NetCms.Structure.Applications.News;
//using NetCms.Structure.WebFS;
//using NetForms;

//namespace NetCms.Structure.Applications.NewsExtended.Model
//{
//    [NetCms.Homepages.HomepageModuleDefiner(NewsApplication.ApplicationID, "News", NewsApplication.ApplicationID, NetCms.Homepages.HomepageModuleDefiner.ModuleType.CmsApp)]
//    public class ModuloNews : ModuloApplicativo
//    {

//        //public virtual ModuloApplicativo ModuloApplicativoRef { get; set; }
//        public virtual ShowCustom ShowCustomNews { get; set; }

//        #region Constructors
//        public ModuloNews() : base() { }        
//        //public ModuloNews(Homepage.Model.Modulo modulo)
                
//        //{
//        //    try
//        //    {
//        //        if (modulo != null)
//        //        {
//        //            ModuloNews moduloNews = 
//        //                NetCms.Structure.Applications.Homepage.HomepageModulesBusinessLogic
//        //                <ModuloNews>.GetModuloById(modulo.ID);

//        //            ShowCustomNews = moduloNews.ShowCustomNews;
//        //            //ModuloError = false;
//        //        }
//        //        else
//        //        {
//        //            //ModuloError = true;
//        //            throw new Exception("La query di selezione del modulo applicativo di tipo News dell'homepage non ha prodotto alcun risultato");
//        //        }
//        //    }
//        //    catch (Exception ex)
//        //    {
//        //        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "117");
//        //    }
//        //}
//        #endregion

//        public enum ShowCustom
//        {
//            [Description("Tutte le news")]
//            Tutte,
//            [Description("Solo le news in evidenza")]
//            InEvidenza,
//            [Description("Solo le news non in evidenza")]
//            NonInEvidenza
//        }

//        #region ModuloApplicativoRef
//        /// Required for Compositekeys
//        /// 
//        //public override bool Equals(object obj)
//        //{
//        //    var other = obj as ModuloNews;
//        //    return (other != null) && (ModuloApplicativoRef.ID == other.ModuloApplicativoRef.ID);
//        //}

//        /// Required for Compositekeys
//        /// 
//        //public override int GetHashCode()
//        //{
//        //    return ModuloApplicativoRef.ID.GetHashCode();
//        //}
//        #endregion
//    }

//    //public class ModuloNewsForm : Homepage.HomepageModuleForm
//    //{
//    //    public ModuloNewsForm(int id)
//    //        : base(id)
//    //    {
//    //    }
//    //    public ModuloNewsForm()
//    //        : base()
//    //    {
//    //    }
//    //    public override NetForms.NetFormTable GetFormTable()
//    //    {
//    //        NetFormTable modulo;
//    //        if (ID != 0)
//    //        {
//    //            int id_modulo_applicativo = 0;
//    //            DataTable table = 
//    //                this.Conn.SqlQuery
//    //                (
//    //                    "SELECT * " +
//    //                    "FROM homepages_moduli_applicativi " +
//    //                    "INNER JOIN homepages_moduli_news " +
//    //                    "ON homepages_moduli_applicativi.Modulo_ModuloApplicativo = homepages_moduli_news.Modulo_news " +
//    //                    "WHERE homepages_moduli_applicativi.Modulo_ModuloApplicativo = "
//    //                    + ID
//    //                )
//    //                ;
//    //            try
//    //            {
//    //                if 
//    //                (
//    //                    table.Rows.Count > 0 
//    //                    && 
//    //                    int.TryParse(table.Rows[0]["Modulo_ModuloApplicativo"].ToString(), out id_modulo_applicativo)
//    //                ) ;
//    //                else 
//    //                    throw new Exception("Errore tabella vuota oppure conversione Modulo_ModuloApplicativo errata nella classe Modulo di News");
//    //            }
//    //            catch (Exception ex)
//    //            {
//    //                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "118");
//    //            }
//    //            modulo = new NetFormTable
//    //                ("homepages_moduli_applicativi", "Modulo_ModuloApplicativo", this.Conn, id_modulo_applicativo);
//    //        }
//    //        else
//    //        {
//    //            modulo = new NetFormTable("homepages_moduli_applicativi", "Modulo_ModuloApplicativo", this.Conn);
//    //            NetExternalField mod = new NetExternalField("homepages_moduli", "Modulo_ModuloApplicativo");
//    //            modulo.addExternalField(mod);
//    //        }

//    //        NetCheckBox showmorelink = new NetCheckBox("Mostra link 'Mostra tutti'", "ShowMoreLink_ModuloApplicativo");
//    //        modulo.addField(showmorelink);

//    //        NetCheckBox showdesc = new NetCheckBox("Mostra descrizione", "ShowDesc_ModuloApplicativo");
//    //        modulo.addField(showdesc);

//    //        NetTextBox desclen = new NetTextBox("Lunghezza Descrizione", "DescLength_ModuloApplicativo");
//    //        desclen.Numeric = true;
//    //        desclen.Required = true;
//    //        modulo.addField(desclen);

//    //        NetCheckBox hideImage = new NetCheckBox("Non esporre le immagini", "HideImage_ModuloApplicativo");
//    //        modulo.addField(hideImage);

//    //        NetDropDownList records = new NetDropDownList("Numero di record da visualizzare", "Records_ModuloApplicativo");
//    //        records.Required = true;
//    //        for (int i = 1; i <= 15; i++)
//    //            records.addItem(i.ToString(), i.ToString());
//    //        modulo.addField(records);

//    //        NetDropDownList showdepth = new NetDropDownList("Profondità di visualizzazione", "Subfolders_ModuloApplicativo");
//    //        showdepth.Required = true;
//    //        showdepth.addItem("Mostra solo news di questa cartella", "0");
//    //        showdepth.addItem("Mostra news di questa cartella e delle sue sottocartelle", "1");
//    //        modulo.addField(showdepth);

//    //        NetDropDownList showItems = new NetDropDownList
//    //            ("Mostra", nameof(ModuloNews.ShowCustomNews));
//    //        showItems.Required = false;
//    //        foreach (ModuloNews.ShowCustom enumKey in (ModuloNews.ShowCustom[])Enum.GetValues(typeof(ModuloNews.ShowCustom)))
//    //            showItems.addItem(SharedUtilities.EnumUtilities.GetEnumDescription(enumKey), enumKey.ToString());
//    //        modulo.addField(showItems);

//    //        if (ID == 0)
//    //        {
//    //            StructureFolderBinder binder = new StructureFolderBinder(PageData.CurrentReference);
//    //            TreeNode root = binder.TreeFromStructure((StructureFolder)NetCms.Networks.NetworksManager.CurrentActiveNetwork.RootFolder, NewsApplication.ApplicationID);
//    //            NetTreeView folders = new NetTreeView("Seleziona una cartella", "Folder_ModuloApplicativo", root);
//    //            folders.NotAllowedMsg = "Attenzione! La cartella selezionata non è di tipo news o non si possiedono i permessi di accesso";
//    //            folders.RootDist = Configurations.Paths.AbsoluteRoot;
//    //            folders.CheckAllows = true;
//    //            folders.Required = true;
//    //            modulo.addField(folders);
//    //        }

//    //        return modulo;
//    //    }
//    //    public override TemplateOptionEnum TemplateOption
//    //    {
//    //        get { return TemplateOptionEnum.ExendedTemplate; }
//    //    }
//    //}
//}
