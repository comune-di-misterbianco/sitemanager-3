﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCms.Structure.Applications.Homepage.Model
{
    public class ModuloApplicativoNews : ModuloApplicativo
    {
        public ModuloApplicativoNews() : base() { }
        public virtual int ShowCustomNews { get; set; }
        public enum ShowCustom
        {
            [Description("Tutte le news")]
            Tutte = 1,
            [Description("Solo le news in evidenza")]
            InEvidenza = 2,
            [Description("Solo le news non in evidenza")]
            NonInEvidenza = 3
        }
    }
}
