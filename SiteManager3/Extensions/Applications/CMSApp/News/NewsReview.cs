using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NHibernate;
using System.Collections.Generic;
using NetService.Utility.ValidatedFields;
using System.Linq;

namespace NetCms.Structure.Applications.News
{   
    public class NewsReview : NetCms.Structure.WebFS.DocumentReviewPage
    {
        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Newspaper_Add; }
        }
        public override string PageTitle
        {
            get { return "Nuova Revisione News '" + this.CurrentDocument.Nome + "'"; }
        }

        private NewsDocContent currentNewsDocContent;

        public NewsReview()
        {
            if (ReviewID > 0)
            {
                //torna null - non riesce a fare il cast
                currentNewsDocContent = NewsBusinessLogic.GetById(this.ContentID);;
            }            
        }

        protected override WebControl BuildControls()
        {           
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "container-fluid";

            NetExternalField content;
            NetFormTable contentTable = NewsDocument.DocsContentFormTable(PageData);
            contentTable.CssClass = "netformtable_contenttable";

            NetFormTable news;

            if (ReviewID == 0)
                news = new NetFormTable("news_docs_contents", "id_News", this.CurrentFolder.StructureNetwork.Connection);
            else
                news = new NetFormTable("news_docs_contents", "id_News", this.CurrentFolder.StructureNetwork.Connection, this.ContentID);

            news.CssClass = "news_contentfield";

            #region Campi

            if (News.Configs.Config.DataPubblicazione)
            {
                NetDateField2 dataPubblicazione = new NetDateField2("Data pubblicazione", "DataPubblicazione_News", this.CurrentNetwork.Connection);             
                dataPubblicazione.Required = false;
                news.addField(dataPubblicazione);
            }

            if (News.Configs.Config.DataScadenza)
            {
                NetDateField2 dataScadenza = new NetDateField2("Data scadenza e archiviazione", "DataScadenza_News", this.CurrentNetwork.Connection);             
                dataScadenza.Required = false;
                news.addField(dataScadenza);
            }

            NetDropDownList Stato = new NetDropDownList("VisibilitÓ", "Stato_News");
            Stato.addItem(NewsDocContent.NewsStatus.Normale.ToString(), NewsDocContent.NewsStatus.Normale.ToString());
            Stato.addItem(NewsDocContent.NewsStatus.In_Evidenza.ToString().Replace("_", " "), NewsDocContent.NewsStatus.In_Evidenza.ToString());
            Stato.Required = true;
            news.addField(Stato);

            RichTextBox Testo = new RichTextBox("Testo", "Testo_News");
            Testo.Required = true;
            Testo.CurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID;
            news.addField(Testo);

            #endregion

            AttachesGui attachControl = new AttachesGui(
              NewsApplication.ApplicationID,
              this.ContentID,
              PageData.CurrentConfig.AttachedEnabledExtensions
          );

            #region NewsImageInEvidenza

            NetFormTable newsImageInEvidenza;
            if (currentNewsDocContent == null || currentNewsDocContent.ImgInEvidenza == null)
                newsImageInEvidenza = new NetFormTable("news_highlightimages", "ID", this.CurrentFolder.StructureNetwork.Connection);
            else
                newsImageInEvidenza = new NetFormTable("news_highlightimages", "ID", this.CurrentFolder.StructureNetwork.Connection, currentNewsDocContent.ImgInEvidenza.ID);

            newsImageInEvidenza.CssClass = "news_highlightimages news_revform";
            newsImageInEvidenza.InfoText = "Immagine in evidenza";

            NetImagePicker docImg = new NetImagePicker("Immagine", "RifDoc", this.PageData);
            docImg.CurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID;
            newsImageInEvidenza.addField(docImg);

            NetTextBoxPlus descNewsImage = new NetTextBoxPlus("Descrizione", "Description", NetTextBoxPlus.ContentTypes.NormalText);
            descNewsImage.CssClass = "description";
            descNewsImage.DefaultControlTag = "div";
            descNewsImage.Rows = 3;
            descNewsImage.Size = 40;
            newsImageInEvidenza.addField(descNewsImage);

            NetDropDownList imageAlign = new NetDropDownList("Allineamento", "Align");
            imageAlign.DefaultControlTag = "div";
            imageAlign.CssClass = "imageAlign";
            imageAlign.addItem("Sinistra", "left");
            imageAlign.addItem("Centrato", "center");
            imageAlign.addItem("Destra", "right");
            newsImageInEvidenza.addField(imageAlign);

            NetCheckBox showinModulo = new NetCheckBox("Mostra nel modulo", "ShowInModulo");
            newsImageInEvidenza.addField(showinModulo);

            NetCheckBox showinList = new NetCheckBox("Mostra nell'elenco", "ShowInTeaser");
            newsImageInEvidenza.addField(showinList);

            NetCheckBox showinDetail = new NetCheckBox("Mostra nel dettaglio", "ShowInDetail");
            newsImageInEvidenza.addField(showinDetail); 

            #endregion


            NetForm form = new NetForm();
            form.CssClass = "newsform";
            
            form.AddTable(contentTable);
            form.AddTable(news);
            form.AddTable(newsImageInEvidenza);
            form.SubmitLabel = "Salva Revisione";

            content = new NetExternalField("docs_contents", "id_News");
            news.ExternalFields.Add(content);

            #region PostBack

            string errors = "";
            if (PageData.IsPostBack)
            {
                if (PageData.SubmitSource == "send")
                {                    
                    using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                    using (ITransaction tx = sess.BeginTransaction())
                    {
                        try
                        {
                            errors = form.serializedInsert(HttpContext.Current.Request.Form);
                            if (form.LastOperation == NetForm.LastOperationValues.OK)
                            {
                                NewsDocContent newsDocContent = NewsBusinessLogic.GetById(contentTable.LastInsert, sess);
                                DocumentLang doc_Lang = DocumentBusinessLogic.GetDocLangById(DocLang, sess);

                                WebFS.Reviews.Review newsReview = DocumentOverview.insertReview(
                                   doc_Lang,
                                   newsDocContent, 
                                   this.ContentID, 
                                   PageData, 
                                   sess);

                                NewsDocContent prevDocContent = NewsBusinessLogic.GetById(this.Review.Content.ID, sess);

                                List<string> attachesToKeep = new List<string>();
                                if (PageData.CurrentConfig.CmsAppAttachToContentStatus == CmsConfigs.Model.Config.AttachToContentStatus.Enabled)
                                {
                                    IList<FileUploadedV2> fileList = (IList<FileUploadedV2>)attachControl.UploadField.PostbackValueObject;

                                    NetUtility.RequestVariable allegatiAttiviStr = new NetUtility.RequestVariable("uploadedfiles_" + attachControl.UploadField.Key, NetUtility.RequestVariable.RequestType.Form);
                                    attachesToKeep = allegatiAttiviStr.StringValue.Split(';').ToList();
                                }

                                NewsBusinessLogic.SaveOrUpdateNewsDocsContent(newsDocContent,sess);
                                
                               if (newsImageInEvidenza.LastInsert > 0)
                               {
                                   NewsHighLigthImage highLightImg = NewsBusinessLogic.GetHighLigthImageByID(newsImageInEvidenza.LastInsert, sess);
                                   if (highLightImg != null)                                   
                                       // associo l'immagine al nuovo content
                                       NewsBusinessLogic.SaveHighLightImageContent(newsReview.Content.ID, highLightImg, sess);                                   
                               }
                               else
                               { 
                                   // businessLogic RimuoviImmagine da news content
                                   NewsBusinessLogic.RemoveHighLightImageContent(newsReview.Content.ID, sess);
                               }

                                if (attachesToKeep.Any())
                                {
                                    // aggiunta di nuovi allegati
                                    Files.AttachesBusinessLogic.SaveAttaches(this.CurrentDocument.ID,
                                                                            newsReview.Content.ID,
                                                                            prevDocContent,
                                                                            attachControl.UploadField,
                                                                            attachesToKeep,
                                                                            this.CurrentNetwork.ID,
                                                                            this.CurrentDocument.Autore,

                                                                            sess);
                                }

                               string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                               NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha salvato una nuova revisione della news'" + this.CurrentDocument.Nome + "' relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName, callerObject: this);
                               tx.Commit();
                            }                            

                            if (form.LastOperation == NetForm.LastOperationValues.Error)
                                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante la revisione della news " + this.CurrentDocument.Nome + " del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "548");                            
                        }
                        catch (Exception ex)
                        {
                            tx.Rollback();
                            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                        }
                    }

                    if (form.LastOperation == NetForm.LastOperationValues.Error)
                        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante la revisione della news " + this.CurrentDocument.Nome + " del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "", "548");

                    NetCms.GUI.PopupBox.AddSessionMessage(errors, NetCms.GUI.PostBackMessagesType.Normal, form.LastOperation == NetForm.LastOperationValues.OK ? true : false);
                }
            }
            else
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di creazione di una nuova revisione della news '" + this.CurrentDocument.Nome + "' relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName, callerObject: this);
            }
            #endregion
        
            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");

            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerText = "Nuova revisione";
            fieldset.Controls.Add(legend);

            HtmlGenericControl contentDiv = new HtmlGenericControl("div");
            contentDiv.Attributes["class"] = "contentForm newsform reviewform col-md-9";
            contentDiv.Controls.Add(contentTable.getForm());
            contentDiv.Controls.Add(news.getForm());

            if (PageData.CurrentConfig.CmsAppAttachToContentStatus == CmsConfigs.Model.Config.AttachToContentStatus.Enabled)
                contentDiv.Controls.Add(attachControl.GetControl());

            HtmlGenericControl colDxDiv = new HtmlGenericControl("div");
            colDxDiv.Attributes["class"] = "coldx col-md-3 border-left";
            colDxDiv.Controls.Add(newsImageInEvidenza.getForm());

            HtmlGenericControl pButtons = new HtmlGenericControl("p");
            pButtons.Attributes["class"] = "buttonscontainer";

            Button bt = new Button();
            bt.Text = "Salva Revisione";
            bt.ID = "send";
            bt.CssClass = "button";
            bt.OnClientClick = "javascript: setSubmitSource('send')";
            pButtons.Controls.Add(bt);

            contentDiv.Controls.Add(pButtons);

            WebControl row = new WebControl(HtmlTextWriterTag.Div);
            row.CssClass = "row";

            row.Controls.Add(contentDiv);
            row.Controls.Add(colDxDiv);

            fieldset.Controls.Add(row);
            
            div.Controls.Add(fieldset);

            return div;
        }

    }
}
