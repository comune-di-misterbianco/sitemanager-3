﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using NetCms.Structure.Applications.News;
using System.Web.UI;

namespace NetFrontend
{
    public class NewsSearchForm : VfManager
    {
        private LabelsManager.Labels _NewsLabels;
        private LabelsManager.Labels NLabels
        {
            get
            {
                if (_NewsLabels == null)
                    _NewsLabels = LabelsManager.LabelsCache.GetTypedLabels(typeof(NewsLabels)) as LabelsManager.Labels;
                return _NewsLabels;
            }
        }


        #region campi form ricerca

        private VfDropDown _VfTipo;
        public VfDropDown VfTipo
        {
            get
            {
                if (_VfTipo == null)
                {
                    _VfTipo = new VfDropDown("Search_DateOptions", "");
                    
                    _VfTipo.Options.Add(NLabels[NewsLabels.NewsLabelsList.SuccessiveAl], "0");
                    _VfTipo.Options.Add(NLabels[NewsLabels.NewsLabelsList.AntecedentiAl], "1");
                    _VfTipo.Options.Add(NLabels[NewsLabels.NewsLabelsList.DataEsatta], "2");
                    _VfTipo.DefaultValue = 0;
                    _VfTipo.Required = false;
                    _VfTipo.ColorOnValidationOK = false;
                    _VfTipo.DropDownList.CssClass = "form-control";
                    
                }
                return _VfTipo;
            }
        }

        private VfTextBox _VfTesto;
        public VfTextBox VfTesto
        {
            get
            {
                if (_VfTesto == null)
                {
                    _VfTesto = new VfTextBox("Testo", NLabels[NewsLabels.NewsLabelsList.Testo], "Testo", InputTypes.Text);
                    _VfTesto.Required = false;
                    _VfTesto.ColorOnValidationOK = false;
                   
                }
                return _VfTesto;
            }
        }

        private VfTextBox _VfTitolo;
        public VfTextBox VfTitolo
        {
            get
            {
                if (_VfTitolo == null)
                {
                    _VfTitolo = new VfTextBox("Label", NLabels[NewsLabels.NewsLabelsList.Titolo], "Label", InputTypes.Text);
                    _VfTitolo.Required = false;
                    _VfTitolo.ColorOnValidationOK = false;
                    _VfTitolo.SearchComparisonCriteria = NetService.Utility.RecordsFinder.Finder.ComparisonCriteria.Like;
                }
                return _VfTitolo;
            }
        }

        private VfTextBox _VfDescrizione;
        public VfTextBox VfDescrizione
        {
            get
            {
                if (_VfDescrizione == null)
                {
                    _VfDescrizione = new VfTextBox("Descrizione", NLabels[NewsLabels.NewsLabelsList.Descrizione], "Descrizione", InputTypes.Text);
                    _VfDescrizione.Required = false;
                    _VfDescrizione.ColorOnValidationOK = false;
                    _VfDescrizione.SearchComparisonCriteria = NetService.Utility.RecordsFinder.Finder.ComparisonCriteria.Like;
                }
                return _VfDescrizione;
            }
        }

        private VfDateCalendar _VfData;
        public VfDateCalendar VfData
        {
            get
            {
                if (_VfData == null)
                {
                    _VfData = new VfDateCalendar("Data", NLabels[NewsLabels.NewsLabelsList.Data], "Data",true);
                    _VfData.Required = false;
                    _VfData.AddAccessoryInputGroup = true; 
                    _VfData.ColorOnValidationOK = false;
                }
                return _VfData;
            }
        }
        #endregion

        public NewsSearchForm(string controlID, bool isPostBack)
            : base(controlID, isPostBack,VfManagerRenderType.Frontend)
        {            
            this.ShowMandatoryInfo = false;
            this.AutoValidation = false;
            this.CssClass = "Axf";

            this.SubmitButton.Text = NLabels[NewsLabels.NewsLabelsList.CercaNews];
            
            this.AddBackButton = true;
            this.BackButton.Text = "Reset";            

            this.Fields.Add(VfTitolo);            
            this.Fields.Add(VfDescrizione);
            this.Fields.Add(VfData);
            this.Fields.Add(VfTipo);      
        }



    }
}
