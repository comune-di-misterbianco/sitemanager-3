﻿using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using NetCms;


namespace NetCms.Structure.Applications.News
{
    public class NewsLabels : LabelsManager.Labels
    {
       public const String ApplicationKey = "news";

        public NewsLabels()
            : base(ApplicationKey)
        {
        }

        public enum NewsLabelsList
        {
            MostraNewsSezione,
            MostraNewsSottosezioni,
            NuovaRicerca,
            AzzeraRicerca,
            NewsContenentitesto,
            SuccessivaAl,
            AntecedenteAl,
            Esatta,
            NewsConData,
            CriteriDiRicerca,
            CercaNews,
            RicercaNews,
            ScegliModalita,
            AntecedentiAl,
            DataEsatta,
            SuccessiveAl,
            TitoloOTestoContenuto,
            DescrizioneRicerca,
            LabelDataFormato,
            LabelSearchButton,
            Data,
            Titolo,
            Descrizione,
            Testo,
            ShowAllModuleBTN,
            SearchFormTitle,
            DateFormatPlaceholder,
        }
    }
}