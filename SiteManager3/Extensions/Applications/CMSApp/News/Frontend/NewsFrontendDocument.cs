using System.Data;
using NetFrontend;
using NetCms.Structure.WebFS;


/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Structure.Applications.News
{
    [FrontendDocumentsDefiner(3, "News")]
    public class NewsFrontendDocument : FrontendDocument
    {
        public override string FrontendUrl
        {
            get { return this.Folder.FrontendUrl + "default.aspx?news=" + this.ID; }
        }

        public NewsFrontendDocument(Document row, FrontendFolder folder) : base(row, folder) { }
        public NewsFrontendDocument(int docID, FrontendFolder folder) : base(docID, folder) { }
    }
}