﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NetFrontend.DAL
{
    public class NewsDAL
    {
        public static DataRow GetDocumentRecord(int ID)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(NetCms.Structure.Applications.NewsExtended.Frontend.SqlQueries.BaseNewsDocumentsSQL, "id_Doc = " + ID, 0, 1);
            return rows.Length > 0 ? rows.First() : null;
        }

        /// <summary>
        /// Restituisce il documento corrispondente ai paramentri richiesti.
        /// La funzione restituisce un oggetto DataRow solo se la query da 1 solo risultato.
        /// </summary>
        /// <param name="sql">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <returns></returns>
        public static DataRow GetDocumentRecord(string sql)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(NetCms.Structure.Applications.NewsExtended.Frontend.SqlQueries.BaseNewsDocumentsSQL, sql, 0, 1);
            return rows.Length > 0 ? rows.First() : null;
        }

        /// <summary>
        /// Restituisce il documento corrispondente ai paramentri richiesti.
        /// La funzione restituisce un oggetto DataRow solo se la query da 1 solo risultato.
        /// </summary>
        /// <param name="sql">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <returns></returns>
        public static DataRow GetDocumentPreviewRecord(string sql)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(NetCms.Structure.Applications.NewsExtended.Frontend.SqlQueries.BaseNewsDocumentsPreviewSQL, sql, 0, 1);
            return rows.Length > 0 ? rows.First() : null;
        }

        /// <summary>
        /// Restituisce un set di documenti corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <param name="page">Un intero che rappresenta la pagina di database da restituire in caso di paginazione. La prima pagina è rappresentata dallo 0</param>
        /// <param name="recordCount">Un intero che rappresenta la il numero di record massimo da restituire per ogni pagina</param>
        /// <returns></returns>
        public static DataRow[] ListDocumentRecords(string sqlConditions, int page = 0, int recordCount = 0, string order = null)
        {
            return GenericDAL.ListRecordsBySqlConditions(NetCms.Structure.Applications.NewsExtended.Frontend.SqlQueries.BaseNewsDocumentsSQL, sqlConditions, page, recordCount, order);
        }

        /// <summary>
        /// Restituisce il numero di record corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <returns></returns>
        public static int CountDocumentRecords(string sqlConditions)
        {
            return GenericDAL.CountRecordsBySqlConditions(NetCms.Structure.Applications.NewsExtended.Frontend.SqlQueries.BaseNewsDocumentsSQL, sqlConditions);
        }
    }
}
