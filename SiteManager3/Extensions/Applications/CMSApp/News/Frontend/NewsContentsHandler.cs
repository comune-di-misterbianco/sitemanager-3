﻿using System;
using System.Xml;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Applications.News;
using NetService.Utility.RecordsFinder;
using System.Collections.Generic;
using System.Linq;
using NetService.Utility.Controls;
using NetService.Utility.ArTable;
using NetService.Utility.ValidatedFields;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.Applications.Files;
using LabelsManager;
using System.Web.UI.WebControls;
using System.IO;
using NetCms.Diagnostics;
using System.Text.RegularExpressions;
using SharedUtilities;
using AutoMapper;


//namespace NetCms.Structure.Applications.News
namespace NetFrontend
{
    public class NewsContentsHandler : ApplicationContentsHandler
    {     
        private NewsDocument DocRecord;
        private FrontendFolder currentFolder;

        protected override string ApplicationCSSClass
        {
            get { return "News"; }
        }
        protected override string QueryStringKey
        {
            get { return "news"; }
        }

        private string _FrontType;
        private string FrontType
        {
            get
            {
                if (_FrontType == null)
                {
                    _FrontType = "table";
                    XmlNode nodeConf = null;
                    if (FrontConfigs != null && FrontConfigs.ChildNodes.Count > 0)
                    {
                        nodeConf = FrontConfigs.SelectSingleNode("frontend");
                        if (nodeConf != null && nodeConf.Attributes["type"] != null)
                            _FrontType = nodeConf.Attributes["type"].Value;
                    }
                }
                return _FrontType;
            }
        }

        private string _ContentToShow;
        private string ContentToShow
        {
            get
            {
                if (_ContentToShow == null)
                {
                    _ContentToShow = "current";
                    XmlNode nodeConf = null;
                    if (FrontConfigs != null && FrontConfigs.ChildNodes.Count > 0)
                    {
                        nodeConf = FrontConfigs.SelectSingleNode("frontend");
                        if (nodeConf != null && nodeConf.Attributes["contentsshow"] != null)
                            _ContentToShow = nodeConf.Attributes["contentsshow"].Value;
                    }
                }
                return _ContentToShow;
            }
        }

        private IDictionary<string, bool> _ReportOrder;
        private IDictionary<string, bool> ReportOrder
        {
            get
            {
                if (_ReportOrder == null)
                {

                    _ReportOrder = new Dictionary<string, bool>();

                    XmlNode nodeConf = null;
                    if (FrontConfigs != null && FrontConfigs.ChildNodes.Count > 0)
                    {
                        nodeConf = FrontConfigs.SelectSingleNode("frontend");
                        if (nodeConf != null && nodeConf.Attributes["reportorder"] != null)
                        {
                            string reportorder = nodeConf.Attributes["reportorder"].Value;

                            string[] arrayReporToOrder = reportorder.Split(';');

                            foreach (string subOrder in arrayReporToOrder)
                            {
                                string[] subArrayOrder = subOrder.Replace("{", "").Replace("}", "").Split(',');
                                string key = subArrayOrder[0].ToString();
                                bool desc = (subArrayOrder[1].ToString().ToLower() == "desc") ? false : true;
                                _ReportOrder.Add(key, desc);
                            }
                        }
                    }
                }
                return _ReportOrder;
            }
        }

        private XmlNode _FrontConfigs;
        private XmlNode FrontConfigs
        {
            get
            {
                if (_FrontConfigs == null)
                {

                    _FrontConfigs = this.PageData.XmlConfigs.SelectSingleNode("/Portal/configs/Networks/" + this.PageData.Network.SystemName + "/applications/news");
                }
                return _FrontConfigs;
            }
        }

        private Labels _NewsLabels;
        private Labels NLabels
        {
            get
            {
                if (_NewsLabels == null)
                {
                    _NewsLabels = LabelsCache.GetTypedLabels(typeof(NewsLabels)) as Labels;
                }
                return _NewsLabels;
            }
        }

        private PageData _PageData;
        private string paramArchivio = "show=archivio";

        private bool _ShowAsArchive = false;
        private bool ShowAsArchive
        {
            get
            {
                NetUtility.RequestVariable archivioReq = new NetUtility.RequestVariable("show", NetUtility.RequestVariable.RequestType.QueryString);
                    if (archivioReq.IsValidString && archivioReq.StringValue == "archivio")
                        _ShowAsArchive = true;                                
                return _ShowAsArchive;
            }
        }

        public NewsContentsHandler(PageData pagedata)
            : base(pagedata)
        {
            _PageData = pagedata;
            currentFolder = PageData.Folder;
          
            //ApplicationContext.SetCurrentComponent(NewsApplication.ApplicationContextFrontLabel);
            
            if (this.HasCurrentDocument)
                //DocRecord = NetFrontend.DAL.NewsDAL.GetDocumentRecord(CurrentDocumentID);
                //if (PageData.Network.HasCurrentDocument)
                DocRecord = NewsBusinessLogic.GetDocumentRecord(CurrentDocumentID);
            SearchForm.BackButton.Click += new EventHandler(BackButton_Click);
            _NewsLabels = LabelsManager.LabelsCache.GetTypedLabels(typeof(NewsLabels)) as LabelsManager.Labels;
        }

        private NewsFolder newsFolder;
        private NewsFolder NewsFolder
        {
            get
            {
                if (newsFolder == null)
                {
                    newsFolder = NewsBusinessLogic.GetNewsFolder(PageData.Folder.ID);                 
                }
                return newsFolder;
            }
        }
        public void BackButton_Click(object sender, EventArgs e)
        {
            NetCms.PageData.Redirect(PageData.Request.Path);
        }

        public override Control Control
        {
            get
            {
                //return (this.CurrentDocumentID > 0) ? GetDetailView() : GetListView();
                return (this.CurrentDocumentID > 0) ?
                    ((this.FrontType == "template") ? GetNewsDetail() : GetDetailView()) : 
                    ((this.FrontType == "template") ? GetNewsList() : GetListView());
            }
        }
   
        public override Control PreviewControl
        {
            get
            {
                DocRecord = NewsBusinessLogic.GetDocumentPreviewRecord(this.ContentID, this.PageData.Folder.ID);
                if (DocRecord != null)
                    return GetDetailPreview();                   
                throw new System.Web.HttpException(404, "File Not Found");
            }
        }

        public HtmlGenericControl GetListView()
        {
            
            HtmlGenericControl page = new HtmlGenericControl("div");
            page.Attributes["class"] = "row" + ApplicationCSSClass + CssClass;

            HtmlGenericControl generale = new HtmlGenericControl("div");
            generale.Attributes["class"] = "col-md-12";

            #region Title

            HtmlGenericControl pageTitle = new HtmlGenericControl("div");
            pageTitle.Attributes["class"] = "row";

            HtmlGenericControl colTitle = new HtmlGenericControl("div");
            colTitle.Attributes["class"] = "col-md-12";

            HtmlGenericControl pageheader = new HtmlGenericControl("div");
            pageheader.Attributes["class"] = "page-header";

            HtmlGenericControl h1 = new HtmlGenericControl("h1");
            h1.InnerHtml = (!ShowAsArchive) ? PageData.Folder.Label : "Archivio " + PageData.Folder.Label;
            pageheader.Controls.Add(h1);

            colTitle.Controls.Add(pageheader);


            StructureFolder currfolder = FolderBusinessLogic.GetById(this.PageData.Folder.ID);
            if (currfolder.HomeType == -2)
            {
                HtmlGenericControl desk = new HtmlGenericControl("div");
                desk.Attributes["class"] = "folderDesc";
                desk.InnerHtml = "<p>" + currfolder.Descrizione + "</p>";
                colTitle.Controls.Add(desk);
            }

                                             
            pageTitle.Controls.Add(colTitle);

            generale.Controls.Add(pageTitle);

            

            #endregion

            #region SearchField
            HtmlGenericControl searchForm = new HtmlGenericControl("div");
            searchForm.Attributes["class"] = "row";

            HtmlGenericControl colsearchForm = new HtmlGenericControl("div");
            colsearchForm.Attributes["class"] = "col-md-12";

            HtmlGenericControl control = new HtmlGenericControl("div");
            control.Attributes["class"] = " searchFieldset";


            HtmlGenericControl button = new HtmlGenericControl("button");
            button.Attributes.Add("class", "btn btn-primary collapsed");
            button.Attributes.Add("data-toggle", "collapse");
            button.Attributes.Add("type", "button");
            button.Attributes.Add("data-target", "#searchformNews");


            HtmlGenericControl spanS = new HtmlGenericControl("span");
            spanS.Attributes.Add("class", "glyphicon glyphicon-search");
            button.Controls.Add(spanS);
            button.Controls.Add(new LiteralControl(" " + NLabels[NewsLabels.NewsLabelsList.CercaNews]));

            control.Controls.Add(button);


            control.Controls.Add(GetSearchView());
            colsearchForm.Controls.Add(control);

            searchForm.Controls.Add(colsearchForm);

            generale.Controls.Add(searchForm);
            generale.Controls.Add(new HtmlGenericControl("hr"));
            #endregion

            
            if (NetCms.Structure.Applications.News.Configs.Config.ArchivioStorico)
            {
                HtmlGenericControls.Par archivio = new HtmlGenericControls.Par();
                archivio.Class = "archivionews";
                if (ShowAsArchive)
                    archivio.InnerHtml = "<a title=\"Torna alla modalità normale\" href=\"default.aspx\">Indietro</a>";
                else
                    archivio.InnerHtml = "<a title=\"archivio " + currfolder.Label + "\" href=\"?" + paramArchivio + "\">Archivio " + currfolder.Label + "</a>";
                generale.Controls.Add(archivio);
            }

            #region SearchSp

            SearchForm.Validate();
           
            List<SearchParameter> folder = new List<SearchParameter>();
            List<SearchParameter> document = new List<SearchParameter>();
            List<SearchParameter> doclang = new List<SearchParameter>();
            List<SearchParameter> content = new List<SearchParameter>();

            List<SearchParameter> ricerca = new List<SearchParameter>();

            string selectedSearchOptionsText = "";
            if (SearchForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                //if (SearchForm.VfTesto.PostBackValue.Length > 0)
                //{
                //    content.Add(SearchForm.VfTesto.SearchParameter);
                //    ricerca.Add(SearchForm.VfTesto.SearchParameter);
                //    selectedSearchOptionsText += "<li><strong>News Contenenti il testo: </strong>" + SearchForm.VfTesto.PostBackValue + "</li>";
                //}

                if (SearchForm.VfDescrizione.PostBackValue.Length > 0)
                {
                    doclang.Add(SearchForm.VfDescrizione.SearchParameter);
                    ricerca.Add(SearchForm.VfDescrizione.SearchParameter);
                    selectedSearchOptionsText += "<li><strong>News Contenenti la descrizione: </strong>" + SearchForm.VfDescrizione.PostBackValue + "</li>";
                }

                if (SearchForm.VfTitolo.PostBackValue.Length > 0)
                {
                    doclang.Add(SearchForm.VfTitolo.SearchParameter);
                    ricerca.Add(SearchForm.VfTitolo.SearchParameter);
                    selectedSearchOptionsText += "<li><strong>News Contenenti il titolo: </strong>" + SearchForm.VfTitolo.PostBackValue + "</li>";
                }

                if (SearchForm.VfTipo.ValidationState == VfGeneric.ValidationStates.Valid && SearchForm.VfData.ValidationState == VfGeneric.ValidationStates.Valid && SearchForm.VfData.PostBackValue.Length > 0)
                {
                    if (int.Parse(SearchForm.VfTipo.PostBackValue) == 0) SearchForm.VfData.SearchParameter.ComparisonCriteria = Finder.ComparisonCriteria.GreaterEquals;
                    if (int.Parse(SearchForm.VfTipo.PostBackValue) == 1) SearchForm.VfData.SearchParameter.ComparisonCriteria = Finder.ComparisonCriteria.LessEquals;
                    if (int.Parse(SearchForm.VfTipo.PostBackValue) == 2) SearchForm.VfData.SearchParameter.ComparisonCriteria = Finder.ComparisonCriteria.Equals;

                    document.Add(new SearchParameter(null, "Data", SearchForm.VfData.PostbackValueObject, SearchForm.VfData.SearchParameter.ComparisonCriteria, false));
                    ricerca.Add(SearchForm.VfData.SearchParameter);
                    ricerca.Add(SearchForm.VfTipo.SearchParameter);

                    string sign = " " + NLabels[NewsLabels.NewsLabelsList.Esatta];
                    if (int.Parse(SearchForm.VfTipo.PostBackValue) == 0) sign = " " + NLabels[NewsLabels.NewsLabelsList.SuccessivaAl];
                    if (int.Parse(SearchForm.VfTipo.PostBackValue) == 1) sign = " " + NLabels[NewsLabels.NewsLabelsList.AntecedenteAl];
                    if (int.Parse(SearchForm.VfTipo.PostBackValue) == 2) sign = " " + NLabels[NewsLabels.NewsLabelsList.Esatta];
                    selectedSearchOptionsText += "<li><strong>" + NLabels[NewsLabels.NewsLabelsList.NewsConData] + " " + sign + " : </strong>" + SearchForm.VfData.PostBackValue + "</li>";
                    SearchQS += "&amp;Search_Date=" + SearchForm.VfData.PostBackValue;
                    SearchQS += "&amp;Search_DateOptions=" + SearchForm.VfTipo.PostBackValue;
                }
                if (SearchInfo != null)
                {
                    SearchInfo = new HtmlGenericControl("p");
                    SearchInfo.InnerHtml = NLabels[NewsLabels.NewsLabelsList.CriteriDiRicerca] + " <ul>" + selectedSearchOptionsText + "</ul>";
                    generale.Controls.Add(SearchInfo);
                }
            }
            #endregion

            #region old search
            //if (SearchInfo != null)
            //    div.Controls.Add(SearchInfo);

            //if (Views.ListSearch == Show)
            //{
            //    all.InnerHtml += "<a class=\"search\" href=\"" + PageData.PageName + "?show=search\">" + NewsLabels[NewsLabels.NewsLabelsList.NuovaRicerca] + "</a>";
            //    all.InnerHtml += "<a class=\"show\" href=\"" + PageData.PageName + "?show=this\">" + NewsLabels[NewsLabels.NewsLabelsList.AzzeraRicerca] + "</a>";
            //}
            //if (Views.ListAll == Show || Views.ListThis == Show)
            //    all.InnerHtml += "<a class=\"search\" href=\"" + PageData.PageName + "?show=search\">" + NewsLabels[NewsLabels.NewsLabelsList.CercaNews] + "</a>";
            ////*********************************************************************************************************************

            //div.Controls.Add(all); 
            #endregion


            List<SearchParameter> additionalQuery = new List<SearchParameter>();
            //if (Show == Views.ListAll || Show == Views.ListSearch)
            if (ContentToShow == "childs")
            {
                StructureFolder mytree = FolderBusinessLogic.GetFolderRecordByPath(PageData.Folder.Path, PageData.Folder.NetworkID);
                if (mytree != null)
                {
                    folder.Add(new SearchParameter(null, "Tree", mytree.Tree + "%", Finder.ComparisonCriteria.Like, false));
                    folder.Add(new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.LessThan, false));
                }
            }
            else
            {
                folder.Add(new SearchParameter(null, "ID", PageData.Folder.ID, Finder.ComparisonCriteria.Equals, false));
            } 

            document.Add(new SearchParameter(null, "Application", 3, Finder.ComparisonCriteria.Equals, false));
            document.Add(new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.Not, false));
            document.Add(new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false)); 

            SearchParameter foldSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, folder.ToArray<SearchParameter>());
            document.Add(foldSP);
                        

            SearchParameter docSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, ReportOrder, document.ToArray<SearchParameter>());
            doclang.Add(docSP);

            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                SearchParameter spLang = new SearchParameter(null, "Lang", LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.ID, Finder.ComparisonCriteria.Equals, false);
                doclang.Add(spLang);
            }

            SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, doclang.ToArray<SearchParameter>());
            content.Add(docLangSP);

            int recordPP = 10;

            int numRecord = (!ShowAsArchive) ? NewsBusinessLogic.GetNumRecordsWithSearch(content) : NewsBusinessLogic.GetNumRecordsWithSearchForArchive(content);
          
            PaginationHandler pager = new PaginationHandler(recordPP, numRecord, "page",1);

            IList<NewsDocContent> News = (!ShowAsArchive) ? NewsBusinessLogic.GetNewsPaged(pager.CurrentPage, recordPP, content) : NewsBusinessLogic.GetArchiviedNewsPaged(pager.CurrentPage, recordPP, content);

            HtmlGenericControl searchResult = new HtmlGenericControl("div");
            searchResult.Attributes["class"] = "row";

            HtmlGenericControl colsearchResult = new HtmlGenericControl("div");
            colsearchResult.Attributes["class"] = "col-md-12";

            if (this.FrontType == "list")
            {
                pager.CssClass = "pagination";
                
                pager.BaseLink = PageData.PageName + "?" + pager.PaginationKey + "={0}";

                foreach (NewsDocContent row in News)
                {
                    string data = row.DocLang.Document.Data.ToString();
                    if (data.Length > 0)
                        data = data.Split(' ')[0];
                    //data = data.Remove(10);

                    string strImgInEvidenza = "";
                    HtmlGenericControls.Div divRow = new HtmlGenericControls.Div();
                    divRow.Class = "row";
                    HtmlGenericControls.Div ImgDateDiv = new HtmlGenericControls.Div();
                    ImgDateDiv.Class = "col-md-3 col-xs-12";
                    
                    HtmlGenericControls.Div ContentDiv = new HtmlGenericControls.Div();
                    
                    

                    HtmlGenericControls.Div divDate = new HtmlGenericControls.Div();
                    divDate.Class = "date";
                    HtmlGenericControl span = new HtmlGenericControl("span");
                    span.Attributes.Add("class", "glyphicon glyphicon-calendar data_news ");
                    divDate.Controls.Add(span);
                    HtmlGenericControl time = new HtmlGenericControl("time");
                    time.Attributes.Add("datetime", data);
                    HtmlGenericControl spand = new HtmlGenericControl("span");
                    HtmlGenericControl spanm = new HtmlGenericControl("span");
                    HtmlGenericControl spany = new HtmlGenericControl("span");

                    spand.Attributes.Add("class", "day");
                    spand.Controls.Add(new LiteralControl(row.DocLang.Document.Data.Day.ToString() + " "));
                    spanm.Attributes.Add("class", "month");
                    spanm.Controls.Add(new LiteralControl(row.DocLang.Document.Data.ToString("MMMM", System.Globalization.CultureInfo.CreateSpecificCulture("it-IT")) + " "));
                    spany.Attributes.Add("class", "year");
                    spany.Controls.Add(new LiteralControl(row.DocLang.Document.Data.Year.ToString()));
                    time.Controls.Add(spand);
                    time.Controls.Add(spanm);
                    time.Controls.Add(spany);
                    divDate.Controls.Add(time);
                   
                    if (row.ImgInEvidenza != null && row.ImgInEvidenza.ShowInTeaser)
                    {
                        ContentDiv.Class = "col-md-9";
                        NewsHighLigthImage image = row.ImgInEvidenza;
                        FilesDocument imgInEvidenza = DocumentBusinessLogic.GetById(image.RifDoc) as FilesDocument;
                        if (imgInEvidenza != null)
                        {

                            strImgInEvidenza = "<p class=\"imgInEvidenza list " + image.Align + "\"><img src=\"" + imgInEvidenza.FrontendLink + ".ashx?preset=newsthumb" + "\" alt=\"" + imgInEvidenza.Descrizione.Replace("\"", "") + "\" /></p>";
                            HtmlGenericControl a = new HtmlGenericControl("a");
                            a.Attributes.Add("href", "#");
                            HtmlGenericControl img = new HtmlGenericControl("img");
                            img.Attributes.Add("class", "img-responsive thumbnail ");
                            img.Attributes.Add("src", imgInEvidenza.FrontendLink + ".ashx?preset=newsdetail");
                            img.Attributes.Add("alt", imgInEvidenza.Descrizione.Replace("\"", ""));
                            a.Controls.Add(img);
                            ImgDateDiv.Controls.Add(a);
                            divRow.Controls.Add(ImgDateDiv);
                        }                       
                    }
                    else
                    {
                        ContentDiv.Class = "col-md-12";
                        ImgDateDiv.Class = "";
                    }
                   
                    ContentDiv.Controls.Add(divDate);
                    HtmlGenericControl h3 = new HtmlGenericControl("h3");
                    h3.Controls.Add(new LiteralControl(row.DocLang.Label));
                    HtmlGenericControl p = new HtmlGenericControl("p");
                    p.Controls.Add(new LiteralControl(row.DocLang.Descrizione));
                    HtmlGenericControl alink = new HtmlGenericControl("a");
                    alink.Attributes.Add("class", "btn btn-primary");
                    alink.Attributes.Add("href", PageData.AbsoluteWebRoot + row.DocLang.Document.Folder.Path + "/" + PageData.PageName + "?news=" + row.DocLang.Document.ID + ((ShowAsArchive) ? "&" + paramArchivio : ""));
                    alink.Controls.Add(new LiteralControl("Leggi"));
                    HtmlGenericControl spanbutton = new HtmlGenericControl("span");
                    spanbutton.Attributes.Add("class", "glyphicon glyphicon-chevron-right");
                    alink.Controls.Add(spanbutton);
                    ContentDiv.Controls.Add(h3);
                    ContentDiv.Controls.Add(p);
                    ContentDiv.Controls.Add(alink);
                    HtmlGenericControl hr = new HtmlGenericControl("hr");
                    
                    divRow.Controls.Add(ContentDiv);

                    colsearchResult.Controls.Add(divRow);
                    colsearchResult.Controls.Add(hr);
                  
                }                                     
                colsearchResult.Controls.Add(pager);
            }
            else if (this.FrontType == "table")
            {            
                ArTable newsTable = new ArTable();
                newsTable.EnablePagination = true;
                newsTable.RecordPerPagina = recordPP;
                newsTable.InnerTableCssClass = "tab";
                newsTable.PaginationKey = "page";
                newsTable.PagesControl = pager;
                newsTable.Records = News;
                newsTable.SearchParameter = ricerca;
                newsTable.ResponsiveTableStriped = true;

                ArTextColumn data = new ArTextColumn(NLabels[NewsLabels.NewsLabelsList.Data], "DocLang.Document.Data");
                data.DataType = ArTextColumn.DataTypes.Date;
                data.dataHide = ArTable.DataHide.phone;
                newsTable.AddColumn(data);

                ArActionColumn titolo = new ArActionColumn(NLabels[NewsLabels.NewsLabelsList.Titolo], "DocLang.Label", PageData.PageName + "?news={DocLang.Document.ID}" + ((ShowAsArchive) ? "&" + paramArchivio : ""));
                titolo.showToggle = true;
                newsTable.AddColumn(titolo);

                ArTextColumn descrizione = new ArTextColumn(NLabels[NewsLabels.NewsLabelsList.Descrizione], "DocLang.Descrizione");
                descrizione.dataHide = ArTable.DataHide.phone;
                newsTable.AddColumn(descrizione);

                colsearchResult.Controls.Add(newsTable);
            }


            searchResult.Controls.Add(colsearchResult);

            generale.Controls.Add(searchResult);
            //************Search************************************************************************************************
            string qs = "";
            if (Show == Views.ListSearch)
            {
                qs = "?show=finded";
            }
            else
                qs = "?show=" + ShowRequest.StringValue;
            qs += SearchQS;
            //*********************************************************************************************************************


            
            page.Controls.Add(generale);
            return page;
        }

        public HtmlGenericControl GetDetailView()
        {

            HtmlGenericControl articlePage = new HtmlGenericControl("article");
            articlePage.Attributes["class"] = "article-News";

            if (DocRecord == null)
                //throw new NetCms.Exceptions.PageNotFound404Exception();
                throw new System.Web.HttpException(404, "File Not Found");

            HtmlGenericControl divblock = new HtmlGenericControl("div");
            divblock.Attributes["class"] =  "row " + ApplicationCSSClass + CssClass;

            NewsDocContent currentNews = DocRecord.ContentRecord as NewsDocContent;

            //Nel caso in cui non è stato pubblicato il contenuto per la lingua scelta, prendo di default il contenuto della lingua predefinita, ad esempio l'italiano.
            if (DocRecord.ContentRecord == null)
            {
                DocContent content = DocRecord.DocLangs.First(x => x.Lang == LanguageManager.BusinessLogic.LanguageBusinessLogic.GetDefaultLanguage().ID).Content;
                currentNews = NewsBusinessLogic.GetById(content.ID);
            }

            //if (NetCms.Structure.Applications.News.Configs.Config.DataPubblicazione &&            
            //    currentNews.DataPubblicazione <= DateTime.Now)
            //    throw new NetCms.Exceptions.PageNotFound404Exception();

            // Consente l'inserimento del titolo della news nelle briciole di pane
            WhereBlock part = new WhereBlock(currentNews.DocLang.Label, "");          
            PageData.Where.Blocks.Add(part);

            if (ShowAsArchive)
                PageData.Where.Blocks[PageData.Where.Blocks.Count - 2].Link += "/?" + paramArchivio + "";

            NetCms.Structure.CmsPathCodes coder = new NetCms.Structure.CmsPathCodes(PageData.AbsoluteWebRoot);
            string testo = coder.DecodeWebRootReference(currentNews.Testo);
            string data = currentNews.Revisione.Data.ToString();
            //data = data.Length > 10 ? data.Remove(10) : data;
            data = data.Split(' ')[0];
            DateTime date = DateTime.Parse(data);
            string date_ok = date.Day.ToString() + " " + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month) + " " + date.Year.ToString();

            PageData.HtmlHead.Title = currentNews.DocLang.Label + " » " + PageData.HtmlHead.Title;

            string strImgInEvidenza = "";

            if (currentNews.ImgInEvidenza != null && currentNews.ImgInEvidenza.ShowInDetail)
            {
                NewsHighLigthImage image = currentNews.ImgInEvidenza;
                FilesDocument imgInEvidenza = DocumentBusinessLogic.GetById(image.RifDoc) as FilesDocument;
                strImgInEvidenza = "<div class=\"col-xs-12 col-md-6 col-md-offset-3 imgInEvidenza detailpage thumbnail" + image.Align + "\"><figure class=\"figure figure-news\"><img class=\"figure-img figure-img-news img-fluid rounded\" src=\"" + imgInEvidenza.FrontendLink + ".ashx?preset=newsdetail" + "\" alt=\"" + imgInEvidenza.Descrizione.Replace("\"", "") + "\" /><figcaption class=\"figure-caption figure-caption-news\">" + imgInEvidenza.Descrizione.Replace("\"", "") + "</figcaption></figure></div>";
            }
         

            WebControl colNews = new WebControl(HtmlTextWriterTag.Div);
            colNews.CssClass = "col-md-12";
                         
            WebControl phblock = new WebControl(HtmlTextWriterTag.Div);
            phblock.CssClass = "page-header";
                         
            WebControl title = new WebControl(HtmlTextWriterTag.H1);
            currentNews.DocLang.Label = currentNews.DocLang.Label.Replace("<%= WebRoot %>", PageData.AbsoluteWebRoot);
            title.Controls.Add(new LiteralControl(currentNews.DocLang.Label));

            phblock.Controls.Add(title);


            WebControl datereview = new WebControl(HtmlTextWriterTag.Div);
            datereview.CssClass = "row";


            WebControl coldatereview = new WebControl(HtmlTextWriterTag.Div);
            coldatereview.CssClass = "col-md-12";

            coldatereview.Controls.Add(new LiteralControl(FrontCommonLabels[CommonLabels.CommonLabelsList.LabelLastRevision].ToString() + ":  <span class=\"glyphicon glyphicon-calendar\"></span> " + date_ok));
                                                   
            datereview.Controls.Add(coldatereview);


            phblock.Controls.Add(datereview);

            colNews.Controls.Add(phblock);


            WebControl article = new WebControl(HtmlTextWriterTag.Div);
            article.CssClass = "row";

            WebControl colarticle = new WebControl(HtmlTextWriterTag.Div);
            colarticle.CssClass = "col-md-12";

            WebControl imgArticle = new WebControl(HtmlTextWriterTag.Div);
            imgArticle. CssClass = "row";
           
            imgArticle.Controls.Add(new LiteralControl(strImgInEvidenza));  
                               
            WebControl text = new WebControl(HtmlTextWriterTag.Div);
            text.CssClass = "row news-article";

            WebControl coltext = new WebControl(HtmlTextWriterTag.Div);
            coltext.CssClass = "col-md-12";

            coltext.Controls.Add(new LiteralControl(testo.Replace("<%= WebRoot %>", PageData.AbsoluteWebRoot)));

            text.Controls.Add(coltext);

            colarticle.Controls.Add(imgArticle);
            colarticle.Controls.Add(text);

            article.Controls.Add(colarticle);

            colNews.Controls.Add(article);

            divblock.Controls.Add(colNews);

            articlePage.Controls.Add(divblock);
            return articlePage;
        }

        public Control GetDetailPreview()
        {                  
            if (this.FrontType != "template")
            { 
                HtmlGenericControl articlePage = new HtmlGenericControl("article");
                articlePage.Attributes["class"] = "article-News";                

                HtmlGenericControl divblock = new HtmlGenericControl("div");
                divblock.Attributes["class"] = "row " + ApplicationCSSClass + CssClass;

                WebControl colNews = new WebControl(HtmlTextWriterTag.Div);
                colNews.CssClass = "col-md-12";

                WebControl phblock = new WebControl(HtmlTextWriterTag.Div);
                phblock.CssClass = "page-header";

                NewsDocContent docContent = NewsBusinessLogic.GetById(this.ContentID);
                if (docContent != null)
                {
                    string testo = "";
                    string label = "";
                    string data = "";
                    string date_ok = "";

                    testo = docContent.Testo;
                    NetCms.Structure.CmsPathCodes coder = new NetCms.Structure.CmsPathCodes(PageData.AbsoluteWebRoot);
                    testo = coder.DecodeWebRootReference(testo);

                    if (docContent.Revisione != null)
                    {
                        label = docContent.Revisione.DocLang.Label;

                        data = docContent.Revisione.Data.ToString();            
                        data = data.Split(' ')[0];

                        DateTime date = DateTime.Parse(data);
                        date_ok = date.Day.ToString() + " " + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month) + " " + date.Year.ToString();

                    }
                    else
                    {
                        label = docContent.DocLang.Label;

                        data = docContent.DocLang.Document.Date;                      
                        data = data.Split(' ')[0];

                        DateTime date = DateTime.Parse(data);
                        date_ok = date.Day.ToString() + " " + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month) + " " + date.Year.ToString();
                    }

                    WebControl title = new WebControl(HtmlTextWriterTag.H1);
                    title.Controls.Add(new LiteralControl(label));
                    phblock.Controls.Add(title);

                    WebControl datereview = new WebControl(HtmlTextWriterTag.Div);
                    datereview.CssClass = "row";

                    WebControl coldatereview = new WebControl(HtmlTextWriterTag.Div);
                    coldatereview.CssClass = "col-md-12";
                    coldatereview.Controls.Add(new LiteralControl(FrontCommonLabels[CommonLabels.CommonLabelsList.LabelLastRevision].ToString() + ": <span class=\"glyphicon glyphicon-calendar\"></span> " + date_ok));
                    datereview.Controls.Add(coldatereview);
                    phblock.Controls.Add(datereview);
                    colNews.Controls.Add(phblock);

                    PageData.HtmlHead.Title = label + " » " + PageData.HtmlHead.Title;

                    WebControl article = new WebControl(HtmlTextWriterTag.Div);
                    article.CssClass = "row";

                    WebControl colarticle = new WebControl(HtmlTextWriterTag.Div);
                    colarticle.CssClass = "col-md-12";

                    colarticle.Controls.Add(new LiteralControl(testo.Replace("<%= WebRoot %>", PageData.AbsoluteWebRoot)));

                    article.Controls.Add(colarticle);

                    colNews.Controls.Add(article);
                }
                else
                {
                    WebControl title = new WebControl(HtmlTextWriterTag.H1);
                    title.Controls.Add(new LiteralControl("Errore nella preview delle News"));
                    phblock.Controls.Add(title);
                    colNews.Controls.Add(phblock);
                }
                divblock.Controls.Add(colNews);

                articlePage.Controls.Add(divblock);
                return articlePage;
            }
            else
            {
                return GetNewsDetail(true);
            }
        }

        public HtmlGenericControl GetSearchView()
        {
            HtmlGenericControl divOut = new HtmlGenericControl("div");
            divOut.Attributes["class"] = "searchForm collapse";
            //divOut.Attributes.Add("style", "display:none");
            divOut.Attributes.Add("id", "searchformNews");
            divOut.Controls.Add(SearchForm);

            #region oldcode
            //HtmlGenericControl divOut = new HtmlGenericControl("div");
            //divOut.Attributes["class"] = ApplicationCSSClass + CssClass;

            //HtmlGenericControl div = new HtmlGenericControl("fieldset");

            //HtmlGenericControl legend = new HtmlGenericControl("legend");
            //legend.InnerHtml = NewsLabels[NewsLabels.NewsLabelsList.RicercaNews];
            //div.Controls.Add(legend);
            //div.Attributes["class"] = "SearchSet";
            //HtmlGenericControl p = new HtmlGenericControl("p");
            //p.InnerHtml = NewsLabels[NewsLabels.NewsLabelsList.DescrizioneRicerca];
            //div.Controls.Add(p);

            //p = new HtmlGenericControl("p");
            ////******************************************************************
            //TextBox text = new TextBox();
            //text.Columns = 60;
            //text.ID = "Search_Text";

            //Label textlabel = new Label();
            //textlabel.AssociatedControlID = text.ID;
            //textlabel.Text = NewsLabels[NewsLabels.NewsLabelsList.TitoloOTestoContenuto];

            //p.Controls.Add(textlabel);
            //p.Controls.Add(text);

            //div.Controls.Add(p);
            ////******************************************************************
            //p = new HtmlGenericControl("p");

            //TextBox date = new TextBox();
            //date.Columns = 10;
            //date.ID = "Search_Date";

            //DropDownList dateopt = new DropDownList();
            //dateopt.ID = "Search_DateOptions";
            //dateopt.Items.Add(new ListItem(NewsLabels[NewsLabels.NewsLabelsList.SuccessiveAl], "0"));
            //dateopt.Items.Add(new ListItem(NewsLabels[NewsLabels.NewsLabelsList.AntecedentiAl], "1"));
            //dateopt.Items.Add(new ListItem(NewsLabels[NewsLabels.NewsLabelsList.DataEsatta], "2"));

            //Label datelabel = new Label();
            //datelabel.AssociatedControlID = date.ID;
            //datelabel.Text = NewsLabels[NewsLabels.NewsLabelsList.LabelDataFormato];

            //p.Controls.Add(datelabel);
            //p.Controls.Add(dateopt);
            //p.Controls.Add(date);

            //div.Controls.Add(p);
            ////******************************************************************

            //Button send = new Button();
            //send.ID = "Search_Button";
            //send.Text = NewsLabels[NewsLabels.NewsLabelsList.LabelSearchButton];

            //div.Controls.Add(send);

            //divOut.Controls.Add(div); 
            #endregion

            return divOut;
        }


        //public string CheckSearch()
        //{
        //    string where = "";
        //    string selectedSearchOptionsText = "";



        //    NetUtility.RequestVariable text = new NetUtility.RequestVariable("Search_Text", NetUtility.RequestVariable.RequestType.Form_QueryString);
        //    NetUtility.RequestVariable date = new NetUtility.RequestVariable("Search_Date", NetUtility.RequestVariable.RequestType.Form_QueryString);
        //    NetUtility.RequestVariable dateopt = new NetUtility.RequestVariable("Search_DateOptions", NetUtility.RequestVariable.RequestType.Form_QueryString);

        //    if (text.IsValid() && text.StringValue.ToString().Trim().Length > 0)
        //    {
        //        where += "( ";
        //        where += "  Testo_News LIKE '%" + text.StringValue + "%' OR";
        //        where += "  Desc_DocLang LIKE '%" + text.StringValue + "%' OR";
        //        where += "  Label_DocLang LIKE '%" + text.StringValue + "%' OR";
        //        where += "  Name_Doc LIKE '%" + text.StringValue + "%' ";
        //        where += ")";

        //        selectedSearchOptionsText += "<li><strong>News Conteneti il testo: </strong>" + text.StringValue + "</li>";
        //        SearchQS = "&amp;Search_Text=" + text.StringValue;
        //    }

        //    if (dateopt.IsValid(NetUtility.RequestVariable.VariableType.Integer) && date.IsValid(NetUtility.RequestVariable.VariableType.Date))
        //    {
        //        if (where.Length > 0)
        //            where += " AND ";
        //        string sign = "=";
        //        if (dateopt.IntValue == 0) sign = ">=";
        //        if (dateopt.IntValue == 1) sign = "<=";
        //        if (dateopt.IntValue == 2) sign = "=";
        //        where += "  Data_Doc " + sign + " " + NetFrontend.DAL.GenericDAL.Conn.FilterDate(date.DateTimeValue);

        //        sign = " " + NewsLabels[NewsLabels.NewsLabelsList.Esatta];
        //        if (dateopt.IntValue == 0) sign = " " + NewsLabels[NewsLabels.NewsLabelsList.SuccessivaAl];
        //        if (dateopt.IntValue == 1) sign = " " + NewsLabels[NewsLabels.NewsLabelsList.AntecedenteIl];
        //        if (dateopt.IntValue == 2) sign = " " + NewsLabels[NewsLabels.NewsLabelsList.Esatta];
        //        selectedSearchOptionsText += "<li><strong>" + NewsLabels[NewsLabels.NewsLabelsList.NewsConData] + " " + sign + " : </strong>" + date.StringValue + "</li>";
        //        SearchQS += "&amp;Search_Date=" + date.StringValue;
        //        SearchQS += "&amp;Search_DateOptions=" + dateopt.StringValue;
        //    }

        //    if (where.Length > 0)
        //    {
        //        where = "(" + where + ")";

        //        SearchInfo = new HtmlGenericControl("p");
        //        SearchInfo.InnerHtml = NewsLabels[NewsLabels.NewsLabelsList.CriteriDiRicerca] + " <ul>" + selectedSearchOptionsText + "</ul>";
        //    }

        //    return where;
        //}

        private NewsSearchForm _SearchForm;
        public NewsSearchForm SearchForm
        {
            get
            {
                if (_SearchForm == null)
                {
                    _SearchForm = new NewsSearchForm("SearchForm", _PageData.IsPostBack);
                    _SearchForm.TitleControl.Controls.Add(new LiteralControl("<h4>" + NLabels[NewsLabels.NewsLabelsList.SearchFormTitle] + "</h4>"));
                    _SearchForm.UsedForFrontend = true;
                   
                }
                return _SearchForm;
            }
        }

        /* Template Labels */
        private Dictionary<string, string> _TemplateLabels;
        public Dictionary<string, string> TemplateLabels
        {
            get
            {
                if (_TemplateLabels == null)
                {
                    _TemplateLabels = new Dictionary<string, string>();
                    foreach (NewsLabels.NewsLabelsList label in Enum.GetValues(typeof(NewsLabels.NewsLabelsList)))
                        _TemplateLabels.Add(label.ToString().ToLower(), NLabels[label].ToString());
                }
                return _TemplateLabels;
            }
        }

        public Control GetNewsDetail(bool isForPreview = false)
        {
            WebControl page = new WebControl(HtmlTextWriterTag.Div);
            page.CssClass = "wrapper";

            NewsDocContent currentNews;

            if (DocRecord == null)             
                throw new System.Web.HttpException(404, "File Not Found");
            else if (isForPreview)
                currentNews = NewsBusinessLogic.GetById(this.ContentID);
            else
            {
                currentNews = DocRecord.ContentRecord as NewsDocContent;

                //Nel caso in cui non è stato pubblicato il contenuto per la lingua scelta, prendo di default il contenuto della lingua predefinita, ad esempio l'italiano.
                if (DocRecord.ContentRecord == null)
                {
                    DocContent content = DocRecord.DocLangs.First(x => x.Lang == LanguageManager.BusinessLogic.LanguageBusinessLogic.GetDefaultLanguage().ID).Content;
                    currentNews = NewsBusinessLogic.GetById(content.ID);
                }
            }

            // Consente l'inserimento del titolo della news nelle briciole di pane
            WhereBlock part = new WhereBlock(currentNews.Revisione.DocLang.Label, "");
            PageData.Where.Blocks.Add(part);

            NetCms.Structure.CmsPathCodes coder = new NetCms.Structure.CmsPathCodes(PageData.AbsoluteWebRoot);
            string testo = coder.DecodeWebRootReference(currentNews.Testo);
            testo = ReplaceShortCode(testo);            

            string datapub = string.Empty;
            if (currentNews.DataPubblicazione == null)
            {
                if (currentNews.DocLang != null)
                    datapub = (currentNews.DocLang.Document.Data.Day.ToString() + " " + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(currentNews.DocLang.Document.Data.Month) + " " + currentNews.DocLang.Document.Data.Year.ToString());
                else
                    datapub = "";
            }
            else
            {
                datapub  = (currentNews.DataPubblicazione.Value.Day.ToString() + " " + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(currentNews.DataPubblicazione.Value.Month) + " " + currentNews.DataPubblicazione.Value.Year.ToString());
            }        

            DateTime date = currentNews.Revisione.Data;
            string datarev = date.Day.ToString() + " " + System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month) + " " + date.Year.ToString();

            // indirizzo immagine per il tag image di opengraph
            string imageThumbUrl = string.Empty;
            object immagine;
            
            if (currentNews.ImgInEvidenza != null)
            {
                NewsHighLigthImage imgEvidenza = currentNews.ImgInEvidenza;
                immagine = new { src = imgEvidenza.GetSrc(NewsHighLigthImage.Presets.None), descrizione = imgEvidenza.Description, showindetail = imgEvidenza.ShowInDetail };
                imageThumbUrl = imgEvidenza.GetSrc(NewsHighLigthImage.Presets.None);
            }
            else
            {
                immagine = new { src = "", descrizione = "", showindetail = false };
            }                     

            object attachedFile = AttachesBusinessLogic.GetAttachesByDocContent(currentNews.Revisione.Content.ID);

            var document = new {
                titolo = currentNews.Revisione.DocLang.Label,
                testo = testo,
                datarev = datarev,
                datapub = datapub,
                descrizione = currentNews.Revisione.DocLang.Descrizione,             
                img = immagine,
                tags = DocRecord.DocumentTags,
                allegati = attachedFile,
            };

            string srcsharecontent = GetShareContentBlock(
                System.Web.HttpContext.Current.Server.UrlEncode(currentNews.Revisione.DocLang.Label), 
                currentNews.Revisione.DocLang.Document.FrontendLink,
                System.Web.HttpContext.Current.Server.UrlEncode(currentNews.Revisione.DocLang.Descrizione)
                );

            SetOpenGraph(currentNews.Revisione.DocLang.Label, 
                         currentNews.Revisione.DocLang.Descrizione, 
                         "article", 
                         currentNews.Revisione.DocLang.Document.FrontendLink, 
                         imageThumbUrl
                         );

            PageData.HtmlHead.Title = currentNews.Revisione.DocLang.Label + " » " + PageData.HtmlHead.Title;

            string sectionmenu = "false";
            if (currentFolder.ShowMenu)
            {
                // TODO: Implementare la logica di recupero e generazione menu contestuale
            }
           
            var sezione = new { titolo = this.currentFolder.Label, url = this.currentFolder.FrontendUrl, pagename = "default.aspx", menu = sectionmenu };        

            string tpl_path = base.GetContentTemplatePath("Dettaglio", "news");

            var result = NetCms.Themes.TemplateEngine.RenderTpl(document, sezione, srcsharecontent, tpl_path, TemplateLabels, TemplateCommonLabels);

            page.Controls.Add(new LiteralControl(result));
            return page;
        }

        public Control GetNewsList()
        {
            WebControl page = new WebControl(HtmlTextWriterTag.Div);
            page.CssClass = "wrapper";

            #region SearchParameter

            int currentYear = DateTime.Now.Year;
            int selectedMonth = -1;

            int selectedMonthStart = 1;
            int selectedMonthEnd = 12;

            int selectedDay = -1;

            List<SearchParameter> folder = new List<SearchParameter>();
            List<SearchParameter> document = new List<SearchParameter>();
            List<SearchParameter> doclang = new List<SearchParameter>();
            List<SearchParameter> content = new List<SearchParameter>();

            SearchForm.Validate();

            if (!string.IsNullOrEmpty(SearchForm.VfTitolo.PostBackValue))
            {
                doclang.Add(SearchForm.VfTitolo.SearchParameter);                                       
            }

            if (SearchForm.VfDescrizione.PostBackValue.Length > 0)
            {
                doclang.Add(SearchForm.VfDescrizione.SearchParameter);                                       
            }

            if (SearchForm.VfTipo.ValidationState == VfGeneric.ValidationStates.Valid && !string.IsNullOrEmpty(SearchForm.VfTipo.PostBackValue) && SearchForm.VfData.ValidationState == VfGeneric.ValidationStates.Valid && SearchForm.VfData.PostBackValue.Length > 0)
            {
                if (int.Parse(SearchForm.VfTipo.PostBackValue) == 0) SearchForm.VfData.SearchParameter.ComparisonCriteria = Finder.ComparisonCriteria.GreaterEquals;
                if (int.Parse(SearchForm.VfTipo.PostBackValue) == 1) SearchForm.VfData.SearchParameter.ComparisonCriteria = Finder.ComparisonCriteria.LessEquals;
                if (int.Parse(SearchForm.VfTipo.PostBackValue) == 2) SearchForm.VfData.SearchParameter.ComparisonCriteria = Finder.ComparisonCriteria.Equals;

                document.Add(new SearchParameter(null, "Data", SearchForm.VfData.PostbackValueObject, SearchForm.VfData.SearchParameter.ComparisonCriteria, false));                                                                         
            }

            #region Archivio storico
            if (string.IsNullOrEmpty(SearchForm.VfData.PostBackValue) && string.IsNullOrEmpty(SearchForm.VfTesto.PostBackValue))
            {
                bool queryByArchStorico = false;

                NetUtility.RequestVariable rqAnno = new NetUtility.RequestVariable("anno", NetUtility.RequestVariable.RequestType.QueryString);
                if (rqAnno.IsValidInteger)
                {
                    currentYear = rqAnno.IntValue;
                    queryByArchStorico = true;
                }

                NetUtility.RequestVariable rqMese = new NetUtility.RequestVariable("mese", NetUtility.RequestVariable.RequestType.QueryString);
                if (rqMese.IsValidInteger)
                {
                    //monthSelected = true;
                    selectedMonthStart = rqMese.IntValue;
                    selectedMonth = rqMese.IntValue;
                    queryByArchStorico = true;
                }

                NetUtility.RequestVariable rqGiorno = new NetUtility.RequestVariable("giorno", NetUtility.RequestVariable.RequestType.QueryString);
                if (rqGiorno.IsValidInteger)
                {
                    selectedDay = rqGiorno.IntValue;
                    queryByArchStorico = true;
                }

                DateTime periodo_ricerca_da;
                if (selectedDay == -1)
                    periodo_ricerca_da = new DateTime(currentYear, selectedMonthStart, 1);
                else
                    periodo_ricerca_da = new DateTime(currentYear, selectedMonthStart, selectedDay);

                if (selectedMonth == -1)
                    selectedMonth = selectedMonthEnd;

                DateTime lastDayOfThisMonth;
                if (selectedDay == -1)
                    lastDayOfThisMonth = new DateTime(currentYear, selectedMonth, 1).AddMonths(1).AddDays(-1);
                else
                    lastDayOfThisMonth = new DateTime(currentYear, selectedMonth, selectedDay);

                DateTime periodo_ricerca_a = new DateTime(currentYear, selectedMonth, lastDayOfThisMonth.Day);

                if (queryByArchStorico)
                {
                    SearchParameter archivioStoricoFromSP = new SearchParameter(null, "Data", periodo_ricerca_da, Finder.ComparisonCriteria.GreaterEquals, false);
                    document.Add(archivioStoricoFromSP);

                    SearchParameter archivioStoricoToSP = new SearchParameter(null, "Data", periodo_ricerca_a, Finder.ComparisonCriteria.LessEquals, false);
                    document.Add(archivioStoricoToSP);
                }
            }
            #endregion


            List<SearchParameter> additionalQuery = new List<SearchParameter>();

            if (ContentToShow == "childs" && NewsFolder.ShowChild)
            {
                StructureFolder mytree = FolderBusinessLogic.GetFolderRecordByPath(PageData.Folder.Path, PageData.Folder.NetworkID);
                if (mytree != null)
                {
                    folder.Add(new SearchParameter(null, "Tree", mytree.Tree + "%", Finder.ComparisonCriteria.Like, false));
                    folder.Add(new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.LessThan, false));
                }
            }
            else
            {
                folder.Add(new SearchParameter(null, "ID", PageData.Folder.ID, Finder.ComparisonCriteria.Equals, false));
            }

            document.Add(new SearchParameter(null, "Application", 3, Finder.ComparisonCriteria.Equals, false));
            document.Add(new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.Not, false));
            document.Add(new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false));

            SearchParameter foldSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, folder.ToArray<SearchParameter>());
            document.Add(foldSP);

            SearchParameter docSP = new SearchParameter(null, "Document", SearchParameter.RelationTypes.OneToMany, ReportOrder, document.ToArray<SearchParameter>());
            doclang.Add(docSP);

            SearchParameter docLangSP = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToOne, doclang.ToArray<SearchParameter>());
            content.Add(docLangSP);
            #endregion

            int recordPP = 12;

            int numRecord = (!ShowAsArchive) ? NewsBusinessLogic.GetNumRecordsWithSearch(content) : NewsBusinessLogic.GetNumRecordsWithSearchForArchive(content);

            PaginationHandler pager = new PaginationHandler(recordPP, numRecord, "page", SearchForm.IsPostBack, 1, true);            
            pager.SearchFields = SearchForm.Fields;


            IList<NewsDocContent> News = (!ShowAsArchive)
                ? NewsBusinessLogic.GetNewsPaged(pager.CurrentPage, recordPP, content)
                : NewsBusinessLogic.GetArchiviedNewsPaged(pager.CurrentPage, recordPP, content);
          
            var config = new MapperConfiguration(cfg =>
            {
                // mapping dei dati del documento
                cfg.CreateMap<NewsDocContent, NewsDTO>()
                 .ForMember(x => x.ID, o => o.MapFrom(y => y.DocLang.Document.ID))
                 .ForMember(x => x.Titolo, o => o.MapFrom(y => y.DocLang.Label))
                 .ForMember(x => x.Descrizione, o => o.MapFrom(y => y.DocLang.Descrizione))
                 .ForMember(x => x.Data, o => o.MapFrom(y => y.DocLang.Document.Data))
                 .ForMember(x => x.Content, o => o.MapFrom(y => y.Testo))
                 .ForMember(x => x.Categoria, o => o.MapFrom(y => y.DocLang.Document.GetFolderLabel))
                 .ForMember(x => x.Folderpath, o => o.MapFrom(y => y.DocLang.Document.GetFolderPath))
                 .ForMember(x => x.Imgevidenza, o => o.MapFrom(y => y.ImgInEvidenza));

                // mapping dell'oggetto immagine correlato
                cfg.CreateMap<NewsHighLigthImage, NewsImageDTO>()
                     .ForMember(x => x.Iddoc, o => o.MapFrom(y => y.RifDoc))
                     .ForMember(x => x.Description, o => o.MapFrom(y => y.Description))
                     .ForMember(x => x.Cssclass, o => o.MapFrom(y => y.Align))
                     .ForMember(x => x.Src, o => o.MapFrom(y => y.GetSrc(NewsHighLigthImage.Presets.List)))
                     .ForMember(x => x.Show, o => o.MapFrom(y => y.ShowInTeaser));
            });

            config.AssertConfigurationIsValid();
            
            var mapper = config.CreateMapper();
            IEnumerable<NewsDTO> documents = mapper.Map<IEnumerable<NewsDocContent>, IEnumerable<NewsDTO>>(News);

            #region ArchivioAnniMese

            IList<YearMonth> annimese = (ContentToShow == "childs")
               ? annimese = NewsBusinessLogic.GetArchivioStorico(this.currentFolder.Tree)
               : NewsBusinessLogic.GetArchivioStorico(this.currentFolder.ID);

            var years = (from anni in annimese
                         select anni.Year)
                          .Distinct();

            var months = (from month in annimese
                          where month.Year == currentYear
                          select new
                          {
                              monthname = month.MonthName,
                              month = month.Month
                          }).Distinct();

            var days = (from month in annimese
                        where month.Month == selectedMonth && month.Year == currentYear
                        select new
                        {
                            monthname = month.MonthName,
                            month = month.Month,
                            day = month.Day
                        }).Distinct();

            var archiviostorico = new { anni = years, mesi = months, giorni = days, selectedyear = currentYear, selectedmonth = selectedMonth, selectedday = selectedDay };

            #endregion


            var sezione = new { 
                titolo = this.currentFolder.Label,
                descrizione = this.currentFolder.Descrizione,
                showchild = NewsFolder.ShowChild,
                url = this.currentFolder.FrontendUrl,                                
                pagename = "default.aspx",
                archivio = archiviostorico
            };

            int pageLimit = 12;
            int range = 5;
            int minRange = Math.Max(1, pager.CurrentPage - range);
            int maxRange = Math.Min(pager.PagesCount, pager.CurrentPage + range);
            
            Object paging = new { 
                pages = pager.PagesCount,
                pagekey = pager.PaginationKey, 
                current = pager.CurrentPage, 
                pagelimit = pageLimit, 
                lowrange = minRange, 
                toprange = maxRange,
                pageandquerystring = pager.BuildPaginationBaseAddress()
            };
         
            string tpl_path = base.GetContentTemplatePath(string.IsNullOrEmpty(this.currentFolder.TemplateStartPage) ? "list" : this.currentFolder.TemplateStartPage, "news");
            var result = NetCms.Themes.TemplateEngine.RenderTpl(documents, sezione, tpl_path, TemplateLabels, TemplateCommonLabels, paging);

            page.Controls.Add(new LiteralControl(result));

            return page;                      
        }       
    }
}
