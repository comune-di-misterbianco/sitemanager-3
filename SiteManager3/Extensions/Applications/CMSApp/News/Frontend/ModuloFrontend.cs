﻿using System;
using System.Data;
using System.Globalization;
using System.Web.UI.HtmlControls;
using NetFrontend;
using NetCms.Front;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.Applications.Files;
using LabelsManager;
using NetCms.Connections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.Structure.Applications.Homepage.Model;
using LanguageManager.BusinessLogic;
using NetCms.Structure.WebFS;

namespace NetCms.Structure.Applications.News
{
    [NetCms.Homepages.HomepageModuleDefiner(NewsApplication.ApplicationID, "News", NewsApplication.ApplicationID, NetCms.Homepages.HomepageModuleDefiner.ModuleType.CmsApp)]
    public class LayoutModuloNewsBlock : 
        NetCms.Structure.Applications.NewsExtended.Frontend.GenericApplicationModuloNews
    {

        #region

        /// <summary>
        /// CommonLabels
        /// </summary>
        public static Labels CommonLabel
        {
            get
            {
                return LabelsCache.GetTypedLabels(typeof(NewsLabels), LanguageBusinessLogic.CurrentLanguage.CultureCode_Lang) as Labels;
            }
        }
        #endregion

        protected override string BaseSqlQuery
        {
            get            
            {
                string baseSqlQuery = "";

                ConnectionsManager ConnManager = new ConnectionsManager();
                

                if (NetCms.Structure.Applications.News.Configs.Config.DataPubblicazione)
                {
                    baseSqlQuery += " (ISNULL(DataPubblicazione_News) OR  DataPubblicazione_News <= " + ConnManager.CommonConnection.FilterDate(DateTime.Today) + ") AND ";
                }
                if (NetCms.Structure.Applications.News.Configs.Config.DataScadenza)
                {
                    baseSqlQuery += " (ISNULL(DataScadenza_News) OR DataScadenza_News >=  " + ConnManager.CommonConnection.FilterDate(DateTime.Today) + ") AND ";
                }

                baseSqlQuery += "Application_Doc = 3 AND Lang_DocLang = " + LanguageBusinessLogic.CurrentLanguage.ID;

                switch(ShowCustomNews)
                {
                    case 1:
                        break;
                    case 2:
                        baseSqlQuery += " AND Stato_News = '" + NetCms.Structure.Applications.News.NewsDocContent.NewsStatus.In_Evidenza.ToString() + "'"; ;
                        break;
                    case 3:
                        baseSqlQuery += " AND Stato_News = '" + NetCms.Structure.Applications.News.NewsDocContent.NewsStatus.Normale.ToString() + "'"; ;
                        break;
                    default:
                        break;
                }

                return baseSqlQuery;
            }
        }
        protected override string LocalTypeCssClass
        {
            get { return "modulo_news"; }
        }

        public override string Title
        {
            get { return this.Label; }
        }

        public LayoutModuloNewsBlock(Homepage.Model.Modulo modulo)
            : base(modulo)
        {

        }        

        public override DataRow GetRecord(string sqlConditions)
        {
            return NetFrontend.DAL.NewsDAL.GetDocumentRecord(sqlConditions);
        }

        public override DataRow[] SelectRecords(string sqlConditions, int page = 0, int recordCount = 0, string order = null)
        {
            return NetFrontend.DAL.NewsDAL.ListDocumentRecords(sqlConditions, page, recordCount, order);
        }

        public override int CountRecords(string sqlConditions)
        {
            return NetFrontend.DAL.NewsDAL.CountDocumentRecords(sqlConditions);
        }
        
        protected override HtmlGenericControl BlockContent()
        {
            HtmlGenericControl divmodulo = new HtmlGenericControl("div");            

            string awr = FrontendNetwork.GetAbsoluteWebRoot();
            var Records = InitRecords(RecordsToShow);

            #region No_Template
            if (string.IsNullOrEmpty(base.Template) || base.Template == TemplateType.No_Template.ToString())
            {
                divmodulo.Attributes["class"] = "panel panel-default " + CssClass;

                HtmlGenericControl divheading = new HtmlGenericControl("div");
                divheading.Attributes["class"] = "panel-heading";

                HtmlGenericControl h3 = new HtmlGenericControl("h3");
                h3.InnerHtml = "<a href=\"" + awr + FolderPath + "/" + "\">" + this.Label + "</a>";
                divheading.Controls.Add(h3);

                divmodulo.Controls.Add(divheading);

                HtmlGenericControl divbody = new HtmlGenericControl("div");
                divbody.Attributes["class"] = "panel-body";

                for (int i = 0; i < (Records.Length <= RecordsToShow ? Records.Length : RecordsToShow); i++)
                {
                    DateTime date = DateTime.Parse(Records[i]["Data_Doc"].ToString());
                    string date_ok = date.Day.ToString() + " " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month) + " " + date.Year.ToString();

                    string cssClass = "";

                    if (Records[i]["Stato_News"] != null && Records[i]["Stato_News"].ToString() == "2")
                        cssClass = "class=\"inevidenza\"";

                    string strImgInEvidenza = "";

                    if (Records[i]["HighlightImages_News"] != null && !string.IsNullOrEmpty(Records[i]["HighlightImages_News"].ToString()))
                    {
                        int idHighlightImages = int.Parse(Records[i]["HighlightImages_News"].ToString());

                        NewsHighLigthImage images = NewsBusinessLogic.GetHighLigthImageByID(idHighlightImages);
                        if (images != null && images.ShowInModulo)
                        {
                            FilesDocument imgInEvidenza = DocumentBusinessLogic.GetById(images.RifDoc) as FilesDocument;
                            if (imgInEvidenza != null)
                                strImgInEvidenza = "<div class=\"media-left image  thumbnails\"><span class=\"imgInEvidenza thumb " + images.Align + "\"><img class=\"img-thumbnail media-object img-responsive\" src=\"" + imgInEvidenza.FrontendLink + ".ashx?preset=newsmodulothumb" + "\" alt=\"" + imgInEvidenza.Descrizione.Replace("\"", "") + "\" /></span></div>";
                        }
                    }

                    divbody.InnerHtml += "<li class=\"media \"" + cssClass + ">"
                    + strImgInEvidenza
                    + "<div class=\"media-body post-holder \">"
                    + "<span class=\"glyphicon glyphicon-calendar data_news \"></span>  " + date_ok + " "
                    + " - <a href=\"" + awr + FolderPath
                    + "/default.aspx?news=" + Records[i]["id_Doc"].ToString() + "\">"
                    + "<span class=\"titolo_news\" >" + Records[i]["Label_DocLang"] + "</span></a>";

                    if (ShowDesc)
                    {
                        string desc = Records[i]["Desc_DocLang"].ToString();
                        if (desc.Length > DescLength)
                            desc = desc.Remove(DescLength) + "...";
                        divbody.InnerHtml += "<p>" + desc + "</p>";
                    }
                    divbody.InnerHtml += "</div></li>";
                }
                if (divbody.InnerHtml.Length != 0)
                {
                    divbody.InnerHtml = "<ul class=\"media-list\">" + divbody.InnerHtml + "</ul>";
                }

                divmodulo.Controls.Add(divbody);

                // link mostra tutti
                if (this.ShowMoreLink)
                {
                    HtmlGenericControl divShowAll = new HtmlGenericControl("div");
                    divShowAll.Attributes["class"] = "panel-footer text-center body showAll";
                    divShowAll.InnerHtml = "<a class=\"btn btn-primary\" href=\"" + awr + FolderPath + "/" + "\">" + CommonLabel[NewsLabels.NewsLabelsList.ShowAllModuleBTN] + "</a>";
                    divmodulo.Controls.Add(divShowAll);
                }
            } 
            #endregion
            else
            {               
                LiteralControl cSrc = GetTemplateContent(awr, Records);

                if (string.IsNullOrEmpty(CssClass))
                {
                    divmodulo.Controls.Add(cSrc);
                }
                else
                {
                    WebControl divContainer = new WebControl(HtmlTextWriterTag.Div);
                    divContainer.CssClass = CssClass;
                    divContainer.Controls.Add(cSrc);
                    divmodulo.Controls.Add(divContainer);
                }                
            }

            return divmodulo;
        }

        public LiteralControl GetTemplateContent(string awr, DataRow[] Records)
        {
            var currentmodulo = new {
                                      label = this.Label,
                                      url = awr + FolderPath + "/default.aspx",
                                      itemshowdesc = (this.ShowDesc),
                                      itemshowmore = (this.ShowMoreLink),
                                      showmorelabel = this.FrontCommonLabels[CommonLabels.CommonLabelsList.LabelAccediSezione],
                                      showimages = (!this.HideImages)
            };

            Dictionary<string, string> labels = new Dictionary<string, string>();
            foreach (NewsLabels.NewsLabelsList label in Enum.GetValues(typeof(NewsLabels.NewsLabelsList)))
            {
                labels.Add(label.ToString(), CommonLabel[label].ToString());
            }

            List<Object> items = new List<Object>();

            for (int i = 0; i < (Records.Length <= RecordsToShow ? Records.Length : RecordsToShow); i++)
            {                
                string titolo = Records[i]["Label_DocLang"].ToString();

                string descrizione = string.Empty;
                if (DescLength > 0)
                descrizione = StringUtilities.CutString(Records[i]["Desc_DocLang"].ToString(), DescLength);

                DateTime date = DateTime.Parse(Records[i]["Data_Doc"].ToString());
                string date_ok = date.Day.ToString() + " " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month) + " " + date.Year.ToString();
                
                string itemfolderurl = awr + Records[i]["Path_Folder"].ToString() + "/";
                string itemurl = itemfolderurl + "default.aspx?news=" + Records[i]["id_Doc"].ToString();
                

                int id_folder = int.Parse(Records[i]["id_Folder"].ToString());
                StructureFolder itemfolder = FolderBusinessLogic.GetById(id_folder);
                string folder = itemfolder.Label;

                //Aggiungo awr per concatenare anche il record della lingua. Da verificare che non impatta nel caso in cui ci sono più network.
                //string folderpath = Records[i]["Path_Folder"].ToString();
                string folderpath = awr + FolderPath.ToString();

                Object imgevidenza = null;
                if (Records[i]["HighlightImages_News"] != null && !string.IsNullOrEmpty(Records[i]["HighlightImages_News"].ToString()))
                {
                    int idHighlightImages = int.Parse(Records[i]["HighlightImages_News"].ToString());

                    NewsHighLigthImage images = NewsBusinessLogic.GetHighLigthImageByID(idHighlightImages);
                    if (images != null && images.ShowInModulo)
                    {
                        FilesDocument imgInEvidenza = DocumentBusinessLogic.GetById(images.RifDoc) as FilesDocument;
                        if (imgInEvidenza != null)
                        {
                            imgevidenza = new { imgsrc = imgInEvidenza.FrontendLink,
                                                imgalt = imgInEvidenza.Descrizione.Replace("\"", ""),                                                
                                                imgalign = (!string.IsNullOrEmpty(images.Align) ? GetImgAlign(images.Align) : "false")
                            };
                        }
                    }
                }

                Object item = new { titolo = titolo, descrizione = descrizione, date = date_ok, itemurl = itemurl, img = imgevidenza, categoria = folder, itemcategoryurl = itemfolderurl };
                items.Add(item);
            }

            Object dati = new { modulo = currentmodulo, items = items };

            
            //string base_path = System.Web.HttpContext.Current.Server.MapPath( TplPath + "/content/homepage/");
            string tpl_path = GetModuleTemplatePath(base.Template, "");

            string src = string.Empty;
            src = base.RenderTemplate(dati, tpl_path, labels);
            LiteralControl srcCntr = new LiteralControl(src);
            return srcCntr;            
        }

        private string GetImgAlign(string imgAlign)
        {
            string align = "";

            if (imgAlign == "0")
                align = "pull-left";
            if (imgAlign == "2")
                align = "pull-right";
            if (imgAlign == "1")
                align = "center";
            return align;
        }
    }
}
