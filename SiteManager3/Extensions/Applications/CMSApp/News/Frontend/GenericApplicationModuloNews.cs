﻿using NetCms.Structure.Applications.Homepage.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.HtmlControls;

namespace NetCms.Structure.Applications.NewsExtended.Frontend
{
    public abstract class GenericApplicationModuloNews : NetFrontend.GenericApplicationModulo
    {
        protected int ShowCustomNews;

        public GenericApplicationModuloNews(Modulo modulo)
            : base(modulo)
        {
            ModuloApplicativoNews moduloApp = 
                NetCms.Structure.Applications.Homepage.HomepageModulesBusinessLogic<ModuloApplicativoNews>.GetModuloById(modulo.ID); 

            if (moduloApp != null)
            {
                ShowCustomNews = moduloApp.ShowCustomNews;
            }
            else IsValid = false;
        }

       
    }
}
