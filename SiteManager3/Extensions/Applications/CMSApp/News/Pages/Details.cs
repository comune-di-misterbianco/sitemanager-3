using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Grants;
using System.Reflection;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.Structure.Applications.News.Pages
{
    public class Details : NetCms.Structure.WebFS.DocumentDetailsPage
    {

        protected override ToolbarButton ReserveForNewsletter
        {
            get
            {
                return new ToolbarButton("details.aspx?doc=" + CurrentDocument.ID + "&folder=" + PageData.CurrentFolder.ID + "&nlreserve=" + CurrentDocument.ID, NetCms.GUI.Icons.Icons.Email_Go, "Prenota per Newsletter");                
            }
        }

        protected override ToolbarButton ModifyButton
        {
            get
            {
                if (this.CurrentFolder.ReviewEnabled)
                    return new ApplicationToolbarButton("news_overview.aspx?id=" + CurrentDocument.ID, NetCms.GUI.Icons.Icons.Pencil, "Revisioni", this.CurrentFolder.StructureNetwork, this.CurrentFolder.RelativeApplication);
                else
                    return new ApplicationToolbarButton("news_mod.aspx?id=" + CurrentDocument.ID + "&amp;folder=" + PageData.CurrentFolder.ID, NetCms.GUI.Icons.Icons.Pencil, "Modifica News", this.CurrentFolder.StructureNetwork, this.CurrentFolder.RelativeApplication);
            }
        }



        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {           
            get { return "Dettaglio News: '" + this.CurrentDocument.Nome + "'"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Newspaper_Add; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        public Details()
        {
            ApplicationContext.SetCurrentComponent(NewsApplication.ApplicationContextBackLabel);
            if (this.CurrentDocument.Criteria[CriteriaTypes.Show].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina dei dettagli di una news", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else if (!IsPostBack)
                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina di dettaglio della news " + this.CurrentDocument.Nome + " del portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");               

        }

        private bool NewsletterIsInstalled
        {
            get
            {
                return NetCms.Vertical.VerticalApplicationsPool.ActiveAssemblies.Contains(2);
            }
        }

        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);

            string scheda = "<div class=\"DetailFrameError\"><h3>Nessuna revisione attiva per questo documento<h3></div>";

            #region NewsletterButton

            if (NewsletterIsInstalled && this.CurrentDocument.Content > 0)
            {
                Assembly assemblyNL = NetCms.Vertical.VerticalApplicationsPool.GetByID(2).Assembly;
                Type typePrenotazioneBL = assemblyNL.GetType("Newsletter.BusinessLogic.PrenotazioniBusinessLogic");

                var objPrenotazioneBL = Activator.CreateInstance(typePrenotazioneBL);
                MethodInfo mi = objPrenotazioneBL.GetType().GetMethod("CheckPrenotazione");

                // verifico se il documento � prenotato x la newsletter
                bool prenotato = (bool)mi.Invoke(objPrenotazioneBL, new object[] { this.CurrentDocument, null });

                if (!prenotato)
                {
                    bool insert = false;
                    NetUtility.RequestVariable NewsLetterReserve = new NetUtility.RequestVariable("nlreserve", PageData.Request.QueryString);
                    if (NewsLetterReserve.IsValid(NetUtility.RequestVariable.VariableType.Integer) && this.CurrentDocument.ID == NewsLetterReserve.IntValue)
                    {
                        Type typePrenotazione = assemblyNL.GetType("Newsletter.Model.Prenotazioni");

                        Object[] args = { this.CurrentDocument, NetCms.Users.AccountManager.CurrentAccount, 0 };
                        var objPrenotazione = Activator.CreateInstance(typePrenotazione, args);

                        mi = objPrenotazioneBL.GetType().GetMethod("SaveOrUpdatePrenotazioni");
                        insert = (bool)mi.Invoke(objPrenotazioneBL, new object[] { objPrenotazione, null });

                        if (insert)
                            NetCms.GUI.PopupBox.AddSessionMessage("Prenotazione eseguita con successo", PostBackMessagesType.Success);
                        else
                            NetCms.GUI.PopupBox.AddSessionMessage("Errore durante la prenotazione del documento", PostBackMessagesType.Error);

                    }

                    if (!insert && this.CurrentDocument.Content > 0)
                        this.Toolbar.Buttons.Add(ReserveForNewsletter);
                }
            }
         
            #endregion


            if (this.CurrentDocument.Content > 0)
            {
             
                WebControl iframe = new WebControl(HtmlTextWriterTag.Iframe);
                iframe.Attributes["src"] = this.CurrentFolder.FrontendHyperlink + "/preview.aspx?id=" + this.CurrentDocument.Content;
                iframe.Attributes["width"] = "100%";
                iframe.Attributes["height"] = "700px";

                div.Controls.Add(iframe);
            }
            else
                div.Controls.Add(new LiteralControl(scheda));

            return div;
        }
    }
}