using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Linq;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.Reviews;
using NetCms.Grants;
using NHibernate;

namespace NetCms.Structure.Applications.News.Pages
{
    public class NewsAdd : NetCms.GUI.NetworkPage
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Creazione Nuova News"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Newspaper_Add; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        public NewsAdd()
        {
            ApplicationContext.SetCurrentComponent(NewsApplication.ApplicationContextBackLabel);
            if (this.CurrentFolder.Criteria[CriteriaTypes.Create].Value == GrantsValues.Deny)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Utente non autorizzato a vedere la pagina di aggiunta di una nuova news", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else if (!IsPostBack)
                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente sta accedendo alla pagina di creazione di una news nel portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");
        }

        private int docContentID = 0;

        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "container-fluid";

            NetFormTable netDocs = NewsDocument.NetworkDocsFormTable(PageData);
            netDocs.CssClass = "netformtable_netDocs";

            NetFormTable docs = NewsDocument.DocsFormTable(PageData, NewsApplication.ApplicationID);
            docs.CssClass = "netformtable_docs";

            NetFormTable langs = NewsDocument.LanguageFormTable(PageData);
            langs.CssClass = "netformtable_langs";

            AttachesGui attachControl = new AttachesGui(
                NewsApplication.ApplicationID,
                (this.CurrentDocument != null) ? docContentID : -1,
                PageData.CurrentConfig.AttachedEnabledExtensions
            );

            NetExternalField content;
            NetExternalField contentForDocLang;
            NetExternalField docsToNetDocs;
            NetExternalField docsToWebDocsDoc;

            NetFormTable contentTable = NewsDocument.DocsContentFormTable(PageData);
            contentTable.CssClass = "netformtable_contenttable";

            #region NewsFormTable
            NetFormTable news = new NetFormTable("news_docs_contents", "id_News", this.CurrentFolder.StructureNetwork.Connection);
            news.CssClass = "news_contentfield";

            NetFormTable newsDoc = new NetFormTable("news_docs", "id_docs", this.CurrentFolder.StructureNetwork.Connection);

            if (News.Configs.Config.DataPubblicazione)
            {
                NetDateField2 dataPubblicazione = new NetDateField2("Data pubblicazione", "DataPubblicazione_News", this.CurrentNetwork.Connection);
                dataPubblicazione.Required = false;
                news.addField(dataPubblicazione);
            }

            if (News.Configs.Config.DataScadenza)
            {
                NetDateField2 dataScadenza = new NetDateField2("Data scadenza e archiviazione", "DataScadenza_News", this.CurrentNetwork.Connection);
                dataScadenza.Required = false;
                news.addField(dataScadenza);
            }

            NetDropDownList Stato = new NetDropDownList("Visibilit�", "Stato_News");
            Stato.addItem(NewsDocContent.NewsStatus.Normale.ToString(), NewsDocContent.NewsStatus.Normale.ToString());
            Stato.addItem(NewsDocContent.NewsStatus.In_Evidenza.ToString().Replace("_", " "), NewsDocContent.NewsStatus.In_Evidenza.ToString());
            Stato.Required = true;
            news.addField(Stato);

            RichTextBox Testo = new RichTextBox("Testo", "Testo_News");
            Testo.Value = "<p> <br /> </p>";
            Testo.Required = true;
            Testo.CurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID;
            news.addField(Testo);       
            #endregion      

            #region NewsImageInEvidenza

            NetFormTable newsImageInEvidenza = new NetFormTable("news_highlightimages", "ID", this.CurrentFolder.StructureNetwork.Connection);
            newsImageInEvidenza.CssClass = "news_highlightimages news_addform";
            newsImageInEvidenza.InfoText = "Immagine in evidenza";


            NetImagePicker docImg = new NetImagePicker("Immagine", "RifDoc", this.PageData);
			docImg.Value = "0";
            docImg.CurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID;
            newsImageInEvidenza.addField(docImg);

            NetTextBoxPlus descNewsImage = new NetTextBoxPlus("Descrizione", "Description", NetTextBoxPlus.ContentTypes.NormalText);
            descNewsImage.CssClass = "description";
            descNewsImage.DefaultControlTag = "div";
            descNewsImage.Rows = 3;
            descNewsImage.Size = 40;
            newsImageInEvidenza.addField(descNewsImage);

            NetDropDownList imageAlign = new NetDropDownList("Allineamento", "Align");
            imageAlign.DefaultControlTag = "div";
            imageAlign.CssClass = "imageAlign";
            imageAlign.addItem("Sinistra", "left");
            imageAlign.addItem("Centrato", "center");
            imageAlign.addItem("Destra", "right");            
            newsImageInEvidenza.addField(imageAlign);

            NetCheckBox showinModulo = new NetCheckBox("Mostra nel modulo", "ShowInModulo");
            newsImageInEvidenza.addField(showinModulo);

            NetCheckBox showinList = new NetCheckBox("Mostra nell'elenco", "ShowInTeaser");
            newsImageInEvidenza.addField(showinList);

            NetCheckBox showinDetail = new NetCheckBox("Mostra nel dettaglio", "ShowInDetail");
            newsImageInEvidenza.addField(showinDetail); 

            #endregion

            NetForm form = new NetForm();            

            form.AddTable(contentTable);
            form.AddTable(news);
            
            form.AddTable(netDocs);
            form.AddTable(docs);
            form.AddTable(newsDoc);

            form.AddTable(langs);

            form.AddTable(newsImageInEvidenza);
            NetFormTable review = NewsDocument.ReviewFormTable(PageData);
            review.CssClass = "netformtable_review";

            if (!this.CurrentFolder.ReviewEnabled && !this.CurrentFolder.IsRootFolder)
            {
                contentForDocLang = new NetExternalField("docs_contents", "Content_DocLang");
                langs.ExternalFields.Add(contentForDocLang);               
            }
            else
            {
                contentForDocLang = new NetExternalField("docs_contents", "Contenuto_Revisione");
                review.ExternalFields.Add(contentForDocLang);
                form.AddTable(review);
            }

            content = new NetExternalField("docs_contents", "id_News");
            news.ExternalFields.Add(content);

            docsToNetDocs = new NetExternalField("network_docs", "id_Doc");
            docs.ExternalFields.Add(docsToNetDocs);

            docsToWebDocsDoc = new NetExternalField("network_docs", "id_docs");
            newsDoc.ExternalFields.Add(docsToWebDocsDoc);

            #region PostBack

            string errors = "";
            if (PageData.IsPostBack)
            {
                if (PageData.SubmitSource == "send")
                {
                    errors = form.serializedInsert(HttpContext.Current.Request.Form);
                    //string name = HttpContext.Current.Request.Form["Name_Doc"].ToString().Replace("'", "''");
                    NetUtility.RequestVariable name = new NetUtility.RequestVariable("Name_Doc", NetUtility.RequestVariable.RequestType.Form);

                    if (netDocs.LastInsert > 0 && name.IsValid())
                    {
                        bool done = false;                  

                        NewsDocument document = null;

                        using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                        using (ITransaction tx = sess.BeginTransaction())
                        {
                            try
                            {                                
                                //NetCms.Structure.WebFS.Document document = DocumentBusinessLogic.GetById(netDocs.LastInsert, sess);
                                document = DocumentBusinessLogic.GetById(netDocs.LastInsert, sess) as NewsDocument;                                
                                document.PhysicalName = name.OriginalValue;//document.PhysicalName = name.StringValue;                                
                                document.DocLangs.Where(x => x.Lang == PageData.DefaultLang).First().Label = name.OriginalValue;
                                DocumentBusinessLogic.SaveOrUpdateDocument(document, sess);                              

                                if (this.CurrentFolder.ReviewEnabled)
                                {
                                    DateTime ora = DateTime.Now;
                                    string date = ora.Year + "-" + ora.Month + "-" + ora.Day + " " + ora.Hour + "." + ora.Minute + "." + ora.Second;
                                    //this.CurrentFolder.Network.Connection.Execute("UPDATE revisioni SET Data_Revisione='" + date + "' WHERE id_Revisione = " + review.LastInsert);
                                    Review revisione = ReviewBusinessLogic.GetReviewById(review.LastInsert, sess);
                                    revisione.Data = DateTime.Now;                                    
                                    
                                    ReviewBusinessLogic.SaveOrUpdateReview(revisione, sess);
                                    
                                    docContentID = revisione.Content.ID;
                                }

                                if (newsImageInEvidenza.LastInsert > 0)
                                {
                                    Review rev = ReviewBusinessLogic.GetReviewById(review.LastInsert, sess);
                                    if (rev != null)
                                    {
                                        NewsHighLigthImage highLightImg = NewsBusinessLogic.GetHighLigthImageByID(newsImageInEvidenza.LastInsert);
                                        if (highLightImg != null)
                                        {
                                            NewsBusinessLogic.SaveHighLightImageContent(rev.Content.ID, highLightImg, sess);
                                        }
                                    }
                                }

                                if (PageData.CurrentConfig.CmsAppAttachToContentStatus == CmsConfigs.Model.Config.AttachToContentStatus.Enabled)
                                    Files.AttachesBusinessLogic.SaveAttaches(document.ID, docContentID, null, attachControl.UploadField, null,this.CurrentNetwork.ID, document.Autore, sess);                                

                                G2Core.Common.RequestVariable tags = new G2Core.Common.RequestVariable("Tags");
                                NetCms.Structure.WebFS.Tags.AddTagsForDoc(tags.StringValue, docs.LastInsert.ToString(), this.CurrentFolder.StructureNetwork.Connection);

                                done = true;
                                tx.Commit();                       
                            }
                            catch (Exception ex)
                            {
                                tx.Rollback();
                                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                            }                       
                        }                   

                        if (done == false)
                            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non � stato possibile inserire i dati della news nel database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "119");
                        else Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente ha creato con successo una news nel portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label, "", "");

                        //SISTEMARE CON CACHE 2� LIVELLO
                        //this.CurrentFolder.Invalidate();
                        NetCms.GUI.PopupBox.AddSessionMessage(errors, PostBackMessagesType.Success, form.LastOperation == NetForm.LastOperationValues.OK ? true : false);
                    }
                    else
                    {
                        NetCms.GUI.PopupBox.AddMessage(errors, PostBackMessagesType.Error);
                    }                    
                }
            }
            #endregion

            #region Creazione dei controlli

            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerText = "Nuova News";
            fieldset.Controls.Add(legend);

            HtmlGenericControl contentDiv = new HtmlGenericControl("div");
            contentDiv.Attributes["class"] = "contentForm newsform col-md-9";

            contentDiv.Controls.Add(docs.getForm());
            contentDiv.Controls.Add(langs.getForm());
            contentDiv.Controls.Add(news.getForm());                    

            if (CurrentFolder.ReviewEnabled)
                contentDiv.Controls.Add(review.getForm());
            
            if (PageData.CurrentConfig.CmsAppAttachToContentStatus == CmsConfigs.Model.Config.AttachToContentStatus.Enabled)
            contentDiv.Controls.Add(attachControl.GetControl());

            HtmlGenericControl colDxDiv = new HtmlGenericControl("div");
            colDxDiv.Attributes["class"] = "coldx col-md-3 border-left";

            colDxDiv.Controls.Add(newsImageInEvidenza.getForm());           

            HtmlGenericControl pButtons = new HtmlGenericControl("p");
            pButtons.Attributes["class"] = "buttonscontainer";

            Button bt = new Button();
            bt.Text = "Crea News";
            bt.ID = "send";
            bt.CssClass = "button";
            bt.OnClientClick = "javascript: setSubmitSource('send')";
            pButtons.Controls.Add(bt);

            contentDiv.Controls.Add(pButtons);

            WebControl row = new WebControl(HtmlTextWriterTag.Div);
            row.CssClass = "row";

            row.Controls.Add(contentDiv);
            row.Controls.Add(colDxDiv);

            fieldset.Controls.Add(row);
         
            div.Controls.Add(fieldset);
            
            #endregion



            return div;
        }
    }
}