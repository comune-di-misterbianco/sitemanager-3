﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using NetCms.Vertical;
using NetCms.DBSessionProvider;
using System.Reflection;
using NHibernate;
using NetCms.Vertical.BusinessLogic;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomAnagrafeSetup : VerticalApplicationSetup
    {
        public override string[,] Pages
        {
            get
            {
                return null;
            }
        }

        public override string[,] Criteria
        {
            get 
            {
                return null;
            }
        }

        public override bool IsNetworkDependant
        {
            get { return false; }
        }

        public CustomAnagrafeSetup(VerticalApplication baseApp)
            : base(baseApp)
        {
        }

        protected override bool LocalInstall(bool isInsidePackage = false)
        {
            return true;
        }
    }
}
