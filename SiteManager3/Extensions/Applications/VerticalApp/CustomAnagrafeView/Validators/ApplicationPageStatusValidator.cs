﻿namespace NetCms.Users.CustomAnagrafe
{
    public abstract class ApplicationPageStatusValidator
    {
        public abstract string ErrorMessage { get; }
        public abstract string ErrorTitle { get; }

        public abstract bool Validate();
    }
}
