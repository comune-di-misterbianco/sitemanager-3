﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using NetService.Utility.UI;
using GenericDAO.DAO.Utility;
using G2Core.Common;
using System.Collections.Generic;
using NHibernate;
using NetCms.DBSessionProvider;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("/default.aspx")]
    public class MainPage : CustomAnagrafePage
    {

        public bool EmptyRecordRequest
        {
            get
            {
                RequestVariable request = new RequestVariable("action", RequestVariable.RequestType.QueryString);
                if (request.IsValidString && request.StringValue == "empty")
                    _EmptyRecordRequest = true;
                return _EmptyRecordRequest;
            }
        }
        private bool _EmptyRecordRequest = false;

        public override string PageTitle
        {
            get { return "Anagrafiche Custom"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("anagrafica_new.aspx", NetCms.GUI.Icons.Icons.Vcard_Add, "Crea Anagrafica Custom"));
            this.Toolbar.Buttons.Add(new ToolbarButton("groups_export.aspx", NetCms.GUI.Icons.Icons.Group_Go, "Esporta Gruppi"));         
            this.Toolbar.Buttons.Add(new ToolbarButton("anagrafica_backimport.aspx", NetCms.GUI.Icons.Icons.Group_Go, "Importa utenti backoffice"));
            this.Toolbar.Buttons.Add(new ToolbarButton("anagrafica_frontimport.aspx", NetCms.GUI.Icons.Icons.Group_Go, "Importa utenti frontend"));           

        }

        public NhRequestObject<CustomAnagrafe> CustomAnagrafe { get; private set; }

        public MainPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            CustomAnagrafe = new NhRequestObject<CustomAnagrafe>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "ca");
            SetToolbars();
        }

        public override WebControl BuildControl()
        {
            if (EliminationRequest && CustomAnagrafe.Exists)
            {
                EliminationConfirmControl delcontrol = DeleteControl("Conferma eliminazione Anagrafica Custom '" + CustomAnagrafe.Instance.Nome + "'", "Sei sicuro di voler eliminare l'anagrafica custom '" + CustomAnagrafe.Instance.Nome + "'", NetCms.Vertical.PagesHandler.GetPageAddress(typeof(MainPage)));
                delcontrol.ConfirmButton.Click += new EventHandler(ConfirmButton_Click);
                return delcontrol;
            }
            else if (EmptyRecordRequest && CustomAnagrafe.Exists)
            {
                EliminationConfirmControl delcontrol = DeleteControl("Conferma eliminazione record dall'Anagrafica Custom '" + CustomAnagrafe.Instance.Nome + "'", "Sei sicuro di voler eliminare tutti i record dell'anagrafica custom '" + CustomAnagrafe.Instance.Nome + "'", NetCms.Vertical.PagesHandler.GetPageAddress(typeof(MainPage)));
                delcontrol.ConfirmButton.Click += new EventHandler(ConfirmButton_Click);
                return delcontrol;
            }
            return GetTableControl();
        }

        void ConfirmButton_Click(object sender, EventArgs e)
        {
            bool isOk = true;
            string errorText = "";

            if (EliminationRequest)
            {
                using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = session.BeginTransaction())
                {
                    try
                    {
                        CustomAnagrafe customAnagrafeToDelete = CustomAnagrafeBusinessLogic.GetCustomAnagrafeById(CustomAnagrafe.Instance.ID, session);

                        if (CustomAnagrafeBusinessLogic.CountAnagraficheByCustomAnagrafe(customAnagrafeToDelete, session) == 0)
                        {
                            CustomAnagrafeBusinessLogic.DeleteCustomAnagrafe(customAnagrafeToDelete, session);
                        }
                        else
                        {
                            NetCms.GUI.PopupBox.AddSessionMessage("Per eliminare una Anagrafe Custom elimina prima tutti i record al suo interno", PostBackMessagesType.Error, true);
                        }
                        tx.Commit();
                    }
                    catch (Exception ex)
                    {
                        isOk = false;
                        errorText = ex.Message;
                        tx.Rollback();
                    }

                    if (isOk)
                    {
                        CustomAnagrafeBusinessLogic.DropAnagrafica(CustomAnagrafe.Instance, Application.Conn);
                        NetCms.GUI.PopupBox.AddSessionMessage("Anagrafe custom eliminata con successo", PostBackMessagesType.Success, true);
                    }
                    else
                        NetCms.GUI.PopupBox.AddMessage("Errore nella cancellazione dell'anagrafica: " + errorText, PostBackMessagesType.Error);
                }
            }
            else if (EmptyRecordRequest)
            {

                using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = session.BeginTransaction())
                {
                    try
                    {
                        CustomAnagrafe customAnagrafeToDelete = CustomAnagrafeBusinessLogic.GetCustomAnagrafeById(CustomAnagrafe.Instance.ID, session);
                        CustomAnagrafeBusinessLogic.DeleteAnagrafiche(customAnagrafeToDelete, session);
                        tx.Commit();
                    }
                    catch (Exception ex)
                    {
                        errorText = ex.Message;
                        isOk = false;
                        tx.Rollback();
                    }
                }

                if (isOk)
                    NetCms.GUI.PopupBox.AddSessionMessage("Record anagrafe custom eliminati con successo", PostBackMessagesType.Success, true);
                else
                    NetCms.GUI.PopupBox.AddMessage("Errore nella cancellazione dei record: " + errorText, PostBackMessagesType.Error);

            }
        }

        private WebControl GetTableControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);
            
            ArTable caTable = new ArTable();
            caTable.EnablePagination = true;
            caTable.RecordPerPagina = 10;
            caTable.InnerTableCssClass = "tab";
            caTable.NoRecordMsg = "Nessuna anagrafica creata";

            PaginationHandler pagination = new PaginationHandler(caTable.RecordPerPagina, CustomAnagrafeBusinessLogic.CountCustomAnagrafe(), caTable.PaginationKey);
            caTable.PagesControl = pagination;

            var anagrafiche = CustomAnagrafeBusinessLogic.FindCustomAnagrafe(((pagination.CurrentPage - 1) * pagination.PageSize), pagination.PageSize, "Nome", false);

            caTable.Records = anagrafiche;

            ArTextColumn nomeAnagrafica = new ArTextColumn("Nome", "Nome");
            caTable.AddColumn(nomeAnagrafica);

            ArOptionsColumn installataAnagrafica = new ArOptionsColumn("Installata", "Installata", new string[] { "NO", "SI" });
            caTable.AddColumn(installataAnagrafica);

            ArActionColumn gestione = new ArActionColumn("Campi", ArActionColumn.Icons.Details, "anagrafica_gest.aspx?ca={ID}");
            caTable.AddColumn(gestione);

            ArActionColumn frontendGest = new ArActionColumn("Frontend", ArActionColumn.Icons.Details, "campifrontend_gest.aspx?ca={ID}");
            caTable.AddColumn(frontendGest);

            ArActionColumn groupGest = new ArActionColumn("Raggruppamenti", ArActionColumn.Icons.Details, "customgroup_gest.aspx?ca={ID}");
            caTable.AddColumn(groupGest);

            ArActionColumn associaServizi = new ArActionColumn("Servizi", ArActionColumn.Icons.Details, "applicazioni.aspx?ca={ID}");
            caTable.AddColumn(associaServizi);

            ArButtonColumn actions = new ArButtonColumn("Azione", "ID");
            actions.PreRenderHandler = new EventHandler(Check_Install_State);
            actions.ClickHandler = new EventHandler(Action_Click);
            caTable.AddColumn(actions);

            ArActionColumn modify = new ArActionColumn("Modifica", ArActionColumn.Icons.Edit, "anagrafica_mod.aspx?ca={ID}");
            caTable.AddColumn(modify);

            ArActionColumn exportAnagraficaStructure = new ArActionColumn("Esporta Struttura", ArActionColumn.Icons.Go, "anagrafica_structure_export.aspx?ca={ID}");
            caTable.AddColumn(exportAnagraficaStructure);

            ArActionColumn exportAnagraficaDati = new ArActionColumn("Esporta Dati", "", "anagrafica_dati_export.aspx?ca={ID}");
            caTable.AddColumn(exportAnagraficaDati);

            ArCustomAnagrafeActionColumn emptyRecord = new ArCustomAnagrafeActionColumn("Svuota Record", ArActionColumn.Icons.Delete, "default.aspx?action=empty&ca={ID}");
            caTable.AddColumn(emptyRecord);

            ArCustomAnagrafeActionColumn delete = new ArCustomAnagrafeActionColumn("Elimina", ArActionColumn.Icons.Delete, "default.aspx?action=delete&ca={ID}");
            caTable.AddColumn(delete);

            control.Controls.Add(caTable);

            Button updateSessionFactory = new Button();
            updateSessionFactory.ID = "update_session";
            updateSessionFactory.Text = "Aggiorna Session Factory";
            updateSessionFactory.Click += new EventHandler(updateSessionFactory_Click);

            G2Core.XhtmlControls.Fieldset fieldset = new G2Core.XhtmlControls.Fieldset("Aggiorna la session factory per rendere persistenti i cambiamenti");
            fieldset.Controls.Add(updateSessionFactory);

            control.Controls.Add(fieldset);
            return control;

        }

        void updateSessionFactory_Click(object sender, EventArgs e)
        {
            CustomAnagrafeBusinessLogic.UpdateMappingConfiguration();

            CustomAnagrafeBusinessLogic.DropDeletedCustomField(Application.Conn);

            NetCms.Diagnostics.Diagnostics.Redirect(NetCms.Vertical.PagesHandler.GetPageAddress(typeof(MainPage)));
        }

        void Action_Click(object sender, EventArgs e)
        {
            int anagraficaId = int.Parse((sender as Button).ID.Remove(0, 3));
            CustomAnagrafe ca = CustomAnagrafeBusinessLogic.GetCustomAnagrafeById(anagraficaId);

            bool ok = false;
            try
            {
                if (ca.Installata)
                {
                    CustomAnagrafeBusinessLogic.BuildAnagrafeXmlMappingRemove(ca);
                    ca.Installata = false;
                }
                else
                {
                    CustomAnagrafeBusinessLogic.BuildAnagrafeXmlMappingInsert(ca);
                    ca.Installata = true;
                }

                ok = CustomAnagrafeBusinessLogic.SaveOrUpdateCustomAnagrafe(ca);
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, "Errore nell'aggiornamento delle anagrafiche custom");
                NetCms.GUI.PopupBox.AddMessage("Errore nell'aggiornamento delle anagrafiche custom", PostBackMessagesType.Error);
            }

            if (ok)
            {
                NetCms.Diagnostics.Diagnostics.Redirect(NetCms.Vertical.PagesHandler.GetPageAddress(typeof(MainPage)));
            }
        }

        

        void Check_Install_State(object sender, EventArgs e)
        {
            int anagraficaId = int.Parse((sender as Button).ID.Remove(0,3));
            CustomAnagrafe ca = CustomAnagrafeBusinessLogic.GetCustomAnagrafeById(anagraficaId);
            if (ca.Installata)
                (sender as Button).Text = "Disinstalla";
            else (sender as Button).Text = "Installa nel sistema";
        }
    }
}