﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ArTable;
using System.Web.UI.WebControls;

namespace NetCms.Users.CustomAnagrafe.View
{
    public class ArCustomAnagrafeActionColumn : ArActionColumn
    {
        public ArCustomAnagrafeActionColumn(string caption, Icons icon, string href)
            : base(caption, icon, href)
        { }

        protected override TableCell GetCellControl(object objectInstance, string label, string href, Icons icon)
        {
            NetCms.Users.CustomAnagrafe.CustomAnagrafe ca = objectInstance as NetCms.Users.CustomAnagrafe.CustomAnagrafe;
            TableCell td = new TableCell();

            if (!ca.Installata)
            {
                if (icon == Icons.Custom)
                {
                    if (!string.IsNullOrEmpty(this.CssClass)) td.CssClass = this.CssClass;
                }
                else
                    td.CssClass = "action " + icon.ToString().ToLower();

                string anchorCssClassHtml = string.IsNullOrEmpty(AnchorCssClass) ? "" : "class=\"" + this.AnchorCssClass + "\"";
                string anchorTitle = string.IsNullOrEmpty(ToolTip) ? "" : "title=\"" + base.ParseString(objectInstance, ToolTip) + "\"";

                td.Text = "<a " + anchorCssClassHtml + " href=\"" + base.ParseString(objectInstance, href) + "\"><span>" + base.ParseString(objectInstance, label) + "</span></a>";
            }
            return td;
        }
    }
}
