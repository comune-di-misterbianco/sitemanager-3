﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ArTable;
using System.Web.UI.WebControls;
using NetCms.Vertical;

namespace NetCms.Users.CustomAnagrafe.View
{
    public class ArApplicationTextColumn : ArTextColumn
    {
        public ArApplicationTextColumn(string caption, string key)
            : base(caption, key)
        {
            DataType = DataTypes.Integer;
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            int idApplicazione = int.Parse(FilterCellValue(FilterDataType(GetValue(objectInstance, this.Key))));

            VerticalApplication vapp = VerticalApplicationsPool.GetByID(idApplicazione);

            TableCell td = new TableCell();
            if (!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;
            td.Text = "<span>" + vapp.Label + "</span>";

            return td;
        }
    }
}
