﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI;
using NetCms.Users.CustomAnagrafe;

namespace NetCms.Users.CustomAnagrafe.View
{
    public class ApplicationsForm : VfManager
    {

        private VfDropDown _Servizi;
        public VfDropDown Servizi
        {
            get
            {
                if (_Servizi == null)
                {
                    _Servizi = new VfDropDown("servizi", "Servizio");
                    _Servizi.Required = true;
                    _Servizi.Options.Add("", "");

                    foreach (NetCms.Vertical.VerticalApplication vapp in NetCms.Vertical.VerticalApplicationsPool.ActiveAssemblies)
                    {
                        if (vapp.HasService)
                            _Servizi.Options.Add(vapp.Label, vapp.ID.ToString());
                    }
                }
                return _Servizi;
            }
        }


        public ApplicationsForm(string controlID)
            : base(controlID)
        {
            this.CssClass = "Axf";
            this.SubmitButton.Text = "Aggiungi";

            this.Fields.Add(Servizi);
        }
    }
}
