﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;

namespace NetCms.Users.CustomAnagrafe.View
{
    public class CCKForm : VfManager
    {
        public CustomField Field
        {
            get;
            set;
        }

        public CCKForm(string controlId, CustomField field = null)
            : base(controlId)
        {
            this.Field = field;

            bool isUpdate = field != null;

            this.AddSubmitButton = false;

            if (!isUpdate)
                this.Fields.Add(TipoCampo);

            if (!isUpdate && !string.IsNullOrEmpty(_TipoCampo.PostBackValue))
            {
                this.AddSubmitButton = true;

                CustomField.TipoProprietà tipo = (CustomField.TipoProprietà) Enum.Parse(typeof(CustomField.TipoProprietà), _TipoCampo.PostBackValue);

                switch (tipo)
                {
                    case CustomField.TipoProprietà.Testo:
                        {
                            Field = new CustomTextField();
                            break;
                        }
                    case CustomField.TipoProprietà.Dropdown:
                        {
                            Field = new CustomDropdownField();
                            break;
                        }
                    case CustomField.TipoProprietà.Checkbox:
                        {
                            Field = new CustomCheckboxField();
                            break;
                        }
                    case CustomField.TipoProprietà.Data:
                        {
                            Field = new CustomDateField();
                            break;
                        }
                    case CustomField.TipoProprietà.Comune:
                        {
                            Field = new CustomComuneField();
                            break;
                        }
                    case CustomField.TipoProprietà.CheckboxList:
                        {
                            Field = new CustomCheckboxListField();
                            break;
                        }
                    case CustomField.TipoProprietà.DropdownProvincia:
                        {
                            Field = new CustomProvinciaField();
                            break;
                        }
                    case CustomField.TipoProprietà.File:
                        {
                            Field = new CustomFileField();
                            break;
                        }
                    case CustomField.TipoProprietà.MoltiAMoltiAClasseMappata:
                        {
                            Field = new CustomManyToManyToMappedTableField();
                            break;
                        }
                    case CustomField.TipoProprietà.MoltiAUnoAClasseMappata:
                        {
                            Field = new CustomForeignKeyToMappedTableField();
                            break;
                        }
                    //case CustomField.TipoProprietà.NRPC:
                    //    {
                    //        Field = new CustomNRPCField();
                    //        break;
                    //    }

                }
                this.Fields.AddRange(Field.FormConfigurazione);
            }

            if (isUpdate)
            {
                this.AddSubmitButton = true;
                this.AddBackButton = true;
                this.BackButton.Text = "Indietro";
                this.Fields.AddRange(Field.FormConfigurazione);
            }
        }

        private VfDropDown TipoCampo
        {
            get
            {
                if (_TipoCampo == null)
                {
                    _TipoCampo = new VfDropDown("TipoCampo", "Tipologia Campo");
                    _TipoCampo.Required = true;
                    _TipoCampo.BindField = false;
                    _TipoCampo.DropDownList.AutoPostBack = true;

                    _TipoCampo.Options.Add("", "");
                    foreach(CustomField.TipoProprietà tipoProp in Enum.GetValues(typeof(CustomField.TipoProprietà)))
                    {
                        _TipoCampo.Options.Add(tipoProp.ToString(),tipoProp.ToString());
                    }
                    
                }
                return _TipoCampo;
            }
        }
        private VfDropDown _TipoCampo;
    }
}
