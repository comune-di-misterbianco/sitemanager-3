﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI;
using NetCms.Users.CustomAnagrafe;

namespace NetCms.Users.CustomAnagrafe.View
{
    public class CustomGroupFieldsForm : VfManager
    {

        private CustomGroup group;
        private CustomField field;

        private VfDropDown _CustomField;
        public VfDropDown CustomField
        {
            get
            {
                if (_CustomField == null)
                {
                    _CustomField = new VfDropDown("CustomField", "Campo Custom");


                    if (field != null)
                    {
                        _CustomField.Options.Add(field.Name, field.ID.ToString());
                        _CustomField.DefaultValue = field.ID.ToString();
                    }
                    else
                    {
                        _CustomField.Options.Add("", "");
                        foreach (CustomField cf in CustomAnagrafeBusinessLogic.FindCustomFieldNotYetAddedToGroups(group.CustomAnagrafe))
                            _CustomField.Options.Add(cf.Name, cf.ID.ToString());
                    }

                    _CustomField.Required = true;
                }
                return _CustomField;
            }
        }

        private VfTextBox posizioneField;
        public VfTextBox PosizioneField
        {
            get 
            {
                if (posizioneField == null)
                {
                    posizioneField = new VfTextBox("PositionInGroup", "Posizione", InputTypes.Number);

                    if (field != null)
                        posizioneField.DefaultValue = field.PositionInGroup;

                    posizioneField.Required = true;
                }
 
                return posizioneField; 
            }
        }

        public VfCheckBoxList ValoriDropDownDiDipendenza
        {
            get
            {
                if (_ValoriDropDownDiDipendenza == null)
                {
                    _ValoriDropDownDiDipendenza = new VfCheckBoxList("ValoriDropDown", "Elenco valori per cui il campo è attivo ",true);
                    _ValoriDropDownDiDipendenza.AtLeastOneRequired = true;
           
                    ICollection<CustomDependencyItem> Dipendenze = null;
                    if (field != null)
                        Dipendenze = field.DependencyValuesCollection;

                    CustomDropdownField ddField = CustomAnagrafeBusinessLogic.GetCustomDropdownFieldById(group.CustomAnagrafe.CustomFields.First(y => y.Name == group.DependencyCustomFieldName).ID);
                    foreach (CustomDropdownValue value in ddField.Values)
                    {
                        if (Dipendenze != null && Dipendenze.Count(x=>x.DependencyDDValue.Equals(value.Value)) > 0)
                            _ValoriDropDownDiDipendenza.addItem(value.Label, value.Value,true);
                        else
                            _ValoriDropDownDiDipendenza.addItem(value.Label, value.Value, false);
                    }

                }

                return _ValoriDropDownDiDipendenza;
            }
        }

        private VfCheckBoxList _ValoriDropDownDiDipendenza;


        public VfCheckBoxList RichiestoPer
        {
            get
            {
                if (_RichiestoPer == null)
                {
                    _RichiestoPer = new VfCheckBoxList("RichiestoPer", "Campo richiesto per ", true);
                    
                    //CustomDropdownField ddField = CustomAnagrafeBusinessLogic.GetCustomDropdownFieldById(group.DependencyCustomFieldId.Value);
                    //foreach (CustomDropdownValue value in ddField.Values)
                    //{
                    //    _RichiestoPer.addItem(value.Label, value.Value, false);
                    //}

                    ICollection<CustomDependencyItem> Dipendenze = null;
                    if (field != null)
                        Dipendenze = field.DependencyValuesCollection;

                    CustomDropdownField ddField = CustomAnagrafeBusinessLogic.GetCustomDropdownFieldById(group.CustomAnagrafe.CustomFields.First(y => y.Name == group.DependencyCustomFieldName).ID);
                    foreach (CustomDropdownValue value in ddField.Values)
                    {
                        if (Dipendenze != null && Dipendenze.Count(x => x.DependencyDDValue.Equals(value.Value)) > 0 && Dipendenze.First(x=>x.DependencyDDValue.Equals(value.Value)).isRequiredForValue)
                            _RichiestoPer.addItem(value.Label, value.Value, true);
                        else
                            _RichiestoPer.addItem(value.Label, value.Value, false);
                    }

                }

                return _RichiestoPer;
            }
        }

        private VfCheckBoxList _RichiestoPer;

        public CustomGroupFieldsForm(string controlID, bool isPostBack, CustomGroup cg, CustomField cf = null)
            : base(controlID, isPostBack)
        {
            this.group = cg;
            this.field = cf;

            this.TitleControl.Controls.Add(new LiteralControl("In questa pagina è possibile definire i custom field che verranno utilizzati nel gruppo " + group.Nome + " e la relativa posizione."));
            this.CssClass = "Axf";
            if (this.field != null)
                this.SubmitButton.Text = "Modifica";
            else
                this.SubmitButton.Text = "Aggiungi";

            this.Fields.Add(CustomField);
            this.Fields.Add(PosizioneField);

            if (group.DependsOnAnotherField)
            {
                this.Fields.Add(ValoriDropDownDiDipendenza);
                this.Fields.Add(RichiestoPer);
            }
        }
    }
}
