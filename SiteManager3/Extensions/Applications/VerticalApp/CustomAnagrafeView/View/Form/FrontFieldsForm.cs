﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI;
using NetCms.Users.CustomAnagrafe;

namespace NetCms.Users.CustomAnagrafe.View
{
    public class FrontFieldsForm : VfManager
    {

        private CustomAnagrafe anagrafe;


        private VfDropDown _VfField;
        public VfDropDown VfField
        {
            get
            {
                if (_VfField == null)
                {
                    _VfField = new VfDropDown("custom_field", "Campo Custom");
                    _VfField.Required = true;
                    IEnumerable<CustomField> fields = CustomAnagrafeBusinessLogic.FindFrontCustomFieldsByAnagrafe(anagrafe);
                    _VfField.Options.Add("","");
                    foreach(CustomField cf in fields)
                        _VfField.Options.Add(cf.Name, cf.ID.ToString());
                }
                return _VfField;
            }
        }


        public FrontFieldsForm(string controlID, CustomAnagrafe ca)
            : base(controlID)
        {
            this.anagrafe = ca;

            this.TitleControl.Controls.Add(new LiteralControl("In questa pagina è possibile definire i custom field che verranno utilizzati nel frontend per visualizzare l'utente che ha effettuato l'accesso. <br/> I campi verranno utilizzati nell'ordine in cui sono visualizzati nella tabella sottostante, separati da uno spazio."));
            this.CssClass = "Axf";
            this.SubmitButton.Text = "Aggiungi";

            this.Fields.Add(VfField);
        }
    }
}
