﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Reflection;

namespace NetCms.Users.CustomAnagrafe.View
{
    public class ExportToDbForm : VfManager
    {
        public ExportToDbForm(string controlId, bool checkRegRequest = false)
            : base(controlId)
        {
            this.Fields.Add(Server);
            this.Fields.Add(Database);
            this.Fields.Add(TcpPort);
            this.Fields.Add(UserName);
            this.Fields.Add(Password);
            this.Fields.Add(Assemblies);

            if (checkRegRequest)
                this.Fields.Add(ManageWebRequest);
        }

        private VfTextBox Server
        {
            get
            {
                if (_Server == null)
                {
                    _Server = new VfTextBox("Server", "Server", InputTypes.Text);
                    _Server.Required = true;
                    _Server.MaxLenght = 150;
                }
                return _Server;
            }
        }
        private VfTextBox _Server;

        private VfTextBox Database
        {
            get
            {
                if (_Database == null)
                {
                    _Database = new VfTextBox("Database", "Database", InputTypes.Text);
                    _Database.Required = true;
                    _Database.MaxLenght = 150;
                }
                return _Database;
            }
        }
        private VfTextBox _Database;

        private VfTextBox TcpPort
        {
            get
            {
                if (_TcpPort == null)
                {
                    _TcpPort = new VfTextBox("TcpPort", "Porta TCP", InputTypes.Number);
                    _TcpPort.Required = true;
                    _TcpPort.MaxLenght = 5;
                    _TcpPort.DefaultValue = "3306";
                }
                return _TcpPort;
            }
        }
        private VfTextBox _TcpPort;

        private VfTextBox UserName
        {
            get
            {
                if (_UserName == null)
                {
                    _UserName = new VfTextBox("UserName", "UserName", InputTypes.Text);
                    _UserName.Required = true;
                    _UserName.MaxLenght = 150;
                }
                return _UserName;
            }
        }
        private VfTextBox _UserName;

        private VfTextBox Password
        {
            get
            {
                if (_Password == null)
                {
                    _Password = new VfTextBox("Password", "Password", InputTypes.Text);
                    _Password.Required = true;
                    _Password.MaxLenght = 150;
                }
                return _Password;
            }
        }
        private VfTextBox _Password;

        private VfCheckBoxList Assemblies
        {
            get
            {
                if (_Assemblies == null)
                {
                    _Assemblies = new VfCheckBoxList("Assemblies", "Assemblies", false);
                    _Assemblies.AtLeastOneRequired = true;
                    foreach (Assembly ass in DBSessionProvider.FluentSessionProvider.FluentAssemblies)
                    {
                        _Assemblies.addItem(ass.FullName, ass.FullName, false);
                    }
                }
                return _Assemblies;
            }
        }
        private VfCheckBoxList _Assemblies;

        private VfCheckBox ManageWebRequest 
        {
            get
            {
                if (_ManageWebRequest == null)
                {
                    _ManageWebRequest = new VfCheckBox("ManageWebRequestField", "Il sistema remoto gestisce le richieste di registrazione?");
                    _ManageWebRequest.Required = false;
                }
                return _ManageWebRequest;
            }
        }
        private VfCheckBox _ManageWebRequest;

    }
}
