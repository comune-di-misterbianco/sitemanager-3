﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI;

namespace NetCms.Users.CustomAnagrafe.View
{
    public class CustomAnagrafeForm : VfManager
    {
        private CustomAnagrafe Anagrafe;
        public CustomAnagrafeForm(string controlId, CustomAnagrafe ca = null)
            : base(controlId)
        {
            this.Anagrafe = ca;

            if (ca== null)
                this.Fields.Add(Nome);

            this.Fields.Add(Rendering);
            this.Fields.Add(Group);
            this.Fields.Add(UseSpid);
            this.Fields.Add(TestoEmailAttivazione);
            this.Fields.Add(TestoEmailRegistrazioneNegata);
            this.Fields.Add(MostraInFront);
            this.Fields.Add(AggiungiCaptcha);
            this.Fields.Add(EmailUserName);
            EmailUserName.Attributes.Add("onClick", "ShowHide()");

            if (ca != null)
            {
                if (ca.EmailAsUserName)
                {
                    Username.Attributes.Add("style","display : none");                  
                    
                }
                this.Fields.Add(Username);

                if (!ca.AggiungiInformativaPrivacy)
                {
                    LabelInformativa.Attributes.Add("style", "display : none");
                    TestoInformativa.Attributes.Add("style", "display : none");
                    DomandaAccettazioneInformativa.Attributes.Add("style", "display : none");
                    NumeroColonneAreaInformativa.Attributes.Add("style", "display : none");
                    NumeroRigheAreaInformativa.Attributes.Add("style", "display : none");
                }
            }

            this.Fields.Add(ConfermaMail);

            this.Fields.Add(AggiungiInformativa);
            AggiungiInformativa.Attributes.Add("onClick", "ShowHideInformativa()");
            this.Fields.Add(LabelInformativa);
            this.Fields.Add(TestoInformativa);
            this.Fields.Add(DomandaAccettazioneInformativa);
            this.Fields.Add(NumeroColonneAreaInformativa);
            this.Fields.Add(NumeroRigheAreaInformativa);

            if (System.Web.Configuration.WebConfigurationManager.AppSettings["ManageRegRequests"] == "1")
                this.Fields.Add(NotifyGroup);

        }

        private VfTextBox Nome
        {
            get
            {
                if (_Nome == null)
                {
                    _Nome = new VfTextBox("Nome", "Nome", "Nome", InputTypes.Text);
                    _Nome.Required = true;
                    _Nome.MaxLenght = 50;
                }
                return _Nome;
            }
        }
        private VfTextBox _Nome;

        private VfDropDown Rendering
        {
            get
            {
                if (_Rendering == null)
                {
                    _Rendering = new VfDropDown("RenderType", "Modalità di Rendering delle form di registrazione", "RenderType");
                    _Rendering.Options.Add("", "");
                    foreach (CustomAnagrafe.RenderTypes type in Enum.GetValues(typeof(CustomAnagrafe.RenderTypes)))
                    {
                        _Rendering.Options.Add(type.ToString(), ((int)type).ToString());
                    }
                }
                return _Rendering;
            }
        }
        private VfDropDown _Rendering;

        private VfDropDown Group
        {
            get
            {
                if (_Group == null)
                {
                    _Group = new VfDropDown("Group", "Gruppo in cui verranno inseriti gli utenti che si registrano da Frontend", "DefaultGroup");
                    ICollection<Group> groups = GroupsBusinessLogic.FindAllOrderedByTree();
                    _Group.Options.Add("", "");
                    foreach(Group group in groups)
                        _Group.Options.Add((group.Parent != null && group.Parent.Name.ToLower() != "root"? group.Parent.Name + " -> ":"") + group.Name,group.ID.ToString());

                }
                return _Group;
            }
        }
        private VfDropDown _Group;

        private VfRichTextBox TestoEmailAttivazione
        {
            get
            {
                if (_TestoEmailAttivazione == null)
                {
                    _TestoEmailAttivazione = new VfRichTextBox("TestoEmailAtt", "Testo Email attivazione", "TestoEmailAttivazione");
                    _TestoEmailAttivazione.FullPage = true;
                }
                return _TestoEmailAttivazione;
            }

        }
        private VfRichTextBox _TestoEmailAttivazione;

        private VfRichTextBox TestoEmailRegistrazioneNegata
        {
            get
            {
                if (_TestoEmailRegistrazioneNegata == null)
                {
                    _TestoEmailRegistrazioneNegata = new VfRichTextBox("TestoEmailRej", "Testo Email registrazione negata", "TestoEmailRegistrazioneNegata");
                    _TestoEmailAttivazione.FullPage = true;
                }
                return _TestoEmailRegistrazioneNegata;
            }

        }
        private VfRichTextBox _TestoEmailRegistrazioneNegata;

        private VfDropDown Username
        {
            get
            {
                if (_Username == null)
                {
                    _Username = new VfDropDown("Username", "Campo che verra' utilizzato come username");
                    _Username.ID = "campo_username";
                    _Username.Required = false;
                    _Username.BindField = false;                   
                    _Username.Options.Add("", "");
                    if (Anagrafe != null)
                    {
                        foreach (CustomField cf in Anagrafe.CustomFields.Where(x => x.Cancellato==false))
                        {
                            _Username.Options.Add(cf.Name, cf.ID.ToString());
                            if(cf.IsUsername)
                                _Username.DefaultValue=cf.ID.ToString();
                        }
                    }
                }
                return _Username;
            }
        }
        private VfDropDown _Username;

        private VfCheckBox _MostraInFront;
        private VfCheckBox MostraInFront
        {
            get {
                if (_MostraInFront == null)
                {
                    _MostraInFront = new VfCheckBox("VisibileInFrontend", "Mostra nella pagina di registrazione del frontend ", "VisibileInFrontend");                    
                }
                return _MostraInFront;
            }
        }

        private VfCheckBox _AggiungiCaptcha;
        private VfCheckBox AggiungiCaptcha
        {
            get
            {
                if (_AggiungiCaptcha == null)
                {
                    _AggiungiCaptcha = new VfCheckBox("AggiungiCaptcha", "Aggiungi validazione con captcha ", "ValidaConCaptcha");
                }
                return _AggiungiCaptcha;
            }
        }

        private VfCheckBox _EmailUserName;
        protected VfCheckBox EmailUserName
        {
            get
            {
                if (_EmailUserName == null)
                {
                    _EmailUserName = new VfCheckBox("EmailAsUserName", "Usa email come username", "EmailAsUserName");                    
                }
                return _EmailUserName;
            }
        }

        private VfCheckBox _ConfermaMail;
        private VfCheckBox ConfermaMail
        {
            get
            {
                if (_ConfermaMail == null)
                {
                    _ConfermaMail = new VfCheckBox("ConfermaMail", "Richiedi conferma E-mail ", "RichiediConfermaEmail");
                }
                return _ConfermaMail;
            }
        }

        private VfCheckBox _AggiungiInformativa;
        private VfCheckBox AggiungiInformativa
        {
            get
            {
                if (_AggiungiInformativa == null)
                {
                    _AggiungiInformativa = new VfCheckBox("AggiungiInformativa", "Aggiungi informativa sulla privacy ", "AggiungiInformativaPrivacy");
                }
                return _AggiungiInformativa;
            }
        }

        private VfTextBox LabelInformativa
        {
            get
            {
                if (_LabelInformativa == null)
                {
                    _LabelInformativa = new VfTextBox("LabelInformativa", "Label informativa sulla privacy", "LabelInformativaPrivacy", InputTypes.Text);
                    _LabelInformativa.ID = "LabelInformativaContainer";
                    _LabelInformativa.Required = false;
                    _LabelInformativa.DefaultValue = VfTermsPrivacyRadioButton.LabelInformativaPrivacy;
                    _LabelInformativa.Columns = 60;
                }
                return _LabelInformativa;
            }
        }
        private VfTextBox _LabelInformativa;

        private VfTextBox TestoInformativa
        {
            get
            {
                if (_TestoInformativa == null)
                {
                    _TestoInformativa = new VfTextBox("TestoInformativa", "Testo informativa sulla privacy", "TestoInformativaPrivacy", InputTypes.Text);
                    _TestoInformativa.ID = "TestoInformativaContainer";
                    _TestoInformativa.Required = false;
                    _TestoInformativa.MaxLenght = 160000;
                    _TestoInformativa.TextMode = System.Web.UI.WebControls.TextBoxMode.MultiLine;
                    _TestoInformativa.Rows = 10;
                    _TestoInformativa.Columns = 60;
                }
                return _TestoInformativa;
            }

        }
        private VfTextBox _TestoInformativa;


        private VfTextBox DomandaAccettazioneInformativa
        {
            get
            {
                if (_DomandaAccettazioneInformativa == null)
                {
                    _DomandaAccettazioneInformativa = new VfTextBox("DomandaAccettazioneInformativa", "Testo domanda accettazione informativa sulla privacy", "DomandaAccettazioneInformativaPrivacy", InputTypes.Text);
                    _DomandaAccettazioneInformativa.ID = "DomandaAccettazioneInformativaContainer";
                    _DomandaAccettazioneInformativa.Required = false;
                    _DomandaAccettazioneInformativa.DefaultValue = VfTermsPrivacyRadioButton.DomandaAccettazionePrivacy;
                    _DomandaAccettazioneInformativa.Columns = 60;
                }
                return _DomandaAccettazioneInformativa;
            }
        }
        private VfTextBox _DomandaAccettazioneInformativa;

        private VfTextBox NumeroColonneAreaInformativa
        {
            get
            {
                if (_NumeroColonneAreaInformativa == null)
                {
                    _NumeroColonneAreaInformativa = new VfTextBox("NumeroColonneAreaInformativa", "Numero caratteri da visualizzare nell'area informativa (Impostazione grafica - 0 valore di default)", "NumeroCaratteriDaVisualizzareAreaInformativa", InputTypes.Number);
                    _NumeroColonneAreaInformativa.ID = "NumeroColonneAreaInformativaContainer";
                    _NumeroColonneAreaInformativa.Required = false;
                    _NumeroColonneAreaInformativa.DefaultValue = 0;
                }
                return _NumeroColonneAreaInformativa;
            }
        }
        private VfTextBox _NumeroColonneAreaInformativa;

        private VfTextBox NumeroRigheAreaInformativa
        {
            get
            {
                if (_NumeroRigheAreaInformativa == null)
                {
                    _NumeroRigheAreaInformativa = new VfTextBox("NumeroRigheAreaInformativa", "Numero righe da visualizzare nell'area informativa (Impostazione grafica - 0 valore di default)", "NumeroRigheDaVisualizzareAreaInformativa", InputTypes.Number);
                    _NumeroRigheAreaInformativa.ID = "NumeroRigheAreaInformativaContainer";
                    _NumeroRigheAreaInformativa.Required = false;
                    _NumeroRigheAreaInformativa.DefaultValue = 0;
                }
                return _NumeroRigheAreaInformativa;
            }
        }
        private VfTextBox _NumeroRigheAreaInformativa;

        private VfDropDown NotifyGroup
        {
            get
            {
                if (_NotifyGroup == null)
                {
                    _NotifyGroup = new VfDropDown("NotifyGroup", "Gruppo di utenti che riceveranno le notifiche per la gestione delle richieste di registrazione da Frontend", "RegRequestNotifyGroup");
                    ICollection<Group> groups = GroupsBusinessLogic.FindAllOrderedByTree();
                    _NotifyGroup.Options.Add("", "");
                    foreach (Group group in groups)
                        _NotifyGroup.Options.Add((group.Parent != null && group.Parent.Name.ToLower() != "root" ? group.Parent.Name + " -> " : "") + group.Name, group.ID.ToString());
                }
                return _NotifyGroup;
            }
        }
        private VfDropDown _NotifyGroup;


        private VfCheckBox _UseSpid;
        private VfCheckBox UseSpid
        {
            get
            {
                if (_UseSpid == null)
                {
                    _UseSpid = new VfCheckBox("UseSpid", "L'anagrafica fa uso di spid per l'autenticazione: ", "UseSpid");
                }
                return _UseSpid;
            }
        }
    }
}
