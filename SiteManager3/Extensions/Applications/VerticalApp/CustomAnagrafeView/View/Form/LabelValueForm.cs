﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;

namespace NetCms.Users.CustomAnagrafe.View
{
    public class LabelValueForm<T> : VfManager
    {
        public LabelValue<T> Value
        {
            get;
            set;
        }

        public LabelValueForm(string controlId)
            : base(controlId)
        {
            this.Fields.Add(Text_Label);
            this.Fields.Add(Text_Value);
        }

        private VfTextBox _Text_Label;
        public VfTextBox Text_Label
        {
            get
            {
                if(_Text_Label == null)
                    _Text_Label = new VfTextBox("label", "Descrizione valore", "Label", InputTypes.Text) { Required = true };
                return _Text_Label;
            }
        }
        private VfTextBox _Text_Value;
        public VfTextBox Text_Value
        {
            get
            {
                if (_Text_Value == null)
                    _Text_Value = new VfTextBox("value", "Valore", "Value", InputTypes.Text) { Required = true };
                return _Text_Value;
            }
        }

    }
}
