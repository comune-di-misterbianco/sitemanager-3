﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI;

namespace NetCms.Users.CustomAnagrafe.View
{
    public class CustomGroupForm : VfManager
    {
        private CustomAnagrafe _CustomAnagrafe;

        private CustomGroup _Group;

        public CustomGroupForm(string controlId, CustomAnagrafe ca, CustomGroup group = null)
            : base(controlId)
        {
            _CustomAnagrafe = ca;
            _Group = group;
            this.Fields.Add(Nome);
            this.Fields.Add(Descrizione);
            this.Fields.Add(Posizione);
            this.Fields.Add(DipendeDaUnAltroCampo);
            this.Fields.Add(CustomFieldDiDipendenza);

            DipendeDaUnAltroCampo.Attributes.Add("onClick", "ShowHide()");

            if (group != null)
            {
                if (!group.DependsOnAnotherField)
                {
                    CustomFieldDiDipendenza.Attributes.Add("style", "display : none");

                }
                //this.Fields.Add(CustomFieldDiDipendenza);
            }
            else
                CustomFieldDiDipendenza.Attributes.Add("style", "display : none");

            
        }

        private VfTextBox Nome
        {
            get
            {
                if (_Nome == null)
                {
                    _Nome = new VfTextBox("Nome", "Nome", "Nome", InputTypes.Text);
                    _Nome.Required = true;
                    _Nome.MaxLenght = 50;
                }
                return _Nome;
            }
        }
        private VfTextBox _Nome;

        private VfTextBox Descrizione
        {
            get
            {
                if (_Descrizione == null)
                {
                    _Descrizione = new VfTextBox("Descrizione", "Descrizione", "Descrizione", InputTypes.Text);
                    _Descrizione.Required = false;
                    _Descrizione.MaxLenght = 150;
                }
                return _Descrizione;
            }
        }
        private VfTextBox _Descrizione;

        private VfTextBox Posizione
        {
            get
            {
                if (_Posizione == null)
                {
                    _Posizione = new VfTextBox("Posizione", "Posizione", "Posizione", InputTypes.Number);
                    _Posizione.Required = true;
                }
                return _Posizione;
            }
        }
        private VfTextBox _Posizione;



        protected VfCheckBox DipendeDaUnAltroCampo
        {
            get
            {
                if (_DipendeDaUnAltroCampo == null)
                {
                    _DipendeDaUnAltroCampo = new VfCheckBox("DependsOnAnotherField", "Il raggruppamento dipende dai valori di un custom field ", "DependsOnAnotherField");
                }
                return _DipendeDaUnAltroCampo;
            }
        }
        private VfCheckBox _DipendeDaUnAltroCampo;

        private VfDropDown _CustomFieldDiDipendenza;
        public VfDropDown CustomFieldDiDipendenza
        {
            get
            {
                if (_CustomFieldDiDipendenza == null)
                {
                    _CustomFieldDiDipendenza = new VfDropDown("DependencyCustomFieldId", "Campo Custom ", "DependencyCustomFieldName");//, "DependencyCustomFieldId");
                    _CustomFieldDiDipendenza.ID = "CFDependencyContainer";
                    _CustomFieldDiDipendenza.InfoControl.Controls.Add(new LiteralControl("Campo di tipo drop down dai cui valori dipendono i campi custom inseriti in questo raggruppamento"));
                    _CustomFieldDiDipendenza.Options.Add("", "");
                    
                    foreach (CustomField cf in _CustomAnagrafe.CustomFields.Where(x=>x.Tipo == CustomField.TipoProprietà.Dropdown))
                        _CustomFieldDiDipendenza.Options.Add(cf.Name, cf.Name);


                    _CustomFieldDiDipendenza.Required = false;
                }
                return _CustomFieldDiDipendenza;
            }
        }
    }
}
