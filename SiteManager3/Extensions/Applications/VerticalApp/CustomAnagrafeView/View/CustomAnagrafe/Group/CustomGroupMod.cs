﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.GUI;
using GenericDAO.DAO.Utility;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetService.Utility.ValidatedFields;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("/customgroup_mod.aspx")]
    public class CustomGroupMod : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Modifica Gruppo"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Group_Edit; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("customgroup_gest.aspx?ca=" + CustomGroup.Instance.CustomAnagrafe.ID, NetCms.GUI.Icons.Icons.Group, "Gestione Gruppi Anagrafica Custom"));
        }

        public NhRequestObject<CustomGroup> CustomGroup { get; private set; }

        public CustomGroupMod(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            CustomGroup = new NhRequestObject<CustomGroup>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "cg");
            OuterScripts.Add(@"<script type='text/javascript'> function ShowHide() {
                            if (document.getElementById('DependsOnAnotherField').checked ) {                                 
                                 $('#CFDependencyContainer').show();
                                } else {                                        
                                         $('#CFDependencyContainer').hide();
                                         } return false;}</script>");

            this.StatusValidators.Add(new NhRequestObjectValidator<CustomGroup>(CustomGroup));
            SetToolbars();
            this.CustomGroupForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
            this.CustomGroupForm.BackButton.Click += new EventHandler(BackButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.CustomGroupForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                CustomGroup group = CustomGroup.Instance;

                string nomeGruppo = this.CustomGroupForm.Fields.First(x => x.Key == "Nome").PostBackValue;

                if (group.CustomAnagrafe.CustomGroups.Where(x => x.ID != group.ID && x.Nome == nomeGruppo).Count() == 0)
                {
                    bool status = CustomAnagrafeBusinessLogic.ModCustomGroup(this.CustomGroupForm, group);
                    if (status)
                    {
                        NetCms.GUI.PopupBox.AddSessionMessage("Gruppo modificato con successo.", PostBackMessagesType.Success, true);
                    }
                    else
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
                }
                else
                    NetCms.GUI.PopupBox.AddMessage("Esiste già un gruppo con lo stesso nome per la custom anagrafica corrente.", PostBackMessagesType.Notify);
            }
        }

        void BackButton_Click(object sender, EventArgs e)
        {
            NetCms.Diagnostics.Diagnostics.Redirect(NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomGroupGest)) + "?ca=" + CustomGroup.Instance.CustomAnagrafe.ID);
        }

        public CustomGroupForm CustomGroupForm
        {
            get
            {
                if (_CustomGroupForm == null)
                {
                    _CustomGroupForm = new CustomGroupForm("CustomGroupForm",CustomGroup.Instance.CustomAnagrafe,CustomGroup.Instance);
                    _CustomGroupForm.CssClass = "Axf";
                    _CustomGroupForm.SubmitButton.Text = "Modifica";
                    _CustomGroupForm.DataBindAdvanced(CustomGroup.Instance);
                }
                return _CustomGroupForm;
            }
        }
        private CustomGroupForm _CustomGroupForm;


        public override WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.Controls.Add(CustomGroupForm);
            return control;
        }

    }
}

