﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using GenericDAO.DAO.Utility;
using System;
using NetService.Utility.ValidatedFields;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using NetService.Utility.UI;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("/customgroup_gest.aspx")]
    public class CustomGroupGest : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Gestione Gruppi di Campi dell'Anagrafica Custom '" + CustomAnagrafe.Instance.Nome + "'"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Group; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Gestione Anagrafiche Custom"));
        }

        public NhRequestObject<CustomAnagrafe> CustomAnagrafe { get; private set; }

        public NhRequestObject<CustomGroup> CustomGroup { get; private set; }

        public CustomGroupGest(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            CustomAnagrafe = new NhRequestObject<CustomAnagrafe>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "ca");
            CustomGroup = new NhRequestObject<CustomGroup>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "cg");

            OuterScripts.Add(@"<script type='text/javascript'> function ShowHide() {
                            if (document.getElementById('DependsOnAnotherField').checked ) {                                 
                                 $('#CFDependencyContainer').show();
                                } else {                                        
                                         $('#CFDependencyContainer').hide();
                                         } return false;}</script>");

            this.StatusValidators.Add(new NhRequestObjectValidator<CustomAnagrafe>(CustomAnagrafe));
            SetToolbars();
            this.Form.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.Form.IsValid == VfGeneric.ValidationStates.Valid)
            {
                bool status = CustomAnagrafeBusinessLogic.CreateCustomGroup(this.Form, CustomAnagrafe.Instance);
                if (status)
                    NetCms.GUI.PopupBox.AddSessionMessage("Gruppo creato con successo.", PostBackMessagesType.Success, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomGroupGest)) + "?ca=" + CustomAnagrafe.Instance.ID);
                else
                    NetCms.GUI.PopupBox.AddMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
            }
        }

        public override WebControl BuildControl()
        {
            if (EliminationRequest && CustomGroup.Exists)
            {
                EliminationConfirmControl delcontrol = DeleteControl("Conferma eliminazione Gruppo '" + CustomGroup.Instance.Nome + "' dall'anagrafica " + CustomAnagrafe.Instance.Nome, "Sei sicuro di voler eliminare il gruppo '" + CustomGroup.Instance.Nome + "' ?", NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomGroupGest)) + "?ca=" + CustomAnagrafe.Instance.ID);
                delcontrol.ConfirmButton.Click += new EventHandler(ConfirmEliminationButton_Click);
                return delcontrol;
            }
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.Controls.Add(Form);
            control.Controls.Add(GetTableControl());
            return control;
        }

        void ConfirmEliminationButton_Click(object sender, EventArgs e)
        {
            //CustomAnagrafe.Instance.CustomGroups.Remove(CustomGroup.Instance);
            //CustomAnagrafeBusinessLogic.SaveOrUpdateCustomAnagrafe(CustomAnagrafe.Instance);

            CustomAnagrafeBusinessLogic.DeleteCustomGroup(CustomGroup.Instance);

            NetCms.GUI.PopupBox.AddSessionMessage("Gruppo eliminato con successo", PostBackMessagesType.Success, true);
        }

        public CustomGroupForm Form
        {
            get
            {
                if (_Form == null)
                {
                    _Form = new CustomGroupForm("Custom Group",CustomAnagrafe.Instance);
                    _Form.SubmitButton.Text = "Crea gruppo";
                }
                return _Form;
            }
        }
        private CustomGroupForm _Form;

        private WebControl GetTableControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);

            ArTable customGroupsTable = new ArTable();
            customGroupsTable.EnablePagination = true;
            customGroupsTable.RecordPerPagina = 20;
            customGroupsTable.InnerTableCssClass = "tab";
            customGroupsTable.NoRecordMsg = "Nessun gruppo nell'anagrafica " + CustomAnagrafe.Instance.Nome;

            PaginationHandler pagination = new PaginationHandler(customGroupsTable.RecordPerPagina, CustomAnagrafeBusinessLogic.CountCustomGroupsByAnagrafe(CustomAnagrafe.Instance), customGroupsTable.PaginationKey);
            customGroupsTable.PagesControl = pagination;

            customGroupsTable.Records = CustomAnagrafe.Instance.CustomGroups;

            ArTextColumn nomeCustomGroup = new ArTextColumn("Nome", "Nome");
            customGroupsTable.AddColumn(nomeCustomGroup);

            ArTextColumn descrizioneCustomGroup = new ArTextColumn("Descrizione", "Descrizione");
            customGroupsTable.AddColumn(descrizioneCustomGroup);

            ArTextColumn posizioneCustomGroup = new ArTextColumn("Posizione", "Posizione");
            customGroupsTable.AddColumn(posizioneCustomGroup);

            ArActionColumn fieldCustomGroup = new ArActionColumn("Gestione Campi Associati", ArActionColumn.Icons.Details, "customgroup_fields_gest.aspx?cg={ID}");
            customGroupsTable.AddColumn(fieldCustomGroup);

            ArActionColumn modify = new ArActionColumn("Modifica", ArActionColumn.Icons.Edit, "customgroup_mod.aspx?cg={ID}");
            customGroupsTable.AddColumn(modify);

            ArActionColumn delete = new ArActionColumn("Elimina", ArActionColumn.Icons.Delete, "customgroup_gest.aspx?action=delete&ca={CustomAnagrafe.ID}&cg={ID}");
            customGroupsTable.AddColumn(delete);

            control.Controls.Add(customGroupsTable);

            return control;

        }

    }
}
