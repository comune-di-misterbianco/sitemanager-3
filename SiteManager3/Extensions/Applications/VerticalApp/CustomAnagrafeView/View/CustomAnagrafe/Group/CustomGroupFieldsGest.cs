﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;
using NetCms.GUI;
using GenericDAO.DAO.Utility;
using NetService.Utility.ValidatedFields;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using NetService.Utility.UI;


namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("/customgroup_fields_gest.aspx")]
    public class CustomGroupFieldsGest : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Gestione Campi associato al gruppo '" + CustomGroup.Instance.Nome + "'"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Brick; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("customgroup_gest.aspx?ca=" + CustomGroup.Instance.CustomAnagrafe.ID, NetCms.GUI.Icons.Icons.Group, "Gestione Gruppi Anagrafica Custom"));
        }

        public NhRequestObject<CustomGroup> CustomGroup { get; private set; }

        public NhRequestObject<CustomField> CustomField { get; private set; }

        public CustomGroupFieldsGest(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            CustomGroup = new NhRequestObject<CustomGroup>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "cg");
            CustomField = new NhRequestObject<CustomField>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "cf");

            this.StatusValidators.Add(new NhRequestObjectValidator<CustomGroup>(CustomGroup));
            SetToolbars();
            this.Form.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.Form.IsValid == VfGeneric.ValidationStates.Valid)
            {
                CustomField cf = CustomAnagrafeBusinessLogic.GetCustomFieldById(int.Parse(this.Form.CustomField.PostBackValue));
                cf.PositionInGroup = int.Parse(this.Form.PosizioneField.PostBackValue);
                cf.CustomGroup = CustomGroup.Instance;

                if (CustomGroup.Instance.DependsOnAnotherField)
                {
                    cf.DependencyValuesCollection.Clear();
                    if (Form.ValoriDropDownDiDipendenza != null)
                    {
                        List<string> ValoriDipendenza = Form.ValoriDropDownDiDipendenza.PostbackValueObject as List<string>;
                        List<string> RequiredForValue = Form.RichiestoPer.PostbackValueObject as List<string>;
                        foreach (string valore in ValoriDipendenza)
                        {
                            CustomDependencyItem item = new CustomDependencyItem();
                            item.DependencyDDValue = valore;
                            item.isRequiredForValue = RequiredForValue.Contains(valore);
                            item.DependentField = cf;
                            cf.DependencyValuesCollection.Add(item);
                        }
                    }
                }

                CustomGroup.Instance.CustomFields.Add(cf);

                bool ok = CustomAnagrafeBusinessLogic.SaveOrUpdateCustomField(cf);
                if (ok)
                    NetCms.GUI.PopupBox.AddSessionMessage("Campo custom inserito con successo nel gruppo.", PostBackMessagesType.Success, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomGroupFieldsGest)) + "?cg=" + CustomGroup.Instance.ID);
                else
                    NetCms.GUI.PopupBox.AddMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);

            }
        }

        public override WebControl BuildControl()
        {
            if (EliminationRequest && CustomField.Exists)
            {
                EliminationConfirmControl delcontrol = DeleteControl("Conferma eliminazione Campo Custom '" + CustomField.Instance.Name + "' dal Gruppo " + CustomGroup.Instance.Nome, "Sei sicuro di voler eliminare il campo custom '" + CustomField.Instance.Name + "' ?", NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomGroupFieldsGest)) + "?cg=" + CustomGroup.Instance.ID);
                delcontrol.ConfirmButton.Click += new EventHandler(ConfirmEliminationButton_Click);
                return delcontrol;
            }
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.Controls.Add(Form);
            control.Controls.Add(GetTableControl());
            return control;
        }

        void ConfirmEliminationButton_Click(object sender, EventArgs e)
        {
            CustomField cf = CustomField.Instance;
            cf.PositionInGroup = 0;
            cf.CustomGroup = null;
            cf.DependencyValuesCollection.Clear();
            CustomGroup.Instance.CustomFields.Remove(cf);
            CustomAnagrafeBusinessLogic.SaveOrUpdateCustomField(cf);

            NetCms.GUI.PopupBox.AddSessionMessage("Campo custom eliminato con successo dal gruppo", PostBackMessagesType.Success, true);
        }

        public CustomGroupFieldsForm Form
        {
            get
            {
                if (_Form == null)
                {
                    _Form = new CustomGroupFieldsForm("Custom Field selector", this.IsPostBack, CustomGroup.Instance,CustomField.Instance);
                }
                return _Form;
            }
        }

        private CustomGroupFieldsForm _Form;

        private WebControl GetTableControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);

            ArTable customFieldsTable = new ArTable();
            customFieldsTable.EnablePagination = true;
            customFieldsTable.RecordPerPagina = 20;
            customFieldsTable.InnerTableCssClass = "tab";
            customFieldsTable.NoRecordMsg = "Nessun campo custom nel gruppo " + CustomGroup.Instance.Nome;

            PaginationHandler pagination = new PaginationHandler(customFieldsTable.RecordPerPagina, CustomAnagrafeBusinessLogic.CountCustomFieldsByCustomGroup(CustomGroup.Instance), customFieldsTable.PaginationKey);
            customFieldsTable.PagesControl = pagination;

            customFieldsTable.Records = CustomGroup.Instance.CustomFields.OrderBy(x=>x.PositionInGroup);

            ArTextColumn nomeCustomField = new ArTextColumn("Nome", "Name");
            customFieldsTable.AddColumn(nomeCustomField);

            ArTextColumn posizioneCustomField = new ArTextColumn("Posizione nel gruppo", "PositionInGroup");
            customFieldsTable.AddColumn(posizioneCustomField);

            ArActionColumn modify = new ArActionColumn("Modifica", ArActionColumn.Icons.Edit, "customgroup_fields_gest.aspx?action=modify&cg={CustomGroup.ID}&cf={ID}");
            customFieldsTable.AddColumn(modify);

            ArActionColumn delete = new ArActionColumn("Elimina", ArActionColumn.Icons.Delete, "customgroup_fields_gest.aspx?action=delete&cg={CustomGroup.ID}&cf={ID}");
            customFieldsTable.AddColumn(delete);

            control.Controls.Add(customFieldsTable);

            return control;

        }

    }
}
