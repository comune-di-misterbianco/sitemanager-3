﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using System;
using NetService.Utility.ValidatedFields;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("anagrafica_new.aspx")]
    public class CustomAnagrafeAdd : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Creazione Nuova Anagrafica Custom"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User_Add; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Gestione Anagrafiche Custom"));
        }

        public CustomAnagrafeAdd(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            SetToolbars();
            OuterScripts.Add(@"<script type='text/javascript'> function ShowHideInformativa() {
                            if (document.getElementById('AggiungiInformativa').checked ) {                                 
                                 $('#LabelInformativaContainer').show();
                                 $('#TestoInformativaContainer').show();
                                 $('#DomandaAccettazioneInformativaContainer').show();
                                 $('#NumeroColonneAreaInformativaContainer').show();
                                 $('#NumeroRigheAreaInformativaContainer').show();
                                } else {                                        
                                         $('#LabelInformativaContainer').hide();
                                         $('#TestoInformativaContainer').hide();
                                         $('#DomandaAccettazioneInformativaContainer').hide();
                                         $('#NumeroColonneAreaInformativaContainer').hide();
                                         $('#NumeroRigheAreaInformativaContainer').hide();
                                         } return false;}

                            $(function () {
                                ShowHideInformativa(); });
                            </script>");

            this.CustomAnagrafeForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.CustomAnagrafeForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                bool presente = false;
                string nome = CustomAnagrafeForm.Fields.Find(x => x.Key == "Nome").PostBackValue;
                if (CustomAnagrafeBusinessLogic.CustomAnagrafePresenteInInsert(nome))
                {
                    presente = true;
                    NetCms.GUI.PopupBox.AddMessage("Anagrafica custom già presente.", PostBackMessagesType.Notify);
                }

                if (!presente)
                {
                    bool status = CustomAnagrafeBusinessLogic.CreateAnagraficaCustom(this.CustomAnagrafeForm);
                    if (status)
                        NetCms.GUI.PopupBox.AddSessionMessage("Anagrafica custom inserita con successo.", PostBackMessagesType.Success, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(MainPage)));
                    else
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
                }
            }
        }

        public CustomAnagrafeForm CustomAnagrafeForm
        {
            get
            {
                if (_CustomAnagrafeForm == null)
                {
                    _CustomAnagrafeForm = new CustomAnagrafeForm("CustomAnagrafeForm");
                    _CustomAnagrafeForm.CssClass = "Axf";
                    _CustomAnagrafeForm.SubmitButton.Text = "Salva";
                }
                return _CustomAnagrafeForm;
            }
        }
        private CustomAnagrafeForm _CustomAnagrafeForm;

        public override WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.Controls.Add(CustomAnagrafeForm);
            return control;
        }
    }
}
