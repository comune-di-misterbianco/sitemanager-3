﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using GenericDAO.DAO.Utility;
using System;
using NetService.Utility.ValidatedFields;
using NetCms.Users.CustomAnagrafe;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using G2Core.Common;
using NetService.Utility.UI;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("customdropdownvalue_gest.aspx")]
    public class CustomDropdownValueMod : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Modifica Valore Dropdown"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User_Edit; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("anagrafica_gest.aspx?ca="+DropDownValue.Instance.CustomField.Anagrafe.ID, NetCms.GUI.Icons.Icons.Vcard, "Generazione Campi Anagrafica Custom"));
        }

        public NhRequestObject<CustomDropdownValue> DropDownValue { get; private set; }

        public CustomDropdownValueMod(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            DropDownValue = new NhRequestObject<CustomDropdownValue>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "va");
            this.StatusValidators.Add(new NhRequestObjectValidator<CustomDropdownValue>(DropDownValue));
           
            SetToolbars();
            this.DDValueForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
            this.DDValueForm.BackButton.Click += new EventHandler(BackButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.DDValueForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                bool status = CustomAnagrafeBusinessLogic.ModDropdownValue(DDValueForm, DropDownValue.Instance);
                if (status)
                    NetCms.GUI.PopupBox.AddSessionMessage("Valore dropdown modificato con successo.", PostBackMessagesType.Success, true);
                else
                    NetCms.GUI.PopupBox.AddMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
            }
        }

        void BackButton_Click(object sender, EventArgs e)
        {
            NetCms.Diagnostics.Diagnostics.Redirect(NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomAnagrafeCCK)) + "?cf=" + DropDownValue.Instance.CustomField.ID);
        }


        private LabelValueForm<CustomDropdownField> _DDValueForm;
        public LabelValueForm<CustomDropdownField> DDValueForm
        {
            get
            {
                if (_DDValueForm == null)
                {
                    _DDValueForm = new LabelValueForm<CustomDropdownField>("DDValueForm");
                    _DDValueForm.CssClass = "Axf";
                    _DDValueForm.SubmitButton.Text = "Modifica";
                    _DDValueForm.DataBind(DropDownValue.Instance);
                    _DDValueForm.InfoControl.Controls.Add(new LiteralControl("Modifica valore nella Dropdown"));
                }
                return _DDValueForm;
            }
        }

        public override WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            if (EliminationRequest && DropDownValue.Exists)
            {
                EliminationConfirmControl delcontrol = DeleteControl("Conferma eliminazione valore Dropdown '" + DropDownValue.Instance.Label + "' del campo Dropdown '" + DropDownValue.Instance.CustomField.Name + "'", "Sei sicuro di voler eliminare il valore '" + DropDownValue.Instance.Label + "'", NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomDropdownFieldMod)) + "?cf=" + DropDownValue.Instance.CustomField.ID);
                delcontrol.ConfirmButton.Click += new EventHandler(ConfirmEliminationButton_Click);
                return delcontrol;
            }
            control.Controls.Add(DDValueForm);
            return control;
        }

        void ConfirmEliminationButton_Click(object sender, EventArgs e)
        {
            CustomAnagrafeBusinessLogic.DeleteDropdownValue(DropDownValue.Instance);
            NetCms.GUI.PopupBox.AddSessionMessage("Valore della dropdown eliminato con successo", PostBackMessagesType.Success, true);
        }

    }
}
