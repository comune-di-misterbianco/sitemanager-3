﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using GenericDAO.DAO.Utility;
using System;
using NetService.Utility.ValidatedFields;
using NetCms.Users.CustomAnagrafe;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using G2Core.Common;
using NetService.Utility.UI;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("customcheckboxlistvalue_gest.aspx")]
    public class CustomCheckboxListValueMod : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Modifica Valore CheckboxList"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User_Edit; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("anagrafica_gest.aspx?ca=" + CheckboxListValue.Instance.CustomField.Anagrafe.ID, NetCms.GUI.Icons.Icons.Vcard, "Generazione Campi Anagrafica Custom"));
        }

        public NhRequestObject<CustomCheckboxListValue> CheckboxListValue { get; private set; }

        public CustomCheckboxListValueMod(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            CheckboxListValue = new NhRequestObject<CustomCheckboxListValue>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "va");
            this.StatusValidators.Add(new NhRequestObjectValidator<CustomCheckboxListValue>(CheckboxListValue));

            SetToolbars();
            this.CBLValueForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
            this.CBLValueForm.BackButton.Click += new EventHandler(BackButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.CBLValueForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                bool status = CustomAnagrafeBusinessLogic.ModCheckboxListValue(CBLValueForm, CheckboxListValue.Instance);
                if (status)
                    NetCms.GUI.PopupBox.AddSessionMessage("Valore checkboxList modificato con successo.", PostBackMessagesType.Success, true);
                else
                    NetCms.GUI.PopupBox.AddMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
            }
        }

        void BackButton_Click(object sender, EventArgs e)
        {
            NetCms.Diagnostics.Diagnostics.Redirect(NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomAnagrafeCCK)) + "?cf=" + CheckboxListValue.Instance.CustomField.ID);
        }


        private LabelValueForm<CustomCheckboxListField> _CBLValueForm;
        public LabelValueForm<CustomCheckboxListField> CBLValueForm
        {
            get
            {
                if (_CBLValueForm == null)
                {
                    _CBLValueForm = new LabelValueForm<CustomCheckboxListField>("CBLValueForm");
                    _CBLValueForm.CssClass = "Axf";
                    _CBLValueForm.SubmitButton.Text = "Modifica";
                    _CBLValueForm.DataBind(CheckboxListValue.Instance);
                    _CBLValueForm.InfoControl.Controls.Add(new LiteralControl("Modifica valore nella CheckboxList"));
                }
                return _CBLValueForm;
            }
        }

        public override WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            if (EliminationRequest && CheckboxListValue.Exists)
            {
                EliminationConfirmControl delcontrol = DeleteControl("Conferma eliminazione valore CheckboxList '" + CheckboxListValue.Instance.Label + "' del campo CheckboxList '" + CheckboxListValue.Instance.CustomField.Name + "'", "Sei sicuro di voler eliminare il valore '" + CheckboxListValue.Instance.Label + "'", NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomCheckboxListFieldMod)) + "?cf=" + CheckboxListValue.Instance.CustomField.ID);
                delcontrol.ConfirmButton.Click += new EventHandler(ConfirmEliminationButton_Click);
                return delcontrol;
            }
            control.Controls.Add(CBLValueForm);
            return control;
        }

        void ConfirmEliminationButton_Click(object sender, EventArgs e)
        {
            CustomAnagrafeBusinessLogic.DeleteCheckboxListValue(CheckboxListValue.Instance);
            NetCms.GUI.PopupBox.AddSessionMessage("Valore della checkboxList eliminato con successo", PostBackMessagesType.Success, true);
        }

    }
}
