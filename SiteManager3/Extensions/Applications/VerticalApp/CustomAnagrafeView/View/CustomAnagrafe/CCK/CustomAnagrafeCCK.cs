﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using GenericDAO.DAO.Utility;
using System;
using NetService.Utility.ValidatedFields;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using NetService.Utility.UI;
using System.Linq;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("/anagrafica_gest.aspx")]
    public class CustomAnagrafeCCK : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Generazione Campi Anagrafica Custom '" + CustomAnagrafe.Instance.Nome + "'"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Brick; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Gestione Anagrafiche Custom"));
        }

        public NhRequestObject<CustomAnagrafe> CustomAnagrafe { get; private set; }

        public NhRequestObject<CustomField> CustomField { get; private set; }

        public CustomAnagrafeCCK(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            CustomAnagrafe = new NhRequestObject<CustomAnagrafe>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "ca");
            CustomField = new NhRequestObject<CustomField>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "cf");

            this.StatusValidators.Add(new NhRequestObjectValidator<CustomAnagrafe>(CustomAnagrafe));
            SetToolbars();
            this.CCKForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.CCKForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                string fieldLabel = "";

                if (this.CCKForm.Field.FormConfigurazione.Where(x => x.Key == "label").Count() > 0)
                    fieldLabel = this.CCKForm.Field.FormConfigurazione.First(x => x.Key == "label").PostBackValue;
                else if (this.CCKForm.Field.CommonFields.Where(x => x.Key == "label").Count() > 0)
                    fieldLabel = this.CCKForm.Field.CommonFields.First(x => x.Key == "label").PostBackValue;

                if (CustomAnagrafe.Instance.CustomFields.Where(x => x.Name == fieldLabel && x.Cancellato == false).Count() == 0)
                {
                    bool ok = CustomAnagrafeBusinessLogic.CreateCustomField(this.CCKForm, this.CCKForm.Field, this.CustomAnagrafe.Instance);
                    if (ok)
                        NetCms.GUI.PopupBox.AddSessionMessage("Campo custom inserito con successo.", PostBackMessagesType.Success, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomAnagrafeCCK)) + "?ca=" + CustomAnagrafe.Instance.ID);
                    else
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
                }
                else
                    NetCms.GUI.PopupBox.AddMessage("Esiste già un campo custom con lo stesso nome per la custom anagrafica corrente.", PostBackMessagesType.Notify);
            }
        }

        public override WebControl BuildControl()
        {
            if (EliminationRequest && CustomField.Exists)
            {
                EliminationConfirmControl delcontrol = DeleteControl("Conferma eliminazione Campo Custom '" + CustomField.Instance.Name + "' dell'anagrafe custom '" + CustomAnagrafe.Instance.Nome + "'", "Sei sicuro di voler eliminare il campo custom '" + CustomField.Instance.Name + "'", NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomAnagrafeCCK)) + "?ca=" + CustomAnagrafe.Instance.ID);
                delcontrol.ConfirmButton.Click += new EventHandler(ConfirmEliminationButton_Click);
                return delcontrol;
            }
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.Controls.Add(CCKForm);
            control.Controls.Add(GetTableControl());
            return control;
        }

        void ConfirmEliminationButton_Click(object sender, EventArgs e)
        {
            //BusinessLogic.DeleteCustomField(CustomField.Instance);
            CustomAnagrafeBusinessLogic.MarkForDeleteCustomField(CustomField.Instance);
            //BusinessLogic.DropColonna(CustomField.Instance, Application.Connection);
            NetCms.GUI.PopupBox.AddSessionMessage("Campo custom eliminato con successo", PostBackMessagesType.Success, true);
        }

        public CCKForm CCKForm
        {
            get
            {
                if (_CCKForm == null)
                {
                    _CCKForm = new CCKForm("CCKForm");
                    _CCKForm.CssClass = "Axf";
                    _CCKForm.SubmitButton.Text = "Inserisci campo";
                    _CCKForm.InfoControl.Controls.Add(new LiteralControl("Inserimento Nuovo Campo Custom nell'anagrafica '" + CustomAnagrafe.Instance.Nome + "'"));
                }
                return _CCKForm;
            }
        }

        private CCKForm _CCKForm;

        private WebControl GetTableControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);

            ArTable customFieldsTable = new ArTable();
            customFieldsTable.EnablePagination = true;
            customFieldsTable.RecordPerPagina = 30;
            customFieldsTable.InnerTableCssClass = "tab";
            customFieldsTable.NoRecordMsg = "Nessun campo custom collegato all'anagrafica " + CustomAnagrafe.Instance.Nome;

            PaginationHandler pagination = new PaginationHandler(customFieldsTable.RecordPerPagina, CustomAnagrafeBusinessLogic.CountCustomFieldsByAnagrafe(CustomAnagrafe.Instance), customFieldsTable.PaginationKey);
            customFieldsTable.PagesControl = pagination;

            var customFields = CustomAnagrafeBusinessLogic.FindCustomFieldsByAnagrafe(CustomAnagrafe.Instance, ((pagination.CurrentPage - 1) * pagination.PageSize), pagination.PageSize);

            customFieldsTable.Records = customFields;

            ArTextColumn nomeCustomField= new ArTextColumn("Nome", "Name");
            customFieldsTable.AddColumn(nomeCustomField);

            ArOptionsColumn richiestoCustomField = new ArOptionsColumn("Richiesto", "Required", new string[] { "NO", "SI" });
            customFieldsTable.AddColumn(richiestoCustomField);

            ArActionColumn modify = new ArActionColumn("Modifica", ArActionColumn.Icons.Edit, "campocustom_mod.aspx?cf={ID}");
            customFieldsTable.AddColumn(modify);

            ArActionColumn delete = new ArActionColumn("Elimina", ArActionColumn.Icons.Delete, "anagrafica_gest.aspx?action=delete&ca={Anagrafe.ID}&cf={ID}");
            customFieldsTable.AddColumn(delete);

            control.Controls.Add(customFieldsTable);

            return control;

        }

    }
}
