﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using GenericDAO.DAO.Utility;
using System;
using NetService.Utility.ValidatedFields;
using NetCms.Users.CustomAnagrafe;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("/campocustomdropdown_gest.aspx")]
    public class CustomDropdownFieldMod : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Modifica Valori Dropdown"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User_Edit; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("anagrafica_gest.aspx?ca="+CustomField.Instance.Anagrafe.ID, NetCms.GUI.Icons.Icons.Vcard, "Generazione Campi Anagrafica Custom"));
        }

        public NhRequestObject<CustomField> CustomField { get; private set; }
        public CustomDropdownValue DropDownValue { get; private set; }

        public CustomDropdownFieldMod(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            CustomField = new NhRequestObject<CustomField>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "cf");
            this.StatusValidators.Add(new NhRequestObjectValidator<CustomField>(CustomField));
            DropDownValue = new CustomDropdownValue();
            DropDownValue.CustomField = CustomField.Instance as CustomDropdownField;
            SetToolbars();
            this.DDValueForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
            this.DDValueForm.BackButton.Click += new EventHandler(BackButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.DDValueForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                bool status = CustomAnagrafeBusinessLogic.CreateDropdownValue(DDValueForm, CustomField.Instance as CustomDropdownField);
                if (status)
                    NetCms.GUI.PopupBox.AddMessageAndReload("Valore dropdown aggiunto con successo.", PostBackMessagesType.Success);
                else
                    NetCms.GUI.PopupBox.AddMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
            }
        }

        void BackButton_Click(object sender, EventArgs e)
        {
            NetCms.Diagnostics.Diagnostics.Redirect(NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomAnagrafeCCK)) + "?ca=" + CustomField.Instance.Anagrafe.ID);
        }

        private ArTable _TableValue;
        public ArTable TableValue
        {
            get
            {
                if (_TableValue == null)
                {
                    _TableValue = new ArTable();

                    _TableValue.EnablePagination = true;
                    _TableValue.RecordPerPagina = 10;
                    _TableValue.InnerTableCssClass = "tab";
                    _TableValue.NoRecordMsg = "Nessun valore disponibile";

                    PaginationHandler pagination = new PaginationHandler(_TableValue.RecordPerPagina, CustomAnagrafeBusinessLogic.CountDropDownValues(CustomField.Instance.ID), _TableValue.PaginationKey);
                    _TableValue.PagesControl = pagination;

                    var valori = CustomAnagrafeBusinessLogic.FindDropDownValues(((pagination.CurrentPage - 1) * pagination.PageSize), pagination.PageSize, CustomField.Instance.ID);

                    _TableValue.Records = valori;

                    ArTextColumn nomeChiave = new ArTextColumn("Label", "Label");
                    _TableValue.AddColumn(nomeChiave);

                    ArTextColumn nomeValore = new ArTextColumn("Valore", "Value");
                    _TableValue.AddColumn(nomeValore);

                    ArActionColumn modify = new ArActionColumn("Modifica", ArActionColumn.Icons.Edit, "customdropdownvalue_gest.aspx?va={ID}");
                    _TableValue.AddColumn(modify);

                    ArActionColumn delete = new ArActionColumn("Elimina", ArActionColumn.Icons.Delete, "customdropdownvalue_gest.aspx?action=delete&va={ID}");
                    _TableValue.AddColumn(delete);

                }
                return _TableValue;
            }
        }

        private LabelValueForm<CustomDropdownField> _DDValueForm;
        public LabelValueForm<CustomDropdownField> DDValueForm
        {
            get
            {
                if (_DDValueForm == null)
                {
                    _DDValueForm = new LabelValueForm<CustomDropdownField>("DDValueForm");
                    _DDValueForm.AddBackButton = true;
                    _DDValueForm.BackButton.Text = "Indietro";
                    _DDValueForm.CssClass = "Axf";
                    _DDValueForm.SubmitButton.Text = "Salva";
                    _DDValueForm.InfoControl.Controls.Add(new LiteralControl("Inserimento nuovo valore nella Dropdown '"+CustomField.Instance.Name+"'"));
                }
                return _DDValueForm;
            }
        }

        public override WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);

            control.Controls.Add(DDValueForm);
            control.Controls.Add(TableValue);
            return control;
        }

    }
}
