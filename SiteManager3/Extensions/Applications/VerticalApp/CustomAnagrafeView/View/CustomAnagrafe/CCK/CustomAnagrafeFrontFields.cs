﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using GenericDAO.DAO.Utility;
using System;
using NetService.Utility.ValidatedFields;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using NetService.Utility.UI;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("/campifrontend_gest.aspx")]
    public class CustomAnagrafeFrontFields : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Gestione Campi di Frontend dell'Anagrafica Custom '" + CustomAnagrafe.Instance.Nome + "'"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Brick; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Gestione Anagrafiche Custom"));
        }

        public NhRequestObject<CustomAnagrafe> CustomAnagrafe { get; private set; }

        public NhRequestObject<CustomField> CustomField { get; private set; }

        public CustomAnagrafeFrontFields(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            CustomAnagrafe = new NhRequestObject<CustomAnagrafe>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "ca");
            CustomField = new NhRequestObject<CustomField>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "cf");

            this.StatusValidators.Add(new NhRequestObjectValidator<CustomAnagrafe>(CustomAnagrafe));
            SetToolbars();
            this.Form.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.Form.IsValid == VfGeneric.ValidationStates.Valid)
            {
                if (this.Form.VfField.PostBackValue.Length > 0)
                {
                    CustomField cf = CustomAnagrafeBusinessLogic.GetCustomFieldById(int.Parse(this.Form.VfField.PostBackValue));
                    CustomAnagrafe.Instance.FrontendFields.Add(cf);
                    bool ok = CustomAnagrafeBusinessLogic.SaveOrUpdateCustomAnagrafe(CustomAnagrafe.Instance);
                    if (ok)
                        NetCms.GUI.PopupBox.AddSessionMessage("Campo custom inserito con successo nel Frontend.", PostBackMessagesType.Success, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomAnagrafeFrontFields)) + "?ca=" + CustomAnagrafe.Instance.ID);
                    else
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
                }
                else
                    NetCms.GUI.PopupBox.AddMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
            }
        }

        public override WebControl BuildControl()
        {
            if (EliminationRequest && CustomField.Exists)
            {
                EliminationConfirmControl delcontrol = DeleteControl("Conferma eliminazione Campo Custom '" + CustomField.Instance.Name + "' dal Frontend", "Sei sicuro di voler eliminare il campo custom '" + CustomField.Instance.Name + "' dal frontend", NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomAnagrafeFrontFields)) + "?ca=" + CustomAnagrafe.Instance.ID);
                delcontrol.ConfirmButton.Click += new EventHandler(ConfirmEliminationButton_Click);
                return delcontrol;
            }
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.Controls.Add(Form);
            control.Controls.Add(GetTableControl());
            return control;
        }

        void ConfirmEliminationButton_Click(object sender, EventArgs e)
        {
            CustomAnagrafe.Instance.FrontendFields.Remove(CustomField.Instance);
            CustomAnagrafeBusinessLogic.SaveOrUpdateCustomAnagrafe(CustomAnagrafe.Instance);

            NetCms.GUI.PopupBox.AddSessionMessage("Campo custom eliminato con successo dal Frontend", PostBackMessagesType.Success, true);
        }

        public FrontFieldsForm Form
        {
            get
            {
                if (_Form == null)
                {
                    _Form = new FrontFieldsForm("Custom Field selector", CustomAnagrafe.Instance);
                }
                return _Form;
            }
        }

        private FrontFieldsForm _Form;

        private WebControl GetTableControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);

            ArTable customFieldsTable = new ArTable();
            customFieldsTable.EnablePagination = true;
            customFieldsTable.RecordPerPagina = 20;
            customFieldsTable.InnerTableCssClass = "tab";
            customFieldsTable.NoRecordMsg = "Nessun campo custom nel frontend dell'anagrafica " + CustomAnagrafe.Instance.Nome;

            PaginationHandler pagination = new PaginationHandler(customFieldsTable.RecordPerPagina, CustomAnagrafeBusinessLogic.CountFrontendCustomFieldsByAnagrafe(CustomAnagrafe.Instance), customFieldsTable.PaginationKey);
            customFieldsTable.PagesControl = pagination;

            customFieldsTable.Records = CustomAnagrafe.Instance.FrontendFields;

            ArTextColumn nomeCustomField = new ArTextColumn("Nome", "Name");
            customFieldsTable.AddColumn(nomeCustomField);

            ArActionColumn delete = new ArActionColumn("Elimina", ArActionColumn.Icons.Delete, "campifrontend_gest.aspx?action=delete&ca={Anagrafe.ID}&cf={ID}");
            customFieldsTable.AddColumn(delete);

            control.Controls.Add(customFieldsTable);

            return control;

        }

    }
}
