﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using GenericDAO.DAO.Utility;
using System;
using NetService.Utility.ValidatedFields;
using NetCms.Users.CustomAnagrafe;
using System.Linq;
using System.Collections;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("campocustom_mod.aspx")]
    public class CustomFieldMod : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Modifica Campo Custom"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User_Edit; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("anagrafica_gest.aspx?ca="+CustomField.Instance.Anagrafe.ID, NetCms.GUI.Icons.Icons.Vcard, "Generazione Campi Anagrafica Custom"));
            foreach (ToolbarButton tb in this.CustomField.Instance.AdditionalToolbar)
            {
                this.Toolbar.Buttons.Add(tb);
            }
        }

        public NhRequestObject<CustomField> CustomField { get; private set; }

        public CustomFieldMod(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            CustomField = new NhRequestObject<CustomField>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "cf");
            this.StatusValidators.Add(new NhRequestObjectValidator<CustomField>(CustomField));
            SetToolbars();
            this.CCKForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
            this.CCKForm.BackButton.Click += new EventHandler(BackButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.CCKForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                CustomField field = CustomField.Instance;

                VfTextBox fieldTextBox = this.CCKForm.Field.FormConfigurazione.FirstOrDefault(x => x.Key == "label") as VfTextBox;

                string fieldLabel = "";

                if (fieldTextBox != null)
                    fieldLabel = fieldTextBox.PostBackValue;

                bool nomeNonModificato = (string.IsNullOrEmpty(fieldLabel) || fieldLabel == field.Name);
                
                //Dal controllo escludo il custom field che sto modificando 
                //così se il campo che sto modificando avrà lo stesso nome, questo sarà permesso
                //se invece sto cambiando il nome del custom field devo verificare che nei rimanenti nessuno si chiami allo stesso modo
                if (field.Anagrafe.CustomFields.Where(x => x.ID != field.ID ).Where(x => x.Name == fieldLabel && x.Cancellato == false).Count() == 0)
                {

                    bool cFieldInFront = field.Anagrafe.FrontendFields.Contains(field);

                    bool status = CustomAnagrafeBusinessLogic.ModCustomField(this.CCKForm, CustomField.Instance.ID, nomeNonModificato);
                    if (status)
                    {
                        if (cFieldInFront)
                            NetCms.GUI.PopupBox.AddSessionMessage("Campo custom modificato con successo. Verifica l'ordinamento dei campi visualizzati sul frontend.", PostBackMessagesType.Success, true);
                        else
                            NetCms.GUI.PopupBox.AddSessionMessage("Campo custom modificato con successo.", PostBackMessagesType.Success, true);
                    }
                    else
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
                }
                else
                    NetCms.GUI.PopupBox.AddMessage("Esiste già un campo custom con lo stesso nome per la custom anagrafica corrente.", PostBackMessagesType.Notify);
            }
        }

        void BackButton_Click(object sender, EventArgs e)
        {
            NetCms.Diagnostics.Diagnostics.Redirect(NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomAnagrafeCCK)) + "?ca=" + CustomField.Instance.Anagrafe.ID);
        }

        public CCKForm CCKForm
        {
            get
            {
                if (_CCKForm == null)
                {
                    _CCKForm = new CCKForm("CCKForm", CustomField.Instance);
                    _CCKForm.CssClass = "Axf";
                    _CCKForm.SubmitButton.Text = "Modifica";
                    _CCKForm.DataBindAdvanced(CustomField.Instance);
                }
                return _CCKForm;
            }
        }
        private CCKForm _CCKForm;


        public override WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.Controls.Add(CCKForm);
            return control;
        }

    }
}
