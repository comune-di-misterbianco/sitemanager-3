﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using System;
using System.Linq;
using NetService.Utility.ValidatedFields;
using System.Collections;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Web;
using NHibernate;
using NetCms.DBSessionProvider;
using System.Collections.ObjectModel;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("anagrafica_frontimport.aspx")]
    public class CustomAnagrafeFrontImport : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Importazione Utenti FrontEnd"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User_Add; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Gestione Anagrafiche Custom"));
        }

        public CustomAnagrafeFrontImport(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            SetToolbars();
            this.CustomAnagrafeForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        private string _path;
        private string path
        {
            get
            {
                if (_path == null)
                {
                    _path = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/dataToImport/";
                    _path = HttpContext.Current.Server.MapPath(_path);
                }
                return _path;
            }
        }

        //questo dizionario serve per associare il vecchio id del gruppo preso dal file xml con il corrispondente id del gruppo appena inserito.
        //private Dictionary<int, int> _Options;
        //private Dictionary<int, int> Options
        //{
        //    get
        //    {
        //        if (_Options == null)
        //        {
        //            _Options = new Dictionary<int, int>();
        //        }
        //        return _Options;
        //    }
        //}
        
        void SubmitButton_Click(object sender, EventArgs e)
        {           
                if (this.CustomAnagrafeForm.IsValid == VfGeneric.ValidationStates.Valid)
                {
                    bool ManageReg = false;
                    NetUtility.RequestVariable ck = new NetUtility.RequestVariable("ck", NetUtility.RequestVariable.RequestType.Form);
                    if (!string.IsNullOrEmpty(ck.StringValue) && ck.StringValue.Equals("on"))
                    {
                        ManageReg = true;
                    }

                    ICollection<Group> groupToAssociate = new Collection<Group>();
                    
                    int id_gruppo_selected = -1;
                    
                    if (Groups.PostbackValueObject != null)
                    {
                        id_gruppo_selected = int.Parse(Groups.PostbackValueObject.ToString());
                        Group gruppoSelezionato = GroupsBusinessLogic.GetGroupById(id_gruppo_selected);
                        groupToAssociate.Add(gruppoSelezionato);
                    }

                    var all = XDocument.Load(path + "\\data.xml").Descendants("NewDataSet");//

                    #region vecchia gestion gruppi - non necessaria per l'importazione utenti da cms1
                    //var gruppi = from gruppo in all.Descendants("Groups-Profiles")

                    //             select new
                    //             {
                    //                 id = gruppo.Element("id_Group").Value,
                    //                 name = gruppo.Element("Name_Group").Value,
                    //                 tree = gruppo.Element("Tree_Group").Value,
                    //                 parent = gruppo.Element("Parent_Group").Value,
                    //                 profile = gruppo.Element("Profile_Group").Value,
                    //                 depth = gruppo.Element("Depth_Group").Value,

                    //             };
                    //var gruppi_ordinati = gruppi.OrderBy(y => y.depth);

                    //var gruppi_profile = from gruppo_profile in all.Descendants("Groups-Profiles")

                    //                     select new
                    //                     {
                    //                         id = gruppo_profile.Element("id_Profile").Value,
                    //                         name = gruppo_profile.Element("Name_Profile").Value,
                    //                         type = gruppo_profile.Element("Type_Profile").Value,
                    //                         systemkey = gruppo_profile.Element("SystemKey_Group").Value,
                    //                         macrogroup = gruppo_profile.Element("MacroGroup_Group").Value,

                    //                     };
                    //var macrogruppi_profile = from macrogruppo_profile in all.Descendants("Groups-Profiles")

                    //                          select new
                    //                          {
                    //                              label = macrogruppo_profile.Element("Label_MacroGroup").Value,
                    //                              key = macrogruppo_profile.Element("Key_MacroGroup").Value,

                    //                          };
                    //var selectedMacrogruppo = macrogruppi_profile.FirstOrDefault();
                    ////lo seleziono una volta perchè tanto è lo stesso è tutti i gruppi!!
                    //MacroGroup macroGruppo = GroupsBusinessLogic.GetMacroGroupByKey(selectedMacrogruppo.key);

                    //# region inserimento e creazione collection di gruppi

                    //foreach (var x in gruppi_ordinati)
                    //{
                    //    using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                    //    using (ITransaction tx = sess.BeginTransaction())
                    //    {
                    //        try
                    //        {
                    //            var parent = from par in all.Descendants("Groups-Profiles")
                    //                         where par.Element("id_Group").Value == x.parent
                    //                         select new
                    //                         {
                    //                             id = par.Element("id_Group").Value,
                    //                             nameparent = par.Element("Name_Group").Value,
                    //                             depth = par.Element("Depth_Group").Value,
                    //                             id_parent = par.Element("Parent_Group").Value

                    //                         };
                    //            var selectedParent = parent.FirstOrDefault();
                    //            Group child;

                    //            if ((int.Parse(x.parent)) != 0 && (int.Parse(x.depth) != 0))
                    //            {
                    //                Group padre;
                    //                if ((int.Parse(x.parent)) == 1)
                    //                    padre = GroupsBusinessLogic.GetRootGroup();
                    //                else
                    //                {
                    //                    var parent_padre = from par in all.Descendants("Groups-Profiles")
                    //                                       where par.Element("id_Group").Value == selectedParent.id_parent//x.parent---selectedParent.id_parent
                    //                                       select new
                    //                                       {
                    //                                           id = par.Element("id_Group").Value,
                    //                                           nameparent = par.Element("Name_Group").Value,
                    //                                           depth = par.Element("Depth_Group").Value,
                    //                                           key = par.Element("SystemKey_Group").Value

                    //                                       };
                    //                    var selectedParentPadre = parent_padre.FirstOrDefault();
                    //                    int id_new_group = 0;

                    //                    if (Options.Count != 0)
                    //                    {
                    //                        //id_new_group = Options.Single(y => y.Key == int.Parse(selectedParentPadre.id)).Value;
                    //                        id_new_group = Options.Single(y => y.Key == int.Parse(selectedParent.id)).Value;
                    //                    }


                    //                    if (id_new_group == 0)
                    //                        if (selectedParentPadre.id.Equals("1"))
                    //                            padre = GroupsBusinessLogic.GetGroupByNameAndParent(selectedParent.nameparent, GroupsBusinessLogic.GetRootGroup());
                    //                        else
                    //                            padre = GroupsBusinessLogic.GetGroupByNameAndParentName(selectedParent.nameparent, selectedParentPadre.nameparent);

                    //                    else
                    //                        padre = GroupsBusinessLogic.GetGroupById(id_new_group);
                    //                }



                    //                child = GroupsBusinessLogic.GetGroupByNameAndParent(x.name, padre, sess);
                    //                if (child == null)
                    //                {
                    //                    child = new Group();
                    //                    child.Name = x.name;
                    //                    child.MacroGroup = macroGruppo;

                    //                    child = GroupsBusinessLogic.SaveOrUpdateGroup(child, sess);
                    //                    var selectedProfile = gruppi_profile.Where(y => y.id == x.profile).FirstOrDefault();

                    //                    GroupProfile profile = new GroupProfile();
                    //                    profile.Nome = selectedProfile.name;

                    //                    profile = ProfileBusinessLogic.SaveOrUpdateProfile(profile, sess) as GroupProfile;

                    //                    child.SystemName = selectedProfile.systemkey;
                    //                    child.Tree = padre.Tree + NetUtility.TreeUtility.FormatNumber(child.ID.ToString()) + ".";
                    //                    child.Depth = padre.Depth + 1;
                    //                    child.Parent = padre;
                    //                    child.Profile = profile;
                    //                    profile.Group = child;

                    //                    child.Groups.Add(child);
                    //                    child = GroupsBusinessLogic.SaveOrUpdateGroup(child, sess);
                    //                    Options.Add(int.Parse(x.id), child.ID);
                    //                    tx.Commit();
                    //                    //Gruppi.Add(child);
                    //                }
                    //                else
                    //                {
                    //                    Options.Add(int.Parse(x.id), child.ID);
                    //                    //Gruppi.Add(child);
                    //                }

                    //            }
                    //            else
                    //            {
                    //                //Gruppi.Add(GroupsBusinessLogic.GetRootGroup());
                    //            }


                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            tx.Rollback();
                    //        }
                    //    }

                    //}

                    //# endregion 
                    #endregion

                    Hashtable userToInsert = new Hashtable();
                    string SistemType = "AnagrafeTable-CustomAnagrafe.Runtime.AnagrafeFrontend";
                    string[] split = SistemType.Split('-');
                    string type = split[1];// contiene CustomAnagrafe.Runtime.AnagrafeFrontend                   

                    var utenti = from utente in all.Descendants(SistemType)
                                 select new
                                 {                                    
                                     id = (utente.Element("id_Anagrafica") == null) ? "" : utente.Element("id_Anagrafica").Value,
                                     nome = (utente.Element("Nome_Anagrafica") == null) ? "" : utente.Element("Nome_Anagrafica").Value,
                                     cognome = (utente.Element("Cognome_Anagrafica") == null) ? "" : utente.Element("Cognome_Anagrafica").Value,
                                     citta = (utente.Element("Citta_Anagrafica") == null) ? "" : utente.Element("Citta_Anagrafica").Value,
                                     indirizzo = (utente.Element("Indirizzo_Anagrafica") == null) ? "" : utente.Element("Indirizzo_Anagrafica").Value,
                                     cap = (utente.Element("CAP_Anagrafica") == null) ? "" : utente.Element("CAP_Anagrafica").Value,
                                     telefono = (utente.Element("Telefono_Anagrafica") == null) ? "" : utente.Element("Telefono_Anagrafica").Value,
                                     cellulare = (utente.Element("Cellulare_Anagrafica") == null) ? "" : utente.Element("Cellulare_Anagrafica").Value,
                                     email = (utente.Element("Email_Anagrafica") == null) ? "" : utente.Element("Email_Anagrafica").Value,
                                     codfisc = (utente.Element("CodiceFiscale_Anagrafica") == null) ? "" : utente.Element("CodiceFiscale_Anagrafica").Value,
                                     provincia = (utente.Element("Provincia_Anagrafica") == null) ? "" : utente.Element("Provincia_Anagrafica").Value, 
                                     azienda = (utente.Element("Azienda_Anagrafica") == null) ? "" : utente.Element("Azienda_Anagrafica").Value,
                                     fax = (utente.Element("Fax_Anagrafica") == null) ? "" : utente.Element("Fax_Anagrafica").Value,
                                     settore = (utente.Element("SettoreAzienda_Anagrafica") == null) ? "" : utente.Element("SettoreAzienda_Anagrafica").Value,
                                     funzione = (utente.Element("FunzioneAzienda_Anagrafica") == null) ? "" : utente.Element("FunzioneAzienda_Anagrafica").Value,
                                     data = (utente.Element("Data_Anagrafica") == null) ? "" : utente.Element("Data_Anagrafica").Value,

                                 };
                    var allutenti = utenti.ToList();
                    bool isOk = true;
                    foreach (var x in allutenti)
                    {
                        userToInsert.Add("Nome", x.nome);
                        userToInsert.Add("Cognome", x.cognome);
                        userToInsert.Add("Email", x.email);
                        //userToInsert.Add("Comune_di_Residenza", x.citta);
                        userToInsert.Add("Indirizzo", x.indirizzo);

                        string cap = (string.IsNullOrEmpty(x.cap) || x.cap.Length > 5) ? "" : x.cap;                       
                        userToInsert.Add("CAP", cap);                        
                                      
                        userToInsert.Add("Telefono", x.telefono);
                        userToInsert.Add("Cellulare", x.cellulare);
                        
                        string cf = (string.IsNullOrEmpty(x.codfisc) || x.codfisc.Length > 16) ? "" : x.codfisc;
                        userToInsert.Add("Codice Fiscale", cf);

                        userToInsert.Add("Azienda", x.azienda);
                        userToInsert.Add("Data_di_Nascita", x.data);
                        userToInsert.Add("Fax", x.fax);
                        userToInsert.Add("Settore Azienda", x.settore);
                        userToInsert.Add("Mansione Azienda", x.funzione);                                               

                        var profile = from par in all.Descendants("Users-Profiles")
                                      where (par.Element("Anagrafica_User").Value == x.id && par.Element("AnagraficaType_User").Value == type)
                                      select new
                                      {
                                          preview = (par.Element("PreviewType_User") == null) ? "" : par.Element("PreviewType_User").Value,
                                          type = (par.Element("Type_User") == null) ? "" : par.Element("Type_User").Value,
                                          name = (par.Element("Name_User") == null) ? "" : par.Element("Name_User").Value,
                                          anagrafica = (par.Element("Anagrafica_User")== null) ? "" : par.Element("Anagrafica_User").Value,
                                          profile = (par.Element("Profile_User") == null) ? "" : par.Element("Profile_User").Value,
                                          password = (par.Element("Password_User") == null) ? "" : par.Element("Password_User").Value,
                                          state = (par.Element("State_User") == null) ? "" : (par.Element("State_User").Value == "2" ? "0" : par.Element("State_User").Value),
                                          key = (par.Element("Key_User") == null) ? "" : par.Element("Key_User").Value,
                                          network = (par.Element("Network_User") == null) ? "" : par.Element("Network_User").Value,
                                          deleted = (par.Element("Deleted_User") == null) ? "" : par.Element("Deleted_User").Value,
                                          foldergui = (par.Element("FoldersGUI_User") == null) ? "" : par.Element("FoldersGUI_User").Value,
                                          editor = (par.Element("Editor_User") == null) ? "" : par.Element("Editor_User").Value,
                                          creationTime = (par.Element("CreationTime_User") == null) ? DateTime.Now.ToString() : par.Element("CreationTime_User").Value,
                                          lastaccessTime = (par.Element("LastAccess_User") == null) ? DateTime.Now.ToString() : par.Element("LastAccess_User").Value,
                                          lastUpdate = (par.Element("LastUpdate_User") == null) ? DateTime.Now.ToString() : par.Element("LastUpdate_User").Value,

                                      };
                        var selectedProfile = profile.FirstOrDefault();



                        using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                        using (ITransaction tx = sess.BeginTransaction())
                        {
                            try
                            {
                                User user = UsersBusinessLogic.GetUserByUserName(selectedProfile.name);
                                if (user == null)//!x.id.Equals("1") && 
                                {
                                    UsersBusinessLogic.CreateUser(sess, selectedProfile.name, selectedProfile.password,
                                    DateTime.Parse(selectedProfile.creationTime),
                                    DateTime.Parse(selectedProfile.lastaccessTime),
                                    DateTime.Parse(selectedProfile.lastUpdate),
                                    int.Parse(selectedProfile.state),
                                    userToInsert,
                                    "AnagraficaFrontend",
                                    groupToAssociate,
                                    ManageReg);
                                    tx.Commit();
                                    isOk = isOk && true;
                                }
                            }
                            catch (Exception ex)
                            {
                                tx.Rollback();
                                isOk = isOk && false;
                            }
                        }
                        userToInsert.Clear();
                       
                    }

                    if (isOk)
                        PopupBox.AddSessionMessage("Utenti importati con successo", PostBackMessagesType.Success, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(MainPage)));
                    else
                        PopupBox.AddMessage("Errore nell'importazione degli utenti: ", PostBackMessagesType.Error);


                }
            
        }

        public VfManager CustomAnagrafeForm
        {
            get
            {
                if (_CustomAnagrafeForm == null)
                {
                    _CustomAnagrafeForm = new VfManager("CustomAnagrafeForm");
                    
                    _CustomAnagrafeForm.CssClass = "Axf";
                    _CustomAnagrafeForm.Controls.Add(new LiteralControl("<p class=\"info\">Copiare il file data.xml nella cartella " + path + "</p><br />"));
                    _CustomAnagrafeForm.Controls.Add(Groups);                    
                    _CustomAnagrafeForm.Controls.Add(ManageReg);
                    _CustomAnagrafeForm.SubmitButton.Text = "Importa";
                }
                return _CustomAnagrafeForm;
            }
        }
        private VfManager _CustomAnagrafeForm;        

        private VfCheckBox _ManageReg;
        private VfCheckBox ManageReg
        {
            get
            {
                if (_ManageReg == null)
                {
                    _ManageReg = new VfCheckBox("ck", "Attiva validazione da backend");
                    //_ManageReg.Controls.Add(new LiteralControl("Attiva validazione da backend"));
                }
                return _ManageReg;
            }
        }

        private VfDropDown _Groups;
        private VfDropDown Groups 
        {
            get 
            {
                if (_Groups == null)
                {
                    _Groups = new VfDropDown("group", "Selezionare il gruppo in cui importare gli utenti");
                    foreach (Group group in GroupsBusinessLogic.FindAllOrderedByTree())
                    {
                        _Groups.Options.Add(group.Name,group.ID.ToString() );
                    }
                }
                return _Groups;
            }
        }

        public override WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);

            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            legend.Controls.Add(new LiteralControl(this.PageTitle));
            control.Controls.Add(legend);

            control.Controls.Add(CustomAnagrafeForm);

            return control;
        }
    }
}
