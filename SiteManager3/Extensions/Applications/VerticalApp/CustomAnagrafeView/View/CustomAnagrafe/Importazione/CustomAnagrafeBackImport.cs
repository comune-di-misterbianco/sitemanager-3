﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using System;
using System.Collections;
using System.Linq;
using NetService.Utility.ValidatedFields;
using System.Xml.Linq;
using NHibernate;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using NetCms.DBSessionProvider;
using System.Web;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("anagrafica_backimport.aspx")]
    public class CustomAnagrafeBackImport : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Importazione Utenti BackOffice"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User_Add; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Gestione Anagrafiche Custom"));
        }

        public CustomAnagrafeBackImport(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            SetToolbars();
            this.CustomAnagrafeForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        private string _path;
        private string path
        {
            get
            {
                if (_path == null)
                {
                    _path = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/dataToImport" + "/";
                    _path = HttpContext.Current.Server.MapPath(_path);
                }
                return _path;
            }
        }

        //questo dizionario serve per associare il vecchio id del gruppo preso dal file xml con il corrispondente id del gruppo appena inserito.
        private Dictionary<int, int> _Options;
        private Dictionary<int, int> Options
        {
            get
            {
                if (_Options == null)
                {
                    _Options = new Dictionary<int,int>();
                }
                return _Options;
            }
        }

        private ICollection<Group> Gruppi = new Collection<Group>();
        
        private bool AnagrafeContainsNomeUtente
        {
            get {                 
                 CustomAnagrafe backofficeAnagrafe = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName("Backoffice");
                 return backofficeAnagrafe.CustomFields.Any(x => x.Name.ToLower() == "nome utente");                
            }
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.CustomAnagrafeForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                bool ManageReg = false;
                NetUtility.RequestVariable ck = new NetUtility.RequestVariable("ck", NetUtility.RequestVariable.RequestType.Form);
                if (!string.IsNullOrEmpty(ck.StringValue) && ck.StringValue.Equals("on"))
                {
                    ManageReg = true;
                }                
                var all = XDocument.Load(path + "\\data.xml").Descendants("NewDataSet");//

                var gruppi = from gruppo in all.Descendants("Groups-Profiles")

                             select new
                             {
                                 id = gruppo.Element("id_Group").Value,
                                 name = gruppo.Element("Name_Group").Value,
                                 tree = gruppo.Element("Tree_Group").Value,
                                 parent = gruppo.Element("Parent_Group").Value,
                                 profile = gruppo.Element("Profile_Group").Value,
                                 depth = gruppo.Element("Depth_Group").Value,

                             };
                var gruppi_ordinati = gruppi.OrderBy(y => y.depth);

                var gruppi_profile = from gruppo_profile in all.Descendants("Groups-Profiles")

                                     select new
                                     {
                                         id = gruppo_profile.Element("id_Profile").Value,
                                         name = gruppo_profile.Element("Name_Profile").Value,
                                         type = gruppo_profile.Element("Type_Profile").Value,
                                         systemkey = gruppo_profile.Element("SystemKey_Group").Value,
                                         macrogroup = gruppo_profile.Element("MacroGroup_Group").Value,

                                     };
                var macrogruppi_profile = from macrogruppo_profile in all.Descendants("Groups-Profiles")

                                          select new
                                          {
                                              label = macrogruppo_profile.Element("Label_MacroGroup").Value,
                                              key = macrogruppo_profile.Element("Key_MacroGroup").Value,

                                          };
                var selectedMacrogruppo = macrogruppi_profile.FirstOrDefault();
                //lo seleziono una volta perchè tanto è lo stesso è tutti i gruppi!!
                MacroGroup macroGruppo = GroupsBusinessLogic.GetMacroGroupByKey(selectedMacrogruppo.key);

                # region inserimento e creazione collection di gruppi                
                
                foreach (var x in gruppi_ordinati)
                {
                    using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                    using (ITransaction tx = sess.BeginTransaction())
                    {
                        try
                        {
                            var parent = from par in all.Descendants("Groups-Profiles")
                                         where par.Element("id_Group").Value == x.parent
                                         select new
                                         {
                                             id = par.Element("id_Group").Value,
                                             nameparent = par.Element("Name_Group").Value,
                                             depth = par.Element("Depth_Group").Value,
                                             id_parent = par.Element("Parent_Group").Value

                                         };
                            var selectedParent = parent.FirstOrDefault();
                            Group child;

                            if ((int.Parse(x.parent)) != 0 && (int.Parse(x.depth) != 0))
                            {
                                Group padre;
                                if ((int.Parse(x.parent)) == 1)
                                    padre = GroupsBusinessLogic.GetRootGroup();
                                else
                                {
                                    var parent_padre = from par in all.Descendants("Groups-Profiles")
                                                       where par.Element("id_Group").Value == selectedParent.id_parent//x.parent---selectedParent.id_parent
                                                       select new
                                                       {
                                                           id = par.Element("id_Group").Value,
                                                           nameparent = par.Element("Name_Group").Value,
                                                           depth = par.Element("Depth_Group").Value,
                                                           key = par.Element("SystemKey_Group").Value

                                                       };
                                    var selectedParentPadre = parent_padre.FirstOrDefault();
                                    int id_new_group = 0;

                                    if (Options.Count != 0)
                                    {
                                        //id_new_group = Options.Single(y => y.Key == int.Parse(selectedParentPadre.id)).Value;
                                        id_new_group = Options.Single(y => y.Key == int.Parse(selectedParent.id)).Value;
                                    }


                                    if (id_new_group == 0)
                                        if (selectedParentPadre.id.Equals("1"))
                                            padre = GroupsBusinessLogic.GetGroupByNameAndParent(selectedParent.nameparent, GroupsBusinessLogic.GetRootGroup());
                                        else
                                            padre = GroupsBusinessLogic.GetGroupByNameAndParentName(selectedParent.nameparent, selectedParentPadre.nameparent);

                                    else
                                        padre = GroupsBusinessLogic.GetGroupById(id_new_group);
                                }



                                child = GroupsBusinessLogic.GetGroupByNameAndParent(x.name, padre, sess);
                                if (child == null)
                                {
                                    child = new Group();
                                    child.Name = x.name;
                                    child.MacroGroup = macroGruppo;

                                    child = GroupsBusinessLogic.SaveOrUpdateGroup(child, sess);
                                    var selectedProfile = gruppi_profile.Where(y => y.id == x.profile).FirstOrDefault();

                                    GroupProfile profile = new GroupProfile();
                                    profile.Nome = selectedProfile.name;

                                    profile = ProfileBusinessLogic.SaveOrUpdateProfile(profile, sess) as GroupProfile;

                                    child.SystemName = selectedProfile.systemkey;
                                    child.Tree = padre.Tree + NetUtility.TreeUtility.FormatNumber(child.ID.ToString()) + ".";
                                    child.Depth = padre.Depth + 1;
                                    child.Parent = padre;
                                    child.Profile = profile;
                                    profile.Group = child;

                                    child.Groups.Add(child);
                                    child = GroupsBusinessLogic.SaveOrUpdateGroup(child, sess);
                                    Options.Add(int.Parse(x.id), child.ID);
                                    tx.Commit();
                                    //Gruppi.Add(child);
                                }
                                else
                                {
                                    Options.Add(int.Parse(x.id), child.ID);
                                    //Gruppi.Add(child);
                                }

                            }
                            else
                            {                                
                                //Gruppi.Add(GroupsBusinessLogic.GetRootGroup());
                            }


                        }
                        catch (Exception ex)
                        {
                            tx.Rollback();
                        }
                    }
                    
                }
                
                # endregion

                Hashtable userToInsert = new Hashtable();
                string SistemType = "AnagrafeTable-NetCms.Users.Anagrafica";
                string[] split = SistemType.Split('-');
                string type = split[1];// contiene NetCms.Users.Anagrafica

                var utenti = from utente in all.Descendants(SistemType)
                             select new
                             {
                                 id = (utente.Element("id_Anagrafica") == null) ? "" : utente.Element("id_Anagrafica").Value,
                                 nome = (utente.Element("Nome_Anagrafica") == null) ? "" : utente.Element("Nome_Anagrafica").Value,
                                 cognome =(utente.Element("Cognome_Anagrafica") == null) ? "" : utente.Element("Cognome_Anagrafica").Value,                
                                 email = (utente.Element("Email_Anagrafica") == null)? "" :  utente.Element("Email_Anagrafica").Value,                                 
                                 data = (utente.Element("Data_Anagrafica") == null) ? "" : utente.Element("Data_Anagrafica").Value,
                                 iduser = (utente.Element("id_User") == null) ? "" : utente.Element("id_User").Value     
                             };


                var allutenti = utenti.ToList();
                bool isOk = false;
                foreach (var x in allutenti)
                {
                    userToInsert.Add("Nome", x.nome);
                    userToInsert.Add("Cognome", x.cognome);
                    userToInsert.Add("Email", x.email);                                       

                    var profile = from par in all.Descendants("Users-Profiles")
                                  where (par.Element("Anagrafica_User").Value == x.id && par.Element("AnagraficaType_User").Value == type)
                                  select new
                                  {                                    
                                      preview = (par.Element("PreviewType_User") == null) ? "" : par.Element("PreviewType_User").Value,
                                      type = (par.Element("Type_User") == null) ? "" : par.Element("Type_User").Value,
                                      name = (par.Element("Name_User") == null) ? "" : par.Element("Name_User").Value,
                                      anagrafica = (par.Element("Anagrafica_User") == null) ? "" : par.Element("Anagrafica_User").Value,
                                      profile = (par.Element("Profile_User") == null) ? "" : par.Element("Profile_User").Value,
                                      password = (par.Element("Password_User") == null) ? "" : par.Element("Password_User").Value,
                                      state = (par.Element("State_User") == null) ? "" : par.Element("State_User").Value,
                                      key = (par.Element("Key_User") == null) ? "" : par.Element("Key_User").Value,
                                      network = (par.Element("Network_User") == null) ? "" : par.Element("Network_User").Value,
                                      deleted = (par.Element("Deleted_User") == null) ? "" : par.Element("Deleted_User").Value,
                                      foldergui = (par.Element("FoldersGUI_User") == null) ? "" : par.Element("FoldersGUI_User").Value,
                                      editor = (par.Element("Editor_User") == null) ? "" : par.Element("Editor_User").Value,
                                      creationTime = (par.Element("CreationTime_User") == null) ? DateTime.Now.ToString() : par.Element("CreationTime_User").Value,
                                      lastaccessTime = (par.Element("LastAccess_User") == null) ? DateTime.Now.ToString() : par.Element("LastAccess_User").Value,
                                      lastUpdate = (par.Element("LastUpdate_User") == null) ? DateTime.Now.ToString() : par.Element("LastUpdate_User").Value,
                                      oldID = (par.Element("id_User") == null) ? 0 : int.Parse(par.Element("id_User").Value),
                                  };
                    var selectedProfile = profile.FirstOrDefault();

                    if (AnagrafeContainsNomeUtente)
                        userToInsert.Add("Nome utente", selectedProfile.name);

                  

                    var gruppi_utente = from gruppo_utente in all.Descendants("usersgroups")
                                        where (gruppo_utente.Element("User_UserGroup").Value == selectedProfile.oldID.ToString())
                                        select new
                                        {
                                            id = gruppo_utente.Element("id_UserGroup").Value,
                                            user = gruppo_utente.Element("User_UserGroup").Value,
                                            gruppo = gruppo_utente.Element("Group_UserGroup").Value,
                                            type = gruppo_utente.Element("Type_UserGroup").Value,
                                            publishRole = gruppo_utente.Element("PublishRole_UserGroup").Value
                                        };

                    ICollection<Group> groupToAssociate = new Collection<Group>();
                    foreach (var ass in gruppi_utente)
                    {
                        if (int.Parse(ass.gruppo) == 1)
                            groupToAssociate.Add(GroupsBusinessLogic.GetRootGroup());
                        else
                        {
                            int id = Options.Single(y => y.Key == int.Parse(ass.gruppo)).Value;
                            groupToAssociate.Add(GroupsBusinessLogic.GetGroupById(id));
                        }
                    }
                    
                    using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                    using (ITransaction tx = sess.BeginTransaction())
                    {
                        try
                        {
                            User user = UsersBusinessLogic.GetUserByUserName(selectedProfile.name,sess);
                            if (!x.id.Equals("1") && user == null)
                            {
                                UsersBusinessLogic.CreateUser(sess, selectedProfile.name, selectedProfile.password,
                                DateTime.Parse(selectedProfile.creationTime),
                                DateTime.Parse(selectedProfile.lastaccessTime),
                                DateTime.Parse(selectedProfile.lastUpdate),
                                int.Parse(selectedProfile.state),
                                userToInsert,
                                "AnagraficaBackoffice",
                                groupToAssociate,
                                ManageReg,
                                selectedProfile.oldID);
                                tx.Commit();
                                isOk = isOk && true;
                            }
                            else if(int.Parse(x.id) > 1 && user != null)
                            {
                                user.OldID = selectedProfile.oldID;
                                UsersBusinessLogic.SaveOrUpdateUser(user, sess);
                                tx.Commit();
                                isOk = isOk && true;
                            }
                            else
                            {
                                isOk = true; // skip administrator
                            }
                        }
                        catch (Exception ex)
                        {
                            tx.Rollback();
                            isOk = isOk && false;
                        }
                    }
                    userToInsert.Clear();
                   
                }

                if (isOk)
                    PopupBox.AddSessionMessage("Utenti importati con successo", PostBackMessagesType.Success, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(MainPage)));
                else
                    PopupBox.AddMessage("Errore nell'importazione degli utenti: ", PostBackMessagesType.Error);
                

            }
        }
        
        public VfManager CustomAnagrafeForm
        {
            get
            {
                if (_CustomAnagrafeForm == null)
                {
                    _CustomAnagrafeForm = new VfManager("CustomAnagrafeForm");
                    _CustomAnagrafeForm.CssClass = "Axf";
                    _CustomAnagrafeForm.Controls.Add(new LiteralControl("<p class=\"info\">Copiare il file data.xml nella cartella " + path + "</p><br />"));
                    _CustomAnagrafeForm.Controls.Add(ManageReg);
                    _CustomAnagrafeForm.SubmitButton.Text = "Importa";
                }
                return _CustomAnagrafeForm;
            }
        }
        private VfManager _CustomAnagrafeForm;

        //private FileUpload upload;
        //private FileUpload Upload
        //{
        //    get
        //    {
        //        if (upload == null)
        //        {
        //            upload = new FileUpload();                    
        //        }
        //        return upload;
        //    }

        //}

        private VfCheckBox _ManageReg;
        private VfCheckBox ManageReg
        {
            get
            {
                if (_ManageReg == null)
                {
                    _ManageReg = new VfCheckBox("ck", "");
                    _ManageReg.Controls.Add(new LiteralControl("Attiva validazione da backend"));
                }
                return _ManageReg;
            }
        }
        
        public override WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);

            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            legend.Controls.Add(new LiteralControl(this.PageTitle));
            control.Controls.Add(legend);
            
            control.Controls.Add(CustomAnagrafeForm);

            return control;            
        }
    }
}
