﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using GenericDAO.DAO.Utility;
using System;
using NetService.Utility.ValidatedFields;
using System.Linq;
using NetCms.DBSessionProvider;
using NHibernate;
using System.Reflection;
using System.Collections.Generic;
using NHibernate.Criterion;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("anagrafica_structure_export.aspx")]
    public class CustomAnagrafeStructureExport : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Esporta Struttura Anagrafica Custom"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User_Go; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Gestione Anagrafiche Custom"));
        }

        public NhRequestObject<NetCms.Users.CustomAnagrafe.CustomAnagrafe> CustomAnagrafe { get; private set; }

        public CustomAnagrafeStructureExport(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            CustomAnagrafe = new NhRequestObject<CustomAnagrafe>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "ca");
            this.StatusValidators.Add(new NhRequestObjectValidator<CustomAnagrafe>(CustomAnagrafe));
            this.ExportToDbForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
            SetToolbars();
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (ExportToDbForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                string database = ExportToDbForm.Fields.First(x => x.Key == "Database").PostBackValue;
                string server = ExportToDbForm.Fields.First(x => x.Key == "Server").PostBackValue;
                string username = ExportToDbForm.Fields.First(x => x.Key == "UserName").PostBackValue;
                string password = ExportToDbForm.Fields.First(x => x.Key == "Password").PostBackValue;
                string tcpport = ExportToDbForm.Fields.First(x => x.Key == "TcpPort").PostBackValue;
                bool manageRegRequest = (bool) ExportToDbForm.Fields.First(x => x.Key == "ManageWebRequestField").PostbackValueObject;
                List<string> assembliesName = ExportToDbForm.Fields.First(x => x.Key == "Assemblies").PostbackValueObject as List<string>;

                List<Assembly> assembliesToWorkWith = FluentSessionProvider.FluentAssemblies.Where(x => assembliesName.Contains(x.FullName)).ToList();

                string exceptionText = "";

                if (FluentSessionHelper.GetSessionFactory(database, server, tcpport, username, password, assembliesToWorkWith) != null)
                {
                    bool isOk = true;
                    CustomAnagrafe anagrafeToExport = null;
                    
                    using (ISession sessionCms = FluentSessionProvider.Instance.OpenInnerSession())
                    {
                        anagrafeToExport = CustomAnagrafeBusinessLogic.GetCustomAnagrafeById(CustomAnagrafe.Instance.ID, sessionCms);
                    }

                    using (ISession sessionExternal = FluentSessionHelper.GetSession())
                    using (ITransaction tx = sessionExternal.BeginTransaction())
                    {
                        try
                        {
                            sessionExternal.SaveOrUpdate(anagrafeToExport.Clone());

                            tx.Commit();
                        }
                        catch (Exception ex)
                        {
                            exceptionText = ex.Message;
                            tx.Rollback();
                            isOk = false;
                        }
                    }
                           
                    FluentSessionHelper.CloseSessionFactory();

                    if (isOk)
                        PopupBox.AddSessionMessage("Struttura Anagrafica importata con successo", PostBackMessagesType.Success, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(MainPage)));
                    else
                        PopupBox.AddMessage("Errore nell'importazione: " + exceptionText, PostBackMessagesType.Error);

                }
                else
                    PopupBox.AddMessage("Errore nella creazione della session factory remota", PostBackMessagesType.Error);
            }
        }

        public ExportToDbForm ExportToDbForm
        {
            get
            {
                if (_ExportToDbForm == null)
                {
                    _ExportToDbForm = new ExportToDbForm("ExportToDbForm", true);
                    _ExportToDbForm.CssClass = "Axf";
                    _ExportToDbForm.SubmitButton.Text = "Esporta sul db";
                    _ExportToDbForm.InfoControl.Controls.Add(new LiteralControl("Ricordati prima di tutto di aver importato i gruppi. Inoltre devi copia-incollare la parte 'joinedsubclass' relativa all'anagrafica che vuoi esportare dal file 'AnagraficaBase.hbm' locale a quello remoto. Dopo aver importato l'anagrafe, bisogna assegnare il gruppo di default in cui verranno inseriti gli utenti che si registrano da frontend."));
                }
                return _ExportToDbForm;
            }
        }
        private ExportToDbForm _ExportToDbForm;
        
        public override WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.Controls.Add(ExportToDbForm);

            ExportToDbForm.Fields.Find(x => x.Key == "Assemblies").Controls.Add(ToggleAssemblies());

            return control;
        }

        private WebControl ToggleAssemblies()
        {
            WebControl p_toggle_namespace = new WebControl(HtmlTextWriterTag.P);
            p_toggle_namespace.Controls.Add(new LiteralControl("<input type=\"checkbox\" id=\"checkall\" /><label for=\"checkall\">Selezione/deseleziona tutti</label>"));
            

            string strJsCode = @"<script type=""text/javascript"" >
                                $(document).ready(function() {
                                  $('#checkall').click(function() {
                                    var checked = $(this).prop('checked');
                                    $('#Assemblies').find('input:checkbox').prop('checked', checked);
                                  });
                                })
                                </script>";

            p_toggle_namespace.Controls.Add(new LiteralControl(strJsCode));

            return p_toggle_namespace;
        }
    }
}
