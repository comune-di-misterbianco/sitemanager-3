﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using GenericDAO.DAO.Utility;
using System;
using NetService.Utility.ValidatedFields;
using System.Linq;
using NetCms.DBSessionProvider;
using NHibernate;
using System.Reflection;
using System.Collections.Generic;
using NHibernate.Criterion;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("anagrafica_dati_export.aspx")]
    public class CustomAnagrafeDatiExport : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Esporta Dati Anagrafica Custom"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User_Go; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Gestione Anagrafiche Custom"));
        }

        public NhRequestObject<NetCms.Users.CustomAnagrafe.CustomAnagrafe> CustomAnagrafe { get; private set; }

        public CustomAnagrafeDatiExport(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            CustomAnagrafe = new NhRequestObject<CustomAnagrafe>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "ca");
            this.StatusValidators.Add(new NhRequestObjectValidator<CustomAnagrafe>(CustomAnagrafe));
            this.ExportToDbForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
            SetToolbars();
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (ExportToDbForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                string database = ExportToDbForm.Fields.First(x => x.Key == "Database").PostBackValue;
                string server = ExportToDbForm.Fields.First(x => x.Key == "Server").PostBackValue;
                string username = ExportToDbForm.Fields.First(x => x.Key == "UserName").PostBackValue;
                string password = ExportToDbForm.Fields.First(x => x.Key == "Password").PostBackValue;
                bool manageRegRequest = (bool)ExportToDbForm.Fields.First(x => x.Key == "ManageWebRequestField").PostbackValueObject;
                List<string> assembliesName = ExportToDbForm.Fields.First(x => x.Key == "Assemblies").PostbackValueObject as List<string>;

                List<Assembly> assembliesToWorkWith = FluentSessionProvider.FluentAssemblies.Where(x => assembliesName.Contains(x.FullName)).ToList();

                string exceptionText = "";

                if (FluentSessionHelper.GetSessionFactory(database, server, username, password, assembliesToWorkWith) != null)
                {
                    bool isOk = true;

                    ICollection<User> usersToExport = null;
                    ICollection<UsersGroupsAssoc> assoc = new List<UsersGroupsAssoc>();

                    using (ISession sessionCms = FluentSessionProvider.Instance.OpenInnerSession())
                    {
                        CustomAnagrafe anagrafeToExport = CustomAnagrafeBusinessLogic.GetCustomAnagrafeById(CustomAnagrafe.Instance.ID, sessionCms);
                        usersToExport = UsersBusinessLogic.FindUsersByAnagraficaType("Anagrafica" + anagrafeToExport.Nome, sessionCms);

                        // carico le associazioni tra utenti e gruppi memorizzando username, nome del gruppo e nome del gruppo parent
                        foreach (User user in usersToExport)
                        {
                            foreach (GroupAssociation ga in user.AssociatedGroups)
                            {
                                assoc.Add(new UsersGroupsAssoc(user.UserName, ga.Group.Name, ga.Group.Parent == null ? "" : ga.Group.Parent.Name));
                            }
                        }
                    }

                    using (ISession sessionExternal = FluentSessionHelper.GetSession())
                    using (ITransaction txExternal = sessionExternal.BeginTransaction())
                    {
                        try
                        {
                            foreach (User utente in usersToExport)
                            {
                                using (ISession sessionExternalForUser = FluentSessionHelper.GetSession())
                                using (ITransaction txExternalForUser = sessionExternalForUser.BeginTransaction())
                                {
                                    if (UsersBusinessLogic.GetUserByUserName(utente.UserName, sessionExternalForUser) == null)
                                    {
                                        ICollection<AnagraficaCollection> collections = new List<AnagraficaCollection>();

                                        Hashtable anagrafeHashData = null;

                                        // leggo le associazioni utente-gruppi
                                        ICollection<UsersGroupsAssoc> usersGroupsAssocByUsername = assoc.Where(x => x.userName == utente.UserName).ToList();

                                        ICollection<Group> groupsToInsertTo = new List<Group>();

                                        // vedo quali sono i gruppi da associare
                                        foreach (UsersGroupsAssoc ass in usersGroupsAssocByUsername)
                                        {
                                            if (!string.IsNullOrEmpty(ass.parentName))
                                                groupsToInsertTo.Add(GroupsBusinessLogic.GetGroupByNameAndParentName(ass.groupName, ass.parentName, sessionExternalForUser));
                                            else groupsToInsertTo.Add(GroupsBusinessLogic.GetRootGroup(sessionExternalForUser));
                                        }

                                        IList<AllegatoUtente> allegatiToExport = new List<AllegatoUtente>();
                                        IList<AllegatoUtente> allegatiToImport = new List<AllegatoUtente>();

                                        using (ISession sessionCms = FluentSessionProvider.Instance.OpenInnerSession())
                                        {
                                            allegatiToExport = CustomAnagrafeBusinessLogic.FindAttachmentsByAnagrafe(utente.AnagraficaMapped, sessionCms);

                                            ISession mapSessionCms = sessionCms.GetSession(EntityMode.Map);
                                            anagrafeHashData = UsersBusinessLogic.GetAnagraficaMapUser(utente, mapSessionCms);

                                            // carico i riferimenti per le collezioni (uno a molti e molti a molti) che non siano gli allegati
                                            foreach (DictionaryEntry entry in anagrafeHashData)
                                            {
                                                if (entry.Value is ICollection && entry.Key.ToString() != "Allegati" && entry.Key.ToString() != "User")
                                                {
                                                    IList list = (IList)entry.Value;

                                                    List<ItemOfCollection> items = new List<ItemOfCollection>();

                                                    foreach (Hashtable item in list)
                                                    {
                                                        items.Add(new ItemOfCollection(int.Parse(item["ID"].ToString()), item["$type$"].ToString()));
                                                    }

                                                    collections.Add(new AnagraficaCollection(entry.Key.ToString(), items));
                                                }
                                            }
                                        }

                                        foreach (AllegatoUtente allegato in allegatiToExport)
                                        {
                                            allegatiToImport.Add(allegato.Clone());
                                        }

                                        // setto le collezioni (uno a molti e molti a molti)
                                        UsersBusinessLogic.SetCollectionsAnagrafeData(sessionExternalForUser, anagrafeHashData, collections);

                                        // setto le foreign key e gli allegati
                                        UsersBusinessLogic.CleanAnagrafeData(sessionExternalForUser, anagrafeHashData, UsersBusinessLogic.BuilAllegatiDictionaries(allegatiToImport));

                                        UsersBusinessLogic.CreateUser(sessionExternalForUser,
                                            utente.UserName,
                                            utente.Password,
                                            utente.CreationTime,
                                            utente.LastAccess,
                                            utente.LastUpdate,
                                            utente.State,
                                            anagrafeHashData,
                                            utente.AnagraficaType,
                                            groupsToInsertTo,                                            
                                            manageRegRequest,
                                            utente.ActivationKey);
                                    }
                                    txExternalForUser.Commit();
                                }
                            }
                            txExternal.Commit();
                        }
                        catch (Exception ex)
                        {
                            exceptionText = ex.Message;
                            txExternal.Rollback();
                            isOk = false;
                        }
                    }

                    FluentSessionHelper.CloseSessionFactory();

                    if (isOk)
                        PopupBox.AddSessionMessage("Utenti importati con successo", PostBackMessagesType.Success, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(MainPage)));
                    else
                        PopupBox.AddMessage("Errore nell'importazione: " + exceptionText, PostBackMessagesType.Error);

                }
                else
                    PopupBox.AddMessage("Errore nella creazione della session factory remota", PostBackMessagesType.Error);
            }
        }

        private struct UsersGroupsAssoc
        {
            public UsersGroupsAssoc(string userName, string groupName, string parentName)
            {
                this.userName = userName;
                this.groupName = groupName;
                this.parentName = parentName;
            }

            public string userName;
            public string parentName;
            public string groupName;
        }

        public ExportToDbForm ExportToDbForm
        {
            get
            {
                if (_ExportToDbForm == null)
                {
                    _ExportToDbForm = new ExportToDbForm("ExportToDbForm", true);
                    _ExportToDbForm.CssClass = "Axf";
                    _ExportToDbForm.SubmitButton.Text = "Esporta sul db";
                    _ExportToDbForm.InfoControl.Controls.Add(new LiteralControl("Ricordati prima di tutto di aver importato i gruppi. Inoltre devi copia-incollare la parte 'joinedsubclass' relativa all'anagrafica che vuoi esportare dal file 'AnagraficaBase.hbm' locale a quello remoto. Infine devi aver già importato la struttura di questa anagrafe"));
                }
                return _ExportToDbForm;
            }
        }
        private ExportToDbForm _ExportToDbForm;

        public override WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.Controls.Add(ExportToDbForm);
            return control;
        }
    }
}
