﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using GenericDAO.DAO.Utility;
using System;
using NetService.Utility.ValidatedFields;
using System.Linq;
using NetCms.DBSessionProvider;
using NHibernate;
using System.Reflection;
using System.Collections.Generic;
using NHibernate.Criterion;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("groups_export.aspx")]
    public class GroupsExport : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Esportazione Gruppi CMS"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Group_Go; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Gestione Anagrafiche Custom"));
        }

        public GroupsExport(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            this.ExportToDbForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
            SetToolbars();
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (ExportToDbForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                string database = ExportToDbForm.Fields.First(x => x.Key == "Database").PostBackValue;
                string server = ExportToDbForm.Fields.First(x => x.Key == "Server").PostBackValue;
                string username = ExportToDbForm.Fields.First(x => x.Key == "UserName").PostBackValue;
                string password = ExportToDbForm.Fields.First(x => x.Key == "Password").PostBackValue;
                List<string> assembliesName = ExportToDbForm.Fields.First(x => x.Key == "Assemblies").PostbackValueObject as List<string>;

                List<Assembly> assembliesToWorkWith = FluentSessionProvider.FluentAssemblies.Where(x => assembliesName.Contains(x.FullName)).ToList();

                if (FluentSessionHelper.GetSessionFactory(database, server, username, password, assembliesToWorkWith) != null)
                {
                    bool isOk = true;

                    ICollection<MacroGroup> macroGroupsToExport = null;
                    ICollection<Group> groupsToExport = null;
                    ICollection<GroupMacroGroupAssoc> assoc = new List<GroupMacroGroupAssoc>();

                    using (ISession sessionCms = FluentSessionProvider.Instance.OpenInnerSession())
                    {
                        macroGroupsToExport = GroupsBusinessLogic.FindAllMacroGroups(sessionCms);
                        groupsToExport = GroupsBusinessLogic.GetRootGroup(sessionCms).Groups;

                        foreach (Group group in groupsToExport)
                        {
                            // memorizzo i figli di root con i riferimenti ai macrogruppi
                            assoc.Add(new GroupMacroGroupAssoc(group.Name, group.Parent.Name, group.MacroGroup.Key));
                        }
                    }

                    using (ISession sessionExternal = FluentSessionHelper.GetSession())
                    using (ITransaction tx = sessionExternal.BeginTransaction())
                    {
                        try
                        {
                            foreach (MacroGroup macroGruppo in macroGroupsToExport)
                            {
                                if (GroupsBusinessLogic.GetMacroGroupByKey(macroGruppo.Key, sessionExternal) == null)
                                    sessionExternal.SaveOrUpdate(macroGruppo.Clone());
                            }
                            
                            Group rootRemoteGroup = GroupsBusinessLogic.GetRootGroup(sessionExternal);

                            foreach (Group group in groupsToExport)
                            {
                                GroupMacroGroupAssoc ass = assoc.Where(x => x.groupName == group.Name).FirstOrDefault();

                                if (GroupsBusinessLogic.GetGroupByNameAndParentName(ass.groupName, ass.parentName, sessionExternal) == null)
                                    sessionExternal.SaveOrUpdate(group.Clone(rootRemoteGroup));
                            }

                            foreach (GroupMacroGroupAssoc ass in assoc)
                            {
                                Group group = GroupsBusinessLogic.GetGroupByNameAndParentName(ass.groupName, ass.parentName, sessionExternal);
                                group.MacroGroup = GroupsBusinessLogic.GetMacroGroupByKey(ass.macroGroupKey, sessionExternal);

                                foreach (Group subGroup in GroupsBusinessLogic.FindGroupsByTreeStart(group.Tree, sessionExternal))
                                    subGroup.MacroGroup = group.MacroGroup;

                                GroupsBusinessLogic.SaveOrUpdateGroup(group, sessionExternal);
                            }

                            tx.Commit();
                        }
                        catch (Exception ex)
                        {
                            tx.Rollback();
                            isOk = false;
                        }
                    }

                    FluentSessionHelper.CloseSessionFactory();

                    if (isOk)
                        PopupBox.AddSessionMessage("Gruppi importati con successo", PostBackMessagesType.Success, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(MainPage)));
                    else
                        PopupBox.AddMessage("Errore nell'importazione", PostBackMessagesType.Error);

                }
                else
                    PopupBox.AddMessage("Errore nella creazione della session factory remota", PostBackMessagesType.Error);
            }
        }

        private struct GroupMacroGroupAssoc
        {
            public GroupMacroGroupAssoc(string groupName, string parentName, string macroGroupKey)
            {
                this.groupName = groupName;
                this.parentName = parentName;
                this.macroGroupKey = macroGroupKey;
            }

            public string groupName;
            public string parentName;
            public string macroGroupKey;
        }

        public ExportToDbForm ExportToDbForm
        {
            get
            {
                if (_ExportToDbForm == null)
                {
                    _ExportToDbForm = new ExportToDbForm("ExportToDbForm");
                    _ExportToDbForm.CssClass = "Axf";
                    _ExportToDbForm.SubmitButton.Text = "Esporta sul db";
                }
                return _ExportToDbForm;
            }
        }
        private ExportToDbForm _ExportToDbForm;

        public override WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.Controls.Add(ExportToDbForm);
            return control;
        }
    }
}
