﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using GenericDAO.DAO.Utility;
using System;
using NetService.Utility.ValidatedFields;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using NetService.Utility.UI;
using NetCms.Vertical;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("/applicazioni.aspx")]
    public class CustomAnagrafeApplicationsPage : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Gestione Servizi associati agli utenti dell'Anagrafica Custom '" + CustomAnagrafe.Instance.Nome + "'"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Application; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Gestione Anagrafiche Custom"));
        }

        public NhRequestObject<CustomAnagrafe> CustomAnagrafe { get; private set; }

        public NhRequestObject<CustomAnagrafeApplicazioni> CustomAnagrafeApplicazione { get; private set; }

        public CustomAnagrafeApplicationsPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            CustomAnagrafe = new NhRequestObject<CustomAnagrafe>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "ca");
            CustomAnagrafeApplicazione = new NhRequestObject<CustomAnagrafeApplicazioni>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "caapp");

            this.StatusValidators.Add(new NhRequestObjectValidator<CustomAnagrafe>(CustomAnagrafe));
            SetToolbars();
            this.Form.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.Form.IsValid == VfGeneric.ValidationStates.Valid)
            {
                CustomAnagrafe ca = CustomAnagrafe.Instance;
                int applicationId = int.Parse(this.Form.Servizi.PostBackValue);

                if (CustomAnagrafeBusinessLogic.GetCustomAnagrafeApplication(ca, applicationId) == null)
                {

                    CustomAnagrafeApplicazioni caapp = new CustomAnagrafeApplicazioni();
                    caapp.Anagrafe = ca;
                    caapp.IdApplicazione = applicationId;

                    bool ok = CustomAnagrafeBusinessLogic.SaveOrUpdateCustomAnagrafeApplication(caapp);
                    if (ok)
                        NetCms.GUI.PopupBox.AddSessionMessage("Associazione inserita con successo.", PostBackMessagesType.Success, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomAnagrafeApplicationsPage)) + "?ca=" + CustomAnagrafe.Instance.ID);
                    else
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
                }
                else NetCms.GUI.PopupBox.AddMessage("Associazione già presente", PostBackMessagesType.Notify);
            }
        }

        public override WebControl BuildControl()
        {
            if (EliminationRequest && CustomAnagrafeApplicazione.Exists)
            {
                EliminationConfirmControl delcontrol = DeleteControl("Conferma eliminazione associazione", "Sei sicuro di voler eliminare l'associazione?", NetCms.Vertical.PagesHandler.GetPageAddress(typeof(CustomAnagrafeApplicationsPage)) + "?ca=" + CustomAnagrafe.Instance.ID);
                delcontrol.ConfirmButton.Click += new EventHandler(ConfirmEliminationButton_Click);
                return delcontrol;
            }
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.Controls.Add(Form);
            control.Controls.Add(GetTableControl());
            return control;
        }

        void ConfirmEliminationButton_Click(object sender, EventArgs e)
        {
            CustomAnagrafeBusinessLogic.DeleteCustomAnagrafeApplication(CustomAnagrafeApplicazione.Instance);

            NetCms.GUI.PopupBox.AddSessionMessage("Associazione eliminata con successo", PostBackMessagesType.Success, true);
        }

        public ApplicationsForm Form
        {
            get
            {
                if (_Form == null)
                {
                    _Form = new ApplicationsForm("Applications selector");
                }
                return _Form;
            }
        }

        private ApplicationsForm _Form;

        private WebControl GetTableControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);

            ArTable applicationsTable = new ArTable();
            applicationsTable.EnablePagination = true;
            applicationsTable.RecordPerPagina = 20;
            applicationsTable.InnerTableCssClass = "tab";
            applicationsTable.NoRecordMsg = "Nessuna applicazione associata all'anagrafica custom " + CustomAnagrafe.Instance.Nome;

            PaginationHandler pagination = new PaginationHandler(applicationsTable.RecordPerPagina, CustomAnagrafeBusinessLogic.CountAnagraficaApplicationsByAnagrafe(CustomAnagrafe.Instance), applicationsTable.PaginationKey);
            applicationsTable.PagesControl = pagination;

            applicationsTable.Records = CustomAnagrafeBusinessLogic.FindAnagraficaApplicationsByAnagrafe(CustomAnagrafe.Instance);

            ArApplicationTextColumn appColumn = new ArApplicationTextColumn("Applicazione", "IdApplicazione");
            applicationsTable.AddColumn(appColumn);

            ArActionColumn delete = new ArActionColumn("Elimina", ArActionColumn.Icons.Delete, "applicazioni.aspx?action=delete&ca={Anagrafe.ID}&caapp={ID}");
            applicationsTable.AddColumn(delete);

            control.Controls.Add(applicationsTable);

            return control;

        }

    }
}
