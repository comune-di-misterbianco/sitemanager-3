﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using GenericDAO.DAO.Utility;
using System;
using NetService.Utility.ValidatedFields;

namespace NetCms.Users.CustomAnagrafe.View
{
    [DynamicUrl.PageControl("anagrafica_mod.aspx")]
    public class CustomAnagrafeMod : CustomAnagrafePage
    {
        public override string PageTitle
        {
            get { return "Modifica Anagrafica Custom"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User_Edit; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Gestione Anagrafiche Custom"));
            
        }

        public NhRequestObject<NetCms.Users.CustomAnagrafe.CustomAnagrafe> CustomAnagrafe { get; private set; }

        public CustomAnagrafeMod(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            CustomAnagrafe = new NhRequestObject<CustomAnagrafe>(CustomAnagrafeBusinessLogic.GetCurrentSession(), "ca");
            this.StatusValidators.Add(new NhRequestObjectValidator<CustomAnagrafe>(CustomAnagrafe));
            SetToolbars();
            this.CustomAnagrafeForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);

            OuterScripts.Add(@"<script type='text/javascript'> function ShowHide() {
                            if (document.getElementById('EmailAsUserName').checked ) {                                 
                                 $('#campo_username').hide();
                                } else {                                        
                                         $('#campo_username').show();
                                         } return false;}</script>");


            OuterScripts.Add(@"<script type='text/javascript'> function ShowHideInformativa() {
                            if (document.getElementById('AggiungiInformativa').checked ) {                                 
                                 $('#LabelInformativaContainer').show();
                                 $('#TestoInformativaContainer').show();
                                 $('#DomandaAccettazioneInformativaContainer').show();
                                 $('#NumeroColonneAreaInformativaContainer').show();
                                 $('#NumeroRigheAreaInformativaContainer').show();
                                } else {                                        
                                         $('#LabelInformativaContainer').hide();
                                         $('#TestoInformativaContainer').hide();
                                         $('#DomandaAccettazioneInformativaContainer').hide();
                                         $('#NumeroColonneAreaInformativaContainer').hide();
                                         $('#NumeroRigheAreaInformativaContainer').hide();
                                         } return false;}

                                 $(function () {
                                    ShowHideInformativa(); });
                                </script>");

            Username = this.CustomAnagrafeForm.Fields.Find(x => x.Key == "Username") as VfDropDown;           
           
           
        }
        private VfDropDown Username
        {
            get;
            set;
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (!((this.CustomAnagrafeForm.Fields.Find(x => x.Key == "EmailAsUserName") as VfCheckBox).PostBackValue == "on"))
            {
                Username.Validators.Add(new RequiredIfValidator("Campo Richiesto!", Username.PostbackValueObject, RequiredIfValidator.CompareTypes.ValueNotNull));
            }
            Username.Validate();
            if (this.CustomAnagrafeForm.IsValid == VfGeneric.ValidationStates.Valid && Username.ValidationState == VfGeneric.ValidationStates.Valid)
            {
                bool status = CustomAnagrafeBusinessLogic.ModAnagraficaCustom(this.CustomAnagrafeForm, CustomAnagrafe.Instance);
                if (status)
                    NetCms.GUI.PopupBox.AddSessionMessage("Anagrafe custom modificata con successo.", PostBackMessagesType.Success, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(MainPage)));
                else
                    NetCms.GUI.PopupBox.AddMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
            }
        }

        public CustomAnagrafeForm CustomAnagrafeForm
        {
            get
            {
                if (_CustomAnagrafeForm == null)
                {                   
                    _CustomAnagrafeForm = new CustomAnagrafeForm("CustomAnagrafeForm", CustomAnagrafe.Instance);
                    _CustomAnagrafeForm.CssClass = "Axf";
                    _CustomAnagrafeForm.SubmitButton.Text = "Modifica";
                    _CustomAnagrafeForm.DataBindAdvanced(CustomAnagrafe.Instance);
                }
                return _CustomAnagrafeForm;
            }
        }
        private CustomAnagrafeForm _CustomAnagrafeForm;

        public override WebControl BuildControl()
        {
           
            if (IsPostBack && !((this.CustomAnagrafeForm.Fields.Find(x => x.Key == "EmailAsUserName") as VfCheckBox).PostBackValue == "on"))
            {
               
                (this.CustomAnagrafeForm.Fields.Find(x => x.Key == "EmailAsUserName") as VfCheckBox).DefaultValue = false;
                Username.Attributes.Add("style", "display : block");
            }
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.Controls.Add(CustomAnagrafeForm);
            return control;
        }

    }
}
