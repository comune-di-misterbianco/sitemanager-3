﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using NetCms.Vertical;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomAnagrafeApplication : VerticalApplication
    {
        public override VerticalApplication.AccessModes AccessMode
        {
            get { return AccessModes.Admin; }
        }

        public NetCms.Vertical.Configuration.Configuration Configuration
        {
            get
            {
                return _Configuration ?? (_Configuration = new NetCms.Vertical.Configuration.Configuration(this.SystemName));
            }
        }
        private NetCms.Vertical.Configuration.Configuration _Configuration;

        public override int HomepageGroup
        {
            get { return 3; }
        }

        public const string SystemNameKey = "customanagrafe";
        public const int IDKey = 3001;

        public override bool CheckForDefaultGrant
        {
            get { return false; }
        }

        public override string Description
        {
            get { return "Custom Anagrafe"; }
        }

        public override int ID
        {
            get { return IDKey; }
        }

        public override string Label
        {
            get { return "Generazione Anagrafiche Custom"; }
        }

        public override bool NetworkDependant
        {
            get { return false; }
        }

        public override int SecurityLevel
        {
            get { return 0; }
        }

        public override string SystemName
        {
            get { return "customanagrafe"; }
        }

        public override bool DoAdditionalConfigurationForExternalDB()
        {
            throw new NotImplementedException();
        }

        public override bool NeedAdditionalConfiguration
        {
            get { return false; }
        }

        public override VerticalApplicationSetup Installer
        {
            get
            {
                if (_Installer == null)
                    _Installer = new CustomAnagrafeSetup(this);
                return _Installer;
            }
        }
        private CustomAnagrafeSetup _Installer;       

        public CustomAnagrafeApplication() : base()
        {
           
        }

        public CustomAnagrafeApplication(Assembly assembly)
            : base(assembly)
        {
        }

        private string _BackofficeUrl;
        public string BackofficeUrl
        {
            get 
            {
                return "/applications/" + SystemNameKey + "/admin/default.aspx";
            }
        }

        public override bool UseCmsSessionFactory
        {
            get { return true; }
        }
    }
}
