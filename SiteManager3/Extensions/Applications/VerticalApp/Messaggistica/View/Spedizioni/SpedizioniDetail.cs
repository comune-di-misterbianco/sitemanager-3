﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using Messaggistica.Model;
using NetService.Utility.ArTable;
using GenericDAO.DAO.Utility;

namespace Messaggistica.View
{
    [DynamicUrl.PageControl("/details.aspx")]
    public class SpedizioniDetail : MessaggisticaPage
    {
        public override string PageTitle
        {
            get { return "Dettagli spedizione"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("spedizioni_list.aspx", NetCms.GUI.Icons.Icons.Table, "Archivio Spedizioni"));
        }

        public NhRequestObject<Spedizioni> Spedizione { get; private set; }

        public SpedizioniDetail(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            Spedizione = new NhRequestObject<Spedizioni>(BusinessLogic.GetCurrentSession(), "spedizione");
            this.StatusValidators.Add(new NhRequestObjectValidator<Spedizioni>(Spedizione));
            SetToolbars();
        }

        public override WebControl BuildControl()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);

            Spedizioni sped = Spedizione.Instance;
            
            div.Controls.Add(sped.GetDetailControl());

            WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            fieldset.CssClass = "messaggistica_fieldset";
            fieldset.Controls.Add(legend);
            fieldset.Controls.Add(sped.GetArTableDestinatari());
            legend.Controls.Add(new LiteralControl("Destinatari"));

            div.Controls.Add(fieldset);

            return div;
        }

    }
}
