﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetService.Utility.ValidatedFields;
using Messaggistica.Model;
using System.Collections.Generic;
using System.Linq;
using UsersAggregator.Model;
using NHibernate;
using NetCms.Diagnostics;

namespace Messaggistica.View
{
    [DynamicUrl.PageControl("/spedizioni_new.aspx")]
    public class SpedizioniNew : MessaggisticaPage
    {
        public override string PageTitle
        {
            get { return "Nuova spedizione"; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("spedizioni_list.aspx", NetCms.GUI.Icons.Icons.Table, "Archivio Spedizioni"));
        }

        public SpedizioniNew(StateBag viewstate, bool IsPostBack, NetCms.GUI.Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewstate, IsPostBack, toolbar, informationBox, networkKey)
        {
            SetToolbars();
            this.SpedizioneForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.SpedizioneForm.CanaleProviderForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                List<string> destinatari = BuildDestinatariList();

                if (destinatari.Count > 0)
                {
                    bool ok = false;
                    using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                    using (ITransaction tx = sess.BeginTransaction())
                    {
                        try
                        {
                            Result result = BusinessLogic.SendMessage(this.SpedizioneForm.CanaleProviderForm, destinatari, sess);
                            if (result.IsOk)
                            {
                                tx.Commit();
                                ok = true;
                            }
                            else
                            {
                                tx.Rollback();
                                PopupBox.AddMessage(result.Error, PostBackMessagesType.Error);
                            }
                            
                            
                        }
                        catch (Exception ex)
                        {
                            tx.Rollback();
                            Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, "", "", "");
                            
                        }
                        
                    }
                    if (ok)
                        PopupBox.AddSessionMessage("Messaggio inviato con successo.", PostBackMessagesType.Success, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(SpedizioniList)));

                    //Result result = BusinessLogic.SendMessage(this.SpedizioneForm.CanaleProviderForm, destinatari);
                    //if (result.IsOk)
                    //    PopupBox.AddSessionMessage("Messaggio inviato con successo.", PostBackMessagesType.Success, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(SpedizioniList)));
                    //else PopupBox.AddMessage(result.Error, PostBackMessagesType.Error);                
                }
                else
                    PopupBox.AddMessage("Destinatari non scelti o dati inseriti non corretti.", PostBackMessagesType.Error);
            }
        }

        private List<string> BuildDestinatariList()
        {
            List<string> destinatari = new List<string>();

            if (this.SpedizioneForm.InvioSingolo != null)
            {
                this.SpedizioneForm.InvioSingolo.Validate();
                if (this.SpedizioneForm.InvioSingolo.ValidationState == VfGeneric.ValidationStates.Valid)
                {
                    destinatari.Add(this.SpedizioneForm.InvioSingolo.PostBackValue);
                }
            }

            if (this.SpedizioneForm.UtentiInLista != null)
            {
                this.SpedizioneForm.InvioListe.Validate();
                if (this.SpedizioneForm.InvioListe.ValidationState == VfGeneric.ValidationStates.Valid)
                {
                    if (this.SpedizioneForm.CanaleProviderForm.CanaleField.PostBackValue.ToLower() == "sms")
                        destinatari.AddRange((this.SpedizioneForm.UtentiInLista.Records as IEnumerable<Destinatari>).Where(x => !string.IsNullOrEmpty(x.Cellulare)).Select(x => x.Cellulare.Replace("+","")));
                    else destinatari.AddRange((this.SpedizioneForm.UtentiInLista.Records as IEnumerable<Destinatari>).Where(x => !string.IsNullOrEmpty(x.Email)).Select(x => x.Email));
                }
            }
            return destinatari;
        }

        public SpedizioneForm SpedizioneForm
        {
            get
            {
                if (_SpedizioneForm == null)
                {
                    _SpedizioneForm = new SpedizioneForm("SpedizioneForm");                    
                    _SpedizioneForm.CssClass = "Axf";
                }
                return _SpedizioneForm;
            }
        }
        private SpedizioneForm _SpedizioneForm;

        public override WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.ID = Application.SystemName;

            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            legend.Controls.Add(new LiteralControl(PageTitle));
            control.Controls.Add(legend);

            control.Controls.Add(SpedizioneForm);

            return control;
        }
    }
}
