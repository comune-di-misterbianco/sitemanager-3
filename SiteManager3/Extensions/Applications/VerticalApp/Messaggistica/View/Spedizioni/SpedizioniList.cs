﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using NetService.Utility.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NetService.Utility.RecordsFinder;

namespace Messaggistica.View
{
    [DynamicUrl.PageControl("/spedizioni_list.aspx")]
    public class SpedizioniList : MessaggisticaPage
    {
        public override string PageTitle
        {
            get { return "Archivio Spedizioni"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Table; }
        }

        private void SetToolbars()
        {            
            this.Toolbar.Buttons.Add(new ToolbarButton("spedizioni_new.aspx", NetCms.GUI.Icons.Icons.Email_Add, "Nuova Spedizione"));
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Email, "Torna indietro"));
        }

        public SpedizioniList(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            SetToolbars();
        }

        public override WebControl BuildControl()
        {
            return GetTableControl();
        }

        private SpedizioneSearchForm _SearchForm;
        public SpedizioneSearchForm SearchForm
        {
            get
            {
                if (_SearchForm == null)
                {
                    _SearchForm = new SpedizioneSearchForm("SearchForm");
                    _SearchForm.TitleControl.Controls.Add(new LiteralControl("Cerca nell'archivio delle spedizioni"));
                    _SearchForm.ShowMandatoryInfo = false;
                    _SearchForm.AutoValidation = false;
                    _SearchForm.CssClass = "Axf";
                    _SearchForm.SubmitButton.Text = "Cerca";
                }
                return _SearchForm;

            }
        }

        private WebControl GetTableControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);

            int rpp = 15;

            #region Form di ricerca

            WebControl findForm = new WebControl(HtmlTextWriterTag.Fieldset);

            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            legend.Controls.Add(new LiteralControl("Modulo di ricerca"));
            findForm.Controls.Add(legend);

            findForm.Controls.Add(SearchForm);
            control.Controls.Add(findForm);

            #endregion
 
            List<SearchParameter> searchParameter = SearchForm.GetSearchParameter();           

            int numSpedizioni = BusinessLogic.CountSpedizioni(searchParameter.ToArray());
            
            ArTable spedizioniTable = new ArTable();
            spedizioniTable.EnablePagination = true;
            spedizioniTable.RecordPerPagina = rpp;
            spedizioniTable.InnerTableCssClass = "tab";
            spedizioniTable.NoRecordMsg = "Nessuna spedizione effettuata";
            spedizioniTable.MainTitle = " " + numSpedizioni + " record trovati";       

            PaginationHandler pagination = new PaginationHandler(rpp, numSpedizioni, spedizioniTable.PaginationKey);
            spedizioniTable.PagesControl = pagination;

            IDictionary<string, bool> orders = new Dictionary<string, bool>();
            orders.Add("DataOra", true);

            var spedizioni = BusinessLogic.FindSpedizioni(pagination.CurrentPage, 
                                                          pagination.PageSize,
                                                          orders,
                                                          searchParameter.ToArray());

            spedizioniTable.Records = spedizioni;

            ArTextColumn dataOra = new ArTextColumn("Data/Ora", "DataOra");
            dataOra.DataType = ArTextColumn.DataTypes.DateTime;
            spedizioniTable.AddColumn(dataOra);

            ArUsersColumn operatore = new ArUsersColumn("Operatore", "IdOperatore");
            spedizioniTable.AddColumn(operatore);

            ArTextColumn provider = new ArTextColumn("Provider", "Provider.Nome");
            spedizioniTable.AddColumn(provider);

            ArTextColumn canale = new ArTextColumn("Canale", "Provider.Canale.Nome");
            spedizioniTable.AddColumn(canale);

            ArActionColumn dettaglioSpedizione = new ArActionColumn("Dettagli", ArActionColumn.Icons.Details, "details.aspx?spedizione={Id}");
            spedizioniTable.AddColumn(dettaglioSpedizione);

            control.Controls.Add(spedizioniTable);

            return control;

        }
    }
}
