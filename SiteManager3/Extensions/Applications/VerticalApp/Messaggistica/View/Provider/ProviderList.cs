﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using NetService.Utility.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GenericDAO.DAO.Utility;
using Messaggistica.Model;
using G2Core.Common;

namespace Messaggistica.View
{
    [DynamicUrl.PageControl("/provider_list.aspx")]
    public class ProviderList : MessaggisticaPage
    {
        private RequestVariable idCanale = new RequestVariable("idc", RequestVariable.RequestType.QueryString);

        public override string PageTitle
        {
            get { return "Elenco Provider"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Application_View_List; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Email, "Pagina iniziale"));
            this.Toolbar.Buttons.Add(new ToolbarButton("provider_new.aspx", NetCms.GUI.Icons.Icons.Transmit_Add, "Nuovo Provider"));
        }

        public NhRequestObject<Provider> Provider { get; private set; }

        public ProviderList(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            this.StatusValidators.Add(new PagineConfigurazioneValidator(this.Application));

            Provider = new NhRequestObject<Provider>(BusinessLogic.GetCurrentSession(), "provider");
            SetToolbars();
        }

        public override WebControl BuildControl()
        {
            //  Toolbar.Buttons.Add(new ToolbarButton("provider_add", "../admin/newsletter/provider/provider_add.aspx?idc=" + idCanale, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Page_Code), "Provider"));
         
            if (EliminationRequest && Provider.Exists)
            {
                EliminationConfirmControl delcontrol = DeleteControl("Conferma eliminazione provider '" + Provider.Instance.Nome + "'", "Sei sicuro di voler eliminare il provider?", NetCms.Vertical.PagesHandler.GetPageAddress(typeof(ProviderList)));
                delcontrol.ConfirmButton.Click += new EventHandler(ConfirmButton_Click);
                return delcontrol;
            }
            return GetTableControl();
        }

        void ConfirmButton_Click(object sender, EventArgs e)
        {
            BusinessLogic.DeleteProvider(Provider.Instance);
            NetCms.GUI.PopupBox.AddSessionMessage("Provider eliminato con successo", PostBackMessagesType.Success, true);
        }

        private WebControl GetTableControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);

            ArTable providerTable = new ArTable();
            providerTable.EnablePagination = false;
            providerTable.RecordPerPagina = 20;
            providerTable.InnerTableCssClass = "tab";
            providerTable.NoRecordMsg = "Nessun provider presente";

            PaginationHandler pagination = new PaginationHandler(20, BusinessLogic.CountProvider(null), providerTable.PaginationKey);
            providerTable.PagesControl = pagination;

            var providers = BusinessLogic.FindProvider(((pagination.CurrentPage - 1) * pagination.PageSize), pagination.PageSize, "Nome", true, null);

            providerTable.Records = providers;

            ArTextColumn nome = new ArTextColumn("Nome", "Nome");
            providerTable.AddColumn(nome);

            ArTextColumn canale = new ArTextColumn("Canale", "Canale.Nome");
            providerTable.AddColumn(canale);

            ArActionColumn modifica = new ArActionColumn("Modifica", ArActionColumn.Icons.Edit, "provider_mod.aspx?provider={Id}");
            providerTable.AddColumn(modifica);

            ArActionColumn dettaglio = new ArActionColumn("Dettagli", ArActionColumn.Icons.Details, "provider_details.aspx?provider={Id}");
            providerTable.AddColumn(dettaglio);

            ArActionColumn elimina = new ArActionColumn("Elimina", ArActionColumn.Icons.Delete, "provider_list.aspx?action=delete&provider={Id}");
            providerTable.AddColumn(elimina);

            control.Controls.Add(providerTable);

            return control;

        }
    }
}
