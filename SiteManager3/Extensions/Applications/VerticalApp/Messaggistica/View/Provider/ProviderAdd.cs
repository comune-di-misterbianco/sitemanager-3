﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetService.Utility.ValidatedFields;
using Messaggistica.Model;

namespace Messaggistica.View
{
    [DynamicUrl.PageControl("/provider_new.aspx")]
    public class ProviderAdd : MessaggisticaPage
    {
        public override string PageTitle
        {
            get { return "Nuovo provider"; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("provider_list.aspx", NetCms.GUI.Icons.Icons.Application_View_List, "Elenco Provider"));
        }

        public ProviderAdd(StateBag viewstate, bool IsPostBack, NetCms.GUI.Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewstate, IsPostBack, toolbar, informationBox, networkKey)
        {
            this.StatusValidators.Add(new PagineConfigurazioneValidator(this.Application));

            SetToolbars();
            this.ProviderForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.ProviderForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                Provider prov = BusinessLogic.CreateProvider(this.ProviderForm);
                if (prov!= null)
                    PopupBox.AddSessionMessage("Provider creato con successo.", PostBackMessagesType.Success, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(ProviderList)));
                else PopupBox.AddMessage("Errore nella creazione del provider", PostBackMessagesType.Error);
            }
        }

        public ProviderForm ProviderForm
        {
            get
            {
                if (_ProviderForm == null)
                {
                    _ProviderForm = new ProviderForm("ProviderForm");
                    _ProviderForm.CssClass = "Axf";
                    _ProviderForm.SubmitButton.Text = "Salva";
                }
                return _ProviderForm;
            }
        }
        private ProviderForm _ProviderForm;

        public override WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.ID = Application.SystemName;

            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            legend.Controls.Add(new LiteralControl(PageTitle));
            control.Controls.Add(legend);

            control.Controls.Add(ProviderForm);
            return control;
        }
    }
}
