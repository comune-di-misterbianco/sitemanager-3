﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetService.Utility.ValidatedFields;
using System.Linq;
using GenericDAO.DAO.Utility;
using Messaggistica.Model;
using System.Collections.Generic;

namespace Messaggistica.View
{
    [DynamicUrl.PageControl("/provider_mod.aspx")]
    public class ProviderMod : MessaggisticaPage
    {
        public override string PageTitle
        {
            get { return "Modifica provider"; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("provider_list.aspx", NetCms.GUI.Icons.Icons.Application_View_List, "Elenco Provider"));
        }

        public NhRequestObject<Provider> Provider { get; private set; }

        public ProviderMod(StateBag viewstate, bool IsPostBack, NetCms.GUI.Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewstate, IsPostBack, toolbar, informationBox, networkKey)
        {
            this.StatusValidators.Add(new PagineConfigurazioneValidator(this.Application));

            Provider = new NhRequestObject<Provider>(BusinessLogic.GetCurrentSession(), "provider");
            this.StatusValidators.Add(new NhRequestObjectValidator<Provider>(Provider));
            SetToolbars();
            this.ProviderForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.ProviderForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                bool presente = false;
                string nome = ProviderForm.Fields.First(x=>x.Key=="Nome").PostBackValue;
                if (BusinessLogic.ProviderPresenteInUpdate(Provider.Instance, nome))
                {
                    presente = true;
                    NetCms.GUI.PopupBox.AddMessage("Provider già presente.", PostBackMessagesType.Notify);
                }

                if (!presente)
                {
                    if (BusinessLogic.ModProvider(this.ProviderForm, Provider.Instance) != null)
                        NetCms.GUI.PopupBox.AddSessionMessage("Provider modificato con successo.", PostBackMessagesType.Success, true);
                    else
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
                }
            }
        }

        public ProviderForm ProviderForm
        {
            get
            {
                if (_ProviderForm == null)
                {
                    _ProviderForm = new ProviderForm("ProviderForm", Provider.Instance);
                    _ProviderForm.CssClass = "Axf";
                    _ProviderForm.SubmitButton.Text = "Modifica";
                    _ProviderForm.DataBindAdvanced(Provider.Instance);
                    
                    List<string> networks = new List<string>();
                    foreach (ProviderNetwork pn in Provider.Instance.ProviderNetworks)
                    {
                        networks.Add(pn.IdNetwork.ToString());
                    }

                    _ProviderForm.Fields.First(x => x.Key == "ProviderNetworks").DefaultValue = networks;
                }
                return _ProviderForm;
            }
        }
        private ProviderForm _ProviderForm;

        public override WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.ID = Application.SystemName;

            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            legend.Controls.Add(new LiteralControl(PageTitle));
            control.Controls.Add(legend);

            control.Controls.Add(ProviderForm);
            return control;
        }
    }
}
