﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using Messaggistica.Model;
using GenericDAO.DAO.Utility;

namespace Messaggistica.View
{
    [DynamicUrl.PageControl("/provider_details.aspx")]
    public class ProviderDetail : MessaggisticaPage
    {
        public override string PageTitle
        {
            get { return "Dettagli Provider " + Provider.Instance.Nome; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("provider_list.aspx", NetCms.GUI.Icons.Icons.Application_View_List, "Elenco Provider"));
        }

        public NhRequestObject<Provider> Provider { get; private set; }

        public ProviderDetail(StateBag viewstate, bool IsPostBack, NetCms.GUI.Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewstate, IsPostBack, toolbar, informationBox, networkKey)
        {
            this.StatusValidators.Add(new PagineConfigurazioneValidator(this.Application));

            Provider = new NhRequestObject<Provider>(BusinessLogic.GetCurrentSession(), "provider");
            this.StatusValidators.Add(new NhRequestObjectValidator<Provider>(Provider));
            SetToolbars();
        }

        public override WebControl BuildControl()
        {
            G2Core.UI.DetailsSheet sheet = new G2Core.UI.DetailsSheet("Dettaglio Provider", "Informazioni relative al provider " + Provider.Instance.Nome);
            sheet.AddRow(NomeRow, G2Core.UI.DetailsSheet.Colors.Default);
            sheet.AddRow(CanaleRow, G2Core.UI.DetailsSheet.Colors.Default);
            sheet.AddRow(NumeroSpedizioniRow, G2Core.UI.DetailsSheet.Colors.Default);
            sheet.AddRow(NetworkAbilitateRow, G2Core.UI.DetailsSheet.Colors.Default);
            if (Provider.Instance.Canale == BusinessLogic.GetCanaleSMS())
            {
                sheet.AddRow(SmsResiduiRow, G2Core.UI.DetailsSheet.Colors.Default);
            }

            return sheet;
        }

        private TableRow NomeRow
        {
            get
            {
                TableRow row = new TableRow();
                TableCell cell = new TableCell();
                cell.Text = "Nome Provider";
                row.Cells.Add(cell);
                cell = new TableCell();
                cell.Text = Provider.Instance.Nome;
                row.Cells.Add(cell);
                return row;
            }
        }

        private TableRow CanaleRow
        {
            get
            {
                TableRow row = new TableRow();
                TableCell cell = new TableCell();
                cell.Text = "Canale";
                row.Cells.Add(cell);
                cell = new TableCell();
                cell.Text = Provider.Instance.Canale.Nome;
                row.Cells.Add(cell);
                return row;
            }
        }

        private TableRow NumeroSpedizioniRow
        {
            get
            {
                TableRow row = new TableRow();
                TableCell cell = new TableCell();
                cell.Text = "Numero Spedizioni";
                row.Cells.Add(cell);
                cell = new TableCell();
                cell.Text = Provider.Instance.Spedizioni.Count.ToString();
                row.Cells.Add(cell);
                return row;
            }
        }

        private TableRow NetworkAbilitateRow
        {
            get
            {
                TableRow row = new TableRow();
                TableCell cell = new TableCell();
                cell.Text = "Network Abilitate";
                row.Cells.Add(cell);
                cell = new TableCell();
                foreach (ProviderNetwork pn in Provider.Instance.ProviderNetworks)
                {
                    cell.Text += NetCms.Networks.NetworksManager.GlobalIndexer.GetNetworkByID(pn.IdNetwork).Label + "\n";
                }
                row.Cells.Add(cell);
                return row;
            }
        }

        private TableRow SmsResiduiRow
        {
            get
            {
                TableRow row = new TableRow();
                TableCell cell = new TableCell();
                cell.Text = "Sms Residui";
                row.Cells.Add(cell);
                cell = new TableCell();
                cell.Text = (Provider.Instance as ProviderSms).SmsResidui.ToString();
                row.Cells.Add(cell);
                return row;
            }
        }

    }
}
