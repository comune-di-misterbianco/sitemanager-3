﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;

namespace Messaggistica.View.Form
{
    public class SimpleMailForm : VfManager
    {
        /// <summary>
        /// TextBox per l'input di un singolo indirizzo mail
        /// </summary>
        public VfTextBox SingleDestinationAddress
        {
            get
            {
                if (_SingleDestinationAddress == null)
                {
                    _SingleDestinationAddress = new VfTextBox("to", "Destinatario", InputTypes.Email);
                    _SingleDestinationAddress.Required = true;

                    if (this.IsPostBack)
                        _SingleDestinationAddress.DefaultValue = SingleDestinationAddress.PostBackValue;

                }
                return _SingleDestinationAddress;
            }
        }
        private VfTextBox _SingleDestinationAddress;

        /// <summary>
        /// Oggetto della mail
        /// </summary>
        public VfTextBox Subject
        {
            get
            {
                if (_Subject == null)
                {
                    _Subject = new VfTextBox("subject", "Oggetto", InputTypes.Text);
                    _Subject.Required = true;
                    _Subject.TextBox.Width = 500;

                    if (this.IsPostBack)
                        _Subject.DefaultValue = Subject.PostBackValue;

                }
                return _Subject;
            }
        }
        private VfTextBox _Subject;

        /// <summary>
        /// Corpo della mail
        /// </summary>
        public VfTextBox Body
        {
            get
            {
                if (_Body == null)
                {
                    _Body = new VfTextBox("body", "Testo", InputTypes.Text);
                    _Body.Required = true;
                    _Body.TextBox.Rows = 6;
                    _Body.TextBox.Columns = 40;
                    _Body.TextMode = System.Web.UI.WebControls.TextBoxMode.MultiLine;
                }
                return _Body;
            }
        }
        private VfTextBox _Body;




        /// <summary>
        /// Il form non contiene inizialmente alcun field, si possono inserire quelli presenti o aggiungerne altri se necessario
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isPostBack"></param>
        public SimpleMailForm(string id, bool isPostBack)
            : base(id, isPostBack)
        {
        }
    }
}
