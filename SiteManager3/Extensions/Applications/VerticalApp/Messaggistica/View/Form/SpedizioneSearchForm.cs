﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

using Messaggistica.Model;
using NetService.Utility.ValidatedFields;
using NetService.Utility.RecordsFinder;

namespace Messaggistica.View
{
    public class SpedizioneSearchForm : VfManager
    {
        public SpedizioneSearchForm(string controlId)
            :base(controlId)
        {
            this.Fields.Add(CanaleField);
        }

        public VfDropDown CanaleField
        {
            get
            {
                if (_CanaleField == null)
                {
                    _CanaleField = new VfDropDown("Nome", "Tipo canale", "Nome");
                    _CanaleField.BindField = false;
                    _CanaleField.Required = true;
                   // _CanaleField.DropDownList.AutoPostBack = true;
                    _CanaleField.Options.Add("", "");
                    foreach (Canali canale in BusinessLogic.FindCanali())
                    {
                        _CanaleField.Options.Add(canale.Nome, canale.Nome);
                    }
                }
                return _CanaleField;
            }
        }
        private VfDropDown _CanaleField;

        public List<SearchParameter> GetSearchParameter()
        {
            List<SearchParameter> searchparameters = new List<SearchParameter>();

            
            SearchParameter canaleSP = new SearchParameter(null, "Canale", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { CanaleField.SearchParameter });           

            SearchParameter providerSP = new SearchParameter(null, "Provider", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { canaleSP });
            searchparameters.Add(providerSP);

            return searchparameters;
        }

    }
}
