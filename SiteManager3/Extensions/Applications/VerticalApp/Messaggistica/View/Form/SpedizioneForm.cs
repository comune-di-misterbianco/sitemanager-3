﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using G2Core.Common;
using NetService.Utility.ValidatedFields;
using UsersAggregator.Model;
using NetService.Utility.ArTable;

namespace Messaggistica.View
{
    public class SpedizioneForm : WebControl
    {
        public CanaleProviderForm CanaleProviderForm
        {
            get
            {
                if (_CanaleProviderForm == null)
                {
                    _CanaleProviderForm = new CanaleProviderForm(this.ID + "_canale_provider") { AddSubmitButton = false };
                    _CanaleProviderForm.IsPostBack = FormIsPostBack;
                }
                return _CanaleProviderForm;
            }
        }
        private CanaleProviderForm _CanaleProviderForm;

        public VfRadioButton SceltaInvio
        {
            get
            {
                if (_SceltaInvio == null)
                {
                    _SceltaInvio = new VfRadioButton("SceltaInvio", "Scegli se effettuare un invio singolo o se inviare ad una lista", false);
                    _SceltaInvio.addItem("Singolo", "Singolo", false);
                    _SceltaInvio.addItem("Da Lista Utenti", "Lista", false);
                    _SceltaInvio.RadioButtonList.AutoPostBack = true;

                }
                return _SceltaInvio;
            }
        }
        private VfRadioButton _SceltaInvio;

        public VfTextBox InvioSingolo
        {
            get
            {
                if (_InvioSingolo == null)
                {
                    if (_CanaleProviderForm.CanaleField.PostBackValue.ToLower() == "sms")
                    {
                        _InvioSingolo = new VfTextBox("InvioSingolo", "Cellulare");
                        _InvioSingolo.InputType = InputTypes.Phone;
                    }
                    else
                    {
                        _InvioSingolo = new VfTextBox("InvioSingolo", "Email");
                        _InvioSingolo.InputType = InputTypes.Email;
                    } 
                    _InvioSingolo.BindField = false;
                    _InvioSingolo.Required = true;
                }
                return _InvioSingolo;
            }
        }
        private VfTextBox _InvioSingolo;

        public VfDropDown InvioListe
        {
            get
            {
                if (_InvioListe == null)
                {
                    _InvioListe = new VfDropDown("InvioListe", "Invio a Lista");
                    _InvioListe.BindField = false;
                    _InvioListe.Required = true;
                    _InvioListe.DropDownList.AutoPostBack = true;
                    _InvioListe.Options.Add("", "");
                    foreach (Liste lista in BusinessLogic.FindListe())
                    {
                        _InvioListe.Options.Add(lista.Nome, lista.Nome);
                    }
                    _InvioListe.InfoControl.Controls.Add(new LiteralControl("Verranno selezionati solo gli utenti per i quali esiste il cellulare o l'email"));
                }
                return _InvioListe;
            }
        }
        private VfDropDown _InvioListe;

        public ArTable UtentiInLista
        {
            get 
            {
                if (_UtentiInLista == null)
                {
                    if (_InvioListe != null && !string.IsNullOrEmpty(_InvioListe.PostBackValue))
                    {
                        Liste lista = BusinessLogic.GetListaByName(_InvioListe.PostBackValue);
                        _UtentiInLista = new ArTable(lista.Destinatari);
                        _UtentiInLista.InnerTableCssClass = "tab";
                        _UtentiInLista.AddColumn(new ArTextColumn("Nome Completo", "NomeCompleto"));
                        _UtentiInLista.AddColumn(new ArTextColumn("Email", "Email"));
                        _UtentiInLista.AddColumn(new ArTextColumn("Cellulare", "Cellulare"));
                    }
                }
                return _UtentiInLista;
            }
        }
        private ArTable _UtentiInLista;

        public Button SubmitButton
        {
            get
            {
                if (_SubmitButton == null)
                {
                    _SubmitButton = new Button();
                    _SubmitButton.ID = this.ID + "_submit";
                    _SubmitButton.Text = "Invia Messaggio";
                }
                return _SubmitButton;
            }
        }
        private Button _SubmitButton;

        public bool FormIsPostBack { get; private set; }

        public SpedizioneForm(string controlID)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = controlID;
            RequestVariable buttonPostback = new RequestVariable(SubmitButton.ID);
            FormIsPostBack = (buttonPostback.IsPostBack && buttonPostback.StringValue == SubmitButton.Text);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            fieldset.CssClass = "messaggistica_fieldset";
            fieldset.Controls.Add(legend);
            fieldset.Controls.Add(CanaleProviderForm);
            legend.Controls.Add(new LiteralControl("Scelta Canale e Provider"));
            this.Controls.Add(fieldset);

            fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
            legend = new WebControl(HtmlTextWriterTag.Legend);
            fieldset.CssClass = "messaggistica_fieldset";
            fieldset.Controls.Add(legend);
            fieldset.Controls.Add(SceltaInvio);
            legend.Controls.Add(new LiteralControl("Scelta Modalità d'Invio"));
            this.Controls.Add(fieldset);

            if (!string.IsNullOrEmpty(SceltaInvio.PostBackValue))
            {
                fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
                legend = new WebControl(HtmlTextWriterTag.Legend);
                fieldset.CssClass = "messaggistica_fieldset";
                fieldset.Controls.Add(legend);

                if (SceltaInvio.PostBackValue == "Singolo")
                    fieldset.Controls.Add(InvioSingolo);
                else
                {
                    fieldset.Controls.Add(InvioListe);
                    if (UtentiInLista != null)
                        fieldset.Controls.Add(UtentiInLista);
                } 
                legend.Controls.Add(new LiteralControl("Scelta Tipologia Invio"));
                this.Controls.Add(fieldset);

                fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
                legend = new WebControl(HtmlTextWriterTag.Legend);
                fieldset.CssClass = "messaggistica_fieldset";
                fieldset.Controls.Add(legend);
                fieldset.Controls.Add(SubmitButton);
                legend.Controls.Add(new LiteralControl("Invio messaggio"));
                this.Controls.Add(fieldset);
            }
        }
    }
}