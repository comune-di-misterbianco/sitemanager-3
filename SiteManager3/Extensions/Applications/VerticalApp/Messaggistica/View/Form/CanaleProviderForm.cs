﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using Messaggistica.Model;

namespace Messaggistica.View
{
    public class CanaleProviderForm : VfManager
    {
        public CanaleProviderForm(string controlId)
            : base(controlId)
        {
            this.Fields.Add(CanaleField);
            this.Fields.Add(ProviderField);
            if (!string.IsNullOrEmpty(ProviderField.PostBackValue))
            {
                Provider provider = BusinessLogic.GetProviderByName(ProviderField.PostBackValue);
                //this.Fields.AddRange(provider.VfFields);
                Fields.AddRange(provider.FormFields);
            }
        }

        public VfDropDown CanaleField
        {
            get
            {
                if (_CanaleField == null)
                {
                    _CanaleField = new VfDropDown("Canale", "Canale");
                    _CanaleField.BindField = false;
                    _CanaleField.Required = true;
                    _CanaleField.DropDownList.AutoPostBack = true;
                    _CanaleField.Options.Add("", "");
                    foreach (Canali canale in BusinessLogic.FindCanali())
                    {
                        _CanaleField.Options.Add(canale.Nome, canale.Nome);
                    }
                }
                return _CanaleField;
            }
        }
        private VfDropDown _CanaleField;

        public VfDropDown ProviderField
        {
            get
            {
                if (_ProviderField == null)
                {
                    _ProviderField = new VfDropDown("Provider", "Provider");
                    _ProviderField.BindField = false;
                    _ProviderField.Required = true;
                    _ProviderField.DropDownList.AutoPostBack = true;
                    _ProviderField.Options.Add("", "");
                    if (!string.IsNullOrEmpty(CanaleField.PostBackValue))
                    {
                        Canali canale = BusinessLogic.GetCanaleByName(CanaleField.PostBackValue);
                        if (canale != null)
                        {
                            foreach (Provider provider in BusinessLogic.FindProviderByCanale(canale).Where(x=>x.ProviderNetworks.Count==0))
                            {
                                _ProviderField.Options.Add(provider.Nome, provider.Nome);
                            }
                        }
                    }
                    
                }
                return _ProviderField;
            }
        }
        private VfDropDown _ProviderField;
    }
}
