﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using Messaggistica.Model;

namespace Messaggistica.View
{
    public class ProviderForm : VfManager
    {
        public ProviderForm(string controlId, Provider provider = null)
            : base(controlId)
        {
            this.Fields.Add(NomeField);
            this.Fields.Add(IsDefault);
            this.Fields.Add(NetworksField);

            Provider.Type tipo = Provider.Type.Email;
            ProviderSms.SmsProviderType smsProviderType = ProviderSms.SmsProviderType.None;
            // insert
            if (provider == null)
            {

                this.Fields.Add(TipoProviderField);
                if (!string.IsNullOrEmpty(TipoProviderField.PostBackValue))                
                    tipo = (Provider.Type)Enum.Parse(typeof(Provider.Type), TipoProviderField.PostBackValue, true);
                if (tipo == Provider.Type.Sms)
                {
                    this.Fields.Add(SmsTipoProviderField);
                    if (SmsTipoProviderField.PostBackValue != null && !string.IsNullOrEmpty(SmsTipoProviderField.PostBackValue))
                    smsProviderType = (ProviderSms.SmsProviderType)Enum.Parse(typeof(ProviderSms.SmsProviderType), SmsTipoProviderField.PostBackValue, true);
                }
            }
            // update
            else
            {
                tipo = provider.Tipo;

                if (tipo == Provider.Type.Sms)
                    smsProviderType = (provider as ProviderSms).SmsType;                    
            }

            switch (tipo)
            {                
                case Provider.Type.Sms:
                    {
                        //if (!string.IsNullOrEmpty(smsProviderType))
                        //{
                            //smsProviderType = (ProviderSms.SmsProviderType)Enum.Parse(typeof(ProviderSms.SmsProviderType), SmsTipoProviderField.PostBackValue, true);
                            switch (smsProviderType)
                            {
                                case ProviderSms.SmsProviderType.Aruba:
                                    provider = new ProviderAruba();
                                    this.Fields.AddRange(provider.VfFields);
                                    break;
                                case ProviderSms.SmsProviderType.SMSHosting:
                                    provider = new ProviderSMSHosting();
                                    this.Fields.AddRange(provider.VfFields);
                                    break;
                                case ProviderSms.SmsProviderType.Clickatell:
                                    provider = new ProviderClickatell();
                                    this.Fields.AddRange(provider.VfFields);
                                    break;                                
                            }
                        //}
                        break;
                    }
                case Provider.Type.Email:
                    provider = new ProviderEmail();
                    this.Fields.AddRange(provider.VfFields); //this.Fields.AddRange(ProviderEmail.CampiAssociati);
                    break;
            }            
        }

        public VfDropDown SmsTipoProviderField
        {
            get
            {
                if (_SmsTipoProviderField == null)
                {
                    _SmsTipoProviderField = new VfDropDown("SmsTipoProvider", "SmsTipoProvider");
                    _SmsTipoProviderField.Required = true;
                    _SmsTipoProviderField.BindField = false;
                    _SmsTipoProviderField.DropDownList.AutoPostBack = true;
                    _SmsTipoProviderField.Options.Add("", "");
                    foreach (ProviderSms.SmsProviderType smstype in Enum.GetValues(typeof(ProviderSms.SmsProviderType)))
                    {
                        _SmsTipoProviderField.Options.Add(smstype.ToString(), smstype.ToString());
                    }
                }
                return _SmsTipoProviderField;
            }
        }
        private VfDropDown _SmsTipoProviderField;

        public VfDropDown TipoProviderField
        {
            get
            {
                if (_TipoProviderField == null)
                {
                    _TipoProviderField = new VfDropDown("TipoProvider", "TipoProvider");
                    _TipoProviderField.Required = true;
                    _TipoProviderField.BindField = false;
                    _TipoProviderField.DropDownList.AutoPostBack = true;
                    _TipoProviderField.Options.Add("", "");
                    foreach (Provider.Type tipo in Enum.GetValues(typeof(Provider.Type)))
                    {
                        _TipoProviderField.Options.Add(tipo.ToString(), tipo.ToString());
                    }
                }
                return _TipoProviderField;
            }
        }
        private VfDropDown _TipoProviderField;


        public VfTextBox NomeField
        {
            get
            {
                if (_NomeField == null)
                {
                    _NomeField = new VfTextBox("Nome", "Nome", "Nome", InputTypes.Text);
                    _NomeField.Required = true;
                }
                return _NomeField;
            }
        }
        private VfTextBox _NomeField;

        public VfCheckBoxList NetworksField
        {
            get
            {
                if (_NetworksField == null)
                {
                    _NetworksField = new VfCheckBoxList("ProviderNetworks", "ProviderNetworks", false);
                    _NetworksField.AtLeastOneRequired = false;
                    _NetworksField.BindField = false;
                    foreach (var network in NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks)
                    {
                        _NetworksField.addItem(network.Value.Label, network.Value.ID.ToString(), false);
                    }
                }
                return _NetworksField;
            }
        }
        private VfCheckBoxList _NetworksField;

        public VfCheckBox IsDefault
        {
            get
            {
                if (_IsDefault == null)
                    _IsDefault = new VfCheckBox(ProviderSms.Column.IsDefault.ToString(), "Imposta come predefinito");
                return _IsDefault;
            }
        }
        private VfCheckBox _IsDefault;

    }
}
