﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;

namespace Messaggistica.View
{
    [DynamicUrl.PageControl("/default.aspx")]
    public class MainPage : MessaggisticaPage
    {
        public override string PageTitle
        {
            get { return "Messaggistica"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Email; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("spedizioni_new.aspx", NetCms.GUI.Icons.Icons.Email_Add, "Nuova Spedizione"));
            this.Toolbar.Buttons.Add(new ToolbarButton("spedizioni_list.aspx", NetCms.GUI.Icons.Icons.Table, "Archivio Spedizioni"));
            if (this.Application.GrantsManager["netservice"].Allowed)
                this.Toolbar.Buttons.Add(new ToolbarButton("provider_list.aspx", NetCms.GUI.Icons.Icons.Page_White_Gear, "Gestione Provider"));
        }

        public MainPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            SetToolbars();
        }

        public override WebControl BuildControl()
        {
            WebControl webcontrol = new WebControl(HtmlTextWriterTag.Div);
            webcontrol.ID = Application.SystemName;

            WebControl control = new WebControl(HtmlTextWriterTag.Div);
            control.CssClass = "vertical_application_toolbar";

            LiteralControl benvenuto = new LiteralControl("<h1>Messaggistica</h1>");
            control.Controls.Add(benvenuto);

            string htmlCode = "";
            htmlCode += "<ul>";
            htmlCode += "<li class=\"new_sped\"><a href=\"spedizioni_new.aspx\"><img src=\"" + NetCms.Configurations.Paths.AbsoluteRoot + "/cms/css/admin/" + Application.SystemName + "/images/new_spedizione.png\" /><span>Nuova spedizione</span></a></li>";
            htmlCode += "<li class=\"new_sped\"><a href=\"spedizioni_list.aspx\"><img src=\"" + NetCms.Configurations.Paths.AbsoluteRoot + "/cms/css/admin/" + Application.SystemName + "/images/archivio_spedizioni.png\" /><span>Archivio spedizioni</span></a></li>";
            if (this.Application.GrantsManager["netservice"].Allowed)
                htmlCode += "<li class=\"new_sped\"><a href=\"provider_list.aspx\"><img src=\"" + NetCms.Configurations.Paths.AbsoluteRoot + "/cms/css/admin/" + Application.SystemName + "/images/provider.png\" /><span>Gestione Provider</span></a></li>";
            htmlCode += "</ul>";
            htmlCode += "<div class=\"clr\"></div>";

            control.Controls.Add(new LiteralControl(htmlCode));

            webcontrol.Controls.Add(control);

            return webcontrol;
        }
    }
}
