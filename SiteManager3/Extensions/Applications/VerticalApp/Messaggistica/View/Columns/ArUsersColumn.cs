﻿using System.Web.UI.WebControls;
using System;
using NetCms.Users;
using System.Reflection;

namespace NetService.Utility.ArTable
{
    public class ArUsersColumn : ArTextColumn
    {
        private bool _ShowProfileLink;

        public ArUsersColumn(string caption, string key, bool showProfileLink = false)
            : base(caption, key)
        {
            _ShowProfileLink = showProfileLink;
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            int userId = 0;
            try
            {
                userId = (int)GetValue(objectInstance, Key);
            }
            catch { } 

            TableCell td = new TableCell();
            if (!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;

            string value = "Nome utente non disponibile";
            string celText = "";
            try
            {
                if (userId > 0)
                {
                    User user = UsersBusinessLogic.GetById(userId);
                    if (user != null)
                    {
                        value = user.NomeCompleto;
                        if (_ShowProfileLink)
                            celText = "<a href=\"" + user.AdminDetailLink + "\"><span>" + value + "</span></a>";
                        else
                            celText = "<span>" + value + "</span>";
                    }
                }
            }
            catch { }

            td.Text = celText;
            return td;
        }

        public override string GetExportValue(object obj)
        {
            int userId = (int)GetValue(obj, Key);

            //User user = NetCms.Users.User.Fabricate(userId.ToString());

            User user = UsersBusinessLogic.GetById(userId);

            string value = "Nome utente non disponibile";

            try
            {
                value = user.NomeCompleto;
            }
            catch { }
            return value;
        }
    }
}
