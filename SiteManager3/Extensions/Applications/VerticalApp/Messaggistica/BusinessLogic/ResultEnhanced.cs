﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Messaggistica.Model;

namespace Messaggistica
{
    public class ResultEnhanced
    {
        public Spedizioni Spedizione
        { get; set; }

        public int RecordId
        { get; set; }

        public Result Result
        { get; set; }

        public ResultEnhanced(Result result)
        {
            Result = result;
        }
    }
}
