﻿using NHibernate;
using Messaggistica.Model;
using System.Collections.Generic;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using UsersAggregator.Model;
using System.Reflection;
using System.Linq;
using System;
using Messaggistica.View;
using NetCms.DBSessionProvider;

namespace Messaggistica
{
    public static class BusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static IList<Canali> FindCanali()
        {
            IGenericDAO<Canali> dao = new CriteriaNhibernateDAO<Canali>(GetCurrentSession());
            return dao.FindAll();
        }

        public static Canali GetCanaleSMS(ISession session = null)
        {
            return GetCanaleByName("sms",session);
        }

        public static Canali GetCanaleEmail(ISession session = null)
        {
            return GetCanaleByName("email",session);
        }

        public static Canali GetCanaleByName(string nome, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<Canali> dao = new CriteriaNhibernateDAO<Canali>(session);
            SearchParameter nomeParam = new SearchParameter(null, "Nome", nome, Finder.ComparisonCriteria.Equals, false);
            return dao.GetByCriteria(new SearchParameter[] { nomeParam });
        }

        public static int CountProvider(SearchParameter[] parameters)
        {
            IGenericDAO<Provider> dao = new CriteriaNhibernateDAO<Provider>(GetCurrentSession());
            return dao.FindByCriteriaCount(parameters);
        }

        public static Provider GetProvider(int id, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();
            IGenericDAO<Provider> dao = new CriteriaNhibernateDAO<Provider>(session);
            return dao.GetById(id);
        }

        public static Provider GetProviderByName(string nome, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<Provider> dao = new CriteriaNhibernateDAO<Provider>(session);
            SearchParameter nomeParam = new SearchParameter(null, "Nome", nome, Finder.ComparisonCriteria.Equals, false);
            return dao.GetByCriteria(new SearchParameter[] { nomeParam });
        }

        public static Provider GetDefaultProvider(Canali canale, ISession session = null)
        {
            IGenericDAO<Provider> dao = new CriteriaNhibernateDAO<Provider>(GetCurrentSession());
            SearchParameter canaleParam = new SearchParameter(null, Provider.Column.Canale.ToString(), canale, Finder.ComparisonCriteria.Equals, false);
            SearchParameter defaultProvider = new SearchParameter(null, Provider.Column.IsDefault.ToString(), true, Finder.ComparisonCriteria.Equals, false);
            return dao.GetByCriteria(new SearchParameter[] { defaultProvider, canaleParam });
        }

        public static IList<Provider> FindProviderByCanale(Canali canale, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<Provider> dao = new CriteriaNhibernateDAO<Provider>(session);
            SearchParameter canaleParam = new SearchParameter(null, "Canale", canale, Finder.ComparisonCriteria.Equals, false);
            return dao.FindByCriteria(new SearchParameter[] { canaleParam });
        }

        public static IList<Provider> FindProvider(int pageStart, int pageSize, string sortBy, bool descending, SearchParameter[] searchParameters)
        {
            IGenericDAO<Provider> dao = new CriteriaNhibernateDAO<Provider>(GetCurrentSession());
            return dao.FindByCriteria(((pageStart - 1) * pageSize),
                                            pageSize,
                                            sortBy,
                                            descending,
                                            searchParameters);
        }

        public static bool ProviderPresenteInUpdate(Provider provider, string nome)
        {
            IGenericDAO<Provider> dao = new CriteriaNhibernateDAO<Provider>(GetCurrentSession());
            SearchParameter canaleParam = new SearchParameter(null, "Canale", provider.Canale, Finder.ComparisonCriteria.Equals, false);
            SearchParameter nomeParam = new SearchParameter(null, "Nome", nome, Finder.ComparisonCriteria.Equals, false);
            SearchParameter idParam = new SearchParameter(null, "Id", provider.Id, Finder.ComparisonCriteria.Not, false);

            return dao.GetByCriteria(new SearchParameter[] { canaleParam, nomeParam, idParam }) != null;
        }

        public static void DeleteProvider(Provider provider)
        {
            IGenericDAO<Provider> dao = new CriteriaNhibernateDAO<Provider>(GetCurrentSession());
            dao.Delete(provider);
        }

        public static Provider CreateProvider(ProviderForm form)
        {
            Provider provider = null;

            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {

                try
                {

            string tipoProvider = form.Fields.First(x => x.Key == "TipoProvider").PostBackValue;
                    string tipoSmsProvider = (form.Fields.Find(x => x.Key == "SmsTipoProvider") != null) ? form.Fields.Find(x => x.Key == "SmsTipoProvider").PostBackValue : "";
                    string isDefault = form.Fields.Find(x => x.Key == Provider.Column.IsDefault.ToString()).PostBackValue;
                    
                    Provider.Type tipo = (Provider.Type)Enum.Parse(typeof(Provider.Type), tipoProvider, true);
            switch (tipo)
            {
                        #region oldcode
                        //case Provider.Type.Clickatell:
                        //    {
                        //        provider = new ProviderClickatell();
                        //        provider.Canale = GetCanaleSMS(session);
                        //        break;
                        //    }
                        //case Provider.Type.Aruba:
                        //    {
                        //        provider = new ProviderAruba();
                        //        provider.Canale = GetCanaleSMS(session);
                        //        break;
                        //    }
                        //case Provider.Type.SMSHosting:
                        //    {
                        //        provider = new ProviderSMSHosting();
                        //        provider.Canale = GetCanaleSMS(session);
                        //        break;
                        //    } 
                        #endregion

                        case Provider.Type.Sms:
                    {

                                ProviderSms.SmsProviderType smsProviderType = (ProviderSms.SmsProviderType)Enum.Parse(typeof(ProviderSms.SmsProviderType), tipoSmsProvider, true);
                                switch (smsProviderType)
                                {
                                    case ProviderSms.SmsProviderType.Aruba:
                                        provider = new ProviderAruba();                                       
                                        break;
                                    case ProviderSms.SmsProviderType.SMSHosting:
                                        provider = new ProviderSMSHosting();                                       
                                        break;
                                    case ProviderSms.SmsProviderType.Clickatell:
                        provider = new ProviderClickatell();
                        break;
                    }
                                provider.Canale = GetCanaleSMS(session);
                                break;
                            }
                        case Provider.Type.Email:
                    {
                        provider = new ProviderEmail();
                                provider.Canale = GetCanaleEmail(session);
                        break;
                    }
            }

                    if (isDefault == "on")
                        BusinessLogic.ToggleDefaultProvider(provider.Canale, session);

                    bool filled = form.FillObjectAdvanced(provider, session);
            if (filled)
            {
                foreach (string idNetwork in form.Fields.First(x => x.Key == "ProviderNetworks").PostbackValueObject as List<string>)
                {
                    ProviderNetwork pn = new ProviderNetwork();
                    pn.Provider = provider;
                    pn.IdNetwork = int.Parse(idNetwork);
                    provider.ProviderNetworks.Add(pn);
                }
                        IGenericDAO<Provider> dao = new CriteriaNhibernateDAO<Provider>(session);
                        dao.SaveOrUpdate(provider);
                        tx.Commit();
            }
                    //else return null;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante l'inserimento del record " + typeof(Provider) + ". " + ex.Message);
                }
            }

            return provider;
        }

        public static Provider ModProvider(ProviderForm form, Provider provider)
        {
            Provider currentProvider = null;

            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {

                try
                {
                    currentProvider = BusinessLogic.GetProvider(provider.Id, session);

                    string isDefault = form.Fields.Find(x => x.Key == Provider.Column.IsDefault.ToString()).PostBackValue;

                    if (isDefault == "on")
                        BusinessLogic.ToggleDefaultProvider(currentProvider.Canale, session);                                       

                    bool filled = form.FillObjectAdvanced(currentProvider, session);

            if (filled)
            {
                        currentProvider.ProviderNetworks.Clear();
                foreach (string idNetwork in form.Fields.First(x => x.Key == "ProviderNetworks").PostbackValueObject as List<string>)
                {
                    ProviderNetwork pn = new ProviderNetwork();
                            pn.Provider = currentProvider;
                    pn.IdNetwork = int.Parse(idNetwork);
                            currentProvider.ProviderNetworks.Add(pn);
                        }

                        

                        IGenericDAO<Provider> dao = new CriteriaNhibernateDAO<Provider>(session);
                        dao.SaveOrUpdate(currentProvider);                      

                        tx.Commit();
                    }
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante l'aggiornamento del record " + typeof(Provider) + ". " + ex.Message);
                }
            }
            return currentProvider;
                }

        public static void ToggleDefaultProvider(Canali canale, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Provider> dao = new CriteriaNhibernateDAO<Provider>(innerSession);

            IList<Provider> providers = FindProviderByCanale(canale, innerSession);

            foreach (Provider provider in providers)
            {
                provider.IsDefault = false;
                dao.SaveOrUpdate(provider);
            }

        }

        public static IList<Liste> FindListe()
        {
            IGenericDAO<Liste> dao = new CriteriaNhibernateDAO<Liste>(GetCurrentSession());
            return dao.FindAll();
        }

        public static Liste GetListaByName(string nome)
        {
            IGenericDAO<Liste> dao = new CriteriaNhibernateDAO<Liste>(GetCurrentSession());
            SearchParameter nomeParam = new SearchParameter(null, "Nome", nome, Finder.ComparisonCriteria.Equals, false);
            return dao.GetByCriteria(new SearchParameter[] { nomeParam });
        }

        public static IList<UtentiRubrica> FindUtentiByLista(Liste lista)
        {
            IGenericDAO<UtentiRubrica> dao = new CriteriaNhibernateDAO<UtentiRubrica>(GetCurrentSession());
            SearchParameter listaSP = new SearchParameter(null, "Lista", lista, Finder.ComparisonCriteria.Equals, false);
            return dao.FindByCriteria(new SearchParameter[] { listaSP });
        }

        public static int CountSpedizioni(SearchParameter[] parameters)
        {
            IGenericDAO<Spedizioni> dao = new CriteriaNhibernateDAO<Spedizioni>(GetCurrentSession());
            return dao.FindByCriteriaCount(parameters);
        }

        public static IList<Spedizioni> FindSpedizioni(int pageStart, int pageSize, IDictionary<string, bool> sortBy, SearchParameter[] searchParameters)
        {
            IGenericDAO<Spedizioni> dao = new CriteriaNhibernateDAO<Spedizioni>(GetCurrentSession());
            return dao.FindByCriteria(((pageStart - 1) * pageSize),
                                        pageSize,
                                        sortBy,                                          
                                        searchParameters);
        }

        public static Spedizioni GetSpedizioneById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Spedizioni> dao = new CriteriaNhibernateDAO<Spedizioni>(innerSession);
            return dao.GetById(id);
        }

        // questa è la versione utilizzata internamente da messaggistica
        public static Result SendMessage(CanaleProviderForm canaleProviderForm, IList<string> destinatari, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            Result result = null;
            IGenericDAO<Spedizioni> dao = new CriteriaNhibernateDAO<Spedizioni>(innerSession);
            Spedizioni spedizione = null;

            try
            {
                Canali canale = GetCanaleByName(canaleProviderForm.CanaleField.PostBackValue,innerSession);
                Provider provider = GetProviderByName(canaleProviderForm.ProviderField.PostBackValue,innerSession);
                string testo = canaleProviderForm.Fields.Find(x => x.Key == "Testo").PostBackValue;

                Dictionary<string, object> campiAggiuntiviProvider = new Dictionary<string, object>();
                foreach (string labelCampo in provider.LabelsCampiAggiuntiviFormInserimento)
                {
                    campiAggiuntiviProvider.Add(labelCampo, canaleProviderForm.Fields.Find(x => x.Key == labelCampo).PostbackValueObject);
                }

                Type spedizioneDaIstanziare = GetSpedizioneType(canale, provider);

                object istanzaSpedizione = Activator.CreateInstance(spedizioneDaIstanziare);

                spedizione = GetSpedizione(provider, testo, destinatari, spedizione, campiAggiuntiviProvider, spedizioneDaIstanziare, istanzaSpedizione, innerSession);

                result = spedizione.Send(innerSession);
            }
            catch (Exception ex)
            {
                result = ManageError(result, dao, spedizione, ex);
            }
            return result;
        }

        // questa è la versione che consente l'utilizzo da parte di librerie client esterne
        public static Result SendMessage(Canali canale, string testo, IList<string> destinatari, Dictionary<string, object> campiAggiuntiviProvider, int networkId, ISession innerSession = null)
        {

            if (innerSession == null)
                innerSession = GetCurrentSession();

            Result result = null;
            IGenericDAO<Spedizioni> dao = new CriteriaNhibernateDAO<Spedizioni>(innerSession);
            Spedizioni spedizione = null;

            try
            {
                result = canale.HasProviderByNetwork(networkId);
                if (result.IsOk)
                {
                    Provider provider = canale.Providers.Where(p => p.ProviderNetworks.Where(pn => pn.IdNetwork == networkId).Count() > 0).First();

                    Type spedizioneDaIstanziare = GetSpedizioneType(canale, provider);

                    object istanzaSpedizione = Activator.CreateInstance(spedizioneDaIstanziare);

                    spedizione = GetSpedizione(provider, testo, destinatari, spedizione, campiAggiuntiviProvider, spedizioneDaIstanziare, istanzaSpedizione,innerSession);

                    result = spedizione.Send(networkId, innerSession);
                }
                else
                    return result;

            }
            catch (Exception ex)
            {
                result = ManageError(result, dao, spedizione, ex);
            }
            return result;
        }

        // questa è la versione che consente l'utilizzo da parte di librerie client esterne che utilizza i provider generici
        public static ResultEnhanced SendMessageWithGenericProviders(Canali canale, string testo, IList<string> destinatari, Dictionary<string, object> campiAggiuntiviProvider, ISession innerSession = null)
        {

            if (innerSession == null)
                innerSession = GetCurrentSession();

            ResultEnhanced resultToReturn = null;
            Result result = null;
            IGenericDAO<Spedizioni> dao = new CriteriaNhibernateDAO<Spedizioni>(innerSession);
            Spedizioni spedizione = null;

            try
            {
                //Provider provider = canale.Providers.Where(p => p.ProviderNetworks.Count == 0).First();
                Provider provider = canale.Providers.Where(x => x.IsDefault == true).FirstOrDefault();
                if (provider != null)
                {
                Type spedizioneDaIstanziare = GetSpedizioneType(canale, provider);

                object istanzaSpedizione = Activator.CreateInstance(spedizioneDaIstanziare);

                spedizione = GetSpedizione(provider, testo, destinatari, spedizione, campiAggiuntiviProvider, spedizioneDaIstanziare, istanzaSpedizione, innerSession);
                if (spedizione != null)
                {
                        result = spedizione.Send(innerSession);
                        resultToReturn = new ResultEnhanced(result);
                        resultToReturn.RecordId = spedizione.Id;
                        resultToReturn.Spedizione = spedizione;
                }
                else
                {
                    result = ManageError(result, dao,null,null);
                    resultToReturn = new ResultEnhanced(result);
                }
            }
                else
                {
                    throw new Exception("Non è stato trovato nessun provider predefinito.");
                }
            }
            catch (Exception ex)
            {
                result = ManageError(result, dao, spedizione, ex);
                resultToReturn = new ResultEnhanced(result);
            }
            return resultToReturn;
        }


        private static Result ManageError(Result result, IGenericDAO<Spedizioni> dao, Spedizioni spedizione, Exception ex)
        {
            if (spedizione != null)
                dao.Delete(spedizione);

            if (result == null)
                result = new Result("Errore tecnico nell'invio del messaggio");

            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore nell'invio di un messaggio in Messaggistica: " + ex.Message);
            return result;
        }

        private static Spedizioni GetSpedizione(Provider provider, string testo, IList<string> destinatari, Spedizioni spedizione, Dictionary<string, object> campiAggiuntiviProvider, Type spedizioneDaIstanziare, object istanzaSpedizione, ISession innersession = null)
        {
            MethodInfo method = spedizioneDaIstanziare.GetMethod("Create");
            //Se è presente un operatore loggato inserisco il suo ID nella spedizione, altrimenti imposto di default l'id dell'amministratore di sistema (sempre 1)
            //per evitare l'eccezione sulla creazione della spedizione
            int idOperatore = 1;
            //if (NetCms.Users.AccountManager.CurrentAccount != null)
            //    idOperatore = NetCms.Users.AccountManager.CurrentAccount.ID;
            object[] parametri = new object[] { DateTime.Now, idOperatore, testo, provider, destinatari, campiAggiuntiviProvider, innersession };

            spedizione = method.Invoke(istanzaSpedizione, parametri) as Spedizioni;
            return spedizione;
        }

        private static Type GetSpedizioneType(Canali canale, Provider provider)
        {
            var spedizioniDisponibili = Assembly.GetCallingAssembly().GetTypes().Where(type => type.IsSubclassOf(typeof(Spedizioni))).Where(x => x.GetCustomAttributes(typeof(ChannelAttribute), false).Length > 0);

            Type spedizioneDaIstanziare = null;

            foreach (Type type in spedizioniDisponibili)
            {
                Attribute[] attrs = System.Attribute.GetCustomAttributes(type);

                IEnumerable<Attribute> attributoProvider = null;

                if (provider.Tipo == Provider.Type.Sms)
                {
                    ProviderSms smsProvider = (ProviderSms)provider;
                    attributoProvider = attrs.Where(x => x.GetType() == typeof(ProviderAttribute) &&
                                                   ((x as ProviderAttribute).Name == smsProvider.SmsType.ToString() || (x as ProviderAttribute).Name == "All"));
                }
                else if (provider.Tipo == Provider.Type.Email)
                {
                    attributoProvider = attrs.Where(x => x.GetType() == typeof(ProviderAttribute) && ((x as ProviderAttribute).Name == provider.Tipo.ToString() || (x as ProviderAttribute).Name == "All"));
                }

                var attributoCanale = attrs.Where(x => x.GetType() == typeof(ChannelAttribute) && (x as ChannelAttribute).Name.ToLower() == canale.Nome.ToLower());

                if (attributoProvider != null && attributoCanale != null && attributoProvider.Count() == 1 && attributoCanale.Count() == 1)
                {
                    spedizioneDaIstanziare = type;
                    break;
                }
            }
            return spedizioneDaIstanziare;
        }
    }
}
