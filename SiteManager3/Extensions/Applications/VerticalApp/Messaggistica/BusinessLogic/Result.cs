﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Messaggistica
{
    public class Result
    {
        public string Error
        {
            get;
            set;
        }

        public bool IsOk
        {
            get;
            set;
        }

        public string  Esito
        {
            get;
            set;
        }

        public Result()
        {
            IsOk = true;
        }

        public Result(string errore)
        {
            IsOk = false;
            Error = errore;
        }
    }
}
