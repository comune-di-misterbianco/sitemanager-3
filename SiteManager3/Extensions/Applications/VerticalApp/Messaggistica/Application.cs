﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using NetCms.Vertical;
using NetCms.Vertical.Grants;
using NHibernate;
using NetCms.Diagnostics;


namespace Messaggistica
{
    public class Messaggistica : VerticalApplication
    {
        public override VerticalApplication.AccessModes AccessMode
        {
            get { return AccessModes.Admin; }
        }

        public NetCms.Vertical.Configuration.Configuration Configuration
        {
            get
            {
                return _Configuration ?? (_Configuration = new NetCms.Vertical.Configuration.Configuration(this.SystemName));
            }
        }
        private NetCms.Vertical.Configuration.Configuration _Configuration;

        public override int HomepageGroup
        {
            get { return 2; }
        }

        public const string SystemNameKey = "messaggistica";
        public const int IDKey = 2002;

        public override bool CheckForDefaultGrant
        {
            get { return false; }
        }

        public override string Description
        {
            get { return "Messaggistica"; }
        }

        public override int ID
        {
            get { return IDKey; }
        }

        public override string Label
        {
            get { return "Messaggistica"; }
        }

        public override bool NetworkDependant
        {
            get { return false; }
        }

        public override int SecurityLevel
        {
            get { return 0; }
        }

        public override string SystemName
        {
            get { return "messaggistica"; }
        }

        public override bool DoAdditionalConfigurationForExternalDB()
        {
            using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    if (BusinessLogic.GetCanaleSMS(sess) == null)
                        sess.CreateSQLQuery("INSERT INTO messaggistica_canali (Id, Nome) VALUES (1, 'Sms');").ExecuteUpdate();
                    if (BusinessLogic.GetCanaleEmail(sess) == null)
                        sess.CreateSQLQuery("INSERT INTO messaggistica_canali (Id, Nome) VALUES (2, 'Email');").ExecuteUpdate();
                    
                    
                    tx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, "", "", "");
                    return false;
                }
            }
        }

        public override bool NeedAdditionalConfiguration
        {
            get { return true; }
        }

        public GrantElement GrantsManager
        {
            get
            {
                if (_GrantsManager == null)
                    _GrantsManager = new GrantElement(this.ID.ToString(), NetCms.Users.Profile.CurrentProfile);
                return _GrantsManager;
            }
        }
        private GrantElement _GrantsManager;

        public override VerticalApplicationSetup Installer
        {
            get
            {
                if (_Installer == null)
                    _Installer = new MessaggisticaSetup(this);
                return _Installer;
            }
        }
        private MessaggisticaSetup _Installer;

        public Messaggistica()
            : base()
        {
           
        }

        public Messaggistica(Assembly assembly)
            : base(assembly)
        {
        }

        private string _BackofficeUrl;
        public string BackofficeUrl
        {
            get 
            {
                return "/applications/" + SystemNameKey + "/admin/default.aspx";
            }
        }

        public override bool UseCmsSessionFactory
        {
            get { return true; }
        }

        public override ICollection<Assembly> FluentAssembliesRelated
        {
            get
            {
                ICollection<Assembly> coll = new List<Assembly>();
                coll.Add(this.Assembly);
                coll.Add(Assembly.GetAssembly(typeof(UsersAggregator.UsersAggregator)));
                return coll;
            }
        }
    }
}
