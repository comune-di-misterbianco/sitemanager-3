﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Messaggistica.Model
{
    public class EmailInviate
    {
        public virtual int Id
        {
            get;
            set;
        }

        public virtual string IndirizzoDestinatario
        {
            get;
            set;
        }

        public virtual SpedizioniEmail SpedizioneEmail
        {
            get;
            set;
        }
    }
}
