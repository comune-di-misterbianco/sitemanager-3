﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;

namespace Messaggistica.Model
{
    public abstract class Provider
    {
        public Provider()
        {
            Spedizioni = new HashSet<Spedizioni>();
            ProviderNetworks = new HashSet<ProviderNetwork>();
        }

        public virtual int Id
        {
            get;
            set;
        }

        public virtual string Nome
        {
            get;
            set;
        }

        public virtual Canali Canale
        {
            get;
            set;
        }

        public virtual bool IsDefault
        {
            get;
            set;
        }

        public virtual ICollection<Spedizioni> Spedizioni
        {
            get;
            set;
        }

        public virtual ICollection<ProviderNetwork> ProviderNetworks
        {
            get;
            set;
        }

        // usati per l'invio dei messaggi
        public virtual List<VfGeneric> VfFields
        {
            get;
            set;
        }

        public virtual List<VfGeneric> FormFields { get; set; }

        // usati per l'invio dei messaggi
        public abstract string[] LabelsCampiAggiuntiviFormInserimento
        {
            get;
        }

        public enum Type
        {
            Email,
            Sms,        
        }

        public abstract Type Tipo
        {
            get;
        }

        public enum Column {
            Id,
            Nome,
            Canale,
            Spedizioni,
            ProviderNetworks,
            VfFields,
            FormFiedls,
            IsDefault,
        }
    }
}
