﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Messaggistica.Model.Mapping
{
    public class ProviderEmailMapping : SubclassMap<ProviderEmail>
    {
        public ProviderEmailMapping()
        {
            Table("messaggistica_provider_email");
            KeyColumn("IDprovider_email");
            Map(x => x.SmtpHost);
            Map(x => x.SmtpPassword);
            Map(x => x.SmtpPort);
            Map(x => x.SmtpUser);
            Map(x => x.EnableSsl);
            Map(x => x.LabelMittente);
            Map(x => x.EmailFrom);
            Map(x => x.IsDefault);
        }
    }
}
