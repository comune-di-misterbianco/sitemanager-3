﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Messaggistica.Model.Mapping
{
    public class ProviderNetworkMapping : ClassMap<ProviderNetwork>
    {
        public ProviderNetworkMapping()
        {
            Table("messaggistica_provider_network");
            Id(x => x.ID);
            Map(x => x.IdNetwork);
            References(x => x.Provider)
                .Column("IDprovider")
                .Fetch.Select();
        }
    }
}
