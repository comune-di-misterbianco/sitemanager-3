﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Messaggistica.Model.Mapping
{
    public class ProviderClickatellMapping : SubclassMap<ProviderClickatell>
    {
        public ProviderClickatellMapping()
        {
            Table("messaggistica_provider_sms_clickatell");
            KeyColumn("IDprovider_sms_clickatell");
            Map(x => x.ApiId);
            Map(x => x.UserName);
            Map(x => x.Password);
            Map(x => x.Mittente);
        }
    }
}
