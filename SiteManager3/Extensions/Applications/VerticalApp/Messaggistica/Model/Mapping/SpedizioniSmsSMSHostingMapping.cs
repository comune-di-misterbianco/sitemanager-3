﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Messaggistica.Model.Mapping
{
    public class SpedizioniSmsSMSHostingMapping : SubclassMap<SpedizioniSmsSMSHosting>
    {
        public SpedizioniSmsSMSHostingMapping()
        {
            Table("messaggistica_spedizioni_sms_smshosting");
            KeyColumn("IDspedizioni_sms_smshosting");
            //KeyColumn("IDSpedizione");
        }
    }
}
