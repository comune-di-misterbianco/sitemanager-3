﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Messaggistica.Model.Mapping
{
    public class ProviderSMSHostingMapping : SubclassMap<ProviderSMSHosting>
    {
        public ProviderSMSHostingMapping()
        {
            Table("messaggistica_provider_sms_smshosting");
            KeyColumn("IDprovider_sms_smshosting");
            Map(x => x.AUTH_KEY);
            Map(x => x.AUTH_SECRET);
            Map(x => x.Mittente);
        }
    }
}
