﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Messaggistica.Model.Mapping
{
    public class SpedizioniSmsArubaMapping : SubclassMap<SpedizioniSmsAruba>
    {
        public SpedizioniSmsArubaMapping()
        {
            Table("messaggistica_spedizioni_sms_aruba");
            //KeyColumn("IDSpedizione");
            KeyColumn("IDspedizioni_sms_aruba");
        }
    }
}
