﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Messaggistica.Model.Mapping
{
    public class SpedizioniSmsMapping : SubclassMap<SpedizioniSms>
    {
        public SpedizioniSmsMapping()
        {
            Table("messaggistica_spedizioni_sms");
            KeyColumn("IDspedizioni_sms");
            //KeyColumn("IDSpedizione");
            HasMany(x => x.SmsInviati)
                .KeyColumn("IDspedizione_sms")
                .Fetch.Select().AsSet()
                .Cascade.All();
        }
    }
}
