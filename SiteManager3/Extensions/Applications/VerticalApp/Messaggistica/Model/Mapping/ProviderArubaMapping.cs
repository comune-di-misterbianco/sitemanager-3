﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Messaggistica.Model.Mapping
{
    public class ProviderArubaMapping : SubclassMap<ProviderAruba>
    {
        public ProviderArubaMapping()
        {
            Table("messaggistica_provider_sms_aruba");
            KeyColumn("IDprovider_sms_aruba");
            Map(x => x.UserName);
            Map(x => x.Password);
            Map(x => x.Mittente);
        }
    }
}
