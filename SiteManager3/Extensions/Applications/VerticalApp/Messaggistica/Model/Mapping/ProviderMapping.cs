﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Messaggistica.Model.Mapping
{
    public class ProviderMapping : ClassMap<Provider>
    {
        public ProviderMapping()
        {
            Table("messaggistica_provider");
            Id(x => x.Id);
            Map(x => x.Nome);
            References(x => x.Canale)
                .Column("IDcanale")
                .Fetch.Select();
            HasMany(x => x.ProviderNetworks)
               .KeyColumn("IDprovider")
               .Fetch.Select().AsSet()
               .Cascade.All();
            HasMany(x => x.Spedizioni)
               .KeyColumn("IDprovider")
               .Fetch.Select().AsSet()
               .Cascade.All();
        }

    }
}
