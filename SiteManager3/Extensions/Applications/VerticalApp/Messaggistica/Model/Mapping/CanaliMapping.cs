﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Messaggistica.Model.Mapping
{
    public class CanaliMapping : ClassMap<Canali>
    {
        public CanaliMapping()
        {
            Table("messaggistica_canali");
            Id(x => x.Id);
            Map(x => x.Nome);
            HasMany(x => x.Providers)
                .KeyColumn("IDcanale")
                .Fetch.Select().AsSet()
                .Cascade.All();
        }
    }
}
