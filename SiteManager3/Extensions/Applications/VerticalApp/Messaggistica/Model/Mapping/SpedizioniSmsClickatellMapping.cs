﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Messaggistica.Model.Mapping
{
    public class SpedizioniSmsClickatellMapping : SubclassMap<SpedizioniSmsClickatell>
    {
        public SpedizioniSmsClickatellMapping()
        {
            Table("messaggistica_spedizioni_sms_clickatell");
            KeyColumn("IDspedizioni_sms_clickatell");
            //KeyColumn("IDSpedizione");
        }
    }
}
