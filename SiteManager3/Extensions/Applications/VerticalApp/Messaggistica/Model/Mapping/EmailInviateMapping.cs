﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Messaggistica.Model.Mapping
{
    public class EmailInviateMapping: ClassMap<EmailInviate>
    {
        public EmailInviateMapping()
        {
            Table("messaggistica_email_inviate");
            Id(x => x.Id);
            Map(x => x.IndirizzoDestinatario);
            References(x => x.SpedizioneEmail)
                .Column("IDspedizione_email")
                .Fetch.Select();
        }
    }
}
