﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Messaggistica.Model.Mapping
{
    public class SpedizioniEmailMapping : SubclassMap<SpedizioniEmail>
    {
        public SpedizioniEmailMapping()
        {
            Table("messaggistica_spedizioni_email");
            //KeyColumn("IDSpedizione");
            KeyColumn("IDspedizioni_email");
            Map(x => x.Oggetto);
            HasMany(x => x.EmailInviate)
                .KeyColumn("IDspedizione_email")
                .Fetch.Select().AsSet()
                .Cascade.All();
        }
    }
}
