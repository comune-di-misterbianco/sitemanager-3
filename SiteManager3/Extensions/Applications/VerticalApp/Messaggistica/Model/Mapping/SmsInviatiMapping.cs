﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Messaggistica.Model.Mapping
{
    public class SmsInviatiMapping : ClassMap<SmsInviati>
    {
        public SmsInviatiMapping()
        {
            Table("messaggistica_sms_inviati");
            Id(x => x.Id);
            Map(x => x.NumeroDestinatario);
            Map(x => x.Stato);
            References(x => x.SpedizioneSms)
                .Column("IDspedizione_sms")
                .Fetch.Select();
        }
    }
}
