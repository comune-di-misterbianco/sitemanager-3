﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Messaggistica.Model.Mapping
{
    public class ProviderSmsMapping : SubclassMap<ProviderSms>
    {
        public ProviderSmsMapping()
        {
            Table("messaggistica_provider_sms");
            KeyColumn("IDprovider_sms");
            Map(x => x.IsDefault);
        }
    }
}
