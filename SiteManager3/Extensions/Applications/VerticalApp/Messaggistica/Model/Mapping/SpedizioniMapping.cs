﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Messaggistica.Model.Mapping
{
    public class SpedizioniMapping : ClassMap<Spedizioni>
    {
        public SpedizioniMapping()
        {
            Table("messaggistica_spedizioni");
            Id(x => x.Id);//.Column("IDSpedizione");
            Map(x => x.DataOra);
            Map(x => x.IdOperatore);
            Map(x => x.Testo).CustomSqlType("text"); 
            References(x => x.Provider)
                .Column("IDprovider")
                .Fetch.Select();
        }
    }
}
