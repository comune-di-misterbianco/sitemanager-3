﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GenericDAO.DAO;
using NHibernate;
using System.Net;
using System.IO;
using Messaggistica.Utils;

namespace Messaggistica.Model
{
    [ChannelAttribute("Sms")]
    [ProviderAttribute("Aruba")]
    public class SpedizioniSmsAruba : SpedizioniSms
    {
        public SpedizioniSmsAruba() : base() { }

        public override Spedizioni Create(DateTime dataOra, int idOperatore, string testo, Provider provider, IList<string> destinatari, Dictionary<string, object> campiAggiuntiviProvider, NHibernate.ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = BusinessLogic.GetCurrentSession();

            IGenericDAO<SpedizioniSmsAruba> dao = new CriteriaNhibernateDAO<SpedizioniSmsAruba>(innerSession);
            SpedizioniSmsAruba aruba = new SpedizioniSmsAruba();
            CostruisciSpedizione(dataOra, idOperatore, testo, provider, destinatari, campiAggiuntiviProvider, aruba as SpedizioniSms);
            return dao.SaveOrUpdate(aruba);
        }

        public override Result Send(NHibernate.ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = BusinessLogic.GetCurrentSession();

            Result result = new Result();

            string url = "http://admin.sms.aruba.it/sms/send.php";

            ProviderAruba provider = (ProviderAruba)Provider;


            if (provider.SmsResidui >= SmsInviati.Count)
            {

                /*
                Parametri obbligatori

                • rcpt → Numero destinatario nel formato internazionale +XXYYYZZZZZZZ
                • data → Testo del messaggio(la lunghezza del testo dipende dall'operation utilizzata)
                • sender → Mittente del messaggio(max 11 caratteri alfanumerici o numero +XXYYYZZZZZZZ)
                • qty → Qualita del messaggio.Queste sono i possibili valori:
                        n → Notifica
                        h → Alta
                        a → Automatica
                        l(lettera elle minuscola) → Media
                        ll(doppia elle minuscola) → Bassa
                */
                string postParameterPattern = "data={0}&rcpt={1}&sender={2}&qty={3}&user={4}&pass={5}";

                string numDestinatario = SmsInviati.FirstOrDefault().NumeroDestinatario;

                string data = string.Format(postParameterPattern,
                                           Testo,
                                            "%2b39" + numDestinatario,
                                            provider.Mittente,
                                            "a",
                                            provider.UserName,
                                            provider.Password
                                            );

                string esito = Utility.Post(url, data, provider.UserName, provider.Password);


                if (esito.Contains("OK"))
                {
                    result.IsOk = true;
                    result.Esito = esito;
                    this.SmsInviati.Single(x => x.NumeroDestinatario == numDestinatario).Stato = Model.SmsInviati.TipoStato.Inviato;
                }
                else
                {
                    result.IsOk = false;
                    result.Error = esito;
                    this.SmsInviati.Single(x => x.NumeroDestinatario == numDestinatario).Stato = Model.SmsInviati.TipoStato.Errore;
                }

                IGenericDAO<SpedizioniSmsAruba> dao = new CriteriaNhibernateDAO<SpedizioniSmsAruba>(innerSession);
                dao.SaveOrUpdate(this);
            }
            else
                result = new Result("Numero di crediti insufficiente per l'invio");

            return result;
        }

        public override Result Send(int networkId, NHibernate.ISession innerSession = null)
        {
           return Send(innerSession);
        }


        //private string Post(string url, string data, string user, string password)
        //{
        //    string result = null;
        //    try
        //    {
        //        byte[] buffer = Encoding.Default.GetBytes(data);

        //        HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(url);

        //        //string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(user + ":" + password));

        //        string authInfo = user + ":" + password;
        //        authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
        //        WebReq.Headers.Add("Authorization", "Basic " + authInfo);

        //        WebReq.Method = "POST";
        //        WebReq.ContentType = "application/x-www-form-urlencoded";
        //        WebReq.ContentLength = buffer.Length;
        //        Stream PostData = WebReq.GetRequestStream();

        //        PostData.Write(buffer, 0, buffer.Length);
        //        PostData.Close();
        //        HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
        //        //Console.WriteLine(WebResp.StatusCode);

        //        Stream Response = WebResp.GetResponseStream();
        //        StreamReader _Response = new StreamReader(Response);
        //        result = _Response.ReadToEnd();
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }
        //    return result.Trim() + "\n";
        //}
    }
}
