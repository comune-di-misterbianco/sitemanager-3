﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GenericDAO.DAO;
using System.Collections;
using NetService.Utility.ArTable;
using System.Web.UI.WebControls;
using NetService.Utility.ValidatedFields;
using System.Web.UI;
using G2Core.Common;
using System.Threading.Tasks;
using NHibernate;

namespace Messaggistica.Model
{
    [ChannelAttribute("Email")]
    [ProviderAttribute("All")]
    public class SpedizioniEmail : Spedizioni
    {
        public SpedizioniEmail():base()
        {
            EmailInviate = new HashSet<EmailInviate>();
        }

        public virtual string Oggetto
        {
            get;
            set;
        }

        public virtual ICollection<EmailInviate> EmailInviate
        {
            get;
            set;
        }

        //Aggiunta la sessione per prevenire problemi di sessione "mista"
        public override Spedizioni Create(DateTime dataOra, int idOperatore, string testo, Provider provider, IList<string> destinatari, Dictionary<string, object> campiAggiuntiviProvider, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = BusinessLogic.GetCurrentSession();

            IGenericDAO<SpedizioniEmail> dao = new CriteriaNhibernateDAO<SpedizioniEmail>(innerSession);
            SpedizioniEmail email = new SpedizioniEmail();
            email.Provider = provider;
            email.IdOperatore = idOperatore;
            email.DataOra = dataOra;
            email.Testo = testo;

            Type reflectionObject = email.GetType();
            foreach (KeyValuePair<string,object> campoAggiuntivo in campiAggiuntiviProvider)
            {
                reflectionObject.GetProperty(campoAggiuntivo.Key).SetValue(email, campoAggiuntivo.Value, null);
            }

            foreach (string destinatario in destinatari)
            {
                EmailInviate inviate = new EmailInviate();
                inviate.IndirizzoDestinatario = destinatario;
                inviate.SpedizioneEmail = email;
                email.EmailInviate.Add(inviate);
            }
            return dao.SaveOrUpdate(email);
        }

        public override ArTable GetArTableDestinatari()
        {
            ArTable table = new ArTable(this.EmailInviate);
            table.InnerTableCssClass = "tab";
            table.AddColumn(new ArTextColumn("Indirizzo email", "IndirizzoDestinatario"));
            return table;
        }

        public override WebControl GetDetailControl()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);

            VfRichTextBox rte = new VfRichTextBox("Testo", "Testo") { DefaultValue = Testo, Required = false, ReadOnly = true };

            WebControl fieldsetTestEmail = new WebControl(HtmlTextWriterTag.Fieldset);
            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            fieldsetTestEmail.CssClass = "messaggistica_fieldset";
            fieldsetTestEmail.Controls.Add(legend);
            fieldsetTestEmail.Controls.Add(rte);
            legend.Controls.Add(new LiteralControl("Testo email"));

            G2Core.UI.DetailsSheet sheet = new G2Core.UI.DetailsSheet("Dettagli spedizione", "Dettagli per la spedizione");
            sheet.AddTitleTextRow("Oggetto", Oggetto);
            sheet.AddTitleTextRow("Data/Ora", DataOra.ToShortDateString());
            //sheet.AddTitleTextRow("Operatore", NetCms.Users.User.Fabricate(IdOperatore.ToString()).NomeCompleto);
            sheet.AddTitleTextRow("Operatore", NetCms.Users.UsersBusinessLogic.GetById(IdOperatore).NomeCompleto);
            sheet.AddTitleTextRow("Provider", Provider.Nome);
            sheet.AddTitleTextRow("Canale", Provider.Canale.Nome);

            div.Controls.Add(fieldsetTestEmail);
            div.Controls.Add(sheet);

            return div;
        }

        /// <summary>
        /// invia solo se trova nel db una relazione tra portale e provider (tabella "messaggistica_provider_network")
        /// in quel caso usa come label mittente la label del portale
        /// </summary>
        /// <param name="network"></param>
        /// <returns></returns>
        public override Result Send(int networkId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = BusinessLogic.GetCurrentSession();

            Result result = null;
            ProviderEmail provider = Provider as ProviderEmail;

            string labelMittente = "";
            //if (provider.ProviderNetworks.Select(x => x.IdNetwork).Contains(networkId))
            //{
                labelMittente = NetCms.Networks.NetworksBusinessLogic.GetById(networkId).Label;
                try
                {
                    Parallel.ForEach(EmailInviate, emailCorrente =>
                    {
                        MailSender sender = new MailSender(emailCorrente.IndirizzoDestinatario, Oggetto, Testo, provider.EmailFrom, labelMittente);
                        sender.EnableSsl = provider.EnableSsl;
                        sender.SetSmtpHost = provider.SmtpHost;
                        sender.SetSmtpPort = provider.SmtpPort;
                        sender.SmtpPassword = provider.SmtpPassword;
                        sender.SmtpUser = provider.SmtpUser;
                        sender.SendAsHtml = true;
                        sender.Send();
                    });
                    result = new Result();
                }
                catch
                {
                    result = new Result("Errore tecnico nell'invio delle email");
                }
            //}
            //else result = new Result("Il portale utilizzato non possiede il provider " + provider.Nome);
            return result;
        }

        public override Result Send(ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = BusinessLogic.GetCurrentSession();

            Result result = null;
            ProviderEmail provider = Provider as ProviderEmail;
            try
            {
                Parallel.ForEach(EmailInviate, emailCorrente =>
                {
                    MailSender sender = new MailSender(emailCorrente.IndirizzoDestinatario, Oggetto, Testo, provider.EmailFrom, provider.LabelMittente);
                    sender.EnableSsl = provider.EnableSsl;
                    sender.SetSmtpHost = provider.SmtpHost;
                    sender.SetSmtpPort = provider.SmtpPort;
                    sender.SmtpPassword = provider.SmtpPassword;
                    sender.SmtpUser = provider.SmtpUser;
                    sender.SendAsHtml = true;
                    sender.Send();
                });
                result = new Result();
            }
            catch
            {
                result = new Result("Errore tecnico nell'invio delle email");
            }
            return result;
        }
    }
}
