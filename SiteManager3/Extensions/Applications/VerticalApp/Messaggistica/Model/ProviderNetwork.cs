﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Messaggistica.Model
{
    public class ProviderNetwork
    {
        public ProviderNetwork()
        { }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual int IdNetwork
        {
            get;
            set;
        }

        public virtual Provider Provider
        {
            get;
            set;
        }
    }
}
