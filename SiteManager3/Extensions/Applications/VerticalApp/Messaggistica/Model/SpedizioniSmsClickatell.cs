﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GenericDAO.DAO;
using NHibernate;

namespace Messaggistica.Model
{
    [ChannelAttribute("Sms")]
    [ProviderAttribute("Clickatell")]
    public class SpedizioniSmsClickatell : SpedizioniSms
    {
        public SpedizioniSmsClickatell(): base() { }
        public override Spedizioni Create(DateTime dataOra, int idOperatore, string testo, Provider provider, IList<string> destinatari, Dictionary<string, object> campiAggiuntiviProvider, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = BusinessLogic.GetCurrentSession();
            IGenericDAO<SpedizioniSmsClickatell> dao = new CriteriaNhibernateDAO<SpedizioniSmsClickatell>(innerSession);
            SpedizioniSmsClickatell clickaTell = new SpedizioniSmsClickatell();
            CostruisciSpedizione(dataOra, idOperatore, testo, provider, destinatari, campiAggiuntiviProvider, clickaTell as SpedizioniSms);
            return dao.SaveOrUpdate(clickaTell);
        }

        /// <summary>
        /// questo dovrebbe essere utilizzato solo per i test
        /// </summary>
        /// <returns></returns>
        public override Result Send(ISession innerSession = null)
        {

            if (innerSession == null)
                innerSession = BusinessLogic.GetCurrentSession();

            Result result = null;
            Clickatell.PushServerWSPortTypeClient client = new Clickatell.PushServerWSPortTypeClient();
            ProviderClickatell provider = (ProviderClickatell)Provider;

            string session_id = client.auth(provider.ApiId, provider.UserName, provider.Password);
            if (session_id.Contains("OK: "))
            {
                session_id = session_id.Replace("OK: ", "");
                string creditoResiduo = client.getbalance(session_id, provider.ApiId, provider.UserName, provider.Password);
                if (creditoResiduo.Contains("Credit: "))
                {
                    creditoResiduo = creditoResiduo.Replace(".", ",");
                    float creditLeft = float.Parse(creditoResiduo.Replace("Credit: ", ""));
                    int numeroSmsRimasti = (int)Math.Floor(creditLeft / 2);
                    if (numeroSmsRimasti >= SmsInviati.Count)
                    {
                        // il mittente, per essere visualizzato nell'sms, deve essere registrato presso Clickatell
                        string[] sendMsgResults = client.sendmsg(session_id, provider.ApiId, provider.UserName, provider.Password, SmsInviati.Select(x => x.NumeroDestinatario = "39" + x.NumeroDestinatario).ToArray(), provider.Mittente, Testo, 0, 1, 0, 0, 2, 0, 3, 1, 0, this.Id.ToString(), 0, "SMS_TEXT", "", "", 1440);

                        string[] errorsMsgs = sendMsgResults.Where(x => x.Contains("ERR:")).ToArray();
                        if (SmsInviati.Count == 1)
                        {
                            if (errorsMsgs.Length > 0)
                            {
                                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Errore nell'invio di un messaggio al destinatario: " + SmsInviati.ElementAt(0).NumeroDestinatario + " durante la spedizione con id " + Id + ". Codice errore: " + errorsMsgs[0]);
                                result = new Result("Errore durante l'invio del messaggio");
                            }
                        }
                        else // sms inviati > 1
                        {
                            string[] errorsNumeriDestinatari = errorsMsgs.Select(x => x = x.Substring(x.LastIndexOf("To:") + 4).Replace("\n", "")).ToArray();
                            SmsInviati = SmsInviati.Where(x => !errorsNumeriDestinatari.Contains(x.NumeroDestinatario)).ToList<SmsInviati>();

                            foreach (string error in errorsMsgs)
                            {
                                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Errore nell'invio di un messaggio durante la spedizione con id " + Id + ". Codice errore: " + error);
                            }
                        }
                    }
                    else result = new Result("Numero crediti insufficiente per l'invio");
                }
                else result = new Result("Impossibile recuperare il credito residuo");
            }
            else result = new Result("Impossibile autenticarsi al servizio Clickatell");

            result = new Result();
            return result;
        }

        public override Result Send(int networkId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = BusinessLogic.GetCurrentSession();

            Result result = new Result();
            Clickatell.PushServerWSPortTypeClient client = new Clickatell.PushServerWSPortTypeClient();
            ProviderClickatell provider = (ProviderClickatell)Provider;

            //if (provider.ProviderNetworks.Select(x => x.IdNetwork).Contains(networkId))
            //{
                string session_id = client.auth(provider.ApiId, provider.UserName, provider.Password);
                if (session_id.Contains("OK: "))
                {
                    session_id = session_id.Replace("OK: ", "");
                    string creditoResiduo = client.getbalance(session_id, provider.ApiId, provider.UserName, provider.Password);
                    if (creditoResiduo.Contains("Credit: "))
                    {
                        creditoResiduo = creditoResiduo.Replace(".", ",");
                        float creditLeft = float.Parse(creditoResiduo.Replace("Credit: ", ""));
                        int numeroSmsRimasti = (int)Math.Floor(creditLeft / 2);
                        if (numeroSmsRimasti >= SmsInviati.Count)
                        {
                            // il mittente, per essere visualizzato nell'sms, deve essere registrato presso Clickatell
                            string[] sendMsgResults = client.sendmsg(session_id, provider.ApiId, provider.UserName, provider.Password, SmsInviati.Select(x => x.NumeroDestinatario = "39" + x.NumeroDestinatario).ToArray(), provider.Mittente, Testo, 0, 1, 0, 0, 2, 0, 3, 1, 0, this.Id.ToString(), 0, "SMS_TEXT", "", "", 1440);

                            string[] errorsMsgs = sendMsgResults.Where(x => x.Contains("ERR:")).ToArray();
                            if (SmsInviati.Count == 1)
                            {
                                if (errorsMsgs.Length > 0)
                                {
                                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Errore nell'invio di un messaggio al destinatario: " + SmsInviati.ElementAt(0).NumeroDestinatario + " durante la spedizione con id " + Id + ". Codice errore: " + errorsMsgs[0]);
                                    result = new Result("Errore durante l'invio del messaggio");
                                }
                            }
                            else // sms inviati > 1
                            {
                                string[] errorsNumeriDestinatari = errorsMsgs.Select(x => x = x.Substring(x.LastIndexOf("To:") + 4).Replace("\n", "")).ToArray();
                                SmsInviati = SmsInviati.Where(x => !errorsNumeriDestinatari.Contains(x.NumeroDestinatario)).ToList<SmsInviati>();

                                foreach (string error in errorsMsgs)
                                {
                                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Errore nell'invio di un messaggio durante la spedizione con id " + Id + ". Codice errore: " + error);
                                }

                                if (errorsMsgs.Length > 0)
                                {
                                    result = new Result("Errore durante l'invio del messaggio");
                                }
                            }
                        }
                        else result = new Result("Numero crediti insufficiente per l'invio");
                    }
                    else result = new Result("Impossibile recuperare il credito residuo");
                }
                else result = new Result("Impossibile autenticarsi al servizio Clickatell");
            //}
            //else result = new Result("Il portale utilizzato non possiede il provider " + provider.Nome);

            return result;
        }
    }
}
