﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using NetService.Utility.ArTable;
using G2Core.UI;
using System.Web.UI.WebControls;
using NHibernate;

namespace Messaggistica.Model
{
    public abstract class Spedizioni
    {

        public Spedizioni() { }
         
        public virtual int Id
        {
            get;
            set;
        }

        public virtual DateTime DataOra
        {
            get;
            set;
        }

        public virtual int IdOperatore
        {
            get;
            set;
        }

        public virtual string Testo
        {
            get;
            set;
        }

        public virtual Provider Provider
        {
            get;
            set;
        }

        public abstract Spedizioni Create(DateTime dataOra, int idOperatore, string testo, Provider provider, IList<string> destinatari, Dictionary<string, object> campiAggiuntiviProvider, ISession innerSession = null);

        public abstract ArTable GetArTableDestinatari();

        public abstract WebControl GetDetailControl();

        /// <summary>
        /// metodo generico senza specificare il portale
        /// </summary>
        /// <returns></returns>
        public abstract Result Send(ISession innerSession = null);

        /// <summary>
        /// metodo che invia ricercando il provider per il portale passato come parametro
        /// </summary>
        /// <param name="network"></param>
        /// <returns></returns>
        public abstract Result Send(int networkId, ISession innerSession = null);
    }
}
