﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Messaggistica.Model
{
    public class SmsInviati
    {
        public SmsInviati() { }
        public virtual int Id
        {
            get;
            set;
        }

        public virtual string NumeroDestinatario
        {
            get;
            set;
        }

        public enum TipoStato
        {
            Inviato,
            NonInviato,
            Errore,
            Sconosciuto
        }

        public virtual TipoStato Stato
        {
            get;
            set;
        }

        public virtual SpedizioniSms SpedizioneSms
        {
            get;
            set;
        }
    }
}
