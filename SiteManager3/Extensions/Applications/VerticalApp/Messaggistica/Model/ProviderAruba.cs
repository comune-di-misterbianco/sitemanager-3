﻿using NetService.Utility.ValidatedFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using Messaggistica.Utils;

namespace Messaggistica.Model
{
    public class ProviderAruba : ProviderSms
    {


        public virtual string UserName
        {
            get;
            set;
        }

        public virtual string Password
        {
            get;
            set;
        }

        public virtual string Mittente
        {
            get;
            set;
        }

        // usati per la creazione o modifica di un provider
        public override List<VfGeneric> VfFields
        {
            get
            {
                List<VfGeneric> fields = new List<VfGeneric>();

                VfTextBox userName = new VfTextBox("UserName", "UserName", "UserName", InputTypes.Text) { Required = true };
                VfTextBox password = new VfTextBox("Password", "Password", "Password", InputTypes.Text) { Required = true };
                VfTextBox mittente = new VfTextBox("Mittente", "Etichetta Mittente", "Mittente", InputTypes.Text) { Required = true };
                mittente.InfoControl.Controls.Add(new LiteralControl("Usato se non associato ad uno specifico portale. In quel caso sarà utilizzato il nome del portale"));


                fields.Add(userName);
                fields.Add(password);
                fields.Add(mittente);

                return fields;
            }
        }

        public override SmsProviderType SmsType
        {
            get
            {
                return SmsProviderType.Aruba;
            }
        }   

        public override int SmsResidui
        {
            get
            {

                int smsResidui = -1;

                string urlCredit = "http://admin.sms.aruba.it/sms/credit.php";

                string postParameterPattern = "user={0}&pass={1}";

                string data = string.Format(postParameterPattern, UserName, Password);

                try {

                    string esito = Utility.Post(urlCredit, data, UserName, Password);

                    if (esito.Contains("OK"))
                    {
                        double costoUnitario = 0.046;
                        string strCrediti = esito.Replace("OK ", "").Replace(".",",").Replace("\n","");
                        double credito = 0;                        
                        credito = Convert.ToDouble(strCrediti);
                        smsResidui = Convert.ToInt32(credito/costoUnitario);
                    }                    
                }
                catch(Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la richiesta POST dei crediti residui " + typeof(ProviderAruba) + ". " + ex.Message);
                }           
                return smsResidui;
            }
        }

    }
}
