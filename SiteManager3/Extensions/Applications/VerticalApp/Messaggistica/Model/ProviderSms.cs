﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;

namespace Messaggistica.Model
{
    public abstract class ProviderSms : Provider
    {
        public override List<VfGeneric> FormFields
        {
            get
            {
                List<VfGeneric> fields = new List<VfGeneric>();
                VfTextBox textBox = new VfTextBox("Testo", "Testo", InputTypes.Text) { MaxLenght = 160, Required = true };
                textBox.TextBox.Rows = 3;
                textBox.TextBox.TextMode = System.Web.UI.WebControls.TextBoxMode.MultiLine;
                textBox.TextBox.MaxLength = 160;
                textBox.TextBox.Wrap = true;
                fields.Add(textBox);
                return fields;
            }
        }

        public override string[] LabelsCampiAggiuntiviFormInserimento
        {
            get 
            { 
                return new string[]{}; 
            }
        }

        public abstract int SmsResidui
        {
            get;
        }

        public override Type Tipo
        {
            get
            {
                return Type.Sms;
            }
        }
        public abstract SmsProviderType SmsType
        {
            get;
        }
        public enum SmsProviderType
        {
            None,
            Aruba,
            SMSHosting,
            Clickatell,
        }
    }
}
