﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI;

namespace Messaggistica.Model
{
    public class ProviderClickatell : ProviderSms
    {
        public virtual int ApiId
        {
            get;
            set;
        }

        public virtual string UserName
        {
            get;
            set;
        }

        public virtual string Password
        {
            get;
            set;
        }

        public virtual string Mittente
        {
            get;
            set;
        }

        // usati per la creazione o modifica di un provider
        public override List<VfGeneric> VfFields
        {
            get
            {
                List<VfGeneric> fields = new List<VfGeneric>();

                VfTextBox apiId = new VfTextBox("ApiId", "Api Id", "ApiId", InputTypes.Number) { Required = true };
                VfTextBox userName = new VfTextBox("UserName", "UserName", "UserName", InputTypes.Text) { Required = true };
                VfTextBox password = new VfTextBox("Password", "Password", "Password", InputTypes.Text) { Required = true };
                VfTextBox mittente = new VfTextBox("Mittente", "Etichetta Mittente", "Mittente", InputTypes.Text) { Required = true };
                mittente.InfoControl.Controls.Add(new LiteralControl("Usato se non associato ad uno specifico portale. In quel caso sarà utilizzato il nome del portale"));

                fields.Add(apiId);
                fields.Add(userName);
                fields.Add(password);
                fields.Add(mittente);

                return fields;
            }
        }

        public override SmsProviderType SmsType
        {
            get
            {
                return SmsProviderType.Clickatell;
            }
        }        

        public override int SmsResidui
        {
            get
            {
                int numeroSmsRimasti = 0;
                Clickatell.PushServerWSPortTypeClient client = new Clickatell.PushServerWSPortTypeClient();
                string session_id = client.auth(this.ApiId, this.UserName, this.Password);
                if (session_id.Contains("OK: "))
                {
                    session_id = session_id.Replace("OK: ", "");
                    string creditoResiduo = client.getbalance(session_id, this.ApiId, this.UserName, this.Password);
                    if (creditoResiduo.Contains("Credit: "))
                    {
                        creditoResiduo = creditoResiduo.Replace(".", ",");
                        float creditLeft = float.Parse(creditoResiduo.Replace("Credit: ", ""));
                        numeroSmsRimasti = (int)Math.Floor(creditLeft / 2);
                    }
                }
                return numeroSmsRimasti;
            }
        }     
    }
}
