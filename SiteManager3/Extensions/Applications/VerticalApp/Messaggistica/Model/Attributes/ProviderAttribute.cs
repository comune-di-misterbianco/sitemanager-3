﻿using System;

namespace Messaggistica.Model
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ProviderAttribute : Attribute
    {
        public string Name
        {
            get;
            private set;
        }
        
        public ProviderAttribute(string name)
        {
            this.Name = name;
        }
    }
}