﻿using System;

namespace Messaggistica.Model
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ChannelAttribute : Attribute
    {
        public string Name
        {
            get;
            private set;
        }

        public ChannelAttribute(string name)
        {
            this.Name = name;
        }
    }
}