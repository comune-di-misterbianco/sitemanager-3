﻿using GenericDAO.DAO;
using NetCms.Diagnostics;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace Messaggistica.Model
{
    [ChannelAttribute("Sms")]
    [ProviderAttribute("SMSHosting")]
    public class SpedizioniSmsSMSHosting : SpedizioniSms
    {

        public SpedizioniSmsSMSHosting() : base() { }


        public override Spedizioni Create(DateTime dataOra, int idOperatore, string testo, Provider provider, IList<string> destinatari, Dictionary<string, object> campiAggiuntiviProvider, ISession innerSession = null)
        {

            if (innerSession == null)
                innerSession = BusinessLogic.GetCurrentSession();
            try
            {
                IGenericDAO<SpedizioniSmsSMSHosting> dao = new CriteriaNhibernateDAO<SpedizioniSmsSMSHosting>(innerSession);
                SpedizioniSmsSMSHosting SMSHosting = new SpedizioniSmsSMSHosting();
                CostruisciSpedizione(dataOra, idOperatore, testo, provider, destinatari, campiAggiuntiviProvider, SMSHosting as SpedizioniSms);
                return dao.SaveOrUpdate(SMSHosting);
            }
            catch(Exception ex)
            {
                Diagnostics.TraceException(ex, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName);
                Diagnostics.TraceMessage(TraceLevel.Error, "Create Spedizione");
                return null;
            }
        }       

        public override Result Send(NHibernate.ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = BusinessLogic.GetCurrentSession();

            Result result = new Result();

            string numDestinatario = SmsInviati.FirstOrDefault().NumeroDestinatario;

            ProviderSMSHosting provider = (ProviderSMSHosting)Provider;

            // verifica credito residuo
            if (provider.SmsResidui >= SmsInviati.Count)
            {
                SendSmsResponse response = SendSms(
                                      provider.AUTH_KEY,
                                      provider.AUTH_SECRET,
                                      provider.Mittente,
                                      "+39" + numDestinatario,
                                      string.Empty,
                                      Testo,
                                      string.Empty,
                                      "spedizione n." + this.Id,
                                      false,
                                      string.Empty,
                                      string.Empty
                                      );

                if (response.sms.FirstOrDefault().status == "INSERTED")
                {
                    result.IsOk = true;
                    result.Esito = response.sms.FirstOrDefault().statusDetail;
                    this.SmsInviati.Single(x => x.NumeroDestinatario == numDestinatario).Stato = Model.SmsInviati.TipoStato.Inviato;
                }
                else
                {
                    result.IsOk = false;
                    result.Error = response.sms.FirstOrDefault().statusDetail;
                    this.SmsInviati.Single(x => x.NumeroDestinatario == numDestinatario).Stato = Model.SmsInviati.TipoStato.Errore;
                }

                IGenericDAO<SpedizioniSmsSMSHosting> dao = new CriteriaNhibernateDAO<SpedizioniSmsSMSHosting>(innerSession);
                dao.SaveOrUpdate(this);              
            }
            else
                result = new Result("Numero di crediti insufficiente per l'invio");

            return result;
        }

        public override Result Send(int networkId, NHibernate.ISession innerSession = null)
        {
            return Send(innerSession);
        }       

        public class Sms
        {
            public int id { get; set; }
            public string to { get; set; }
            public string status { get; set; }
            public string statusDetail { get; set; }
        }

        public class SendSmsResponse
        {
            public string from { get; set; }
            public string text { get; set; }
            public string transactionId { get; set; }
            public int smsInserted { get; set; }
            public int smsNotInserted { get; set; }
            public List<Sms> sms { get; set; }
        }

        //private string MakeWebRequest(string url, string authKey, string authSecret, NameValueCollection values, string method)
        //{
        //    try
        //    {
        //        if (method == "POST")
        //        {
        //            using (var client = new WebClient())
        //            {
        //                client.Credentials = new NetworkCredential(authKey, authSecret);
        //                var response = client.UploadValues(url, method, values);
        //                var responseString = Encoding.Default.GetString(response);
        //                return responseString;
        //            }
        //        }

        //        if (method == "GET")
        //        {

        //            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
        //            request.Credentials = new NetworkCredential(authKey, authSecret);
        //            request.Method = "GET";
        //            String test = String.Empty;
        //            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        //            {
        //                Stream dataStream = response.GetResponseStream();
        //                if (dataStream != null)
        //                {
        //                    StreamReader reader = new StreamReader(dataStream);
        //                    test = reader.ReadToEnd();
        //                    reader.Close();
        //                    dataStream.Close();
        //                }
        //                return test;
        //            }
        //        }

        //        return string.Empty;
        //    }
        //    catch (WebException we)
        //    {
        //        var response = (HttpWebResponse)we.Response;
        //        if (response != null)
        //        {
        //            var statusCode = (int)response.StatusCode;
        //            var statusMessage = response.StatusDescription;

        //            Stream receiveStream = response.GetResponseStream();
        //            if (receiveStream != null)
        //            {
        //                string content = new StreamReader(receiveStream, Encoding.GetEncoding("utf-8")).ReadToEnd();
        //                var jsonSerializer = new JavaScriptSerializer();
        //                var _webError = (webError)jsonSerializer.Deserialize<webError>(content);
        //                throw new Exception(_webError.errorCode + " - " + _webError.errorMsg);
        //            }
        //            else
        //            {
        //                throw new Exception(statusCode + " - " + statusMessage);
        //            }
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }

        //}

        public virtual SendSmsResponse SendSms(string authKey, string authSecret, string from, string to, string group, string text, string date, string transactionId, bool sandbox, string statusCallback, string encoding)
        {
            SendSmsResponse smsResponse = new SendSmsResponse();
            var values = new NameValueCollection();

            try
            {
                if (authKey == string.Empty)
                    throw new Exception("Provide value for AUTH_KEY");

                if (authSecret == string.Empty)
                    throw new Exception("Provide value for AUTH_SECRET");

                if (to == string.Empty && group == string.Empty)
                    throw new Exception("Provide value at least one of the fields to and group ");

                values.Add("from", from);
                values.Add("to", to);
                values.Add("date", date);
                values.Add("group", group);

                if (text != string.Empty)
                    values.Add("text", text);
                else
                    throw new Exception("Provide value for text");

                values.Add("transactionId", transactionId);
                values.Add("sandbox", (sandbox == true ? "true" : "false"));
                values.Add("statusCallback", statusCallback);
                values.Add("encoding", encoding);
                const string url = "https://api.smshosting.it/rest/api/sms/send";

                var responseText = Utils.Utility.MakeWebRequest(url, authKey, authSecret, values, "POST");

                var jsonSerializer = new JavaScriptSerializer();
                var sendResponseText = (dynamic)jsonSerializer.DeserializeObject(responseText);
                var objSendSmsResponse = new SendSmsResponse
                {
                    @from = sendResponseText["from"],
                    text = sendResponseText["text"],
                    transactionId = sendResponseText["transactionId"],
                    smsInserted = sendResponseText["smsInserted"],
                    smsNotInserted = sendResponseText["smsNotInserted"],
                    sms = new List<Sms>()
                };
                foreach (var _sms in sendResponseText["sms"])
                {
                    var sms = new Sms { to = _sms["to"], status = _sms["status"] };
                    sms.id = sms.status == "INSERTED" ? _sms["id"] : -1;
                    sms.statusDetail = sms.status == "NOT_INSERTED" ? _sms["statusDetail"] : string.Empty;
                    objSendSmsResponse.sms.Add(sms);
                }

                smsResponse = objSendSmsResponse;
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante l'invio dell'sms " + typeof(SendSmsResponse) + ". " + ex.Message);
            }
            return smsResponse;
        }
    }
}
