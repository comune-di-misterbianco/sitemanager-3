﻿using NetService.Utility.ValidatedFields;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.UI;

namespace Messaggistica.Model
{
    public class ProviderSMSHosting : ProviderSms
    {

        const string userInfoUrl = "https://api.smshosting.it/rest/api/user";

        public override int SmsResidui
        {
            get {

                int smsresidui = -1;

                try
                {
                    var values = new NameValueCollection();
                    var responseText = Utils.Utility.MakeWebRequest(userInfoUrl, AUTH_KEY, AUTH_SECRET, values, "GET");

                    var jsonSerializer = new JavaScriptSerializer();
                    var objResponse = jsonSerializer.Deserialize<UserData>(responseText);

                    smsresidui = objResponse.italysmsLow;
                }
                catch(Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: impossibile recuperare gli sms residuri" + typeof(Provider) + ". " + ex.Message);
                }
                return smsresidui;
            }
        }

        public virtual string AUTH_KEY
        {
            get;
            set;
        }

        public virtual string AUTH_SECRET
        {
            get;
            set;
        }

        public virtual string Mittente
        {
            get;
            set;
        }

        // usati per la creazione o modifica di un provider
        public override List<VfGeneric> VfFields
        {
            get
            {
                List<VfGeneric> fields = new List<VfGeneric>();

                VfTextBox AUTH_KEY = new VfTextBox("AUTH_KEY", "AUTH_KEY", "AUTH_KEY", InputTypes.Text) { Required = true };
                VfTextBox AUTH_SECRET = new VfTextBox("AUTH_SECRET", "AUTH_SECRET", "AUTH_SECRET", InputTypes.Text) { Required = true };                
                VfTextBox mittente = new VfTextBox("Mittente", "Etichetta Mittente", "Mittente", InputTypes.Text) { Required = true };
                mittente.InfoControl.Controls.Add(new LiteralControl("Usato se non associato ad uno specifico portale. In quel caso sarà utilizzato il nome del portale"));

                fields.Add(AUTH_KEY);
                fields.Add(AUTH_SECRET);                
                fields.Add(mittente);

                return fields;
            }
        }

        public override SmsProviderType SmsType
        {
            get
            {
                return SmsProviderType.SMSHosting;
            }
        }
    }

    public class UserData
    {
        public string name { get; set; }
        public string lastname { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string postcode { get; set; }
        public string province { get; set; }
        public string country { get; set; }
        public string birthdate { get; set; }
        public string phone { get; set; }
        public string msisdn { get; set; }
        public string sender { get; set; }
        public string taxcode { get; set; }
        public DateTime registrationDate { get; set; }
        public DateTime expirationDate { get; set; }
        public double credit { get; set; }
        public int italysms { get; set; }
        public int italysmsLow { get; set; }
        public string[] senderAlias { get; set; }
    }
}
