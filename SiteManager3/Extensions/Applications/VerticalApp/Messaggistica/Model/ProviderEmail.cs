﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI;

namespace Messaggistica.Model
{
    public class ProviderEmail : Provider
    {
        public virtual string SmtpHost
        {
            get;
            set;
        }

        public virtual int SmtpPort
        {
            get;
            set;
        }

        public virtual string SmtpUser
        {
            get;
            set;
        }

        public virtual string SmtpPassword
        {
            get;
            set;
        }

        public virtual string LabelMittente
        {
            get;
            set;
        }

        public virtual string EmailFrom
        {
            get;
            set;
        }

        public virtual bool EnableSsl
        {
            get;
            set;
        }

        public override List<VfGeneric> FormFields
        {
            get
            {
                List<VfGeneric> fields = new List<VfGeneric>();

                VfTextBox oggetto = new VfTextBox("Oggetto", "Oggetto") { Required = true };

                VfTextBox rte = new VfTextBox("Testo", "Testo");
                rte.Required = true;
                rte.TextBox.Rows = 6;
                rte.TextBox.Columns = 60;
                rte.TextBox.TextMode = System.Web.UI.WebControls.TextBoxMode.MultiLine;

                fields.Add(oggetto);
                fields.Add(rte);

                return fields;
            }           
        }


        public override List<VfGeneric> VfFields
        {
            get
            {
                //List<VfGeneric> fields = new List<VfGeneric>();

                //VfTextBox oggetto = new VfTextBox("Oggetto", "Oggetto") { Required = true };

                //VfTextBox rte = new VfTextBox("Testo", "Testo");
                //rte.Required = true;
                //rte.TextBox.Rows = 6;
                //rte.TextBox.Columns = 60;
                //rte.TextBox.TextMode = System.Web.UI.WebControls.TextBoxMode.MultiLine;

                //fields.Add(oggetto);
                //fields.Add(rte);

                //return fields;

                List<VfGeneric> fields = new List<VfGeneric>();

                VfTextBox smtpHost = new VfTextBox("SmtpHost", "Host SMTP", "SmtpHost", InputTypes.Text) { Required = true };
                VfTextBox smtpPort = new VfTextBox("SmtpPort", "Porta SMTP", "SmtpPort", InputTypes.Number) { Required = true };
                VfTextBox smtpUser = new VfTextBox("SmtpUser", "Utente SMTP", "SmtpUser", InputTypes.Text) { Required = true };
                VfTextBox smtpPassword = new VfTextBox("SmtpPassword", "Password SMTP", "SmtpPassword", InputTypes.Text) { Required = true };
                VfTextBox labelMittente = new VfTextBox("LabelMittente", "Etichetta Mittente", "LabelMittente", InputTypes.Text) { Required = true };
                labelMittente.InfoControl.Controls.Add(new LiteralControl("Usato se non associato ad uno specifico portale. In quel caso sarà utilizzato il nome del portale"));

                VfTextBox emailFrom = new VfTextBox("EmailFrom", "Indirizzo Email Mittente", "EmailFrom", InputTypes.Email) { Required = true };
                VfCheckBox enableSSL = new VfCheckBox("EnableSsl", "Abilita SSL", "EnableSsl") { Required = false };

                fields.Add(smtpHost);
                fields.Add(smtpPort);
                fields.Add(smtpUser);
                fields.Add(smtpPassword);
                fields.Add(smtpHost);
                fields.Add(labelMittente);
                fields.Add(emailFrom);
                fields.Add(enableSSL);

                return fields;
            }
        }

        public override string[] LabelsCampiAggiuntiviFormInserimento
        {
            get
            {
                return new string[] { "Oggetto" };
            }
        }

        // usati per la creazione o modifica di un provider
        public static List<VfGeneric> CampiAssociati
        {
            get 
            {
                List<VfGeneric> fields = new List<VfGeneric>();

                VfTextBox smtpHost = new VfTextBox("SmtpHost", "Host SMTP", "SmtpHost", InputTypes.Text) { Required = true };
                VfTextBox smtpPort = new VfTextBox("SmtpPort", "Porta SMTP", "SmtpPort", InputTypes.Number) { Required = true };
                VfTextBox smtpUser = new VfTextBox("SmtpUser", "Utente SMTP", "SmtpUser", InputTypes.Text) { Required = true };
                VfTextBox smtpPassword = new VfTextBox("SmtpPassword", "Password SMTP", "SmtpPassword", InputTypes.Text) { Required = true };
                VfTextBox labelMittente = new VfTextBox("LabelMittente", "Etichetta Mittente", "LabelMittente", InputTypes.Text) { Required = true };
                labelMittente.InfoControl.Controls.Add(new LiteralControl("Usato se non associato ad uno specifico portale. In quel caso sarà utilizzato il nome del portale"));

                VfTextBox emailFrom = new VfTextBox("EmailFrom", "Indirizzo Email Mittente", "EmailFrom", InputTypes.Email) { Required = true };
                VfCheckBox enableSSL = new VfCheckBox("EnableSsl", "Abilita SSL", "EnableSsl") { Required = false };

                fields.Add(smtpHost);
                fields.Add(smtpPort);
                fields.Add(smtpUser);
                fields.Add(smtpPassword);
                fields.Add(smtpHost);
                fields.Add(labelMittente);
                fields.Add(emailFrom);
                fields.Add(enableSSL);

                return fields;
            }
        }

        public override Provider.Type Tipo
        {
            get { return Type.Email; }
        }
    }
}
