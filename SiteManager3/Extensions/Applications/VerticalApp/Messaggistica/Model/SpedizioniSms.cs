﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using NetService.Utility.ArTable;
using System.Web.UI.WebControls;
using NetCms.Diagnostics;

namespace Messaggistica.Model
{
    public abstract class SpedizioniSms : Spedizioni
    {
        public SpedizioniSms():base()
        {
            SmsInviati = new HashSet<SmsInviati>();
        }

        public virtual ICollection<SmsInviati> SmsInviati
        {
            get;
            set;
        }

        protected void CostruisciSpedizione(DateTime dataOra, int idOperatore, string testo, Provider provider, IList<string> destinatari, Dictionary<string, object> campiAggiuntiviProvider, SpedizioniSms spedizione)
        {
            try
            {


                spedizione.Provider = provider;
                spedizione.IdOperatore = idOperatore;
                spedizione.DataOra = dataOra;
                spedizione.Testo = testo;


                Type reflectionObject = spedizione.GetType();
                foreach (KeyValuePair<string, object> campoAggiuntivo in campiAggiuntiviProvider)
                {
                    reflectionObject.GetProperty(campoAggiuntivo.Key).SetValue(spedizione, campoAggiuntivo.Value, null);
                }

                foreach (string destinatario in destinatari)
                {
                    SmsInviati inviati = new SmsInviati();
                    inviati.NumeroDestinatario = destinatario;
                    inviati.Stato = Model.SmsInviati.TipoStato.Sconosciuto;
                    
                    inviati.SpedizioneSms = spedizione as SpedizioniSms;

                    spedizione.SmsInviati.Add(inviati);
                }
            }
            catch(Exception ex)
            {
                Diagnostics.TraceException(ex , NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName);
                Diagnostics.TraceMessage(TraceLevel.Error, "Costruisci Spedizione");
            }
        }

        public override NetService.Utility.ArTable.ArTable GetArTableDestinatari()
        {
            ArTable table = new ArTable(this.SmsInviati);
            table.InnerTableCssClass = "tab";
            table.AddColumn(new ArTextColumn("Cellulare Destinatario", "NumeroDestinatario"));
            table.AddColumn(new ArTextColumn("Stato Sms", "Stato"));
            return table;
        }

        public override WebControl GetDetailControl()
        {
            G2Core.UI.DetailsSheet sheet = new G2Core.UI.DetailsSheet("Dettagli spedizione", "Dettagli per la spedizione");
            sheet.AddTitleTextRow("Testo", Testo);
            sheet.AddTitleTextRow("Data/Ora", DataOra.ToShortDateString());
            sheet.AddTitleTextRow("Operatore", NetCms.Users.UsersBusinessLogic.GetById(IdOperatore).NomeCompleto);
            sheet.AddTitleTextRow("Provider", Provider.Nome);
            sheet.AddTitleTextRow("Canale", Provider.Canale.Nome);
            return sheet;
        }

     
    }
}
