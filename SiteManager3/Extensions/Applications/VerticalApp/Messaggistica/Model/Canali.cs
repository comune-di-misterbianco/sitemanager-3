﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Messaggistica.Model
{
    public class Canali
    {
        public Canali()
        {
            Providers = new HashSet<Provider>();
        }

        public virtual int Id
        {
            get;
            set;
        }

        public virtual string Nome
        {
            get;
            set;
        }

        public virtual ICollection<Provider> Providers
        {
            get;
            set;
        }

        public virtual Result HasProviderByNetwork(int netId)
        {
            if (Providers.Where(p => p.ProviderNetworks.Where(pn => pn.IdNetwork == netId).Count() > 0).Count() > 0)
                return new Result();
            return new Result("Provider non trovato per questo portale!");
        }
    }
}
