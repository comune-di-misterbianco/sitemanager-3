﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Messaggistica
{
    public class PagineConfigurazioneValidator : ApplicationPageStatusValidator
    {
        public Messaggistica Application
        {
            get;
            private set;
        }

        public PagineConfigurazioneValidator(Messaggistica application)
        {
            Application = application;
        }

        public override string ErrorMessage
        {
            get { return "Funzionalità disponibile solo per gli amministratori di sistema"; }
        }

        public override string ErrorTitle
        {
            get { return "Non hai diritto ad accedere a questa pagina"; }
        }

        public override bool Validate()
        {
            return Application.GrantsManager["netservice"].Allowed;
        }
    }
}
