﻿using GenericDAO.DAO.Utility;

namespace Messaggistica
{
    public class NhRequestObjectValidator<T> : ApplicationPageStatusValidator
    {
        public NhRequestObject<T> NhRequestObject { get; private set; }
        public NhRequestObjectValidator(NhRequestObject<T> nhRequestObject)
        {
            NhRequestObject = nhRequestObject;
        }
        public override string ErrorTitle { get { return "Spiacente, ma non sei autorizzato a visualizzare questa pagina."; } }
        public override string ErrorMessage { get { return "Il record selezionato non esiste, o non possiedi i diritti per visualizzare questa pagina"; } }
        public override bool Validate()
        {
            return NhRequestObject.Exists;
        }
    }
}