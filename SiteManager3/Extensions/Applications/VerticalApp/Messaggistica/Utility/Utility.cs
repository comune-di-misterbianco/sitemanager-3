﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace Messaggistica.Utils
{
    public class Utility
    {
        public static string Post(string url, string data, string user, string password)
        {
            string result = null;
            try
            {
                byte[] buffer = Encoding.Default.GetBytes(data);

                HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(url);

                string authInfo = user + ":" + password;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                WebReq.Headers.Add("Authorization", "Basic " + authInfo);

                WebReq.Method = "POST";
                WebReq.ContentType = "application/x-www-form-urlencoded";
                WebReq.ContentLength = buffer.Length;
                Stream PostData = WebReq.GetRequestStream();

                PostData.Write(buffer, 0, buffer.Length);
                PostData.Close();
                HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

                Stream Response = WebResp.GetResponseStream();
                StreamReader _Response = new StreamReader(Response);
                result = _Response.ReadToEnd();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result.Trim() + "\n";
        }

        public static string MakeWebRequest(string url, string authKey, string authSecret, NameValueCollection values, string method)
        {
            try
            {
                if (method == "POST")
                {
                    using (var client = new WebClient())
                    {
                        client.Credentials = new NetworkCredential(authKey, authSecret);
                        var response = client.UploadValues(url, method, values);
                        var responseString = Encoding.Default.GetString(response);
                        return responseString;
                    }
                }

                if (method == "GET")
                {

                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
                    request.Credentials = new NetworkCredential(authKey, authSecret);
                    request.Method = "GET";
                    String test = String.Empty;
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        Stream dataStream = response.GetResponseStream();
                        if (dataStream != null)
                        {
                            StreamReader reader = new StreamReader(dataStream);
                            test = reader.ReadToEnd();
                            reader.Close();
                            dataStream.Close();
                        }
                        return test;
                    }
                }

                return string.Empty;
            }
            catch (WebException we)
            {
                var response = (HttpWebResponse)we.Response;
                if (response != null)
                {
                    var statusCode = (int)response.StatusCode;
                    var statusMessage = response.StatusDescription;

                    Stream receiveStream = response.GetResponseStream();
                    if (receiveStream != null)
                    {
                        string content = new StreamReader(receiveStream, Encoding.GetEncoding("utf-8")).ReadToEnd();
                        var jsonSerializer = new JavaScriptSerializer();
                        var _webError = (webError)jsonSerializer.Deserialize<webError>(content);
                        throw new Exception(_webError.errorCode + " - " + _webError.errorMsg);
                    }
                    else
                    {
                        throw new Exception(statusCode + " - " + statusMessage);
                    }
                }
                else
                {
                    throw;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        private class webError
        {
            public string errorMsg { get; set; }
            public int errorCode { get; set; }
        }
    }
}
