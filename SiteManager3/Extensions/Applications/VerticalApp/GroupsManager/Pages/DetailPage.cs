﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetTable;
using System.Collections.Generic;
using NetCms.Users.Search;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using System.Linq;
using NetCms.Vertical.Grants;
using NetCms.Grants;

namespace NetCms.Users.GroupsManager
{
    [DynamicUrl.PageControl("group_details.aspx", ApplicationID = GroupsApplication.ApplicationID)]
    public class DetailPage : AbstractPage
    {        
        public override string PageTitle
        {
            get { return base.PageTitle + "Dettagli gruppo '" + Group.Name + "'"; }
        }

        private Group _Group;
        private Group Group
        {
            get
            {
                if (_Group == null)
                {
                    G2Core.Common.RequestVariable req = new G2Core.Common.RequestVariable("id", G2Core.Common.RequestVariable.RequestType.QueryString);
                    if (req.IsValidInteger)
                        _Group = GroupsBusinessLogic.GetGroupById(req.IntValue);
                }
                if (_Group == null)
                    PopupBox.AddSessionMessage("Il gruppo non esiste", PostBackMessagesType.Error, this.Account.History.GetBackURL());

                return _Group;
            }
        }

        public ICollection<User> UsersAdministratorTable
        {
            get
            {
                if (_UsersAdministratorTable == null)
                {
                    Search.UserRelated UsersDataManager = new Search.UserRelated(NetCms.Users.AccountManager.CurrentAccount);
                    SearchCondition idGroupCondition = new SearchCondition("ID", this.Group.ID, SearchCondition.ComparisonType.Equals, SearchCondition.CriteriaType.Group);

                    SearchCondition administratorCondition = new SearchCondition("Type", 0, SearchCondition.ComparisonType.Not, SearchCondition.CriteriaType.GroupAssociation);

                    _UsersAdministratorTable = UsersDataManager.UsersIAdmin(0, UsersDataManager.UsersIAdminCount(null, new SearchCondition[] { idGroupCondition, administratorCondition }), null, new SearchCondition[] { idGroupCondition, administratorCondition });

                }
                return _UsersAdministratorTable;
            }
        }
        private ICollection<User> _UsersAdministratorTable;



        public ICollection<User> UsersPublisherTable
        {
            get
            {
                if (_UsersPublisherTable == null)
                {
                    Search.UserRelated UsersDataManager = new Search.UserRelated(NetCms.Users.AccountManager.CurrentAccount);
                    SearchCondition idGroupCondition = new SearchCondition("ID", this.Group.ID, SearchCondition.ComparisonType.Equals, SearchCondition.CriteriaType.Group);

                    SearchCondition publisherCondition = new SearchCondition("PublishRole", 0, SearchCondition.ComparisonType.Not, SearchCondition.CriteriaType.GroupAssociation);

                    _UsersPublisherTable = UsersDataManager.UsersIAdmin(0, UsersDataManager.UsersIAdminCount(null, new SearchCondition[] { idGroupCondition, publisherCondition }), null, new SearchCondition[] { idGroupCondition, publisherCondition });

                }
                return _UsersPublisherTable;
            }
        }
        private ICollection<User> _UsersPublisherTable;

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Chart_Organisation; }
        }
        
        private void SetToolbars()
        {
            if (Group.Parent != null && !Group.IsSystem)
                this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/{0}/group_move.aspx?gid=".FormatByParameters(GroupsApplication.ApplicationSystemName) + this.Group.ID, NetCms.GUI.Icons.Icons.Arrow_Switch, "Sposta Gruppo"));
            this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/{0}/group_mod.aspx?gid=".FormatByParameters(GroupsApplication.ApplicationSystemName) + this.Group.ID, NetCms.GUI.Icons.Icons.Pencil, "Modifica Gruppo"));
            this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/{0}/default.aspx".FormatByParameters(GroupsApplication.ApplicationSystemName), NetCms.GUI.Icons.Icons.Chart_Organisation, "Elenco Gruppi"));
            this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/{0}/group_users_list.aspx?gid=".FormatByParameters(GroupsApplication.ApplicationSystemName) + this.Group.ID, NetCms.GUI.Icons.Icons.Pencil, "Elenco Utenti del Gruppo"));
            if (this.Group.Depth != 0 && !this.Group.IsSystem)
                this.Toolbar.Buttons.Add(new ToolbarButton("javascript: setFormValueConfirm('" + this.Group.Tree + "','DeleteGroupField',1,'Sei sicuro di voler eliminare questo gruppo?')", NetCms.GUI.Icons.Icons.Delete, "Elimina"));
        }

        public DetailPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            if(!Grants.Grant)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

            SetToolbars();
        }

        public override WebControl Control
        {
            get
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                value.Controls.Add(getView());
                return value;
            }
        }

        public HtmlGenericControl getView()
        {
            HtmlGenericControl output = new HtmlGenericControl("div");
            HtmlGenericControl DeleteGroupField = new HtmlGenericControl("div");
            output.Controls.Add(DeleteGroupField);

            if (this.Group.Depth != 0)
                DeleteGroupField.InnerHtml += "<input type=\"hidden\" name=\"DeleteGroupField\" id=\"DeleteGroupField\" />";
            //log
            //string Utente=  Account.NomeCompleto + " (" + Account.UserName + ")";

            #region Delete Group Postback

            if (IsPostBack && !this.Group.IsSystem)
            {
                NetUtility.RequestVariable DeleteFieldRequest = new NetUtility.RequestVariable("DeleteGroupField");
                if (DeleteFieldRequest.IsValidString && PageData.CurrentReference.SubmitSource == DeleteFieldRequest.Key)
                {
                    ICollection<Group> groupsToDelete = GroupsBusinessLogic.FindGroupsByTreeStart(DeleteFieldRequest.StringValue);                        
                    foreach (Group group in groupsToDelete)
                    {
                        GroupsBusinessLogic.DeleteGroup(group);
                    }
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha eliminato il Gruppo '" + Group.Name + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    NetCms.GUI.PopupBox.AddSessionMessage("Gruppo Eliminato", PostBackMessagesType.Success, true);                        
                }
            }
            else
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina dei dettagli del Gruppo '" + Group.Name + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            }

            #endregion

            //string logText = "L'utente " + Utente + " vuole visualizzare la scheda del gruppo  "+Group.Name;
            //GuiLog.SaveLogRecord(logText, false);

            NetCms.GUI.DetailsSheet details = new DetailsSheet("Dettagli gruppo " + this.Group.Name + ".", "Informazioni principali del gruppo");
            output.Controls.Add(details);
            details.AddRow(new DetailsSheetRow("Nome", this.Group.Name));
            details.AddRow(new DetailsSheetRow("Gruppo di Sistema", this.Group.IsSystem ? "Si" : "No"));
            //if (this.Group.IsSystem)
            //details.AddRow(new DetailsSheetRow("Nome di Sistema", this.Group.SystemName));
            if (this.Group.MacroGroup != null)
                details.AddRow(new DetailsSheetRow("Macro Gruppo", this.Group.MacroGroup.Label));
            if (this.Group.Parent != null)
                details.AddRow(new DetailsSheetRow("Figlio di", this.Group.Parent.Name));
            details.AddRow(new DetailsSheetRow("Numero di utenti appartenenti", this.Group.Users.Count(x=> !x.Deleted).ToString()));

            output.Controls.Add(Permessi());
            output.Controls.Add(Administrators());
            output.Controls.Add(Publishers());

            return output;

        }

        public WebControl Permessi()
        {
            WebControl container = new WebControl(HtmlTextWriterTag.Div);

            G2Core.UI.DetailsSheet extraNet = new G2Core.UI.DetailsSheet("Permessi Generici", "Riepilogo dei permessi generici impostati per il gruppo");
            string buttonGeneric = "<a href=\"" + NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/grants/review.aspx?pid=" + this.Group.Profile.ID + "&nid=0&show=outside\" class=\"action key\"><span>Riepiloga Permessi Applicazioni non di Portale</span><a>";
            extraNet.AddRow(buttonGeneric);
            container.Controls.Add(extraNet);


            G2Core.UI.DetailsSheet nets = new G2Core.UI.DetailsSheet("Permessi applicazioni di portale", "Riepilogo dei permessi per le applicazioni di portale impostati per il gruppo");
            GrantElement grants = new GrantElement("users", Account.Profile);

            nets.AddColumn("Portale", G2Core.UI.DetailsSheet.Colors.Default);
            nets.AddColumn("Riepilogo dei Permessi CMS", G2Core.UI.DetailsSheet.Colors.Default);
            nets.AddColumn("Riepilogo dei Permessi Applicazioni", G2Core.UI.DetailsSheet.Colors.Default);
                        

            bool showGrantsButtons = grants["users_grants"].Value == GrantsValues.Allow;

            foreach (var network in NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks)
            {
                string button = "&nbsp;";
                string button1 = "&nbsp;";
                if (network.Value.Enabled)
                {
                    if (network.Value.CmsStatus == NetCms.Networks.NetworkCmsStates.Enabled)
                        if (showGrantsButtons)
                        {
                            button = "<a href=\"" + NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/" + network.Value.SystemName + "/cms/grants/grants_view.aspx?pid=" + this.Group.Profile.ID + "\" class=\"action key\"><span>Riepiloga Permessi</span><a>";
                        }
                        else
                            button = "<a class=\"action stop\"><span>Cms Disabilitato</span><a>";
                    button1 = "<a href=\"" + NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/grants/review.aspx?pid=" + this.Group.Profile.ID + "&nid=" + network.Value.ID + "\" class=\"action key\"><span>Riepiloga Permessi</span><a>";

                }
                else
                {
                    button = "<a class=\"action stop\"><span>Servizi Disabilitati</span><a>";
                    button1 = "<a class=\"action stop\"><span>Servizi Disabilitati</span><a>";
                }
                nets.AddRow(network.Value.Label, button, button1);
            }

            container.Controls.Add(nets);

            return container;
        }
        public WebControl Administrators()
        {
            WebControl container = new WebControl(HtmlTextWriterTag.Div);
            WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            fieldset.CssClass = "Details";

            container.Controls.Add(fieldset);
            fieldset.Controls.Add(legend);
            legend.Controls.Add(new LiteralControl("Elenco utenti amministratori del gruppo."));

            HtmlGenericControl text = new HtmlGenericControl("div");
            text.Attributes["class"] = "DetailsText";
            fieldset.Controls.Add(text);
            text.InnerHtml = "Elenco degli utenti che fanno parte del gruppo ed impostati come Amministratore o Super Amministratore (gli utenti rappresentati sono solo quelli che appartengono direttamente al gruppo, sono quindi esclusi eventuali utenti facenti parte di gruppi genitori o figli di questo gruppo).";

            HtmlGenericControl scheda = new HtmlGenericControl("div");
            fieldset.Controls.Add(scheda);

            //SmDisconnectedTable table = new SmDisconnectedTable(PageData.CurrentReference.PageIndex, UsersTable.Select(" Type_UserGroup <> 0"));
            //table.CssClass = "";

            //SmTableLinkColumn nome = new SmTableLinkColumn("id_User", "Name_User", "", "Nome", Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/users/user_details.aspx?id={0}", "Vai alla Scheda dell'utente");
            //nome.ColumnCssClass = "DetailsLabel";
            //table.addColumn(nome);

            //SmTableMultiOptionColumn role = new SmTableMultiOptionColumn("Type_UserGroup", "Ruolo Amministrazione Utenti", "Utente,Amministratore,Super Amministratore");
            //table.addColumn(role);

            //SmTableMultiOptionColumn crole = new SmTableMultiOptionColumn("PublishRole_UserGroup", "Ruolo Amministrazione Contenuti", "Redattore,Revisore,Pubblicatore");
            //table.addColumn(crole);

            //scheda.Controls.Add(table.getTable());

            var usersToShow = (from user in UsersAdministratorTable
                               //where user.AssociatedGroups.Where(x => x.Type != 0).Count() > 0
                               select new
                               {
                                   id_User = user.ID,
                                   Name_User = user.UserName,
                                   Type_UserGroup = user.AssociatedGroups.Where(x => x.Type!= 0).First().Type,
                                   PublishRole_UserGroup = user.AssociatedGroups.Where(x => x.Type!= 0).First().PublishRole
                               });

            ArTable table = new ArTable();
            table.EnablePagination = true;
            table.RecordPerPagina = 20;
            table.InnerTableCssClass = "tab";
            table.NoRecordMsg = "Nessun amministratore del gruppo";

            PaginationHandler pagination = new PaginationHandler(20, usersToShow.Count(), table.PaginationKey);
            table.PagesControl = pagination;

            table.Records = usersToShow;

            ArTextColumn nome = new ArTextColumn("Nome", "Name_User");
            table.AddColumn(nome);

            ArOptionsColumn role = new ArOptionsColumn("Ruolo Amministrazione Utenti", "Type_UserGroup", new string[] { "Utente","Amministratore","Super Amministratore" });
            table.AddColumn(role);

            ArOptionsColumn crole = new ArOptionsColumn("Ruolo Amministrazione Contenuti", "PublishRole_UserGroup", new string[] { "Redattore","Revisore","Pubblicatore" });
            table.AddColumn(crole);

            ArActionColumn dettagli = new ArActionColumn("Vai alla Scheda dell'utente", ArActionColumn.Icons.Details, Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/users/user_details.aspx?id={id_User}");
            table.AddColumn(dettagli);

            scheda.Controls.Add(table);

            return container;
        }
        public WebControl Publishers()
        {
            WebControl container = new WebControl(HtmlTextWriterTag.Div);
            WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            fieldset.CssClass = "Details";

            container.Controls.Add(fieldset);
            fieldset.Controls.Add(legend);
            legend.Controls.Add(new LiteralControl("Elenco utenti Revisori e Pubblicatori del gruppo."));

            HtmlGenericControl text = new HtmlGenericControl("div");
            text.Attributes["class"] = "DetailsText";
            fieldset.Controls.Add(text);
            text.InnerHtml = "Elenco degli utenti che fanno parte del gruppo ed impostati come Pubblicatore o Revisore (gli utenti rappresentati sono solo quelli che appartengono direttamente al gruppo, sono quindi esclusi eventuali utenti facenti parte di gruppi genitori o figli di questo gruppo).";


            HtmlGenericControl scheda = new HtmlGenericControl("div");
            fieldset.Controls.Add(scheda);

            var usersToShow = (from user in UsersPublisherTable
                               //where user.AssociatedGroups.Where(x => x.PublishRole != 0).Count() > 0
                               select new
                               {
                                   id_User = user.ID,
                                   Name_User = user.UserName,
                                   Type_UserGroup = user.AssociatedGroups.Where(x => x.PublishRole != 0).First().Type,
                                   PublishRole_UserGroup = user.AssociatedGroups.Where(x => x.PublishRole != 0).First().PublishRole
                               });

            ArTable table = new ArTable();
            table.EnablePagination = true;
            table.RecordPerPagina = 20;
            table.InnerTableCssClass = "tab";
            table.NoRecordMsg = "Revisori e/o Pubblicatori del gruppo non trovati";

            PaginationHandler pagination = new PaginationHandler(20, usersToShow.Count(), table.PaginationKey);
            table.PagesControl = pagination;

            table.Records = usersToShow;
            //SmDisconnectedTable table = new SmDisconnectedTable(PageData.CurrentReference.PageIndex, UsersTable.Select("PublishRole_UserGroup <> 0"));
            //table.CssClass = "";

            //SmTableLinkColumn nome = new SmTableLinkColumn("id_User", "Name_User", "", "Nome", Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/users/user_details.aspx?id={0}", "Vai alla Scheda dell'utente");
            //nome.ColumnCssClass = "DetailsLabel";
            //table.addColumn(nome);

            //SmTableMultiOptionColumn crole = new SmTableMultiOptionColumn("PublishRole_UserGroup", "Ruolo Amministrazione Contenuti", "Redattore,Revisore,Pubblicatore");
            //table.addColumn(crole);

            //SmTableMultiOptionColumn role = new SmTableMultiOptionColumn("Type_UserGroup", "Ruolo Amministrazione Utenti", "Utente,Amministratore,Super Amministratore");
            //table.addColumn(role);

            ArTextColumn nome = new ArTextColumn("Nome", "Name_User");
            table.AddColumn(nome);

            ArOptionsColumn crole = new ArOptionsColumn("Ruolo Amministrazione Contenuti", "PublishRole_UserGroup", new string[] { "Redattore","Revisore","Pubblicatore" });
            table.AddColumn(crole);
            
            ArOptionsColumn role = new ArOptionsColumn("Ruolo Amministrazione Utenti", "Type_UserGroup", new string[] { "Utente","Amministratore","Super Amministratore" });
            table.AddColumn(role);

            ArActionColumn dettagli = new ArActionColumn("Vai alla Scheda dell'utente", ArActionColumn.Icons.Details, Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/users/user_details.aspx?id={id_User}");
            table.AddColumn(dettagli);

            scheda.Controls.Add(table);

            return container;
        }
    }
}
