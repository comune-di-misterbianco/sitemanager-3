﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;

namespace NetCms.Users.GroupsManager
{
    [DynamicUrl.PageControl("default.aspx", ApplicationID = GroupsApplication.ApplicationID)]
    public class MainPage : AbstractPage
    {
        public override string PageTitle
        {
            get { return "Gruppi"; }
        }
        
        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Chart_Organisation; }
        }
        
        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/{0}/group_new.aspx".FormatByParameters(GroupsApplication.ApplicationSystemName), NetCms.GUI.Icons.Icons.Chart_Organisation_Add, "Crea Gruppo"));
            this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/{0}/macro.aspx".FormatByParameters(GroupsApplication.ApplicationSystemName), NetCms.GUI.Icons.Icons.Chart_Organisation, "Gestione Macro Gruppi"));
        }
        
        public MainPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            if(!Grants.Grant)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

            SetToolbars();
        }

        public override WebControl Control
        {
            get
            {
                return new GroupsListControl(Conn, Account);
            }
        }
    }
}
