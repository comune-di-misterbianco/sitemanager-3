﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetTable;
using System.Collections.Generic;
using NetCms.Users.Search;
using System.Linq;
using NetService.Utility.ArTable;
using NetCms.Vertical.Grants;
using NetService.Utility.Controls;

namespace NetCms.Users.GroupsManager
{
    [DynamicUrl.PageControl("group_users_list.aspx", ApplicationID = GroupsApplication.ApplicationID)]
    public class UsersPage : AbstractPage
    {        
        public override string PageTitle
        {
            get { return base.PageTitle + "Utenti appartenenti al gruppo '" + Group.Name + "'"; }
        }

        //public DataTable UsersTable
        //{
        //    get
        //    {
        //        if (_UsersTable == null)
        //        {
        //            Search.UserRelated UsersDataManager = new Search.UserRelated(NetCms.Users.AccountManager.CurrentAccount, this.Connection);
        //            _UsersTable = UsersDataManager.UsersIAdmin("", " id_Group = " + this.Group.ID, "Type_UserGroup, PublishRole_UserGroup");
        //        }
        //        return _UsersTable;
        //    }
        //}
        //private DataTable _UsersTable;

        //public ICollection<User> UsersTable
        //{
        //    get
        //    {
        //        if (_UsersTable == null)
        //        {
        //            Search.UserRelated UsersDataManager = new Search.UserRelated(NetCms.Users.AccountManager.CurrentAccount);
        //            SearchCondition idGroupCondition = new SearchCondition("ID",this.Group.ID, SearchCondition.ComparisonType.Equals, SearchCondition.CriteriaType.Group);
        //            _UsersTable = UsersDataManager.UsersIAdmin(0, UsersDataManager.UsersIAdminCount(null, new SearchCondition[] { idGroupCondition }), null, new SearchCondition[] { idGroupCondition });

        //        }
        //        return _UsersTable;
        //    }
        //}
        //private ICollection<User> _UsersTable;

        public ICollection<User> UsersTable
        {
            get
            {
                if (_UsersTable == null)
                {
                    Search.UserRelated UsersDataManager = new Search.UserRelated(NetCms.Users.AccountManager.CurrentAccount);
                    SearchCondition idGroupCondition = new SearchCondition("ID",this.Group.ID, SearchCondition.ComparisonType.Equals, SearchCondition.CriteriaType.Group);
                    _UsersTable = UsersDataManager.UsersIAdmin((UsersSearchControl.ButtonRequest.IsPostBack && UsersSearchControl.ButtonRequest.IsValidString) ? 0 : Pagination.CurrentPage - 1, RecordPerPage, null, new SearchCondition[] { idGroupCondition });
                                        
                }
                return _UsersTable;
            }
        }
        private ICollection<User> _UsersTable;

        public int UsersTableCount
        {
            get
            {
                Search.UserRelated UsersDataManager = new Search.UserRelated(NetCms.Users.AccountManager.CurrentAccount);
                SearchCondition idGroupCondition = new SearchCondition("ID", this.Group.ID, SearchCondition.ComparisonType.Equals, SearchCondition.CriteriaType.Group);
                _UsersTableCount = UsersDataManager.UsersIAdminCount(null, new SearchCondition[] { idGroupCondition });
                return _UsersTableCount;
            }
        }
        private int _UsersTableCount;

        private int RecordPerPage = 10;

        public ArTable SearchResultTable
        {
            get
            {
                if (_SearchResultTable == null)
                {
                    _SearchResultTable = new ArTable();
                    _SearchResultTable.EnablePagination = true;
                    _SearchResultTable.RecordPerPagina = RecordPerPage;
                    _SearchResultTable.NoSearchParameter = true;
                    _SearchResultTable.InnerTableCssClass = "tab";
                    _SearchResultTable.GoToFirstPageOnSearch = true;
                }
                return _SearchResultTable;
            }
        }
        private ArTable _SearchResultTable;

        public PaginationHandler Pagination
        {
            get
            {
                if (_Pagination == null)
                {

                    _Pagination = new PaginationHandler(RecordPerPage, TotalRecordCount(), SearchResultTable.PaginationKey, (UsersSearchControl.ButtonRequest.IsPostBack && UsersSearchControl.ButtonRequest.IsValidString));
                }
                return _Pagination;
            }
        }
        private PaginationHandler _Pagination;

        private int TotalRecordCount()
        {
            int count = 0;
            switch (UsersSearchControl.SearchStatus)
            {
                case UsersSearchControl.SearchStatuses.SearchAvailable: count = this.UsersSearchControl.GetSearchResultCount(this.Account); break;
                case UsersSearchControl.SearchStatuses.NoSearch: count = UsersTableCount; break;
                case UsersSearchControl.SearchStatuses.WrongData: break;
            }
            return count;
        }

       
        public Search.UsersSearchControl UsersSearchControl
        {
            get
            {
                if (_UsersSearchControl == null)
                {
                    SearchCondition idGroupCondition = new SearchCondition("ID", this.Group.ID, SearchCondition.ComparisonType.Equals, SearchCondition.CriteriaType.Group);
                    _UsersSearchControl = new Search.UsersSearchControl(null, new SearchCondition[] { idGroupCondition });
                }
                return _UsersSearchControl;
            }
        }
        private Search.UsersSearchControl _UsersSearchControl;

        private Group _Group;
        private Group Group
        {
            get
            {
                if (_Group == null)
                {
                    G2Core.Common.RequestVariable req = new G2Core.Common.RequestVariable("gid", G2Core.Common.RequestVariable.RequestType.QueryString);
                    if (req.IsValidInteger)
                        try
                        {
                            _Group = NetCms.Users.GroupsHandler.RootGroup.FindGroup(req.IntValue);
                        }
                        catch (Exception e)
                        {
                            PopupBox.AddSessionMessage("Il gruppo non esiste", PostBackMessagesType.Error, this.Account.History.GetBackURL());
                            //Trace
                        }

                }
                if (_Group == null)
                    PopupBox.AddSessionMessage("Il gruppo non esiste", PostBackMessagesType.Error, this.Account.History.GetBackURL());

                return _Group;
            }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Chart_Organisation; }
        }
        
        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/{0}/group_details.aspx?id=".FormatByParameters(GroupsApplication.ApplicationSystemName) + this.Group.ID, NetCms.GUI.Icons.Icons.Arrow_Left, "Indietro"));
            //NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/{0}/group_mod.aspx?gid=".FormatByParameters(GroupsApplication.ApplicationSystemName)
        }

        public UsersPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            if(!Grants.Grant)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

            SetToolbars();
        }

        public override WebControl Control
        {
            get
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);

                value.Controls.Add(UpdateUserControl);
                value.Controls.Add(UpdateAllUsersControl);

                if (this.UsersSearchControl.SearchStatus == Search.UsersSearchControl.SearchStatuses.WrongData)
                    PopupBox.AddMessage("I dati di ricerca immessi non sono validi, assicurarsi di aver riempito almeno un campo e di aver inserito dati corretti.", PostBackMessagesType.Notify);

                G2Core.XhtmlControls.InputField updateUser = new G2Core.XhtmlControls.InputField("updateUser");
                G2Core.XhtmlControls.InputField updateUsers = new G2Core.XhtmlControls.InputField("updateAllUsers");

                if (!updateUsers.RequestVariable.IsValidInteger && !updateUser.RequestVariable.IsValidInteger)
                {
                    G2Core.Common.RequestVariable req = new G2Core.Common.RequestVariable("gid", G2Core.Common.RequestVariable.RequestType.QueryString);
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta visualizzando la lista di utenti appartenenti al gruppo con id=" + req, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                }
                NetCms.GUI.Collapsable Legenda = new Collapsable("Informazioni su questa pagina");
                Legenda.AppendControl(new LiteralControl(@"<fieldset><legend>Legenda</legend>
<br />
<p>
    <strong>Amministrazione Permessi</strong>
    <ul style=""list-style-type: none;"">
        <li>U = Utente</li>
        <li>A = Amministratore</li>
        <li>S = Super-Amministratore</li>
    </ul>
</p>
<br />
<p>
    <strong>Amministrazione Contenuti</strong>
    <ul style=""list-style-type: none;"">
        <li>W = Redattore</li>
        <li>R = Revisore</li>
        <li>P = Pubblicatore</li>
    </ul>
</p>
<br />
<p>Nella tabella, per ogni utente, sono mostrate in grassetto le lettere associate all'impostazione attuale dei ruoli.</p></fieldset>
<br />
<br />
"));
                value.Controls.Add(Legenda);

                NetCms.GUI.Collapsable collapsableSearch = new Collapsable("Ricerca");

                
                collapsableSearch.StartCollapsed = UsersSearchControl.SearchStatus == Search.UsersSearchControl.SearchStatuses.NoSearch;
                collapsableSearch.AppendControl(UsersSearchControl);

                value.Controls.Add(collapsableSearch);

                NetCms.GUI.Collapsable collapsableTable = new Collapsable("Elenco degli Utenti");
                HtmlGenericControl output = new HtmlGenericControl("div");

                collapsableTable.AppendControl(output);
                value.Controls.Add(collapsableTable);

                GrantElement grants = new GrantElement("users", Account.Profile);

                ICollection<UserSearchRowTemplate> datatable = new List<UserSearchRowTemplate>();

                switch (UsersSearchControl.SearchStatus)
                {
                    case UsersSearchControl.SearchStatuses.SearchAvailable: datatable = this.UsersSearchControl.GetSearchResult((UsersSearchControl.ButtonRequest.IsPostBack && UsersSearchControl.ButtonRequest.IsValidString) ? 0 : Pagination.CurrentPage - 1, RecordPerPage,this.Account); break;
                    case UsersSearchControl.SearchStatuses.NoSearch:
                        foreach (User user in UsersTable)
                        {
                            try
                            {
                                if (datatable.Where(x => x.id_User == user.ID).Count() == 0)
                                {
                                    datatable.Add(new UserSearchRowTemplate()
                                    {
                                        id_User = user.ID,
                                        Name_User = user.UserName,
                                        State_User = user.State,
                                        AnagraficaType_User = user.AnagraficaType,
                                        Anagrafica_User = user.AnagraficaID,
                                        AnagraficaNome = user.NomeCompleto,
                                        Profile_User = user.Profile.ID
                                    });
                                }
                            }
                            catch (Exception ex)
                            {
                                //trace
                            }
                        }
                        break;
                    case UsersSearchControl.SearchStatuses.WrongData: break;
                }

                SearchResultTable.PagesControl = Pagination;


                if (datatable.Count() > 0)
                {

                    SearchResultTable.Records = datatable;
                    string[] CustomSearchFieldsQS = UsersSearchControl.WorkerFinder.Finder.CustomSearchFields.Select(x => x.QueryString).ToArray();
                    CustomSearchFieldsQS = CustomSearchFieldsQS.Union(new string[] { ("gid=" + this.Group.ID) }).ToArray();
                    SearchResultTable.CustomSearchArray = CustomSearchFieldsQS;

                    ArTextColumn colName = new ArTextColumn("User Name", "Name_User");
                    SearchResultTable.AddColumn(colName);

                    ArTextColumn colAnagraficaName = new ArTextColumn("Nome", "AnagraficaNome");
                    SearchResultTable.AddColumn(colAnagraficaName);

                    //ArActionColumn colDettagli = new ArActionColumn("Scheda e Opzioni", ArActionColumn.Icons.Details, "user_details.aspx?id={id_User}");
                    ArActionColumn colDettagli = new ArActionColumn("Scheda e Opzioni", ArActionColumn.Icons.Details, Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/users/user_details.aspx?id={id_User}");
                    SearchResultTable.AddColumn(colDettagli);

                    ArOptionsColumn colStato = new ArOptionsColumn("Stato", "State_User", new string[] { "Disabilitato", "Abilitato", "Mai Attivato" });
                    SearchResultTable.AddColumn(colStato);

                    //ICollection<GroupAssociation> groupsAssociated = this.UsersTable.Where(x => x.AssociatedGroups.Where(g => g.GroupID == this.Group.ID).Count() > 0).Select(x => x.AssociatedGroups.Where(g => g.GroupID == this.Group.ID).First()).ToList();
                    SearchResultTable.AddColumn(new AdminRoleColumn(this.Group.ID));
                    SearchResultTable.AddColumn(new ContentAdminColumn(this.Group.ID));

                    ArActionColumn colAssociationUpdate = new ArActionColumn("Aggiorna Associazione", ArActionColumn.Icons.Custom, "javascript: setFormValue({id_User},'updateUser',1)");
                    colAssociationUpdate.CssClass = "action modify";
                    SearchResultTable.AddColumn(colAssociationUpdate);
                                       
                    HtmlInputHidden setUserToAdd = new HtmlInputHidden();
                    setUserToAdd.ID = "SelectedUser";
                    output.Controls.Add(setUserToAdd);
                    output.Controls.Add(SearchResultTable);

                    string js = @"<script type=""text/javascript"">
                function setAllRadios(classe)
                {
                    $('.'+classe).attr('checked','checked');
                }
</script>
";

                    DetailsSheet sheet = new DetailsSheet("Operazioni di massa sugli utenti in elenco", "Questa tabella contiene una serie di collegamenti che permettono di impostare automaticamente tutti gli utenti nell'elenco soprastante al valore espresso dal bottone stesso. Dopo aver impostato un valore sarà necessario confermarlo premendo su 'Aggiorna Tutti'" + js);
                    sheet.AddRow(new DetailsSheetRow("", "<a href=\"javascript: setAllRadios('radioU');\">Imposta Tutti come 'Utenti'</a>"));
                    sheet.AddRow(new DetailsSheetRow("", "<a href=\"javascript: setAllRadios('radioA');\">Imposta Tutti come 'Amministratori'</a>"));
                    sheet.AddRow(new DetailsSheetRow("", "<a href=\"javascript: setAllRadios('radioS');\">Imposta Tutti come 'Super Amministratori'</a>"));
                    sheet.AddRow(new DetailsSheetRow("", "<a href=\"javascript: setAllRadios('radioW');\">Imposta Tutti come 'Redattori'</a>"));
                    sheet.AddRow(new DetailsSheetRow("", "<a href=\"javascript: setAllRadios('radioR');\">Imposta Tutti come 'Revisori'</a>"));
                    sheet.AddRow(new DetailsSheetRow("", "<a href=\"javascript: setAllRadios('radioP');\">Imposta Tutti come 'Pubblicatori'</a>"));
                    sheet.AddRow(new DetailsSheetRow("", "<a href=\"javascript: setFormValue(1,'updateAllUsers',1);\">Aggiorna Tutti</a>"));
                    value.Controls.Add(sheet);  
                }
                return value;
            }
        }


        private HtmlGenericControl UpdateUserControl
        {
            get
            {
                G2Core.XhtmlControls.InputField updateUser = new G2Core.XhtmlControls.InputField("updateUser");
                if (updateUser.RequestVariable.IsValidInteger)
                {
                    G2Core.Common.RequestVariable admin = new G2Core.Common.RequestVariable("admin" + updateUser.RequestVariable.StringValue);
                    G2Core.Common.RequestVariable content = new G2Core.Common.RequestVariable("content" + updateUser.RequestVariable.StringValue);

                    GroupAssociation ga = GroupsBusinessLogic.GetGroupAssociationByUserAndGroup(updateUser.RequestVariable.IntValue,this.Group.ID);
                    ga.Type = admin.IntValue;
                    ga.PublishRole = content.IntValue;
                    GroupsBusinessLogic.SaveOrUpdateGroupAssociation(ga);

                    NetCms.GUI.PopupBox.AddMessage("Aggiornamento effettuato con successo");
                    G2Core.Common.RequestVariable req = new G2Core.Common.RequestVariable("gid", G2Core.Common.RequestVariable.RequestType.QueryString);
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha aggiornato la lista di utenti appartenenti al gruppo con id=" + req, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                }
                return updateUser.Control;
            }
        }

        private HtmlGenericControl UpdateAllUsersControl
        {
            get
            {
                G2Core.XhtmlControls.InputField updateUsers = new G2Core.XhtmlControls.InputField("updateAllUsers");
                if (updateUsers.RequestVariable.IsValidInteger)
                {
                    G2Core.Common.RequestVariable records = new G2Core.Common.RequestVariable("records");
                    if (records.IsValidString)
                    {
                        char[] splitters = { ',' };
                        string[] IDs = records.StringValue.Split(splitters, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string uid in IDs)
                        {
                            G2Core.Common.RequestVariable admin = new G2Core.Common.RequestVariable("admin" + uid);
                            G2Core.Common.RequestVariable content = new G2Core.Common.RequestVariable("content" + uid);

                            GroupAssociation ga = GroupsBusinessLogic.GetGroupAssociationByUserAndGroup(int.Parse(uid), this.Group.ID);
                            
                            ga.Type = admin.IntValue;
                            ga.PublishRole = content.IntValue;
                            GroupsBusinessLogic.SaveOrUpdateGroupAssociation(ga);

                        }
                        NetCms.GUI.PopupBox.AddMessage("Aggiornamento effettuato con successo");
                        G2Core.Common.RequestVariable req = new G2Core.Common.RequestVariable("gid", G2Core.Common.RequestVariable.RequestType.QueryString);
                        string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha aggiornato la lista di utenti appartenenti al gruppo con id=" + req, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                    }
                }
                return updateUsers.Control;
            }
        }
    }
}
