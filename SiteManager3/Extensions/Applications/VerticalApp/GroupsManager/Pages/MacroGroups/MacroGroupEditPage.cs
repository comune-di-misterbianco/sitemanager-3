﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetTable;
using NetService.Utility.ValidatedFields;

namespace NetCms.Users.GroupsManager
{
    [DynamicUrl.PageControl("macro_edit.aspx", ApplicationID = GroupsApplication.ApplicationID)]
    public class EditMacroGroupPage : AbstractPage
    {
        public G2Core.Common.RequestVariable Request
        {
            get
            {
                if (_Request == null)
                    _Request = new G2Core.Common.RequestVariable("mid", G2Core.Common.RequestVariable.RequestType.QueryString);
                return _Request;
            }
        }
        private G2Core.Common.RequestVariable _Request;

        public override string PageTitle
        {
            get { return "Gestione Macro Gruppi &raquo; Modifica Macro Gruppo"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Chart_Organisation; }
        }
        
        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/{0}/macro.aspx".FormatByParameters(GroupsApplication.ApplicationSystemName), NetCms.GUI.Icons.Icons.Chart_Organisation_Add, "Elenco Macro Gruppi"));
        }

        public EditMacroGroupPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            if(!Grants.Grant)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

            SetToolbars();
        }

        public override WebControl Control
        {
            get
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                value.Controls.Add(getView());
                return value;
            }
        }

        public VfManager FormModifica
        {
            get
            {
                if (_FormModifica == null)
                {
                    _FormModifica = new VfManager("modifyMacroGroupForm", IsPostBack);
                    _FormModifica.CssClass = "Axf";
                    //_FormModifica.TitleControl.Controls.Add(new LiteralControl("Modifica Macro Gruppo"));
                    _FormModifica.AddSubmitButton = true;
                    _FormModifica.SubmitButton.Text = "Invia";
                    _FormModifica.SubmitButton.Click += new EventHandler(SubmitButton_Click);
                    _FormModifica.SubmitButton.CssClass = "Axf_Control Axf_SubmitButtons";
                    _FormModifica.MandatoryControl.CssClass = "Axf_Control";

                    VfTextBox nome = new VfTextBox("nomeMacroGroup", "Nome", "Label", InputTypes.Text);
                    nome.CustomCssClass = "Axf_Control";
                    nome.Required = true;
                    _FormModifica.Fields.Add(nome);
                }
                return _FormModifica;
            }
        }

        public MacroGroup MacroGruppo
        {
            get 
            {
                if (_MacroGruppo == null)
                {
                    if (Request.IsValidInteger)
                        _MacroGruppo = GroupsBusinessLogic.GetMacroGroupById(Request.IntValue);
                }
                return _MacroGruppo;
            }
        }
        private MacroGroup _MacroGruppo;

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (FormModifica.IsValid == VfGeneric.ValidationStates.Valid)
            {
                if (GroupsBusinessLogic.ModifyMacroGroup(FormModifica, MacroGruppo) != null)
                {
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha aggiornato con successo il Macro Gruppo.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    NetCms.GUI.PopupBox.AddSessionMessage("Macro Gruppo aggiornato con successo", PostBackMessagesType.Success, true);
                }
                else
                    NetCms.GUI.PopupBox.AddSessionMessage("Errore durante la modifica del Macro Gruppo", PostBackMessagesType.Error, false);
            }
        }
        private VfManager _FormModifica;

        public HtmlGenericControl getView()
        {
            if (!IsPostBack)
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di modifica del Macro Gruppo.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            }

            HtmlGenericControl output = new HtmlGenericControl("div");

            if (MacroGruppo != null)
            {
                WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
                fieldset.ID = "fieldsetModify";
                WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
                legend.Controls.Add(new LiteralControl("Modifica Macro Gruppo"));
                legend.Attributes["for"] = "fieldsetModify";
                fieldset.Controls.Add(legend);

                FormModifica.DataBind(MacroGruppo);
                fieldset.Controls.Add(FormModifica);

                output.Controls.Add(fieldset);
            }
            else
                NetCms.GUI.PopupBox.AddSessionMessage("Errore nell'accesso ai dati del MacroGruppo, impossibile effettuare la modifica!", PostBackMessagesType.Error,true);

            return output;
        }
    }
}
