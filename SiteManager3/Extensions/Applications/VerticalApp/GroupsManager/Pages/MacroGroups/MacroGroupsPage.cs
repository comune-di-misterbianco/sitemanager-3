﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetTable;
using System.Collections.Generic;
using NetService.Utility.ArTable;

namespace NetCms.Users.GroupsManager
{
    [DynamicUrl.PageControl("macro.aspx", ApplicationID = GroupsApplication.ApplicationID)]
    public class MacroGroupsPage : AbstractPage
    {
        public override string PageTitle
        {
            get { return base.PageTitle + "Gestione Macro Gruppi"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Chart_Organisation; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/{0}/macro_add.aspx".FormatByParameters(GroupsApplication.ApplicationSystemName), NetCms.GUI.Icons.Icons.Chart_Organisation_Add, "Crea Macro Gruppo"));
        }

        public MacroGroupsPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            if (!Grants.Grant)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

            SetToolbars();
        }

        public override WebControl Control
        {
            get
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                value.Controls.Add(getView());
                return value;
            }
        }

        public HtmlGenericControl getView()
        {
            HtmlGenericControl output = new HtmlGenericControl("div");
            if (Grants.Grant)
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di gestione dei Macro Gruppi del CMS.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                #region Delete MacroGroup Postback
                /*
                NetUtility.RequestVariable DeleteFieldRequest = new NetUtility.RequestVariable("del");
                if (DeleteFieldRequest.IsValidInteger && this.IsPostBack)
                {
                    this.Connection.Execute("UPDATE groups SET MacroGroup_Group = 0 WHERE MacroGroup_Group = " + DeleteFieldRequest.IntValue);
                    this.Connection.Execute("DELETE FROM groups_macrogroups WHERE id_MacroGroup =" + DeleteFieldRequest.IntValue);

                    NetCms.GUI.PopupBox.AddMessage("Il i Macro Gruppo è stato eliminato con successo.", NetCms.GUI.PostBackMessagesType.Success);
                }
                */
                #endregion

                ICollection<MacroGroup> macroGruppi = GroupsBusinessLogic.FindAllMacroGroups();


                ArTable table = new ArTable(macroGruppi);
                table.InnerTableCssClass = "GenericTable";
                table.EnablePagination = false;

                #region Campi della tabella



                ArTextColumn labelCol = new ArTextColumn("Macro Gruppo", "Label");
                table.AddColumn(labelCol);

                ArTextColumn systemCol = new ArTextColumn("Nome di Sistema", "Key");
                table.AddColumn(systemCol);

                ArActionColumn actionCol = new ArActionColumn("Modifica", ArActionColumn.Icons.Edit, "macro_edit.aspx?mid={ID}");
                table.AddColumn(actionCol);
                /*
                col = new AxtActionColumn("id_MacroGroup", "Elimina","action delete",  "javascript: setdel({0})");
                table.Columns.Add(col);*/
                #endregion

                output.Controls.Add(table);
            }
            else
                PopupBox.AddNoGrantsMessageAndGoBack();

            return output;
        }
    }
}
