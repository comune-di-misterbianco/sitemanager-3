﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetService.Utility.ValidatedFields;

namespace NetCms.Users.GroupsManager
{
    [DynamicUrl.PageControl("macro_add.aspx", ApplicationID = GroupsApplication.ApplicationID)]
    public class MacroGroupAddPage : AbstractPage
    {        
        public override string PageTitle
        {
            get { return base.PageTitle + "Gestione Macro Gruppi &raquo; Creazione Nuovo Macro Gruppo"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Chart_Organisation; }
        }
        
        private void SetToolbars()
        {
            //NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/{0}/group_mod.aspx?gid=".FormatByParameters(GroupsApplication.ApplicationSystemName)
        }

        public MacroGroupAddPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            if(!Grants.Grant)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

            SetToolbars();
        }

        public override WebControl Control
        {
            get
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                value.Controls.Add(getView());
                return value;
            }
        }

        public VfManager FormInserimento
        {
            get 
            {
                if (_FormInserimento == null)
                {
                    _FormInserimento = new VfManager("addMacroGroupForm", IsPostBack);
                    _FormInserimento.CssClass = "Axf";
                    //_FormInserimento.TitleControl.Controls.Add(new LiteralControl("Creazione Macro Gruppo"));
                    _FormInserimento.AddSubmitButton = true;
                    _FormInserimento.SubmitButton.Text = "Invia";
                    _FormInserimento.SubmitButton.CssClass = "Axf_Control Axf_SubmitButtons";
                    _FormInserimento.SubmitButton.Click += new EventHandler(SubmitButton_Click);
                    _FormInserimento.MandatoryControl.CssClass = "Axf_Control";

                    VfTextBox nome = new VfTextBox("nomeMacroGroup", "Nome", "Label", InputTypes.Text);
                    nome.CustomCssClass = "Axf_Control";
                    nome.Required = true;
                    _FormInserimento.Fields.Add(nome);

                    VfTextBox system = new VfTextBox("systemMacroGroup", "Nome di Sistema", "Key", InputTypes.Text);
                    system.CustomCssClass = "Axf_Control";
                    system.Required = false;
                    _FormInserimento.Fields.Add(system);
                }
                return _FormInserimento;
            }
        }
        private VfManager _FormInserimento;

        public HtmlGenericControl getView()
        {
            if (!IsPostBack)
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di creazione di un Macro Gruppo.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);            
            }

            HtmlGenericControl output = new HtmlGenericControl("div");
            WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
            fieldset.ID = "fieldsetInsert";
            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            legend.Controls.Add(new LiteralControl("Creazione Macro Gruppo"));
            legend.Attributes["for"] = "fieldsetInsert";
            fieldset.Controls.Add(legend);
            fieldset.Controls.Add(FormInserimento);
            output.Controls.Add(fieldset);

            return output;
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (FormInserimento.IsValid == VfGeneric.ValidationStates.Valid)
            {
                if (GroupsBusinessLogic.CreateMacroGroup(FormInserimento) != null)
                {
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha creato con successo il Macro Gruppo.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    NetCms.GUI.PopupBox.AddSessionMessage("Macro Gruppo creato con successo", PostBackMessagesType.Success, true);
                }
                else
                    NetCms.GUI.PopupBox.AddSessionMessage("Errore durante la creazione del Macro Gruppo", PostBackMessagesType.Error,false);

            }
        }
    }
}
