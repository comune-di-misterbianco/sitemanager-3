﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetTable;
using NetCms.Structure.Grants;
using NetUtility.NetTreeViewControl;
using System.Collections.Generic;

namespace NetCms.Users.GroupsManager
{
    [DynamicUrl.PageControl("group_new.aspx", ApplicationID = GroupsApplication.ApplicationID)]
    public class NewPage : AbstractPage
    {        
        public override string PageTitle
        {
            get { return base.PageTitle +  ""; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Chart_Organisation_Add; }
        }
        
        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/{0}/default.aspx".FormatByParameters(GroupsApplication.ApplicationSystemName), NetCms.GUI.Icons.Icons.Chart_Organisation, "Elenco Gruppi"));
        }

        public NewPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            if(!Grants.Grant)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();
            
            PostBack();
            SetToolbars();
        }

        public override WebControl Control
        {
            get
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                if (HttpContext.Current.Request.Form["xTreeViewSelectedNodeValue"] != null &&
                    HttpContext.Current.Request.Form["xTreeViewSelectedNodeValue"].Length > 0
                    )
                {
                    int ParentGroupID = int.Parse(HttpContext.Current.Request.Form["xTreeViewSelectedNodeValue"]);

                    HtmlGenericControl script = new HtmlGenericControl("script");
                    script.Attributes["type"] = "text/javascript";
                    script.InnerHtml = @"$(document).ready(function() {
                                          if(document.getElementById('"+ ParentGroupID + @"_link')!=null )
		                                    {
		                                    document.getElementById('" + ParentGroupID + @"_link').className='xNodeSelected';
		                                    }
                                        });";
                    value.Controls.Add(script);
                }
                value.Controls.Add(getView());
                return value;
            }
        }


        private void PostBack()
        {

            if (HttpContext.Current.Request.Form["BtAggiungi"] != null)
            {
                if (HttpContext.Current.Request.Form["Nome_Gruppo"] != null && HttpContext.Current.Request.Form["xTreeViewSelectedNodeValue"] != null &&
                    HttpContext.Current.Request.Form["Nome_Gruppo"].Length > 0 && HttpContext.Current.Request.Form["xTreeViewSelectedNodeValue"].Length > 0
                    )
                {
                    NetUtility.RequestVariable macroGroup = new NetUtility.RequestVariable("MacroGroup_Group");
                    NetUtility.RequestVariable Nome_Gruppo = new NetUtility.RequestVariable("Nome_Gruppo");
                    NetUtility.RequestVariable SystemKey_Group = new NetUtility.RequestVariable("SystemKey_Group");
                    int ParentGroupID = int.Parse(HttpContext.Current.Request.Form["xTreeViewSelectedNodeValue"]);
                    //NetCms.Users.Group parent = NetCms.Users.GroupsHandler.RootGroup.FindGroup(ParentGroupID);
                    NetCms.Users.Group parent = GroupsBusinessLogic.GetGroupById(ParentGroupID);

                    MacroGroup macro = GroupsBusinessLogic.GetMacroGroupById(macroGroup.IntValue);

                    if (!NetCms.Configurations.Debug.GenericEnabled)
                        parent.CreateChildGroup(Nome_Gruppo.StringValue, "", macro);
                    else
                        parent.CreateChildGroup(Nome_Gruppo.StringValue, SystemKey_Group.StringValue, macro);

                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha creato il gruppo '" + Nome_Gruppo + "' nel CMS.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    NetCms.GUI.PopupBox.AddSessionMessage("Gruppo Creato", PostBackMessagesType.Success, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                }
                else
                    NetCms.GUI.PopupBox.AddMessage("Devi selezionare un gruppo padre e inserire un nome", PostBackMessagesType.Error);


            }
        }

        public HtmlGenericControl getView()
        {
            HtmlGenericControl output = new HtmlGenericControl("div");

            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            output.Controls.Add(fieldset);
            
            HtmlGenericControl Legend = new HtmlGenericControl("legend");
            Legend.InnerHtml = "Creazione Nuovo Gruppo";
            fieldset.Controls.Add(Legend);
            
            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di creazione di un nuovo gruppo nel CMS.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            HtmlGenericControl p = new HtmlGenericControl("p");
            Label label = new Label();
            label.Text = "Nome*";
            label.AssociatedControlID = "Nome_Gruppo";
            label.Style.Add("display", "block");
            p.Controls.Add(label);

            TextBox textBox = new TextBox();
            textBox.ID = "Nome_Gruppo";
            p.Controls.Add(textBox);

            fieldset.Controls.Add(p);

            p = new HtmlGenericControl("p");
            label = new Label();
            label.Text = "Macro Gruppo";
            label.AssociatedControlID = "MacroGroup_Group";
            label.Style.Add("display", "block");
            p.Controls.Add(label);

            DropDownList dropDownList = new DropDownList();
            dropDownList.ID = "MacroGroup_Group";
            p.Controls.Add(dropDownList);

            //DataTable rdr = this.Connection.SqlQuery("SELECT * FROM groups_macrogroups WHERE id_MacroGroup > 0 ORDER BY Label_MacroGroup");
            //dropDownList.Items.Add(new ListItem("Nessuno", "0"));
            //foreach (DataRow row in rdr.Rows)
            //    dropDownList.Items.Add(new ListItem(row["Label_MacroGroup"].ToString(), row["id_MacroGroup"].ToString()));

            ICollection<MacroGroup> rdr = GroupsBusinessLogic.FindAllMacroGroups();
            foreach (MacroGroup mg in rdr)
            {
                dropDownList.Items.Add(new ListItem(mg.Label, mg.ID.ToString()));
            }

            fieldset.Controls.Add(p);

            if (NetCms.Configurations.Debug.GenericEnabled)
            {
                p = new HtmlGenericControl("p");
                label = new Label();
                label.Text = "Nome di sistema";
                label.AssociatedControlID = "SystemKey_Group";
                label.Style.Add("display", "block");
                p.Controls.Add(label);

                textBox = new TextBox();
                textBox.ID = "SystemKey_Group";
                p.Controls.Add(textBox);
                fieldset.Controls.Add(p);
            }
            Conn.CacheEnabled = false;
            p = new HtmlGenericControl("p");
            p.InnerHtml = "Seleziona Gruppo Padre*";
            fieldset.Controls.Add(p);
            GroupTreeBinder binder = new GroupTreeBinder(PageData.CurrentReference);
            NetTreeNode node = binder.getTree(new NetTreeNodeStyle(NetCms.Configurations.Paths.AbsoluteRoot + "/css/"));
            NetTreeView tree = new NetTreeView();
            tree.Root = node;
            tree.Type = NetTreeViewType.StaticSimple;

            p.Controls.Add(tree.getControl());
            p = new HtmlGenericControl("p");
            fieldset.Controls.Add(p);
            Button bt = new Button();
            bt.Text = "Crea";
            bt.ID = "BtAggiungi";
            bt.OnClientClick = "setSubmitSource('BtAggiungi')";
            p.Controls.Add(bt);
            Conn.CacheEnabled = true;
            return output;
        }
    }
}
