﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetTable;

namespace NetCms.Users.GroupsManager
{
    //[DynamicUrl.PageControl(".aspx", ApplicationID = GroupsApplication.ApplicationID)]
    public class TemplatePage : AbstractPage
    {        
        public override string PageTitle
        {
            get { return "Template"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Chart_Organisation; }
        }
        
        private void SetToolbars()
        {
            //NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/{0}/group_mod.aspx?gid=".FormatByParameters(GroupsApplication.ApplicationSystemName)
        }

        public TemplatePage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            if(!Grants.Grant)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

            SetToolbars();
        }

        public override WebControl Control
        {
            get
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                return value;
            }
        }
    }
}
