﻿using System.Web.UI;
using NetCms.GUI;
using NetCms.Vertical.Grants;
using NetCms.Connections;

namespace NetCms.Users.GroupsManager
{
    public abstract class AbstractPage : NetCms.Vertical.ApplicationPageEx
    {
        public GrantElement Grants
        {
            get
            {
                if (_Grants == null)
                    _Grants = new GrantElement(GroupsApplication.ApplicationID.ToString(), NetCms.Users.Profile.CurrentProfile);
                return _Grants;
            }
        }
        private GrantElement _Grants;

        public NetCms.Users.User Account
        {
            get
            {
                return _Account ?? (_Account = NetCms.Users.AccountManager.CurrentAccount);
            }
        }
        private NetCms.Users.User _Account;

        private Connection _Conn;
        public Connection Conn
        {
            get
            {
                //if (_Conn == null)
                //    _Conn = NetCms.Connections.ConnectionsManager.CommonConnection;
                //return _Conn;

                ConnectionsManager ConnManager = new ConnectionsManager();
                return ConnManager.CommonConnection;
            }
        }

        public override string PageTitle
        {
            get { return "Gruppi &raquo; "; }
        }

        public AbstractPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
        }

        public void CheckGrantsOnThisUser(NetCms.Users.User handledUser )
        {
            bool iCanAdminThisUserByGroups = Account.IsAdministratorForUser(handledUser.ID);
            bool userHasNoGroupsAndICanHandleUngrouppedUsers = Grants[GroupsApplication.ApplicationSystemName + "_ungroupped"].Allowed && handledUser.Groups.Count == 0;
            if (!iCanAdminThisUserByGroups && !userHasNoGroupsAndICanHandleUngrouppedUsers)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();
        }
    }
}
