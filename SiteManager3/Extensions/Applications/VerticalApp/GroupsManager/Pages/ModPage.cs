﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetTable;
using NetForms;
using System.Collections.Generic;

namespace NetCms.Users.GroupsManager
{
    [DynamicUrl.PageControl("group_mod.aspx", ApplicationID = GroupsApplication.ApplicationID)]
    public class ModPage : AbstractPage
    {
        private NetCms.Users.Group _Group;
        private Group Group
        {
            get
            {
                if (_Group == null)
                {
                    G2Core.Common.RequestVariable req = new G2Core.Common.RequestVariable("gid", G2Core.Common.RequestVariable.RequestType.QueryString);
                    if (req.IsValidInteger)
                        _Group = NetCms.Users.GroupsHandler.RootGroup.FindGroup(req.IntValue);
                    else
                        PopupBox.AddNoGrantsMessageAndGoBack();
                }

                return _Group;
            }
        }
        
        public override string PageTitle
        {
            get { return base.PageTitle + "Modifica gruppo '" + Group.Name + "'"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Chart_Organisation; }
        }
        
        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/{0}/default.aspx".FormatByParameters(GroupsApplication.ApplicationSystemName), NetCms.GUI.Icons.Icons.Chart_Organisation, "Gestisci Gruppi"));
        }

        public ModPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            if(!Grants.Grant)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

        }

        public override WebControl Control
        {
            get
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                value.Controls.Add(getView());
                return value;
            }
        }


        public HtmlGenericControl getView()
        {

            HtmlGenericControl output = new HtmlGenericControl("div");
            SetToolbars();


            NetFormTable table = new NetFormTable("Groups", "id_Group", Conn, this.Group.ID);

            NetTextBox nome = new NetTextBox("Nome", "Name_Group");
            nome.Required = true;
            table.addField(nome);

            NetDropDownList macroGroup = new NetDropDownList("Macro Gruppo", "MacroGroup_Group");
            macroGroup.ClearList();
            macroGroup.Required = false;

            //DataTable rdr = Connection.SqlQuery("SELECT * FROM groups_macrogroups WHERE id_MacroGroup > 0 ORDER BY Label_MacroGroup");
            //macroGroup.addItem(new ListItem("Nessuno", "0"));
            //foreach (DataRow row in rdr.Rows)
            //    macroGroup.addItem(new ListItem(row["Label_MacroGroup"].ToString(), row["id_MacroGroup"].ToString()));

            ICollection<MacroGroup> rdr = GroupsBusinessLogic.FindAllMacroGroups();
            foreach (MacroGroup mg in rdr)
            {
                macroGroup.addItem(new ListItem(mg.Label, mg.ID.ToString()));
            }


            table.addField(macroGroup);

            NetForm forms = new NetForm();
            forms.AddTable(table);

            if (this.IsPostBack)
            {
                string errors = forms.serializedUpdate(HttpContext.Current.Request.Form);
                if (forms.LastOperation == NetForm.LastOperationValues.OK)
                {
                    //this.Group.Update();
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha modificato il Gruppo '" + Group.Name + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    NetCms.GUI.PopupBox.AddSessionMessage("Gruppo aggiornato con successo", PostBackMessagesType.Success, true);
                }
                else
                {
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a modificare il Gruppo '" + Group.Name + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    NetCms.GUI.PopupBox.AddMessage(errors, PostBackMessagesType.Error);
                }
            }
            else
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di modifica del Gruppo '" + Group.Name + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            }

            output.Controls.Add(forms.getForms("Modifica Dati Gruppo"));

            return output;
        }
    }
}
