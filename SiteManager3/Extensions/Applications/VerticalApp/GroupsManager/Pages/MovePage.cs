﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetTable;
using NetUtility.NetTreeViewControl;

namespace NetCms.Users.GroupsManager
{
    [DynamicUrl.PageControl("group_move.aspx", ApplicationID = GroupsApplication.ApplicationID)]
    public class MovePage : AbstractPage
    {        
        public override string PageTitle
        {
            get { return base.PageTitle +  "Spostamento Gruppo '" + Group.Name + "'"; }
        }

        private Group _Group;
        private Group Group
        {
            get
            {
                if (_Group == null)
                {
                    G2Core.Common.RequestVariable req = new G2Core.Common.RequestVariable("gid", G2Core.Common.RequestVariable.RequestType.QueryString);
                    if (req.IsValidInteger)
                        try
                        {
                            _Group = NetCms.Users.GroupsHandler.RootGroup.FindGroup(req.IntValue);
                        }
                        catch (Exception e)
                        {
                            PopupBox.AddSessionMessage("Il gruppo non esiste", PostBackMessagesType.Error, this.Account.History.GetBackURL());
                            //Trace
                        }

                }
                if (_Group == null)
                    PopupBox.AddSessionMessage("Il gruppo non esiste", PostBackMessagesType.Error, this.Account.History.GetBackURL());

                return _Group;
            }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Chart_Organisation; }
        }
        
        private void SetToolbars()
        {
            //NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/{0}/group_mod.aspx?gid=".FormatByParameters(GroupsApplication.ApplicationSystemName)
        }

        public MovePage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            if(!Grants.Grant)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

            SetToolbars();
        }

        public override WebControl Control
        {
            get
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                CheckPostBack();
                value.Controls.Add(getView());
                return value;
            }
        }

        private void CheckPostBack()
        {
            if (this.IsPostBack)
            {
                G2Core.Common.RequestVariable request = new G2Core.Common.RequestVariable("xTreeViewSelectedNodeValue", G2Core.Common.RequestVariable.RequestType.Form);
                if (request.IsValidInteger)
                {
                    var parentGroup = NetCms.Users.GroupsHandler.RootGroup.FindGroup(request.IntValue);

                    if (parentGroup != null)
                    {
                        if (parentGroup.ID == this.Group.Parent.ID)
                        {
                            NetCms.GUI.PopupBox.AddMessage("Impossibile spostare il gruppo. Il nuovo gruppo padre in cui spostare il gruppo '" + this.Group.Name + "' è uguale al vecchio gruppo padre.", PostBackMessagesType.Notify);
                            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a spostare il gruppo " + this.Group.Name + ", poichè il nuovo gruppo padre è uguale al vecchio.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        }
                        else
                        {
                            if (this.Group.Move(parentGroup))
                            {
                                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha spostato con successo il gruppo " + this.Group.Name + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                                NetCms.GUI.PopupBox.AddSessionMessage("Il gruppo '" + this.Group.Name + "' è stato spostato con successo.", PostBackMessagesType.Success, true);
                            }
                            else
                            {
                                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a spostare il gruppo " + this.Group.Name + ", poichè non ha selezionato un nuovo gruppo padre.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                                NetCms.GUI.PopupBox.AddMessage("Impossibile spostare il gruppo. Devi selezionare il nuovo gruppo in cui spostare il gruppo '" + this.Group.Name + "'", PostBackMessagesType.Notify);
                            }
                        }
                    }
                    else
                    {
                        string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a spostare il gruppo " + this.Group.Name + ", poichè non ha selezionato un nuovo gruppo padre.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        NetCms.GUI.PopupBox.AddMessage("Impossibile spostare il gruppo. Devi selezionare il nuovo gruppo in cui spostare il gruppo '" + this.Group.Name + "'", PostBackMessagesType.Notify);
                    }
                }
                else
                {
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a spostare il gruppo " + this.Group.Name + ", poichè non ha selezionato un nuovo gruppo padre.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    NetCms.GUI.PopupBox.AddMessage("Impossibile spostare il gruppo. Devi selezionare il nuovo gruppo in cui spostare il gruppo '" + this.Group.Name + "'", PostBackMessagesType.Notify);
                }
            }
        }

        public HtmlGenericControl getView()
        {
            HtmlGenericControl output = new HtmlGenericControl("div");

            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            output.Controls.Add(fieldset);

            HtmlGenericControl Legend = new HtmlGenericControl("legend");
            Legend.InnerHtml = "Spostamento Gruppo '" + Group.Name + "'";
            fieldset.Controls.Add(Legend);

            if (this.Group.IsSystem)
            {
                HtmlGenericControl p = new HtmlGenericControl("p");
                p.InnerHtml = "Spiacente ma non è possibile spostare un gruppo di sistema.";
                fieldset.Controls.Add(p);

            }
            else
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di spostamento del gruppo " + Group.Name + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);


                HtmlGenericControl p = new HtmlGenericControl("p");
                Label label = new Label();

                p = new HtmlGenericControl("p");
                p.InnerHtml = "Seleziona un nuovo Gruppo Padre*";
                fieldset.Controls.Add(p);
                NetCms.Structure.Grants.GroupTreeBinder binder = new NetCms.Structure.Grants.GroupTreeBinder(PageData.CurrentReference);
                binder.UnavailableBranchID = this.Group.ID;
                NetTreeNode node = binder.getTree(new NetTreeNodeStyle(NetCms.Configurations.Paths.AbsoluteRoot + "/css/"));
                NetTreeView tree = new NetTreeView();
                tree.Root = node;
                tree.Type = NetTreeViewType.StaticSimple;

                p.Controls.Add(tree.getControl());
                p = new HtmlGenericControl("p");
                fieldset.Controls.Add(p);
                Button bt = new Button();
                bt.Text = "Sposta";
                bt.ID = "BtAggiungi";
                bt.OnClientClick = "setSubmitSource('BtAggiungi')";
                p.Controls.Add(bt);
            }

            return output;
        }
    }
}
