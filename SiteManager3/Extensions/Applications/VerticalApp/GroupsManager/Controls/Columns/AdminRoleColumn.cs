using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetService.Utility.ArTable;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Users.GroupsManager
{
    public class AdminRoleColumn : ArAbstractColumn
    {

        private int groupId;

        public AdminRoleColumn(int _groupId)
            : base("Amministrazione","Type")
        {
            this.groupId = _groupId;
        }

        public override bool ExportData
        {
            get { return true; }
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            int userID = int.Parse(GetValue(objectInstance, "id_User").ToString());
            
            object valueObj = GetValue(objectInstance, this.Key);
            string value = "";

            if (valueObj == null)
            {
                GroupAssociation groupAssociation = GroupsBusinessLogic.GetGroupAssociationByUserAndGroup(userID, groupId); //usersGroupsList.Where(x => x.UserID == userID).ToList();
                if (groupAssociation != null)
                {
                    value = groupAssociation.Type.ToString();
                }
            }
            else
                value = valueObj.ToString();


            /*
            switch (value)
            {
                case "0": value = "Utente"; break;
                case "1": value = "Amministratore"; break;
                case "2": value = "Super Amministratore"; break; 
            }
            */
            
            TableCell td = new TableCell();
            if (!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;

            td.Text = @"
                                <span class=""radioontab""><input class=""radioU""  " + (value == "0" ? "checked=\"checked\"" : "") + @" type=""radio"" name=""admin" + userID + @""" id=""admin" + userID + @""" value=""0"" /><label " + (value == "0" ? "class=\"current\"" : "") + @" for=""admin" + userID + @""">U</label></span>
                                <span class=""radioontab""><input  class=""radioA"" " + (value == "1" ? "checked=\"checked\"" : "") + @" type=""radio"" name=""admin" + userID + @""" id=""admin" + userID + @""" value=""1"" /><label " + (value == "1" ? "class=\"current\"" : "") + @" for=""admin" + userID + @""">A</label></span>
                                <span class=""radioontab""><input class=""radioS""  " + (value == "2" ? "checked=\"checked\"" : "") + @" type=""radio"" name=""admin" + userID + @""" id=""admin" + userID + @""" value=""2"" /><label " + (value == "2" ? "class=\"current\"" : "") + @" for=""admin" + userID + @""">S</label></span>
                                <input type=""hidden"" name=""records"" id=""records"" value=""" + userID + @""" />
                            ";


            return td;
        }

        public override string GetExportValue(object obj)
        {
            int userID = int.Parse(GetValue(obj, "id_User").ToString());

            object valueObj = GetValue(obj, this.Key);
            string value = "";

            if (valueObj == null)
            {
                GroupAssociation groupAssociation = GroupsBusinessLogic.GetGroupAssociationByUserAndGroup(userID, groupId); //usersGroupsList.Where(x => x.UserID == userID).ToList();
                if (groupAssociation != null)
                {
                    value = groupAssociation.Type.ToString();
                }
            }
            else
                value = valueObj.ToString();

            switch (value)
            {
                case "0": value = "Utente"; break;
                case "1": value = "Amministratore"; break;
                case "2": value = "Super Amministratore"; break; 
            }
            return value;
        }
    }
}