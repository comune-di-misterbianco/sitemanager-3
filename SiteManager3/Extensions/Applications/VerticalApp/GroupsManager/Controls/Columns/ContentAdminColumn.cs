using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetService.Utility.ArTable;
using System.Collections.Generic;
using System.Linq;
/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Users.GroupsManager
{
    public class ContentAdminColumn : ArAbstractColumn
    {
        private int groupId;

        public ContentAdminColumn(int _groupId)
            : base("Contenuti", "PublishRole")
        {
            this.groupId = _groupId;
        }

        public override bool ExportData
        {
            get { return true; }
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            int userID = int.Parse(GetValue(objectInstance, "id_User").ToString());
            object valueObj = GetValue(objectInstance, this.Key);
            string value = "";

            if (valueObj == null)
            {
                GroupAssociation groupAssociation = GroupsBusinessLogic.GetGroupAssociationByUserAndGroup(userID, groupId);
                if (groupAssociation != null)
                {
                    value = groupAssociation.PublishRole.ToString();
                }
            }
            else
                value = valueObj.ToString();

            TableCell td = new TableCell();
            if (!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;
            td.Text = @"
                                <span class=""radioontab""><input class=""radioW"" " + (value == "0" ? "checked=\"checked\"" : "") + @" type=""radio"" name=""content" + userID + @""" id=""content" + userID + @""" value=""0"" /><label " + (value == "0" ? "class=\"current\"" : "") + @"  for=""content" + userID + @""">W</label></span>
                                <span class=""radioontab""><input class=""radioR""  " + (value == "1" ? "checked=\"checked\"" : "") + @" type=""radio"" name=""content" + userID + @""" id=""content" + userID + @""" value=""1"" /><label " + (value == "1" ? "class=\"current\"" : "") + @"  for=""content" + userID + @""">R</label></span>
                                <span class=""radioontab""><input class=""radioP""  " + (value == "2" ? "checked=\"checked\"" : "") + @" type=""radio"" name=""content" + userID + @""" id=""content" + userID + @""" value=""2"" /><label " + (value == "2" ? "class=\"current\"" : "") + @"  for=""content" + userID + @""">P</label></span>
                            ";


            return td;
        }

        public override string GetExportValue(object obj)
        {
            int userID = int.Parse(GetValue(obj, "id_User").ToString());

            object valueObj = GetValue(obj, this.Key);
            string value = "";

            if (valueObj == null)
            {
                GroupAssociation groupAssociation = GroupsBusinessLogic.GetGroupAssociationByUserAndGroup(userID, groupId);
                if (groupAssociation != null)
                {
                    value = groupAssociation.PublishRole.ToString();
                }
            }
            else
                value = valueObj.ToString();

            switch (value)
            {
                case "0": value = "Redattore"; break;
                case "1": value = "Revisore"; break;
                case "2": value = "Pubblicatore"; break;
            }
            return value;
        }
    }
}