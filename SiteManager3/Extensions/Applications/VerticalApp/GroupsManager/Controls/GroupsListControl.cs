﻿using System;
using System.Web.UI.WebControls;
using NetCms.Vertical.Grants;
using NetCms.Connections;
using NetService.Utility.ArTable;
using System.Collections.Generic;
using System.Linq;
/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Users.GroupsManager
{
    public class GroupsListControl : WebControl
    {      
        public Users.User Account { get; private set; }
        public Connection Connection { get; private set; } 

        public GrantElement Grants
        {
            get
            {
                if (_Grants == null)
                    _Grants = new GrantElement(GroupsApplication.ApplicationSystemName, this.Account.Profile);
                return _Grants;
            }
        }
        private GrantElement _Grants;

        public GroupsListControl(Connection connection, Users.User account)
        {
            this.Connection = connection;
            this.Account = account;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            getView();
        }
        
//        public void getView()
//        {
//            var output = this;

//            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
//            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di gestione dei gruppi del CMS.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

//            NetCms.Users.Search.UserRelated rels = new NetCms.Users.Search.UserRelated(Account, Connection);
//            string sql = @"SELECT DISTINCT groups.*,groups_macrogroups.*  FROM groups                
//                                LEFT JOIN usersgroups ON (usersgroups.Group_UserGroup = groups.id_Group)
//                                LEFT JOIN groups_macrogroups ON (id_MacroGroup = groups.MacroGroup_Group) 
//                                WHERE false " + rels.MyGroupsConditions + " ORDER BY Tree_Group";
            
//            SmTable table = new SmTable(1, sql, this.Connection);
//            table.CssClass = "GenericTable";
//            table.Pagination = false;

//            #region Campi della tabella

//            SmTableColumn col;

//            SmTableTreeColumn2 treecol = new SmTableTreeColumn2("Nome", "id_Group", "Name_Group", "Depth_Group", this.Connection);
//            treecol.IconType = SmTableTreeColumn2.IconTypes.CssClass;
//            treecol.Icon = "GrantsTableGroupIcon";
//            table.addColumn(treecol);

//            col = new SmTableColumn("Label_MacroGroup", "Macro Gruppo");
//            table.addColumn(col);

//            col = new GroupsCustomSmTableActionColumn("id_Group", "action details", "Scheda e Opzioni", "group_details.aspx?id={0}");
//            table.addColumn(col);
//            #endregion

//            output.Controls.Add(table.getTable());
//        }

        public void getView()
        {
            var output = this;

            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di gestione dei gruppi del CMS.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

            ArTable table = new ArTable();
            table.InnerTableCssClass = "tab";
            table.NoRecordMsg = "Nessun gruppo trovato";

            ICollection<Group> groups = GroupsBusinessLogic.FindGroupsByUser(Account);

            table.Records = groups;

            #region Campi della tabella

            // SISTEMARE SE SERVE
            //SmTableTreeColumn2 treecol = new SmTableTreeColumn2("Nome", "id_Group", "Name_Group", "Depth_Group", this.Connection);
            //treecol.IconType = SmTableTreeColumn2.IconTypes.CssClass;
            //treecol.Icon = "GrantsTableGroupIcon";
            //table.addColumn(treecol);

            ArGroupTreeColumn nome = new ArGroupTreeColumn("Nome", "Name",groups.ToList<object>());
            nome.Icon = "GrantsTableGroupIcon";
            table.AddColumn(nome);

            ArTextColumn macroGruppo = new ArTextColumn("Macro Gruppo", "MacroGroup.Label");
            table.AddColumn(macroGruppo);

            ArActionColumn action = new ArActionColumn("Scheda e Opzioni", ArActionColumn.Icons.Details, "group_details.aspx?id={ID}");
            table.AddColumn(action);
            #endregion

            output.Controls.Add(table);
        }

        //private class GroupsCustomSmTableActionColumn : SmTableActionColumn
        //{
        //    public GroupsCustomSmTableActionColumn(string fieldName, string classe, string Caption, string href)
        //        : base(fieldName, classe, Caption, href, "")
        //    {
        //    }

        //    public override HtmlGenericControl getField(DataRow row)
        //    {
        //        return base.getField(row);
        //    }
        //}
    }
}