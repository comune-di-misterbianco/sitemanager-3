﻿using System.Web.UI.WebControls;
/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Users.GroupsManager
{
    public class AbstractControl : WebControl
    {
        public AbstractPage VerticalPage { get; private set; } 

        public AbstractControl(AbstractPage page)
        {
            this.VerticalPage = page;
        }
    }
}