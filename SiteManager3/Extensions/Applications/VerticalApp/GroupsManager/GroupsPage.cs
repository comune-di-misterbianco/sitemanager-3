using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetTable;
using NetCms.GUI;
using NetCms.Vertical.Grants;
using System.Collections.Generic;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using NetCms.Vertical;
/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Users.Groups
{
    public class GroupsPage : SmPageVertical
    {
        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/groups/group_new.aspx", NetCms.GUI.Icons.Icons.Chart_Organisation_Add, "Crea Gruppo"));
            this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/groups/macro.aspx", NetCms.GUI.Icons.Icons.Chart_Organisation, "Gestione Macro Gruppi"));
        }

        public override string PageTitle
        {
            get { return "Gestione Gruppi"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Chart_Organisation; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_StartPage"; }
        }

        public GroupsPage()
        {
            GrantElement element = new GrantElement("groups", this.Account.Profile);
            if (!element[ "enable"].Allowed)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();
        }

        protected override WebControl RootControl
        {
            get
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                value.Controls.Add(getView());
                return value;
            }
        }

//        public HtmlGenericControl getView()
//        {
//            HtmlGenericControl output = new HtmlGenericControl("div");
//            ExternalAppzGrantsChecker Checker = new ExternalAppzGrantsChecker("groups");
//            if (Checker.Grant)
//            {
//                SetToolbars();

//                //string Utente = this.Account.Label;
//                //string logText = "L'utente " + Utente + " vuole gestire i Gruppi Utenti";
//                //this.GuiLog.SaveLogRecord(logText, false);

//                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
//                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di gestione dei gruppi del CMS.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);                        
                    
//                NetCms.Users.Search.UserRelated rels = new  NetCms.Users.Search.UserRelated(Account, Connection);
//                string sql = @"SELECT DISTINCT groups.*,groups_macrogroups.*  FROM groups                
//                                LEFT JOIN usersgroups ON (usersgroups.Group_UserGroup = groups.id_Group)
//                                LEFT JOIN groups_macrogroups ON (id_MacroGroup = groups.MacroGroup_Group) 
//                                WHERE false " + rels.MyGroupsConditions + " ORDER BY Tree_Group";               
//                //sql += " WHERE " + ;
//                //sql += " ORDER BY Tree_Group";
//                SmTable table = new SmTable(1,sql, this.Connection);
//                table.CssClass = "GenericTable";
//                table.Pagination = false;

//                #region Campi della tabella

//                SmTableColumn col;

//                SmTableTreeColumn2 treecol = new SmTableTreeColumn2("Nome", "id_Group", "Name_Group", "Depth_Group", this.Connection);
//                treecol.IconType = SmTableTreeColumn2.IconTypes.CssClass;
//                treecol.Icon = "GrantsTableGroupIcon";
//                table.addColumn(treecol);

//                col = new SmTableColumn("Label_MacroGroup", "Macro Gruppo");
//                table.addColumn(col);

//                col = new GroupsCustomSmTableActionColumn("id_Group", "action details", "Scheda e Opzioni", "group_details.aspx?id={0}");
//                table.addColumn(col);
//                #endregion

//                output.Controls.Add(table.getTable());
//            }
//            else
//                output.Controls.Add(Checker.ErrorControl);

//            return output;
//        }

        public HtmlGenericControl getView()
        {
            HtmlGenericControl output = new HtmlGenericControl("div");
            //ExternalAppzGrantsChecker Checker = new ExternalAppzGrantsChecker("groups");
            VerticalApplication groupsApp = VerticalApplicationsPool.GetBySystemName("groups");
            if (groupsApp.Grant)
            {
                SetToolbars();

                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di gestione dei gruppi del CMS.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                ArTable table = new ArTable();
                table.InnerTableCssClass = "tab";
                table.NoRecordMsg = "Nessun gruppo trovato";

                ICollection<Group> groups = GroupsBusinessLogic.FindGroupsByUser(Account);

                table.Records = groups;

                #region Campi della tabella

                // SISTEMARE SE SERVE
                //SmTableTreeColumn2 treecol = new SmTableTreeColumn2("Nome", "id_Group", "Name_Group", "Depth_Group", this.Connection);
                //treecol.IconType = SmTableTreeColumn2.IconTypes.CssClass;
                //treecol.Icon = "GrantsTableGroupIcon";
                //table.addColumn(treecol);

                ArTextColumn nome = new ArTextColumn("Nome", "Name");
                table.AddColumn(nome);

                ArTextColumn macroGruppo = new ArTextColumn("Macro Gruppo", "MacroGroup.Label");
                table.AddColumn(macroGruppo);

                ArActionColumn action = new ArActionColumn("Scheda e Opzioni", ArActionColumn.Icons.Details, "group_details.aspx?id={ID}");
                table.AddColumn(action);
                #endregion

                output.Controls.Add(table);
            }
            else
                output.Controls.Add(groupsApp.ErrorControl);

            return output;
        }

        private class GroupsCustomSmTableActionColumn : SmTableActionColumn
        {
            public GroupsCustomSmTableActionColumn(string fieldName, string classe, string Caption, string href)
                : base(fieldName, classe, Caption, href, "")
            {
            }

            public override HtmlGenericControl getField(DataRow row)
            {
                return base.getField(row);
            }
        }
    }
}