﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using System.Linq;
using NetCms.Vertical;
using DynamicUrl;


namespace NetCms.Users.GroupsManager
{
    public class Setup : NetCms.Vertical.VerticalApplicationSetup
    {
        public override string[,] Pages
        {
            get
            {
                if (_Pages == null)
                {
                    PageHandler handler = new PageHandler();
                    handler.Assemblies.Add(this.GetType().Assembly);
                    handler.InitPages();

                    var pages = new string[handler.Pages.Count, 2];

                    int i = 0;
                    foreach (var page in handler.Pages.Where(x => x.Value.FullName.StartsWith("NetCms.Users.GroupsManager")))
                    {
                        pages[i, 0] = page.Key;
                        pages[i++, 1] = page.Value.FullName;
                    }

                    _Pages = pages;
                }
                return _Pages;
            }
        }
        private string[,] _Pages;

        public override string[,] Criteria
        {
            get
            {
                return null;
            }
        }	

        //Groups
        //case "/applications/groups/default.aspx": return new NetCms.Pages.Networks.Cms.Grants.Groups(page, viewState);
        //case "/applications/groups/group_new.aspx": return new NetCms.Pages.Networks.Cms.Grants.GroupNew(page, viewState);               
        //case "/applications/groups/macro.aspx": return new NetCms.Pages.Networks.Cms.Grants.MacroGroups(page, viewState);
        //case "/applications/groups/macro_add.aspx": return new NetCms.Pages.Networks.Cms.Grants.MacroGroupsAdd(page, viewState);
        //case "/applications/groups/macro_edit.aspx": return new NetCms.Pages.Networks.Cms.Grants.MacroGroupsEdit(page, viewState);
        ////Il link rimanda a groups.aspx
        //case "/applications/groups/group_admins.aspx": return new NetCms.Pages.Networks.Cms.Grants.GroupAdmin(page, viewState);
        ////end
        //case "/applications/groups/group_details.aspx": return new NetCms.Pages.Networks.Cms.Grants.GroupDetails(page, viewState);
        //case "/applications/groups/group_mod.aspx": return new NetCms.Pages.Networks.Cms.Grants.GroupMod(page, viewState);                                                
        ////sembra non faccia niente, verificare se rimuoverlo
        //case "/applications/groups/group_grants.aspx": return new NetCms.Pages.Networks.Cms.Grants.GroupGrants(page, viewState);
        ////end

        //case "/applications/groups/group_move.aspx": return new NetCms.Pages.Networks.Cms.Grants.GroupMove(page, viewState);
        ////momentaneamente le pagine contengono un bug sulla ricerca degli utenti
        //case "/applications/groups/group_user_add.aspx": return new NetCms.Pages.Networks.Cms.Grants.GroupUserAdd(page, viewState);
        //case "/applications/groups/group_users_list.aspx": return new NetCms.Pages.Networks.Cms.Grants.GroupUsersList(page, viewState);
        ////end

        //case "/applications/groups/group_users.aspx": return new NetCms.Pages.Networks.Cms.Grants.GroupUsers(page, viewState);
        ////Duplicato di default.aspx
        //case "/applications/groups/groups.aspx": return new NetCms.Pages.Networks.Cms.Grants.Groups(page, viewState);
        ////end
        ////mai richiamata. 
        //case "/applications/groups/group_add.aspx": return new NetCms.Pages.Networks.Cms.Grants.GroupAdd(page, viewState);
        ////end

        public override bool IsNetworkDependant
        {
            get { return false; }
        }

        public Setup(VerticalApplication baseApp)
            : base(baseApp)
        {
        }

        protected override bool LocalInstall(bool isInsidePackage = false)
        {
            return true;
        }
    }
}
