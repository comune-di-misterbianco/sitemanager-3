﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace NetCms.NetworksManager
{
    public class NetworksManager :NetCms.Vertical.VerticalApplication
    {
        public const string ApplicationContextBackLabel = "NetworksManager";
        public override int HomepageGroup
        {
            get { return 3; }
        }

        public override NetCms.Vertical.VerticalApplication.AccessModes AccessMode
        {
            get { return AccessModes.Admin; }
        }
        public override string Description
        {
            get { return "Gestione delle Reti"; }
        }

        public override bool CheckForDefaultGrant
        {
            get { return true; }
        }

        public override int ID
        {
            get { return 235; }
        }

        public override string Label
        {
            get { return "Gestione delle Reti"; }
        }

        public override bool NetworkDependant
        {
            get { return false; }
        }

        public override int SecurityLevel
        {
            get { return 0; }
        }

        public override string SystemName
        {
            get { return "networksmanager"; }
        }

        public override bool DoAdditionalConfigurationForExternalDB()
        {
            throw new NotImplementedException();
        }

        public override bool NeedAdditionalConfiguration
        {
            get { return false; }
        }

        public override Vertical.VerticalApplicationSetup  Installer
        {
            get
            {
                if (_Installer == null)
                    _Installer = new NetworksManagerSetup(this);
                return _Installer;
            }
        }
        private NetworksManagerSetup _Installer;

        public NetworksManager(Assembly assembly)
            : base(assembly)
        {            
        }

        public override bool UseCmsSessionFactory
        {
            get { return false; }
        }
    }
}
