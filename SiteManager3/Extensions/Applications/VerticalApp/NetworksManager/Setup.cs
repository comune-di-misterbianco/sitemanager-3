﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using NetCms.Vertical;


namespace NetCms.NetworksManager
{
    public class NetworksManagerSetup : NetCms.Vertical.VerticalApplicationSetup
    {
        public override string[,] Pages
        {
            get
            {
                string[,] pages =   {
                                        //{"default.aspx", "NetCms.NetworksManager.NetworksManagerMainPage" },                                                                       
                                        {"add.aspx", "NetCms.NetworksManager.NetworksManagerAddPage" },                                                                     
                                        {"details.aspx", "NetCms.NetworksManager.NetworksManagerDetailPage" },                                                                     
                                        {"delete.aspx", "NetCms.NetworksManager.NetworksManagerDeletePage" }                                                                     
                                    };
                return pages;
            }
        }

        public override string[,] Criteria
        {
            get
            {
                return null;
                /* string[,] criteria = {
                                        {"Abilita l'appicazione a MyCriteria","mycriteria"}
                                    };
                return criteria; */
            }
        }

        public override bool IsNetworkDependant
        {
            get { return true; }
        }
        public NetworksManagerSetup(VerticalApplication baseApp)
            : base(baseApp)
        {
        }

        protected override bool LocalInstall(bool isInsidePackage = false)
        {
            return true;
        }

    }
}
