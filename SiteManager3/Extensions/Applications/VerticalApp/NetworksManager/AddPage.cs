﻿using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetCms.Connections;

namespace NetCms.NetworksManager
{
    public class NetworksManagerAddPage : NetCms.Vertical.ApplicationPageEx
    {
        public override string PageTitle
        {
            get { return "Gestione Portali » Creazione Nuova Portale"; }
        }
        public override WebControl Control
        {
            get
            {
                return BuildControls();
            }
        }

		private Connection _Conn;
        private Connection Conn
        {
            get
            {
                //if (_Conn == null)
                //    _Conn = NetCms.Connections.ConnectionsManager.CommonConnection;
                //return _Conn;

                ConnectionsManager ConnManager = new ConnectionsManager();
                return ConnManager.CommonConnection;
            }
        }

        public NetworksManagerAddPage(System.Web.UI.StateBag viewstate, bool IsPostBack, NetCms.GUI.Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewstate, IsPostBack, toolbar, informationBox, networkKey)
        {
            ApplicationContext.SetCurrentComponent(NetworksManager.ApplicationContextBackLabel);
            AddToolbarButtons(); 
        }

        protected void AddToolbarButtons()
        {
            NetCms.GUI.ToolbarButton button = new NetCms.GUI.ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Pagina Iniziale");
            this.Toolbar.Buttons.Add(button);
        }
        private WebControl BuildControls()
        {
            WebControl controls = new WebControl(HtmlTextWriterTag.Div);

            NetForms.NetForm form = new NetForms.NetForm();
            NetForms.NetFormTable table = new NetForms.NetFormTable("networks", "id_Network", Conn);
            form.Tables.Add(table);

            NetForms.NetTextBoxPlus nome = new NetForms.NetTextBoxPlus("Nome", "Label_Network");
            nome.Required = true;
            table.addField(nome);

            NetForms.NetTextBoxPlus system = new NetForms.NetTextBoxPlus("Nome di Sistema", "Nome_Network");
            system.Required = true;
            table.addField(system);

            NetForms.NetDropDownList cms = new NetForms.NetDropDownList("Genera CMS", "CMS_Network");
            cms.Required = true;
            cms.addItem("Si", "1");
            cms.addItem("No", "0");
            table.addField(cms);

            if (table.IsPostBack)
            {
                System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("[àèéìòù'A-Za-z0-9_-]$",
                   System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace);

                //if (!system.PostBackValue.Contains(" "))
                if (regex.IsMatch(system.PostBackValue) && (!system.PostBackValue.Contains(" ")) )               
                {
                    string errors = table.validateForm();

                    if (errors == null)
                    {
                        bool GenerateCMS = cms.RequestVariable.IntValue == 1;
                        Networks.NetworkGenerator generator = new NetCms.Networks.NetworkGenerator(nome.RequestVariable.StringValue, system.RequestVariable.StringValue, GenerateCMS);

                        generator.AllowUser(NetCms.Users.AccountManager.CurrentAccount.ID);
                        bool result = generator.Generate();
                        if (result)
                        {
                            NetCms.GUI.PopupBox.AddMessage("Il Portale " + nome.RequestVariable.StringValue + " è stato creato con successo.", NetCms.GUI.PostBackMessagesType.Success);
                            NetCms.GUI.GuiUtility.RestartApplication();
                        }
                        else
                        {
                            NetCms.GUI.PopupBox.AddMessage("Non è stato possibile creare il Portale " + nome.RequestVariable.StringValue, NetCms.GUI.PostBackMessagesType.Error);
                            foreach (string str in generator.Errors)
                                NetCms.GUI.PopupBox.AddMessage(str, PostBackMessagesType.Normal);
                        }
                    }
                }
                else
                    NetCms.GUI.PopupBox.AddMessage("Il nome della network non può contenere caratteri come spazi, $,%' ecc.", PostBackMessagesType.Normal);
            }
            controls.Controls.Add(form.getForms("Creazione Nuovo Portale"));
            return controls;
        }

    }

}
