﻿using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;


namespace NetCms.NetworksManager
{
    public class NetworksManagerDetailPage : NetCms.Vertical.ApplicationPageEx
    {
        public G2Core.Common.RequestVariable Request
        {
            get
            {
                if (_Request == null)
                    _Request = new G2Core.Common.RequestVariable("pkey", G2Core.Common.RequestVariable.RequestType.QueryString);
                return _Request;
            }
        }
        private G2Core.Common.RequestVariable _Request;

        public NetCms.Networks.BasicNetwork Network
        {
            get
            {
                if (_Network == null)
                    _Network = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks[Request.StringValue.ToLower()];
                return _Network;
            }
        }
        private NetCms.Networks.BasicNetwork _Network;

        public override string PageTitle
        {
            get { return "Gestione Portali » Gestione Portale '"+Network.Label+"'"; }
        }
        public override WebControl Control
        {
            get
            {
                return BuildControls();
            }
        }
        
        public NetworksManagerDetailPage(System.Web.UI.StateBag viewstate, bool IsPostBack, NetCms.GUI.Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewstate, IsPostBack, toolbar, informationBox, networkKey)
        {
            ApplicationContext.SetCurrentComponent(NetworksManager.ApplicationContextBackLabel);
            AddToolbarButtons();
        }

        protected void AddToolbarButtons()
        {
            NetCms.GUI.ToolbarButton button = new NetCms.GUI.ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Pagina Iniziale");
            this.Toolbar.Buttons.Add(button);
        }
        private WebControl BuildControls()
        {
            WebControl controls = new WebControl(HtmlTextWriterTag.Div);

            #region CMS Postback

            HtmlGenericControls.InputField cms = new HtmlGenericControls.InputField("cmsRequest");
            controls.Controls.Add(cms.Control);
            if (cms.RequestVariable.IsPostBack && NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.ContainsKey(cms.RequestVariable.StringValue))
            {
                NetCms.Networks.BasicNetwork net = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks[cms.RequestVariable.StringValue];
                if (net.CmsStatus == NetCms.Networks.NetworkCmsStates.NotCreated)
                {
                    Networks.NetworkGenerator generator = new NetCms.Networks.NetworkGenerator(net.Label, net.SystemName, true);

                    generator.AllowUser(NetCms.Users.AccountManager.CurrentAccount.ID);
                    bool result = generator.Generate();
                    if (result)
                    {
                        NetCms.GUI.PopupBox.AddMessage("Il Cms del Portale " + net.Label + " è stato creato con successo.", NetCms.GUI.PostBackMessagesType.Success);
                        NetCms.GUI.GuiUtility.RestartApplication();
                    }
                    else
                    {
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile creare il Portale " + net.Label, NetCms.GUI.PostBackMessagesType.Error);
                        foreach (string str in generator.Errors)
                            NetCms.GUI.PopupBox.AddMessage(str, PostBackMessagesType.Notify);
                    }
                }
                else
                {
                    if (net.CmsStatus == NetCms.Networks.NetworkCmsStates.Enabled) net.DisableCms();
                    else net.EnableCms();
                }
                _Network = null;
                NetCms.Users.AccountManager.UpdateCurrentUser();
            }


            #endregion

            #region Enable/Disable user Postback

            HtmlGenericControls.InputField enable = new HtmlGenericControls.InputField("EnableDisableNet");
            controls.Controls.Add(enable.Control);

            if (enable.RequestVariable.IsPostBack && enable.RequestVariable.IsValidInteger)
            {
                var net = new NetCms.Structure.WebFS.BasicStructureNetwork(enable.RequestVariable.IntValue);
                if (net.Enabled) net.DisableNetwork();
                else net.EnableNetwork();
                _Network = null;
                NetCms.Users.AccountManager.UpdateCurrentUser();
            }


            #endregion

            G2Core.UI.DetailsSheet ds = new G2Core.UI.DetailsSheet(Network.Label, "Da qui potrai gestire opzioni di questo portale quali: Creazione e Stato del CMS, Stato del Portale.");
            ds.SeparateColumns = G2Core.UI.DetailsSheet.SeparateColumnsOptions.All;

            ds.AddColumn("&nbsp;", G2Core.UI.DetailsSheet.Colors.Default);
            ds.AddColumn("Descrizione", G2Core.UI.DetailsSheet.Colors.Default);
            ds.AddColumn("Stato", G2Core.UI.DetailsSheet.Colors.Default);
            ds.AddColumn("Opzioni", G2Core.UI.DetailsSheet.Colors.Default);

            string cmsState = "";
            string Actionlabel = "";
            string ActionClass = "";
            switch (Network.CmsStatus)
            {
                case NetCms.Networks.NetworkCmsStates.Enabled:
                    cmsState = "Abilitato";
                    Actionlabel = "Disabilita";
                    ActionClass = "action stop"; 
                    break;
                case NetCms.Networks.NetworkCmsStates.NotCreated: 
                    cmsState = "Non Ancora Creato";
                    Actionlabel = "Crea CMS";
                    ActionClass = "action add";
                    break;
                case NetCms.Networks.NetworkCmsStates.Disabled: 
                    cmsState = "Disabilitato";
                    Actionlabel = "Abilita";
                    ActionClass = "action ok";
                    break;
            }


            TableRow row = new TableRow();

            TableCell cell = new TableCell();
            cell.Text = "Portale";
            row.Cells.Add(cell);

            cell = new TableCell();
            cell.Text = "Permette di Abilitare/Disabilitare l'intero portale, sia i contenuti (CMS) che le Applicazioni Di Portale";
            row.Cells.Add(cell);

            cell = new TableCell();
            cell.Text = (this.Network.Enabled ? "Abilitato" : "Disabilitato");
            row.Cells.Add(cell);

            cell = new TableCell();
            cell.Text = "<a class=\"" + "action " + (this.Network.Enabled ? "stop" : "ok") + "\" href=\"javascript: setFormValue('"+Network.ID+"','" + enable.ID + "',1)\"><span>" + (this.Network.Enabled ? "Disabilita" : "Abilita") + "</span></a>";
            row.Cells.Add(cell);

            ds.AddRow(row, G2Core.UI.DetailsSheet.Colors.Default);
            row = new TableRow();

            cell = new TableCell();
            cell.Text = "CMS";
            row.Cells.Add(cell);

            cell = new TableCell();
            cell.Text = "Permette di Abilitare/Disabilitare i contenuti del portale lasciando attive solo le Applicazioni Di Portale";
            row.Cells.Add(cell);

            cell = new TableCell();
            cell.Text = cmsState;
            row.Cells.Add(cell);

            cell = new TableCell();
            cell.Text = "<a class=\"" + ActionClass + "\" href=\"javascript: setFormValue('" + Network.SystemName + "','" + cms.ID + "',1)\"><span>" + Actionlabel + "</span></a>";
            row.Cells.Add(cell);

            ds.AddRow(row, G2Core.UI.DetailsSheet.Colors.Default);

            row = new TableRow();

            cell = new TableCell();
            cell.Text = "Eliminazione Portale";
            row.Cells.Add(cell);

            cell = new TableCell();
            cell.Text = "Permette di eliminare questo portale, ma ricorda che una volta eliminato non sarà possibile recuperarlo.";
            row.Cells.Add(cell);

            cell = new TableCell();
            cell.Text = "&nbsp;";
            row.Cells.Add(cell);

            cell = new TableCell();
            cell.Text = "<a class=\"action delete\" href=\"delete.aspx?nid="+Network.ID+"\"><span>Elimina Portale</span></a>";
            row.Cells.Add(cell);

            ds.AddRow(row, G2Core.UI.DetailsSheet.Colors.Default);

            controls.Controls.Add(ds);

            return controls;
        }

    }

}
