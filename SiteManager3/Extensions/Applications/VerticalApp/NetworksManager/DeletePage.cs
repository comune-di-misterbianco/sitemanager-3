﻿using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;


namespace NetCms.NetworksManager
{
    public class NetworksManagerDeletePage : NetCms.Vertical.ApplicationPageEx
    {
        public override string PageTitle
        {
            get { return "Gestione Reti » Eliminazione Portale"; }
        }
        public override WebControl Control
        {
            get
            {
                return BuildControls();
            }
        }

        public string PasswordCode
        {
            get
            {
                if (_PasswordCode == null)
                    _PasswordCode = "4HY53JIJ8297";
                return _PasswordCode;
            }
        }
        private string _PasswordCode;

        public NetUtility.RequestVariable NetworkID
        {
            get
            {
                if (_NetworkID == null)
                    _NetworkID = new NetUtility.QueryStringRequestVariable("nid");
                return _NetworkID;
            }
        }
        private NetUtility.RequestVariable _NetworkID;

        public Networks.INetwork Network
        {
            get
            {
                if (_Network == null)
                {
                    try
                    {
                        _Network = new NetCms.Structure.WebFS.BasicStructureNetwork(NetworkID.IntValue);
                    }
                    catch (Exception e) { }
                }
                return _Network;
            }
        }
        private Networks.INetwork _Network;


        public G2Core.AdvancedXhtmlForms.Fields.AxfPassword Password
        {
            get
            {
                if (_Password == null)
                    _Password = new G2Core.AdvancedXhtmlForms.Fields.AxfPassword("Password","password");
                return _Password;
            }
        }
        private G2Core.AdvancedXhtmlForms.Fields.AxfPassword _Password;

        public G2Core.XhtmlControls.InputField DeleteRequest
        {
            get
            {
                if (_DeleteRequest == null)
                    _DeleteRequest = new G2Core.XhtmlControls.InputField("delete");
                return _DeleteRequest;
            }
        }
        private G2Core.XhtmlControls.InputField _DeleteRequest;

        public NetworksManagerDeletePage(System.Web.UI.StateBag viewstate, bool IsPostBack, NetCms.GUI.Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewstate, IsPostBack, toolbar, informationBox, networkKey)
        {
            ApplicationContext.SetCurrentComponent(NetworksManager.ApplicationContextBackLabel);
            AddToolbarButtons();
        }

        protected void AddToolbarButtons()
        {
            NetCms.GUI.ToolbarButton button = new NetCms.GUI.ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Pagina Iniziale");
            this.Toolbar.Buttons.Add(button);
        }
        private WebControl BuildControls()
        {
            bool removed = false;
            if (DeleteRequest.RequestVariable.IsPostBack)
            {
                if (DeleteRequest.RequestVariable.IsValidInteger && Password.validateInput(Password.RequestVariable.StringValue) && Password.RequestVariable.StringValue == PasswordCode)
                {
                    NetCms.Networks.NetworkRemover remover = new NetCms.Networks.NetworkRemover(this.Network.ID, this.Network.SystemName);
                    removed = remover.DestroyNetwork();
                    if (removed)
                    {
                        NetCms.GUI.PopupBox.AddSessionMessage("Il Portale è stata rimosso correttamente correttamente", NetCms.GUI.PostBackMessagesType.Notify);
                        PageData.Redirect("default.aspx");
                    }
                    else
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile rimuovere il Portale, a causa di un errore di sistema.", NetCms.GUI.PostBackMessagesType.Error);
                }
                else
                    NetCms.GUI.PopupBox.AddMessage("Non è stata possibile eliminare il Portale. La password era errata o la password e la conferma non coincidono.", NetCms.GUI.PostBackMessagesType.Error);
            }

            NetCms.GUI.DetailsSheet controls;

            if (this.Network != null && !removed)
            {
                controls = new DetailsSheet("Richiesta di eliminazione Portale '" + this.Network.Label + "'", "Stai per eliminare un Portale, questa procedura è irreversibile. Il Portale eliminato non potrà in alcun modo essere recuperato.<br /><br />Per continuare inserisci la tua password di accesso e premi su bottone 'Si sono sicuro di voler eliminare il Portale.'");
                controls.Controls.Add(this.DeleteRequest.Control);

                DetailsSheetRow dsr1 = new DetailsSheetRow("Inserisci la password scritta a destra in entrambe le caselle sottostanti", this.PasswordCode);
                dsr1.CssClassRight = "big strong";
                controls.AddRow(dsr1);

                DetailsSheetRow row = new DetailsSheetRow("Password", "");
                row.ControlsToAppendRight.Add(this.Password.Control);
                controls.AddRow(row);

                DetailsSheetRow dsr = new DetailsSheetRow("", "Si sono sicuro di voler eliminare il Portale.", "error");
                dsr.LinkRight = "javascript: setFormValue('" + this.Network.ID + "','delete',1)";
                dsr.CssClassRight = "big strong";
                controls.AddRow(dsr);
            }
            else
            {
                controls = new DetailsSheet("Richiesta di eliminazione Portale", "");
                DetailsSheetRow dsr = new DetailsSheetRow("Stato Portale:", "Il Portale richiesto non esiste o è stato eliminato.", "notice");
                dsr.CssClassRight = "big strong";
                controls.AddRow(dsr);
            }
            return controls;
        }

    }

}
