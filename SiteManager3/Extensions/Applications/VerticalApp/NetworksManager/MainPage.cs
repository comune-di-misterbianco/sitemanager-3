﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetService.Utility.ArTable;
using System.Linq;
using NetCms.Networks;

namespace NetCms.NetworksManager
{
    public class NetworksManagerMainPage : NetCms.Vertical.VerticalApplicationMainPage
    {

        public override string PageTitle
        {
            get { return "Gestione Portali » Pagina Iniziale"; }
        }

        public NetworksManagerMainPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            ApplicationContext.SetCurrentComponent(NetworksManager.ApplicationContextBackLabel);
        }

        public override WebControl Control
        {
            get
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                /*
                #region CMS Postback

                HtmlGenericControls.InputField cms = new HtmlGenericControls.InputField("cmsRequest");
                value.Controls.Add(cms.Control);
                if (cms.RequestVariable.IsPostBack && NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.ContainsKey(cms.RequestVariable.StringValue))
                {
                    NetCms.Networks.BasicNetwork net = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks[cms.RequestVariable.StringValue];
                    if (net.CmsStatus == NetCms.Networks.NetworkCmsStates.NotCreated)
                    {
                        Networks.NetworkGenerator generator = new NetCms.Networks.NetworkGenerator(net.Label, net.SystemName, this.Connection, true);

                        generator.UsersToAllow.Add(NetCms.Users.AccountManager.CurrentAccount.ID);
                        bool result = generator.Generate();
                        if (result)
                            NetCms.GUI.PopupBox.AddMessage("Il Cms del Portale " + net.Label + " è stato creato con successo. Per permettere l'attivazione del Portale verrà riavviato l'intera applicazione, quindi sarà necessario riaffettuare il login.", NetCms.GUI.PostBackMessagesType.Success);
                        else
                        {
                            NetCms.GUI.PopupBox.AddMessage("Non è stato possibile creare il Portale " + net.Label, NetCms.GUI.PostBackMessagesType.Error);
                            foreach (string str in generator.Errors)
                                NetCms.GUI.PopupBox.AddMessage(str, PostBackMessagesType.Notify);
                        }
                    }
                    else
                    {
                        if (net.CmsStatus == NetCms.Networks.NetworkCmsStates.Enabled) net.DisableCms();
                        else net.EnableCms();
                    }
                }


                #endregion

                #region Enable/Disable user Postback

                HtmlGenericControls.InputField enable = new HtmlGenericControls.InputField("EnableDisableNet");
                value.Controls.Add(enable.Control);

                if (enable.RequestVariable.IsPostBack && enable.RequestVariable.IsValidInteger)
                {
                    NetCms.Networks.BasicNetwork net = new NetCms.Structure.WebFS.BasicStructureNetwork(enable.RequestVariable.IntValue);
                    if (net.Enabled) net.DisableNetwork();
                    else net.EnableNetwork();
                }
                

                #endregion
                */
                Toolbar.Buttons.Add(new ToolbarButton("add.aspx", NetCms.GUI.Icons.Icons.World_Add, "Aggiungi Nuovo Portale"));

                //ArTable tab = new ArTable(new NetCms.Networks.NetworksData(false));
                ArTable tab = new ArTable(NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.Select(x=>x.Value));

                //G2Core.AdvancedXhtmlTable.AxtTable tab = new G2Core.AdvancedXhtmlTable.AxtTable(new NetCms.Networks.NetworksData(false));
                tab.NoRecordMsg = "Non ci sono reti installate";
                
                //AGGIUNGERE LA PAGINAZIONE
                //tab.Pagination = true;
                tab.InnerTableCssClass = "tab";
                tab.RecordPerPagina = 25;

                tab.AddColumn(new ArTextColumn("Nome", "Label"));
                tab.AddColumn(new ArTextColumn("Codice", "SystemName"));
                tab.AddColumn(new ArOptionsColumn("Stato", "Enabled", new string[] { "Disablilitato","Abilitata" }));
                tab.AddColumn(new ArOptionsColumn("CMS", "StatoCMS", new string[] { "Non Esistente","Abilitato","Disabilitato" }));

                //tab.Columns.Add(new G2Core.AdvancedXhtmlTable.AxtGenericColumn("Label_Network", "Nome"));
                //tab.Columns.Add(new G2Core.AdvancedXhtmlTable.AxtGenericColumn("Nome_Network", "Codice"));
                //tab.Columns.Add(new G2Core.AdvancedXhtmlTable.AxtBooleanColumn("Enabled_Network", "Stato", "Disablilitato,Abilitata"));
                //tab.Columns.Add(new G2Core.AdvancedXhtmlTable.AxtBooleanColumn("Cms_Network", "CMS", "Non Esistente,Abilitato,Disabilitato"));
                /*
                G2Core.AdvancedXhtmlTable.AxtBooleanColumn.Option opt = new G2Core.AdvancedXhtmlTable.AxtBooleanColumn.Option();
                opt.CssClass = "action Add";
                opt.Label = "Crea CMS";
                opt.Link = "javascript: setFormValue('{Nome_Network}','" + cms.ID + "',1)";

                G2Core.AdvancedXhtmlTable.AxtBooleanColumn.Option opt2 = new G2Core.AdvancedXhtmlTable.AxtBooleanColumn.Option();
                opt2.CssClass = "action Abilita";
                opt2.Label = "Abilita";
                opt2.Link = "javascript: setFormValue('{Nome_Network}','" + cms.ID + "',1)";

                G2Core.AdvancedXhtmlTable.AxtBooleanColumn.Option opt3 = new G2Core.AdvancedXhtmlTable.AxtBooleanColumn.Option();
                opt3.CssClass = "action Disabilita";
                opt3.Label = "Disabilita";
                opt3.Link = "javascript: setFormValue('{Nome_Network}','" + cms.ID + "',1)";

                G2Core.AdvancedXhtmlTable.AxtBooleanColumn col = new G2Core.AdvancedXhtmlTable.AxtBooleanColumn("Cms_Network", "Stato CMS");
                col.Options.Add(opt);
                col.Options.Add(opt3);
                col.Options.Add(opt2);
                col.DatabaseHrefValueFields.Add("Nome_Network");
                tab.Columns.Add(col);


                tab.Columns.Add(new G2Core.AdvancedXhtmlTable.AxtBooleanColumn("Enabled_Network", "Stato Portale", "Disablilitata,Abilitata"));


                opt = new G2Core.AdvancedXhtmlTable.AxtBooleanColumn.Option();
                opt.CssClass = "action Abilita";
                opt.Label = "Abilita";
                opt.Link = "javascript: setFormValue('{id_Network}','" + enable.ID + "',1)";

                opt2 = new G2Core.AdvancedXhtmlTable.AxtBooleanColumn.Option();
                opt2.CssClass = "action Disabilita";
                opt2.Label = "Disabilita";
                opt2.Link = "javascript: setFormValue('{id_Network}','" + enable.ID + "',1)";

                col = new G2Core.AdvancedXhtmlTable.AxtBooleanColumn("Enabled_Network", "Stato");
                col.Options.Add(opt);
                col.Options.Add(opt2);
                col.DatabaseHrefValueFields.Add("id_Network");
                tab.Columns.Add(col);

                */
                tab.AddColumn(new ArActionColumn("Scheda e Opzioni", ArActionColumn.Icons.Details, "details.aspx?pkey={SystemName}"));
                //tab.Columns.Add(new G2Core.AdvancedXhtmlTable.AxtActionColumn("Nome_Network", "Scheda e Opzioni", "action details", "details.aspx?pkey={0}"));
                //tab.Columns.Add(new G2Core.AdvancedXhtmlTable.AxtActionColumn("id_Network", "Elimina", "action delete", "delete.aspx?nid={0}"));


                value.Controls.Add(tab);
                return value;
            }
        }
    }
   
}
