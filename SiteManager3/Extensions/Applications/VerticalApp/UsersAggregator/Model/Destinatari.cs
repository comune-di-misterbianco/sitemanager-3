﻿using System.Collections.Generic;

namespace UsersAggregator.Model
{
    public class Destinatari
    {
        public Destinatari()
        {
            Liste = new HashSet<Liste>();
        }

        public virtual int Id
        {
            get;
            set;
        }

        public virtual int IdUtente
        {
            get;
            set;
        }

        public virtual TipiUtente TipoUtente
        {
            get;
            set;
        }

        public enum TipiUtente
        {
            Cms = 1,
            Rubrica = 2
        }

        public virtual string TipoAnagrafe
        {
            get;
            set;
        }

        //public virtual string Cognome
        //{
        //    get;
        //    set;
        //}

        //public virtual string Nome
        //{
        //    get;
        //    set;
        //}

        public virtual string NomeCompleto
        {
            get;
            set;
        }

        public virtual string Email
        {
            get;
            set;
        }

        public virtual string Cellulare
        {
            get;
            set;
        }

        public virtual ICollection<Liste> Liste
        {
            get;
            set;
        }
    }
}
