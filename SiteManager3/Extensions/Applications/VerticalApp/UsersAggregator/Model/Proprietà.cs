﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UsersAggregator.Model
{
    public class Proprietà
    {
        public Proprietà()
        { }

        public virtual int Id
        {
            get;
            set;
        }

        public virtual string Value
        {
            get;
            set;
        }

        public virtual UtentiRubrica Utente
        {
            get;
            set;
        }

        public virtual CampiCustom CampoCustom
        {
            get;
            set;
        }

    }
}
