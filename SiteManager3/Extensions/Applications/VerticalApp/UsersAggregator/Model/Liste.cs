﻿using System.Collections.Generic;

namespace UsersAggregator.Model
{
    public class Liste
    {
        public Liste()
        {
            Destinatari = new HashSet<Destinatari>();
        }

        public virtual int Id
        {
            get;
            set;
        }

        public virtual string Nome
        {
            get;
            set;
        }

        public virtual ICollection<Destinatari> Destinatari
        {
            get;
            set;
        }
    }
}
