﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UsersAggregator.Model
{
    public class UtentiRubrica
    {
        public UtentiRubrica()
        {
            Proprietà = new HashSet<Proprietà>();
        }

        public virtual int Id
        {
            get;
            set;
        }

        //public virtual string Cognome
        //{
        //    get;
        //    set;
        //}

        //public virtual string Nome
        //{
        //    get;
        //    set;
        //}

        public virtual string NomeCompleto
        {
            get;
            set;
        }

        public virtual string Email
        {
            get;
            set;
        }

        public virtual string Cellulare
        {
            get;
            set;
        }

        public virtual ICollection<Proprietà> Proprietà
        {
            get;
            set;
        }
    }
}
