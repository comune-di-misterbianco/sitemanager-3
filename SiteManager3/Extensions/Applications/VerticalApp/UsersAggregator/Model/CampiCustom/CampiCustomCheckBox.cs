﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using NetService.Utility.ArTable;
using NetService.Utility.RecordsFinder;

namespace UsersAggregator.Model
{
    public class CampiCustomCheckBox : CampiCustom
    {
        public override VfGeneric VfField
        {
            get 
            {
                return new VfCheckBox(ControlId, Label) { Required = this.Required, BindField=false };
            }
        }

        public override SearchParameter SearchParameter
        {
            get
            {
                object value = null;
                if (new[] { "ON", "TRUE", "1" }.Any(x => x == VfField.Request.StringValue.ToUpper()))
                    value = "True";
                SearchParameter valueSearch = new SearchParameter(null, "Value", value, Finder.ComparisonCriteria.Equals, false);
                return new SearchParameter(null, "Proprietà", NetService.Utility.RecordsFinder.SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { valueSearch });
            }
        }
    }
}
