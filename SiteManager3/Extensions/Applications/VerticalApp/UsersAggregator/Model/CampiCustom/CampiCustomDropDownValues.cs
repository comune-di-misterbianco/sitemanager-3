﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UsersAggregator.Model
{
    public class CampiCustomDropDownValues
    {
        public CampiCustomDropDownValues()
        { }

        public virtual int Id
        {
            get;
            set;
        }

        public virtual string Value
        {
            get;
            set;
        }

        public virtual CampiCustomDropDown CampoDropDown
        {
            get;
            set;
        }
    }
}
