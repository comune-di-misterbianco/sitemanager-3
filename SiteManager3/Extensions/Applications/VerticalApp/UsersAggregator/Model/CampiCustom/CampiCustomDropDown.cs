﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using NetService.Utility.RecordsFinder;

namespace UsersAggregator.Model
{
    public class CampiCustomDropDown : CampiCustom
    {
        public virtual ICollection<CampiCustomDropDownValues> Values
        {
            get;
            set;
        }

        public override VfGeneric VfField
        {
            get 
            {
                VfDropDown dropdown = new VfDropDown(ControlId, Label) { Required = this.Required, BindField = false };
                dropdown.Options.Add("", "");
                for (int i = 0; i < Values.Count; i++)
                {
                    dropdown.Options.Add(Values.ElementAt(i).Value, Values.ElementAt(i).Value);
                }
                return dropdown;
            }
        }
    }
}
