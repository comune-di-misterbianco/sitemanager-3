﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using NetService.Utility.ArTable;
using NetService.Utility.RecordsFinder;

namespace UsersAggregator.Model
{
    public abstract class CampiCustom
    {
        public CampiCustom()
        {
            Proprietà = new HashSet<Proprietà>();
        }

        public virtual int Id
        {
            get;
            set;
        }

        public virtual string ControlId
        {
            get;
            set;
        }

        public virtual string Label
        {
            get;
            set;
        }

        public virtual bool Required
        {
            get;
            set;
        }
        
        public virtual ICollection<Proprietà> Proprietà
        {
            get;
            set;
        }

        public abstract VfGeneric VfField
        {
            get;
        }

        public virtual SearchParameter SearchParameter
        {
            get
            {
                object value = string.IsNullOrEmpty(VfField.Request.StringValue)? null:VfField.Request.StringValue;
                SearchParameter valueSearch = new SearchParameter(null, "Value", value, Finder.ComparisonCriteria.Like, false);
                return new SearchParameter(null, "Proprietà", NetService.Utility.RecordsFinder.SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { valueSearch });
            }
        }
    }
}
