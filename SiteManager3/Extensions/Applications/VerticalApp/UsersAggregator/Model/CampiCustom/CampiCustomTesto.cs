﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using NetService.Utility.ArTable;

namespace UsersAggregator.Model
{
    public class CampiCustomTesto : CampiCustom
    {
        public virtual int MaxLength
        {
            get;
            set;
        }

        public virtual InputTypes InputType
        {
            get;
            set;
        }

        public override VfGeneric VfField
        {
            get 
            {
                return new VfTextBox(ControlId, Label, InputType) { MaxLenght = this.MaxLength, Required = this.Required, BindField = false };
            }
        }
    }
}
