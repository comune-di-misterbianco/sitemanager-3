﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace UsersAggregator.Model.Mapping
{
    public class ListeMapping : ClassMap<Liste>
    {
        public ListeMapping()
        {
            Table("usersaggregator_liste");
            Id(x => x.Id);
            Map(x => x.Nome);
            HasManyToMany(x => x.Destinatari)
                .ChildKeyColumn("IDdestinatario")
                .ParentKeyColumn("IDlista")
                .Table("usersaggregator_destinatari_liste")
                .Fetch.Select()
                .AsSet()
                .Cascade.All();

        }
    }
}
