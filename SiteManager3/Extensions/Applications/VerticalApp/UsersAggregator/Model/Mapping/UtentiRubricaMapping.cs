﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace UsersAggregator.Model.Mapping
{
    public class UtentiRubricaMapping : ClassMap<UtentiRubrica>
    {
        public UtentiRubricaMapping()
        {
            Table("usersaggregator_utenti_rubrica");
            Id(x => x.Id);
            Map(x => x.NomeCompleto);
            Map(x => x.Cellulare);
            Map(x => x.Email);
            HasMany(x => x.Proprietà)
                .KeyColumn("IDutente_rubrica")
                .Fetch.Select().AsSet()
                .Cascade.All();
        }
    }
}
