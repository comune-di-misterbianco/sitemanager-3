﻿using FluentNHibernate.Mapping;

namespace UsersAggregator.Model.Mapping
{
    public class DestinatariMapping : ClassMap<Destinatari>
    {
        public DestinatariMapping()
        {
            Table("usersaggregator_destinatari");
            Id(x => x.Id);
            Map(x => x.IdUtente);
            Map(x => x.TipoAnagrafe);
            Map(x => x.NomeCompleto);
            Map(x => x.Email);
            Map(x => x.Cellulare);
            Map(x => x.TipoUtente);
            HasManyToMany(x => x.Liste)
                .ChildKeyColumn("IDlista")
                .ParentKeyColumn("IDdestinatario")
                .Table("usersaggregator_destinatari_liste")
                .Inverse()
                .Fetch.Select()
                .AsSet();
        }
    }
}
