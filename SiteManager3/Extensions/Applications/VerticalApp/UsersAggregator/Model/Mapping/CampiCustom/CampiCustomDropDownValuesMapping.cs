﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace UsersAggregator.Model.Mapping
{
    public class CampiCustomDropDownValuesMapping : ClassMap<CampiCustomDropDownValues>
    {
        public CampiCustomDropDownValuesMapping()
        {
            Table("usersaggregator_campi_custom_dropdown_values");
            Id(x => x.Id);
            Map(x => x.Value);
            References(x => x.CampoDropDown)
                .Column("IDcampi_custom_dropdown")
                .Fetch.Select();
        }
    }
}
