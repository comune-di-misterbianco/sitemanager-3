﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace UsersAggregator.Model.Mapping
{
    public class CampiCustomMapping : ClassMap<CampiCustom>
    {
        public CampiCustomMapping()
        {
            Table("usersaggregator_campi_custom");
            Id(x => x.Id);
            Map(x => x.ControlId);
            Map(x => x.Label);
            Map(x => x.Required);
            HasMany(x => x.Proprietà)
                .KeyColumn("IDcampo_custom")
                .Fetch.Select().AsSet()
                .Cascade.All();
        }
    }
}
