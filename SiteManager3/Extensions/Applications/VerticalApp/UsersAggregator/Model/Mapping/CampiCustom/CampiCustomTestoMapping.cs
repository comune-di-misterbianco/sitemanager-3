﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace UsersAggregator.Model.Mapping
{
    public class CampiCustomTestoMapping : SubclassMap<CampiCustomTesto>
    {
        public CampiCustomTestoMapping()
        {
            Table("usersaggregator_campi_custom_testo");
            KeyColumn("IDcampi_custom_testo");
            Map(x => x.MaxLength);
            Map(x => x.InputType);
        }
    }
}
