﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace UsersAggregator.Model.Mapping
{
    public class CampiCustomCheckBoxMapping : SubclassMap<CampiCustomCheckBox>
    {
        public CampiCustomCheckBoxMapping()
        {
            Table("usersaggregator_campi_custom_checkbox");
            KeyColumn("IDcampi_custom_checkbox");
        }
    }
}
