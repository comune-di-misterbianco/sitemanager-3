﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace UsersAggregator.Model.Mapping
{
    public class CampiCustomDropDownMapping : SubclassMap<CampiCustomDropDown>
    {
        public CampiCustomDropDownMapping()
        {
            Table("usersaggregator_campi_custom_dropdown");
            KeyColumn("IDcampi_custom_dropdown");
            HasMany(x => x.Values)
                .KeyColumn("IDcampi_custom_dropdown")
                .Fetch.Select()
                .AsSet()
                .Cascade.All();
        }
    }
}
