﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace UsersAggregator.Model.Mapping
{
    public class ProprietàMapping : ClassMap<Proprietà>
    {
        public ProprietàMapping()
        {
            Table("usersaggregator_proprieta");
            Id(x => x.Id);
            Map(x => x.Value);
            References(x => x.Utente)
                .Column("IDutente_rubrica")
                .Fetch.Select();
            References(x => x.CampoCustom)
                .Column("IDcampo_custom")
                .Fetch.Select();
        }
    }
}
