﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using NetCms.Vertical;
using NetCms.DBSessionProvider;
using System.Reflection;
using NHibernate;
using NetCms.Vertical.BusinessLogic;
using NetCms.Diagnostics;

namespace UsersAggregator
{
    public class UsersAggregatorSetup : VerticalApplicationSetup
    {
        public override string[,] Pages
        {
            get
            {
                return null;
            }
        }

        public override string[,] Criteria
        {
            get 
            {
                return null;
            }
        }

        public override bool IsNetworkDependant
        {
            get { return false; }
        }

        public UsersAggregatorSetup(VerticalApplication baseApp)
            : base(baseApp)
        {
        }

        protected override bool LocalInstall(bool isInsidePackage = false)
        {
            try
            {
                FluentSessionProvider.Instance.UpdateSessionFactoryAddRange(ApplicationBaseData.FluentAssembliesRelated);
            }
            catch (Exception ex)
            {
                this.RollBack();
                return false;
            }
            return true;
        }

        public override bool Uninstall()
        {
            bool ok = base.Uninstall();
            if (ok)
                FluentSessionProvider.Instance.UpdateSessionFactoryRemove(Assembly.GetAssembly(this.GetType()));
            return ok;
        }

        private void RollBack()
        {
            using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    VerticalApplication vapp = VerticalAppBusinessLogic.GetVerticalAppById(this.ApplicationBaseData.ID, sess);
                    if (vapp != null)
                    {
                        vapp.Pages.Clear();
                        vapp.AppzCriteria.Clear();

                        VerticalAppBusinessLogic.DeleteVerticalApp(vapp, sess);
                    }
                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                }
            }
        }
    }
}
