﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using NetCms.Users;
using UsersAggregator.Model;
using GenericDAO.DAO;
using NHibernate;
using NetService.Utility.RecordsFinder;
using NetService.Utility.ValidatedFields;
using NetCms.DBSessionProvider;
using System.Collections;

namespace UsersAggregator
{
    public static class BusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        //public static string GetObjectPropertyByName(object obj, string propertyName)
        //{
        //    string value ="";
        //    try
        //    {
        //        PropertyInfo propNome = obj.GetType().GetProperty(propertyName);
        //        value = propNome.GetValue(obj, null).ToString();
        //    }
        //    catch { return ""; }
        //    return value;
        //}

        public static Liste AddCmsUserToTempList(Liste lista, int idCmsUser,ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            //User utente = NetCms.Users.User.Fabricate(idCmsUser.ToString());
            User utente = UsersBusinessLogic.GetById(idCmsUser, innerSession);
            Hashtable anagrafeUtente = utente.AnagraficaMapped;
            Destinatari destinatario = GetDestinatariByIdUser(idCmsUser,innerSession);
            if (destinatario == null)
            {
                destinatario = new Destinatari();
                destinatario.IdUtente = idCmsUser;
                destinatario.TipoUtente = Destinatari.TipiUtente.Cms;
                destinatario.NomeCompleto = utente.NomeCompleto;
                destinatario.TipoAnagrafe = utente.AnagraficaType;
                if (anagrafeUtente.ContainsKey("Cellulare") && anagrafeUtente["Cellulare"]!=null)
                {
                    destinatario.Cellulare = anagrafeUtente["Cellulare"].ToString();
                }
                destinatario.Email = anagrafeUtente["Email"].ToString();
            }
            else
            {
                //Codice aggiunto per gestire il caso in cui il destinatario esista ma sono state cambiate mail e/o cellulare
                if (destinatario.Email != anagrafeUtente["Email"].ToString())
                    destinatario.Email = anagrafeUtente["Email"].ToString();
                if (anagrafeUtente.ContainsKey("Cellulare"))
                {
                    if (anagrafeUtente["Cellulare"] != null && destinatario.Cellulare != anagrafeUtente["Cellulare"].ToString())
                        destinatario.Cellulare = anagrafeUtente["Cellulare"].ToString();
                }
            }
            lista.Destinatari.Add(destinatario);
            destinatario.Liste.Add(lista);
            return lista;
        }

        public static Liste RemoveCmsUserFromTempList(Liste lista, int idCmsUser, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            Destinatari destinatario = GetDestinatariByIdUser(idCmsUser, innerSession);
            lista.Destinatari.Remove(destinatario);
            destinatario.Liste.Remove(lista);
            return lista;
        }

        public static Liste AddRubricaUserToTempList(Liste lista, int idRubricaUser, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            UtentiRubrica utente = GetUtenteRubricaById(idRubricaUser, innerSession);
            Destinatari destinatario = new Destinatari();
            destinatario.IdUtente = idRubricaUser;
            destinatario.TipoUtente = Destinatari.TipiUtente.Rubrica;
            destinatario.NomeCompleto = utente.NomeCompleto;
            destinatario.TipoAnagrafe = null;
            destinatario.Cellulare = utente.Cellulare;
            destinatario.Email = utente.Email;
            lista.Destinatari.Add(destinatario);
            destinatario.Liste.Add(lista);
            return lista;
        }

        public static UtentiRubrica GetUtenteRubricaById(int idRubricaUser,ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<UtentiRubrica> dao = new CriteriaNhibernateDAO<UtentiRubrica>(innerSession);
            return dao.GetById(idRubricaUser);
        }

        public static Destinatari GetDestinatariByIdUser(int idUser, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Destinatari> dao = new CriteriaNhibernateDAO<Destinatari>(innerSession);
            SearchParameter idUserParam = new SearchParameter(null, "IdUtente", idUser, Finder.ComparisonCriteria.Equals, false);
            return dao.GetByCriteria(new SearchParameter[] { idUserParam });
        }

        public static bool SaveOrUpdateLista(Liste lista,ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Liste> dao = new CriteriaNhibernateDAO<Liste>(innerSession);
            return (dao.SaveOrUpdate(lista) != null);
        }

        public static int CountListe()
        {
            IGenericDAO<Liste> dao = new CriteriaNhibernateDAO<Liste>(GetCurrentSession());
            return dao.FindByCriteriaCount(null);
        }

        public static Liste GetListaByName(string nome)
        {
            IGenericDAO<Liste> dao = new CriteriaNhibernateDAO<Liste>(GetCurrentSession());
            SearchParameter nomeParam = new SearchParameter(null, "Nome", nome, Finder.ComparisonCriteria.Equals, false);
            return dao.GetByCriteria(new SearchParameter[] { nomeParam });
        }

        public static Liste GetListaById(int idLista, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Liste> dao = new CriteriaNhibernateDAO<Liste>(innerSession);
            return dao.GetById(idLista);
        }

        public static IList<Liste> FindListe(int pageStart, int pageSize,string sortBy, bool descending)
        {
            IGenericDAO<Liste> dao = new CriteriaNhibernateDAO<Liste>(GetCurrentSession());
            return dao.FindAll(pageStart,pageSize,sortBy,descending);
        }

        public static void DeleteLista(Liste lista)
        {
            IGenericDAO<Liste> dao = new CriteriaNhibernateDAO<Liste>(GetCurrentSession());
            dao.Delete(lista);
        }

        public static void DeleteUtenteRubrica(UtentiRubrica utente)
        {
            IGenericDAO<UtentiRubrica> dao = new CriteriaNhibernateDAO<UtentiRubrica>(GetCurrentSession());
            dao.Delete(utente);
        }

        public static int CountUtentiInRubrica(SearchParameter[] searchParameters)
        {
            IGenericDAO<UtentiRubrica> dao = new CriteriaNhibernateDAO<UtentiRubrica>(GetCurrentSession());
            return dao.FindByCriteriaCount(searchParameters);
        }

        public static IList<UtentiRubrica> FindUtentiInRubrica(int pageStart, int pageSize, string sortBy, bool descending, SearchParameter[] searchParameters)
        {
            IGenericDAO<UtentiRubrica> dao = new CriteriaNhibernateDAO<UtentiRubrica>(GetCurrentSession());
            return dao.FindByCriteria(((pageStart - 1) * pageSize),
                                            pageSize,
                                            sortBy,
                                            descending,
                                            searchParameters);
        }

        public static IList<CampiCustom> FindCampiCustom()
        {
            IGenericDAO<CampiCustom> dao = new CriteriaNhibernateDAO<CampiCustom>(GetCurrentSession());
            return dao.FindAll();
        }

        public static bool CreateUtenteInRubrica(VfManager manager)
        {
            bool status = false;
            try
            {
                UtentiRubrica utente = new UtentiRubrica();
                bool filled = manager.FillObject(utente);

                IGenericDAO<CampiCustom> campiDao = new CriteriaNhibernateDAO<CampiCustom>(BusinessLogic.GetCurrentSession());

                ICollection<Proprietà> proprietà = new HashSet<Proprietà>();
                foreach (VfGeneric customField in manager.Fields.Where(x => x.BindField == false))
                {
                    Proprietà prop = new Proprietà();
                    prop.Utente = utente;
                    prop.Value = customField.PostbackValueObject.ToString();

                    SearchParameter nomeCampoSearchParameter = new SearchParameter(null,"Label",customField.Label, Finder.ComparisonCriteria.Equals,false);
                    CampiCustom campo = campiDao.GetByCriteria(new SearchParameter[] { nomeCampoSearchParameter });

                    prop.CampoCustom = campo;
                    proprietà.Add(prop);
                }

                utente.Proprietà = proprietà;

                IGenericDAO<UtentiRubrica> utentiDao = new CriteriaNhibernateDAO<UtentiRubrica>(BusinessLogic.GetCurrentSession());
                utentiDao.SaveOrUpdate(utente);
                
                status = true;
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la creazione del record " + typeof(UtentiRubrica) + ". " + ex.Message);
            }
            return status;
        }

        public static bool ModUtenteInRubrica(VfManager manager, UtentiRubrica utente)
        {
            bool status = false;
            try
            {
                bool filled = manager.FillObject(utente);

                foreach (VfGeneric customField in manager.Fields.Where(x => x.BindField == false))
                {
                    utente.Proprietà.First(x => x.CampoCustom.Label == customField.Label).Value = customField.PostbackValueObject.ToString();
                }

                IGenericDAO<UtentiRubrica> utentiDao = new CriteriaNhibernateDAO<UtentiRubrica>(BusinessLogic.GetCurrentSession());
                utentiDao.SaveOrUpdate(utente);

                status = true;
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la modifica del record " + typeof(UtentiRubrica) + " con id " + utente.Id + ". " + ex.Message);
            }
            return status;
        }

        public static bool UtentePresenteInInsert(string email, string cellulare)
        {
            IGenericDAO<UtentiRubrica> utentiDao = new CriteriaNhibernateDAO<UtentiRubrica>(BusinessLogic.GetCurrentSession());
            SearchParameter emailSearchParameter = new SearchParameter(null, "Email", email, Finder.ComparisonCriteria.Equals, false);
            SearchParameter cellulareSearchParameter = new SearchParameter(null, "Cellulare", cellulare, Finder.ComparisonCriteria.Equals, false);

            return utentiDao.GetByCriteria(new SearchParameter[] { emailSearchParameter }) != null || utentiDao.GetByCriteria(new SearchParameter[] { cellulareSearchParameter }) != null;
        }

        public static bool UtentePresenteInUpdate(UtentiRubrica utente)
        {
            IGenericDAO<UtentiRubrica> utentiDao = new CriteriaNhibernateDAO<UtentiRubrica>(BusinessLogic.GetCurrentSession());
            SearchParameter idSearchParameter = new SearchParameter(null, "Id", utente.Id, Finder.ComparisonCriteria.Not, false);
            SearchParameter emailSearchParameter = new SearchParameter(null, "Email", utente.Email, Finder.ComparisonCriteria.Equals, false);
            SearchParameter cellulareSearchParameter = new SearchParameter(null, "Cellulare", utente.Cellulare, Finder.ComparisonCriteria.Equals, false);

            return utentiDao.GetByCriteria(new SearchParameter[] { emailSearchParameter, idSearchParameter }) != null || utentiDao.GetByCriteria(new SearchParameter[] { cellulareSearchParameter, idSearchParameter }) != null;
        }

        
    }
}
