﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using NetCms.Vertical;


namespace UsersAggregator
{
    public class UsersAggregator : VerticalApplication
    {
        public override VerticalApplication.AccessModes AccessMode
        {
            get { return AccessModes.Admin; }
        }

        public NetCms.Vertical.Configuration.Configuration Configuration
        {
            get
            {
                return _Configuration ?? (_Configuration = new NetCms.Vertical.Configuration.Configuration(this.SystemName));
            }
        }
        private NetCms.Vertical.Configuration.Configuration _Configuration;

        public override int HomepageGroup
        {
            get { return 2; }
        }

        public const string SystemNameKey = "usersaggregator";
        public const int IDKey = 2001;

        public override bool CheckForDefaultGrant
        {
            get { return false; }
        }

        public override string Description
        {
            get { return "ListeUtenti"; }
        }

        public override int ID
        {
            get { return IDKey; }
        }

        public override string Label
        {
            get { return "Liste Utenti"; }
        }

        public override bool NetworkDependant
        {
            get { return false; }
        }

        public override int SecurityLevel
        {
            get { return 0; }
        }

        public override string SystemName
        {
            get { return "usersaggregator"; }
        }

        public override bool DoAdditionalConfigurationForExternalDB()
        {
            throw new NotImplementedException();
        }

        public override bool NeedAdditionalConfiguration
        {
            get { return false; }
        }

        public override VerticalApplicationSetup Installer
        {
            get
            {
                if (_Installer == null)
                    _Installer = new UsersAggregatorSetup(this);
                return _Installer;
            }
        }
        private UsersAggregatorSetup _Installer;       

        public UsersAggregator() : base()
        {
           
        }

        public UsersAggregator(Assembly assembly)
            : base(assembly)
        {
        }

        private string _BackofficeUrl;
        public string BackofficeUrl
        {
            get 
            {
                return "/applications/" + SystemNameKey + "/admin/default.aspx";
            }
        }

        public override bool UseCmsSessionFactory
        {
            get { return true; }
        }

        public override ICollection<Assembly> FluentAssembliesRelated
        {
            get
            {
                ICollection<Assembly> coll = new List<Assembly>();
                coll.Add(this.Assembly);
                return coll;
            }
        }
    }
}
