﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetService.Utility.ValidatedFields;
using UsersAggregator;
using UsersAggregator.Model;

namespace UsersAggregator.View
{
    [DynamicUrl.PageControl("/rubrica/add.aspx")]
    public class UtenteRubricaNew : UsersAggregatorPage
    {
        public override string PageTitle
        {
            get { return "Nuovo Utente in Rubrica"; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Book, "Gestione Rubrica"));
        }

        public UtenteRubricaNew(StateBag viewstate, bool IsPostBack, NetCms.GUI.Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewstate, IsPostBack, toolbar, informationBox, networkKey)
        {
            SetToolbars();
            this.UtenteForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.UtenteForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                bool presente = false;
                string cellulare = UtenteForm.Fields.Find(x => x.Key == "Cellulare").PostBackValue;
                string email = UtenteForm.Fields.Find(x => x.Key == "Email").PostBackValue;
                if (BusinessLogic.UtentePresenteInInsert(email, cellulare))
                {
                    presente = true;
                    NetCms.GUI.PopupBox.AddMessage("Utente (email o cellulare) già presente.", PostBackMessagesType.Notify);
                }

                if (!presente)
                {
                    bool status = BusinessLogic.CreateUtenteInRubrica(this.UtenteForm);
                    if (status)
                        NetCms.GUI.PopupBox.AddSessionMessage("Utente inserito con successo.", PostBackMessagesType.Success, true);
                    else
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
                }
            }
        }

        public UtenteRubricaForm UtenteForm
        {
            get
            {
                if (_UtenteForm == null)
                {
                    _UtenteForm = new UtenteRubricaForm("UtenteForm");
                    _UtenteForm.CssClass = "Axf";
                    _UtenteForm.SubmitButton.Text = "Salva";
                    foreach (CampiCustom campo in BusinessLogic.FindCampiCustom())
                    {
                        _UtenteForm.Fields.Add(campo.VfField);
                    }
                }
                return _UtenteForm;
            }
        }
        private UtenteRubricaForm _UtenteForm;

        public override WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.Controls.Add(UtenteForm);
            return control;
        }
    }
}
