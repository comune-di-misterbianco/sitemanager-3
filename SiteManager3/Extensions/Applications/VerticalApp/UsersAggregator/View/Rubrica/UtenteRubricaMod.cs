﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetService.Utility.ValidatedFields;
using UsersAggregator;
using UsersAggregator.Model;
using NetService.Utility.Common;
using System.Linq;
using GenericDAO.DAO.Utility;

namespace UsersAggregator.View
{
    [DynamicUrl.PageControl("/rubrica/mod.aspx")]
    public class UtenteRubricaMod : UsersAggregatorPage
    {
        public override string PageTitle
        {
            get { return "Modifica Utente in Rubrica"; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Book, "Gestione Rubrica"));
        }

        public NhRequestObject<UtentiRubrica> Utente { get; private set; }

        public UtenteRubricaMod(StateBag viewstate, bool IsPostBack, NetCms.GUI.Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewstate, IsPostBack, toolbar, informationBox, networkKey)
        {
            Utente = new NhRequestObject<UtentiRubrica>(BusinessLogic.GetCurrentSession(), "utente");
            this.StatusValidators.Add(new NhRequestObjectValidator<UtentiRubrica>(Utente));
            SetToolbars();
            this.UtenteForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.UtenteForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                bool presente = false;
                if (BusinessLogic.UtentePresenteInUpdate(Utente.Instance))
                {
                    presente = true;
                    NetCms.GUI.PopupBox.AddMessage("Utente (email o cellulare) già presente.", PostBackMessagesType.Notify);
                }

                if (!presente)
                {
                    bool status = BusinessLogic.ModUtenteInRubrica(this.UtenteForm, Utente.Instance);
                    if (status)
                        NetCms.GUI.PopupBox.AddSessionMessage("Utente modificato con successo.", PostBackMessagesType.Success, true);
                    else
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
                }
            }
        }

        public UtenteRubricaForm UtenteForm
        {
            get
            {
                if (_UtenteForm == null)
                {
                    _UtenteForm = new UtenteRubricaForm("UtenteForm");
                    _UtenteForm.CssClass = "Axf";
                    _UtenteForm.SubmitButton.Text = "Modifica";
                    foreach (CampiCustom campo in BusinessLogic.FindCampiCustom())
                    {
                        _UtenteForm.Fields.Add(campo.VfField);
                        if (campo is CampiCustomCheckBox)
                            _UtenteForm.Fields.Find(x => x.Label == campo.Label).DefaultValue = bool.Parse(Utente.Instance.Proprietà.First(x => x.CampoCustom.Label == campo.Label).Value);
                        else _UtenteForm.Fields.Find(x => x.Label == campo.Label).DefaultValue = Utente.Instance.Proprietà.First(x => x.CampoCustom.Label == campo.Label).Value;
                    }
                    UtenteForm.DataBind(Utente.Instance);
                }
                return _UtenteForm;
            }
        }
        private UtenteRubricaForm _UtenteForm;

        public override WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.Controls.Add(UtenteForm);
            return control;
        }
    }
}
