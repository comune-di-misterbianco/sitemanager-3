﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using UsersAggregator.Model;
using NetService.Utility.Common;
using NetService.Utility.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GenericDAO.DAO.Utility;

namespace UsersAggregator.View
{
    [DynamicUrl.PageControl("/rubrica/default.aspx")]
    public class UtentiRubricaList : UsersAggregatorPage
    {
        public override string PageTitle
        {
            get { return "Gestione Rubrica"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Book; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("add.aspx", NetCms.GUI.Icons.Icons.Book_Add, "Crea Nuovo Utente Rubrica"));
            this.Toolbar.Buttons.Add(new ToolbarButton("../default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Gestione Liste"));
        }

        public NhRequestObject<UtentiRubrica> UtenteRubrica { get; private set; }

        public UtentiRubricaList(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            UtenteRubrica = new NhRequestObject<UtentiRubrica>(BusinessLogic.GetCurrentSession(), "utente");
            SetToolbars();
        }

        public override WebControl BuildControl()
        {
            if (EliminationRequest && UtenteRubrica.Exists)
            {
                EliminationConfirmControl delcontrol = DeleteControl("Conferma eliminazione utente '" + UtenteRubrica.Instance.NomeCompleto + "'", "Sei sicuro di voler eliminare l'utente?", NetCms.Vertical.PagesHandler.GetPageAddress(typeof(UtentiRubricaList)));
                delcontrol.ConfirmButton.Click += new EventHandler(ConfirmButton_Click);
                return delcontrol;
            }
            return GetTableControl();
        }

        void ConfirmButton_Click(object sender, EventArgs e)
        {
            BusinessLogic.DeleteUtenteRubrica(UtenteRubrica.Instance);
            NetCms.GUI.PopupBox.AddSessionMessage("Utente eliminato con successo", PostBackMessagesType.Success, true);
        }

        private WebControl GetTableControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);

            ArTable utentiRubricaTable = new ArTable();
            utentiRubricaTable.EnablePagination = true;
            utentiRubricaTable.RecordPerPagina = 20;
            utentiRubricaTable.InnerTableCssClass = "tab";
            utentiRubricaTable.NoRecordMsg = "Nessun utente in rubrica";

            PaginationHandler pagination = new PaginationHandler(20, BusinessLogic.CountUtentiInRubrica(null), utentiRubricaTable.PaginationKey);
            utentiRubricaTable.PagesControl = pagination;

            var liste = BusinessLogic.FindUtentiInRubrica(((pagination.CurrentPage - 1) * pagination.PageSize), pagination.PageSize, "NomeCompleto", false,null);

            utentiRubricaTable.Records = liste;

            ArTextColumn nomeUtenteInRubrica = new ArTextColumn("Nome Completo", "NomeCompleto");
            utentiRubricaTable.AddColumn(nomeUtenteInRubrica);

            IList<CampiCustom> listaCampiCustom = BusinessLogic.FindCampiCustom();
            for (int i=0; i< listaCampiCustom.Count; i++)
            {
                ArCampiCustomColumn customColumn = new ArCampiCustomColumn(listaCampiCustom.ElementAt(i).Label, "Proprietà", i);
                utentiRubricaTable.AddColumn(customColumn);
            }

            ArActionColumn modifyUtenteInRubrica = new ArActionColumn("Modifica", ArActionColumn.Icons.Edit, "mod.aspx?utente={Id}");
            utentiRubricaTable.AddColumn(modifyUtenteInRubrica);

            ArActionColumn deleteUtenteInRubrica = new ArActionColumn("Elimina", ArActionColumn.Icons.Delete, "default.aspx?action=delete&utente={Id}");
            utentiRubricaTable.AddColumn(deleteUtenteInRubrica);

            control.Controls.Add(utentiRubricaTable);

            return control;

        }
    }
}
