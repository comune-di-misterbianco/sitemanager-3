﻿using System.Web.UI.WebControls;
using System;
using NetCms.Users;
using System.Reflection;
using System.Collections.Generic;
using UsersAggregator.Model;
using System.Linq;


namespace NetService.Utility.ArTable
{
    public class ArCampiCustomColumn : ArTextColumn
    {
        private int Indice;

        public ArCampiCustomColumn(string caption, string key, int indice)
            : base(caption, key)
        {
            Indice = indice;
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            ICollection<Proprietà> proprietà = (ICollection<Proprietà>)GetValue(objectInstance, Key);
            string value = proprietà.ElementAt(Indice).Value;

            if (value.Equals("True", StringComparison.CurrentCultureIgnoreCase))
                value = "SI";

            if (value.Equals("False", StringComparison.CurrentCultureIgnoreCase))
                value = "NO";

            TableCell td = new TableCell();
            if (!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;

            td.Text = "<span>" + value + "</span>";
            return td;
        }

        public override string GetExportValue(object obj)
        {
            ICollection<Proprietà> proprietà = (ICollection<Proprietà>)GetValue(obj, Key);
            string value = proprietà.ElementAt(Indice).Value;
            return value;
        }
    }
}
