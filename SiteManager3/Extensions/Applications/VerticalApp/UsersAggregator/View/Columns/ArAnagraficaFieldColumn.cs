﻿using System.Web.UI.WebControls;
using System;
using NetCms.Users;
using System.Reflection;
using System.Collections;

namespace NetService.Utility.ArTable
{
    public class ArAnagraficaFieldColumn : ArTextColumn
    {
        public ArAnagraficaFieldColumn(string caption, string key)
            : base(caption, key)
        {
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            int idUser = int.Parse(FilterCellValue(FilterDataType(GetValue(objectInstance, "id_User"))));

            TableCell td = new TableCell();
            if (!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;

            string value = "";
            try
            {
                if (idUser > 0)
                {
                    Hashtable anagrafica = UsersBusinessLogic.GetAnagraficaMapUser(UsersBusinessLogic.GetById(idUser));

                    if (anagrafica != null && anagrafica.ContainsKey(Key))
                    {
                        value = anagrafica[Key].ToString();
                    }
                }
                td.Text = "<span>" + value + "</span>";
            }
            catch { }
            return td;
        }

        public override string GetExportValue(object obj)
        {
            int idUser = int.Parse(FilterCellValue(FilterDataType(GetValue(obj, "id_User"))));

            string value = "";

            Hashtable anagrafica = UsersBusinessLogic.GetAnagraficaMapUser(UsersBusinessLogic.GetById(idUser));

            try
            {
                if (anagrafica != null)
                {
                    if (anagrafica != null && anagrafica.ContainsKey(Key))
                    {
                        value = anagrafica[Key].ToString();
                    }
                }
            }
            catch { }
            return value;
        }
    }
}
