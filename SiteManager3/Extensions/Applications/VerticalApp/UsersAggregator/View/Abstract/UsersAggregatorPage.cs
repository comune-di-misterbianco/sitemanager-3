﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using NetCms.GUI;
using System.Web.UI.WebControls;
using G2Core.Common;
using NetService.Utility.UI;

namespace UsersAggregator.View
{
    public abstract class UsersAggregatorPage : NetCms.Vertical.ApplicationPageEx
    {
        public UsersAggregatorPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
        }

        public override string PageTitle
        {
            get { return "Creazione Liste Utenti &raquo; "; }
        }

        private UsersAggregator _Application;
        public UsersAggregator Application
        {
            get
            {
                if (_Application == null)
                {
                    _Application = new UsersAggregator();
                }
                return _Application;
            }

        }

        public List<ApplicationPageStatusValidator> StatusValidators
        {
            get
            {
                return _StatusValidators ?? (_StatusValidators = new List<ApplicationPageStatusValidator>());
            }
        }
        private List<ApplicationPageStatusValidator> _StatusValidators;

        public ViewsTypes CurrentView { get; private set; }
        public enum ViewsTypes { Default, Error }

        public ApplicationPageStatusValidator StatusValidatorInException { get; private set; }

        public abstract WebControl BuildControl();

        public override WebControl Control
        {
            get
            {
                CheckStatus();
                switch (this.CurrentView)
                {
                    case ViewsTypes.Error: return GetErrorControl();
                    default:
                        return BuildControl();
                }
            }
        }

        private WebControl GetErrorControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.CssClass = "usersaggregator_fieldset";
            control.Controls.Add(new TextWebControl(HtmlTextWriterTag.Legend, StatusValidatorInException.ErrorTitle));
            var p = new TextWebControl(HtmlTextWriterTag.P, StatusValidatorInException.ErrorMessage);
            control.Controls.Add(p);

            WebControl ul = new WebControl(HtmlTextWriterTag.Ul);
            control.Controls.Add(ul);

            WebControl li = new WebControl(HtmlTextWriterTag.Li);
            ul.Controls.Add(li);

            HyperLink a = new HyperLink()
            {
                NavigateUrl = NetCms.Vertical.PagesHandler.GetPageAddress(typeof(MainPage)),
                Text = "Indietro"
            };
            li.Controls.Add(a);
            return control;
        }

        protected WebControl GetErrorControl(string titoloErrore, string messaggioErrore)
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.CssClass = "usersaggregator_fieldset";
            control.Controls.Add(new TextWebControl(HtmlTextWriterTag.Legend, titoloErrore));
            var p = new TextWebControl(HtmlTextWriterTag.P, messaggioErrore);
            control.Controls.Add(p);
            return control;
        }

        protected WebControl GetErrorControl(string titoloErrore, string messaggioErrore, string urlBack)
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.CssClass = "usersaggregator_fieldset";
            control.Controls.Add(new TextWebControl(HtmlTextWriterTag.Legend, titoloErrore));
            var p = new TextWebControl(HtmlTextWriterTag.P, messaggioErrore);
            control.Controls.Add(p);

            HyperLink backLink = new HyperLink();
            backLink.NavigateUrl = urlBack;
            backLink.Text = "Torna Indietro";
            control.Controls.Add(backLink);

            return control;
        }

        protected EliminationConfirmControl DeleteControl(string titoloMessaggio, string messaggio, string urlBack)
        {
            EliminationConfirmControl control = new EliminationConfirmControl(titoloMessaggio, messaggio, urlBack);
            return control;
        }

        public bool EliminationRequest
        {
            get
            {
                RequestVariable request = new RequestVariable("action", RequestVariable.RequestType.QueryString);
                if (request.IsValidString && request.StringValue == "delete")
                    _EliminationRequest = true;
                return _EliminationRequest;
            }
        }
        private bool _EliminationRequest = false;

        private void CheckStatus()
        {
            StatusValidatorInException = ValidateStatus();
            if (StatusValidatorInException == null)
                OnPageValidated(EventArgs.Empty);

            bool error = StatusValidatorInException == null;
            if (!error) CurrentView = ViewsTypes.Error;

        }

        public event EventHandler PageValidated;
        protected virtual void OnPageValidated(EventArgs e)
        {
            if (PageValidated != null)
                PageValidated(this, e);
        }

        protected ApplicationPageStatusValidator ValidateStatus()
        {
            return StatusValidators.FirstOrDefault(validator => !validator.Validate());
        }
    }
}