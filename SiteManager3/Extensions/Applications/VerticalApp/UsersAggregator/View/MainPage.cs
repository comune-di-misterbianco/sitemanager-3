﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using UsersAggregator.Model;
using NetService.Utility.Common;
using NetService.Utility.UI;
using GenericDAO.DAO.Utility;

namespace UsersAggregator.View
{
    [DynamicUrl.PageControl("/default.aspx")]
    public class MainPage : UsersAggregatorPage
    {
        public override string PageTitle
        {
            get { return "Liste Utenti"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("lista_new.aspx", NetCms.GUI.Icons.Icons.Vcard_Add, "Crea Lista Utenti"));
            this.Toolbar.Buttons.Add(new ToolbarButton("rubrica/default.aspx", NetCms.GUI.Icons.Icons.Book, "Gestione Rubrica"));
        }

        public NhRequestObject<Liste> Lista { get; private set; }

        public MainPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            Lista = new NhRequestObject<Liste>(BusinessLogic.GetCurrentSession(), "lista");
            SetToolbars();
        }

        public override WebControl BuildControl()
        {
            if (EliminationRequest && Lista.Exists)
            {
                EliminationConfirmControl delcontrol = DeleteControl("Conferma eliminazione Lista '" + Lista.Instance.Nome + "'", "Sei sicuro di voler eliminare la lista '" + Lista.Instance.Nome + "'", NetCms.Vertical.PagesHandler.GetPageAddress(typeof(MainPage)));
                delcontrol.ConfirmButton.Click += new EventHandler(ConfirmButton_Click);
                return delcontrol;
            }
            return GetTableControl();
        }

        void ConfirmButton_Click(object sender, EventArgs e)
        {
            BusinessLogic.DeleteLista(Lista.Instance);
            NetCms.GUI.PopupBox.AddSessionMessage("Lista eliminata con successo", PostBackMessagesType.Success, true);
        }

        private WebControl GetTableControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);

            ArTable listeTable = new ArTable();
            listeTable.EnablePagination = true;
            listeTable.RecordPerPagina = 20;
            listeTable.InnerTableCssClass = "tab";
            listeTable.NoRecordMsg = "Nessuna lista creata";

            PaginationHandler pagination = new PaginationHandler(20, BusinessLogic.CountListe(), listeTable.PaginationKey);
            listeTable.PagesControl = pagination;

            var liste = BusinessLogic.FindListe(((pagination.CurrentPage - 1) * pagination.PageSize), pagination.PageSize, "Nome", false);

            listeTable.Records = liste;

            ArTextColumn nomeLista = new ArTextColumn("Nome", "Nome");
            listeTable.AddColumn(nomeLista);

            ArActionColumn modifyTipologia = new ArActionColumn("Modifica", ArActionColumn.Icons.Edit, "lista_mod.aspx?lista={Id}");
            listeTable.AddColumn(modifyTipologia);

            ArActionColumn deleteTipologia = new ArActionColumn("Elimina", ArActionColumn.Icons.Delete, "default.aspx?action=delete&lista={Id}");
            listeTable.AddColumn(deleteTipologia);

            control.Controls.Add(listeTable);

            return control;

        }
    }
}