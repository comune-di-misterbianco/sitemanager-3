﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.Users.Search;
using G2Core.Common;
using NetCms.Connections;
using NetService.Utility.ArTable;
using System.Linq;
using NetCms.GUI;
using UsersAggregator.Model;
using System.Collections;
using System.Collections.Generic;
using G2Core.Caching;
using NetService.Utility.Controls;
using NetService.Utility.QueryStringManager;
using NetService.Utility.ValidatedFields;
using NetService.Utility.RecordsFinder;
using NetCms.Users;

namespace UsersAggregator.View
{
    public class UsersPickerControl : WebControl
    {
        private const string TempUsersTableKey="temporaryUsersList";

        private enum ViewType { Insert, Update }

        private ViewType Vista;

        private Liste Lista;

        public User Account
        {
            get
            {
                if (_Account == null)
                {
                    _Account = AccountManager.CurrentAccount;
                }
                return _Account;
            }
        }
        private User _Account;

        //private Connection _Connection;
        //public Connection Connection
        //{
        //    get
        //    {
        //        if (_Connection == null)
        //            _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;

        //        return _Connection;
        //    }
        //}

        public bool AddCmsUserRequest
        {
            get
            {
                RequestVariable request = new RequestVariable("action", RequestVariable.RequestType.QueryString);
                if (request.IsValidString && request.StringValue == "addCmsUser")
                    _AddCmsUserRequest = true;
                return _AddCmsUserRequest;
            }
        }
        private bool _AddCmsUserRequest = false;

        public bool AddRubricaUserRequest
        {
            get
            {
                RequestVariable request = new RequestVariable("action", RequestVariable.RequestType.QueryString);
                if (request.IsValidString && request.StringValue == "addRubricaUser")
                    _AddRubricaUserRequest = true;
                return _AddRubricaUserRequest;
            }
        }
        private bool _AddRubricaUserRequest = false;

        public bool RemoveTempUserRequest
        {
            get
            {
                RequestVariable request = new RequestVariable("action", RequestVariable.RequestType.QueryString);
                if (request.IsValidString && request.StringValue == "removeTempUser")
                    _RemoveTempUserRequest = true;
                return _RemoveTempUserRequest;
            }
        }
        private bool _RemoveTempUserRequest = false;

        public int IdCmsUser
        {
            get
            {
                if (_IdCmsUser == 0)
                {
                    RequestVariable request = new RequestVariable("idCmsUser", RequestVariable.RequestType.QueryString);
                    if (request.IsValidInteger)
                        _IdCmsUser = request.IntValue;
                }
                return _IdCmsUser;
            }
        }
        private int _IdCmsUser = 0;

        public int IdRubricaUser
        {
            get
            {
                if (_IdRubricaUser == 0)
                {
                    RequestVariable request = new RequestVariable("idRubricaUser", RequestVariable.RequestType.QueryString);
                    if (request.IsValidInteger)
                        _IdRubricaUser = request.IntValue;
                }
                return _IdRubricaUser;
            }
        }
        private int _IdRubricaUser = 0;

        public int IdTempUser
        {
            get
            {
                if (_IdTempUser == 0)
                {
                    RequestVariable request = new RequestVariable("IdTempUser", RequestVariable.RequestType.QueryString);
                    if (request.IsValidInteger)
                        _IdTempUser = request.IntValue;
                }
                return _IdTempUser;
            }
        }
        private int _IdTempUser = 0;

        public string UserType
        {
            get
            {
                if (string.IsNullOrEmpty(_UserType))
                {
                    RequestVariable request = new RequestVariable("UserType", RequestVariable.RequestType.QueryString);
                    if (request.IsValidString)
                        _UserType = request.StringValue;
                }
                return _UserType;
            }
        }
        private string _UserType;

        //public ICollection<User> UsersTable
        //{
        //    get
        //    {
        //        if (_UsersTable == null)
        //        {
        //            UsersDataManager UsersDataManager = new UsersDataManager(NetCms.Users.AccountManager.CurrentAccount);
        //            _UsersTable = UsersDataManager.MyUsersTable;
        //        }
        //        return _UsersTable;
        //    }
        //}
        //private ICollection<User> _UsersTable;

        private int RecordPerPage = 10;

        public ICollection<User> UsersTable
        {
            get
            {
                if (_UsersTable == null)
                {
                    UsersDataManager UsersDataManager = new UsersDataManager(this.Account);
                    UsersDataManager.PageSize = RecordPerPage;
                    UsersDataManager.PageStart = (UsersSearchControl.ButtonRequest.IsPostBack && UsersSearchControl.ButtonRequest.IsValidString) ? 0 : CmsPagination.CurrentPage - 1;
                    _UsersTable = UsersDataManager.MyUsersTable;
                }
                return _UsersTable;
            }
        }
        private ICollection<User> _UsersTable;

        public UsersSearchControl UsersSearchControl
        {
            get
            {
                if (_UsersSearchControl == null)
                    _UsersSearchControl = new UsersSearchControl();
                return _UsersSearchControl;
            }
        }
        private UsersSearchControl _UsersSearchControl;

        public UtenteRubricaForm UtenteRubricaForm
        {
            get
            {
                if (_UtenteRubricaForm == null)
                {
                    _UtenteRubricaForm = new UtenteRubricaForm("RubricaSearchForm");
                    _UtenteRubricaForm.ShowMandatoryInfo = false;
                    _UtenteRubricaForm.AutoValidation = false;
                    _UtenteRubricaForm.CssClass = "Axf";
                    _UtenteRubricaForm.SubmitButton.Text = "Cerca";
                    _UtenteRubricaForm.SubmitButton.ID = "ButtonSearchRubricaRequest";
                    foreach (CampiCustom campo in BusinessLogic.FindCampiCustom())
                    {
                        _UtenteRubricaForm.Fields.Add(campo.VfField);
                    }
                }
                return _UtenteRubricaForm;
            }
        }
        private UtenteRubricaForm _UtenteRubricaForm;

        public G2Core.Common.RequestVariable ButtonSearchRubricaRequest
        {
            get
            {
                if (_ButtonSearchRubricaRequest == null)
                    _ButtonSearchRubricaRequest = new G2Core.Common.RequestVariable("ButtonSearchRubricaRequest");
                return _ButtonSearchRubricaRequest;
            }
        }
        private G2Core.Common.RequestVariable _ButtonSearchRubricaRequest;


        public G2Core.Common.RequestVariable ButtonSaveListRequest
        {
            get
            {
                if (_ButtonSaveListRequest == null)
                    _ButtonSaveListRequest = new G2Core.Common.RequestVariable("save_list");
                return _ButtonSaveListRequest;
            }
        }
        private G2Core.Common.RequestVariable _ButtonSaveListRequest;

        private string SearchButtonKey
        {
            get { return this.ID + "_SearchButton"; }

        }

        public ExtendedSession UserSession
        {
            get 
            {
                if (_UserSession == null)
                {
                    _UserSession = new ExtendedSession();
                }
                return _UserSession;
            }
        }
        private ExtendedSession _UserSession;

        private string _BackUrl;

        //Con questa proprietà si possono specificare una serie di valori in query string da portare durante la paginazione
        //oltre quelli provenienti dai custom fields, nel caso in cui la proprietà di ArTable .NoSearchParameter = true
        private string[] ExtraCustomQS;

        public UsersPickerControl(string modPageUrl = "lista_mod.aspx", string addPageUrl = "lista_new.aspx", string backUrl = "",string[] extraCustomQS = null)
            : base(HtmlTextWriterTag.Div)
        {
            ModPageUrl = modPageUrl;
            AddPageUrl = addPageUrl;
            _BackUrl = (backUrl.Length == 0) ? NetCms.Vertical.PagesHandler.GetPageAddress(typeof(MainPage)) : backUrl;
            ExtraCustomQS = extraCustomQS;
            Vista = ViewType.Insert;

            GetActions();

            GetControls();
        }

        public UsersPickerControl(Liste lista, string modPageUrl = "lista_mod.aspx", string addPageUrl = "lista_new.aspx", string backUrl = "", string[] extraCustomQS = null)
            : base(HtmlTextWriterTag.Div)
        {
            ModPageUrl = modPageUrl;
            AddPageUrl = addPageUrl;
            _BackUrl = (backUrl.Length == 0) ? NetCms.Vertical.PagesHandler.GetPageAddress(typeof(MainPage)) : backUrl;
            ExtraCustomQS = extraCustomQS;
            Vista = ViewType.Update;

            TempList = Lista = lista;

            GetActions();

            GetControls();
        }

        public string ModPageUrl
        {
            get;
            set;
        }

        public string AddPageUrl
        {
            get;
            set;
        }

        private void GetActions()
        {
            if (this.UsersSearchControl.SearchStatus == UsersSearchControl.SearchStatuses.WrongData)
                PopupBox.AddMessage("I dati di ricerca immessi non sono validi, assicurarsi di aver riempito almeno un campo e di aver inserito dati corretti.", PostBackMessagesType.Notify);

            if (AddCmsUserRequest && IdCmsUser >0 && !UsersSearchControl.ButtonRequest.IsPostBack && !ButtonSearchRubricaRequest.IsPostBack && !ButtonSaveListRequest.IsPostBack)
            {
                if (TempList.Destinatari.Where(x => x.IdUtente == IdCmsUser && x.TipoUtente == Destinatari.TipiUtente.Cms).Count() == 0)
                {
                    TempList = BusinessLogic.AddCmsUserToTempList(TempList, IdCmsUser);
                }
                else
                {
                    PopupBox.AddMessage("Utente già presente in lista.", PostBackMessagesType.Notify);
                }
            }

            if (AddRubricaUserRequest && IdRubricaUser > 0 && !UsersSearchControl.ButtonRequest.IsPostBack && !ButtonSearchRubricaRequest.IsPostBack && !ButtonSaveListRequest.IsPostBack)
            {
                if (TempList.Destinatari.Where(x => x.IdUtente == IdRubricaUser && x.TipoUtente == Destinatari.TipiUtente.Rubrica).Count() == 0)
                {
                    TempList = BusinessLogic.AddRubricaUserToTempList(TempList, IdRubricaUser);
                }
                else
                {
                    PopupBox.AddMessage("Utente già presente in lista.", PostBackMessagesType.Notify);
                }
            }

            if (RemoveTempUserRequest && IdTempUser > 0 && !string.IsNullOrEmpty(UserType) && !UsersSearchControl.ButtonRequest.IsPostBack && !ButtonSearchRubricaRequest.IsPostBack && !ButtonSaveListRequest.IsPostBack)
            {
                Destinatari.TipiUtente tipo = (Destinatari.TipiUtente)Enum.Parse(typeof(Destinatari.TipiUtente), UserType);
                TempList.Destinatari = new HashSet<Destinatari>(TempList.Destinatari.Except(TempList.Destinatari.Where(x => x.IdUtente == IdTempUser && x.TipoUtente == tipo)));
            }
        }

        private Liste TempList
        {
            get
            {
                Liste tempList = null;
                if (UserSession.Contains(TempUsersTableKey))
                {
                    tempList = (Liste)UserSession[TempUsersTableKey];
                }
                else
                {
                    tempList = new Liste();
                }
                return tempList;
            }
            set
            {
                UserSession.Add(TempUsersTableKey, value);
            }
        }

        public int UsersTableCount
        {
            get
            {
                UsersDataManager UsersDataManager = new UsersDataManager(this.Account);
                _UsersTableCount = UsersDataManager.MyUsersTableCount;
                return _UsersTableCount;
            }
        }
        private int _UsersTableCount;

        private int TotalCmsRecordCount()
        {
            int count = 0;
            switch (UsersSearchControl.SearchStatus)
            {
                case UsersSearchControl.SearchStatuses.SearchAvailable: count = this.UsersSearchControl.GetSearchResultCount(this.Account); break;
                case UsersSearchControl.SearchStatuses.NoSearch: count = UsersTableCount; break;
                case UsersSearchControl.SearchStatuses.WrongData: break;
            }
            return count;
        }

        public PaginationHandler CmsPagination
        {
            get
            {
                if (_Pagination == null)
                {

                    _Pagination = new PaginationHandler(RecordPerPage, TotalCmsRecordCount(), CmsUsersTable.PaginationKey, (UsersSearchControl.ButtonRequest.IsPostBack && UsersSearchControl.ButtonRequest.IsValidString));
                }
                return _Pagination;
            }
        }
        private PaginationHandler _Pagination;

        private void GetControls()
        {
            ICollection<UserSearchRowTemplate> datatable = new List<UserSearchRowTemplate>();

            switch (UsersSearchControl.SearchStatus)
            {
                case UsersSearchControl.SearchStatuses.SearchAvailable: datatable = this.UsersSearchControl.GetSearchResult((UsersSearchControl.ButtonRequest.IsPostBack && UsersSearchControl.ButtonRequest.IsValidString) ? 0 : CmsPagination.CurrentPage - 1, RecordPerPage, this.Account); break;
                case UsersSearchControl.SearchStatuses.NoSearch:
                    foreach (User user in UsersTable)
                    {
                        try
                        {
                            if (datatable.Where(x => x.id_User == user.ID).Count() == 0)
                            {
                                datatable.Add(new UserSearchRowTemplate()
                                {
                                    id_User = user.ID,
                                    Name_User = user.UserName,
                                    State_User = user.State,
                                    AnagraficaType_User = user.AnagraficaType,
                                    Anagrafica_User = user.AnagraficaID,
                                    AnagraficaNome = user.NomeCompleto,
                                    Profile_User = user.Profile.ID,
                                    Registered_User = user.Registrato
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            //trace
                        }
                    }
                    break;
                case UsersSearchControl.SearchStatuses.WrongData: break;
            }

            ArTable cmsUsersTable = GetCmsUsersTable(datatable);

            ArTable rubricaUsersTable = GetRubricaUsersTable();

            ArTable tempUsersTable = GetTempUsersTable();

            NetCms.GUI.Collapsable collapsableSearchCmsUsers = new Collapsable("Ricerca Utenti CMS");
            collapsableSearchCmsUsers.StartCollapsed = UsersSearchControl.SearchStatus == UsersSearchControl.SearchStatuses.NoSearch;
            collapsableSearchCmsUsers.AppendControl(UsersSearchControl);

            NetCms.GUI.Collapsable collapsableCmsUsersTable = new Collapsable("Elenco degli Utenti di CMS");
            collapsableCmsUsersTable.AppendControl(cmsUsersTable);

            NetCms.GUI.Collapsable collapsableSearchRubricaUsers = new Collapsable("Ricerca Utenti Rubrica");            
            WebControl fieldsetRicercaUtenti = new WebControl(HtmlTextWriterTag.Fieldset);
            fieldsetRicercaUtenti.Controls.Add(UtenteRubricaForm);
            collapsableSearchRubricaUsers.AppendControl(fieldsetRicercaUtenti);

            NetCms.GUI.Collapsable collapsableRubricaUsersTable = new Collapsable("Elenco degli Utenti in Rubrica");
            collapsableRubricaUsersTable.AppendControl(rubricaUsersTable);

            NetCms.GUI.Collapsable collapsableTempUsersTable = new Collapsable("Elenco Utenti in Lista");
            collapsableTempUsersTable.AppendControl(tempUsersTable);

            this.Controls.Add(collapsableSearchCmsUsers);
            this.Controls.Add(collapsableCmsUsersTable);
            this.Controls.Add(collapsableSearchRubricaUsers);
            this.Controls.Add(collapsableRubricaUsersTable);
            this.Controls.Add(collapsableTempUsersTable);
        }

        private ArTable CmsUsersTable
        {
            get
            {
                if (_CmsUsersTable == null)
                {
                    _CmsUsersTable = new ArTable();
                    _CmsUsersTable.EnablePagination = true;
                    _CmsUsersTable.RecordPerPagina = RecordPerPage;
                    _CmsUsersTable.NoSearchParameter = true;
                    _CmsUsersTable.InnerTableCssClass = "tab";
                    _CmsUsersTable.GoToFirstPageOnSearch = true;
                }
                return _CmsUsersTable;
            }
        }
        private ArTable _CmsUsersTable;

        private ArTable GetCmsUsersTable(ICollection<UserSearchRowTemplate> datatable)
        {
            //int rpp = 5;
            //string paginationKey = "cmsP";

            //ArTable cmsUsersTable = new ArTable();

            //cmsUsersTable.EnablePagination = true;
            //cmsUsersTable.RecordPerPagina = RecordPerPage;
            //cmsUsersTable.NoSearchParameter = true;
            //cmsUsersTable.InnerTableCssClass = "tab";
            //cmsUsersTable.GoToFirstPageOnSearch = true;

            CmsUsersTable.PagesControl = CmsPagination;

            if (datatable.Count() > 0)
            {
                CmsUsersTable.Records = datatable;

                CmsUsersTable.CustomSearchArray = UsersSearchControl.WorkerFinder.Finder.CustomSearchFields.Select(x => x.QueryString).ToArray();
                if (ExtraCustomQS != null)
                   CmsUsersTable.CustomSearchArray = CmsUsersTable.CustomSearchArray.Concat(ExtraCustomQS).ToArray();

                CmsUsersTable.AddColumn(new ArTextColumn("UserName", "Name_User"));
                CmsUsersTable.AddColumn(new ArTextColumn("Nome", "AnagraficaNome"));
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["ManageRegRequests"] == "1")
                {
                    ArOptionsColumn colRegistrato = new ArOptionsColumn("Registrazione", "Registered_User", new string[] { "In attesa di accettazione", "Registrazione Accettata" });
                    CmsUsersTable.AddColumn(colRegistrato);
                }

                ArOptionsColumn colStato = new ArOptionsColumn("Stato", "State_User", new string[] { "Disabilitato", "Abilitato" });
                CmsUsersTable.AddColumn(colStato);

                CmsUsersTable.AddColumn(new ArAnagraficaFieldColumn("Email", "Email"));
                CmsUsersTable.AddColumn(new ArAnagraficaFieldColumn("Cellulare", "Cellulare"));

                //PaginationHandler pagination = new PaginationHandler(rpp, datatable.Rows.Count, paginationKey);

                //if (datatable.Rows.Count > 0)
                //{
                //    datatable = datatable.AsEnumerable().Skip((pagination.CurrentPage - 1) * rpp).Take(rpp).CopyToDataTable();
                //}

                //var searchQueryStringItems = UsersSearchControl.SearchQueryStringItems;

                //cmsUsersTable.NoSearchParameter = true;
                //cmsUsersTable.CustomSearchArray = searchQueryStringItems == null ? new string[0] : searchQueryStringItems.ToArray();
                //cmsUsersTable.Records = datatable.Rows;
                //cmsUsersTable.PagesControl = pagination;
                //cmsUsersTable.AddColumn(new ArTextColumn("UserName", "Name_User"));
                //cmsUsersTable.AddColumn(new ArAnagraficaFieldColumn("Nome", "NomeCompleto"));
                //cmsUsersTable.AddColumn(new ArAnagraficaFieldColumn("Città", "Città"));
                //cmsUsersTable.AddColumn(new ArAnagraficaFieldColumn("Cellulare", "Cellulare"));
                //cmsUsersTable.AddColumn(new ArAnagraficaFieldColumn("Email", "Email"));

                //cmsUsersTable.EnablePagination = true;
                //cmsUsersTable.RecordPerPagina = rpp;
                //cmsUsersTable.InnerTableCssClass = "tab";
                //cmsUsersTable.PaginationKey = paginationKey;

                if (Vista == ViewType.Insert)
                    CmsUsersTable.AddColumn(new ArActionColumn("Aggiungi alla Lista", ArActionColumn.Icons.Add, AddPageUrl + "?action=addCmsUser&IdCmsUser={id_User}" + SearchQSToAdd()));
                else CmsUsersTable.AddColumn(new ArActionColumn("Aggiungi alla Lista", ArActionColumn.Icons.Add, ModPageUrl + "?action=addCmsUser&IdCmsUser={id_User}&lista=" + Lista.Id + SearchQSToAdd()));

            }
            return CmsUsersTable;
        }

        private ArTable GetRubricaUsersTable()
        {
            int rpp = 5;
            string paginationKey = "rubP";

            ArTable utentiRubricaTable = new ArTable();
            utentiRubricaTable.EnablePagination = true;
            utentiRubricaTable.PaginationKey = paginationKey;
            utentiRubricaTable.RecordPerPagina = rpp;
            utentiRubricaTable.InnerTableCssClass = "tab";
            utentiRubricaTable.NoRecordMsg = "Nessun utente in rubrica";

            List<SearchParameter> searchparameters = new List<SearchParameter>();
            
            foreach (CampiCustom campo in BusinessLogic.FindCampiCustom())
            {
                searchparameters.Add(campo.SearchParameter);
            }

            searchparameters.Add(UtenteRubricaForm.Fields.Find(x => x.Key == "NomeCompleto").SearchParameter);
            searchparameters.Add(UtenteRubricaForm.Fields.Find(x => x.Key == "Email").SearchParameter);
            searchparameters.Add(UtenteRubricaForm.Fields.Find(x => x.Key == "Cellulare").SearchParameter);

            PaginationHandler pagination = new PaginationHandler(rpp, BusinessLogic.CountUtentiInRubrica(searchparameters.ToArray()), utentiRubricaTable.PaginationKey);
            utentiRubricaTable.PagesControl = pagination;

            var liste = BusinessLogic.FindUtentiInRubrica(((pagination.CurrentPage - 1) * pagination.PageSize), pagination.PageSize, "NomeCompleto", false, searchparameters.ToArray());

            utentiRubricaTable.Records = liste;

            utentiRubricaTable.SearchParameter = searchparameters;

            ArTextColumn nomeUtenteInRubrica = new ArTextColumn("Nome Completo", "NomeCompleto");
            utentiRubricaTable.AddColumn(nomeUtenteInRubrica);

            ArTextColumn emailUtenteInRubrica = new ArTextColumn("Email", "Email");
            utentiRubricaTable.AddColumn(emailUtenteInRubrica);

            ArTextColumn cellulareUtenteInRubrica = new ArTextColumn("Cellulare", "Cellulare");
            utentiRubricaTable.AddColumn(cellulareUtenteInRubrica);

            IList<CampiCustom> listaCampiCustom = BusinessLogic.FindCampiCustom();
            for (int i = 0; i < listaCampiCustom.Count; i++)
            {
                ArCampiCustomColumn customColumn = new ArCampiCustomColumn(listaCampiCustom.ElementAt(i).Label, "Proprietà", i);
                utentiRubricaTable.AddColumn(customColumn);
            }

            if (Vista == ViewType.Insert)
                utentiRubricaTable.AddColumn(new ArActionColumn("Aggiungi alla Lista", ArActionColumn.Icons.Add, AddPageUrl + "?action=addRubricaUser&IdRubricaUser={Id}"));
            else utentiRubricaTable.AddColumn(new ArActionColumn("Aggiungi alla Lista", ArActionColumn.Icons.Add, ModPageUrl + "?action=addRubricaUser&IdRubricaUser={Id}&lista=" + Lista.Id));

            return utentiRubricaTable;
        }

        private ArTable GetTempUsersTable()
        {
            ArTable tempUsersTable = new ArTable();
            tempUsersTable.NoRecordMsg = "Lista ancora vuota";
            tempUsersTable.AddColumn(new ArTextColumn("Nome Completo", "NomeCompleto"));
            tempUsersTable.AddColumn(new ArTextColumn("Cellulare", "Cellulare"));
            tempUsersTable.AddColumn(new ArTextColumn("Email", "Email"));

            if (Vista == ViewType.Insert)
                tempUsersTable.AddColumn(new ArActionColumn("Rimuovi", ArActionColumn.Icons.Remove, AddPageUrl + "?action=removeTempUser&IdTempUser={IdUtente}&UserType={TipoUtente}" + SearchQSToAdd()));
            else tempUsersTable.AddColumn(new ArActionColumn("Rimuovi", ArActionColumn.Icons.Remove, ModPageUrl + "?action=removeTempUser&IdTempUser={IdUtente}&UserType={TipoUtente}&lista=" + Lista.Id + SearchQSToAdd()));

            if (Vista == ViewType.Update && TempList != null && TempList.Destinatari.Count == 0)
                TempList=Lista;

            QueryStringManager qsmanager = new QueryStringManager();
            if (Vista == ViewType.Insert && !qsmanager.HasData)
                TempList = new Liste();

            if (UserSession.Contains(TempUsersTableKey))
            {
                ICollection<Destinatari> destinatari = TempList.Destinatari;
                if (destinatari.Count > 0)
                {
                    int rpp = 5;
                    string paginationKey = "tempP";

                    tempUsersTable.NoSearchParameter = true;
                    tempUsersTable.EnablePagination = true;
                    tempUsersTable.RecordPerPagina = rpp;
                    tempUsersTable.InnerTableCssClass = "tab";

                    if (ExtraCustomQS != null)
                        CmsUsersTable.CustomSearchArray = CmsUsersTable.CustomSearchArray.Concat(ExtraCustomQS).ToArray();

                    PaginationHandler pagination = new PaginationHandler(rpp, destinatari.Count, paginationKey);
                    tempUsersTable.PagesControl = pagination;
                    tempUsersTable.PaginationKey = paginationKey;

                    tempUsersTable.Records = destinatari.Skip((pagination.CurrentPage - 1) * rpp).Take(rpp);
                    tempUsersTable.Controls.Add(GetListNameTextBox());
                    tempUsersTable.Controls.Add(GetSaveButton());
                    tempUsersTable.Controls.Add(GetDeleteButton());
                }
            }
            return tempUsersTable;
        }

        private string SearchQSToAdd()
        {
            string searchQS = string.Empty;

            var searchQueryStringItems = UsersSearchControl.SearchQueryStringItems;

            if (searchQueryStringItems != null && searchQueryStringItems.Count > 0)
                searchQS = "&" + string.Join("&", searchQueryStringItems);

            return searchQS;
        }

        private TextBox GetListNameTextBox()
        {
            TextBox text = new TextBox();
            text.ID = "name_list";

            if (Vista == ViewType.Update)
                text.Text = Lista.Nome;

            return text;
        }

        private Button GetDeleteButton()
        {
            Button cancella = new Button();
            cancella.ID = "delete_list";
            cancella.Text = "Annulla";
            cancella.Click += new EventHandler(cancella_Click);
            return cancella;
        }

        void cancella_Click(object sender, EventArgs e)
        {
            TempList = new Liste();
            PopupBox.AddSessionMessage("Operazione annullata", PostBackMessagesType.Success, _BackUrl);
        }

        private Button GetSaveButton()
        {
            Button salva = new Button();
            salva.ID = "save_list";
            salva.Text = "Salva";
            salva.Click += new EventHandler(salva_Click);
            return salva;
        }

        void salva_Click(object sender, EventArgs e)
        {
            RequestVariable listName = new RequestVariable("name_list", RequestVariable.RequestType.Form_QueryString);
            if (listName.IsValidString)
            {
                if (BusinessLogic.GetListaByName(listName.StringValue) == null || Vista == ViewType.Update)
                {
                    Liste lista = TempList;
                    if (lista.Destinatari.Count > 0)
                    {
                        lista.Nome = listName.StringValue;
                        if (BusinessLogic.SaveOrUpdateLista(lista))
                        {
                            PopupBox.AddSessionMessage("Lista salvata con successo", PostBackMessagesType.Success, _BackUrl);
                        }
                        else PopupBox.AddMessage("Errore tecnico nel salvataggio della lista", PostBackMessagesType.Error);
                    }
                    else PopupBox.AddMessage("La lista che stai provando ad inserire è vuota", PostBackMessagesType.Error);
                }
                else PopupBox.AddMessage("Esiste già una lista con quel nome", PostBackMessagesType.Error);
            }
            else PopupBox.AddMessage("Inserisci il nome della lista", PostBackMessagesType.Error);
        }
    }
}



