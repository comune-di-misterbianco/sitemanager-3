﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;

namespace UsersAggregator.View
{
    public class UtenteRubricaForm : VfManager
    {
        public UtenteRubricaForm(string controlId)
            : base(controlId)
        {
            this.Fields.Add(NomeCompleto);
            this.Fields.Add(Email);
            this.Fields.Add(Cellulare);
        }

        private VfTextBox NomeCompleto
        {
            get
            {
                if (_NomeCompleto == null)
                {
                    _NomeCompleto = new VfTextBox("NomeCompleto", "Nome Completo", "NomeCompleto", InputTypes.Text);
                    _NomeCompleto.Required = true;
                    _NomeCompleto.MaxLenght = 50;
                }
                return _NomeCompleto;
            }
        }
        private VfTextBox _NomeCompleto;

        private VfTextBox Cellulare
        {
            get
            {
                if (_Cellulare == null)
                {
                    _Cellulare = new VfTextBox("Cellulare", "Cellulare", "Cellulare", InputTypes.Phone);
                    _Cellulare.Required = true;
                }
                return _Cellulare;
            }
        }
        private VfTextBox _Cellulare;

        private VfTextBox Email
        {
            get
            {
                if (_Email == null)
                {
                    _Email = new VfTextBox("Email", "Email", "Email", InputTypes.Email);
                    _Email.Required = true;
                }
                return _Email;
            }
        }
        private VfTextBox _Email;
    }
}
