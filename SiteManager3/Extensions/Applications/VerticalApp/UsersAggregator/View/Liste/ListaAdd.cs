﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;

namespace UsersAggregator.View
{
    [DynamicUrl.PageControl("/lista_new.aspx")]
    public class ListaAdd : UsersAggregatorPage
    {
        public override string PageTitle
        {
            get { return "Creazione Nuova Lista"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User_Add; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Gestione Liste"));
        }

        public ListaAdd(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            SetToolbars();
        }

        public override WebControl BuildControl()
        {
            return new UsersPickerControl();
        }
    }
}
