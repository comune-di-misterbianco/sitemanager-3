﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using UsersAggregator.Model;
using NetService.Utility.Common;
using GenericDAO.DAO.Utility;

namespace UsersAggregator.View
{
    [DynamicUrl.PageControl("/lista_mod.aspx")]
    public class ListaMod : UsersAggregatorPage
    {
        public override string PageTitle
        {
            get { return "Modifica Lista"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User_Edit; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Gestione Liste"));
        }

        public NhRequestObject<Liste> Lista { get; private set; }

        public ListaMod(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            Lista = new NhRequestObject<Liste>(BusinessLogic.GetCurrentSession(), "lista");
            this.StatusValidators.Add(new NhRequestObjectValidator<Liste>(Lista));
            SetToolbars();
        }

        public override WebControl BuildControl()
        {
            return new UsersPickerControl(Lista.Instance);
        }

    }
}
