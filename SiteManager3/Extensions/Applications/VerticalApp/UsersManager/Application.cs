﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using NetCms.Vertical;

namespace NetCms.Users.UsersManager
{
    public class UsersApplication:VerticalApplication
    {
        public const string ApplicationContextBackLabel = "Gestione Utenti";

        public const int ApplicationID = 5;
        public const string ApplicationSystemName = "users";

        public override int HomepageGroup
        {
            get { return 1; }
        }

        public override VerticalApplication.AccessModes AccessMode
        {
            get { return AccessModes.Admin; }
        }
        public override string Description
        {
            get { return "Gestione degli utenti"; }
        }

        public override int ID
        {
            get { return ApplicationID; }
        }

        public override bool IsSystemApplication
        {
            get
            {
                return true;
            }
        }

        public override bool CheckForDefaultGrant
        {
            get { return true; }
        }

        public override string Label
        {
            get { return "Utenti"; }
        }

        public override bool NetworkDependant
        {
            get { return false; }
        }

        public override int SecurityLevel
        {
            get { return 0; }
        }

        public override string SystemName
        {
            get { return ApplicationSystemName; }
        }

        public override bool DoAdditionalConfigurationForExternalDB()
        {
            throw new NotImplementedException();
        }

        public override bool NeedAdditionalConfiguration
        {
            get { return false; }
        }

        public override VerticalApplicationSetup Installer
        {
            get
            {
                return new Setup(this);
            }
        }

        public UsersApplication(Assembly assembly)
            : base(assembly)
        {            
        }

        public override bool UseCmsSessionFactory
        {
            get { return false; }
        }
    }
}
