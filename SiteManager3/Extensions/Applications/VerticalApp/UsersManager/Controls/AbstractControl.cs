﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetTable;
using NetCms.GUI;
using NetCms.Vertical.Grants;
using NetCms.Users.Search;
using NetCms.Users;
using NetCms.Connections;
/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Users.UsersManager
{
    public class AbstractControl : WebControl
    {
        public AbstractPage VerticalPage { get; private set; } 

        public AbstractControl(AbstractPage page)
        {
            this.VerticalPage = page;
        }
    }
}