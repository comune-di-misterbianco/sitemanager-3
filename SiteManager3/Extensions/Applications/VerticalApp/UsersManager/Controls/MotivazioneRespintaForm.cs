﻿using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI;

namespace NetCms.Users.UsersManager
{
    public class MotivazioneRespintaForm : VfManager
    {
        public MotivazioneRespintaForm(string controlID)
            : base(controlID)
        {
            this.Fields.Add(Motivazione);
            this.Fields.Add(SelectionIndex);
        }

        public VfHidden SelectionIndex
        {
            get
            {
                if (_SelectionIndex == null)
                {
                    _SelectionIndex = new VfHidden("UtenteSelezionato", "");
                    _SelectionIndex.BindField = false;
                    _SelectionIndex.Required = false;
                }
                return _SelectionIndex;
            }
        }
        private VfHidden _SelectionIndex;

        public VfTextBox Motivazione
        {
            get
            {
                if (_Motivazione == null)
                {
                    _Motivazione = new VfTextBox("motivazioneRespinta", "Inserisci la motivazione", "MotivazioneRespinta", InputTypes.Text);
                    _Motivazione.InfoControl.Controls.Add(new LiteralControl("Spiegare motivazione di respinta all'utente"));
                    _Motivazione.Required = true;
                    _Motivazione.TextBox.TextMode = System.Web.UI.WebControls.TextBoxMode.MultiLine;
                    _Motivazione.TextBox.Rows = 2; 
                }
                return _Motivazione;
            }
        }
        private VfTextBox _Motivazione;
    }
}