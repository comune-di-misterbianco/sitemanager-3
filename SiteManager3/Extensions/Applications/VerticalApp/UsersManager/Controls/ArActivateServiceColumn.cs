﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using NetCms.Users;
using NetCms.Vertical;

namespace NetService.Utility.ArTable
{
    public class ArActivateServiceColumn : ArActionColumn
    {
        public virtual string HrefMoreData { get; protected set; }


        public ArActivateServiceColumn(string caption, Icons icon, string hrefNoMoreData, string hrefMoreData)
            : base(caption, icon, hrefNoMoreData)
        {
            this.Href = hrefNoMoreData;
            this.HrefMoreData = hrefMoreData;
            this.Icon = icon;
        }

        protected override TableCell GetCellControl(object objectInstance, string label, string href, Icons icon)
        {
            TableCell td = new TableCell();
            if (icon != null)
            {
                if (icon == Icons.Custom)
                {
                    if (!string.IsNullOrEmpty(this.CssClass)) td.CssClass = base.ParseString(objectInstance, this.CssClass);
                }
                else
                    td.CssClass = "action " + icon.ToString().ToLower();
            }

            string anchorCssClassHtml = string.IsNullOrEmpty(AnchorCssClass) ? "" : "class=\"" + this.AnchorCssClass + "\"";
            string anchorTitle = string.IsNullOrEmpty(ToolTip) ? "" : "title=\"" + base.ParseString(objectInstance, ToolTip) + "\"";

            VerticalApplicationWithService vapp = objectInstance as VerticalApplicationWithService;

            if (!vapp.Servizio.RichiedeDatiAggiuntivi)
                td.Text = "<a " + anchorCssClassHtml + " href=\"" + base.ParseString(objectInstance, href) + "\"><span>" + base.ParseString(objectInstance, label) + "</span></a>";
            else
            {
                td.Text = "<a " + anchorCssClassHtml + " href=\"" + base.ParseString(objectInstance,HrefMoreData) + "\"><span>" + base.ParseString(objectInstance, label) + "</span></a>";
            }

            return td;
        }
    }
}
