﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using G2Core.Common;
using NetService.Utility.ArTable;
using NetService.Utility.ValidatedFields;
using NetCms.Users;
using NetCms.Users.CustomAnagrafe;

namespace NetCms.Users.UsersManager
{
    public class UsersAddVFManager : WebControl
    {
        public VfManager AnagraficaTypeForm
        {
            get
            {
                if (_AnagraficaTypeForm == null)
                {
                    _AnagraficaTypeForm = new VfManager(this.ID + "_anagraficaType") { AddSubmitButton = false };
                    _AnagraficaTypeForm.IsPostBack = FormIsPostBack;

                    VfDropDown anagraficheCustom = new VfDropDown("AnagraficheCustom", "Scegli l'anagrafica dell'utente");
                    anagraficheCustom.DropDownList.AutoPostBack = true;

                    anagraficheCustom.Options.Add("", "");
                    foreach (CustomAnagrafe.CustomAnagrafe ca in CustomAnagrafeBusinessLogic.FindAllCustomAnagrafe())
                    {
                        anagraficheCustom.Options.Add(ca.Nome, ca.Nome);
                    }

                    _AnagraficaTypeForm.Fields.Add(anagraficheCustom);
                }
                return _AnagraficaTypeForm;
            }
        }
        private VfManager _AnagraficaTypeForm;

        public VfManager UserForm
        {
            get
            {
                if (_UserForm == null)
                {
                    _UserForm = new VfManager(this.ID + "_user") { AddSubmitButton = false };
                    _UserForm.IsPostBack = FormIsPostBack;

                    //VfTextBox nomeUtente = new VfTextBox("UserName", "Nome Utente", "UserName", InputTypes.Text);
                    //nomeUtente.MaxLenght = 60;
                    //nomeUtente.Required = true;
                    //_UserForm.Fields.Add(nomeUtente);

                    VfPassword password = new VfPassword("Password");
                    password.Required = true;
                    _UserForm.Fields.Add(password);

                }
                return _UserForm;
            }
        }
        private VfManager _UserForm;

        //public VfManager AnagraficaForm
        //{
        //    get
        //    {
        //        if (_AnagraficaForm == null)
        //        {
        //            _AnagraficaForm = new VfManager(this.ID + "_anagrafica");
        //            _AnagraficaForm.AddSubmitButton = false;
        //            _AnagraficaForm.IsPostBack = FormIsPostBack;

        //            VfTextBox fieldEmail = new VfTextBox("Email", "Email", "Email", InputTypes.Email);
        //            fieldEmail.Required = true;
        //            fieldEmail.BindField = false;

        //            _AnagraficaForm.Fields.Add(fieldEmail);

        //            NetCms.Users.CustomAnagrafe.CustomAnagrafe strutturaAnagrafe = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(AnagraficaTypeForm.Fields.Find(x => x.Key == "AnagraficheCustom").PostBackValue);
        //            if (strutturaAnagrafe != null && strutturaAnagrafe.CustomFields.Count > 0)
        //            {
        //                foreach (CustomField fieldToAdd in strutturaAnagrafe.CustomFields)
        //                {
        //                    _AnagraficaForm.Fields.Add(fieldToAdd.CampoInAnagrafe);
        //                }
        //            }
        //        }
        //        return _AnagraficaForm;
        //    }
        //}
        //private VfManager _AnagraficaForm;

        public AnagraficaVfManager AnagraficaForm
        {
            get
            {
                if (_AnagraficaForm == null)
                {
                    NetCms.Users.CustomAnagrafe.CustomAnagrafe strutturaAnagrafe = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(AnagraficaTypeForm.Fields.Find(x => x.Key == "AnagraficheCustom").PostBackValue);
                    _AnagraficaForm = new AnagraficaVfManager(this.ID + "_anagrafica", FormIsPostBack, strutturaAnagrafe, false);
                }
                return _AnagraficaForm;
            }
        }
        private AnagraficaVfManager _AnagraficaForm;

        public Button SubmitButton
        {
            get
            {
                if (_SubmitButton == null)
                {
                    _SubmitButton = new Button();
                    _SubmitButton.ID = this.ID + "_submit";
                    _SubmitButton.Text = "Crea";
                }
                return _SubmitButton;
            }
        }
        private Button _SubmitButton;

        public bool FormIsPostBack { get; private set; }

        public UsersAddVFManager(string controlID)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = controlID;
            RequestVariable buttonPostback = new RequestVariable(SubmitButton.ID);
            FormIsPostBack = (buttonPostback.IsPostBack && buttonPostback.StringValue == SubmitButton.Text);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            fieldset.Controls.Add(legend);
            fieldset.Controls.Add(AnagraficaTypeForm);
            legend.Controls.Add(new LiteralControl("Scelta Tipologia Anagrafica"));
            this.Controls.Add(fieldset);

            bool anagraficaTypeDropdownPostback = !string.IsNullOrEmpty(AnagraficaTypeForm.Fields.Find(x => x.Key == "AnagraficheCustom").PostBackValue);

            if (anagraficaTypeDropdownPostback)
            {
                fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
                legend = new WebControl(HtmlTextWriterTag.Legend);
                fieldset.Controls.Add(legend);
                fieldset.Controls.Add(AnagraficaForm);
                legend.Controls.Add(new LiteralControl("Dati Anagrafici"));
                this.Controls.Add(fieldset);

                fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
                legend = new WebControl(HtmlTextWriterTag.Legend);
                fieldset.Controls.Add(legend);
                fieldset.Controls.Add(UserForm);
                legend.Controls.Add(new LiteralControl("Dati Login"));
                this.Controls.Add(fieldset);

                //fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
                //legend = new WebControl(HtmlTextWriterTag.Legend);
                //fieldset.Controls.Add(legend);
                //fieldset.Controls.Add(SubmitButton);
                //legend.Controls.Add(new LiteralControl("Inserisci Utente"));
                //this.Controls.Add(fieldset);

                WebControl submitButton_Container = new WebControl(HtmlTextWriterTag.Div);
                submitButton_Container.CssClass = "vf submit-vf";
                submitButton_Container.Controls.Add(SubmitButton);
                this.Controls.Add(submitButton_Container);
            }

            if (!FormIsPostBack && !anagraficaTypeDropdownPostback)
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di creazione di un nuovo utente.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            }
        }
    }
}