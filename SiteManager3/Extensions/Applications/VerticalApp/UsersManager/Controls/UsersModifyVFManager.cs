﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using G2Core.Common;
using NetService.Utility.ArTable;
using NetService.Utility.ValidatedFields;
using NetCms.Users;
using NetCms.Users.CustomAnagrafe;

namespace NetCms.Users.UsersManager
{
    public class UsersModifyVFManager : WebControl
    {
        public VfManager UserForm
        {
            get
            {
                if (_UserForm == null)
                {
                    _UserForm = new VfManager(this.ID + "_user") { AddSubmitButton = false };
                    _UserForm.IsPostBack = FormIsPostBack;

                    VfPassword password = new VfPassword("Password");
                    password.Required = false;

                    _UserForm.Fields.Add(password);
                }
                return _UserForm;
            }
        }
        private VfManager _UserForm;

        //public VfManager AnagraficaForm
        //{
        //    get
        //    {
        //        if (_AnagraficaForm == null)
        //        {
        //            _AnagraficaForm = this.User.Anagrafica.GetProfileVfManager();
        //            _AnagraficaForm.AddSubmitButton = false;
        //            _AnagraficaForm.IsPostBack = FormIsPostBack;
        //        }
        //        return _AnagraficaForm;
        //    }
        //}
        //private VfManager _AnagraficaForm;

        public AnagraficaVfManager AnagraficaForm
        {
            get
            {
                if (_AnagraficaForm == null)
                {
                    NetCms.Users.CustomAnagrafe.CustomAnagrafe strutturaAnagrafe = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(User.AnagraficaName);
                    _AnagraficaForm = new AnagraficaVfManager(this.ID + "_anagrafica", FormIsPostBack, strutturaAnagrafe, false, User, false);
                }
                return _AnagraficaForm;
            }
        }
        private AnagraficaVfManager _AnagraficaForm;

        public Button SubmitButton
        {
            get
            {
                if (_SubmitButton == null)
                {
                    _SubmitButton = new Button();
                    _SubmitButton.ID = this.ID + "_submit";
                    _SubmitButton.Text = "Aggiorna Dati Utente";
                }
                return _SubmitButton;
            }
        }
        private Button _SubmitButton;

        public bool FormIsPostBack { get; private set; }

        public User User
        {
            get;
            private set;
        }

        public UsersModifyVFManager(string controlID, User user)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = controlID;
            RequestVariable buttonPostback = new RequestVariable(SubmitButton.ID);
            FormIsPostBack = (buttonPostback.IsPostBack && buttonPostback.StringValue == SubmitButton.Text);
            this.User = user;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            fieldset.Controls.Add(legend);
            fieldset.Controls.Add(AnagraficaForm);
            legend.Controls.Add(new LiteralControl("Dati Anagrafici"));
            this.Controls.Add(fieldset);

            fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
            legend = new WebControl(HtmlTextWriterTag.Legend);
            fieldset.Controls.Add(legend);
            fieldset.Controls.Add(UserForm);
            legend.Controls.Add(new LiteralControl("Dati Login"));
            this.Controls.Add(fieldset);

            fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
            legend = new WebControl(HtmlTextWriterTag.Legend);
            fieldset.Controls.Add(legend);
            fieldset.Controls.Add(SubmitButton);
            legend.Controls.Add(new LiteralControl("Aggiorna Dati"));
            this.Controls.Add(fieldset);

            if (!FormIsPostBack)
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di aggiornamento dati dell'utente " + this.User.NomeCompleto + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            }
        }
    }
}