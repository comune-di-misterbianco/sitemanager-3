﻿using System.Web.UI.WebControls;
using System;

namespace NetService.Utility.ArTable
{
    public class ArScadenzaColumn : ArTextColumn
    {
        public override TableCell GetCellControl(object objectInstance)
        {
            DateTime value = (DateTime) GetValue(objectInstance, this.Key);
            value = value.ToLocalTime();

            var CellText = "Nessuna Scadenza";
            if(value < DateTime.MaxValue)
                CellText = FilterCellValue(FilterDataType(GetValue(objectInstance, this.Key)));

            TableCell td = new TableCell();
            if (!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;
            td.Text = "<span>" + CellText + "</span>";

            return td;
        }

        public ArScadenzaColumn(string caption, string key)
            : base(caption, key)
        {
        
        }
    }
}