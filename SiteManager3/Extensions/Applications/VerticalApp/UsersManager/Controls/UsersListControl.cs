﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetTable;
using NetCms.GUI;
using NetCms.Vertical.Grants;
using NetCms.Users.Search;
using NetCms.Users;
using NetCms.Connections;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using NetUtility.Report.Exporter;
using System.ComponentModel;
/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Users.UsersManager
{
    public class UsersListControl : WebControl
    {
        public UsersSearchControl UsersSearchControl
        {
            get
            {
                if (_UsersSearchControl == null)
                    _UsersSearchControl = new UsersSearchControl(null,null,true);
                return _UsersSearchControl;
            }
        }
        private UsersSearchControl _UsersSearchControl;

        public Users.User Account { get; private set; }
        //public Connection Connection { get; private set; } 

        //public DataTable UsersTable
        //{
        //    get
        //    {
        //        if (_UsersTable == null)
        //        {
        //            UsersDataManager UsersDataManager = new UsersDataManager(NetCms.Users.AccountManager.CurrentAccount, this.Connection);
        //            _UsersTable = this.ShowUngrouppedUsers ? UsersDataManager.NonGrouppedUsersTable : UsersDataManager.MyUsersTable;
        //        }
        //        return _UsersTable;
        //    }
        //}
        //private DataTable _UsersTable;

        public ICollection<User> UsersTable
        {
            get
            {
                if (_UsersTable == null)
                {
                    UsersDataManager UsersDataManager = new UsersDataManager(NetCms.Users.AccountManager.CurrentAccount);
                    UsersDataManager.PageSize = RecordPerPage;
                    UsersDataManager.PageStart = (UsersSearchControl.ButtonRequest.IsPostBack && UsersSearchControl.ButtonRequest.IsValidString) ? 0 : Pagination.CurrentPage - 1;
                    _UsersTable = this.ShowUngrouppedUsers ? UsersDataManager.NonGrouppedUsersTable : UsersDataManager.MyUsersTable;
                }
                return _UsersTable;
            }
        }
        private ICollection<User> _UsersTable;

        public IList<User> UsersResultsForReport
        {
            get
            {
                if (_UsersResultsForReport == null)
                {
                    UsersDataManager UsersDataManager = new UsersDataManager(NetCms.Users.AccountManager.CurrentAccount);
                    _UsersResultsForReport = UsersDataManager.MyUsersTable.ToList();
                }
                return _UsersResultsForReport.ToList();
            }
        }
        private IList<User> _UsersResultsForReport;

        private DataTable _Users;
        private DataTable Users
        {
            get {
                if (_Users == null)
                {
                    _Users = new DataTable();
                    _Users.Clear();
                    _Users.Columns.Add("UserName", typeof(String));
                    _Users.Columns.Add("NomeCompleto", typeof(String));
                    _Users.Columns.Add("StatoReg", typeof(String));
                    _Users.Columns.Add("StatoAccount", typeof(String));
                    _Users.Columns.Add("Email", typeof(String));
                    _Users.Columns.Add("AnagrafeType", typeof(String));
                    _Users.Columns.Add("DataInserimento", typeof(DateTime));
                    _Users.Columns.Add("DataLastAccess", typeof(DateTime));

                    DataRow userRow;

                    IList<User> searchUsers = UsersResultsForReport;

                    if (UsersSearchControl.EnableSearchByAnagrafe && UsersSearchControl.TipologiaAnagrafiche.Request.OriginalValue != null && UsersSearchControl.TipologiaAnagrafiche.Request.OriginalValue.Length > 0)
                        searchUsers = searchUsers.Where(x => x.AnagraficaName == UsersSearchControl.TipologiaAnagrafiche.Request.OriginalValue).ToList();

                    foreach (User user in searchUsers)
                    {
                        userRow = _Users.NewRow();
                        userRow["UserName"] = user.UserName;
                        userRow["NomeCompleto"] = user.UserName;
                        userRow["StatoReg"] = (user.Registrato) ? "In attesa di accettazione" : "Registrazione Accettata";
                        userRow["StatoAccount"] = (user.State == 1) ? "Abilitato" : "Disabilitato";
                        userRow["Email"] = user.Anagrafica.Email;
                        userRow["AnagrafeType"] = user.AnagraficaName;
                        userRow["DataInserimento"] = user.CreationTime;
                        userRow["DataLastAccess"] = user.LastAccess;
                        _Users.Rows.Add(userRow);
                    }
                }
                return _Users;
            }
        }

        public int UsersTableCount
        {
            get
            {  
                UsersDataManager UsersDataManager = new UsersDataManager(NetCms.Users.AccountManager.CurrentAccount);
                _UsersTableCount = this.ShowUngrouppedUsers ? UsersDataManager.NonGrouppedUsersTableCount : UsersDataManager.MyUsersTableCount;
                return _UsersTableCount;
            }
        }
        private int _UsersTableCount;
        
        public bool ShowUngrouppedUsers
        {
            get { return _ShowUngrouppedUsers; }
            private set { _ShowUngrouppedUsers = value; }
        }
        private bool _ShowUngrouppedUsers;

        public G2Core.Common.RequestVariable AddToGroups
        {
            get
            {
                if (_AddToGroups == null)
                    _AddToGroups = new G2Core.Common.RequestVariable("group", G2Core.Common.RequestVariable.RequestType.QueryString);
                return _AddToGroups;
            }
        }
        private G2Core.Common.RequestVariable _AddToGroups;

        private const string EnableUserFieldID = "EnableUserField";
        private const string DisableUserFieldID = "DisableUserField";

        //private void AddToGroupPostBack()
        //{
        //    if (this.Page.IsPostBack)
        //    {
        //        int group;


        //        if (HttpContext.Current.Request.Form["SelectedUser"] != null && int.TryParse(HttpContext.Current.Request.Form["SelectedUser"], out group))
        //        {
        //            string sql = "";
        //            sql = "";
        //            sql += "SELECT * FROM usersgroups ";
        //            sql += " WHERE Group_UserGroup = " + AddToGroups + "";
        //            sql += " AND User_UserGroup  = " + group + "";
        //            DataTable table = this.Connection.SqlQuery(sql);

        //            if (table.Rows.Count == 0)
        //            {
        //                sql = "";
        //                sql += "INSERT INTO usersgroups ";
        //                sql += " (Group_UserGroup,User_UserGroup)";
        //                sql += " Values (" + AddToGroups + "," + group + ")";
        //                this.Connection.Execute(sql);


        //                string Utente = this.Account.Label;
        //                string logText = "L'utente " + Utente + " aggiunge al gruppo ";

        //                sql = "SELECT * FROM groups WHERE id_Group = " + AddToGroups + "";
        //                table = this.Connection.SqlQuery(sql);
        //                logText += table.Rows[0]["Name_Group"].ToString();

        //                sql = "SELECT * FROM users WHERE id_User = " + group + "";
        //                table = this.Connection.SqlQuery(sql);
        //                logText += " l'utente " + table.Rows[0]["Name_User"].ToString();

        //                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, logText);

        //                #region invalido la struttura

        //                //Networks.NetworksManager.CurrentActiveNetwork.Invalidate();
        //                G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
        //                cache.RemoveAll("");
        //                #endregion

        //                NetCms.GUI.PopupBox.AddMessage("Utente aggiunto", PostBackMessagesType.Success);
        //            }
        //            else
        //            {
        //                NetCms.GUI.PopupBox.AddMessage("Utente gia appartenente", PostBackMessagesType.Notify);
        //            }
        //        }
        //    }

        //}

        private void AddToGroupPostBack()
        {
            if (this.Page.IsPostBack)
            {
                int userId;

                if (HttpContext.Current.Request.Form["SelectedUser"] != null && int.TryParse(HttpContext.Current.Request.Form["SelectedUser"], out userId))
                {
                    GroupAssociation ga = GroupsBusinessLogic.GetGroupAssociationByUserAndGroup(userId, AddToGroups.IntValue);

                    if (ga == null)
                    {
                        ga = new GroupAssociation();
                        ga.User = UsersBusinessLogic.GetById(userId);
                        ga.Group = GroupsBusinessLogic.GetGroupById(AddToGroups.IntValue);
                        GroupsBusinessLogic.SaveOrUpdateGroupAssociation(ga);


                        string Utente = this.Account.Label;
                        string logText = "L'utente " + Utente + " aggiunge al gruppo " + ga.Group.Name + " l'utente " + ga.User.UserName;

                        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, logText);

                        #region invalido la struttura

                        //Networks.NetworksManager.CurrentActiveNetwork.Invalidate();
                        G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
                        cache.RemoveAll("");
                        #endregion

                        NetCms.GUI.PopupBox.AddMessage("Utente aggiunto", PostBackMessagesType.Success);
                    }
                    else
                    {
                        NetCms.GUI.PopupBox.AddMessage("Utente gia appartenente", PostBackMessagesType.Notify);
                    }
                }
            }

        }
        
        public GrantElement Grants
        {
            get
            {
                if (_Grants == null)
                    _Grants = new GrantElement(UsersApplication.ApplicationSystemName, this.Account.Profile);
                return _Grants;
            }
        }
        private GrantElement _Grants;

        public UsersListControl(/*Connection connection,*/ Users.User account, bool showUngrouppedUsers)
        {
            //this.Connection = connection;
            this.Account = account;
            this.ShowUngrouppedUsers = showUngrouppedUsers;
            if (!Grants["enable"].Allowed)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();
        }

        public UsersListControl(/*Connection connection,*/ Users.User account)
            : this(/*connection,*/ account, false)
        {
        }

        private int RecordPerPage = 20;

        public ArTable SearchResultTable
        {
            get 
            {
                if (_SearchResultTable == null)
                {
                    _SearchResultTable = new ArTable();
                    _SearchResultTable.EnablePagination = true;
                    _SearchResultTable.RecordPerPagina = RecordPerPage;                    
                    _SearchResultTable.NoSearchParameter = true;
                    _SearchResultTable.InnerTableCssClass = "tab";
                    _SearchResultTable.GoToFirstPageOnSearch = true;
                    //_SearchResultTable.ExportController.BaseUrl = _SearchResultTable.BuildBaseAddress(); // "default.aspx";
                }
                return _SearchResultTable;
            }
        }
        private ArTable _SearchResultTable;

        public PaginationHandler Pagination
        {
            get 
            {
                if (_Pagination == null)
                { 
      
                    _Pagination = new PaginationHandler(RecordPerPage,TotalRecordCount(),SearchResultTable.PaginationKey,(UsersSearchControl.ButtonRequest.IsPostBack && UsersSearchControl.ButtonRequest.IsValidString));
                }
                return _Pagination;
            }
        }
        private PaginationHandler _Pagination;

        private int TotalRecordCount()
        {
            int count = 0;
            switch (UsersSearchControl.SearchStatus)
            {
                case UsersSearchControl.SearchStatuses.SearchAvailable: count = this.UsersSearchControl.GetSearchResultCount(this.Account); break;
                case UsersSearchControl.SearchStatuses.NoSearch: count = UsersTableCount; break;
                case UsersSearchControl.SearchStatuses.WrongData: break;
            }
            return count;
        }

        private NetService.Utility.ExportControls _ExportController;
        public NetService.Utility.ExportControls ExportController
        {
            get
            {
                if (_ExportController == null)
                {
                    _ExportController = new NetService.Utility.ExportControls("usersExpCtrl");
                    _ExportController.BaseUrl = "default.aspx" + GetQueryStringBySearchForm();
                    _ExportController.TemplateFileName = "template-elenco-utenti.xml";
                    _ExportController.ExportRequestEvent += new NetService.Utility.ExportControls.AskExportEventHandler(_ExportController_ExportRequestEvent);
                }
                return _ExportController;
            }
        }

        void _ExportController_ExportRequestEvent(object sender, EventArgs e)
        {           
            _ExportController.ExportFormatsEnabled.Add(ExportFormats.Pdf);
            _ExportController.ExportFormatsEnabled.Add(ExportFormats.Csv);
            _ExportController.DataTable = Users;
            _ExportController.CustomPlaceholderValues.Add("dataora_generazione", DateTime.Now.ToString());
            _ExportController.CustomPlaceholderValues.Add("num_user", UsersResultsForReport.Count);

            string anagrafeFiltro = "";
            if (UsersSearchControl.TipologiaAnagrafiche.PostbackValueObject != null)
            {
                anagrafeFiltro = "Tipologia anagrafica: " + UsersSearchControl.TipologiaAnagrafiche.Request.OriginalValue;               
            }
            _ExportController.CustomPlaceholderValues.Add("anagraficaFiltro", anagrafeFiltro);
        }

        private string GetQueryStringBySearchForm() 
        {
            string querystring = "";

            if (UsersSearchControl.TipologiaAnagrafiche.PostbackValueObject != null)
                querystring = "?" + UsersSearchControl.TipologiaAnagrafiche.Key + "=" + UsersSearchControl.TipologiaAnagrafiche.Request.OriginalValue;

            return querystring;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            bool AllowStatusChange = Grants[UsersApplication.ApplicationSystemName + "_status"].Allowed;

            HtmlGenericControl output = new HtmlGenericControl("div");
            if (AllowStatusChange)
            {
                #region Enable/Disable user Postback

                HtmlGenericControl UserFields = new HtmlGenericControl("span");
                UserFields.InnerHtml = "<input type=\"hidden\" name=\"" + EnableUserFieldID + "\" id=\"" + EnableUserFieldID + "\" value=\"\">";
                UserFields.InnerHtml += "<input type=\"hidden\" name=\"" + DisableUserFieldID + "\" id=\"" + DisableUserFieldID + "\" value=\"\">";
                output.Controls.Add(UserFields);

                if (this.Page.IsPostBack)
                {
                    if (this.AddToGroups.IsValidInteger) AddToGroupPostBack();

                    NetUtility.RequestVariable EnableUserFieldRequest = new NetUtility.RequestVariable(EnableUserFieldID);
                    if (EnableUserFieldRequest.IsValidInteger /*&& PageData.SubmitSource == EnableUserFieldID*/)
                    {
                        //NetCms.Users.User user = new NetCms.Users.User(EnableUserFieldRequest.IntValue);
                        NetCms.Users.User user = UsersBusinessLogic.GetById(EnableUserFieldRequest.IntValue);
                        user.Enable();

                        string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                        string utente = user.NomeCompleto;
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha abilitato l'utente " + utente + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        NetCms.GUI.PopupBox.AddMessage("L'utente è stato abilitato con successo, riceverà via email una notifica dell'avvenuta attivazione", NetCms.GUI.PostBackMessagesType.Success);

                    }
                    NetUtility.RequestVariable DisableUserFieldRequest = new NetUtility.RequestVariable(DisableUserFieldID);
                    if (DisableUserFieldRequest.IsValidInteger /*&& PageData.SubmitSource == DisableUserFieldID*/)
                    {
                        if (this.Account.ID != DisableUserFieldRequest.IntValue)
                        {
                            //NetCms.Users.User user = new NetCms.Users.User(DisableUserFieldRequest.IntValue);
                            NetCms.Users.User user = UsersBusinessLogic.GetById(DisableUserFieldRequest.IntValue);
                            user.Disable();

                            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                            string utente = user.NomeCompleto;
                            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha disabilitato l'utente " + utente + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                            NetCms.GUI.PopupBox.AddMessage("L'utente è stato disabilitato con successo, riceverà via email una notifica dell'avvenuta disattivazione", NetCms.GUI.PostBackMessagesType.Notify);

                        }
                        else NetCms.GUI.PopupBox.AddMessage("Non puoi disabilitare il tuo account", PostBackMessagesType.Error);
                    }
                }
                else
                {
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    if (this.ShowUngrouppedUsers)
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di gestione degli utenti non associati ad alcun gruppo.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    else NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di gestione degli utenti.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                }

                #endregion
            }

            if (this.UsersSearchControl.SearchStatus == UsersSearchControl.SearchStatuses.WrongData)
                PopupBox.AddMessage("I dati di ricerca immessi non sono validi, assicurarsi di aver riempito almeno un campo e di aver inserito dati corretti.", PostBackMessagesType.Notify);

            NetCms.GUI.Collapsable collapsableSearch = new Collapsable("Ricerca");
            collapsableSearch.StartCollapsed = (UsersSearchControl.SearchStatus == UsersSearchControl.SearchStatuses.SearchAvailable || UsersSearchControl.SearchStatus == UsersSearchControl.SearchStatuses.NoSearch);
            collapsableSearch.AppendControl(UsersSearchControl);
            this.Controls.Add(collapsableSearch);

            NetCms.GUI.Collapsable collapsableTable = new Collapsable("Elenco degli Utenti");

            collapsableTable.AppendControl(output);
            this.Controls.Add(collapsableTable);

            ICollection<UserSearchRowTemplate> datatable = new List<UserSearchRowTemplate>();

            switch (UsersSearchControl.SearchStatus)
            {
                case UsersSearchControl.SearchStatuses.SearchAvailable: datatable = this.UsersSearchControl.GetSearchResult((UsersSearchControl.ButtonRequest.IsPostBack && UsersSearchControl.ButtonRequest.IsValidString) ? 0 : Pagination.CurrentPage - 1, RecordPerPage, this.Account); break;
                case UsersSearchControl.SearchStatuses.NoSearch:
                    foreach (User user in UsersTable)
                    {
                        try
                        {
                            if (datatable.Where(x => x.id_User == user.ID).Count() == 0)
                            {
                                datatable.Add(new UserSearchRowTemplate()
                                {
                                    id_User = user.ID,
                                    Name_User = user.UserName,
                                    State_User = user.State,
                                    AnagraficaType_User = user.AnagraficaType,
                                    Anagrafica_User = user.AnagraficaID,
                                    AnagraficaNome = user.NomeCompleto,
                                    Profile_User = user.Profile.ID,
                                    Registered_User = user.Registrato
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            //trace
                        }
                    }
                    break;
                case UsersSearchControl.SearchStatuses.WrongData: break;
            }

            SearchResultTable.PagesControl = Pagination;

            if (datatable.Count() > 0)
            {
 
                SearchResultTable.Records = datatable;
                SearchResultTable.CustomSearchArray = UsersSearchControl.WorkerFinder.Finder.CustomSearchFields.Select(x => x.QueryString).ToArray();
                SearchResultTable.ExportController.BaseUrl = SearchResultTable.BuildBaseAddress();

                //Riga necessaria se si abilita il filtraggio per anagrafica
                if (UsersSearchControl.EnableSearchByAnagrafe)
                    SearchResultTable.CustomSearchArray = SearchResultTable.CustomSearchArray.Union(new string[] { (UsersSearchControl.TipologiaAnagrafiche.GetFieldQueryString())}).ToArray();

                ArTextColumn colName = new ArTextColumn("User Name","Name_User");
                SearchResultTable.AddColumn(colName);

                ArTextColumn colAnagraficaName = new ArTextColumn("Nome completo", "AnagraficaNome");
                SearchResultTable.AddColumn(colAnagraficaName);

                ArTextColumn colAnagraficaTypeName = new ArTextColumn("Tipo Anagrafe", "AnagraficaType_User");
                colAnagraficaTypeName.CutTextFrom = "Anagrafica".Length;
                SearchResultTable.AddColumn(colAnagraficaTypeName);

                if (System.Web.Configuration.WebConfigurationManager.AppSettings["ManageRegRequests"] == "1")
                {
                    ArOptionsColumn colRegistrato = new ArOptionsColumn("Registrazione", "Registered_User", new string[] { "In attesa di accettazione", "Registrazione Accettata" });
                    SearchResultTable.AddColumn(colRegistrato);
                }

                ArOptionsColumn colStato = new ArOptionsColumn("Stato", "State_User", new string[] { "Disabilitato","Abilitato","Mai abilitato" });
                SearchResultTable.AddColumn(colStato);

                if (AllowStatusChange) 
                {

                    ArActionColumn colAbilitaDisabilita = new ArActionColumn("{UserStateActionLabel}", ArActionColumn.Icons.Custom, "javascript: setFormValue('{id_User}','{UserStateActionHref}',1)");
                    colAbilitaDisabilita.ColumnCaption = "Gestisci stato";
                    colAbilitaDisabilita.CssClass = "action {UserStateActionLabel}";
                    SearchResultTable.AddColumn(colAbilitaDisabilita);

                }

                ArActionColumn colDettagli = new ArActionColumn("Scheda e Opzioni", ArActionColumn.Icons.Details, "user_details.aspx?id={id_User}");
                SearchResultTable.AddColumn(colDettagli);

                if (this.AddToGroups.IsValidInteger)
                {
                    NetCms.Users.Group group = NetCms.Users.GroupsHandler.RootGroup.FindGroup(this.AddToGroups.IntValue);

                    ArActionColumn colAddToGroup = new ArActionColumn("Aggiungi Utente a Gruppo '" + group.Name + "'", ArActionColumn.Icons.Add , "javascript: setFormValue({id_User},'SelectedUser','1')");
                    SearchResultTable.AddColumn(colAddToGroup);
                }
                HtmlInputHidden setUserToAdd = new HtmlInputHidden();
                setUserToAdd.ID = "SelectedUser";
                output.Controls.Add(setUserToAdd);
                output.Controls.Add(SearchResultTable);

                //
                output.Controls.Add(ExportController);

                #region oldcode
                //AxtTable table = new AxtTable(datatable.Select("", "Name_User"));
                //table.Pagination = true;
                //table.RecordPerPagina = 25;
                //table.CssClass = "tab";

                //#region Campi della tabella

                //AxtColumn col;

                //col = new AxtGenericColumn("Name_User", "User Name");
                //table.Columns.Add(col);

                //col = new NomeAnagraficaColumn("Anagrafica_User", "Nome");
                //table.Columns.Add(col);

                //col = new AxtBooleanColumn("State_User", "Stato", "Disabilitato,Abilitato");
                //table.Columns.Add(col);
                //if (AllowStatusChange)
                //{
                //    AxtBooleanColumn.Option opt = new AxtBooleanColumn.Option();
                //    opt.CssClass = "action Abilita";
                //    opt.Label = "Abilita";
                //    opt.Link = "javascript: setFormValue('{id_User}','" + EnableUserFieldID + "',1)";

                //    AxtBooleanColumn.Option opt2 = new AxtBooleanColumn.Option();
                //    opt2.CssClass = "action Disabilita";
                //    opt2.Label = "Disabilita";
                //    opt2.Link = "javascript: setFormValue('{id_User}','" + DisableUserFieldID + "',1)";

                //    AxtBooleanColumn.Option opt3 = new AxtBooleanColumn.Option();
                //    opt3.CssClass = "action Abilita";
                //    opt3.Label = "Abilita";
                //    opt3.Link = "javascript: setFormValue('{id_User}','" + EnableUserFieldID + "',1)";

                //    AxtBooleanColumn bcol = new AxtBooleanColumn("State_User", "Stato");
                //    bcol.ExportData = false;
                //    bcol.Options.Add(opt);
                //    bcol.Options.Add(opt2);
                //    bcol.Options.Add(opt3);
                //    bcol.DatabaseHrefValueFields.Add("id_User");

                //    table.Columns.Add(bcol);
                //}
                //col = new AxtActionColumn("id_User", "Scheda e Opzioni", "action details", "user_details.aspx?id={id_User}");
                //col.DatabaseHrefValueFields.Add("id_User");
                //table.Columns.Add(col);

                //if (this.AddToGroups.IsValidInteger)
                //{
                //    NetCms.Users.Group group = NetCms.Users.GroupsHandler.RootGroup.FindGroup(this.AddToGroups.IntValue);

                //    col = new AxtActionColumn("id_User", "Aggiungi Utente a Gruppo '" + group.Name + "'", "action add", "javascript: setFormValue({0},'SelectedUser','1')");
                //    col.DatabaseHrefValueFields.Add("id_User");
                //    table.Columns.Add(col);
                //}
                //HtmlInputHidden setUserToAdd = new HtmlInputHidden();
                //setUserToAdd.ID = "SelectedUser";
                //output.Controls.Add(setUserToAdd);
                //output.Controls.Add(table.Control);
                //#endregion 
                #endregion
            }
            else
            {
                // no record message
                output.Controls.Add(new LiteralControl("<p>Nessun record trovato.</p>"));
            }
           // output.Controls.Add(SearchResultTable);
            

        }
    }
}