using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetTable;
using NetCms.GUI;
using NetCms.Vertical.Grants;
using NetCms.Users;
using NetService.Utility.ArTable;
using System.Collections.Generic;

namespace NetCms.Users.UsersManager
{
    [DynamicUrl.PageControl("user_groups.aspx", ApplicationID = UsersApplication.ApplicationID)]
    public class UsersGroupsPage : AbstractPage
    {
        private User _RequestedUser;
        private User RequestedUser
        {
            get
            {
                if (_RequestedUser == null)
                {
                    NetUtility.RequestVariable req = new NetUtility.RequestVariable("id", NetUtility.RequestVariable.RequestType.QueryString);
                    if (req.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                        //_RequestedUser = new User(req.IntValue.ToString());
                        _RequestedUser = UsersBusinessLogic.GetById(req.IntValue);
                }
                if (_RequestedUser == null || _RequestedUser.Anagrafica == null)
                    NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

                return _RequestedUser;
            }
        }

        public override string PageTitle
        {
            get { return base.PageTitle + "Gestione Gruppi dell'Utente '" + this.RequestedUser.NomeCompleto + "'"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User; }
        }


        public UsersGroupsPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            SetToolbars();

            if (!GrantsManager[UsersApplication.ApplicationSystemName + "_groups"].Allowed)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

            CheckGrantsOnThisUser(RequestedUser);
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Table, "Elenco Utenti"));
            this.Toolbar.Buttons.Add(new ToolbarButton("user_details.aspx?id=" + this.RequestedUser.ID, NetCms.GUI.Icons.Icons.Page_White_Magnify, "Scheda Utente"));
        }

        public override WebControl Control
        {
            get 
            {
                WebControl root = new WebControl(HtmlTextWriterTag.Div);
                root.CssClass = "users_groups";
                root.Controls.Add(GetInformationsControl());
                root.Controls.Add(GetAddGroupsControl());
                root.Controls.Add(GetAssociationsListControl());
                
                return root;
            }
        }

        private WebControl GetInformationsControl()
        {
            Collapsable info = new Collapsable("Informazioni sulla gestione dei gruppi");

            string InnerHtml = "<p>Questa pagina permette di creare associazioni tra gruppi e utenti. Per ogni utente � necessario specificare almeno un associazione con un gruppo, in caso contrario il sistema associer� automaticamente l'utente al gruppo principale del sistema.</p>";
            InnerHtml += "<p>Per aggiungere un associazione tra utente e gruppo all'elenco dei gruppi dell'utente � necessario selezionare, dalla tabella 'Gruppi Disponibili', una delle voci relative al ruolo (Utente,Amministratore o Super Amministratore) presente nella riga relativa al gruppo che intendete associare.</p>";

            info.AppendControl(new LiteralControl(InnerHtml));

            return info;
        }

//        private WebControl GetAddGroupsControl()
//        {
//            Collapsable groups = new Collapsable("Gruppi Disponibili");

//            HtmlGenericControls.InputField groupToAdd = new HtmlGenericControls.InputField("GroupToAdd");
//            if (groupToAdd.RequestVariable.IsValidInteger && groupToAdd.RequestVariable.IntValue > 0)
//            {

//                this.Connection.Execute("DELETE FROM UsersGroups WHERE User_UserGroup = " + this.RequestedUser.ID + " AND  Group_UserGroup = " + groupToAdd.RequestVariable.StringValue);

//                string sql = "INSERT INTO UsersGroups (User_UserGroup,Group_UserGroup,Type_UserGroup,PublishRole_UserGroup) VALUES (" + this.RequestedUser.ID + "," + groupToAdd.RequestVariable.StringValue + ",0,0)";
//                this.Connection.Execute(sql);
               
//                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
//                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha aggiunto il gruppo con id=" + groupToAdd.RequestVariable.IntValue + " all'utente " + this.RequestedUser.NomeCompleto + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

//                GUI.PopupBox.AddMessage("Gruppo Aggiunto, la modifica sar� attiva al prossimo login dell'utente '" + this.RequestedUser.NomeCompleto + "'", PostBackMessagesType.Notify);

//            }            

//            NetCms.Users.Search.UserRelated rels = new NetCms.Users.Search.UserRelated(Administrator, Connection);
//            string sql1 = @"SELECT DISTINCT groups.*,groups_macrogroups.*  FROM groups                
//                                LEFT JOIN usersgroups ON (usersgroups.Group_UserGroup = groups.id_Group)
//                                LEFT JOIN groups_macrogroups ON (id_MacroGroup = groups.MacroGroup_Group) 
//                                WHERE false " + rels.MyGroupsConditions + " ORDER BY Tree_Group";
//            SmTable table = new SmTable(1, sql1, this.Connection);
//            table.CssClass = "GenericTable";
//            table.Pagination = false;

//            #region Campi della tabella

//            SmTableColumn col;

//            SmTableTreeColumn2 treecol = new SmTableTreeColumn2("Nome", "id_Group", "Name_Group", "Depth_Group", this.Connection);
//            treecol.IconType = SmTableTreeColumn2.IconTypes.CssClass;
//            treecol.Icon = "GrantsTableGroupIcon";
//            table.addColumn(treecol);

//            col = new SmTableActionColumn("id_Group", "action add", "Associa", "javascript: setFormValue('{0}','GroupToAdd',1);");
//            table.addColumn(col);
//            #endregion

//            groups.AppendControl(groupToAdd.Control);
//            groups.AppendControl(table.getTable());

//            return groups;
//        }

        private WebControl GetAddGroupsControl()
        {
            Collapsable groups = new Collapsable("Gruppi Disponibili");

            HtmlGenericControls.InputField groupToAdd = new HtmlGenericControls.InputField("GroupToAdd");
            if (groupToAdd.RequestVariable.IsValidInteger && groupToAdd.RequestVariable.IntValue > 0)
            {
                GroupAssociation ga = GroupsBusinessLogic.GetGroupAssociationByUserAndGroup(this.RequestedUser.ID, groupToAdd.RequestVariable.IntValue);
                if (ga == null)
                {
                    ga = new GroupAssociation();
                    ga.User = this.RequestedUser;
                    ga.Group = GroupsBusinessLogic.GetGroupById(groupToAdd.RequestVariable.IntValue);
                    ga.Type = 0;
                    ga.PublishRole = 0;
                    GroupsBusinessLogic.SaveOrUpdateGroupAssociation(ga,true);
                    
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha aggiunto il gruppo con id=" + groupToAdd.RequestVariable.IntValue + " all'utente " + this.RequestedUser.NomeCompleto + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                    GUI.PopupBox.AddMessage("Gruppo Aggiunto, la modifica sar� attiva al prossimo login dell'utente '" + this.RequestedUser.NomeCompleto + "'", PostBackMessagesType.Notify);
                }
                else
                {
                    GUI.PopupBox.AddMessage("Gruppo gi� associato all'utente '" + this.RequestedUser.NomeCompleto + "'", PostBackMessagesType.Notify);
                }
            }

            ArTable table = new ArTable();
            table.InnerTableCssClass = "tab";
            table.NoRecordMsg = "Nessun gruppo trovato";

            ICollection<Group> groupsList = GroupsBusinessLogic.FindGroupsByUser(Administrator);

            table.Records = groupsList;

            #region Campi della tabella
            
            // SISTEMARE SE SERVE
            //SmTableTreeColumn2 treecol = new SmTableTreeColumn2("Nome", "id_Group", "Name_Group", "Depth_Group", this.Connection);
            //treecol.IconType = SmTableTreeColumn2.IconTypes.CssClass;
            //treecol.Icon = "GrantsTableGroupIcon";
            //table.addColumn(treecol);

            ArGroupTreeColumn nome = new ArGroupTreeColumn("Nome", "Name",groupsList.ToList<object>());
            nome.Icon = "GrantsTableGroupIcon";
            table.AddColumn(nome);

            //ArTextColumn nome = new ArTextColumn("Nome", "Name");
            //table.AddColumn(nome);

            ArActionColumn actionCol = new ArActionColumn("Associa", ArActionColumn.Icons.Add, "javascript: setFormValue('{ID}','GroupToAdd',1);");
            table.AddColumn(actionCol);

            #endregion

            groups.AppendControl(groupToAdd.Control);
            groups.AppendControl(table);

            return groups;
        }

        #region Metodi riguardanti la lista dei gruppi associati

        private void RemoveAssociationHandler(HtmlGenericControls.InputField RemoveGroupField)
        {
            if (RemoveGroupField.RequestVariable.IsPostBack && RemoveGroupField.RequestVariable.IsValidInteger)
            {
                string gname = "";
                foreach (GroupAssociation assoc in RequestedUser.AssociatedGroupsOrdered)
                {
                    if (assoc.GroupID == RemoveGroupField.RequestVariable.IntValue)
                    {
                        gname = assoc.Group.Name;
                        break;
                    }
                }

                //this.Connection.Execute("DELETE FROM UsersGroups WHERE User_UserGroup = " + this.RequestedUser.ID + " AND  Group_UserGroup = " + RemoveGroupField.RequestVariable.StringValue);

                GroupAssociation ga = GroupsBusinessLogic.GetGroupAssociationByUserAndGroup(this.RequestedUser.ID, RemoveGroupField.RequestVariable.IntValue);
                if (ga != null)
                {
                    GroupsBusinessLogic.DeleteGroupAssociation(ga,true);
                }
                
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha rimosso l'utente " + this.RequestedUser.NomeCompleto + " dal gruppo con id=" + RemoveGroupField.RequestVariable.StringValue + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString() + "'.", userName);

                GUI.PopupBox.AddMessage("Gruppo rimosso, la modifica sar� attiva al prossimo login dell'utente '" + this.RequestedUser.NomeCompleto + "'", PostBackMessagesType.Notify);
            }
        }

        private WebControl GetAssociationsListControl()
        {
            WebControl Associations = new WebControl(HtmlTextWriterTag.Div);

            HtmlGenericControls.InputField RemoveGroupField = new HtmlGenericControls.InputField("RemoveGroupField");
            Associations.Controls.Add(RemoveGroupField.Control);
            if (this.IsPostBack) RemoveAssociationHandler(RemoveGroupField);
            else
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina gestione gruppi dell'utente " + this.RequestedUser.NomeCompleto + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

            }

            Associations.PreRender += new EventHandler(Associations_Init);

            //Associations.Controls.Add(GetAssociationsListTable());


            return Associations;
        }
        void Associations_Init(object sender, EventArgs e)
        {
            ((WebControl)sender).Controls.Add(GetAssociationsListTable());
        }
        private HtmlGenericControl GetAssociationsListTable()
        {
            Conn.CacheEnabled = false;
            string sql = "SELECT * FROM Users";
            sql += " INNER JOIN usersgroups ON (users.id_User = usersgroups.User_UserGroup)";
            sql += " INNER JOIN groups ON (usersgroups.Group_UserGroup = groups.id_Group)";
            sql += " WHERE id_User = " + this.RequestedUser.ID;
            sql += " ORDER BY Tree_Group";

            SmTable table = new SmTable(0, sql, Conn);
            table.OverTitle = "Gruppi Associati all'Utente '" + this.RequestedUser.NomeCompleto + "'";
            //table.CssClass = "tab";

            HtmlGenericControl control = new HtmlGenericControl("div");
            
            HtmlGenericControls.InputField groupToAdd = new HtmlGenericControls.InputField("GroupToEdit");
            if (groupToAdd.RequestVariable.IsPostBack)
            {

                G2Core.Common.RequestVariable admin = new G2Core.Common.RequestVariable("adminField" + groupToAdd.RequestVariable.StringValue);
                G2Core.Common.RequestVariable content = new G2Core.Common.RequestVariable("contentField" + groupToAdd.RequestVariable.StringValue);


                if (admin.IsValidInteger && content.IsValidInteger && groupToAdd.RequestVariable.IsValidInteger)
                {
                    this.Conn.Execute("DELETE FROM UsersGroups WHERE User_UserGroup = " + this.RequestedUser.ID + " AND Group_UserGroup = " + groupToAdd.RequestVariable.StringValue);

                    string sqlI = "INSERT INTO UsersGroups (User_UserGroup,Group_UserGroup,Type_UserGroup,PublishRole_UserGroup) VALUES (" + this.RequestedUser.ID + "," + groupToAdd.RequestVariable.StringValue + "," + admin + "," + content + ")";
                    this.Conn.Execute(sqlI);
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha aggiornato i ruoli dell'utente " + this.RequestedUser.NomeCompleto + " nel gruppo con id=" + groupToAdd.RequestVariable.StringValue + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString() + "'.", userName);

                    GUI.PopupBox.AddMessage("Ruoli Aggiornati con Successo, la modifica sar� attiva al prossimo login dell'utente '" + this.RequestedUser.NomeCompleto + "'", PostBackMessagesType.Notify);

                }
            }

            #region Campi della tabella

            SmTableColumn col = new SmTableColumn("Name_Group", "Gruppo");
            table.addColumn(col);

            col = new SmTableGroupColumn("Percorso", this.Conn);
            table.addColumn(col);

            //col = new SmTableMultiOptionColumn("Type_UserGroup", "Ruoli", "Utente,Amministratore,Super Amministratore");
            //table.addColumn(col);

            //col = new SmTableMultiOptionColumn("PublishRole_UserGroup", "Ruoli", "Redattore,Revisore,Publicatore");
            //table.addColumn(col);

            col = new GroupsCustomColumn_UsersRole("id_Group", "Ruolo di Gestione Utenti", this.Administrator);
            table.addColumn(col);
            col = new GroupsCustomColumn_ContentsRole("id_Group", "Ruolo di Gestione Contenuti", this.Administrator);
            table.addColumn(col);

            col = new GroupsCustomActionColumn("id_Group", "action modify", "Salva Ruoli", "javascript: setFormValue({0},'" + groupToAdd.ID + "',1)", this.Administrator);
            table.addColumn(col);

            col = new GroupsCustomActionColumn("id_Group", "action delete", "Rimuovi", "javascript: setFormValue({0},'RemoveGroupField',1)", this.Administrator);
            table.addColumn(col);

            #endregion
            control.Controls.Add(groupToAdd.Control);
            control.Controls.Add(table.getTable());
            Conn.CacheEnabled = true;
            return control;
        }

        #endregion

        #region Custom SMTable Columns per

        private class GroupsCustomColumn_UsersRole : GroupsCustomColumn
        {
            public GroupsCustomColumn_UsersRole(string fieldName, string Caption, NetCms.Users.User account)
                : base(fieldName, Caption, account)
            {
            }

            public override HtmlGenericControl getFieldLocal(DataRow row, Users.Group gruppo)
            {
                HtmlGenericControl td = new HtmlGenericControl("td");
                bool super = gruppo.ImSuperAdmin(PageData.CurrentReference.Account);
                bool admin = gruppo.ImAdmin(PageData.CurrentReference.Account);
                string typeUG = row["Type_UserGroup"].ToString();
                if (admin)
                {
                    if (!super && typeUG == "2")
                    {
                        td.Attributes["class"] = "tr-ddl";
                        td.Controls.Add(new LiteralControl("Super Amministratore"));
                        td.Controls.Add(new LiteralControl("<input type=\"hidden\" value=\"2\" id=\"" + "adminField" + row[this.FieldName].ToString() + "\" name=\"" + "adminField" + row[this.FieldName].ToString() + "\" />"));
                    }
                    else
                    {
                        DropDownList adminList = new DropDownList();
                        adminList.ID = "adminField" + row[this.FieldName].ToString();


                        ListItem item = new ListItem("Utente", "0");
                        item.Selected = typeUG == item.Value;
                        adminList.Items.Add(item);

                        item = new ListItem("Amministratore", "1");
                        item.Selected = typeUG == item.Value;
                        adminList.Items.Add(item);

                        if (super)
                        {
                            item = new ListItem("Super Amministratore", "2");
                            item.Selected = typeUG == item.Value;
                            adminList.Items.Add(item);
                        }

                        td.Attributes["class"] = "tr-ddl";
                        td.Controls.Add(adminList);
                    }
                }
                else
                {
                    td.Attributes["class"] = "tr-ddl";
                    switch (typeUG)
                    {
                        case "0": td.Controls.Add(new LiteralControl("Utente")); break;
                        case "1": td.Controls.Add(new LiteralControl("Amministratore")); break;
                        case "2": td.Controls.Add(new LiteralControl("Super Amministratore")); break;
                    }
                    td.Controls.Add(new LiteralControl("<input type=\"hidden\" value=\"" + typeUG + "\" id=\"" + "adminField" + row[this.FieldName].ToString() + "\" name=\"" + "adminField" + row[this.FieldName].ToString() + "\" />"));
                  
                }
                return td;
            }
        }
        private class GroupsCustomColumn_ContentsRole : GroupsCustomColumn
        {
            public GroupsCustomColumn_ContentsRole(string fieldName, string Caption, NetCms.Users.User account)
                : base(fieldName, Caption, account)
            {
            }

            public override HtmlGenericControl getFieldLocal(DataRow row, Users.Group gruppo)
            {
                bool ICanHandleThisAssociation = CheckIfICanHandleThisAssociation(gruppo);
                if (ICanHandleThisAssociation)
                {
                    DropDownList contentsList = new DropDownList();
                    contentsList.ID = "contentField" + row[this.FieldName].ToString();

                    string PublishRole_UserGroup = row["PublishRole_UserGroup"].ToString();

                    ListItem item = new ListItem("Redattore", "0");
                    item.Selected = PublishRole_UserGroup == item.Value;
                    contentsList.Items.Add(item);

                    item = new ListItem("Revisore", "1");
                    item.Selected = PublishRole_UserGroup == item.Value;
                    contentsList.Items.Add(item);

                    item = new ListItem("Pubblicatore", "2");
                    item.Selected = PublishRole_UserGroup == item.Value;
                    contentsList.Items.Add(item);

                    HtmlGenericControl td = new HtmlGenericControl("td");
                    td.Attributes["class"] = "tr-ddl";
                    td.Controls.Add(contentsList);
                    return td;
                }
                else
                {
                    HtmlGenericControl td = new HtmlGenericControl("td");
                    td.Attributes["class"] = "tr-ddl";
                    switch (row["PublishRole_UserGroup"].ToString())
                    {
                        case "0": td.Controls.Add(new LiteralControl("Redattore")); break;
                        case "1": td.Controls.Add(new LiteralControl("Revisore")); break;
                        case "2": td.Controls.Add(new LiteralControl("Pubblicatore")); break;
                    }
                    return td;
                }
            }
        }

        private abstract class GroupsCustomColumn : SmTableColumn
        {
            public NetCms.Users.User Account { get; private set; } 

            public GroupsCustomColumn(string fieldName, string Caption, NetCms.Users.User account)
                : base(fieldName, Caption)
            {
                this.Account = account;
            }

            public override HtmlGenericControl getField(DataRow row)
            {
                Users.Group gruppo = Users.GroupsHandler.RootGroup.FindGroup(int.Parse(row[this.FieldName].ToString()));
                return getFieldLocal(row, gruppo);
            }
            public abstract HtmlGenericControl getFieldLocal(DataRow row, Users.Group gruppo);

            public bool CheckIfICanHandleThisAssociation(Users.Group userGroup)
            {
                return Account.AssociatedGroupsOrdered.Cast<GroupAssociation>().Count(x => x.CanIHandleGroup(userGroup)) > 0;
            }
        }
        private class GroupsCustomActionColumn : SmTableActionColumn
        {
            public NetCms.Users.User Account { get; private set; }

            public GroupsCustomActionColumn(string fieldName, string classe, string Caption, string href, NetCms.Users.User account)
                : base(fieldName, classe, Caption, href)
            {
                this.Account = account;
            }

            public override HtmlGenericControl getField(DataRow row)
            {
                Users.Group gruppo = Users.GroupsHandler.RootGroup.FindGroup(int.Parse(row[this.FieldName].ToString()));

                bool ICanHandleThisAssociation = CheckIfICanHandleThisAssociation(gruppo);
                if (ICanHandleThisAssociation) return base.getField(row);
                else
                {
                    HtmlGenericControl td = new HtmlGenericControl("td");
                    td.Controls.Add(new LiteralControl("&nbsp;"));
                    return td;
                }
            }

            public bool CheckIfICanHandleThisAssociation(Users.Group userGroup)
            {
                return Account.AssociatedGroupsOrdered.Cast<GroupAssociation>().Count(x => x.CanIHandleGroup(userGroup)) > 0;
            }
        }

        /// <summary>
        /// SMTable Column per Path del Gruppo
        /// </summary>
        public class SmTableGroupColumn : SmTableColumn
        {
            private NetCms.Connections.Connection Conn;
            public SmTableGroupColumn(string caption, NetCms.Connections.Connection conn)
                : base("", caption)
            {
                Conn = conn;
            }

            public override HtmlGenericControl getField(DataRow row)
            {
                HtmlGenericControl td = new HtmlGenericControl("td");

                string family = "";

                string[] parents = row["Tree_Group"].ToString().Split('.');
                string sql = "";
                foreach (string parent in parents)
                {
                    if (parent.Length > 0)
                    {
                        if (sql.Length > 0)
                            sql += " OR ";
                        sql += "id_Group = " + parent;
                    }
                }

                DataRow[] rows = Conn.SqlQuery("SELECT * FROM Groups").Select(sql, "Depth_Group");
                foreach (DataRow parent in rows)
                {
                    if (family.Length > 0)
                        family += " /";
                    family += " " + parent["Name_Group"];
                }

                td.InnerHtml = "&nbsp;" + family;

                return td;
            }
        }

        #endregion
    }
}