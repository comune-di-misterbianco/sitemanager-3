using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetTable;
using NetCms.GUI;
using NetCms.Vertical.Grants;
using NetCms.Users;
using NetForms;
using System.Web.Security;

namespace NetCms.Users.UsersManager
{
    [DynamicUrl.PageControl("user_new.aspx", ApplicationID = UsersApplication.ApplicationID)]
    public class UsersNewPage : AbstractPage
    {
        public override string PageTitle
        {
            get { return base.PageTitle + "Creazione Nuovo Utente"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User; }
        }

        private UsersAddVFManager UsersAddForm
        {
            get
            {
                if (_UsersAddForm == null)
                {
                    _UsersAddForm = new UsersAddVFManager("add");
                    _UsersAddForm.CssClass = "Axf";
                }
                return _UsersAddForm;
            }
        }
        private UsersAddVFManager _UsersAddForm;

        //private NetForm _AnagraficaForm;
        //private NetForm AnagraficaForm
        //{
        //    get
        //    {
        //        if (_AnagraficaForm == null)
        //        {
        //            _AnagraficaForm = new NetForm();
        //            _AnagraficaForm.AddSubmitButton = false;
        //            _AnagraficaForm.SubmitSourceID = "BtAggiungi";
        //            _AnagraficaForm.AddTable(AnagraficaFormTable);
        //            _AnagraficaForm.AddTable(UserFormTable);
        //        }

        //        return _AnagraficaForm;
        //    }
        //}

        //private NetFormTable _AnagraficaFormTable;
        //private NetFormTable AnagraficaFormTable
        //{
        //    get
        //    {
        //        if (_AnagraficaFormTable == null)
        //        {

        //            NetFormTable anagrafica = new NetFormTable("Anagrafica", "id_Anagrafica", Connection);

        //            #region Campi Anagrafica

        //            NetForms.NetTextBoxPlus nome = new NetTextBoxPlus("Nome", "Nome_Anagrafica");
        //            nome.Size = 60;
        //            nome.Required = true;
        //            anagrafica.addField(nome);

        //            NetForms.NetTextBoxPlus cognome = new NetTextBoxPlus("Cognome", "Cognome_Anagrafica");
        //            cognome.Size = 60;
        //            cognome.Required = true;
        //            anagrafica.addField(cognome);

        //            NetForms.NetDateField Data = new NetDateField("Data di Nascita", "Data_Anagrafica", Connection);
        //            Data.Size = 60;
        //            //Data.Required = true;
        //            anagrafica.addField(Data);

        //            NetForms.NetTextBoxPlus Indirizzo = new NetTextBoxPlus("Indirizzo", "Indirizzo_Anagrafica");
        //            Indirizzo.Size = 60;
        //            Indirizzo.MaxLenght = 1000;
        //            //citta.Required = true;
        //            anagrafica.addField(Indirizzo);

        //            NetForms.NetTextBoxPlus citta = new NetTextBoxPlus("Citt�", "Citta_Anagrafica");
        //            citta.Size = 60;
        //            //citta.Required = true;
        //            anagrafica.addField(citta);

        //            NetForms.NetDropDownList provincia = new NetDropDownList("Provincia", "Provincia_Anagrafica");
        //            provincia.ClearList();
        //            provincia.addItem("", "0");
        //            provincia.setDataBind("SELECT Nome_Provincia,id_Provincia FROM Provincie ORDER BY Nome_Provincia", Connection);
        //            //citta.Required = true;
        //            anagrafica.addField(provincia);

        //            NetForms.NetTextBoxPlus cap = new NetTextBoxPlus("CAP", "Cap_Anagrafica");
        //            cap.Size = 60;
        //            //citta.Required = true;
        //            cap.MaxLenght = 5;
        //            anagrafica.addField(cap);

        //            NetForms.NetTextBoxPlus telefono = new NetTextBoxPlus("Telefono", "Telefono_Anagrafica");
        //            telefono.Size = 12;
        //            //citta.Required = true;
        //            telefono.MaxLenght = 12;
        //            telefono.ContentType = NetTextBoxPlus.ContentTypes.Phone;
        //            anagrafica.addField(telefono);

        //            NetForms.NetTextBoxPlus cellulare = new NetTextBoxPlus("Cellulare", "Cellulare_Anagrafica");
        //            cellulare.Size = 12;
        //            //citta.Required = true;
        //            cellulare.MaxLenght = 12;
        //            cellulare.ContentType = NetTextBoxPlus.ContentTypes.Phone;
        //            anagrafica.addField(cellulare);

        //            NetForms.NetTextBoxPlus email = new NetTextBoxPlus("e-Mail", "Email_Anagrafica");
        //            email.Size = 60;
        //            email.Required = true;
        //            email.MaxLenght = 255;
        //            email.ContentType = NetTextBoxPlus.ContentTypes.eMail;
        //            anagrafica.addField(email);

        //            NetForms.NetTextBoxPlus codicefiscale = new NetTextBoxPlus("CodiceFiscale", "CodiceFiscale_Anagrafica");
        //            codicefiscale.Size = 16;
        //            codicefiscale.MaxLenght = 16;
        //            anagrafica.addField(codicefiscale);


        //            NetForms.NetDropDownList sesso = new NetDropDownList("Sesso", "Sesso_Anagrafica");
        //            sesso.addItem("M", "0");
        //            sesso.addItem("F", "1");
        //            sesso.Required = true;
        //            anagrafica.addField(sesso);

        //            NetForms.NetTextBoxPlus azienda = new NetTextBoxPlus("Azienda/Ufficio", "Azienda_Anagrafica");
        //            codicefiscale.Size = 60;
        //            anagrafica.addField(azienda);



        //            #endregion

        //            _AnagraficaFormTable = anagrafica;
        //        }

        //        return _AnagraficaFormTable;
        //    }
        //}

        //private NetFormTable _UserFormTable;
        //private NetFormTable UserFormTable
        //{
        //    get
        //    {
        //        if (_UserFormTable == null)
        //        {

        //            NetFormTable user = new NetFormTable("Users", "id_User", Connection);

        //            #region Campi Anagrafica

        //            NetForms.NetTextBoxPlus nomeutente = new NetTextBoxPlus("Nome Utente", "Name_User");
        //            nomeutente.Size = 60;
        //            nomeutente.Required = true;
        //            user.addField(nomeutente);

        //            NetForms.NetPasswordField Password = new NetPasswordField("Password", "Password_User");
        //            Password.CyperType = NetPasswordField.CyperingType.MD5;
        //            Password.HighSecurityField = true;
        //            user.addField(Password);

        //            NetForms.NetExternalField anagrafica = new NetExternalField("Anagrafica", "Anagrafica_User");
        //            user.addExternalField(anagrafica);


        //            NetForms.NetHiddenField anagraficat = new NetHiddenField("AnagraficaType_User");
        //            anagraficat.Value = "'NetCms.Users.Anagrafica'";
        //            user.addField(anagraficat);

        //            NetForms.NetDropDownList NetServiceAdministrator = new NetDropDownList("Tipo di Utente", "Type_User");
        //            NetServiceAdministrator.ClearList();
        //            NetServiceAdministrator.addItem("Utente Frontend", "0");
        //            NetServiceAdministrator.addItem("Utente Backoffice", "1");
        //            user.addField(NetServiceAdministrator);

        //            #endregion

        //            _UserFormTable = user;
        //        }

        //        return _UserFormTable;
        //    }
        //}

        public UsersNewPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            SetToolbars();

            //btPostBack();
            this.UsersAddForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Table, "Elenco Utenti"));
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.UsersAddForm.AnagraficaTypeForm.IsValid == NetService.Utility.ValidatedFields.VfGeneric.ValidationStates.Valid &&
                this.UsersAddForm.UserForm.IsValid == NetService.Utility.ValidatedFields.VfGeneric.ValidationStates.Valid &&
                this.UsersAddForm.AnagraficaForm.IsValid == NetService.Utility.ValidatedFields.VfGeneric.ValidationStates.Valid)
            {
                string emailInserita = this.UsersAddForm.AnagraficaForm.Fields.Find(x => x.Key == "Email").PostBackValue;
                //string userNameUtenteInserito = this.RegistrazioneForm.UserForm.Fields.Find(x => x.Key == "UserName").PostBackValue;

                string customAnagrafeName = this.UsersAddForm.AnagraficaTypeForm.Fields.Find(x => x.Key == "AnagraficheCustom").PostBackValue;
                CustomAnagrafe.CustomAnagrafe customAnagrafe = CustomAnagrafe.CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(customAnagrafeName);
                //CustomAnagrafe.CustomField usernameField = customAnagrafe.CustomFields.First(x => x.IsUsername);                
                //string userNameUtenteInserito = this.UsersAddForm.AnagraficaForm.Fields.Find(x => x.Key == customAnagrafe.DatabaseName + "_" + usernameField.Name.Replace(' ', '_')).PostBackValue;

                CustomAnagrafe.CustomField usernameField = customAnagrafe.CustomFields.ToList().Find(x => x.IsUsername);
                string userNameUtenteInserito = "";
                if (usernameField != null)
                {
                    userNameUtenteInserito = this.UsersAddForm.AnagraficaForm.Fields.Find(x => x.Key == customAnagrafe.DatabaseName + "_" + usernameField.Name.Replace(' ', '_')).PostBackValue;
                }
                else
                {
                    if (customAnagrafe.EmailAsUserName)
                        userNameUtenteInserito = emailInserita;
                }
                
                string password = this.UsersAddForm.UserForm.Fields.Find(x=>x.Key== "Password").PostBackValue;
                password = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();
                string anagraficaType = "Anagrafica" + customAnagrafeName;

                if (UsersBusinessLogic.GetUserByUserName(userNameUtenteInserito) == null)
                {
                    if (UsersBusinessLogic.GetUserByEmail(emailInserita) == null)
                    {
                        //User utenteCreato = UsersBusinessLogic.CreateUser(userNameUtenteInserito, password, this.UsersAddForm.AnagraficaForm, anagraficaType, GroupsBusinessLogic.GetRootGroup().ID);
                        User utenteCreato = UsersBusinessLogic.CreateUser(userNameUtenteInserito, password, this.UsersAddForm.AnagraficaForm, anagraficaType, customAnagrafe.DefaultGroup.ID);
                        if (utenteCreato != null)
                        {
                            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha creato con successo il nuovo utente '" + userNameUtenteInserito + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                            if (GrantsManager[UsersApplication.ApplicationSystemName + "_groups"].Allowed)
                            {
                                PopupBox.AddSessionMessage("Utente creato con successo, ora � possibile associare l'utente ad uno o pi� gruppi e gestirne i ruoli.", PostBackMessagesType.Success, "user_groups.aspx?id=" + utenteCreato.ID);
                            }
                            else
                            {
                                PopupBox.AddSessionMessage("Utente creato con successo", PostBackMessagesType.Success, true);
                            }
                        }
                        else
                        {
                            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore tecnico nell'inserimento dell'utente");
                            PopupBox.AddMessage("Errore tecnico nell'inserimento dell'utente. ", PostBackMessagesType.Error);
                        }
                    }
                    else
                    {
                        PopupBox.AddMessage("Email gi� presente. ", PostBackMessagesType.Notify);
                    }
                }
                else
                {
                    PopupBox.AddMessage("Username gi� utilizzato, sceglierne uno diverso. ", PostBackMessagesType.Notify);
                }
            }
        }
        
        //private void btPostBack()
        //{
        //    if (HttpContext.Current.Request.Form["BtAggiungi"] != null)
        //    {
        //        DataTable table = this.Connection.SqlQuery("SELECT id_User FROM Users WHERE Name_User LIKE '" + HttpContext.Current.Request.Form["Name_User"] + "'");
        //        if (table.Rows.Count == 0)
        //        {
        //            string errors = AnagraficaForm.serializedInsert(HttpContext.Current.Request.Form);

        //            if (UserFormTable.LastInsert > 0)
        //            {
        //                string sql = "";

        //                int ID = UserFormTable.LastInsert;

        //                sql = "";
        //                sql += "INSERT INTO Profiles ";
        //                sql += " (Name_Profile,Type_Profile)";
        //                sql += " Values ('" + HttpContext.Current.Request.Form["Name_User"].Replace("'", "''") + ID + "',0)";
        //                int ProfileID = Connection.ExecuteInsert(sql);


        //                string CreationTime_User = Connection.FilterDateTime(System.DateTime.Now);
        //                string LastAccess_User = Connection.FilterDateTime(System.DateTime.MinValue);

        //                sql = "";
        //                sql += "UPDATE Users SET ";
        //                sql += " Profile_User = " + ProfileID;
        //                sql += ",CreationTime_User = " + CreationTime_User;
        //                sql += ",LastAccess_User = " + LastAccess_User;
        //                sql += ",LastUpdate_User = " + CreationTime_User;
        //                sql += "  WHERE id_User = " + ID;
        //                Connection.Execute(sql);

        //                table = Connection.SqlQuery("SELECT id_Group FROM Groups ORDER BY Tree_Group");
        //                if (table.Rows.Count > 0)
        //                {
        //                    sql = "";
        //                    sql += "INSERT INTO UsersGroups ";
        //                    sql += " (User_UserGroup,Group_UserGroup,Type_UserGroup) VALUES";
        //                    sql += " (" + ID + "," + table.Rows[0]["id_Group"] + ",0)";
        //                    Connection.Execute(sql);
        //                }

        //                if (GrantsManager[UsersApplication.ApplicationSystemName + "_groups"].Allowed)
        //                {
        //                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
        //                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha creato con successo il nuovo utente '" + HttpContext.Current.Request.Form["Name_User"] + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
        //                    PopupBox.AddSessionMessage("Utente Creato con Successo, ora � possibile associare l'utente ad uno o pi� gruppi e gestirne i ruoli.", PostBackMessagesType.Success, "user_groups.aspx?id=" + ID);
        //                }
        //                else
        //                {
        //                    PopupBox.AddSessionMessage("Utente Creato con Successo", PostBackMessagesType.Success, true);
        //                }
        //            }
        //            else
        //                PopupBox.AddMessage(errors, PostBackMessagesType.Notify); //PopupBox.AddMessage("Devi inserire un Username ", PostBackMessagesType.Notify);
        //        }
        //        else
        //            PopupBox.AddMessage("Username gi� utilizzato, sceglierne uno diverso. ", PostBackMessagesType.Notify);
        //    }
        //}

        public override WebControl Control
        {
            get
            {
                WebControl output = new WebControl(HtmlTextWriterTag.Div);
                output.Controls.Add(UsersAddForm);
                return output;
            }
        }
    }
}