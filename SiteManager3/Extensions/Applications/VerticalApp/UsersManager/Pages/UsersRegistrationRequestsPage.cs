﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetTable;
using System.Collections.Generic;
using NetCms.Users.Search;
using System.Linq;
using NetService.Utility.ArTable;
using NetCms.Vertical.Grants;
using NetService.Utility.Controls;
using System.Web.Security;

namespace NetCms.Users.UsersManager
{
    [DynamicUrl.PageControl("reg_requests.aspx", ApplicationID = UsersApplication.ApplicationID)]
    public class UsersRegistrationRequestsPage : AbstractPage
    {        
        public override string PageTitle
        {
            get { return base.PageTitle + "Richieste di registrazione"; }
        }

        public ICollection<User> UsersTable
        {
            get
            {
                if (_UsersTable == null)
                {
                    Search.UserRelated UsersDataManager = new Search.UserRelated(NetCms.Users.AccountManager.CurrentAccount);
                    SearchCondition RegistrationCondition = new SearchCondition("Registrato",false, SearchCondition.ComparisonType.Equals, SearchCondition.CriteriaType.User);
                    _UsersTable = UsersDataManager.UsersIAdmin((UsersSearchControl.ButtonRequest.IsPostBack && UsersSearchControl.ButtonRequest.IsValidString) ? 0 : Pagination.CurrentPage - 1, RecordPerPage, null, new SearchCondition[] { RegistrationCondition });
                                        
                }
                return _UsersTable;
            }
        }
        private ICollection<User> _UsersTable;

        public int UsersTableCount
        {
            get
            {
                Search.UserRelated UsersDataManager = new Search.UserRelated(NetCms.Users.AccountManager.CurrentAccount);
                SearchCondition RegistrationCondition = new SearchCondition("Registrato", false, SearchCondition.ComparisonType.Equals, SearchCondition.CriteriaType.User);
                _UsersTableCount = UsersDataManager.UsersIAdminCount(null, new SearchCondition[] { RegistrationCondition });
                return _UsersTableCount;
            }
        }
        private int _UsersTableCount;

        private int RecordPerPage = 10;

        public ArTable SearchResultTable
        {
            get
            {
                if (_SearchResultTable == null)
                {
                    _SearchResultTable = new ArTable();
                    _SearchResultTable.EnablePagination = true;
                    _SearchResultTable.RecordPerPagina = RecordPerPage;
                    _SearchResultTable.NoSearchParameter = true;
                    _SearchResultTable.InnerTableCssClass = "tab";
                    _SearchResultTable.GoToFirstPageOnSearch = true;
                }
                return _SearchResultTable;
            }
        }
        private ArTable _SearchResultTable;

        public PaginationHandler Pagination
        {
            get
            {
                if (_Pagination == null)
                {

                    _Pagination = new PaginationHandler(RecordPerPage, TotalRecordCount(), SearchResultTable.PaginationKey, (UsersSearchControl.ButtonRequest.IsPostBack && UsersSearchControl.ButtonRequest.IsValidString));
                }
                return _Pagination;
            }
        }
        private PaginationHandler _Pagination;

        private int TotalRecordCount()
        {
            int count = 0;
            switch (UsersSearchControl.SearchStatus)
            {
                case UsersSearchControl.SearchStatuses.SearchAvailable: count = this.UsersSearchControl.GetSearchResultCount(NetCms.Users.AccountManager.CurrentAccount); break;
                case UsersSearchControl.SearchStatuses.NoSearch: count = UsersTableCount; break;
                case UsersSearchControl.SearchStatuses.WrongData: break;
            }
            return count;
        }

       
        public Search.UsersSearchControl UsersSearchControl
        {
            get
            {
                if (_UsersSearchControl == null)
                {
                    SearchCondition RegistrationCondition = new SearchCondition("Registrato", false, SearchCondition.ComparisonType.Equals, SearchCondition.CriteriaType.User);
                    _UsersSearchControl = new Search.UsersSearchControl(null, new SearchCondition[] { RegistrationCondition },true);
                }
                return _UsersSearchControl;
            }
        }
        private Search.UsersSearchControl _UsersSearchControl;


        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Vcard_Edit; }
        }
        
        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Table, "Elenco Utenti"));
        }

        public UsersRegistrationRequestsPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            if(!GrantsManager.Grant)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

            SetToolbars();

            string script = @"<script type='text/javascript'>
            $(function () {
                $('#dialog').dialog({
                autoOpen: false,
                closeOnEscape: false,
                resizable: false,
                draggable: false,
                modal: true,
                title: 'Inserisci la motivazione',
                    buttons: {
                        Conferma: function () {
                            $('[id*=submitMotivazione]').click();
                        },
                        Annulla: function () {    
                            $(this).dialog('close');
                            if ($('#MotivazioneForm').find('.not-valid').length > 0) {
                                $('#MotivazioneForm').find('.not-valid>.validation-results').hide();
                            }
                        }
                    }
               });

                ";

            foreach (UserSearchRowTemplate utente in DataTableUtenti)
            {
                script += @"
                            
                            $('#deny_" + utente.id_User + @"').click(function () {
                                $('#UtenteSelezionato').val('" + utente.id_User + @"');
                                $('#dialog').dialog('open');
                                $('#dialog').parent().appendTo($('form:first'));
                                return false;
                            });";
            }

            script += @" 
        });</script>";

            OuterScripts.Add(script);
        }

        public MotivazioneRespintaForm MotivazioneForm
        {
            get
            {
                if (_MotivazioneForm == null)
                {
                    _MotivazioneForm = new MotivazioneRespintaForm("MotivazioneForm");
                    _MotivazioneForm.CssClass = "Axf";
                    _MotivazioneForm.SubmitButton.ID = "submitMotivazione";
                    _MotivazioneForm.SubmitButton.Style.Add(HtmlTextWriterStyle.Display, "none");
                }
                return _MotivazioneForm;
            }
        }
        private MotivazioneRespintaForm _MotivazioneForm;

        public ICollection<UserSearchRowTemplate> DataTableUtenti
        {
            get 
            {
                if (_DataTableUtenti == null)
                {
                    _DataTableUtenti = new List<UserSearchRowTemplate>();

                    switch (UsersSearchControl.SearchStatus)
                    {
                        case UsersSearchControl.SearchStatuses.SearchAvailable: _DataTableUtenti = this.UsersSearchControl.GetSearchResult((UsersSearchControl.ButtonRequest.IsPostBack && UsersSearchControl.ButtonRequest.IsValidString) ? 0 : Pagination.CurrentPage - 1, RecordPerPage, NetCms.Users.AccountManager.CurrentAccount); break;
                        case UsersSearchControl.SearchStatuses.NoSearch:
                            foreach (User user in UsersTable)
                            {
                                try
                                {
                                    if (_DataTableUtenti.Where(x => x.id_User == user.ID).Count() == 0)
                                    {
                                        _DataTableUtenti.Add(new UserSearchRowTemplate()
                                        {
                                            id_User = user.ID,
                                            Name_User = user.UserName,
                                            State_User = user.State,
                                            AnagraficaType_User = user.AnagraficaType,
                                            Anagrafica_User = user.AnagraficaID,
                                            AnagraficaNome = user.NomeCompleto,
                                            Profile_User = user.Profile.ID,
                                            Registered_User = user.Registrato
                                        });
                                    }
                                }
                                catch (Exception ex)
                                {
                                    //trace
                                }
                            }
                            break;
                        case UsersSearchControl.SearchStatuses.WrongData: break;
                    }
                }
                return _DataTableUtenti;
            }
        }
        private ICollection<UserSearchRowTemplate> _DataTableUtenti;

        public override WebControl Control
        {
            get
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);

               
                if (this.UsersSearchControl.SearchStatus == Search.UsersSearchControl.SearchStatuses.WrongData)
                    PopupBox.AddMessage("I dati di ricerca immessi non sono validi, assicurarsi di aver riempito almeno un campo e di aver inserito dati corretti.", PostBackMessagesType.Notify);


                NetCms.GUI.Collapsable collapsableSearch = new Collapsable("Ricerca");


                collapsableSearch.StartCollapsed = (UsersSearchControl.SearchStatus == UsersSearchControl.SearchStatuses.SearchAvailable || UsersSearchControl.SearchStatus == UsersSearchControl.SearchStatuses.NoSearch);
                collapsableSearch.AppendControl(UsersSearchControl);

                value.Controls.Add(collapsableSearch);

                NetCms.GUI.Collapsable collapsableTable = new Collapsable("Elenco degli Utenti");
                HtmlGenericControl output = new HtmlGenericControl("div");

                collapsableTable.AppendControl(output);
                value.Controls.Add(collapsableTable);

                SearchResultTable.PagesControl = Pagination;

                if (DataTableUtenti.Count() > 0)
                {

                    SearchResultTable.Records = DataTableUtenti;
                    SearchResultTable.CustomSearchArray = UsersSearchControl.WorkerFinder.Finder.CustomSearchFields.Select(x => x.QueryString).ToArray();

                    //Riga necessaria se si abilita il filtraggio per anagrafica
                    if (UsersSearchControl.EnableSearchByAnagrafe)
                        SearchResultTable.CustomSearchArray = SearchResultTable.CustomSearchArray.Union(new string[] { (UsersSearchControl.TipologiaAnagrafiche.GetFieldQueryString()) }).ToArray();

                    ArTextColumn colName = new ArTextColumn("User Name", "Name_User");
                    SearchResultTable.AddColumn(colName);

                    ArTextColumn colAnagraficaName = new ArTextColumn("Nome", "AnagraficaNome");
                    SearchResultTable.AddColumn(colAnagraficaName);

                    ArOptionsColumn colRegistrato = new ArOptionsColumn("Registrazione", "Registered_User", new string[] { "In attesa di accettazione", "Registrazione Accettata" });
                    SearchResultTable.AddColumn(colRegistrato);

                    ArActionColumn colDettagli = new ArActionColumn("Scheda e Opzioni", ArActionColumn.Icons.Details, Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/users/user_details.aspx?id={id_User}");
                    SearchResultTable.AddColumn(colDettagli);


                    ArButtonColumn colAccetta = new ArButtonColumn("Accetta", "id_User");
                    colAccetta.ButtonIDOffset = "accept";
                    colAccetta.ClickHandler += new EventHandler(AcceptRequest);    
                    SearchResultTable.AddColumn(colAccetta);

                    ArButtonColumn colRifiuta = new ArButtonColumn("Rifiuta", "id_User");
                    colRifiuta.ButtonIDOffset = "deny";
                    SearchResultTable.AddColumn(colRifiuta);
                }
                value.Controls.Add(SearchResultTable);

                WebControl divModale = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
                divModale.ID = "dialog";
                divModale.Style.Add(HtmlTextWriterStyle.Display, "none");
                divModale.CssClass = "richiedi_modifica";

                MotivazioneForm.SubmitButton.Attributes["runat"] = "server";
                MotivazioneForm.SubmitButton.Click += new EventHandler(DenyRequest);
                divModale.Controls.Add(MotivazioneForm);
                value.Controls.Add(divModale);

                return value;
            }

            
        }


        void AcceptRequest(object sender, EventArgs e)
        {
            string idBtn = ((Button)sender).ID;

            string id = idBtn.Split('_')[1];

            User RequestedUser = UsersBusinessLogic.GetById(int.Parse(id));
            // devo generare una nuova password da inviare all'utente in chiaro perchè non è possibile decriptare quella nel db
            G2Core.Common.PasswordGenerator gen = new G2Core.Common.PasswordGenerator(8);

            string password = gen.Generate();

            UsersBusinessLogic.SendConfirmMail(RequestedUser, password, RequestedUser.CurrentNetworkPath);

            // e poi la salvo criptata nel db
            RequestedUser.Password = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();
            RequestedUser.Registrato = true;

            UsersBusinessLogic.SaveOrUpdateUser(RequestedUser);

            NetCms.GUI.PopupBox.AddMessageAndReload("Registrazione accettata con successo", PostBackMessagesType.Success);
           
        }

        void DenyRequest(object sender, EventArgs e)
        {
            //string idBtn = ((Button)sender).ID;

            string id = MotivazioneForm.SelectionIndex.PostBackValue;

            User RequestedUser = UsersBusinessLogic.GetById(int.Parse(id));
               
            MotivazioneForm.Motivazione.Validate();
            if (MotivazioneForm.Motivazione.ValidationState == NetService.Utility.ValidatedFields.VfGeneric.ValidationStates.Valid)
            {
                UsersBusinessLogic.SendRejectMail(RequestedUser, MotivazioneForm.Motivazione.PostBackValue);
                UsersBusinessLogic.DeleteUser(RequestedUser);
                NetCms.GUI.PopupBox.AddMessageAndReload("Registrazione negata con successo", PostBackMessagesType.Notify);
            }
            else
            {
                var page = HttpContext.Current.CurrentHandler as Page;
                if (page != null)
                    page.ClientScript.RegisterClientScriptBlock(page.GetType(), "script", "$(function() {$('#dialog').dialog('open');});", true);
            }
        }
    }
}
