﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;

namespace NetCms.Users.UsersManager
{
    [DynamicUrl.PageControl("default.aspx", ApplicationID = UsersApplication.ApplicationID)]
    public class MainPage : AbstractPage
    {      
        public override string PageTitle
        {
            get { return "Utenti"; }
        }
        
        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User; }
        }

        private void SetToolbars()
        {
            //Mostro la creazione utenti solo ad utenti di tipo anagrafica interna
            if (GrantsManager[UsersApplication.ApplicationSystemName + "_create"].Allowed)
            {
                //if (this.Administrator.AnagraficaType == "NetCms.Users.Anagrafica")
                    this.Toolbar.Buttons.Add(new ToolbarButton("user_new.aspx", NetCms.GUI.Icons.Icons.Vcard_Add, "Crea Utente"));
            }

            if (GrantsManager[UsersApplication.ApplicationSystemName + "_ungroupped"].Allowed)
                this.Toolbar.Buttons.Add(new ToolbarButton("nogroups.aspx", NetCms.GUI.Icons.Icons.Table, "Utenti Non Associati a Gruppi"));

            if (System.Web.Configuration.WebConfigurationManager.AppSettings["ManageRegRequests"] == "1")
                this.Toolbar.Buttons.Add(new ToolbarButton("reg_requests.aspx", NetCms.GUI.Icons.Icons.Vcard_Edit, "Richieste di registrazione"));

        }
        
        public MainPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            if (!GrantsManager.Grant)
                PopupBox.AddNoGrantsMessageAndGoBack();

            SetToolbars();
        }

        public override WebControl Control
        {
            get
            {
                return new UsersListControl(/*this.Connection,*/ this.Administrator);
            }
        }
    }
}
