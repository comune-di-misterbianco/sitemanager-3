using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetTable;
using NetCms.GUI;
using NetCms.Vertical.Grants;
using NetCms.Users;
using System.Collections.Generic;
using System.Web.Security;
using NetCms.Grants;

namespace NetCms.Users.UsersManager
{
    [DynamicUrl.PageControl("user_details.aspx", ApplicationID = UsersApplication.ApplicationID)]
    public class UsersDetailsPage : AbstractPage
    {
        public override string PageTitle
        {
            get { return base.PageTitle + " Dettagli Utente '" + this.RequestedUser.NomeCompleto + "'"; }
        }

        private AnagraficaBase Anagrafica
        {
            get
            {
                return RequestedUser.Anagrafica;
            }
        }

        private User _RequestedUser;
        private User RequestedUser
        {
            get
            {
                if (_RequestedUser == null)
                {
                    NetUtility.RequestVariable req = new NetUtility.RequestVariable("id", NetUtility.RequestVariable.RequestType.QueryString);
                    if (req.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                        //_RequestedUser = new User(req.IntValue.ToString());
                        _RequestedUser = UsersBusinessLogic.GetById(req.IntValue);
                }
                if (_RequestedUser == null || _RequestedUser.Anagrafica == null)
                    throw new NetCms.Exceptions.ServerError500Exception("L'utente selezionato non esiste."); 

                return _RequestedUser;
            }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Table, "Elenco Utenti"));

            if (GrantsManager[UsersApplication.ApplicationSystemName + "_grants"].Value == GrantsValues.Allow)
                this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/grants/review.aspx?pid=" + RequestedUser.Profile.ID + "&show=outside", NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Key), "Riepilogo Permessi Generici Applicazioni"));

            if (GrantsManager[UsersApplication.ApplicationSystemName + "_edit"].Value == GrantsValues.Allow)
                this.Toolbar.Buttons.Add(new ToolbarButton("user_mod.aspx?id=" + RequestedUser.ID, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Pencil), "Modifica Dati Utente"));

            if (GrantsManager[UsersApplication.ApplicationSystemName + "_groups"].Value == GrantsValues.Allow)
                this.Toolbar.Buttons.Add(new ToolbarButton("user_groups.aspx?id=" + RequestedUser.ID, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Chart_Organisation), "Modifica Gruppi Utente"));

            if (GrantsManager[UsersApplication.ApplicationSystemName + "_networks"].Value == GrantsValues.Allow)
                this.Toolbar.Buttons.Add(new ToolbarButton("networks.aspx?id=" + RequestedUser.ID, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.World_Edit), "Portali Abilitati"));
            
            if (GrantsManager[UsersApplication.ApplicationSystemName + "_services"].Value == GrantsValues.Allow)
                this.Toolbar.Buttons.Add(new ToolbarButton("services.aspx?id=" + RequestedUser.ID, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Package), "Gestione Servizi"));

            //RequestedUser.LastAccess == DateTime.MinValue && 
            if (GrantsManager[UsersApplication.ApplicationSystemName + "_new_password"].Value == GrantsValues.Allow)
                this.Toolbar.Buttons.Add(new ToolbarButton("new_password.aspx?id=" + RequestedUser.ID, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Key_Add), "Invia nuova password"));

            if (GrantsManager[UsersApplication.ApplicationSystemName + "_edit"].Value == GrantsValues.Allow && RequestedUser.ID != Administrator.ID)
            {
                bool canIDeleteUser = RequestedUser.Groups.Count > 0;
                foreach (Group gruppo in RequestedUser.Groups)
                    canIDeleteUser = canIDeleteUser && Administrator.AssociatedGroupsOrdered.Cast<GroupAssociation>().Count(x => x.CanIHandleGroup(gruppo)) > 0;

                if (canIDeleteUser)
                    this.Toolbar.Buttons.Add(new ToolbarButton("javascript: setFormValueConfirm('" + RequestedUser.ID + "','DeleteUserField',1,'Sei sicuro di voler eliminare questo utente?')", NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Delete), "Elimina Utente"));
            }
        }

        public UsersDetailsPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            SetToolbars();

            CheckGrantsOnThisUser(RequestedUser);
        }

        private Button _ProfileEnable;
        private Button ProfileEnableBtn 
        {
            get
            {
                if (_ProfileEnable == null)
                {
                    _ProfileEnable = new Button();
                    _ProfileEnable.ID = "userEnable";
                    _ProfileEnable.Text = "Abilita profilo";
                    _ProfileEnable.CssClass = "btn btn-success";
                    _ProfileEnable.Click += ProfileEnable_Click;
                }
                return _ProfileEnable;
            }
        }

        private Button _ProfileDisable;
        private Button ProfileDisableBtn
        {
            get
            {
                if (_ProfileDisable == null)
                {
                    _ProfileDisable = new Button();
                    _ProfileDisable.ID = "userEnable";
                    _ProfileDisable.Text = "Disabilita profilo";
                    _ProfileDisable.CssClass = "btn btn-danger";
                    _ProfileDisable.Click += ProfileDisable_Click;
                }
                return _ProfileDisable;
            }
        }


        private Button _SendActivationEmail;
        private Button SendActivationEmailBtn
        {
            get
            {
                if (_SendActivationEmail == null)
                {
                    _SendActivationEmail = new Button();
                    _SendActivationEmail.ID = "sendActivationEmail";
                    _SendActivationEmail.Text = "Invia email di attivazione";
                    _SendActivationEmail.CssClass = "btn btn-info";
                    _SendActivationEmail.Click += SendActivationEmail_Click; 
                }
                return _SendActivationEmail;
            }
        }

        private void SendActivationEmail_Click(object sender, EventArgs e)
        {
            try
            {
                
                NetCms.Users.User user = UsersBusinessLogic.GetById(RequestedUser.ID);
                if (user.SendActivationMail())
                    NetCms.GUI.PopupBox.AddSessionMessage("Le nuove credenziali sono state inoltate all'utente.", NetCms.GUI.PostBackMessagesType.Success, "/cms/applications/users/user_details.aspx?id=" + RequestedUser.ID);
                else
                    NetCms.GUI.PopupBox.AddSessionMessage("Errore: errore interno durante l'invio delle nuove credenziali.", NetCms.GUI.PostBackMessagesType.Error, false);
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceException(ex, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName);
            }
        }

        private void ProfileEnable_Click(object sender, EventArgs e)
        {
            try
            {
                NetCms.Users.User user = UsersBusinessLogic.GetById(RequestedUser.ID);
                user.EnableSimple();
                NetCms.GUI.PopupBox.AddSessionMessage("Profilo utente attivato. Una email di notifica � stata spedita all'indirizzo " + RequestedUser.Anagrafica.Email, NetCms.GUI.PostBackMessagesType.Success, "/cms/applications/users/user_details.aspx?id=" + RequestedUser.ID);
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + NetCms.Users.AccountManager.CurrentAccount.UserName + " ha abilitato l'utente " + user.UserName + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName);
            }
            catch(Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceException(ex, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName);
            }
        }

        private void ProfileDisable_Click(object sender, EventArgs e)
        {
            try
            {
                NetCms.Users.User user = UsersBusinessLogic.GetById(RequestedUser.ID);
                user.Disable();
                NetCms.GUI.PopupBox.AddSessionMessage("Profilo utente disattivato. Una email di notifica � stata spedita all'indirizzo " + RequestedUser.Anagrafica.Email, NetCms.GUI.PostBackMessagesType.Success, "/cms/applications/users/user_details.aspx?id=" + RequestedUser.ID);
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + NetCms.Users.AccountManager.CurrentAccount.UserName + " ha disabilitato l'utente " + user.UserName + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName);
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceException(ex, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName);
            }
        }

        public override WebControl Control
        {
            get 
            {
                WebControl output = new WebControl(HtmlTextWriterTag.Div);
                output.CssClass = "userDetail";
                if (!this.IsPostBack)
                {
                    string userName = Administrator.UserName;
                    string utente = Administrator.NomeCompleto;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta visualizzando la scheda dell'utente " + utente + ".");
                }

                #region Delete User Postback
                if (IsPostBack && RequestedUser.ID != Administrator.ID)
                {
                    if (GrantsManager[UsersApplication.ApplicationSystemName + "_edit"].Value == GrantsValues.Allow)
                    {
                        NetUtility.RequestVariable DeleteFieldRequest = new NetUtility.RequestVariable("DeleteUserField");
                        if (DeleteFieldRequest.IsValidInteger)
                        {
                            RequestedUser.Delete();
                            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                            string utente = RequestedUser.NomeCompleto;
                            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha eliminato l'utente " + utente + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                            NetCms.GUI.PopupBox.AddSessionMessage("L'utente � stato eliminato con successo.", NetCms.GUI.PostBackMessagesType.Notify, Configurations.Generics.DefaultPageName);
                        }
                    }
                    else
                    {
                        NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();
                    }
                }

                #endregion

                #region Tabella Dati Account

                WebControl setshield = new WebControl(HtmlTextWriterTag.Div);
                setshield.CssClass = "Details";
                output.Controls.Add(setshield);

                HtmlGenericControls.Fieldset fieldset = new HtmlGenericControls.Fieldset("Dati Account");
               // fieldset.Class = "Details";
                setshield.Controls.Add(fieldset);

                HtmlGenericControl text = new HtmlGenericControl("div");
                text.Attributes["class"] = "DetailsText";
                fieldset.Controls.Add(text);
                text.InnerHtml = "Informazioni sull'account dell'utente.";

                //Hidden Field per eliminazione utente
                text.InnerHtml += "<input type=\"hidden\" name=\"DeleteUserField\" id=\"DeleteUserField\" />";

                /* Informazioni account */
                HtmlGenericControl content = new HtmlGenericControl("table");
                content.Attributes["class"] = "DetailsContainer";
                content.Attributes["cellpadding"] = "0";
                content.Attributes["cellspacing"] = "0";
                fieldset.Controls.Add(content);

                string format = "<tr class=\"DetailsRow DetailsRow{0}\"><td class=\"DetailsLabel\"><strong>{1}: </strong></td><td>{2}</td></tr>";

                string statoUtente = "";

                switch (RequestedUser.Stato)
                {
                    case User.Stati.Disabilitato:
                        statoUtente = "Non Abilitato";
                        break;
                    case User.Stati.Abilitato:
                        statoUtente = "Abilitato";
                        break;
                    case User.Stati.MaiLoggato:
                        statoUtente = "Mai Abilitato"; 
                        break;
                                      
                }

                content.InnerHtml += string.Format(format, "Normal", "Username", RequestedUser.UserName);
                content.InnerHtml += string.Format(format, "Alternate", "eMail", Anagrafica.Email);
                content.InnerHtml += string.Format(format, "Normal", "Stato", statoUtente);
                content.InnerHtml += string.Format(format, "Alternate", "Data attivazione account", RequestedUser.CreationTime);
                content.InnerHtml += string.Format(format, "Normal", "Data ultimo accesso", RequestedUser.LastAccess);
                content.InnerHtml += string.Format(format, "Alternate", "Data ultimo aggiornamento", RequestedUser.LastUpdate);
                content.InnerHtml += string.Format(format, "Normal", "ID Profilo", RequestedUser.Profile.ID);
                content.InnerHtml += string.Format(format, "Alternate", "ID Anagrafe", RequestedUser.Anagrafica.ID);
                content.InnerHtml += string.Format(format, "Normal", "Anagrafe", RequestedUser.CustomAnagrafe.Nome);

                WebControl buttons = new WebControl(HtmlTextWriterTag.Div);
                buttons.CssClass = "m-3";

                switch (RequestedUser.Stato)
                {
                    case User.Stati.Disabilitato:
                        buttons.Controls.Add(ProfileEnableBtn);
                        break;
                    case User.Stati.Abilitato:
                        buttons.Controls.Add(ProfileDisableBtn);
                        buttons.Controls.Add(SendActivationEmailBtn);
                        break;
                    case User.Stati.MaiLoggato:
                        buttons.Controls.Add(SendActivationEmailBtn);
                        buttons.Controls.Add(ProfileEnableBtn);
                        break;
                    default:
                        break;
                }

                fieldset.Controls.Add(buttons);

                #endregion

                #region Accettazione Registrazione

                if (!RequestedUser.Registrato)
                {
                    setshield = new WebControl(HtmlTextWriterTag.Div);
                    setshield.CssClass = "Details";
                    output.Controls.Add(setshield);

                    fieldset = new HtmlGenericControls.Fieldset("Accettazione Registrazione");
                    //fieldset.Class = "Details";
                    setshield.Controls.Add(fieldset);

                    text = new HtmlGenericControl("div");
                    text.Attributes["class"] = "DetailsText";
                    fieldset.Controls.Add(text);
                    text.InnerHtml = "Gestione della richiesta di registrazione.";

                    Button accetta = new Button();
                    accetta.ID = "accetta_registrazione";
                    accetta.Text = "Accetta Registrazione";
                    accetta.Click += new EventHandler(accetta_Click);

                    fieldset.Controls.Add(accetta);

                    Button rifiuta = new Button();
                    rifiuta.ID = "rifiuta_registrazione";
                    rifiuta.Text = "Rifiuta Registrazione";
                    rifiuta.Click += new EventHandler(rifiuta_Click);

                    fieldset.Controls.Add(rifiuta);

                    TextBox motivazione = new TextBox();
                    motivazione.ID = "motivazione_rifiuta_registrazione";
                    motivazione.Rows = 5;
                    motivazione.Columns = 30;
                    motivazione.TextMode = TextBoxMode.MultiLine;

                    Label label = new Label { AssociatedControlID = motivazione.ID, Text = "Inserisci la motivazione" };
                    
                    fieldset.Controls.Add(label);
                    fieldset.Controls.Add(motivazione);

                }
                
                #endregion

                #region Tabella Gruppi

                setshield = new WebControl(HtmlTextWriterTag.Div);
                setshield.CssClass = "Details";
                output.Controls.Add(setshield);

                fieldset = new HtmlGenericControls.Fieldset("Gruppi dell'Utente");
                //fieldset.Class = "Details";
                setshield.Controls.Add(fieldset);

                text = new HtmlGenericControl("div");
                text.Attributes["class"] = "DetailsText";
                fieldset.Controls.Add(text);
                text.InnerHtml = "Elenco gruppi di cui l'utente fa parte";

                content = new HtmlGenericControl("table");
                content.Attributes["class"] = "DetailsContainer";
                content.Attributes["cellpadding"] = "0";
                content.Attributes["cellspacing"] = "0";
                fieldset.Controls.Add(content);

                string format2 = "<tr class=\"DetailsRow DetailsRow{0}\"><td class=\"DetailsLabel\">{1}</td><td>{2}</td></tr>";

                content.InnerHtml += string.Format(format2, "Alternate", "<strong>Gruppo</strong>", "<strong>Ruolo</strong>");

                format2 = "<tr class=\"DetailsRow DetailsRow{0}\"><td class=\"DetailsLabel\"><a href=\"" + Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/groups/group_details.aspx?id={3}\">{1}</a></td><td>{2}</td></tr>";

                bool alternate = true;
                foreach (GroupAssociation association in RequestedUser.AssociatedGroupsOrdered)
                {
                    content.InnerHtml += string.Format(format2, alternate ? "Normal" : "Alternate", association.Group.FullPathName, association.GrantsRoleLabel + " - " + association.ContentsRoleLabel, association.Group.ID);
                    alternate = !alternate;
                }

                #endregion
                
                #region Tabella Reti

                setshield = new WebControl(HtmlTextWriterTag.Div);
                setshield.CssClass = "Details";
                output.Controls.Add(setshield);
                //NetCms.Users.GrantElement grants = new NetCms.Users.GrantElement("users", Account.Profile);
                G2Core.UI.DetailsSheet nets = new G2Core.UI.DetailsSheet("Elenco dei portali abilitati", "Riepilogo dei portali al quale l'utente � autorizzato ad accedere con il proprio account. (E' possibile gestire i permessi Cms dei portali ABILITATI con i servizi del relativo portale ABILITATI)");                

                if (RequestedUser.HasNeworksEnabled)
                {
                    nets.AddColumn("Portale", G2Core.UI.DetailsSheet.Colors.Default);
                    nets.AddColumn("Riepilogo dei Permessi CMS", G2Core.UI.DetailsSheet.Colors.Default);
                    nets.AddColumn("Riepilogo dei Permessi Applicazioni", G2Core.UI.DetailsSheet.Colors.Default);

                    bool showGrantsButtons = GrantsManager[UsersApplication.ApplicationSystemName + "_grants"].Value == GrantsValues.Allow;

                    foreach (var network in NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks)
                    {
                        if (RequestedUser.NetworkEnabledForUser(network.Value.ID))
                        {
                            string button = "&nbsp;";
                            string button1 = "&nbsp;";
                            if (network.Value.Enabled)
                            {
                                if (network.Value.CmsStatus == NetCms.Networks.NetworkCmsStates.Enabled)
                                    if (showGrantsButtons)
                                    {
                                       // button = "<a href=\"" + NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/" + network.Value.SystemName + "/cms/grants/grants_view.aspx?pid=" + RequestedUser.Profile.ID + "\" class=\"action key\"><span>Riepiloga Permessi</span><a>";
                                    }
                                    else
                                        button = "<a class=\"action stop\"><span>Cms Disabilitato</span><a>";
                                button1 = "<a href=\"" + NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/grants/review.aspx?pid=" + RequestedUser.Profile.ID + "&nid=" + network.Value.ID + "\" class=\"action key\"><span>Riepiloga Permessi</span><a>";

                            }
                            else
                            {
                                button = "<a class=\"action stop\"><span>Servizi Disabilitati</span><a>";
                                button1 = "<a class=\"action stop\"><span>Servizi Disabilitati</span><a>";
                            }
                            nets.AddRow(network.Value.Label, button, button1);
                        }
                    }
                }
                else
                    nets.AddColumn("Non ci sono Portali associati a questo utente.", G2Core.UI.DetailsSheet.Colors.Default);
                setshield.Controls.Add(nets);
                #endregion

                #region Tabella Dati Anagrafici

                if (GrantsManager[UsersApplication.ApplicationSystemName + "_fulldetails"].Value == GrantsValues.Allow)
                {
                    NetCms.GUI.DetailsSheet anagrafe = new DetailsSheet("Dati Anagrafici", "Informazioni anagrafiche dell'utente.");

                    foreach (AnagraficaDetail detail in Anagrafica.Details)
                        anagrafe.AddRow(new DetailsSheetRow(detail.Title, detail.Value));
                    output.Controls.Add(anagrafe);
                }

                #endregion

                return output;
            }
        }

        void rifiuta_Click(object sender, EventArgs e)
        {
            G2Core.Common.RequestVariable motivazione = new G2Core.Common.RequestVariable("motivazione_rifiuta_registrazione", G2Core.Common.RequestVariable.RequestType.Form);

            if (motivazione.IsValidString && !string.IsNullOrEmpty(motivazione.StringValue))
            {
                UsersBusinessLogic.SendRejectMail(RequestedUser, motivazione.StringValue);
                UsersBusinessLogic.DeleteUser(RequestedUser);
                NetCms.GUI.PopupBox.AddSessionMessage("Registrazione negata con successo", PostBackMessagesType.Notify,true);
            }
            else
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Inserire la motivazione", PostBackMessagesType.Error);
            }
        }

        void accetta_Click(object sender, EventArgs e)
        {
            // devo generare una nuova password da inviare all'utente in chiaro perch� non � possibile decriptare quella nel db
            G2Core.Common.PasswordGenerator gen = new G2Core.Common.PasswordGenerator(8);

            string password = gen.Generate();

            UsersBusinessLogic.SendConfirmMail(RequestedUser, password, RequestedUser.CurrentNetworkPath);

            // e poi la salvo criptata nel db
            RequestedUser.Password = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();
            RequestedUser.Registrato = true;

            UsersBusinessLogic.SaveOrUpdateUser(RequestedUser);
   
            NetCms.GUI.PopupBox.AddMessageAndReload("Registrazione accettata con successo", PostBackMessagesType.Success);
        }
    }
}