using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetTable;
using NetCms.GUI;
using NetCms.Vertical.Grants;
using NetCms.Users;
using System.Collections.Generic;
using NetCms.Grants;

namespace NetCms.Users.UsersManager
{
    [DynamicUrl.PageControl("new_password.aspx", ApplicationID = UsersApplication.ApplicationID)]
    public class UsersNewPassword : AbstractPage
    {
        public override string PageTitle
        {
            get { return base.PageTitle + " Inoltro di nuove credenziali temporanee all'utente '" + this.RequestedUser.NomeCompleto + "'"; }
        }

        private AnagraficaBase Anagrafica
        {
            get
            {
                return RequestedUser.Anagrafica;
            }
        }

        private User _RequestedUser;
        private User RequestedUser
        {
            get
            {
                if (_RequestedUser == null)
                {
                    NetUtility.RequestVariable req = new NetUtility.RequestVariable("id", NetUtility.RequestVariable.RequestType.QueryString);
                    if (req.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                        //_RequestedUser = new User(req.IntValue.ToString());
                        _RequestedUser = UsersBusinessLogic.GetById(req.IntValue);
                }
                if (_RequestedUser == null || _RequestedUser.Anagrafica == null)
                    NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

                return _RequestedUser;
            }
        }

        //public DataTable UsersTable
        //{
        //    get
        //    {
        //        if (_UsersTable == null)
        //        {
        //            NetCms.Users.Search.UsersDataManager UsersDataManager = new NetCms.Users.Search.UsersDataManager(Administrator, Connection);
        //            _UsersTable = UsersDataManager.MyUsersTable;
        //        }
        //        return _UsersTable;
        //    }
        //}
        //private DataTable _UsersTable;

        //public ICollection<User> UsersTable
        //{
        //    get
        //    {
        //        if (_UsersTable == null)
        //        {
        //            NetCms.Users.Search.UsersDataManager UsersDataManager = new NetCms.Users.Search.UsersDataManager(Administrator);
        //            _UsersTable = UsersDataManager.MyUsersTable;
        //        }
        //        return _UsersTable;
        //    }
        //}
        //private ICollection<User> _UsersTable;

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Table, "Elenco Utenti"));
            this.Toolbar.Buttons.Add(new ToolbarButton("user_details.aspx?id=" + this.RequestedUser.ID, NetCms.GUI.Icons.Icons.Page_White_Magnify, "Scheda Utente"));

            if (GrantsManager[UsersApplication.ApplicationSystemName + "_grants"].Value == GrantsValues.Allow)
                this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/grants/review.aspx?pid=" + RequestedUser.Profile.ID, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Key), "Riepilogo Permessi Generici Applicazioni"));

            if (GrantsManager[UsersApplication.ApplicationSystemName + "_edit"].Value == GrantsValues.Allow)
                this.Toolbar.Buttons.Add(new ToolbarButton("user_mod.aspx?id=" + RequestedUser.ID, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Pencil), "Modifica Dati Utente"));

            if (GrantsManager[UsersApplication.ApplicationSystemName + "_groups"].Value == GrantsValues.Allow)
                this.Toolbar.Buttons.Add(new ToolbarButton("user_groups.aspx?id=" + RequestedUser.ID, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Chart_Organisation), "Modifica Gruppi Utente"));

            if (GrantsManager[UsersApplication.ApplicationSystemName + "_networks"].Value == GrantsValues.Allow)
                this.Toolbar.Buttons.Add(new ToolbarButton("networks.aspx?id=" + RequestedUser.ID, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.World_Edit), "Portali Abilitati"));

            //if (GrantsManager[UsersApplication.ApplicationSystemName + "_new_password"].Value == GrantsValues.Allow)
            //    this.Toolbar.Buttons.Add(new ToolbarButton("new_password.aspx?id=" + RequestedUser.ID, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Key_Add), "Inoltra Nuove Credenziali"));

            if (GrantsManager[UsersApplication.ApplicationSystemName + "_edit"].Value == GrantsValues.Allow && RequestedUser.ID != Administrator.ID)
            {
                bool canIDeleteUser = RequestedUser.Groups.Count > 0;
                foreach (Group gruppo in RequestedUser.Groups)
                    canIDeleteUser = canIDeleteUser && Administrator.AssociatedGroupsOrdered.Cast<GroupAssociation>().Count(x => x.CanIHandleGroup(gruppo)) > 0;

                if (canIDeleteUser)
                    this.Toolbar.Buttons.Add(new ToolbarButton("javascript: setFormValueConfirm('" + RequestedUser.ID + "','DeleteUserField',1,'Sei sicuro di voler eliminare questo utente?')", NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Delete), "Elimina Utente"));
            }
        }

        public UsersNewPassword(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            SetToolbars();

            if (!GrantsManager[UsersApplication.ApplicationSystemName + "_new_password"].Allowed)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();
            CheckGrantsOnThisUser(RequestedUser);
        }

        private Button _SendPasswordBtn;
        private Button SendPasswordBtn
        {
            get
            {
                if (_SendPasswordBtn == null)
                {
                    _SendPasswordBtn = new Button();
                    _SendPasswordBtn.ID = "userEnable";
                    _SendPasswordBtn.Text = "Invia password";
                    _SendPasswordBtn.CssClass = "btn btn-success";
                    _SendPasswordBtn.Click += _SendPasswordBtn_Click;
                }
                return _SendPasswordBtn;
            }
        }

        private void _SendPasswordBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (RequestedUser.SendNewPasswordFromBackoffice())
                    NetCms.GUI.PopupBox.AddSessionMessage("La nuova password � stata inviata all'utente all'indirizzo di posta " + RequestedUser.Anagrafica.Email + ".", NetCms.GUI.PostBackMessagesType.Success, false);
                else
                    NetCms.GUI.PopupBox.AddSessionMessage("Errore: errore interno durante l'invio delle nuove credenziali.", NetCms.GUI.PostBackMessagesType.Error, false);
            }
            catch(Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceException(ex, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName);
            }
        }

    
        public override WebControl Control
        {
            get 
            {
                WebControl output = new WebControl(HtmlTextWriterTag.Div);

                G2Core.XhtmlControls.InputField field = new G2Core.XhtmlControls.InputField("newPwdConfirm");
                output.Controls.Add(field.Control);
              
                //if (field.RequestVariable.IsPostBack && field.RequestVariable.IsValidInteger)
                //{
                //    //do send new password
                //    //if (RequestedUser.SendActivationMail())
                  
                //}

                string title = "Invio nuova password";
                string description = "Da questa pagina � possibile inoltrare all'utente '"+RequestedUser.NomeCompleto+"' una nuova password di accesso generata automaticamente. <br /> L'utente sar� costretto ad aggiornarle al primo accesso.";
                
                //G2Core.UI.DetailsSheet sheet = new  G2Core.UI.DetailsSheet(title, description);
                //sheet.AddRow("<a class=\"action ok\" href=\"{0}\"><span>Invia nuova password all'email \""+RequestedUser.Anagrafica.Email+"\"</span><a>".FormatByParameters(field.getSetFormValueJScript("1")));
                //sheet.AddRow("<a class=\"action stop\" href=\"user_details.aspx?id={0}\"><span>Torna Indietro</span><a>".FormatByParameters(RequestedUser.ID));

                output.Controls.Add(new LiteralControl("<div class=\"DetailsText\">" + description + "</div>"));

                string cardContent = @"<h5 class=""card-title"">"+title+ @"</h5>
                                       <p class=""card-text"">Invia nuova password all'email <strong>" + RequestedUser.Anagrafica.Email + @"</strong></p>";


                WebControl card = new WebControl(HtmlTextWriterTag.Div);
                card.CssClass = "card m-2";
                
                WebControl cardbody = new WebControl(HtmlTextWriterTag.Div);
                cardbody.CssClass = "card-body";
                cardbody.Controls.Add(new LiteralControl(cardContent));
                cardbody.Controls.Add(SendPasswordBtn);
                card.Controls.Add(cardbody);

                output.Controls.Add(card);
                
                return output;
            }
        }
    }
}