﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetService.Utility.ValidatedFields;
using System.Web.UI.HtmlControls;
using System.Linq;
using NetCms.Users.CustomAnagrafe;
using NetCms.GUI;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Users.UsersManager
{
    [DynamicUrl.PageControl("account_edit.aspx", ApplicationID = UsersApplication.ApplicationID)]
    public class UserProfileEditPage : AbstractPage
    {
        private User _Account;
        public User Account
        {
            get
            {
                if (_Account == null && AccountManager.Logged)
                    _Account = AccountManager.CurrentAccount;
                return _Account;
            }
        }

        public UserProfileEditPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot, NetCms.GUI.Icons.Icons.Arrow_Left, "Indietro"));

            ModProfileForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (ModProfileForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                bool sendupdateemailconfirmation = false;
                string oldemail = Account.Anagrafica.Email;
                string newemail = "";
              
                newemail = ModProfileForm.Fields.Where(x => x.Key == "Email").First().PostBackValue;

                string oldIsUsername = "";
                string newIsUsername = "";
                string ifIsUsernameChanged = "";         

                if (string.Compare(oldemail, newemail, true) != 0)
                {
                    sendupdateemailconfirmation = true;
                }

                string userNameUtenteInserito = "";
                if (Account.CustomAnagrafe.EmailAsUserName)
                    userNameUtenteInserito = newemail;
                else 
                {
                    CustomField customFieldIsUsername = Account.CustomAnagrafe.CustomFields.Where(x => x.IsUsername).FirstOrDefault();
                    userNameUtenteInserito = ModProfileForm.Fields.Where(x => x.Key == customFieldIsUsername.CampoInAnagrafe.Key).First().PostBackValue;
                }

                User utente = UsersBusinessLogic.GetUserByUserName(userNameUtenteInserito);
                if (utente == null || (utente != null && utente.ID == Account.ID))
                {

                    if (UsersBusinessLogic.UpdateUserAnagrafica(ModProfileForm, Account.Anagrafica.ID, Account.AnagraficaType))
                    {
                        CustomField customFieldIsUsername = Account.CustomAnagrafe.CustomFields.Where(x => x.IsUsername).FirstOrDefault();
                        if (customFieldIsUsername != null)
                        {
                            oldIsUsername = Account.AnagraficaMapped[customFieldIsUsername.Name].ToString();
                            newIsUsername = ModProfileForm.Fields.Where(x => x.Key == customFieldIsUsername.CampoInAnagrafe.Key).First().PostBackValue;
                            if (string.Compare(oldIsUsername, newIsUsername, true) != 0)
                            {
                                ifIsUsernameChanged = "Il campo '" + customFieldIsUsername.Name + "' usato come User Name è stato modificato. A seguito di questa operazione la nuova UserName è stata cambiata da '" + oldIsUsername + "' a '" + newIsUsername + "'";
                            }
                        }
                        AccountManager.UpdateCurrentUser();

                        string message = "Profilo aggiornato con successo." + ifIsUsernameChanged;
                        if (sendupdateemailconfirmation)
                        {
                            NetCms.Users.AccountManager.SendConfirmationEmailUpdate(Account.NomeCompleto, oldemail, newemail, Account.ID.ToString(), null);
                            if (newemail != null && newemail.Length > 0)
                                message += " Per attuare la modifica dell'email a \"" + newemail + "\", seguite le indicazioni inviateVi per posta elettronica all'indirizzo fornito.";
                        }
                        NetCms.GUI.PopupBox.AddSessionMessage(message, PostBackMessagesType.Success, true);

                    }
                    else
                    {
                        NetCms.GUI.PopupBox.AddMessage("Errore nell'aggiornamento del profilo.", PostBackMessagesType.Error);
                    }
                }
                else
                {
                    NetCms.GUI.PopupBox.AddMessage("Lo Username '" + userNameUtenteInserito + "' è già utilizzato, sceglierne uno diverso.", PostBackMessagesType.Error);
                }
            }
        }

        public AnagraficaVfManager ModProfileForm
        {
            get
            {
                if (_ModProfileForm == null)
                {
                    NetCms.Users.CustomAnagrafe.CustomAnagrafe strutturaAnagrafe = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(Account.AnagraficaName);
                    _ModProfileForm = new AnagraficaVfManager(this.Account.ID + "_anagrafica", IsPostBack, strutturaAnagrafe, true, Account);
                    _ModProfileForm.SubmitButton.Text = "Modifica";
                }
                return _ModProfileForm;
            }
        }
        private AnagraficaVfManager _ModProfileForm;

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User_Edit; }
        }

        public override string PageTitle
        {
            get { return base.PageTitle + " Modifica profilo utente '" + this.Account.NomeCompleto + "'"; }
        }

        public override WebControl Control
        {
            get
            {
                if (NetCms.Users.AccountManager.Logged)
                {
                    WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
                    WebControl legend = new WebControl( HtmlTextWriterTag.Legend);
                    legend.Controls.Add(new LiteralControl("Modifica profilo"));
                    fieldset.Controls.Add(legend);
                    fieldset.CssClass = "NetCms_profileForm";

                    fieldset.Controls.Add(new System.Web.UI.LiteralControl("<span class=\"smallDescription\">La modifica dell'email richiederà una conferma della nuova email prima di essere attuata.</span>"));
                    fieldset.Controls.Add(ModProfileForm);
                    return fieldset;
                }
                else
                {
                    return new NetCms.Users.LoginControl();
                }
            }
        }
    }
}