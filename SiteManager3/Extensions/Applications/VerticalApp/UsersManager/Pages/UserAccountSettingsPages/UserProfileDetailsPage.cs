﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Users.UsersManager
{
    [DynamicUrl.PageControl("account_details.aspx", ApplicationID = UsersApplication.ApplicationID)]
    public class UserProfileDetailsPage : AbstractPage
    {
        private User _Account;
        public User Account
        {
            get
            {
                if (_Account == null && NetCms.Users.AccountManager.Logged)
                    _Account = NetCms.Users.AccountManager.CurrentAccount;
                return _Account;
            }
        }

        public UserProfileDetailsPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot, NetCms.GUI.Icons.Icons.Arrow_Left, "Indietro"));
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User_Suit; }
        }

        public override string PageTitle
        {
            get { return base.PageTitle + " Dettagli profilo utente '" + this.Account.NomeCompleto + "'"; }
        }

        public override WebControl Control
        {
            get
            {
                if (NetCms.Users.AccountManager.Logged)
                {
                    return NetCms.Users.AccountManager.CurrentAccount.Anagrafica.GetProfileDetails("account_details.aspx");
                }
                else
                {
                    return new NetCms.Users.LoginControl();
                }
            }
        }

    }
}