﻿using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetForms;
using NetService.Utility.ValidatedFields;
using NetCms.GUI;

namespace NetCms.Users.UsersManager
{
    [DynamicUrl.PageControl("account_pwd_change.aspx", ApplicationID = UsersApplication.ApplicationID)]
    public class UserChangePasswordPage : AbstractPage
    {
        private User _Account;
        public User Account
        {
            get
            {
                if (_Account == null && NetCms.Users.AccountManager.Logged)
                    _Account = NetCms.Users.AccountManager.CurrentAccount;
                return _Account;
            }
        }

        public UserChangePasswordPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot, NetCms.GUI.Icons.Icons.Arrow_Left, "Indietro"));
        }

        public VfManager ChangePasswordForm
        {
            get
            {
                if (_ChangePasswordForm == null)
                {
                    _ChangePasswordForm = new VfManager("changePassword");
                    _ChangePasswordForm.SubmitButton.Text = "Aggiorna";

                    VfPassword password = new VfPassword("password");
                    password.Required = true;                    
                    _ChangePasswordForm.Fields.Add(password);
                }
                return _ChangePasswordForm;
            }
        }
        private VfManager _ChangePasswordForm;

        public override WebControl Control
        {
            get
            {
                if (NetCms.Users.AccountManager.Logged)
                {
                    ChangePasswordForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
                    return getView();
                }
                else
                {
                    return new NetCms.Users.LoginControl();
                }
            }
        }


        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (ChangePasswordForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                string password = ChangePasswordForm.Fields.Find(x=>x.Key == "password").PostBackValue;
                string reqPwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();

                User current = UsersBusinessLogic.GetById(NetCms.Users.AccountManager.CurrentAccount.ID);

                if (current.Password == reqPwd || current.NuovaPassword == reqPwd)
                {
                    NetCms.GUI.PopupBox.AddMessage("E' necessario inserire una password diversa da quella correntemente impostata", PostBackMessagesType.Error);
                }
                else
                {
                    current.Password = reqPwd;
                    current.LastUpdate = DateTime.Now;

                    UsersBusinessLogic.MergeUser(current);
                    NetCms.Users.AccountManager.LoginUser(current.ID.ToString());

                    NetCms.GUI.PopupBox.AddSessionMessage("La password è stata aggiornata con successo. Sarà necessario reimpostarla tra 90 giorni.", PostBackMessagesType.Success,true);
                }
            }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Server_Key; }
        }

        public override string PageTitle
        {
            get { return base.PageTitle + " Modifica password utente '" + this.Account.NomeCompleto + "'"; }
        }

        private WebControl getView()
        {

            WebControl output = new WebControl(HtmlTextWriterTag.Div);
            output.CssClass = "form-user form-changepw";

            G2Core.XhtmlControls.Fieldset fieldset = new G2Core.XhtmlControls.Fieldset("Modifica Password");
            output.Controls.Add(fieldset);

            fieldset.Controls.Add(ChangePasswordForm);

            return output;
        }
    }
}
