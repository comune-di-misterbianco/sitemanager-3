using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetTable;
using NetCms.GUI;
using NetCms.Vertical.Grants;
using NetCms.Users;

namespace NetCms.Users.UsersManager
{
    [DynamicUrl.PageControl("nogroups.aspx", ApplicationID = UsersApplication.ApplicationID)]
    public class UsersListNoGroupsPage : AbstractPage
    {
        public override string PageTitle
        {
            get { return base.PageTitle + "Utenti non associati ad alcun gruppo."; }
        }
                
        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User; }
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Table, "Elenco Utenti"));
        }

        public UsersListNoGroupsPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            SetToolbars();

            if (!GrantsManager[UsersApplication.ApplicationSystemName+"_ungroupped"].Allowed)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();
        }

        public override WebControl Control
        {
            get
            {
                return new UsersListControl(/*this.Connection,*/ this.Administrator, true);
            }
        }
    }
}