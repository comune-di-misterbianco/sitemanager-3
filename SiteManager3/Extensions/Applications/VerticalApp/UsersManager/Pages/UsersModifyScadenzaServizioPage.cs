﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetCms.Users;
using System.Data;
using NetService.Utility.ArTable;
using System.Linq;
using NetCms.Vertical;
using System.Collections.Generic;
using NetCms.Vertical.BusinessLogic;
using System;
using NHibernate;
using NetCms.DBSessionProvider;
using NetService.Utility.ValidatedFields;

namespace NetCms.Users.UsersManager
{
    [DynamicUrl.PageControl("scadenzamod.aspx", ApplicationID = UsersApplication.ApplicationID)]
    public class UsersModifyScadenzaServizioPage : AbstractPage
    {
        public override string PageTitle
        {
            get { return base.PageTitle + "Gestione Servizi Associati all'utente '" + RequestedUser.NomeCompleto + "'"; }
        }

        private User _RequestedUser;
        private User RequestedUser
        {
            get
            {
                if (_RequestedUser == null)
                {
                    NetUtility.RequestVariable req = new NetUtility.RequestVariable("idUtente", NetUtility.RequestVariable.RequestType.QueryString);
                    if (req.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                        _RequestedUser = UsersBusinessLogic.GetById(req.IntValue);
                }
                if (_RequestedUser == null || _RequestedUser.Anagrafica == null)
                    NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

                return _RequestedUser;
            }
        }

        private VerticalApplicationServiceUser _ServiceUser;
        private VerticalApplicationServiceUser ServiceUser
        {
            get
            {
                if (_ServiceUser == null)
                {
                    NetUtility.RequestVariable req = new NetUtility.RequestVariable("idServizio", NetUtility.RequestVariable.RequestType.QueryString);
                    if (req.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                        _ServiceUser = VerticalAppBusinessLogic.GetVerticalApplicationServiceUserById(req.IntValue);
                }

                return _ServiceUser;
            }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User; }
        }

        public UsersModifyScadenzaServizioPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            if (!GrantsManager[UsersApplication.ApplicationSystemName + "_services"].Allowed)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

            CheckGrantsOnThisUser(RequestedUser);

            ScadenzaForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);

            SetToolbars();
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("user_details.aspx?id=" + this.RequestedUser.ID, NetCms.GUI.Icons.Icons.Page_White_Magnify, "Scheda Utente"));
            this.Toolbar.Buttons.Add(new ToolbarButton("services.aspx?id=" + this.RequestedUser.ID, NetCms.GUI.Icons.Icons.Package, "Servizi Utente"));
        }

        public override WebControl Control
        {
            get
            {
                WebControl control = new WebControl(HtmlTextWriterTag.Div);
                control.Controls.Add(ScadenzaForm);
                return control;
            }
        }

        private VfManager ScadenzaForm
        {
            get
            {
                if (_ScadenzaForm == null)
                {
                    _ScadenzaForm = new VfManager("modScadenzaForm");
                    _ScadenzaForm.SubmitButton.Text = "Modifica";
                    _ScadenzaForm.CssClass = "Axf";
                    VfDateCalendar dateCalendar = new VfDateCalendar("ScadenzaServizio", "Scadenza Servizio", "ScadenzaServizio");
                    dateCalendar.MinDate = DateTime.Now;
                    dateCalendar.MaxDate = DateTime.MaxValue;
                    dateCalendar.Required = true;

                    _ScadenzaForm.Fields.Add(dateCalendar);
                    _ScadenzaForm.DataBindAdvanced(ServiceUser);

                }
                return _ScadenzaForm;
            }
        }
        private VfManager _ScadenzaForm;

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (ScadenzaForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                ScadenzaForm.FillObjectAdvanced(ServiceUser, VerticalAppBusinessLogic.GetCurrentSession());
                VerticalAppBusinessLogic.SaveOrUpdateVerticalApplicationServiceUser(ServiceUser);
                NetCms.GUI.PopupBox.AddSessionMessage("Scadenza modificata con successo.", PostBackMessagesType.Success, true);
            }
        }
    }
}