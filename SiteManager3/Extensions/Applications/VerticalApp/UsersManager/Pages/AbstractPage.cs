﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetCms.Vertical.Grants;
using NetCms.Connections;

namespace NetCms.Users.UsersManager
{
    public abstract class AbstractPage : NetCms.Vertical.ApplicationPageEx
    {
        public GrantElement GrantsManager
        {
            get
            {
                if (_GrantsManager == null)
                    _GrantsManager = new GrantElement(NetCms.Users.UsersManager.UsersApplication.ApplicationID.ToString(), NetCms.Users.Profile.CurrentProfile);
                return _GrantsManager;
            }
        }
        private GrantElement _GrantsManager;

        public NetCms.Users.User Administrator
        {
            get
            {
                return _Administrator ?? (_Administrator = NetCms.Users.AccountManager.CurrentAccount);
            }
        }
        private NetCms.Users.User _Administrator;

        private NetCms.Connections.Connection _Conn;
        public NetCms.Connections.Connection Conn
        {
            get
            {
                //if (_Conn == null)
                //    _Conn = NetCms.Connections.ConnectionsManager.CommonConnection;
                //return _Conn;
                ConnectionsManager ConnManager = new ConnectionsManager();
                return ConnManager.CommonConnection;
            }
        }

        public override string PageTitle
        {
            get { return "Utenti &raquo; "; }
        }

        public AbstractPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            ApplicationContext.SetCurrentComponent(UsersApplication.ApplicationContextBackLabel);
        }

        public void CheckGrantsOnThisUser(NetCms.Users.User handledUser )
        {
            bool iCanAdminThisUserByGroups = Administrator.IsAdministratorForUser(handledUser.ID);
            bool userHasNoGroupsAndICanHandleUngrouppedUsers = GrantsManager[UsersApplication.ApplicationSystemName + "_ungroupped"].Allowed && handledUser.Groups.Count == 0;
            if (!iCanAdminThisUserByGroups && !userHasNoGroupsAndICanHandleUngrouppedUsers)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();
        }
    }
}
