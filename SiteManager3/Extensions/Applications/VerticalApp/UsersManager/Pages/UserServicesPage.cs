﻿using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetCms.Users;
using System.Data;
using NetService.Utility.ArTable;
using System.Linq;
using NetCms.Vertical;
using System.Collections.Generic;
using NetCms.Vertical.BusinessLogic;
using System;
using NHibernate;
using NetCms.DBSessionProvider;

namespace NetCms.Users.UsersManager
{
    [DynamicUrl.PageControl("services.aspx", ApplicationID = UsersApplication.ApplicationID)]
    public class UsersServicesPage : AbstractPage
    {
        public override string PageTitle
        {
            get { return base.PageTitle + "Gestione Servizi Associati all'utente '" + RequestedUser.NomeCompleto + "'"; }
        }

        private User _RequestedUser;
        private User RequestedUser
        {
            get
            {
                if (_RequestedUser == null)
                {
                    NetUtility.RequestVariable req = new NetUtility.RequestVariable("id", NetUtility.RequestVariable.RequestType.QueryString);
                    if (req.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                        _RequestedUser = UsersBusinessLogic.GetById(req.IntValue);
                }
                if (_RequestedUser == null || _RequestedUser.Anagrafica == null)
                    NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

                return _RequestedUser;
            }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User; }
        }

        public UsersServicesPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            if (!GrantsManager[UsersApplication.ApplicationSystemName + "_services"].Allowed)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

            CheckGrantsOnThisUser(RequestedUser);

            SetToolbars();
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Elenco Utenti"));
            this.Toolbar.Buttons.Add(new ToolbarButton("user_details.aspx?id=" + this.RequestedUser.ID, NetCms.GUI.Icons.Icons.Page_White_Magnify, "Scheda Utente"));
        }

        public override WebControl Control
        {
            get
            {
                WebControl output = BuildControls();
                return output;
            }
        }

        private WebControl BuildControls()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);

            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;

            G2Core.XhtmlControls.InputField add = new G2Core.XhtmlControls.InputField("add");
            if (add.RequestVariable.IsPostBack && add.RequestVariable.IsValidInteger)
            {
                using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = session.BeginTransaction())
                {
                    try
                    {
                        VerticalApplication vapp = VerticalAppBusinessLogic.GetVerticalAppById(add.RequestVariable.IntValue, session);
                        User reqUser = UsersBusinessLogic.GetById(RequestedUser.ID, session);
                        if (!VerticalAppBusinessLogic.UserHasService(reqUser, vapp, session))
                        {
                            VerticalApplicationWithService service = VerticalApplicationsPool.GetByID(vapp.ID) as VerticalApplicationWithService;
                            if (!service.Servizio.RichiedeDatiAggiuntivi)
                            {
                                service.Servizio.Esegui(reqUser, true, session);
                                tx.Commit();

                                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha associato con successo il servizio con id=" + add.RequestVariable.StringValue + " all'utente " + this.RequestedUser.NomeCompleto + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                                GUI.PopupBox.AddMessage("Servizio associato con successo", NetCms.GUI.PostBackMessagesType.Success);
                            }
                            else
                            {
                                GUI.PopupBox.AddMessage("Il servizio richiede l'attivazione attraverso l'inserimento di dati aggiuntivi.", NetCms.GUI.PostBackMessagesType.Notify);
                            }
                        }
                        else
                        {
                            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha provato ad associare il servizio con id=" + add.RequestVariable.StringValue + " all'utente " + this.RequestedUser.NomeCompleto + ", che però era già presente tra i servizi associati all'utente.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                            GUI.PopupBox.AddMessage("Il servizio è già associato all'utente.", NetCms.GUI.PostBackMessagesType.Notify);
                        }
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        GUI.PopupBox.AddMessage("Errore nell'associazione del servizio", NetCms.GUI.PostBackMessagesType.Error);
                    }
                }
            }
            
            G2Core.XhtmlControls.InputField delete = new G2Core.XhtmlControls.InputField("delete");
            if (delete.RequestVariable.IsPostBack && delete.RequestVariable.IsValidInteger)
            {
                using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = session.BeginTransaction())
                {
                    try
                    {
                        VerticalApplicationServiceUser association = VerticalAppBusinessLogic.GetVerticalApplicationServiceUserById(delete.RequestVariable.IntValue, session);
                        VerticalApplicationWithService vapp = VerticalApplicationsPool.GetByID(association.VerticalApplication.ID) as VerticalApplicationWithService;
                        User reqUser = UsersBusinessLogic.GetById(RequestedUser.ID, session);

                        vapp.Servizio.Disattiva(reqUser, session);
                        
                        tx.Commit();
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha rimosso il servizio con id=" + delete.RequestVariable.StringValue + " dall'utente " + this.RequestedUser.NomeCompleto + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        GUI.PopupBox.AddMessage("Servizio rimosso con successo", NetCms.GUI.PostBackMessagesType.Success);
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        GUI.PopupBox.AddMessage("Errore nella rimozione del servizio", NetCms.GUI.PostBackMessagesType.Error);
                    }
                }
            }

            if (!IsPostBack)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di gestione dei servizi associati all'utente " + this.RequestedUser.NomeCompleto + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            }

            control.Controls.Add(add.Control);
            control.Controls.Add(delete.Control);

            ICollection<VerticalApplicationWithService> serviziDisponibili = VerticalAppBusinessLogic.FindAllServicesByCustomAnagrafe(RequestedUser.CustomAnagrafe);

            ArTable tableDisponibili = new ArTable(serviziDisponibili);
            tableDisponibili.InnerTableCssClass = "tab";
            tableDisponibili.MainTitle = "Servizi disponibili";

            ArTextColumn nome = new ArTextColumn("Nome Servizio", "Label");
            tableDisponibili.AddColumn(nome);

            ArTextColumn descrizione = new ArTextColumn("Descrizione", "Servizio.Descrizione");
            tableDisponibili.AddColumn(descrizione);

            ArTextColumn tipo = new ArTextColumn("Tipologia Servizio", "Servizio.TipoServizio");
            tableDisponibili.AddColumn(tipo);

            ArActivateServiceColumn aggiungi = new ArActivateServiceColumn("Aggiungi", ArActionColumn.Icons.Disable, "javascript: setFormValue({ID},'" + add.ID + "',1)", "servicesMoreData.aspx?id=" + RequestedUser.ID + "&idServizio={ID}");
            aggiungi.CssClass = "action add";
            tableDisponibili.AddColumn(aggiungi);

            control.Controls.Add(tableDisponibili);

            ICollection<VerticalApplicationServiceUser> serviziAssociati = VerticalAppBusinessLogic.FindServicesByUser(RequestedUser);

            ArTable tableAssociati = new ArTable(serviziAssociati);
            tableAssociati.InnerTableCssClass = "tab";
            tableAssociati.MainTitle = "Servizi associati all'Utente";

            ArTextColumn nomeAssociato = new ArTextColumn("Nome Servizio", "VerticalApplication.Label");
            tableAssociati.AddColumn(nomeAssociato);

            ArTextColumn dataAttivazione = new ArTextColumn("Data Attivazione", "DataAttivazioneServizio");
            dataAttivazione.DataType = ArTextColumn.DataTypes.DateTime;
            tableAssociati.AddColumn(dataAttivazione);

            ArScadenzaColumn dataScadenza = new ArScadenzaColumn("Data Scadenza", "ScadenzaServizio");
            dataScadenza.DataType = ArTextColumn.DataTypes.Date;
            tableAssociati.AddColumn(dataScadenza);

            ArActionColumn modificaScadenza = new ArActionColumn("Modifica Scadenza", ArActionColumn.Icons.Edit, "scadenzamod.aspx?idUtente=" + RequestedUser.ID + "&idServizio={ID}");
            modificaScadenza.CssClass = "action modify";
            tableAssociati.AddColumn(modificaScadenza);

            ArActionColumn rimuoviAssociato = new ArActionColumn("Rimuovi", ArActionColumn.Icons.Disable, "javascript: setFormValue({ID},'" + delete.ID + "',1)");
            rimuoviAssociato.CssClass = "action delete";
            tableAssociati.AddColumn(rimuoviAssociato);

            control.Controls.Add(tableAssociati);

            return control;
        }
    }
}