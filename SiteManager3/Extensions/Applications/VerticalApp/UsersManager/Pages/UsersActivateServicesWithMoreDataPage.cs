﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using NetCms.GUI;
using System.Web.UI.WebControls;
using NetCms.Vertical;
using NetCms.Vertical.BusinessLogic;
using NHibernate;
using NetService.Utility.ValidatedFields;

namespace NetCms.Users.UsersManager
{
    [DynamicUrl.PageControl("servicesMoreData.aspx", ApplicationID = UsersApplication.ApplicationID)]
    public class UsersActivateServicesWithMoreDataPage : AbstractPage
    {
        public override string PageTitle
        {
            get { return base.PageTitle + "Attivazione Servizio " + VappWithService.Servizio.Nome + " per l'utente '" + RequestedUser.NomeCompleto + "'"; }
        }

        private User _RequestedUser;
        private User RequestedUser
        {
            get
            {
                if (_RequestedUser == null)
                {
                    NetUtility.RequestVariable req = new NetUtility.RequestVariable("id", NetUtility.RequestVariable.RequestType.QueryString);
                    if (req.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                        _RequestedUser = UsersBusinessLogic.GetById(req.IntValue);
                }
                if (_RequestedUser == null || _RequestedUser.Anagrafica == null)
                    NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

                return _RequestedUser;
            }
        }

        private VerticalApplicationWithService _VappWithService;
        private VerticalApplicationWithService VappWithService
        {
            get
            {
                if (_VappWithService == null)
                {
                    NetUtility.RequestVariable req = new NetUtility.RequestVariable("idServizio", NetUtility.RequestVariable.RequestType.QueryString);
                    if (req.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                        _VappWithService = VerticalApplicationsPool.GetByID(req.IntValue) as VerticalApplicationWithService;
                }

                return _VappWithService;
            }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User; }
        }

        public UsersActivateServicesWithMoreDataPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            if (!GrantsManager[UsersApplication.ApplicationSystemName + "_services"].Allowed)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

            CheckGrantsOnThisUser(RequestedUser);

            SetToolbars();

            //FormAttivazione.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (FormAttivazione.IsValid == VfGeneric.ValidationStates.Valid)
            {
                if (!VerticalAppBusinessLogic.UserHasService(RequestedUser, VappWithService))
                {
                    bool activationOk = false;
                    using (ISession session = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                    using (ITransaction tx = session.BeginTransaction())
                    {
                        try
                        {

                            User user = UsersBusinessLogic.GetById(RequestedUser.ID, session);
                            VappWithService.Servizio.Esegui(user, true, session, FormAttivazione);
                            tx.Commit();
                            activationOk = true;
                        }
                        catch (Exception ex)
                        {
                            tx.Rollback();
                            NetCms.Diagnostics.Diagnostics.TraceException(ex, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName);
                            activationOk = false;
                        }
                        if (activationOk)
                            PopupBox.AddSessionMessage("Servizio attivato con successo.", PostBackMessagesType.Normal, true);
                        else
                            PopupBox.AddSessionMessage("Errore durante l'attivazione del servizio.", PostBackMessagesType.Error);
                    }
                }
                else
                    PopupBox.AddSessionMessage("Errore durante l'attivazione del servizio. Il servizio è già attivato per l'utente.", PostBackMessagesType.Error, true);

            }
        }

        private VfManager FormAttivazione
        {
            get
            {
                if (_Form == null)
                {
                    _Form = ((VappWithService.Servizio) as ServizioConDatiAggiuntivi).FormAttivazione;
                    _Form.SubmitButton.Click += new EventHandler(SubmitButton_Click);
                }
                return _Form;
            }
        }
        private VfManager _Form;

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Elenco Utenti"));
            this.Toolbar.Buttons.Add(new ToolbarButton("services.aspx?id=" + RequestedUser.ID, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Package), "Gestione Servizi"));
        }

        public override WebControl Control
        {
            get
            {
                WebControl control = new WebControl(HtmlTextWriterTag.Div);
                control.Controls.Add(FormAttivazione);
                return control;
            }
        }
    }
}
