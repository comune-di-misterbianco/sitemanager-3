using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetCms.Users;
using System.Data;
using NetService.Utility.ArTable;
using System.Linq;

namespace NetCms.Users.UsersManager
{
    [DynamicUrl.PageControl("networks.aspx", ApplicationID = UsersApplication.ApplicationID)]
    public class UsersNetworkPage : AbstractPage
    {
        public override string PageTitle
        {
            get { return base.PageTitle + "Elenco Portali Associati all'utente '" + RequestedUser.NomeCompleto + "'"; }
        }
            
        private User _RequestedUser;
        private User RequestedUser
        {
            get
            {
                if (_RequestedUser == null)
                {
                    NetUtility.RequestVariable req = new NetUtility.RequestVariable("id", NetUtility.RequestVariable.RequestType.QueryString);
                    if (req.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                        //_RequestedUser = new User(req.IntValue.ToString());
                        _RequestedUser = UsersBusinessLogic.GetById(req.IntValue);
                }
                if (_RequestedUser == null || _RequestedUser.Anagrafica == null)
                    NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

                return _RequestedUser;
            }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User; }
        }

        public UsersNetworkPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            if (!GrantsManager[UsersApplication.ApplicationSystemName + "_networks"].Allowed)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

            CheckGrantsOnThisUser(RequestedUser);

            SetToolbars();
        }

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Table, "Elenco Utenti"));
            this.Toolbar.Buttons.Add(new ToolbarButton("user_details.aspx?id=" + this.RequestedUser.ID, NetCms.GUI.Icons.Icons.Page_White_Magnify, "Scheda Utente"));
        }

        public override WebControl Control
        {
            get 
            {
                WebControl output = BuildControls();
                return output;
            }
        }

        private WebControl BuildControls()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);

            G2Core.XhtmlControls.InputField add = new G2Core.XhtmlControls.InputField("add");
            if (add.RequestVariable.IsPostBack && add.RequestVariable.IsValidInteger)
            {
                if (!RequestedUser.NetworkEnabledForUser(add.RequestVariable.IntValue))
                {
                    RequestedUser.EnableNetwork(add.RequestVariable.IntValue);
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha aggiunto con successo il portale con id=" + add.RequestVariable.StringValue + " all'utente " + this.RequestedUser.NomeCompleto + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    GUI.PopupBox.AddMessage("Portale aggiunto con successo", NetCms.GUI.PostBackMessagesType.Success);
                }
                else
                {
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha provato ad aggiungere il portale con id=" + add.RequestVariable.StringValue + " all'utente " + this.RequestedUser.NomeCompleto + ", che per� era gi� presente tra i portali dell'utente.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    GUI.PopupBox.AddMessage("Il portale � gi� presente tra i portali dell'utente.", NetCms.GUI.PostBackMessagesType.Notify);

                }
            }
            G2Core.XhtmlControls.InputField delete = new G2Core.XhtmlControls.InputField("delete");
            if (delete.RequestVariable.IsPostBack && delete.RequestVariable.IsValidInteger)
            {
                RequestedUser.DisableNetwork(delete.RequestVariable.IntValue);
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha rimosso il portale con id=" + delete.RequestVariable.StringValue + " all'utente " + this.RequestedUser.NomeCompleto + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                GUI.PopupBox.AddMessage("Portale rimosso con successo", NetCms.GUI.PostBackMessagesType.Success);
            }

            if (!IsPostBack)
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo all'elenco portali associati all'utente " + this.RequestedUser.NomeCompleto + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

            }
            control.Controls.Add(add.Control);
            control.Controls.Add(delete.Control);

            //string mynetsSQL = "SELECT * FROM Users_Networks INNER JOIN Networks ON id_network = Network_UserNetwork WHERE User_UserNetwork = " + this.Administrator.ID;
            //DataTable networks = Connection.SqlQuery(mynetsSQL);

            //G2Core.AdvancedXhtmlTable.AxtTable table = new G2Core.AdvancedXhtmlTable.AxtTable(networks);
            //table.CssClass = "tab";
            //table.OverTitle = "Portali presenti nel Sistema";

            //AxtGenericColumn nome = new AxtGenericColumn("Label_Network", "Nome Portale");
            //table.Columns.Add(nome);

            //AxtActionColumn aggiungi = new AxtActionColumn("id_Network", "Aggiungi", "action add", "javascript: setFormValue({0},'" + add.ID + "',1)");
            //table.Columns.Add(aggiungi);

            //control.Controls.Add(table.Control);

            ArTable table = new ArTable(this.Administrator.AssociatedNetworks.Select(x => x.Network).Where(x => this.RequestedUser.AssociatedNetworks.Where(y=>y.Network.ID == x.ID).Count() == 0));
            table.InnerTableCssClass = "tab";
            table.MainTitle = "Portali presenti nel Sistema";

            ArTextColumn nome = new ArTextColumn("Nome Portale", "Label");
            table.AddColumn(nome);

            ArActionColumn aggiungi = new ArActionColumn("Aggiungi", ArActionColumn.Icons.Disable, "javascript: setFormValue({ID},'" + add.ID + "',1)");
            aggiungi.CssClass = "action add";
            table.AddColumn(aggiungi);

            control.Controls.Add(table);


//            string sql = @"SELECT * FROM Users_Networks 
//INNER JOIN Networks ON id_network = Network_UserNetwork 
//INNER JOIN (SELECT id_network FROM Users_Networks INNER JOIN Networks ON id_network = Network_UserNetwork WHERE User_UserNetwork = " + this.Administrator.ID + @") AS SOURCE ON Networks.id_network = SOURCE.id_network
//WHERE User_UserNetwork = " + this.RequestedUser.ID;

//            DataTable enablednetworks = Connection.SqlQuery(sql);

//            table = new G2Core.AdvancedXhtmlTable.AxtTable(enablednetworks);
//            table.OverTitle = "Portali associati all'Utente";
//            table.CssClass = "tab";

//            nome = new AxtGenericColumn("Label_Network", "Nome Portale");
//            table.Columns.Add(nome);

//            AxtActionColumn rimuovi = new AxtActionColumn("id_Network", "Rimuovi", "action delete", "javascript: setFormValue({0},'" + delete.ID + "',1)");
//            table.Columns.Add(rimuovi);

//            control.Controls.Add(table.Control);

            ArTable tableAssociati = new ArTable(this.RequestedUser.AssociatedNetworks.Select(x => x.Network));
            tableAssociati.InnerTableCssClass = "tab";
            tableAssociati.MainTitle = "Portali associati all'Utente";

            ArTextColumn nomeAssociato = new ArTextColumn("Nome Portale", "Label");
            tableAssociati.AddColumn(nomeAssociato);

            ArActionColumn rimuoviAssociato = new ArActionColumn("Rimuovi", ArActionColumn.Icons.Disable, "javascript: setFormValue({ID},'" + delete.ID + "',1)");
            aggiungi.CssClass = "action delete";
            tableAssociati.AddColumn(rimuoviAssociato);

            control.Controls.Add(tableAssociati);

            return control;
        }
    }
}