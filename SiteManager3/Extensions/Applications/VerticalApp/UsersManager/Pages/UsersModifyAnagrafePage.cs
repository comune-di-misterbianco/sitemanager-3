using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetTable;
using NetCms.GUI;
using NetCms.Vertical.Grants;
using NetCms.Users;
using NetForms;
using System.Collections;
using System.Web.Security;
using NetCms.Vertical;
using NetCms.Users.CustomAnagrafe;

namespace NetCms.Users.UsersManager
{
    [DynamicUrl.PageControl("user_mod.aspx", ApplicationID = UsersApplication.ApplicationID)]
    public class UsersModifyAnagrafePage : AbstractPage
    {
        public override string PageTitle
        {
            get { return base.PageTitle + " Aggiornamento Dati Utente '" + this.RequestedUser.NomeCompleto + "'"; }
        }

        private AnagraficaBase Anagrafica
        {
            get
            {
                return RequestedUser.Anagrafica;
            }
        }

        private User _RequestedUser;
        private User RequestedUser
        {
            get
            {
                if (_RequestedUser == null)
                {
                    NetUtility.RequestVariable req = new NetUtility.RequestVariable("id", NetUtility.RequestVariable.RequestType.QueryString);
                    if (req.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                        //_RequestedUser = new User(req.IntValue.ToString());
                        _RequestedUser = UsersBusinessLogic.GetById(req.IntValue);
                }
                if (_RequestedUser == null || _RequestedUser.Anagrafica == null)
                    NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

                return _RequestedUser;
            }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User; }
        }

        private UsersModifyVFManager UsersModifyForm
        {
            get
            {
                if (_UsersModifyForm == null)
                {
                    _UsersModifyForm = new UsersModifyVFManager("modify", this.RequestedUser);
                    _UsersModifyForm.CssClass = "Axf";
                }
                return _UsersModifyForm;
            }
        }
        private UsersModifyVFManager _UsersModifyForm;
        
        //private AxfForm _AnagraficaForm;
        //private AxfForm AnagraficaForm
        //{
        //    get
        //    {
        //        if (_AnagraficaForm == null)
        //        {
        //            _AnagraficaForm = new AxfForm();
        //            _AnagraficaForm.AddSubmitButton = false;
        //            _AnagraficaForm.SubmitSourceID = "BtAggiungi";

        //            _AnagraficaForm.Tables.Add(Anagrafica.GetProfileAxtTable());
        //            _AnagraficaForm.Tables.Add(UserFormTable);
        //        }

        //        return _AnagraficaForm;
        //    }
        //}

        //private bool PostBackExit = false;

        //private AxfGenericTable _UserFormTable;
        //private AxfGenericTable UserFormTable
        //{
        //    get
        //    {
        //        if (_UserFormTable == null)
        //        {

        //            AxfGenericTable user = new AxfGenericTable("Users", "id_User", Connection);
        //            user.RelatedRecordID = RequestedUser.ID;
        //            #region Campi Anagrafica

        //            AxfPassword Password = new AxfPassword("Password", "Password_User");
        //            Password.CyperType = AxfPassword.CyperingType.MD5;// NetPasswordField.CyperingType.MD5;
        //            Password.Required = false;
        //            Password.HighSecurityField = true;
        //            user.addField(Password);

        //            //AxfDropDownList NetServiceAdministrator = new AxfDropDownList("Tipo di Utente", "Type_User");
        //            ////NetServiceAdministrator.ClearList();
        //            //NetServiceAdministrator.Required = true;
        //            //NetServiceAdministrator.addItem("Utente Frontend", "0");
        //            //NetServiceAdministrator.addItem("Utente Backoffice", "1");
        //            //user.addField(NetServiceAdministrator);

        //            #endregion

        //            _UserFormTable = user;
        //        }

        //        return _UserFormTable;
        //    }
        //}

        private void SetToolbars()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.Vcard, "Elenco Utenti"));
            this.Toolbar.Buttons.Add(new ToolbarButton("user_details.aspx?id=" + this.RequestedUser.ID, NetCms.GUI.Icons.Icons.Page_White_Magnify, "Scheda Utente"));
        }

        public UsersModifyAnagrafePage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
            CheckGrantsOnThisUser(RequestedUser);

            if (!GrantsManager[UsersApplication.ApplicationSystemName+"_edit"].Allowed)
                NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();

            SetToolbars();

            this.UsersModifyForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);

            //btPostBack();
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (this.UsersModifyForm.UserForm.IsValid == NetService.Utility.ValidatedFields.VfGeneric.ValidationStates.Valid &&
                this.UsersModifyForm.AnagraficaForm.IsValid == NetService.Utility.ValidatedFields.VfGeneric.ValidationStates.Valid)
            {
                bool sendupdateemailconfirmation = false;
                string oldemail = Anagrafica.Email;
                string newemail = "";

                newemail = this.UsersModifyForm.AnagraficaForm.Fields.Where(x => x.Key == "Email").First().PostBackValue;

                string oldIsUsername = "";
                string newIsUsername = "";
                string ifIsUsernameChanged = "";

                if (string.Compare(oldemail, newemail, true) != 0)
                {
                    sendupdateemailconfirmation = true;
                }

                string nuovaPassword = this.UsersModifyForm.UserForm.Fields.Find(x => x.Key == "Password").PostBackValue;
                

                if (!string.IsNullOrEmpty(nuovaPassword))
                {
                    nuovaPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(nuovaPassword, "MD5").ToString();
                    this.RequestedUser.Password = nuovaPassword;
                    this.RequestedUser.LastUpdate = DateTime.Now;
                }

                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;

                string userNameUtenteInserito = "";
                if (RequestedUser.CustomAnagrafe.EmailAsUserName)
                    userNameUtenteInserito = newemail;
                else 
                {
                    CustomField customFieldIsUsername = RequestedUser.CustomAnagrafe.CustomFields.Where(x => x.IsUsername).FirstOrDefault();
                    userNameUtenteInserito = this.UsersModifyForm.AnagraficaForm.Fields.Where(x => x.Key == customFieldIsUsername.CampoInAnagrafe.Key).First().PostBackValue;
                }

                User utente = UsersBusinessLogic.GetUserByUserName(userNameUtenteInserito);
                if (utente == null || (utente != null && utente.ID == RequestedUser.ID))
                {

                    if (UsersBusinessLogic.UpdateUserAnagrafica(this.UsersModifyForm.AnagraficaForm, this.Anagrafica.ID, this.RequestedUser.AnagraficaType))
                    {
                        CustomField customFieldIsUsername = RequestedUser.CustomAnagrafe.CustomFields.Where(x => x.IsUsername).FirstOrDefault();
                        if (customFieldIsUsername != null)
                        {
                            oldIsUsername = RequestedUser.AnagraficaMapped[customFieldIsUsername.Name].ToString();
                            newIsUsername = this.UsersModifyForm.AnagraficaForm.Fields.Where(x => x.Key == customFieldIsUsername.CampoInAnagrafe.Key).First().PostBackValue;
                            if (string.Compare(oldIsUsername, newIsUsername, true) != 0)
                            {
                                ifIsUsernameChanged = "Il campo '" + customFieldIsUsername.Name + "' usato come User Name � stato modificato. A seguito di questa operazione la nuova UserName � stata cambiata da '" + oldIsUsername + "' a '" + newIsUsername + "'";
                            }
                        }
                        string message = "";
                        if (sendupdateemailconfirmation)
                        {
                            NetCms.Users.AccountManager.SendConfirmationEmailUpdate(RequestedUser.NomeCompleto, oldemail, newemail, RequestedUser.ID.ToString(), null);
                            if (newemail != null && newemail.Length > 0)
                                message += " Per attuare la modifica dell'email a \"" + newemail + "\", seguite le indicazioni inviateVi per posta elettronica all'indirizzo fornito.";
                        }
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha aggiornato con successo i dati dell'utente " + this.RequestedUser.NomeCompleto + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        PopupBox.AddSessionMessage("Utente Aggiornato</br>" + ifIsUsernameChanged + "</br>" + message, PostBackMessagesType.Success, true);
                    }
                    else
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore tecnico nel tentativo di modifica dei dati dell'utente con id " + this.RequestedUser.ID + " da parte dell'utente con username " + userName, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        PopupBox.AddSessionMessage("Errore tecnico nella modifica dell'utente", PostBackMessagesType.Error);
                    }
                }
                else
                {
                    PopupBox.AddMessage("Lo Username '" + userNameUtenteInserito + "' � gi� utilizzato, sceglierne uno diverso.", PostBackMessagesType.Error);
                }
            }
        }

        //private void btPostBack()
        //{
        //    //if (HttpContext.Current.Request.Form["BtAggiungi"] != null)
        //    //{
        //    //    string errors = "<ul>";

        //    //    if (AnagraficaForm.serializedValidation())
        //    //    {
        //    //        string errorMessage = "";
        //    //        bool validated = true;
        //    //        if (RequestedUser.Anagrafica.NonLegalCodeDeclared != null && !RequestedUser.Anagrafica.NonLegalCodeDeclared.Value)
        //    //        {
        //    //            bool CFisValid = RequestedUser.Anagrafica.ValidateCF((G2Core.AdvancedXhtmlForms.AxfGenericTable)AnagraficaForm.Tables[0]);
        //    //            if (!CFisValid)
        //    //            {
        //    //                errorMessage = "I dati immessi non corrispondono con quelli presenti nel codice fiscale!";
                                                        
        //    //                validated = false;
        //    //            }
        //    //        }

        //    //        if (validated)
        //    //        {
        //    //            if (AnagraficaForm.serializedUpdate() == AxfForm.LastOperationValues.OK)
        //    //            {
        //    //                PopupBox.AddSessionMessage("Utente Aggiornato", PostBackMessagesType.Success, true);
        //    //            }
        //    //        }
        //    //        else
        //    //        {
        //    //            PopupBox.AddSessionMessage(errorMessage, PostBackMessagesType.Notify);
        //    //        }
        //    //    }
        //    //    else
        //    //    {
        //    //        ICollection tabelle = AnagraficaForm.FormStringsForLog.Values;
        //    //        foreach (ArrayList tabella in tabelle)
        //    //        {
        //    //            string[] log_campi = (string[])tabella.ToArray(typeof(string));
        //    //            foreach (string log_campo in log_campi)
        //    //            {
        //    //                string log_error = "";
        //    //                if (log_campo.Contains("L'utente ha inserito nel campo "))
        //    //                {
        //    //                    log_error = log_campo.Replace("L'utente ha inserito nel campo ", "Il campo ");
        //    //                    int index = log_error.IndexOf(" della tabella ");
        //    //                    log_error = log_error.Remove(index + 1);
        //    //                    log_error += " non � valido.";
        //    //                }
        //    //                if (log_campo.Contains("L'utente non ha valorizzato il campo "))
        //    //                {
        //    //                    log_error = log_campo.Replace("L'utente non ha valorizzato il campo ", "Il campo ");
        //    //                    int index = log_error.IndexOf(" della tabella ");
        //    //                    log_error = log_error.Remove(index + 1);
        //    //                    log_error += " � obbligatorio.";
        //    //                }
        //    //                errors += "<li>" + log_error + "</li>";
        //    //            }
        //    //        }
        //    //        errors += "</ul>";
        //    //        PopupBox.AddSessionMessage(errors, PostBackMessagesType.Notify);
        //    //    }
        //    //}
        //}

        public override WebControl Control
        {
            get
            {
                WebControl output = new WebControl(HtmlTextWriterTag.Div);

                HtmlGenericControl container = new HtmlGenericControl("div");
                output.Controls.Add(container);
                //ExternalAppzGrantsChecker Checker = new ExternalAppzGrantsChecker("users");
                VerticalApplication usersApp = VerticalApplicationsPool.GetBySystemName("users");
                if (!usersApp.Grant)
                {
                    output.Controls.Add(usersApp.ErrorControl);
                }
                else
                {
                    //if (!PostBackExit)
                    //{
                    //    HtmlGenericControls.Fieldset fieldset = new HtmlGenericControls.Fieldset("Dati Anagrafici");
                    //    container.Controls.Add(fieldset);
                    //    //fieldset.Controls.Add(RequestedUser.Anagrafica.GetProfileAxtTable().GetControl());
                    //    fieldset.Controls.Add(UsersModifyForm.AnagraficaForm);

                    //    fieldset = new HtmlGenericControls.Fieldset("Dati Login");
                    //    container = new HtmlGenericControl("div");
                    //    output.Controls.Add(container);
                    //    container.Controls.Add(fieldset);
                    //    //fieldset.Controls.Add(UserFormTable.GetControl());
                    //    fieldset.Controls.Add(UsersModifyForm.UserForm);

                    //    HtmlGenericControl p;

                    //    fieldset = new HtmlGenericControls.Fieldset("Aggiorna dati");
                    //    container = new HtmlGenericControl("div");
                    //    output.Controls.Add(container);
                    //    container.Controls.Add(fieldset);

                    //    p = new HtmlGenericControl("p");
                    //    fieldset.Controls.Add(p);
                    //    Button bt = new Button();
                    //    bt.Text = "Aggiorna Dati Utente";
                    //    bt.ID = "BtAggiungi";
                    //    bt.OnClientClick = "setSubmitSource('BtAggiungi')";
                    //    p.Controls.Add(bt);
                    //    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    //    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di aggiornamento dati dell'utente " + this.RequestedUser.NomeCompleto + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                    //}
                    //else
                    //{
                    //    HtmlGenericControls.Fieldset ok = new HtmlGenericControls.Fieldset("Aggiornamento Dati Utente");
                    //    HtmlGenericControls.Div div = new HtmlGenericControls.Div();
                    //    string qs = HttpContext.Current.Request.QueryString["folder"];

                    //    int folder;
                    //    if (qs != null && int.TryParse(qs, out folder))
                    //        qs = "folder=" + folder;
                    //    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    //    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha aggiornato con successo i dati dell'utente " + this.RequestedUser.NomeCompleto + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    //    div.InnerHtml = "Utente aggiornato con successo<br /><a href=\"" + PageData.BackAddress + "\">Torna Indietro</a>";
                    //    output.Controls.Add(ok);
                    //    ok.Controls.Add(div);
                    //}

                    output.Controls.Add(UsersModifyForm);

                }

                return output;
            }
        }
    }
}