﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using NetCms.Vertical;


namespace NetCms.Users.UsersManager
{
    public class Setup : NetCms.Vertical.VerticalApplicationSetup
    {
        public override string[,] Pages
        {
            get
            {                
                return null;
            }
        }
        public override string[,] Criteria
        {
            get
            {
                string[,] criteria = {
                                        {"Abilita creazione di nuovi utenti." , this.ApplicationBaseData.SystemName + "_create" } ,
                                        {"Abilita la modifica dei dati relativi agli utenti.", this.ApplicationBaseData.SystemName + "_edit"},
                                        {"Abilita la visione di tutti i dati anagrafici dell'utente.", this.ApplicationBaseData.SystemName + "_fulldetails"},
                                        {"Abilita la gestione dei gruppi associati all'utente.", this.ApplicationBaseData.SystemName + "_groups"},
                                        {"Abilita la gestione dei portali al quale gli utenti ha diritto accedere.", this.ApplicationBaseData.SystemName + "_networks"},
                                        {"Abilita la modifica dello stato dell'utente.", this.ApplicationBaseData.SystemName + "_status"},
                                        {"Abilita la gestione degli Utenti non associati ad alcun gruppo.", this.ApplicationBaseData.SystemName + "_ungroupped"},
                                        {"Abilita l'inoltro di nuove credenziali temporanee di accesso agli utenti.", this.ApplicationBaseData.SystemName + "_new_password"},
                                        {"Abilita visualizzazione riepilogo permessi.", this.ApplicationBaseData.SystemName + "_grants"},
                                        {"Abilita la gestione dei servizi associabili all'utente.", this.ApplicationBaseData.SystemName + "_services"}
                                     };
                return criteria;
            }
        }

        public override bool IsNetworkDependant
        {
            get { return false; }
        }

        public Setup(VerticalApplication baseApp)
            : base(baseApp)
        {
        }

        protected override bool LocalInstall(bool isInsidePackage = false)
        {
            return true;
        }
    }
}
