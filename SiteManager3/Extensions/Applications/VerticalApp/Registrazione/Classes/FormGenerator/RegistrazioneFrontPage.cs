﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using NetCms.Front.Static;
//using Registrazione;
//using NetService.Utility.Request;

//namespace NetCms.Users.CustomAnagrafe
//{
//    public class RegistrazioneFrontPage: WebControl
//    {
//        public QSVar<bool> RegistrationComplete
//        {
//            get
//            {
//                return _RegistrationComplete ?? (_RegistrationComplete = new QSVar<bool>("complete"));
//            }
//        }
//        private QSVar<bool> _RegistrationComplete;

//        public QSVar<string> RegistrationData
//        {
//            get
//            {
//                return _RegistrationData ?? (_RegistrationData = new QSVar<string>("data", true));
//            }
//        }
//        private QSVar<string> _RegistrationData;

//        public CustomAnagrafeConfiguration CurrentCustomAnagrafe
//        {
//            get
//            {
//                return _CurrentCustomAnagrafe ?? (_CurrentCustomAnagrafe = CustomAnagrafeContext.CurrentCustomAnagrafe);
//            }
//        }
//        private CustomAnagrafeConfiguration _CurrentCustomAnagrafe;

//        public RegistrazioneFrontPage()
//            : base(HtmlTextWriterTag.Div)
//        {
//            if (CurrentCustomAnagrafe == null)
//                throw new NetCms.Exceptions.PageNotFound404Exception();
//        }

//        protected override void OnInit(EventArgs e)
//        {
//            base.OnInit(e);
//            if (!RegistrationComplete.Value)
//            {
//                FormGenerator form = new FormGenerator(CurrentCustomAnagrafe);
//                this.Controls.Add(form);
//            }
//            else
//            {
//                AddCompleteControls();
//            }
//        }

//        protected void AddCompleteControls()
//        {
//            string html = this.CurrentCustomAnagrafe.TestoCompletamentoRegistrazione;
//            html = html.Replace("{RIEPILOGO_DATI}", GetReviewSheet());
//            this.Controls.Add(new LiteralControl(html));
//        }

//        protected string GetReviewSheet()
//        {
//            string list = "";
//            if (RegistrationData.HasValidValue)
//            {
//                string[] regDataArray = RegistrationData.Value.Split(new[] { "###" }, StringSplitOptions.RemoveEmptyEntries);
//                list += "<ul>";

//                var usableData = from splittedData in
//                                     (from fieldData in regDataArray
//                                      select fieldData.Split(new[] { "===" }, StringSplitOptions.RemoveEmptyEntries))
//                                 where splittedData.Length == 2
//                                 select splittedData;
//                foreach (string[] fieldData in usableData)
//                {
//                    list += "<li><strong>{0}:&nbsp;</strong>{1}</li>".FormatByParameters(fieldData);
//                }
//                list += "</ul>";
//            }
//            return list;
//        }
//    }
//}
