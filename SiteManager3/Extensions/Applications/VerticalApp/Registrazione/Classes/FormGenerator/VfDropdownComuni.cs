﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetService.Utility.ValidatedFields;
using System.Linq;
using NetService.Utility.RecordsFinder;
using System.Data;

namespace Registrazione
{
    public class VfDropDownComune: VfGeneric
    {
        public override WebControl Field
        {
            get { return ComuneDropDownList; }
        }

        public override object PostbackValueObject
        {
            get
            {
                int res;
                bool val;
                if (int.TryParse(this.Request.OriginalValue,out res))
                    return int.Parse(this.Request.OriginalValue);
                if (bool.TryParse(this.Request.OriginalValue, out val))
                    return bool.Parse(this.Request.OriginalValue);
                return this.Request.OriginalValue;
            }
        }
        
        public Dictionary<string,string> Options
        {
            get
            {
                if (_Options == null)
                {
                    _Options = new Dictionary<string,string>();
                }
                return _Options;
            }
        }
        private Dictionary<string,string> _Options;

        public Dictionary<string, string> OptionsProvincia
        {
            get
            {
                if (_OptionsProvincia == null)
                {
                    _OptionsProvincia = new Dictionary<string, string>();
                }
                return _OptionsProvincia;
            }
        }
        private Dictionary<string, string> _OptionsProvincia;

        public DropDownList ComuneDropDownList
        {
            get
            {
                if (_ComuneDropDownList == null)
                {
                    _ComuneDropDownList = new DropDownList();
                    _ComuneDropDownList.ID = this.Key;

                    CheckProvinciaRequest();
                }
                return _ComuneDropDownList;
            }
        }
        private DropDownList _ComuneDropDownList;
        
        public override VfSearchParameter SearchParameter
        {
            get
            {
                if (_SearchParameter == null)
                {
                    _SearchParameter = new VfDropDownListSearchParameter(this, SearchComparisonCriteria);
                }
                return _SearchParameter;
            }
        }
        private VfSearchParameter _SearchParameter;

        public SearchParameter ProvinciaSearchParameter
        {
            get
            {
                return null;
            }
        }
        private SearchParameter _ProvinciaSearchParameter;

        public override Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return Finder.ComparisonCriteria.Equals; }
            set
            { }
        }

        protected virtual void InitComuniList(int idProvincia)
        {
            if (idProvincia>0)
            {
                var comuni = GetComuniByProvincia(idProvincia);
                Options.Clear();
                Options.Add("", "");
                foreach (var comune in comuni)
                    Options.Add(comune["comune"].ToString(), comune["IDComune"].ToString());
            }
        }
        protected virtual void InitProvinciaList()
        {
            this.OptionsProvincia.Add("", "");

            var province = GetProvince();

            foreach (var prov in province)
                OptionsProvincia.Add(prov["provincia"].ToString(), prov["IDProvincia"].ToString());
        }

        public override string ValueForDetails
        {
            get
            {
                string value = string.Empty;
                if(Request.IsValidInteger)
                {
                    var comuneData = GetComune(Request.IntValue);
                    if (comuneData != null)
                    {
                        value = comuneData["comune"].ToString();
                    }
                }
                return value;
            }
        }

        public DropDownList ProvinciaDropDownList
        {
            get
            {
                if (_ProvinciaDropDownList == null)
                {
                    _ProvinciaDropDownList = new DropDownList {ID = this.Key + "_provincie", AutoPostBack = true};
                    InitProvinciaList();
                }
                return _ProvinciaDropDownList;
            }
        }

        public NetService.Utility.Common.RequestVariable ProvinciaRequest
        {
            get
            {
                if (_ProvinciaRequest == null)
                {
                    _ProvinciaRequest = new NetService.Utility.Common.RequestVariable(ProvinciaDropDownList.ID);
                }
                return _ProvinciaRequest;
            }
        }
        private NetService.Utility.Common.RequestVariable _ProvinciaRequest;

        public int ProvinciaDefaultValue
        {
            get
            {
                return _ProvinciaDefaultValue;
            }
            set
            {
                _ProvinciaDefaultValue = value;
            }
        }
        private int _ProvinciaDefaultValue; 

        void CheckProvinciaRequest()
        {
            //try
            bool enabled = false;
            if(ProvinciaRequest.IsPostBack && ProvinciaRequest.IsValidInteger)
            {
                var provinciaSelezionata = GetProvincia(ProvinciaRequest.IntValue);
                if (provinciaSelezionata != null) 
                    InitComuniList(provinciaSelezionata["IDProvincia"].ToString().ToInt());
                enabled = provinciaSelezionata != null;
            }
            this.ComuneDropDownList.Enabled = enabled;
            //catch{}
        }
        private DropDownList _ProvinciaDropDownList;

        public WebControl ScriptControl
        {
            get
            {
                if (_ScriptControl == null)
                {
                    _ScriptControl = new WebControl(HtmlTextWriterTag.Script);
                    _ScriptControl.Attributes.Add("type","text/javascript");

                    string script = @"

";
                }
                return _ScriptControl;
            }
        }
        private WebControl _ScriptControl;

        public NetCms.Connections.Connection Connection { get; private set; } 

        public override string LocalCssClass
        {
            get
            {
                return "dropdown-vf dropdown-vf-comune";
            }
        }

        public VfDropDownComune(string key, string labelResourceKey, string activeRecordPropertyName, NetCms.Connections.Connection conn)
            : base(key, labelResourceKey, activeRecordPropertyName)
        {
            Connection = conn;
        }

        public VfDropDownComune(string id, string label, NetCms.Connections.Connection conn)
            : base(id, label)
        {
            Connection = conn;
        }

        protected override void FillFieldValue()
        {
            if (this.ProvinciaDefaultValue >0)
            {
                this.ProvinciaDropDownList.SelectedValue = this.ProvinciaDefaultValue.ToString();
                var provinciaSelezionata = GetProvincia(ProvinciaDefaultValue);
                if (provinciaSelezionata != null)
                {
                    InitComuniList(provinciaSelezionata["IDProvincia"].ToString().ToInt());
                    this.ComuneDropDownList.Enabled = true;
                }
            }
            else
            {
                if (this.DefaultValue != null && this.DefaultValue is int)
                {
                    InitComuniList(GetComune((int)this.DefaultValue)["IDProvincia"].ToString().ToInt());
                    this.ComuneDropDownList.Enabled = true;
                }
            }

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            
            this.ComuneDropDownList.Items.Clear();

            foreach (var entry in this.Options)
            {
                ListItem item = new ListItem( NetService.Localization.LabelsManager.GetLabel(entry.Key), entry.Value);
                if (this.Request.IsPostBack)
                {
                    item.Selected = this.Request.IsValidString && this.Request.StringValue == entry.Value;
                }
                else
                {
                    if (!item.Selected)
                    {
                        string value = (DefaultValue != null) ? DefaultValue.GetType().IsEnum ? ((int)DefaultValue).ToString() : DefaultValue.ToString() : null;
                        if (value != null && value == entry.Value)
                            item.Selected = true;
                    }
                    if (!item.Selected)
                        item.Selected = this.DefaultValue != null && this.DefaultValue is int && entry.Value == this.DefaultValue.ToString();
                }

                this.ComuneDropDownList.Items.Add(item);
            }

            this.ProvinciaDropDownList.Items.Clear();

            foreach (var entry in this.OptionsProvincia)
            {
                ListItem item = new ListItem(NetService.Localization.LabelsManager.GetLabel(entry.Key), entry.Value);
                if (this.ProvinciaRequest.IsPostBack)
                {
                    if (this.ProvinciaRequest.IsValidString && this.ProvinciaRequest.StringValue == entry.Value)
                        item.Selected = true;
                }
                else
                {
                    if (!item.Selected)
                    {
                        string value = (ProvinciaDefaultValue >= 0) ? ProvinciaDefaultValue.GetType().IsEnum ? ((int)ProvinciaDefaultValue).ToString() : ProvinciaDefaultValue.ToString() : null;
                        if (value != null && value == entry.Value)
                            item.Selected = true;
                    }
                    if (!item.Selected)
                        item.Selected = (this.DefaultValue != null && this.DefaultValue is int && entry.Value == this.DefaultValue.ToString()) ||
                                    (this.ProvinciaDefaultValue > 0 && entry.Value == ProvinciaDefaultValue.ToString());

                }

                this.ProvinciaDropDownList.Items.Add(item);
            }
        }

        protected override void AttachFieldControls()
        {
            Label label = new Label { AssociatedControlID = this.Key, Text = Label + (this.Required ? "*" : "") };
            Label selezionaProvincia = new Label { Text = "Seleziona Provincia", CssClass = "vf-label-provincia" };
            Label selezionaComune = new Label { Text = "Seleziona Comune", CssClass = "vf-label-comune" };
            
            label.ViewStateMode = System.Web.UI.ViewStateMode.Disabled;
            selezionaProvincia.ViewStateMode = System.Web.UI.ViewStateMode.Disabled;
            selezionaComune.ViewStateMode = System.Web.UI.ViewStateMode.Disabled;

            ProvinciaDropDownList.ViewStateMode = System.Web.UI.ViewStateMode.Disabled;
            ComuneDropDownList.ViewStateMode = System.Web.UI.ViewStateMode.Disabled;

            this.ViewStateMode = System.Web.UI.ViewStateMode.Disabled;
            
            this.Controls.Add(label);
            
            this.Controls.Add(selezionaProvincia);
            this.Controls.Add(ProvinciaDropDownList);

            this.Controls.Add(selezionaComune);
            this.Controls.Add(ComuneDropDownList);

            this.Field.ID = this.Key;
        }

        protected override void LocalValidate()
        {
            if (!this.Request.IsValidString && this.Required)
            {
                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
            }
        }

        public DataRow GetProvincia(int id)
        {
            return Connection.SqlQuery("SELECT * FROM province WHERE IDProvincia = {0}".FormatByParameters(id)).Rows.Cast<DataRow>().FirstOrDefault();
        }
        public DataRow GetComune(int id)
        {
            return Connection.SqlQuery("SELECT * FROM comuni WHERE IDComune = {0}".FormatByParameters(id)).Rows.Cast<DataRow>().FirstOrDefault();
        }
        public List<DataRow> GetProvince()
        {
            return Connection.SqlQuery("SELECT * FROM province").Rows.Cast<DataRow>().ToList();
        }
        public List<DataRow> GetComuniByProvincia(int idProvincia)
        {
            return Connection.SqlQuery("SELECT * FROM comuni WHERE IDProvincia = {0}".FormatByParameters(idProvincia)).Rows.Cast<DataRow>().ToList();
        }
    }
}
