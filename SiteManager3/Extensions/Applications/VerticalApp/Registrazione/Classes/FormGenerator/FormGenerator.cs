﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Xml;
//using System.Reflection;
//using System.Web.UI.WebControls;
//using NetService.Utility.ValidatedFields;
//using NetCms.Connections;
//using NetCms.Users.CustomAnagrafe;
//using NetCms.Diagnostics;
//using NetService.Utility.Request;
//using NetCms.Users;
//using System.Data;

//namespace Registrazione
//{
//    public class FormGenerator:VfManager
//    {
//        public string CurrentNetworkPath
//        {
//            get
//            {
//                return NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;
//            }
//        }

//        private CustomAnagrafeConfiguration config;
//        public bool RegistrationComplete { get; private set; } 
//        public FormGenerator(CustomAnagrafeConfiguration config)
//            :base(config.ClassName)
//        {
//            this.config = config;
//            this.AfterValidation += new PostbackEventHandler(FormGenerator_Postback);
//        }

//        void FormGenerator_Postback(object sender, EventArgs e)
//        {
//            if (this.IsValid == VfGeneric.ValidationStates.Valid)
//            {
//                string sql = BuildInsertQueryString();

//                CustomAnagraficaBase anagrafe = null;
//                NetCms.Users.User user = null;
//                string password = null;
//                int ID = config.Connection.ExecuteInsert(sql);
//                bool ok = ID > 0;

//                if (ok)
//                {
//                    anagrafe = config.GetAnagrafeInstance(ID);
//                    ok = anagrafe != null;
//                }
//                if (ok)
//                {
//                    AccountGenerator builder = new AccountGenerator(config, anagrafe);
//                    ok = builder.CreateAccount();
//                    if (ok)
//                    {
//                        user = builder.LastGeneratedUser;
//                        password = builder.LastGeneratedUserPassword;
//                    }
//                }
//                RegistrationComplete = ok;

//                if (RegistrationComplete)
//                {
//                    SendConfirmMail(user, password);
//                    var regData = from f in this.Fields
//                                  where !(f is VfHidden || f is VfCaptcha)
//                                  select GetDetailValue(f);

//                    string regDataQueryString = RequestVariable<string>.Encrypt(string.Join("###", regData));
//                    Diagnostics.Redirect(NetCms.Configurations.Paths.PageName+"?complete=true&data=" + regDataQueryString);
//                }
//            }
//        }

//        protected string GetDetailValue(VfGeneric f)
//        {
//            string model = "{0}==={1}";
//            string value = f.ValueForDetails;
//            var p = this.config.CustomFields.FirstOrDefault(x => x.DatabaseFieldName == f.Key);
//            if (p!=null && p.DetailsLabelQuery.HasContent())
//            {
//                try
//                {
//                    value = this.config.Connection.SqlQuery(p.DetailsLabelQuery.FormatByParameters(value)).Rows[0][0].ToString();
//                }
//                catch { }
//            }
//            model = model.FormatByParameters(f.Label, value);
//            return model;
//        }

//        protected override void OnInit(EventArgs e)
//        {
//            var fields = config.CustomFields.Select(field => BuildField(field));
//            this.Fields.AddRange(fields);

//            VfCaptcha captcha = new VfCaptcha("captcha", "Codice di Controllo");
//            this.Fields.Add(captcha);

//            base.OnInit(e);
//        }

//        public VfGeneric BuildField(Field customAnagrafeField)
//        {
//            VfGeneric fieldToReturn = null;
//            if(customAnagrafeField.ValidateFieldType.FullName == typeof(VfTextBox).FullName)
//            {
//                fieldToReturn = BuildFieldGeneric(customAnagrafeField);
//            }
//            else if(customAnagrafeField.ValidateFieldType.FullName == typeof(VfDropDown).FullName)
//            {
//                fieldToReturn = BuildFieldVfDropDown(customAnagrafeField);
//            }
//            else if (typeof(VfGeneric).IsAssignableFrom(customAnagrafeField.ValidateFieldType))
//            {
//                fieldToReturn = BuildFieldGeneric(customAnagrafeField);
//            }
//            return fieldToReturn;
//        }
//        protected VfDropDown BuildFieldVfDropDown(Field customAnagrafeField)
//        {
//            VfDropDown fieldToReturn = new VfDropDown(customAnagrafeField.DatabaseFieldName, customAnagrafeField.Label);

//            customAnagrafeField.ValidateFieldCustomProperties.ForEach(prop =>
//            {
//                try
//                {
//                    SetCustomProperty(fieldToReturn, prop);
//                }
//                catch { }
//            });

//            var propertyFieldData = customAnagrafeField.NodeData.ChildNodes.Cast<XmlNode>().First(node => node.Name == "propertyFieldData");
//            var data = propertyFieldData.ChildNodes.Cast<XmlNode>().First(node => node.Name == "validatedField");
//            var bindData = data.ChildNodes.Cast<XmlNode>().Where(x => x.Name == "sqlDatabind").FirstOrDefault();
//            if (bindData != null)
//            {
//                string select = bindData.Attributes["select"].Value;
//                string valueColumn = bindData.Attributes["valueColumn"].Value;
//                string labelColumn = bindData.Attributes["labelColumn"].Value;
//                bool addEmptyValue = false;
//                if (bindData.Attributes["addEmptyValue"] != null)
//                    addEmptyValue = bool.Parse(bindData.Attributes["addEmptyValue"].Value);
//                var connString = bindData.Attributes["conn"];
//                NetCms.Connections.Connection conn = config.Connection;
//                if (connString != null)
//                    conn = new G2Core.Connections.MySqlConnection(connString.Value);
//                var table = conn.SqlQuery(select);
//                if (addEmptyValue)
//                {
//                    //Aggiunto per evitare eccezioni di ViewState quando si lascia il campo richiesto vuoto
//                    fieldToReturn.DropDownList.EnableViewState = false;
//                    if (!fieldToReturn.Options.ContainsKey(""))
//                        fieldToReturn.Options.Add("", "");
//                }
//                foreach (DataRow row in table.Rows)
//                {
//                    if(!fieldToReturn.Options.ContainsKey(row[labelColumn].ToString()))
//                        fieldToReturn.Options.Add(row[labelColumn].ToString(), row[valueColumn].ToString());
//                }
//            }

//            return fieldToReturn;
//        }
//        protected VfGeneric BuildFieldGeneric(Field customAnagrafeField)
//        {
//            var type = customAnagrafeField.ValidateFieldType;
//            var parametersTypes = new[] { typeof(string), typeof(string) };
//            var parameters = new object[] { customAnagrafeField.DatabaseFieldName, customAnagrafeField.Label };
//            var costructor = type.GetConstructor(parametersTypes);
//            VfGeneric fieldToReturn = costructor.Invoke(parameters) as VfGeneric;

//            customAnagrafeField.ValidateFieldCustomProperties.ForEach(prop =>
//            {
//                SetCustomProperty(fieldToReturn, prop);
//            });

//            return fieldToReturn;
//        }

//        protected void SetCustomProperty(VfGeneric vfField, ControlToRenderPropertyDefinition customProperty)
//        {
//            var prop = vfField.GetType().GetProperty(customProperty.Name);
//            object value = GetDefault(prop.PropertyType);
            
//            if (prop.PropertyType.IsEnum)
//            {
//                value = Enum.Parse(prop.PropertyType, customProperty.Value);
//            }
//            else if (prop.PropertyType == typeof(string))
//            {
//                value = customProperty.Value;
//            }
//            else if (prop.PropertyType.IsValueType)
//            {
//                object[] args = new object[] { customProperty.Value as string };
//                Type[] types = new [] { typeof(string) };
//                var parseMethod = prop.PropertyType.GetMethod("Parse", types);
//                value = parseMethod.Invoke(null, args);
//            }

//            prop.GetSetMethod().Invoke(vfField, new[] { value });
//        }
//        public static object GetDefault(Type type)
//        {
//            if (type.IsValueType)
//            {
//                return Activator.CreateInstance(type);
//            }
//            return null;
//        }

//        private string BuildInsertQueryString()
//        {
//            List<string> values = new List<string>();
//            List<string> fields = new List<string>();

//            foreach (var field in this.Fields.Where(x => x.BindField))
//            {
//                string value = field.Request.IsValidString ?  field.Request.StringValue:string.Empty;
//                value = value.FilterForDB();
//                values.Add("'" + value + "'");
//                fields.Add(field.Key);
//            }
//            string sql = "INSERT INTO {0} ({1}) VALUES({2})";

//            sql = sql.FormatByParameters(
//                config.DatabaseTableName,
//                string.Join(",", fields),
//                string.Join(",", values)
//                );

//            return sql;
//        }
//        private string BuildUpdateQueryString()
//        {
//            List<string> values = new List<string>();
//            List<string> where = new List<string>();
//            List<string> fields = new List<string>();

//            foreach (var field in this.Fields.Where(x => x.BindField))
//            {
//                string value = field.Request.IsValidString ? field.Request.StringValue : string.Empty;
//                value = value.FilterForDB();
//                values.Add(value);
//                fields.Add(field.Key);
//                where.Add(field.Key + " = '" + value + "'");
//            }
//            string sql = "INSERT INTO {0} ({1}) VALUES({2})";

//            sql = sql.FormatByParameters(
//                config.DatabaseTableName,
//                string.Join(",", fields),
//                string.Join(",", values)
//                );

//            return sql;
//        }

//        public void SendConfirmMail(User user, string password)
//        {
//            string testoMail = this.config.TestoMailConfermaRegistrazione;
//            testoMail = FillStringPlaceholders(testoMail, user, password);
//            string subject = "" + NetCms.Configurations.PortalData.Nome + ": email di conferma registrazione al portale.";
//            string targetEmail = user.Anagrafica.Email;
//            if (NetCms.Configurations.Debug.GenericEnabled)
//                targetEmail = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailToReport"].ToString();

//            G2Core.Common.MailSender msg = new G2Core.Common.MailSender(
//                                                                            targetEmail,
//                                                                            subject,
//                                                                            testoMail,
//                                                                            NetCms.Configurations.PortalData.eMail,
//                                                                            NetCms.Configurations.PortalData.Nome);
//            msg.SetSmtpHost = NetCms.Configurations.PortalData.SmtpHost;
//            msg.SetSmtpPort = int.Parse(NetCms.Configurations.PortalData.SmtpPort);
//            msg.EnableSsl = NetCms.Configurations.PortalData.EnableSSL;

//            if (NetCms.Configurations.PortalData.SmtpUser != null && NetCms.Configurations.PortalData.SmtpPassword != null)
//            {
//                msg.SmtpUser = NetCms.Configurations.PortalData.SmtpUser;
//                msg.SmtpPassword = NetCms.Configurations.PortalData.SmtpPassword;
//            }

//            msg.Send();
//        }
//        public string FillStringPlaceholders(string text, User user, string password)
//        {
//            string value = text;
//            CustomAnagraficaBase anagrafe = user.Anagrafica as CustomAnagraficaBase;
//            value = anagrafe.FillStringPlaceholders(value);

//            value = value.Replace("{USERNAME}", user.UserName);
//            value = value.Replace("{PASSWORD}", password);
//            value = value.Replace("{KEY}", user.ActivationKey);

//            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();
//            value = value.Replace("{IP}", ip);
//            value = value.Replace("{ACTIVATE_URL}", "http://" + ip + CurrentNetworkPath + "/attivazione/default.aspx");

//            value = value.Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);
//            value = value.Replace("{EMAIL_PORTALE}", NetCms.Configurations.PortalData.eMail);

//            return value;
//        }
//    }
//}
