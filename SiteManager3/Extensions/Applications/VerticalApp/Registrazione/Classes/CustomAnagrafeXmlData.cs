﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Xml;
//using NetCms.Users.CustomAnagrafe;
//using System.Data;

//namespace Registrazione
//{
//    public class CustomAnagrafeConfiguration
//    {
//        public XmlDocument XmlDocument { get; private set; }
//        public XmlNamespaceManager XmlNamespaceManager { get; private set; }

//        public NetService.Utility.Context.ContextPageProperty<G2Core.Connections.MySqlConnection> ConnectionStorage
//        {
//            get
//            {
//                return _ConnectionStorage ?? (_ConnectionStorage = new NetService.Utility.Context.ContextPageProperty<G2Core.Connections.MySqlConnection>("CustomAnagrafeConfigurationConnection"));
//            }
//        }
//        private NetService.Utility.Context.ContextPageProperty<G2Core.Connections.MySqlConnection> _ConnectionStorage;

//        private NetCms.Connections.Connection _Connection;
//        public NetCms.Connections.Connection Connection
//        {
//            get
//            {
//                if (!ConnectionStorage.Exists)
//                {
//                    ConnectionStorage.Value = new G2Core.Connections.MySqlConnection(ConnectionString);
//                }
//                return ConnectionStorage.Value;
//            }
//        }

//        public string Label { get; private set; }
//        public string XmlPhysicalPath { get; private set; }
//        public string ConnectionString { get; private set; }
//        public string ClassName { get; private set; }
//        public string FrontendPageAddress { get; private set; }
//        public string FrontendPageTitle { get; private set; }
//        public string CreateTableSQL { get; private set; }
//        public string CreateSearchRoutineSQL { get; private set; }
//        public string DatabaseTableName { get; private set; }
//        public string SearchRoutineName { get; private set; }
//        public string TestoCompletamentoRegistrazione { get; private set; }
//        public string TestoMailConfermaRegistrazione { get; private set; }
//        public List<Field> CustomFields { get; private set; }
//        public List<MandatoryFieldInfo> MandatoryFieldsInfo { get; private set; }
//        public List<Group> GroupsToAssociate { get; private set; }

//        public string InstallStatusLabel
//        {
//            get { return IsInstalled ? "Installata" : "Non Installata"; }
//        }
//        public bool IsInstalled
//        {
//            get { return CustomAnagrafeContext.DataCollection.ContainsKey(this.GetFullClassName()); }
//        }        

//        public CustomAnagrafeConfiguration(string xmlPhysicalPath)
//        {

//            XmlDocument = new XmlDocument();
//            XmlDocument.Load(xmlPhysicalPath);

//            CustomFields = new List<Field>(
//                                                            GetNodes("/registrazione/fields/field")
//                                                            .Cast<XmlNode>()
//                                                            .Select(x=>new Field(x))
//                                                        );
//            MandatoryFieldsInfo = new List<MandatoryFieldInfo>(
//                                                                GetNode("/registrazione/mandatoryFields").ChildNodes
//                                                                .Cast<XmlNode>()
//                                                                .Select(x => new MandatoryFieldInfo(x))
//                                                               );
                                                        
//            GroupsToAssociate = new List<Group>(
//                                                            GetNodes("/registrazione/groups/group")
//                                                            .Cast<XmlNode>()
//                                                            .Select(x => new Group(x))
//                                                        );
//            var RootNode = XmlDocument.DocumentElement;
//            XmlPhysicalPath = xmlPhysicalPath;

//            Label = RootNode.Attributes["label"].Value;
//            ConnectionString = GetAttribute("/mstns:registrazione", "connectionString");
//            ClassName = GetAttribute(RootNode, "className");
//            FrontendPageAddress = GetAttribute(RootNode, "frontendPageAddress");
//            FrontendPageTitle = GetAttribute(RootNode, "frontendPageTitle");
//            CreateTableSQL = RemoveCDATA(GetNodeXml("/registrazione/database/createTableSQL"));
//            CreateSearchRoutineSQL = RemoveCDATA(GetNodeXml("/registrazione/database/createSearchRoutineSQL"));
//            DatabaseTableName = GetAttribute("/registrazione/database/createTableSQL", "tableName");
//            SearchRoutineName = GetAttribute("/registrazione/database/createSearchRoutineSQL", "routineName");
//            TestoMailConfermaRegistrazione = RemoveCDATA(GetNodeXml("/registrazione/texts/testoMailConfermaRegistrazione"));
//            TestoCompletamentoRegistrazione = RemoveCDATA(GetNodeXml("/registrazione/texts/testoCompletamentoRegistrazione"));
//        }

//        //public XmlNode GetNodeContent(string path)
//        //{
//        //    lock (XmlDocument)
//        //    {
//        //        XmlNodeList oNodeList = XmlDocument.SelectNodes(path, XmlNamespaceManager);
//        //        if (oNodeList != null && oNodeList.Count == 1)
//        //            return oNodeList[0];
//        //        else
//        //            return null;
//        //    }
//        //}
//        public string GetNodeXml(string path)
//        {
//            XmlNode oNode = GetNode(path);
//            if (oNode != null)
//                return oNode.InnerXml;
//            else
//                return null;
//        }
//        public List<XmlNode> GetNodes(string path)
//        {
//            XmlNode root = XmlDocument.DocumentElement;
//            string[] pathArray = path.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
//            lock (XmlDocument)
//            {
//                XmlNode result = root;
//                foreach (string s in pathArray.Skip(1).Take(pathArray.Length - 2))
//                {
//                    result = result.ChildNodes.Cast<XmlNode>().First(n => n.LocalName == s);
//                }
//                return result.ChildNodes.Cast<XmlNode>().Where(n=>n.LocalName == pathArray.Last()).ToList();
//            }
//        }
//        public string GetAttribute(string path, string attributeName)
//        {
//            XmlNode oNode = GetNode(path);
//            if (oNode != null)
//                if (oNode.Attributes[attributeName] != null)
//                    return oNode.Attributes[attributeName].Value;

//            return null;
//        }
//        public string GetAttribute(XmlNode oNode, string attributeName)
//        {
//            if (oNode != null)
//                if (oNode.Attributes[attributeName] != null)
//                    return oNode.Attributes[attributeName].Value;

//            return null;
//        }
//        public static string RemoveCDATA(string text)
//        {
//            if (text == null)
//                return "";

//            string str = text.Replace("<![CDATA[", "");
//            str = str.Replace("]]>", "");

//            return str;
//        }


//        public XmlNode GetNode(string path)
//        {
//            XmlNode root = XmlDocument.DocumentElement;
//            string[] pathArray = path.Split(new []{"/"}, StringSplitOptions.RemoveEmptyEntries);
//            lock (XmlDocument)
//            {
//                XmlNode result = root;
//                foreach (string s in pathArray.Skip(1))
//                {
//                    result = result.ChildNodes.Cast<XmlNode>().First(n => n.LocalName == s);
//                }
//                return result;
//            }
//        }

//        public bool Install()
//        {
//            G2Core.Connections.MySqlConnection conn = new G2Core.Connections.MySqlConnection(this.ConnectionString);
//            bool ok = true;

//            if (ok) ok = ok && NetCms.Connections.ConnectionsManager.CommonMySqlConnection.Execute("INSERT INTO custom_anagrafe (classname) VALUES('{0}')".FormatByParameters(GetFullClassName().FilterForDB()));
//            if (ok) ok = ok && conn.ExecuteCommand(CreateTableSQL, CommandType.Text, null);
//            if (ok) ok = ok && conn.ExecuteCommand(CreateSearchRoutineSQL, CommandType.Text, null);
//            if (ok)
//            {
//                try
//                {
//                    CustomAnagrafeContext.AddConfigurationToPool(this);
//                }
//                catch { ok = false; }
//            }
//            if (ok)
//            {
//                try
//                {
//                    Configurator.ConfigureFrontPage(this);
//                }
//                catch { ok = false; }
//            }
//            if (!ok)
//            {
//                UninstallFromDB();
//            }
//            return ok;
//        }
//        public bool Uninstall()
//        {
//            G2Core.Connections.MySqlConnection conn = new G2Core.Connections.MySqlConnection(this.ConnectionString);
//            bool ok = UninstallFromDB();
//            if (ok)
//            {
//                CustomAnagrafeContext.RemoveConfigurationFromPool(this.GetFullClassName());
//            } 
//            return ok;
//        }
//        private bool UninstallFromDB()
//        {
//            G2Core.Connections.MySqlConnection conn = new G2Core.Connections.MySqlConnection(this.ConnectionString);
//            bool ok = true;

//            string drop = "DROP TABLE IF EXISTS {0};";
//            string dropProc = "DROP PROCEDURE IF EXISTS {0};";

//            if (ok) ok = ok && NetCms.Connections.ConnectionsManager.CommonMySqlConnection.Execute("DELETE FROM users WHERE AnagraficaType_User LIKE '{0}'".FormatByParameters(GetFullClassName().FilterForDB()));
//            if (ok) ok = ok && NetCms.Connections.ConnectionsManager.CommonMySqlConnection.Execute("DELETE FROM custom_anagrafe WHERE classname LIKE '{0}'".FormatByParameters(GetFullClassName().FilterForDB()));
//            if (ok) ok = ok && conn.Execute(drop.FormatByParameters(this.DatabaseTableName));
//            if (ok) ok = ok && conn.Execute(dropProc.FormatByParameters(SearchRoutineName));

//            return ok;
//        }
//        public string GetFullClassName()
//        {
//            return "CustomAnagrafe.Runtime." + ClassName;
//        }

//        public CustomAnagraficaBase GetAnagrafeInstance(int ID)
//        {
//            return NetCms.Users.Anagraphics.AnagraphicsClassesPool.BuildInstanceOf(ID.ToString(), this.GetFullClassName()) as CustomAnagraficaBase;
//        }
//    }
//}
