﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Xml;
//using System.Reflection;

//namespace Registrazione
//{
//    public class Field
//    {
//        public XmlNode NodeData { get; private set; }
//        private List<XmlNode> ChildNodes{ get; set; }

//        public string DatabaseFieldName { get; private set; }
//        public string DatabaseFieldType { get; private set; }
//        public string DetailsLabelQuery { get; private set; }
//        public string Label { get; private set; }
//        public string Placeholder { get; private set; }
//        public string PropertyName { get; private set; }
//        public Type PropertyType { get; private set; }
//        public Type ValidateFieldType { get; private set; }
//        public Type AxfFieldType { get; private set; }
//        public List<ControlToRenderPropertyDefinition> ValidateFieldCustomProperties { get; private set; }
//        public List<ControlToRenderPropertyDefinition> AxfFieldCustomProperties { get; private set; }

//        public Field(XmlNode data)
//        {
//            this.NodeData = data;
//            ChildNodes = data.ChildNodes.Cast<XmlNode>().ToList();
//            DatabaseFieldName = ChildNodes.First(node => node.Name == "databaseFieldData").Attributes["name"].Value;
//            DatabaseFieldType = ChildNodes.First(node => node.Name == "databaseFieldData").Attributes["type"].Value;

//            var propertyFieldData = ChildNodes.First(node => node.Name == "propertyFieldData");

//            var detailsLabelQuery = propertyFieldData.ChildNodes.Cast<XmlNode>().FirstOrDefault(node => node.Name == "detailsLabelQuery");
//            if (detailsLabelQuery != null)
//            {
//                this.DetailsLabelQuery = detailsLabelQuery.Attributes["select"].Value;
//            }
//            Label = data.Attributes["label"].Value;
//            Placeholder = data.Attributes["placeholder"].Value;
//            PropertyName = propertyFieldData.Attributes["name"].Value;
//            PropertyType = Type.GetType(propertyFieldData.Attributes["propertyType"].Value);

//            var validatedField = propertyFieldData.ChildNodes.Cast<XmlNode>().First(node => node.Name == "validatedField");
//            ValidateFieldType = GetTypeFromAllAssemblies(validatedField.Attributes["type"].Value);
//            ValidateFieldCustomProperties = new List<ControlToRenderPropertyDefinition>();

//            foreach (XmlNode prop in validatedField.ChildNodes.Cast<XmlNode>().Where(x => x.Name == "property"))
//            {
//                ValidateFieldCustomProperties.Add(new ControlToRenderPropertyDefinition(
//                     prop.Attributes["name"].Value,
//                     prop.Attributes["value"].Value)
//                    );
//            }

//            var axfField = propertyFieldData.ChildNodes.Cast<XmlNode>().First(node => node.Name == "axfField");
//            AxfFieldType = GetTypeFromAllAssemblies(axfField.Attributes["type"].Value);
//            AxfFieldCustomProperties = new List<ControlToRenderPropertyDefinition>();

//            foreach (XmlNode prop in axfField.ChildNodes.Cast<XmlNode>().Where(x => x.Name == "property"))
//            {
//                AxfFieldCustomProperties.Add(new ControlToRenderPropertyDefinition(
//                     prop.Attributes["name"].Value,
//                     prop.Attributes["value"].Value)
//                    );
//            }
//        }
        
//        public Type GetTypeFromAllAssemblies(string typeFullName)
//        {
//            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
//            foreach (var assembly in assemblies)
//            {
//                var t = assembly.GetType(typeFullName);
//                if (t != null)
//                    return t;
//            }
//            return null;
//        }
//        public XmlNodeList GetNodes(string path)
//        {
//            return NodeData.SelectNodes(path);
//        }
//    }
//    public class ControlToRenderPropertyDefinition
//    {
//        public string Name { get; private set; }
//        public string Value { get; private set; }
//        public ControlToRenderPropertyDefinition(string name, string value)
//        {
//            this.Name = name;
//            this.Value = value;
//        }
//    }
//}
