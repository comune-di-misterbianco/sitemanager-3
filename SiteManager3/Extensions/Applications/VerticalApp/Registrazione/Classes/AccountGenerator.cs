﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using NetCms.Users.CustomAnagrafe;
//using System.Web.Security;
//using NetCms.Users;

//namespace Registrazione
//{
//    internal class AccountGenerator
//    {
//        private CustomAnagrafeConfiguration config;
//        private CustomAnagraficaBase anagrafe;

//        public User LastGeneratedUser { get; private set; }
//        public string LastGeneratedUserPassword { get; private set; }

//        public NetCms.Connections.Connection CmsConnection
//        {
//            get
//            {
//                return _CmsConnection ?? (_CmsConnection = NetCms.Connections.ConnectionsManager.CommonConnection);
//            }
//        }
//        private NetCms.Connections.Connection _CmsConnection;

//        public NetCms.Connections.Connection LocalConnection
//        {
//            get
//            {
//                return _LocalConnection ?? (_LocalConnection = config.Connection);
//            }
//        }
//        private NetCms.Connections.Connection _LocalConnection;

//        public AccountGenerator(CustomAnagrafeConfiguration config, CustomAnagraficaBase anagrafe)
//        {
//            this.config = config;
//            this.anagrafe = anagrafe;
//        }

//        private string BuildPassword()
//        {
//            string password = "";

//            string PASSWORD_CHARS_LCASE = "abcdefgijkmnopqrstwxyz";
//            string PASSWORD_CHARS_UCASE = "ABCDEFGHJKLMNPQRSTWXYZ";
//            string PASSWORD_CHARS_NUMERIC = "0123456789";
//            string PASSWORD_CHARS_SPECIAL = "*$-+?_&=!%[]/";
//            RandomNumbers ran = new RandomNumbers();
//            int offset = ran.Next(0, 4);
//            for (int i = offset; i < NetCms.Configurations.Generics.MinimumPasswordLength + offset; i++)
//            {
//                Random Random = new Random(DateTime.Now.Millisecond);
//                switch (i % 4)
//                {
//                    case 0: password += PASSWORD_CHARS_LCASE[ran.Next(0, PASSWORD_CHARS_LCASE.Length - 1)]; break;
//                    case 1: password += PASSWORD_CHARS_UCASE[ran.Next(0, PASSWORD_CHARS_UCASE.Length - 1)]; break;
//                    case 2: password += PASSWORD_CHARS_NUMERIC[ran.Next(0, PASSWORD_CHARS_NUMERIC.Length - 1)]; break;
//                    case 3: password += PASSWORD_CHARS_SPECIAL[ran.Next(0, PASSWORD_CHARS_SPECIAL.Length - 1)]; break;
//                }
//            }

//            return password;
//        }
//        public bool CreateAccount()
//        {
//            string insertProfile = @"
//            INSERT INTO profiles (Name_Profile, Type_Profile) VALUES ('" + anagrafe.Username + anagrafe.ID + @"',0)";
//            int profileID = CmsConnection.ExecuteInsert(insertProfile);

//            LastGeneratedUserPassword = BuildPassword();
//            string newPasswordMD5 = FormsAuthentication.HashPasswordForStoringInConfigFile(LastGeneratedUserPassword, "MD5").ToString();
//            string insert = @"
//            INSERT INTO Users (
//                                    Name_User,
//                                    Anagrafica_User,
//                                    Profile_User,
//                                    Password_User,
//                                    Type_User,
//                                    AnagraficaType_User,
//                                    Key_User,
//                                    NewPassword_User,
//                                    CreationTime_User,
//                                    LastAccess_User,
//                                    FoldersGUI_User,
//                                    State_User,
//                                    Deleted_User,
//                                    Editor_User
//                               )
//                               VALUES(
//                                    '{0}',
//                                    {1},
//                                    {2},
//                                    '{3}',
//                                    {4},
//                                    '{5}',
//                                    '{6}',
//                                    '{7}',
//                                    {8},
//                                    {9},
//                                    1,
//                                    2,
//                                    0,
//                                    0
//                                    )
//";
//            string Name_User = anagrafe.Username;
//            string Anagrafica_User = this.anagrafe.ID;
//            string Profile_User = profileID.ToString();
//            string Password_User = newPasswordMD5;
//            string Type_User = "5";
//            string AnagraficaType_User = this.config.GetFullClassName();
//            string Key_User = new Random().Next(9999999).ToString(); ;
//            string NewPassword_User = "";
//            string CreationTime_User = CmsConnection.FilterDateTime(DateTime.Now);
//            string LastAccess_User = CmsConnection.FilterDateTime(DateTime.MinValue);

//            string[] parameters = { Name_User, Anagrafica_User, Profile_User, Password_User, Type_User, AnagraficaType_User, Key_User, NewPassword_User, CreationTime_User, LastAccess_User };

//            string sql = string.Format(insert, parameters);
//            int uid = CmsConnection.ExecuteInsert(sql);
//            HandleUserGroups(uid);

//            //LastGeneratedUser = new User(uid);

//            LastGeneratedUser = UsersBusinessLogic.GetById(uid);

//            return LastGeneratedUser != null;
//            //SendActivationMail(newPassword, Key_User);
//        }

//        public void HandleUserGroups(int uid)
//        {
//            User user = UsersBusinessLogic.GetById(uid);

//            foreach (var grouptoAddDefinition in config.GroupsToAssociate)
//            {
//                NetCms.Users.Group group = NetCms.Users.GroupsHandler.RootGroup.FindGroupByKey(grouptoAddDefinition.SystemName);
//                if (group != null) group.AddUserToGroup(user, grouptoAddDefinition.GrantAdministrationType, grouptoAddDefinition.ContentAdministrationType);
//            }
//        }
//    }
//}
