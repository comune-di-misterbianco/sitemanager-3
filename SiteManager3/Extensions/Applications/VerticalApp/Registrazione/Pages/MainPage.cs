﻿//using System;
//using System.Data;
//using System.Linq;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.HtmlControls;
//using NetCms.GUI;
//using NetService.Utility.ArTable;
//using NetService.Utility.RecordsFinder;
//using System.IO;

//namespace NetCms.Users.CustomAnagrafe
//{
//    [DynamicUrl.PageControl("default.aspx", ApplicationID = Application.ApplicationID)]
//    public class MainPage : AbstractPage
//    {        
//        public override string PageTitle
//        {
//            get
//            {
//                return base.PageTitle + "Pagina Iniziale";
//            }
//        }

//        public MainPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
//            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
//        {
//            if (isPostBack)
//                CheckInstall();
//        }


//        public override WebControl Control
//        {
//            get { return GetControl(); }
//        }
//        protected WebControl GetControl()
//        {
//            WebControl div = new WebControl(HtmlTextWriterTag.Div);
//            div.Controls.Add(InstallField.Control);
//            div.Controls.Add(RemoveField.Control);
//            div.Controls.Add(List());
//            return div;
//        }

//        public static DynamicUrl.RedirectAddress GetPageAddress()
//        {
//            return new DynamicUrl.RedirectAddress(NetCms.Vertical.PagesHandler.GetPageAddress(typeof(MainPage)));
//        }

//        public G2Core.XhtmlControls.InputField InstallField
//        {
//            get
//            {
//                if (_InstallField == null)
//                {
//                    _InstallField = new G2Core.XhtmlControls.InputField("install");
//                }
//                return _InstallField;
//            }
//        }
//        private G2Core.XhtmlControls.InputField _InstallField;

//        public G2Core.XhtmlControls.InputField RemoveField
//        {
//            get
//            {
//                if (_RemoveField == null)
//                {
//                    _RemoveField = new G2Core.XhtmlControls.InputField("remove");
//                }
//                return _RemoveField;
//            }
//        }
//        private G2Core.XhtmlControls.InputField _RemoveField;

//        protected WebControl List()
//        {
//            FileSystemFinder finder = new FileSystemFinder(NetCms.Configurations.Paths.AbsoluteConfigRoot + "\\custom-anagrafe\\");
//            //ATTENZIONE: Verificare il funzionamento
//            ArTable table = new ArTable(finder.Find())
//            //ArTableExtended table = new ArTableExtended(finder, GetPageAddress().Address)
//            {
//                InnerTableCssClass = "tab"
//            };
//            table.AddColumn(new CustomAnagrafeXmlDataColumn("Nome", "Label"));
//            table.AddColumn(new CustomAnagrafeXmlDataColumn("File Name", "XmlPhysicalPath"));
//            table.AddColumn(new CustomAnagrafeXmlDataColumn("Stato", "InstallStatusLabel"));
//            table.AddColumn(new InstallActionColumn(this.InstallField, this.RemoveField));

//            return table;
//        }

//        public void CheckInstall()
//        {
//            if (InstallField.RequestVariable.IsPostBack)
//            {
//                string path = InstallField.RequestVariable.StringValue.Replace("#", "\\");
//                if (File.Exists(path))
//                {
//                    Registrazione.CustomAnagrafeConfiguration data = new Registrazione.CustomAnagrafeConfiguration(path);
//                    data.Install();
//                }
//            }
//            if (RemoveField.RequestVariable.IsPostBack)
//            {
//                string className = RemoveField.RequestVariable.StringValue;
//                if (CustomAnagrafeContext.DataCollection.ContainsKey(className))
//                {
//                    Registrazione.CustomAnagrafeConfiguration data = CustomAnagrafeContext.DataCollection[className];
//                    data.Uninstall();
//                }
//            }
//        }
//    }
//}
