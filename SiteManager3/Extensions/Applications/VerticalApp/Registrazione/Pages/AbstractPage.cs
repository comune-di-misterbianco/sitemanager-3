﻿//using System;
//using System.Data;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.HtmlControls;
//using NetCms.GUI;
//using System.Collections.Generic;

//namespace NetCms.Users.CustomAnagrafe
//{
//    public abstract class AbstractPage : NetCms.Vertical.ApplicationPageEx
//    {
//        public override NetCms.GUI.Icons.Icons PageIcon
//        {
//            get { return NetCms.GUI.Icons.Icons.Newspaper; }
//        }

//        public NetCms.Users.GrantElement GrantsManager
//        {
//            get
//            {
//                if (_GrantsManager == null)
//                    _GrantsManager = new NetCms.Users.GrantElement(Application.ApplicationID.ToString(), NetCms.Users.Profile.CurrentProfile);
//                return _GrantsManager;
//            }
//        }
//        private NetCms.Users.GrantElement _GrantsManager;

//        public NetCms.Users.User Account
//        {
//            get
//            {
//                return _Account ?? (_Account = NetCms.Users.AccountManager.CurrentAccount);
//            }
//        }
//        private NetCms.Users.User _Account;

//        public static bool CurrentPageIsPostBack
//        {
//            get
//            {
//                return (HttpContext.Current != null &&
//                       HttpContext.Current.Items != null &&
//                       HttpContext.Current.Items["Newsletter.Page.IsPostback"] != null &&
//                       HttpContext.Current.Items["Newsletter.Page.IsPostback"] is bool) ?
//                       (bool)HttpContext.Current.Items["Newsletter.Page.IsPostback"] : false;
//            }
//        }
        
//        private NetCms.Connections.Connection _Connection;
//        public NetCms.Connections.Connection Connection
//        {
//            get
//            {
//                if (_Connection == null)
//                    _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
//                return _Connection;
//            }
//        }

//        private string _PageTitle;
//        public override string PageTitle
//        {
//            get { return "Anagrafe Custom &raquo; "; }
//        }

//        public AbstractPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
//            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
//        {
//            ApplicationContext.SetCurrentComponent(Application.ApplicationContextBackLabel);
//            if (!GrantsManager.Grant)
//                PopupBox.AddNoGrantsMessageAndGoBack();
//        }
//    }
//}
