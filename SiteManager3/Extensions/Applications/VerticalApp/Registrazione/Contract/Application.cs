﻿//using System;
//using System.Collections.Generic;
//using System.Reflection;
//using System.Linq;
//using System.Text;
//using NetCms.Vertical;
//using NetCms.Front.Static;

//namespace NetCms.Users.CustomAnagrafe
//{
//    public class Application : VerticalApplication
//    {
//        public const string ApplicationContextBackLabel = "CustomAnagrafe.Backoffice";
//        public const string ApplicationContextFrontLabel = "CustomAnagrafe.Frontend";
//        public const int ApplicationID = 1983;
//        public const string ApplicationSystemName = "customanagrafe";

//        public override int HomepageGroup
//        {
//            get { return NetCms.GUI.Startpage.Startpage.ConfigsHomeGroupID; }
//        }

//        public override VerticalApplication.AccessModes AccessMode
//        {
//            get { return AccessModes.Admin; }
//        }
//        public override string Description
//        {
//            get { return "Anagrafe Custom"; }
//        }

//        public override int ID
//        {
//            get { return ApplicationID; }
//        }

//        public override bool IsSystemApplication
//        {
//            get
//            {
//                return false;
//            }
//        }

//        public override bool CheckForDefaultGrant
//        {
//            get { return true; }
//        }

//        public override string Label
//        {
//            get { return "Anagrafe Custom"; }
//        }

//        public override bool NetworkDependant
//        {
//            get { return false; }
//        }

//        public override int SecurityLevel
//        {
//            get { return 0; }
//        }

//        public override string SystemName
//        {
//            get { return ApplicationSystemName; }
//        }

//        public override bool DoAdditionalConfigurationForExternalDB()
//        {
//            throw new NotImplementedException();
//        }

//        public override bool NeedAdditionalConfiguration
//        {
//            get { return false; }
//        }

//        public override VerticalApplicationSetup Installer
//        {
//            get
//            {
//                return new Setup(this);
//            }
//        }

//        public Application(Assembly assembly)
//            : base(assembly)
//        { 
//        }

//        public override bool UseCmsSessionFactory
//        {
//            get { return false; }
//        }
//    }
//}
