//using System;
//using System.Data;
//using System.Linq;
//using System.Reflection;
//using System.Reflection.Emit;
//using Registrazione;
//using NetCms.Front.Static;
//using System.Collections.Generic;

///// <summary>
///// Summary description for StructureGrants
///// </summary>

//namespace NetCms.Users.CustomAnagrafe
//{
//    public static class Configurator
//    {
//        public static string RepositoryPath
//        {
//            get
//            {
//                if (_RepositoryPath == null)
//                {
//                    _RepositoryPath = NetCms.Configurations.Paths.AbsoluteConfigRoot + "\\custom-anagrafe\\";
//                }
//                return _RepositoryPath;
//            }
//        }
//        private static string _RepositoryPath;
        
//        public static void Configure()
//        {
//            var conn =  NetCms.Connections.ConnectionsManager.CommonMySqlConnection;
//            DataTable installedAnagrafe = conn.SqlQuery("SELECT * FROM custom_anagrafe");
//            if (installedAnagrafe != null)
//            {
//                var xmlConfigurations = System.IO.Directory.GetFiles(RepositoryPath).Select(x => new CustomAnagrafeConfiguration(x));

//                var configs = (from row in installedAnagrafe.Rows.Cast<DataRow>()
//                               where xmlConfigurations.Any(x => x.GetFullClassName() == row["classname"].ToString())
//                               select xmlConfigurations.First(x => x.GetFullClassName() == row["classname"].ToString())).ToList();

//                configs.ToList().ForEach(config => CustomAnagrafeContext.AddConfigurationToPool(config));

//                NetCms.Users.Anagraphics.AnagraphicsClassesPool.Switches.Add(new CustomAnagrafeSwitch());
//                ConfigureFrontPages(configs);
//            }
//        }

//        private static void ConfigureFrontPages(List<CustomAnagrafeConfiguration> configs)
//        {
//            configs.ForEach(x=>
//            {
//                AttributeTypeCouple couple = new AttributeTypeCouple(new FrontendStaticPage(x.FrontendPageAddress, x.FrontendPageTitle), typeof(RegistrazioneFrontPage));
//                StaticPagesCollector.ClassesKeysAssociations.Add(couple.Attribute.Address, couple);
//            });
//        }
//        public static void ConfigureFrontPage(CustomAnagrafeConfiguration config)
//        {
//            AttributeTypeCouple couple = new AttributeTypeCouple(new FrontendStaticPage(config.FrontendPageAddress, config.FrontendPageTitle), typeof(RegistrazioneFrontPage));
//            StaticPagesCollector.ClassesKeysAssociations.Add(couple.Attribute.Address, couple);
//        }
//    }
//}