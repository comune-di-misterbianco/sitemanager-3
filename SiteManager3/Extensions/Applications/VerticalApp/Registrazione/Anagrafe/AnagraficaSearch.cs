//using System;
//using System.Data;
//using System.Data.Common;
//using System.Configuration;
//using System.Web;
//using System.Collections.Generic;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using NetCms.Connections;
//using NetCms.Users.Search;

///// <summary>
///// Summary description for StructureGrants
///// </summary>

//namespace NetCms.Users.CustomAnagrafe
//{

//    public abstract class CustomAnagraficaSearch : NetCms.Users.AbstractAnagraficaSearch
//    {
//        public override string NamespaceKeyOnUsersTable { get { return namespaceKeyOnUsersTable; } }
//        public override string PrimaryKeyField { get { return primaryKeyField; } }

//        protected override Type AnagraficaType
//        {
//            get { return anagraficaType; }
//        }
//        protected override string ProcudureName
//        {
//            get { return procudureName; }
//        }

//        protected override NetCms.Connections.Connection InnerConnection
//        {
//            get
//            {
//                return innerConnection;
//            }
//        }
//        protected override NetCms.Connections.Connection OuterConnection
//        {
//            get
//            {
//                return outerConnection;
//            }
//        }

//        protected NetCms.Connections.Connection outerConnection;
//        protected NetCms.Connections.Connection innerConnection;
//        protected string procudureName;
//        protected Type anagraficaType;
//        protected string namespaceKeyOnUsersTable;
//        protected string primaryKeyField;
//        protected Registrazione.CustomAnagrafeConfiguration config;

//        public CustomAnagraficaSearch()
//        {
//            outerConnection = NetCms.Connections.ConnectionsManager.CommonConnection;
//            FindAssociatedAnagrafe();
//            if (anagraficaType != null && config != null)
//            {
//                procudureName = config.SearchRoutineName;
//                namespaceKeyOnUsersTable = anagraficaType.FullName;
//                primaryKeyField = "ID";
//                innerConnection = config.Connection;
//            }
//            else
//            {
//                throw new InvalidOperationException(this.GetType().Name + " was unable to find associated Anagrafica.");
//            }
//        }

//        protected void FindAssociatedAnagrafe()
//        {
//            string name = this.GetType().FullName;
//            name = name.Remove(name.Length - 6);

//            anagraficaType = CustomAnagrafeContext.AnagrafeTypes[name];
//            config = CustomAnagrafeContext.DataCollection[name];
//        }
//    }
//}