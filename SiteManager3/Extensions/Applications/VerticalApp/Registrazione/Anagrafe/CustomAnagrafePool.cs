﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using System.Reflection.Emit;
//using NetCms.Users.Anagraphics;
//using Registrazione;


///// <summary>
///// Summary description for StructureGrants
///// </summary>

//namespace NetCms.Users.CustomAnagrafe
//{
//    public static class CustomAnagrafeContext
//    {
//        private const string AnagraphicsClassesKey = "NetCms.Users.CustomAnagrafeXMLPool";

//        public static Dictionary<string, CustomAnagrafeConfiguration> DataCollection { get; private set; }
//        public static Dictionary<string, Type> AnagrafeTypes { get; private set; }

//        static CustomAnagrafeContext()
//        {
//            AnagrafeTypes = new Dictionary<string, Type>();
//            DataCollection = new Dictionary<string, CustomAnagrafeConfiguration>();
//        }

//        private static Type BuildAnagrafeClass(CustomAnagrafeConfiguration data, ModuleBuilder moduleBuilder)
//        {
//            // create a type in the module
//            TypeBuilder typeBuilder = moduleBuilder.DefineType(data.GetFullClassName(), TypeAttributes.Public, typeof(CustomAnagraficaBase));

//            //creo un array con i tipi di dato che prendono in input sia il costruttore della classe base, sia il costruttore della classe che stiamo generando
//            Type[] constructorArgs = { typeof(string) };

//            // Define the default constructor.
//            ConstructorBuilder myDefaultConstructorBuilder = typeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard,System.Type.EmptyTypes);

//            // Define the constructor.
//            ConstructorBuilder myConstructorBuilder = typeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, constructorArgs);

//            //Default constructor
//            ILGenerator defaultConstructorIL = myDefaultConstructorBuilder.GetILGenerator();
//            defaultConstructorIL.Emit(OpCodes.Ldarg_0);//Metto sullo stack il riferimento a this
//            ConstructorInfo defaultSuperConstructor = typeof(CustomAnagraficaBase).GetConstructor(System.Type.EmptyTypes); //Dal type della classe base ottengo il riferimento al supercotruttore
//            defaultConstructorIL.Emit(OpCodes.Call, defaultSuperConstructor);//Invoco il supercotruttore
//            defaultConstructorIL.Emit(OpCodes.Ret);//Invoco il return sul costruttore
//            //End Default constructor

//            ILGenerator constructorIL = myConstructorBuilder.GetILGenerator();

//            constructorIL.Emit(OpCodes.Ldarg_0);//Metto sullo stack il riferimento a this
//            constructorIL.Emit(OpCodes.Ldarg_1);//Metto sullo stack primo paramentro (ed unico) del costruttore, al fine di passarlo al supercostruttore
//            ConstructorInfo superConstructor = typeof(CustomAnagraficaBase).GetConstructor(constructorArgs); //Dal type della classe base ottengo il riferimento al supercotruttore
//            constructorIL.Emit(OpCodes.Call, superConstructor);//Invoco il supercotruttore
//            constructorIL.Emit(OpCodes.Ret);//Invoco il return sul costruttore

//            return typeBuilder.CreateType();
//        }
//        private static Type BuildAnagrafeSearchClass(CustomAnagrafeConfiguration data, ModuleBuilder moduleBuilder)
//        {
//            // create a type in the module
//            TypeBuilder typeBuilder = moduleBuilder.DefineType(data.GetFullClassName()+"Search", TypeAttributes.Public, typeof(CustomAnagraficaSearch));


//            //creo un array con i tipi di dato che prendono in input sia il costruttore della classe base, sia il costruttore della classe che stiamo generando
//            Type[] constructorArgs = Type.EmptyTypes;

//            // Define the constructor.
//            ConstructorBuilder myConstructorBuilder = typeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, constructorArgs);

//            ILGenerator constructorIL = myConstructorBuilder.GetILGenerator();

//            constructorIL.Emit(OpCodes.Ldarg_0);//Metto sullo stack il riferimento a this
//            ConstructorInfo superConstructor = typeof(CustomAnagraficaSearch).GetConstructor(constructorArgs); //Dal type della classe base ottengo il riferimento al supercotruttore
//            constructorIL.Emit(OpCodes.Call, superConstructor);//Invoco il supercotruttore
//            constructorIL.Emit(OpCodes.Ret);//Invoco il return sul costruttore

//            return typeBuilder.CreateType();
//        }

//        public static void AddConfigurationToPool(CustomAnagrafeConfiguration data)
//        {
//            DataCollection.Add(data.GetFullClassName(), data);

//            AssemblyName assemblyName = new AssemblyName();
//            assemblyName.Name = "CustomAnagrafe.Runtime";

//            AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);

//            // create a module in the assembly
//            ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("CustomAnagrafe.Runtime");

//            Type newAnagraficaType = BuildAnagrafeClass(data, moduleBuilder);
//            AnagrafeTypes.Add(data.GetFullClassName(), newAnagraficaType);

//            Type newAnagraficaSearchType = BuildAnagrafeSearchClass(data, moduleBuilder);
//            Anagraphics.AnagraphicsClassesPool.AnagraficheSearchs.Add(newAnagraficaSearchType.FullName, newAnagraficaSearchType);
//        }
//        public static void RemoveConfigurationFromPool(string key)
//        {
//            DataCollection.Remove(key);
//            NetCms.GUI.GuiUtility.RestartApplication();
//        }
//        public static CustomAnagrafeConfiguration CurrentCustomAnagrafe
//        {
//            get
//            {
//                if (System.Web.HttpContext.Current != null &&
//                    System.Web.HttpContext.Current.Request != null)
//                {
//                    string referenceAddress = System.Web.HttpContext.Current.Request.Url.PathAndQuery.ToLower().Split('?').First();
//                    return (from config in DataCollection.Values.ToList()
//                            where referenceAddress.EndsWith(config.FrontendPageAddress.ToLower())
//                            select config).FirstOrDefault();
//                }

//                return null;
//            }
//        }
//    }

//    public class CustomAnagrafeSwitch : IAnagraficaSwitch
//    {
//        public bool ContainsAnagrafica(string anagraficaTypeName)
//        {
//            return CustomAnagrafeContext.AnagrafeTypes.ContainsKey(anagraficaTypeName);
//        }
//        public Type GetAnagrafica(string anagraficaTypeName)
//        {
//            return CustomAnagrafeContext.AnagrafeTypes[anagraficaTypeName];
//        }
//    }

//}