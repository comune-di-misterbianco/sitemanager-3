//using System;
//using System.Data;
//using System.Data.Common;
//using System.Configuration;
//using System.Linq;
//using System.Web;
//using System.Collections.Generic;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
//using NetCms.Connections;
//using G2Core.AdvancedXhtmlForms;
//using G2Core.AdvancedXhtmlForms.Fields;
//using G2Core.AdvancedXhtmlForms.Fields.Reference;
//using NetCms.Users.Search;
//using NetForms;
//using NetService.Utility.ValidatedFields;
//using Registrazione;
//using System.Xml;

///// <summary>
///// Summary description for StructureGrants
///// </summary>

//namespace NetCms.Users.CustomAnagrafe
//{
//    public abstract class CustomAnagraficaBase : AnagraficaBase
//    {
//        public const string FieldKeyNomeCompleto = "nomeCompleto";
//        public const string FieldKeyIndirizzo = "indirizzo";
//        public const string FieldKeyTelefono = "telefono";
//        public const string FieldKeyCap = "cap";
//        public const string FieldKeyEmail = "email";
//        public const string FieldKeyCodice = "codice";
//        public const string FieldKeyCitt� = "citta";
//        public const string FieldKeyUsername = "username";

//        private string _NomeCompleto;
//        public override string NomeCompleto
//        {
//            get
//            {
//                return _NomeCompleto ?? (_NomeCompleto =  GetMandatoryValueFromTemplate(FieldKeyNomeCompleto));
//            }
//        }

//        public override string Citt�
//        {
//            get
//            {
//                if (_Citt� == null)
//                {
//                    return GetMandatoryValueFromTemplate(FieldKeyCitt�);
//                }
//                return _Citt�;
//            }
//        }
//        private string _Citt�;

//        public override string Indirizzo
//        {
//            get
//            {
//                if (_Indirizzo == null)
//                {
//                    return GetMandatoryValueFromTemplate(FieldKeyIndirizzo);
//                }
//                return _Indirizzo;
//            }
//        }
//        private string _Indirizzo;

//        public override string Telefono
//        {
//            get
//            {
//                if (_Telefono == null)
//                {
//                    return GetMandatoryValueFromTemplate(FieldKeyTelefono);
//                }
//                return _Telefono;
//            }
//        }
//        private string _Telefono;

//        public override string CAP
//        {
//            get
//            {
//                if (_CAP == null)
//                {
//                    return GetMandatoryValueFromTemplate(FieldKeyCap);
//                }
//                return _CAP;
//            }
//        }
//        private string _CAP;

//        private string _Codice;
//        public override string Codice
//        {
//            get
//            {
//                if (_Codice == null)
//                {
//                    return GetMandatoryValueFromTemplate(FieldKeyCodice);
//                }
//                return _Codice;
//            }
//        }

//        public override string Email
//        {
//            get
//            {
//                if (_Email == null)
//                {
//                    return GetMandatoryValueFromTemplate(FieldKeyEmail);
//                }
//                return _Email;
//            }
//        }
//        private string _Email;

//        public string Username
//        {
//            get
//            {
//                if (_Username == null)
//                {
//                    return GetMandatoryValueFromTemplate(FieldKeyUsername);
//                }
//                return _Username;
//            }
//        }
//        private string _Username;

//        protected string LabelNomeCompleto
//        {
//            get
//            {
//                return CustomRegistrationData.MandatoryFieldsInfo.First(f => f.Key == FieldKeyNomeCompleto).Label;
//            }
//        }
//        protected string LabelCap
//        {
//            get
//            {
//                return CustomRegistrationData.MandatoryFieldsInfo.First(f => f.Key == FieldKeyCap).Label;
//            }
//        }
//        protected string LabelTelefono
//        {
//            get
//            {
//                return CustomRegistrationData.MandatoryFieldsInfo.First(f => f.Key == FieldKeyTelefono).Label;
//            }
//        }
//        protected string LabelEmail
//        {
//            get
//            {
//                return CustomRegistrationData.MandatoryFieldsInfo.First(f => f.Key == FieldKeyEmail).Label;
//            }
//        }
//        protected string LabelCodice
//        {
//            get
//            {
//                return CustomRegistrationData.MandatoryFieldsInfo.First(f => f.Key == FieldKeyCodice).Label;
//            }
//        }
//        protected string LabelCitt�
//        {
//            get
//            {
//                return CustomRegistrationData.MandatoryFieldsInfo.First(f => f.Key == FieldKeyCitt�).Label;
//            }
//        }
//        protected string LabelIndirizzo
//        {
//            get
//            {
//                return CustomRegistrationData.MandatoryFieldsInfo.First(f => f.Key == FieldKeyIndirizzo).Label;
//            }
//        }

//        public override bool? NonLegalCodeDeclared
//        {
//            get { return false; }
//        }

//        private NetCms.Connections.Connection _Connection;
//        private NetCms.Connections.Connection Conn
//        {
//            get
//            {
//                if (_Connection == null)
//                {
//                    _Connection = new MySqlConnection(CustomRegistrationData.ConnectionString) { CacheEnabled = false };
//                }
//                return _Connection;
//            }
//        }

//        public override bool ValidateCF(AxfGenericTable anagrafeTable)
//        {
//            return true;
//        }

//        public DataRow Comune
//        {
//            get
//            {
//                if (_Comune == null)
//                {
//                    _Comune = Conn.SqlQuery("SELECT * FROM comuni WHERE IDComune = {0}".FormatByParameters(Data["IDComune"])).Rows[0];
//                }
//                return _Comune;
//            }
//        }
//        private DataRow _Comune;

//        public override System.Data.DataRow Data
//        {
//            get
//            {
//                return _Data;
//            }
//        }
//        private System.Data.DataRow _Data;

//        public override int ImportAnagrafica(DataRow datiAnagrafica)
//        {
//            List<string> values = new List<string>();
//            List<string> fields = new List<string>();

//            for (int i = 1; i < datiAnagrafica.Table.Columns.Count; i++)
//            {
//                if (datiAnagrafica[i].ToString().Length>0)
//                    values.Add("'" + datiAnagrafica[i].ToString().Replace("'","''") + "'");
//                else
//                    values.Add("NULL");
//                fields.Add(datiAnagrafica.Table.Columns[i].ColumnName);
//            }
//            string sql = "INSERT INTO {0} ({1}) VALUES({2})";

//            sql = sql.FormatByParameters(
//                CustomRegistrationData.DatabaseTableName,
//                string.Join(",", fields),
//                string.Join(",", values)
//                );

//            return Conn.ExecuteInsert(sql);
//        }

//        public override bool Exist
//        {
//            get { return Data != null; }
//        }

//        public Registrazione.CustomAnagrafeConfiguration CustomRegistrationData
//        {
//            get
//            {
//                if (_CustomRegistrationData == null)
//                {
//                    _CustomRegistrationData = CustomAnagrafeContext.DataCollection[this.GetType().FullName];
//                }
//                return _CustomRegistrationData;
//            }
//        }
//        private Registrazione.CustomAnagrafeConfiguration _CustomRegistrationData;

//        public CustomAnagraficaBase()
//            : base()
//        { }

//        public CustomAnagraficaBase(string id)
//            : base(id)
//        {
//            _Data = Conn.SqlQuery("SELECT * FROM {0} WHERE ID = {1}".FormatByParameters(CustomRegistrationData.DatabaseTableName, id)).Rows[0];
//        }

//        private List<NetCms.Users.AnagraficaDetail> _DetailsCollection;
//        public override List<NetCms.Users.AnagraficaDetail> Details
//        {
//            get
//            {
//                if (_DetailsCollection == null)
//                {
//                    _DetailsCollection = new List<NetCms.Users.AnagraficaDetail>();

//                    foreach (var p in this.CustomRegistrationData.CustomFields)
//                    {
//                        string value = Data[p.DatabaseFieldName].ToString();
//                        if (p.DetailsLabelQuery.HasContent())
//                        {
//                            try
//                            {
//                                value = Conn.SqlQuery(p.DetailsLabelQuery.FormatByParameters(value)).Rows[0][0].ToString();
//                            }
//                            catch { }
//                        }
//                        _DetailsCollection.Add(new AnagraficaDetail(p.Label, value));
//                    }
//                }

//                return _DetailsCollection;
//            }
//        }

//        public override WebControl GetProfileDetails(StateBag _viewstate, bool _IsPostBack)
//        {
//            WebControl div = new WebControl(HtmlTextWriterTag.Div);
//            div.CssClass = "Details";

//            WebControl field = new WebControl(HtmlTextWriterTag.Fieldset);
//            field.CssClass = "NetCms_profile";

//            HtmlGenericControl legend = new HtmlGenericControl("legend");
//            legend.InnerHtml = "Dettaglio profilo";
//            field.Controls.Add(legend);

//            HtmlGenericControl user_detail = new HtmlGenericControl("table");
//            user_detail.Attributes["class"] = "DetailsContainer";
//            bool alternate = false;
//            foreach (NetCms.Users.AnagraficaDetail Detail in this.Details)
//            {
//                HtmlGenericControl row_detail = new HtmlGenericControl("tr");
//                if (!alternate)
//                    row_detail.Attributes["class"] = " DetailsRow DetailsRowNormal";
//                else
//                    row_detail.Attributes["class"] = " DetailsRow DetailsRowAlternate";

//                HtmlGenericControl row_label = new HtmlGenericControl("td");
//                row_label.Attributes["class"] = "DetailsLabel";
//                row_label.InnerHtml = "<strong>" + Detail.Title + ": " + "</strong>";

//                HtmlGenericControl row_value = new HtmlGenericControl("td");
//                row_value.InnerHtml = Detail.Value;

//                row_detail.Controls.Add(row_label);
//                row_detail.Controls.Add(row_value);

//                user_detail.Controls.Add(row_detail);

//                if (!alternate)
//                    alternate = true;
//                else
//                    alternate = false;
//            }

//            field.Controls.Add(user_detail);

//            div.Controls.Add(field);
//            return div;
//        }

//        public override WebControl GetProfileForm(StateBag _viewstate, bool _IsPostBack)
//        {
//            VfManager manager = new VfManager("CustomAnagraficaForm{0}".FormatByParameters(this.ID));
//            FormGenerator generator = new FormGenerator(this.CustomRegistrationData);

//            var vfields = this.CustomRegistrationData.CustomFields.Select(field => generator.BuildField(field)).ToArray();
//            manager.Fields.AddRange(vfields);

//            if (manager.IsPostBack)
//            {
//                if (manager.IsValid == VfGeneric.ValidationStates.Valid)
//                {
//                    string sql = "UPDATE {0} SET {2} WHERE ID = {1}";
//                    var fields = from f in manager.Fields 
//                                 select "{0} = '{1}'".FormatByParameters(f.Key, f.Request.StringValue.FilterForDB());

//                    bool ok = Conn.Execute(sql.FormatByParameters(
//                                                            this.CustomRegistrationData.DatabaseTableName, 
//                                                            this.ID, 
//                                                            string.Join(",", fields))
//                                                        );
//                    if (ok) return ModificaDatiResultControl();
//                }
//            }

//            return manager;
//        }

//        public WebControl ModificaDatiResultControl()
//        {
//            WebControl container = new WebControl(HtmlTextWriterTag.Div);
//            G2Core.XhtmlControls.Fieldset ok = new G2Core.XhtmlControls.Fieldset("Modifica Dati");
//            G2Core.XhtmlControls.Div content = new G2Core.XhtmlControls.Div();
//            container.Controls.Add(ok);
//            ok.Controls.Add(content);
//            content.Controls.Add(new LiteralControl("Aggiornamento effettutato con successo"));

//            return container;
//        }

//        public override AxfGenericTable GetProfileAxtTable(bool editMailFields = true)
//        {
//            AxfGenericTable anagrafica = new AxfGenericTable(CustomRegistrationData.DatabaseTableName, "ID", Conn);
//            anagrafica.RelatedRecordID = int.Parse(ID);

//            var customAxfField = this.CustomRegistrationData.CustomFields.Select(field => BuildField(field));
//            foreach (var field in customAxfField)
//            {
//                anagrafica.addField(field);
//            }

//            return anagrafica;
//        }

//        protected AxfField BuildField(Field customAnagrafeField)
//        {
//            AxfField fieldToReturn = null;
//            if (customAnagrafeField.AxfFieldType.FullName == typeof(AxfDropDownList).FullName)
//            {
//                fieldToReturn = BuildFieldDropDown(customAnagrafeField);
//            }
//            else if (typeof(AxfField).IsAssignableFrom(customAnagrafeField.AxfFieldType))
//            {
//                fieldToReturn = BuildFieldGeneric(customAnagrafeField);
//            }
//            return fieldToReturn;
//        }
//        protected AxfField BuildFieldGeneric(Field customAnagrafeField)
//        {
//            var type = customAnagrafeField.AxfFieldType;
//            var parametersTypes = new[] { typeof(string), typeof(string) };
//            var parameters = new object[] { customAnagrafeField.Label, customAnagrafeField.DatabaseFieldName };
//            var costructor = type.GetConstructor(parametersTypes);
//            AxfField fieldToReturn = costructor.Invoke(parameters) as AxfField;

//            customAnagrafeField.AxfFieldCustomProperties.ForEach(prop => 
//            {
//                SetCustomProperty(fieldToReturn, prop);
//            });

//            return fieldToReturn;
//        }
//        protected AxfField BuildFieldDropDown(Field customAnagrafeField)
//        {
//            AxfDropDownList fieldToReturn = new AxfDropDownList(customAnagrafeField.Label, customAnagrafeField.DatabaseFieldName);

//            customAnagrafeField.ValidateFieldCustomProperties.ForEach(prop =>
//            {
//                try
//                {
//                    SetCustomProperty(fieldToReturn, prop);
//                }
//                catch { }
//            });


//            var propertyFieldData = customAnagrafeField.NodeData.ChildNodes.Cast<XmlNode>().First(node => node.Name == "propertyFieldData");
//            var data = propertyFieldData.ChildNodes.Cast<XmlNode>().First(node => node.Name == "validatedField");
//            var bindData = data.ChildNodes.Cast<XmlNode>().Where(x => x.Name == "sqlDatabind").FirstOrDefault();
//            if (bindData != null)
//            {
//                string select = bindData.Attributes["select"].Value;
//                string valueColumn = bindData.Attributes["valueColumn"].Value;
//                string labelColumn = bindData.Attributes["labelColumn"].Value;
//                var connString = bindData.Attributes["conn"];
//                NetCms.Connections.Connection conn = this.CustomRegistrationData.Connection;
//                if (connString != null)
//                    conn = new G2Core.Connections.MySqlConnection(connString.Value);
//                fieldToReturn.setDataBind(select, conn);
//            }


//            return fieldToReturn;
//        }

//        protected void SetCustomProperty(AxfField vfField, ControlToRenderPropertyDefinition customProperty)
//        {
//            var prop = vfField.GetType().GetProperty(customProperty.Name);
//            object value = FormGenerator.GetDefault(prop.PropertyType);

//            if (prop.PropertyType.IsEnum)
//            {
//                value = Enum.Parse(prop.PropertyType, customProperty.Value);
//            }
//            else if (prop.PropertyType == typeof(string))
//            {
//                value = customProperty.Value;
//            }
//            else if (prop.PropertyType.IsValueType)
//            {
//                object[] args = new object[] { customProperty.Value as string };
//                Type[] types = new[] { typeof(string) };
//                var parseMethod = prop.PropertyType.GetMethod("Parse", types);
//                value = parseMethod.Invoke(null, args);
//            }

//            prop.GetSetMethod().Invoke(vfField, new[] { value });
//        }
//        public override void HardDelete()
//        {
//            this.Conn.Execute("DELETE FROM {0} WHERE ID = {1}".FormatByParameters(this.CustomRegistrationData.DatabaseTableName,this.ID));
//        }

//        public override bool UpdateEmail(string newemail)
//        {
//            string sql = "UPDATE " + this.CustomRegistrationData.DatabaseTableName + " "
//                               + " SET email = " + G2Core.Common.Utility.ValidateTextValue(newemail)
//                               + " WHERE ID = " + NetCms.Users.AccountManager.CurrentAccount.Anagrafica.ID;
//            bool result = Conn.Execute(sql);
//            return (result);
//        }

//        public string GetFieldValue(string fieldName)
//        {
//            return Data[fieldName].ToString();
//        }

//        private string GetMandatoryValueFromTemplate(string key)
//        {
//            string template = CustomRegistrationData.MandatoryFieldsInfo.First(f => f.Key == key).Template;

//            return FillStringPlaceholders(template);
//        }

//        public string FillStringPlaceholders(string text)
//        {
//            string value = text;
//            CustomAnagraficaBase anagrafe = this;
//            foreach (var field in this.CustomRegistrationData.CustomFields)
//            {
//                value = value.Replace(field.Placeholder, anagrafe.GetFieldValue(field.DatabaseFieldName));
//            }
//            return value;
//        }
//    }
//}