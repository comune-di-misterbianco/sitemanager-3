﻿//using System;
//using System.Data;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.HtmlControls;
//using NetCms.GUI;
//using System.Collections.Generic;
//using NetService.Utility.ArTable;
//using System.IO;

//namespace NetCms.Users.CustomAnagrafe
//{
//    public class InstallActionColumn : ArActionColumn
//    {
//        Registrazione.CustomAnagrafeConfiguration data;

//        public G2Core.XhtmlControls.InputField InstallInputField { get; private set; } 
//        public G2Core.XhtmlControls.InputField RemoveInputField { get; private set; }

//        public InstallActionColumn(G2Core.XhtmlControls.InputField installInputField, G2Core.XhtmlControls.InputField removeInputField)
//            : base(string.Empty, Icons.Add ,string.Empty)
//        {
//            this.InstallInputField = installInputField;
//            this.RemoveInputField = removeInputField;
//        }

//        protected override TableCell GetCellControl(object objectInstance, string label, string href, Icons icon)
//        {

//            data = new Registrazione.CustomAnagrafeConfiguration((objectInstance as FileInfo).FullName);
//            Icon = Icons.Custom;
//            if (data.IsInstalled)
//            {
//                this.CssClass = "action delete";
//                return base.GetCellControl(objectInstance, "Rimuovi", RemoveInputField.getSetFormValueConfirmJScript("'" + data.GetFullClassName() + "'", "Sei sicuro di voler disinstallare questa anagrafica, in questo modo saranno anche eliminati tutti gli utenti associati, inoltre sarà riavviato il sistema.?"), Icons.Custom);
//            }
//            else
//            {
//                this.CssClass = "action ok";
//                return base.GetCellControl(objectInstance, "Installa", InstallInputField.getSetFormValueConfirmJScript("'" + (objectInstance as FileInfo).FullName.Replace("\\", "#") + "'", "Vuoi istallare questa anagrafica?"), Icons.Custom);
//            }
//        }
//    }
//}
