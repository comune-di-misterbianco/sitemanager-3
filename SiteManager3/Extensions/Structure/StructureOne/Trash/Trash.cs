using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using NetCms.Structure.TrashBusinessLogic;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NetCms.Users;
using NetService.Utility.ArTable;
using NetService.Utility.Common;
using NetService.Utility.UI;
using NetCms.Diagnostics;
using NetCms.Networks;

/// <summary>
/// Summary description for Trash
/// </summary>
namespace NetCms
{
    namespace Structure
    {
        public class Trash
        {
            private StructureNetwork Network;
            private int SelectedLang;
            private int AccountID;

            public Trash(StructureNetwork net, int selLang, int accId)
            {
                Network = net;
                SelectedLang = selLang;
                AccountID = accId;
            }

            public bool ResumeDocRequest
            {
                get
                {
                    RequestVariable request = new RequestVariable("action", RequestVariable.RequestType.QueryString);
                    if (request.IsValidString && request.StringValue == "ResumeDoc")
                        _ResumeDocRequest = true;
                    return _ResumeDocRequest;
                }
            }
            private bool _ResumeDocRequest = false;

            public bool ResumeFolderRequest
            {
                get
                {
                    RequestVariable request = new RequestVariable("action", RequestVariable.RequestType.QueryString);
                    if (request.IsValidString && request.StringValue == "ResumeFolder")
                        _ResumeFolderRequest = true;
                    return _ResumeFolderRequest;
                }
            }
            private bool _ResumeFolderRequest = false;

            public int ContentId
            {
                get
                {
                    if (_ContentId == 0)
                    {
                        NetService.Utility.Common.RequestVariable idQS = new NetService.Utility.Common.RequestVariable("cID", NetService.Utility.Common.RequestVariable.RequestType.QueryString);
                        if (idQS.IsValidInteger)
                            _ContentId = idQS.IntValue;
                    }
                    return _ContentId;
                }
            }
            private int _ContentId = 0;


            public bool DeleteDocRequest
            {
                get
                {
                    RequestVariable request = new RequestVariable("action", RequestVariable.RequestType.QueryString);
                    if (request.IsValidString && request.StringValue == "DeleteDoc")
                        _DeleteDocRequest = true;
                    return _DeleteDocRequest;
                }
            }
            private bool _DeleteDocRequest = false;

            public bool DeleteFolderRequest
            {
                get
                {
                    RequestVariable request = new RequestVariable("action", RequestVariable.RequestType.QueryString);
                    if (request.IsValidString && request.StringValue == "DeleteFolder")
                        _DeleteFolderRequest = true;
                    return _DeleteFolderRequest;
                }
            }
            private bool _DeleteFolderRequest = false;


            public bool DeleteAllRequest
            {
                get
                {
                    RequestVariable request = new RequestVariable("action", RequestVariable.RequestType.QueryString);
                    if (request.IsValidString && request.StringValue == "Empty")
                        _DeleteAllRequest = true;
                    return _DeleteAllRequest;
                }
            }
            private bool _DeleteAllRequest = false;

            public void ResumeItems()
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                if (ResumeFolderRequest && ContentId > 0)
                {
                    if (TrashedFolds.Where(x => x.ID == ContentId).Count() > 0)
                    {
                        string message = StructureFolder.ResumeFolder(ContentId);
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha ripristinato la cartella con id=" + ContentId + " nel portale '" + Network.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        NetCms.GUI.PopupBox.AddMessage(message, PostBackMessagesType.Success);
                    }
                    else
                    {
                        string message = "Impossibile trovare la cartella. E' possibile che la cartella non esista o non si trovi nel cestino";
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "L'utente con username " + userName + " ha tentato di ripristinare la cartella con id =" + ContentId + " nel portale '" + Network.Label + "', ma essa non risulta cestinata.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        NetCms.GUI.PopupBox.AddMessage(message, PostBackMessagesType.Error);
                    }
                }

                if (ResumeDocRequest && ContentId > 0)
                {
                    if (TrashedDocuments.Where(x => x.ID == ContentId).Count() > 0)
                    {
                        string message = "";
                        bool result = false;
                        Document doc = NetCms.Structure.WebFS.FileSystemBusinessLogic.DocumentBusinessLogic.GetById(ContentId);

                        StructureFolder folder = FolderBusinessLogic.GetById(doc.Folder.ID);

                        if (folder.Trash == 0)//((doc.Folder as StructureFolder).Trash == 0)
                        {
                            //StructureFolder folder = FolderBusinessLogic.GetById(doc.Folder.ID);
                            result = folder.ResumeDoc(doc.ID);

                            if (result)
                            {
                                message = "Documento Ripristinato";
                                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha ripristinato il file con id=" + doc.ID + " nel portale '" + Network.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                                NetCms.GUI.PopupBox.AddMessage(message, PostBackMessagesType.Success);
                            }
                            else
                            {
                                message = "Si � verificato un errore e non � stato possibile ripristinare il documento";
                                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "L'utente con username " + userName + " ha riscontrato un errore durante il ripristino del file con id=" + doc.ID + " nel portale '" + Network.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                                NetCms.GUI.PopupBox.AddMessage(message, PostBackMessagesType.Error);
                            }
                        }
                        else
                        {
                            message = "La cartella del documento � cestinata. Si prega di ripristinare prima la cartella";
                            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "L'utente con username " + userName + " ha tentato di ripristinare il file con id=" + doc.ID + " nel portale '" + Network.Label + "', ma la cartella di appartenenza risulta cestinata.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                            NetCms.GUI.PopupBox.AddMessage(message, PostBackMessagesType.Notify);
                        }
                    }
                    else
                    {
                        string message = "Impossibile trovare il documento. E' possibile che il documento non esista o non si trovi nel cestino";
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "L'utente con username " + userName + " ha tentato di ripristinare il documento con id =" + ContentId + " nel portale '" + Network.Label + "', ma esso non risulta cestinato.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        NetCms.GUI.PopupBox.AddMessage(message, PostBackMessagesType.Error);
                    }
                }
            }

            public Control GetTrashView()
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                if (DeleteFolderRequest && ContentId > 0)
                {
                    if (TrashedFolds.Where(x => x.ID == ContentId).Count() > 0)
                        return GetDeleteFolderControl;
                    else
                    {
                        string message = "Impossibile trovare la cartella. E' possibile che la cartella non esista o non si trovi nel cestino";
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "L'utente con username " + userName + " ha tentato di eliminare la cartella con id =" + ContentId + " nel portale '" + Network.Label + "', ma essa non risulta cestinata.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        NetCms.GUI.PopupBox.AddMessage(message, PostBackMessagesType.Error);
                    }
                }

                if (DeleteDocRequest && ContentId > 0)
                {
                    if (TrashedDocuments.Where(x => x.ID == ContentId).Count() > 0)
                        return GetDeleteDocumentControl;
                    else
                    {
                        string message = "Impossibile trovare il documento. E' possibile che il documento non esista o non si trovi nel cestino";
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "L'utente con username " + userName + " ha tentato di eliminare il documento con id =" + ContentId + " nel portale '" + Network.Label + "', ma esso non risulta cestinato.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        NetCms.GUI.PopupBox.AddMessage(message, PostBackMessagesType.Error);
                    }
                }

                if (DeleteAllRequest)
                    return GetDeleteAllControl;

                return GetTrashListView();
            }

            public IList<StructureFolder> TrashedFolds
            {
                get 
                {
                    bool isGod = AccountManager.CurrentAccount.God == 1;
                    return FolderBusinessLogic.GetTrashedFolders(SelectedLang, Network.ID, AccountID, isGod);                      
                }
            }

            public IList<Document> TrashedDocuments
            {
                get
                {
                    bool isGod = AccountManager.CurrentAccount.God == 1;
                    return DocumentBusinessLogic.GetTrashedDocuments(Network.ID, AccountID, isGod);
                }
            }

            public HtmlGenericControl GetTrashListView()
            {
                if (ResumeDocRequest || ResumeFolderRequest)
                {
                    ResumeItems();
                }
                else
                {
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo al Cestino del portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                }
                
                var trashedFold = from fold in TrashedFolds
                                  select new
                                  {
                                      ID = fold.ID,
                                      Nome = fold.Nome,
                                      Percorso = fold.Path,
                                      Tipo = "Cartella",
                                      EliminataDa = (fold.Trash < 0) ? UsersBusinessLogic.GetById(Math.Abs(fold.Trash)).UserName : UsersBusinessLogic.GetById(fold.Trash).UserName,
                                      ResumeKey = "ResumeFolder",
                                      DeleteKey = "DeleteFolder"
                                  };

              
                var trashedDoc = from doc in TrashedDocuments
                                  select new
                                  {
                                      ID = doc.ID,
                                      Nome = doc.Nome,
                                      Percorso = doc.Folder.Path,
                                      Tipo = "File",
                                      EliminataDa = (doc.Trash < 0) ? UsersBusinessLogic.GetById(Math.Abs(doc.Trash)).UserName : UsersBusinessLogic.GetById(doc.Trash).UserName,
                                      ResumeKey = "ResumeDoc",
                                      DeleteKey = "DeleteDoc"
                                  };

                var allTrashed = (trashedFold.Concat(trashedDoc)).OrderBy(x=>x.Tipo).ThenBy(x=>x.EliminataDa);


                ArTable TrashedTable = new ArTable(allTrashed);
                TrashedTable.TitleForExport = "Elenco cartelle e documenti cestinati";
                TrashedTable.PrintExportLinks = true;
                TrashedTable.InnerTableCssClass = "tab";
                TrashedTable.NoRecordMsg = "Cestino Vuoto";
                
                ArTextColumn Nome = new ArTextColumn("Nome", "Nome");
                TrashedTable.AddColumn(Nome);

                ArTextColumn Percorso = new ArTextColumn("Percorso", "Percorso");
                TrashedTable.AddColumn(Percorso);

                ArTextColumn Tipo = new ArTextColumn("Tipo", "Tipo");
                TrashedTable.AddColumn(Tipo);

                ArTextColumn Eliminato = new ArTextColumn("Eliminato da", "EliminataDa");
                TrashedTable.AddColumn(Eliminato);

                ArActionColumn ripristina = new ArActionColumn("Ripristina", ArActionColumn.Icons.Enable, "trash.aspx?action={ResumeKey}&cID={ID}");
                TrashedTable.AddColumn(ripristina);

                ArActionColumn elimina = new ArActionColumn("Elimina", ArActionColumn.Icons.Delete, "trash.aspx?action={DeleteKey}&cID={ID}");
                TrashedTable.AddColumn(elimina);

                HtmlGenericControl div = new HtmlGenericControl("div");
                               
                div.Controls.Add(TrashedTable);

                return div;
            }

            #region Controllo eliminazione cartella
            
            public EliminationConfirmControl GetDeleteFolderControl
            {
                get
                {
                    if (_GetDeleteFolderControl == null)
                    {
                        StructureFolder folder = TrashedFolds.Where(x => x.ID == ContentId).First();
                        _GetDeleteFolderControl = new EliminationConfirmControl("Conferma eliminazione cartella '" + folder.Nome + "'", "Sei sicuro di voler eliminare definitivamente questa cartella? <br /><strong class=\"red\">ATTENZIONE: Non sar� pi� possibile recuperarla!</strong>", Network.Paths.AbsoluteAdminRoot + "/cms/trash.aspx");
                        _GetDeleteFolderControl.ConfirmButton.Click += new EventHandler(ConfirmDelFolderButton_Click);
                    }
                    return _GetDeleteFolderControl;
                }
            }

            void ConfirmDelFolderButton_Click(object sender, EventArgs e)
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(TraceLevel.Info, "L'utente " + userName + " sta tentando di eliminare la cartella con id = " + ContentId, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                
                #region Elimino La cartella
                if (TrashBusinessLogic.TrashBusinessLogic.DeleteFolder(ContentId, NetworksManager.GlobalIndexer.AllNetworks[Network.SystemName], TrashBusinessLogic.TrashBusinessLogic.GetCurrentSession()))
                {

                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha eliminato definitivamente la cartella con id=" + ContentId + ", relativamente al portale '" + Network.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    NetCms.GUI.PopupBox.AddSessionMessage("Cartella eliminata con successo", PostBackMessagesType.Success, Network.Paths.AbsoluteAdminRoot + "/cms/trash.aspx");
                }
                else
                {

                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore nell'eliminazione della cartella con id=" + ContentId + ", relativamente al portale '" + Network.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    NetCms.GUI.PopupBox.AddSessionMessage("Errore nell'eliminazione della cartella", PostBackMessagesType.Error, Network.Paths.AbsoluteAdminRoot + "/cms/trash.aspx");
                }
                #endregion

            }
            private EliminationConfirmControl _GetDeleteFolderControl;
            #endregion

            #region Controllo eliminazione documento
            
            public EliminationConfirmControl GetDeleteDocumentControl
            {
                get
                {
                    if (_GetDeleteDocumentControl == null)
                    {
                        Document doc = TrashedDocuments.Where(x => x.ID == ContentId).First();
                        _GetDeleteDocumentControl = new EliminationConfirmControl("Conferma eliminazione documento '" + doc.Nome + "'", "Sei sicuro di voler eliminare definitivamente questo documento? <br /><strong class=\"red\">ATTENZIONE: Non sar� pi� possibile recuperarlo!</strong>", Network.Paths.AbsoluteAdminRoot + "/cms/trash.aspx");
                        _GetDeleteDocumentControl.ConfirmButton.Click += new EventHandler(ConfirmDelDocumentButton_Click);
                    }
                    return _GetDeleteDocumentControl;
                }
            }

            void ConfirmDelDocumentButton_Click(object sender, EventArgs e)
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(TraceLevel.Info, "L'utente " + userName + " sta tentando di eliminare il documento con id = " + ContentId, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                #region Elimino il documento
                if (TrashBusinessLogic.TrashBusinessLogic.DeleteDoc(ContentId))
                {

                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha eliminato definitivamente il documento con id=" + ContentId + ", relativamente al portale '" + Network.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    NetCms.GUI.PopupBox.AddSessionMessage("Documento eliminato con successo", PostBackMessagesType.Success, Network.Paths.AbsoluteAdminRoot + "/cms/trash.aspx");
                }
                else
                {

                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore nell'eliminazione del documento con id=" + ContentId + ", relativamente al portale '" + Network.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    NetCms.GUI.PopupBox.AddSessionMessage("Errore nell'eliminazione del documento", PostBackMessagesType.Error, Network.Paths.AbsoluteAdminRoot + "/cms/trash.aspx");
                }
                #endregion

            }
            private EliminationConfirmControl _GetDeleteDocumentControl;
            #endregion

            #region Controllo svuotamento cestino
            
            public EliminationConfirmControl GetDeleteAllControl
            {
                get
                {
                    if (_GetDeleteAllControl == null)
                    {
                        _GetDeleteAllControl = new EliminationConfirmControl("Conferma svuotamento", "Sei sicuro di voler eliminare definitivamente tutti i documenti e le cartelle presenti nel cestino? <br /><strong class=\"red\">ATTENZIONE: Non sar� pi� possibile recuperarli!</strong>", Network.Paths.AbsoluteAdminRoot + "/cms/trash.aspx");
                        _GetDeleteAllControl.ConfirmButton.Click += new EventHandler(ConfirmDelAllButton_Click);
                    }
                    return _GetDeleteAllControl;
                }
            }

            void ConfirmDelAllButton_Click(object sender, EventArgs e)
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(TraceLevel.Info, "L'utente " + userName + " sta tentando di svuotare il cestino", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                bool ok = true;
                #region Folders
                                
                foreach (StructureFolder folder in TrashedFolds)
                    ok &= TrashBusinessLogic.TrashBusinessLogic.DeleteFolder(folder.ID, NetworksManager.GlobalIndexer.AllNetworks[Network.SystemName], TrashBusinessLogic.TrashBusinessLogic.GetCurrentSession());

                #endregion

                #region Docs
                
                foreach (Document doc in TrashedDocuments)
                    ok &= TrashBusinessLogic.TrashBusinessLogic.DeleteDoc(doc.ID);


                #endregion
                if (ok)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha svuotato il cestino del portale '" + Network.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    NetCms.GUI.PopupBox.AddSessionMessage("Cestino svuotato con successo", PostBackMessagesType.Success, Network.Paths.AbsoluteAdminRoot + "/cms/trash.aspx");
                }
                else
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha riscontrato un errore durante lo svuotamento del cestino del portale '" + Network.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    NetCms.GUI.PopupBox.AddSessionMessage("Errore durante lo svuotamento del cestino", PostBackMessagesType.Error, Network.Paths.AbsoluteAdminRoot + "/cms/trash.aspx");
                }

            }
            private EliminationConfirmControl _GetDeleteAllControl;
            #endregion

        }
    }
}



