﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Data;
using System.Web;
using NetCms.DBSessionProvider;
using NHibernate;
using NetCms.Grants;

namespace NetCms.Structure.TrashBusinessLogic
{
    public static class TrashBusinessLogic
    {

        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static bool DocForME(int DocID)
        {
            /*int id = int.Parse(row["id_Doc"].ToString());
            bool xMe = id==PageData.Account.ID;
            if(!xMe)
            {
            Document doc = PageData.RootFolder.FindDoc(id.ToString());
            Account deleter = new Account(PageData.Conn, int.Parse(row["id_User"].ToString()));

            for (int i = 0; i < deleter.Groups.Length; i++)
            {
                StructureGroups.Group Group = PageData.Groups.FindGroup(deleter.Groups[i].ID);
                int value = 0;
                while (Group != null)
                {
                    for (int j = 0; j < Group.AllUsers.Length; j++)
                    {
                        if (Group.AllUsers[i].Type > value && PageData.Account.ID == Group.AllUsers[i].ID)
                            return true;
                    }
                    Group = Group.Parent;
                    value = 1;
                }
            }
            }
            return xMe;*/

            return true;
        }
        public static bool DocForME(DataRow row)
        {
            /*int id = int.Parse(row["id_Doc"].ToString());
            bool xMe = id==PageData.Account.ID;
            if(!xMe)
            {
            Document doc = PageData.RootFolder.FindDoc(id.ToString());
            Account deleter = new Account(PageData.Conn, int.Parse(row["id_User"].ToString()));

            for (int i = 0; i < deleter.Groups.Length; i++)
            {
                StructureGroups.Group Group = PageData.Groups.FindGroup(deleter.Groups[i].ID);
                int value = 0;
                while (Group != null)
                {
                    for (int j = 0; j < Group.AllUsers.Length; j++)
                    {
                        if (Group.AllUsers[i].Type > value && PageData.Account.ID == Group.AllUsers[i].ID)
                            return true;
                    }
                    Group = Group.Parent;
                    value = 1;
                }
            }
            }
            return xMe;*/

            return true;
        }
        public static bool FolderForME(int FolderID)
        {
            /*
            int id = int.Parse(row["id_Doc"].ToString());
            bool xMe = id == PageData.Account.ID;
            if (!xMe)
            {
                Document doc = PageData.RootFolder.FindDoc(id.ToString());
                Account deleter = new Account(PageData.Conn, int.Parse(row["id_User"].ToString()));

                for (int i = 0; i < deleter.Groups.Length; i++)
                {
                    StructureGroups.Group Group = PageData.Groups.FindGroup(deleter.Groups[i].ID);
                    int value = 0;
                    while (Group != null)
                    {
                        for (int j = 0; j < Group.AllUsers.Length; j++)
                        {
                            if (Group.AllUsers[i].Type > value && PageData.Account.ID == Group.AllUsers[i].ID)
                                return true;
                        }
                        Group = Group.Parent;
                        value = 1;
                    }
                }
            }
            return xMe;*/
            return true;
        }
        public static bool FolderForME(DataRow row)
        {
            /*
            int id = int.Parse(row["id_Doc"].ToString());
            bool xMe = id == PageData.Account.ID;
            if (!xMe)
            {
                Document doc = PageData.RootFolder.FindDoc(id.ToString());
                Account deleter = new Account(PageData.Conn, int.Parse(row["id_User"].ToString()));

                for (int i = 0; i < deleter.Groups.Length; i++)
                {
                    StructureGroups.Group Group = PageData.Groups.FindGroup(deleter.Groups[i].ID);
                    int value = 0;
                    while (Group != null)
                    {
                        for (int j = 0; j < Group.AllUsers.Length; j++)
                        {
                            if (Group.AllUsers[i].Type > value && PageData.Account.ID == Group.AllUsers[i].ID)
                                return true;
                        }
                        Group = Group.Parent;
                        value = 1;
                    }
                }
            }
            return xMe;*/
            return true;
        }

        public static void DeleteFolderRecursive(DirectoryInfo dir)
        {
            if (dir.Exists)
            {
                FileInfo[] files = dir.GetFiles();
                for (int i = 0; i < files.Length; i++)
                {
                    files[i].Delete();
                }

                DirectoryInfo[] dirs = dir.GetDirectories();
                for (int i = 0; i < dirs.Length; i++)
                {
                    DeleteFolderRecursive(dirs[i]);
                }
            }
        }

        public static bool DeleteDoc(int DocID)
        {
            bool status = true;
            bool deleteFile = false;
            string strFile = "";

            using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {              
                try
                {
                    if (DocForME(DocID))
                    {
                        Document doc = DocumentBusinessLogic.GetById(DocID, session);
                        if (doc != null)
                        {
                            if (doc.Application == 4)
                            {
                                deleteFile = true;
                                strFile = System.Web.HttpContext.Current.Server.MapPath(doc.Network.Paths.AbsoluteFrontRoot + doc.FullName);
                            }


                            doc.Folder.Documents.Remove(doc);

                            if (doc.Allegati.Count > 0)
                            {
                                DeleteAllegati(doc.Allegati, session);
                            }
                            doc.Allegati.Clear();
                            DocumentBusinessLogic.DeleteDocument(doc, session);
                        }
                    }

                    tx.Commit();

                    // delete file
                    if (deleteFile && System.IO.File.Exists(strFile))
                       System.IO.File.Delete(strFile);
                }
                catch (Exception ex)
                {
                    status = false;
                    tx.Rollback();
                }
              

                return status;
            }
        }

        private static void DeleteAllegati(ICollection<Document> allegati, ISession session)
        {
            foreach (Document allegato in allegati)
            {
                if (allegato.Allegati.Count > 0)
                    DeleteAllegati(allegato.Allegati, session);

                allegato.Folder.Documents.Remove(allegato);
                allegato.Allegati.Clear();
                DocumentBusinessLogic.DeleteDocument(allegato, session);
            }
        }

        public static bool DeleteFolder(int FolderID, PageData PageData)
        {
            bool result = true;
            try
            {
                if (FolderForME(FolderID))
                {
                    StructureFolder folder = NetCms.Structure.WebFS.FileSystemBusinessLogic.FolderBusinessLogic.GetById(FolderID);

                    if (folder != null)
                    {
                        string folderpath = folder.Path;

                        folderpath = PageData.Paths.AbsoluteWebRoot + folderpath;
                        folderpath = PageData.Server.MapPath(folderpath);

                        DirectoryInfo dir = new DirectoryInfo(folderpath);
                        DeleteFolderRecursive(dir);

                        string FolderTree = folder.Tree;
                        GrantsBusinessLogic.DeleteFolderProfilesByFolder(folder.ID, folder.NetworkID, FolderBusinessLogic.GetCurrentSession());
                        folder.DeleteSomething(FolderBusinessLogic.GetCurrentSession());
                        folder.DeleteModuloApplicativo(FolderBusinessLogic.GetCurrentSession());
                        //string sql = "SELECT Modulo_ModuloApplicativo FROM homepages_moduli_applicativi WHERE Folder_ModuloApplicativo = " + FolderID;
                        //DataTable ModuliToDelete = PageData.Conn.SqlQuery(sql);
                        //PageData.Conn.Execute("DELETE FROM homepages_moduli_applicativi WHERE Folder_ModuloApplicativo = " + FolderID);
                        //foreach (DataRow row in ModuliToDelete.Rows)
                        //{
                        //    PageData.Conn.Execute("DELETE FROM homepages_moduli WHERE id_Modulo = " + row["Modulo_ModuloApplicativo"]);
                        //}
                        FolderBusinessLogic.DeleteFolder(folder);
                        PageData.Conn.ClearCache();
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        public static bool DeleteFolder(int FolderID, Networks.BasicNetwork network, ISession session)
        {
            if (FolderForME(FolderID))
            {
                try
                {
                    StructureFolder folder = NetCms.Structure.WebFS.FileSystemBusinessLogic.FolderBusinessLogic.GetById(session, FolderID);

                    //string folderpath = dataTable.Rows[0]["Path_Folder"].ToString();
                    string folderpath = folder.Path;

                    folderpath = network.Paths.AbsoluteFrontRoot + folderpath;
                    folderpath = HttpContext.Current.Server.MapPath(folderpath);

                    DirectoryInfo dir = new DirectoryInfo(folderpath);
                    DeleteFolderRecursive(dir);

                    //string FolderTree = dataTable.Rows[0]["Tree_Folder"].ToString();
                    string FolderTree = folder.Tree;

                    //network.Connection.Execute("DELETE homepages_moduli FROM homepages_moduli INNER JOIN homepages_moduli_applicativi ON id_Modulo = Modulo_ModuloApplicativo WHERE Folder_ModuloApplicativo = " + FolderID);

                    folder.DeleteModuloApplicativo(session);
                    GrantsBusinessLogic.DeleteFolderProfilesByFolder(folder.ID, folder.NetworkID, session);

                    //string sql = "SELECT Modulo_ModuloApplicativo FROM homepages_moduli_applicativi WHERE Folder_ModuloApplicativo = " + FolderID;
                    //DataTable ModuliToDelete = network.Connection.SqlQuery(sql);
                    //network.Connection.Execute("DELETE FROM homepages_moduli_applicativi WHERE Folder_ModuloApplicativo = " + FolderID);
                    //foreach (DataRow row in ModuliToDelete.Rows)
                    //{
                    //    network.Connection.Execute("DELETE FROM homepages_moduli WHERE id_Modulo = " + row["Modulo_ModuloApplicativo"]);
                    //}

                    //PageData.Conn.Execute("DELETE FROM Folders WHERE Tree_Folder LIKE '%" + FolderTree + "%'");
                    FolderBusinessLogic.DeleteFolder(session, folder);
                    network.Connection.ClearCache();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                    throw ex;
                }
            }
            return false;
        }
    }

}
