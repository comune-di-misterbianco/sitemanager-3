﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure;


namespace NetCms.Structure.WebFS
{
    public class TrashMainPage : NetCms.GUI.NetworkPage
    {
        public TrashMainPage() : base() 
        {
            if (Cestino.TrashedDocuments.Count > 0 || Cestino.TrashedFolds.Count > 0)
                this.Toolbar.Buttons.Add(new ToolbarButton("trash.aspx?action=Empty", NetCms.GUI.Icons.Icons.Bin_Empty, "Svuota cestino"));
            this.Toolbar.Buttons.Add(new ToolbarButton(this.CurrentNetwork.Paths.AbsoluteAdminRoot + "/cms/default.aspx", NetCms.GUI.Icons.Icons.Arrow_Left, "Indietro"));
        }
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Cestino '" + this.CurrentNetwork.Label + "'"; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Bin; }
        }

        public Trash Cestino
        {
            get
            {
                if (_Cestino == null)
                    _Cestino = new Trash(this.CurrentNetwork, this.CurrentNetwork.DefaultLang, NetCms.Users.AccountManager.CurrentAccount.ID);
                return _Cestino;
            }
        }
        private Trash _Cestino;

        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);

            div.Controls.Add(Cestino.GetTrashView());

            return div;
        }

    }
}