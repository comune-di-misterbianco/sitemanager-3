using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Users;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.Structure
{
    public class Structure
    {
        public HtmlGenericControl Control
        {
            get { return getControl(); }
        }

        private PageData _PageData;
        public PageData PageData
        {
            get
            {
                if (_PageData == null || _PageData.GetHashCode() != ((PageData)HttpContext.Current.Items["SM_PageData"]).GetHashCode())
                    _PageData = (PageData)HttpContext.Current.Items["SM_PageData"];
                return _PageData;
            }
        }

        public Structure()
        {
            if (Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.OutsideNetworks)
                PageData.Redirect(PageData.Paths.AbsoluteSiteManagerRoot + "/default.aspx");

            #region  Aggiunta dei Preferiti

            NetUtility.RequestVariable starred = new NetUtility.RequestVariable("star", PageData.Request.QueryString);
            if (!PageData.IsPostBack && starred.IsValid(NetUtility.RequestVariable.VariableType.Integer))
            {
                StructureFolder FavoriteFolder = this.PageData.RootFolder.FindFolderByID(starred.IntValue) as StructureFolder;
                if (FavoriteFolder != null && !FavoriteFolder.Starred && FavoriteFolder.Grant)
                {
                    User user = UsersBusinessLogic.GetById(PageData.Account.ID);
                    user.FavoritesFolders.Add(FavoriteFolder);
                    UsersBusinessLogic.SaveOrUpdateUser(user);
                    
                    //PageData.Conn.Execute("INSERT INTO Favorites (User_Favorite,Folder_Favorite) VALUES (" + PageData.Account.ID + "," + starred.IntValue.ToString() + ")");
                    
                    FavoriteFolder.InitStarred();
                    PageData.Session["Favorites"] = null;
                }
            }
            #endregion
        }

        private HtmlGenericControl getControl()
        {
            HttpContext.Current.Items.Add("CheckCurentFolderGrant", false);
            return PageData.CurrentFolder.getView();
        }
    }
}



