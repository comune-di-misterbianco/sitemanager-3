using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using NetForms;
using NetCms;
/// <summary>
/// Summary description for NetField
/// </summary>

namespace NetCms
{
    namespace Structure
    {
        public class StructureMenuTreeView : NetField
        {
            public TreeNode Tree;
            public bool AutoPostBack = false;
            public bool CheckAllows = true;
            public bool IsStatic = false;

            private PageData _Config;

            private int idSeed = 0;

            public StructureMenuTreeView(string label, string fieldname, PageData config)
                : base(label, fieldname)
            {
                //
                // TODO: Add constructor logic here
                //

                Required = false;
                _Config = config;
            }

            public StructureMenuTreeView(string label, string fieldname, PageData config, TreeNode tree)
                : base(label, fieldname)
            {
                Required = false;
                Tree = tree;
                _Config = config;
            }

            public override HtmlGenericControl getControl()
            {
                HtmlGenericControl par = new HtmlGenericControl("p");
                par.Attributes["style"] = "text-align: left;";
                HtmlInputHidden hidden;

                hidden = new HtmlInputHidden();
                hidden.ID = _FieldName;
                hidden.Value = Value;
                par.Controls.Add(hidden);
                par.Controls.Add(getTree());

                return par;
            }

            public override String getFieldName()
            {
                return _FieldName;
            }

            public override string validateInput(string input)
            {

                string errors = "";

                if (Required && input.Length == 0)
                {
                    errors += "<li>";
                    errors += "Il campo '" + _Label + "' � obbligatorio";
                    errors += "</li>";
                }

                return errors;
            }

            public override string getFilter(string value)
            {
                string filter = " ";
                if (value.Length > 0)
                {
                    filter += _FieldName;

                    filter += " = ";
                    filter += value;
                    return filter;
                }
                return filter;
            }

            private Control getTree()
            {
                idSeed = 0;
                HtmlGenericControl div = new HtmlGenericControl("div");
                div.Attributes["style"] = "width:250px;text-align: left;";
                HtmlGenericControl script = new HtmlGenericControl("script");

                div.Controls.Add(script);
                script.Attributes["type"] = "text/javascript";
                string html = "\n";
                html += "<!--\n";

                html += "\nfunction setSelectedNode(value){\n";
                html += "   document.getElementById(\"" + _FieldName + "\").value = value; \n";
                if (AutoPostBack)
                {
                    html += "   setSubmitSource(\"" + _FieldName + "\");\n";
                    html += "   if (document.getElementById(\"page\")!=null){\n";
                    html += "       setpage(0);}\n";
                    html += "   document.getElementById(\"PageForm\").submit();\n";
                }
                html += "}\n";
                if (IsStatic)
                    html += "	d = new dTree('d',true);\n";
                else
                    html += "	d = new dTree('d');\n";
                html += getNodes(Tree, idSeed++, -1);

                html += "		document.write(d);\n";
                if (IsStatic)
                    html += "		javascript: d.openAll();\n";
                html += "//-->\n";
                script.InnerHtml = html;

                return div;
            }

            private string getNodes(TreeNode node, int id, int padre)
            {
                string image = _Config.RootDist + "App_images/dtree/folder.gif";
                string image_open = _Config.RootDist + "App_images/dtree/folderopen.gif";
                string js_command = "javascript: setSelectedNode(\\\'" + node.Value + "\\\')";

                //if (_Config.PageName!="default.aspx")
                js_command = _Config.Paths.AbsoluteNetworkRoot + "/cms/default.aspx?folder=" + node.Value + "";

                if (node.Value =="0")
                {
                    image = _Config.RootDist + "App_images/dtree/trash.gif";
                    image_open = _Config.RootDist + "App_images/dtree/trash.gif";
                    js_command = _Config.Paths.AbsoluteNetworkRoot + "/cms/trash.aspx";
                }

                else
                if (CheckAllows && !node.Checked)
                {
                    image = _Config.RootDist + "App_images/dtree/redfolder.gif";
                    image_open = _Config.RootDist + "App_images/dtree/redfolderopen.gif";
                    js_command = "javascript: alert(\\\'Attenzione non si possedono i permessi per la categoria selezionata\\\')";
                }

                string html = "d.add(" + id + "," + padre + ",'" + node.Text.Replace("'","\\\'") + "','" + js_command + "','','','" + image + "','" + image_open + "'); \n";
                foreach (TreeNode childnode in node.ChildNodes)
                {
                    html += getNodes(childnode, idSeed++, id);
                }
                return html;
            }

            public override string validateValue(string value)
            {
                string output = value;
                output = output.Trim();

                bool parseResult;
                int intvalue;
                parseResult = int.TryParse(output, out intvalue);
                if (parseResult)
                {
                    output = output.Replace("'", "''");
                    output = "" + output + "";
                }
                else
                {
                    output = output.Replace("'", "''");
                    output = "'" + output + "'";
                }

                return output;
            }
        }
    }
}