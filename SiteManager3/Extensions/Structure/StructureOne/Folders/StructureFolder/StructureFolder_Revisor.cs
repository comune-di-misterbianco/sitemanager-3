using System;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Collections.Generic;
using NetCms.Users;
using System.Linq;

/// <summary>
/// Summary description for Folder
/// </summary>
namespace NetCms
{
    namespace Structure
    {
        public class RevisorsLoader
        {
            private PageData PageData;
            private StructureFolder Folder;
            private Document Doc;

            private Users.Group _Root;

            private enum ObjectTypes
            {
                Document = 0,
                Folder = 1
            }
            private ObjectTypes ObjectType;

            public RevisorsLoader(PageData pagedata,StructureFolder folder)
            {
                PageData = pagedata;
                Folder = folder;
                ObjectType = ObjectTypes.Folder;
            }
            public RevisorsLoader(PageData pagedata, Document doc)
            {
                PageData = pagedata;
                Doc = doc;
                Folder = FolderBusinessLogic.GetById(Doc.Folder.ID);
                ObjectType = ObjectTypes.Document;
            }

            public Users.Group getRootGroup()
            {

                //DataTable table = PageData.Conn.SqlQuery("SELECT * FROM groups ORDER BY Tree_group");

                ICollection<Group> gruppi = GroupsBusinessLogic.FindAllOrderedByTree();

                //DataTable users = PageData.Conn.SqlQuery("SELECT * FROM Users INNER JOIN usersgroups ON (users.id_User = usersgroups.User_UserGroup) WHERE Deleted_User = 0");
                NetCms.Users.Group root = null;

                //string groupName;
                //string groupID;
                //int groupProfile;
                //int groupDepth;
                //string groupTree;
                NetCms.Users.Group lastgroup = root;
                for (int i = 0; i < gruppi.Count; i++)
                {
                    Group group = gruppi.ElementAt(i);

                    //DataRow row = table.Rows[i];
                    //groupName = row["Name_Group"].ToString();
                    //groupTree = row["Tree_Group"].ToString();
                    //groupID = row["id_Group"].ToString();
                    //groupProfile = int.Parse(row["Profile_Group"].ToString());
                    //groupDepth = int.Parse(row["Depth_Group"].ToString());
                    //Users.UserStruct[] groupUsers = buildUsers(users, int.Parse(groupID));
                    //NetCms.Users.Group group = new NetCms.Users.Group(row);

                    if (i == 0)
                        root = group;
                    else
                    {
                        if (group.Depth == lastgroup.Depth)
                            lastgroup.Parent.Groups.Add(group);

                        if (group.Depth > lastgroup.Depth)
                            lastgroup.Groups.Add(group);

                        if (group.Depth < lastgroup.Depth)
                        {
                            int groupGap = lastgroup.Depth - group.Depth;
                            NetCms.Users.Group parent = lastgroup;
                            for (int p = 0; p <= groupGap; p++)
                                parent = parent.Parent;
                            parent.Groups.Add(group);
                        }
                    }
                    lastgroup = group;
                }

                //table.Dispose();
                _Root = root;
                return _Root;
            }

            //NON SEMBRA ESSERE USATO
            //public Users.UserStruct[] buildUsers(DataTable users, int group)
            //{
            //    DataRow[] rows = users.Select(" Group_UserGroup = " + group);
            //    Users.UserStruct[] groupUsers = new Users.UserStruct[rows.Length];

            //    for (int i = 0; i < groupUsers.Length; i++)
            //    {
            //        groupUsers[i].ID = int.Parse(rows[i]["id_User"].ToString());
            //        groupUsers[i].Profile = int.Parse(rows[i]["Profile_User"].ToString());
            //        groupUsers[i].Revisor = CheckForRevisor(groupUsers[i]);
            //    }
            //    return groupUsers;
            //}

            private int CheckForRevisor(Users.UserStruct user)
            {
                /*NetCms.Users.User account = new NetCms.Users.User(user.ID.ToString());
                CriteriaCollection Criteria = null;
                if (ObjectType == ObjectTypes.Document)
                {
                    Grants.DocGrantsFinder finder = new Grants.DocGrantsFinder(account);

                    Criteria = finder.Search(Doc);
                }

                StructureFolder TempFolder = Folder;
                while (TempFolder != null && Criteria!=null && Criteria["redactor"].Value == 0)
                {
                    Grants.FolderGrantsFinder finder = new Grants.FolderGrantsFinder(account);
                    Criteria = finder.Search(TempFolder);
                    TempFolder = TempFolder.Parent;
                }
                
                return Criteria["redactor"].Value;*/
                return 0;
            }
        }
    }
}
