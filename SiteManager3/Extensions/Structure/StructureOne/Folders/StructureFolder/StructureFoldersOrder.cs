using System;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.Connections;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using NetCms.Grants;

/// <summary>
/// Summary description for Folder
/// </summary>
namespace NetCms
{
    namespace Structure
    {
        public class StructureFoldersOrder
        {
            private string SubfoldersSQL
            {
                get
                {
                    if (_SubfoldersSQL == null)
                        _SubfoldersSQL = "SELECT id_Folder,Label_FolderLang,Order_Folder FROM (Folders INNER JOIN network_folders ON id_Folder = id_NetworkFolder) INNER JOIN FoldersLang ON id_Folder = Folder_FolderLang WHERE (Stato_Folder = 0 OR Stato_Folder = 4) AND System_Folder = 0 AND Depth_Folder = " + (PageData.CurrentFolder.Depth + 1) + " AND Tree_Folder LIKE '" + PageData.CurrentFolder.Tree + "%' AND Trash_Folder = 0 ORDER BY Order_Folder DESC";
                    return _SubfoldersSQL;
                }
            }
            private string _SubfoldersSQL;


            private string DocsSQL
            {
                get
                {
                    if (_DocsSQL == null)
                        _DocsSQL = "SELECT id_Doc,Label_DocLang,Order_Doc FROM (Docs INNER JOIN network_docs ON id_Doc = id_NetworkDoc) INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang WHERE Stato_Doc <> 2 AND (Application_Doc = 2 OR Application_Doc = 4  OR Application_Doc = 9) AND Folder_Doc = " + (PageData.CurrentFolder.ID) + " AND Trash_Doc = 0 ORDER BY Order_Doc DESC";
                    return _DocsSQL;
                }
            }
            private string _DocsSQL;

            private const string  ControlID = "Folders_Order";
            public PageData PageData;

            #region Costruttore

            public StructureFoldersOrder(PageData pageData)
            {
                PageData = pageData;
            }

            #endregion

            public HtmlGenericControl GetView()
            {
                //Inserito per risolvere un problema di caching quando si apre la pagina di ordinamento, successivamente si crea una folder e poi si torna alla pagina di ordinamento
                if (!PageData.IsPostBack)
                    PageData.Conn.ClearCache();

                HtmlGenericControls.Div container = new HtmlGenericControls.Div();
                HtmlGenericControls.Fieldset info = new HtmlGenericControls.Fieldset("Informazioni sull'ordinamento del men�",new HtmlGenericControl("p"));
                HtmlGenericControls.Fieldset div = new HtmlGenericControls.Fieldset("Ordinamento cartelle contenute nella cartella '" + PageData.CurrentFolder.Label + "'");
                container.Controls.Add(info);
                container.Controls.Add(div);

                info.Content.InnerHtml = "Da qu� � possibile gestire l'ordine di visualizzazione delle voci di men� contenute nella cartella '" + PageData.CurrentFolder.Label + "', le voci che verranno ordinate sono l'insieme di cartelle e documenti di tipo Pagina Web o Link, impostate come 'Men� + Raggiungibile' all'interno della cartella data.";

                if (PageData.CurrentFolder != null && PageData.CurrentFolder.Grant && PageData.CurrentFolder.Criteria[CriteriaTypes.Advanced].Allowed)
                {
                    PageData.InfoToolbar.Icon = "Folder";
                    PageData.InfoToolbar.Title = "Ordinamento contenuti della cartella '"+PageData.CurrentFolder.Label+"'";

                    string Utente =  PageData.Account.NomeCompleto + " (" + PageData.Account.UserName + ")";
                    string logText = "L'utente " + Utente + " vuole riordinare i contenuti della cartella " + PageData.CurrentFolder.Label;
                    PageData.GuiLog.SaveLogRecord(logText, false);


                    NetForms.NetUpDownListBox updown = new NetUpDownListBox("Contenuti", ControlID);
                    //updown.setDataBind(SubfoldersSQL, PageData.Conn);
                    DataTable docs = PageData.Conn.SqlQuery(this.DocsSQL);
                    DataTable folds = PageData.Conn.SqlQuery(this.SubfoldersSQL);
                    int maxd = docs.Rows.Count > 0 ? int.Parse(docs.Rows[0]["Order_Doc"].ToString()) : 0;
                    int maxf = folds.Rows.Count>0 ? int.Parse(folds.Rows[0]["Order_Folder"].ToString()) : 0;
                    int max = maxd > maxf ? maxd : maxf;
                    max = docs.Rows.Count + folds.Rows.Count> max ?docs.Rows.Count + folds.Rows.Count :max;
                    for (int i = max; i >= 0; i--)
                    {
                        DataRow[] rows = folds.Select("Order_Folder = "+i);
                        foreach(DataRow row in rows)
                            updown.ListBox.Items.Add(new ListItem(row["Label_FolderLang"].ToString(), "Folder" + row["id_Folder"]));
                        
                        rows = docs.Select("Order_Doc = " + i);
                        foreach (DataRow row in rows)
                        {
                            if (row["Label_DocLang"].ToString() != "Pagina Indice")
                            updown.ListBox.Items.Add(new ListItem(row["Label_DocLang"].ToString(), "Doc" + row["id_Doc"]));
                        }
                    }
                    if (updown.ListBox.Items.Count > 0)
                    {
                        div.Controls.Add(updown.getControl());

                        Button submit = new Button();
                        submit.ID = "SaveOrder";
                        submit.Text = "Salva Ordine";
                        submit.OnClientClick = "setSubmitSource('" + submit.ID + "')";

                        div.Controls.Add(submit);

                        if (PageData.IsPostBack && PageData.SubmitSource == submit.ID)
                        {
                            //Utente =  PageData.Account.NomeCompleto + " (" + PageData.Account.UserName + ")";
                            //logText = "L'utente " + Utente + " ha salvato le modifiche sull'ordine dei contenuti della cartella "+ PageData.CurrentFolder.Label;;
                            //PageData.GuiLog.SaveLogRecord(logText);
                            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha salvato le modifiche sull'ordine dei contenuti della cartella'" + PageData.CurrentFolder.Label + "' (id=" + PageData.CurrentFolder.ID + "), relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                            PostBack();
                        }
                        else
                        {
                            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di gestione dell'ordinamento dei contenuti della cartella'" + PageData.CurrentFolder.Label + "' (id=" + PageData.CurrentFolder.ID + "), relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        }
                    }
                    else 
                    {
                        container.Controls.Add(new LiteralControl("Le cartelle e documenti contenuti nella cartella corrente non presentano i requisiti per eseguire un ordinamento. Verificare se i documenti hanno le rispettive revisioni pubblicate."));
                    }
                }
                else
                {
                    PageData.GoToErrorPage();
                }

                return container;
            }

            private void PostBack()
            {
                NetUtility.RequestVariable neworder = new NetUtility.RequestVariable(ControlID,PageData.Request.Form);
                if(neworder.IsValidString)
                {
                    string[] NewOrder = neworder.StringValue.Split(',');
                    int order = NewOrder.Length-1;
                    foreach (string s in NewOrder)
                    {
                        if (s.Contains("Doc"))
                        {
                            int id = int.Parse(s.Replace("Doc", ""));
                            string query = "UPDATE Docs SET Order_Doc = '" + order + "' WHERE id_Doc = " + id;
                            PageData.Conn.Execute(query);
                        }
                        else
                        if (s.Contains("Folder"))
                        {
                            int id = int.Parse(s.Replace("Folder", ""));
                            string query = "UPDATE Folders SET Order_Folder = '" + order + "' WHERE id_Folder = " + id;
                            PageData.Conn.Execute(query);
                        }
                        order--;
                    }
                    #region oldcode
                    /*
                                DataTable allfolders = PageData.Conn.SqlQuery("SELECT * FROM Folders WHERE Tree_Folder LIKE '" + PageData.CurentFolder.Tree + "%' ORDER BY Tree_Folder");
                                DataTable folders = PageData.Conn.SqlQuery(SubfoldersSQL);

                                StringCollection SwitchSql = new StringCollection();
                                for (int i = 0; i < folders.Rows.Count; i++)
                                {
                                    //Seleziono una cartella
                                    string FolderID = folders.Rows[i]["id_Folder"].ToString();;
                                    string tree = folders.Rows[i]["Tree_Folder"].ToString();

                                    //Controllo in che posizione � stata spostata
                                    bool found = false;
                                    int k = 0;
                                    while (k < NewOrder.Length&&!found)
                                    {
                                        if(NewOrder[k] == FolderID)
                                        {
                                            found = true;
                                        }
                                        else
                                            k++;
                                    }
                                    string ordertree = GetTreeFromID(folders.Rows[k]["id_Folder"].ToString());

                                    if (tree != ordertree)
                                    {
                                        //Posiziono
                                        DataRow[] rows = allfolders.Select("Tree_Folder LIKE '" + tree + "%'");
                                        for (int j = 0; j < rows.Length; j++)
                                        {
                                            string newtree = rows[j]["Tree_Folder"].ToString();
                                            newtree = newtree.Replace(tree, ordertree);
                                            string query = "UPDATE Folders SET Tree_Folder = '" + newtree + "' WHERE id_Folder = " + rows[j]["id_Folder"].ToString() + ";"; 
                                            if(!SwitchSql.Contains(query))
                                                SwitchSql.Add(query);
                                        }

                                    }

                                }
                                for (int i = 0; i < SwitchSql.Count; i++)
                                    PageData.Conn.Execute(SwitchSql[i]);
                                */ 
                    #endregion

                    #region invalido la struttura
                    //Controllare l'invalidazione. Verificare se va invalidata tutta la cartella o solo 1 documento

                    //SISTEMARE CON CACHE 2� LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                    PageData.CurrentFolder.Invalidate();
                    if (PageData.CurrentFolder.Parent != null)
                        ((StructureFolder)PageData.CurrentFolder.Parent).Invalidate();


                    #endregion
                    PageData.Redirect("default.aspx?folder="+PageData.CurrentFolder.ID);

                }
            }

            private string GetTreeFromID(string id)
            {
                string output  ="";
                StructureFolder folder = PageData.CurrentFolder.FindFolderByID(int.Parse(id)) as StructureFolder;
                if (folder != null)
                    output = folder.Tree;
                return output;
            }
        }
    }
}
