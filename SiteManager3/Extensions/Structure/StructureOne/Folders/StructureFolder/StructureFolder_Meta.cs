using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using NetCms.Grants;

/// <summary>
/// Summary description for StructureDocument
/// </summary>
namespace NetCms
{
    namespace Structure
    {
        public class StructureFolderMeta
        {
            private int _Meta = -1;
            private int Meta
            {
                get
                {
                    if (_Meta < 0)
                    {
                        NetUtility.RequestVariable RequestMeta = new NetUtility.RequestVariable("meta", NetUtility.RequestVariable.RequestType.QueryString);
                        if (RequestMeta.IsValidInteger)
                            _Meta = RequestMeta.IntValue;
                        else
                            _Meta = 0;
                    }
                    return _Meta;
                }
            }


            public string getMetaName(string id)
            {
                string sql = "SELECT * FROM Folders_Meta";
                sql += " INNER JOIN Meta ON id_Meta = Meta_FolderMeta ";
                sql += " WHERE id_FolderMeta = " + id;
                DataTable reader = PageData.Conn.SqlQuery(sql);
                if (reader.Rows.Count > 0)
                    return reader.Rows[0]["Name_Meta"].ToString();
                return "";
            }
            public string getMetaType(string id)
            {
                string sql = "SELECT * FROM Folders_Meta";
                sql += " INNER JOIN Meta ON id_Meta = Meta_FolderMeta ";
                sql += " WHERE id_FolderMeta = " + id;
                DataTable reader = PageData.Conn.SqlQuery(sql);
                if (reader.Rows.Count > 0)
                    return reader.Rows[0]["id_Meta"].ToString();
                return "";
            }
           
            private int _ID;
            private int ID
            {
                get
                {
                    if (_ID == 0)
                    {
                        NetUtility.RequestVariable RequestDoc = new NetUtility.RequestVariable("folder", NetUtility.RequestVariable.RequestType.QueryString);
                        if (RequestDoc.IsValidInteger)
                            _ID = RequestDoc.IntValue;
                        else
                            PageData.GoBack();
                    }
                    return _ID;
                }
            }

            private PageData PageData ;

            private StructureFolder _Folder;
            private StructureFolder Folder
            {
                get
                {
                    if (_Folder == null)
                        _Folder = (StructureFolder)PageData.RootFolder.FindFolderByID(ID);
                    if (_Folder == null || !_Folder.Grant || _Folder.Criteria[CriteriaTypes.Modify].Value == GrantsValues.Deny)
                        PageData.GoBack();
                    return _Folder;

                }
            }

            public StructureFolderMeta(PageData pageData)
            {
                PageData = pageData;               
            }

            private NetFormTable _FormTable;
            private NetFormTable FormTable
            {
                get
                {
                    if (_FormTable == null)
                    {
                        _FormTable = new NetFormTable("Folders_Meta", "id_FolderMeta", PageData.Conn);

                        string MetaTypesSql;

                        if (_Meta > 0)
                        {
                            _FormTable.ID = Meta;
                        }


                        #region Campi

                        //MetaTypesSql = "SELECT * FROM Folders_Meta WHERE Folder_FolderMeta = " + ID + "";
                        //DataTable tab = PageData.Conn.SqlQuery(MetaTypesSql);

                        MetaTypesSql = "SELECT Name_Meta,id_Meta FROM Meta ORDER BY Name_Meta";
                        DataTable MetaTable = PageData.Conn.SqlQuery(MetaTypesSql);
                        
                        NetDropDownList meta = new NetDropDownList("Meta", "Meta_FolderMeta");
                        meta.ClearList();
                        foreach (DataRow row in MetaTable.Rows)
                        {
                            //if (tab.Select("Meta_FolderMeta = " + row["id_Meta"]).Length == 0 || tab.Select("id_FolderMeta = " + this.Meta + " AND Meta_FolderMeta = " + row["id_Meta"].ToString()).Length > 0)
                            //    meta.addItem(row["Name_Meta"].ToString(), row["id_Meta"].ToString());

                            //var selectedRows = tab.Select("Meta_FolderMeta = " + row["id_Meta"]);
                            //if (selectedRows.Length == 0 || Meta.ToString() == selectedRows[0]["id_FolderMeta"].ToString())
                                meta.addItem(row["Name_Meta"].ToString(), row["id_Meta"].ToString());
                        }
                        meta.Required = true;
                        _FormTable.addField(meta);

                        NetDropDownList Stato = new NetDropDownList("Stato", "Stato_FolderMeta"); 
                        Stato.ClearList();
                        Stato.addItem("Attivo", "1");
                        Stato.addItem("Non Attivo", "0"); 
                        Stato.Required = true;
                        _FormTable.addField(Stato);

                        NetTextBoxPlus content = new NetTextBoxPlus("Content", "Content_FolderMeta");
                        content.Required = true;
                        content.MaxLenght = 0;
                        content.Rows = 7;
                        content.Size = 70;
                        _FormTable.addField(content);

                        NetHiddenField doc = new NetHiddenField("Folder_FolderMeta");
                        doc.Value = ID.ToString();
                        _FormTable.addField(doc);
                        
                        #endregion
                    }
                    return _FormTable;
                }
            }
             
            public HtmlGenericControl GetNewView()
            {
                HtmlGenericControl div = new HtmlGenericControl("div");

                //string Utente =  PageData.Account.NomeCompleto + " (" + PageData.Account.UserName + ")";
                //string logText = "L'utente " + Utente + " vuole aggiungere meta tag per la cartella " + PageData.CurrentFolder.Nome + " (" + PageData.CurrentFolder.ID + ")";
                //PageData.GuiLog.SaveLogRecord(logText, false);

                G2Core.Common.RequestVariable meta = new G2Core.Common.RequestVariable("Meta_FolderMeta", G2Core.Common.RequestVariable.RequestType.Form);

                G2Core.XhtmlControls.Div divset = new G2Core.XhtmlControls.Div();
                
                NetForm form = new NetForm();
                form.AddTable(FormTable);
                form.SubmitLabel = "Aggiungi Meta";
                string errors="";

                PageData.InfoToolbar.Icon = "Meta";
                PageData.InfoToolbar.Title = "Aggiunta Meta alla Cartella '" + Folder.Label + "'";
                PageData.InfoToolbar.ShowFolderPath = true;

                bool presente = false;
                if (PageData.IsPostBack)
                {
                    string sql = "SELECT * FROM Folders_Meta WHERE Folder_FolderMeta = " + ID + " AND Meta_FolderMeta = " + meta.StringValue;
                    DataTable tab = PageData.Conn.SqlQuery(sql);
                    G2Core.Common.RequestVariable confermaOk = new G2Core.Common.RequestVariable("conferma");
                    if (tab.Rows.Count > 0 && !confermaOk.StringValue.Equals("Sovrascrivi"))
                    {
                        presente = true;
                    }
                    else
                    {
                        string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                        errors = form.serializedValidation();
                        if (errors == null)
                        {
                            if (confermaOk.StringValue.Equals("Sovrascrivi"))
                            {
                                sql = "DELETE FROM Folders_Meta WHERE Folder_FolderMeta = " + ID + " AND Meta_FolderMeta = " + meta.StringValue;
                                PageData.Conn.Execute(sql);
                            }

                            form.serializedInsert();
                            if (form.LastOperation == NetForm.LastOperationValues.OK)
                            {
                                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha aggiunto con successo un meta tag alla cartella '" + Folder.Label + ", relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                                NetCms.GUI.PopupBox.AddSessionMessage("Meta aggiunto con successo.", PostBackMessagesType.Success, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                            
                                HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset(PageData.InfoToolbar.Title);
                                HtmlGenericControls.Div setcontent = new HtmlGenericControls.Div();
                                set.Controls.Add(setcontent);
                                setcontent.InnerHtml = "Meta aggiunto con successo.<br /><a href=\"" + PageData.BackAddress + "\">Torna Indietro</a>";
                            }
                        }
                        else
                        {
                            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non � riuscito ad aggiungere un meta tag alla cartella '" + Folder.Label + ", relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                            NetCms.GUI.PopupBox.AddSessionMessage(errors, PostBackMessagesType.Error);
                        }
                    }
                    
                }
                else
                {
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di inserimento di nuovi meta tag alla cartella '" + Folder.Label + ", relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                }

                if (presente)
                    form.AddSubmitButton = false;
                
                divset.Controls.Add(form.getForms(PageData.InfoToolbar.Title));
                div.Controls.Add(divset);

                if (presente)
                {
                    divset = new G2Core.XhtmlControls.Div();
                    Label labelPresente = new Label();
                    labelPresente.Text = "Meta della tipologia selezionata gi� presente. Vuoi sovrascriverlo?";
                    Button conferma = new Button();
                    conferma.ID = "conferma";
                    conferma.Text = "Sovrascrivi";
                    divset.Controls.Add(labelPresente);
                    divset.Controls.Add(conferma);
                    G2Core.XhtmlControls.Fieldset set = new G2Core.XhtmlControls.Fieldset("Conferma", divset);
                    set.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#ffffee");
                    div.Controls.Add(set);
                }

                return div;
            }

            public HtmlGenericControl GetModView()
            {
                if (Meta < 1)
                    PageData.GoBack();

                HtmlGenericControl div = new HtmlGenericControl("div");
                HtmlGenericControl divset = new G2Core.XhtmlControls.Div();

                G2Core.Common.RequestVariable metaDropDown = new G2Core.Common.RequestVariable("Meta_FolderMeta", G2Core.Common.RequestVariable.RequestType.Form);

                NetForm form = new NetForm();
                form.AddTable(FormTable);
                form.SubmitLabel = "Aggiorna Meta";
                string errors = "";

                //string Utente = PageData.Account.NomeCompleto + " (" + PageData.Account.UserName + ")";
                //string logText = "L'utente " + Utente + " vuole modificare il meta tag " + getMetaName(Meta.ToString()) + " per la cartella " + PageData.CurrentFolder.Nome + " (" + PageData.CurrentFolder.ID + ")";
                //PageData.GuiLog.SaveLogRecord(logText, false);

                PageData.InfoToolbar.Icon = "Meta";
                PageData.InfoToolbar.Title = "Aggiornamento Meta della Cartella '" + Folder.Label + "'";
                PageData.InfoToolbar.ShowFolderPath = true;

                bool presente = false;

                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                if (PageData.IsPostBack)
                {
                    DataTable tab=null;
                    G2Core.Common.RequestVariable confermaOk = new G2Core.Common.RequestVariable("conferma");
                    if (metaDropDown.StringValue != getMetaType(Meta.ToString()))
                    {
                        string sql = "SELECT * FROM Folders_Meta WHERE Folder_FolderMeta = " + ID + " AND Meta_FolderMeta = " + metaDropDown.StringValue;
                        tab = PageData.Conn.SqlQuery(sql);
                        if (tab.Rows.Count > 0 && !confermaOk.StringValue.Equals("Sovrascrivi"))
                        {
                            presente = true;
                        }
                    }

                    if (!presente)
                    {
                        errors = form.serializedValidation();
                        if (errors == null)
                        {
                            if (confermaOk.StringValue.Equals("Sovrascrivi"))
                            {
                                string sql = "DELETE FROM Folders_Meta WHERE Folder_FolderMeta = " + ID + " AND Meta_FolderMeta = " + metaDropDown.StringValue;
                                PageData.Conn.Execute(sql);
                            }

                            form.serializedUpdate();
                            PageData.MsgBox.InnerHtml = errors;

                            if (form.LastOperation == NetForm.LastOperationValues.OK)
                            {
                                HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset(PageData.InfoToolbar.Title);
                                HtmlGenericControls.Div setcontent = new HtmlGenericControls.Div();
                                set.Controls.Add(setcontent);

                                setcontent.InnerHtml = "Meta aggiornato con successo.<br /><a href=\"" + PageData.BackAddress + "\">Torna Indietro</a>";
                                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha modificato il meta tag della cartella '" + PageData.CurrentFolder.Nome + "' (id=" + PageData.CurrentFolder.ID + "), relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                                NetCms.GUI.PopupBox.AddSessionMessage("Meta aggiornato con successo.", PostBackMessagesType.Success, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                            }
                        }
                        else
                        {
                            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non � riuscito ad aggiungere un meta tag alla cartella '" + PageData.CurrentFolder.Nome + "' (id=" + PageData.CurrentFolder.ID + "), relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);                        
                            NetCms.GUI.PopupBox.AddSessionMessage(errors, PostBackMessagesType.Error);
                        }
                    }
                }
                else {
                    
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagine di modifica del meta tag della cartella '" + PageData.CurrentFolder.Nome + "' (id=" + PageData.CurrentFolder.ID + "), relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);                        
                }

                if (presente)
                    form.AddSubmitButton = false;

                divset.Controls.Add(form.getForms(PageData.InfoToolbar.Title));
                div.Controls.Add(divset);

                if (presente)
                {
                    divset = new G2Core.XhtmlControls.Div();
                    Label labelPresente = new Label();
                    labelPresente.Text = "Meta della tipologia selezionata gi� presente. Vuoi sovrascriverlo?";
                    Button conferma = new Button();
                    conferma.ID = "conferma";
                    conferma.Text = "Sovrascrivi";
                    divset.Controls.Add(labelPresente);
                    divset.Controls.Add(conferma);
                    G2Core.XhtmlControls.Fieldset set = new G2Core.XhtmlControls.Fieldset("Conferma", divset);
                    set.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#ffffee");
                    div.Controls.Add(set);
                }

                return div;
            }          
                         
            public HtmlGenericControl GetListView()
            {
                PageData.SideToolbar.addButton(new SideToolbarButton("Meta", "cms/folders_meta_add.aspx?folder=" + ID,NetCms.GUI.GuiUtility.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Toolbar_Meta_Add), "Aggiungi Meta", PageData, true));

                PageData.InfoToolbar.Icon = "Meta";
                PageData.InfoToolbar.Title = "Meta della cartella '" + Folder.Label + "'";
                PageData.InfoToolbar.ShowFolderPath = true;

                HtmlGenericControl div = new HtmlGenericControl("div");

                //string Utente = PageData.Account.NomeCompleto + " (" + PageData.Account.UserName + ")";
                //string logText = "L'utente " + Utente + " vuole impostare i meta tag della cartella " + PageData.CurrentFolder.Nome + " (" + PageData.CurrentFolder.ID + ")";
                //PageData.GuiLog.SaveLogRecord(logText, false);
                        

                #region Eliminazione Record

                if (PageData.IsPostBack)
                {
                    NetUtility.RequestVariable DeleteRequest = new NetUtility.RequestVariable("del", NetUtility.RequestVariable.RequestType.Form);

                    if (DeleteRequest.IsValidInteger){

                        //Utente = PageData.Account.NomeCompleto + " (" + PageData.Account.UserName + ")";
                        //logText = "L'utente " + Utente + " elimina il meta tag " + getMetaName(DeleteRequest.ToString());
                        //PageData.GuiLog.SaveLogRecord(logText);
                        string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha eliminato il meta tag '" + getMetaName(DeleteRequest.ToString()) + "' dalla cartella '" + PageData.CurrentFolder.Nome + "' (id=" + PageData.CurrentFolder.ID + "), relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        PageData.Conn.Execute("DELETE FROM Folders_Meta WHERE id_FolderMeta = " + DeleteRequest.IntValue);
                    }
                }
                else {
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di gestione dei meta tag della cartella '" + PageData.CurrentFolder.Nome + "' (id=" + PageData.CurrentFolder.ID + "), relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);                    
                }

                #endregion

                string sql = "SELECT * FROM Folders_Meta";
                sql += " INNER JOIN Meta ON id_Meta = Meta_FolderMeta ";
                sql += " INNER JOIN Folders ON id_Folder = Folder_FolderMeta";
                sql += " INNER JOIN FoldersLang ON id_Folder = Folder_FolderLang";
                sql += " WHERE id_Folder = " + ID + " AND Lang_FolderLang = " + PageData.DefaultLang;
                
                NetTable.SmTable table = new NetTable.SmTable(sql,PageData.Conn);

                NetTable.SmTableColumn nome = new NetTable.SmTableColumn("Name_Meta","Name");
                table.addColumn(nome);

                NetTable.SmTableColumn content = new NetTable.SmTableColumn("Content_FolderMeta","Content");
                content.CutTextAt = 150;
                table.addColumn(content);

                NetTable.SmTableBoleanColumn stato = new NetTable.SmTableBoleanColumn("Stato_FolderMeta", "Stato","Attivo","Non Attivo");
                table.addColumn(stato);
                
                NetTable.SmTableActionColumn modifica = new NetTable.SmTableActionColumn("id_FolderMeta", "action modify", "Modifica", "Folders_meta_mod.aspx?meta={0}&amp;Folder="+ID);
                table.addColumn(modifica);

                NetTable.SmTableActionColumn elimina = new NetTable.SmTableActionColumn("id_FolderMeta","action delete","Elimina","javascript: setdel({0});");
                table.addColumn(elimina);

                div.Controls.Add(table.getTable());

                return div;
            }
            
        }
    }
}