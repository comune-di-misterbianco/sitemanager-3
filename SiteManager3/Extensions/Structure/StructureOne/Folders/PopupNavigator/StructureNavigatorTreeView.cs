using System;
using System.Data;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

using NetForms;
using NetCms;
using NetCms.Structure.WebFS;
/// <summary>
/// Summary description for NetField
/// </summary>

namespace NetCms
{
    namespace Structure
    {
        public class StructureNavigatorMenuTreeView
        {
            private TreeNode Tree;
            public bool IsStatic
            {
                get
                {
                    return _IsStatic;

                }
                set
                {
                    _IsStatic = value;
                }
            }  
            private bool _IsStatic = false;

            private string FieldName;
            private PageData PageData;
            
            private int idSeed = 0;
            
            private int _FolderType;
            public int FolderType
            {
                get { return _FolderType; }
                set { _FolderType = value; }
            }
	
            public StructureNavigatorMenuTreeView(string fieldname, PageData pagedata, TreeNode tree)
            {
                FieldName = fieldname;
                Tree = tree;
                PageData = pagedata;
            }

            public HtmlGenericControl getControl()
            {
                HtmlGenericControl par = new HtmlGenericControl("p");
                par.Attributes["style"] = "text-align: left;";
                HtmlInputHidden hidden;

                hidden = new HtmlInputHidden();
                hidden.ID = FieldName;

                par.Controls.Add(hidden);
                par.Controls.Add(getTree());

                return par;
            }


            private Control getTree()
            {
                idSeed = 0;
                HtmlGenericControl div = new HtmlGenericControl("div");
                div.Attributes["style"] = "width:250px;text-align: left;";
                HtmlGenericControl script = new HtmlGenericControl("script");

                div.Controls.Add(script);
                script.Attributes["type"] = "text/javascript";
                string html = "\n";
                html += "<!--\n";

                html += "\nfunction setSelectedNode(value){\n";

                html += "   document.getElementById(\"" + FieldName + "\").value = value; \n";
                //if (AutoPostBack)
                {
                    html += "   document.getElementById(\"PageForm\").submit();\n";
                }
                html += "}\n";
                if (IsStatic)
                    html += "	d = new dTree('d',true);\n";
                else
                    html += "	d = new dTree('d');\n";
                html += getNodes(Tree, idSeed++, -1);

                html += "		document.write(d);\n";
                if (IsStatic)
                    html += "		javascript: d.openAll();\n";
                html += "//-->\n";
                script.InnerHtml = html;

                return div;
            }

            private string getNodes(TreeNode node, int id, int padre)
            {
                string html = "";
                StructureFolder folder = PageData.RootFolder.FindFolderByID(int.Parse(node.Value)) as StructureFolder;

                if (folder != null)
                {
                    if (FolderType == 0 || folder.Tipo == FolderType || folder.SubFolders.Count > 0)
                    {
                        string image = PageData.RootDist + "App_images/dtree/folder.gif";
                        string image_open = PageData.RootDist + "App_images/dtree/folderopen.gif";
                        string js_command = "javascript: setSelectedNode(\\\'" + node.Value + "\\\')";

                        if (node.Value != "0" || (!node.Checked && node.ChildNodes.Count == 0))
                        {
                            if (!node.Checked || (folder.Tipo != FolderType && FolderType!= 0 ))
                            {
                                image = PageData.RootDist + "App_images/dtree/redfolder.gif";
                                image_open = PageData.RootDist + "App_images/dtree/redfolderopen.gif";
                                js_command = "javascript: alert(\\\'Attenzione non � possibile prendere allegati dalla cartella selezionata\\\')";
                            }
                            string strNode = node.Text.Replace("'", "\\\'");
                            strNode = strNode.Replace("\"","\\\"");

                            html = "d.add(" + id + "," + padre + ",\"" + strNode + "\",'" + js_command + "','','','" + image + "','" + image_open + "'); \n";
                            foreach (TreeNode childnode in node.ChildNodes)
                            {
                                html += getNodes(childnode, idSeed++, id);
                            }
                        }
                    }
                }
                return html;
            }
        }
    }
}