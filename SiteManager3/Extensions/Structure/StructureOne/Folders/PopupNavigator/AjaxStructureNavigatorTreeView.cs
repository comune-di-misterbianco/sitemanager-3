using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using NetForms;
using NetCms;
/// <summary>
/// Summary description for NetField
/// </summary>

namespace NetCms.Structure
{
        public class AjaxStructureNavigatorMenuTreeView
        {
            private string FieldName;
            private PageData PageData;
            
            private int _FolderType;
            public int FolderType
            {
                get { return _FolderType; }
                set { _FolderType = value; }
            }

            public AjaxStructureNavigatorMenuTreeView(string fieldname, PageData pagedata)
            {
                FieldName = fieldname;
                PageData = pagedata;
            }

            public HtmlGenericControl getControl()
            {
                HtmlGenericControl par = new HtmlGenericControl("p");
                par.Attributes["style"] = "text-align: left;";
                HtmlInputHidden hidden;

                hidden = new HtmlInputHidden();
                hidden.ID = FieldName;

                par.Controls.Add(hidden);
                HtmlGenericControl AjaxTree = new HtmlGenericControl("ul");
                AjaxTree.ID = "FolderPickerAjaxTree";
                par.Controls.Add(AjaxTree);

//                this.PageData.OuterScripts += @"
//    <link rel=""stylesheet"" href=""" + this.PageData.Paths.AbsoluteRoot + @"/scripts/jquery-treeview/jquery.treeview.css"" />
//    <link rel=""stylesheet"" href=""" + this.PageData.Paths.AbsoluteRoot + @"/scripts/jquery-treeview/red-treeview.css"" />
//	<link rel=""stylesheet"" href=""" + this.PageData.Paths.AbsoluteRoot + @"/scripts/screen.css"" />
//	
//	<script src=""" + this.PageData.Paths.AbsoluteRoot + @"/scripts/jquery-treeview/lib/jquery.js"" type=""text/javascript""></script>
//
//	<script src=""" + this.PageData.Paths.AbsoluteRoot + @"/scripts/jquery-treeview/lib/jquery.cookie.js"" type=""text/javascript""></script>
//	<script src=""" + this.PageData.Paths.AbsoluteRoot + @"/scripts/jquery-treeview/jquery.treeview.js"" type=""text/javascript""></script>
//	<script src=""" + this.PageData.Paths.AbsoluteRoot + @"/scripts/jquery-treeview/jquery.treeview.async.js"" type=""text/javascript""></script>
//	<script type=""text/javascript"">
//	$(document).ready(function(){
//		$(""#FolderPickerAjaxTree"").treeview({
//			url: """ + PageData.Paths.AbsoluteNetworkRoot + @"/cms/ajaxfolderpopup.aspx""
//		})
//	});
//	</script>
//        ";
                HtmlGenericControl script = new HtmlGenericControl("script");

                par.Controls.Add(script);
                script.Attributes["type"] = "text/javascript";
                string html = "\n";
                html += "<!--\n";
                html += "\nfunction setSelectedNode(value){\n";
                html += "   document.getElementById(\"" + FieldName + "\").value = value; \n";
                html += "   document.getElementById(\"PageForm\").submit();\n";
                html += "}\n";
                html += "//-->\n";
                script.InnerHtml = html;

                return par;
            }
        }
}