using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms
{
    public class CmsPopup:PopupNavigator
    {
        protected override int FolderType
        {
            get { return 0; }
        }
        private string TargetControlID;
        public CmsPopup(PageData pageData, string targetControlID)
            :base(pageData)
        {
            TargetControlID = targetControlID;
        }

        protected override HtmlGenericControl SendBox()
        {
            HtmlGenericControl output = new HtmlGenericControl("div");
            output.Attributes["class"] = "send_imageblock";
            output.ID = "send_imageblock";

            HtmlInputHidden selectedImage = new HtmlInputHidden();
            selectedImage.ID = "selectedImage";

            #region Selezionato

            HtmlGenericControl div = new HtmlGenericControl("div");
            output.Controls.Add(div);

            Label label = new Label();
            /*label.AssociatedControlID = "Selezionato";
            label.Text = "File Selezionato ";
            div.Controls.Add(label);
            */
            HtmlGenericControl Selezionato = new HtmlGenericControl("p");

            Selezionato.InnerHtml = "<label for=\"Selezionato\">";
            Selezionato.InnerHtml += "File Selezionato";
            Selezionato.InnerHtml += " </label>";

            Selezionato.InnerHtml += "<input ";
            Selezionato.InnerHtml += " id=\"Selezionato\"";
            Selezionato.InnerHtml += " name=\"Selezionato\"";
            Selezionato.InnerHtml += " type=\"text\"";
            Selezionato.InnerHtml += " size=\"60\"";

            /*TextBox Selezionato = new TextBox();
            Selezionato.ID = "Selezionato";
            Selezionato.Columns = 60;*/
            
            NetUtility.RequestVariable SelFolderID = new NetUtility.RequestVariable(FolderTreeID, PageData.Request.Form);
            if(SelFolderID.IsValid(NetUtility.RequestVariable.VariableType.Integer) && SelFolderID.IntValue>0)
            {
                StructureFolder SelectedFolder = PageData.RootFolder.FindFolderByID(SelFolderID.IntValue) as StructureFolder;
                if(SelectedFolder!=null)
                    //Selezionato.InnerHtml += " value=\"" + NetCms.Structure.Utility.ImportContentFilter.WebRoot + SelectedFolder.Path + "\"";
                    //Era cos� prima della prova con il segnaposto
                    Selezionato.InnerHtml += " value=\"" + PageData.Paths.AbsoluteWebRoot + SelectedFolder.Path +"\"";

            }
            Selezionato.InnerHtml += " />";
 
            div.Controls.Add(Selezionato);

            #endregion

            #region Inserisci

            div = new HtmlGenericControl("div");
            output.Controls.Add(div);
            div.ID = "send_imageblock_dx";

            HtmlInputHidden target = new HtmlInputHidden();
            target.ID = "TargetFreeTextBox";
            target.Value = PageData.Request.QueryString["ftb"];
            div.Controls.Add(target);

            HtmlGenericControl button = new HtmlGenericControl("div");
            div.Controls.Add(button);
            button.InnerHtml = @"<input onclick=""SendCMS('"+ TargetControlID +@"')"" type=""button"" id=""send"" name=""send"" value=""Inserisci"" />";

            #endregion

            #region Javascript Code

            HtmlGenericControl script = new HtmlGenericControl("script");
            script.Attributes["type"] = "text/javascript";
            script.InnerHtml += @"
                            function SendCMS(target)
                            {
                                oOpener = window.opener;
                                oTarget = oOpener.document.getElementById(target);
                                oSelected = document.getElementById(""Selezionato"");
                                oTarget.value = oSelected.value;
                                window.close();
                            }
                            ";
            output.Controls.Add(script);

            #endregion

            return output;
        }

    }

}



