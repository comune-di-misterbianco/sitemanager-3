using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;
using System.Linq;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms
{
    public abstract class PopupNavigator
    {
        protected const string FolderTreeID = "StructureFolderSideTree";
        protected PageData PageData;

        protected virtual HtmlGenericControl SplashBox
        {
            get
            {
                return null;
            }
        }

        protected abstract int FolderType
        {
            get;
        }

        public PopupNavigator(PageData pageData)
        {
            PageData = pageData;
        }

        public HtmlGenericControl GetView()
        {
            HtmlGenericControl output = new HtmlGenericControl("div");

            output.Controls.Add(FoldersBox());
            output.Controls.Add(PreviewBox());
            output.Controls.Add(DocsBox());
            output.Controls.Add(SendBox());
           
            return output;
        }

        protected virtual HtmlGenericControl FoldersBox()
        {
            HtmlGenericControl output = new HtmlGenericControl("div");
            output.Attributes["class"] = "folders_tree";
            //Structure.StructureFolderBinder binder = new Structure.StructureFolderBinder(PageData);
            //TreeNode node = binder.getTree();

           // Structure.StructureNavigatorMenuTreeView tree = new Structure.StructureNavigatorMenuTreeView(FolderTreeID, PageData, node);
            //tree.FolderType = FolderType;

            HtmlGenericControl menu = new HtmlGenericControl("div");
            menu.Attributes["class"] = "Tree";
            menu.InnerHtml = "<div class=\"title\">Menu</div>";

            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "sideTree";
            menu.Controls.Add(div);
            //div.Controls.Add(tree.getControl());

            div.Controls.Add(new Structure.AjaxStructureNavigatorMenuTreeView(FolderTreeID, PageData).getControl());
            output.Controls.Add(div);

            if (SplashBox != null)
            {
                div = new HtmlGenericControl("div");
                div.InnerHtml = "<br /><p><a href=\""+PageData.Request.Url.ToString()+"\"/>Mostra Istruzioni</a></p>";
                output.Controls.Add(div);
            }


            return output;
        }

        protected virtual HtmlGenericControl DocsBox()
        {
            HtmlGenericControl output = new HtmlGenericControl("div");
            output.Attributes["class"] = "docs_block";
            output.ID = "docs_block";
            if (PageData.Request.Form["StructureFolderSideTree"] != null)
            {
                string folderid = PageData.Request.Form["StructureFolderSideTree"];
                HtmlGenericControl table = new HtmlGenericControl("table");
                table.Attributes["class"] = "table_gallery";
                table.Attributes["cellpadding"] = "0";
                table.Attributes["cellspacing"] = "0";
                output.Controls.Add(table);

                table.InnerHtml += "<tr>";
                table.InnerHtml += "<th>";
                table.InnerHtml += "Nome";
                table.InnerHtml += "</th>";
                table.InnerHtml += "<th>";
                table.InnerHtml += "Data";
                table.InnerHtml += "</th>";
                table.InnerHtml += "<th>";
                table.InnerHtml += "Seleziona";
                table.InnerHtml += "</th>";
                table.InnerHtml += "</tr>";


                StructureFolder folder = PageData.RootFolder.FindFolderByID(int.Parse(folderid)) as StructureFolder;
                if (folder.Documents.Count > 11)
                    output.Attributes["class"] = "docs_block over";
                for (int i = 0; i < folder.Documents.Count; i++)
                {
                    Document doc = folder.Documents.ElementAt(i) as Document;
                    if (doc.Grant && ShowDoc(doc))
                    {
                        //string Link = PageData.Paths.AbsoluteWebRoot + folder.Documents[i].Link.Replace("'", "\\\'");

                        string Link = doc.FrontendLink.Replace("'", "\\\'"); //.Replace(NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot, NetCms.Structure.Utility.ImportContentFilter.WebRoot);

                        //string Link = "";
                        //if (!string.IsNullOrEmpty(NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot))
                        //    Link = doc.FrontendLink.Replace("'", "\\\'").Replace(NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot, NetCms.Structure.Utility.ImportContentFilter.WebRoot);
                        //else
                        //    Link = doc.FrontendLink.Replace("'", "\\\'");

                        string Desc = (doc.DocLangs.ElementAt(0).Descrizione != null) ? doc.DocLangs.ElementAt(0).Descrizione.Replace("'", "\\\'") : "";

                        table.InnerHtml += "<tr>";
                        table.InnerHtml += "<td>";
                        //table.InnerHtml += doc.PhysicalName;
                        table.InnerHtml += doc.Nome;
                        table.InnerHtml += "</td>";
                        table.InnerHtml += "<td>";
                        if (doc.Extension.Length > 0 && (
                            doc.Extension.ToLower() == "jpg" ||
                            doc.Extension.ToLower() == "png" ||
                            doc.Extension.ToLower() == "gif")
                            )
                        {
                            table.InnerHtml += "<a href=\"javascript: previewImage('" + PageData.Paths.AbsoluteWebRoot + doc.FullName.Replace("'", "\\\'") + "');\">Anteprima</a>";
                        }
                        table.InnerHtml += "&nbsp;";
                        table.InnerHtml += "</td>";
                        table.InnerHtml += "<td>";
                        table.InnerHtml += "<a href=\"javascript: selectFile('" + Link + "','" + Desc + "');\">Seleziona</a>";
                        table.InnerHtml += "</td>";
                        table.InnerHtml += "</tr>";
                    }
                }

            }
            else
                if(SplashBox!=null)
                    output.Controls.Add(SplashBox);
            return output;
        }

        protected virtual HtmlGenericControl PreviewBox()
        {
            HtmlGenericControl output = new HtmlGenericControl("div");
            output.Attributes["class"] = "preview_block";
            output.ID = "preview_block";
            HtmlGenericControl iframe = new HtmlGenericControl("iframe");
            iframe.Attributes["class"] = "preview_frame";
            iframe.Attributes["scrolling"] = "auto";
            iframe.ID = "preview_frame";
            
            output.Controls.Add(iframe);
            return output;
        }

        protected abstract HtmlGenericControl SendBox();

        //Filtra i documenti in modo da poter visualizzare solo un tipo di documento(utile per i tipi degli allegati)
        protected virtual bool ShowDoc(Document doc)
        {
            return true;
        }
    }

}



