using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for xNode
/// </summary>
namespace NetCms.Users
{
    public class StructureGroups
    {

        public Group Root
        {
            get
            {
                if (_Root == null)
                    buildRoot();
                return _Root;
            }
        }
        private Group _Root;

        private PageData PageData;

        public StructureGroups(PageData pageData)
        {
            PageData = pageData;
        }

        public void buildRoot()
        {

            //DataTable table = PageData.Conn.SqlQuery("SELECT * FROM groups ORDER BY Tree_group");

            ICollection<Group> gruppi = GroupsBusinessLogic.FindAllOrderedByTree();

            //DataTable users = PageData.Conn.SqlQuery("SELECT * FROM Users INNER JOIN usersgroups ON (users.id_User = usersgroups.User_UserGroup)");

            Group root = null;

            //string groupName;
            //string groupID;
            //int groupProfile;
            //int groupDepth;
            //string groupTree;
            Group lastgroup = root;
            for (int i = 0; i < gruppi.Count; i++)
            {
                Group group = gruppi.ElementAt(i);

                //DataRow row = table.Rows[i];
                //groupName = row["Name_group"].ToString();
                //groupTree = row["Tree_Group"].ToString();
                //groupID = row["id_group"].ToString();
                //groupProfile = int.Parse(row["Profile_group"].ToString());
                //groupDepth = int.Parse(row["Depth_group"].ToString());
                //UserStruct[] groupUsers = buildUsers(users, group.ID);
                //Group group = new Group(row);

                if (i == 0)
                    root = group;
                else
                {
                    if (group.Depth == lastgroup.Depth)
                        lastgroup.Parent.Groups.Add(group);

                    if (group.Depth > lastgroup.Depth)
                        lastgroup.Groups.Add(group);

                    if (group.Depth < lastgroup.Depth)
                    {
                        int groupGap = lastgroup.Depth - group.Depth;
                        Group parent = lastgroup;
                        for (int p = 0; p <= groupGap; p++)
                            parent = parent.Parent;
                        if (parent != null)
                        {
                            parent.Groups.Add(group);
                        }
                    }
                }
                if (group != null)
                    lastgroup = group;
            }

            //table.Dispose();
            _Root = root;
        }


        // NON SEMBRA ESSERE USATO
        //public UserStruct[] buildUsers(DataTable users, int group)
        //{
        //    DataRow[] rows = users.Select(" Group_UserGroup = " + group);
        //    UserStruct[] groupUsers = new UserStruct[rows.Length];

        //    for (int i = 0; i < groupUsers.Length; i++)
        //    {
        //        groupUsers[i].ID = int.Parse(rows[i]["id_User"].ToString());
        //        groupUsers[i].Profile = int.Parse(rows[i]["Profile_User"].ToString());
        //        groupUsers[i].Type = int.Parse(rows[i]["Type_UserGroup"].ToString());
        //    }
        //    return groupUsers;
        //}
    }
}