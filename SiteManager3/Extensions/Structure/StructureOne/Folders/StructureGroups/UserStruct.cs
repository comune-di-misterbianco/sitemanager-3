using System;
using System.Collections.Generic;
using System.Text;

namespace NetCms
{
    namespace Users
    {
        public struct UserStruct
        {
            private int id;

            public int ID
            {
                get { return id; }
                set { id = value; }
            }

            private int _Profile;

            public int Profile
            {
                get { return _Profile; }
                set { _Profile = value; }
            }

            private int _Type;

            public int Type
            {
                get { return _Type; }
                set { _Type = value; }
            }

            private int _Revisor;

            public int Revisor
            {
                get { return _Revisor; }
                set { _Revisor = value; }
            }
	
        }
    }
}
