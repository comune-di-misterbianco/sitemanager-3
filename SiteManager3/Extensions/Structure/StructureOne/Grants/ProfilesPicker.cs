
//CLASSE INTERAMENTE COMMENTATA PERCHE' NON USATA - VERIFICARE CHE SIA REALMENTE COSI'
//using System;
//using System.Data;
//using System.Web.UI.HtmlControls;
//using NetTable;
//using System.Linq;
///// <summary>
///// Summary description for Class1
///// </summary>
//namespace NetCms
//{
//    namespace Structure
//    {
//        public partial class ProfilesPicker
//        {
//            private string sqlExecuteModel;
//            public string SqlExecuteModel
//            {
//                get { return sqlExecuteModel; }
//                set { sqlExecuteModel = value; }
//            }

//            private string _Title;
//            public string Title
//            {
//                get { return _Title; }
//                set { _Title = value; }
//            }

//            private string sqlSelectModel;
//            public string SqlSelectModel
//            {
//                get { return sqlSelectModel; }
//                set { sqlSelectModel = value; }
//            }

//            private string groupsSelectCriteria;
//            public string GroupsSelectCriteria
//            {
//                get 
//                {
//                    if (groupsSelectCriteria == null)
//                    {
//                        string gsql = "";
//                        gsql = " INNER JOIN groups_profile ON (groups.Profile_Group = groups_profile.id_Profile)";

//                        string conditions = "";
//                        for (int i = 0; i < PageData.Account.Groups.Count; i++)
//                        {
//                            NetCms.Users.GroupAssociation GroupAssociation = PageData.Account.AssociatedGroupsOrdered.ElementAt(i);

//                            if (GroupAssociation.IsAdministrator)
//                            {
//                                conditions += " OR ";
//                                conditions += " Tree_Group LIKE '" + GroupAssociation.Group.Tree + "%'";
//                            }
//                            if (GroupAssociation.GrantsRole == NetCms.Users.GrantAdministrationTypes.Administrator)
//                                conditions += " AND (id_User <> " + PageData.Account.ID + ")";
//                        }
//                        //La condizione 1=0 serve ad esculudere tutti i record tranne quelli selezionati dalle
//                        //condizioni specificate nella variabile conditions
//                        gsql += " AND (1=0" + conditions + ")";
//                        groupsSelectCriteria = gsql;
//                    }
//                    return groupsSelectCriteria; 
//                }
                
//            }

//            private bool _FilterGroups;
//            public bool FilterGroups
//            {
//                get { return _FilterGroups; }
//                set { _FilterGroups = value; }
//            }

//            private bool _UserGroups;
//            public bool UserGroups
//            {
//                get { return _UserGroups; }
//                set { _UserGroups = value; }
//            }

//            private string usersSelectCriteria;
//            public string UsersSelectCriteria
//            {
//                get 
//                {
//                    if (usersSelectCriteria == null)
//                    {
//                        

//                        //string sql = "SELECT id_User,Name_User,Profile_User,Anagrafica.* FROM Users";
//                        //sql += " INNER JOIN anagrafica ON (Anagrafica_User = id_Anagrafica)";
//                        //sql += " INNER JOIN Profiles ON (Profile_User = id_Profile)";
//                        //sql += " INNER JOIN UsersGroups ON (User_UserGroup = id_User)";
//                        //sql += " INNER JOIN Groups ON (Group_UserGroup = id_Group)";

//                        //string conditions = "";
//                        //for (int i = 0; i < PageData.Account.Groups.Count; i++)
//                        //{
//                        //    NetCms.Users.GroupAssociation GroupAssociation = PageData.Account.AssociatedGroupsOrdered.ElementAt(i);

//                        //    if (GroupAssociation.IsAdministrator)
//                        //    {
//                        //        conditions += " OR ";
//                        //        conditions += " Tree_Group LIKE '" + GroupAssociation.Group.Tree + "%'";
//                        //    }
//                        //    if (GroupAssociation.GrantsRole == NetCms.Users.GrantAdministrationTypes.Administrator)
//                        //        conditions += " AND (id_User <> " + PageData.Account.ID + ")";

//                        //}
//                        ////La condizione 1=0 serve ad esculudere tutti i record tranne quelli selezionati dalle
//                        ////condizioni specificate nella variabile conditions
//                        //sql += " AND Deleted_User = 0 AND (1=0" + conditions + ")";

//                        //sql += " GROUP BY id_User,Name_User";
//                        //usersSelectCriteria = sql;
//                    }
//                    return usersSelectCriteria; 
//                }

//            }

//            private PageData PageData;

//            private bool queryExecuted = false;
//            public bool QueryExecuted
//            {
//                get
//                {
//                    return queryExecuted;
//                }
//            }

//            private int lastExecuteID = 0;
//            public int LastExecuteID
//            {
//                get
//                {
//                    return lastExecuteID;
//                }
//            }

//            public ProfilesPicker(string title, PageData pageData)
//            {
//                PageData = pageData;
//                _Title = title;
//            }

//            private HtmlGenericControls.InputField  _AddProfileField;
//            public HtmlGenericControls.InputField  AddProfileField
//            {
//                get 
//                {
//                    if (_AddProfileField == null)
//                    {
//                        _AddProfileField = new HtmlGenericControls.InputField("AddProfileField");

//                    }
//                    return _AddProfileField; 
//                }
//            }

//            public HtmlGenericControl Control
//            {
//                get
//                {
//                    HtmlGenericControls.Div set = new HtmlGenericControls.Div();
//                    set.Class = "FullSizeFieldset";

//                    #region Campi Hidden e Relativi Postback

//                    /****************************************************************************************************************************************/
//                    /****************  Aggiunta Profilo  ************************************************************************************/
//                    /****************************************************************************************************************************************/

//                    set.Controls.Add(AddProfileField.Control);

//                    if (PageData.IsPostBack && PageData.SubmitSource == AddProfileField.ID && AddProfileField.RequestVariable.IsValidInteger)
//                    {
//                        System.Data.DataTable sqltable = PageData.Conn.SqlQuery(string.Format(SqlSelectModel, AddProfileField.RequestVariable.StringValue));
//                        if (sqltable.Rows.Count == 0)
//                        {
//                            if(SqlExecuteModel.ToLower().StartsWith("insert "))
//                            {
//                                this.lastExecuteID = PageData.Conn.ExecuteInsert(string.Format(SqlExecuteModel, AddProfileField.RequestVariable.StringValue));
//                                this.queryExecuted = this.lastExecuteID>0;
//                            }
//                            else
//                                this.queryExecuted = PageData.Conn.Execute(string.Format(SqlExecuteModel, AddProfileField.RequestVariable.StringValue));
//                        }
//                    }

//                    #endregion


//                    HtmlGenericControls.Div title = new HtmlGenericControls.Div();
//                    title.Class = "FullSizeFieldset_Title";
//                    set.Controls.Add(title);
//                    title.InnerHtml = this.Title;

//                    NetUtility.SheetsControl sheets = new NetUtility.SheetsControl("MessagesSheets");
//                    HtmlGenericControl sheetcontent;

//                    #region Tabella dei Gruppi

//                    string GroupsSql = "SELECT * FROM Groups";
                    
//                    SmTable stable = new SmTable(GroupsSql+" ORDER BY Tree_Group", PageData.Conn);
//                    stable.CssClass = "GenericTable";

//                    SmTableTreeColumn2 treecol = new SmTableTreeColumn2("Gruppo", "id_Group", "Name_Group", "Depth_Group", PageData.Conn);
//                    treecol.IconType = SmTableTreeColumn2.IconTypes.CssClass;
//                    treecol.Icon = "GrantsTableGroupIcon";
//                    stable.addColumn(treecol);

//                    SmTableColumn col;
//                    if (FilterGroups)
//                    {
//                        GroupsSql += GroupsSelectCriteria;
//                        col = new ProfilePickerTableActionColumn("Profile_Group", "action add", "Aggiungi", "javascript: setFormValue('{0}','" + AddProfileField.ID + "',1)", PageData.Conn.SqlQuery(GroupsSql));
//                    }
//                    else
//                        col = new SmTableActionColumn("Profile_Group", "action add", "Aggiungi", "javascript: setFormValue('{0}','" + AddProfileField.ID + "',1)");
//                    stable.addColumn(col);

//                    sheetcontent = stable.getTable();
//                    sheets.Sheets.Add(new NetUtility.HtmlSheet("Gruppi", sheetcontent));

//                    #endregion

//                    #region Tabella dei Utenti

//                    HtmlGenericControls.Div div = new HtmlGenericControls.Div();
//                    FrontUserSelectControl users = new FrontUserSelectControl(PageData, "UsersCTR", 7, "javascript: setFormValue('{3}','" + AddProfileField.ID + "',1)", true,UsersSelectCriteria);
//                    div.Controls.Add(users);

//                    sheets.Sheets.Add(new NetUtility.HtmlSheet("Utenti", div));

//                    #endregion

//                    set.Controls.Add(sheets.GetControl());

//                    return set;
//                }
//            }

//            private class ProfilePickerTableActionColumn : SmTableColumn
//            {
//                private string Classe;
//                private string Label;
//                private string Href;
//                private string Title;
//                private DataTable Table;

//                public ProfilePickerTableActionColumn(string fieldName, string classe, string Caption, string href,DataTable table)
//                    : this(fieldName, classe, Caption, href, "", Caption,table)
//                {
//                    Table = table;
//                }
//                public ProfilePickerTableActionColumn(string fieldName, string classe, string Caption, string href, string title, DataTable table)
//                    : this(fieldName, classe, Caption, href, "", Caption,table)
//                {
//                    Table = table;
//                }

//                public ProfilePickerTableActionColumn(string fieldName, string classe, string Caption, string href, string title, string label, DataTable table)
//                    : base(fieldName, Caption)
//                {
//                    Classe = classe;
//                    Label = label;
//                    Href = href;
//                    Table = table;
//                    Title = title;
//                }
//                public override HtmlGenericControl getField(DataRow row)
//                {
//                    string value = row[this.FieldName].ToString();
//                    string DbLabel = row.Table.Columns.Contains(this.Label) ? row[this.Label].ToString() : Label;
//                    HtmlGenericControl td = new HtmlGenericControl("td");
//                    td.Attributes["class"] = Classe;
//                    string html = "";
//                    if (Table.Select("id_Group = " + row["id_Group"].ToString()).Length > 0)
//                    {
//                        html += "<a href=\"";
//                        html += Href.Replace("{0}", value);
//                        html += "\" title=\"" + (Title.Length > 0 ? Title : Label) + "\"><span>";
//                        html += DbLabel;
//                        html += "</span></a>";
//                    }
//                    else
//                    {
//                        html = "<span>";
//                        html += "&nbsp;";
//                        html += "</span>";
//                    }
//                    td.InnerHtml = html;
//                    return td;
//                }
//            }
//        }
//    }
//}

