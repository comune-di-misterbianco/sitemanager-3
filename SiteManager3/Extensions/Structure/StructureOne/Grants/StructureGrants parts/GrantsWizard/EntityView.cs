using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetUtility.NetTreeViewControl;
using NetCms.GUI;
using NetTable;
using NetCms.Networks.WebFS;
using System.Collections.Generic;
using NetCms.Users;
using System.Linq;
using NetCms.Grants.Interfaces;
using NetCms.Grants;
/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms
{
    namespace Structure
    {
        namespace Grants.Wizard
        {
            public class EntityView
            {
                private PageData PageData;

                //private GrantsObjectType Object;

                private IGrantsFindable Object;

                public EntityView(PageData pageData, IGrantsFindable obj)
                {
                    PageData = pageData;

                    Object = obj;

                }

                public void DeletePostBack()
                {
                    if (PageData.SubmitSource == "DeleteProfile")
                    {
                        NetUtility.RequestVariable Profile = new NetUtility.RequestVariable("DeleteProfile", PageData.Request.Form);
                        if(Profile.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                        {
                            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;      
                            //if (PageData.Conn.Execute("DELETE FROM " + Object.Extension + "Profiles WHERE id_" + Object.Extension + "Profile = " + Profile.IntValue))
                            if (Object.DeleteGrantProfile(Profile.IntValue))
                            {
                                //string Utente = PageData.Account.NomeCompleto + " (" + PageData.Account.UserName + ")";
                                //string logText = "L'utente " + Utente;
                                
                                if (Object.GrantObjectType == GrantObjectType.Folder)
                                {
                                    //logText += " vuole rimuovere il profilo " + Profile.IntValue + " dalle specifiche della cartella " + PageData.CurrentFolder.Nome + " (" + PageData.CurrentFolder.ID + ")";
                                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha rimosso il profilo " + Profile.IntValue + " dalle specifiche della cartella " + PageData.CurrentFolder.Nome + " (id=" + PageData.CurrentFolder.ID + ").", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);                                    
                                }

                                if (Object.GrantObjectType == GrantObjectType.Document)
                                {
                                    //logText += " vuole rimuovere il profilo " + Profile.IntValue + " alle specifiche del documento " + PageData.CurrentFolder.FindDoc(Object.ID.ToString()).Label;
                                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha rimosso il profilo " + Profile.IntValue + " dalle specifiche del documento " + PageData.CurrentFolder.FindDocByID(Object.ID).Nome + "' (id=" + Object.ID.ToString() + ").", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                                }

                                //PageData.GuiLog.SaveLogRecord(logText);

                                PageData.MsgBox.InnerHtml = "Rimozione effettuata con successo";
                            }
                        }
                    }
                }

                public HtmlGenericControl getView()
                {
                    HtmlGenericControl output = new HtmlGenericControl("div");

                    Collapsable col = new Collapsable("Informazioni sulla gestione dei Permessi");
                    string text = @"<p class=""info"">Questa pagina permette di creare associazioni tra gruppi, utenti e " + (Object.GrantObjectType == GrantObjectType.Folder ? "la cartella " : "il documento ") + ((WebFSObject)Object).Nome + @" al fine di impostare i permessi.</p>";
                    col.AppendControl(new LiteralControl(text));
                    output.Controls.Add(col);


                    string Target = Object.GrantObjectType == GrantObjectType.Document ? "Doc" : "Folder";
                    //string sqlInsertModel = "INSERT INTO {0}profiles ({0}_{0}Profile,Profile_{0}Profile,Creator_{0}Profile) Values (" + Object.ID + ",{1}," + PageData.Account.ID + ")";
                    //string sqlSelectModel = "SELECT * FROM {0}profiles WHERE {0}_{0}Profile = " + Object.ID + " AND Profile_{0}Profile = {1}";

                    ProfilesPickerForGrants picker = new ProfilesPickerForGrants("Gruppi e Utenti che � possibile aggiungere alle specifiche della cartella", this.PageData.Account,Object);
                    //picker.SqlExecuteModel = string.Format(sqlInsertModel, Target, "{0}");
                    //picker.SqlSelectModel = string.Format(sqlSelectModel, Target, "{0}");
                    //picker.FilterGroups = true;
                    //picker.GroupsSelectCriteria = " INNER JOIN profiles ON (groups.Profile_Group = profiles.id_Profile) WHERE Profile";
                    output.Controls.Add(picker.Control);
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    if (PageData.IsPostBack)
                    {
                        if (picker.ProfileInserted)
                        {
                            #region invalido la struttura

                            //SISTEMARE CON CACHE 2� LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                            PageData.CurrentFolder.Invalidate();

                            #endregion



                            //string Utente = PageData.Account.NomeCompleto + " (" + PageData.Account.UserName + ")";
                            //string logText = "L'utente " + Utente;
                            if (PageData.SubmitSource == "AddProfileField")
                            {
                                if (Object.GrantObjectType == GrantObjectType.Folder)
                                {
                                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha aggiunto il profilo " + picker.AddProfileField.RequestVariable.StringValue + " alle specifiche della cartella " + PageData.CurrentFolder.Nome + " (id=" + PageData.CurrentFolder.ID + ").", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                                    //logText += " vuole aggiungere il profilo " + picker.AddProfileField.RequestVariable.StringValue + " alle specifiche della cartella " + PageData.CurrentFolder.Nome + " (" + PageData.CurrentFolder.ID + ")";
                                }
                                if (Object.GrantObjectType == GrantObjectType.Document)
                                {
                                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha aggiunto il profilo " + picker.AddProfileField.RequestVariable.StringValue + " alle specifiche del documento " + PageData.CurrentFolder.FindDocByID(Object.ID).Nome + "' (id=" + Object.ID.ToString() + ").", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                                    //logText += " vuole aggiungere il il profilo " + picker.AddProfileField.RequestVariable.StringValue + " alle specifiche del documento " + PageData.CurrentFolder.FindDoc(Object.ID.ToString()).Label;
                                }
                            }

                            //PageData.GuiLog.SaveLogRecord(logText);


                        }
                        else
                        {
                            if (PageData.SubmitSource == "AddProfileField")
                            {
                                if (Object.GrantObjectType == GrantObjectType.Folder)
                                {
                                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non � riuscito ad aggiungere il profilo " + picker.AddProfileField.RequestVariable.StringValue + " alle specifiche della cartella " + PageData.CurrentFolder.Nome + " (id=" + PageData.CurrentFolder.ID + ").", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                                    //logText += " vuole aggiungere il profilo " + picker.AddProfileField.RequestVariable.StringValue + " alle specifiche della cartella " + PageData.CurrentFolder.Nome + " (" + PageData.CurrentFolder.ID + ")";
                                }
                                if (Object.GrantObjectType == GrantObjectType.Document)
                                {
                                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non � riuscito ad aggiungere  il profilo " + picker.AddProfileField.RequestVariable.StringValue + " alle specifiche del documento " + PageData.CurrentFolder.FindDocByID(Object.ID).Nome + "' (id=" + Object.ID.ToString() + ").", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                                    //logText += " vuole aggiungere il il profilo " + picker.AddProfileField.RequestVariable.StringValue + " alle specifiche del documento " + PageData.CurrentFolder.FindDoc(Object.ID.ToString()).Label;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Object.GrantObjectType == GrantObjectType.Folder)
                        {
                            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di gestione dei permessi per la cartella '" + PageData.CurrentFolder.Nome + "' (id=" + PageData.CurrentFolder.ID + ").", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        }

                        if (Object.GrantObjectType == GrantObjectType.Document)
                        {
                            NetworkDocument doc = PageData.CurrentFolder.FindDocByID(Object.ID);
                            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di gestione dei permessi per il documento '" + doc.Nome + "' (id=" + doc.ID + ").", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        }

                    }


                    HtmlGenericControls.Div set = new HtmlGenericControls.Div();
                    //set.Class = "FullSizeFieldset";
                    output.Controls.Add(set);

                    HtmlGenericControls.Div title = new HtmlGenericControls.Div();
                    title.Class = "FullSizeFieldset_Title";
                    set.Controls.Add(title);
                    title.InnerHtml = "Gruppi e Utenti presenti nelle specifiche della cartella";
                    
                    if (PageData.IsPostBack)
                    {
                           
                        DeletePostBack();
                    }
                        PageData.InfoToolbar.Icon = "Grants";

                    HtmlGenericControl table = new HtmlGenericControl("table");
                    table.Attributes["cellspacing"] = "0";
                    table.Attributes["cellpadding"] = "00";
                    table.Attributes["class"] = "tab";

                    PageData.InfoToolbar.ShowFolderPath = true;

                    NetCms.Users.Group Group = PageData.Groups;

                    HtmlGenericControl tr;
                    //****************************************************************
                    //lista di grppi e utenti

                    #region Header della tabella
                    tr = new HtmlGenericControl("tr");
                    HtmlGenericControl th;

                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "&nbsp;";
                    tr.Controls.Add(th);

                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "Nome";
                    tr.Controls.Add(th);

                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "Tipo";
                    tr.Controls.Add(th);

                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "Eredita da";
                    tr.Controls.Add(th);

                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "Dettagli Permessi";
                    tr.Controls.Add(th);

                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "Imposta Permessi";
                    tr.Controls.Add(th);

                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "Rimuovi";
                    tr.Controls.Add(th);

                    table.Controls.Add(tr);

                    #endregion

                    #region Corpo della tabella (Elenco Gruppi)

                   /* VECCHIO CODICE
                    string sql = "SELECT * FROM groups";
                    sql += " INNER JOIN (profiles inner join groups_profile on profiles.id_Profile = groups_profile.id_Profile) ON (groups.Profile_Group = groups_profile.id_Profile)";
                    sql += Object.SQLQuery;

                    string conditions = "";
                    for (int i = 0; i < PageData.Account.Groups.Count; i++)
                    {
                        NetCms.Users.GroupAssociation GroupAssociation = PageData.Account.AssociatedGroupsOrdered.ElementAt(i);

                        if (GroupAssociation.IsAdministrator)
                        {
                            conditions += " OR ";
                            conditions += " Tree_Group LIKE '" + GroupAssociation.Group.Tree + "%'";
                        }
                        //COME SUPPONEVO, QUESTA COSA NON FUNZIONA (By Giovanni)
                        if (GroupAssociation.GrantsRole == NetCms.Users.GrantAdministrationTypes.Administrator)
                            conditions += " AND (id_User <> " + PageData.Account.ID + ")";  
                    }
                    //La condizione 1=0 serve ad esculudere tutti i record tranne quelli selezionati dalle
                    //condizioni specificate nella variabile conditions
                    sql += " AND (1=0" + conditions + ")";

                    DataTable dataTable = PageData.Conn.SqlQuery(sql); */

                    /* 
                     Dall'interpretazione del codice originale, sembra selezionare i profili (di documenti o cartelle) relativi ai gruppi a partire dall'elenco
                     * dei gruppi associati all'utente corrente. Per ciascun gruppo associato all'utente corrente, se l'utente � amministratore
                     * o super amministratore, si seleziona il gruppo in funzione del tree. Inoltre se l'utente � solo amministratore, non permette di gestire i permessi
                     * del gruppo di cui � amministratore.
                     */

                    var resObjectGroups = new List<ObjectProfile>();
                    if (Object.GrantObjectProfiles != null)
                    {
                        resObjectGroups = (from pro in Object.GrantObjectProfiles
                               from ga in PageData.Account.AssociatedGroupsOrdered
                               where pro.Profile.IsGroup &&
                               (ga.IsAdministrator && (pro.Profile as GroupProfile).Group.Tree.StartsWith(ga.Group.Tree)) &&
                               ((ga.GrantsRole == GrantAdministrationTypes.Administrator && (pro.Profile as GroupProfile).Group.Users.Where(x => x.ID == PageData.Account.ID).Count() == 0) || ga.GrantsRole != GrantAdministrationTypes.Administrator)
                               select pro).ToList();
                    }
                    foreach (ObjectProfile objProfile in resObjectGroups)
                    {
                        tr = new HtmlGenericControl("tr");

                        HtmlGenericControl td;
                        td = new HtmlGenericControl("td");
                        td.InnerHtml = "&nbsp;";
                        td.Attributes["class"] = "icon_group";
                        tr.Controls.Add(td);

                        td = new HtmlGenericControl("td");
                        td.InnerHtml = objProfile.Profile.EntityName; //row["Name_Group"].ToString();
                        tr.Controls.Add(td);

                        td = new HtmlGenericControl("td");
                        td.InnerHtml = "Gruppo";
                        tr.Controls.Add(td);

                        string family = "&nbsp;";
                        //NetCms.Users.Group parent = Groups.Groups.FindGroup(row["Parent_Group"].ToString());
                        Group parent = objProfile.Profile.HasParent ? objProfile.Profile.ParentsGroups.First() : null; //row["Parent_Group"].ToString().Length > 0 ? Group.FindGroup(int.Parse(row["Parent_Group"].ToString())) : null;
                        while (parent != null)
                        {
                            family = parent.Name + "/" + family;
                            if (parent.Parent != null)
                                //parent = Groups.Groups.FindGroup(parent.Parent.ID.ToString());
                                parent = Group.FindGroup(parent.Parent.ID);
                            else
                                parent = null;
                        }

                        td = new HtmlGenericControl("td");
                        td.InnerHtml = family;
                        tr.Controls.Add(td);
                        
                        td = new HtmlGenericControl("td");
                        td.Attributes["Class"] = "action details";
                        //td.InnerHtml = "<a href=\" javascript: setFormValue(" + row["id_" + Object.Extension + "Profile"] + ",'SelectedObject',1)\" >";
                        td.InnerHtml = "<a href=\" wizard.aspx?folder=" + PageData.CurrentFolder.ID;
                        td.InnerHtml += "&amp;fp=" + objProfile.ID + "&amp;show=o";
                        if (Object.GrantObjectType == GrantObjectType.Document)
                            td.InnerHtml += "&amp;doc=" + Object.ID;
                        td.InnerHtml += "\" >";
                        td.InnerHtml += "<span>Mostra Dettagli</span>";
                        td.InnerHtml += "</a>";
                        tr.Controls.Add(td);

                        td = new HtmlGenericControl("td");
                        td.Attributes["Class"] = "action grant";
                        //td.InnerHtml = "<a href=\" javascript: setFormValue(" + row["id_" + Object.Extension + "Profile"] + ",'SelectedObject',1)\" >";
                        td.InnerHtml = "<a href=\" wizard.aspx?folder=" + PageData.CurrentFolder.ID;
                        td.InnerHtml += "&amp;fp=" + objProfile.ID;
                        if (Object.GrantObjectType == GrantObjectType.Document)
                            td.InnerHtml += "&amp;doc="+ Object.ID;
                        td.InnerHtml +=  "\" >";
                        td.InnerHtml += "<span>Imposta</span>";
                        td.InnerHtml += "</a>";
                        tr.Controls.Add(td);

                        td = new HtmlGenericControl("td");
                        td.Attributes["Class"] = "delete";
                        //td.InnerHtml = "<a href=\"grants.aspx?remove=" + row["id_" + Object.Extension + "Profile"] + "&" + PageData.ObjectQueryString + "\" >";
                        string href = "javascript: setFormValueConfirm('" + objProfile.ID + "','DeleteProfile',1,'Sei sicuro di voler rimuovere questa specifica?')";
                        td.InnerHtml = "<a href=\""+href+"\" >";
                        td.InnerHtml += "<span>Rimuovi</span>";
                        td.InnerHtml += "</a>";
                        tr.Controls.Add(td);


                        table.Controls.Add(tr);
                    }

                    #endregion

                    #region Corpo della tabella  (Elenco Utenti)
                    /* VECCHIO CODICE
                    sql = "SELECT id_User,Name_User,id_" + Object.Extension + "Profile FROM Users";
                    sql += " INNER JOIN Profiles ON (Profile_User = id_Profile)";
                    sql += " INNER JOIN UsersGroups ON (User_UserGroup = id_User)";
                    sql += " INNER JOIN Groups ON (Group_UserGroup = id_Group)";
                    sql += Object.SQLQuery;

                    conditions = "";
                    for (int i = 0; i < PageData.Account.Groups.Count; i++)
                    {
                        NetCms.Users.GroupAssociation GroupAssociation = PageData.Account.AssociatedGroupsOrdered.ElementAt(i);

                        if (GroupAssociation.IsAdministrator)
                        {
                            conditions += " OR ";
                            conditions += " Tree_Group LIKE '" + GroupAssociation.Group.Tree + "%'";
                        }
                        if (GroupAssociation.GrantsRole == NetCms.Users.GrantAdministrationTypes.Administrator)
                            conditions += " AND (id_User <> " + PageData.Account.ID + ")";
                    }
                    //La condizione 1=0 serve ad esculudere tutti i record tranne quelli selezionati dalle
                    //condizioni specificate nella variabile conditions
                    sql += " AND (1=0" + conditions + ")";

                    sql += " GROUP BY id_User,Name_User,id_" + Object.Extension + "Profile";
                    dataTable = PageData.Conn.SqlQuery(sql); */

                    /* 
                     Dall'interpretazione del codice originale, sembra selezionare id utente, nome utente e id profilo (di documenti o cartelle) relativi agli utenti a partire dall'elenco
                     * dei gruppi associati all'utente corrente. Per ciascun gruppo associato all'utente corrente, se l'utente � amministratore
                     * o super amministratore, si seleziona il gruppo in funzione del tree. Inoltre se l'utente � solo amministratore, non permette di gestire i permessi
                     * dati a se stesso.
                     */

                    var resUsers = new List<UserListItem>();
                    if (Object.GrantObjectProfiles != null)
                    {
                       resUsers = (from pro in Object.GrantObjectProfiles
                                   from ga in PageData.Account.AssociatedGroupsOrdered
                                   let profilo = ProfileBusinessLogic.GetById(pro.Profile.ID) as UserProfile
                                   where pro.Profile.IsUser &&
                                   (ga.IsAdministrator && profilo.User.AssociatedGroupsOrdered.Where(x => x.Group.Tree.StartsWith(ga.Group.Tree)).Count() > 0) &&
                                   ((ga.GrantsRole == GrantAdministrationTypes.Administrator && profilo.User.ID != PageData.Account.ID) || ga.GrantsRole != GrantAdministrationTypes.Administrator)
                                   select new UserListItem(pro.ID, profilo.User)
                                   ).ToList();
                    }
                    foreach (UserListItem userItem in resUsers)
                    {
                        tr = new HtmlGenericControl("tr");

                        HtmlGenericControl td;
                        td = new HtmlGenericControl("td");
                        td.InnerHtml = "&nbsp;";
                        td.Attributes["class"] = "icon_user";
                        tr.Controls.Add(td);

                        td = new HtmlGenericControl("td");
                        td.InnerHtml = userItem.Utente.UserName; //row["Name_User"].ToString();
                        tr.Controls.Add(td);

                        td = new HtmlGenericControl("td");
                        td.InnerHtml = "Utente";
                        tr.Controls.Add(td);

                        //DataTable dt = PageData.Conn.SqlQuery("SELECT * From UsersGroups WHERE User_UserGroup = " + row["id_User"].ToString());
                        string families = "";
                        foreach (Group group in userItem.Utente.Groups)
                        {
                            string family = "";
                            //NetCms.Users.Group parent = Group.ChildGroups.FindGroup(group["Group_UserGroup"].ToString());
                            NetCms.Users.Group parent = Group.FindGroup(group.ID);
                            while (parent != null)
                            {
                                family = parent.Name + "/" + family;
                                if (parent.Parent != null)
                                    parent = Group.FindGroup(parent.Parent.ID);
                                else
                                    parent = null;
                            }
                            if (families != "")
                                families += "<br />";
                            families += family;

                        } td = new HtmlGenericControl("td");
                        td.InnerHtml = families;
                        tr.Controls.Add(td);
                        

                        td = new HtmlGenericControl("td");
                        td.Attributes["class"] = "action details";
                        //td.InnerHtml = "<a href=\" javascript: setFormValue(" + row["id_" + Object.Extension + "Profile"] + ",'SelectedObject',1)\" >";
                        td.InnerHtml = "<a href=\" wizard.aspx?folder=" + PageData.CurrentFolder.ID;
                        td.InnerHtml += "&amp;fp=" + userItem.IdObjectProfile + "&amp;show=o";
                        if (Object.GrantObjectType == GrantObjectType.Document)
                            td.InnerHtml += "&amp;doc=" + Object.ID;
                        td.InnerHtml += "\" >";
                        td.InnerHtml += "<span>Mostra Dettagli</span>";
                        td.InnerHtml += "</a>";
                        tr.Controls.Add(td);

                        td = new HtmlGenericControl("td");
                        td.Attributes["class"] = "action grant";
                        //td.InnerHtml = "<a href=\" javascript: setFormValue(" + row["id_" + Object.Extension + "Profile"] + ",'SelectedObject',1)\" >";
                        td.InnerHtml = "<a href=\" wizard.aspx?folder=" + PageData.CurrentFolder.ID;
                        td.InnerHtml += "&amp;fp=" + userItem.IdObjectProfile;
                        if (Object.GrantObjectType == GrantObjectType.Document)
                            td.InnerHtml += "&amp;doc=" + Object.ID;
                        td.InnerHtml += "\" >";
                        td.InnerHtml += "<span>Imposta</span>";
                        td.InnerHtml += "</a>";
                        tr.Controls.Add(td);

                        td = new HtmlGenericControl("td");
                        td.Attributes["class"] = "delete";
                        string href = "javascript: setFormValueConfirm('" + userItem.IdObjectProfile + "','DeleteProfile',1,'Sei sicuro di voler rimuovere questa specifica?')";
                        td.InnerHtml = "<a href=\"" + href + "\" >";
                        td.InnerHtml += "<span>Rimuovi</span>";
                        td.InnerHtml += "</a>";
                        tr.Controls.Add(td);


                        table.Controls.Add(tr);
                    }

                    #endregion

                    if (table.Controls.Count == 1)//Non ci sono specifiche
                    {
                        tr = new HtmlGenericControl("tr");

                        HtmlGenericControl td;
                        td = new HtmlGenericControl("td");
                        td.InnerHtml = "&nbsp;Non ci sono specifiche per questo oggetto.<br />Aggiungere un gruppo o un utente per specificare dei permessi per questo oggetto";
                        td.Attributes["colspan"] = "6";
                        tr.Controls.Add(td);

                        table.Controls.Add(tr);
                    }

                    set.Controls.Add(table);
                    //****************************************************************
                    HtmlInputHidden SelectedObject = new HtmlInputHidden();
                    SelectedObject.ID = "SelectedObject";
                    output.Controls.Add(SelectedObject);

                    HtmlInputHidden DeleteProfile = new HtmlInputHidden();
                    DeleteProfile.ID = "DeleteProfile";
                    output.Controls.Add(DeleteProfile);
                    //****************************************************************



                    return output;
                }

            }

            public class UserListItem
            {
                public int IdObjectProfile
                { get; set; }

                public string UserName
                { get; set; }

                public User Utente
                { get; set; }

                public UserListItem(int id, User utente)
                {
                    IdObjectProfile = id;
                    Utente = utente;
                }
            }
        }
        
    }
}