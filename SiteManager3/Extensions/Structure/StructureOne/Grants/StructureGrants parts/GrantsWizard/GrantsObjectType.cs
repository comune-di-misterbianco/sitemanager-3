//CLASSE COMMENTATA PERCHE' NON PIU' UTILIZZATA
//using System;
//using System.Data;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.HtmlControls;
//using NetUtility.NetTreeViewControl;
//using NetCms.GUI;
//using NetCms.Structure.WebFS;
//using NetCms.Structure.WebFS.FileSystemBusinessLogic;
//using NetCms.Grants;

///// <summary>
///// Summary description for StructureGrants
///// </summary>

//namespace NetCms
//{
//    namespace Structure
//    {
//        namespace Grants.Wizard
//        {
//            public class GrantsObjectType
//            {
//                private PageData PageData;

//                private StructureFolder _SelectedFolder;
//                public StructureFolder SelectedFolder
//                {
//                    get 
//                    {
//                        if (GrantsType.Folder == Type)
//                        {
//                            _SelectedFolder = PageData.RootFolder.FindFolderByID(ID) as StructureFolder;
//                        }
//                        else
//                            _SelectedFolder = FolderBusinessLogic.GetById(SelectedDoc.Folder.ID);
//                        return _SelectedFolder; 
//                    }
//                }

//                private Document _SelectedDoc;
//                public Document SelectedDoc
//                {
//                    get
//                    {
//                        if (GrantsType.Document == Type)
//                        {
//                            _SelectedDoc = PageData.RootFolder.FindDocByID(ID) as Document;
//                        }
//                        return _SelectedDoc;
//                    }
//                }

//                public bool Allowed
//                {
//                    get
//                    {
//                        if (Type == GrantsType.Document && SelectedDoc!=null)
//                            return SelectedDoc.Criteria[CriteriaTypes.Grants].Allowed;
                        
//                        if (Type == GrantsType.Folder && SelectedFolder != null)
//                            return SelectedFolder.Criteria[CriteriaTypes.Grants].Allowed;
                            
//                        return false;
//                    }
//                }

//                public string Extension
//                {
//                    get
//                    {
//                        if (Type == GrantsType.Document)
//                            return "doc";
//                        else
//                            return "folder";
//                    }
//                }

//                public int ObjectProfile
//                {
//                    get { return _ObjectProfile; }
//                    set { _ObjectProfile = value; }
//                }
//                private int _ObjectProfile;

                 

//                public  NetCms.Users.Profile Profile
//                {
//                    get
//                    {
//                        if (_Profile == null)
//                        {
//                            string sql;
//                            sql = "SELECT * FROM " + Extension + (Type == GrantsType.Document ? "profiles INNER JOIN docs ON Doc_DocProfile = id_Doc INNER JOIN folders ON Folder_Doc = id_Folder" : "profiles INNER JOIN folders ON Folder_FolderProfile = id_Folder");
//                            sql += " WHERE id_" + Extension + "Profile = " + ObjectProfile + " AND Network_Folder = " + PageData.Network.ID;

//                            DataTable reader = PageData.Conn.SqlQuery(sql);
//                            if (reader.Rows.Count > 0)
//                                //_Profile = new NetCms.Users.Profile(reader.Rows[0]["Profile_" + Extension + "Profile"].ToString());
//                                 _Profile = Users.ProfileBusinessLogic.GetById(int.Parse(reader.Rows[0]["Profile_" + Extension + "Profile"].ToString()));
//                        }
//                        return _Profile;
//                    }
//                }
//                private NetCms.Users.Profile _Profile;

//                //public  NetCms.Users.Profile Profile
//                //{
//                //    get
//                //    {
//                //        if (_Profile == null)
//                //        {
//                //            string sql;
//                //            sql = "SELECT * FROM "+PageData.Network.SystemName+"_" + Extension + (Type == GrantsType.Document ? "profiles" : "profiles");
//                //            sql += " WHERE id_" + Extension + "Profile = " + ObjectProfile;

//                //            DataTable reader = PageData.Conn.SqlQuery(sql);
//                //            if (reader.Rows.Count > 0)
//                //                _Profile = new NetCms.Users.Profile(reader.Rows[0]["Profile_" + Extension + "Profile"].ToString());
//                //        }
//                //        return _Profile;
//                //    }
//                //}
//                //private NetCms.Users.Profile _Profile;
                
			
			
			
//                #region ID Property

//                private int _ID;
//                public int ID
//                {
//                    get
//                    {
//                        return _ID;
//                    }
//                } 
//                #endregion

//                #region Type Property
//                private GrantsType _Type;
//                public GrantsType Type
//                {
//                    get
//                    {
//                        return _Type;
//                    }
//                }
//                #endregion

//                #region SqlQuery Property
//                public string SQLQuery
//                {
//                    get
//                    {
//                        if (_SQLQuery == null)
//                        {
//                            if (Type == GrantsType.Document)
//                            {
//                                _SQLQuery = " INNER JOIN docprofiles ON (profiles.id_Profile = Profile_DocProfile)";
//                                _SQLQuery += " WHERE Doc_DocProfile = " + ID;
//                            }
//                            else
//                            {
//                                _SQLQuery = " INNER JOIN folderprofiles ON (profiles.id_Profile = folderprofiles.Profile_FolderProfile)";
//                                _SQLQuery += " WHERE Folder_FolderProfile = " + ID;
//                            }
//                        }
//                        return _SQLQuery;
//                    }

//                }
//                private string _SQLQuery; 
//                #endregion

//                #region Name Property

//                private string _Name;
//                public string Name
//                {
//                    get
//                    {
//                        if (_Name == null)
//                        {
//                            string sql;
//                            sql = "SELECT * FROM " + Extension + (Type == GrantsType.Document? "s_Lang":"sLang");
//                            sql += " WHERE " + Extension + "_" + Extension + "Lang = " + ID;

//                            DataTable reader = PageData.Conn.SqlQuery(sql);
//                            if (reader.Rows.Count>0) _Name = reader.Rows[0]["Label_" + Extension + "Lang"].ToString();
//                        }
//                        return _Name;
//                    }
//                } 
//                #endregion

//                #region EntityName Property

//                private string _EntityName;
//                public string EntityName(int SelectedEntity)
//                {
//                    if (_EntityName == null)
//                    {
//                        string sql = "SELECT * FROM ";
//                        sql += Extension + "profiles";
//                        sql += " INNER JOIN groups ON (" + Extension + "profiles.Profile_" + Extension + "Profile = groups.Profile_Group)";
//                        sql += " WHERE id_" + Extension + "Profile = " + SelectedEntity;
//                        DataTable reader = PageData.Conn.SqlQuery(sql);
//                        if (reader.Rows.Count>0) _EntityName = reader.Rows[0]["Name_Group"].ToString();
//                        else
//                        {
//                            sql = "SELECT * FROM ";
//                            sql += Extension + "profiles";
//                            sql += " INNER JOIN users ON (" + Extension + "profiles.Profile_" + Extension + "Profile = users.Profile_User)";
//                            sql += " WHERE id_" + Extension + "Profile = " + SelectedEntity;
//                            reader = PageData.Conn.SqlQuery(sql);
//                            if (reader.Rows.Count > 0) _EntityName = reader.Rows[0]["Name_User"].ToString();
//                        }
//                    }
//                    return _EntityName;
//                } 
//                #endregion

//                public void RemoveEntity(int Entity)
//                {                
//                    if (Type == GrantsType.Document)
//                    {
//                        string sql = "DELETE FROM DocProfilesCriteria";
//                        sql += " WHERE DocProfile_DocProfileCriteria = " + Entity;
//                        PageData.Conn.Execute(sql);
                        
//                        sql = "DELETE FROM DocProfiles";
//                        sql += " WHERE id_DocProfile = " + Entity;
//                        PageData.Conn.Execute(sql);

//                        #region invalido la struttura
//                        //Controllare l'invalidazione. Verificare se va invalidata tutta la cartella o solo 1 documento

//                        //SISTEMARE CON CACHE 2� LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
//                        PageData.CurrentFolder.Invalidate();
//                        #endregion
//                    }
//                    else
//                    {
//                        string sql = "DELETE FROM FolderProfilesCriteria";
//                        sql += " WHERE FolderProfile_FolderProfileCriteria = " + Entity;
//                        PageData.Conn.Execute(sql);

//                        sql = "DELETE FROM FolderProfiles";
//                        sql += " WHERE id_FolderProfile = " + Entity;
//                        PageData.Conn.Execute(sql);

//                        #region invalido la struttura
//                        //Controllare l'invalidazione. Verificare se va invalidata tutta la cartella o solo 1 documento

//                        //SISTEMARE CON CACHE 2� LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
//                        PageData.CurrentFolder.Invalidate();
//                        #endregion
//                    }
                    
//                }

//                public GrantsObjectType(PageData pageData, GrantsType ObjectType, int id)
//                {
//                    PageData = pageData;
//                    _Type = ObjectType;
//                    _ID = id;
//                }
//                public GrantsObjectType(PageData pageData, GrantsType ObjectType, int id,int objectProfile)
//                {
//                    PageData = pageData;
//                    _Type = ObjectType;
//                    _ID = id;
//                    ObjectProfile = objectProfile;
//                }

//            }

//            public enum GrantsType
//            {
//                Document = 0,
//                Folder = 1
//            }
//        }
        
//    }
//}