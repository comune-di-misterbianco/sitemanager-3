using System;
using System.Xml;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms
{
    public class RssItem
    {
        /*
            <author> 	Optional. Specifies the e-mail address to the author of the item
            <category> 	Optional. Defines one or more categories the item belongs to
            <comments> 	Optional. Allows an item to link to comments about that item
            <description> 	Required. Describes the item
            <enclosure> 	Optional. Allows a media file to be included with the item
            <guid> 	Optional. Defines a unique identifier for the item
            <link> 	Required. Defines the hyperlink to the item
            <pubDate> 	Optional. Defines the last-publication date for the item
            <source> 	Optional. Specifies a third-party source for the item
            <title> 	Required. Defines the title of the item
         */

        private string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        private string _Link;
        public string Link
        {
            get { return _Link; }
            set { _Link = value; }
        }

        private string _Title;
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _Category;
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        private string _PubDate;
        public string PubDate
        {
            get { return _PubDate; }
            set { _PubDate = value; }
        }

        private string _Author;
        public string Author
        {
            get { return _Author; }
            set { _Author = value; }
        }

        private string _Source;
        public string Source
        {
            get { return _Source; }
            set { _Source = value; }
        }

        private string _Guid;
        public string Guid
        {
            get { return _Guid; }
            set { _Guid = value; }
        }

        private string _Comments;
        public string Comments
        {
            get { return _Comments; }
            set { _Comments = value; }
        }

        private string _Enclusure;
        public string Enclusure
        {
            get { return _Enclusure; }
            set { _Enclusure = value; }
        }

        //<media:content url="https://st.ilfattoquotidiano.it/wp-content/uploads/2020/04/28/bagnasco-1200-1050x551.jpg" width="1050" height="551" medium="image" type="image/jpeg" />

        private Media _Media;
        public Media Media
        {
            get { return _Media;  }
            set { _Media = value; }
        }

        public RssItem()
        {

        }

        public RssItem(string link, string title, string description)
        {
            _Link = link;
            _Title = title;
            _Description = description;
        }

        public RssItem(string link, string title, string pubdate, string description)
        {
            _Link = link;
            _Title = title;
            _Description = description;
            _PubDate = pubdate;
        }

        public XmlNode Node(XmlDocument Document)
        {
            XmlNode node = Document.CreateElement("", "item", "");
            // XmlNode cmnNode;
            XmlElement cmnNode;

            if (Author != null)
            {
                cmnNode = Document.CreateElement("", "author", "");
                cmnNode.InnerXml = this.Author.Replace("&", "&amp;").Replace("&amp;amp;", "&amp;");
                node.AppendChild(cmnNode);
            }
            if (Category != null)
            {
                cmnNode = Document.CreateElement("", "category", "");
                cmnNode.InnerXml = this.Category.Replace("&", "&amp;").Replace("&amp;amp;", "&amp;");
                node.AppendChild(cmnNode);
            }

            if (Comments != null)
            {
                cmnNode = Document.CreateElement("", "comments", "");
                cmnNode.InnerXml = this.Comments.Replace("&", "&amp;").Replace("&amp;amp;", "&amp;");
                node.AppendChild(cmnNode);
            }

            cmnNode = Document.CreateElement("", "description", "");
            cmnNode.InnerXml = "<![CDATA[" + this.Description.Replace("&", "&amp;").Replace("&amp;amp;", "&amp;") + "]]>";
            node.AppendChild(cmnNode);

            if (this.Enclusure != null)
            {
                cmnNode = Document.CreateElement("", "enclosure", "");
                cmnNode.InnerXml = this.Enclusure.Replace("&", "&amp;").Replace("&amp;amp;", "&amp;");
                node.AppendChild(cmnNode);
            }

            if (Guid != null)
            {
                cmnNode = Document.CreateElement("", "guid", "");
                cmnNode.SetAttribute("isPermaLink", "false");
                cmnNode.InnerXml = this.Guid.Replace("&", "&amp;").Replace("&amp;amp;", "&amp;");
                node.AppendChild(cmnNode);
            }

            cmnNode = Document.CreateElement("", "link", "");
            cmnNode.InnerXml = this.Link.Replace("&", "&amp;").Replace("&amp;amp;", "&amp;");
            node.AppendChild(cmnNode);

            if (PubDate != null)
            {
                cmnNode = Document.CreateElement("", "pubDate", "");
                cmnNode.InnerXml = this.PubDate.Replace("&", "&amp;").Replace("&amp;amp;", "&amp;");
                node.AppendChild(cmnNode);
            }

            if (Source != null)
            {
                cmnNode = Document.CreateElement("", "source", "");
                cmnNode.InnerXml = this.Source.Replace("&", "&amp;").Replace("&amp;amp;", "&amp;");
                node.AppendChild(cmnNode);
            }

            if (Media != null)
            {
                cmnNode = Document.CreateElement("media", "content", "media:content");
                cmnNode.SetAttribute("url", Media.Url);
                cmnNode.SetAttribute("width", Media.Width.ToString());
                cmnNode.SetAttribute("height", Media.Height.ToString());
                cmnNode.SetAttribute("medium", Media.Medium);
                cmnNode.SetAttribute("type", Media.Type);
                node.AppendChild(cmnNode);
            }

            cmnNode = Document.CreateElement("", "title", "");
            cmnNode.InnerXml = this.Title.Replace("&", "&amp;").Replace("&amp;amp;", "&amp;");
            node.AppendChild(cmnNode);

            return node;

        }
    }

    public class Media
    {
        //<media:content url="https://" 
        //width="1050" height="551" medium="image" type="image/jpeg" />
        public string Url
        {
            get;
            set;
        }

        public int Width
        {
            get;
            set;
        }
        public int Height 
        { 
            get;
            set; 
        }

        public string Medium
        {
            get;
            set;
        }
        public string Type
        {
            get;
            set;
        }

    }
}