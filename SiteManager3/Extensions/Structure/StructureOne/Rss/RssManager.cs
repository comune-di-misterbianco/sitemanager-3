using System;
using System.Web;
using System.Xml;
using System.IO;
using NetCms.Structure.WebFS;
using System.Collections.Generic;
using System.Linq;
using NetCms.Structure.WebFS.Reviews;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NHibernate;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms
{
    public class RssManager
    {
        private int _ItemsNumber;
        public int ItemsNumber
        {
            get { return _ItemsNumber; }
            set { _ItemsNumber = value; }
        }

        protected StructureFolder Folder;

        protected string Link;
        protected string Title;
        protected string Desc;

        public string BaseUrl
        {
            get { return HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority; }
        }

        public RssManager(string link, string title, string desc, StructureFolder folder)
        {
            Link = link;
            Title = title;
            Desc = desc;
            _ItemsNumber = 10;
            Folder = folder;
        }
        public enum RecourseSide { Both,None, Top,Bottom }
        public void SaveXML(ISession session = null)
        {
            this.SaveXML(RecourseSide.Top, session);
        }
        public void SaveXML(RecourseSide direction, ISession session = null)
        {
            RssDocument Document = InitDocument();

            if (direction == RecourseSide.Top && direction == RecourseSide.Both && Folder.Parent != null)
                ((Folder.Parent) as StructureFolder).RssManager.SaveXML(direction);

            if (direction == RecourseSide.Bottom && direction == RecourseSide.Both && Folder.Parent != null)
                foreach (StructureFolder child in this.Folder.SubFolders)
                    child.BuildRssRecursive();
            
            if (this.Folder.EnableRss)
            {
                int i = 0;
                int tot = 0;

                // Get Last 3 months record != allegati and != links Limit by ItemsNumber// x.Modify > DateTime.Now.AddMonths(-3) &&
                //List<Document> Docs = Folder.DocumentsByAllTree(ItemsNumber).Where(x => 
                //(x.Content != 0))// && (x.Status == NetCms.Structure.WebFS.Document.DocumentStates.Reachable || x.Status == NetCms.Structure.WebFS.Document.DocumentStates.Menu)))
                //.ToList();

                List<Document> Docs = DocumentBusinessLogic.GetDocumentsByIdFolderAndTree(Folder.ID, Folder.Tree, ItemsNumber, session).ToList();


                while (i < Docs.Count && tot < ItemsNumber)
                {
                    Document doc = Docs.ElementAt(i);
                  
                    string link = BaseUrl + Folder.StructureNetwork.Paths.AbsoluteFrontRoot + doc.FrontendLink;
                    RssItem item = null;

                    StructureFolder currentFolder = FolderBusinessLogic.GetById(session,doc.Folder.ID);// as StructureFolder;
                    string dataRssItem = NetCms.Utility.Rfc822Date.GetRFC822Date(doc.Modify);

                    if (currentFolder != null && currentFolder.ReviewEnabled)
                    {
                        Review currentReview = doc.DocLangs.ElementAt(0).Revisioni.Where(x => x.Pubblicata).FirstOrDefault();
                        if (currentReview != null)
                            dataRssItem = NetCms.Utility.Rfc822Date.GetRFC822Date(currentReview.Data);
                    }

                    if (currentFolder != null && currentFolder.Tipo == 2 && currentFolder.Homepage == 0)
                    {
                        if (currentFolder.Label != this.Title)
                            item = new RssItem(link, "<![CDATA[" + currentFolder.Label + "]]>", dataRssItem, "<![CDATA[" + currentFolder.Descrizione + "]]>");
                    }
                    else if (doc.Application == 3 || doc.Application == 7 || doc.Application == 8)
                    {
                        item = new RssItem();
                        AddCustomContents(item, doc,session);
                        //Document.AppendItem(item);
                        //tot++;
                    }
                    else
                    {
                        string descrizione = string.Empty;
                        descrizione = doc.DocLangs.ElementAt(0).Descrizione;
                        item = new RssItem(link, "<![CDATA[" + doc.Nome + "]]>", dataRssItem, "<![CDATA[" + descrizione + "]]>");
                    }

                    if (item != null && !string.IsNullOrEmpty(item.Description) && !string.IsNullOrEmpty(item.Link))
                    {
                        //AddCustomContents(item, doc);
                        Document.AppendItem(item);
                        tot++;
                    }


                    i++;

                }

                Document.SaveXml(HttpContext.Current.Server.MapPath(this.Folder.StructureNetwork.Paths.AbsoluteFrontRoot + Folder.Path + "/rss.xml"));
            }
        }
        public virtual void AddCustomContents(RssItem item, Document doc, ISession session = null)
        {

        }

        public virtual RssDocument InitDocument()
        {
            RssDocument newDoc = new RssDocument(Link, Title, Desc);
            
            newDoc.Copyright = "Copyright " + DateTime.Today.Year + " " + CmsConfigs.Model.GlobalConfig.CurrentConfig.PortalName;//   Folder.Label;
            newDoc.WebMaster = NetCms.Configurations.PortalData.eMail + " (per segnalazioni sui contenuti del servizio o malfunzionamenti)";
            newDoc.PubDate = NetCms.Utility.Rfc822Date.GetRFC822Date(DateTime.Now);
            newDoc.LastBuildDate = NetCms.Utility.Rfc822Date.GetRFC822Date(DateTime.Now);

            return newDoc;
        }
    }
}