using System;
using System.IO;
using System.Text;
using System.Xml;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms
{
    public class RssDocument
    {
        /*
            <category> 	Optional. Defines one or more categories for the feed
            <copyright> 	Optional. Notifies about copyrighted material
            <description> 	Required. Describes the channel
            <image> 	Optional. Allows an image to be displayed when aggregators present a feed
            <lastBuildDate> 	Optional. Defines the last-modified date of the content of the feed
            <link> 	Required. Defines the hyperlink to the channel
            <pubDate> 	Optional. Defines the last publication date for the content of the feed
            <title> 	Required. Defines the title of the channel
            <webMaster> 	Optional. Defines the e-mail address to the webmaster of the feed
         */

        private string _Description;
        public string Description
        {
            get { return _Description; }
        }

        private string _Link;
        public string Link
        {
            get { return _Link; }
        }

        private string _Title;
        public string Title
        {
            get { return _Title; }
        }

        private string _Category;
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        private string _PubDate;
        public string PubDate
        {
            get { return _PubDate; }
            set { _PubDate = value; }
        }

        private string _WebMaster;
        public string WebMaster
        {
            get { return _WebMaster; }
            set { _WebMaster = value; }
        }

        private string _Source;
        public string Source
        {
            get { return _Source; }
            set { _Source = value; }
        }

        private string _Copyright;
        public string Copyright
        {
            get { return _Copyright; }
            set { _Copyright = value; }
        }

        private string _LastBuildDate;
        public string LastBuildDate
        {
            get { return _LastBuildDate; }
            set { _LastBuildDate = value; }
        }

        private string _Image;
        public string Image
        {
            get { return _Image; }
            set { _Image = value; }
        }

        private XmlDocument Document;
        private XmlNode Channel;

        public RssDocument(string link, string title, string description)
        {
            _Link = link;
            _Title = title;
            _Description = description;            
        }

        private void BuildDocument()
        {
            Document = new XmlDocument();

            XmlDeclaration xmlDeclaration = Document.CreateXmlDeclaration("1.0", "UTF-8", null);
            Document.AppendChild(xmlDeclaration);

          //  XmlNode xmlnode = Document.CreateNode(XmlNodeType.XmlDeclaration, "", "");            
          //  Document.AppendChild(xmlnode);

            XmlElement root = Document.CreateElement("rss");
            root.SetAttribute("version", "2.0");
            root.SetAttribute("xmlns:content", "http://purl.org/rss/1.0/modules/content/");
            root.SetAttribute("xmlns:wfw", "http://wellformedweb.org/CommentAPI/");
            root.SetAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1/");
            root.SetAttribute("xmlns:atom", "http://www.w3.org/2005/Atom");
            root.SetAttribute("xmlns:sy", "http://purl.org/rss/1.0/modules/syndication/");
            root.SetAttribute("xmlns:slash", "http://purl.org/rss/1.0/modules/slash/");
            root.SetAttribute("xmlns:media", "http://search.yahoo.com/mrss/");
            Document.AppendChild(root);

            //XmlAttribute att = Document.CreateAttribute("version");
            //att.Value = "2.0"; //imposto il valore                
            //root.Attributes.Append(att); // aggiungo l'attributo alla collezione degli attributi del nodo

            //att = Document.CreateAttribute("xmlns:content");
            //att.Value = "http://purl.org/rss/1.0/modules/content/";
            //root.Attributes.Append(att);

            //att = Document.CreateAttribute("xmlns:wfw");
            //att.Value = "http://wellformedweb.org/CommentAPI/";
            //root.Attributes.Append(att);

            //att = Document.CreateAttribute("xmlns:dc");
            //att.Value = "http://purl.org/dc/elements/1.1/";
            //root.Attributes.Append(att);

            //att = Document.CreateAttribute("xmlns:atom");
            //att.Value = "http://www.w3.org/2005/Atom";
            //root.Attributes.Append(att);

            //att = Document.CreateAttribute("xmlns:sy");
            //att.Value = "http://purl.org/rss/1.0/modules/syndication/";
            //root.Attributes.Append(att);

            //att = Document.CreateAttribute("xmlns:slash");
            //att.Value = "http://purl.org/rss/1.0/modules/slash/";
            //root.Attributes.Append(att);

            //att = Document.CreateAttribute("xmlns:media");
            //att.Value = "http://search.yahoo.com/mrss/";
            //root.Attributes.Append(att);

            Channel = Document.CreateElement("", "channel", "");
            root.AppendChild(Channel);

            XmlNode node = Channel;
            XmlNode cmnNode;

            if (Category != null)
            {
                cmnNode = Document.CreateElement("", "category", "");
                cmnNode.InnerXml = this.Category;
                node.AppendChild(cmnNode);
            }

            if (Copyright != null)
            {
                cmnNode = Document.CreateElement("", "copyright", "");
                cmnNode.InnerXml = this.Copyright;
                node.AppendChild(cmnNode);
            }

            cmnNode = Document.CreateElement("", "description", "");
            cmnNode.InnerXml = this.Description;
            node.AppendChild(cmnNode);

            if (this.Image != null)
            {
                cmnNode = Document.CreateElement("", "image", "");
                cmnNode.InnerXml = this.Image;
                node.AppendChild(cmnNode);
            }

            if (LastBuildDate != null)
            {
                cmnNode = Document.CreateElement("", "lastBuildDate", "");
                cmnNode.InnerXml = this.LastBuildDate;
                node.AppendChild(cmnNode);
            }

            cmnNode = Document.CreateElement("", "link", "");
            cmnNode.InnerXml = this.Link;
            node.AppendChild(cmnNode);

            if (PubDate != null)
            {
                cmnNode = Document.CreateElement("", "pubDate", "");
                cmnNode.InnerXml = this.PubDate;
                node.AppendChild(cmnNode);
            }

            cmnNode = Document.CreateElement("", "title", "");
            cmnNode.InnerXml = this.Title;
            node.AppendChild(cmnNode);

            if (WebMaster != null)
            {
                cmnNode = Document.CreateElement("", "webMaster", "");
                cmnNode.InnerXml = this.WebMaster;
                node.AppendChild(cmnNode);
            }
        }

        public void AppendItem(RssItem item)
        {
            if (Document == null)
                BuildDocument();
            Channel.AppendChild(item.Node(this.Document));
        }

        public void SaveXml(string path)
        {
            try
            {
                if (Document != null)
                {
                    string folderPath = new System.IO.FileInfo(path).Directory.FullName;

                    if (!System.IO.Directory.Exists(folderPath))
                    {
                        System.IO.Directory.CreateDirectory(folderPath);
                    }

                    using (TextWriter sw = new StreamWriter(path, false, Encoding.UTF8))
                    {
                        Document.Save(sw);
                    }                                           
                   //     Document.Save(path);
                }
            }
            catch(Exception ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", "RssDocument");
            }
        }
    }
}