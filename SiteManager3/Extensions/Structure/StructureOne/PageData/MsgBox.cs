using System;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms
{
    public class MsgBox
    {
        private PageData PageData;
        public HtmlGenericControl Content
        {
            get { return _Content; }
            set { _Content = value; }
        }
        private HtmlGenericControl _Content;
        private HtmlGenericControl _Box;
        public HtmlGenericControl Box
        {
            get
            {
                return _Box;
            }
        }
        
        public string InnerHtml
        {
            get
            {
                return _Content.InnerHtml;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    GUI.PopupBox.AddMessage(value, NetCms.GUI.PostBackMessagesType.Normal);
                    _Content.InnerHtml = value;
                    if (_Content.InnerHtml.Length > 0)
                        ShowMsgBox();
                }
            }
        }

        private bool ShowValue;
        public bool Show
        {
            get
            {
                return ShowValue;
            }
            set
            {
                if (value)
                    ShowMsgBox();
                else
                    HideMsgBox();
                ShowValue = value;
            }
        }

        public MsgBox(PageData pageData)
        {
            PageData = pageData;
            _Content = new HtmlGenericControl("div");
            _Box = Structure.Side.SideObject("Avviso", "MsgBox",false, Content);
            _Box.Attributes["class"] += " Notice hide";
        }
        public void ShowMsgBox()
        {
            Box.Attributes["class"] = Box.Attributes["class"].Replace(" hide", "");
        }

        public void HideMsgBox()
        {
            string classe = Box.Attributes["class"];
            classe += classe.Contains(" hide") ? "" : " hide";
            Box.Attributes["class"] = classe;
        }
        
    }
}