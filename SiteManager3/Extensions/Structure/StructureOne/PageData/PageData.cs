using System;
using System.Xml;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using NetCms.Connections;
using NetCms.GUI;
using NetCms.Users;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms
{
    public class PageData
    {
        #region Properties

        public NetCms.PageBuilder GUIBuilder
        {
            get { return _GUIBuilder; }
            set { _GUIBuilder = value; }
        }
        private NetCms.PageBuilder _GUIBuilder;

        //private static ChangeLog _ChangeLog;
        //public static ChangeLog ChangeLog
        //{
        //    get 
        //    {
        //        return _ChangeLog; 
        //    }
        //    set 
        //    {
        //        _ChangeLog = value; 
        //    }
        //}
        	
        #region Folders&Paths
        
        public string FilePath
        {
            get {

                string path = _FilePath.ToLower();
                path = path.Replace(PageName.ToLower(), "");
                return path; 
            }
        }
        private string _FilePath;
        
        public string RootDist
        {
            get
            {
                return this.Paths.AbsoluteRoot + "/"; ;
            }
        }

        public string PageName
        {
            get
            {
                return Paths.PageName;
            }
        }


        private PagePaths _Paths;
        public PagePaths Paths
        {
            get
            {
                if (_Paths == null)
                    _Paths = new PagePaths(this);
                
                return _Paths;
            }
        }

        public CmsConfigs.Model.Config CurrentConfig
        {
            get
            {
                int networkID = 0;
                if (Network != null)
                    networkID = Network.ID;

                return CmsConfigs.Model.GlobalConfig.GetConfigByNetworkID(networkID);
            }
        }


        #endregion

        #region StructureFolders

        private StructureFolder _CurrentFolder;
        public StructureFolder CurrentFolder
        {
            get
            {
                if (_CurrentFolder == null && Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork && this.Network.CurrentFolder != null)
                    _CurrentFolder = (StructureFolder)this.Network.CurrentFolder;

                return _CurrentFolder;
            }
        }

        public bool CheckCurentFolderGrant
        {
            get 
            {

                bool _CheckCurentFolderGrant = true;
                if (HttpContext.Current.Items["CheckCurentFolderGrant"] != null)                
                    _CheckCurentFolderGrant = (bool)HttpContext.Current.Items["CheckCurentFolderGrant"] ;
                            
                return _CheckCurentFolderGrant; 
            }
        }
	

        private StructureFolder _RootFolder;
        public StructureFolder RootFolder
        {
            get
            {
                if (_RootFolder == null)
                    _RootFolder = (StructureFolder)Networks.NetworksManager.CurrentActiveNetwork.RootFolder;

                return _RootFolder;
            }
        }

        private TreeNode _RootFolderNode;
        public TreeNode RootFolderNode
        {
            get { return _RootFolderNode; }
            set { _RootFolderNode = value; }
        }

        #endregion

        private Users.Group _Groups;
        public Users.Group Groups
        {
            get
            {
                if (_Groups == null)
                {
                    Users.StructureGroups binder = new Users.StructureGroups(this);
                    _Groups = binder.Root;
                }
                return _Groups;
            }
        }

        private HtmlGenericControl _CurentObjectInfoColtrol;
        public HtmlGenericControl CurentObjectInfoColtrol
        {
            get { return _CurentObjectInfoColtrol; }
            set { _CurentObjectInfoColtrol = value; }
        }


        private User _Account;
        public User Account
        {
            get
            {
                return _Account;
            }
        }

        public bool BuildAccountAndCheckLogin()
        {/* 
            //Se � stato fatto un postback, controllo se a farlo � stato il controllo di login rapido
            if( this.IsPostBack) _Account = AccountManager.CheckLogin(this.Conn);

            if (_Account == null)
            {
                if (Session["Account"] != null)
                    _Account = (SitemanagerUser)Session["Account"];
                else if (Session[ConfigOptions.BackAccountTokenID] != null)
                {
                    NetUtility.AccountToken LoginToken = (NetUtility.AccountToken)HttpContext.Current.Session[ConfigOptions.BackAccountTokenID];
                    _Account = new SitemanagerUser(LoginToken.ID);
                    HttpContext.Current.Session["Account"] = _Account;
                    HttpContext.Current.Session[ConfigOptions.BackAccountTokenID] = _Account;
                }
                   
            }
            
            _Account = AccountManager..();
            return _Account != null;*/
            if (AccountManager.CurrentAccount != null)
              _Account = AccountManager.CurrentAccount;
            return _Account != null;
        }

        public string ObjectQueryString
        {
            get
            {
                string _ObjectQueryString = "";
                string link = "";
                int Doc = checkNumericGetExist("doc", "");
                int Folder = checkNumericGetExist("folder", "");
                if (Doc != 0)
                    link = "doc=" + Doc;
                else
                    if (Folder != 0)
                        link = "folder=" + Folder;
                _ObjectQueryString = link;

                return _ObjectQueryString;
            }
        }

        public int CurentFolderID
        {
            get
            {               
                return CurrentFolder.ID;
            }
        }

        private WebFileSystem.GUI.History.HistoryManager History;

        #region Connections

        private NetCms.Connections.Connection _Conn;
        public NetCms.Connections.Connection Conn
        {
            get
            {
                //if (_Conn == null)
                //    _Conn = NetCms.Connections.ConnectionsManager.CommonConnection;
                //return _Conn;

                ConnectionsManager ConnManager = new ConnectionsManager();
                return ConnManager.CommonConnection;
            }
        }


      

        /*
        public NetCms.Connections.Connection GlobalConn
        {
            get
            {
                if (_GlobalConn == null)
                {
                    _GlobalConn = NetCms.Connections.Sm2Connection.GetPageConnection(GlobalConnectionString, Faces.Admin);
                    _GlobalConn.CacheSize = 15;
                }
                return _GlobalConn;
            }
        }
        private NetCms.Connections.Connection _GlobalConn;
        
        private string _GlobalConnectionString;
        public string GlobalConnectionString
        {
            get
            {
                if (_GlobalConnectionString == null)
                {
                    _GlobalConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["GlobalDB"].ConnectionString.ToString();
                }
                return _GlobalConnectionString;
            }
        }*/

        #endregion

        public HtmlGenericControl Console
        {
            get { return _Console; }
        }
        private HtmlGenericControl _Console;

        #region Page Class Data
        //*************************************************************
        public HttpSessionState Session
        {
            get { return HttpContext.Current.Session; }
        }

        public HttpRequest Request
        {
            get { return HttpContext.Current.Request; }
        }

        public HttpServerUtility Server
        {
            get { return HttpContext.Current.Server; }
        }

        #region IsPostBack

        public bool IsPostBack
        {
            get
            {
                return ValidPostBack;
            }
        }
        
        public bool ValidPostBack
        {
            get
            {                
                return _ValidPostBack;
            }
        }
        private bool _ValidPostBack;
        private bool _IsPostBack;

        private void ValidatePostBack()
        {
            string key = "PostBackRefreshControl_" + this.PageName;
            _ValidPostBack = false;

            //ATTENZIONE: NUOVO CODICE PER RISOLVERE IL PROBLEMA DI POSTBACK IN FIREFOX --- TESTARE BENE! ---
            if (_IsPostBack && ViewState != null)
            {
                int NewViewState = Session[key] != null ? (int)Session[key] : 0;

                int OldViewState = ViewState[key] != null ? (int)ViewState[key] : 0;
                if (NewViewState == OldViewState)
                {
                    _ValidPostBack = true;
                    int pb = NewViewState + 1;
                    Session[key] = pb;
                }
            }
            if (ViewState != null)
                ViewState[key] = Session[key];            
        }
        
        #endregion
         
        public static void Redirect(string url)
        {
           NetCms.Diagnostics.Diagnostics.Redirect(url);
        }

        public HttpApplicationState Application
        {
            get { return HttpContext.Current.Application; }
        }
        //*************************************************************
        #endregion
                
        public int PageIndex
        {
            get { return _PageIndex; }
        }
        private int _PageIndex;

        public string SubmitSource
        {
            get { return _SubmitSource; }
        }
        private string _SubmitSource = "";
        /*

        public NetCms.Structure.CmsPathCodes Codes
        {
            get
            {
                if (_Codes == null)
                    _Codes = new NetCms.Structure.CmsPathCodes(this.Network);
                return _Codes;
            }
        }
        private NetCms.Structure.CmsPathCodes _Codes;
        */
        private XmlNode xmlConfigs;
        public XmlNode XmlConfigs
        {
            get
            {
                if (xmlConfigs == null)
                {
                    XmlDocument oXmlDoc = new XmlDocument();
                    try
                    {
                        string xmlFilePath = Configurations.Paths.AbsoluteConfigRoot + "\\config.xml";
                        oXmlDoc.Load(xmlFilePath);
                    }
                    catch (Exception ex)
                    {
                        //xmlConfigs = null;
                        /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                        ExLog.SaveLog(Account.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error,ex);*/
                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, "Errore nel caricamento del file xml di configurazione", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "53");
                    }
                    xmlConfigs = oXmlDoc.DocumentElement;
                }
                return xmlConfigs;
            }
        }

        private bool _AddToHistory = true;
        public bool AddToHistory
        {
            get 
            {
                return _AddToHistory;
            }
            set
            {
                _AddToHistory = true;
            }
        }

        public StateBag ViewState
        {
            get
            {
                return _ViewState;
            }
        }
        private StateBag _ViewState;

        public int SelectedLang
        {
            get { return this.DefaultLang; }
        }

        private Networks.Network _Network;
        public Networks.Network Network
        {
            get
            {
                if (_Network == null)
                    _Network = Networks.NetworksManager.CurrentActiveNetwork;

                return _Network;
            }
        }

        private MsgBox _MsgBox;
        public MsgBox MsgBox
        {
            get
            {
                if(_MsgBox == null)
                    _MsgBox = new MsgBox(this);
                return _MsgBox;
            }

        }

        private SideToolbar _SideToolbar;
        public SideToolbar SideToolbar
        {
            get {
                if (_SideToolbar == null)
                    _SideToolbar = new NetCms.GUI.SideToolbar();
                return _SideToolbar; }
            //set { _SideToolbar = value; }
        }

        private  NetCms.LogAndTrance.HighLevelLogger _GuiLog;
        public NetCms.LogAndTrance.HighLevelLogger GuiLog
        {
            get
            {
                if (_GuiLog == null)
                    _GuiLog = new NetCms.LogAndTrance.HighLevelLogger(this.Account.ID,this.IsPostBack);
                return _GuiLog;
            }
        }

        private Structure.Side _Side;
        public Structure.Side Side
        {
            get
            {
                return _Side;
            }
            set { _Side = value; }
        }

        private InfoToolbar _InfoToolbar;
        public InfoToolbar InfoToolbar
        {
            get
            {
                if (_InfoToolbar == null)
                {
                    _InfoToolbar = new InfoToolbar(this.CurrentFolder);
                }
                return _InfoToolbar;
            }
            set { _InfoToolbar = value; }
        }
	
        private string _OuterScripts = "";
        public string OuterScripts
        {
            get { return _OuterScripts; }
            set { _OuterScripts += value; }
        }
         
        public System.Web.UI.Page SystemPage
        {
            get { return _SystemPage; }
            private set { _SystemPage = value; }
        }
        private System.Web.UI.Page _SystemPage;

        public static PageData CurrentReference
        {
            get
            {
                return (PageData)HttpContext.Current.Items["SM_PageData"];
            }
        }

        #endregion

        #region Costructors

        public PageData(System.Web.UI.Page page, StateBag viewState)
        {
            HttpContext.Current.Items["SM_PageData"] = this;
            SystemPage = page;
            this.GUIBuilder = GUIBuilder;
            _ViewState = viewState;
            _FilePath = Request.Url.LocalPath.ToLower();
            _PageIndex = 1;
            _IsPostBack = page.IsPostBack;
            BootLoad();
            //if (_ChangeLog == null)
            //    _ChangeLog = new ChangeLog(this.Conn);
        }

        private void BootLoad()
        {
            initVariables();
            initHistoryBack();
            ValidatePostBack();
        }

        #endregion

        #region Methods

        
        private void initVariables()
        {
            string page = Request.Form.Get("page");
            if (page != null)
            {
                int value;
                int.TryParse(page,out value);
                if (value > 1)
                    _PageIndex = value;
            }
            else
            {
                if (Request.QueryString.Get("page") != null)
                {
                    int value = 0;
                    if (int.TryParse(Request.QueryString.Get("page"), out value))
                        _PageIndex = value;
                }
            }

            if (Request.Form["SubmitSource"] != null)
            {
                _SubmitSource = Request.Form["SubmitSource"];
            }

        }

        private void initHistoryBack()
        {
            /*History = new History(this);
            string queryHistory = Request.QueryString["history"];
            if (queryHistory != null)
            {
                if (queryHistory == "back")
                {
                    string url = History.GetBackURL();

                    if (url != "")
                        PageData.Redirect(url);
                }
            }
            else
                if (!IsPostBack && AddToHistory)
                    History.AddUrl(Request.Url.ToString());*/
        }

        public int checkNumericGetExist(string key, string redirect)
        {
            int output = 0;
            if (Request.QueryString[key] == null && redirect.Length > 0)
                PageData.Redirect(redirect);
            if (!int.TryParse(Request.QueryString[key], out output) && redirect.Length > 0)
                PageData.Redirect(redirect);
            return output;
        }

        public string GetConfigAttribute(string XmlPath,string AttributeName)
        {
            string attribute = "";
            XmlNodeList oNodeList = XmlConfigs.SelectNodes(XmlPath);
            if (oNodeList != null && oNodeList.Count > 0 && oNodeList[0].Attributes[AttributeName]!=null)
                attribute = oNodeList[0].Attributes[AttributeName].Value;

            return attribute;
        }

        private int _DefaultLang;
        public int DefaultLang
        {
            get 
            {
                if (_DefaultLang == 0)
                {
                    DataTable reader = this.Conn.SqlQuery("SELECT * FROM Lang WHERE Default_Lang = 1");
                    if (reader.Rows.Count>0)
                        _DefaultLang = int.Parse(reader.Rows[0]["id_Lang"].ToString());
                    else _DefaultLang = 1;
                }
                return _DefaultLang; 
            }
        }

        public static void GoBack()
        {
            PageData.Redirect(NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
        }
                
        public static void GoToErrorPage()
        {
            GoToErrorPage(null);
        }
        public static void GoToErrorPage(Document doc)
        {
            PageData.Redirect(NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
        }
        public const string BackAddress = "?history=back";
        
        #endregion
    }
}