using System;
using System.IO;
using System.Data;

using System.Data.Common;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;

using NetUtility.NetTreeViewControl;
using NetCms.Connections;
using NetCms.GUI;
using NetUtility;
using System.Xml;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms
{
    public class PagePaths
    {
        //private PageData PageData;

        public string AbsoluteRoot
        {
            get
            {
                return Configurations.Paths.AbsoluteRoot;
            }
        }

        public string AbsoluteConfigRoot
        {
            get
            {
                return Configurations.Paths.AbsoluteConfigRoot;
            }
        }

        private string _AbsoluteNetworkConfigRoot;
        public string AbsoluteNetworkConfigRoot
        {
            get
            {
                if (_AbsoluteNetworkConfigRoot == null)
                    _AbsoluteNetworkConfigRoot = Configurations.Paths.AbsoluteConfigRoot+"\\" + Network.SystemName.ToLower();
                
                return _AbsoluteNetworkConfigRoot;
            }
        }

        public string AbsoluteWebRoot
        {
            get
            {
                return Network.Paths.AbsoluteFrontRoot;
            }
        }
        
        private string _AbsoluteSiteManagerRoot;
        public string AbsoluteSiteManagerRoot
        {
            get
            {
                return Configurations.Paths.AbsoluteSiteManagerRoot;
            }
        }

        public string AbsoluteNetworkRoot
        {
            get
            {
                return Network.Paths.AbsoluteAdminRoot;
            }
        }

        private string _PageName;
        public string PageName
        {
            get
            {
                if (_PageName == null)
                {
                    string sPath = System.Web.HttpContext.Current.Request.Url.LocalPath;
                    System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
                    string sRet = oInfo.Name;
                    _PageName = sRet.ToLower();
                }
                return _PageName;
            }
        }

        private Networks.Network _Network;
        public Networks.Network Network
        {
            get
            {
                return _Network;
            }
        }

        public PagePaths(PageData pageData)
            :this(pageData,pageData.Network)
        {
        }

        public PagePaths(PageData pageData,Networks.Network network)
        {
            //PageData = pageData;
            _Network = network;
        }

    }
}