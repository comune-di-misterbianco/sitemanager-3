using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Users;
using System.Collections.Generic;
using NetCms.Networks.WebFS;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms
{
    namespace Structure
    {
        public partial class Side
        {
            #region Objects

            #region Search Criteria

            private HtmlGenericControl SearchCriteria
            {
                get
                {
                    HtmlGenericControl _SearchCriteria = SideObject("Criteri di ricerca", "Cirteria", false);
                    _SearchCriteria.Attributes["class"] += " SideSearchCriteriaInfo";

                    HtmlGenericControl dati = new HtmlGenericControl("div");

                    /*OrderBy_Search
                        Date_Search
                    DateType_Search
                        Tipo_Search
                    Titolo_Search*/

                    NetUtility.RequestVariable Titolo_Search = new NetUtility.RequestVariable("Titolo_Search", NetUtility.RequestVariable.RequestType.Form);
                    NetUtility.RequestVariable Date_Search = new NetUtility.RequestVariable("Date_Search", NetUtility.RequestVariable.RequestType.Form);
                    NetUtility.RequestVariable DateType_Search = new NetUtility.RequestVariable("DateType_Search", NetUtility.RequestVariable.RequestType.Form);
                    NetUtility.RequestVariable OrderBy_Search = new NetUtility.RequestVariable("OrderBy_Search", NetUtility.RequestVariable.RequestType.Form);
                    NetUtility.RequestVariable Tipo_Search = new NetUtility.RequestVariable("Tipo_Search", NetUtility.RequestVariable.RequestType.Form);
                    dati.InnerHtml += "";

                    if (Titolo_Search.IsValidString)
                    {
                        dati.InnerHtml += " che contengono il testo '" + Titolo_Search + "' nel titolo o nella descrizione";
                    }
                    if (Tipo_Search.IsValidInteger)
                    {
                        //DataTable table = PageData.Conn.SqlQuery("SELECT * FROM Applicazioni");
                        //DataRow[] row = table.Select("id_Applicazione = " + Tipo_Search.IntValue);
                        //if (row.Length > 0)
                        //{
                        //    if (dati.InnerHtml.Length > 0)
                        //        dati.InnerHtml += ",";
                        //    dati.InnerHtml += " di tipo " + row[0]["SystemName_Applicazione"].ToString() + "";
                        //}

                        Applications.Application dbApp = Applications.ApplicationBusinessLogic.GetApplicationById(Tipo_Search.IntValue);
                        if (dbApp != null)
                        {
                            if (dati.InnerHtml.Length > 0)
                                dati.InnerHtml += ",";
                            dati.InnerHtml += " di tipo " + dbApp.SystemName + "";
                        }
                    }
                    if (Date_Search.IsValid(NetUtility.RequestVariable.VariableType.Date) && DateType_Search.IsValidInteger)
                    {
                        if (dati.InnerHtml.Length > 0)
                            dati.InnerHtml += ",";
                        string Operator = "";
                        if (DateType_Search.IntValue == 1)
                            Operator = " precedente al";
                        if (DateType_Search.IntValue == 2)
                            Operator = " successiva al";
                        dati.InnerHtml += " la cui data di creazione sia " + Operator + " " + Date_Search.DateTimeValue.Day + "/" + Date_Search.DateTimeValue.Month + "/" + Date_Search.DateTimeValue.Year;
                    }

                    if (OrderBy_Search.IsValidInteger)
                    {
                        string OrderBy = "";
                        switch (OrderBy_Search.IntValue)
                        {
                            case 0:
                                OrderBy += " Nome";
                                break;
                            case 1:
                                OrderBy += " Percorso";
                                break;
                            case 2:
                                OrderBy += " Tipo";
                                break;
                            case 3:
                                OrderBy += " Data crescente";
                                break;
                            case 4:
                                OrderBy += " Data decrescente";
                                break;
                            default:
                                OrderBy += " Nome";
                                break;
                        }
                        if (dati.InnerHtml.Length > 0)
                            dati.InnerHtml += ",";
                        dati.InnerHtml += " ordinati per" + OrderBy;
                    }

                    if (dati.InnerHtml.Length > 0)
                        dati.InnerHtml = "<p>Documenti"+dati.InnerHtml+"</p>";
                        
                    else
                        dati.InnerHtml = "<p>Nessun Criterio Selezionato</p>";

                    _SearchCriteria.Controls[1].Controls.Add(dati);

                    return _SearchCriteria;
                }
            }

            #endregion

            #region MsgBox

            private HtmlGenericControl MsgBox
            {
                get
                {
                    return PageData.MsgBox.Box;
                }
            }

            #endregion

            #region Commands

            private HtmlGenericControl Commands
            {
                get
                {
                    if (_Commands == null)
                    {
                        _Commands = SideObject("Operazioni", "Commands", false);
                        _Commands.Attributes["class"] += " Commands";
                        _Commands.Controls[1].Controls.Add(SideOperationsBlock());
                    }
                    return _Commands;
                }
            }
            private HtmlGenericControl _Commands;


            #endregion

            #region AccountControl

            private HtmlGenericControl AccountControl
            {
                get
                {
                    HtmlGenericControl Object = new HtmlGenericControl("div");

                    Object.Attributes["id"] = "AccountControl";
                    Object.Attributes["class"] = "SideBlock SideBlockAccount";
                    
                    #region Forms

                    string forms = "<select name=\"Accounts\" id=\"Accounts\">";
                    string li = "<option {0} value=\"{2}\">{1}</option>";

                    DataTable users = PageData.Conn.SqlQuery("SELECT * FROM Users WHERE State_User = 1 AND Deleted_User = 0");

                    foreach (DataRow row in users.Rows)
                    {
                        string selected = PageData.Account != null && row["id_User"].ToString() == PageData.Account.ID.ToString() ? "selected=\"selected\"" : "";
                        forms += string.Format(li, selected, row["Name_User"].ToString(), row["id_User"].ToString());
                    }

                    forms += "</select>";

                    forms += "<input id=\"DebugLoginButton\" name=\"DebugLoginButton\" type=\"button\" value=\"Login\" onclick=\"Submit('DebugLogin')\" />";

                    #endregion

                    HtmlGenericControl title = new HtmlGenericControl("div");

                    title.Attributes["id"] = "AccountControl_Title";
                    title.Attributes["class"] = "SideBlock_Title";
                    //title.InnerHtml = "<a id=\"AccountControl_Link\" class=\"SideBlock_Link SideBlock_Collapsed\" href=\"javascript: ColapseSide('AccountControl')\"><span class=\"SideBlock_Text\">" + forms + "</span></a>";
                    title.InnerHtml = forms;

                    

                    Object.Controls.Add(title);

                    return Object;
                }
            }

            #endregion

            #region Info
            
            public string FooterContentHtml
            {
                get
                {
                    if (_FooterContentHtml == null)
                    {
                        _FooterContentHtml = Configurations.XmlConfig.GetNodeXml("/Portal/configs/admin-gui/footer");
                        if (_FooterContentHtml != null)
                            _FooterContentHtml = G2Core.Common.Utility.RemoveCDATA(_FooterContentHtml);
                    }
                    return _FooterContentHtml;
                }
            }
            private string _FooterContentHtml;

            private HtmlGenericControl Info(bool colapse)
            {
                HtmlGenericControl _Info = SideObject("About", "About", colapse);
                
                _Info.Attributes["class"] += " CurentObjectSideInfo";

                WebControl Content = new WebControl(HtmlTextWriterTag.Div);
                Content.ID = "About_Content";
                Content.CssClass = "content p-1";


                if (this.FooterContentHtml != null)
                    Content.Controls.Add(new LiteralControl(FooterContentHtml));

                _Info.Controls.Add(Content);

                return _Info;
            }

            #endregion

            #region Preferiti
            private HtmlGenericControl Preferiti
            {
                get
                {
                    if (_Preferiti == null)
                    {
                        HtmlGenericControl dati = new HtmlGenericControl("div");
                        dati.InnerHtml += Favorites;

                        _Preferiti = SideObject("Preferiti", "Preferiti",false, dati);
                        _Preferiti.Attributes["class"] += " Commands";
                    }
                    return _Preferiti;
                }
            }
            private HtmlGenericControl _Preferiti;
            #endregion

            #region Richieste Revisione
            private HtmlGenericControl Revisione
            {
                get
                {
                    //if (_Revisione == null)
                      //  _Revisione = Revisioni();

                    return _Revisione;
                }
            }
            private HtmlGenericControl _Revisione;
            #endregion

            #region Messenger
            private HtmlGenericControl Messenger
            {
                get
                {
                    if (_Messenger == null)
                        _Messenger = BuildMessengerInfo();

                    return _Messenger;
                }
            }
            private HtmlGenericControl _Messenger;
            #endregion

            #region FrontUsers
            private HtmlGenericControl FrontUsers
            {
                get
                {
                    if (_FrontUsers == null)
                        _FrontUsers = BuildFrontUsersInfo();

                    return _FrontUsers;
                }
            }
            private HtmlGenericControl _FrontUsers;
            #endregion

            #region Folders 2

            private HtmlGenericControl _AjaxFolders;
            private HtmlGenericControl AjaxFolders
            {
                get
                {
                    if (_AjaxFolders == null)
                    {
                        _AjaxFolders = SideObject("Cartelle", "Tree", false);
                        _AjaxFolders.Attributes["class"] += " SideFolders";
                        
                        HtmlGenericControl list = new HtmlGenericControl("ul");
                        list.ID = "FoldersTreeAjax";
                        _AjaxFolders.Controls[1].Controls.Add(list);

                    }
                    return _AjaxFolders;
                }
            }

            #endregion

            #region Ricerca

            private HtmlGenericControl _Search;
            private HtmlGenericControl Search
            {
                get
                {
                    if (_Search == null && PageData.CurrentFolder != null)
                    {
                        _Search = SideObject("Cerca", "Search",true);
                        ((HtmlGenericControl)_Search.Controls[1]).Attributes.CssStyle["display"] = "none";

                        _Search.Controls[1].Controls.Add(PageData.CurrentFolder.SearchForm);
                    }

                    return _Search;
                }
            }
            #endregion

            #region Version

            //private HtmlGenericControl _Version;
            //private HtmlGenericControl Version 
            //{
            //    get
            //    {
            //        if (_Version == null)
            //        {
            //            _Version = SideObject("Informazioni su SiteManager", "Version", false);
            //            _Version.Attributes["class"] += " CurentObjectSideInfo";

            //            HtmlGenericControl dati = new HtmlGenericControl("ul");

            //            dati.InnerHtml += "<li>SiteManager v." + PageData.ChangeLog.Version + "</li>";

            //            _Version.Controls[1].Controls.Add(dati);
            //        }

            //        return _Version;
            //    }
            //}
            #endregion
            #endregion

            #region Favorites

            private String _Favorites;
            private String Favorites
            {
                get
                {
                    if (_Favorites == null)
                    {
                        InitFavorites();
                    }
                    return _Favorites;
                }
            }
            private void InitFavorites()
            {
                if (PageData.Session["Favorites"] != null)
                {
                    _Favorites = (String)PageData.Session["Favorites"];
                }
                else if (PageData.Account!=null )
                {
                    /*string sql = "SELECT * FROM Favorites";
                    sql += " INNER JOIN Folders ON id_Folder = Folder_Favorite";
                    sql += " INNER JOIN FoldersLang ON id_Folder = Folder_FolderLang";
                    sql += " INNER JOIN Lang ON id_Lang = Lang_FolderLang";
                    sql += " INNER JOIN Networks ON id_Network = Network_Folder";
                    sql += " WHERE User_Favorite = " + PageData.Account.ID;
                    sql += " AND Default_Lang = 1 ORDER BY Label_FolderLang";
                    DataTable table = PageData.Conn.SqlQuery(sql);

                    if (table.Rows.Count > 0)
                    {
                        _Favorites = "<ul class=\"favorites\">";
                        foreach (DataRow favorite in table.Rows)
                        {
                            _Favorites += "<li class=\"SideToolbarButton\"><a alt=\"" + favorite["Label_FolderLang"] + " (" + favorite["Nome_Network"] + ")\" href=\"" + PageData.Paths.AbsoluteSiteManagerRoot + "/" + favorite["Nome_Network"] + "/cms/default.aspx?folder=" + favorite["Folder_Favorite"] + "\"><div style=\"background-image:url(" + PageData.Paths.AbsoluteRoot + "/css/default/toolbar/star.png)\" ><span>" + favorite["Label_FolderLang"] + "</span></div></a></li>";
                        }
                        _Favorites += "<li class=\"SideToolbarButton\"><a alt=\"Gestisci Preferiti\" href=\"" + PageData.Paths.AbsoluteSiteManagerRoot + "/favorites.aspx\"><div style=\"background-image:url(" + PageData.Paths.AbsoluteRoot + "/css/default/toolbar/manage.png)\" ><span>Gestisci Preferiti</span></div></a></li>";
                        _Favorites += "</ul>";

                    }
                    else
                        _Favorites += "Non ci sono preferiti<br />Per aggiungerne uno utilizzare l'apposito tasto presente in ogni cartella del gestore";
                    */

                    ICollection<NetworkFolder> favorites = UsersBusinessLogic.GetById(PageData.Account.ID).FavoritesFolders;

                    if (favorites.Count > 0)
                    {
                        _Favorites = "<ul class=\"favorites\">";
                        foreach (NetworkFolder favorite in favorites)
                        {
                            string favoriteLabel = (favorite as StructureFolder).Label;
                            _Favorites += "<li class=\"SideToolbarButton\"><a alt=\"" + favoriteLabel + " (" + favorite.Network.SystemName + ")\" href=\"" + PageData.Paths.AbsoluteSiteManagerRoot + "/" + favorite.Network.SystemName + "/cms/default.aspx?folder=" + favorite.ID + "\"><div style=\"background-image:url(" + PageData.Paths.AbsoluteRoot + "/css/default/toolbar/star.png)\" ><span>" + favoriteLabel + "</span></div></a></li>";
                        }
                        _Favorites += "<li class=\"SideToolbarButton\"><a alt=\"Gestisci Preferiti\" href=\"" + PageData.Paths.AbsoluteSiteManagerRoot + "/favorites.aspx\"><div style=\"background-image:url(" + PageData.Paths.AbsoluteRoot + "/css/default/toolbar/manage.png)\" ><span>Gestisci Preferiti</span></div></a></li>";
                        _Favorites += "</ul>";
                    }
                    else
                        _Favorites += "Non ci sono preferiti<br />Per aggiungerne uno utilizzare l'apposito tasto presente in ogni cartella del gestore";

                    PageData.Session["Favorites"] = _Favorites;
                }
                else
                    _Favorites += "";
                    
            } 

            #endregion

            private bool _ShowSearchFields;
            public bool ShowSearchFields
            {
                get { return _ShowSearchFields; }
                set { _ShowSearchFields = value; }
            }

            private bool _ShowSearchCriteria;
            public bool ShowSearchCriteria
            {
                get { return _ShowSearchCriteria; }
                set { _ShowSearchCriteria = value; }
            }
	
            public PageData PageData;

            public Side(PageData pageData)
            { 
                PageData = pageData;
                PageData.Side = this;                
            }

            public HtmlGenericControl getMenu()
            {
                //SetHeaderData();
                HtmlGenericControl Side = new HtmlGenericControl("div");
                Side.ID = "side";
                
                HtmlGenericControl container = new HtmlGenericControl("div");
                container.Attributes["class"] = "sideContainer";
                container.Controls.Add(MsgBox);

                HtmlGenericControl clicker = new HtmlGenericControl("div");
                clicker.Attributes["class"] = "sideClicker";
                clicker.ID = "sideClicker";
                clicker.InnerHtml = "<span>&nbsp;</span><strong id=\"sideClickerText\">Nascondi Barra</strong>";
                clicker.Attributes["onclick"] = "HideShowSide()";
               
                if (ShowSearchCriteria)
                    container.Controls.Add(SearchCriteria);

                if (PageData.CurentObjectInfoColtrol != null)
                    container.Controls.Add(PageData.CurentObjectInfoColtrol);

                container.Controls.Add(Commands);


                if (Revisione != null)
                    container.Controls.Add(Revisione);
                
                if(ShowSearchFields)
                    container.Controls.Add(Search);
                container.Controls.Add(Info(true));

                Side.Controls.Add(container);
                //Side.Controls.Add(clicker);

                return Side;
            }
            public HtmlGenericControl getHomeMenu()
            {
                HtmlGenericControl side = new HtmlGenericControl("div");
                side.ID = "side";

                side.Controls.Add(MsgBox);
                
                if (Revisione != null)
                    side.Controls.Add(Revisione);

                side.Controls.Add(Preferiti);

                if (_Revisione != null)
                    side.Controls.Add(Revisione);
                side.Controls.Add(Info(false));
                //side.Controls.Add(Version);

                return side;
            }
            public HtmlGenericControl getCommonMenu()
            {
                HtmlGenericControl side = new HtmlGenericControl("div");
                side.ID = "side";

                side.Controls.Add(MsgBox);
                side.Controls.Add(Commands);
                side.Controls.Add(Preferiti);

                if (_Revisione != null)
                    side.Controls.Add(Revisione);

                side.Controls.Add(Info(true));

                return side;
            }

            public HtmlGenericControl getLogoutControl()
            {
                HtmlGenericControl output = new HtmlGenericControl("li");

                Button bt = new Button();
                bt.Text = "Logout";
                bt.OnClientClick = "setSubmitSource('Logout')";
                bt.ID = "Logout";
                output.Controls.Add(bt);

                return output;
            }

            public static HtmlGenericControl SideObject(string Title, string id,bool colapsed)
            {
                HtmlGenericControl Object = BuildSideObject(Title, id, colapsed);
                               
                HtmlGenericControl Content = new HtmlGenericControl("div");
                Content.Attributes["id"] = id + "_Content";
                Content.Attributes["class"] = "content";

                Object.Controls.Add(Content);

                return Object;
            }
            public static HtmlGenericControl SideObject(string Title, string id, bool colapsed, HtmlGenericControl Content)
            {
                HtmlGenericControl Object = BuildSideObject(Title, id, colapsed);

                Content.Attributes["id"] = id + "_Content";
                Content.Attributes["class"] = "content";

                Object.Controls.Add(Content);

                return Object;
            }

            private static HtmlGenericControl BuildSideObject(string Title, string id, bool colapsed)
            {
                HtmlGenericControl Object = new HtmlGenericControl("div");

                Object.Attributes["id"] = id;
                Object.Attributes["class"] = "SideBlock";

                HtmlGenericControl title = new HtmlGenericControl("div");

                title.Attributes["id"] = id + "_Title";
                title.Attributes["class"] = "SideBlock_Title";
                title.InnerHtml = "<a id=\""+id+"_Link\" class=\"SideBlock_Link " + (colapsed ? "SideBlock_Collapsed" : "SideBlock_Opened") + "\" href=\"javascript: ColapseSide('" + id + "')\"><span class=\"SideBlock_Text\">" + Title + "</span></a>";
                
                Object.Controls.Add(title);
                
                return Object;            
            }

            public HtmlGenericControl BuildMessengerInfo()
            {
                HtmlGenericControl messenger = SideObject("Messaggi", "Messaggi", false);
                messenger.Attributes["class"] += " Commands";

                string sql = "";
                sql += "SELECT * FROM MessengerMessages";
                sql += " INNER JOIN MessengerReceivers ON id_Message = Message_Receiver";
                sql += " INNER JOIN Users ON id_User = Author_Message";
                sql += " INNER JOIN Anagrafica ON id_Anagrafica = Anagrafica_User";
                sql += " WHERE Readed_Receiver = 0 AND User_Receiver = " + PageData.Account.ID;
                sql += " ORDER BY Date_Message DESC";

                DataTable table = PageData.Conn.SqlQuery(sql);

                string msgs = "";
                HtmlGenericControl dati = new HtmlGenericControl("div");
                dati.InnerHtml += "<div class=\"MessengerInfo\" ><a title=\"Vai a Messenger\" href=\"" + PageData.Paths.AbsoluteSiteManagerRoot + "/applications/messenger/default.aspx\">";
                if (table.Rows.Count > 0)
                {
                    for (int i = 0; i < table.Rows.Count && i < 5; i++)
                    {
                        DataRow row = table.Rows[i];
                        msgs += @"
                        <li class=""SideToolbarButton"">
                            <a href=""" + PageData.Paths.AbsoluteSiteManagerRoot + @"/messenger/message_details.aspx?mess=" + row["id_Message"] + @""" >
                                <div style=""background-image:url(" + NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Email) + @")"">
                                <span>" + row["Title_Message"] + @" da " + row["Cognome_Anagrafica"] + @" " + row["Nome_Anagrafica"] + @"</span>
                                </div>
                            </a>
                        </li>";
                    }

                    dati.InnerHtml += "<strong>Ci sono " + table.Rows.Count + " nuovi messaggi</strong>";
                }
                else
                    dati.InnerHtml += "<strong>Non ci sono nuovi messaggi</strong>";
                
                dati.InnerHtml += "</a></div>";
                msgs += @"<li class=""SideToolbarButton"">
                            <a href=""" + PageData.Paths.AbsoluteSiteManagerRoot + @"/applications/messenger/default.aspx"" >
                                <div style=""background-image:url(" + NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Email_Go) + @")"">
                                <span>Vai a Messenger</span>
                                </div>
                            </a>
                        </li>";
                dati.InnerHtml += "<div><ul>" + msgs + "</ul></div>";
                messenger.Controls.Add(dati);
                return messenger;

            }
            public HtmlGenericControl Revisioni()
            {
                if (PageData.Network!=null)
                {
                    HtmlGenericControl Revisione = SideObject("Richieste Revisione", "Revisione",false);
                    string sql = "SELECT * FROM Revisioni_Notifiche";
                    sql += " INNER JOIN Docs ON id_Doc = Doc_RevisioneNotifica";
                    sql += " INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang";
                    sql += " INNER JOIN Applicazioni  ON Application_Doc = id_Applicazione";
                    sql += " INNER JOIN Users ON id_User = Mittente_RevisioneNotifica";
                    sql += " INNER JOIN Folders ON id_Folder = Folder_Doc";
                    sql += " INNER JOIN Networks ON id_Network = Network_Folder";
                    sql += " WHERE Destinatario_RevisioneNotifica = " + PageData.Account.ID;
                    sql += " AND Stato_RevisioneNotifica = 0";
                    sql += " AND Network_Folder = "+PageData.Network.ID;
                    sql += " AND Lang_DocLang = " + PageData.DefaultLang;
                    sql += " ORDER BY id_RevisioneNotifica DESC";
                    DataTable reviews = PageData.Conn.SqlQuery(sql);
                    int count = reviews.Rows.Count;
                    if (count > 0)
                    {
                        HtmlGenericControl dati = new HtmlGenericControl("div");
                        dati.InnerHtml = "<a href=\"" + PageData.Paths.AbsoluteNetworkRoot +"/cms/overviews.aspx\">";
                        dati.InnerHtml += "Lei ha " + reviews.Rows.Count + " richieste di revisione <br />";
                        dati.InnerHtml += "</a>";

                        if (count >= 5)
                            dati.InnerHtml += "<p>Ultime 5 Richeste: ";
                        else if (count > 1)
                            dati.InnerHtml += "<p>Ultime " + count + " Richeste: ";
                        else
                            dati.InnerHtml += "<p>Ultima Richesta: ";
                        for (int i = 0; i < count; i++)
                        {
                            if (i == 0)
                                dati.InnerHtml += "<ol>";
                            string app_folder = reviews.Rows[i]["Table_Applicazione"].ToString();
                            string networkname = reviews.Rows[i]["Nome_Network"].ToString();
                            dati.InnerHtml += "<li>";
                            dati.InnerHtml += "<a href=\"" + PageData.Paths.AbsoluteNetworkRoot + "/" + app_folder + "/" + app_folder + "_overview.aspx?folder=" + reviews.Rows[i]["Folder_Doc"] + "&amp;id=" + reviews.Rows[i]["id_Doc"] + "\">";
                            dati.InnerHtml += "Documento \"" + reviews.Rows[i]["Label_DocLang"] + "\" da " + reviews.Rows[i]["Name_User"] + "";
                            dati.InnerHtml += "</a>";
                            dati.InnerHtml += "</li>";
                            if (i + 1 == count)
                                dati.InnerHtml += "</ol>";
                        }
                        dati.InnerHtml += "</p>";

                        dati.InnerHtml += "<a href=\"" + PageData.Paths.AbsoluteNetworkRoot + "/cms/overviews.aspx\">";
                        dati.InnerHtml += "<strong>Elenco Completo</strong>";
                        dati.InnerHtml += "</a>";

                        Revisione.Controls.Add(dati);
                        return Revisione;
                    }
                }
                return null;
            }
            public HtmlGenericControl SideOperationsBlock()
            {
                return PageData.SideToolbar.getControl();
            }
            public HtmlGenericControl BuildFrontUsersInfo()
            {
                HtmlGenericControl messenger = SideObject("Utenti del Frontend", "FrontUsers", false);
                messenger.Attributes["class"] += " Commands";

                string sql = "";
                sql += "SELECT * FROM users_frontend INNER JOIN users_frontend_types ON id_FrontUserType = Stato_User WHERE BasicStatus_FrontUserType = 0";

                DataTable table = PageData.Conn.SqlQuery(sql);

                string msgs = "";
                HtmlGenericControl dati = new HtmlGenericControl("div");
                dati.InnerHtml += "<div class=\"MessengerInfo\" ><a title=\"Vai a Messenger\" href=\"" + PageData.Paths.AbsoluteSiteManagerRoot + "/users/users.aspx\">";
                if (table.Rows.Count > 0)
                {
                    dati.InnerHtml += "<strong>Ci sono " + table.Rows.Count + " nuovi utenti da convalidare</strong>";
                }
                else
                    return new HtmlGenericControl("span");

                dati.InnerHtml += "</a></div>";
                messenger.Controls.Add(dati);
                return messenger;

            }
        }
    }
}


