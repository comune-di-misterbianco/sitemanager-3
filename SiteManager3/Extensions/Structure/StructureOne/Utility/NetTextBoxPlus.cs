using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class CMSTextBoxPlus : NetTextBoxPlus
    {
        private NetCms.PageData PageData;
        public CMSTextBoxPlus(string label, string fieldname, NetCms.PageData pageData)
            : this(label, fieldname, pageData,ContentTypes.NormalText)
        {
        }

        public CMSTextBoxPlus(string label, string fieldname, NetCms.PageData pageData, ContentTypes type)
            : base(label, fieldname, type)
        {
            PageData = pageData;
        }

        private int _CurrentFolder = -1;
        public int CurrentFolder
        {
            get { return _CurrentFolder; }
            set { _CurrentFolder = value; }
        }

        public override HtmlGenericControl getControl()
        {

            HtmlGenericControl par = base.getControl();

            HtmlGenericControl lb = new HtmlGenericControl("div");
            //            lb.InnerHtml = "<a href=\"javascript: OpenCMSNavigatorPopup()\" >Seleziona un documento da questo sito</a>";
            //            lb.InnerHtml += @"<script type=""text/javascript"">
            //                            function OpenCMSNavigatorPopup()
            //                            {
            //				
            //	window.open("""+PageData.Paths.AbsoluteNetworkRoot+@"/cms/popup/navigate.aspx?targetctrid="+FieldName+@""","""",""height=600,width=800,status=no,toolbar=no,menubar=no,location=no"");	    
            //
            //                            }
            //                            </script>";

            //lb.InnerHtml = "<a href=\"" + PageData.Paths.AbsoluteNetworkRoot + @"/cms/popup/navigate.aspx?targetctrid=" + FieldName + "\" >Seleziona un documento da questo sito</a>";


            string StructureFolderSideTree = "";

            if (CurrentFolder != -1)
                StructureFolderSideTree = "&StructureFolderSideTree=" + CurrentFolder;

            //lb.InnerHtml = "<a class=\"open-modal\" href=\"" + PageData.Paths.AbsoluteNetworkRoot + @"/cms/popup/navigate.aspx?targetctrid=" + FieldName + StructureFolderSideTree + "&amp;modal=true\" >Seleziona documento</a>";
            //lb.InnerHtml += @"<script type=""text/javascript"">
            //                $(""a.open-modal"").bind(""click"", function() {
            //                   var $this = $(this);
            //                   var outputHolder = $(""<div id=\'uimodal-output\'></div>"");
            //                   $(""body #uimodal-output"").remove();
            //                   $(""body"").append(outputHolder);
            //                   outputHolder.load($this.attr(""href""), null, function() {
            //                      outputHolder.dialog({
            //                        autoOpen: true,
            //                        closeOnEscape: true,
            //                        resizable: false,
            //                        draggable: true,
            //                        position: [""100px"", ""50px""],
            //                        modal: true,
            //                        width:""auto"",
            //                        height:""auto"",
            //                        title: ""Seleziona File""
            //                      });
            //                   });
            //                   return false;
            //                });
            //                </script>";

            lb.InnerHtml += "<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#docPicker\">Seleziona documento</button>";
            //lb.InnerHtml += @"<script type=""text/javascript"">
            //                $(""a.open-modal"").bind(""click"", function() {
            //                   var $this = $(this);
            //                   var outputHolder = $(""<div id=\'uimodal-output\'></div>"");
            //                   $(""body #uimodal-output"").remove();
            //                   $(""body"").append(outputHolder);
            //                   outputHolder.load($this.attr(""href""), null, function() {
            //                      outputHolder.dialog({
            //                        autoOpen: true,
            //                        closeOnEscape: true,
            //                        resizable: false,
            //                        draggable: true,
            //                        position: 'center',
            //                        modal: true,
            //                        width:""auto"",
            //                        height:""auto"",
            //                        title: ""Seleziona immagine""
            //                      });
            //                   });
            //                   return false;
            //                });

            //                function closeModal()
            //                {                                 
            //                    $(""body #uimodal-output"").remove();                                
            //                    return false;
            //                }

            //                </script>";



            par.Controls.Add(lb);

            string modalUrl = PageData.Paths.AbsoluteNetworkRoot + @"/cms/popup/navigate.aspx?targetctrid=" + FieldName + StructureFolderSideTree + @"&modal=true";

            string modaleEditor = ""
                           + "<div id=\"docPicker\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\">"
                           + "   <div class=\"modal-dialog modal-dialog-centered modal-xlg\" role=\"document\">"
                           + "       <div class=\"modal-content\">"
                           + "               <div class=\"modal-body\">"
                           + "                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">"
                           + "                       <span aria-hidden=\"true\">&times;</span>"
                           + "                   </button>"
                           + "                  <iframe src=\""+ modalUrl + "\" width=\"100%\" height=\"800px\" frameborder=\"0\"></iframe>"
                           + "               </div>"
                           + "           </div>"
                           + "   </div>"
                           + "</div>";
            par.Controls.Add(new LiteralControl(modaleEditor));

            return par;
        }
    }

}