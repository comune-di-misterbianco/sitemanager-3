﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS;
using System.Web.UI;

namespace NetForms
{
    public class NetImagePicker : NetField
    {
        private NetCms.PageData PageData;     

        public NetImagePicker(string label, string fieldname, NetCms.PageData pageData)
            : base(label, fieldname)
        {
            PageData = pageData;
        }

        private bool _ReturnFilePath = false;
        public bool ReturnFilePath
        {
            get;
            set;
        }

        private int _CurrentFolder = -1;
        public int CurrentFolder
        {
            get { return _CurrentFolder; }
            set { _CurrentFolder = value; }
        }

        private Document _CurrentDocument;
        private Document CurrentDocument 
        {
            get
            {
                if (_CurrentDocument == null && !string.IsNullOrEmpty(Value))
                {
                    _CurrentDocument = DocumentBusinessLogic.GetById(int.Parse(Value));
                }
                return _CurrentDocument;
            }
        }

        public override System.Web.UI.HtmlControls.HtmlGenericControl getControl()
        {
            // check is postback
            if (PostBackValue != null)
            {
                Value = PostBackValue;
            }            

            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "netimagepicker";


            HtmlGenericControl lb = new HtmlGenericControl("div");
            lb.Attributes["class"] = "innercontent";

            string StructureFolderSideTree = "";
            if (CurrentFolder != -1)
                StructureFolderSideTree = "&amp;StructureFolderSideTree=" + CurrentFolder;

            //lb.InnerHtml = "<a class=\"open-modal\" href=\"" + PageData.Paths.AbsoluteNetworkRoot + @"/cms/popup/navigate.aspx?targetctrid=" + FieldName + "&amp;gridmode=true" + StructureFolderSideTree + "&amp;type=1&amp;modal=true\" >Seleziona immagine</a>";
            lb.InnerHtml += "<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#filebrowser\">Seleziona immagine</button>";

            string styleRemoveImage = "";
           
           if (CurrentDocument == null)
                 styleRemoveImage = @"style=""display:none !important""";

            //lb.InnerHtml += "<a id=\"removeimage\" " + styleRemoveImage + " class=\"remove-image btn-link\" href=\"#\" >Rimuovi immagine</a>";
            lb.InnerHtml += "<a id=\"removeimage\" " + styleRemoveImage + " class=\"remove-image btn btn-secondary ml-1\" role=\"button\" href=\"#\" >Rimuovi immagine</a>";

            lb.InnerHtml += @"<script type=""text/javascript"">
                            $(""a.open-modal"").bind(""click"", function() {
                               var $this = $(this);
                               var outputHolder = $(""<div id=\'uimodal-output\'></div>"");
                               $(""body #uimodal-output"").remove();
                               $(""body"").append(outputHolder);
                               outputHolder.load($this.attr(""href""), null, function() {
                                  outputHolder.dialog({
                                    autoOpen: true,
                                    closeOnEscape: true,
                                    resizable: false,
                                    draggable: true,
                                    position: 'center',
                                    modal: true,
                                    width:""auto"",
                                    height:""auto"",
                                    title: ""Seleziona immagine""
                                  });
                               });
                               return false;
                            });
                            function closeModal()
                            {                                 
                                $(""body #uimodal-output"").remove();                                
                                return false;
                            }
                            $(""a.remove-image"").bind(""click"", function() {                               
                               resetImageForm('" + NetCms.Configurations.Paths.AbsoluteRoot + @"', '" + FieldName + @"');       
                               return false;                         
                            });
                            </script>";

            div.Controls.Add(lb);

            HtmlGenericControl divImage = new HtmlGenericControl("div");
            divImage.Attributes["class"] = "imageContainer";

            HtmlImage images = new HtmlImage();
            images.ID = "selectedimage";
            images.Attributes["class"] = "img-fluid";

            if (!string.IsNullOrEmpty(Value) && int.Parse(Value) > 0)
            {

                if (CurrentDocument != null)
                {
                    images.Src = CurrentDocument.FrontendLink + "?preset=netimagepickerthumb";
                    images.Alt = CurrentDocument.Descrizione;
                }
            }
            else
            {
                images.Src = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/imagepicker/noimages.jpg" + "?preset=netimagepickerthumb";
                images.Alt = "Nessuna immagine selezionata";
            }

            divImage.Controls.Add(images);
            div.Controls.Add(divImage);

            HtmlInputHidden hidden = new HtmlInputHidden();
            hidden.ID = FieldName;
            hidden.Value = !string.IsNullOrEmpty(Value) ? Value : "-1";

            div.Controls.Add(hidden);

            #region code4BootstrapModal

            string modalUrl = PageData.Paths.AbsoluteNetworkRoot + @"/cms/popup/navigate.aspx?targetctrid=" + FieldName + "&amp;gridmode=true" + StructureFolderSideTree + "&amp;type=1&amp;modal=true";

            string modalBoostrap = ""
                       + "<div class=\"modal fade\" id=\"filebrowser\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"filebrowserModalCenterTitle\" aria-hidden=\"true\">"
                       + "  <div class=\"modal-dialog modal-dialog-centered modal-xlg\" role=\"document\">"                       
                       + "      <div class=\"modal-content\">"
                       + "          <div class=\"modal-body\">"
                       + "          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>"
                       + "              <iframe src=\"" + modalUrl +"\"  width=\"100%\" height=\"800px\" frameborder=\"0\"></iframe>"
                       + "          </div>"
                       + "      </div>"
                       + "  </div>"
                       + "</div>";
            div.Controls.Add(new LiteralControl(modalBoostrap));
            #endregion



            return div;
        }


        public System.Web.UI.HtmlControls.HtmlGenericControl getControl2()
        {
            // check is postback
            if (PostBackValue != null)
            {
                Value = PostBackValue;
            }

            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "netimagepicker";

            HtmlGenericControl lb = new HtmlGenericControl("div");

            string StructureFolderSideTree = "";
            if (CurrentFolder != -1)
                StructureFolderSideTree = "&amp;StructureFolderSideTree=" + CurrentFolder;

            lb.InnerHtml = "<a class=\"open-modal\" href=\"" + PageData.Paths.AbsoluteNetworkRoot + @"/cms/popup/navigate.aspx?targetctrid=" + FieldName + "&amp;gridmode=true" + StructureFolderSideTree + "&amp;type=1&amp;modal=true\" >Seleziona immagine</a>";

            string styleRemoveImage = "";

            if (CurrentDocument == null)
                styleRemoveImage = @"style=""display:none !important""";

            lb.InnerHtml += "<a id=\"removeimage\" " + styleRemoveImage + " class=\"remove-image btn btn-primary\" href=\"#\" >Rimuovi immagine</a>";

            lb.InnerHtml += @"<script type=""text/javascript"">
                            $(""a.open-modal"").bind(""click"", function() {
                               var $this = $(this);
                               var outputHolder = $(""<div id=\'uimodal-output\'></div>"");
                               $(""body #uimodal-output"").remove();
                               $(""body"").append(outputHolder);
                               outputHolder.load($this.attr(""href""), null, function() {
                                  outputHolder.dialog({
                                    autoOpen: true,
                                    closeOnEscape: true,
                                    resizable: false,
                                    draggable: true,
                                    position: 'center',
                                    modal: true,
                                    width:""auto"",
                                    height:""auto"",
                                    title: ""Seleziona immagine""
                                  });
                               });
                               return false;
                            });
                            function closeModal()
                            {                                 
                                $(""body #uimodal-output"").remove();                                
                                return false;
                            }
                            $(""a.remove-image"").bind(""click"", function() {                               
                               resetImageForm('" + NetCms.Configurations.Paths.AbsoluteRoot + @"', '"+ FieldName + @"');       
                               return false;                         
                            });
                            </script>";

            div.Controls.Add(lb);
            HtmlGenericControl divImage = new HtmlGenericControl("div");
            divImage.Attributes["class"] = "imageContainer";

            HtmlImage images = new HtmlImage();
            images.ID = "selectedimage";
            if (!string.IsNullOrEmpty(Value))//&& int.Parse(Value) > 0)
            {

                if (CurrentDocument != null)
                {
                    images.Src = CurrentDocument.FrontendLink + ".ashx?preset=netimagepickerthumb";
                    images.Alt = CurrentDocument.Descrizione;
                }
            }
            else
            {
                images.Src = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/imagepicker/noimages.jpg" + "?preset=netimagepickerthumb";
                images.Alt = "Nessuna immagine selezionata";
            }

            divImage.Controls.Add(images);
            div.Controls.Add(divImage);

            HtmlInputHidden hidden = new HtmlInputHidden();
            hidden.ID = FieldName;
            hidden.Value = Value;

            div.Controls.Add(hidden);

            return div;
        }



        public override string validateInput(string input)
        {
            return "";
        }

        public override string getFilter(string value)
        {
            string filter = " ";
            return filter;
        }
    }
}
