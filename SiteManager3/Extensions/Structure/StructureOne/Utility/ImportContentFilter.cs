﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace NetCms.Structure.Utility
{
    public static class ImportContentFilter
    {
        public static string WebRoot
        { get { return "<%= WebRoot %>"; } }

        /// <summary>
        /// Metodo pubblico per il parsing e l'adattamento degli URL presenti all'interno del contenuto di un campo.
        /// Da utilizzare per tutti i campi che hanno un contenuto tipo webdocs,ecc.
        /// </summary>
        /// <param name="originalHtml"></param>
        /// <returns></returns>
        public static string ParseImportedContentUrls(string originalHtml)
        {
            //var baseUri = new Uri("");
            var pattern = @"(?<name>src|href)=""(?<value>/[^""]*|" + WebRoot + @"[^""]*)""";
            var matchEvaluator = new MatchEvaluator(
                match =>
                {
                    var value = match.Groups["value"].Value;

                    string newValue = HttpUtility.UrlDecode(value);
                    //if (value.StartsWith("/") || value.StartsWith(WebRoot))

                    string portalDomain = NetCms.Configurations.Debug.GenericEnabled ? NetCms.Configurations.PortalData.PortalFakeDomain : NetCms.Configurations.PortalData.PortalAddress;

                    if (newValue.Contains("http://") || newValue.Contains("https://"))
                    {
                        // rimuovo il dominio associato al portale corrente 
                        if (portalDomain.Length > 0 && newValue.Contains(portalDomain))
                        {
                            newValue = newValue.Replace(portalDomain, "/");
                            //newValue = UrlImportedAdapter(HttpUtility.UrlDecode(value));
                            //newValue = (newValue.StartsWith(WebRoot) ? newValue : WebRoot + (newValue.StartsWith("/" + NetCms.Networks.NetworksManager.CurrentFakeNetwork) ? WebRoot + newValue.Replace("/" + NetCms.Networks.NetworksManager.CurrentFakeNetwork, "") : WebRoot + newValue));
                        }
                    }

                    newValue = UrlImportedAdapter(HttpUtility.UrlDecode(value));

                    //Uri uri;

                    //if (Uri.TryCreate(baseUri, newValue, out uri))
                    //{
                    var name = match.Groups["name"].Value;




                    //string newValue = (newValue.StartsWith(WebRoot) ? newValue : (WebRoot + (newValue.StartsWith("/" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Path) ? newValue.Replace("/" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Path, "") : newValue)));

                    if (!newValue.StartsWith(WebRoot))
                    {
                        if (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Path.Length > 0)
                        {
                            if (newValue.StartsWith("/" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Path))
                            {
                                newValue = WebRoot + newValue.Replace("/" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Path, "");
                            }
                        }
                        else
                        {
                            newValue = WebRoot + newValue;
                        }
                    }

                    return string.Format("{0}=\"{1}\"", name, newValue);
                    //}
                    
                    //return null;
                });
            var adjustedHtml = Regex.Replace(originalHtml, pattern, matchEvaluator);
            return Encoding.UTF8.GetString(Encoding.Convert(Encoding.UTF8,Encoding.UTF8,Encoding.UTF8.GetBytes(adjustedHtml.ToCharArray())));
        }


        /// <summary>
        /// Metodo pubblico per il parsing e adattamento degli URL. Da utilizzare per tutti i campi che contengono solo l'url
        /// </summary>
        /// <param name="originalUrl"></param>
        /// <returns></returns>
        public static string ParseImportedCleanUrls(string originalUrl)
        {
            string newUrl = originalUrl;
            string portalDomain = NetCms.Configurations.Debug.GenericEnabled ? NetCms.Configurations.PortalData.PortalFakeDomain : NetCms.Configurations.PortalData.PortalAddress;

            if (newUrl.Contains("http://") || newUrl.Contains("https://"))
            {
                // rimuovo il dominio associato al portale corrente 
                if (portalDomain.Length > 0 && newUrl.Contains(portalDomain))
                {
                    newUrl = newUrl.Replace(portalDomain, "/");
                    newUrl = UrlImportedAdapter(newUrl);
                    
                    //newUrl = (newUrl.StartsWith(WebRoot) ? newUrl : WebRoot + (newUrl.StartsWith("/" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Path) ? WebRoot + newUrl.Replace("/" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Path, "") : WebRoot + newUrl));

                    if (!newUrl.StartsWith(WebRoot))
                    {
                        if (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Path.Length > 0)
                        {
                            if (newUrl.StartsWith("/" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Path))
                            {
                                newUrl = WebRoot + newUrl.Replace("/" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Path, "");
                            }
                        }
                        else
                        {
                            newUrl = WebRoot + newUrl;
                        }
                    }
                }
            }
            else 
            {
                newUrl = UrlImportedAdapter(newUrl);
              
                //  newUrl = (newUrl.StartsWith(WebRoot) ? newUrl : (newUrl.StartsWith("/" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Path) ? WebRoot + newUrl.Replace("/" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Path, "") : WebRoot + newUrl));


                if (!newUrl.StartsWith(WebRoot))                
                {
                    if (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Path.Length > 0)
                    {
                        if (newUrl.StartsWith("/" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Path))
                        {
                            newUrl = WebRoot + newUrl.Replace("/" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Path, "");
                        }
                    }
                    else
                    {
                        newUrl = WebRoot + newUrl;
                    } 
                }

            }

            return newUrl;
        }
        

        /// <summary>
        /// Metodo privato che si occupa delle procedure di adattamento degli url.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private static string UrlImportedAdapter(string url)
        {
            string newUrl = "";

            if (url.Contains(WebRoot))
            {
                if (!url.Contains(WebRoot + "/"))
                    url = url.Replace(WebRoot, WebRoot + "/");
            }

            string[] pathSplitted = url.Split('/');
            foreach (string s in pathSplitted)
            {
                if (s.Length > 0 && s != "/" && s != WebRoot)
                    newUrl += "/" + ((url.EndsWith(s) && s.Contains('.')) ? (s.EndsWith(".aspx") ? ClearFileSystemName(s.Remove(s.Length-4,4)) + ".aspx" :s) : FsFolderName(s));
            }

            return newUrl;
        }

        /// <summary>
        /// Metodo contenente le regole di filtraggio dei nomi di file
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static string ClearFileSystemName(string filename)
        {
            char[] chars = filename.ToLower().ToCharArray();
            string FileSystemName = "";

            for (int i = 0; i < chars.Length && i < 256; i++)
            {
                char carattere = chars[i];
                switch (carattere)
                {
                    case 'à': carattere = 'a'; break;
                    case 'è': carattere = 'e'; break;
                    case 'é': carattere = 'e'; break;
                    case 'ì': carattere = 'i'; break;
                    case 'ò': carattere = 'o'; break;
                    case 'ù': carattere = 'u'; break;
                }
                FileSystemName += ((carattere >= 97 && carattere <= 122) || (carattere >= 48 && carattere <= 57)) ? carattere : '-';
            }
            while (FileSystemName.Contains("--")) FileSystemName = FileSystemName.Replace("--", "-");

            //Rimuovo i trattini alla fine del nome
            while (FileSystemName.EndsWith("-"))
                FileSystemName = FileSystemName.Remove(FileSystemName.Length - 1);

            //Rimuovo i trattini all inizio del nome
            while (FileSystemName.StartsWith("-"))
                FileSystemName = FileSystemName.Substring(1, FileSystemName.Length - 1);

            return FileSystemName;
        }

        /// <summary>
        /// Metodo privato contenente le regole di adattamento dei nomi cartelle
        /// </summary>
        /// <param name="folderName"></param>
        /// <returns></returns>
        private static string FsFolderName(string folderName)
        {
            string FileSystemName = folderName;
            FileSystemName = FileSystemName.Trim();
            FileSystemName = FileSystemName.ToLower();

            for (int i = 0; i < FileSystemName.Length; i++)
            {
                int integerValue;
                int convertedPw = Convert.ToInt32(FileSystemName[i]);
                if (!int.TryParse(FileSystemName[i].ToString(), out integerValue) && (convertedPw < 97 || convertedPw > 122))
                    FileSystemName = FileSystemName.Replace(FileSystemName[i], '-');
            }
            //Rimuovo i doppi trattini
            while (FileSystemName.Contains("--"))
                FileSystemName = FileSystemName.Replace("--", "-");

            //Rimuovo i trattini alla fine del nome
            while (FileSystemName.EndsWith("-"))
                FileSystemName = FileSystemName.Remove(FileSystemName.Length - 1);

            //Rimuovo i trattini all inizio del nome
            while (FileSystemName.StartsWith("-"))
                FileSystemName = FileSystemName.Substring(1, FileSystemName.Length - 1);

            //Rimuovo i punti alla fine del nome
            while (FileSystemName.EndsWith("."))
                FileSystemName = FileSystemName.Remove(FileSystemName.Length - 1);

            //Taglio il nome della cartella se è più lungo di 60 caratteri
            if (FileSystemName.Length > 60)
                FileSystemName = FileSystemName.Remove(60);

            while (FileSystemName.EndsWith("-"))
                FileSystemName = FileSystemName.Remove(FileSystemName.Length - 1);
            return FileSystemName;
        }
    }
}
