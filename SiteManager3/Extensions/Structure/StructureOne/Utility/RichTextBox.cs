﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Web.UI;
using NetCms.Structure.GUI.FileSystemNavigator.Model;

namespace NetForms
{
    public class RichTextBox : NetField
    {
        public RichTextBox(string label, string fieldname, string toolbarConfigToUse = "full")
            : base(label, fieldname)
        {
            ToolbarConfigToUse = toolbarConfigToUse;
        }


        private static NetCms.Vertical.Configuration.Configuration Configuration
        {
            get
            {
                return _Configuration ?? (_Configuration = new NetCms.Vertical.Configuration.Configuration("ckeditor"));
            }
        }
        private static NetCms.Vertical.Configuration.Configuration _Configuration;

        private List<string> ToolbarConfig
        {
            get
            {
                try
                {
                    if (Configuration != null && Configuration["ToolbarConfigurations"] != null)
                        return Configuration["ToolbarConfigurations"].ToLower().Split(',').ToList();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Configurazione di ckeditor nel web.config mancante!");
                }
                return null;
            }
        }

        public string ToolbarConfigToUse
        {
            get;
            set;
        }

        public string Width
        {
            get
            {
                try
                {
                    if (Configuration != null && Configuration["width"] != null)
                        return Configuration["width"];
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Configurazione di ckeditor nel web.config mancante!");
                }
                return "720";
            }     
        }

        public string Height
        {
            get
            {
                try
                {
                    if (Configuration != null && Configuration["height"] != null)
                        return Configuration["height"];
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Configurazione di ckeditor nel web.config mancante!");
                }
                return "440";
            }
        }

        private string VerifyToolbarConfig()
        {
            if (ToolbarConfig != null && ToolbarConfig.Contains(ToolbarConfigToUse.ToLower()))
                return ToolbarConfigToUse;
            return "default";
        }

        private int _CurrentFolder = -1;
        public int CurrentFolder 
        {
            get { return _CurrentFolder; }
            set { _CurrentFolder = value; }
        }


        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl par = new HtmlGenericControl(DefaultControlTag);

            Label lb = new Label();
            lb.AssociatedControlID = _FieldName;
            if (Required)
                lb.Text = _Label + "*";
            else
                lb.Text = _Label;
            lb.Style.Add("display", "block");
            par.Controls.Add(lb);

                
            HtmlTextArea TargetCKEditorTextBox = new HtmlTextArea();
            //TargetCKEditorTextBox.Attributes["class"] = "ckeditor";
            TargetCKEditorTextBox.Name = "TargetFreeTextBox";
            TargetCKEditorTextBox.ID = _FieldName;
            // pulisco dal codice html eventuali refusi introdotti in fase di elaborazione
            TargetCKEditorTextBox.Value = System.Web.HttpContext.Current.Server.HtmlDecode(Value.Replace("&lt;%= WebRoot %&gt;", "")); 
            par.Controls.Add(TargetCKEditorTextBox);
            
            string StructureFolderSideTree = "";
            if (CurrentFolder != -1)
                StructureFolderSideTree = "&StructureFolderSideTree=" + CurrentFolder;


            string CKEditorInit = @"<script type=""text/javascript"">
                      
                            CKEDITOR.replace('" + _FieldName + @"', {
                                width: '"+Width+@"',
                                height: '"+Height+@"',
                                toolbar: '" + VerifyToolbarConfig() + @"',
                                filebrowserBrowseUrl: '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteAdminRoot + @"/cms/popup/navigate.aspx?targetctrid=" + FieldName + StructureFolderSideTree + @"',
                                filebrowserImageBrowseUrl: '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteAdminRoot + @"/cms/popup/navigate.aspx?type=" + (int)TipoSelettore.Images + "&targetctrid=" + FieldName + StructureFolderSideTree + @"',
                                filebrowserFlashBrowseUrl: '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteAdminRoot + @"/cms/popup/navigate.aspx?type=" + (int)TipoSelettore.Flash + "&targetctrid=" + FieldName + StructureFolderSideTree + @"',
                                filebrowserWindowWidth : '" + Width + @"',
                                filebrowserWindowHeight : '" + Height + @"'
                            }); 
                        

                            /* override dialog */
                            CKEDITOR.on( 'dialogDefinition', function(ev)
                                {
                                    var editor = ev.editor;
                                    var dialogName = ev.data.name; 
                                    var dialogDefinition = ev.data.definition;

                                    // This function will be called when the user will pick a file in file manager
                                    var cleanUpFuncRef = CKEDITOR.tools.addFunction(function (docUrl)
                                    {    
                                        $('#file_browser-modal').modal('hide');                                        
                                        CKEDITOR.tools.callFunction(editor._.filebrowserFn, docUrl); 
                                    });

                                    var tabCount = dialogDefinition.contents.length - 1;
	                                for (var i = 0; i < tabCount; i++) {
	                                    var browseButton = dialogDefinition.contents[i].get('browse');
	                                    if (browseButton !== null) {
                                                browseButton.onClick = function (dialog, i)
                                                {
                                                    editor._.filebrowserSe = this;
                                                    
                                                    var filebrowserUrl = editor.config.filebrowserBrowseUrl+'&CKEditor=body&CKEditorFuncNum='+cleanUpFuncRef; 
                                                    if (dialogName == 'image2')
                                                      filebrowserUrl = editor.config.filebrowserImageBrowseUrl+'&CKEditor=body&CKEditorFuncNum='+cleanUpFuncRef;                                                     

                                                    var iframe = $('#file_browser-modal').find('iframe').attr({                                                                                                                 
                                                        src:filebrowserUrl
                                                    });

                                                    $('#file_browser-modal').appendTo('body').modal('show');
                                                }
                                           }
                                       }
                               });
                            
                            

                            </script>";
            
            par.Controls.Add(new LiteralControl(CKEditorInit));


            string modaleEditor = "" 
                          + "<div id=\"file_browser-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\">"
                          + "   <div class=\"modal-dialog modal-dialog-centered modal-xlg\" role=\"document\">"
                          + "       <div class=\"modal-content\">"                                                   
                          + "               <div class=\"modal-body\">"
                          + "                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">"
                          + "                       <span aria-hidden=\"true\">&times;</span>"
                          + "                   </button>"
                          + "                  <iframe width=\"100%\" height=\"800px\" frameborder=\"0\"></iframe>"
                          + "               </div>"
                          + "           </div>"                          
                          + "   </div>"
                          + "</div>";
            par.Controls.Add(new LiteralControl(modaleEditor));
            return par;
        }

            public int MaxLenght = 160000;

        public override string validateInput(string input)
        {
            string errors = "";

            //string innerText = Regex.Replace(input.Trim(), @"</?\w+((\s+\w+(\s*=\s*(?:"".*?""|'.*?'|[^'"">\s]+))?)+\s*|\s*)/?>", "").Trim();
            if (Required && (input.Length == 0))// || string.IsNullOrEmpty(innerText)))
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio], this.Label);
                errors += "</li>";
            }
            if (input.Length > MaxLenght)
                errors += "<li>La lunghezza massima del campo " + Label + " è " + MaxLenght + " caratteri";
            return errors;
        }

        public override string getFilter(string value)
        {
            string filter = " ";
            if (value.Length > 0)
            {
                filter += _FieldName;

                filter += " Like '%";
                filter += value;
                filter += "%'";
            }
            return filter;
        }
    }
}
