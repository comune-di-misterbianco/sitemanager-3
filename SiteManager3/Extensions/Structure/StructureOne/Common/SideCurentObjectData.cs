using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;
using NetCms.Front;
using System.Linq;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Grants;

namespace NetCms
{
    namespace Structure
    {
    public class SideCurentObjectData
    {
        private enum SideCurentObjectDataType
        {
            Folder,
            Document
        }

        private string ObjectTypeName
        {
            get
            {
                if (ObjectType == SideCurentObjectDataType.Document)
                    return "Documento";
                else
                    return "Cartella";
            }
        }
        private SideCurentObjectDataType ObjectType;

        private StructureFolder Folder;
        private Document Doc;
        private string ID;
        private PageData PageData;

        public SideCurentObjectData(string id,StructureFolder folder, PageData pageData)
        {
            ID = id;
            Folder = folder;
            ObjectType = SideCurentObjectDataType.Folder;
            PageData = pageData;
        }
        public SideCurentObjectData(string id, Document doc, PageData pageData)
        {
            ID = id;
            Doc = doc;
            ObjectType = SideCurentObjectDataType.Document;
            PageData = pageData;
        }

        public HtmlGenericControl getControl()
        {
            HtmlGenericControl control = new HtmlGenericControl("div");

            control = Side.SideObject("Informazioni " + ObjectTypeName + " Corrente", ID,false);
            control.Attributes["class"] += " CurentObjectSideInfo";

            if (ObjectType == SideCurentObjectDataType.Document)
                control.Controls[1].Controls.Add(DocumentInfo());
            else
                control.Controls[1].Controls.Add(FolderInfo());

            return control;
        }
        
        private HtmlGenericControl FolderInfo()
        {
            HtmlGenericControl control = new HtmlGenericControl("ul");

            control.InnerHtml = "";

            control.InnerHtml += "<li>";
            control.InnerHtml += "<strong>Nome: </strong>" +  Folder.Label;
            control.InnerHtml += "</li>";

            control.InnerHtml += "<li>";
            control.InnerHtml += "<strong>Tipo: </strong>" + Folder.TypeName;
            control.InnerHtml += "</li>";

            if (Folder.Criteria[CriteriaTypes.Advanced].Allowed)
            {
                control.InnerHtml += "<li>";
                control.InnerHtml += "<strong>Titolo del Frontend: </strong>" + (Folder.AddKeywordsToTitle ? "Keywords" : "Path");
                control.InnerHtml += "</li>";

                string rss = "";
                switch (Folder.RssValue)
                {
                    case StructureFolder.RssStatus.Disabled: rss = "Disabilitati"; break;
                    case StructureFolder.RssStatus.Enabled: rss = "Abilitati"; break;
                    case StructureFolder.RssStatus.Inheriths: rss = (Folder.EnableRss ? "Abilitati" : "Disabilitati") + "(Imp.Eredita)"; break;
                }
                if (rss.Length > 0)
                {
                    control.InnerHtml += "<li>";
                    control.InnerHtml += "<strong>Rss: </strong>" + rss;
                    control.InnerHtml += "</li>";
                }
            }

            if (Folder.Depth > 0)
            {
                control.InnerHtml += "<li>";
                control.InnerHtml += "<strong>Stato: </strong>";

                if (Folder.IsSystemFolder)
                    control.InnerHtml += "Non Attiva";
                else
                    control.InnerHtml += Folder.StatusLabel;
            }
            if (Folder.FrontLayout!= FrontLayouts.SystemError)
            {
                control.InnerHtml += "<li>";
                control.InnerHtml += "<strong>Colonne abilitate: </strong>";
                switch (Folder.FrontLayout)
                {
                    case FrontLayouts.All: control.InnerHtml += "Tutte"; break;
                    case FrontLayouts.Left: control.InnerHtml += "Solo Sinistra"; break;
                    case FrontLayouts.Right: control.InnerHtml += "Solo Destra"; break;
                    case FrontLayouts.Footer: control.InnerHtml += "Solo Footer"; break;
                    case FrontLayouts.LeftFooter: control.InnerHtml += "Sinistra e Footer"; break;
                    case FrontLayouts.RightFooter: control.InnerHtml += "Destra e Footer"; break;
                    case FrontLayouts.Inherits: control.InnerHtml += "Eredita"; break;
                }
            }
            control.InnerHtml += "</li>";
            
            return control;
        }
        private HtmlGenericControl DocumentInfo()
        {
            HtmlGenericControl control = new HtmlGenericControl("ul");
            StructureFolder folder = FolderBusinessLogic.GetById(Doc.Folder.ID);
            Applications.Application app = folder.RelativeApplication;

            control.InnerHtml = "";

            control.InnerHtml += "<li>";
            control.InnerHtml += "<strong>Titolo: </strong>" + Doc.Nome;
            control.InnerHtml += "</li>";

            control.InnerHtml += "<li>";
            control.InnerHtml += "<strong>Tipo: </strong>" + app.Label;
            control.InnerHtml += "</li>";

            control.InnerHtml += "<li>";
            control.InnerHtml += "<strong>Cartella: </strong><a title=\"" + folder.LabeledPath + "\" href=\"" + folder.BackofficeHyperlink + "\">" + folder.Label + "</a>";
            control.InnerHtml += "</li>";

            control.InnerHtml += "<li>";
            control.InnerHtml += "<strong>Data Creazione: </strong>" + Doc.Date;
            control.InnerHtml += "</li>";

            control.InnerHtml += "<li>";
            control.InnerHtml += "<strong>Data Ultima Modifica: </strong>" + Doc.LastModify;
            control.InnerHtml += "</li>";
            
            control.InnerHtml += "<li>";
            control.InnerHtml += "<strong>Stato: </strong>" + Doc.StatusLabel;
            control.InnerHtml += "</li>";
            
            control.InnerHtml += "<li>";
            control.InnerHtml += "<strong>Creato da: </strong>" + Doc.AuthorName;
            control.InnerHtml += "</li>";

            if (Doc.Criteria[CriteriaTypes.Advanced].Allowed)
            {
                control.InnerHtml += "<li>";
                control.InnerHtml += "<strong>Titolo del Frontend: </strong>" + (Doc.AddKeywordsToTitle ? "Keywords" : "Path");
                control.InnerHtml += "</li>";
            }

            if (folder.ReviewEnabled)
            {
                control.InnerHtml += "<li>";
                control.InnerHtml += "<strong>Stato Revisione: </strong>" + (Doc.DocLangs.ElementAt(0).Content != null ? "Pubblicato" : "Non Pubblicato");
                control.InnerHtml += "</li>";

                control.InnerHtml += "<li>";
                control.InnerHtml += "<strong>Contenuto revisione di: </strong>" + Doc.ReviewAuthorName;
                control.InnerHtml += "</li>";

            }
            return control;
        }
    }
    }
}
