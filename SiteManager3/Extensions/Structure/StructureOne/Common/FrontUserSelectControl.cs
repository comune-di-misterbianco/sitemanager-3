using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using HtmlGenericControls;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms
{
    public partial class FrontUserSelectControl : HtmlGenericControl
    {
        private const string BoxDefaultValue = "Ricerca (Cognome,Nome o eMail)";

        private PageData PageData;

        private String usersQuery;

        public String UsersQuery
        {
            get
            {
                if (usersQuery == null)
                {
                    usersQuery = "SELECT * FROM Users";// INNER JOIN Anagrafica ON id_Anagrafica = Anagrafica_User";
                    //if(SqlWhere.Length>0)
                    usersQuery += " WHERE Deleted_User = 0 AND State_User = 1 ";
                }
                return usersQuery;
            }
            set { usersQuery = value; }
        }


        private bool _CmsUsers;
        private bool CmsUsers
        {
            get { return _CmsUsers; }
        }

        private string Action;

        private int _RPP;
        private int RPP
        {
            get { return _RPP; }
        }

        private string PageRequestKey
        {
            get { return this.ID + "_Page"; }

        }
        private string SearchButtonKey
        {
            get { return this.ID + "_SearchButton"; }

        }

        private NetUtility.RequestVariable PageRequest;
        private int _Page;
        public int CurentPage
        {
            get
            {
                if (PageRequest == null)
                {
                    PageRequest = new NetUtility.RequestVariable(PageRequestKey, NetUtility.RequestVariable.RequestType.Form);
                    if (PageRequest.IsValidInteger && PageRequest.IntValue > 0)
                        _Page = PageRequest.IntValue;
                    else
                        _Page = 1;
                }
                return _Page;
            }
        }
        //private const int CurentPage = 1;

        public FrontUserSelectControl(PageData pageData, string id, int rpp, string action, bool cmsusers, string userquery)
        {
            _RPP = rpp;
            PageData = pageData;
            this.ID = id;
            Action = action;
            _CmsUsers = cmsusers;
            //usersQuery = userquery;
            InitControls();
            this.Attributes["class"] = "FrontUserSelectControl";
            this.TagName = "div";
            PageData.OuterScripts = "   <script type=\"text/javascript\" src=\"" + PageData.RootDist + "scripts/FrontUserSelectControl.js\" ></script>\n";

        }
        public FrontUserSelectControl(PageData pageData, string id, int rpp, string action, bool cmsusers)
            : this(pageData, id, rpp, action, cmsusers, null)
        {
        }

        public FrontUserSelectControl(PageData pageData, string id, int rpp, string action)
            : this(pageData, id, rpp, action, false)
        {
        }
        public FrontUserSelectControl(PageData pageData, string id, int rpp)
            : this(pageData, id, rpp, "", false)
        {
        }

        private void InitControls()
        {
            #region Codice di controllo sulla ricerca

            string SqlWhere = "";
            NetUtility.RequestVariable SearchRequest = new NetUtility.RequestVariable(this.ID + "_SearchBox", NetUtility.RequestVariable.RequestType.Form);
            if (PageData.IsPostBack &&
                SearchRequest.IsValidString &&
                SearchRequest.StringValue != BoxDefaultValue &&
                SearchRequest.StringValue.Length > 0)
            {
                //SqlWhere = " AND ( Nome_Anagrafica LIKE '%" + SearchRequest.StringValue + "%' ";
                //SqlWhere += " OR Cognome_Anagrafica LIKE '%" + SearchRequest.StringValue + "%' ";
                //SqlWhere += " OR Name_User LIKE '%" + SearchRequest.StringValue + "%' )";
                //SqlWhere = " ( Nome_Anagrafica LIKE '%" + SearchRequest.StringValue + "%' ";
                //SqlWhere += " OR Cognome_Anagrafica LIKE '%" + SearchRequest.StringValue + "%' ";
                SqlWhere = " OR Name_User LIKE '%" + SearchRequest.StringValue + "%' )";
            }

            #endregion
            HtmlGenericControls.Div Contents = new HtmlGenericControls.Div();
            this.Controls.Add(Contents);
            Contents.Attributes["class"] = "FrontUserSelectControl_Contents";

            HtmlGenericControls.Div title = new HtmlGenericControls.Div();
            title.Attributes["class"] = "Title";
            title.InnerHtml = "Elenco Utenti" + (CmsUsers ? "" : "Registrati al SitoWeb");

            Contents.Controls.Add(title);

            HtmlGenericControls.Div search = new HtmlGenericControls.Div();
            search.Attributes["class"] = "Search";

            TextBox searchbox = new TextBox();
            searchbox.Attributes["class"] = "textbox";
            searchbox.Attributes["onfocus"] = "if(this.value=='" + BoxDefaultValue + "')this.value=''";
            searchbox.Attributes["onblur"] = "if(this.value=='')this.value='" + BoxDefaultValue + "'";
            searchbox.ID = SearchRequest.Key;
            searchbox.Text = SearchRequest.IsValid() ? SearchRequest.StringValue : BoxDefaultValue;
            search.Controls.Add(searchbox);
            Contents.Controls.Add(search);

            HtmlGenericControl bt = new HtmlGenericControl("span");
            bt.InnerHtml = "<a title=\"Ricerca\" class=\"btsearch\" href=\"javascript: FrontUserSelectControl_Submit()\" id=\"" + SearchButtonKey + "\"><span>Cerca</span>&nbsp;</a>";
            bt.InnerHtml += "<a title=\"Azzera Ricerca\" class=\"btreset\" href=\"javascript: FrontUserSelectControl_Reset()\" id=\"" + this.ID + "_ResetButton\"><span>Azzera</span>&nbsp;</a>";
            bt.InnerHtml += "<input type=\"hidden\" name=\"" + this.ID + "_Value\" id=\"" + this.ID + "_Value\" value=\"\" />";
            bt.InnerHtml += "<input type=\"hidden\" name=\"" + this.PageRequestKey + "\" id=\"" + this.PageRequestKey + "\" value=\"" + CurentPage + "\" />";
            bt.InnerHtml += "<script type=\"text/javascript\">" + @"
function FrontUserSelectControl_Submit()
{
    oPage = document.getElementById('" + this.PageRequestKey + @"');
    if(oPage)
    {
        oPage.value = 1;
    }
    Submit('" + SearchButtonKey + @"');
}

function FrontUserSelectControl_Reset()
{
    oSearchBox = document.getElementById('" + SearchRequest.Key + @"');
    oPage = document.getElementById('" + this.PageRequestKey + @"');
    if(oSearchBox)
    {
        oSearchBox.value = '';
        if(oPage)
            oPage.value = 1;
        Submit('" + SearchButtonKey + @"');
    }
}
" + "</script>";

            search.Controls.Add(bt);

            HtmlGenericControl table = new HtmlGenericControl("table");
            HtmlGenericControl th = new HtmlGenericControl("th");
            HtmlGenericControl tr = new HtmlGenericControl("tr");
            HtmlGenericControl td = new HtmlGenericControl("td");

            table.ID = this.ID + "_Table";
            table.Attributes["class"] = "tab";

            table.Attributes["cellpadding"] = "0";
            table.Attributes["cellspacing"] = "0";
            table.Controls.Add(tr);
            Contents.Controls.Add(table);

            th = new HtmlGenericControl("th");
            table.Controls.Add(th);
            th.InnerHtml = "Nome";
            tr.Controls.Add(th);

            th = new HtmlGenericControl("th");
            table.Controls.Add(th);
            th.InnerHtml = "Username";
            tr.Controls.Add(th);

            if (CmsUsers)
            {

                th = new HtmlGenericControl("th");
                table.Controls.Add(th);
                th.InnerHtml = "Azienda/Ufficio";
                tr.Controls.Add(th);
            }


            th = new HtmlGenericControl("th");
            table.Controls.Add(th);
            th.InnerHtml = "Data di Nascita";
            tr.Controls.Add(th);


            th = new HtmlGenericControl("th");
            table.Controls.Add(th);
            th.InnerHtml = "Aggiungi";
            tr.Controls.Add(th);



            DataRow[] tab;


            tab = PageData.Conn.SqlQuery(UsersQuery + SqlWhere).Select();
            NetUtility.PageMaker pager = new NetUtility.PageMaker(this.CurentPage, NetUtility.PageMaker.VariabileType.Form);
            pager.RPP = this.RPP;            
            DataRow[] users = pager.MakeTablePagination(tab);
            foreach (DataRow user in users)
            {
                tr = new HtmlGenericControl("tr");
                table.Controls.Add(tr);

                td = new HtmlGenericControl("td");
                tr.Controls.Add(td);
                //td.InnerHtml = user["Cognome_Anagrafica"].ToString() + " " + user["Nome_Anagrafica"].ToString();
                tr.Controls.Add(td);

                td = new HtmlGenericControl("td");
                tr.Controls.Add(td);
                td.InnerHtml = user["Name_User"].ToString();
                tr.Controls.Add(td);
                if (!CmsUsers)
                {

                   /* td = new HtmlGenericControl("td");
                    tr.Controls.Add(td);
                    td.InnerHtml = user["DataNascita_Anagrafica"].ToString();
                    if (td.InnerHtml.Length > 10)
                        td.InnerHtml = td.InnerHtml.Remove(10);
                    else
                        if (td.InnerHtml.Length == 0)
                            td.InnerHtml = "&nbsp;";
                    tr.Controls.Add(td);*/

                }
                else
                {
                    /*td = new HtmlGenericControl("td");
                    tr.Controls.Add(td);
                    td.InnerHtml = user["Azienda_Anagrafica"].ToString();
                    tr.Controls.Add(td);

                    td = new HtmlGenericControl("td");
                    tr.Controls.Add(td);
                    td.InnerHtml = user["Data_Anagrafica"].ToString();
                    if (td.InnerHtml.Length > 10)
                        td.InnerHtml = td.InnerHtml.Remove(10);
                    else
                        if (td.InnerHtml.Length == 0)
                            td.InnerHtml = "&nbsp;";
                    tr.Controls.Add(td);*/
                }
                string href = Action;
                href = href.Replace("{0}", user["id_User"].ToString());
                //href = href.Replace("{2}", user["Cognome_Anagrafica"].ToString());
                //href = href.Replace("{1}", user["Nome_Anagrafica"].ToString());
                if (CmsUsers)
                    href = href.Replace("{3}", user["Profile_User"].ToString());

                td = new HtmlGenericControl("td");
                td.Attributes["class"] = "action add";
                tr.Controls.Add(td);
                td.InnerHtml = "<a href=\"" + href + "\"><span>Aggiungi</span></a>";
                tr.Controls.Add(td);
            }
            Contents.Controls.Add(pager.PageNavigation("javascript: setFormValue({0},'" + this.PageRequestKey + "',1);", tab.Length));
        }
    }
}


