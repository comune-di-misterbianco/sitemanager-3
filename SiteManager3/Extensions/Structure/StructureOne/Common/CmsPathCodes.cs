using System;
using System.IO;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using NetCms;
using NetUtility.NetTreeViewControl;
using NetCms.Connections;
using NetCms.GUI;

using System.Xml;
using System.Xml.XPath;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Structure
{

        public class CmsPathCodes
        {
            //private PageData PageData;
            private string AbsoluteWebRoot;
            
            public CmsPathCodes(Networks.INetwork network)
            {
                AbsoluteWebRoot = network.Paths.AbsoluteFrontRoot;
            }

            public CmsPathCodes(string absoluteWebRoot)
            {
                AbsoluteWebRoot = absoluteWebRoot;
            }

            /*public string Server 
            { 
                get 
                {
                    return "http://"+HttpContext.Current.Request.Url.Authority;
                } 
            }*/
            public string WebRoot
            { get { return "<%= WebRoot %>"; } }
            public string OldWebRoot
            { get { return "%%%WebRoot%%%"; } }
             public string PageName
             { get { return "???PageName???"; } }

            public string RemoveServerReference(string str)
            {
                //Dato che l'indirizzo del server non deve essere presente questa funzione ro rimuove

                StringCollection servers = new StringCollection();

                for (int i = 0; i < Configurations.Generics.Servers.Count; i++)
                {
                    servers.Add("http://" + Configurations.Generics.Servers[i]);
                }

                servers.Add("http://" + HttpContext.Current.Request.Url.Authority);
                servers.Add("http://" + HttpContext.Current.Request.Url.DnsSafeHost);
                servers.Add("http://" + HttpContext.Current.Request.Url.Host);
                servers.Add("http://" + HttpContext.Current.Request.UserHostName);
                servers.Add("http://" + HttpContext.Current.Request.UserHostAddress);

                string val = str;
                for (int i = 0; i < servers.Count; i++)
                {
                    val = val.Replace(servers[i].ToLower(), "");
                    val = val.Replace(servers[i].ToUpper(), "");
                }

                /*if (val.Contains("http://"))
                {
                    val = val.Replace("http://", "");
                    int start = val.IndexOf('/') + 1;
                    int len = val.Length - val.IndexOf('/') - 1;
                    val = val.Substring(start,len);
                }*/

                return val;
            }

            /*public string AddServerReference(string str)
            {
                //Dato che l'indirizzo del server non deve essere presernte questa funzione lo aggiunge pi�
                return str;
            }*/

            public string EncodeWebRootReference(string str)
            {
                string value = str;

                if (AbsoluteWebRoot.Length > 0)
                {
                    value = str.Replace(AbsoluteWebRoot.ToLower(), WebRoot);
                }

                return value;
            }

            public string DecodeWebRootReference(string str)
            {
                string value = str;
                                
                value = str.Replace(WebRoot, AbsoluteWebRoot.ToLower());
                value = value.Replace(OldWebRoot,AbsoluteWebRoot.ToLower());

                return value;
            }

            public string AddWebRootAspCode(string str)
            {
                return str.Replace(WebRoot, "<%= WebRoot %>");
            }
            public string AddPageNameReference(string str,string pagename)
            {
                return str.Replace(PageName, pagename);
            }/**/
        }

	
    
}