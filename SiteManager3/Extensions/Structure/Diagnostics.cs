using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Reflection;

/// <summary>
/// Summary description for FilesDocument
/// </summary>
namespace NetCms.Diagnostics
{
    public class Tracer
    {
        public static void TraceMessage(TraceLevel messageLevel, string message)
        {
            TraceLog(messageLevel, message,"", "");
        } 
        public static void TraceMessage(TraceLevel messageLevel, string message,string errorcode)
        {           
            TraceLog(messageLevel,message,"",errorcode);            
        }      

        public static void TraceMessage(TraceLevel messageLevel, string message, string component, string errorcode)
        {
            TraceLog(messageLevel, message, component, errorcode);
        }

        public static void TraceLog(TraceLevel messageLevel, string message, string component, string errorcode)
        {
            string UserID = "";
            string UserName = "";

            NetCms.Users.User account = NetCms.Users.AccountManager.CurrentAccount;
            if (account != null)
            {
                UserID = account.ID.ToString();
                UserName = account.UserName;
            }

            Diagnostics.TraceMessage(messageLevel, message, UserID, UserName, component, errorcode);
        }
    }
}
