﻿using CmsConfigs.Model;
using G2Core.Common;
using GenericSearch;
using NetCms.GUI;
using NetCms.Structure.Search.Forms;
using NetCms.Structure.Search.Models;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using SolrNet;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Structure.Search
{
    public class CmsSearch : SmPageVertical
    {
        public CmsSearch()
        {
            GlobalDiv.Init += new EventHandler(GlobalDiv_Init);
            SearchForm.SubmitButton.Click += SubmitButton_Click;
        }

        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.HomeSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_Search"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Find; }
        }

        public override string PageTitle
        {
            get { return "Cerca nei contenuti del portale"; }
        }

        private CmsSearchForm _SearchForm;
        public CmsSearchForm SearchForm
        {
            get
            {
                if (_SearchForm == null)
                {
                    _SearchForm = new CmsSearchForm("SearchForm");
                    _SearchForm.ShowMandatoryInfo = false;
                    _SearchForm.CssClass = "Axf";
                    _SearchForm.SubmitButton.Text = "Cerca";
                    _SearchForm.EnableInLineButtonsStyle = true;

                }
                return _SearchForm;
            }
        }

        private WebControl _SearchResults;
        public WebControl SearchResults
        {
            get
            {
                if (_SearchResults == null)
                {
                    _SearchResults = new WebControl(HtmlTextWriterTag.Div);
                    _SearchResults.CssClass = "searchResults";
                    _SearchResults.ID = "searchResults";
                }
                return _SearchResults;
            }
            set { _SearchResults = value; }
        }

        private WebControl _GlobalDiv;
        public WebControl GlobalDiv
        {
            get
            {
                if (_GlobalDiv == null)
                {
                    _GlobalDiv = new WebControl(HtmlTextWriterTag.Div);
                    _GlobalDiv.CssClass = "searchPage";
                }
                return _GlobalDiv;
            }
        }

        //??
        private WebControl _ClusterDiv;
        public WebControl ClusterDiv
        {
            get
            {
                if (_ClusterDiv == null)
                {
                    _ClusterDiv = new WebControl(HtmlTextWriterTag.Div);                
                    _ClusterDiv.ID = "clusters";
                }
                return _ClusterDiv;
            }
        }

        void GlobalDiv_Init(object sender, EventArgs e)
        {
            // searchBar
            GlobalDiv.Controls.Add(SearchForm);

            // searchResult
            GlobalDiv.Controls.Add(SearchResults);

            GlobalDiv.Controls.Add(ClusterDiv);
            GlobalDiv.Controls.Add(new WebControl(HtmlTextWriterTag.Div) { CssClass = "clear" });

            if (SearchTexts.IsValidString && CurrentPage.IsValidInteger)
                Search(SearchTexts.StringValue);

        }

        protected override System.Web.UI.WebControls.WebControl RootControl
        {
            get
            {
                return GlobalDiv;
            }
        }

        public RequestVariable SearchTexts
        {
            get
            {
                RequestVariable searchTexts = new RequestVariable("searchBox", RequestVariable.RequestType.Form_QueryString);
                return searchTexts;
            }

        }
        public RequestVariable CurrentPage
        {
            get
            {
                RequestVariable CurrentPage = new RequestVariable("searchPage", RequestVariable.RequestType.QueryString);
                return CurrentPage;
            }
        }

        void Search(string searchTerm)
        {
            IndexConfig currentconfig = CmsConfigs.BusinessLogic.ConfigsBusinessLogic.GetIndexConfig();
            CmsConfigs.DAO.CmsLuceneDao luceneDao = new CmsConfigs.DAO.CmsLuceneDao(currentconfig.LuceneSettingsFolderPath);

            int rpp = 10;
            string paginationKey = "searchPage";
            int currentPage = 1;

            if (CurrentPage.IsValidInteger)
                currentPage = CurrentPage.IntValue;

            Dictionary<string, string> filters = new Dictionary<string, string>();
            filters.Add("document_ispublic", "true");

            Dictionary<string, string> orders = new Dictionary<string, string>();
            orders.Add("", "DESC");

            int resultCount = luceneDao.SearchCount(searchTerm);

            PaginationHandler pagination = new PaginationHandler(rpp,
                                                                     resultCount,
                                                                     paginationKey,
                                                                     SearchForm.IsPostBack);

            pagination.PaginationKey = paginationKey;
            pagination.CssClass = "pagination";
            pagination.BaseLink = "search.aspx?" + pagination.PaginationKey + "={0}&searchBox=" + Uri.EscapeUriString(searchTerm);
            pagination.GoToFirstPageOnSearch = true;

            int startNumRecord = (CurrentPage.IntValue > 1) ? ((CurrentPage.IntValue - 1) * rpp) : 1;

            List<CmsDocumentLucene> results = luceneDao.Search(searchTerm, currentPage, rpp); 

            if (results != null && results.Any())
            {
                #region Risultati ricerca

                string strResults = "";
                string strItem = "";

                string portalAddress = NetCms.Configurations.PortalData.PortalAddress.Remove(NetCms.Configurations.PortalData.PortalAddress.Length - 1);

                foreach (CmsDocumentLucene doc in results)
                {
                    strItem += @"
                                    <div class=""item"">
                                         <h3>
                                            <a href=""" + doc.BackUrl + @""" rel=""esterno"">" + doc.Title + @"</a>
                                         </h3>
                                         <div class=""action""><a class=""modify"" href=""/cms/" + doc.NetworkSystemName + @"/docs/overview.aspx?folder=" + doc.FolderID + @"&doc=" + doc.DocumentID + @"""><span>Modifica</span></a></div>     
                                         <div class=""itemdata"">
                                              <div class=""fronturl""><a href=""" + portalAddress + (doc.FrontUrl.StartsWith("/") ? doc.FrontUrl : "/"+ doc.FrontUrl) + @""">" + portalAddress + doc.FrontUrl + @"</a></div>                                                  
                                              <div class=""description"">
                                                   <p><span class=""score"">" + doc.Score + @"</span> - <span class=""lastmodify"">" + doc.LastModify + @" - </span> " + doc.Description + @"</p>                                                                                                   
                                              </div>                                              
                                         </div>
                                    </div>
                                     ";
                }

                strResults = @"
                                        <div id=""results-container"">
                                            " + strItem + @"
                                        </div>
                                        ";

                GlobalDiv.FindControl("searchResults").Controls.Clear();
                GlobalDiv.FindControl("searchResults").Controls.Add(new LiteralControl(strResults));
                GlobalDiv.FindControl("searchResults").Controls.Add(pagination);

                #endregion               
            }           
        }

        void ResetSearchResults()
        {
            GlobalDiv.FindControl("searchResults").Controls.Clear();
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (SearchTexts.IsValidString)
            {
                ResetSearchResults();
                Search(SearchTexts.StringValue.Replace(" ", "+"));
            }
        }
    }
}
