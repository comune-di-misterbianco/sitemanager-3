﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetCms.Vertical;
using NetCms.Installation;
using NetCms.GUI.Icons;
using System.Linq;
using System.Collections.Generic;
using NetService.Utility.ArTable;
using NetCms.Structure.WebFS;
using CmsConfigs.Forms;
using CmsConfigs.Model;
using NHibernate;
using NetCms.Structure.Search.Models;
using NetCms.Structure.Search.TextAnalysis;
using NetService.Utility.ValidatedFields;
using NetCms.Structure.Search.Forms;
using G2Core.Common;
using CmsConfigs.BusinessLogic.Semantic;

namespace NetCms.Structure.Applications.Configs
{
    public class ContentIndex : SmPageVertical
    {

        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.HomeSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_Configs"; }
        }

        public override Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Plugin; }
        }

        public override string PageTitle
        {
            get { return "Indicizzazione contenuti"; }
        }

        public ContentIndex()
        {        
            this.Toolbar.Buttons.Add(new ToolbarButton("index-config.aspx", Icons.Arrow_Left, "Torna indietro"));
            this.FolderForm.SubmitButton.Click += SubmitButton_Click;
            this.FolderForm.BackButton.Click += BackButton_Click;
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            bool status = false;

            RequestVariable rvFolderID = new RequestVariable("folderID", RequestVariable.RequestType.Form);
            int idFolder = rvFolderID.IntValue;

            try
            {
                TextAnalyze analyzer = new TextAnalyze();
                analyzer.AnalyzeFolder(idFolder);
                status = true;
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante l'indicizzazione della cartella con id " + idFolder + ". " + ex.Message);
            }

            if (status)
                NetCms.GUI.PopupBox.AddSessionMessage("I documenti contenuti nella cartella sono stati analizzati con successo.", PostBackMessagesType.Success);
            else
                NetCms.GUI.PopupBox.AddSessionMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);

        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            bool status = false;

            if (FolderForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                RequestVariable rvFolderID = new RequestVariable("folderID", RequestVariable.RequestType.Form);
                int idFolder = rvFolderID.IntValue;

                try
                {
                    IndexConfig currentconfig = CmsConfigs.BusinessLogic.ConfigsBusinessLogic.GetIndexConfig();
                    if (currentconfig.Mode == IndexConfig.IndexMode.solr)
                        CmsConfigs.BusinessLogic.ContentIndex.IndexContentSolr.IndexFolder(idFolder);
                    else if (currentconfig.Mode == IndexConfig.IndexMode.lucene) 
                    {
                        CmsConfigs.BusinessLogic.ContentIndex.IndexContentLucene.IndexFolder(idFolder,currentconfig.LuceneSettingsFolderPath);
                    }
                    status = true;
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante l'indicizzazione della cartella con id " + idFolder + ". " + ex.Message);
                }
            }

                if (status)
                    NetCms.GUI.PopupBox.AddSessionMessage("Cartella indicizzata con successo.", PostBackMessagesType.Success);
                else
                    NetCms.GUI.PopupBox.AddSessionMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);

            
        }

        protected override WebControl RootControl
        {
            get
            {
                WebControl control = new WebControl(HtmlTextWriterTag.Div);
                control.Controls.Add(new LiteralControl("<p>Sezione per l'indicizzazione dei contenuti del cms.</p>"));

                List<Networks.BasicNetwork> networks = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.Select(x => x.Value).ToList();

                ArTable tab = new ArTable(networks);

                tab.InnerTableCssClass = "tab";
                tab.RecordPerPagina = 25;

                tab.AddColumn(new ArTextColumn("Nome", "Label"));
                tab.AddColumn(new ArTextColumn("Codice", "SystemName"));
                tab.AddColumn(new ArOptionsColumn("Stato", "Enabled", new string[] { "Disablilitato", "Abilitata" }));
                tab.AddColumn(new ArOptionsColumn("CMS", "StatoCMS", new string[] { "Non Esistente", "Abilitato", "Disabilitato" }));

                ArButtonColumn index = new ArButtonColumn("Indicizza", "ID");
                index.ButtonText = "Indicizza contenuti";
                index.ButtonIDOffset = "net";
                index.ClickHandler += new EventHandler(IndexNetwork); ;

                tab.AddColumn(index);

                ArButtonColumn analyze = new ArButtonColumn("Analizza i contenuti", "ID");
                analyze.ButtonText = "Analizza";
                analyze.ButtonIDOffset = "texta";
                analyze.ClickHandler += new EventHandler(AnalyzeContent); ;

                tab.AddColumn(analyze);


                control.Controls.Add(tab);

                //WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
                //fieldset.Controls.Add(IndexFormConfig);
                //control.Controls.Add(fieldset);


                //control.Controls.Add(TestSolr());

                control.Controls.Add(FolderForm);

                return control;
            }
        }

        #region MyRegion
        //private IndexConfig currentConfig;

        //private ContentIndexForm _indexconfigform;
        //public ContentIndexForm IndexFormConfig
        //{
        //    get
        //    {
        //        if (_indexconfigform == null)
        //        {
        //            _indexconfigform = new ContentIndexForm("indexconfigform", currentConfig);
        //            _indexconfigform.ShowMandatoryInfo = false;
        //            _indexconfigform.CssClass = "Axf";
        //            _indexconfigform.SubmitButton.Text = "Salva";
        //            _indexconfigform.SubmitButton.Click += SubmitButton_Click;
        //        }
        //        return _indexconfigform;
        //    }
        //}

        //void SubmitButton_Click(object sender, EventArgs e)
        //{
        //    if (IndexFormConfig.IsValid == NetService.Utility.ValidatedFields.VfGeneric.ValidationStates.Valid)
        //    {
        //        bool status = CmsConfigs.BusinessLogic.ConfigsBusinessLogic.SaveIndexConfig(this.IndexFormConfig);
        //        if (status)
        //            NetCms.GUI.PopupBox.AddSessionMessage("Configurazione salvata con successo.", PostBackMessagesType.Success);
        //        else
        //            NetCms.GUI.PopupBox.AddSessionMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
        //    }
        //} 
        #endregion


        void IndexNetwork(object sender, EventArgs e)
        {
            string idBtn = ((Button)sender).ID;
            string id = idBtn.Split('_')[1];

            int idNetwork = int.Parse(id);

            bool status = false;
            //bool error = false;
            //string msg = "";
            int idFolder = -1;

            try
            {
                Networks.BasicNetwork network = NetCms.Networks.NetworksBusinessLogic.GetById(idNetwork);
                StructureNetwork strNetwork = new StructureNetwork(network);
                idFolder = strNetwork.ID;

                //----> IndexConfig indexConfig = CmsConfigs.BusinessLogic.ConfigsBusinessLogic.GetIndexConfig();
                //GenericSearch.Solr.Configuration solrConfig = new GenericSearch.Solr.Configuration(indexConfig.SolrHostAddress, indexConfig.SolrHostTcpPort);
                //GenericSearch.Solr.Connection.SolrConnection<CmsDocument> solrConn = new GenericSearch.Solr.Connection.SolrConnection<CmsDocument>("test1", solrConfig);


                CmsConfigs.BusinessLogic.ContentIndex.IndexContentSolr.IndexFolder(idFolder);

                status = true;
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante l'indicizzazione della cartella root con id " + idFolder + ". " + ex.Message);
            }

            if (status)
                NetCms.GUI.PopupBox.AddSessionMessage("Network indicizzata con successo.", PostBackMessagesType.Success);
            else
                NetCms.GUI.PopupBox.AddSessionMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);

        }

        #region MyRegion
        //private WebControl TestSolr() 
        //{
        //    WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);

        //    fieldset.Controls.Add(new LiteralControl("<legend>Testing Solr Connection and doc add</legend>"));

        //    Button addContent = new Button();
        //    addContent.ID = "addContent";
        //    addContent.Text = "Test Solr";
        //    addContent.Click += addContent_Click;

        //    fieldset.Controls.Add(addContent);

        //    return fieldset;
        //}

        //void addContent_Click(object sender, EventArgs e)
        //{
        //    CmsDocument test = new CmsDocument();
        //    test.Id = "1";
        //    test.Title = "Document di test";
        //    test.Description = "Descrizione doc";
        //    test.DocumentID = 248;
        //    test.Content = "t's just a POCO with some attributes: SolrField maps the attribute to a Solr field and SolrUniqueKey (optional but recommended) maps an attribute to a Solr unique key field ";
        //    test.Meta = "test";

        //    IndexConfig indexConfig = CmsConfigs.BusinessLogic.ConfigsBusinessLogic.GetIndexConfigByID(1);

        //    //GenericSearch.Solr.Configuration solrConfig = new GenericSearch.Solr.Configuration(indexConfig.SolrHostAddress, indexConfig.SolrHostTcpPort);            

        //    //GenericSearch.Solr.Connection.SolrConnection<CmsDocument> solrConn = new GenericSearch.Solr.Connection.SolrConnection<CmsDocument>("test1",solrConfig);                                 

        //    GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument> dao = new GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument>();



        //    dao.AddContent(test);
        //} 
        #endregion

        void AnalyzeContent(object sender, EventArgs e)
        {
            string idBtn = ((Button)sender).ID;
            string id = idBtn.Split('_')[1];

            int idNetwork = int.Parse(id);
          
            //int idDoc = 3757;

            //using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            //{
               
                StructureFolder rootFolder =  NetCms.Structure.WebFS.FileSystemBusinessLogic.FolderBusinessLogic.FindRootFoldersByNetwork(idNetwork);
                TextAnalyze analyze = new CmsConfigs.BusinessLogic.Semantic.TextAnalyze();
                analyze.AnalyzeFolder(rootFolder.ID);
                //NetCms.Structure.WebFS.Document doc = NetCms.Structure.WebFS.FileSystemBusinessLogic.DocumentBusinessLogic.GetById(idDoc, sess);

                //if (doc.DocLangs != null && doc.DocLangs.FirstOrDefault() != null)
                //{
                //    DocumentLang docLang = doc.DocLangs.FirstOrDefault();
                //    IndexDocContent indexDocContent = docLang.Content.GetContentToIndex(sess);

                //    string textContent = NetCms.Utility.HtmlCleaning.HtmlClean.StripHtmlTags(indexDocContent.Fields[CmsDocument.Fields.document_content.ToString()].ToString());
                //    textContent = System.Net.WebUtility.HtmlDecode(textContent);

                //    if (textContent.Length > 300)
                //    {
                //        TextAnalyzer analyzer = new TextAnalyzer("http://spotlight.sztaki.hu:2230/rest/annotate");
                //        ICollection<DocAnnotate> results = analyzer.GetNamedEntities(textContent, "0.5", 1, 1);                                                                    
                //        CmsConfigs.BusinessLogic.Annotate.AnnotateBusinessLogic.SaveDocContentAnnotate(docLang.Content.ID, results);
                //    }                                       
                //}
           // }

        }

        private FolderIndexForm _folderForm;
        private FolderIndexForm FolderForm
        {
            get
            {
                if (_folderForm == null)
                {
                    _folderForm = new FolderIndexForm("IndexFolderForm");
                    _folderForm.ShowMandatoryInfo = false;
                    _folderForm.CssClass = "Axf";
                    _folderForm.SubmitButton.Text = "Indicizza";
                    _folderForm.AddBackButton = true;
                    _folderForm.BackButton.Text = "Analizza";
                    _folderForm.BackButtonID = "analyzeFolder";
                    _folderForm.EnableInLineButtonsStyle = true;
                }
                return _folderForm;
            }
           
        }
    }
}
