﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.Search.Models
{
    public class CmsClusterDoc
    {
        public string Title
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string FrontUrl
        {
            get;
            set;
        }
        public string BackUrl
        {
            get;
            set;
        }
    }
}
