﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Structure.WebFS;

namespace NetCms.Structure.Search.TextAnalysis.Mapping
{
    public class DocAnnotateMapping : ClassMap<DocAnnotate>
    {
        public DocAnnotateMapping()
        {
            Table("docs_annotates");
            Id(x => x.ID);

            Map(x => x.offset);
            Map(x => x.score);
            Map(x => x.surfaceForm);
            Map(x => x.types);
            Map(x => x.URI);


            References(x => x.ContentAnnotate)               
               .ForeignKey("DocContentAnnotate")
               .Column("RifDocContentAnnotate")
               .Fetch.Select();
        }
    }
}
