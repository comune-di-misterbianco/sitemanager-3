﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.Search.Models
{
    public class CmsCluster
    {
        public CmsCluster() { }

        public string Label { get; set; }

        public List<CmsClusterDoc> Docs { get; set; }

        public double Score { get; set; }
    }
}
