﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.Search.Models
{
    public class SearchResults
    {
        public SearchResults() { }

        public List<CmsDocument> CmsDocuments { get; set; }

        public List<CmsCluster> CmsClusters { get; set; }
    }
}
