﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SolrNet.Attributes;

namespace NetCms.Structure.Search.Models
{
    public class CmsDocument
    {
        [SolrUniqueKey("id")]
        public string Id { get; set; }

        [SolrField("document_id")]
        public int DocumentID { get; set; }

        [SolrField("document_title")]
        public string Title { get; set; }

        [SolrField("name")]
        public string Name { get; set; }

        [SolrField("document_description")]
        public string Description { get; set; }

        [SolrField("document_creation")]
        public DateTime? Creation { get; set; }

        [SolrField("document_lastmodify")]
        public DateTime? LastModify { get; set; }

        [SolrField("document_meta")]
        public string[] Meta { get; set; }

        [SolrField("document_content")]
        public string Content { get; set; }

        [SolrField("document_content_text")]
        public string ContentText { get; set; }

        [SolrField("document_fronturl")]
        public string FrontUrl { get; set; }

        [SolrField("document_backurl")]
        public string BackUrl { get; set; }

        [SolrField("document_type")]
        public string Type { get; set; }

        [SolrField("document_app")]
        public string App { get; set; }

        [SolrField("document_ispublic")]
        public bool IsPublic { get; set; }

        [SolrField("document_stato")]
        public int Stato { get; set; }

        [SolrField("document_folderid")]
        public int FolderID { get; set; }

        [SolrField("document_statofolder")]
        public int StatoFolder { get; set; }

        [SolrField("document_networksystemname")]
        public string NetworkSystemName { get; set; }

        [SolrField("score")]
        public string Score { get; set; }

        public enum Fields
        {
            id,
            document_id,
            document_title,
            document_description,
            document_meta,
            document_content,
            document_content_text,
            document_fronturl,
            document_backurl,
            document_type,
            document_app,
            document_ispublic,
            document_folderid,
            document_statofolder,
            document_stato,
            document_networksystemname,
            document_creation,
            document_lastmodify,
            score
        }

    }
}
