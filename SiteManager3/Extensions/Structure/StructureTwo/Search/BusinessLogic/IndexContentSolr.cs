﻿using CmsConfigs.Model;
using log4net;
using NetCms.DBSessionProvider;
using NetCms.Diagnostics;
using NetCms.Diagnostics.Log4Net;
using NetCms.Networks.WebFS;
using NetCms.Structure.Applications;
using NetCms.Structure.Search.Models;
using NetCms.Structure.WebFS;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CmsConfigs.BusinessLogic.ContentIndex
{
    public static class IndexContentSolr
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static void IndexFolder(int idFolder)
        {
            using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                using (GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument> solrDao = new GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument>())
                    IndexFolder(idFolder, solrDao, sess);
            }
        }

        public static void DeleteFolder(int folderID)
        {
            using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                using (GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument> solrDao = new GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument>())
                    DeleteFolder(folderID, solrDao, sess);
            }
        }

        public static void DeleteFolder(int folderID, GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument> solrDao, ISession sess)
        {
            StructureFolder folder = NetCms.Structure.WebFS.FileSystemBusinessLogic.FolderBusinessLogic.GetById(sess, folderID);
            if (folder != null && folder.Documents != null && folder.Documents.Any() && folder.Tipo != 1 && !folder.Hidden && !folder.Restricted)
            {
                int statoFolder = folder.Stato;

                foreach (NetworkDocument document in folder.Documents)
                {
                    NetCms.Structure.WebFS.Document doc = document as NetCms.Structure.WebFS.Document;
                    // non vanno controllate le homepage e i link
                    if (doc != null && doc.Application != 1 && doc.Application != 9 && doc.Trash == 0)
                        DeleteDocument(doc.ID, solrDao);
                }
            }

            if (folder.Folders == null && !folder.HasSubFolders || (folder.IsSystemFolder || folder.Hidden || folder.Restricted || folder.Trash > 0))
                return;

            Parallel.ForEach(folder.Folders, childFolder =>
            {
                TraceLog(TraceLevel.Error, "Chiamata a funzione ricorsiva per la eliminazione della folder " + childFolder.ID + ".", "", "");
                DeleteFolder(childFolder.ID);
            });
        }

        private static void IndexFolder(int idFolder, GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument> solrDao, ISession sess)
        {
            StructureFolder folder = NetCms.Structure.WebFS.FileSystemBusinessLogic.FolderBusinessLogic.GetById(sess, idFolder);

            if (folder != null && folder.Documents != null && folder.Documents.Any() && folder.Tipo != 1 && !folder.Hidden && !folder.Restricted)
            {

                int statoFolder = folder.Stato;

                foreach (NetworkDocument document in folder.Documents)
                {
                    NetCms.Structure.WebFS.Document doc = document as NetCms.Structure.WebFS.Document;
                    
                    // non vanno indicizzati le homepage e i link 
                    
                    // per velocizzare l'indicizzazione si possono evitare anche gli allegati in quanto non viene eseguita nessuna indicizzazione del contenuto dell'allegato

                    if (doc != null && doc.Application != 1 && doc.Application != 4 && doc.Application != 9 && doc.Trash == 0)
                        IndexDocument(doc.ID, solrDao, statoFolder, sess);
                }
            }

            if (folder.Folders == null && !folder.HasSubFolders || (folder.IsSystemFolder || folder.Hidden || folder.Restricted || folder.Trash > 0))
                return;

            Parallel.ForEach(folder.Folders, childFolder =>
            {
                //TraceLog(TraceLevel.Error, "Chiamata a funzione ricorsiva per la scansione della folder " + childFolder.ID + ".", "", "");
                IndexFolder(childFolder.ID);
            });

            //foreach (NetworkFolder childFolder in folder.Folders)
            //{
            //    //TraceLog(TraceLevel.Error, "Chiamata a funzione ricorsiva per la scansione della folder " + childFolder.ID + ".", "", "");
            //    IndexFolder(childFolder.ID); //, solrConn);
            //}           
        }

        /// <summary>
        /// Permette l'indicizzazione del documento nell'indice di solr
        /// </summary>
        /// <param name="idDoc">Id del documento</param>
        /// <param name="solrDao">Accesso Dao Solr</param>
        /// <param name="statoFolder">Indicare lo stato della cartella ()</param>
        /// <param name="sess">Sessione NHibernate corrente</param>
        private static void IndexDocument(int idDoc, GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument> solrDao, int statoFolder, ISession sess)
        {
            try
            {
                NetCms.Structure.WebFS.Document doc = NetCms.Structure.WebFS.FileSystemBusinessLogic.DocumentBusinessLogic.GetById(idDoc, sess);

                IndexDocContent indexDocContent;
                CmsDocument cmsDoc;

                // non utilizzabile in quanto cerca valori nel context non disponibile in fase di ciclo parallelo va costriuto in modo alternativo
                //string frontUrlDoc = doc.FrontendLink; 
                // string urlDoc = doc.Link;

                if (doc != null)
                {                    
                    GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument> dao = new GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument>();

                    if (doc.DocLangs != null && doc.DocLangs.FirstOrDefault() != null)
                    {
                        DocumentLang docLang = doc.DocLangs.FirstOrDefault();

                        if (docLang != null && docLang.Content != null && docLang.Content.EnableIndexContent)
                        {
                            indexDocContent = docLang.Content.GetContentToIndex(sess);

                            cmsDoc = new CmsDocument();
                            cmsDoc.Id = doc.ID.ToString();
                            cmsDoc.DocumentID = doc.ID;
                            cmsDoc.Title = indexDocContent.Fields[CmsDocument.Fields.document_title.ToString()].ToString(); //titoloDoc = doc.DocLangs.FirstOrDefault().Label;
                            cmsDoc.Name = indexDocContent.Fields[CmsDocument.Fields.document_title.ToString()].ToString();
                            cmsDoc.Description = indexDocContent.Fields[CmsDocument.Fields.document_description.ToString()].ToString(); // (doc.DocLangs.FirstOrDefault() != null) ? doc.DocLangs.FirstOrDefault().Descrizione : "";                            
                            cmsDoc.Content = (indexDocContent != null && indexDocContent.Fields[CmsDocument.Fields.document_content.ToString()] != null) ? indexDocContent.Fields[CmsDocument.Fields.document_content.ToString()].ToString().Replace("<%= WebRoot %>", doc.Folder.Network.Paths.AbsoluteFrontRoot) : "NN";
                            cmsDoc.ContentText = NetCms.Utility.HtmlCleaning.HtmlClean.StripHtmlTags(indexDocContent.Fields[CmsDocument.Fields.document_content.ToString()].ToString());
                            cmsDoc.FrontUrl = indexDocContent.Fields[CmsDocument.Fields.document_fronturl.ToString()].ToString();
                            cmsDoc.BackUrl = indexDocContent.Fields[CmsDocument.Fields.document_backurl.ToString()].ToString();
                            cmsDoc.Type = indexDocContent.Fields[CmsDocument.Fields.document_type.ToString()].ToString();
                            //  cmsDoc.App = docLang.Document.RelativeApplication.SystemName;
                            cmsDoc.IsPublic = (doc.Application != 6 ) ? (docLang.Revisioni.FirstOrDefault(x => x.Pubblicata == true)).Pubblicata : true;
                            cmsDoc.Stato = doc.Stato;
                            cmsDoc.StatoFolder = statoFolder;
                            cmsDoc.FolderID = doc.Folder.ID;
                            cmsDoc.NetworkSystemName = doc.Folder.Network.SystemName;
                            cmsDoc.Creation = doc.Create;
                            cmsDoc.LastModify = doc.Modify;

                            //da aggiungere al cmsDocument
                            //cmsDoc.Meta = doc.Tags; //va modificato il codice che recupera il dato dal db                                

                            // add document to index                             
                            solrDao.AddContent(cmsDoc);
                        }


                    }
                    //TraceLog(TraceLevel.Error, "Il documento " + idDoc + " è stato indicizzato. Altri dati titolo: " + titoloDoc + "  ", "", "");
                }
            }
            catch (Exception ex)
            {
                TraceLog(TraceLevel.Error, "errore il doc con " + idDoc + " non è stato indicizzato.", "", "", ex);
                throw;
            }

        }

        /// <summary>
        /// Permette l'aggiornamento del record relativo al documento dall'indice solr
        /// </summary>
        public static void AddUpdateDocument(int idDoc, ISession sess = null)
        {
            if (sess == null)
                sess = GetCurrentSession();

            try
            {
                NetCms.Structure.WebFS.Document doc = NetCms.Structure.WebFS.FileSystemBusinessLogic.DocumentBusinessLogic.GetById(idDoc, sess);
                if (doc != null)
                {
                    GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument> solrDao = new GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument>();

                    CmsDocument cmsDoc = solrDao.GetById(idDoc.ToString());
                    if (cmsDoc != null && doc.DocLangs != null && doc.DocLangs.FirstOrDefault() != null)
                    {
                        DocumentLang docLang = doc.DocLangs.FirstOrDefault();

                        if (docLang != null && docLang.Content != null && docLang.Content.EnableIndexContent)
                        {
                            IndexDocContent indexDocContent = docLang.Content.GetContentToIndex(sess);

                            cmsDoc.Title = indexDocContent.Fields[CmsDocument.Fields.document_title.ToString()].ToString(); //titoloDoc = doc.DocLangs.FirstOrDefault().Label;
                            cmsDoc.Name = indexDocContent.Fields[CmsDocument.Fields.document_title.ToString()].ToString();
                            cmsDoc.Description = indexDocContent.Fields[CmsDocument.Fields.document_description.ToString()].ToString(); // (doc.DocLangs.FirstOrDefault() != null) ? doc.DocLangs.FirstOrDefault().Descrizione : "";                            
                            cmsDoc.Content = (indexDocContent != null && indexDocContent.Fields[CmsDocument.Fields.document_content.ToString()] != null) ? indexDocContent.Fields[CmsDocument.Fields.document_content.ToString()].ToString() : "NN"; // indexDocContent.Fields[CmsDocument.Fields.document_content.ToString()].ToString().Replace("<%= WebRoot %>", doc.Folder.Network.Paths.AbsoluteFrontRoot)
                            cmsDoc.ContentText = NetCms.Utility.HtmlCleaning.HtmlClean.StripHtmlTags(indexDocContent.Fields[CmsDocument.Fields.document_content.ToString()].ToString());
                            cmsDoc.FrontUrl = indexDocContent.Fields[CmsDocument.Fields.document_fronturl.ToString()].ToString();
                            cmsDoc.BackUrl = indexDocContent.Fields[CmsDocument.Fields.document_backurl.ToString()].ToString();
                            cmsDoc.Type = indexDocContent.Fields[CmsDocument.Fields.document_type.ToString()].ToString();
                            //  cmsDoc.App = docLang.Document.RelativeApplication.SystemName;
                            cmsDoc.IsPublic = (docLang.Revisioni.FirstOrDefault(x => x.Pubblicata == true)).Pubblicata;
                            cmsDoc.Stato = doc.Stato;
                            //cmsDoc.StatoFolder = statoFolder;
                            cmsDoc.FolderID = doc.Folder.ID;
                            cmsDoc.NetworkSystemName = doc.Network.SystemName;
                            cmsDoc.Creation = doc.Create;
                            cmsDoc.LastModify = doc.Modify;

                            solrDao.AddContent(cmsDoc);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TraceLog(TraceLevel.Error, "errore il doc " + idDoc + " non è stato indicizzato.", "", "", ex);
                //throw;
            }
        }

        /// <summary>
        /// Permette l'eliminazione del record relativo al documento dall'indice solr
        /// </summary>
        public static void DeleteDocument(int idDoc, GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument> solrDao = null)
        {
            if (solrDao == null)
                solrDao = new GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument>();

            //recupero il CmsContent dall'indice di solr
            CmsDocument currentDoc = solrDao.GetById(idDoc.ToString());
            solrDao.DeleteContent(currentDoc);
        }

        private static void TraceLog(TraceLevel messageLevel, string message, string component, string errorcode, Exception ex = null, object callerObject = null)
        {
            ILog logger = LogManager.GetLogger("rollingFileIndexer");
            var loggingEvent = new ExtendedLoggingEvent(logger, message, messageLevel, component, "", "", errorcode, ex);
            logger.Logger.Log(loggingEvent);
        }
    }
}
