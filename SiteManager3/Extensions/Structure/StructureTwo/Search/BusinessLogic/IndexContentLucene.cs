﻿using CmsConfigs.Model;
using GenericSearch;
using log4net;
using NetCms.DBSessionProvider;
using NetCms.Diagnostics;
using NetCms.Diagnostics.Log4Net;
using NetCms.Networks.WebFS;
using NetCms.Structure.Search.Models;
using NetCms.Structure.WebFS;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CmsConfigs.BusinessLogic.ContentIndex
{
    public static class IndexContentLucene
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        private static void TraceLog(TraceLevel messageLevel, string message, string component, string errorcode, Exception ex = null, object callerObject = null)
        {
            ILog logger = LogManager.GetLogger("rollingFileIndexer");
            var loggingEvent = new ExtendedLoggingEvent(logger, message, messageLevel, component, "", "", errorcode, ex);
            logger.Logger.Log(loggingEvent);
        }

        public static void IndexFolder(int idFolder, string luneceFolderPath)
        {
            using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {               
               IndexFolder(idFolder, luneceFolderPath, sess);
            }
        }

        private static void IndexDocument(int idDoc, string luneceFolderPath, int statoFolder, ISession sess)
        {
            try
            {
                NetCms.Structure.WebFS.Document doc = NetCms.Structure.WebFS.FileSystemBusinessLogic.DocumentBusinessLogic.GetById(idDoc, sess);

                IndexDocContent indexDocContent;
                CmsDocument cmsDoc;

                // non utilizzabile in quanto cerca valori nel context non disponibile in fase di ciclo parallelo va costriuto in modo alternativo
                //string frontUrlDoc = doc.FrontendLink; 
                // string urlDoc = doc.Link;

                if (doc != null)
                {
                    CmsConfigs.DAO.CmsLuceneDao daoLucene = new CmsConfigs.DAO.CmsLuceneDao(luneceFolderPath);

                    if (doc.DocLangs != null && doc.DocLangs.FirstOrDefault() != null)
                    {
                        DocumentLang docLang = doc.DocLangs.FirstOrDefault();

                        if (docLang != null && docLang.Content != null && docLang.Content.EnableIndexContent)
                        {
                            indexDocContent = docLang.Content.GetContentToIndex(sess);

                            List<SearchField> listSf = new List<SearchField>();

                            cmsDoc = new CmsDocument();

                            SearchField sfID = new SearchField("ID", doc.ID.ToString(), SearchField.Store.Yes, SearchField.IndexTypes.NOT_ANALYZED);
                            listSf.Add(sfID);

                            SearchField sfTitle = new SearchField("Title", indexDocContent.Fields[CmsDocument.Fields.document_title.ToString()].ToString(), SearchField.Store.Yes, SearchField.IndexTypes.ANALYZED);
                            listSf.Add(sfTitle);

                            SearchField sfDescription = new SearchField("Description", indexDocContent.Fields[CmsDocument.Fields.document_description.ToString()].ToString(), SearchField.Store.Yes, SearchField.IndexTypes.ANALYZED);
                            listSf.Add(sfDescription);

                            SearchField sfTesto = new SearchField("Testo", NetCms.Utility.HtmlCleaning.HtmlClean.StripHtmlTags(indexDocContent.Fields[CmsDocument.Fields.document_content.ToString()].ToString()), SearchField.Store.Yes, SearchField.IndexTypes.ANALYZED);
                            listSf.Add(sfTesto);

                            SearchField sfData = new SearchField("Data", doc.LastModify.ToString(), SearchField.Store.Yes, SearchField.IndexTypes.NOT_ANALYZED);
                            listSf.Add(sfData);

                            SearchField sfFrontUrl = new SearchField("FrontURL", indexDocContent.Fields[CmsDocument.Fields.document_fronturl.ToString()].ToString(), SearchField.Store.Yes, SearchField.IndexTypes.NOT_ANALYZED);
                            listSf.Add(sfFrontUrl);

                            SearchField sfBackUrl = new SearchField("BackURL", indexDocContent.Fields[CmsDocument.Fields.document_backurl.ToString()].ToString(), SearchField.Store.Yes, SearchField.IndexTypes.NOT_ANALYZED);
                            listSf.Add(sfBackUrl);

                            SearchField sfContentType = new SearchField("ContentType", indexDocContent.Fields[CmsDocument.Fields.document_type.ToString()].ToString(), SearchField.Store.Yes, SearchField.IndexTypes.NOT_ANALYZED);
                            listSf.Add(sfContentType);

                            SearchField sfFolderID = new SearchField("FolderID", doc.Folder.ID.ToString(), SearchField.Store.Yes, SearchField.IndexTypes.NOT_ANALYZED);
                            listSf.Add(sfFolderID);

                            SearchField sfDocumentID = new SearchField("DocumentID", doc.ID.ToString(), SearchField.Store.Yes, SearchField.IndexTypes.NOT_ANALYZED);
                            listSf.Add(sfDocumentID);                                                        

                            SearchField sfNetworkSystemName = new SearchField("NetworkSystemName", doc.Folder.Network.SystemName, SearchField.Store.Yes, SearchField.IndexTypes.NOT_ANALYZED);
                            listSf.Add(sfNetworkSystemName);                                                          

                            // add document to index                             

                            daoLucene.LuceneArchive.SaveOrUpdateEntityToIndex(listSf);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TraceLog(TraceLevel.Error, "errore il doc con " + idDoc + " non è stato indicizzato.", "", "", ex);
                throw;
            }
        }
        private static void IndexFolder(int idFolder, string luneceFolderPath, ISession sess)
            {
                StructureFolder folder = NetCms.Structure.WebFS.FileSystemBusinessLogic.FolderBusinessLogic.GetById(sess, idFolder);

                if (folder != null && folder.Documents != null && folder.Documents.Any() && folder.Tipo != 1 && !folder.Hidden && !folder.Restricted)
                {

                    int statoFolder = folder.Stato;

                    foreach (NetworkDocument document in folder.Documents)
                    {
                        NetCms.Structure.WebFS.Document doc = document as NetCms.Structure.WebFS.Document;

                        // non vanno indicizzati le homepage e i link 

                        // per velocizzare l'indicizzazione si possono evitare anche gli allegati in quanto non viene eseguita nessuna indicizzazione del contenuto dell'allegato

                        if (doc != null && doc.Application != 1 && doc.Application != 4 && doc.Application != 9 && doc.Trash == 0)
                            IndexDocument(doc.ID, luneceFolderPath, statoFolder, sess);
                    }
                }

                if (folder.Folders == null && !folder.HasSubFolders || (folder.IsSystemFolder || folder.Hidden || folder.Restricted || folder.Trash > 0))
                    return;

                ////Parallel.ForEach(folder.Folders, childFolder =>
                ////{
                ////    //TraceLog(TraceLevel.Error, "Chiamata a funzione ricorsiva per la scansione della folder " + childFolder.ID + ".", "", "");
                ////    IndexFolder(childFolder.ID, luneceFolderPath);
                ////});
            
                foreach(StructureFolder childFolder in folder.Folders)
                {
                    IndexFolder(childFolder.ID, luneceFolderPath);
                }
        }        
    }
}
