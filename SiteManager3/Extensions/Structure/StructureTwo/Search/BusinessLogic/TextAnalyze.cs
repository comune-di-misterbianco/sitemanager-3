﻿using CmsConfigs.Model;
using NetCms.Networks.WebFS;
using NetCms.Structure.Search.Models;
using NetCms.Structure.Search.TextAnalysis;
using NetCms.Structure.WebFS;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CmsConfigs.BusinessLogic.Semantic
{
    public class TextAnalyze
    {
        public TextAnalyze()
        {

        }

        private NameValueCollection SemanticSection
        {
            get {
                return (NameValueCollection)ConfigurationManager.GetSection("semantic");
            }
        }

        private string TextAnalyzerServiceUrl
        {
            get
            {
                return (SemanticSection["TextAnalyzerServiceUrl"] != null && !string.IsNullOrEmpty(SemanticSection["TextAnalyzerServiceUrl"].ToString())) ? SemanticSection["TextAnalyzerServiceUrl"].ToString() : "http://spotlight.sztaki.hu:2230/rest/annotate";
            }
        }

        private int TextMinLength
        {
            get
            {
                return (SemanticSection["TextMinLength"] != null && !string.IsNullOrEmpty(SemanticSection["TextMinLength"].ToString())) ? int.Parse(SemanticSection["TextMinLength"].ToString()) : 300;             
            }
        }
        private string Confidence
        {
            get
            {
                return (SemanticSection["Confidence"] != null && !string.IsNullOrEmpty(SemanticSection["Confidence"].ToString())) ? SemanticSection["Confidence"].ToString() : "0.5";                
            }
        }
        private double maxScore
        {
            get
            {
                return double.Parse(SemanticSection["maxScore"].ToString(), System.Globalization.CultureInfo.InvariantCulture);  
            }
        }

        private double minScore
        {
            get
            {
                return double.Parse(SemanticSection["minScore"].ToString(), System.Globalization.CultureInfo.InvariantCulture);          
            }
        }        

        public void AnalyzeFolder(int idFolder)
        {
            using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                    AnalyzeFolder(idFolder, sess);
            }
        }

        public void AnalyzeFolder(int idFolder, ISession sess)
        {
            StructureFolder folder = NetCms.Structure.WebFS.FileSystemBusinessLogic.FolderBusinessLogic.GetById(sess, idFolder);

            if (folder != null && folder.Documents != null && folder.Documents.Any() && folder.Tipo != 1 && !folder.Hidden && !folder.Restricted)
            {

                int statoFolder = folder.Stato;

                foreach (NetworkDocument document in folder.Documents)
                {
                    NetCms.Structure.WebFS.Document doc = document as NetCms.Structure.WebFS.Document;

                    // non vanno analizzati le homepage, link e allegati                    
                    if (doc != null && doc.Application != 1 && doc.Application != 4 && doc.Application != 9 && doc.Trash == 0)
                        AnalyzeDoc(doc.ID, sess);
                }
            }

            if (folder.Folders == null && !folder.HasSubFolders || (folder.IsSystemFolder || folder.Hidden || folder.Restricted || folder.Trash > 0))
                return;

            //Parallel.ForEach(folder.Folders, childFolder =>
            //{
            //    AnalyzeFolder(childFolder.ID);
            //});

            foreach(StructureFolder childFolder in folder.Folders )
            {
                AnalyzeFolder(childFolder.ID);
            }

        }

        public void AnalyzeDoc(int idDoc, ISession sess)
        {
            
            NetCms.Structure.WebFS.Document doc = NetCms.Structure.WebFS.FileSystemBusinessLogic.DocumentBusinessLogic.GetById(idDoc, sess);
            if (doc.DocLangs != null && doc.DocLangs.FirstOrDefault() != null)
            {
                DocumentLang docLang = doc.DocLangs.FirstOrDefault();

                if (docLang.Content != null)
                {
                    IndexDocContent indexDocContent = docLang.Content.GetContentToIndex(sess);

                    string textContent = NetCms.Utility.HtmlCleaning.HtmlClean.StripHtmlTags(indexDocContent.Fields[CmsDocument.Fields.document_content.ToString()].ToString());
                    textContent = System.Net.WebUtility.HtmlDecode(textContent);

                    if (textContent.Length > TextMinLength)
                    {
                        TextAnalyzer analyzer = new TextAnalyzer(TextAnalyzerServiceUrl);

                        ICollection<DocAnnotate> results = analyzer.GetNamedEntities(textContent, Confidence, maxScore, minScore);
                        if (results != null && results.Any())
                        {
                            CmsConfigs.BusinessLogic.Annotate.AnnotateBusinessLogic.SaveDocContentAnnotate(docLang.Content.ID, results);

                            // recupero i tag dalle annotazione del current document
                            string[] arrayTags = CmsConfigs.BusinessLogic.Annotate.AnnotateBusinessLogic.GetDocContentTags(docLang.Content.ID, sess);

                            if (arrayTags != null && arrayTags.Length > 0)
                            {
                                // asseggo i tag al document
                                NetCms.Structure.WebFS.Documents.Tags.BusinessLogic.TagsManager tagmanager = new NetCms.Structure.WebFS.Documents.Tags.BusinessLogic.TagsManager(sess);
                                tagmanager.AddTagForDoc(arrayTags, doc.ID);
                            }
                        }
                    }
                }
            }
            
        }
    }
}
