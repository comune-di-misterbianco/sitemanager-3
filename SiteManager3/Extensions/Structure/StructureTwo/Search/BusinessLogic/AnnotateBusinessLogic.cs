﻿using GenericDAO.DAO;
using NetCms.DBSessionProvider;
using NetCms.Structure.Search.Models;
using NetCms.Structure.Search.TextAnalysis;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetService.Utility.RecordsFinder;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CmsConfigs.BusinessLogic.Annotate
{
    public static class AnnotateBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        /// <summary>
        /// Restituisce le annotazione relative al DocContent 
        /// </summary>
        /// <param name="idDocContent">ID del DocContent</param>
        /// <param name="innerSession">Sessione Nhibernate</param>
        /// <returns></returns>
        public static DocContentAnnotate GetDocContentAnnotatesByIdDocContent(int idDocContent, ISession innerSession =null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            CriteriaNhibernateDAO<DocContentAnnotate> dao = new CriteriaNhibernateDAO<DocContentAnnotate>(innerSession);

            SearchParameter spDocContent = new SearchParameter("", "RifContent", idDocContent, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { spDocContent });
        }

        //public static void DeleteDocContentAnnotates(int idContentAnnotate, ISession innerSession = null)
        //{
        //    if (innerSession == null)
        //        innerSession = GetCurrentSession();

        //    try
        //    {
        //        DocContentAnnotate docContentAnnotate = GetDocContentAnnotatesByIdDocContent(idContentAnnotate, innerSession);
        //        if (docContentAnnotate != null)
        //        {
        //            CriteriaNhibernateDAO<DocContentAnnotate> dao = new CriteriaNhibernateDAO<DocContentAnnotate>(innerSession);
        //            docContentAnnotate.Annotates.Clear();                    
        //            dao.SaveOrUpdate(docContentAnnotate);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message);
        //    }
        //}   

        public static void SaveDocContentAnnotate(int idDocContent, ICollection<DocAnnotate> annotates)
        {
            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    CriteriaNhibernateDAO<DocContentAnnotate> dao = new CriteriaNhibernateDAO<DocContentAnnotate>(session);

                    DocContentAnnotate docContentAnnotate = GetDocContentAnnotatesByIdDocContent(idDocContent, session);
                    if (docContentAnnotate == null)
                    {
                        DocContentAnnotate contentAnnotate = new DocContentAnnotate();
                        contentAnnotate.RifContent = idDocContent;

                        foreach(DocAnnotate annotate in annotates)
                        {
                            contentAnnotate.Annotates.Add(annotate);
                        }
                        
                        dao.SaveOrUpdate(contentAnnotate);
                    }
                    else
                    {
                        docContentAnnotate.Annotates.Clear();

                        foreach (DocAnnotate annotate in annotates)
                        {
                            docContentAnnotate.Annotates.Add(annotate);
                        }

                        dao.SaveOrUpdate(docContentAnnotate);
                    }

                    tx.Commit();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message);
                    tx.Rollback();
                }
            }
        }

        public static string[] GetDocContentTags(int idDocContent,ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            CriteriaNhibernateDAO<DocContentAnnotate> dao = new CriteriaNhibernateDAO<DocContentAnnotate>(innerSession);

            SearchParameter spDocContent = new SearchParameter("", "RifContent", idDocContent, Finder.ComparisonCriteria.Equals, false);

            DocContentAnnotate docContentAnnotate = dao.GetByCriteria(new SearchParameter[] { spDocContent });

            if (docContentAnnotate != null && docContentAnnotate.Annotates != null)
            {
                string[] results = docContentAnnotate.Annotates.Select(x => x.surfaceForm).ToArray<string>();
                return results;
            }
            else
                return null;

        }
        /// <summary>
        /// Ritorna una lista di CmsDocument 
        /// </summary>
        /// <param name="tags">Array di string contenenti i tag da cercare</param>
        /// <param name="idDocContent">ID cotenuto della pagina visitata</param>
        /// <param name="maxResult">Numero massimo di risultati da ritornare</param>
        /// <param name="innerSession">Sessione nhibernete</param>
        /// <returns></returns>
        public static List<SemResult> GetDocsByTags(string[] tags, int idDocContent, int maxResult, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();
        
            List<SemResult> semresults = new List<SemResult>();
         
            Disjunction or = new Disjunction();            
            foreach (string tag in tags)
            {
                or.Add(Restrictions.Eq(DocAnnotate.Colums.surfaceForm.ToString(), tag));
            }
            
            ICriteria criteria2 = innerSession
                                 .CreateCriteria<DocContentAnnotate>("docContent")
                                 .CreateCriteria("docContent.Annotates", NHibernate.SqlCommand.JoinType.InnerJoin);

            criteria2.SetMaxResults(maxResult);
            criteria2.Add(or);
            criteria2.Add(Restrictions.Not(Restrictions.Eq("docContent.RifContent", idDocContent)));
            criteria2.SetProjection(Projections.ProjectionList().Add(Projections.Distinct(Projections.Property("docContent.RifContent"))));

            IList<int> results = criteria2.List<int>();

            if (results != null)
            {                
                IEnumerable<int> docContentIDs = results;
                SemResult r;

                CmsDocument doc;
                DocContent document;

                GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument> solrDao = new GenericSearch.Solr.DAO.CriteriaSolrDAO<CmsDocument>();

                if (solrDao.GetStatus())
                {
                    foreach (int docContentID in docContentIDs)
                    {
                        document = DocumentBusinessLogic.GetDocContentById(docContentID, innerSession);
                        int idDoc = document.DocLang.Document.ID;

                        doc = solrDao.GetById(idDoc.ToString());

                        if (doc != null)
                        {
                            r.Titolo = doc.Title;
                            r.Url = doc.FrontUrl;
                            r.IdDoc = idDoc;
                            semresults.Add(r);
                        }
                    }
                }
                else
                {
                    // caso in cui non sia disponibile solr 
                    // recupereare i dati da restituire dalla BL del cms

                    foreach (int docContentID in docContentIDs)
                    {
                        document = DocumentBusinessLogic.GetDocContentById(docContentID, innerSession);

                        string docLabel = string.Empty;

                        if (document.DocLang.Label.ToLower() == "Pagina Indice".ToLower())
                        {
                            StructureFolder folder = FolderBusinessLogic.GetById(innerSession, document.DocLang.Document.Folder.ID);
                            if (folder != null)
                                docLabel = folder.Label;
                        }
                        else
                        {
                            docLabel = document.DocLang.Label;
                        }

                        r.Titolo = docLabel;
                        r.Url = document.DocLang.Document.FullName;
                        r.IdDoc = document.DocLang.Document.ID; 
                        semresults.Add(r);
                       
                    }
                }
            }
            return semresults;
        }

        public struct SemResult
        {
            public string Titolo;
            public string Url;
            public int IdDoc;
        }
     }
}
