﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;

namespace NetCms.Structure.Search.TextAnalysis
{
    public class TextAnalyzer 
    {     
        public TextAnalyzer(string srvAnalyzerUri)
        {
            SrvAnalyzerUri = srvAnalyzerUri;
        }

        private string SrvAnalyzerUri;

        public IEnumerable<string> GetHashtags(string Text)
        {
            string pattern = @"#\w+";

            if (Regex.IsMatch(Text, pattern))
            {
                MatchCollection matchlist = Regex.Matches(Text, pattern);
                return matchlist.Cast<Match>().Select(x => x.Value).ToList();
            }
            else
                return Enumerable.Empty<string>();
        }

        public List<DocAnnotate> GetNamedEntities(string text, string confidence, double maxScore = -1, double minScore = -1)
        {
            List<DocAnnotate> neIe = new List<DocAnnotate>();

            HttpResponseMessage responseResult = new HttpResponseMessage();

            using (HttpClient client = new HttpClient())
            {

                var parameters = new Dictionary<string, string>();
                parameters.Add("text", text);
                parameters.Add("confidence", confidence);

                client.DefaultRequestHeaders.Add("Accept", "application/json");              

                responseResult = client.PostAsync(
                                        SrvAnalyzerUri,
                                        new FormUrlEncodedContent(parameters)
                                )
                                .Result;

                if (responseResult.IsSuccessStatusCode)
                {
                    string result = responseResult.Content.ReadAsStringAsync().Result;

                    Annotation annotations = JsonConvert.DeserializeObject<Annotation>(result);
                    if (annotations != null && annotations.Resources != null && annotations.Resources.Any())
                    { 
                        foreach (var item in annotations.Resources)
                        {
                            if (!neIe.Any(x => x.surfaceForm == item.SurfaceForm.ToString()))// && !string.IsNullOrEmpty(item.Types.ToString()))
                            {
                                DocAnnotate ne = new DocAnnotate();
                                ne.surfaceForm = item.SurfaceForm.ToString();
                                ne.types = item.Types.ToString();
                                ne.URI = item.Uri.ToString();
                                ne.score = double.Parse(item.SimilarityScore.ToString().Replace('.', ','));
                                ne.offset = int.Parse(item.Offset.ToString());
                                neIe.Add(ne);
                            }
                        }
                        if (maxScore != -1 && minScore != -1)
                            return neIe.Where(x => (x.score >= minScore && x.score <= maxScore)).ToList();
                        }
                    }   
                else {
                    //log error
                }
            }

            return neIe;
        }
    }
}
