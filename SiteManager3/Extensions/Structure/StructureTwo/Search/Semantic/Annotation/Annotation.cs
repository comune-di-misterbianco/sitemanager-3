﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetCms.Structure.Search.TextAnalysis.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace NetCms.Structure.Search.TextAnalysis
{
    public class Annotation
    {
        [JsonProperty("@text")]
        public string Text { get; set; }

        [JsonProperty("@confidence")]
        public string Confidence { get; set; }

        [JsonProperty("@support")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Support { get; set; }

        [JsonProperty("@types")]
        public string Types { get; set; }

        [JsonProperty("@sparql")]
        public string Sparql { get; set; }

        [JsonProperty("@policy")]
        public string Policy { get; set; }

        [JsonProperty("Resources")]
        public List<Resource> Resources { get; set; }
    }


}
