﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetCms.Structure.Search.TextAnalysis.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace NetCms.Structure.Search.TextAnalysis
{
    public class Resource
    {
        [JsonProperty("@URI")]
        public Uri Uri { get; set; }

        [JsonProperty("@support")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Support { get; set; }

        [JsonProperty("@types")]
        public string Types { get; set; }

        [JsonProperty("@surfaceForm")]
        public string SurfaceForm { get; set; }

        [JsonProperty("@offset")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Offset { get; set; }

        [JsonProperty("@similarityScore")]
        public string SimilarityScore { get; set; }

        [JsonProperty("@percentageOfSecondRank")]
        public string PercentageOfSecondRank { get; set; }
    }
}
