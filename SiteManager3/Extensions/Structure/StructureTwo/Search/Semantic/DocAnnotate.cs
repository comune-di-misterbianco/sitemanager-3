﻿using NetCms.Structure.WebFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCms.Structure.Search.TextAnalysis
{
    public class DocAnnotate
    {
        public virtual int ID { get; set; }    
        public virtual string URI { get; set; }
        public virtual string types { get; set; }
        public virtual string surfaceForm { get; set; }
        public virtual double score { get; set; }
        public virtual int offset { get; set; }

        public virtual DocContentAnnotate ContentAnnotate
        {
            get;
            set;    
        }

        public enum Colums
        {
            ID, URI, types, surfaceForm, score, offset, ContentAnnotate
        }
    }
}
