﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SolrNet.Commands.Parameters;
using SolrNet;
using Microsoft.Practices.ServiceLocation;
using SolrNet.Impl.ResponseParsers;
using System.Collections.ObjectModel;
using NetCms.Structure.Search.Models;

namespace CmsConfigs.DAO
{
    public class CmsSolrDao : System.IDisposable
    { 
        public CmsSolrDao()
        {


        }

        private ISolrOperations<CmsDocument> Solr
        {
            get
            {
                //return CmsConfigs.Model.GlobalConfig.Instance.Solr;
                ISolrOperations<CmsDocument> solr = ServiceLocator.Current.GetInstance<ISolrOperations<CmsDocument>>();
                return solr;
            }
        }

        public bool GetStatus()
        {
            bool status = true;
            try
            {
                Solr.Ping();
            }
            catch (Exception ex)
            {
                status = false;
            }
            return status;
        }

        public ClusterResults GetCluster(string searchTerm) //IDictionary<string, string> Parameters, int StartRecordNumber, int pageSize
        {


            ClusterResults cluRes = null;

            QueryOptions QueryOptions = new QueryOptions();
            QueryOptions.Rows = 10;
            QueryOptions.Fields = new[] { "*,score" };
            //QueryOptions.Start = StartRecordNumber;

            ClusteringParameters cluParam = new ClusteringParameters();

            //cluParam.ProduceSummary = true;
            cluParam.Collection = true;
            cluParam.Results = true;

            //cluParam.Engine = "kmeans";                  
            //cluParam.Algorithm = "org.carrot2.clustering.kmeans.BisectingKMeansClusteringAlgorithm";

            cluParam.Engine = "lingo";
            cluParam.Algorithm = "org.carrot2.clustering.lingo.LingoClusteringAlgorithm";
            cluParam.LexicalResources = "clustering/carrot2";

            //cluParam.NumDescriptions = 5;
            //cluParam.SubClusters = false;

            //cluParam.Title = "name";
            //cluParam.Url = "id";
            //cluParam.Snippet = "document_content_text";

            QueryOptions.Clustering = cluParam;

            IEnumerable<CmsDocument> Documents = Enumerable.Empty<CmsDocument>();

            SolrQuery query = new SolrQuery(searchTerm);
            SolrQueryResults<CmsDocument> results = Solr.Query(query, QueryOptions);

            cluRes = results.Clusters;

            return cluRes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="StartRecordNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="qt">Query type</param>
        /// <param name="qf"></param>
        /// <param name="useStopWords"></param>
        /// <param name="lowercaseOperators"></param>
        /// <param name="filters"></param>
        /// <returns></returns>
        public SearchResults SearchEdismax(string searchTerm, int StartRecordNumber, int pageSize, string qt, string qf, bool useStopWords, bool lowercaseOperators, bool enableCluster, IDictionary<string, string> filters = null, IDictionary<string, Order> orders = null)
        {


            QueryOptions QueryOptions = new QueryOptions();
            QueryOptions.Rows = pageSize;
            QueryOptions.Start = StartRecordNumber;
            QueryOptions.Fields = new Collection<string>() { "*", "score" };

            if (filters != null)
            {
                #region Filters
                Collection<SolrNet.ISolrQuery> filterQueries = new Collection<ISolrQuery>();
                SolrQueryByField filterQuerie;
                foreach (KeyValuePair<string, string> filter in filters)
                {
                    filterQuerie = new SolrQueryByField(filter.Key, filter.Value);
                    filterQueries.Add(filterQuerie);
                }

                QueryOptions.FilterQueries = filterQueries;
                #endregion
            }

            if (orders != null)
            {
                ICollection<SolrNet.SortOrder> orderList = new Collection<SolrNet.SortOrder>();
                foreach (KeyValuePair<string, Order> order in orders)
                {
                    SolrNet.Order ord = (SolrNet.Order)Enum.Parse(typeof(SolrNet.Order), order.Value.ToString());
                    orderList.Add(new SolrNet.SortOrder(order.Key.ToString(), ord));
                }
                QueryOptions.OrderBy = orderList;
            }

            if (enableCluster)
            {
                #region Cluster
                ClusteringParameters cluParam = new ClusteringParameters();

                //cluParam.ProduceSummary = true;
                cluParam.Collection = true;
                cluParam.Results = true;

                //cluParam.Engine = "kmeans";                  
                //cluParam.Algorithm = "org.carrot2.clustering.kmeans.BisectingKMeansClusteringAlgorithm";

                cluParam.Engine = "lingo";
                cluParam.Algorithm = "org.carrot2.clustering.lingo.LingoClusteringAlgorithm";
                cluParam.LexicalResources = "clustering/carrot2";

                //cluParam.ProduceSummary = true;
                //cluParam.FragSize = 70;                

                //cluParam.Title = "document_title";
                //cluParam.Url = "id";
                //cluParam.Snippet = "document_content_text";

                QueryOptions.Clustering = cluParam;
                #endregion
            }

            Dictionary<string, string> extraParams = new Dictionary<string, string>();
            extraParams.Add("qt", qt); //extraParams.Add("qt", "edismax");
            //extraParams.Add("q.alt", "*:*");
            extraParams.Add("qf", qf);
            extraParams.Add("stopwords", useStopWords.ToString().ToLower());
            extraParams.Add("lowercaseOperators", lowercaseOperators.ToString().ToLower());

            QueryOptions.ExtraParams = extraParams;

            SolrQuery query = new SolrQuery(searchTerm);

            SearchResults results = new SearchResults();


            SolrQueryResults<CmsDocument> queryResults = Solr.Query(query, QueryOptions);

            results.CmsDocuments = queryResults;

            ClusterResults clusters = queryResults.Clusters;

            if (clusters != null)
            {
                List<CmsCluster> cmsclusters = new List<CmsCluster>();
                CmsCluster cmsClu = null;
                foreach (Cluster cluster in clusters.Clusters)
                {
                    cmsClu = new CmsCluster() { Label = cluster.Label, Docs = GetDocumentByDocId(cluster.Documents), Score = cluster.Score };
                    cmsclusters.Add(cmsClu);
                }
                results.CmsClusters = cmsclusters;
            }
            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="qt"></param>
        /// <param name="qf"></param>
        /// <param name="useStopWords"></param>
        /// <param name="lowercaseOperators"></param>
        /// <param name="filters"></param>
        /// <returns></returns>
        public Int64 SearchEdismaxCount(string searchTerm, string qt, string qf, bool useStopWords, bool lowercaseOperators, IDictionary<string, string> filters = null)
        {
            QueryOptions QueryOptions = new QueryOptions();
            QueryOptions.Rows = 10;

            if (filters != null)
            {
                Collection<SolrNet.ISolrQuery> filterQueries = new Collection<ISolrQuery>();
                SolrQueryByField filterQuerie;
                foreach (KeyValuePair<string, string> filter in filters)
                {
                    filterQuerie = new SolrQueryByField(filter.Key, filter.Value);
                    filterQueries.Add(filterQuerie);
                }

                QueryOptions.FilterQueries = filterQueries;
            }


            Dictionary<string, string> extraParams = new Dictionary<string, string>();
            extraParams.Add("qt", qt); //extraParams.Add("qt", "edismax");
           // extraParams.Add("q.alt", "*:*");
            extraParams.Add("qf", qf);
            extraParams.Add("stopwords", useStopWords.ToString().ToLower());
            extraParams.Add("lowercaseOperators", lowercaseOperators.ToString().ToLower());

            QueryOptions.ExtraParams = extraParams;

            SolrQuery query = new SolrQuery(searchTerm);

            return Solr.Query(query, QueryOptions).NumFound;
        }

        public List<CmsClusterDoc> GetDocumentByDocId(ICollection<string> docs)
        {
            QueryOptions QueryOptions = new QueryOptions();
            QueryOptions.Rows = 10;
            QueryOptions.Fields = new Collection<string>() { "document_title", "document_description", "document_fronturl", "document_backurl" };


            List<string> docsTitle = new List<string>();

            ICollection<SolrQueryByField> Queries = new List<SolrQueryByField>();
            foreach (string doc in docs)
            {
                SolrQueryByField FieldQuery = new SolrQueryByField("id", doc);
                Queries.Add(FieldQuery);
            }

            SolrQueryResults<CmsDocument> queryResults = Solr.Query(new SolrMultipleCriteriaQuery(Queries.ToArray(), "OR"), QueryOptions);

            List<CmsClusterDoc> cmsCluDocs = new List<CmsClusterDoc>();
            foreach (CmsDocument doc in queryResults)
            {
                docsTitle.Add(doc.Title);
                cmsCluDocs.Add(new CmsClusterDoc() { Title = doc.Title, Description = doc.Description, BackUrl = doc.BackUrl, FrontUrl = doc.FrontUrl });
            }

            return cmsCluDocs;
        }

        public void AddContent(CmsDocument entity)
        {

            Solr.Add(entity);
            Solr.Commit();
        }

        public void DeleteContent(CmsDocument entity)
        {
            Solr.Delete(entity);
            Solr.Commit();
        }

        public void Optimize()
        {
            Solr.Optimize();
            Solr.Commit();
        }

        public void Rollback()
        {
            Solr.Rollback();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Free other state (managed objects).
                }
                // Free your own state (unmanaged objects).
                // Set large fields to null.
                disposed = true;
            }
        }

        public enum Order
        {
            ASC = 0,
            DESC = 1
        }
    }
}
