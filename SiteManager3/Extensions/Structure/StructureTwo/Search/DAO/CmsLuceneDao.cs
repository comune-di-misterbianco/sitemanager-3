﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericSearch;
using GenericSearch.LuceneWrapper;


namespace CmsConfigs.DAO
{
    public class CmsLuceneDao : System.IDisposable
    {
        private SearchLucene _LuceneArchive;
        public SearchLucene LuceneArchive         
        {
            get { 
                if(_LuceneArchive == null)
                {
                    _LuceneArchive = new SearchLucene(_IndexStorePath);
                }
                return _LuceneArchive;
            }            
        }

        private string _IndexStorePath;

        public CmsLuceneDao(string indexStorePath)
        {
            _IndexStorePath = indexStorePath;
        }


        public List<CmsDocumentLucene> Search(string searchText, int currentPage, int rpp)
        {
            List<string> otherField = new List<string>();
            otherField.Add("Description");
            otherField.Add("Testo");

            ResultFieldToShow fieldResults = new ResultFieldToShow("ID", "Title", otherField);

            SearchTerm searchTerm = new SearchTerm("Testo", searchText);

            List<CmsDocumentLucene> results = LuceneArchive.SearchContent(fieldResults, searchTerm, currentPage, rpp);

            return results;
        }
      
        public int SearchCount(string searchText)
        {
            SearchTerm searchTerm = new SearchTerm("Testo", searchText);
            int items = LuceneArchive.ResultsCount(new SearchTerm("Testo", searchText));
            return items;
        }


        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Free other state (managed objects).
                }
                // Free your own state (unmanaged objects).
                // Set large fields to null.
                disposed = true;
            }
        }
    }
}
