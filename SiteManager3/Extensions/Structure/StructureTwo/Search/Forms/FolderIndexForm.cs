﻿using NetService.Utility.ValidatedFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.Search.Forms
{
    public class FolderIndexForm : VfManager
    {
        public FolderIndexForm(string controlID):base(controlID)
        {
            this.Fields.Add(FolderID);
        }

        private VfTextBox _FolderID;
        public VfTextBox FolderID
        {
            get
            {
                if (_FolderID == null)
                {
                    _FolderID = new VfTextBox("folderID", "ID Folder");
                    _FolderID.TextBox.Columns = 30;
                    _FolderID.Required = true;
                }
                return _FolderID;
            }
        }
    }
}
