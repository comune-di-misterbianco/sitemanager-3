﻿using NetService.Utility.ValidatedFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace NetCms.Structure.Search.Forms
{
    public class CmsSearchForm : VfManager
    {
        public CmsSearchForm(string controlID)
            : base(controlID)
        {
            this.Fields.Add(SearchBox);
        }

        private VfTextBox _SearchBox;
        public VfTextBox SearchBox
        {
            get
            {
                if (_SearchBox == null)
                {
                    _SearchBox = new VfTextBox("searchBox", "Cerca nel sito");
                    _SearchBox.TextBox.Columns = 60;
                    _SearchBox.Required = true;
                }
                return _SearchBox;
            }
        }

    }
}
