﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using DynamicProxyLibrary;
//using System.Reflection;

//namespace NetCms.Structure.MultiEnte
//{
//    public class MultiEnteProxy
//    {
//        private DynamicProxyFactory factory;
//        private DynamicProxy proxy;

//        public MultiEnteProxy()
//        {
//            factory = new DynamicProxyFactory(System.Web.Configuration.WebConfigurationManager.AppSettings["MultiEnteWCFService"]);
//            proxy = factory.CreateProxy("IService");          
//        }

//        public Ente GetEnteById(int id)
//        {
//            object enteObj = proxy.CallMethod("GetEnteById", id);
//            proxy.Close();
//            Ente ente = new Ente();
//            MakeMap(enteObj, ente);
//            return ente;
//        }

//        public IList<Ente> GetEnti()
//        {
//            IList<object> entiObj = (IList<object>)proxy.CallMethod("GetEnti");
//            proxy.Close();
//            IList<Ente> enti = new List<Ente>();
//            foreach (object enteObj in entiObj)
//            {
//                Ente ente = new Ente();
//                MakeMap(enteObj, ente);
//                enti.Add(ente);
//            }
//            return enti;
//        }

//        public int GetNumeroEnti()
//        {
//            int numero = (int)proxy.CallMethod("GetNumeroEnti");
//            proxy.Close();
//            return numero;
//        }

//        private void MakeMap(object enteObj, Ente ente)
//        {
//            foreach(PropertyInfo prop in enteObj.GetType().GetProperties().Where(x=>x.Name!="ExtensionData"))
//            {
//                PropertyInfo enteProp = ente.GetType().GetProperty(prop.Name);
//                enteProp.SetValue(ente, prop.GetValue(enteObj, null), null);
//            }

//        }
//    }
//}
