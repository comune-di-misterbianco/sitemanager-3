﻿using System.Runtime.Serialization;

namespace NetCms.Structure.MultiEnte
{
    public class Ente
    {
        public Ente() { }

        public int ID
        {
            get;
            set;
        }

        public string Nome
        {
            get;
            set;
        }

        public string URI
        {
            get;
            set;
        }

        public string Root
        {
            get;
            set;
        }
    }
}