﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Reflection;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Users.Activities
{
    public static class ActivitiesDataPool
    {
        private const string AssembliesApplicationKey = "NetCms.Users.Activities.ActivitiesDataPool";

        public static List<ActivitiesData> Assemblies
        {
            get
            {
                if (_Assemblies == null)
                {
                    if (HttpContext.Current.Application[AssembliesApplicationKey] != null)
                        _Assemblies = (List<ActivitiesData>)HttpContext.Current.Application[AssembliesApplicationKey];
                    else
                        HttpContext.Current.Application[AssembliesApplicationKey] = _Assemblies = new List<ActivitiesData>();
                        
                }

                return _Assemblies;
            }
        }
        private static List<ActivitiesData> _Assemblies;
        /*
        private static void FindAppToInstall()
        {
            _Assemblies = new Dictionary<string, ActivitiesData>();
            DirectoryInfo binFolder = new DirectoryInfo(HttpContext.Current.Server.MapPath(Configurations.Paths.AbsoluteRoot + "/bin/"));

            foreach (FileInfo file in binFolder.GetFiles())
            {
                if (file.Extension == ".dll")
                {
                    Assembly asm = Assembly.LoadFrom(file.FullName);
                    Type[] types = asm.GetTypes();
                    foreach (Type type in types)
                    {
                        
                    }
                }
            }

            HttpContext.Current.Application[AssembliesApplicationKey] = Assemblies;
        }
        */
        /*public static object BuildInstanceOf(string className, string applicationID)
        {
            return BuildInstanceOf(className, applicationID, null);
        }

        public static object BuildInstanceOf(string className, string applicationID, object[] parameters)
        {
            if (Assemblies.ContainsKey(applicationID))
            {
                ActivitiesData application = Assemblies[applicationID];
                Type ApplicationFolderType = application.GetType();
                //Type[] array = application.Assembly.GetTypes();
                ConstructorInfo[] c = ApplicationFolderType.GetConstructors();
                if (parameters != null) return System.Activator.CreateInstance(ApplicationFolderType, parameters);
                else return System.Activator.CreateInstance(ApplicationFolderType);
            }
            //Trace?
            return null;
        }*/
        
        public static void ParseType(Type type, Assembly asm)
        {
            if (type.BaseType == Type.GetType("NetCms.Users.Activities.ActivitiesData"))
            {
                Object[] paramsArray = {};
                ActivitiesData ActivitiesData = (ActivitiesData)Activator.CreateInstance(type, paramsArray);

                Assemblies.Add(ActivitiesData);
            }
        }
    }
}