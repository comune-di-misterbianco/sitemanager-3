﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using NetCms.Networks;

namespace NetCms.Users.Activities
{
    public class Activity:TableRow
    {

        public int IdActivity
        {
            get { return _idActivity; }
            private set { _idActivity = value; }
        }
        private int _idActivity;

        public string Title
        {
            get { return _Title; }
            private set { _Title = value; }
        }
        private string _Title;

        public string Text
        {
            get { return _Text; }
            private set { _Text = value; }
        }
        private string _Text;

        public string TypeLabel
        {
            get { return _TypeLabel; }
            private set { _TypeLabel = value; }
        }
        private string _TypeLabel;

        public string TypeUrl
        {
            get { return _TypeUrl; }
            private set { _TypeUrl = value; }
        }
        private string _TypeUrl;

        public int NetworkID
        {
            get { return _NetworkID; }
            private set { _NetworkID = value; }
        }
        private int _NetworkID;
        
        public string NetworkSystemName
        {
            get
            {
                if (_NetworkSystemName == null && NetworkID != 0)
                {

                    var net = Networks.NetworksBusinessLogic.GetById(this.NetworkID);
                    if(net != null)
                        _NetworkSystemName = net.SystemName;
                }
                return _NetworkSystemName;
            }
        }
        private string _NetworkSystemName;

        public bool State
        {
            get { return _State; }
        }
        private bool _State;

        public Profile Profile
        {
            get { return _Profile; }
            private set { _Profile = value; }
        }
        private Profile _Profile;

        public DateTime Date
        {
            get { return _Date; }
            private set { _Date = value; }
        }
        private DateTime _Date;

        //public string Subject
        //{
        //    get { return _Subject; }
        //    private set { _Subject = value; }
        //}
        //private string _Subject;
        						
        //private NetCms.Connections.Connection _Connection;
        //private NetCms.Connections.Connection Connection
        //{
        //    get
        //    {
        //        if (_Connection == null)
        //            _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
        //        return _Connection;
        //    }
        //}

        //public Activity(DataRow recordData)
        //{
        //    LocalInit(recordData);
        //}

        public Activity(NotificaBackoffice recordData)
        {
            LocalInit(recordData);
        }

        //private void LocalInit(DataRow recordData)
        //{
        //    //this.Profile = new Profile(recordData["Profile_Activity"].ToString());
        //    this.Profile = ProfileBusinessLogic.GetById(int.Parse(recordData["Profile_Activity"].ToString()));
        //    this._State = recordData["Status_Activity"].ToString();
        //    this.Subject = recordData["Subject_Activity"].ToString();
        //    this.Title = recordData["Title_Activity"].ToString();
        //    this.Text = recordData["Text_Activity"].ToString();
        //    this.TypeLabel = recordData["Label_ActivityType"].ToString();
        //    this.NetworkID = int.Parse(recordData["Network_Activity"].ToString());
        //    this.TypeUrl = recordData["DetailUrl_ActivityType"].ToString();
        //    DateTime.TryParse(recordData["Date_Activity"].ToString(), out _Date);
        //}

        private void LocalInit(NotificaBackoffice recordData)
        {
            this._idActivity = recordData.ID;
            this.Profile = recordData.Destinatario.Profile;
            this._State = recordData.Letta;
            this.Title = recordData.Titolo;
            this.Text = recordData.Testo;
            if(recordData.Network != null)
                this.NetworkID = recordData.Network.ID;
            this.TypeUrl = recordData.Link;
            this._Date = recordData.Data;
            this._TypeLabel = recordData.NotifyBackofficeType.ToString();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            TableCell cell = new TableCell();
            if (this.State)
                cell.Text = this.Title;
            else
                cell.Text = "<strong>" + this.Title + "</strong>";
            this.Cells.Add(cell);

            cell = new TableCell();
            if (this.State)
                cell.Text = this.TypeLabel;
            else
                cell.Text = "<strong>" + this.TypeLabel + "</strong>";
            this.Cells.Add(cell);

            cell = new TableCell();
            //cell.Text = string.Format("{0:00}",Date.Day) + "/" + string.Format("{0:00}",Date.Month) + "/" + string.Format("{0:00}",Date.Year )+ " alle " + string.Format("{0:00}",Date.Hour) + ":" + string.Format("{0:00}",Date.Minute);
            cell.Text = string.Format("{0:00}", Date.Day) + "/" + string.Format("{0:00}", Date.Month) + "/" + string.Format("{0:00}", Date.Year);
            this.Cells.Add(cell);

            cell = new TableCell();
            string url = TypeUrl;
            if (NetworkSystemName != null) url = url.Replace("{NETWORK_SYSTEM_NAME}", NetworkSystemName);

            //url = url.Replace("{SUBJECT}", this.Subject);

            if (!url.Contains("http://"))
                url = NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + url;

            //cell.Text = "<a class=\"action details\" href=\"" + url + "\"><span>Mostra</span></a>";
            LinkButton lbtn = new LinkButton();
            lbtn.ID = "activity_" + this.IdActivity;
            lbtn.Text = "Mostra";
            lbtn.CssClass = "action details";
            lbtn.Click += new EventHandler((s, ev) => lbtn_Click(s, ev, url));
            //lbtn.Attributes["runat"] = "server";
            //lbtn.PostBackUrl = url;
            cell.Controls.Add(lbtn);
            this.Cells.Add(cell);

            cell = new TableCell();
            LinkButton delbtn = new LinkButton();
            delbtn.ID = "delactivity_" + this.IdActivity;
            delbtn.Text = "Rimuovi";
            delbtn.CssClass = "action delete";
            delbtn.Click += new EventHandler((s, ev) => delbtn_Click(s, ev, url));
            //lbtn.Attributes["runat"] = "server";
            //lbtn.PostBackUrl = url;
            cell.Controls.Add(delbtn);
            this.Cells.Add(cell);
        }
        //protected override void OnPreRender(EventArgs e)
        //{
        //    base.OnPreRender(e);

        //    TableCell cell = new TableCell();
        //    if (this.State)
        //        cell.Text = this.Title;
        //    else
        //        cell.Text = "<strong>" + this.Title + "</strong>";
        //    this.Cells.Add(cell);

        //    cell = new TableCell();
        //    if (this.State)
        //        cell.Text = this.TypeLabel;
        //    else
        //        cell.Text = "<strong>" + this.TypeLabel + "</strong>";
        //    this.Cells.Add(cell);

        //    cell = new TableCell();
        //    //cell.Text = string.Format("{0:00}",Date.Day) + "/" + string.Format("{0:00}",Date.Month) + "/" + string.Format("{0:00}",Date.Year )+ " alle " + string.Format("{0:00}",Date.Hour) + ":" + string.Format("{0:00}",Date.Minute);
        //    cell.Text = string.Format("{0:00}",Date.Day) + "/" + string.Format("{0:00}",Date.Month) + "/" + string.Format("{0:00}",Date.Year );
        //    this.Cells.Add(cell);

        //    cell = new TableCell();
        //    string url = TypeUrl;
        //    if(NetworkSystemName!=null) url = url.Replace("{NETWORK_SYSTEM_NAME}", NetworkSystemName);
            
        //    //url = url.Replace("{SUBJECT}", this.Subject);
            
        //    if (!url.Contains("http://"))
        //        url = NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + url;

        //    //cell.Text = "<a class=\"action details\" href=\"" + url + "\"><span>Mostra</span></a>";
        //    LinkButton lbtn = new LinkButton();
        //    lbtn.ID = "activity_" + this.IdActivity;
        //    lbtn.Text = "Mostra";
        //    lbtn.CssClass = "action details";
        //    lbtn.Click += new EventHandler((s,ev) => lbtn_Click(s,ev, url));
        //    //lbtn.PostBackUrl = url;
        //    cell.Controls.Add(lbtn);
        //    this.Cells.Add(cell);
        //}

        void lbtn_Click(object sender, EventArgs e, string url)
        {
            string idBtn = ((LinkButton)sender).ID;

            string id = idBtn.Split('_')[1];
               
            NetCms.Users.Notifica.SetNotificaDone(id);

            HttpContext.Current.Response.Redirect(url);
        }


        void delbtn_Click(object sender, EventArgs e, string url)
        {
            string idBtn = ((LinkButton)sender).ID;

            string id = idBtn.Split('_')[1];

            NetCms.Users.Notifica.SetNotificaDeleted(id);

            HttpContext.Current.Response.Redirect("#activity");
        }
        //public static void CreateNewActivity(string Title, string Text, string Type, string ProfileID, string SenderProfileID, string Subject, string NetworkID)
        //{
        //    string insert = "INSERT INTO users_activities (Title_Activity,Text_Activity,Type_Activity,Subject_Activity,Status_Activity,Profile_Activity,SenderProfile_Activity,Date_Activity,";
        //    if (NetworkID != null) insert += "Network_Activity";
        //    insert += ") VALUES(";

        //    string values = "'" + Title + "'";
        //    values += ",'" + Text + "'";
        //    values += "," + Type + "";
        //    values += "," + Subject + "";
        //    values += ",1";
        //    values += ",'" + ProfileID + "'";
        //    values += ",'" + SenderProfileID + "'";
        //    values += "," + G2Core.Connections.OdbcConnection.OdbcFilterDateTime(DateTime.Now) + "";
        //    if (NetworkID != null) values += ",'" + NetworkID + "'";
        //    values += ")";

        //    NetCms.Connections.ConnectionsManager.CommonConnection.Execute(insert +  values);
        //}

        //public static void CreateNewActivity(string Title, string Text, NetCms.Users.NotificaBackoffice.NotificaBackofficeType type, int DestProfileID, int SenderProfileID, string url, int NetworkID)
        //{
        //    NotificaBackoffice notifica = new NotificaBackoffice();
        //    notifica.Titolo = Title;
        //    notifica.Testo = Text;
        //    notifica.Data = DateTime.Now;
        //    notifica.Letta = false;
        //    notifica.Letta = false;
        //    notifica.NotifyBackofficeType = type;
        //    notifica.Link = url;
        //    User Destinatario = (ProfileBusinessLogic.GetById(DestProfileID) as UserProfile).User;
        //    notifica.Destinatario = Destinatario;
        //    notifica.Responsabile = (ProfileBusinessLogic.GetById(SenderProfileID) as UserProfile).User;
        //    Destinatario.Notifiche.Add(notifica);

        //    if (NetworkID != null)
        //    {
        //        notifica.Network = NetworksBusinessLogic.GetById(NetworkID);
        //    }

        //    UsersBusinessLogic.SaveOrUpdateNotifica(notifica);
        //}

        //public static void SetActivityDone(string activityID)
        //{
        //    string date = G2Core.Connections.OdbcConnection.OdbcFilterDateTime(DateTime.Now);
        //    string update = "UPDATE users_activities SET DateDone_Activity = " + date + ", Status_Activity = 1 WHERE id_Activity = " + activityID;
        //    NetCms.Connections.ConnectionsManager.CommonConnection.Execute(update);
        //}

        //public static void SetActivityDone(string activityID)
        //{
        //    NotificaBackoffice notifica = UsersBusinessLogic.GetNotificaById(int.Parse(activityID)) as NotificaBackoffice;
        //    notifica.Letta = true;
        //    UsersBusinessLogic.SaveOrUpdateNotifica(notifica);
        //}

        //public static void SetActivityDeleted(string activityID)
        //{
        //    NotificaBackoffice notifica = UsersBusinessLogic.GetNotificaById(int.Parse(activityID)) as NotificaBackoffice;
        //    notifica.Cancellata = true;
        //    UsersBusinessLogic.SaveOrUpdateNotifica(notifica);
        //}
    }
}
