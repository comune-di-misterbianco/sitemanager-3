﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS.Reviews;
using NetCms.DBSessionProvider;
using NHibernate;

namespace NetCms.Users.Activities
{
    public class ReviewsActivitiesData:ActivitiesData
    {
        public override string ID
        {
            get { return "1"; }
        }

        public ReviewsActivitiesData()
        {
        }

        //public override DataTable GetDataTable(User currentUser)
        //{            
        //    DataTable table = this.TemplateTable.Copy();
        //    //DataTable tmptable = null;
        //    foreach (var network in NetCms.Networks.NetworksManager.GlobalIndexer.CmsEnabled)
        //    {
        //        if (currentUser.NetworkEnabledForUser(network.Value.ID))
        //        {
        //            var notificheUtente = ReviewBusinessLogic.FindNotifyByUserId(currentUser.ID);

        //            //string sql = "SELECT DISTINCT Mittente_RevisioneNotifica,Label_DocLang,Revisione_RevisioneNotifica,Data_Doc,Folder_Doc,id_Doc FROM docs_lang INNER JOIN docs ON Doc_DocLang = id_Doc INNER JOIN revisioni_notifiche ON Doc_RevisioneNotifica = id_Doc WHERE Stato_RevisioneNotifica = 0 AND Destinatario_RevisioneNotifica = " + currentUser.ID;

        //            //tmptable = network.Value.Connection.SqlQuery(sql);
                    
        //            //foreach (DataRow row in tmptable.Rows)
        //            foreach (ReviewNotify notifica in notificheUtente)
        //            {
        //                try
        //                {
        //                    DataRow newRow = table.NewRow();

        //                    //User usr = new User(notifica.Mittente);

        //                    User usr = UsersBusinessLogic.GetById(notifica.Mittente);

        //                    newRow["Title_Activity"] = newRow["Text_Activity"] = "L'Utente '" + usr.NomeCompleto + "' chiede che sia revisionato il documento '" + notifica.Document.DocLangs.ElementAt(0).Label + "' del portale '" + network.Value.Label + "'.";
        //                    newRow["Type_Activity"] = NetCms.Users.Activities.ActivitiesTypes.Revisioni;
        //                    newRow["Label_ActivityType"] = "Richiesta di Revisione";
        //                    newRow["Subject_Activity"] = notifica.Review.ID;
        //                    newRow["Status_Activity"] = "0";
        //                    newRow["Profile_Activity"] = currentUser.Profile.ID;
        //                    newRow["Network_Activity"] = network.Value.ID;
        //                    newRow["Date_Activity"] = notifica.Document.Data;
        //                    newRow["DetailUrl_ActivityType"] = "/" + network.Value.SystemName + "/docs/overview.aspx?folder=" + notifica.Document.Folder.ID + "&doc=" +notifica.Document.ID + "&review=" + notifica.Review.ID;
        //                    /*http://localhost:52904/website/cms/istituti/docs/overview.aspx?folder=141&doc=122*/
        //                    table.Rows.Add(newRow);
        //                }
        //                catch (Exception e) { }
        //            }
        //        }
        //    }
        //    return table;
        //}

        public override ICollection<NotificaBackoffice> GetDataTable(User currentUser)
        {
            ICollection<NotificaBackoffice> table = new List<NotificaBackoffice>();

            //using (ISession sess = FluentSessionProvider.Instance.OpenInnerSession())
            //using (ITransaction tx = sess.BeginTransaction())
            //{
            //    try
            //    {
            //        foreach (var network in NetCms.Networks.NetworksManager.GlobalIndexer.CmsEnabled)
            //        {
            //            if (currentUser.NetworkEnabledForUser(network.Value.ID))
            //            {
            //                var notificheUtente = ReviewBusinessLogic.FindNotifyByUserId(currentUser.ID, sess);

            //                foreach (ReviewNotify notifica in notificheUtente)
            //                {
            //                    if (notifica.Document.NetworkID == network.Value.ID)
            //                    {
            //                        try
            //                        {
            //                            User usr = UsersBusinessLogic.GetById(notifica.Mittente,sess);


            //                            string title = "L'Utente '" + usr.NomeCompleto + "' chiede che sia revisionato il documento '" + notifica.Document.DocLangs.ElementAt(0).Label + "' del portale '" + network.Value.Label + "'.";
            //                            string Text = title;

            //                            User destinatario = UsersBusinessLogic.GetById(notifica.Destinatario,sess);

            //                            string url = "/" + network.Value.SystemName + "/docs/overview.aspx?folder=" + notifica.Document.Folder.ID + "&doc=" + notifica.Document.ID + "&review=" + notifica.Review.ID;

            //                            if (UsersBusinessLogic.GetNotificaByTesto(destinatario, Text, Notifica.TipoNotifica.Backoffice, network.Value.ID,sess) == null)
            //                                NetCms.Users.NotificaBackoffice.CreateNewNotificaBackoffice(title, Text, NetCms.Users.NotificaBackoffice.NotificaBackofficeType.Revisioni, destinatario.Profile.ID, usr.Profile.ID, url, network.Value.ID,sess);

            //                        }
            //                        catch (Exception e)
            //                        {
            //                            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, e.Message);
            //                        }
            //                    }
            //                }
            //            }
            //        }

            //        tx.Commit();

            //    }
            //    catch (Exception ex)
            //    {
            //        tx.Rollback();
               
            //    }
            //}


            foreach (var network in NetCms.Networks.NetworksManager.GlobalIndexer.CmsEnabled)
            {
                if (currentUser.NetworkEnabledForUser(network.Value.ID))
                {
                    try
                    {
                        var notificheUtente = UsersBusinessLogic.GetNotificheNonCancellate(AccountManager.CurrentAccount, Notifica.TipoNotifica.Backoffice, network.Value.ID);

                        if (notificheUtente.Count > 0)
                        {
                            //table.(notificheUtente.Where(x => (x as NotificaBackoffice).NotifyBackofficeType == NotificaBackoffice.NotificaBackofficeType.NotificaGenerica).ToList());
                            var selection = from notifica in notificheUtente
                                            where ((notifica as NotificaBackoffice).NotifyBackofficeType == NotificaBackoffice.NotificaBackofficeType.Revisioni)
                                            select notifica as NotificaBackoffice;

                            if (selection != null && selection.Count() > 0)
                                table = table.Union(selection).ToList();
                        }
                    }
                    catch (Exception ex)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante il caricamento delle notifiche di backoffice di tipo revisione per l'utente '" + AccountManager.CurrentAccount.UserName + "' relativamente alla network '" + network.Value.SystemName + "'. " + ex.Message);
                    }


                }
            }
            
            return table;
        }
    }
}
