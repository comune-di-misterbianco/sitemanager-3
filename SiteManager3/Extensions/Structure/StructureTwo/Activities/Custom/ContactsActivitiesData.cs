﻿//using System;
//using System.IO;
//using System.Web;
//using System.Data;
//using System.Xml;
//using System.Web.UI.WebControls;
//using System.Collections;
//using System.Collections.Generic;

//namespace NetCms.Users.Activities
//{
//    public class ContactsActivitiesData:ActivitiesData
//    {
//        public NetCms.Users.User CurrentUser
//        {
//            get
//            {
//                if (_CurrentUser == null)
//                    _CurrentUser = NetCms.Users.AccountManager.CurrentAccount;
//                return _CurrentUser;
//            }
//        }
//        private NetCms.Users.User _CurrentUser;

//        public override string ID
//        {
//            get { return "4"; }
//        }

//        public ContactsActivitiesData()
//        {
//        }

//        public override DataTable GetDataTable(NetCms.Users.User currentUser)
//        {
//            DataTable table = this.TemplateTable.Copy();
//            DataTable tmptable = null;

//            if (NetCms.Vertical.VerticalApplicationsPool.ActiveAssemblies.Contains("239"))
//            {
//                foreach (var network in NetCms.Networks.NetworksManager.GlobalIndexer.CmsEnabled)
//                {
//                    if (this.CurrentUser.NetworkEnabledForUser(network.Value.ID))
//                    {
//                        string MyOffices = string.Empty;

//                        foreach (NetCms.Users.Group g in this.CurrentUser.Groups)
//                        {
//                            MyOffices += " OR Profile_Member = " + g.Profile.ID;

//                            NetCms.Users.Group gp = g;
//                            while (gp.Profile.HasParent)
//                            {
//                                gp = gp.Parent;
//                                if (!MyOffices.Contains("Profile_Member = " + gp.Profile.ID))
//                                    MyOffices += " OR Profile_Member = " + gp.Profile.ID;
//                            }
//                        }
//                        MyOffices = "Profile_Member = " + CurrentUser.Profile.ID + MyOffices;

//                        NetUtility.RequestVariable page = new NetUtility.RequestVariable("page", NetUtility.RequestVariable.RequestType.Form_QueryString);
//                        string sql = "SELECT * FROM Contacts ";
//                        sql += " INNER JOIN Uffici ON Office_Contact = id_Ufficio";
//                        sql += " INNER JOIN Uffici_Membri ON id_Ufficio = Office_Member";

//                        sql += " WHERE (" + MyOffices + ") AND Network_Ufficio = " + network.Value.ID;
//                        sql += " AND Stato_Contact = 0";

//                        tmptable = network.Value.Connection.SqlQuery(sql);

//                        foreach (DataRow row in tmptable.Rows)
//                        {
//                            try
//                            {
//                                DataRow newRow = table.NewRow();

//                                newRow["Title_Activity"] = newRow["Text_Activity"] = "L'Utente '" + row["Name_Contact"] + "' ha fatto una domanda all'ufficio " + row["Nome_Ufficio"] + " del portale '" + network.Value.Label + "'.";
//                                newRow["Type_Activity"] = NetCms.Users.Activities.ActivitiesTypes.Revisioni;
//                                newRow["Label_ActivityType"] = "Contatto da Web";
//                                newRow["Subject_Activity"] = row["id_Contact"];
//                                newRow["Status_Activity"] = "0";
//                                newRow["Profile_Activity"] = currentUser.Profile.ID;
//                                newRow["Network_Activity"] = network.Value.ID;
//                                newRow["Date_Activity"] = row["Date_Contact"];
//                                newRow["DetailUrl_ActivityType"] = "/" + network.Value.SystemName + "/applications/contacts/answer.aspx?contact=" + row["id_Contact"];
//                                /*http://localhost:52904/website/cms/AGAA846027/applications/contacts2/answer.aspx?contact=9*/
//                                table.Rows.Add(newRow);
//                            }
//                            catch (Exception e) { }
//                        }
//                    }
//                }
//            }

//            return table;
//        }
//    }
//}
