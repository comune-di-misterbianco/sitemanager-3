﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Users.Activities
{
    public class SimpleActivitiesData : ActivitiesData
    {
        public override string ID
        {
            get { return "2"; }
        }

        public SimpleActivitiesData()
        {
        }

        public override ICollection<NotificaBackoffice> GetDataTable(User currentUser)
        {
            ICollection<NotificaBackoffice> table = new List<NotificaBackoffice>();

            ///Codice che effettua il controllo incrociato delle notifiche utente, verificando che questo abbia i permessi di accesso alla network
            foreach (var network in NetCms.Networks.NetworksManager.GlobalIndexer.CmsEnabled)
            {
                if (currentUser.NetworkEnabledForUser(network.Value.ID))
                {
                    try
                    {
                        var notificheUtente = UsersBusinessLogic.GetNotificheNonCancellate(AccountManager.CurrentAccount, Notifica.TipoNotifica.Backoffice, network.Value.ID);

                        if (notificheUtente.Count > 0)
                        {
                            //table.(notificheUtente.Where(x => (x as NotificaBackoffice).NotifyBackofficeType == NotificaBackoffice.NotificaBackofficeType.NotificaGenerica).ToList());
                            var selection = from notifica in notificheUtente
                                            where ((notifica as NotificaBackoffice).NotifyBackofficeType == NotificaBackoffice.NotificaBackofficeType.Notifica)
                                            select notifica as NotificaBackoffice;

                            if(selection!= null && selection.Count() > 0)
                                table = table.Union(selection).ToList();              
                        }
                    }
                    catch(Exception ex)
                    {
                       NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante il caricamento delle notifiche di backoffice per l'utente '" + AccountManager.CurrentAccount.UserName +  "' relativamente alla network '" + network.Value.SystemName + "'. " + ex.Message);
                    }


                }
            }

            ///Le applicazioni verticali non di CMS non è detto che siano legate alla network, in quel caso la colonna Network sarà null nella tabella notifiche e non ci sono vincoli di permessi.
            ///Il seguente codice, unisce quindi alla collezione precedentemente prevista, le notifiche non vincolate alla network ma di cui l'utente è destinatario.
            ///
            var NoNetworksNotifications = UsersBusinessLogic.GetNotificheNonCancellateNonDiNetwork(AccountManager.CurrentAccount, Notifica.TipoNotifica.Backoffice);
            if (NoNetworksNotifications.Count > 0)
            {
                var BackOfficeNotifications = from notifica in NoNetworksNotifications
                                                where ((notifica as NotificaBackoffice).NotifyBackofficeType == NotificaBackoffice.NotificaBackofficeType.Notifica)
                                                select notifica as NotificaBackoffice;

                if (BackOfficeNotifications != null && BackOfficeNotifications.Count() > 0)
                    table = table.Union(BackOfficeNotifications).ToList();
            }

            

            return table;
        }
    }
}
