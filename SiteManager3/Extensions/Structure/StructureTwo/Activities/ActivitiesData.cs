﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;

namespace NetCms.Users.Activities
{
    public abstract class ActivitiesData
    {        						
        //private NetCms.Connections.Connection _Connection;
        //protected NetCms.Connections.Connection Connection
        //{
        //    get
        //    {
        //        if (_Connection == null)
        //            _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
        //        return _Connection;
        //    }
        //}

        public abstract string ID
        {
            get;
        }

        //private const string sql = "SELECT * FROM users_activities INNER JOIN users_activities_types ON Type_Activity = id_ActivityType";
						
        //public DataTable TemplateTable 
        //{
        //    get 
        //    { 
        //        if(_TemplateTable == null)
        //            _TemplateTable = this.Connection.SqlQuery(sql);
        //        return _TemplateTable; 
        //    } 
        //}
        //private DataTable _TemplateTable;

        public ActivitiesData()
        {
        }

        //public abstract DataTable GetDataTable(NetCms.Users.User currentUser);

        public abstract ICollection<NotificaBackoffice> GetDataTable(NetCms.Users.User currentUser);

    }
}
