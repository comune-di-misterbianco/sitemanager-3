﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NetCms.Users.Activities
{
    public class Activities:Table
    {        					
        public NetCms.Users.User CurrentUser
        {
            get
            {
                if (_CurrentUser == null)
                    _CurrentUser = NetCms.Users.AccountManager.CurrentAccount;
                return _CurrentUser;
            }
        }
        private NetCms.Users.User _CurrentUser;

        //public DataTable ActivitiesTable
        //{
        //    get
        //    {
        //        if (_ActivitiesTable == null)
        //            _ActivitiesTable = BuildActivitiesDataTable();
        //        return _ActivitiesTable;
        //    }
        //}
        //private DataTable _ActivitiesTable;

        public ICollection<NotificaBackoffice> ActivitiesTable
        {
            get
            {
                if (_ActivitiesTable == null)
                    _ActivitiesTable = BuildActivitiesDataTable();
                return _ActivitiesTable;
            }
        }
        private ICollection<NotificaBackoffice> _ActivitiesTable;
	
        public string ProfileID
        {
            get { return _ProfileID; }
            private set { _ProfileID = value; }
        }
        private string _ProfileID;

        public Activities(string profileID)
        {
            ProfileID = profileID;
        }

        //protected DataTable BuildActivitiesDataTable()
        //{
        //    string sql = "SELECT * FROM users_activities INNER JOIN users_activities_types ON Type_Activity = id_ActivityType WHERE Profile_Activity = " + ProfileID;
        //    DataTable table = this.Connection.SqlQuery(sql);
        //    table = table.Copy();

        //    foreach (var ActivitiesData in ActivitiesDataPool.Assemblies)
        //    {
        //        DataTable tmptable = ActivitiesData.GetDataTable(CurrentUser);

        //        foreach (DataRow row in tmptable.Rows)
        //            table.ImportRow(row);
        //    }

        //    return table;
        //}

        protected ICollection<NotificaBackoffice> BuildActivitiesDataTable()
        {
            ICollection<NotificaBackoffice> table = new List<NotificaBackoffice>();

            foreach (var ActivitiesData in ActivitiesDataPool.Assemblies)
            {
                ICollection<NotificaBackoffice> tmptable = ActivitiesData.GetDataTable(CurrentUser);

                foreach (NotificaBackoffice row in tmptable)
                    table.Add(row);
            }

            return table;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        //}

        //protected override void OnPreRender(EventArgs e)
        //{
        //    base.OnPreRender(e);

            this.CellPadding = 0;
            this.CellSpacing = 0;
            this.CssClass = "DetailsTable activities-table";

            TableHeaderRow row = new TableHeaderRow();
                        
            TableHeaderCell cell = new TableHeaderCell();
            cell.Text = "Attività";
            row.Cells.Add(cell);

            cell = new TableHeaderCell();
            cell.Text = "Tipo";
            row.Cells.Add(cell);

            cell = new TableHeaderCell();
            cell.Text = "Data";
            row.Cells.Add(cell);

            cell = new TableHeaderCell();
            cell.Text = "Mostra";
            row.Cells.Add(cell);

            cell = new TableHeaderCell();
            cell.Text = "Rimuovi";
            row.Cells.Add(cell);

            this.Rows.Add(row);
            bool alternate = false;
            if (ActivitiesTable.Count > 0)
            {
                G2Core.Common.PageMaker pager= new G2Core.Common.PageMaker(G2Core.Common.PageMaker.VariabileType.QueryString);
                pager.RPP = 20;
                
                //DataRow[] rowsSource = ActivitiesTable.Select("","Date_Activity DESC");

                ICollection<NotificaBackoffice> rowsSource = ActivitiesTable.OrderByDescending(x => x.Data).Skip((pager.Page - 1) * pager.RPP).Take(pager.RPP).ToList();
                //DataRow[] rows = pager.MakeTablePagination(rowsSource);

                foreach (NotificaBackoffice record in rowsSource)
                {
                    TableRow trow = new Activity(record);
                    if (alternate)
                    {
                        trow.CssClass += " alternate";
                    }
                    alternate = !alternate;
                    this.Rows.Add(trow);
                }
                TableRow tfrow = new TableRow();
                TableCell tfcell = new TableCell();
                tfcell.ColumnSpan = 4;
                tfcell.Controls.Add(pager.PageNavigation("?page={0}&#activity", ActivitiesTable.Count));
                tfrow.Controls.Add(tfcell);
                this.Rows.Add(tfrow);

            }
            else
            {
                TableHeaderRow erow = new TableHeaderRow();

                TableCell ecell = new TableCell();
                ecell.Text = "Al momento non hai alcuna attività";
                ecell.ColumnSpan = 5;
                erow.Cells.Add(ecell);

                this.Rows.Add(erow);
            }
        }
    }
}
