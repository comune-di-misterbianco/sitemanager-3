﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CmsConfigs.Model
{
    public class IndexConfig
    {
        //public virtual int ID
        //{
        //    get;
        //    set;
        //}

        public virtual string IndexConfigKey
        {
            get;
            set;
        }

        public virtual IndexMode Mode
        {
            get;
            set;
        }

        public virtual string ZoomSettingsFolderPath
        {
            get;
            set;
        }

        public virtual string LuceneSettingsFolderPath
        {
            get;
            set;
        }

        public virtual string SolrHostAddress
        {
            get;
            set;
        }

        public virtual int SolrHostTcpPort
        {
            get;
            set;
        }

        public virtual string SolrCore
        {
            get;
            set;
        }

        public virtual bool SolrEnabled
        {
            get
            {
                if (this.Mode == CmsConfigs.Model.IndexConfig.IndexMode.solr)
                    return true;
                return false;
            }

        }

        public enum IndexMode
        {
            zoom = 0,
            lucene = 1,
            solr = 2
        }

        public enum Column
        {
            ID,
            Mode,
            ZoomSettingsFolderPath,
            LuceneSettingsFolderPath,
            SolrHostAddress,
            SolrHostTcpPort,
            SolrCore,
        }
    }
}
