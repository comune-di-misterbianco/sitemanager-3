﻿using System;
using System.Collections.Generic;
using LoginManager.Models;
using NetCms.DBSessionProvider;
using NetCms.Structure.Search.Models;
using System.Linq;
using NHibernate;
using Microsoft.Practices.ServiceLocation;
using System.Web.UI.WebControls;
using System.Web.UI;
using NetCms.Themes.Model;

namespace CmsConfigs.Model
{
    public class GlobalConfig
    {
        //private static GlobalConfig _Instance;
        //public static GlobalConfig Instance 
        //{
        //    get
        //    {
        //        if (_Instance == null)
        //        {
        //            _Instance = new GlobalConfig();
        //        }
        //        return _Instance;
        //    }
        //}     

        //public static WebControl GetLoginMessagesControl()
        //{
        //    string loginFrontendInfo = NetCms.Configurations.XmlConfig.RemoveCDATA(NetCms.Configurations.XmlConfig.GetNodeXml("/Portal/configs/loginFrontendInfo"));

        //    WebControl div = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
        //    div.CssClass = "LoginMessagesControl";
        //    div.Controls.Add(new LiteralControl(loginFrontendInfo));

        //    return div;
        //}

        public static List<Config> CMSConfigs
        {
            get;
            set;
        }

        public static Config GetConfigByNetworkID(int idNetwork)
        {
            Config currentConfig = null;
            try
            {
                if (CMSConfigs != null)
                {
                    currentConfig = CMSConfigs.Find(x => x.RifNetwork == idNetwork);

                    if (currentConfig == null)
                    {
                        currentConfig = CMSConfigs.Find(x => x.RifNetwork == 0);
                    }

                    if (LanguageManager.Utility.Config.EnableMultiLanguage)
                    {
                        currentConfig.CurrentLanguage = LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage;
                    }

                    CurrentConfig = currentConfig;
                }
            }
            catch(Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Record di default mancante nella tabella 'config_settings'.", "","",ex);                
            }
            return currentConfig;
        }

        public static Config CurrentConfig { get; protected set; }


        public static List<Theme> AvailableThemes { get; set; }

        public static List<LoginSetting> LoginSettings
        {
            get;
            set;
        }

        public static IndexConfig IndexConfig
        {
            get;
            set;
        }

        public static LoginSetting GeneralLoginSetting
        {
            get
            {
                return LoginManager.BusinessLogic.LoginSettingsBL.GetGeneralSetting();
            }
        }

        public static WebControl GetLoginControl(int idCurrentNetwork)
        {
            return LoginManager.BusinessLogic.LoginSettingsBL.GetLoginControl(idCurrentNetwork);
        }
        
        public static void Init()
        {
            ISession session = FluentSessionProvider.Instance.OpenInnerSession();
            try
            {
                #region Init Info.json
                //#region css

                //List<Css> cssDep = new List<Css>();
                //cssDep.Add(new Css("/scripts/bootstrap-4/bootstrap-min.css", "", ""));
                //cssDep.Add(new Css("/scripts/front/footable/css/footable.core.css", "", ""));
                //cssDep.Add(new Css("/scripts/front/DateTimePickerEonasdan/css/bootstrap-datetimepicker.css", "", ""));
                //cssDep.Add(new Css("/bootstrap-italia/dist/css/bootstrap-italia.min.css", "", ""));

                //List<Css> cssTheme = new List<Css>();
                //cssTheme.Add(new Css("css/theme.css", "", ""));
                //cssTheme.Add(new Css("css/stampa.css", "", ""));

                //NetCms.Themes.Model.GlobalCss css = new NetCms.Themes.Model.GlobalCss(cssDep, cssTheme);

                //#endregion

                //#region js
                //List<string> jsDep = new List<string>();
                //jsDep.Add("/scripts/bootstrap-4/bootstrap-min.js");
                //jsDep.Add("https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js");
                //jsDep.Add("https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js");
                //jsDep.Add("https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js");
                //jsDep.Add("/scripts/front/jquery-ui/jquery-ui.min.js");
                //jsDep.Add("/scripts/front/footable/js/footable.js");
                //jsDep.Add("/scripts/front/MomentJS/moment-with-locales.js");
                //jsDep.Add("front/DateTimePickerEonasdan/js/bootstrap-datetimepicker.min.js");
                //jsDep.Add("/bootstrap-italia/dist/bootstrap-italia.min.js");

                //List<string> jsTheme = new List<string>();
                //jsTheme.Add("js/theme.js");

                //NetCms.Themes.Model.GlobalJS js = new NetCms.Themes.Model.GlobalJS(jsDep, jsTheme);

                //#endregion

                //NetCms.Themes.Model.Assets assets = new NetCms.Themes.Model.Assets(css, js);

                //NetCms.Themes.Model.Theme agid2019 = new NetCms.Themes.Model.Theme(
                //   "Agid 2019",
                //   "Implementazione della libreria Bootstrap Italia per lo sviluppo di applicazioni web per la PA",
                //   "favicon.ico",
                //   "logo.png",
                //   "screenshot.jpg",
                //   "1.0",
                //   assets
                //   );

                //string absoluteThemesPath = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.ThemesFolder);

                //agid2019.Save(absoluteThemesPath + "\\agid2019\\");
                #endregion

                // Lettura dalla cartella Themes dei temi disponibili e "validi"
                AvailableThemes = NetCms.Themes.Parser.GetAllTheme(NetCms.Configurations.ThemeEngine.ThemesFolder);

                // leggere tutte le configurazioni presenti 
                // successivamente se necessario recuperare ove richiesto quella specializzata filtrando per idnetwork                                
                CMSConfigs = BusinessLogic.ConfigsBusinessLogic.GetAllConfig(session, AvailableThemes);

                

                LoginSettings = LoginManager.BusinessLogic.LoginSettingsBL.GetAllSettings(session);

                IndexConfig = BusinessLogic.ConfigsBusinessLogic.GetIndexConfig(session); //FluentSessionProvider.Instance.OpenInnerSession()
                 
                //SOLR Startup - Sistemare meglio con la scansione di eventuali multicore - nel caso di più network attive
                if (IndexConfig != null && IndexConfig.Mode == IndexConfig.IndexMode.solr)
                {
                    SolrNet.Startup.Init<CmsDocument>(IndexConfig.SolrHostAddress + ":" + IndexConfig.SolrHostTcpPort.ToString() + "/solr/" + IndexConfig.SolrCore);                    
                }

            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante l'inizializzazione della configurazione del cms. " + ex.Message);
            }
            finally
            {
                session.Close();
            }

        }


       

    }
}
