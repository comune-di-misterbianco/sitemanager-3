﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CmsConfigs.Model
{
    public class IndexDocContent
    {
        public virtual int ID
        {
            get;
            set;
        }

        private Dictionary<string, object> _Fields;
        public virtual Dictionary<string, object> Fields
        {
            get
            {
                if (_Fields == null)
                    _Fields = new Dictionary<string, object>();
                return _Fields;
            }

        }


    }
}
