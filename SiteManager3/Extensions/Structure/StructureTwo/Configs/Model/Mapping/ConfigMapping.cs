﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace CmsConfigs.Model.Mapping
{
    public class ConfigMapping : ClassMap<Config>
    {
        public ConfigMapping()
        {
            Table("config_settings");
            Id(x => x.ID);
            Map(x => x.RifNetwork);
            Map(x => x.PortalName);
            Map(x => x.Layout); //TODO: da rimuovere
            Map(x => x.TopMenuState);
            Map(x => x.TopMenuStyle);
            Map(x => x.Theme);
            Map(x => x.SupportedAttachmentsExtensions);
            Map(x => x.CmsAppAttachToContentStatus);
        }
    }
}
