﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace CmsConfigs.Model.Mapping
{
    public class IndexConfigMapping : ClassMap<IndexConfig>
    {
        public IndexConfigMapping()
        {
            Table("config_index_settings");
            Id(x => x.IndexConfigKey).GeneratedBy.Assigned();
            Map(x => x.Mode);
            Map(x => x.SolrHostAddress);
            Map(x => x.SolrHostTcpPort);
            Map(x => x.LuceneSettingsFolderPath);
            Map(x => x.ZoomSettingsFolderPath);
            Map(x => x.SolrCore);
        }
    }
}
