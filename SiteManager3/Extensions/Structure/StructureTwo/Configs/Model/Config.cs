﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CmsConfigs.Model
{
    public class Config
    {
        public Config()
        {
           
        }

        /// <summary>
        /// Define key for config record
        /// </summary>
        public virtual int ID { get; set; }

        /// <summary>
        /// Define name for portal or sub-portal (network)
        /// </summary>
        public virtual string PortalName { get; set; }


        /// <summary>
        /// Define network reference - if 0 is relative for globally network
        /// </summary>
        public virtual int RifNetwork { get; set; }

        /// <summary>
        /// Define frontend layout type - standard responsive or design italia guide lines (Agid) for governative sites  (deprecato)
        /// </summary>
        public virtual LayoutType Layout { get; set; }


        public virtual string Theme { get; set; }

        /// <summary>
        /// Define current theme set 
        /// </summary>
        public virtual NetCms.Themes.Model.Theme CurrentTheme
        {
            get;
            set;
        }
        /// <summary>
        /// Define top menu state on - off
        /// </summary>
        public virtual State TopMenuState { get; set; }

        /// <summary>
        /// Define front top menu style
        /// </summary>
        public virtual MenuType TopMenuStyle { get; set; }


        /// <summary>
        /// Return relativa path for static .aspx and .aspx.cs files used as master.page
        /// </summary>
        public virtual string ModelsFolder
        {
            get
            {
                string modelsFolder = string.Empty;
                if (NetCms.Configurations.ThemeEngine.Status == NetCms.Configurations.ThemeEngine.ThemeEngineStatus.on)          
                    modelsFolder = CurrentTheme.ModelFolder;  //modelsFolder = "/" + Layout.ToString().ToLower();
                else
                    modelsFolder = "/models";

                return modelsFolder;
            }
        }

        /// <summary>
        /// Return folder that contains themes
        /// </summary>
        public virtual string ThemesFolder
        {
            get
            {
                return NetCms.Configurations.ThemeEngine.ThemesFolder;
            }
        }      

        public virtual LanguageManager.Model.Language CurrentLanguage
        {
            get;
            set;
        }

        public virtual string SupportedAttachmentsExtensions
        {
            get;
            set;
        }

        public virtual String[] AttachedEnabledExtensions 
        {
            get
            {
                String[] exts = new String[] { "doc", "docx", "pdf", "p7m", "rtf", "zip", "xlsx", "xls" };
                if (!string.IsNullOrEmpty(SupportedAttachmentsExtensions))
                    exts = SupportedAttachmentsExtensions.Split(';');
                return exts;
            }
        }

        public virtual AttachToContentStatus CmsAppAttachToContentStatus
        {
            get;
            set;
        }

        public enum AttachToContentStatus
        {
            Disabled,
            Enabled
        }

        public enum State{
            Off = 0,
            On = 1
        }

        public enum MenuType {
            Normal = 1,
            Megamenu = 2,
        }

        //TODO: da rimuovere
        /// <summary>
        /// Elenco dei template disponibili (deprecato)
        /// </summary>
        public enum LayoutType
        {
            Responsive = 1,
            AGID = 2,
            AGID2019 = 3,
        }
        public enum Column
        {
            ID,
            PortalName,
            RifNetwork,
            Layout,
            TopMenuState,
            TopMenuStyle,
            Theme,
            SupportedAttachmentsExtensions,
            CmsAppAttachToContentStatus
        }
    }
}
