﻿using System.Linq;
using NetCms.GUI;
using NetCms.GUI.Icons;
using System.Web.UI.WebControls;
using System.Web.UI;
using CmsConfigs.Model;
using CmsConfigs.Forms;
using System;

namespace NetCms.Structure.Applications.Configs
{
    public class IndexConfigPage : SmPageVertical 
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.HomeSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_Configs"; }
        }

        public override Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Plugin; }
        }

        public override string PageTitle
        {
            get { return "Configurazione motore di ricerca"; }
        }

        public IndexConfigPage() 
        {

            this.Toolbar.Buttons.Add(new ToolbarButton("cms-search-engine.aspx", Icons.Database_Refresh, "Indicizzazione contenuti"));
            this.Toolbar.Buttons.Add(new ToolbarButton("generic.aspx", Icons.Arrow_Left, "Torna alla pagina di Configurazione"));

            currentConfig = CmsConfigs.BusinessLogic.ConfigsBusinessLogic.GetIndexConfig();
        }

        private IndexConfig currentConfig;

        private ContentIndexForm _indexconfigform;
        public ContentIndexForm IndexFormConfig
        {
            get
            {
                if (_indexconfigform == null)
                {
                    _indexconfigform = new ContentIndexForm("indexconfigform", currentConfig);
                    _indexconfigform.ShowMandatoryInfo = false;
                    _indexconfigform.CssClass = "Axf";
                    _indexconfigform.SubmitButton.Text = "Salva";
                    _indexconfigform.SubmitButton.Click += SubmitButton_Click;
                }
                return _indexconfigform;
            }
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (IndexFormConfig.IsValid == NetService.Utility.ValidatedFields.VfGeneric.ValidationStates.Valid)
            {
                bool status = CmsConfigs.BusinessLogic.ConfigsBusinessLogic.SaveIndexConfig(this.IndexFormConfig);
                if (status)
                    NetCms.GUI.PopupBox.AddSessionMessage("Configurazione salvata con successo.", PostBackMessagesType.Success);
                else
                    NetCms.GUI.PopupBox.AddSessionMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);
            }
        }

        protected override System.Web.UI.WebControls.WebControl RootControl
        {
            get { 
                WebControl control = new WebControl(HtmlTextWriterTag.Div);

                WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
                fieldset.Controls.Add(new LiteralControl("<legend>Configurazione motore di ricerca</legend>"));
                fieldset.Controls.Add(IndexFormConfig);
                control.Controls.Add(fieldset);


                return control;
            }
        }
    }
}