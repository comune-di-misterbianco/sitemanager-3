﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NetCms.Themes.Model
{
    public class GlobalJS
    {
        public static string Pattern = "<script src=\"{0}\"></script>" + System.Environment.NewLine;

        public GlobalJS(List<string> dependences, List<string> local)
        {
            Dependences = dependences;
            Local = local;
        }

        public List<string> Dependences
        {
            get;
            set;
        }

        public List<string> Local
        {
            get;
            set;
        }

        public List<string> AllJS(Theme theme)
        {
            List<string> local = (from string js in Local
                                  select theme.JsFolder + js).ToList();

            return Dependences.Union(local).ToList();
            
        }


    }
}
