﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NetCms.Themes.Model
{
    public class GlobalCss
    {
        public static string Pattern = "<link href=\"{0}\" rel=\"{1}\" {2} />" + System.Environment.NewLine;
        //public GlobalCss(List<Css> dependences, List<Css> local)
        //{
        //    Dependences = dependences;
        //    Local = local;
        //}


        public List<Css> Dependences
        {
            get;
            set;
        }

        public List<Css> Local
        {
            get;
            set;
        }       

        public List<Css> AllCss(Theme theme)
        {
            List<Css> local = (from Css css in Local
            select new Css(theme.CssFolder + css.Path, css.Media, css.Rel)).ToList();
            var cssTheme = Dependences.Union(local).ToList();

            return Dependences.Union(local).ToList();            
        }
    }
}
