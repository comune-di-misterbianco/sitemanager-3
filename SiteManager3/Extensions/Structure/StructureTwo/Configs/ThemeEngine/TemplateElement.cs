﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCms.Themes.Model
{
    public class TemplateElement
    {
        public TemplateElement(string filename, string label, string description, string preview, TplTypes type)
        {
            Filename = filename;
            Label = label;
            Description = description;
            Preview = preview;
            Type = type;
        }

        public string Filename { get; set; }
        public string Label { get; set; }

        public string Description { get; set; }

        public string Preview { get; set; }

        [JsonIgnore]
        public string AssociatedThemeName { get; set; }

        public TplTypes Type
        {
            get; set;
        }

        public enum TplTypes
        {
            modulo,
            moduloapplicativo,
            content,
            contentapplicativo,
            head,
            system,
            systemmodulo,
        }

        public bool CheckType(List<TplTypes> tplTypes)
        {
          return tplTypes.Exists(x => x == this.Type);
        }

        public ContentTypeRepoEnum ContentTypeRepo
        {
            get
            {
                ContentTypeRepoEnum contentTypeRepo = ContentTypeRepoEnum.commons;
                switch (Type)
                {
                    case TplTypes.modulo:
                        contentTypeRepo = ContentTypeRepoEnum.homepage;        
                        break;
                    case TplTypes.moduloapplicativo:
                        contentTypeRepo = ContentTypeRepoEnum.homepage;
                        break;
                    case TplTypes.content:
                        contentTypeRepo = ContentTypeRepoEnum.content;
                        break;
                    case TplTypes.contentapplicativo:
                        contentTypeRepo = ContentTypeRepoEnum.content;
                        break;
                    case TplTypes.head:
                        contentTypeRepo = ContentTypeRepoEnum.commons;
                        break;
                    case TplTypes.system:
                        contentTypeRepo = ContentTypeRepoEnum.commons;
                        break;                    
                }
                return contentTypeRepo;
            }
        }


        public enum  ContentTypeRepoEnum
        {
            commons,
            content,
            homepage
        }
    }
}
