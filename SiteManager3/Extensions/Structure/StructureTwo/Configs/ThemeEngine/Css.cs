﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCms.Themes.Model
{
    public class Css
    {
        public Css(string path, string media, string rel)
        {
            Path = path;
            Media = media;
            Rel = rel;
        }

        public string Path { get; set; }
        public string Media { get; set; }
        public string Rel { get; set; }


    }
}
