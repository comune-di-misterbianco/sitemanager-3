﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Web;
using NetCms.Themes.Model;

namespace NetCms.Themes
{
    public static class Parser
    {
  
       public static List<Theme> GetAllTheme(string themesFolder)
       {
            List<Theme> themes = new List<Theme>();
            try
            {
                themesFolder = HttpContext.Current.Server.MapPath(themesFolder) + "\\";
                
                // recupero l'elenco delle cartelle presenti nella cartella themesFolder
                List<string> themeFolders = Directory.EnumerateDirectories(themesFolder).ToList();

                // iterazione di ogni cartella alla ricerca del info.json
                foreach (string themefolder in themeFolders)
                {                   
                    if (CheckFile(themefolder))
                    {
                        Theme currentTheme = new Theme();
                        currentTheme = currentTheme.Load(themefolder);
                        themes.Add(currentTheme);
                    }
                }
                //AvailableThemes = new List<Theme>();
                AvailableThemes = themes;
            }
            catch(Exception ex)
            {
                // cartella themes non trovata
                Diagnostics.Diagnostics.TraceException(ex, "", "", "TemplateParser");
            }
            return themes;
        }


        public static List<Theme> AvailableThemes
        {
            get;
            set;
        }

        public static Theme SystemTheme
        {
            get
            {
                return AvailableThemes.Where(x => x.Name == "System").FirstOrDefault();
            }
        }


        //TODO: verifica le dipendenze mappate negli assets
        public static void CheckAssets(string themefolder)
       {

       }

        public static bool CheckFile(string themefolder)
        {
           // verificare syntassi
           return File.Exists(themefolder + "\\" + Theme.jsonFilename);
        }

        //public static Theme ReadInfoYaml(string yamlfile)
        //{
        //    Theme theme = new Theme();

        //    try
        //    {
        //        var input = new StringReader(yamlfile);

        //        var yaml = new YamlStream();
        //        yaml.Load(input);

        //        var mapping = (YamlMappingNode)yaml.Documents[0].RootNode;

        //        foreach(var entry in mapping.Children)
        //        {
        //          var e = ((YamlMappingNode)entry.Key).;
        //        }

        //        // List all the items
        //        var items = (YamlSequenceNode)mapping.Children[new YamlScalarNode("items")];
        //        foreach (YamlMappingNode item in items)
        //        {
        //           var aaa= (
        //                "{0}\t{1}",
        //                item.Children[new YamlScalarNode("part_no")],
        //                item.Children[new YamlScalarNode("descrip")]
        //            );
        //        }

        //    }
        //    catch(Exception ex)
        //    {

        //    }

        //    return theme;
        //}



        
    }
}
