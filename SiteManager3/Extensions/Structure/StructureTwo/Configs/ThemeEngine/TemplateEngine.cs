﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using NetCms.Diagnostics;

namespace NetCms.Themes
{
    public class TemplateEngine
    {
 
        /// <summary>
        /// Renderizza il tpl in funzione dei parametri passati usato generalmente per i moduli
        /// </summary>
        /// <param name="data"></param>
        /// <param name="tplPath">Persorco assoluto del path del tpl</param>
        /// <param name="labels">Dizionario delle </param>
        /// <returns></returns>
        public static string RenderTpl(Object data, string tplPath, Object labels, Object CommonTemplateLabels = null)
        {
            string source = string.Empty;

            var template = Scriban.Template.Parse(System.IO.File.ReadAllText(tplPath), tplPath);
            try
            {
                if (template.HasErrors)
                {
                    if (NetCms.Configurations.Debug.DebugLoginEnabled)
                        foreach (var error in template.Messages)
                            source += error.Message + System.Environment.NewLine;
                    else
                        foreach (var error in template.Messages)
                            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, error.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                }
                else
                {
                  
                    var context = new Scriban.TemplateContext();
                    context.PushCulture(System.Globalization.CultureInfo.CurrentCulture);

                    var scriptObj = new Scriban.Runtime.ScriptObject();
                    scriptObj.Add("data", data);                    
                    scriptObj.Add("labels", labels);
                    if (CommonTemplateLabels != null)
                    scriptObj.Add("commonlabels", CommonTemplateLabels);

                    context.PushGlobal(scriptObj);

                    source = template.Render(context);
                }
            }
            catch (Exception ex)
            {
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Template parsing error", "", "", "", ex);
            }

            if (NetCms.Configurations.Debug.GenericEnabled)
            {
                source += "<p class=\"debug\"><small>" + tplPath + "</small></p>";
            }

            return source;     
        }
        
        // usato per webdocs 
        public static string RenderTpl(Object sezione, string srcsharecontent, string tplPath, Object TemplateLabels, Object CommonTemplateLabels)
        {
            string source = string.Empty;

            try
            {
                var template = Scriban.Template.Parse(System.IO.File.ReadAllText(tplPath), tplPath);
                if (template.HasErrors)
                {
                    if (NetCms.Configurations.Debug.DebugLoginEnabled)
                        foreach (var error in template.Messages)
                            source += error.Message + System.Environment.NewLine;
                    else
                        foreach (var error in template.Messages)
                            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, error.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                }
                else
                {
                    var context = new Scriban.TemplateContext();
                    context.PushCulture(System.Globalization.CultureInfo.CurrentCulture);

                    var scriptObj = new Scriban.Runtime.ScriptObject();
                    scriptObj.Add("sezione", sezione);
                    scriptObj.Add("sharecontent", srcsharecontent);
                    scriptObj.Add("labels", TemplateLabels);
                    scriptObj.Add("commonlabels", CommonTemplateLabels);

                    context.PushGlobal(scriptObj);

                    source = template.Render(context);                  
                }
            }
            catch (Exception ex)
            {
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Template parsing error", "", "", "", ex);
            }

            if (NetCms.Configurations.Debug.GenericEnabled)
            {
                source += "<p class=\"debug\"><small>" + tplPath + "</small></p>";
            }

            return source;
        }
    
        // usato per applicazioni 
        public static string RenderTpl(Object documents, Object sezione,  string tplPath, Object TemplateLabels, Object CommonTemplateLabels, Object paging = null , Object formvalue = null, Object options = null)
        {
            string source = string.Empty;

            var template = Scriban.Template.Parse(System.IO.File.ReadAllText(tplPath), tplPath);
            try
            {
                if (template.HasErrors)
                {
                    if (NetCms.Configurations.Debug.DebugLoginEnabled)
                        foreach (var error in template.Messages)
                            source += error.Message + System.Environment.NewLine;
                    else
                        foreach (var error in template.Messages)
                            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, error.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                }
                else
                {
                    var context = new Scriban.TemplateContext();
                    context.PushCulture(System.Globalization.CultureInfo.CurrentCulture);

                    var scriptObj = new Scriban.Runtime.ScriptObject();
                    scriptObj.Add("documents", documents);                    
                    scriptObj.Add("sezione", sezione);
                    scriptObj.Add("labels", TemplateLabels);
                    scriptObj.Add("commonlabels", CommonTemplateLabels);
                    
                    if (paging != null)
                        scriptObj.Add("pager", paging);
                    
                    if (formvalue != null)
                        scriptObj.Add("form", formvalue);

                    if (options != null)
                        scriptObj.Add("options", options);

                    context.PushGlobal(scriptObj);

                    source = template.Render(context);
                    
                }
            }
            catch (Exception ex)
            {
               Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Template parsing error", "", "", "", ex);
            }

            if (NetCms.Configurations.Debug.GenericEnabled)
            {
                source += "<p class=\"debug\"><small>" + tplPath + "</small></p>";
            }

            return source;
        }

        // usato per il content detail
        public static string RenderTpl(Object document, Object sezione, string srcsharecontent, string tplPath, Object TemplateLabels, Object CommonTemplateLabels, Object options = null)
        {
            string source = string.Empty;
            var template = Scriban.Template.Parse(System.IO.File.ReadAllText(tplPath), tplPath);
            try
            {
                if (template.HasErrors)
                {
                    if (NetCms.Configurations.Debug.DebugLoginEnabled)
                        foreach (var error in template.Messages)
                            source += error.Message + System.Environment.NewLine;
                    else
                        foreach (var error in template.Messages)
                            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, error.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                }
                else
                {
                    var context = new Scriban.TemplateContext();
                    context.PushCulture(System.Globalization.CultureInfo.CurrentCulture);

                    var scriptObj = new Scriban.Runtime.ScriptObject();
                    scriptObj.Add("document", document);
                    scriptObj.Add("sezione", sezione);
                    scriptObj.Add("sharecontent", srcsharecontent);
                    scriptObj.Add("labels", TemplateLabels);
                    scriptObj.Add("commonlabels", CommonTemplateLabels);
                    if (options != null)
                        scriptObj.Add("options", options);

                    context.PushGlobal(scriptObj);

                    source = template.Render(context);                   
                }
            }
            catch (Exception ex)
            {
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Template parsing error", "", "", "", ex);
            }

            if (NetCms.Configurations.Debug.GenericEnabled)
            {
                source += "<p class=\"debug\"><small>" + tplPath + "</small></p>";
            }

            return source;
        }
    }
}
