﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

namespace NetCms.Themes.Model
{
    public class Theme
    {
        public const string jsonFilename = "info.json";

        public string Name { get; set; }

        public string Description { get; set; }

        public string Favicon { get; set; }

        public string Logo { get; set; }

        public string Screenshot { get; set; }

        public string Version { get; set; }

        public string InheritFrom { get; set; }
      
        /// <summary>
        /// Framework web in uso dal tema ("bootstrap4","bootstrap3","")
        /// </summary>
        public string Framework { get; set; }

        [JsonIgnore]
        public Theme ParentTheme {
            get
            {
                Theme parentTheme = null;
                if (!string.IsNullOrEmpty(this.InheritFrom))
                {
                    parentTheme = Parser.AvailableThemes.Where(x => x.Name == this.InheritFrom).FirstOrDefault();
                }
                return parentTheme;
            }
        }

        public Assets Assets { get; set; }



        public Theme() { }

        public Theme(string name, string description, string favicon, string logo, string screenshot, string version, Assets assets)
        {
            Name = name;
            Description = description;
            Favicon = favicon;
            Logo = logo;
            Screenshot = screenshot;
            Version = version;
           
            Assets = assets;           
        }

        /// <summary>
        /// Salva le informazioni del tema nel file json
        /// </summary>
        /// <param name="themefolder">Percorso assoluto della cartella del tema</param>
        public void Save(string themefolder)
        {
            string filejson = themefolder + jsonFilename;

            var json = JsonConvert.SerializeObject(this,Formatting.Indented);

            using(var fs = new System.IO.StreamWriter(filejson,true))
            {
                fs.WriteLine(json.ToString());
                fs.Close();
            }
        }


        /// <summary>
        /// Legge le informazioni del tema dal file json
        /// </summary>
        /// <param name="themefolder">Percorso assoluto della cartella del tema</param>
        /// <returns>L'istanza dell'oggetto theme valorizzato con i dati presenti nel file json</returns>
        public Theme Load(string themefolder)
        {
            string filejson = themefolder + "\\" + jsonFilename;
            string jsonStream = System.IO.File.ReadAllText(filejson);
            return JsonConvert.DeserializeObject<Theme>(jsonStream);
        }

        [JsonIgnore]
        public string RelativeFolder
        {
            get { return NetCms.Configurations.ThemeEngine.ThemesFolder + "/" + Name.ToLower();  }
        }

        [JsonIgnore]
        public string CssFolder
        {
            get { return RelativeFolder + "/assets/css/"; }
        }

        [JsonIgnore]
        public string JsFolder
        {
            get { return RelativeFolder + "/assets/js/"; }
        }

        [JsonIgnore]
        public string TemplateFolder
        {
            get 
            {
                if (TemplateFolderExist)
                    return RelativeFolder + "/templates/";
                else if (ParentTheme != null)  //TODO: attenzione se esiste un override di un .tpl va gestito il caso specifico e caricato dalla cartella locale
                    return ParentTheme.TemplateFolder;
                else
                {
                    // TODO: logica per utilizzare la cartella del template system
                    Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Current theme " + this.Name + " not contains template folder with tpl files.");
                    return Parser.SystemTheme.TemplateFolder;
                }
            }
        }

        [JsonIgnore]
        public string ModelFolder
        {
            get {
                if (ModelsFolderExist)
                    return RelativeFolder + "/models/";
                else if (ParentTheme != null)
                    return ParentTheme.ModelFolder;
                else
                    return Parser.SystemTheme.ModelFolder;
            }
        }

        [JsonIgnore]
        public bool ModelsFolderExist
        {
            get
            {
                string tmpFolder = RelativeFolder + "/models/";
                tmpFolder = System.Web.HttpContext.Current.Server.MapPath(tmpFolder);
                return System.IO.Directory.Exists(tmpFolder);
            }
        }

        [JsonIgnore]
        public bool TemplateFolderExist
        {
            get
            {
                string tmpFolder = RelativeFolder + "/templates/";
                tmpFolder = System.Web.HttpContext.Current.Server.MapPath(tmpFolder);
                return System.IO.Directory.Exists(tmpFolder);
            }
        }

        public string GetJSToHtml()
        {
            string src = string.Empty;
            List<string> parentThemeJS = null;

            if (ParentTheme != null)
            {
                parentThemeJS = ParentTheme.Assets.JS.AllJS(ParentTheme);
            }

            //List<string> local = (from string js in Assets.JS.Local
            //                      select JsFolder + js).ToList();

            //var jsTheme = Assets.JS.Dependences.Union(local).ToList();
            var jsTheme = Assets.JS.AllJS(this);
            var alljs = (parentThemeJS!= null) ? parentThemeJS.Union(jsTheme).ToList(): jsTheme;

            alljs.ForEach(s => src += string.Format(GlobalJS.Pattern, s));

            return src;
        }


        public string GetCssInHtml()
        {
            string src = string.Empty;
            List<Css> parentThemeCss = null;

            if (ParentTheme != null)
            {
                parentThemeCss = ParentTheme.Assets.Css.AllCss(ParentTheme);
            }

            //List<Css> local = (from Css css in Assets.Css.Local
            //                   select new Css(CssFolder + css.Path, css.Media, css.Rel)).ToList(); 

            //var cssTheme = Assets.Css.Dependences.Union(local).ToList();

            var cssTheme = Assets.Css.AllCss(this);
            
            var allCss = (parentThemeCss != null) ? parentThemeCss.Union(cssTheme).ToList(): cssTheme;
            

            allCss.ForEach(s => src += string.Format(GlobalCss.Pattern, s.Path, s.Rel, !string.IsNullOrEmpty(s.Media) ? "media=\"" + s.Media + "\"": ""));

            return src;
        }

        public List<TemplateElement> GetTemplateElements(TemplateElement.TplTypes type, bool onlyBase = false)
        {            
            if (onlyBase)
            {               
                return Parser.SystemTheme.Assets.TemplateElemens.Where(x => x.Type == type).ToList();
            }
            else
            {
                List<TemplateElement> parentThemeElements = new List<TemplateElement>();                

                List<TemplateElement> allThemeElements = new List<TemplateElement>();

                allThemeElements = Parser.SystemTheme.Assets.TemplateElemens;

                if (ParentTheme!= null)
                {
                    parentThemeElements = ParentTheme.Assets.TemplateElemens;
                    allThemeElements = allThemeElements.Union(parentThemeElements).ToList();
                }

                allThemeElements = allThemeElements.Union(this.Assets.TemplateElemens).ToList();

                return allThemeElements.Where(x => x.Type == type).ToList();
            }
        }
       
        public string GetTplPathByLabel(string label, List<TemplateElement.TplTypes> tplTypes, string applicationName = null)
        {
            string filename = string.Empty;
            string path = string.Empty;
            string tplPath = string.Empty;
            
            List<TemplateElement> parentThemeElements = new List<TemplateElement>();
            List<TemplateElement> localThemeElements = new List<TemplateElement>();

            List<TemplateElement> allThemeElements = new List<TemplateElement>();
            try
            {
                allThemeElements = Parser.SystemTheme.Assets.TemplateElemens.Select(c => { c.AssociatedThemeName = Parser.SystemTheme.Name; return c; }).ToList();//.Union(TemplateElemens);

                if (ParentTheme != null)
                {
                    parentThemeElements = ParentTheme.Assets.TemplateElemens.Select(c => { c.AssociatedThemeName = ParentTheme.Name; return c; }).ToList();
                    allThemeElements = allThemeElements.Union(parentThemeElements).ToList();
                }

                localThemeElements = this.Assets.TemplateElemens.Select(c => { c.AssociatedThemeName = this.Name; return c; }).ToList();

                allThemeElements = allThemeElements.Union(localThemeElements).ToList();

                TemplateElement te = allThemeElements.Where(x => x.Label.ToLower() == label.ToLower() && x.CheckType(tplTypes)).LastOrDefault();

                if (te != null)
                {
                    // themefolder
                    string themefolder = NetCms.Configurations.ThemeEngine.ThemesFolder + "/" + te.AssociatedThemeName;

                    // tpl filename
                    filename = te.Filename;

                    // content type of tpl
                    if (te.ContentTypeRepo == TemplateElement.ContentTypeRepoEnum.homepage)
                        path = "/content/homepage/";
                    else if (te.ContentTypeRepo == TemplateElement.ContentTypeRepoEnum.commons)
                        path = "/" + te.ContentTypeRepo.ToString() + "/";
                    else
                        path = "/" + te.ContentTypeRepo.ToString() + "/" + applicationName + "/";

                    tplPath = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + themefolder + "/templates/" + path + filename);
                }
                else
                {                   
                    if (tplTypes.Contains(TemplateElement.TplTypes.moduloapplicativo))
                        tplPath = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + NetCms.Configurations.ThemeEngine.ThemesFolder + "/system/templates/content/homepage/modulo-app-list.tpl");

                    Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Il template con etichetta " + label + " non è stato trovato nella collezione TemplateElemens allTplElement;");
                }
            }
            catch(Exception ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", this.GetType());
            }
            return tplPath;
        }
    
        /// <summary>
        /// Restituisce un elenco di viste disponibili
        /// </summary>
        /// <param name="tplTypes">Tipologie di template da restituire</param>
        /// <param name="applicationName">Eventuale nome applicazione (non implementato)</param>
        /// <returns>Elenco in formato List<string></returns>
        public List<string> GetTplElement(List<TemplateElement.TplTypes> tplTypes, string applicationName = null)
        {
            List<TemplateElement> parentThemeElements = new List<TemplateElement>();
            List<TemplateElement> localThemeElements = new List<TemplateElement>();

            List<TemplateElement> allThemeElements = new List<TemplateElement>();

            List<string> tplElements = new List<string>();
            tplElements.Add("No_Template");

            try
            {
                allThemeElements = Parser.SystemTheme.Assets.TemplateElemens.Select(c => { c.AssociatedThemeName = Parser.SystemTheme.Name; return c; }).ToList();
                
                if (ParentTheme != null)
                {
                    parentThemeElements = ParentTheme.Assets.TemplateElemens.Select(c => { c.AssociatedThemeName = ParentTheme.Name; return c; }).ToList();
                    allThemeElements = allThemeElements.Union(parentThemeElements).ToList();
                }

                localThemeElements = this.Assets.TemplateElemens.Select(c => { c.AssociatedThemeName = this.Name; return c; }).ToList();

                allThemeElements = allThemeElements.Union(localThemeElements).ToList();

                List<TemplateElement> tpls = allThemeElements.Where(x => x.CheckType(tplTypes)).ToList();

                if (tpls != null)
                {
                    List<string> elements = (from tpl in tpls
                                                      orderby tpl.Label ascending
                                                      select tpl.Label).Distinct().ToList();
                    tplElements.AddRange(elements);
                }               
            }
            catch (Exception ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", this.GetType());
            }

            return tplElements;
        }
    }
}