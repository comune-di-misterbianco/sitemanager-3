﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static NetCms.Themes.Model.TemplateElement;

namespace NetCms.Themes.Model
{
    public class Assets
    {

        public Assets(GlobalCss css, GlobalJS js)
        {
            Css = css;
            JS = js;            
        }

        public GlobalCss Css
        {
            get;
            set;
        }

        public GlobalJS JS
        {
            get;
            set;
        }
      
        //private List<TemplateElement> _BaseTemplateElemens;
        //[JsonIgnore]
        //private List<TemplateElement> BaseTemplateElemens
        //{
        //    get
        //    {
        //        if (_BaseTemplateElemens == null)
        //        {
        //            _BaseTemplateElemens = new List<TemplateElement>();
        //            // commons
        //            _BaseTemplateElemens.Add(new TemplateElement("error-body.tpl", "Error Body", "Corpo pagina di errore", "", TemplateElement.TplTypes.system));
        //            _BaseTemplateElemens.Add(new TemplateElement("lang-head-selector.tpl", "Lang Head Block", "Blocco per la selezione delle lingue in head", "", TemplateElement.TplTypes.system));
        //            _BaseTemplateElemens.Add(new TemplateElement("opengraph.tpl", "Open Graph", "Blocco di codice per l'esposizione dei opengraph", "", TemplateElement.TplTypes.head));
        //            _BaseTemplateElemens.Add(new TemplateElement("share-content.tpl", "Share content", "Blocco social share esposto nelle sezioni di dettaglio", "", TemplateElement.TplTypes.system));
        //            _BaseTemplateElemens.Add(new TemplateElement("topmenu.tpl", "Barra menu", "Barra menu denominata topmenu", "", TemplateElement.TplTypes.system));

        //            // homepage base                    
        //            _BaseTemplateElemens.Add(new TemplateElement("modulo-simple-box.tpl", "Standard", "Blocco di codice per il modulo statico", "", TemplateElement.TplTypes.modulo));
                   
        //            // homepage app
        //            _BaseTemplateElemens.Add(new TemplateElement("modulo-app-list.tpl", "Lista", "Blocco di codice per esporre il contenuto in modalità lista", "", TemplateElement.TplTypes.moduloapplicativo));
        //            _BaseTemplateElemens.Add(new TemplateElement("modulo-app-carousel.tpl", "Carousel", "Blocco di codice per esporre il contenuto in modalità carousel", "", TemplateElement.TplTypes.moduloapplicativo));
        //            _BaseTemplateElemens.Add(new TemplateElement("modulo-app-gallery.tpl", "Gallery", "Blocco di codice per esporre il contenuto in modalità carousel", "", TemplateElement.TplTypes.moduloapplicativo));

        //            // content
        //            _BaseTemplateElemens.Add(new TemplateElement("simple.tpl", "Simple", "Template per la modalità Nome e descrizione", "", TemplateElement.TplTypes.content));
        //            _BaseTemplateElemens.Add(new TemplateElement("list.tpl", "List", "Template per la modalità Indice contenuti", "", TemplateElement.TplTypes.content));
        //            _BaseTemplateElemens.Add(new TemplateElement("custom.tpl", "Custom", "Template per la modalità Pagina personalizzata", "", TemplateElement.TplTypes.content));
        //            _BaseTemplateElemens.Add(new TemplateElement("detail.tpl", "Dettaglio", "Template per l'esposizione del dettaglio", "", TemplateElement.TplTypes.system)); // TODO : verificare se introdurre altra tipologia

        //            //_BaseTemplateElemens.Add(new TemplateElement("landing.tpl", "Tabella", "Template per l'esposizione del dettaglio", "", TemplateElement.TplTypes.content));
        //            // content applicativo
        //            _BaseTemplateElemens.Add(new TemplateElement("list.tpl", "Lista", "Template per l'esposizione dei record applicativi in modalità lista", "", TemplateElement.TplTypes.contentapplicativo));
        //            _BaseTemplateElemens.Add(new TemplateElement("table.tpl", "Tabella", "Template per l'esposizione dei record applicativi in modalità tabellare", "", TemplateElement.TplTypes.contentapplicativo));
        //            _BaseTemplateElemens.Add(new TemplateElement("detail.tpl", "Dettaglio", "Template per l'esposizione del dettaglio", "", TemplateElement.TplTypes.system));// TODO : verificare se introdurre altra tipologia
        //        }
        //        return _BaseTemplateElemens;
        //    }
        //}


        // template aggiuntivi e implementati nel tema
        
        //private List<TemplateElement> _TemplateElemens;
      
        public List<TemplateElement> TemplateElemens
        {
            get;
            set;
        }

        
        //public List<TemplateElement> GetTemplateByType(TemplateElement.TplTypes type, bool onlyBase = false)
        //{
        //    if (!onlyBase)
        //        return BaseTemplateElemens.Where(x => x.Type == type).ToList();
        //    else
        //    {
        //        var tmp = BaseTemplateElemens.Union(TemplateElemens);
        //        return tmp.Where(x => x.Type == type).ToList();
        //    }
        //}
               
        ///// <summary>
        ///// Restituisce il nomefile del tpl 
        ///// </summary>
        ///// <param name="label">Valore registrata su db</param>
        ///// <returns>Filename del tpl</returns>
        //public string GetTplFileNameByLabel(string label, List<TemplateElement.TplTypes> tplTypes)
        //{
        //    string filename = string.Empty;

        //    var tmp = Parser.SystemTheme.Assets.TemplateElemens.Union(TemplateElemens);
            
        //    TemplateElement te = tmp.Where(x => x.Label == label && x.CheckType(tplTypes)).FirstOrDefault();

        //    if (te != null)
        //        filename = te.Filename;
        //    else
        //        filename = "modulo-app-list-simple.tpl";       // necessario per retrocompatibilità con i moduli applicativi
            
        //    return filename;
        //}     

    }


  
}

