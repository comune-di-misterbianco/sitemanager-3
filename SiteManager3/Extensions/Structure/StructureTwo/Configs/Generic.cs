﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetCms.Vertical;
using NetCms.Installation;
using NetCms.GUI.Icons;
using NetService.Utility.ArTable;
using System.Linq;
using CmsConfigs.BusinessLogic;
using CmsConfigs.Model;
using System.Collections.Generic;

namespace NetCms.Structure.Applications.Configs
{
    public class Generic : SmPageVertical
    {

        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.HomeSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_Configs"; }
        }

        public override Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Plugin; }
        }

        public override string PageTitle
        {
            get { return "Impostazioni generali"; }
        }

        public Generic()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("generic.aspx", Icons.Application_Edit, "Impostazioni generali"));
            this.Toolbar.Buttons.Add(new ToolbarButton("loginmanager.aspx", Icons.Application_Key, "Impostazioni di autenticazione"));
            this.Toolbar.Buttons.Add(new ToolbarButton("languages.aspx", Icons.Rainbow, "Località e lingue"));
            this.Toolbar.Buttons.Add(new ToolbarButton("index-config.aspx", Icons.Database_Refresh, "Indicizzazione contenuti"));
            this.Toolbar.Buttons.Add(new ToolbarButton("seo-meta.aspx", Icons.Server_Chart, "SEO e Meta"));
            this.Toolbar.Buttons.Add(new ToolbarButton("social.aspx", Icons.Image_Link, "Social"));
        }

        protected override WebControl RootControl
        {
            get
            {
                WebControl control = new WebControl(HtmlTextWriterTag.Div);

                WebControl sectionTitle = new WebControl(HtmlTextWriterTag.Div);
                sectionTitle.CssClass = "section_title";
                sectionTitle.Controls.Add(new LiteralControl("<h1>Impostazioni generali del cms.</h1>"));
                control.Controls.Add(sectionTitle);


                WebControl divButton = new WebControl(HtmlTextWriterTag.Div);
                divButton.CssClass = "content_right";

                Button btnReloadCfg = new Button();
                btnReloadCfg.ID = "reloadCfgs";
                btnReloadCfg.Text = "Ricarica configurazioni";
                btnReloadCfg.Click += BtnReloadCfg_Click;
                divButton.Controls.Add(btnReloadCfg);

                control.Controls.Add(divButton);

                WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
                fieldset.Controls.Add(new LiteralControl("<legend>Impostazioni generali del cms.</legend>"));
                fieldset.Controls.Add(new LiteralControl("<p class=\"action big\"><a href=\"setconfig.aspx?key=0\">Imposta la configurazione generale</a><p>"));
                control.Controls.Add(fieldset);
                control.Controls.Add(NetworkTable());

                return control;
            }
        }

        private void BtnReloadCfg_Click(object sender, EventArgs e)
        {

            // Inizializzazione della configurazione del cms
            CmsConfigs.Model.GlobalConfig.Init();

            // Pulizia della informazioni presenti nella cache
            G2Core.Caching.Cache Cache = new G2Core.Caching.Cache();
            Cache.RemoveAll("");

            NetCms.GUI.PopupBox.AddSessionMessage("Configurazione ricaricata con successo", NetCms.GUI.PostBackMessagesType.Success);
        }

        public WebControl NetworkTable()
        {
            ArTable table = new ArTable(NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.Select(x => x.Value));
            table.NoRecordMsg = "Non ci sono reti installate";
            table.EnablePagination = false;
            table.InnerTableCssClass = "tab";

            table.AddColumn(new ArTextColumn("Nome", "Label"));
            table.AddColumn(new ArTextColumn("Codice", "SystemName"));
            table.AddColumn(new ArOptionsColumn("Stato", "Enabled", new string[] { "Disablilitato", "Abilitata" }));
            table.AddColumn(new ArActionColumn("Imposta", ArActionColumn.Icons.Details, "setconfig.aspx?key={ID}"));
            table.AddColumn(new ArConditionedActionColumn("Rimuovi impostazioni", ArConditionedActionColumn.Icons.Delete, "?action=delete&key={ID}", new Predicate<object>(ExistConfig), "Nessun settaggio", "Elimina")); 

            return table;
        }

        private bool ExistConfig(object obj)
        {
            bool esito = false;
            Networks.BasicNetwork currentNetwork = null;

            if (obj.GetType() == typeof(Networks.BasicNetwork))
                currentNetwork = (Networks.BasicNetwork)obj;

            Config currentConfig = ConfigsBusinessLogic.GetConfigByIdNetwork(currentNetwork.ID, null, true);
            if (currentConfig != null)
                esito = true;

            return esito;
        }
    }
}
