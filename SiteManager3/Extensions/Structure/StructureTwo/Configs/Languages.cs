﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetCms.Vertical;
using NetCms.Installation;
using NetCms.GUI.Icons;
using NetService.Utility.ArTable;

namespace NetCms.Structure.Applications.Configs
{
    public class Languages : SmPageVertical
    {

        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.HomeSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_Configs"; }
        }

        public override Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Plugin; }
        }

        public override string PageTitle
        {
            get { return "Località e lingue"; }
        }

        public Languages()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("generic.aspx", Icons.Application_Edit, "Impostazioni generali"));
            this.Toolbar.Buttons.Add(new ToolbarButton("languages.aspx", Icons.Rainbow, "Località e lingue"));
            this.Toolbar.Buttons.Add(new ToolbarButton("index-config.aspx", Icons.Database_Refresh, "Indicizzazione contenuti"));
            this.Toolbar.Buttons.Add(new ToolbarButton("seo-meta.aspx", Icons.Server_Chart, "SEO e Meta"));
            this.Toolbar.Buttons.Add(new ToolbarButton("social.aspx", Icons.Image_Link, "Social"));
        }

        protected override WebControl RootControl
        {
            get
            {
                WebControl control = new WebControl(HtmlTextWriterTag.Div);

                WebControl sectionTitle = new WebControl(HtmlTextWriterTag.Div);
                sectionTitle.CssClass = "section_title";
                sectionTitle.Controls.Add(new LiteralControl("<h1>Sezione per la configurazione delle lingue del cms.</h1>"));
                control.Controls.Add(sectionTitle);

                control.Controls.Add(Langs());
                return control;
            }
        }

        public WebControl Langs()
        {


            ArTable table = new ArTable();
            table.NoRecordMsg = "Nessun record disponibile";
            table.EnablePagination = false;
            table.InnerTableCssClass = "tab";
            table.Records = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetLanguages();

            table.AddColumn(new ArTextColumn("Nome", LanguageManager.Model.Language.Columns.Nome_Lang.ToString()));
            table.AddColumn(new ArTextColumn("Culture Code", LanguageManager.Model.Language.Columns.CultureCode_Lang.ToString()));
            table.AddColumn(new ArTextColumn("Two letter Code", LanguageManager.Model.Language.Columns.LangTwoLetterCode.ToString()));
            table.AddColumn(new ArOptionsColumn("Stato", LanguageManager.Model.Language.Columns.Enabled.ToString(), new string[] { "Disablilitata", "Abilitata" }));
            table.AddColumn(new ArActionColumn("Imposta", ArActionColumn.Icons.Details, "editlanguage.aspx?lang={ID}"));
            return table;
        }



    }
}
