﻿using CmsConfigs.Forms;
using NetCms.GUI;
using NetCms.GUI.Icons;
using NetService.Utility.ValidatedFields;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Structure.Applications.Configs
{
    public class Manutenzione : SmPageVertical
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.HomeSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_Configs"; }
        }

        public override Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Plugin; }
        }

        public override string PageTitle
        {
            get { return "Manutenzione portale"; }
        }

        public Manutenzione()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("generic.aspx", Icons.Application_Edit, "Impostazioni generali"));
            this.Toolbar.Buttons.Add(new ToolbarButton("languages.aspx", Icons.Rainbow, "Località e lingue"));
            this.Toolbar.Buttons.Add(new ToolbarButton("index-config.aspx", Icons.Database_Refresh, "Indicizzazione contenuti"));
            this.Toolbar.Buttons.Add(new ToolbarButton("seo-meta.aspx", Icons.Server_Chart, "SEO e Meta"));
            this.Toolbar.Buttons.Add(new ToolbarButton("social.aspx", Icons.Image_Link, "Social"));
            this.Toolbar.Buttons.Add(new ToolbarButton("manutenzione.aspx", Icons.Page_Gear, "Manutenzione"));

            this.Form.SubmitButton.Click += SubmitButton_Click;
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            bool status = false;
            try
            {

                if (Form.IsValid == VfGeneric.ValidationStates.Valid)
                {                 
                    if (Form.Stato.PostBackValue == "maintenance")
                    {
                        if (!File.Exists(NetCms.Configurations.Paths.FileMaintenancePath))
                        {
                            string[] allowedIp = Form.AllowedIpAddress.PostBackValue.Split(';');

                            NetCms.Utility.Maintenance.SetOffline(
                                int.Parse(Form.Minuti.PostBackValue),
                                allowedIp,
                                Form.MessaggioOpzionale.PostBackValue);
                            status = true;
                        }
                    }
                    else if (Form.Stato.PostBackValue == "normale")
                    {
                        NetCms.Utility.Maintenance.RemoveOffline();
                        status = true;
                    }
                }
            }
            catch(Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message);
            }

            if (status)
                NetCms.GUI.PopupBox.AddSessionMessage("Configurazione salvata con successo.", PostBackMessagesType.Success);
            else
                NetCms.GUI.PopupBox.AddSessionMessage("Non è stato possibile effettuare l'operazione. Controllare i dati inseriti e riprovare.", PostBackMessagesType.Error);


        }

        NetCms.Configurations.CmsStatusEnum currentState = (NetCms.Configurations.CmsStatusEnum)System.Web.HttpContext.Current.Application["CmsStatus"];
        
        private ManutenzioneForm _Form;
        private ManutenzioneForm Form
        {
            get
            {
                if (_Form == null)
                {
                    _Form = new ManutenzioneForm("maintenanceForm", currentState);
                    _Form.CssClass = "Axf";
                    _Form.ShowMandatoryInfo = false;
                    _Form.SubmitButton.Text = "Conferma";
                }
                return _Form;
            }
        }


        protected override WebControl RootControl
        {
            get
            {
                WebControl control = new WebControl(HtmlTextWriterTag.Div);

                WebControl sectionTitle = new WebControl(HtmlTextWriterTag.Div);
                sectionTitle.CssClass = "section_title";
                sectionTitle.Controls.Add(new LiteralControl("<h1>Sezione per la manutenzione del portale cms.</h1>"));
                control.Controls.Add(sectionTitle);

                WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);

                WebControl legend = new WebControl(HtmlTextWriterTag.Legend);

                legend.Controls.Add(new LiteralControl("Stato del portale: " + (currentState == Configurations.CmsStatusEnum.ok ? "normale": Configurations.CmsStatusEnum.maintenance.ToString())));

                fieldset.Controls.Add(legend);

                fieldset.Controls.Add(Form);

                control.Controls.Add(fieldset);

                return control;
            }
        }
    }
}
