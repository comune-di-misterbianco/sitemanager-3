﻿using CmsConfigs.BusinessLogic;
using CmsConfigs.Forms;
using CmsConfigs.Model;
using GenericDAO.DAO.Utility;
using NetCms.GUI;
using NetService.Utility.ArTable;
using NetService.Utility.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Structure.Applications.Configs.Pages
{
    public class SetConfig : BasicPage
    {
        private G2Core.Common.RequestVariable _SelectedNetworkID;

        public SetConfig()
        {
            _SelectedNetworkID = new G2Core.Common.RequestVariable("key", G2Core.Common.RequestVariable.RequestType.QueryString);
            this.CmsConfigFrom.SubmitButton.Click += SubmitButton_Click;
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            if (CmsConfigFrom.IsValid == NetService.Utility.ValidatedFields.VfGeneric.ValidationStates.Valid)
            {
                bool esito = ConfigsBusinessLogic.SaveConfig(CmsConfigFrom, _SelectedNetworkID.IntValue);
                if (esito)
                    NetCms.GUI.PopupBox.AddSessionMessage("Configurazione salvata con successo", NetCms.GUI.PostBackMessagesType.Success);
                else
                    NetCms.GUI.PopupBox.AddSessionMessage("Errore durante il salvataggio della configurazione del cms", NetCms.GUI.PostBackMessagesType.Error);
            }
        }

        private Config CurrentConfig
        {
            get
            {
                return CmsConfigs.BusinessLogic.ConfigsBusinessLogic.GetConfigByIdNetwork(_SelectedNetworkID.IntValue, null, true);
            }
        }
        private ConfigForm _CmsConfigFrom;
        private ConfigForm CmsConfigFrom
        {
            get
            {
                if (_CmsConfigFrom == null)
                {
                    _CmsConfigFrom = new ConfigForm("cmsconfigform", _SelectedNetworkID.IntValue, CurrentConfig);
                    _CmsConfigFrom.CssClass = "Axf";
                    _CmsConfigFrom.ShowMandatoryInfo = false;
                    _CmsConfigFrom.SubmitButton.Text = "Salva";
                }
                return _CmsConfigFrom;
            }
        }

        private string _SelectedNetworkName;
        public string SelectedNetworkName
        {
            get
            {
                if (_SelectedNetworkName == string.Empty)
                {
                    if (_SelectedNetworkID.IntValue > 0)
                        _SelectedNetworkName = NetCms.Networks.NetworksBusinessLogic.GetById(_SelectedNetworkID.IntValue).Label;
                    else
                        _SelectedNetworkName = "Generali";
                }
                return _SelectedNetworkName;
            }
        }

        public override WebControl BodyContent
        {
            get
            {
                WebControl div = new WebControl(HtmlTextWriterTag.Div);

                WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);

                WebControl legend = new WebControl(HtmlTextWriterTag.Legend);

                legend.Controls.Add(new LiteralControl("Modulo di configurazione cms:" + _SelectedNetworkName));

                fieldset.Controls.Add(legend);

                fieldset.Controls.Add(CmsConfigFrom);

                div.Controls.Add(fieldset);

                return div;
            }
        }
    }
}
