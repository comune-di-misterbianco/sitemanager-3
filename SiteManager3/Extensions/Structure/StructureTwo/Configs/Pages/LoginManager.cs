﻿using GenericDAO.DAO.Utility;
using LoginManager.BusinessLogic;
using LoginManager.Models;
using NetCms.GUI;
using NetService.Utility.ArTable;
using NetService.Utility.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Structure.Applications.Configs.Pages
{
    public class LoginManager : BasicPage
    {
        public LoginManager()
        {
            _SelectedNetworkID = new G2Core.Common.RequestVariable("key", G2Core.Common.RequestVariable.RequestType.QueryString);
        }

        private G2Core.Common.RequestVariable _SelectedNetworkID;

        public override WebControl BodyContent
        {
            get
            {
                WebControl div = new WebControl(HtmlTextWriterTag.Div);

                if (EliminationRequest && _SelectedNetworkID.IsValidInteger && _SelectedNetworkID.IntValue > 0)
                {
                    EliminationConfirmControl delControl = DeleteControl("Conferma eliminazione?", "Sei sicuro di voler eliminare il settaggio di autenticazione", "configs/loginmanager.aspx");
                    delControl.ConfirmButton.Click += ConfirmButton_Click;
                    return delControl;
                }

                WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
                fieldset.Controls.Add(new LiteralControl("<legend>Configurazione metodi di autenticazione</legend>"));
                fieldset.Controls.Add(new LiteralControl("<p class=\"action big\"><a href=\"loginconfig.aspx?key=0\">Imposta la modalità generali di autenticazione</a><p>"));
                div.Controls.Add(fieldset);
                div.Controls.Add(NetworkTable());


                // Scansione delle keys salvate in HttpContext.Current.Application che contengono il prefisso AccountSessionID_USER
                fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
                fieldset.Controls.Add(new LiteralControl("<legend>Utenti connessi al sistema</legend>"));

                var loggedUserKeys = System.Web.HttpContext.Current.Application.AllKeys.Where(x => x.StartsWith("AccountSessionID_USER"));
                string utentiConnessi = "<ul class=\"list-group\">";
                foreach (string userKey in loggedUserKeys)
                {
                    string idUser = userKey.Replace("AccountSessionID_USER","");
                    NetCms.Users.User user = NetCms.Users.UsersBusinessLogic.GetById(int.Parse(idUser));
                    utentiConnessi += "<li class=\"list-group-item\">"
                                   + "<i class=\"far fa-user\"></i><strong> Nome utente: </strong> " + user.UserName + "<br />"
                                   + "<i class=\"fas fa-user-alt\"></i> <strong> Nome completo: </strong> " + user.NomeCompleto + "<br />"
                                   + "<i class=\"fas fa-id-badge\"></i> <strong> Nome anagrafica: </strong> " + user.AnagraficaName + " </li>";
                    user = null;
                }
                utentiConnessi += "</ul>";
                fieldset.Controls.Add(new LiteralControl(utentiConnessi));
                div.Controls.Add(fieldset);

                return div;
            }
        }

        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            bool success = false;

            success = LoginSettingsBL.DeleteSettingByIdNetwork(_SelectedNetworkID.IntValue);

            if (success)
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Il settaggio è stato rimosso con successo con successo", PostBackMessagesType.Success, "loginmanager.aspx");
            }
            else
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Errore durante la rimozione settaggio selezionato.", PostBackMessagesType.Error, "loginmanager.aspx");
            }
        }

        public WebControl NetworkTable()
        {
            ArTable table = new ArTable(NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.Select(x => x.Value));
            table.NoRecordMsg = "Non ci sono reti installate";
            table.EnablePagination = false;            
            table.InnerTableCssClass = "tab";

            table.AddColumn(new ArTextColumn("Nome", "Label"));
            table.AddColumn(new ArTextColumn("Codice", "SystemName"));
            table.AddColumn(new ArOptionsColumn("Stato", "Enabled", new string[] { "Disablilitato", "Abilitata" }));
            table.AddColumn(new ArActionColumn("Imposta", ArActionColumn.Icons.Details, "loginconfig.aspx?key={ID}"));
            table.AddColumn(new ArConditionedActionColumn("Rimuovi impostazioni", ArConditionedActionColumn.Icons.Delete, "?action=delete&key={ID}", new Predicate<object>(ExistSetting), "Nessun settaggio", "Elimina")); //ArActionColumn("Rimuovi impostazioni", ArActionColumn.Icons.Delete, "?action=delete&key={ID}")
            
            return table;
        }


        private bool ExistSetting(object obj)
        {
            bool esito = false;
            Networks.BasicNetwork currentNetwork = null;

            if (obj.GetType() == typeof(Networks.BasicNetwork))
                currentNetwork = (Networks.BasicNetwork)obj;

            LoginSetting setting = LoginSettingsBL.GetSettingByIdNetwork(currentNetwork.ID, null, true);
            if (setting != null)
                esito = true;

            return esito;
        }

        protected bool EliminationRequest
        {
            get
            {
                NetService.Utility.Common.RequestVariable request = new NetService.Utility.Common.RequestVariable("action", NetService.Utility.Common.RequestVariable.RequestType.QueryString);
                if (request.IsValidString && request.StringValue == "delete")
                    _EliminationRequest = true;
                return _EliminationRequest;
            }
        }
        private bool _EliminationRequest = false;

        protected EliminationConfirmControl DeleteControl(string messageTitle, string message, string urlBack)
        {
            EliminationConfirmControl control = new EliminationConfirmControl(messageTitle, message, urlBack);
            return control;
        }
    }
}