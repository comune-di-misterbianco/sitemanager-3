﻿using NetService.Utility.ArTable;
using NetService.Utility.ValidatedFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using LoginManager.Forms;
using LoginManager.Models;
using GenericDAO.DAO.Utility;
using LoginManager.BusinessLogic;

namespace NetCms.Structure.Applications.Configs.Pages
{
    public class LoginConfigs : BasicPage
    {
        public LoginConfigs()
        {
          //  CurrentSettings = new NhRequestObject<LoginSetting>(LoginSettingsBL.GetSession(), "setting");

           _SelectedNetworkID = new G2Core.Common.RequestVariable("key", G2Core.Common.RequestVariable.RequestType.QueryString);

            this.SettingForm.SubmitButton.Click += SubmitButton_Click;
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            if (SettingForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                bool esito = LoginSettingsBL.SaveSetting(SettingForm, _SelectedNetworkID.IntValue);
                if (esito)
                    NetCms.GUI.PopupBox.AddSessionMessage("Impostazioni salvate con successo", NetCms.GUI.PostBackMessagesType.Success, "loginmanager.aspx");
                else
                    NetCms.GUI.PopupBox.AddSessionMessage("Errore durante il salvataggio delle impostazioni di autenticazione", NetCms.GUI.PostBackMessagesType.Error);
            }
        }

        private G2Core.Common.RequestVariable _SelectedNetworkID;

        private LoginSetting CurrentSetting
        {
            get
            {
                return LoginSettingsBL.GetSettingByIdNetwork(_SelectedNetworkID.IntValue, null, true);
            }
        }

        private LoginSettingForm _SettingForm;
        private LoginSettingForm SettingForm
        {
            get
            {
                if (_SettingForm == null)
                {
                    _SettingForm = new LoginSettingForm("loginSetting", _SelectedNetworkID.IntValue, CurrentSetting);
                    _SettingForm.CssClass = "Axf";
                    _SettingForm.ShowMandatoryInfo = false;
                    _SettingForm.SubmitButton.Text = "Salva";
                }
                return _SettingForm;
            }
        }

        private string _SelectedNetworkName;
        public string SelectedNetworkName
        {
            get
            {
                if (_SelectedNetworkName == string.Empty)
                {
                    if (_SelectedNetworkID.IntValue > 0)
                        _SelectedNetworkName = NetCms.Networks.NetworksBusinessLogic.GetById(_SelectedNetworkID.IntValue).Label;
                    else
                        _SelectedNetworkName = "Generali";
                }
                return _SelectedNetworkName;
            } 
        }


        public override WebControl BodyContent
        {
            get
            {
                WebControl div = new WebControl(HtmlTextWriterTag.Div);

                WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);

                WebControl legend = new WebControl(HtmlTextWriterTag.Legend);

                legend.Controls.Add(new LiteralControl("Modulo di gestione settaggi di login:" + _SelectedNetworkName));

                fieldset.Controls.Add(legend);

                fieldset.Controls.Add(SettingForm);

                div.Controls.Add(fieldset);

                return div;
            }
        }


        
    }
}