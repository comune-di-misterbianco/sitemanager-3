﻿using CmsConfigs.Forms;
using GenericDAO.DAO.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using LanguageManager.Model;
using NHibernate;
using GenericDAO.DAO;

namespace NetCms.Structure.Applications.Configs.Pages
{
    public class EditLanguage : BasicPage
    {
        public EditLanguage()
        {
            CurrentLanguage = new NhRequestObject<Language>(LanguageManager.BusinessLogic.LanguageBusinessLogic.GetCurrentSession(), "lang");
            this.Form.SubmitButton.Click += SubmitButton_Click;
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            bool esito = false;
            if (Form.IsValid == NetService.Utility.ValidatedFields.VfGeneric.ValidationStates.Valid)
            {
                using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = session.BeginTransaction())
                {
                    try
                    {
                        

                        Language cLanguage = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetLanguageById(CurrentLanguage.Instance.ID, session);

                        bool filled = Form.FillObjectAdvanced(cLanguage, session);
                        if (filled)
                        {
                            IGenericDAO<Language> dao = new CriteriaNhibernateDAO<Language>(session);
                            dao.SaveOrUpdate(cLanguage);
                            tx.Commit();
                            esito = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                    }
                }
                
                if (esito)
                    NetCms.GUI.PopupBox.AddSessionMessage("Impostazioni salvate con successo", NetCms.GUI.PostBackMessagesType.Success);
                else
                    NetCms.GUI.PopupBox.AddSessionMessage("Errore durante il salvataggio delle impostazioni", NetCms.GUI.PostBackMessagesType.Error);
            }
        }

        private NhRequestObject<Language> CurrentLanguage;

        private LanguageForm _Form;
        private LanguageForm Form
        {
            get
            {
                if (_Form == null)
                {
                    _Form = new LanguageForm("langForm", CurrentLanguage.Instance);
                    _Form.CssClass = "Axf";
                    _Form.ShowMandatoryInfo = false;
                    _Form.SubmitButton.Text = "Salva";

                }
                return _Form;
            }
        }

        public override WebControl BodyContent
        {
            get
            {
                WebControl div = new WebControl(HtmlTextWriterTag.Div);

                WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);

                WebControl legend = new WebControl(HtmlTextWriterTag.Legend);

                legend.Controls.Add(new LiteralControl("Gestione lingua: " + CurrentLanguage.Instance.Nome_Lang));

                fieldset.Controls.Add(legend);

                fieldset.Controls.Add(Form);

                div.Controls.Add(fieldset);

                return div;
            }
        }
    }
}
