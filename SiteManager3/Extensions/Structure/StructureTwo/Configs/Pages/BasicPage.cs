﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetCms.GUI.Icons;

namespace NetCms.Structure.Applications.Configs.Pages
{
    public abstract class BasicPage : SmPageVertical
    {
        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_Configs"; }
        }

        public override Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Plugin; }
        }

        public override string PageTitle
        {            
           get { return "Impostazioni generali"; }            
        }

        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.HomeSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public abstract WebControl BodyContent { get; }

        public BasicPage()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("generic.aspx", Icons.Application_Edit, "Impostazioni generali"));
            this.Toolbar.Buttons.Add(new ToolbarButton("loginmanager.aspx", Icons.Application_Key, "Impostazioni di autenticazione"));
            this.Toolbar.Buttons.Add(new ToolbarButton("languages.aspx", Icons.Rainbow, "Località e lingue"));
            this.Toolbar.Buttons.Add(new ToolbarButton("index-config.aspx", Icons.Database_Refresh, "Indicizzazione contenuti"));
            this.Toolbar.Buttons.Add(new ToolbarButton("seo-meta.aspx", Icons.Server_Chart, "SEO e Meta"));
            this.Toolbar.Buttons.Add(new ToolbarButton("social.aspx", Icons.Image_Link, "Social"));
        }

        protected override WebControl RootControl
        {
            get
            {
                return BodyContent;
            }
        }
    }
}
