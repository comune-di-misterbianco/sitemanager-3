﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetCms.Vertical;
using NetCms.Installation;
using NetCms.GUI.Icons;

namespace NetCms.Structure.Applications.Configs
{
    public class Wellcome : SmPageVertical
    {

        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.HomeSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_Configs"; }
        }

        public override Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Plugin; }
        }

        public override string PageTitle
        {
            get { return "Configurazione cms"; }
        }

        public Wellcome()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("generic.aspx", Icons.Application_Edit, "Impostazioni generali"));
            this.Toolbar.Buttons.Add(new ToolbarButton("loginmanager.aspx", Icons.Application_Key, "Impostazioni di autenticazione"));
            this.Toolbar.Buttons.Add(new ToolbarButton("languages.aspx", Icons.Rainbow, "Località e lingue"));
            this.Toolbar.Buttons.Add(new ToolbarButton("index-config.aspx", Icons.Database_Refresh, "Indicizzazione contenuti"));
            this.Toolbar.Buttons.Add(new ToolbarButton("seo-meta.aspx", Icons.Server_Chart, "SEO e Meta"));
            this.Toolbar.Buttons.Add(new ToolbarButton("social.aspx", Icons.Image_Link, "Social"));
            this.Toolbar.Buttons.Add(new ToolbarButton("manutenzione.aspx", Icons.Page_Gear, "Manutenzione"));
        }

        protected override WebControl RootControl
        {
            get
            {
                WebControl control = new WebControl(HtmlTextWriterTag.Div);
                control.CssClass = "vertical_application_toolbar";

                LiteralControl benvenuto = new LiteralControl("<h1>Benvenuto nella sezione di configurazione del cms.</h1>");
                control.Controls.Add(benvenuto);

                string htmlCode = "";
                htmlCode += "<ul>";
                htmlCode += "<li class=\"ico\"><a href=\"generic.aspx\"><img src=\"/cms/css/admin/configs/images/imp-generali.png\" /><span>Impostazioni generali</span></a></li>";
                htmlCode += "<li class=\"ico\"><a href=\"loginmanager.aspx\"><img src=\"/cms/css/admin/configs/images/imp-autenticazione.png\" /><span>Impostazioni di autenticazione</span></a></li>";
                htmlCode += "<li class=\"ico\"><a href=\"languages.aspx\"><img src=\"/cms/css/admin/configs/images/localita-lingue.png\" /><span>Località e lingue</span></a></li>";
                htmlCode += "<li class=\"ico\"><a href=\"index-config.aspx\"><img src=\"/cms/css/admin/configs/images/indicizz-contenuti.png\" /><span>Indicizzazione contenuti</span></a></li>";
                htmlCode += "<li class=\"ico\"><a href=\"seo-meta.aspx\"><img src=\"/cms/css/admin/configs/images/imp-seo-e-meta.png\" /><span>Impostazioni seo e meta</span></a></li>";
                htmlCode += "<li class=\"ico\"><a href=\"social.aspx\"><img src=\"/cms/css/admin/configs/images/imp-social.png\" /><span>Impostazioni social</span></a></li>";
                htmlCode += "</ul>";
                htmlCode += "<div class=\"clr\"></div>";

                control.Controls.Add(new LiteralControl(htmlCode));

                return control;
            }
        }


    }
}
