﻿using NetService.Utility.ValidatedFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CmsConfigs.Forms
{
    public class ManutenzioneForm : VfManager
    {
        public ManutenzioneForm(string controlId, NetCms.Configurations.CmsStatusEnum statoCms) : base(controlId)
        {
            statoCmsCorrente = statoCms;

            Fields.Add(Stato);
            Fields.Add(Minuti);
            Fields.Add(AllowedIpAddress);
            Fields.Add(MessaggioOpzionale);

            if (statoCmsCorrente == NetCms.Configurations.CmsStatusEnum.maintenance)
                Stato.DefaultValue = "maintenance";
            else if (statoCmsCorrente == NetCms.Configurations.CmsStatusEnum.ok)
                Stato.DefaultValue = "normale";

        }

        private NetCms.Configurations.CmsStatusEnum statoCmsCorrente;




        private VfDropDown _Stato;
        public VfDropDown Stato
        {
            get
            {
                if (_Stato == null)
                {
                    _Stato = new VfDropDown("Stato", "Stato portale", "Stato");
                    _Stato.Required = true;
                    _Stato.Options.Add("Modalità manutenzione", "maintenance");
                    _Stato.Options.Add("Modalità normale", "normale");
                    //_Stato.Options.Add("Modalità manutenzione solo al backoffice", "maintenance-backoffice");
                }
                return _Stato;
            }
        }

        private VfTextBox _Minuti;
        public VfTextBox Minuti
        {
            get
            {
                if (_Minuti == null)
                {
                    _Minuti = new VfTextBox("minuti", "Tempo di ritardo in minuti", "minuti", InputTypes.PositiveNumber);
                    _Minuti.Required = true;
                    _Minuti.DefaultValue = 5;
                    //_Minuti.CssClass = "";
                }
                return _Minuti;
            }
        }

        private VfTextBox _AllowedIpAddress;
        public VfTextBox AllowedIpAddress
        {
            get
            {
                if (_AllowedIpAddress == null)
                {
                    _AllowedIpAddress = new VfTextBox("ipaddressallowed", "Indirizzi ip di presentazione abilitati", "ipaddressallowed", InputTypes.Text);
                    _AllowedIpAddress.HelpInlineText = "Inserire gli indirizzi da abilitare separati da un punto e virgola";
                    _AllowedIpAddress.DefaultValue = "::1;127.0.0.1;151.84.84.72";
                    _AllowedIpAddress.Required = true;
                }
                return _AllowedIpAddress;
            }
        }

        private VfTextBox _MessaggioOpzionale;
        public VfTextBox MessaggioOpzionale
        {
            get
            {
                if (_MessaggioOpzionale == null)
                {
                    _MessaggioOpzionale = new VfTextBox("messaggio", "Testo da mostrare all'utente (opzionale)", "messaggio", InputTypes.Text);
                    _MessaggioOpzionale.Required = false;
                    _MessaggioOpzionale.TextBox.TextMode = System.Web.UI.WebControls.TextBoxMode.MultiLine;
                    _MessaggioOpzionale.TextBox.Rows = 4;                    
                }
                return _MessaggioOpzionale;
            }
        }
    }
}
