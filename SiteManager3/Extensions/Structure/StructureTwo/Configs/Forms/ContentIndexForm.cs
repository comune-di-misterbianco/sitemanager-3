﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using CmsConfigs.Model;


namespace CmsConfigs.Forms
{
    public class ContentIndexForm : VfManager
    {
        public ContentIndexForm(string controlId, IndexConfig indexconfig = null)
            : base(controlId)
        {
            currentIndexConfig = indexconfig;

            this.Fields.Add(Mode);

            string mode = string.Empty;

            if (indexconfig != null && !string.IsNullOrEmpty(indexconfig.Mode.ToString()))
                mode = indexconfig.Mode.ToString();
            if (Mode.PostbackValueObject != null)
                mode = (string)Mode.PostbackValueObject;


            if (!string.IsNullOrEmpty(mode))
            {
                switch (mode)
                {
                    case "zoom":
                        this.Fields.Add(ZoomSettingsFolderPath);
                        break;
                    case "lucene":
                        this.Fields.Add(LuceneSettingsFolderPath);
                        break;
                    case "solr":
                        this.Fields.Add(SolrHostAddress);
                        this.Fields.Add(SolrHostTcpPort);
                        this.Fields.Add(SolrCore);
                        break;
                }
            }

            if (currentIndexConfig != null)
                this.DataBindAdvanced(currentIndexConfig);
        }

        private IndexConfig currentIndexConfig;

        private VfRadioButton Mode
        {
            get
            {
                if (_Mode == null)
                {
                    //, typeof(Model.IndexConfig.IndexMode)
                    _Mode = new VfRadioButton(Model.IndexConfig.Column.Mode.ToString(), "Modalità di indicizzazione", Model.IndexConfig.Column.Mode.ToString(), false);

                    foreach (Model.IndexConfig.IndexMode obj in Enum.GetValues(typeof(Model.IndexConfig.IndexMode)))
                    {
                        _Mode.addItem(obj.ToString(), obj.ToString(), false);
                    }

                    //  _Mode.addItem()
                    _Mode.AutoPostBack = true;
                    _Mode.Required = true;
                }
                return _Mode;
            }
        }
        private VfRadioButton _Mode;

        private VfTextBox ZoomSettingsFolderPath
        {
            get
            {
                if (_ZoomSettingsFolderPath == null)
                {
                    _ZoomSettingsFolderPath = new VfTextBox(Model.IndexConfig.Column.ZoomSettingsFolderPath.ToString(), "Percorso assoluto cartella settaggi zoom", Model.IndexConfig.Column.ZoomSettingsFolderPath.ToString(), InputTypes.Text);
                    _ZoomSettingsFolderPath.Required = false;
                    _ZoomSettingsFolderPath.Columns = 160;
                }
                return _ZoomSettingsFolderPath;
            }
        }
        private VfTextBox _ZoomSettingsFolderPath;

        private VfTextBox LuceneSettingsFolderPath
        {
            get
            {
                if (_LuceneSettingsFolderPath == null)
                {
                    _LuceneSettingsFolderPath = new VfTextBox(Model.IndexConfig.Column.LuceneSettingsFolderPath.ToString(), "Percorso assoluto cartella indici lucene", Model.IndexConfig.Column.LuceneSettingsFolderPath.ToString(), InputTypes.Text);
                    _LuceneSettingsFolderPath.Required = false;
                    _LuceneSettingsFolderPath.Columns = 160;
                }
                return _LuceneSettingsFolderPath;
            }
        }
        private VfTextBox _LuceneSettingsFolderPath;

        private VfTextBox SolrHostAddress
        {
            get
            {
                if (_SolrHostAddress == null)
                {
                    _SolrHostAddress = new VfTextBox(Model.IndexConfig.Column.SolrHostAddress.ToString(), "Indirizzo server solr", Model.IndexConfig.Column.SolrHostAddress.ToString(), InputTypes.Text);
                    _SolrHostAddress.Required = false;
                    _SolrHostAddress.TextBox.Rows = 60;
                }
                return _SolrHostAddress;
            }
        }
        private VfTextBox _SolrHostAddress;

        private VfTextBox SolrHostTcpPort
        {
            get
            {
                if (_SolrHostTcpPort == null)
                {
                    _SolrHostTcpPort = new VfTextBox(Model.IndexConfig.Column.SolrHostTcpPort.ToString(), "Porta tcp server solr", Model.IndexConfig.Column.SolrHostTcpPort.ToString(), InputTypes.Number);
                    _SolrHostTcpPort.Required = false;
                    _SolrHostTcpPort.TextBox.Rows = 60;
                }
                return _SolrHostTcpPort;
            }
        }
        private VfTextBox _SolrHostTcpPort;

        private VfTextBox SolrCore
        {
            get
            {
                if (_SolrCore == null)
                {
                    _SolrCore = new VfTextBox(Model.IndexConfig.Column.SolrCore.ToString(), "Indicare il core solr", Model.IndexConfig.Column.SolrCore.ToString(), InputTypes.Text);
                    _SolrCore.Required = false;
                    _SolrCore.TextBox.Rows = 60;
                }
                return _SolrCore;
            }
        }
        private VfTextBox _SolrCore;
    }
}
