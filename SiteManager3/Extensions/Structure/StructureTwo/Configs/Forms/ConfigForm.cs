﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using CmsConfigs.Model;
using NetCms.Themes.Model;

namespace CmsConfigs.Forms
{
    public class ConfigForm : VfManager
    {
        public ConfigForm(string controlId, int rifNetwork, Config cmsConfig) : base(controlId)
        {
            currentConfig = cmsConfig;

            this.Fields.Add(PortalName);
            this.Fields.Add(Theme);
            this.Fields.Add(StatoTopMenu);
            this.Fields.Add(TipoTopMenu);
            this.Fields.Add(AttachesExtensions);
            this.Fields.Add(CmsAppAttachToContentStatus);

            if (cmsConfig != null)
            {
                DataBindAdvanced(cmsConfig);
            }
        }

        private Config currentConfig;

        private VfTextBox _PortalName;
        public VfTextBox PortalName
        {
            get
            {
                if (_PortalName == null)
                {
                    _PortalName = new VfTextBox(Model.Config.Column.PortalName.ToString(), "Titolo del portale", Model.Config.Column.PortalName.ToString(), InputTypes.Text);
                    _PortalName.Required = false;
                }

                return _PortalName;
            }
        }
        
        // TODO: ThemeEngine - Da cambiare con un dropdown con immagine 
        private VfDropDown _Theme;
        private VfDropDown Theme
        {
            get
            {
                if (_Theme == null)
                {
                    _Theme = new VfDropDown(Config.Column.Theme.ToString(), "Tema", Config.Column.Theme.ToString(), typeof(string));
                    _Theme.Required = true;

                    foreach (Theme item in GlobalConfig.AvailableThemes)
                    {
                        _Theme.Options.Add(item.Name, item.Name.ToLower());
                    }
                }
                return _Theme;
            }
        }


        private VfRadioButton _StatoTopMenu;
        private VfRadioButton StatoTopMenu
        {
            get
            {
                if (_StatoTopMenu == null)
                {
                    _StatoTopMenu = new VfRadioButton(Model.Config.Column.TopMenuState.ToString(), "Stato TopMenu", false, false);
                    _StatoTopMenu.Required = true;
                    _StatoTopMenu.addItem(Config.State.On.ToString(), Config.State.On.ToString(), false);
                    _StatoTopMenu.addItem(Config.State.Off.ToString(), Config.State.Off.ToString(), true);                    
                }
                return _StatoTopMenu;
            }
        }

        private VfRadioButton _TipoTopMenu;
        private VfRadioButton TipoTopMenu
        {
            get
            {
                if (_TipoTopMenu == null)
                {
                    _TipoTopMenu = new VfRadioButton(Model.Config.Column.TopMenuStyle.ToString(), "Tipo TopMenu", false, false);
                    _TipoTopMenu.Required = true;
                    _TipoTopMenu.addItem(Config.MenuType.Normal.ToString(), Config.MenuType.Normal.ToString(), true);
                    _TipoTopMenu.addItem(Config.MenuType.Megamenu.ToString(), Config.MenuType.Megamenu.ToString(), false);
                }
                return _TipoTopMenu;
            }
        }


        private VfTextBox _AttachesExtensions;
        public VfTextBox AttachesExtensions
        {
            get
            {
                if (_AttachesExtensions == null)
                {
                    _AttachesExtensions = new VfTextBox(Model.Config.Column.SupportedAttachmentsExtensions.ToString(), "Estensione supportate", Model.Config.Column.SupportedAttachmentsExtensions.ToString(), InputTypes.Text);
                    _AttachesExtensions.Required = true;
                    _AttachesExtensions.HelpInlineText = "Inserire le estensioni consensite all'upload nel cms, separati da ;";
                }

                return _AttachesExtensions;
            }
        }

        private VfRadioButton _CmsAppAttachToContentStatus;
        private VfRadioButton CmsAppAttachToContentStatus
        {
            get
            {
                if (_CmsAppAttachToContentStatus == null)
                {
                    _CmsAppAttachToContentStatus = new VfRadioButton(Model.Config.Column.CmsAppAttachToContentStatus.ToString(), "Abilita allegati ai content delle app di cms", false, false);
                    _CmsAppAttachToContentStatus.Required = true;
                    _CmsAppAttachToContentStatus.addItem(Config.AttachToContentStatus.Enabled.ToString(), Config.AttachToContentStatus.Enabled.ToString(), true );
                    _CmsAppAttachToContentStatus.addItem(Config.AttachToContentStatus.Disabled.ToString(), Config.AttachToContentStatus.Disabled.ToString(), false);
                }
                return _CmsAppAttachToContentStatus;
            }
        }


    }
}
