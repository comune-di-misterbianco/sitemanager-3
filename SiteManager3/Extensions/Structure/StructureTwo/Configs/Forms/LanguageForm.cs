﻿using LanguageManager.Model;
using NetService.Utility.ValidatedFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CmsConfigs.Forms
{
    public class LanguageForm : VfManager
    {
        public LanguageForm(string controlId, Language language) : base(controlId)
        {
            Lang = language;
            
            Fields.Add(Stato);

            if (language != null)
                this.DataBindAdvanced(language);
        }

        private Language Lang;

        private VfDropDown _Stato;
        private VfDropDown Stato
        {
            get
            {
                if (_Stato == null)
                {
                    _Stato = new VfDropDown(LanguageManager.Model.Language.Columns.Enabled.ToString(), "Stato", LanguageManager.Model.Language.Columns.Enabled.ToString());
                    _Stato.Required = true;
                    _Stato.Options.Add("Abilitata", "true");
                    _Stato.Options.Add("Disabilitata", "false");
                }
                return _Stato;
            }
        }
    }
}
