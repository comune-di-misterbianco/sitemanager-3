﻿using CmsConfigs.Model;
using GenericDAO.DAO;
using NetCms.DBSessionProvider;
using NetService.Utility.RecordsFinder;
using NetService.Utility.ValidatedFields;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CmsConfigs.BusinessLogic
{
    public static class ConfigsBusinessLogic
    {
        public static ISession GetSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }      

        public static IndexConfig GetIndexConfig(ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetSession();

            CriteriaNhibernateDAO<IndexConfig> dao = new CriteriaNhibernateDAO<IndexConfig>(innerSession);
            return dao.GetById("IndexConfig");// dao.GetById(idIndexConfig);
        }

        public static bool SaveIndexConfig(VfManager manager)
        {
            bool status = false;

            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    // verifico l'esistenza della configurazione iniziale leggendo il record con id 1
                    IndexConfig currentconfig = GetIndexConfig(session);

                    if (currentconfig == null)
                        currentconfig = new IndexConfig();
                    else
                    {
                        currentconfig.LuceneSettingsFolderPath = string.Empty;
                        currentconfig.ZoomSettingsFolderPath = string.Empty;
                        currentconfig.SolrHostAddress = string.Empty;
                        currentconfig.SolrHostTcpPort = -1;
                    }

                    bool filled = manager.FillObjectAdvanced(currentconfig, session);

                    if (filled)
                    {
                        CriteriaNhibernateDAO<IndexConfig> dao = new CriteriaNhibernateDAO<IndexConfig>(session);
                        currentconfig.IndexConfigKey = "IndexConfig";
                        dao.SaveOrUpdate(currentconfig);
                        tx.Commit();
                        status = true;
                    }
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
            }

            return status;
        }

        public static Config GetConfigByIdNetwork(int idNetwork, ISession innerSession = null, bool returnGeneralIfNull = false)
        {
            if (innerSession == null)
                innerSession = GetSession();

            CriteriaNhibernateDAO<Config> dao = new CriteriaNhibernateDAO<Config>(innerSession);

            SearchParameter spIdNetwork = new SearchParameter("", Model.Config.Column.RifNetwork.ToString(), idNetwork, Finder.ComparisonCriteria.Equals, false);

            Config cmsConfig = dao.GetByCriteria(new SearchParameter[] { spIdNetwork });

            if (cmsConfig == null && !returnGeneralIfNull)
                return GetGeneralConfig(innerSession);

            return cmsConfig;
        }

        public static List<Config> GetAllConfig(ISession innerSession, List<NetCms.Themes.Model.Theme> availableThemes)
        {
            if (innerSession == null)
                innerSession = GetSession();

            CriteriaNhibernateDAO<Config> dao = new CriteriaNhibernateDAO<Config>(innerSession);

            List<Config> cmsconfigs = dao.FindAll().ToList();

            cmsconfigs.ForEach(x => x.CurrentTheme = (availableThemes.Find(k => k.Name.ToLower() == x.Theme.ToLower())));

            return cmsconfigs;
        }

        public static Config GetGeneralConfig(ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetSession();            

            CriteriaNhibernateDAO<Config> dao = new CriteriaNhibernateDAO<Config>(innerSession);

            SearchParameter spIdNetwork = new SearchParameter("", Model.Config.Column.RifNetwork.ToString(), 0, Finder.ComparisonCriteria.Equals, false);

            Config setting = dao.GetByCriteria(new SearchParameter[] { spIdNetwork });

            return setting;
        }

        public static bool SaveConfig(VfManager form, int idNetwork, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetSession();

            bool esito = false;

            using (ITransaction tx = innerSession.BeginTransaction())
            {
                try
                {
                    Config currentConfig = GetConfigByIdNetwork(idNetwork, innerSession, true);
                    if (currentConfig == null)
                        currentConfig = new Config();

                    bool filled = form.FillObjectAdvanced(currentConfig, innerSession);

                    if (filled)
                    {
                        CriteriaNhibernateDAO<Config> dao = new CriteriaNhibernateDAO<Config>(innerSession);
                        currentConfig.RifNetwork = idNetwork;
                        dao.SaveOrUpdate(currentConfig);
                        tx.Commit();
                        esito = true;
                    }

                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message);
                    tx.Rollback();
                }
            }
            return esito;
        }
    }
}
