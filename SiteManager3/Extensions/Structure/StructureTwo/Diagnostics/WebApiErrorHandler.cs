﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Http;


namespace NetCms.Diagnostics
{
    public class WebApiErrorHandler
    {
        /// <summary>
        /// Consente la gestione centralizzata della gestione del messaggio di errore per le webapi in uso nel sistema
        /// </summary>
        /// <example>
        /// Dopo aver inizializzato l'oggetto ApiErrorHandler passando i parametri richiesti utilizzare il metodo HttpResponse() per restiutire l'HttpResponseException con il body content in json.
        /// <code>
        /// Diagnostics.ApiErrorHandler apiError = new Diagnostics.ApiErrorHandler(System.Net.HttpStatusCode.NotFound, "Folder Id not found", string.Format("No folder with ID = {0}", folderId));
        /// throw new HttpResponseException(apiError.HttpResponse());
        /// </code>
        /// </example>
        /// <param name="statusCode">HttpStatusCode</param>
        /// <param name="statusMessage"> Inserire il messaggio di errore da esporre nello status</param>
        /// <param name="errorMessage">Inserire il messaggio di errore da esporre nel body della response</param>
        public WebApiErrorHandler(HttpStatusCode statusCode, string statusMessage, string errorMessage)
        {
            StatusCode = statusCode;
            StatusMessage = statusMessage;
            ErrorMessage = errorMessage;
            InternalCode = 0; // non specificato
        }

        /// <summary>
        /// Consente la gestione centralizzata della gestione del messaggio di errore per le webapi in uso nel sistema
        /// </summary>
        /// <example>
        /// Dopo aver inizializzato l'oggetto ApiErrorHandler passando i parametri richiesti utilizzare il metodo HttpResponse() per restiutire l'HttpResponseException con il body content in json.
        /// <code>
        /// NetCms.Diagnostics.ApiErrorHandler apiError = new Diagnostics.ApiErrorHandler(System.Net.HttpStatusCode.NotFound, "Folder Id not found", string.Format("No folder with ID = {0}", folderId));
        /// throw new HttpResponseException(apiError.HttpResponse());
        /// </code>
        /// </example>
        /// <param name="statusCode">HttpStatusCode</param>
        /// <param name="statusMessage"> Inserire il messaggio di errore da esporre nello status</param>
        /// <param name="errorMessage">Inserire il messaggio di errore da esporre nel body della response</param>
        /// <param name="internalErrorCode">Inserire il codice errore mappato internalmente al sistema</param>
        public WebApiErrorHandler(HttpStatusCode statusCode, string statusMessage, string errorMessage, int internalErrorCode)
        {
            StatusCode = statusCode;
            StatusMessage = statusMessage;
            ErrorMessage = errorMessage;
            InternalCode = internalErrorCode;
        }

        public int InternalCode {
            get;
            set;
        }

        public string StatusMessage
        {
            get;
            set;
        }

        public HttpStatusCode StatusCode { get; set; }

        private string ErrorMessage
        {
            get;
            set;
        }

        private ErrorBodyMessage BodyMessage
        {
            get
            {
                return new ErrorBodyMessage(ErrorMessage, (int)StatusCode, InternalCode);
            }
        }

        public HttpResponseMessage HttpResponse()
        {
            var resp = new System.Net.Http.HttpResponseMessage()
            {                             
                StatusCode = StatusCode,
                Content = new System.Net.Http.StringContent(BodyMessage.ToJson()),
                ReasonPhrase = StatusMessage
            };
            return resp;
        }
    }
}
