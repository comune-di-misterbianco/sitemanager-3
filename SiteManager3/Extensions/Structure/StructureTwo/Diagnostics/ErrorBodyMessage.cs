﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace NetCms.Diagnostics
{
   public class ErrorBodyMessage
    {
        
        /// <summary>
        /// Si occupa dell'esposizione in formato json del messaggio di errore strutturato da restituire nel body della respone
        /// </summary>
        /// <param name="message">Testo del messaggio</param>
        /// <param name="internalCode">Codice interno al cms</param>
        public ErrorBodyMessage(string message, int status, int internalCode)
        {
            Message = message;
            InternalCode = internalCode;
            StatusCode = status;
        }

        /// <summary>
        /// Codice errore interno al cms
        /// </summary>
        public int InternalCode
        {
            get;
            set;
        }

        /// <summary>
        /// Rappresenta lo status code numerico
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// Messaggio di errore da esporre nel body della response
        /// </summary>
        public string Message
        {
            get;
            set;
        }

        public string ToJson()
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(this);
            return json;            
        }
    }
}
