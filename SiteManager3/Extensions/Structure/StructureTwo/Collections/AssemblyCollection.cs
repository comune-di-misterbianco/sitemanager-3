using System;
using System.Reflection;
using System.Collections;
/// <summary>
/// Summary description for xNodeCollection
/// </summary>

namespace NetCms.Collections
{
    public class AssemblyCollection : IEnumerable
    {
        private G2Collection coll;

        public int Count { get { return coll.Count; } }

        public AssemblyCollection()
        {
            coll = new G2Collection();
        }

        public void Add(Assembly obj)
        {
            coll.Add(obj.GetHashCode().ToString(), obj);
        }

        public Assembly this[int i]
        {
            get
            {
                return(Assembly)coll[coll.Keys[i]];
            }
        }
        public Assembly this[string key]
        {
            get
            {
                return (Assembly)coll[key];
            }
        }

        public bool Contains(string key)
        {
            return this[key] != null;
        }
    #region Enumerator

    public IEnumerator GetEnumerator()
    {
        return new CollectionEnumerator(this);
    }

    private class CollectionEnumerator : IEnumerator
    {
        private int CurentPos = -1;
        private AssemblyCollection Collection;
        public CollectionEnumerator(AssemblyCollection coll)
        {
            Collection = coll;
        }
        public object Current
        {
            get
            {
                return Collection[CurentPos];
            }
        }
        public bool MoveNext()
        {
            if (CurentPos < Collection.Count - 1)
            {
                CurentPos++;
                return true;
            }
            else
                return false;
        }
        public void Reset()
        {
            CurentPos = -1;
        }
    }
    #endregion
    }
}