using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetTable;
using System.Linq;
/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms
{
    namespace Structure
    {
        namespace Grants
        {
            public partial class UsersGrants
            {
                public class UsersAddControl
                {
                    private PageData PageData;

                    public string ID
                    {
                        get { return _ID; }
                    }
                    private string _ID;

                    public bool AutoPostBack
                    {
                        get { return _AutoPostBack; }
                        set { _AutoPostBack  = value ; }
                    }
                    private string AutoPostBackValue
                    {
                        get { return AutoPostBack?"1":"0"; }
                    }
                    private bool _AutoPostBack;

                    public SmTableActionColumn ActionColumn
                    {
                        get
                        {
                            if (_ActionColumn == null)
                                _ActionColumn = new SmTableActionColumn("id_User", "add", "Aggiungi", "javascript: setFormValue({0},'" + ID + "','" + AutoPostBackValue + "')");
                            return _ActionColumn; 
                        }
                        set { _ActionColumn = value; }
                    }
                    private SmTableActionColumn _ActionColumn;

                    public UsersAddControl(string ControlID,PageData pageData)
                    {
                        PageData = pageData;
                        _ID = ControlID;
                        _AutoPostBack = true;                        
                    }

                    public HtmlGenericControl getControl()
                    {
                        HtmlGenericControl output = new HtmlGenericControl("div");

                        string sql = "SELECT id_User,Name_User FROM Users";
                        //sql += " INNER JOIN Profiles ON (Profile_User = id_Profile)";
                        sql += " INNER JOIN Users_Profiles ON (Profile_User = id_Profile)";
                        sql += " INNER JOIN UsersGroups ON (User_UserGroup = id_User)";
                        sql += " INNER JOIN Groups ON (Group_UserGroup = id_Group)";

                        string conditions = "";
                        for (int i = 0; i < PageData.Account.Groups.Count; i++)
                        {
                            NetCms.Users.GroupAssociation GroupAssociation = PageData.Account.AssociatedGroupsOrdered.ElementAt(i);

                            if (GroupAssociation.IsAdministrator)
                            {
                                conditions += " OR ";
                                conditions += " Tree_Group LIKE '" + GroupAssociation.Group.Tree + "%'";
                            }
                            if (GroupAssociation.GrantsRole == NetCms.Users.GrantAdministrationTypes.Administrator)
                                conditions += " AND id_User <> " + PageData.Account.ID + ")";
                        }

                        //La condizione 1=0 serve ad esculudere tutti i record tranne quelli selezionati dalle
                        //condizioni specificate nella variabile conditions
                        sql += " AND (1=0" + conditions + ")";

                        sql += " GROUP BY id_User,Name_User";
                        SmTable table = new SmTable(PageData.PageIndex, sql, PageData.Conn);

                        SmTableColumn nome = new SmTableColumn("Name_User", "Nome");
                        table.addColumn(nome);
                        
                        table.addColumn(ActionColumn);

                        HtmlInputHidden setUserToAdd = new HtmlInputHidden();
                        setUserToAdd.ID = ID;
                        output.Controls.Add(setUserToAdd);

                        output.Controls.Add(table.getTable());
                        return output;
                    }
                }
            }
        }
    }
}