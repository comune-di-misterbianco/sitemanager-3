using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using HtmlGenericControls;
using NetCms.Networks.Grants;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Threading;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Runtime.Remoting.Messaging;
using NHibernate;
using System.Web.SessionState;
using LabelsManager;

/// <summary>
/// LoginControl v3
/// </summary>
/// <remarks>This LoginControl contains method of LoginControlv2_AD</remarks>
namespace NetCms.Users
{
    public class LoginControlv3 : WebControl
    {
        #region Label
        public static Labels Labels
        {
            get
            {
                return LabelsCache.GetTypedLabels(typeof(LoginLabels)) as Labels;
            }
        }
               
        /// <summary>
        /// Login Title
        /// </summary>
        public string LoginTitleLabel
        {
            get { return _LoginTitleLabel; }
            set { _LoginTitleLabel = value; }
        }
        private string _LoginTitleLabel = Labels[LoginLabels.LabelsList.LoginTitleLabel];

        public string ForgotTitleLabel
        {
            get { return _ForgotTitleLabel; }
            set { _ForgotTitleLabel = value; }
        }
        private string _ForgotTitleLabel = Labels[LoginLabels.LabelsList.ForgotTitleLabel];

        public string LoginButtonLabel
        {
            get { return _LoginButtonLabel; }
            set { _LoginButtonLabel = value; }
        }
        private string _LoginButtonLabel = Labels[LoginLabels.LabelsList.AccessButtonLabel];

        public string LoginButtonLabel_AD
        {
            get { return _LoginButtonLabel_AD; }
            set { _LoginButtonLabel_AD = value; }
        }
        private string _LoginButtonLabel_AD = Labels[LoginLabels.LabelsList.AccessButtonLabelAD];
                                 
        public string RecoverPasswordButtonLabel
        {
            get { return _RecoverPasswordButtonLabel; }
            set { _RecoverPasswordButtonLabel = value; }
        }
        private string _RecoverPasswordButtonLabel = Labels[LoginLabels.LabelsList.AskPassword];

        public string ResetButtonLabel
        {
            get { return _ResetButtonLabel; }
            set { _ResetButtonLabel = value; }
        }
        private string _ResetButtonLabel = Labels[LoginLabels.LabelsList.Cancel];

        public string RecoverPasswordLinkLabel
        {
            get { return _RecoverPasswordLinkLabel; }
            set { _RecoverPasswordLinkLabel = value; }
        }
        private string _RecoverPasswordLinkLabel = Labels[LoginLabels.LabelsList.PasswordRecovery];


        public string UserRegistrationLinkLabel
        {
            get { return _UserRegistrationLinkLabel; }
            set { _UserRegistrationLinkLabel = value; }
        }
        private string _UserRegistrationLinkLabel = Labels[LoginLabels.LabelsList.UserRegistration];
        #endregion

        #region PropertyLoginControl

        /// <summary>
        /// ID Offset
        /// </summary>
        private string _IDsOffset;
        public string IDsOffset
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_IDsOffset))
                    _IDsOffset = "_LoginControl" + this.LoginsControlsCount;
                return _IDsOffset;
            }
        }
        
        /// <summary>
        /// Redirect Url
        /// </summary>
        /// <remarks> Set Url for redirect after Login</remarks>
        private string _RedirectUrl = "";
        public string RedirectUrl
        {
            get { return _RedirectUrl; }
            private set { _RedirectUrl = value; }
        }
        
        /// <summary>
        ///  Show Registration Link
        /// </summary>
        private bool _ShowRegistrationLink = false;        
        public bool ShowRegistrationLink
        {
            get { return _ShowRegistrationLink; }
            set { _ShowRegistrationLink = value; }
        }

        /// <summary>
        /// Show PasswordRecovery Link
        /// </summary>
        private bool _ShowPasswordRecoveryLink = false;
        public bool ShowPasswordRecoveryLink
        {
            get { return _ShowPasswordRecoveryLink; }
            set { _ShowPasswordRecoveryLink = value; }
        }
          
        /// <summary>
        /// Registration Url
        /// </summary>
        /// <remarks> Set url for Regsistration</remarks>
        private string _RegistrationUrl = "";
        public string RegistrationUrl
        {
            get { return _RegistrationUrl; }
            set { _RegistrationUrl = value; }
        }

        /// <summary>
        /// Debug Mode
        /// </summary>
        private bool DebugModeEnabled
        {
            get
            {
                return NetCms.Configurations.Debug.DebugLoginEnabled;
            }
        }

        /// <summary>
        /// Header Login
        /// </summary>      
        private bool _IsHeadLogin = false;
        public bool IsHeadLogin
        {
            get { return _IsHeadLogin; }
            set { _IsHeadLogin = value; }
        }

        /// <summary>
        /// Modal Login when it's into Header
        /// </summary>     
        private bool _IsModalLogin = false;
        public bool IsModalLogin
        {
            get
            {
                if (IsHeadLogin == false)
                {
                    _IsModalLogin = false;
                    return _IsModalLogin;
                }
                else
                    return _IsModalLogin;
            }
            set { _IsModalLogin = value; }
        }

        /// <summary>
        /// Login Controls Count 
        /// </summary>
        ///<remarks>Indicizza il numero di controlli login aggiunti alla pagina.</remarks>
        private int _LoginsControlsCount;
        public int LoginsControlsCount
        {
            get
            {
                if (_LoginsControlsCount == 0)
                {
                    if (HttpContext.Current.Items.Contains("LoginsControlsCount"))
                    {
                        _LoginsControlsCount = int.Parse(HttpContext.Current.Items["LoginsControlsCount"].ToString());
                        _LoginsControlsCount++;
                        HttpContext.Current.Items["LoginsControlsCount"] = _LoginsControlsCount;
                    }
                    else
                        HttpContext.Current.Items["LoginsControlsCount"] = _LoginsControlsCount = 1;
                }
                return _LoginsControlsCount;
            }
        }
                 
        /// <summary>
        /// Hompage Module Login
        /// </summary>
        private bool _IsModuleLogin = false;
        public bool IsModuleLogin
        {
            get { return _IsModuleLogin; }
            set { _IsModuleLogin = value; }
        }

        /// <summary>
        /// Login AD
        /// </summary>
        private bool _IsLoginAD = false;
        public bool IsLoginAD
        {
            get { return _IsLoginAD; }
            set { _IsLoginAD = value; }
        }
            
        /// <summary>
        /// Show only AD Control
        /// </summary>
        private bool _ShowOnlyADCtrl = false;
        public bool ShowOnlyADCtrl
        {
            get { return _ShowOnlyADCtrl; }
            set { _ShowOnlyADCtrl = value; }
        }
                   
        #endregion

        #region RequestVariable 

        /// <summary>
        /// Login Postback Checker
        /// </summary>
        public G2Core.Common.RequestVariable PostbackChecker
        {
            get
            {
                if (_PostbackChecker == null)
                    _PostbackChecker = new G2Core.Common.RequestVariable("loginpostback" + this.IDsOffset, G2Core.Common.RequestVariable.RequestType.Form);
                return _PostbackChecker;
            }
        }
        private G2Core.Common.RequestVariable _PostbackChecker;

        /// <summary>
        /// Forgot Passowrd Checker
        /// </summary>
        public G2Core.Common.RequestVariable ForgotPasswordRequest
        {
            get
            {
                if (_ForgotPasswordRequest == null)
                    _ForgotPasswordRequest = new G2Core.Common.RequestVariable("forgot", G2Core.Common.RequestVariable.RequestType.QueryString);
                return _ForgotPasswordRequest;
            }
        }
        private G2Core.Common.RequestVariable _ForgotPasswordRequest;
        #endregion                   
                                          
        #region Constructor

        /// <summary>
        /// Login Control v3 Base Constructor
        /// </summary>
        public LoginControlv3() : base(HtmlTextWriterTag.Div)
        {
            RedirectUrl = HttpContext.Current.Request.Url.ToString();
        }

        /// <summary>
        /// Login Control v3 Base v
        /// </summary>
        /// <param name="redirectURL">Url of redirection page after login </param>
        /// <param name="showRegistration">Show Registration Link</param>
        /// <param name="mostraRecuperoPassword">Show Password Recovery Link</param>
        /// <param name="linkReg">Url of Registration Page</param>
        public LoginControlv3(string redirectURL, bool showRegistration = false, bool showRecovery = false, string linkReg = "") : base(HtmlTextWriterTag.Div)
        {
            RedirectUrl = redirectURL;
            CssClass = "LoginControl";
            ShowRegistrationLink = showRegistration;
            ShowPasswordRecoveryLink = showRecovery;
            RegistrationUrl = linkReg;
            
        }

        /// <summary>
        /// Login Control v3 Modal/Panel Contstructor
        /// </summary>
        /// <param name="redirectURL">Url of redirection page after login </param>
        /// <param name="showRegistration">Show Registration Link, default false</param>
        /// <param name="showRecovery">Show Password Recovery Link, default false</param>
        /// <param name="linkReg">Url of Registration Page</param>
        /// <param name="isPanel">true LoginControl as Module(Panel), false LoginControl as Modal; default false</param>
        public LoginControlv3(string redirectURL, bool showRegistration = false, bool showRecovery = false, string linkReg = "", bool isPanel = false) : base(HtmlTextWriterTag.Div)
        {
            RedirectUrl = redirectURL;
            CssClass = "LoginControl";
            ShowRegistrationLink = showRegistration;
            ShowPasswordRecoveryLink = showRecovery;
            RegistrationUrl = linkReg;

            if (isPanel)
            {
                IsHeadLogin = false;
                IsModalLogin = false;
                IsModuleLogin = true;
            }
        }

        /// <summary>
        /// Login Control v3 Modal/Panel Contstructor
        /// </summary>
        /// <param name="redirectURL">Url of redirection page after login </param>
        /// <param name="showRegistration">Show Registration Link, default false</param>
        /// <param name="showRecovery">Show Password Recovery Link, default false</param>
        /// <param name="linkReg">Url of Registration Page</param>
        /// <param name="isPanel">true LoginControl as Module(Panel), false LoginControl as Modal; default false</param>
        /// <param name="isAD">true LoginControl for AD, default false</param>
        /// <param name="showOnlyADCtrl">true Show Only AD Controls, default false</param>                       
        public LoginControlv3(string redirectURL, bool showRegistration = false, bool showRecovery = false, string linkReg = "", bool isPanel = false, bool isAD = false, bool showOnlyADCtrl = false ) : base(HtmlTextWriterTag.Div)
        {
            RedirectUrl = redirectURL;
            CssClass = "LoginControl";
            ShowRegistrationLink = showRegistration;
            ShowPasswordRecoveryLink = showRecovery;
            RegistrationUrl = linkReg;

            if (isPanel)
            {
                IsHeadLogin = false;
                IsModalLogin = false;
                IsModuleLogin = true;
            }

            IsLoginAD = isAD;
            ShowOnlyADCtrl = showOnlyADCtrl;
        }

        #endregion

        #region LoginButton

        /// <summary>
        /// Default Login Button
        /// </summary>
        /// <remarks>Login button for CMS Authentication Method</remarks>
        public Button LoginButton
        {
            get
            {
                if (_LoginButton == null)
                {
                    _LoginButton = new Button();
                    _LoginButton.ID = PostbackChecker.Key;

                    //if (!IsLoginAD && |Configurations.Generics.LoginMode == Configurations.Generics.LoginModeType.Mixed)
                    //{

                    _LoginButton.Click += loginButton_Click;
                    _LoginButton.Text = this.LoginButtonLabel;
                    //}
                    //else
                    //{
                    //    _LoginButton.Click += LoginButtonAD_Click;
                    //    _LoginButton.Text = this.LoginButtonLabel_AD;
                    //}


                    /*if (Configurations.Generics.IsHttps)
                    _LoginButton.PostBackUrl = "https://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl;                    */
                    _LoginButton.CssClass = "btn btn-primary";
                }
                return _LoginButton;
            }
        }
        private Button _LoginButton;
       
        /// <summary>
        /// Login Button AD
        /// </summary>
        /// <remarks>Login Button for AD Authentication Method</remarks>
        public Button LoginButtonAD
        {
            get
            {

                if (_LoginButtonAD == null)
                {
                    _LoginButtonAD = new Button();
                    _LoginButtonAD.ID = PostbackChecker.Key + "_AD";
                    _LoginButtonAD.Text = LoginButtonLabel_AD;
                    _LoginButtonAD.Click += LoginButtonAD_Click;
                    _LoginButtonAD.CssClass = "btn btn-success";
                }
                return _LoginButtonAD;
            }
            set { _LoginButtonAD = value; }
        }
        private Button _LoginButtonAD;

        #endregion

        #region Notification Area
        public WebControl NotifiyList
        {
            get
            {
                if (_NotifiyList == null)
                {
                    _NotifiyList = new WebControl(HtmlTextWriterTag.Ul);
                    _NotifiyList.CssClass = "list-unstyled";
                }
                return _NotifiyList;
            }
        }
        private WebControl _NotifiyList;


        public WebControl NotifiyArea
        {
            get
            {
                if (_NotifiyArea == null)
                {
                    _NotifiyArea = new WebControl(HtmlTextWriterTag.Div);
                    _NotifiyArea.CssClass = "NotifyArea";
                    if (HttpContext.Current.Session != null)                    
                    {
                        if(HttpContext.Current.Session["AnotherBrowserLogged"] != null)
                        {
                            AddNotify(Labels[LoginLabels.LabelsList.AccessFromOtherBrowser], NotifyLevel.warning);
                            HttpContext.Current.Session.Remove("AnotherBrowserLogged");
                        }
                        if (HttpContext.Current.Session["SSOUSER_NOTENABLED"] != null && HttpContext.Current.Session["SSOUSER_NOTENABLED"]=="0")
                        {
                            AddNotify(Labels[LoginLabels.LabelsList.NotGrantedUser], NotifyLevel.danger);
                            HttpContext.Current.Session["SSOUSER_NOTENABLED"] = "1";
                        }                       
                    }
                    
                }
                return _NotifiyArea;
            }
        }
        private WebControl _NotifiyArea;

        public enum NotifyLevel
        {
            success,
            info,
            warning,
            danger
        }

        public bool NotifySent
        {
            get
            {
                return _NotifySent;
            }
            private set
            {
                _NotifySent = value;
            }
        }
        private bool _NotifySent;

        private int LoginErrorCount
        {
            get
            {
                return (HttpContext.Current.Session["LoginTry"] != null) ? (int)HttpContext.Current.Session["LoginTry"] : 0;
            }
        }

        #endregion
                                          
        #region GrantDelegate && Grant Collection
        public delegate ICollection<KeyValuePair<int, bool>> CollectGrantsDelegate(User user);
        private ICollection<KeyValuePair<int, bool>> CollectGrants(User _user)
        {
            ICollection<KeyValuePair<int, bool>> _FoldersCanView = new List<KeyValuePair<int, bool>>();
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                User user = UsersBusinessLogic.GetById(_user.ID, sess);
                var networks = user.AssociatedNetworks;
                foreach (var net in networks)
                {
                    var folders = FolderBusinessLogic.FindNetworkFoldersForGrantsCollection(net.Network.ID, sess);
                    foreach (var folder in folders)//net.Network.NetworkFolders.Cast<StructureFolder>().Where(x => !x.Hidden && !x.IsSystemFolder && x.Depth <= 3))
                    {
                        GrantsFinder Criteria = new FolderGrants(folder, user.Profile, sess);
                        _FoldersCanView.Add(new KeyValuePair<int, bool>(folder.ID, Criteria.NeedToShow("show").Allowed));
                    }
                }
            }
            return _FoldersCanView;
        }

        //private CollectGrantsDelegate _CollectGrants;
        //public CollectGrantsDelegate CollectGrants
        //{
        //    get
        //    {
        //        if (_CollectGrants == null)
        //        {

        //            _CollectGrants = delegate(User user)
        //            {

        //                var networks = user.AssociatedNetworks;
        //                foreach (var net in networks)
        //                {
        //                    var folders = FolderBusinessLogic.FindNetworkFoldersForGrantsCollection(net.Network.ID);
        //                    foreach (var folder in folders)//net.Network.NetworkFolders.Cast<StructureFolder>().Where(x => !x.Hidden && !x.IsSystemFolder && x.Depth <= 3))
        //                    {
        //                        GrantsFinder Criteria = new FolderGrants(folder, user.Profile);
        //                        user.FoldersCanView.Add(new KeyValuePair<int, bool>(folder.ID, Criteria.NeedToShow("show").Allowed));
        //                    }
        //                }
        //            };
        //        }
        //        return _CollectGrants;
        //    }
        //    set 
        //    {
        //        _CollectGrants = value;
        //    }
        //}

        //private void CollectGrants(User user)
        //{

        //        var networks = user.AssociatedNetworks;
        //        foreach (var net in networks)
        //        {
        //            var folders = FolderBusinessLogic.FindNetworkFoldersForGrantsCollection(net.Network.ID);
        //            foreach (var folder in folders)//net.Network.NetworkFolders.Cast<StructureFolder>().Where(x => !x.Hidden && !x.IsSystemFolder && x.Depth <= 3))
        //            {
        //                GrantsFinder Criteria = new FolderGrants(folder, user.Profile);
        //                user.FoldersCanView.Add(new KeyValuePair<int, bool>(folder.ID, Criteria.NeedToShow("show").Allowed));
        //            }
        //        }

        //}
        #endregion
                
        #region  Function
            
        /// <summary>
        /// Callback Method
        /// </summary>
        /// <param name="ar"><see cref="IAsyncResult"/></param>                     
        static void CallbackMethod(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;
            NetCms.Users.LoginControl.CollectGrantsDelegate caller = (NetCms.Users.LoginControl.CollectGrantsDelegate)result.AsyncDelegate;
            ReturnMessage mess = (ReturnMessage)result.GetReplyMessage();

            HttpSessionState session = (HttpSessionState)ar.AsyncState;
            //ICollection<KeyValuePair<int, bool>> returnValue = caller.EndInvoke(ar);

            ICollection<KeyValuePair<int, bool>> _FoldersCanView = (List<KeyValuePair<int, bool>>)mess.ReturnValue;
            if (session != null)
            {
                User currUser = (User)session[Configurations.Generics.BackAccountSessionKey];
                if (currUser != null)
                {
                    if (session["FoldersCanViewUSER_" + currUser.ID] != null)
                    {
                        lock (session["FoldersCanViewUSER_" + currUser.ID])
                        {
                            session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                        }
                    }
                    else
                        session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                }
                //currUser.FoldersCanView = _FoldersCanView;
            }
        }

        /// <summary>
        /// Login Click Event
        /// </summary>
        /// <param name="sender"><see cref="object"/></param>
        /// <param name="e"><see cref="EventArgs"/></param>
        private void loginButton_Click(object sender, EventArgs e)
        {
            NetUtility.RequestVariable userName = new NetUtility.RequestVariable("username" + this.IDsOffset, HttpContext.Current.Request.Form);
            NetUtility.RequestVariable password = new NetUtility.RequestVariable("password" + this.IDsOffset, HttpContext.Current.Request.Form);

            if (userName.IsValid() && password.IsValid() && password.StringValue.Length>0)
            {
                string MD5Pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password.StringValue, "MD5").ToString();

                //DataTable Users = this.Conn.SqlQuery(" SELECT * FROM Users WHERE Deleted_User = 0 AND State_User = 1 AND Name_User = '" + user.StringValue + "' AND (Password_User = '" + MD5Pwd + "' OR NewPassword_User = '" + MD5Pwd + "')");

                User user = UsersBusinessLogic.GetActiveUserByUserNameAndPassword(userName.StringValue, MD5Pwd);

                if (user != null)
                {
                    NetCms.Users.LoginResult result = NetCms.Users.AccountManager.TryLoginUser(user, MD5Pwd);
                    if (result.Result == LoginResult.LoginResults.Logged)
                    {
                        //if (CollectGrants != null)
                        //    CollectGrants(user);

                        HttpSessionState webSession = HttpContext.Current.Session;
                        
                        NetCms.Users.LoginControl.CollectGrantsDelegate myAction = new NetCms.Users.LoginControl.CollectGrantsDelegate(CollectGrants);
                        myAction.BeginInvoke(user, new AsyncCallback(CallbackMethod), webSession);
                        
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente '" + userName.StringValue + "' ha effettuato il Login al sistema.");
                        var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();

                        //if (RedirectUrl.Length == 0)
                        //    RedirectUrl = "/";

                        //NetCms.Diagnostics.Diagnostics.Redirect(httpsHandler.ConvertPageAddressAsNonSecure(RedirectUrl));

                        if (!string.IsNullOrEmpty(RedirectUrl))
                            NetCms.Diagnostics.Diagnostics.Redirect(httpsHandler.ConvertPageAddressAsNonSecure(RedirectUrl));
                    }
                    else
                    {
                        AddLoginTry(userName.StringValue);
                        AddNotify(Labels[LoginLabels.LabelsList.YourAccountWasDisabled], NotifyLevel.danger);
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'account dell'utente '" + userName.StringValue + "' � stato disabilitato a causa della scadenza dei 180 giorni dall'accesso.", "", "", "");
                    }
                }
                else
                {
                    AddLoginTry(userName.StringValue);
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "Tentativo di login al sistema fallito con il seguente nome utente: '" + userName.StringValue + "' ha effettuato il Login al sistema.", "", "", "");
                    AddNotify(Labels[LoginLabels.LabelsList.NotValidUserAndPass], NotifyLevel.danger);
                }
            }
            else
            {
                AddLoginTry(userName.StringValue);
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "Tentativo di login al sistema fallito con il seguente nome utente: '" + userName.StringValue + "' ha effettuato il Login al sistema.", "", "", "");
                AddNotify(Labels[LoginLabels.LabelsList.NotValidUserAndPass], NotifyLevel.danger);
            }   
        }

        /// <summary>
        /// Login AD Click Event
        /// </summary>
        /// <param name="sender"><see cref="object"/></param>
        /// <param name="e"><see cref="EventArgs"/></param>
        private void LoginButtonAD_Click(object sender, EventArgs e)
        {
            System.Web.HttpContext.Current.Session["PreviousPage"] = HttpContext.Current.Request.Url.AbsoluteUri;

            NetCms.Diagnostics.Diagnostics.Redirect("/sso/default.aspx");
        }

        /// <summary>
        /// Add Notify into NotifyArea
        /// </summary>
        /// <param name="notify">Notify Text</param>
        /// <param name="levelError">Notify Level Error <see cref="NotifyLevel"/></param>
        public void AddNotify(string notify, NotifyLevel levelError)
        {
            WebControl li = new WebControl(HtmlTextWriterTag.Li);

            WebControl alert = new WebControl(HtmlTextWriterTag.Div);
            alert.CssClass = "alert alert-"+ levelError.ToString() +" alert-dismissible";
            alert.Attributes.Add("role", "alert");

            alert.Controls.Add(new LiteralControl("<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>"));
            alert.Controls.Add(new LiteralControl(notify));

            li.Controls.Add(alert);
            this.NotifiyList.Controls.Add(li);
        }

        /// <summary>
        /// Register User Attempts to Access
        /// </summary>
        /// <param name="userName">UserName</param>
        private void AddLoginTry(string userName)
        {
            if (HttpContext.Current.Session["LoginTry"] != null)
            {
                int exval = (int)HttpContext.Current.Session["LoginTry"];
                HttpContext.Current.Session["LoginTry"] = ++exval;
            }
            else
                HttpContext.Current.Session["LoginTry"] = 1;

            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Warning, "L'utente '" + userName + "' ha tentato di accedere al sistema senza successo (tot. " + HttpContext.Current.Session["LoginTry"] + " volte consecutive)","","","");
        }
            
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
                  
            if (!IsModalLogin)
            {

                if (!IsModuleLogin)
                {
                    //this.ID = "LoginControl" + this.LoginsControlsCount;
                    this.CssClass = "panel panel-default LoginControl_Off";
                           
                    //this.Controls.Add(NotifiyArea);

                    if (this.ForgotPasswordRequest.StringValue == "done")
                        this.AddNotify(Labels[LoginLabels.LabelsList.NewPasswordSentByMail], NotifyLevel.info);

                    if (this.ForgotPasswordRequest.StringValue == "true")
                        InitForgotPasswordControls();
                    else
                    {
                        if (this.ForgotPasswordRequest.StringValue == "confirm")
                            ConfirmChangePwd();
                        InitLoginControls();
                    }
                }
                else
                {
                    this.CssClass = "panel-body";
                    this.Controls.Add(NotifiyArea);
                    this.Controls.Add(InitModuleLoginBodyContent());
                }
            }
            else
            {  
                this.CssClass = "modal-dialog";
                this.Attributes.Add("role", "document");

                WebControl modalContent = new WebControl(HtmlTextWriterTag.Div);
                modalContent.CssClass = "modal-content";

                #region Modal Header
                WebControl modalHeader = new WebControl(HtmlTextWriterTag.Div);
                modalHeader.CssClass = "modal-header text-center";

                modalHeader.Controls.Add(new LiteralControl("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>"));
                modalHeader.Controls.Add(new LiteralControl("<h4 class=\"modal-title\">" + LoginTitleLabel + "</h4>"));

                modalContent.Controls.Add(modalHeader);
                #endregion

                #region Modal Body
                WebControl modalBody = new WebControl(HtmlTextWriterTag.Div);
                modalBody.CssClass = "modal-body";

                modalBody.Controls.Add(NotifiyArea);

                if (this.ForgotPasswordRequest.StringValue == "done")
                    this.AddNotify(Labels[LoginLabels.LabelsList.NewPasswordSentByMail], NotifyLevel.info);

                modalBody.Controls.Add(InitLoginModalBody());                                                 

                modalContent.Controls.Add(modalBody);

                #endregion
                
                #region Modal Footer

                WebControl modalFooter = new WebControl(HtmlTextWriterTag.Div);
                modalFooter.CssClass = "modal-footer";

                modalFooter.Controls.Add(InitLoginModalFooter());

                if (this.DebugModeEnabled)
                {
                    DebugLoginControlv3 DebugLogin = new DebugLoginControlv3();
                    modalFooter.Controls.Add(DebugLogin);
                }

                modalContent.Controls.Add(modalFooter);

                

                #endregion        

                this.Controls.Add(modalContent);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);


            //if (!IsModalLogin)
            //{
            if (NotifiyList.Controls.Count > 0)
            {
                NotifiyArea.Controls.Add(this.NotifiyList);
                if (IsHeadLogin)
                {
                    HiddenField loginTest = new HiddenField();
                    loginTest.ID = "loginTest" + this.ID;
                    loginTest.Value = "loginError";
                    this.Controls.Add(loginTest);
                }
            }
            //}
            //else
            //{
            //    if(NotifiyArea.Controls.Count > 0)
            //    {
                        
            //    }
            //}
        }

        #region Old Constructor LoginControl
        
        private void InitLoginControls()
        {      
            WebControl panelHeader = new WebControl(HtmlTextWriterTag.Div);
            panelHeader.CssClass = "panel-heading";
                      
            WebControl panelTitle = new WebControl(HtmlTextWriterTag.H3);
            panelTitle.CssClass = "panel-Title text-center";
            panelTitle.Controls.Add(new LiteralControl(this.LoginTitleLabel));
            
            panelHeader.Controls.Add(panelTitle);

            Controls.Add(panelHeader);

            Panel loginPanel = new Panel();
            loginPanel.DefaultButton = LoginButton.ID;
            this.Controls.Add(loginPanel);

            WebControl panelBody = new WebControl(HtmlTextWriterTag.Div);
            panelBody.CssClass = "panel-body";

            panelBody.Controls.Add(NotifiyArea);

            WebControl loginFormGroup = new WebControl(HtmlTextWriterTag.Div);
            loginFormGroup.CssClass = "login-form-group";

            if (!ShowOnlyADCtrl)
            {
                WebControl formGroupUserName = new WebControl(HtmlTextWriterTag.Div);
                formGroupUserName.CssClass = "form-group";

                Label UserName_Label = new Label();
                UserName_Label.Text = Labels[LoginLabels.LabelsList.Username] + ": ";
                UserName_Label.ID = "UserName_Label" + IDsOffset;
                UserName_Label.CssClass = "control-label";

                formGroupUserName.Controls.Add(UserName_Label);

                WebControl inputgroupUsername = new WebControl(HtmlTextWriterTag.Div);
                inputgroupUsername.CssClass = "input-group";

                inputgroupUsername.Controls.Add(new LiteralControl("<div class=\"input-group-addon\"><span class=\"glyphicon glyphicon-user\"></span></div>"));

                TextBox UserName = new TextBox();
                UserName.CssClass = "textbox form-control";
                UserName.ID = "username" + this.IDsOffset;
                //UserName.Attributes.Add("onkeydown", "javascript:if (event.keyCode == 13) __doPostBack('" + LoginButton.ID + "','')");

                inputgroupUsername.Controls.Add(UserName);
                formGroupUserName.Controls.Add(inputgroupUsername);

                loginFormGroup.Controls.Add(formGroupUserName);

                WebControl formGroupPassword = new WebControl(HtmlTextWriterTag.Div);
                formGroupPassword.CssClass = "form-group";

                Label Password_Label = new Label();
                Password_Label.Text = Labels[LoginLabels.LabelsList.Password] + ": ";
                Password_Label.ID = "Password_Label" + this.IDsOffset;
                Password_Label.CssClass = "control-label";

                formGroupPassword.Controls.Add(Password_Label);

                WebControl inputgroupPassword = new WebControl(HtmlTextWriterTag.Div);
                inputgroupPassword.CssClass = "input-group";

                inputgroupPassword.Controls.Add(new LiteralControl("<div class=\"input-group-addon\"><span class=\"glyphicon glyphicon-asterisk\"></span></div>"));

                HtmlInputPassword Password = new HtmlInputPassword();
                Password.Attributes["class"] = "textbox form-control";
                Password.ID = "password" + this.IDsOffset;
                //Password.Attributes.Add("onkeydown", "javascript:if (event.keyCode == 13) __doPostBack('" + LoginButton.ID + "','')");

                inputgroupPassword.Controls.Add(Password);

                formGroupPassword.Controls.Add(inputgroupPassword);

                loginFormGroup.Controls.Add(formGroupPassword);

            }

            WebControl formGroupFooter = new WebControl(HtmlTextWriterTag.Div);
            formGroupFooter.CssClass = "form-group text-center";

            if (Configurations.Generics.LoginMode == Configurations.Generics.LoginModeType.Mixed)
            {
                WebControl ButtonsUL = new WebControl(HtmlTextWriterTag.Ul);
                ButtonsUL.CssClass = "list-inline";

                WebControl ButtonLI = new WebControl(HtmlTextWriterTag.Li);

                ButtonLI.Controls.Add(LoginButton);

                WebControl ButtonLIAD = new WebControl(HtmlTextWriterTag.Li);

                ButtonLIAD.Controls.Add(LoginButtonAD);

                ButtonsUL.Controls.Add(ButtonLI);
                ButtonsUL.Controls.Add(ButtonLIAD);

                formGroupFooter.Controls.Add(ButtonsUL);
            }
            else
                formGroupFooter.Controls.Add(LoginButton);

            if (!ShowOnlyADCtrl)
            {
                WebControl links = new WebControl(HtmlTextWriterTag.Ul);
                links.CssClass = "list-inline";

                WebControl liLink = new WebControl(HtmlTextWriterTag.Li);

                if (ShowPasswordRecoveryLink)
                {
                    liLink = new WebControl(HtmlTextWriterTag.Li);
                    WebControl forgotPasswordLink = new WebControl(HtmlTextWriterTag.A);
                    var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
                    forgotPasswordLink.Attributes.Add("href", httpsHandler.ConvertPageAddressAsSecure("?forgot=true"));
                    forgotPasswordLink.Controls.Add(new LiteralControl(RecoverPasswordLinkLabel));
                    liLink.Controls.Add(forgotPasswordLink);
                    links.Controls.Add(liLink);
                }

                if (ShowRegistrationLink)
                {
                    liLink = new WebControl(HtmlTextWriterTag.Li);
                    WebControl showRegLink = new WebControl(HtmlTextWriterTag.A);
                    showRegLink.Attributes.Add("href", RegistrationUrl);
                    showRegLink.Controls.Add(new LiteralControl(UserRegistrationLinkLabel)); 
                    liLink.Controls.Add(showRegLink);
                    links.Controls.Add(liLink);
                }

                if (links.HasControls())
                {
                    formGroupFooter.Controls.Add(new LiteralControl("<br/>"));
                    formGroupFooter.Controls.Add(new LiteralControl("<br/>"));
                    formGroupFooter.Controls.Add(links);
                }

            }

            loginFormGroup.Controls.Add(formGroupFooter);
            

            if (this.DebugModeEnabled)
            {
                DebugLoginControlv3 DebugLogin = new DebugLoginControlv3();
                loginFormGroup.Controls.Add(DebugLogin);
            }


            panelBody.Controls.Add(loginFormGroup);

            //this.Controls.Add(panelBody);
            loginPanel.Controls.Add(panelBody);
            
            WebControl panelFooter = new WebControl(HtmlTextWriterTag.Div);
            panelFooter.CssClass = "panel-footer";
            this.Controls.Add(panelFooter);

        }

        private void InitForgotPasswordControls()
        {
            //string message = "";

            #region OLD
            //#region Costruisco i Controlli form

            //TextBox UserName = new TextBox();
            //UserName.CssClass = "textbox";
            //UserName.ID = "username" + IDsOffset;

            //Label UserName_Label = new Label();
            //UserName_Label.Text = Labels[LoginLabels.LabelsList.Username] + ": ";
            //UserName_Label.ID = "UserName_Label" + IDsOffset;
            ////UserName_Label.AssociatedControlID = UserName.ID;


            //Button ask = new Button();
            //ask.Text = RecoverPasswordButtonLabel;// Labels[LoginLabels.LabelsList.AskPassword];
            //ask.ID = "Ask" + IDsOffset;
            //ask.Click += new EventHandler(ask_Click);

            //Button reset = new Button();
            //reset.Text = ResetButtonLabel;// Labels[LoginLabels.LabelsList.Cancel];
            //reset.ID = "Undo" + IDsOffset;
            //reset.Click += new EventHandler(reset_Click);

            //#endregion

            //this.Attributes["class"] = "LoginFormContainer";

            //HtmlGenericControl info = new HtmlGenericControl("div");
            //info.Attributes["class"] = "LoginInfo";
            //info.InnerHtml = message;
            //this.Controls.Add(info);

            //Par FormContainer = new Par();
            //FormContainer.Class = "FormContainer";

            //FormContainer.Controls.Add(UserName_Label);
            //FormContainer.Controls.Add(UserName);

            //FormContainer.Controls.Add(UserName_Label);
            //FormContainer.Controls.Add(UserName);

            //this.Controls.Add(FormContainer);

            //FormContainer = new Par();
            //FormContainer.Class = "FormContainer";

            //this.Controls.Add(FormContainer);

            ////Attacco il controllo submit
            //FormContainer = new Par();

            //FormContainer.Controls.Add(ask);
            //FormContainer.Controls.Add(reset);

            //this.Controls.Add(FormContainer);


            #endregion


            WebControl panelHeader = new WebControl(HtmlTextWriterTag.Div);
            panelHeader.CssClass = "panel-heading";
                      
            WebControl panelTitle = new WebControl(HtmlTextWriterTag.H3);
            panelTitle.CssClass = "panel-Title text-center";
            panelTitle.Controls.Add(new LiteralControl(this.ForgotTitleLabel));
            
            panelHeader.Controls.Add(panelTitle);

            Controls.Add(panelHeader);
                                           
            WebControl panelBody = new WebControl(HtmlTextWriterTag.Div);
            panelBody.CssClass = "panel-body";

            panelBody.Controls.Add(NotifiyArea);   

            WebControl loginFormGroup = new WebControl(HtmlTextWriterTag.Div);
            loginFormGroup.CssClass = "login-form-group";

            WebControl formGroup = new WebControl(HtmlTextWriterTag.Div);
            formGroup.CssClass = "form-group";

            Label UserName_Label = new Label();
            UserName_Label.Text = Labels[LoginLabels.LabelsList.Username] + ": ";
            UserName_Label.ID = "UserName_Label" + IDsOffset;
            UserName_Label.CssClass = "control-label";

            formGroup.Controls.Add(UserName_Label);

            WebControl divinputgroup = new WebControl(HtmlTextWriterTag.Div);
            divinputgroup.CssClass = "input-group";

            divinputgroup.Controls.Add(new LiteralControl("<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-user\"></span></span>"));
                  
            TextBox UserName = new TextBox();
            UserName.CssClass = "textbox form-control";
            UserName.ID = "username" + IDsOffset;
            divinputgroup.Controls.Add(UserName);

            formGroup.Controls.Add(divinputgroup);

            loginFormGroup.Controls.Add(formGroup);


            WebControl formGroupFoter = new WebControl(HtmlTextWriterTag.Div);
            formGroupFoter.CssClass = "form-group text-center";

            WebControl buttons = new WebControl(HtmlTextWriterTag.Ul);
            buttons.CssClass = "list-inline";

            WebControl libuttonask = new WebControl(HtmlTextWriterTag.Li);

            Button ask = new Button();
            ask.Text = RecoverPasswordButtonLabel; 
            ask.ID = "Ask" + IDsOffset;
            ask.Click += new EventHandler(ask_Click);
            ask.CssClass = "btn btn-success";

            libuttonask.Controls.Add(ask);

            buttons.Controls.Add(libuttonask);

            WebControl libuttonreset = new WebControl(HtmlTextWriterTag.Li);

            Button reset = new Button();
            reset.Text = ResetButtonLabel; 
            reset.ID = "Undo" + IDsOffset;
            reset.Click += new EventHandler(reset_Click);
            reset.CssClass = "btn btn-default";

            libuttonreset.Controls.Add(reset);
            buttons.Controls.Add(libuttonreset);

            formGroupFoter.Controls.Add(buttons);

            loginFormGroup.Controls.Add(formGroupFoter);

            panelBody.Controls.Add(loginFormGroup);
            Controls.Add(panelBody);

            WebControl panelFooter = new WebControl(HtmlTextWriterTag.Div);
            panelFooter.CssClass = "panel-footer";
            Controls.Add(panelFooter);
        }
        #endregion

        /// <summary>
        /// Modal Body Constructor
        /// </summary>
        /// <returns>Modal Body WebControl <see cref="WebControl"/></returns>
        private WebControl InitLoginModalBody()
        {
            if (this.ForgotPasswordRequest.StringValue == "true")
            {
                WebControl divPanel = new WebControl(HtmlTextWriterTag.Div);
                divPanel.CssClass = "recovery-form-group";

                WebControl formGroup = new WebControl(HtmlTextWriterTag.Div);
                formGroup.CssClass = "form-group";
                                            
                Label UserName_Label = new Label();
                UserName_Label.Text = Labels[LoginLabels.LabelsList.Username] + ": ";
                UserName_Label.ID = "UserName_Label" + IDsOffset;
                UserName_Label.CssClass = "control-label";

                formGroup.Controls.Add(UserName_Label);

                WebControl divinputgroup = new WebControl(HtmlTextWriterTag.Div);
                divinputgroup.CssClass = "input-group";

                divinputgroup.Controls.Add(new LiteralControl("<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-user\"></span></span>"));
                           
                TextBox UserName = new TextBox();
                UserName.CssClass = "textbox form-control";
                UserName.ID = "username" + IDsOffset;
                divinputgroup.Controls.Add(UserName);

                formGroup.Controls.Add(divinputgroup);

                divPanel.Controls.Add(formGroup);

                return divPanel;
            }
            else
            {
                if (this.ForgotPasswordRequest.StringValue == "confirm")
                {
                    ConfirmChangePwd();
                    return new WebControl(HtmlTextWriterTag.Div);
                }
                else
                {
                    Panel loginPanel = new Panel();
                    loginPanel.DefaultButton = LoginButton.ID;

                    WebControl loginFormGroup = new WebControl(HtmlTextWriterTag.Div);
                    loginFormGroup.CssClass = "login-form-group";

                    if (!ShowOnlyADCtrl)
                    {
                        WebControl formGroupUserName = new WebControl(HtmlTextWriterTag.Div);
                        formGroupUserName.CssClass = "form-group";

                        Label UserName_Label = new Label();
                        UserName_Label.Text = Labels[LoginLabels.LabelsList.Username] + ": ";
                        UserName_Label.ID = "UserName_Label" + this.IDsOffset;
                        UserName_Label.CssClass = "control-label";

                        formGroupUserName.Controls.Add(UserName_Label);

                        WebControl inputgroupUsername = new WebControl(HtmlTextWriterTag.Div);
                        inputgroupUsername.CssClass = "input-group";

                        inputgroupUsername.Controls.Add(new LiteralControl("<div class=\"input-group-addon\"><span class=\"glyphicon glyphicon-user\"></span></div>"));

                        TextBox UserName = new TextBox();
                        UserName.CssClass = "textbox form-control";
                        UserName.ID = "username" + this.IDsOffset;
                        //UserName.Attributes.Add("onkeydown", "javascript:if (event.keyCode == 13) __doPostBack('" + LoginButton.ID + "','')");

                        inputgroupUsername.Controls.Add(UserName);
                        formGroupUserName.Controls.Add(inputgroupUsername);

                        loginFormGroup.Controls.Add(formGroupUserName);

                        WebControl formGroupPassword = new WebControl(HtmlTextWriterTag.Div);
                        formGroupPassword.CssClass = "form-group";

                        Label Password_Label = new Label();
                        Password_Label.Text = Labels[LoginLabels.LabelsList.Password] + ": ";
                        Password_Label.ID = "Password_Label" + this.IDsOffset;
                        Password_Label.CssClass = "control-label";

                        formGroupPassword.Controls.Add(Password_Label);

                        WebControl inputgroupPassword = new WebControl(HtmlTextWriterTag.Div);
                        inputgroupPassword.CssClass = "input-group";

                        inputgroupPassword.Controls.Add(new LiteralControl("<div class=\"input-group-addon\"><span class=\"glyphicon glyphicon-asterisk\"></span></div>"));

                        HtmlInputPassword Password = new HtmlInputPassword();
                        Password.Attributes["class"] = "textbox form-control";
                        Password.ID = "password" + this.IDsOffset;
                        //Password.Attributes.Add("onkeydown", "javascript:if (event.keyCode == 13) __doPostBack('" + LoginButton.ID + "','')");

                        inputgroupPassword.Controls.Add(Password);

                        formGroupPassword.Controls.Add(inputgroupPassword);

                        loginFormGroup.Controls.Add(formGroupPassword);
                    }

                    loginPanel.Controls.Add(loginFormGroup);

                    return loginPanel;
                }
            }       
        }

        /// <summary>
        /// Modal Footer Constructor
        /// </summary>
        /// <returns>Modal Footer WebControl <see cref="WebControl"/></returns>
        private WebControl InitLoginModalFooter()
        {
            if (this.ForgotPasswordRequest.StringValue == "true")
            {
                WebControl formGroup = new WebControl(HtmlTextWriterTag.Div);
                formGroup.CssClass = "form-group text-center";

                WebControl buttons = new WebControl(HtmlTextWriterTag.Ul);
                buttons.CssClass = "list-inline";

                WebControl libuttonask = new WebControl(HtmlTextWriterTag.Li);

                Button ask = new Button();
                ask.Text = RecoverPasswordButtonLabel;// Labels[LoginLabels.LabelsList.AskPassword];
                ask.ID = "Ask" + IDsOffset;
                ask.Click += new EventHandler(ask_Click);
                ask.CssClass = "btn btn-success";
                         
                libuttonask.Controls.Add(ask);

                buttons.Controls.Add(libuttonask);

                WebControl libuttonreset = new WebControl(HtmlTextWriterTag.Li);

                Button reset = new Button();
                reset.Text = ResetButtonLabel; // Labels[LoginLabels.LabelsList.Cancel];
                reset.ID = "Undo" + IDsOffset;
                reset.Click += new EventHandler(reset_Click);
                reset.CssClass = "btn btn-default";

                libuttonreset.Controls.Add(reset);
                buttons.Controls.Add(libuttonreset);

                formGroup.Controls.Add(buttons);
                                                
                return formGroup;
            }
            else
            {
                if (this.ForgotPasswordRequest.StringValue == "confirm")
                {
                    return new WebControl(HtmlTextWriterTag.Div);
                }
                else
                {
                    WebControl formGroup = new WebControl(HtmlTextWriterTag.Div);
                    formGroup.CssClass = "form-group text-center";
                 
                    if (Configurations.Generics.LoginMode == Configurations.Generics.LoginModeType.Mixed)
                    {
                        WebControl ButtonsUL = new WebControl(HtmlTextWriterTag.Ul);
                        ButtonsUL.CssClass = "list-inline";

                        WebControl ButtonLI = new WebControl(HtmlTextWriterTag.Li);

                        ButtonLI.Controls.Add(LoginButton);

                        WebControl ButtonLIAD = new WebControl(HtmlTextWriterTag.Li);

                        ButtonLIAD.Controls.Add(LoginButtonAD);

                        ButtonsUL.Controls.Add(ButtonLI);
                        ButtonsUL.Controls.Add(ButtonLIAD);

                        formGroup.Controls.Add(ButtonsUL);
                    }
                    else
                        formGroup.Controls.Add(LoginButton);


                    if (!ShowOnlyADCtrl)
                    {
                        WebControl links = new WebControl(HtmlTextWriterTag.Ul);
                        links.CssClass = "list-inline";

                        WebControl liLink = new WebControl(HtmlTextWriterTag.Li);

                        if (ShowPasswordRecoveryLink)
                        {
                            liLink = new WebControl(HtmlTextWriterTag.Li);
                            WebControl forgotPasswordLink = new WebControl(HtmlTextWriterTag.A);
                            var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
                            forgotPasswordLink.Attributes.Add("href", httpsHandler.ConvertPageAddressAsSecure("?forgot=true"));
                            forgotPasswordLink.Controls.Add(new LiteralControl(RecoverPasswordLinkLabel));// Labels[LoginLabels.LabelsList.PasswordRecovery]));
                            liLink.Controls.Add(forgotPasswordLink);
                            links.Controls.Add(liLink);
                        }

                        if (ShowRegistrationLink)
                        {
                            liLink = new WebControl(HtmlTextWriterTag.Li);
                            WebControl showRegLink = new WebControl(HtmlTextWriterTag.A);
                            showRegLink.Attributes.Add("href", RegistrationUrl);
                            showRegLink.Controls.Add(new LiteralControl(UserRegistrationLinkLabel));// Labels[LoginLabels.LabelsList.UserRegistration]));
                            liLink.Controls.Add(showRegLink);
                            links.Controls.Add(liLink);
                        }

                        if (links.HasControls())
                        {
                            formGroup.Controls.Add(new LiteralControl("<br/>"));
                            formGroup.Controls.Add(new LiteralControl("<br/>"));
                            formGroup.Controls.Add(links);
                        }

                    }

                    return formGroup;
                }
            }          
        }

        /// <summary>
        /// Module Constructor
        /// </summary>
        /// <returns>Module WebControl <see cref="WebControl"/></returns>
        private WebControl InitModuleLoginBodyContent()
        {
            if (this.ForgotPasswordRequest.StringValue == "true")
            {
                WebControl loginFormGroup = new WebControl(HtmlTextWriterTag.Div);
                loginFormGroup.CssClass = "login-form-group";

                WebControl formGroup = new WebControl(HtmlTextWriterTag.Div);
                formGroup.CssClass = "form-group";

                Label UserName_Label = new Label();
                UserName_Label.Text = Labels[LoginLabels.LabelsList.Username] + ": ";
                UserName_Label.ID = "UserName_Label" + IDsOffset;
                UserName_Label.CssClass = "control-label";

                formGroup.Controls.Add(UserName_Label);

                WebControl divinputgroup = new WebControl(HtmlTextWriterTag.Div);
                divinputgroup.CssClass = "input-group";

                divinputgroup.Controls.Add(new LiteralControl("<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-user\"></span></span>"));


                TextBox UserName = new TextBox();
                UserName.CssClass = "textbox form-control";
                UserName.ID = "username" + IDsOffset;
                divinputgroup.Controls.Add(UserName);

                formGroup.Controls.Add(divinputgroup);

                loginFormGroup.Controls.Add(formGroup);


                WebControl formGroupFoter = new WebControl(HtmlTextWriterTag.Div);
                formGroupFoter.CssClass = "form-group text-center";

                WebControl buttons = new WebControl(HtmlTextWriterTag.Ul);
                buttons.CssClass = "list-inline";

                WebControl libuttonask = new WebControl(HtmlTextWriterTag.Li);

                Button ask = new Button();
                ask.Text = RecoverPasswordButtonLabel; //Labels[LoginLabels.LabelsList.AskPassword];
                ask.ID = "Ask" + IDsOffset;
                ask.Click += new EventHandler(ask_Click);
                ask.CssClass = "btn btn-success";

                libuttonask.Controls.Add(ask);

                buttons.Controls.Add(libuttonask);

                WebControl libuttonreset = new WebControl(HtmlTextWriterTag.Li);

                Button reset = new Button();
                reset.Text = ResetButtonLabel; //Labels[LoginLabels.LabelsList.Cancel];
                reset.ID = "Undo" + IDsOffset;
                reset.Click += new EventHandler(reset_Click);
                reset.CssClass = "btn btn-default";

                libuttonreset.Controls.Add(reset);
                buttons.Controls.Add(libuttonreset);

                formGroupFoter.Controls.Add(buttons);

                loginFormGroup.Controls.Add(formGroupFoter);
                                                           
                return loginFormGroup;
            }
            else
            {
                if (this.ForgotPasswordRequest.StringValue == "confirm")
                {
                    ConfirmChangePwd();
                    return new WebControl(HtmlTextWriterTag.Div);
                }
                else
                {
                    Panel loginPanel = new Panel();
                    loginPanel.DefaultButton = LoginButton.ID;

                    WebControl loginFormGroup = new WebControl(HtmlTextWriterTag.Div);
                    loginFormGroup.CssClass = "login-form-group";

                    if (!ShowOnlyADCtrl)
                    {
                        WebControl formGroupUserName = new WebControl(HtmlTextWriterTag.Div);
                        formGroupUserName.CssClass = "form-group";

                        Label UserName_Label = new Label();
                        UserName_Label.Text = Labels[LoginLabels.LabelsList.Username] + ": ";
                        UserName_Label.ID = "UserName_Label" + this.IDsOffset;
                        UserName_Label.CssClass = "control-label";

                        formGroupUserName.Controls.Add(UserName_Label);

                        WebControl inputgroupUsername = new WebControl(HtmlTextWriterTag.Div);
                        inputgroupUsername.CssClass = "input-group";

                        inputgroupUsername.Controls.Add(new LiteralControl("<div class=\"input-group-addon\"><span class=\"glyphicon glyphicon-user\"></span></div>"));

                        TextBox UserName = new TextBox();
                        UserName.CssClass = "textbox form-control";
                        UserName.ID = "username" + this.IDsOffset;
                       // UserName.Attributes.Add("onkeydown", "javascript:if (event.keyCode == 13) __doPostBack('" + LoginButton.ID + "','')");

                        inputgroupUsername.Controls.Add(UserName);
                        formGroupUserName.Controls.Add(inputgroupUsername);

                        loginFormGroup.Controls.Add(formGroupUserName);

                        WebControl formGroupPassword = new WebControl(HtmlTextWriterTag.Div);
                        formGroupPassword.CssClass = "form-group";

                        Label Password_Label = new Label();
                        Password_Label.Text = Labels[LoginLabels.LabelsList.Password] + ": ";
                        Password_Label.ID = "Password_Label" + this.IDsOffset;
                        Password_Label.CssClass = "control-label";

                        formGroupPassword.Controls.Add(Password_Label);

                        WebControl inputgroupPassword = new WebControl(HtmlTextWriterTag.Div);
                        inputgroupPassword.CssClass = "input-group";

                        inputgroupPassword.Controls.Add(new LiteralControl("<div class=\"input-group-addon\"><span class=\"glyphicon glyphicon-asterisk\"></span></div>"));

                        HtmlInputPassword Password = new HtmlInputPassword();
                        Password.Attributes["class"] = "textbox form-control";
                        Password.ID = "password" + this.IDsOffset;
                        //Password.Attributes.Add("onkeydown", "javascript:if (event.keyCode == 13) __doPostBack('" + LoginButton.ID + "','')");

                        inputgroupPassword.Controls.Add(Password);

                        formGroupPassword.Controls.Add(inputgroupPassword);

                        loginFormGroup.Controls.Add(formGroupPassword);

                    }

                    WebControl formGroupFooter = new WebControl(HtmlTextWriterTag.Div);
                    formGroupFooter.CssClass = "form-group text-center";

                    if(Configurations.Generics.LoginMode == Configurations.Generics.LoginModeType.Mixed)
                    {
                        WebControl ButtonsUL = new WebControl(HtmlTextWriterTag.Ul);
                        ButtonsUL.CssClass = "list-inline";

                        WebControl ButtonLI = new WebControl(HtmlTextWriterTag.Li);

                        ButtonLI.Controls.Add(LoginButton);

                        WebControl ButtonLIAD = new WebControl(HtmlTextWriterTag.Li);

                        ButtonLIAD.Controls.Add(LoginButtonAD);

                        ButtonsUL.Controls.Add(ButtonLI);
                        ButtonsUL.Controls.Add(ButtonLIAD);

                        formGroupFooter.Controls.Add(ButtonsUL);
                    }
                    else 
                        formGroupFooter.Controls.Add(LoginButton);

                    if (!ShowOnlyADCtrl)
                    {
                        WebControl links = new WebControl(HtmlTextWriterTag.Ul);
                        links.CssClass = "list-inline";

                        WebControl liLink = new WebControl(HtmlTextWriterTag.Li);

                        if (ShowPasswordRecoveryLink)
                        {
                            liLink = new WebControl(HtmlTextWriterTag.Li);
                            WebControl forgotPasswordLink = new WebControl(HtmlTextWriterTag.A);
                            var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
                            forgotPasswordLink.Attributes.Add("href", httpsHandler.ConvertPageAddressAsSecure("?forgot=true"));
                            forgotPasswordLink.Controls.Add(new LiteralControl(RecoverPasswordLinkLabel));// Labels[LoginLabels.LabelsList.PasswordRecovery]));
                            liLink.Controls.Add(forgotPasswordLink);
                            links.Controls.Add(liLink);
                        }

                        if (ShowRegistrationLink)
                        {
                            liLink = new WebControl(HtmlTextWriterTag.Li);
                            WebControl showRegLink = new WebControl(HtmlTextWriterTag.A);
                            showRegLink.Attributes.Add("href", RegistrationUrl);
                            showRegLink.Controls.Add(new LiteralControl(UserRegistrationLinkLabel)); // Labels[LoginLabels.LabelsList.UserRegistration]));
                            liLink.Controls.Add(showRegLink);
                            links.Controls.Add(liLink);
                        }

                        if (links.HasControls())
                        {
                            formGroupFooter.Controls.Add(new LiteralControl("<br/>"));
                            formGroupFooter.Controls.Add(new LiteralControl("<br/>"));
                            formGroupFooter.Controls.Add(links);
                        }

                    }

                    loginFormGroup.Controls.Add(formGroupFooter);
                    

                    if (this.DebugModeEnabled)
                    {
                        DebugLoginControlv3 DebugLogin = new DebugLoginControlv3();
                        loginFormGroup.Controls.Add(DebugLogin);
                    }

                    loginPanel.Controls.Add(loginFormGroup);

                    return loginPanel;
                }
            }
        }

        /// <summary>
        /// Forgot Password Reset Click Event
        /// </summary>
        /// <remarks>Reset Click on ForgotPassword Control</remarks>
        /// <param name="sender"><see cref="object"/></param>
        /// <param name="e"><see cref="EventArgs"/></param>
        void reset_Click(object sender, EventArgs e)
        {
            var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
            string url = httpsHandler.GetCurrentPageAddressAsNonSecure().Replace("?forgot=true","");
            NetCms.Diagnostics.Diagnostics.Redirect(url);
        }

        /// <summary>
        /// Forgot Password Request Button Click Event
        /// </summary>
        /// <remarks>Forgot Password Request Button on ForgotPassword Control</remarks>
        /// <param name="sender"><see cref="object"/></param>
        /// <param name="e"><see cref="EventArgs"/></param>
        void ask_Click(object sender, EventArgs e)
        {
            NetUtility.RequestVariable usernameReq = new NetUtility.RequestVariable("username"+this.IDsOffset, HttpContext.Current.Request.Form);

            //DataTable Users = this.Conn.SqlQuery(" SELECT * FROM Users WHERE Deleted_User = 0 AND State_User = 1 AND Name_User = '" + user.StringValue + "'");

            User user = UsersBusinessLogic.GetActiveUserByUserName(usernameReq.StringValue);

            if (user!= null)
            {
                string hash = user.HashInfoForValidationCheck();
                var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
                string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();

                
                string urlToMail = httpsHandler.ConvertPageAddressAsSecure("http://" + ip + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/accesso.aspx?u=" + user.UserName + "&h=" + hash + "&" + ForgotPasswordRequest.Key + "=confirm");
                
                
                user.SendConfirmChangePwdMail(urlToMail);
                AddNotify(Labels[LoginLabels.LabelsList.SentRequestForChangePw], NotifyLevel.info);
            }
            else
                this.AddNotify(Labels[LoginLabels.LabelsList.NotExistingUser], NotifyLevel.danger);
        }

        private void ConfirmChangePwd()
        {
            NetUtility.RequestVariable usernameReq = new NetUtility.RequestVariable("u", HttpContext.Current.Request.QueryString);

            //DataTable Users = this.Conn.SqlQuery(" SELECT * FROM Users WHERE Deleted_User = 0 AND State_User = 1 AND Name_User = '" + user.StringValue + "'");

            User user = UsersBusinessLogic.GetActiveUserByUserName(usernameReq.StringValue);

            if (user != null)
            {
                NetUtility.RequestVariable hashReq = new NetUtility.RequestVariable("h", HttpContext.Current.Request.QueryString);
                if (hashReq.StringValue == user.HashInfoForValidationCheck())
                {
                    user.SendNewPassword();
                    var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
                    HttpContext.Current.Response.Redirect(httpsHandler.ConvertPageAddressAsSecure("?" + ForgotPasswordRequest.Key + "=done"));
                }
                else
                    this.AddNotify(Labels[LoginLabels.LabelsList.NotCorrectInputForPwReset], NotifyLevel.danger);
            }
            else
                this.AddNotify(Labels[LoginLabels.LabelsList.NotExistingUser], NotifyLevel.danger);
        }

        #endregion
    }

    /// <summary>
    /// Debug Login Control v3
    /// </summary>
    public class DebugLoginControlv3 : WebControl
    {          

        #region Property
        public string IDsOffset
        {
            get
            {
                if (_IDsOffset == null)
                    _IDsOffset = "_LoginControl" + this.LoginsControlsCount;
                return _IDsOffset;
            }
        }
        private string _IDsOffset;

        public int LoginsControlsCount
        {
            get
            {
                if (_LoginsControlsCount == 0)
                {
                    if (HttpContext.Current.Items.Contains("DebugLoginsControlsCount"))
                    {
                        _LoginsControlsCount = int.Parse(HttpContext.Current.Items["DebugLoginsControlsCount"].ToString());
                        _LoginsControlsCount++;
                        HttpContext.Current.Items["DebugLoginsControlsCount"] = _LoginsControlsCount;
                    }
                    else
                        HttpContext.Current.Items["DebugLoginsControlsCount"] = _LoginsControlsCount = 1;
                }
                return _LoginsControlsCount;
            }
        }
        private int _LoginsControlsCount;

        #endregion

        #region RequestVariable 
        public G2Core.Common.RequestVariable PostbackChecker
        {
            get
            {
                if (_PostbackChecker == null)
                    _PostbackChecker = new G2Core.Common.RequestVariable("debugloginpostback" + this.IDsOffset, G2Core.Common.RequestVariable.RequestType.Form);
                return _PostbackChecker;
            }
        }
        private G2Core.Common.RequestVariable _PostbackChecker;

        #endregion

        #region CachingObject        
        public G2Core.Caching.PersistentCacheObject<User[]> DebugLoginUsers
        {
            get
            {
                if (_DebugLoginUsers == null)
                {
                    _DebugLoginUsers = new G2Core.Caching.PersistentCacheObject<User[]>("Debug_UsersList", G2Core.Caching.StoreTypes.Application);
                    _DebugLoginUsers.ObjectBuilder = () =>
                    {
                        //using (var usersData = Conn.SqlQuery("SELECT * FROM Users WHERE Deleted_User = 0 AND State_User = 1 ORDER BY Name_User"))
                        //{
                        //    var users = usersData.Rows.Cast<DataRow>().Select(x => new User(x)).ToArray();
                        //    return users;
                        //}

                        return UsersBusinessLogic.FindAllActiveUsersOrderByUsername().ToArray();

                    };
                }
                return _DebugLoginUsers;
            }
        }
        private G2Core.Caching.PersistentCacheObject<User[]> _DebugLoginUsers;

        #endregion

        #region Label
        public static Labels Labels
        {
            get
            {
                return LabelsCache.GetTypedLabels(typeof(LoginLabels)) as Labels;
            }
        }

        /// <summary>
        /// Login Title
        /// </summary>
        public string DebugLoginTitleLabel
        {
            get { return _DebugLoginTitleLabel; }
            set { _DebugLoginTitleLabel = value; }
        }
        private string _DebugLoginTitleLabel = Labels[LoginLabels.LabelsList.LoginTitleLabel];

        /// <summary>
        /// Debug Login Button Label
        /// </summary>
        public string DebugLoginButtonLabel
        {
            get { return _DebugLoginButtonLabel; }
            set { _DebugLoginButtonLabel = value; }
        }
        private string _DebugLoginButtonLabel = Labels[LoginLabels.LabelsList.DebugAccessButtonLabel];
        #endregion

        #region Buttons
        public Button LoginButton
        {
            get
            {
                if (_LoginButton == null)
                {
                    _LoginButton = new Button();
                    _LoginButton.ID = PostbackChecker.Key;
                    _LoginButton.Text = "Login";
                    _LoginButton.Click += loginButton_Click;
                    _LoginButton.CssClass = "btn btn-default";
                    _LoginButton.Controls.Add(new LiteralControl("<span class=\"glyphicon glyphicon-cog\"></span>")); 
                }
                return _LoginButton;
            }
        }
        private Button _LoginButton;

        #endregion

        #region Constructor
        public DebugLoginControlv3() : base(HtmlTextWriterTag.Div)
        {
        }

        #endregion

        #region GrantDelegate && Grant Collection
        //private NetCms.Users.LoginControl.CollectGrantsDelegate _CollectGrants;
        //public NetCms.Users.LoginControl.CollectGrantsDelegate CollectGrants
        //{
        //    get
        //    {
        //        return _CollectGrants;
        //    }
        //    set
        //    {
        //        _CollectGrants = value;
        //    }
        //}

        private ICollection<KeyValuePair<int, bool>> CollectGrants(User _user)
        {
            ICollection<KeyValuePair<int, bool>> _FoldersCanView = new List<KeyValuePair<int, bool>>();
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                User user = UsersBusinessLogic.GetById(_user.ID,sess);
                var networks = user.AssociatedNetworks;
                foreach (var net in networks)
                {
                    var folders = FolderBusinessLogic.FindNetworkFoldersForGrantsCollection(net.Network.ID,sess);
                    foreach (var folder in folders)//net.Network.NetworkFolders.Cast<StructureFolder>().Where(x => !x.Hidden && !x.IsSystemFolder && x.Depth <= 3))
                    {
                        GrantsFinder Criteria = new FolderGrants(folder, user.Profile,sess);
                        _FoldersCanView.Add(new KeyValuePair<int, bool>(folder.ID, Criteria.NeedToShow("show").Allowed));
                    }
                }
            }
            return _FoldersCanView;
            //Thread.Sleep(60000);
        }

        #endregion
       
        #region Function

        /// <summary>
        /// Login Click Event
        /// </summary>
        /// <param name="sender"><see cref="object"/></param>
        /// <param name="e"><see cref="EventArgs"/></param>
        private void loginButton_Click(object sender, EventArgs e)
        {
            G2Core.Common.RequestVariable request = new G2Core.Common.RequestVariable("Accounts" + IDsOffset);

            //DataTable Users = this.Conn.SqlQuery(" SELECT * FROM Users WHERE id_User = " + request.IntValue);

            User user = UsersBusinessLogic.GetById(request.IntValue);

            if (user != null)
            {
                NetCms.Users.LoginResult result = NetCms.Users.AccountManager.TryLoginUser(user, "");
                if (result.Result == LoginResult.LoginResults.Logged)
                {
                    //if (CollectGrants != null)
                    //    CollectGrants(user);

                    HttpContext ctx = HttpContext.Current;
                    HttpSessionState webSession = HttpContext.Current.Session;
                    //Parallel.Invoke(() => { HttpContext.Current = ctx; CollectGrants(user); });

                    NetCms.Users.LoginControl.CollectGrantsDelegate myAction = new NetCms.Users.LoginControl.CollectGrantsDelegate(CollectGrants);
                    myAction.BeginInvoke(user, new AsyncCallback(CallbackMethod), webSession);
                                   
     


                    if (HttpContext.Current.Request.Url.PathAndQuery.EndsWith("/login.aspx"))
                       NetCms.Diagnostics.Diagnostics.Redirect("default.aspx");
                    else
                    {
                        var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
                        NetCms.Diagnostics.Diagnostics.Redirect(httpsHandler.ConvertPageAddressAsNonSecure(HttpContext.Current.Request.Url.ToString()));
                    }
                }
            }
        }

        /// <summary>
        /// Callback Method
        /// </summary>
        /// <param name="ar"><see cref="IAsyncResult"/></param>         
        static void CallbackMethod(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;
            NetCms.Users.LoginControl.CollectGrantsDelegate caller = (NetCms.Users.LoginControl.CollectGrantsDelegate)result.AsyncDelegate;
            ReturnMessage mess = (ReturnMessage) result.GetReplyMessage();

            HttpSessionState session = (HttpSessionState)ar.AsyncState;
            //ICollection<KeyValuePair<int, bool>> returnValue = caller.EndInvoke(ar);

            ICollection<KeyValuePair<int, bool>> _FoldersCanView = (List<KeyValuePair<int, bool>>)mess.ReturnValue;
            if (session != null)
            {
                User currUser = (User)session[Configurations.Generics.BackAccountSessionKey];
                if (currUser != null)
                {
                    if (session["FoldersCanViewUSER_" + currUser.ID] != null)
                    {
                        lock (session["FoldersCanViewUSER_" + currUser.ID])
                        {
                            session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                        }
                    }
                    else
                        session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                }
                //currUser.FoldersCanView = _FoldersCanView;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.CssClass = "DebugLoginControl form-group text-center";
            var users = DebugLoginUsers.Instance;

            DropDownList accounts = new DropDownList();
            accounts.ID = "Accounts" + IDsOffset;
            accounts.CssClass = "form-control";
            if (users != null)
            {
                foreach (NetCms.Users.User user in users)
                {
                    string label = user.UserName;

                    if (user.Anagrafica != null)
                        label = user.NomeCompleto + "(" + user.UserName + ")";

                    ListItem item = new ListItem(label, user.ID.ToString());
                    accounts.Items.Add(item);
                }
            }
            this.Controls.Add(accounts);
            this.Controls.Add(new LiteralControl("<br />"));
            this.Controls.Add(LoginButton);
        }

        #endregion
    }
}

