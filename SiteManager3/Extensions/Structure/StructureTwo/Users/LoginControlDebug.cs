﻿
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI.WebControls;

using NetCms.Networks.Grants;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
namespace NetCms.Users
{
    public class LoginControlDebug : LoginManager.Controls.LoginControlDebug
    {     
                   
        public LoginControlDebug()
        {

        }

        public LoginControlDebug(LoginManager.Models.LoginSetting settings)
        {
            // verificare se serve qualche settaggio

        }

        protected override ICollection<KeyValuePair<int, bool>> CollectGrants(User _user)
        {
            ICollection<KeyValuePair<int, bool>> _FoldersCanView = new List<KeyValuePair<int, bool>>();
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                User user = UsersBusinessLogic.GetById(_user.ID, sess);
                var networks = user.AssociatedNetworks;
                foreach (var net in networks)
                {
                    var folders = FolderBusinessLogic.FindNetworkFoldersForGrantsCollection(net.Network.ID, sess);
                    foreach (var folder in folders)//net.Network.NetworkFolders.Cast<StructureFolder>().Where(x => !x.Hidden && !x.IsSystemFolder && x.Depth <= 3))
                    {
                        GrantsFinder Criteria = new FolderGrants(folder, user.Profile, sess);
                        _FoldersCanView.Add(new KeyValuePair<int, bool>(folder.ID, Criteria.NeedToShow("show").Allowed));
                    }
                }
            }
            return _FoldersCanView;
            //Thread.Sleep(60000);
        }

        public override void loginButton_Click(object sender, EventArgs e)
        {
            G2Core.Common.RequestVariable request = new G2Core.Common.RequestVariable("Accounts" + IDsOffset);

            //DataTable Users = this.Conn.SqlQuery(" SELECT * FROM Users WHERE id_User = " + request.IntValue);

            User user = UsersBusinessLogic.GetById(request.IntValue);

            if (user != null)
            {
                NetCms.Users.LoginResult result = NetCms.Users.AccountManager.TryLoginUser(user, "");
                if (result.Result == LoginResult.LoginResults.Logged)
                {                    
                    HttpContext ctx = HttpContext.Current;
                    HttpSessionState webSession = HttpContext.Current.Session;                    

                    NetCms.Users.LoginControl.CollectGrantsDelegate myAction = new NetCms.Users.LoginControl.CollectGrantsDelegate(CollectGrants);
                    myAction.BeginInvoke(user, new AsyncCallback(CallbackMethod), webSession);

                    if (HttpContext.Current.Request.Url.PathAndQuery.EndsWith("/login.aspx"))
                        NetCms.Diagnostics.Diagnostics.Redirect("default.aspx");
                    else
                    {
                        var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
                        NetCms.Diagnostics.Diagnostics.Redirect(httpsHandler.ConvertPageAddressAsNonSecure(HttpContext.Current.Request.Url.ToString()));
                    }
                }
            }
        }

        static void CallbackMethod(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;
            NetCms.Users.LoginControl.CollectGrantsDelegate caller = (NetCms.Users.LoginControl.CollectGrantsDelegate)result.AsyncDelegate;
            ReturnMessage mess = (ReturnMessage)result.GetReplyMessage();

            HttpSessionState session = (HttpSessionState)ar.AsyncState;            

            ICollection<KeyValuePair<int, bool>> _FoldersCanView = (List<KeyValuePair<int, bool>>)mess.ReturnValue;
            if (session != null)
            {
                User currUser = (User)session[Configurations.Generics.BackAccountSessionKey];
                if (currUser != null)
                {
                    if (session["FoldersCanViewUSER_" + currUser.ID] != null)
                    {
                        lock (session["FoldersCanViewUSER_" + currUser.ID])
                        {
                            session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                        }
                    }
                    else
                        session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                }                
            }
        }

        public G2Core.Caching.PersistentCacheObject<User[]> DebugLoginUsers
        {
            get
            {
                if (_DebugLoginUsers == null)
                {
                    _DebugLoginUsers = new G2Core.Caching.PersistentCacheObject<User[]>("Debug_UsersList", G2Core.Caching.StoreTypes.Application);
                    _DebugLoginUsers.ObjectBuilder = () =>
                    {
                        return UsersBusinessLogic.FindAllActiveUsersOrderByUsername().ToArray();
                    };
                }
                return _DebugLoginUsers;
            }
        }
        private G2Core.Caching.PersistentCacheObject<User[]> _DebugLoginUsers;
    
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            InitLoginControls();
        }

        public override void InitLoginControls()
        {
            this.CssClass = "DebugLoginControl";
            var users = DebugLoginUsers.Instance;

            DropDownList accounts = new DropDownList();
            accounts.ID = "Accounts" + IDsOffset;
            if (users != null)
            {
                foreach (NetCms.Users.User user in users)
                {
                    string label = user.UserName;

                    if (user.Anagrafica != null)
                        label = user.NomeCompleto + "(" + user.UserName + ")";

                    ListItem item = new ListItem(label, user.ID.ToString());
                    accounts.Items.Add(item);
                }
            }
            this.Controls.Add(accounts);
            this.Controls.Add(LoginButton);

        }
    }
}
