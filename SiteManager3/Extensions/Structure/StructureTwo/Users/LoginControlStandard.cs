﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using HtmlGenericControls;
using NetCms.Networks.Grants;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Threading;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Remoting.Messaging;
using NHibernate;
using System.Web.SessionState;
using LabelsManager;
using NetUtility;

/// <summary>
/// LoginControl v2: questa login permette di esporre i bottoni della form. E' possibile cambiare label dei bottoni e gestire le funzioni onclick direttamente dalla classe istanziante.
/// </summary>
namespace NetCms.Users
{
    public partial class LoginControlStandard : LoginManager.Controls.LoginControl
    {
        public override string IDsOffset
        {
            get
            {
                if (_IDsOffset == null)
                    _IDsOffset = "_LoginControl" + this.LoginsControlsCount;
                return _IDsOffset;
            }
        }
        private string _IDsOffset;
        
        private int LoginErrorCount
        {
            get
            {
                return  (HttpContext.Current.Session["LoginTry"] != null)?(int)HttpContext.Current.Session["LoginTry"]:0;
            }
        }

        public LoginControlStandard()
        {          
        }

        public LoginControlStandard(string redirectURL, bool mostraRegistrazione = false, bool mostraRecuperoPassword = false, string linkReg = "")         
        {
            this.RedirectUrl = redirectURL;
            this.CssClass = "LoginControl";
            this.MostraRegistrazione = mostraRegistrazione;
            this.MostraRecuperoPassword = mostraRecuperoPassword;
            this.LinkRegistrazione = linkReg;
        }

        public LoginControlStandard(LoginManager.Models.LoginSetting settings)
        {
            CssClass = "LoginControl";

            RedirectUrl = settings.RedirectUrl;
            MostraRegistrazione = settings.ShowRegistrationLink;
            MostraRecuperoPassword = settings.ShowPasswordRecoveryLink;
            LinkRegistrazione = settings.RegistrationtUrl;

            DebugModeEnabled = settings.ShowDebugControl || NetCms.Configurations.Debug.DebugLoginEnabled;
        }

        //private bool isHeadLogin = false;
        //public bool IsHeadLogin
        //{
        //    get { return isHeadLogin; }
        //    set { isHeadLogin = value; }
        //}


        //public override Button LoginButton
        //{
        //    get
        //    {
        //        if (_LoginButton == null)
        //        {
        //            _LoginButton = new Button();
        //            _LoginButton.ID = PostbackChecker.Key;
        //            _LoginButton.Text = this.LoginButtonLabel;
        //            _LoginButton.Click += loginButton_Click;
        //        }
        //        return _LoginButton;
        //    }

        //    set
        //    {
        //        _LoginButton = value;
        //    }
        //}
        //private Button _LoginButton;

        //public override Button RecoverPasswordButton
        //{
        //    get
        //    {
        //        if (_RecoverPasswordButton == null)
        //        {
        //            _RecoverPasswordButton = new Button();
        //            _RecoverPasswordButton.ID = "Ask" + IDsOffset;
        //            _RecoverPasswordButton.Text = this.RecoverPasswordButtonLabel;
        //            _RecoverPasswordButton.Click += ask_Click;
        //            /*if (Configurations.Generics.IsHttps)
        //            _LoginButton.PostBackUrl = "https://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl;                    */
        //        }
        //        return _RecoverPasswordButton;
        //    }
        //    set { _RecoverPasswordButton = value;}
        //}
        //private Button _RecoverPasswordButton;

        //public override Button ResetButton
        //{
        //    get
        //    {
        //        if (_ResetButton == null)
        //        {
        //            _ResetButton = new Button();
        //            _ResetButton.ID = "Undo" + IDsOffset;
        //            _ResetButton.Text = this.ResetButtonLabel;
        //            _ResetButton.Click += reset_Click;
        //            /*if (Configurations.Generics.IsHttps)
        //            _LoginButton.PostBackUrl = "https://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl;                    */
        //        }
        //        return _ResetButton;
        //    }
        //}
        //private Button _ResetButton;

        //public override WebControl NotifiyList
        //{
        //    get
        //    {
        //        if (_NotifiyList == null)
        //        {
        //            _NotifiyList = new WebControl(HtmlTextWriterTag.Ul);


        //        }
        //        return _NotifiyList;
        //    }
        //}
        //private WebControl _NotifiyList;

        //public override WebControl NotifiyArea
        //{
        //    get
        //    {
        //        if (_NotifiyArea == null)
        //        {
        //            _NotifiyArea = new WebControl(HtmlTextWriterTag.Div);
        //            _NotifiyArea.CssClass = "NotifyArea";
        //            if (HttpContext.Current.Session != null)                    
        //            {
        //                if(HttpContext.Current.Session["AnotherBrowserLogged"] != null)
        //                {
        //                    AddNotify(Labels[LoginLabels.LabelsList.AccessFromOtherBrowser]);
        //                    HttpContext.Current.Session.Remove("AnotherBrowserLogged");
        //                }
        //                if (HttpContext.Current.Session["SSOUSER_NOTENABLED"] != null && HttpContext.Current.Session["SSOUSER_NOTENABLED"]=="0")
        //                {
        //                    AddNotify(Labels[LoginLabels.LabelsList.NotGrantedUser]);
        //                    HttpContext.Current.Session["SSOUSER_NOTENABLED"] = "1";
        //                }                       
        //            }
                    
        //        }
        //        return _NotifiyArea;
        //    }
        //}
        //private WebControl _NotifiyArea;

        //public bool NotifySent
        //{
        //    get
        //    {
        //        return _NotifySent;
        //    }
        //    private set
        //    {
        //        _NotifySent = value;
        //    }
        //}
        //private bool _NotifySent;

        //public delegate ICollection<KeyValuePair<int, bool>> CollectGrantsDelegate(User user);

        //private CollectGrantsDelegate _CollectGrants;
        //public CollectGrantsDelegate CollectGrants
        //{
        //    get
        //    {
        //        if (_CollectGrants == null)
        //        {
                    
        //            _CollectGrants = delegate(User user)
        //            {
                        
        //                var networks = user.AssociatedNetworks;
        //                foreach (var net in networks)
        //                {
        //                    var folders = FolderBusinessLogic.FindNetworkFoldersForGrantsCollection(net.Network.ID);
        //                    foreach (var folder in folders)//net.Network.NetworkFolders.Cast<StructureFolder>().Where(x => !x.Hidden && !x.IsSystemFolder && x.Depth <= 3))
        //                    {
        //                        GrantsFinder Criteria = new FolderGrants(folder, user.Profile);
        //                        user.FoldersCanView.Add(new KeyValuePair<int, bool>(folder.ID, Criteria.NeedToShow("show").Allowed));
        //                    }
        //                }
        //            };
        //        }
        //        return _CollectGrants;
        //    }
        //    set 
        //    {
        //        _CollectGrants = value;
        //    }
        //}

        //private void CollectGrants(User user)
        //{
            
        //        var networks = user.AssociatedNetworks;
        //        foreach (var net in networks)
        //        {
        //            var folders = FolderBusinessLogic.FindNetworkFoldersForGrantsCollection(net.Network.ID);
        //            foreach (var folder in folders)//net.Network.NetworkFolders.Cast<StructureFolder>().Where(x => !x.Hidden && !x.IsSystemFolder && x.Depth <= 3))
        //            {
        //                GrantsFinder Criteria = new FolderGrants(folder, user.Profile);
        //                user.FoldersCanView.Add(new KeyValuePair<int, bool>(folder.ID, Criteria.NeedToShow("show").Allowed));
        //            }
        //        }
            
        //}

        protected override ICollection<KeyValuePair<int, bool>> CollectGrants(User _user)
        {
            ICollection<KeyValuePair<int, bool>> _FoldersCanView = new List<KeyValuePair<int, bool>>();
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                User user = UsersBusinessLogic.GetById(_user.ID, sess);
                var networks = user.AssociatedNetworks;
                foreach (var net in networks)
                {
                    var folders = FolderBusinessLogic.FindNetworkFoldersForGrantsCollection(net.Network.ID, sess);
                    foreach (var folder in folders)//net.Network.NetworkFolders.Cast<StructureFolder>().Where(x => !x.Hidden && !x.IsSystemFolder && x.Depth <= 3))
                    {
                        GrantsFinder Criteria = new FolderGrants(folder, user.Profile, sess);
                        _FoldersCanView.Add(new KeyValuePair<int, bool>(folder.ID, Criteria.NeedToShow("show").Allowed));
                    }
                }
            }
            return _FoldersCanView;
        }

        static void CallbackMethod(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;
            NetCms.Users.LoginControl.CollectGrantsDelegate caller = (NetCms.Users.LoginControl.CollectGrantsDelegate)result.AsyncDelegate;
            ReturnMessage mess = (ReturnMessage)result.GetReplyMessage();

            HttpSessionState session = (HttpSessionState)ar.AsyncState;
            //ICollection<KeyValuePair<int, bool>> returnValue = caller.EndInvoke(ar);

            ICollection<KeyValuePair<int, bool>> _FoldersCanView = (List<KeyValuePair<int, bool>>)mess.ReturnValue;
            if (session != null)
            {
                User currUser = (User)session[Configurations.Generics.BackAccountSessionKey];
                if (currUser != null)
                {
                    if (session["FoldersCanViewUSER_" + currUser.ID] != null)
                    {
                        lock (session["FoldersCanViewUSER_" + currUser.ID])
                        {
                            session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                        }
                    }
                    else
                        session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                }
                //currUser.FoldersCanView = _FoldersCanView;
            }
        }

        public override void loginButton_Click(object sender, EventArgs e)
        {
            NetUtility.RequestVariable userName = new NetUtility.RequestVariable("username" + this.IDsOffset, HttpContext.Current.Request.Form);
            NetUtility.RequestVariable password = new NetUtility.RequestVariable("password" + this.IDsOffset, HttpContext.Current.Request.Form);

            if (userName.IsValid() && password.IsValid() && password.StringValue.Length>0)
            {
                string MD5Pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password.StringValue, "MD5").ToString();
              
                User user = UsersBusinessLogic.GetActiveUserByUserNameAndPassword(userName.StringValue, MD5Pwd);

                if (user != null)
                {
                    NetCms.Users.LoginResult result = NetCms.Users.AccountManager.TryLoginUser(user, MD5Pwd);
                    if (result.Result == LoginResult.LoginResults.Logged)
                    {
                        //if (CollectGrants != null)
                        //    CollectGrants(user);

                        HttpSessionState webSession = HttpContext.Current.Session;
                        
                        NetCms.Users.LoginControl.CollectGrantsDelegate myAction = new NetCms.Users.LoginControl.CollectGrantsDelegate(CollectGrants);
                        myAction.BeginInvoke(user, new AsyncCallback(CallbackMethod), webSession);
                        
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente '" + userName.StringValue + "' ha effettuato il Login al sistema.");
                        var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();

                        //if (RedirectUrl.Length == 0)
                        //    RedirectUrl = HttpContext.Current.Request.RawUrl;

                        #region CUSTOM REDIRECT URL
                        if (!string.IsNullOrEmpty(NetCms.Configurations.Generics.ExternalAppQueryParam))
                        {
                            RequestVariable redirectVar = new RequestVariable(NetCms.Configurations.Generics.ExternalAppQueryParam, RequestVariable.RequestType.QueryString);
                            if (redirectVar != null && !string.IsNullOrEmpty(redirectVar.StringValue))
                            {
                                bool redirectFlag = redirectVar.StringValue == "1";                                
                                if (redirectFlag)
                                {
                                    RedirectUrl = NetCms.Configurations.Generics.ExternalAppUrl;
                                }                                
                            }
                        }

                        if (RedirectUrl.Length == 0)
                            RedirectUrl = HttpContext.Current.Request.RawUrl;

                        /// Redirect to page
                        /// 
                        //Diagnostics.Diagnostics.Redirect(urlPage);
                        #endregion

                        NetCms.Diagnostics.Diagnostics.Redirect(httpsHandler.ConvertPageAddressAsNonSecure(RedirectUrl));
                    }
                    else
                    {
                        AddLoginTry(userName.StringValue);
                        AddNotify(Labels[LoginLabels.LabelsList.YourAccountWasDisabled], LoginManager.Models.NotifyLevel.danger);
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'account dell'utente '" + userName.StringValue + "' è stato disabilitato a causa della scadenza dei 180 giorni dall'accesso.", "", "", "");
                    }
                }
                else
                {
                    AddLoginTry(userName.StringValue);
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "Tentativo di login al sistema fallito con il seguente nome utente: '" + userName.StringValue + "' ha effettuato il Login al sistema.", "", "", "");
                    AddNotify(Labels[LoginLabels.LabelsList.NotValidUserAndPass], LoginManager.Models.NotifyLevel.danger);
                }
            }
            else
            {
                AddLoginTry(userName.StringValue);
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "Tentativo di login al sistema fallito con il seguente nome utente: '" + userName.StringValue + "' ha effettuato il Login al sistema.", "", "", "");
                AddNotify(Labels[LoginLabels.LabelsList.NotValidUserAndPass], LoginManager.Models.NotifyLevel.danger);
            }   
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);            
        }

        /*************************************/
        /* controllo per il recupero password*/
        /*************************************/

        public override void InitForgotPasswordControls()
        {
            string message = "";

            #region Costruisco i Controlli form

            TextBox UserName = new TextBox();
            UserName.CssClass = "textbox";
            UserName.ID = "username" + IDsOffset;

            Label UserName_Label = new Label();
            UserName_Label.Text = Labels[LoginLabels.LabelsList.Username] + ": ";
            UserName_Label.ID = "UserName_Label" + IDsOffset;
            UserName_Label.AssociatedControlID = UserName.ID;

            #endregion
            
            this.Attributes["class"] = "LoginFormContainer";

            HtmlGenericControl info = new HtmlGenericControl("div");
            info.Attributes["class"] = "LoginInfo";
            info.InnerHtml = message;
            this.Controls.Add(info);

            Par FormContainer = new Par();
            FormContainer.Class = "FormContainer";

            FormContainer.Controls.Add(UserName_Label);
            FormContainer.Controls.Add(UserName);        

            this.Controls.Add(FormContainer);
            
            FormContainer = new Par();
            FormContainer.Class = "FormContainer";

            this.Controls.Add(FormContainer);

            //Attacco il controllo submit
            FormContainer = new Par();

            FormContainer.Controls.Add(RecoverPasswordButton);
            FormContainer.Controls.Add(ResetButton);

            this.Controls.Add(FormContainer);
        }

        public override void reset_Click(object sender, EventArgs e)
        {
            var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
            string url = httpsHandler.GetCurrentPageAddressAsNonSecure().Replace("?forgot=true","");
            NetCms.Diagnostics.Diagnostics.Redirect(url);
        }

        public override void ask_Click(object sender, EventArgs e)
        {
            NetUtility.RequestVariable usernameReq = new NetUtility.RequestVariable("username"+this.IDsOffset, HttpContext.Current.Request.Form);
            if (usernameReq.IsValidString)
            {
                User user = UsersBusinessLogic.GetActiveUserByUserName(usernameReq.StringValue);

                if (user != null)
                {
                    string hash = user.HashInfoForValidationCheck();
                    var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
                    string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();
                    string urlToMail = httpsHandler.ConvertPageAddressAsSecure("http://" + ip + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/accesso.aspx?u=" + user.UserName + "&h=" + hash + "&" + ForgotPasswordRequest.Key + "=confirm");
                    user.SendConfirmChangePwdMail(urlToMail);
                    this.AddNotify(Labels[LoginLabels.LabelsList.SentRequestForChangePw], LoginManager.Models.NotifyLevel.info);
                }
                else
                {
                    //anche se l'utente non è stato trovato espongo un messaggio positivo per evitare di dare informazioni ad eventuali hacker

                    //this.AddNotify(Labels[LoginLabels.LabelsList.NotExistingUser], LoginManager.Models.NotifyLevel.danger);
                    this.AddNotify(Labels[LoginLabels.LabelsList.SentRequestForChangePw], LoginManager.Models.NotifyLevel.success);
                }
            }
            else
                this.AddNotify(Labels[LoginLabels.LabelsList.UserIsRequired], LoginManager.Models.NotifyLevel.danger);
        }

        public override void ConfirmChangePwd()
        {
            NetUtility.RequestVariable usernameReq = new NetUtility.RequestVariable("u", HttpContext.Current.Request.QueryString);

            User user = UsersBusinessLogic.GetActiveUserByUserName(usernameReq.StringValue);

            if (user != null)
            {
                NetUtility.RequestVariable hashReq = new NetUtility.RequestVariable("h", HttpContext.Current.Request.QueryString);
                if (hashReq.StringValue == user.HashInfoForValidationCheck())
                {
                    user.SendNewPassword();
                    var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
                    HttpContext.Current.Response.Redirect(httpsHandler.ConvertPageAddressAsSecure(HttpContext.Current.Request.Url.AbsolutePath + "?" + ForgotPasswordRequest.Key + "=done"));
                }
                else
                    this.AddNotify(Labels[LoginLabels.LabelsList.NotCorrectInputForPwReset], LoginManager.Models.NotifyLevel.danger);
            }
            else
                this.AddNotify(Labels[LoginLabels.LabelsList.NotExistingUser], LoginManager.Models.NotifyLevel.danger);
        }
    }    
}

