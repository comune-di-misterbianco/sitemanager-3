﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using HtmlGenericControls;
using NetCms.Networks.Grants;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Threading;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Remoting.Messaging;
using NHibernate;
using System.Web.SessionState;
using LabelsManager;
using NetCms.Connections;

/// <summary>
/// LoginControl v2: questa login permette di esporre i bottoni della form. E' possibile cambiare label dei bottoni e gestire le funzioni onclick direttamente dalla classe istanziante.
/// </summary>
namespace NetCms.Users
{
    public partial class LoginControlv2 : WebControl
    {
        public static Labels Labels
        {
            get
            {
                return LabelsCache.GetTypedLabels(typeof(LoginLabels)) as Labels;
            }
        }

        public string LoginButtonLabel
        {
            get { return _LoginButtonLabel; }
            set { _LoginButtonLabel = value; }
        }
        private string _LoginButtonLabel = Labels[LoginLabels.LabelsList.AccessButtonLabel];

        public string RecoverPasswordButtonLabel
        {
            get { return _RecoverPasswordButtonLabel; }
            set { _RecoverPasswordButtonLabel = value; }
        }
        private string _RecoverPasswordButtonLabel = Labels[LoginLabels.LabelsList.AskPassword];

        public string ResetButtonLabel
        {
            get { return _ResetButtonLabel; }
            set { _ResetButtonLabel = value; }
        }
        private string _ResetButtonLabel = Labels[LoginLabels.LabelsList.Cancel];

        public string RecoverPasswordLinkLabel
        {
            get { return _RecoverPasswordLinkLabel; }
            set { _RecoverPasswordLinkLabel = value; }
        }
        private string _RecoverPasswordLinkLabel = Labels[LoginLabels.LabelsList.PasswordRecovery];


        public string UserRegistrationLinkLabel
        {
            get { return _UserRegistrationLinkLabel; }
            set { _UserRegistrationLinkLabel = value; }
        }
        private string _UserRegistrationLinkLabel = Labels[LoginLabels.LabelsList.UserRegistration];


        public string IDsOffset
        {
            get
            {
                if (_IDsOffset == null)
                    _IDsOffset = "_LoginControl" + this.LoginsControlsCount;
                return _IDsOffset;
            }
        }
        private string _IDsOffset;
        
        public G2Core.Common.RequestVariable ForgotPasswordRequest
        {
            get
            {
                if (_ForgotPasswordRequest == null)
                    _ForgotPasswordRequest = new G2Core.Common.RequestVariable("forgot", G2Core.Common.RequestVariable.RequestType.QueryString);
                return _ForgotPasswordRequest;
            }
        }
        private G2Core.Common.RequestVariable _ForgotPasswordRequest;

        private bool DebugModeEnabled
        {
            get
            {
                return NetCms.Configurations.Debug.DebugLoginEnabled;
            }
        }
        
        //Indicizza il numero di controlli login aggiunti alla pagina.
        public int LoginsControlsCount
        {
            get
            {
                if (_LoginsControlsCount == 0)
                {
                    if (HttpContext.Current.Items.Contains("LoginsControlsCount"))
                    {
                        _LoginsControlsCount = int.Parse(HttpContext.Current.Items["LoginsControlsCount"].ToString());
                        _LoginsControlsCount++;
                        HttpContext.Current.Items["LoginsControlsCount"] = _LoginsControlsCount;
                    }
                    else
                        HttpContext.Current.Items["LoginsControlsCount"] = _LoginsControlsCount = 1;
                }
                return _LoginsControlsCount;
            }
        }
        private int _LoginsControlsCount;

        public G2Core.Common.RequestVariable PostbackChecker
        {
            get
            {
                if (_PostbackChecker == null)
                    _PostbackChecker = new G2Core.Common.RequestVariable("loginpostback" + this.IDsOffset,G2Core.Common.RequestVariable.RequestType.Form);
                return _PostbackChecker;
            }
        }
        private G2Core.Common.RequestVariable _PostbackChecker;
        
        public string  RedirectUrl
        {
            get { return _RedirectUrl; }
            private set { _RedirectUrl = value; }
        }
        private string  _RedirectUrl;
        			

        private int LoginErrorCount
        {
            get
            {
                return  (HttpContext.Current.Session["LoginTry"] != null)?(int)HttpContext.Current.Session["LoginTry"]:0;
            }
        }

        private bool MostraRegistrazione;
        private bool MostraRecuperoPassword;
        private string LinkRegistrazione;

        public LoginControlv2(): this("")
        {
            this.RedirectUrl = HttpContext.Current.Request.Url.ToString();
        }

        public LoginControlv2(string redirectURL, bool mostraRegistrazione = false, bool mostraRecuperoPassword = false, string linkReg = "")
            : base(HtmlTextWriterTag.Div)
        {
            this.RedirectUrl = redirectURL;
            this.CssClass = "LoginControl";
            this.MostraRegistrazione = mostraRegistrazione;
            this.MostraRecuperoPassword = mostraRecuperoPassword;
            this.LinkRegistrazione = linkReg;
        }

        private bool isHeadLogin = false;
        public bool IsHeadLogin
        {
            get { return isHeadLogin; }
            set { isHeadLogin = value; }
        }
        
        
        public Button LoginButton
        {
            get
            {
                if (_LoginButton == null)
                {
                    _LoginButton = new Button();
                    _LoginButton.ID = PostbackChecker.Key;
                    _LoginButton.Text = this.LoginButtonLabel;
                    _LoginButton.Click += loginButton_Click;
                    /*if (Configurations.Generics.IsHttps)
                    _LoginButton.PostBackUrl = "https://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl;                    */
                }
                return _LoginButton;
            }

            set
            {
                _LoginButton = value;
            }
        }
        private Button _LoginButton;

        public Button RecoverPasswordButton
        {
            get
            {
                if (_RecoverPasswordButton == null)
                {
                    _RecoverPasswordButton = new Button();
                    _RecoverPasswordButton.ID = "Ask" + IDsOffset;
                    _RecoverPasswordButton.Text = this.RecoverPasswordButtonLabel;
                    _RecoverPasswordButton.Click += ask_Click;
                    /*if (Configurations.Generics.IsHttps)
                    _LoginButton.PostBackUrl = "https://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl;                    */
                }
                return _RecoverPasswordButton;
            }
            set { _RecoverPasswordButton = value;}
        }
        private Button _RecoverPasswordButton;

        public Button ResetButton
        {
            get
            {
                if (_ResetButton == null)
                {
                    _ResetButton = new Button();
                    _ResetButton.ID = "Undo" + IDsOffset;
                    _ResetButton.Text = this.ResetButtonLabel;
                    _ResetButton.Click += reset_Click;
                    /*if (Configurations.Generics.IsHttps)
                    _LoginButton.PostBackUrl = "https://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl;                    */
                }
                return _ResetButton;
            }
        }
        private Button _ResetButton;

        public WebControl NotifiyList
        {
            get
            {
                if (_NotifiyList == null)
                {
                    _NotifiyList = new WebControl(HtmlTextWriterTag.Ul);


                }
                return _NotifiyList;
            }
        }
        private WebControl _NotifiyList;

        public WebControl NotifiyArea
        {
            get
            {
                if (_NotifiyArea == null)
                {
                    _NotifiyArea = new WebControl(HtmlTextWriterTag.Div);
                    _NotifiyArea.CssClass = "NotifyArea";
                    if (HttpContext.Current.Session != null)                    
                    {
                        if(HttpContext.Current.Session["AnotherBrowserLogged"] != null)
                        {
                            AddNotify(Labels[LoginLabels.LabelsList.AccessFromOtherBrowser]);
                            HttpContext.Current.Session.Remove("AnotherBrowserLogged");
                        }
                        if (HttpContext.Current.Session["SSOUSER_NOTENABLED"] != null && HttpContext.Current.Session["SSOUSER_NOTENABLED"]=="0")
                        {
                            AddNotify(Labels[LoginLabels.LabelsList.NotGrantedUser]);
                            HttpContext.Current.Session["SSOUSER_NOTENABLED"] = "1";
                        }                       
                    }
                    
                }
                return _NotifiyArea;
            }
        }
        private WebControl _NotifiyArea;

        public bool NotifySent
        {
            get
            {
                return _NotifySent;
            }
            private set
            {
                _NotifySent = value;
            }
        }
        private bool _NotifySent;

        public delegate ICollection<KeyValuePair<int, bool>> CollectGrantsDelegate(User user);

        //private CollectGrantsDelegate _CollectGrants;
        //public CollectGrantsDelegate CollectGrants
        //{
        //    get
        //    {
        //        if (_CollectGrants == null)
        //        {
                    
        //            _CollectGrants = delegate(User user)
        //            {
                        
        //                var networks = user.AssociatedNetworks;
        //                foreach (var net in networks)
        //                {
        //                    var folders = FolderBusinessLogic.FindNetworkFoldersForGrantsCollection(net.Network.ID);
        //                    foreach (var folder in folders)//net.Network.NetworkFolders.Cast<StructureFolder>().Where(x => !x.Hidden && !x.IsSystemFolder && x.Depth <= 3))
        //                    {
        //                        GrantsFinder Criteria = new FolderGrants(folder, user.Profile);
        //                        user.FoldersCanView.Add(new KeyValuePair<int, bool>(folder.ID, Criteria.NeedToShow("show").Allowed));
        //                    }
        //                }
        //            };
        //        }
        //        return _CollectGrants;
        //    }
        //    set 
        //    {
        //        _CollectGrants = value;
        //    }
        //}

        //private void CollectGrants(User user)
        //{
            
        //        var networks = user.AssociatedNetworks;
        //        foreach (var net in networks)
        //        {
        //            var folders = FolderBusinessLogic.FindNetworkFoldersForGrantsCollection(net.Network.ID);
        //            foreach (var folder in folders)//net.Network.NetworkFolders.Cast<StructureFolder>().Where(x => !x.Hidden && !x.IsSystemFolder && x.Depth <= 3))
        //            {
        //                GrantsFinder Criteria = new FolderGrants(folder, user.Profile);
        //                user.FoldersCanView.Add(new KeyValuePair<int, bool>(folder.ID, Criteria.NeedToShow("show").Allowed));
        //            }
        //        }
            
        //}

        private ICollection<KeyValuePair<int, bool>> CollectGrants(User _user)
        {
            ICollection<KeyValuePair<int, bool>> _FoldersCanView = new List<KeyValuePair<int, bool>>();
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                User user = UsersBusinessLogic.GetById(_user.ID, sess);
                var networks = user.AssociatedNetworks;
                foreach (var net in networks)
                {
                    var folders = FolderBusinessLogic.FindNetworkFoldersForGrantsCollection(net.Network.ID, sess);
                    foreach (var folder in folders)//net.Network.NetworkFolders.Cast<StructureFolder>().Where(x => !x.Hidden && !x.IsSystemFolder && x.Depth <= 3))
                    {
                        GrantsFinder Criteria = new FolderGrants(folder, user.Profile, sess);
                        _FoldersCanView.Add(new KeyValuePair<int, bool>(folder.ID, Criteria.NeedToShow("show").Allowed));
                    }
                }
            }
            return _FoldersCanView;
        }

        static void CallbackMethod(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;
            NetCms.Users.LoginControl.CollectGrantsDelegate caller = (NetCms.Users.LoginControl.CollectGrantsDelegate)result.AsyncDelegate;
            ReturnMessage mess = (ReturnMessage)result.GetReplyMessage();

            HttpSessionState session = (HttpSessionState)ar.AsyncState;
            //ICollection<KeyValuePair<int, bool>> returnValue = caller.EndInvoke(ar);

            ICollection<KeyValuePair<int, bool>> _FoldersCanView = (List<KeyValuePair<int, bool>>)mess.ReturnValue;
            if (session != null)
            {
                User currUser = (User)session[Configurations.Generics.BackAccountSessionKey];
                if (currUser != null)
                {
                    if (session["FoldersCanViewUSER_" + currUser.ID] != null)
                    {
                        lock (session["FoldersCanViewUSER_" + currUser.ID])
                        {
                            session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                        }
                    }
                    else
                        session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                }
                //currUser.FoldersCanView = _FoldersCanView;
            }
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            NetUtility.RequestVariable userName = new NetUtility.RequestVariable("username" + this.IDsOffset, HttpContext.Current.Request.Form);
            NetUtility.RequestVariable password = new NetUtility.RequestVariable("password" + this.IDsOffset, HttpContext.Current.Request.Form);

            if (userName.IsValid() && password.IsValid() && password.StringValue.Length>0)
            {
                string MD5Pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password.StringValue, "MD5").ToString();

                //DataTable Users = this.Conn.SqlQuery(" SELECT * FROM Users WHERE Deleted_User = 0 AND State_User = 1 AND Name_User = '" + user.StringValue + "' AND (Password_User = '" + MD5Pwd + "' OR NewPassword_User = '" + MD5Pwd + "')");

                User user = UsersBusinessLogic.GetActiveUserByUserNameAndPassword(userName.StringValue, MD5Pwd);

                if (user != null)
                {
                    NetCms.Users.LoginResult result = NetCms.Users.AccountManager.TryLoginUser(user, MD5Pwd);
                    if (result.Result == LoginResult.LoginResults.Logged)
                    {
                        //if (CollectGrants != null)
                        //    CollectGrants(user);

                        HttpSessionState webSession = HttpContext.Current.Session;
                        
                        NetCms.Users.LoginControl.CollectGrantsDelegate myAction = new NetCms.Users.LoginControl.CollectGrantsDelegate(CollectGrants);
                        myAction.BeginInvoke(user, new AsyncCallback(CallbackMethod), webSession);
                        
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente '" + userName.StringValue + "' ha effettuato il Login al sistema.");
                        var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
                        
                        if (RedirectUrl.Length == 0)
                            RedirectUrl = "/";
                        
                        NetCms.Diagnostics.Diagnostics.Redirect(httpsHandler.ConvertPageAddressAsNonSecure(RedirectUrl));
                    }
                    else
                    {
                        AddLoginTry(userName.StringValue);
                        AddNotify(Labels[LoginLabels.LabelsList.YourAccountWasDisabled]);
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'account dell'utente '" + userName.StringValue + "' è stato disabilitato a causa della scadenza dei 180 giorni dall'accesso.", "", "", "");
                    }
                }
                else
                {
                    AddLoginTry(userName.StringValue);
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "Tentativo di login al sistema fallito con il seguente nome utente: '" + userName.StringValue + "' ha effettuato il Login al sistema.", "", "", "");
                    AddNotify(Labels[LoginLabels.LabelsList.NotValidUserAndPass]);
                }
            }
            else
            {
                AddLoginTry(userName.StringValue);
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "Tentativo di login al sistema fallito con il seguente nome utente: '" + userName.StringValue + "' ha effettuato il Login al sistema.", "", "", "");
                AddNotify(Labels[LoginLabels.LabelsList.NotValidUserAndPass]);
            }   
        }

        public void AddNotify(string notify)
        {
            WebControl li = new WebControl(HtmlTextWriterTag.Li);
            li.Controls.Add(new LiteralControl(notify));
            this.NotifiyList.Controls.Add(li);
        }

        private void AddLoginTry(string userName)
        {
            if (HttpContext.Current.Session["LoginTry"] != null)
            {
                int exval = (int)HttpContext.Current.Session["LoginTry"];
                HttpContext.Current.Session["LoginTry"] = ++exval;
            }
            else
                HttpContext.Current.Session["LoginTry"] = 1;

            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "L'utente '" + userName + "' ha tentato di accedere al sistema senza successo (tot. " + HttpContext.Current.Session["LoginTry"] + " volte consecutive)","","","");
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.ID = "LoginControl" + this.LoginsControlsCount;            

            this.Controls.Add(NotifiyArea);

            if (this.ForgotPasswordRequest.StringValue == "done")
                this.AddNotify(Labels[LoginLabels.LabelsList.NewPasswordSentByMail]);
            if (this.ForgotPasswordRequest.StringValue == "true")
                InitForgotPasswordControls();
            else
            {
                if (this.ForgotPasswordRequest.StringValue == "confirm")
                    ConfirmChangePwd();
                InitLoginControls();
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
    
            if (NotifiyList.Controls.Count > 0)
            {
                NotifiyArea.Controls.Add(this.NotifiyList);
                if (IsHeadLogin){
                    HiddenField loginTest = new HiddenField();
                    loginTest.ID = "loginTest" + this.ID;
                    loginTest.Value = "loginError";
                    this.Controls.Add(loginTest);
                }
            }   
        }

        private void InitLoginControls()
        {
            #region Costruisco i Controlli form i label e password

            Label UserName_Label = new Label();
            UserName_Label.Text = Labels[LoginLabels.LabelsList.Username] + ": ";
            UserName_Label.ID = "UserName_Label" + this.IDsOffset;
            UserName_Label.AssociatedControlID = "username" + this.IDsOffset;

            Label Password_Label = new Label();
            Password_Label.Text = Labels[LoginLabels.LabelsList.Password] + ": ";
            Password_Label.ID = "Password_Label" + this.IDsOffset;
            Password_Label.AssociatedControlID = "password" + this.IDsOffset;

            TextBox UserName = new TextBox();
            UserName.CssClass = "textbox";
            UserName.ID = "username" + this.IDsOffset;
            UserName.Attributes.Add("onkeydown", "javascript:if (event.keyCode == 13) __doPostBack('" + LoginButton.ID + "','')");

            HtmlInputPassword Password = new HtmlInputPassword();
            Password.Attributes["class"] = "textbox";
            Password.ID = "password" + this.IDsOffset;
            Password.Attributes.Add("onkeydown", "javascript:if (event.keyCode == 13) __doPostBack('" + LoginButton.ID + "','')");


            #endregion

            Panel loginPanel = new Panel();
            loginPanel.DefaultButton = LoginButton.ID;
            this.Controls.Add(loginPanel);

            WebControl fieldsArea = new WebControl(HtmlTextWriterTag.Div);
            //this.Controls.Add(fieldsArea);

            WebControl fieldBox = new WebControl(HtmlTextWriterTag.P);
            fieldBox.CssClass = "FormContainer";
            fieldBox.Controls.Add(UserName_Label);
            fieldBox.Controls.Add(UserName);
            fieldsArea.Controls.Add(fieldBox);

            fieldBox = new WebControl(HtmlTextWriterTag.P);
            fieldBox.CssClass = "FormContainer";
            fieldBox.Controls.Add(Password_Label);
            fieldBox.Controls.Add(Password);

            fieldsArea.Controls.Add(fieldBox);

            //this.Controls.Add(LoginButton);
            fieldsArea.Controls.Add(LoginButton);

            WebControl links = new WebControl(HtmlTextWriterTag.Ul);
            WebControl liLink = new WebControl(HtmlTextWriterTag.Li);

            if (MostraRecuperoPassword)
            {
                liLink = new WebControl(HtmlTextWriterTag.Li);
                WebControl forgotPasswordLink = new WebControl(HtmlTextWriterTag.A);
                var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
                forgotPasswordLink.Attributes.Add("href", httpsHandler.ConvertPageAddressAsSecure("?forgot=true"));
                forgotPasswordLink.Controls.Add(new LiteralControl(RecoverPasswordLinkLabel));
                liLink.Controls.Add(forgotPasswordLink);
                links.Controls.Add(liLink);
            }

            if (MostraRegistrazione)
            {
                liLink = new WebControl(HtmlTextWriterTag.Li);
                WebControl showRegLink = new WebControl(HtmlTextWriterTag.A);
                showRegLink.Attributes.Add("href", LinkRegistrazione);
                showRegLink.Controls.Add(new LiteralControl(UserRegistrationLinkLabel));
                liLink.Controls.Add(showRegLink);
                links.Controls.Add(liLink);
            }

            if (links.HasControls())
                //this.Controls.Add(links);
                fieldsArea.Controls.Add(links);

            loginPanel.Controls.Add(fieldsArea);

            if (this.DebugModeEnabled)
            {
                DebugLoginControl DebugLogin = new DebugLoginControl();
                //DebugLogin.CollectGrants = CollectGrants;
                this.Controls.Add(DebugLogin);
            }
        }

        private void InitForgotPasswordControls()
        {
            string message = "";

            #region Costruisco i Controlli form

            TextBox UserName = new TextBox();
            UserName.CssClass = "textbox";
            UserName.ID = "username" + IDsOffset;

            Label UserName_Label = new Label();
            UserName_Label.Text = Labels[LoginLabels.LabelsList.Username] + ": ";
            UserName_Label.ID = "UserName_Label" + IDsOffset;
            UserName_Label.AssociatedControlID = UserName.ID;

            #endregion
            
            this.Attributes["class"] = "LoginFormContainer";

            HtmlGenericControl info = new HtmlGenericControl("div");
            info.Attributes["class"] = "LoginInfo";
            info.InnerHtml = message;
            this.Controls.Add(info);

            Par FormContainer = new Par();
            FormContainer.Class = "FormContainer";

            FormContainer.Controls.Add(UserName_Label);
            FormContainer.Controls.Add(UserName);

            FormContainer.Controls.Add(UserName_Label);
            FormContainer.Controls.Add(UserName);

            this.Controls.Add(FormContainer);
            
            FormContainer = new Par();
            FormContainer.Class = "FormContainer";

            this.Controls.Add(FormContainer);

            //Attacco il controllo submit
            FormContainer = new Par();

            FormContainer.Controls.Add(RecoverPasswordButton);
            FormContainer.Controls.Add(ResetButton);

            this.Controls.Add(FormContainer);
        }

        private void reset_Click(object sender, EventArgs e)
        {
            var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
            string url = httpsHandler.GetCurrentPageAddressAsNonSecure().Replace("?forgot=true","");
            NetCms.Diagnostics.Diagnostics.Redirect(url);
        }

        private void ask_Click(object sender, EventArgs e)
        {
            NetUtility.RequestVariable usernameReq = new NetUtility.RequestVariable("username"+this.IDsOffset, HttpContext.Current.Request.Form);

            //DataTable Users = this.Conn.SqlQuery(" SELECT * FROM Users WHERE Deleted_User = 0 AND State_User = 1 AND Name_User = '" + user.StringValue + "'");

            User user = UsersBusinessLogic.GetActiveUserByUserName(usernameReq.StringValue);

            if (user!= null)
            {
                string hash = user.HashInfoForValidationCheck();
                var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
                string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();
                string urlToMail = httpsHandler.ConvertPageAddressAsSecure("http://" + ip + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/default.aspx?u=" + user.UserName + "&h=" + hash + "&" + ForgotPasswordRequest.Key + "=confirm");
                user.SendConfirmChangePwdMail(urlToMail);
                this.AddNotify(Labels[LoginLabels.LabelsList.SentRequestForChangePw]);
            }
            else
                this.AddNotify(Labels[LoginLabels.LabelsList.NotExistingUser]);
        }

        private void ConfirmChangePwd()
        {
            NetUtility.RequestVariable usernameReq = new NetUtility.RequestVariable("u", HttpContext.Current.Request.QueryString);

            //DataTable Users = this.Conn.SqlQuery(" SELECT * FROM Users WHERE Deleted_User = 0 AND State_User = 1 AND Name_User = '" + user.StringValue + "'");

            User user = UsersBusinessLogic.GetActiveUserByUserName(usernameReq.StringValue);

            if (user != null)
            {
                NetUtility.RequestVariable hashReq = new NetUtility.RequestVariable("h", HttpContext.Current.Request.QueryString);
                if (hashReq.StringValue == user.HashInfoForValidationCheck())
                {
                    user.SendNewPassword();
                    var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
                    HttpContext.Current.Response.Redirect(httpsHandler.ConvertPageAddressAsSecure("?" + ForgotPasswordRequest.Key + "=done"));
                }
                else
                    this.AddNotify(Labels[LoginLabels.LabelsList.NotCorrectInputForPwReset]);
            }
            else
                this.AddNotify(Labels[LoginLabels.LabelsList.NotExistingUser]);
        }
    }

    public class DebugLoginControlv2 : WebControl
    {

        public string IDsOffset
        {
            get
            {
                if (_IDsOffset == null)
                    _IDsOffset = "_LoginControl" + this.LoginsControlsCount;
                return _IDsOffset;
            }
        }
        private string _IDsOffset;

        public int LoginsControlsCount
        {
            get
            {
                if (_LoginsControlsCount == 0)
                {
                    if (HttpContext.Current.Items.Contains("DebugLoginsControlsCount"))
                    {
                        _LoginsControlsCount = int.Parse(HttpContext.Current.Items["DebugLoginsControlsCount"].ToString());
                        _LoginsControlsCount++;
                        HttpContext.Current.Items["DebugLoginsControlsCount"] = _LoginsControlsCount;
                    }
                    else
                        HttpContext.Current.Items["DebugLoginsControlsCount"] = _LoginsControlsCount = 1;
                }
                return _LoginsControlsCount;
            }
        }
        private int _LoginsControlsCount;

        public G2Core.Common.RequestVariable PostbackChecker
        {
            get
            {
                if (_PostbackChecker == null)
                    _PostbackChecker = new G2Core.Common.RequestVariable("debugloginpostback" + this.IDsOffset, G2Core.Common.RequestVariable.RequestType.Form);
                return _PostbackChecker;
            }
        }
        private G2Core.Common.RequestVariable _PostbackChecker;

        public Button LoginButton
        {
            get
            {
                if (_LoginButton == null)
                {
                    _LoginButton = new Button();
                    _LoginButton.ID = PostbackChecker.Key;
                    _LoginButton.Text = "Login";
                    _LoginButton.Click += loginButton_Click;                    
                }
                return _LoginButton;
            }
        }
        private Button _LoginButton;

        public DebugLoginControlv2()
            : base(HtmlTextWriterTag.Div)
        {
        }

        //private NetCms.Users.LoginControl.CollectGrantsDelegate _CollectGrants;
        //public NetCms.Users.LoginControl.CollectGrantsDelegate CollectGrants
        //{
        //    get
        //    {
        //        return _CollectGrants;
        //    }
        //    set
        //    {
        //        _CollectGrants = value;
        //    }
        //}

        private ICollection<KeyValuePair<int, bool>> CollectGrants(User _user)
        {
            ICollection<KeyValuePair<int, bool>> _FoldersCanView = new List<KeyValuePair<int, bool>>();
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                User user = UsersBusinessLogic.GetById(_user.ID,sess);
                var networks = user.AssociatedNetworks;
                foreach (var net in networks)
                {
                    var folders = FolderBusinessLogic.FindNetworkFoldersForGrantsCollection(net.Network.ID,sess);
                    foreach (var folder in folders)//net.Network.NetworkFolders.Cast<StructureFolder>().Where(x => !x.Hidden && !x.IsSystemFolder && x.Depth <= 3))
                    {
                        GrantsFinder Criteria = new FolderGrants(folder, user.Profile,sess);
                        _FoldersCanView.Add(new KeyValuePair<int, bool>(folder.ID, Criteria.NeedToShow("show").Allowed));
                    }
                }
            }
            return _FoldersCanView;
            //Thread.Sleep(60000);
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            G2Core.Common.RequestVariable request = new G2Core.Common.RequestVariable("Accounts" + IDsOffset);

            //DataTable Users = this.Conn.SqlQuery(" SELECT * FROM Users WHERE id_User = " + request.IntValue);

            User user = UsersBusinessLogic.GetById(request.IntValue);

            if (user != null)
            {
                NetCms.Users.LoginResult result = NetCms.Users.AccountManager.TryLoginUser(user, "");
                if (result.Result == LoginResult.LoginResults.Logged)
                {
                    //if (CollectGrants != null)
                    //    CollectGrants(user);

                    HttpContext ctx = HttpContext.Current;
                    HttpSessionState webSession = HttpContext.Current.Session;
                    //Parallel.Invoke(() => { HttpContext.Current = ctx; CollectGrants(user); });

                    NetCms.Users.LoginControl.CollectGrantsDelegate myAction = new NetCms.Users.LoginControl.CollectGrantsDelegate(CollectGrants);
                    myAction.BeginInvoke(user, new AsyncCallback(CallbackMethod), webSession);
                                   
     


                    if (HttpContext.Current.Request.Url.PathAndQuery.EndsWith("/login.aspx"))
                       NetCms.Diagnostics.Diagnostics.Redirect("default.aspx");
                    else
                    {
                        var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
                        NetCms.Diagnostics.Diagnostics.Redirect(httpsHandler.ConvertPageAddressAsNonSecure(HttpContext.Current.Request.Url.ToString()));
                    }
                }
            }
        }

        static void CallbackMethod(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;
            NetCms.Users.LoginControl.CollectGrantsDelegate caller = (NetCms.Users.LoginControl.CollectGrantsDelegate)result.AsyncDelegate;
            ReturnMessage mess = (ReturnMessage) result.GetReplyMessage();

            HttpSessionState session = (HttpSessionState)ar.AsyncState;
            //ICollection<KeyValuePair<int, bool>> returnValue = caller.EndInvoke(ar);

            ICollection<KeyValuePair<int, bool>> _FoldersCanView = (List<KeyValuePair<int, bool>>)mess.ReturnValue;
            if (session != null)
            {
                User currUser = (User)session[Configurations.Generics.BackAccountSessionKey];
                if (currUser != null)
                {
                    if (session["FoldersCanViewUSER_" + currUser.ID] != null)
                    {
                        lock (session["FoldersCanViewUSER_" + currUser.ID])
                        {
                            session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                        }
                    }
                    else
                        session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                }
                //currUser.FoldersCanView = _FoldersCanView;
            }
        }

        public G2Core.Caching.PersistentCacheObject<User[]> DebugLoginUsers
        {
            get
            {
                if (_DebugLoginUsers == null)
                {
                    _DebugLoginUsers = new G2Core.Caching.PersistentCacheObject<User[]>("Debug_UsersList", G2Core.Caching.StoreTypes.Application);
                    _DebugLoginUsers.ObjectBuilder = ()=>
                    {
                        //using (var usersData = Conn.SqlQuery("SELECT * FROM Users WHERE Deleted_User = 0 AND State_User = 1 ORDER BY Name_User"))
                        //{
                        //    var users = usersData.Rows.Cast<DataRow>().Select(x => new User(x)).ToArray();
                        //    return users;
                        //}

                        return UsersBusinessLogic.FindAllActiveUsersOrderByUsername().ToArray();

                    };
                }
                return _DebugLoginUsers;
            }
        }
        private G2Core.Caching.PersistentCacheObject<User[]> _DebugLoginUsers;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.CssClass = "DebugLoginControl";
            var users = DebugLoginUsers.Instance;

            DropDownList accounts = new DropDownList();
            accounts.ID = "Accounts" + IDsOffset;
            if (users != null)
            {
                foreach (NetCms.Users.User user in users)
                {
                    string label = user.UserName;

                    if (user.Anagrafica != null)
                        label = user.NomeCompleto + "(" + user.UserName + ")";

                    ListItem item = new ListItem(label, user.ID.ToString());
                    accounts.Items.Add(item);
                }
            }
            this.Controls.Add(accounts);
            this.Controls.Add(LoginButton);
        }
    }
}

