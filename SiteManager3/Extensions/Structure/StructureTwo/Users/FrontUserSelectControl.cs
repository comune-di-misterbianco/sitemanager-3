using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using HtmlGenericControls;
using NetTable;
using NetCms.GUI;
using System.Collections.Generic;
using NetService.Utility.Controls;
using NetService.Utility.ArTable;
using System.Linq;
using NetCms.Users.Search;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.Users
{
    public partial class UserSelectControl : NetCms.GUI.Collapsable
    {
        //private NetCms.Connections.Connection _Connection;
        //private NetCms.Connections.Connection Connection
        //{
        //    get
        //    {
        //        if (_Connection == null)
        //            _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
        //        return _Connection;
        //    }
        //}

        private int _RPP;
        private int RPP
        {
            get { return _RPP; }
        }

        private string PageRequestKey
        {
            get { return this.ID + "_Page"; }

        }
        private string SearchButtonKey
        {
            get { return this.ID + "_SearchButton"; }

        }


        public NetUtility.RequestVariable PageRequest
        {
            get
            {
                if (_PageRequest == null)
                    _PageRequest = new NetUtility.RequestVariable(PageRequestKey);
                return _PageRequest;
            }
        }
        private NetUtility.RequestVariable _PageRequest;



        public UsersSearchControl UsersSearchControl
        {
            get
            {
                if (_UsersSearchControl == null)
                    _UsersSearchControl = new UsersSearchControl();
                return _UsersSearchControl;
            }
        }
        private UsersSearchControl _UsersSearchControl;

        public UserSelectControl(string id, string title, int rpp)
            : base(title)
        {
            _RPP = rpp;
            this.ID = id;
        }

        //public DataTable UsersTable
        //{
        //    get
        //    {
        //        if (_UsersTable == null)
        //        {
        //            Search.UsersDataManager UsersDataManager = new Search.UsersDataManager(NetCms.Users.AccountManager.CurrentAccount, this.Connection);
        //            _UsersTable = UsersDataManager.MyUsersTable;
        //        }
        //        return _UsersTable;
        //    }
        //}
        //private DataTable _UsersTable;

        public ICollection<User> UsersTable
        {
            get
            {
                if (_UsersTable == null)
                {
                    Search.UsersDataManager UsersDataManager = new Search.UsersDataManager(NetCms.Users.AccountManager.CurrentAccount);
                    UsersDataManager.PageSize = RPP;
                    UsersDataManager.PageStart = (UsersSearchControl.ButtonRequest.IsPostBack && UsersSearchControl.ButtonRequest.IsValidString) ? 0 : Pagination.CurrentPage - 1;
                    _UsersTable = UsersDataManager.MyUsersTable;

                }
                return _UsersTable;
            }
        }
        private ICollection<User> _UsersTable;

        public int UsersTableCount
        {
            get
            {
                Search.UsersDataManager UsersDataManager = new Search.UsersDataManager(NetCms.Users.AccountManager.CurrentAccount);
                _UsersTableCount = UsersDataManager.MyUsersTableCount;
                return _UsersTableCount;
            }
        }
        private int _UsersTableCount;

        public ArTable SearchResultTable
        {
            get
            {
                if (_SearchResultTable == null)
                {
                    _SearchResultTable = new ArTable();
                    _SearchResultTable.EnablePagination = true;
                    _SearchResultTable.RecordPerPagina = RPP;
                    _SearchResultTable.NoSearchParameter = true;
                    _SearchResultTable.InnerTableCssClass = "tab";
                    _SearchResultTable.GoToFirstPageOnSearch = true;
                }
                return _SearchResultTable;
            }
        }
        private ArTable _SearchResultTable;

        public PaginationHandler Pagination
        {
            get
            {
                if (_Pagination == null)
                {

                    _Pagination = new PaginationHandler(RPP, TotalRecordCount(), SearchResultTable.PaginationKey, (UsersSearchControl.ButtonRequest.IsPostBack && UsersSearchControl.ButtonRequest.IsValidString));
                }
                return _Pagination;
            }
        }
        private PaginationHandler _Pagination;

        private int TotalRecordCount()
        {
            int count = 0;
            switch (UsersSearchControl.SearchStatus)
            {
                case Search.UsersSearchControl.SearchStatuses.SearchAvailable: count = this.UsersSearchControl.GetSearchResultCount(NetCms.Users.AccountManager.CurrentAccount); break;
                case Search.UsersSearchControl.SearchStatuses.NoSearch: break;
                case Search.UsersSearchControl.SearchStatuses.WrongData: break;
            }
            return count;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            
            this.AppendControl(UsersSearchControl);
            if (this.UsersSearchControl.SearchStatus == Search.UsersSearchControl.SearchStatuses.SearchAvailable)
            {
                ICollection<UserSearchRowTemplate> datatable = this.UsersSearchControl.GetSearchResult((UsersSearchControl.ButtonRequest.IsPostBack && UsersSearchControl.ButtonRequest.IsValidString) ? 0 : Pagination.CurrentPage - 1, RPP, NetCms.Users.AccountManager.CurrentAccount);

                SearchResultTable.PagesControl = Pagination;

                if (datatable.Count() > 0)
                {
                    SearchResultTable.Records = datatable;
                    SearchResultTable.CustomSearchArray = UsersSearchControl.WorkerFinder.Finder.CustomSearchFields.Select(x => x.QueryString).ToArray();

                    G2Core.Common.RequestVariable nid = new G2Core.Common.RequestVariable("nid", G2Core.Common.RequestVariable.RequestType.Form_QueryString);
                    if (nid.IsValidInteger)
                        SearchResultTable.CustomSearchArray = SearchResultTable.CustomSearchArray.Union(new string[1] { "nid=" + nid.StringValue }).ToArray();
                    G2Core.Common.RequestVariable app = new G2Core.Common.RequestVariable("app", G2Core.Common.RequestVariable.RequestType.Form_QueryString);
                    if (app.IsValidInteger)
                        SearchResultTable.CustomSearchArray = SearchResultTable.CustomSearchArray.Union(new string[1] { "app=" + app.StringValue }).ToArray();
                    G2Core.Common.RequestVariable folder = new G2Core.Common.RequestVariable("folder", G2Core.Common.RequestVariable.RequestType.Form_QueryString);
                    if (folder.IsValidInteger)
                        SearchResultTable.CustomSearchArray = SearchResultTable.CustomSearchArray.Union(new string[1] { "folder=" + folder.StringValue }).ToArray();
                    G2Core.Common.RequestVariable doc = new G2Core.Common.RequestVariable("doc", G2Core.Common.RequestVariable.RequestType.Form_QueryString);
                    if (doc.IsValidInteger)
                        SearchResultTable.CustomSearchArray = SearchResultTable.CustomSearchArray.Union(new string[1] { "doc=" + doc.StringValue }).ToArray();


                    #region Campi della tabella
                    
                    

                    ArTextColumn colName = new ArTextColumn("User Name", "Name_User");
                    SearchResultTable.AddColumn(colName);

                    ArTextColumn colAnagraficaName = new ArTextColumn("Nome", "AnagraficaNome");
                    SearchResultTable.AddColumn(colAnagraficaName);
                    /*
                    sql = "";
                    sql += "SELECT Name_Group FROM usersgroups INNER JOIN groups ON (usersgroups.Group_UserGroup = groups.id_Group) ";
                    sql += " WHERE User_UserGroup = {0} ORDER BY Tree_Group";

                    col = new SmTableExternalListColumn("id_User", "Gruppi", sql, this.Connection);
                    table.addColumn(col);*/

                    string thickboxHref = NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/users/user_details.aspx?id={id_User}&keepThis=true&TB_iframe=true&height=600&width=800";
                    ArActionColumn colDettagli = new ArActionColumn("Scheda e Opzioni", ArActionColumn.Icons.Details, thickboxHref);
                    colDettagli.CssClass = "thickbox";
                    SearchResultTable.AddColumn(colDettagli);

                    ArActionColumn addProfileColumn = new ArActionColumn("Aggiungi", ArActionColumn.Icons.Add, "javascript: setFormValue('{Profile_User}','AddProfileField',1);");
                    SearchResultTable.AddColumn(addProfileColumn);
                    //acol = new SmTableActionColumn("Profile_User", "action add", "Aggiungi", "javascript: setFormValue('{0}','AddProfileField',1);");
                    //table.addColumn(acol);

                    #endregion

                    
                }
                this.AppendControl(SearchResultTable);
            }

            if (this.UsersSearchControl.SearchStatus == Search.UsersSearchControl.SearchStatuses.WrongData)
                PopupBox.AddMessage("I dati di ricerca immessi non sono validi, assicurarsi di aver riempito almeno un campo e di aver inserito dati corretti.", PostBackMessagesType.Notify);

            if (this.UsersSearchControl.SearchStatus == Search.UsersSearchControl.SearchStatuses.NoSearch)
                this.StartCollapsed = true;
        }
    }
    //public class SmTableNomeAnagrafica : SmTableColumn
    //{
    //    private string src;

    //    public SmTableNomeAnagrafica(string fieldname, string caption)
    //        : base(fieldname, caption)
    //    {
    //    }

    //    public override HtmlGenericControl getField(DataRow row)
    //    {
    //        string value = row[this.FieldName].ToString();
    //        string anagraficaType = row["AnagraficaType_User"].ToString();

    //        HtmlGenericControl td = new HtmlGenericControl("td");

    //        
    //        //Users.AnagraficaBase anagrafica = Users.Anagraphics.AnagraphicsClassesPool.BuildInstanceOf(value, anagraficaType);
    //        //if (anagrafica == null || !anagrafica.Exist) this.SkipRows.Add(row["id_User"].ToString());
    //        //else
    //        //    td.InnerHtml = NomeCompleto;

    //        return td;
    //    }

    //    public override string filterValue(string value)
    //    {
    //        return value;
    //    }
    //}
}


