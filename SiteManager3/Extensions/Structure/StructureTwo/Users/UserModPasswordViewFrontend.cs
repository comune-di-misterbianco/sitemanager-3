﻿//using System;
//using System.Data;
//using System.Data.Common;
//using System.Web.UI.HtmlControls;
//using System.Web.UI.WebControls;
//using NetCms.GUI;
//using NetForms;
//using System.Web;
///// <summary>
///// Summary description for StructureGrants
///// </summary>

//namespace NetCms
//{
//    namespace Structure
//    {
//        namespace Grants
//        {
//            public partial class UsersGrants
//            {
//                public class UsersModPasswordViewFrontend1
//                {
//                    public NetCms.Users.User User
//                    {
//                        get
//                        {
//                            return _User ?? (_User = NetCms.Users.AccountManager.CurrentAccount);
//                        }
//                    }
//                    private NetCms.Users.User _User;

//                    public string InformationText
//                    {
//                        get
//                        {
//                            if (_InformationText == null)
//                                _InformationText = "La tua password è scaduta, per continuare dovrai aggiornarla. La nuova password dovrà contenere almeno 3 di questi elementi: Lettera Maiuscola, Lettera Minuscola, Numero, Simbolo. Ed essere lunga almeno 8 caratteri.";
//                            return _InformationText;
//                        }
//                        set { _InformationText = value; }
//                    }
//                    private string _InformationText;

                    
//                    private string PostBackResults = "";
//                    private bool PostBackExit = false;

//                    public UsersModPasswordViewFrontend1(NetCms.Connections.Connection con)
//                    {
//                        Conn = con;
//                        btPostBack();
//                    }

//                    NetCms.Connections.Connection Conn;
//                    private NetForm _AnagraficaForm;
//                    private NetForm AnagraficaForm
//                    {
//                        get
//                        {
//                            if (_AnagraficaForm == null)
//                            {
//                                _AnagraficaForm = new NetForm();
//                                _AnagraficaForm.AddSubmitButton = false;
//                                _AnagraficaForm.SubmitSourceID = "BtAggiungi";
//                                _AnagraficaForm.AddTable(UserFormTable);
//                            }

//                            return _AnagraficaForm;
//                        }
//                    }

//                    private NetFormTable _UserFormTable;
//                    private NetFormTable UserFormTable
//                    {
//                        get
//                        {
//                            if (_UserFormTable == null)
//                            {

//                                NetFormTable user = new NetFormTable("Users", "id_User", Conn, User.ID);

//                                #region Campi Anagrafica

//                                NetForms.NetPasswordField Password = new NetPasswordField("Nuova Password", "Password_User");
//                                Password.CyperType = NetPasswordField.CyperingType.MD5;
//                                Password.Required = true;

//                                if (this.User.Password.Length > 0)
//                                {
//                                    Password.OldPasswordCheck = false;
//                                    Password.OldPassword = this.User.Password;
//                                }
//                                else
//                                    Password.OldPasswordCheck = false;

//                                Password.HighSecurityField = true;
//                                user.addField(Password);
                                
                                
//                                NetHiddenField hidden = new NetHiddenField("LastUpdate_User");
//                                hidden.Value = Conn.FilterDateTime(DateTime.Now);
//                                user.addField(hidden);

//                                hidden = new NetHiddenField("NewPassword_User");
//                                hidden.Value = "\"\"";
//                                user.addField(hidden);

//                                #endregion

//                                _UserFormTable = user;
//                            }

//                            return _UserFormTable;
//                        }
//                    }

//                    private void btPostBack()
//                    {

//                        if (HttpContext.Current.Request.Form["BtAggiungi"] != null && HttpContext.Current.Request.Form["BtAggiungi"].ToString() == "Aggiorna")
//                        {
//                            string errors = AnagraficaForm.serializedUpdate(HttpContext.Current.Request.Form);

//                            if (AnagraficaForm.LastOperation == NetForm.LastOperationValues.OK)
//                            {
//                                PostBackResults = "<p>Utente Aggiornato</p>";
//                                PostBackExit = true;
//                                Conn.Close();
//                            }
//                            else
//                                PostBackResults = errors;
//                        }
//                    }

//                    public HtmlGenericControl getView()
//                    {

//                        HtmlGenericControl output = new HtmlGenericControl("div");
//                        output.Attributes["class"] = "refreshPWD_frontend";

//                        if (PostBackResults != null && PostBackResults != "")
//                            output.InnerHtml = PostBackResults;

//                        if (!PostBackExit)
//                        {
//                            HtmlGenericControls.Fieldset fieldset = new HtmlGenericControls.Fieldset("Dati Login");
//                            output.Controls.Add(fieldset);
//                            HtmlGenericControl info = new HtmlGenericControl("p");

//                            info.InnerHtml = InformationText;
//                            fieldset.Controls.Add(info);
//                            fieldset.Controls.Add(UserFormTable.getForm());

//                            Button bt = new Button();
//                            bt.Text = "Aggiorna";
//                            bt.ID = "BtAggiungi";
//                            bt.OnClientClick = "setSubmitSource('BtAggiungi')";
//                            fieldset.Controls.Add(bt);

//                            Button logout = new Button();
//                            logout.Text = "Logout";
//                            logout.ID = "BtLogout";
//                            logout.Click += new EventHandler(logout_Click);
//                            fieldset.Controls.Add(logout);
//                        }
//                        else
//                        {
//                            HtmlGenericControls.Fieldset ok = new HtmlGenericControls.Fieldset("Aggiornamento Password Utente");
//                            HtmlGenericControls.Div div = new HtmlGenericControls.Div();
//                            //string qs = HttpContext.Current.Request.QueryString["folder"];
//                            //int folder;
//                            //if (qs != null && int.TryParse(qs, out folder))
//                            //    qs = "folder=" + folder;

//                            NetCms.Users.AccountManager.CurrentAccount.SetPasswordUpdated();
//                            div.InnerHtml = "<p>La password è stata aggiornata con successo. Sarà necessario reimpostarla tra 90 giorni.</p>";
//                            if (NetCms.Networks.NetworksManager.CurrentFace == Faces.Front)
//                            {
//                                div.InnerHtml += "<p><a href=\"" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "\">Vai all'Homepage</a></p>"; 
//                            }

//                            //NetCms.GUI.PopupBox.AddSessionMessage("La password è stata aggiornata con successo. Sarà necessario reimpostarla tra 90 giorni.", NetCms.GUI.PostBackMessagesType.Success);
//                            //HttpContext.Current.NetCms.Diagnostics.Diagnostics.Redirect("default.aspx");

//                            output.Controls.Add(ok);
//                            ok.Controls.Add(div);
//                        }

//                        return output;
//                    }

//                    void logout_Click(object sender, EventArgs e)
//                    {
//                        HttpContext.Current.Response.Redirect("default.aspx?do=logout");
//                    }
//                }
//            }
//        }
//    }
//}