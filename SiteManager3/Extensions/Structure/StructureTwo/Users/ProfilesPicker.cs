using System;
using System.Data;
using System.Web.UI.HtmlControls;
using NetTable;
using System.Linq;
using NetService.Utility.ArTable;
using System.Collections.Generic;
using NetCms.Users;
using System.Web.UI.WebControls;
using NetCms.GUI;
/// <summary>
/// Questa classe � da rimpiazzare completamente nel cms, mantenuta solo per compatibilit�, usare ProfilesPickerForGrants
/// </summary>
namespace NetCms.Structure.Grants
{
    public partial class ProfilesPicker
    {
        private string sqlExecuteModel;
        public string SqlExecuteModel
        {
            get { return sqlExecuteModel; }
            set { sqlExecuteModel = value; }
        }

        private string _Title;
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string sqlSelectModel;
        public string SqlSelectModel
        {
            get { return sqlSelectModel; }
            set { sqlSelectModel = value; }
        }

        //private string groupsSelectCriteria;
        //public string GroupsSelectCriteria
        //{
        //    get
        //    {
        //        if (groupsSelectCriteria == null)
        //        {
        //            string gsql = "";
        //            //gsql = " INNER JOIN profiles ON (groups.Profile_Group = profiles.id_Profile)";

        //            gsql = " INNER JOIN groups_profiles ON (groups.Profile_Group = groups_profiles.id_Profile)";


        //            string conditions = "";
        //            for (int i = 0; i < this.CurrentUser.Groups.Count; i++)
        //            {
        //                NetCms.Users.GroupAssociation GroupAssociation = this.CurrentUser.AssociatedGroupsOrdered.ElementAt(i);

        //                if (GroupAssociation.IsAdministrator)
        //                {
        //                    conditions += " OR ";
        //                    conditions += " Tree_Group LIKE '" + GroupAssociation.Group.Tree + "%'";
        //                }
        //                if (GroupAssociation.GrantsRole == NetCms.Users.GrantAdministrationTypes.Administrator)
        //                    conditions += " AND (Tree_Group NOT LIKE '" + GroupAssociation.Group.Tree + "%')";
        //            }
        //            //La condizione 1=0 serve ad esculudere tutti i record tranne quelli selezionati dalle
        //            //condizioni specificate nella variabile conditions
        //            gsql += " WHERE (1=0" + conditions + ")";
        //            groupsSelectCriteria = gsql;
        //        }
        //        return groupsSelectCriteria;
        //    }

        //}
        /*
        private bool _FilterGroups;
        public bool FilterGroups
        {
            get { return _FilterGroups; }
            set { _FilterGroups = value; }
        }
        */
        private bool _UserGroups;
        public bool UserGroups
        {
            get { return _UserGroups; }
            set { _UserGroups = value; }
        }

        //private string usersSelectCriteria;
        //public string UsersSelectCriteria
        //{
        //    get
        //    {
        //        if (usersSelectCriteria == null)
        //        {
                    
                    //string sql = "SELECT id_User,Name_User,Profile_User,Anagrafica.* FROM Users";
                    //sql += " INNER JOIN anagrafica ON (Anagrafica_User = id_Anagrafica)";
                    //sql += " INNER JOIN Profiles ON (Profile_User = id_Profile)";
                    //sql += " INNER JOIN UsersGroups ON (User_UserGroup = id_User)";
                    //sql += " INNER JOIN Groups ON (Group_UserGroup = id_Group)";

                    //string conditions = "";
                    //for (int i = 0; i < this.CurrentUser.Groups.Count; i++)
                    //{
                    //    NetCms.Users.GroupAssociation GroupAssociation = this.CurrentUser.AssociatedGroupsOrdered.ElementAt(i);

                    //    if (GroupAssociation.IsAdministrator)
                    //    {
                    //        conditions += " OR ";
                    //        conditions += " Tree_Group LIKE '" + GroupAssociation.Group.Tree + "%'";
                    //    }
                    //    if (GroupAssociation.GrantsRole == NetCms.Users.GrantAdministrationTypes.Administrator)
                    //        conditions += " AND (id_User <> " + this.CurrentUser.ID + ")";
                    //}
                    ////La condizione 1=0 serve ad esculudere tutti i record tranne quelli selezionati dalle
                    ////condizioni specificate nella variabile conditions
                    //sql += " AND Deleted_User = 0 AND (1=0" + conditions + ")";

                    //sql += " GROUP BY id_User,Name_User";
                    //usersSelectCriteria = sql;
        //        }
        //        return usersSelectCriteria;
        //    }

        //}

        private bool queryExecuted = false;
        public bool QueryExecuted
        {
            get
            {
                return queryExecuted;
            }
        }

        private int lastExecuteID = 0;
        public int LastExecuteID
        {
            get
            {
                return lastExecuteID;
            }
        }

        public NetCms.Users.User CurrentUser
        {
            get
            {
                return _CurrentUser;
            }
            set { _CurrentUser = value; }
        }
        private NetCms.Users.User _CurrentUser;

        private NetCms.Connections.Connection _Connection;
        private NetCms.Connections.Connection Connection
        {
            get
            {
                return _Connection;
            }
            set { _Connection = value; }
        }

        public ProfilesPicker(string title, NetCms.Users.User currentUser, NetCms.Connections.Connection connection)
        {
            Connection = connection;
            CurrentUser = currentUser;
            _Title = title;
        }

        private HtmlGenericControls.InputField _AddProfileField;
        public HtmlGenericControls.InputField AddProfileField
        {
            get
            {
                if (_AddProfileField == null)
                {
                    _AddProfileField = new HtmlGenericControls.InputField("AddProfileField");

                }
                return _AddProfileField;
            }
        }
        
//        public HtmlGenericControl Control
//        {
//            get
//            {
//                HtmlGenericControls.Div set = new HtmlGenericControls.Div();

//                NetCms.Users.UserSelectControl users = new NetCms.Users.UserSelectControl("UsersCTR", "Utenti che � possibile associare", 7, "javascript: setFormValue('{3}','" + AddProfileField.ID + "',1)");
//                set.Controls.Add(users);

//                #region Campi Hidden e Relativi Postback

//                /****************************************************************************************************************************************/
//                /****************  Aggiunta Profilo  ************************************************************************************/
//                /****************************************************************************************************************************************/

//                set.Controls.Add(AddProfileField.Control);
//                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
//                if (AddProfileField.RequestVariable.IsPostBack && AddProfileField.RequestVariable.IsValidInteger && AddProfileField.RequestVariable.IntValue > 0)
//                {
//                    System.Data.DataTable sqltable = this.Connection.SqlQuery(string.Format(SqlSelectModel, AddProfileField.RequestVariable.StringValue));
//                    if (sqltable.Rows.Count == 0)
//                    {
//                        if (SqlExecuteModel.ToLower().StartsWith("insert "))
//                        {
//                            this.lastExecuteID = this.Connection.ExecuteInsert(string.Format(SqlExecuteModel, AddProfileField.RequestVariable.StringValue));
//                            this.queryExecuted = this.lastExecuteID > 0;
//                        }
//                        else
//                            this.queryExecuted = this.Connection.Execute(string.Format(SqlExecuteModel, AddProfileField.RequestVariable.StringValue));
//                    }
//                    else
//                    {
//                        //NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha provato ad aggiungere il profilo con id " + AddProfileField.RequestVariable.StringValue + ", che per� era gi� associato.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);                        
//                        NetCms.GUI.PopupBox.AddMessage("L'utente � gi� associato all'applicazione", NetCms.GUI.PostBackMessagesType.Notify);
//                    }
//                }
                

//                #endregion

//                GUI.Collapsable collapsable = new NetCms.GUI.Collapsable("Gruppi che � possibile associare");
//                collapsable.StartCollapsed = true;
//                set.Controls.Add(collapsable);
//                #region Tabella dei Gruppi

//                NetCms.Users.Search.UserRelated rels = new NetCms.Users.Search.UserRelated(this.CurrentUser, Connection);
//                string sql = @"SELECT DISTINCT groups.* FROM groups                
//                                LEFT JOIN usersgroups ON (usersgroups.Group_UserGroup = groups.id_Group)
//                                WHERE false " + rels.MyGroupsConditions + " ORDER BY Tree_Group";

//                SmTable stable = new SmTable(sql, this.Connection);
//                stable.CssClass = "GenericTable";

//                SmTableTreeColumn2 treecol = new SmTableTreeColumn2("Gruppo", "id_Group", "Name_Group", "Depth_Group", this.Connection);
//                treecol.IconType = SmTableTreeColumn2.IconTypes.CssClass;
//                treecol.Icon = "GrantsTableGroupIcon";
//                stable.addColumn(treecol);

//                SmTableColumn col;
//                //if (FilterGroups)
//                //{
//                    //GroupsSql += GroupsSelectCriteria;
//                col = new ProfilePickerTableActionColumn("Profile_Group", "action add", "Aggiungi", "javascript: setFormValue('{0}','" + AddProfileField.ID + "',1)");//, this.Connection.SqlQuery(GroupsSql), CurrentUser);
//                //}
//                //else
//                    //col = new SmTableActionColumn("Profile_Group", "action add", "Aggiungi", "javascript: setFormValue('{0}','" + AddProfileField.ID + "',1)");
//                stable.addColumn(col);

//                #endregion

//                collapsable.AppendControl( stable.getTable());

//                return set;
//            }
//        }

        public HtmlGenericControl Control
        {
            get
            {
                HtmlGenericControls.Div set = new HtmlGenericControls.Div();

                NetCms.Users.UserSelectControl users = new NetCms.Users.UserSelectControl("UsersCTR", "Utenti che � possibile associare", 7);
                set.Controls.Add(users);

                #region Campi Hidden e Relativi Postback

                /****************************************************************************************************************************************/
                /****************  Aggiunta Profilo  ************************************************************************************/
                /****************************************************************************************************************************************/

                set.Controls.Add(AddProfileField.Control);
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                if (AddProfileField.RequestVariable.IsPostBack && AddProfileField.RequestVariable.IsValidInteger && AddProfileField.RequestVariable.IntValue > 0)
                {
                    System.Data.DataTable sqltable = this.Connection.SqlQuery(string.Format(SqlSelectModel, AddProfileField.RequestVariable.StringValue));
                    if (sqltable.Rows.Count == 0)
                    {
                        if (SqlExecuteModel.ToLower().StartsWith("insert "))
                        {
                            this.lastExecuteID = this.Connection.ExecuteInsert(string.Format(SqlExecuteModel, AddProfileField.RequestVariable.StringValue));
                            this.queryExecuted = this.lastExecuteID > 0;
                        }
                        else
                            this.queryExecuted = this.Connection.Execute(string.Format(SqlExecuteModel, AddProfileField.RequestVariable.StringValue));
                    }
                    else
                    {
                        //NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha provato ad aggiungere il profilo con id " + AddProfileField.RequestVariable.StringValue + ", che per� era gi� associato.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);                        
                        NetCms.GUI.PopupBox.AddMessage("L'utente � gi� associato all'applicazione", NetCms.GUI.PostBackMessagesType.Notify);
                    }
                }


                #endregion

                Collapsable collapsable = new NetCms.GUI.Collapsable("Gruppi che � possibile associare");
                collapsable.StartCollapsed = true;
                set.Controls.Add(collapsable);
                #region Tabella dei Gruppi

                ArTable table = new ArTable();
                table.InnerTableCssClass = "tab";
                table.NoRecordMsg = "Nessun gruppo trovato";

                NetCms.Users.Search.UserRelated rels = new Users.Search.UserRelated(this.CurrentUser);
                ICollection<Group> groups = rels.GroupsIAdmin(0, rels.GroupsIAdminCount(null, null), null, null).OrderBy(x=>x.Tree).ToList(); //GroupsBusinessLogic.FindGroupsByUser(this.CurrentUser);

                table.Records = groups;

                // SISTEMARE SE SERVE
                //SmTableTreeColumn2 treecol = new SmTableTreeColumn2("Gruppo", "id_Group", "Name_Group", "Depth_Group", this.Connection);
                //treecol.IconType = SmTableTreeColumn2.IconTypes.CssClass;
                //treecol.Icon = "GrantsTableGroupIcon";
                //stable.addColumn(treecol);

                ArTextColumn nome = new ArTextColumn("Gruppo", "Name");
                table.AddColumn(nome);

                ProfilePickerTableActionColumn col = new ProfilePickerTableActionColumn("Aggiungi", "javascript: setFormValue('{ProfileID}','" + AddProfileField.ID + "',1)");
                table.AddColumn(col);

                #endregion

                collapsable.AppendControl(table);

                return set;
            }
        }

        //private class ProfilePickerTableActionColumn : SmTableColumn
        //{
        //    private string Classe;
        //    private string Label;
        //    private string Href;
        //    private string Title;
        //    //private DataTable Table;
        //    /*
        //    public NetCms.Users.User CurrentUser
        //    {
        //        get
        //        {
        //            return _CurrentUser;
        //        }
        //        set { _CurrentUser = value; }
        //    }
        //    private NetCms.Users.User _CurrentUser;
        //    */
        //    public ProfilePickerTableActionColumn(string fieldName, string classe, string Caption, string href)//, DataTable table, NetCms.Users.User currentUser)
        //        : this(fieldName, classe, Caption, href, "", Caption)//, table, currentUser)
        //    {
        //        //Table = table;
        //    }
        //    public ProfilePickerTableActionColumn(string fieldName, string classe, string Caption, string href, string title)//, DataTable table, NetCms.Users.User currentUser)
        //        : this(fieldName, classe, Caption, href, title, Caption)//, table, currentUser)
        //    {
        //        //Table = table;
        //    }

        //    public ProfilePickerTableActionColumn(string fieldName, string classe, string Caption, string href, string title, string label)//, DataTable table, NetCms.Users.User currentUser)
        //        : base(fieldName, Caption)
        //    {
        //        Classe = classe;
        //        Label = label;
        //        Href = href;
        //        //Table = table;
        //        //CurrentUser = currentUser;
        //        Title = title;
        //    }
        //    public override HtmlGenericControl getField(DataRow row)
        //    {
        //        string value = row[this.FieldName].ToString();
        //        string DbLabel = row.Table.Columns.Contains(this.Label) ? row[this.Label].ToString() : Label;
        //        HtmlGenericControl td = new HtmlGenericControl("td");
        //        td.Attributes["class"] = Classe;
        //        string html = "";
        //        html += "<a href=\"";
        //        html += Href.Replace("{0}", value);
        //        html += "\" title=\"" + (Title.Length > 0 ? Title : Label) + "\"><span>";
        //        html += DbLabel;
        //        html += "</span></a>"; 
        //        td.InnerHtml = html;
        //        return td;
                
        //    }
        //}

        private class ProfilePickerTableActionColumn : ArActionColumn
        {
            public ProfilePickerTableActionColumn(string Caption, string href)
                : base(Caption, Icons.Add, href)
            {
                
            }

            protected override System.Web.UI.WebControls.TableCell GetCellControl(object objectInstance, string label, string href, ArActionColumn.Icons icon)
            {
                TableCell td = new TableCell();
                if (icon != null)
                {
                   td.CssClass = "action " + icon.ToString().ToLower();
                }

                string value = objectInstance.GetType().GetProperty("ProfileID").GetValue(objectInstance,null).ToString();

                string anchorCssClassHtml = string.IsNullOrEmpty(AnchorCssClass) ? "" : "class=\"" + this.AnchorCssClass + "\"";

                td.Text = "<a " + anchorCssClassHtml + " href=\"";
                td.Text += href.Replace("{ProfileID}", value);
                td.Text += "\" title=\"" + Caption + "\"><span>";
                td.Text += Caption;
                td.Text += "</span></a>";

                return td;
            }
        }
    }
}


