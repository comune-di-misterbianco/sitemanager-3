using System;
using System.Web.UI;
using System.Web.UI.WebControls;
         
/// <summary>
/// Force Login Control
/// </summary>
namespace NetCms.Users
{
    public partial class NeedToLoginControl : WebControl
    {
        public NeedToLoginControl()
            : base(HtmlTextWriterTag.Div)
        {
            var httpsHanlder = NetCms.Configurations.Generics.GetHttpsHandler();
            if (!httpsHanlder.IsSecureConnection)
                httpsHanlder.RedirectToSecureRequest();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!NetCms.Configurations.Generics.LoginManager)
            {
                this.CssClass = "container force-login";

                WebControl colPage = new WebControl(HtmlTextWriterTag.Div);
                colPage.CssClass = "col-md-12";

                WebControl phblock = new WebControl(HtmlTextWriterTag.Div);
                phblock.CssClass = "page-header";

                WebControl title = new WebControl(HtmlTextWriterTag.H3);
                title.Controls.Add(new LiteralControl("E' necessario effettuare l'accesso per accedere a questa pagina."));

                phblock.Controls.Add(title);

                colPage.Controls.Add(phblock);

                string text = NetCms.Configurations.XmlConfig.RemoveCDATA(NetCms.Configurations.XmlConfig.GetNodeXml("/Portal/configs/loginFrontendInfo"));
                WebControl TextPar = new WebControl(HtmlTextWriterTag.P);
                TextPar.Controls.Add(new LiteralControl(text));

                colPage.Controls.Add(TextPar);


                WebControl loginrow = new WebControl(HtmlTextWriterTag.Div);
                loginrow.CssClass = "row ";

                loginrow.Controls.Add(LoginBox());

                colPage.Controls.Add(loginrow);

                this.Controls.Add(colPage);

                //string litModel = "<{0} {2}>{1}</{0}>";
                //fieldset.Controls.Add(new LiteralControl(string.Format(litModel, "legend", "E' necessario effettuare l'accesso per accedere a questa pagina.", "")));
                //fieldset.Controls.Add(new LiteralControl(string.Format(litModel, "p", text, "class=\"force-login-text\"")));
                //fieldset.Controls.Add(LoginBox());
                //this.Controls.Add(fieldset);
            }
            else
            {
                /*********************************************************************************************************************************************/
                /*   appende alla pagina il controllo di login secondo le modalitÓ di autenticazione configurate in modo globale o per la network corrente   */
                /*   e se non attiva appende il controllo vecchio per retrocompatibilitÓ                                                                     */
                /*********************************************************************************************************************************************/               
                this.CssClass = "container";

                WebControl row = new WebControl(HtmlTextWriterTag.Div);
                row.CssClass = "row no-gutter";

                WebControl loginContainer = new WebControl(HtmlTextWriterTag.Div);
                loginContainer.CssClass = "login-page";
                loginContainer.Controls.Add(new LoginManager.Controls.LoginBox(NetCms.Networks.NetworksManager.CurrentActiveNetwork.ID));

                row.Controls.Add(loginContainer);

                this.Controls.Add(row);
            }
        }

        public string UrlRegistrazione
        {

            get
            {
                string value = NetCms.Configurations.XmlConfig.GetAttribute("/Portal/configs/login", "urlRegistrazione");
                return (value != null) ? value : "";
            }


        }

        public bool MostraRegistrazione
        {
            get
            {
                string value = NetCms.Configurations.XmlConfig.GetAttribute("/Portal/configs/login", "mostraRegistrazione");
                bool active = !string.IsNullOrEmpty(value) && value != "off";
                return active;
            }
        }

        public bool MostraRecuperoPassword
        {
            get
            {
                string value = NetCms.Configurations.XmlConfig.GetAttribute("/Portal/configs/login", "mostraRecuperoPassword");
                bool active = !string.IsNullOrEmpty(value) && value != "off";
                return active;
            }
        }

        public Control LoginBox()
        {
            string currentPagePath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            var currentNetwork = NetCms.Networks.NetworksManager.CurrentActiveNetwork;
            string awp = (currentNetwork != null) ? currentNetwork.Paths.AbsoluteFrontRoot : NetCms.Configurations.Paths.AbsoluteRoot;
            string strLinkReg = awp + "/" + UrlRegistrazione;

            WebControl divInt = new WebControl(HtmlTextWriterTag.Div);
            divInt.CssClass = "col-xs-12 col-md-6 col-md-offset-3 ";

            string redirecturl = currentPagePath; //NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;

            LoginControlv3 loginCtrl = new LoginControlv3
               (
                   redirecturl,
                   MostraRegistrazione,
                   MostraRecuperoPassword,
                   strLinkReg
               );

            divInt.Controls.Add(loginCtrl);


            return divInt;
        }
    }
}

