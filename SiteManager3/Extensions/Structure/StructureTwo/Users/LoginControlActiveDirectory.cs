﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using HtmlGenericControls;
using NetCms.Networks.Grants;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Runtime.Remoting.Messaging;
using NHibernate;
using System.Web.SessionState;
using LabelsManager;

/// <summary>
/// LoginControl v2 AD: 
/// Il controllo permette il login di un utente attraverso l'inserimento di user e password o attraverso active directory
/// Il form e i bottoni sono 
/// E' possibile cambiare label dei bottoni e gestire le funzioni onclick direttamente dalla classe istanziante.
/// 
/// </summary>
namespace NetCms.Users
{
    public partial class LoginControlActiveDirectory : LoginManager.Controls.LoginControl
    {
        // check AD mode is enable
        // check user logged in AD
        // add new user
        // notify admin user new user 
        // login user on cms

        //public static Labels ADLabels
        //{
        //    get
        //    {
        //        return LabelsCache.GetTypedLabels(typeof(LoginLabels),"it-IT") as Labels;
        //    }
        //}

        //public override string LoginButtonLabel
        //{
        //    get { return _LoginButtonLabel; }
        //    set { _LoginButtonLabel = value; }
        //}
        //private string _LoginButtonLabel = Labels[LoginLabels.LabelsList.AccessButtonLabel];

        public string LoginButtonLabel_AD
        {
            get { return _LoginButtonLabel_AD; }
            set { _LoginButtonLabel_AD = value; }
        }
        private string _LoginButtonLabel_AD = Labels[LoginLabels.LabelsList.AccessButtonLabelAD];

        //public override string IDsOffset
        //{
        //    get
        //    {
        //        if (_IDsOffset == null)
        //            _IDsOffset = "_LoginControl" + this.LoginsControlsCount;
        //        return _IDsOffset;
        //    }
        //}
        //private string _IDsOffset;
        
        //private bool DebugModeEnabled
        //{
        //    get;
        //    //{
        //    //    return NetCms.Configurations.Debug.DebugLoginEnabled;
        //    //}

        //    set;
        //}
    
        public override G2Core.Common.RequestVariable PostbackChecker
        {
            get
            {
                if (_PostbackChecker == null)
                    _PostbackChecker = new G2Core.Common.RequestVariable("loginpostback" + this.IDsOffset,G2Core.Common.RequestVariable.RequestType.Form);
                return _PostbackChecker;
            }
        }
        private G2Core.Common.RequestVariable _PostbackChecker;                  		

        private int LoginErrorCount
        {
            get
            {
                return  (HttpContext.Current.Session["LoginTry"] != null)?(int)HttpContext.Current.Session["LoginTry"]:0;
            }
        }

        //private bool MostraRegistrazione;
        //private bool MostraRecuperoPassword;
        //private string LinkRegistrazione;

        private bool MostraSoloADCtrl;
        private string SSOPageUrl;

        public LoginControlActiveDirectory()
        {
            
        }

        public LoginControlActiveDirectory(string redirectURL, string ssoPageUrl = "", bool mostraRegistrazione = false, bool mostraRecuperoPassword = false, bool mostraSoloADCtrl = false, string linkReg = "")           
        {
            RedirectUrl = redirectURL;
            CssClass = "LoginControl";
            MostraRegistrazione = mostraRegistrazione;
            MostraRecuperoPassword = mostraRecuperoPassword;
            LinkRegistrazione = linkReg;
            MostraSoloADCtrl = mostraSoloADCtrl;
            SSOPageUrl = ssoPageUrl;
        }
     
        public LoginControlActiveDirectory(LoginManager.Models.LoginSetting settings)
        {
            CssClass = "LoginControl";

            RedirectUrl = settings.RedirectUrl;            
            MostraRegistrazione = settings.ShowRegistrationLink;
            MostraRecuperoPassword = settings.ShowPasswordRecoveryLink;
            LinkRegistrazione = settings.RegistrationtUrl;
            SSOPageUrl = settings.AutenticationUrl;


            DebugModeEnabled = settings.ShowDebugControl || NetCms.Configurations.Debug.DebugLoginEnabled;

            MostraSoloADCtrl = true;
        }

        //public override Button LoginButton
        //{
        //    get
        //    {
        //        if (_LoginButton == null)
        //        {
        //            _LoginButton = new Button();
        //            _LoginButton.ID = PostbackChecker.Key;
        //            _LoginButton.Text = this.LoginButtonLabel;
        //            _LoginButton.Click += loginButton_Click;                  
        //        }
        //        return _LoginButton;
        //    }

        //    set
        //    {
        //        _LoginButton = value;
        //    }
        //}
        //private Button _LoginButton;

        //public Button LoginButtonAD
        //{
        //    get {

        //        if(_LoginButtonAD == null)
        //        {
        //            _LoginButtonAD = new Button();
        //            _LoginButtonAD.ID = PostbackChecker.Key + "_AD";
        //            _LoginButtonAD.Text = this.LoginButtonLabel_AD;
        //            _LoginButtonAD.Click += LoginButtonAD_Click;
        //        }
        //        return _LoginButtonAD;
        //    }
        //    set { _LoginButtonAD = value; }
        //}
        //private Button _LoginButtonAD;

        //public override WebControl NotifiyList
        //{
        //    get
        //    {
        //        if (_NotifiyList == null)
        //        {
        //            _NotifiyList = new WebControl(HtmlTextWriterTag.Ul);


        //        }
        //        return _NotifiyList;
        //    }
        //}
        //private WebControl _NotifiyList;

        //public override WebControl NotifiyArea
        //{
        //    get
        //    {
        //        if (_NotifiyArea == null)
        //        {
        //            _NotifiyArea = new WebControl(HtmlTextWriterTag.Div);
        //            _NotifiyArea.CssClass = "NotifyArea";
        //            if (HttpContext.Current.Session != null)                    
        //            {
        //                if(HttpContext.Current.Session["AnotherBrowserLogged"] != null)
        //                {
        //                    AddNotify(Labels[LoginLabels.LabelsList.AccessFromOtherBrowser]);
        //                    HttpContext.Current.Session.Remove("AnotherBrowserLogged");
        //                }
        //                if (HttpContext.Current.Session["SSOUSER_NOTENABLED"] != null && HttpContext.Current.Session["SSOUSER_NOTENABLED"]=="0")
        //                {
        //                    AddNotify(Labels[LoginLabels.LabelsList.NotGrantedUser]);
        //                    HttpContext.Current.Session["SSOUSER_NOTENABLED"] = "1";
        //                }                       
        //            }
                    
        //        }
        //        return _NotifiyArea;
        //    }
        //}
        //private WebControl _NotifiyArea;

        protected override ICollection<KeyValuePair<int, bool>> CollectGrants(User _user)
        {
            ICollection<KeyValuePair<int, bool>> _FoldersCanView = new List<KeyValuePair<int, bool>>();
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                User user = UsersBusinessLogic.GetById(_user.ID, sess);
                var networks = user.AssociatedNetworks;
                foreach (var net in networks)
                {
                    var folders = FolderBusinessLogic.FindNetworkFoldersForGrantsCollection(net.Network.ID, sess);
                    foreach (var folder in folders)//net.Network.NetworkFolders.Cast<StructureFolder>().Where(x => !x.Hidden && !x.IsSystemFolder && x.Depth <= 3))
                    {
                        GrantsFinder Criteria = new FolderGrants(folder, user.Profile, sess);
                        _FoldersCanView.Add(new KeyValuePair<int, bool>(folder.ID, Criteria.NeedToShow("show").Allowed));
                    }
                }
            }
            return _FoldersCanView;
        }

        static void CallbackMethod(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;
            NetCms.Users.LoginControl.CollectGrantsDelegate caller = (NetCms.Users.LoginControl.CollectGrantsDelegate)result.AsyncDelegate;
            ReturnMessage mess = (ReturnMessage)result.GetReplyMessage();

            HttpSessionState session = (HttpSessionState)ar.AsyncState;
            //ICollection<KeyValuePair<int, bool>> returnValue = caller.EndInvoke(ar);

            ICollection<KeyValuePair<int, bool>> _FoldersCanView = (List<KeyValuePair<int, bool>>)mess.ReturnValue;
            if (session != null)
            {
                User currUser = (User)session[Configurations.Generics.BackAccountSessionKey];
                if (currUser != null)
                {
                    if (session["FoldersCanViewUSER_" + currUser.ID] != null)
                    {
                        lock (session["FoldersCanViewUSER_" + currUser.ID])
                        {
                            session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                        }
                    }
                    else
                        session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                }
                //currUser.FoldersCanView = _FoldersCanView;
            }
        }

        //public override void loginButton_Click(object sender, EventArgs e)
        //{
        //    NetUtility.RequestVariable userName = new NetUtility.RequestVariable("username" + this.IDsOffset, HttpContext.Current.Request.Form);
        //    NetUtility.RequestVariable password = new NetUtility.RequestVariable("password" + this.IDsOffset, HttpContext.Current.Request.Form);

        //    if (userName.IsValid() && password.IsValid() && password.StringValue.Length>0)
        //    {
        //        string MD5Pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password.StringValue, "MD5").ToString();                

        //        User user = UsersBusinessLogic.GetActiveUserByUserNameAndPassword(userName.StringValue, MD5Pwd);

        //        if (user != null)
        //        {
        //            NetCms.Users.LoginResult result = NetCms.Users.AccountManager.TryLoginUser(user, MD5Pwd);
        //            if (result.Result == LoginResult.LoginResults.Logged)
        //            {                        
        //                HttpSessionState webSession = HttpContext.Current.Session;
                        
        //                NetCms.Users.LoginControl.CollectGrantsDelegate myAction = new NetCms.Users.LoginControl.CollectGrantsDelegate(CollectGrants);
        //                myAction.BeginInvoke(user, new AsyncCallback(CallbackMethod), webSession);
                        
        //                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente '" + userName.StringValue + "' ha effettuato il Login al sistema.");
        //                var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
                        
        //                if (RedirectUrl.Length == 0)
        //                    RedirectUrl = "/";
                        
        //                NetCms.Diagnostics.Diagnostics.Redirect(httpsHandler.ConvertPageAddressAsNonSecure(RedirectUrl));
        //            }
        //            else
        //            {
        //                AddLoginTry(userName.StringValue);
        //                AddNotify(Labels[LoginLabels.LabelsList.YourAccountWasDisabled]);
        //                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'account dell'utente '" + userName.StringValue + "' è stato disabilitato a causa della scadenza dei 180 giorni dall'accesso.", "", "", "");
        //            }
        //        }
        //        else
        //        {
        //            AddLoginTry(userName.StringValue);
        //            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "Tentativo di login al sistema fallito con il seguente nome utente: '" + userName.StringValue + "' ha effettuato il Login al sistema.", "", "", "");
        //            AddNotify(Labels[LoginLabels.LabelsList.NotValidUserAndPass]);
        //        }
        //    }
        //    else
        //    {
        //        AddLoginTry(userName.StringValue);
        //        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "Tentativo di login al sistema fallito con il seguente nome utente: '" + userName.StringValue + "' ha effettuato il Login al sistema.", "", "", "");
        //        AddNotify(Labels[LoginLabels.LabelsList.NotValidUserAndPass]);
        //    }   
        //}

        public override void loginButton_Click(object sender, EventArgs e)
        {
           System.Web.HttpContext.Current.Session["PreviousPage"] = HttpContext.Current.Request.Url.AbsoluteUri;
            if (string.IsNullOrEmpty(SSOPageUrl))
                NetCms.Diagnostics.Diagnostics.Redirect("/sso/default.aspx");
            else
                NetCms.Diagnostics.Diagnostics.Redirect(SSOPageUrl);
        }         

        protected override void OnInit(EventArgs e)
        {
           // base.OnInit(e);

            this.ID = "LoginControl" + this.LoginsControlsCount;            

            this.Controls.Add(NotifiyArea);

            InitLoginControls();           
        }    

        public override void InitLoginControls()
        {
            WebControl links = new WebControl(HtmlTextWriterTag.Ul);
            WebControl liLink = new WebControl(HtmlTextWriterTag.Li);

            //if (MostraRecuperoPassword)
            //{
            //    liLink = new WebControl(HtmlTextWriterTag.Li);
            //    WebControl forgotPasswordLink = new WebControl(HtmlTextWriterTag.A);                
            //    forgotPasswordLink.Attributes.Add("href", ForgotPasswordUrl);
            //    forgotPasswordLink.Controls.Add(new LiteralControl(RecoverPasswordLinkLabel));
            //    liLink.Controls.Add(forgotPasswordLink);
            //    links.Controls.Add(liLink);
            //}

            //if (MostraRegistrazione)
            //{
            //    liLink = new WebControl(HtmlTextWriterTag.Li);
            //    WebControl showRegLink = new WebControl(HtmlTextWriterTag.A);
            //    showRegLink.Attributes.Add("href", LinkRegistrazione);
            //    showRegLink.Controls.Add(new LiteralControl(UserRegistrationLinkLabel));
            //    liLink.Controls.Add(showRegLink);
            //    links.Controls.Add(liLink);
            //}

            //if (links.HasControls())
            //    this.Controls.Add(links);

            LoginButton.Text = LoginButtonLabel_AD;

            this.Controls.Add(LoginButton);
            
            if (this.DebugModeEnabled)
            {
                LoginControlDebug DebugLogin = new LoginControlDebug();               
                this.Controls.Add(DebugLogin);
            }
        }

        //public override void InitForgotPasswordControls()
        //{
        //    string message = "";

        //    #region Costruisco i Controlli form

        //    TextBox UserName = new TextBox();
        //    UserName.CssClass = "textbox";
        //    UserName.ID = "username" + IDsOffset;

        //    Label UserName_Label = new Label();
        //    UserName_Label.Text = Labels[LoginLabels.LabelsList.Username] + ": ";
        //    UserName_Label.ID = "UserName_Label" + IDsOffset;
        //    UserName_Label.AssociatedControlID = UserName.ID;

        //    #endregion
            
        //    this.Attributes["class"] = "LoginFormContainer";

        //    HtmlGenericControl info = new HtmlGenericControl("div");
        //    info.Attributes["class"] = "LoginInfo";
        //    info.InnerHtml = message;
        //    this.Controls.Add(info);

        //    Par FormContainer = new Par();
        //    FormContainer.Class = "FormContainer";

        //    FormContainer.Controls.Add(UserName_Label);
        //    FormContainer.Controls.Add(UserName);

        //    FormContainer.Controls.Add(UserName_Label);
        //    FormContainer.Controls.Add(UserName);

        //    this.Controls.Add(FormContainer);
            
        //    FormContainer = new Par();
        //    FormContainer.Class = "FormContainer";

        //    this.Controls.Add(FormContainer);

        //    //Attacco il controllo submit
        //    FormContainer = new Par();

        //    FormContainer.Controls.Add(RecoverPasswordButton);
        //    FormContainer.Controls.Add(ResetButton);

        //    this.Controls.Add(FormContainer);
        //}     
    }
}

