using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetUtility.NetTreeViewControl;
/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Structure.Grants
{
    public partial class GroupsGrants
    {
        public class GroupTreeControl
        {
            private PageData PageData;

            public string ID
            {
                get { return _ID; }
            }
            private string _ID;

            public NetTreeViewType TreeType
            {
                get { return _TreeType; }
                set { _TreeType = value; }
            }
            private NetTreeViewType _TreeType;

            public string Label
            {
                get
                {
                    return _Label;
                }
                set { _Label = value; }
            }
            private string _Label;

            public GroupTreeControl(string ControlID, string label, PageData pageData)
            {
                PageData = pageData;
                _ID = ControlID;
                Label = label;
                _TreeType = NetTreeViewType.StaticSimple;
            }

            public HtmlGenericControl getControl()
            {
                return getControl(new NetTreeNodeStyle(PageData.Paths.AbsoluteRoot + "/css/"));
            }

            public HtmlGenericControl getControl(NetTreeNodeStyle NodeStyle)
            {
                HtmlGenericControl output = new HtmlGenericControl("div");

                HtmlGenericControl p = new HtmlGenericControl("p");
                p.InnerHtml = Label;
                output.Controls.Add(p);

                GroupTreeBinder binder = new GroupTreeBinder(PageData);
                NetTreeNode node = binder.getTree(NodeStyle);
                NetTreeView tree = new NetTreeView(ID);
                tree.Root = node;
                tree.Type = TreeType;

                output.Controls.Add(tree.getControl());

                return output;
            }
        }
    }
}