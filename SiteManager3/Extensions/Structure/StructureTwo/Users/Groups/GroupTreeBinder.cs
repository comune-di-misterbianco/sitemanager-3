using System.Data;
using NetUtility.NetTreeViewControl;
using NetCms.Connections;

/// <summary>
/// Summary description for StructureFolderBind
/// </summary>
/// 
namespace NetCms.Structure.Grants
{
    public class GroupTreeBinder
    {
        private Connection _Conn;
        private PageData PageData;

        public int UnavailableBranchID
        {
            get { return _UnavailableBranchID; }
            set { _UnavailableBranchID = value; }
        }
        private int _UnavailableBranchID = 0;			

        public GroupTreeBinder(PageData pageData)
        {
            _Conn = pageData.Conn;
            PageData = pageData;
        }

        public NetTreeNode getTree(NetTreeNodeStyle style)
        {
            DataTable table = _Conn.SqlQuery("SELECT * FROM Groups WHERE Tree_Group NOT LIKE '%." + NetUtility.TreeUtility.FormatNumber( UnavailableBranchID.ToString()) + ".%' ORDER BY Tree_Group");
            NetTreeNode root = null;


            string GroupName;
            string GroupID;
            string GroupProfile;
            string GroupTree;
            bool read = table.Rows.Count > 0;

            if (read)
            {
                DataRow row = table.Rows[0];
                GroupName = row["Name_Group"].ToString();
                GroupID = row["id_Group"].ToString();
                GroupTree = row["Tree_Group"].ToString();
                GroupProfile = row["Profile_Group"].ToString();

                root = new NetTreeNode(style);
                root.Text = GroupName;
                root.Tag = GroupProfile;
                root.Value = GroupID;
                root.ID = GroupID;
                root.Grant = getGroupUserLink(GroupID, GroupTree);

                NetTreeNode lastGroup = root;
                for (int j = 1; j < table.Rows.Count; j++)
                {
                    row = table.Rows[j];
                    GroupName = row["Name_Group"].ToString();
                    GroupID = row["id_Group"].ToString();
                    GroupTree = row["Tree_Group"].ToString();
                    GroupProfile = row["Profile_Group"].ToString();


                    NetTreeNode Group = new NetTreeNode(style);
                    Group.Grant = getGroupUserLink(GroupID, GroupTree);
                    Group.Text = GroupName;
                    Group.Tag = GroupProfile;
                    Group.Value = GroupID;
                    Group.ID = GroupID;
                    int GroupDepth = int.Parse(row["Depth_Group"].ToString());
                    if (GroupDepth == lastGroup.Depth)
                    {
                        lastGroup.Parent.ChildNodes.Add(Group);
                    }
                    if (GroupDepth > lastGroup.Depth)
                    {
                        lastGroup.ChildNodes.Add(Group);
                    }
                    if (GroupDepth < lastGroup.Depth)
                    {
                        int GroupGap = lastGroup.Depth - GroupDepth;
                        NetTreeNode parent = lastGroup;
                        for (int i = 0; i <= GroupGap; i++)
                            parent = parent.Parent;
                        parent.ChildNodes.Add(Group);
                    }

                    lastGroup = Group;
                }
            }
            //_Conn.Close();
            return root;
        }
        private DataTable usergroups
        {
            get
            {
                if (_usergroups == null)
                    _usergroups = _Conn.SqlQuery("Select * From UsersGroups WHERE User_UserGroup = " + PageData.Account.ID);
                return _usergroups;
            }
        }
        private DataTable _usergroups;


        public bool getGroupUserLink(string GroupID, string GroupTree)
        {
            string[] parents = GroupTree.Split('.');
            string where = "(";
            for (int i = 1; i < parents.Length - 1; i++)
            {
                if (i != 1)
                    where += " OR ";
                where += "(Group_UserGroup = " + parents[i] + "AND Type_UserGroup = 2)";
            }
            where += ") OR ";
            where += " (Group_UserGroup = " + GroupID + "AND Type_UserGroup = 1)";

            return usergroups.Select(where).Length > 0;
        }
    }
}