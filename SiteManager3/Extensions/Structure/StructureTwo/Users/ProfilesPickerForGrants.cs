﻿using System;
using System.Data;
using System.Web.UI.HtmlControls;
using NetTable;
using System.Linq;
using NetService.Utility.ArTable;
using System.Collections.Generic;
using NetCms.Users;
using System.Web.UI.WebControls;
using NetCms.Grants.Interfaces;
/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.Grants
{
    public partial class ProfilesPickerForGrants
    {


        private string _Title;
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private IGrantsFindable _GrantObject;
        public IGrantsFindable GrantObject
        {
            get { return _GrantObject; }
            set { _GrantObject = value; }
        }

        private int _NetworkId;
        public int NetworkId
        {
            get { return _NetworkId; }
            set { _NetworkId = value; }
        }

        public NetCms.Users.User CurrentUser
        {
            get
            {
                return _CurrentUser;
            }
            set { _CurrentUser = value; }
        }
        private NetCms.Users.User _CurrentUser;

        private bool profileInserted = false;
        public bool ProfileInserted
        {
            get
            {
                return profileInserted;
            }
        }

        public ProfilesPickerForGrants(string title, NetCms.Users.User currentUser,IGrantsFindable obj, int networkId = 0)
        {
            _GrantObject = obj; 
            CurrentUser = currentUser;
            _Title = title;
            NetworkId = networkId;
        }

        private HtmlGenericControls.InputField _AddProfileField;
        public HtmlGenericControls.InputField AddProfileField
        {
            get
            {
                if (_AddProfileField == null)
                {
                    _AddProfileField = new HtmlGenericControls.InputField("AddProfileField");

                }
                return _AddProfileField;
            }
        }
  
        public HtmlGenericControl Control
        {
            get
            {
                HtmlGenericControls.Div set = new HtmlGenericControls.Div();

                NetCms.Users.UserSelectControl users = new NetCms.Users.UserSelectControl("UsersCTR", "Utenti che è possibile associare", 7);
                set.Controls.Add(users);

                #region Campi Hidden e Relativi Postback

                /****************************************************************************************************************************************/
                /****************  Aggiunta Profilo  ************************************************************************************/
                /****************************************************************************************************************************************/

                set.Controls.Add(AddProfileField.Control);
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                if (AddProfileField.RequestVariable.IsPostBack && AddProfileField.RequestVariable.IsValidInteger && AddProfileField.RequestVariable.IntValue > 0)
                {
                    bool IsAssociated = GrantObject.CheckIfProfileIsAssociated(AddProfileField.RequestVariable.IntValue,NetworkId);
                    //System.Data.DataTable sqltable = this.Connection.SqlQuery(string.Format(SqlSelectModel, AddProfileField.RequestVariable.StringValue));
                    //if (sqltable.Rows.Count == 0)
                    if (!IsAssociated)
                    {
                        this.profileInserted = GrantObject.AddGrantProfile(AddProfileField.RequestVariable.IntValue,NetworkId);
                        
                        //if (SqlExecuteModel.ToLower().StartsWith("insert "))
                        //{
                        //    this.lastExecuteID = this.Connection.ExecuteInsert(string.Format(SqlExecuteModel, AddProfileField.RequestVariable.StringValue));
                        //    this.queryExecuted = this.lastExecuteID > 0;
                        //}
                        //else
                        //    this.queryExecuted = this.Connection.Execute(string.Format(SqlExecuteModel, AddProfileField.RequestVariable.StringValue));
                    }
                    else
                    {
                        //NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha provato ad aggiungere il profilo con id " + AddProfileField.RequestVariable.StringValue + ", che però era già associato.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);                        
                        NetCms.GUI.PopupBox.AddMessage("L'utente è già associato all'applicazione", NetCms.GUI.PostBackMessagesType.Notify);
                    }
                }


                #endregion

                GUI.Collapsable collapsable = new NetCms.GUI.Collapsable("Gruppi che è possibile associare");
                collapsable.StartCollapsed = true;
                set.Controls.Add(collapsable);
                #region Tabella dei Gruppi

                ArTable table = new ArTable();
                table.InnerTableCssClass = "tab";
                table.NoRecordMsg = "Nessun gruppo trovato";

                NetCms.Users.Search.UserRelated rels = new Users.Search.UserRelated(this.CurrentUser);
                ICollection<Group> groups = rels.GroupsIAdmin(0, rels.GroupsIAdminCount(null, null), null, null).OrderBy(x=>x.Tree).ToList(); //GroupsBusinessLogic.FindGroupsByUser(this.CurrentUser);

                table.Records = groups;

                // SISTEMARE SE SERVE
                //SmTableTreeColumn2 treecol = new SmTableTreeColumn2("Gruppo", "id_Group", "Name_Group", "Depth_Group", this.Connection);
                //treecol.IconType = SmTableTreeColumn2.IconTypes.CssClass;
                //treecol.Icon = "GrantsTableGroupIcon";
                //stable.addColumn(treecol);

                ArGroupTreeColumn nome = new ArGroupTreeColumn("Gruppo", "Name", groups.ToList<object>());
                nome.Icon = "GrantsTableGroupIcon";
                table.AddColumn(nome);

                //ArTextColumn nome = new ArTextColumn("Gruppo", "Name");
                //table.AddColumn(nome);

                ProfilePickerTableActionColumn col = new ProfilePickerTableActionColumn("Aggiungi", "javascript: setFormValue('{ProfileID}','" + AddProfileField.ID + "',1)");
                table.AddColumn(col);

                #endregion

                collapsable.AppendControl(table);

                return set;
            }
        }

        private class ProfilePickerTableActionColumn : ArActionColumn
        {
            public ProfilePickerTableActionColumn(string Caption, string href)
                : base(Caption, Icons.Add, href)
            {
                
            }

            protected override System.Web.UI.WebControls.TableCell GetCellControl(object objectInstance, string label, string href, ArActionColumn.Icons icon)
            {
                TableCell td = new TableCell();
                if (icon != null)
                {
                   td.CssClass = "action " + icon.ToString().ToLower();
                }

                string value = objectInstance.GetType().GetProperty("ProfileID").GetValue(objectInstance,null).ToString();

                string anchorCssClassHtml = string.IsNullOrEmpty(AnchorCssClass) ? "" : "class=\"" + this.AnchorCssClass + "\"";

                td.Text = "<a " + anchorCssClassHtml + " href=\"";
                td.Text += href.Replace("{ProfileID}", value);
                td.Text += "\" title=\"" + Caption + "\"><span>";
                td.Text += Caption;
                td.Text += "</span></a>";

                return td;
            }
        }
    }
}


