﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using NHibernate;
using NetCms.Vertical;
using NetCms.Vertical.BusinessLogic;
using NetCms.Grants;
using System.Linq;
using NetCms.Networks;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;

namespace NetCms.Users
{
    public class UserPermissionRepair
    {
        //private NetCms.Connections.Connection _Connection;
        //private NetCms.Connections.Connection Connection
        //{
        //    get
        //    {
        //        if (_Connection == null)
        //            _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
        //        return _Connection;
        //    }
        //}

        public User CurrentUser
        {
            get { return _CurrentUser; }
            private set { _CurrentUser = value; }
        }
        private User _CurrentUser;

        public Profile Profile
        {
            get { return _Profile; }
            private set { _Profile = value; }
        }
        private Profile _Profile;

        public int NetworkID
        {
            get { return _NetworkID; }
            private set { _NetworkID = value; }
        }
        private int _NetworkID;

        public UserPermissionRepair(Profile profile, User currentUser)
        {
            Profile = profile;
            CurrentUser = currentUser;
        }
        public UserPermissionRepair(Profile profile, User currentUser, int networkID)
        {
            Profile = profile;
            CurrentUser = currentUser;
            this.NetworkID = networkID;
        }

        public void FixPermissions()
        {
            this.FixPermission_VerticalApplications();
            this.FixPermission_All();
        }

        public void FixPermission_VerticalApplications()
        {
            IList<VerticalApplication> vapps = VerticalAppBusinessLogic.FindAllVerticalApplications();
            //foreach (DataRow vapp in Connection.SqlQuery("SELECT * FROM externalappz").Rows)
            foreach(VerticalApplication vapp in vapps)
            {
                FixPermission_VerticalApplication(vapp.ID);
            }
        }

        public bool FixPermission_VerticalApplication(int AppID)
        {
            return FixPermission_VerticalApplication(AppID, 0);
        }
        public bool FixPermission_VerticalApplication(int AppID, int networkID)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {

                    VerticalApplication app = VerticalAppBusinessLogic.GetVerticalAppById(AppID,sess);
                    ExternalAppzGrantsBusinessLogic.DeleteExternalAppProfileByAppAndProfile(AppID,Profile.ID,sess);

                    ObjectProfile profile = app.AddGrantProfile(Profile.ID, networkID,sess);

                    IList<ExternalAppzCriteria> criteri = ExternalAppzGrantsBusinessLogic.FindCriteriByApp(AppID, sess);
                    foreach (ExternalAppzCriteria criterio in criteri)
                        app.AddGrant(profile.ID, 1, criterio.ID, this.CurrentUser.ID,sess);
                    tx.Commit();
                    return true;

                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    return false;
                }
            }


            //string sql = "DELETE FROM externalappz_profiles WHERE Application_ExternalAppzProfile  =" + AppID + " AND Profile_ExternalAppzProfile = " + Profile.ID;
            //this.Connection.Execute(sql);

            //sql = "INSERT INTO externalappz_profiles (Application_ExternalAppzProfile,Profile_ExternalAppzProfile,Network_ExternalAppzProfile,Creator_ExternalAppzProfile) VALUES (" + AppID + "," + Profile.ID + "," + networkID + "," + this.CurrentUser.ID + ")";
            //int AssociationID = Connection.ExecuteInsert(sql);

            //DataTable criteria = Connection.SqlQuery("SELECT * FROM externalappzcriteria WHERE App_ExternalAppCriteria = " + AppID);
            ////DataTable table = Connection.SqlQuery("SELECT * FROM externalappz_profiles INNER JOIN externalappzcriteria ON App_ExternalAppCriteria = Application_ExternalAppzProfile WHERE Key_ExternalAppCriteria = 'enable' AND Profile_ExternalAppzProfile = " + Profile.ID);
            //sql = "";
            //foreach (DataRow trow in criteria.Rows)
            //    sql += (sql.Length == 0 ? "" : ",") + "(" + trow["id_ExternalAppCriteria"] + "," + AssociationID + ",1," + this.CurrentUser.ID + ")";

            //if (sql.Length > 0)
            //{
            //    sql = "INSERT INTO externalappzgrants (Criteria_ExternalAppGrant,ProfileAssociation_ExternalAppGrant,Value_ExternalAppGrant,Author_ExternalAppGrant) VALUES " + sql;
            //    Connection.Execute(sql);
            //}
        }

        /*
            criteri.Add("show");
            criteri.Add("create");
            criteri.Add("modify");
            criteri.Add("delete");
            criteri.Add("newfolder");
            criteri.Add("grants");
            criteri.Add("redactor");
            criteri.Add("publisher");
            criteri.Add("advanced");
            criteri.Add("foldersvisibility");
         */

        public bool FixPermission_All()
        {
            //DataTable Criteria = Connection.SqlQuery("SELECT * FROM Criteri");
            //List<string> criteri = new List<string>();
            //foreach (DataRow criterion in Criteria.Rows)
            //    criteri.Add(criterion["key_Criterio"].ToString());

            List<string> criteri = GrantsBusinessLogic.FindAllCriteri().Select(x => x.Key).ToList();

            return FixPermission_Networks(criteri);
        }

        public void FixPermission_Publisher()
        {
            List<string> criteri = new List<string>();

            criteri.Add("show");
            criteri.Add("create");
            criteri.Add("modify");
            criteri.Add("delete");
            criteri.Add("redactor");
            criteri.Add("publisher");

            //DataTable Criteria = Connection.SqlQuery("SELECT * FROM Criteri WHERE key_Criterio LIKE 'newfolder%'");
            var newFolderCriteri = GrantsBusinessLogic.FindAllCriteri().Where(x => x.Key.StartsWith("newfolder"));
            //foreach (DataRow criterion in Criteria.Rows)
            //    criteri.Add(criterion["key_Criterio"].ToString());
            foreach (Criterio criterio in newFolderCriteri)
                criteri.Add(criterio.Key);
                        

            FixPermission_Networks(criteri);
        }

        //public void FixPermission_Review()
        //{
        //    List<string> criteri = new List<string>();

        //    criteri.Add("show");
        //    criteri.Add("create");
        //    criteri.Add("modify");
        //    criteri.Add("delete");
        //    criteri.Add("redactor");

        //    DataTable Criteria = Connection.SqlQuery("SELECT * FROM Criteri WHERE key_Criterio LIKE 'newfolder%'");
        //    foreach (DataRow criterion in Criteria.Rows)
        //        criteri.Add(criterion["key_Criterio"].ToString());

        //    FixPermission_Networks(criteri);
        //}

        //public void FixPermission_Write()
        //{
        //    List<string> criteri = new List<string>();

        //    criteri.Add("show");
        //    criteri.Add("create");
        //    criteri.Add("modify");
        //    criteri.Add("newfolder");
        //    criteri.Add("advanced");

        //    FixPermission_Networks(criteri);
        //}

        //public void FixPermission_GrantsAdministrator()
        //{
        //    List<string> criteri = new List<string>();

        //    criteri.Add("show");
        //    criteri.Add("create");
        //    criteri.Add("modify");
        //    criteri.Add("delete");
        //    criteri.Add("grants");
        //    DataTable Criteria = Connection.SqlQuery("SELECT * FROM Criteri WHERE key_Criterio LIKE 'newfolder%'");
        //    foreach (DataRow criterion in Criteria.Rows)
        //        criteri.Add(criterion["key_Criterio"].ToString());

        //    FixPermission_Networks(criteri);
        //}

        private bool FixPermission_Networks(List<string> criteri)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    IList<BasicNetwork> networks = new List<BasicNetwork>();
                    if (NetworkID > 0)
                        networks.Add(NetworksBusinessLogic.GetById(NetworkID, sess));
                    else
                        networks = NetworksBusinessLogic.FindAllNetworksPlusFiltersFromConfig(false, sess);

                    IList<Criterio> criteria = GrantsBusinessLogic.FindAllCriteri(sess);

                    foreach (BasicNetwork net in networks)
                    {
                        GrantsBusinessLogic.DeleteFolderProfilesByProfile(this.Profile.ID, net.ID, sess);

                        StructureNetwork decoratedNet = new StructureNetwork(net);
                        StructureFolder rootFolder = FolderBusinessLogic.GetById(sess,decoratedNet.RootFolderID);
                        ObjectProfile profileAssociation = rootFolder.AddGrantProfile(this.Profile.ID, sess);

                        foreach (Criterio criterio in criteria)
                        {
                            bool allow = criteri.Contains(criterio.Key);
                            rootFolder.AddGrant(profileAssociation.ID, (allow ? 1 : 2), criterio.ID, this.CurrentUser.ID, sess);
                        }

                    }

                    tx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    return false;
                }
            }

            //DataTable networks = Connection.SqlQuery("SELECT * FROM Networks " + (NetworkID != null ? " WHERE id_Network = " + this.NetworkID : ""));

            //DataTable Criteria = Connection.SqlQuery("SELECT * FROM Criteri");

            //foreach (DataRow row in networks.Rows)
            //{
            //    NetCms.Connections.Connection netConnection = NetCms.Connections.ConnectionsManager.GetNetworkConnection(row["Nome_Network"].ToString()); //NetCms.Connections.Sm2Connection.NewConnection1(NetCms.Configurations.ConnectionsStrings.MainDB, row["Nome_Network"].ToString());

            //    string sql = "DELETE FROM folderprofiles WHERE Profile_FolderProfile = " + this.Profile.ID;
            //    netConnection.Execute(sql);

            //    string rootFolderID = netConnection.SqlQuery("SELECT id_Folder FROM Folders WHERE Depth_Folder = 1").Rows[0][0].ToString();

            //    string sqlInsert = "INSERT INTO folderprofiles (Folder_FolderProfile,Profile_FolderProfile,Creator_FolderProfile) VALUES (" + rootFolderID + "," + this.Profile.ID + "," + this.CurrentUser.ID + ")";

            //    int fpid = netConnection.ExecuteInsert(sqlInsert);

            //    sqlInsert = "";

            //    foreach (DataRow criterion in Criteria.Rows)
            //    {
            //        bool allow = true;//criteri.Contains(criterion["Key_Criterio"].ToString());
            //        sqlInsert += (sqlInsert.Length == 0 ? "" : ",");
            //        sqlInsert += "(" + criterion["id_Criterio"] + "," + fpid + "," + (allow ? "1" : "2") + ",0," + this.CurrentUser.ID + ")";
            //    }

            //    sqlInsert = "INSERT INTO folderprofilescriteria (Criteria_FolderProfileCriteria,FolderProfile_FolderProfileCriteria,Value_FolderProfileCriteria,Application_FolderProfileCriteria,Updater_FolderProfileCriteria) VALUES " + sqlInsert;
            //    netConnection.Execute(sqlInsert);
            //}
        }
    }
}
