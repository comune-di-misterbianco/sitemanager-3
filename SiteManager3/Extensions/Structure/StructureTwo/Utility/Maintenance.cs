﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NetCms.Utility
{
    public static class Maintenance
    {
        public static void Handle()
        {
            try
            {
                NetCms.Configurations.CmsStatusEnum currentStatus = (NetCms.Configurations.CmsStatusEnum)HttpContext.Current.Application["CmsStatus"];

                if (currentStatus == NetCms.Configurations.CmsStatusEnum.maintenance)
                {
                    OfflineData data = OfflineFileReadData();

                    if (HttpContext.Current.Request != null && data.IpAddressToLetThrough.Any())
                    {
                        var currentPath = HttpContext.Current.Request.Path;

                        if (!currentPath.ToLower().Contains("/cms"))
                        {
                            if (currentPath == "/" || (currentPath.Contains(".aspx") && !currentPath.ToLower().Contains("errorpage.aspx") && !currentPath.ToLower().Contains("webresource.axd")))
                            {
                                if (NetCms.Users.AccountManager.CurrentAccount == null)
                                {
                                    if (DateTime.UtcNow.Subtract(data.TimeWhenSiteWillGoOfflineUtc).TotalSeconds > 0
                                        && !data.IpAddressToLetThrough.Contains(NetCms.Configurations.Generics.GetClientIPAddress))
                                    HttpContext.Current.Server.Transfer("/models/maintenance.aspx", false);
                                }
                                else if (NetCms.Users.AccountManager.CurrentAccount.God != 1) //add check ip
                                {
                                    if (DateTime.UtcNow.Subtract(data.TimeWhenSiteWillGoOfflineUtc).TotalSeconds > 0
                                       && !data.IpAddressToLetThrough.Contains(NetCms.Configurations.Generics.GetClientIPAddress))
                                        HttpContext.Current.Server.Transfer("/models/maintenance.aspx", false);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static bool Check()
        {
            bool status = false;
            if (System.IO.File.Exists(NetCms.Configurations.Paths.FileMaintenancePath))
            {
                HttpContext.Current.Application["CmsStatus"] = NetCms.Configurations.CmsStatusEnum.maintenance;
                status = true;
            }
            else if ((NetCms.Configurations.CmsStatusEnum)HttpContext.Current.Application["CmsStatus"] == NetCms.Configurations.CmsStatusEnum.maintenance)
                HttpContext.Current.Application["CmsStatus"] = NetCms.Configurations.CmsStatusEnum.ok;
            return status;
        }       

        public static OfflineData OfflineFileReadData()
        {

            OfflineData data = new OfflineData();
            string[] content = null;
            if (System.IO.File.Exists(NetCms.Configurations.Paths.FileMaintenancePath)) {
                content = File.ReadAllText(NetCms.Configurations.Paths.FileMaintenancePath).Split('|');

                DateTime parsedDateTime;

                data.TimeWhenSiteWillGoOfflineUtc = DateTime.TryParse(content[0], null, System.Globalization.DateTimeStyles.RoundtripKind, out parsedDateTime) 
                    ? parsedDateTime : DateTime.UtcNow;
                data.IpAddressToLetThrough = content[1].Split(';');
                data.Message = content[2];
            }
            return data;
        }

        


        public static void SetOffline(int delayByMinutes, string[] allowedIpAddress, string optionalMessage)
        {
            var fields = string.Format("{0:O}{1}{2}{1}{3}",
                                        DateTime.UtcNow.AddMinutes(delayByMinutes), 
                                        "|",
                                        string.Join(";",allowedIpAddress), 
                                        optionalMessage);

            File.WriteAllText(NetCms.Configurations.Paths.FileMaintenancePath, fields);
        }

        public static void RemoveOffline()
        {
            File.Delete(NetCms.Configurations.Paths.FileMaintenancePath);
        }
    }

    public struct OfflineData
    {
        public DateTime TimeWhenSiteWillGoOfflineUtc;
        public string[] IpAddressToLetThrough;
        public string Message;
    }
}
