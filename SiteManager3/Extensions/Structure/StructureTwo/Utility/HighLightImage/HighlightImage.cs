﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace NetCms.HighLightImage
{
    public class HighLightImage
    {
        public HighLightImage() 
        {
        
        }    

        protected internal virtual string Legend
        {
            get;
            set;
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual int RifDoc
        {
            get;
            set;
        }

        public virtual string Align
        {
            get;
            set;
        }

        public virtual string Description
        {
            get;
            set;
        }

        public virtual bool ShowInModulo
        {
            get;
            set;
        }

        public virtual bool ShowInTeaser
        {
            get;
            set;
        }

        public virtual bool ShowInDetail
        {
            get;
            set;
        }
    }
}
