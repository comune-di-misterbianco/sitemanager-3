using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Connections;
using NetCms;
using NetCms.Users;
using NetCms.Networks.Grants;
using System.Linq;
using NetCms.Structure.WebFS;
/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI
{
    public class CmsLoginController:WebControl
    {
        public CmsLoginController()
            : base(HtmlTextWriterTag.Div)
        {
            var httpsHanlder = NetCms.Configurations.Generics.GetHttpsHandler();
            if (!httpsHanlder.IsSecureConnection)
                httpsHanlder.RedirectToSecureRequest();
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.CssClass = "login";

            WebControl logo = new WebControl(HtmlTextWriterTag.Div);
            WebControl Title = new WebControl(HtmlTextWriterTag.H1);
            Title.Controls.Add(new LiteralControl(NetCms.Configurations.PortalData.Nome));
            logo.CssClass = "login-logo";
            logo.Controls.Add(Title);
            this.Controls.Add(logo);

            WebControl Informations = new WebControl(HtmlTextWriterTag.Div);
            {
                WebControl infoTitle = new WebControl(HtmlTextWriterTag.H2);
                infoTitle.Controls.Add(new LiteralControl("Informazioni"));

                Informations.CssClass = "login-info";
                Informations.Controls.Add(infoTitle);
                Informations.Controls.Add(new LiteralControl(G2Core.Common.Utility.RemoveCDATA(NetCms.Configurations.XmlConfig.GetNodeXml("/Portal/configs/loginInfo"))));
            }
            this.Controls.Add(Informations);

            WebControl loginControls = new WebControl(HtmlTextWriterTag.Div);
            {
                WebControl loginEntra = new WebControl(HtmlTextWriterTag.H2);
                loginEntra.Controls.Add(new LiteralControl("Entra"));

                loginControls.CssClass = "login-controls";

                loginControls.Controls.Add(loginEntra);

                //NetCms.Users.LoginControl login = new NetCms.Users.LoginControl();

                LoginControlv3 loginCtrl = new LoginControlv3
               (
                   System.Web.HttpContext.Current.Request.Url.ToString(),
                   false,
                   false,
                   ""
               );


                /*
                List<PopupMessage> PopupMessages = (List<PopupMessage>)HttpContext.Current.Items["PopupMessages"];
                foreach (PopupMessage message in PopupMessages)
                    login.AddNotify(message.Message);*/

                //loginControls.Controls.Add(login);
                loginControls.Controls.Add(loginCtrl);
            }
            this.Controls.Add(loginControls);
        }
    }
}