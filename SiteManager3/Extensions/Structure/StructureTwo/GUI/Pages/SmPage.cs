using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Linq;
using PingSession;
using NetCms.Connections;
using System.Collections.Generic;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI
{
    public abstract class SmPage : WebControl
    {
        #region WebControls

        public abstract SideBar.SideBar SideBar { get; }
        
        private Connection _Conn;
        public virtual Connection Conn
        {
            get
            {
                //if (_Conn == null)
                //    _Conn = NetCms.Connections.ConnectionsManager.CommonConnection;
                //return _Conn;
                ConnectionsManager ConnManager = new ConnectionsManager();
                return ConnManager.CommonConnection;
            }
        }

        public GUI.Toolbar Toolbar
        {
            get
            {
                return this.SideBar.ButtonsToolbar;
            }
        }

        public Info.InfoBox InfoBox
        {
            get
            {
                return this.SideBar.InformationsBox;
            }
        }

        #endregion

        public PopupBox PopupBox
        {
            get
            {
                if (_PopupBox == null)
                    _PopupBox = new PopupBox();
                return _PopupBox;
            }
        }
        private PopupBox _PopupBox;

        public bool DisableContent
        {
            get
            {
                return _DisableContent;
            }
            protected set
            {
                _DisableContent = value;
            }
        }
        public bool _DisableContent;

        public NetCms.GUI.Scripts.ScriptsIndexer Scripts
        {
            get
            {
                if (_Scripts == null)
                    _Scripts = new NetCms.GUI.Scripts.ScriptsIndexer(this.Page);
                return _Scripts;
            }
        }
        private NetCms.GUI.Scripts.ScriptsIndexer _Scripts;

        public NetCms.GUI.SideBar.Boxes.PostbackBox PostbackBox
        {
            get
            {
                if (_PostbackBox == null)
                    _PostbackBox = new NetCms.GUI.SideBar.Boxes.PostbackBox(this.InfoBox);
                return _PostbackBox;
            }
        }
        private NetCms.GUI.SideBar.Boxes.PostbackBox _PostbackBox;

        public NetCms.GUI.Css.ImagesIndexer ImagesIndexer
        {
            get { return NetCms.GUI.GuiUtility.ImagesIndexer; }
        }

        public NetCms.GUI.Icons.IconsIndexer IconsIndexer
        {
            get { return NetCms.GUI.GuiUtility.IconsIndexer; }
        }

        protected NetCms.Users.User Account
        {
            get
            {
                if (_Account == null)
                    _Account = NetCms.Users.AccountManager.CurrentAccount;
                return _Account;
            }
        }
        private NetCms.Users.User _Account;
        public bool AddScriptManager { get; protected set; } 
        
        private NetCms.LogAndTrance.HighLevelLogger _GuiLog;
        public NetCms.LogAndTrance.HighLevelLogger GuiLog
        {
            get
            {
                if (_GuiLog == null)
                    _GuiLog = new NetCms.LogAndTrance.HighLevelLogger(this.Account.ID, this.Page.IsPostBack);
                return _GuiLog;
            }
        }

        public StateBag PageViewState;
        protected const string BaseCssClass = "PageBody";

        public override string CssClass
        {
            get { return BaseCssClass + " " + LocalCssClass; }
        }

        /// <summary>
        /// ProprietÓ da implementare nella classe derivata per specificare una classe Css per i contenuti
        /// </summary>
        public abstract string LocalCssClass { get; }
                
        /// <summary>
        /// Icona della pagina nella toolbar
        /// </summary>
        public abstract NetCms.GUI.Icons.Icons PageIcon
        {
            get;
        }

        /// <summary>
        /// Titolo Della Pagina Nella Toolbar
        /// </summary>
        public abstract string PageTitle
        {
            get;
        }

        /// <summary>
        /// ProprietÓ da implementare nella classe derivata per ottentere il WebControl da aggiungere al corpo pagina
        /// </summary>        
        protected abstract WebControl RootControl { get; }

        protected WebControl _ContentBody;
        protected WebControl ContentBody 
        {
            get
            {
                if (_ContentBody == null)
                {
                    _ContentBody = new WebControl(HtmlTextWriterTag.Div);
                    _ContentBody.ID = "corpo";
                    WebControl root = RootControl;
                    root.CssClass += " innercorpo";
                    if (root != null) 
                        _ContentBody.Controls.Add(root);
                }
                return _ContentBody;
            }
        }

        public bool IsPostBack
        {
            get { return _IsPostBack; }
            private set { _IsPostBack = value; }
        }
        private bool _IsPostBack;

        public SmPage()
            : base(HtmlTextWriterTag.Div)
        {
            PageLayout = PageLayouts.Default;
        }
        
        protected override void OnInit(EventArgs e)
        {            
                base.OnInit(e);

                if (AddScriptManager && Page != null && ScriptManager.GetCurrent(Page) == null)
                    Page.Form.Controls.AddAt(0, new ScriptManager());

                this.IsPostBack = Page.IsPostBack;
                this.Page.Form.Method = "post";
                this.Page.Form.Name = "PageForm";
                this.Page.Form.ID = "PageForm";

                this.AddMetaToHeader();

                if (this.PageLayout == PageLayouts.Default)
                    this.AddCssToHeader();
                else
                    this.AddCssSimpleToHeader();

                if (AddScriptsToHead)
                    this.AddScriptsToHeader();

                this.Page.Header.Title = Configurations.PortalData.TitoloCms + " - " + this.PageTitle;              
        }

        public bool _AddScriptsToHead = true;
        public bool AddScriptsToHead
        {
            get { return _AddScriptsToHead; }
            set {_AddScriptsToHead = value; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (this.SideBar != null && this.PostbackBox.RenderBox && this.AddScriptsToHead)
                this.Controls.Add(PostbackBox.AlertControl);
            
            if (this.AddScriptsToHead)
                this.Controls.Add(PopupBox);
        }

        private bool _AddPingSessionControl = false;
        public virtual bool AddPingSessionControl
        {
            get { return _AddPingSessionControl; }
        }

        private WebControl PingSessionCtrl
        {
            get
            {
                PingSession.PingSession pingCtrl = new PingSession.PingSession();
                return pingCtrl.getWebControl();
            }
        }

        public WebControl Header
        {
            get
            {
                if (_Header == null)
                {
                    WebControl myHeader = new WebControl(HtmlTextWriterTag.Div);

                    myHeader.ID = "int";

                    HtmlGenericControl title = new HtmlGenericControl("div");
                    title.ID = "title";
                    myHeader.Controls.Add(title);

                    HtmlGenericControl h1 = new HtmlGenericControl("h1");
                    h1.InnerHtml = "<span class=\"d-none\">Sitemanager 3.0</span>";
                    title.Controls.Add(h1);

                    // show cms status
                    myHeader.Controls.Add(DisplayCmsStatus());

                    if (Users.AccountManager.CurrentAccount!=null)
                        this.Controls.Add(new SmClock());

                    if (AddPingSessionControl)
                        this.Controls.Add(PingSessionCtrl);

                    _Header = myHeader;
                }
                return _Header;
            }
        }
        private WebControl _Header;

        private LiteralControl DisplayCmsStatus()
        {            
            bool isOffline = Utility.Maintenance.Check();
            if (isOffline)
                return new LiteralControl(@"<div class=""cmsstatus""><h3><span class=""badge badge-warning"">Sistema in manutenzione</span></h3></div>");
            return new LiteralControl("");
        }


        public WebControl Footer
        {
            get
            {
                if (_Footer == null)
                    _Footer = new SmPageFooter();
                
                return _Footer;
            }
        }
        private WebControl _Footer;

        private SmPageTopbar _InfoToolbar;
        public SmPageTopbar InfoToolbar
        {
            get
            {
                if (_InfoToolbar == null)
                    _InfoToolbar = new SmPageTopbar(this);
                return _InfoToolbar;
            }
        }

        protected virtual void AddCssToHeader()
        {
            string customCss = Configurations.XmlConfig.GetAttribute("/Portal/configs/admin-gui","css");

            var addCss = this.Page.Header.FindControl("cssCtrl");
            if (addCss == null)
            {
                string cssLink = ""
                // bootstrap
               + @" <link rel=""stylesheet"" type=""text/css"" href=""" + NetCms.Configurations.Scripts.Bootstrap.Css.FirstOrDefault() + @""" /><!--bootstrap.css-->" + System.Environment.NewLine
               + @"<link media=""all"" rel=""stylesheet"" href=""/scripts/shared/font-awesome-5/css/all.min.css"" />" + System.Environment.NewLine
               + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"style.css"" /><!--style.css-->" + System.Environment.NewLine
               + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"dtree.css"" /><!--dtree.css-->" + System.Environment.NewLine
               + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"side.css"" /><!--side.css-->" + System.Environment.NewLine
               + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"smtable.css"" /><!--side.css-->" + System.Environment.NewLine
               + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"axf.css"" /><!--side.css-->" + System.Environment.NewLine
               + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"axt.css"" /><!--side.css-->" + System.Environment.NewLine
               + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteSiteManagerRoot + @"/test.css"" /><!--test.css-->" + System.Environment.NewLine
               + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteSiteManagerRoot + @"/css/" + customCss + @"/custom.css"" />" + System.Environment.NewLine
               + @" <!--[if lt IE 9]>" + System.Environment.NewLine
               + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteSiteManagerRoot + @"/css/" + customCss + @"/ie.css"" />" + System.Environment.NewLine
               + @" <![endif]-->" + System.Environment.NewLine;

                LiteralControl css = new LiteralControl(cssLink);
                css.ID = "cssCtrl";

                this.Page.Header.Controls.Add(css);
            }
            //+ @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteRoot + @"/css/layout.csx"" /><!--layout.csx-->"
            //+ @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteRoot + @"/css/tree.csx"" /><!--tree.csx-->"
            //+ @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteRoot + @"/css/xTreeView.csx"" /><!--xTreeView.csx-->"
            //+ @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteRoot + @"/css/screen.csx"" /><!--screen.csx-->"
            //+ @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteRoot + @"/css/thickbox.csx"" /><!--Thickbox.csx-->"
            //<link rel=""stylesheet"" type=""text/css"" href=""http://static.flowplayer.org/tools/css/overlay-minimal.css"" /><!--side.csx-->";
        }

        protected virtual void AddCssSimpleToHeader()
        {
            string customCss = Configurations.XmlConfig.GetAttribute("/Portal/configs/admin-gui", "css");

            string cssLink = ""
            // bootstrap            
           + @" <link rel=""stylesheet"" type=""text/css"" href=""" + NetCms.Configurations.Scripts.Bootstrap.Css.FirstOrDefault() + @""" /><!--bootstrap.css-->" + System.Environment.NewLine
           + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"style.css"" /><!--style.css-->" + System.Environment.NewLine
           + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"simple.css"" /><!--simple.css-->" + System.Environment.NewLine          
           + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"smtable.css"" /><!--side.css-->" + System.Environment.NewLine
           + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"axf.css"" /><!--side.css-->" + System.Environment.NewLine
           + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"axt.css"" /><!--side.css-->" + System.Environment.NewLine           
           + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteSiteManagerRoot + @"/css/" + customCss + @"/custom.css"" />" + System.Environment.NewLine
           + @" <!--[if lt IE 9]>" + System.Environment.NewLine
           + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteSiteManagerRoot + @"/css/" + customCss + @"/ie.css"" />" + System.Environment.NewLine
           + @" <![endif]-->" + System.Environment.NewLine;

            this.Page.Header.Controls.Add(new LiteralControl(cssLink));

            //+ @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteRoot + @"/css/layout.csx"" /><!--layout.csx-->"
            //+ @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteRoot + @"/css/tree.csx"" /><!--tree.csx-->"
            //+ @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteRoot + @"/css/xTreeView.csx"" /><!--xTreeView.csx-->"
            //+ @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteRoot + @"/css/screen.csx"" /><!--screen.csx-->"
            //+ @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteRoot + @"/css/thickbox.csx"" /><!--Thickbox.csx-->"
            //<link rel=""stylesheet"" type=""text/css"" href=""http://static.flowplayer.org/tools/css/overlay-minimal.css"" /><!--side.csx-->";
        }

        //        protected virtual void AddScriptsToHeader()
        //        {
        //            string scriptLink = @"   
        //            <script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.xTreeView) + @""" ></script><!--xTreeView.js-->
        //            <script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Homepage) + @""" ></script><!--Homepage.js-->
        //            <script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.DTree) + @""" ></script><!--DTree.js-->
        //            <script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Clock) + @""" ></script><!--Clock.js-->
        //            <script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.JScripts) + @""" ></script><!--JScripts.js-->
        //            <script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery) + @"""></script><!--Jquery.js-->
        //            <script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Cookie) + @"""></script><!--Jquery_Cookie.js-->
        //            <script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Tools) + @"""></script><!--Jquery_Tools.js-->              
        //            <script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Boot) + @"""></script><!--Jquery_Tools.js-->
        //            <script type=""text/javascript"" src=""" + Configurations.Paths.AbsoluteSiteManagerRoot + @"/test.js""></script><!--DebugJS.js-->
        //
        //            <script type=""text/javascript""><!--//--><![CDATA[//><!--
        //            <!--
        //            //ThickBox loading animation
        //            var tb_pathToImage = ""/scripts/thickbox/loadingAnimation.gif"";
        //
        //            function setSubmitSource(value)
        //            {
        //                if(document.getElementById('SubmitSource')!=null)
        //                {
        //                   document.getElementById('SubmitSource').value = value;
        //                } 
        //            }
        //            //--><!]]></script>
        //            <script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Thickbox) + @""" ></script><!--Thickbox.js-->
        //            ";
        //            this.Page.Header.Controls.Add(new LiteralControl(scriptLink));
        //        }
        
        private bool _JqueryUiEnable = true;

        public bool JqueryUiEnable
        {
            get { return _JqueryUiEnable; }
            set { _JqueryUiEnable = value; }
        }


        protected virtual void AddScriptsToHeader()
        {
            var add = this.Page.Header.FindControl("jsCrtl");
            if (add == null) {

                string scriptLink = ""
                + @"<script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.xTreeView) + @""" ></script><!--xTreeView.js-->" + System.Environment.NewLine                
                + @"<script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.DTree) + @""" ></script><!--DTree.js--> " + System.Environment.NewLine
                + @"<script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Clock) + @""" ></script><!--Clock.js--> " + System.Environment.NewLine
                + NetCms.Configurations.Scripts.JQuery.HtmlHeadCode + System.Environment.NewLine
                + NetCms.Configurations.Scripts.JQueryMigrate.HtmlHeadCode + System.Environment.NewLine
                + NetCms.Configurations.Scripts.Popper.HtmlHeadCode + System.Environment.NewLine                
                + @"<script type=""text/javascript"" src="""+NetCms.Configurations.Scripts.Bootstrap.Path+@"""></script><!--boostrap.js-->" + System.Environment.NewLine
                + ((JqueryUiEnable) ? NetCms.Configurations.Scripts.JQueryUI.HtmlHeadCode + System.Environment.NewLine :"")
                + ((JqueryUiEnable) ? NetCms.Configurations.Scripts.JQueryUITimepicker.HtmlHeadCode + System.Environment.NewLine : "")
                + NetCms.Configurations.Scripts.JQueryTools.HtmlHeadCode + System.Environment.NewLine
                + NetCms.Configurations.Scripts.JQueryBlockUI.HtmlHeadCode + System.Environment.NewLine
                + NetCms.Configurations.Scripts.JQueryMask.HtmlHeadCode + System.Environment.NewLine
                + @"<script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Cookie) + @"""></script><!--Jquery_Cookie.js-->" + System.Environment.NewLine
                + @"<script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Boot) + @"""></script><!--Jquery_Tools.js--> " + System.Environment.NewLine
                + @"<script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.JScripts) + @""" ></script><!--JScripts.js--> " + System.Environment.NewLine
                
                + @"<script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Homepage) + @""" ></script><!--Homepage.js-->" + System.Environment.NewLine

                //+ @"<script type=""text/javascript"" src=""" + Configurations.Paths.AbsoluteSiteManagerRoot + @"/test.js""></script><!--DebugJS.js--> " + System.Environment.NewLine

                + @"<script type=""text/javascript"">
            function setSubmitSource(value)
            {
                if(document.getElementById('SubmitSource')!=null)
                {
                   document.getElementById('SubmitSource').value = value;
                } 
            }
            </script>            
            " + NetCms.Configurations.Scripts.Thickbox.HtmlHeadCode + System.Environment.NewLine;

                LiteralControl js = new LiteralControl(scriptLink);
                js.ID = "jsCrtl";

                this.Page.Header.Controls.Add(js);

                this.OuterScripts.ForEach(x => { this.Page.Header.Controls.Add(new LiteralControl(x)); });
            }
                

            
        }


        protected virtual void AddMetaToHeader()
        {
            string scriptLink = @"";
            this.Page.Header.Controls.Add(new LiteralControl(scriptLink));
        }

        public PageLayouts PageLayout { get; protected set; } 
        public enum PageLayouts { Default, Lite }

        public virtual List<string> OuterScripts
        {
            get
            {
                return _OuterScripts ?? (_OuterScripts = new List<string>());
            }
        }
        private List<string> _OuterScripts;

    }
}