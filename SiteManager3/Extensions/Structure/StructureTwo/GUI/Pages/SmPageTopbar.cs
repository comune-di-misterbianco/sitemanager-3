using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for Toolbar
/// </summary>

namespace NetCms.GUI
{
    public class SmPageTopbar:WebControl
    {
        private string _BackAddress;
        public string BackAddress
        {
            get { return _BackAddress==null?"":_BackAddress; }
            set { _BackAddress = value; }
        }

        public Structure.WebFS.StructureFolder CurrentFolder
        {
            get
            {
                if (NetCms.Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork)
                    _CurrentFolder = (Structure.WebFS.StructureFolder)NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder;
                return _CurrentFolder;
            }
        }
        private Structure.WebFS.StructureFolder _CurrentFolder;

        private bool _ShowFolderPath = true;
        public bool ShowFolderPath
        {
            get { return _ShowFolderPath; }
            set { _ShowFolderPath = value; }
        }
	       
        public string IconUrl
        {
            get { return this.Page.IconsIndexer.GetImageURL(Page.PageIcon); }
        }
        public string Title
        {
            get { return Page.PageTitle; }
        }

        public SmPage Page
        {
            get { return _Page; }
            private set { _Page = value; }
        }
        private SmPage _Page;
        		

        public SmPageTopbar(SmPage page):base(HtmlTextWriterTag.Div)
        {
            this.Page = page;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            this.ID = "AppToolbar";

            HtmlGenericControl caption = GetCaption();
            this.Controls.Add(caption);

            HtmlGenericControl hr = new HtmlGenericControl("div");
            hr.Attributes["class"] = "hr";
            this.Controls.Add(hr);

            this.Controls.Add(new BackToolbarButton(false));
            this.Controls.Add(this.Page.PopupBox.PopupButton);

            hr = new HtmlGenericControl("div");
            hr.Attributes["class"] = "hr clearleft";
            this.Controls.Add(hr);
        }

        private HtmlGenericControl GetCaption()
        {
            HtmlGenericControl value = new HtmlGenericControl("div");

            HtmlGenericControl currentnetwork = new HtmlGenericControl("span");
            StructureFolder AuxCurrentFolder = this.CurrentFolder;
            if (AuxCurrentFolder != null)
            {
                currentnetwork.Attributes["class"] = "Where_NetworkInfo";
                currentnetwork.InnerHtml = "<span>Portale Corrente: <strong>" + AuxCurrentFolder.StructureNetwork.Label + "</strong></span>";
            }

            HtmlGenericControl frontend = new HtmlGenericControl("a");
            if (AuxCurrentFolder != null)
            {
                frontend.Attributes["class"] = "Where_FrontLink";
                frontend.Attributes["href"] = AuxCurrentFolder.StructureNetwork.Paths.AbsoluteFrontRoot + "/";
                if (AuxCurrentFolder != null)
                    frontend.Attributes["href"] = AuxCurrentFolder.FrontendHyperlink;

                frontend.Attributes["title"] = "Vai al frontend di " + AuxCurrentFolder.StructureNetwork.Label;
                frontend.Attributes["target"] = "_blank";
                frontend.InnerHtml = "<span>Vai al frontend di " + AuxCurrentFolder.StructureNetwork.Label + "</span>";
            }
            HtmlGenericControl jslabels = new HtmlGenericControl("a");
            jslabels.ID = "Toolbar_JSLabels";
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "Where_Back";
            div.Controls.Add(frontend);
            div.Controls.Add(currentnetwork);
            div.Controls.Add(jslabels);

            HtmlGenericControl right = new HtmlGenericControl("div");
            right.Attributes["class"] = "Where_Right";
            div.Controls.Add(right);

            HtmlGenericControl left = new HtmlGenericControl("div");
            if (Title.Length > 0 && ((AuxCurrentFolder != null && Title != AuxCurrentFolder.LabeledPath) || AuxCurrentFolder == null))
            {
                left.Attributes["class"] = "Where_Left Where_Icon_Custom";
                left.Attributes["style"] = "background-image: url(\"" + IconUrl + "\")";
                right.Controls.Add(left);
            }

            HtmlGenericControl caption = new HtmlGenericControl("div");
            caption.ID = "toolbarCaption";
            caption.InnerHtml = "" + Title + "";
            left.Controls.Add(caption);

            if (AuxCurrentFolder != null && ShowFolderPath)
            {
                HtmlGenericControl path = new HtmlGenericControl("div");
                path.Attributes["class"] = "Where_Path breadCrumb module";
                path.InnerHtml = AuxCurrentFolder.LabelLinkPathBreadCrumbs;
                right.Controls.AddAt(0,path);
            }
                

            value.Controls.Add(div);

            return value;
        }
    }
}