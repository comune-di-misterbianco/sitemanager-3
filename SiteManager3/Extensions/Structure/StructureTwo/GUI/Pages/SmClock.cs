using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI
{
    public class SmClock : System.Web.UI.WebControls.WebControl
    {
        public SmClock() : base(HtmlTextWriterTag.Div) { }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.ID = "UserLoginInfo";
            this.CssClass = "btn btn-light";

            NetCms.Users.User datiuser = NetCms.Users.AccountManager.CurrentAccount;

            string nomeCompleto = (!string.IsNullOrEmpty(datiuser.NomeCompleto)) ? datiuser.NomeCompleto : datiuser.UserName;

            //div.Attributes["class"] += " Logged";

            WebControl list = new WebControl(HtmlTextWriterTag.Ul);

            WebControl liUserName = new WebControl(HtmlTextWriterTag.Li);
            liUserName.ID = "username_account";
            liUserName.Controls.Add(new LiteralControl("<span>" + nomeCompleto + "</span>"));
            list.Controls.Add(liUserName);

            WebControl liUserNav = new WebControl(HtmlTextWriterTag.Li);
            liUserNav.ID = "userCtrl";
            liUserNav.Controls.Add(new LiteralControl("<span>Impostazioni Account</span>"));

            WebControl userNav = new WebControl(HtmlTextWriterTag.Ul);
            userNav.ID = "userNav";

            if (NetCms.Configurations.Debug.GenericEnabled)
                userNav.Controls.Add(new LiteralControl(this.DebugPage));

            if (datiuser.IsAdministrator)
                userNav.Controls.Add(new LiteralControl(this.CmsConfig));

            userNav.Controls.Add(new LiteralControl(this.Profilo));
            userNav.Controls.Add(new LiteralControl(this.EditProfilo));
            userNav.Controls.Add(new LiteralControl(this.ModificaPassword));
            userNav.Controls.Add(new LiteralControl(this.Logout));

            liUserNav.Controls.Add(userNav);

            list.Controls.Add(liUserNav);

            //this.Controls.Add(list);


            this.Controls.Add(GetCmsUserControl());
        }


        public WebControl GetCmsUserControl()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "dropdown cmsuser"; //nav-item

            WebControl avatar = new WebControl(HtmlTextWriterTag.Span);
            avatar.CssClass = "user-avatar";
            avatar.Controls.Add(new LiteralControl(@"<i class=""fa fa-user-circle fa-2"" aria-hidden=""true""></i>"));

            div.Controls.Add(avatar);

            WebControl aInfo = new WebControl(HtmlTextWriterTag.A);
            aInfo.CssClass = "dropdown-toggle"; //nav-link
            aInfo.Attributes["data-toggle"] = "dropdown";
            aInfo.Attributes["href"] = "#";
            aInfo.Attributes["role"] = "button";
            aInfo.Attributes["aria-haspopup"] = "true";
            aInfo.Attributes["aria-expanded"] = "true";

            //aInfo.Controls.Add(avatar);

            NetCms.Users.User datiuser = NetCms.Users.AccountManager.CurrentAccount;
            string nomeCompleto = (!string.IsNullOrEmpty(datiuser.NomeCompleto)) ? datiuser.NomeCompleto : datiuser.UserName;

            WebControl userInfo = new WebControl(HtmlTextWriterTag.Span);
            userInfo.CssClass = "user-info ml-2";            
            userInfo.Controls.Add(new LiteralControl(@"<span class=""username"">" + nomeCompleto + "</span>"));
            aInfo.Controls.Add(userInfo);

            div.Controls.Add(aInfo);


            WebControl userNav = new WebControl(HtmlTextWriterTag.Div);
            userNav.CssClass = "dropdown-menu dropdown-menu-right";       

            if (NetCms.Configurations.Debug.GenericEnabled)
                userNav.Controls.Add(new LiteralControl(this.DebugPage));

            if (datiuser.IsAdministrator)
                userNav.Controls.Add(new LiteralControl(this.CmsConfig));

            userNav.Controls.Add(new LiteralControl(this.Profilo));
            userNav.Controls.Add(new LiteralControl(this.EditProfilo));
            userNav.Controls.Add(new LiteralControl(this.ModificaPassword));

            userNav.Controls.Add(new LiteralControl(@"<div class=""dropdown-divider""></div>"));

            userNav.Controls.Add(new LiteralControl( this.Logout));

            div.Controls.Add(userNav);

            return div;
        }



        public string DebugPage
        {
            get {
                string href = Configurations.Paths.AbsoluteSiteManagerRoot + "/debug.aspx?keepThis=true&TB_iframe=true&height=600&width=800";
                return BuildElementHtml("Debug page", href, "DebugPage thickbox", "", @"<i class=""fa fa-check mr-2"" aria-hidden=""true""></i>");
            }
        }

        public string CmsConfig
        {
            get {
                string href = Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/configs/default.aspx";
                return BuildElementHtml("Configurazione Cms", href, "CmsConfig", "", @"<i class=""fa fa-cog mr-2"" aria-hidden=""true""></i>");
            }
        }

        public string Profilo
        {
            get
            {
                string href = NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/users/account_details.aspx";
                return BuildElementHtml("Dati profilo", href, "ProfiloLink", "", @"<i class=""fa fa-user mr-2"" aria-hidden=""true""></i>");
            }
        }

        public string EditProfilo
        {
            get
            {
                string href = NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/users/account_edit.aspx";
                return BuildElementHtml("Modifica profilo", href, "ModificaProfiloLink", "", @"<i class=""fa fa-pencil mr-2"" aria-hidden=""true""></i>");
            }
        }

        public string ModificaPassword
        {
            get
            {
                string href = NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/users/account_pwd_change.aspx";
                return BuildElementHtml("Modifica password", href, "ModificaPasswordLink", "", @"<i class=""fa fa-key mr-2"" aria-hidden=""true""></i>");
            }
        }
        public string Logout
        {
            get
            {
                string href;
                if (NetCms.Configurations.Debug.GenericEnabled)
                    href = "?do=logout";
                else
                    href = "?do=logout&amp;goto=" + NetCms.Configurations.Paths.AbsoluteSiteManagerRoot;

                //string href = "?do=logout&amp;goto=" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;
                return BuildElementHtml("Logout", href, "LogoutLink","", @"<i class=""fa fa-sign-out mr-2"" aria-hidden=""true""></i>");
            }
        }

        //string template = @"<li{2}><a{3} class=""nav-link"" href=""{1}"">{4}{0}</a></li>";
        string template = "<a{3} {2} href=\"{1}\">{4}{0}</a>";

        public string BuildElementHtml(string label, string href, string cssClass, string thickbox, string awensomeicon)
        {
            if (!string.IsNullOrEmpty(cssClass))
                cssClass = " class=\"dropdown-item " + cssClass + "\"";
            return string.Format(template, label, href, cssClass, thickbox, awensomeicon);
        }

        private string[] Mesi = { "Gennaio", "Febbario", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre" };
        private string[] Settimana = { "Luned�", "Marted�", "Mercoled�", "Gioved�", "Venerd�", "Sabato", "Domenica" };

        private string GiornoSettimana(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    return Settimana[0];
                case DayOfWeek.Tuesday:
                    return Settimana[1];
                case DayOfWeek.Wednesday:
                    return Settimana[2];
                case DayOfWeek.Thursday:
                    return Settimana[3];
                case DayOfWeek.Friday:
                    return Settimana[4];
                case DayOfWeek.Sunday:
                    return Settimana[6];
                case DayOfWeek.Saturday:
                    return Settimana[5];
            }
            return null;
        }
			
    }
}