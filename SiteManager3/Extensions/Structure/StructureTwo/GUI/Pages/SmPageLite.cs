using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI
{
    public abstract class SmPageLite : SmPage
    {
        public SmPageLite()
        {
        }
        
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.Controls.Add(this.ContentBody);
        }
        protected override void AddCssToHeader()
        {
            string cssLink = @"<link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"lite.css"" /><!--lite.csx-->";
            this.Page.Header.Controls.Add(new LiteralControl(cssLink));
        }
    }
}