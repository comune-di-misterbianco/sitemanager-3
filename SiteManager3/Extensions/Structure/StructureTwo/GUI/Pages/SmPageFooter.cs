using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI
{
    public class SmPageFooter : System.Web.UI.WebControls.WebControl
    {
        protected NetCms.Users.User Account
        {
            get
            {
                if (_Account == null)
                    _Account = NetCms.Users.AccountManager.CurrentAccount;
                return _Account;
            }
        }
        private NetCms.Users.User _Account;

        public SmPageFooter() : base(HtmlTextWriterTag.Div) { }
	

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            WebControl Footer = new WebControl(HtmlTextWriterTag.Div);
            this.ID = "footer";
            this.CssClass = "footer";


            HtmlInputHidden hidden = new HtmlInputHidden();
            hidden.ID = "SubmitSource";
            this.Controls.Add(hidden);

            if (Account != null)
            {
                if (Account != null && Account.GUI.PreviewType == NetCms.Users.SitemanagerUserGUI.PreviewTypes.Floating)
                {
                    HtmlGenericControls.Div popup = new HtmlGenericControls.Div();
                    popup.ID = "PopupControl";
                    Footer.Controls.Add(popup);
                }
            }
        }
    }
}