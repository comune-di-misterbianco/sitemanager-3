using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Diagnostics;
using System.Net.Mail;
using System.Net;
using System.Management;
using System.Collections;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI
{
    public class SmPageError : SmPage
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                return null;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Si � verificato un errore."; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Error; }
        }

        public override string LocalCssClass
        {
            get
            {
                return "Error";
            }
        }

        public Exception ExceptionData
        {
            get
            {
                return _ExceptionData;
            }
        }
        private Exception _ExceptionData;
			
        public SmPageError()
        {
        }

        public SmPageError(Exception ex)
        {
            _ExceptionData = ex;
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

  //          this.Controls.Add(this.Header);
            this.Controls.Add(this.InfoToolbar);
            this.Controls.Add(this.ContentBody);
            this.Controls.Add(this.Footer);
        }

        protected override WebControl RootControl
        {
            get 
            {                
											

                if (NetCms.Configurations.Paths.PageName == "error-no-grants.aspx")
                   return ErrorsControls.NoGrantsControl(ExceptionData);                
                if (NetCms.Diagnostics.Diagnostics.ExceptionsForThisPage.Count > 0)
                   return ErrorsControls.SystemErrorControl(ExceptionData);
                if (ExceptionData != null && ExceptionData.Message.Contains("404"))
                    return ErrorsControls.Error404Control(ExceptionData);
                if (ExceptionData != null)
                   return ErrorsControls.SystemErrorControl(ExceptionData);
                
                 return ErrorsControls.GenericErrorControl(ExceptionData);               
																 
			  
														  

								
            }
        }
    }
}