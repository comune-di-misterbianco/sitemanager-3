using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI
{
    public abstract class SmPageVertical : SmPage
    {

        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.HomeSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;
        public SmPageVertical()
        {
            this.AddScriptManager = true;

            NetCms.Users.AccountManager.CurrentAccount.History.UpdateHistoryData();
        }
        
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.Controls.Add(this.Header);
            if(SideBar!=null)
                this.Controls.Add(this.SideBar);
            this.Controls.Add(this.InfoToolbar);
            this.Controls.Add(this.ContentBody);
            this.Controls.Add(this.Footer);
        }
    }
}