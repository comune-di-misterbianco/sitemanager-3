using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI
{
    public abstract class SmPageDebug : SmPage
    {
        public SmPageDebug()
        {
        }
        
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Controls.Add(this.ContentBody);
        }
        protected override void AddCssToHeader()
        {
            //<link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteRoot + @"/cms/css/admin/jqueryui.css"" /><!--jqueryui.csx-->
            string cssLink = @"   
            <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"debug.css"" /><!--debug.csx-->
            <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"axt.css"" /><!--axt.csx-->            
            <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteSiteManagerRoot + @"/test.css"" /><!--test.csx-->";
            this.Page.Header.Controls.Add(new LiteralControl(cssLink));
        }
        protected override void AddScriptsToHeader()
        {
            base.AddScriptsToHeader();

            string value = @"
	<script src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery) + @""" type=""text/javascript""></script><!--Jquery.js-->
	<script src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Cookie) + @""" type=""text/javascript""></script><!--Jquery_Cookie.js-->
	<script src=""" + this.Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_UI) + @""" type=""text/javascript""></script><!--Jquery_UI.js-->
	<script type=""text/javascript"">
	$(document).ready(function()
    {
		$(function() {
		    $(""#tabs"").tabs();
	    })

    $(document).ready(function(){
	    $(function() {
	        $(""#TypesListAccordion"").accordion();
        })
    });
</script>
	});
	</script>
        ";
            this.Page.Header.Controls.Add(new LiteralControl(value));
        }
    }
}