using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Linq;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI
{
    public abstract class NetworkPage : SmPage
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;
        protected PageData PageData
		{
			get 
			{ 
				if(_PageData == null)
                    _PageData = PageData.CurrentReference;
				return _PageData; 
			} 
		}
		private PageData _PageData;

        /// <summary>
        /// Propriet� da sovrascrivibile, che definisce i controlli da mostrare all'utente,
        /// nel caso in cui l'utente ha tentato l'accesso a una pagina di Network, 
        /// ma non esiste la Network associata all'url.
        /// </summary>        
        protected virtual WebControl OutsideNetworkErrorControl
        {
            get
            {
                WebControl output = new WebControl(HtmlTextWriterTag.Fieldset);

                HtmlGenericControl legend = new HtmlGenericControl("legend");
                legend.InnerHtml = ("Non � possibile visualizzare questa pagina");
                
                HtmlGenericControls.Div div = new HtmlGenericControls.Div();
                div.Class = "red";
                div.InnerHtml = "Si � verificato un errore <br />";
                div.InnerHtml += "Possibili cause: <ol>";
                div.InnerHtml += "<li>Non si dispone dei diritti per vedere questa pagina</li>";
                div.InnerHtml += "<li>Il la pagina non esiste</li>";
                div.InnerHtml += "<li>Il la pagina � stato eliminata</li>";
                div.InnerHtml += "</ol>";

                output.Controls.Add(legend);
                output.Controls.Add(div);
                return output;
            }
        }
        
        protected virtual bool ValidCurrentDocument
        {
            get
            {
                return this.ValidCurrentFolder && this.CurrentDocument != null && this.CurrentDocument.Grant;
            }
        }
        protected virtual bool ValidCurrentFolder
        {
            get
            {
                return this.ValidNetwork && this.CurrentFolder != null && this.CurrentFolder.Grant;
            }
        }
        protected bool ValidNetwork
        {
            get
            {
                return Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork;
            }
        }

        public virtual Structure.WebFS.Document CurrentDocument
        {
            get
            {
                if (_CurrentDocument == null)
                {
                    G2Core.Common.QueryStringRequestVariable request = new G2Core.Common.QueryStringRequestVariable("doc");
                    if (this.CurrentFolder.Documents.Where(x => x.ID == request.IntValue).Count() > 0)
                        _CurrentDocument = this.CurrentFolder.Documents.Where(x => x.ID == request.IntValue).First() as Structure.WebFS.Document;
                }
                return _CurrentDocument;
            }
        }
        private Structure.WebFS.Document _CurrentDocument;

        public Structure.WebFS.StructureFolder CurrentFolder
        {
            get
            {
                if (_CurrentFolder == null)
                {
                    G2Core.Common.QueryStringRequestVariable request = new G2Core.Common.QueryStringRequestVariable("folder");
                    if (request.StringValue.Length == 0)
                        _CurrentFolder = (Structure.WebFS.StructureFolder)Networks.NetworksManager.CurrentActiveNetwork.RootFolder.FindFolderByPath(request.StringValue);
                    else
                        _CurrentFolder = (Structure.WebFS.StructureFolder)Networks.NetworksManager.CurrentActiveNetwork.RootFolder.FindFolderByID(request.IntValue);
                }
                return _CurrentFolder;
            }
        }
        private Structure.WebFS.StructureFolder _CurrentFolder;

        public Structure.WebFS.StructureNetwork CurrentNetwork
        {
            get
            {
                if (_CurrentNetwork == null)
                {
                    _CurrentNetwork = (Structure.WebFS.StructureNetwork)Networks.NetworksManager.CurrentActiveNetwork;
                }
                return _CurrentNetwork;
            }
        }
        private Structure.WebFS.StructureNetwork _CurrentNetwork;

        protected override WebControl RootControl
        {
            get
            {
                if (ValidNetwork)
                    return BuildControls();
                else
                    return OutsideNetworkErrorControl;
            }
        }

        public NetworkPage()
        {
            NetCms.Users.AccountManager.CurrentAccount.History.UpdateHistoryData();
        }

        protected override void AddScriptsToHeader()
        {
            base.AddScriptsToHeader();        

            string jqueryLink = @"
<script src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Treeview) + @""" type=""text/javascript""></script><!--Jquery_Treeview.js-->
<script src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Treeview_Async) + @""" type=""text/javascript""></script><!--Jquery_Treeview_Async.js-->
<script src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Treeview_Edit) + @""" type=""text/javascript""></script><!--Jquery_Treeview_Edit.js-->
<script src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_CapSlide) + @""" type=""text/javascript""></script><!--Jquery_CapSlide.js-->
<script src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_mCustomScrollbar) + @""" type=""text/javascript""></script><!--Jquery_mCustomScrollbar.js-->
<script src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_MouseWheel_min) + @""" type=""text/javascript""></script><!--Jquery_MouseWheel_min.js-->
<script src=""" + Configurations.Scripts.Filepicker.Path + @""" type=""text/javascript""></script><!-- ImagePicker -->
        ";
            jqueryLink += @"
<script type=""text/javascript"">
$(document).ready(function(){
    $(""#FoldersTreeAjax"").treeview({
        url: """ + this.CurrentFolder.StructureNetwork.Paths.AbsoluteAdminRoot + @"/cms/ajaxfolder.aspx?folder=" + this.CurrentFolder.ID + @"""
    })
});

$(function () {
		  //  $('.date').datepicker({ dateFormat: ""dd/mm/yy"" });

            //HorizontalScroll 
		    $(""#content_1"").mCustomScrollbar({
			    horizontalScroll:true,
			    advanced:{
				    autoExpandHorizontalScroll:true
			    }
		    });							
		    //capslide on hover
		    $("".capslide_img_cont"").capslide({
			    caption_color	: 'white',
			    caption_bgcolor	: 'black',
			    overlay_bgcolor : 'white',
			    border			: '',
			    showcaption	    : false
		    });	
        });
</script>
                ";

           this.Page.Header.Controls.Add(new LiteralControl(jqueryLink));

           this.Page.Header.Controls.Add(new LiteralControl(Configurations.Scripts.CKEditor.HtmlHeadCode));
           //this.Page.Header.Controls.Add(new LiteralControl(Configurations.Scripts.Momentjs.HtmlHeadCode));
           //this.Page.Header.Controls.Add(new LiteralControl(Configurations.Scripts.DatepickerUxsolutions.HtmlHeadCode));
           //this.Page.Header.Controls.Add(new LiteralControl(Configurations.Scripts.DatePickerTempusdominus.HtmlHeadCode));
        }

        protected override void AddCssToHeader()
        {
            base.AddCssToHeader();

            
                if (Account.GUI.PreviewType == NetCms.Users.SitemanagerUserGUI.PreviewTypes.Floating)
                {
                    string cssLink = @"<link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"floating_popup.css"" /><!--Floating_Popup.csx-->";
                    this.Page.Header.Controls.Add(new LiteralControl(cssLink));
                }
                else if (Account.GUI.PreviewType == NetCms.Users.SitemanagerUserGUI.PreviewTypes.Static)
                {
                    string cssLink = @"<link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"static_popup.css"" /><!--Static_Popup.csx-->";
                    this.Page.Header.Controls.Add(new LiteralControl(cssLink));
                }

                string jqueryLink = @"<link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"jquery-treeview.css"" /><!--jquery-treeview.csx-->";
                this.Page.Header.Controls.Add(new LiteralControl(jqueryLink));

                string CustomScrollBarLink = @"<link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"jquery-mCustomScrollbar.css"" /><!--jquery-treeview.csx-->";
                this.Page.Header.Controls.Add(new LiteralControl(CustomScrollBarLink));
        }

        protected abstract WebControl BuildControls();

        protected void AddFolderInformationsToSide(){}
        protected void AddDocumentInformationsToSide(){}

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
                
            this.Controls.Add(this.Header);
            this.Controls.Add(this.SideBar);
            this.Controls.Add(this.InfoToolbar);
            this.Controls.Add(this.ContentBody);
            this.Controls.Add(this.Footer);
        }

        protected WebControl InvalidCurrentDocumentControl 
        {
            get { return null; }
        }
        protected WebControl InvalidCurrentFolderControl
        {
            get { return null; }
        }
        protected WebControl InvalidCurrentNetworkControl
        {
            get { return null; }
        }
        protected WebControl InvalidGrantsControl
        {
            get { return null; }
        }
    }
}