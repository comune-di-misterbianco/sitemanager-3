using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI
{
    public abstract class SmPageLogin : SmPage
    {
        public SmPageLogin()
        {
        }
        
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Controls.Add(new NetCms.GUI.CmsLoginController());
        }
        protected override void OnPreRender(EventArgs e)
        {
        }
        protected override void AddCssToHeader()
        {
            string customCss = Configurations.XmlConfig.GetAttribute("/Portal/configs/admin-gui", "css");

            string cssLink = @"<link rel=""stylesheet"" type=""text/css"" href=""" + NetCms.Configurations.Scripts.Bootstrap.Css.FirstOrDefault() + @""" /><!--bootstrap.css-->" + System.Environment.NewLine
                           + @"<link media=""all"" rel=""stylesheet"" href=""/scripts/shared/font-awesome-5/css/all.min.css"" />" + System.Environment.NewLine
                           + @"<link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteSiteManagerRoot + @"/css/" + customCss + @"/login.css"" />";

            string conditionalComments = @"<!--[if lt IE 9]>
                                            <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteSiteManagerRoot + @"/css/" + customCss + @"/ie.css"" />
                                          <![endif]-->";
            this.Page.Header.Controls.Add(new LiteralControl(cssLink));
            this.Page.Header.Controls.Add(new LiteralControl(conditionalComments));
        }
        protected override void AddScriptsToHeader()
        {
        }
    }
}