﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.GUI
{
    public class SiteManagerHtmlTable : Collapsable
    {
        public TableHeaderRow HeaderRow
        {
            get
            {
                if (_HeaderRow == null)
                {
                    _HeaderRow = new TableHeaderRow();
                    _HeaderRow.CssClass = "header-row";
                }
                return _HeaderRow;
            }
        }
        private TableHeaderRow _HeaderRow;

        public TableRowCollection Rows
        {
            get
            {
                return InnerTable.Rows;
            }
        }

        public Table InnerTable
        {
            get
            {
                if (_InnerTable == null)
                {
                    _InnerTable = new Table();

                    _InnerTable.CellPadding = 0;
                    _InnerTable.CellSpacing = 0;
                }
                return _InnerTable;
            }
        }
        private Table _InnerTable;

        public SiteManagerHtmlTable()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.AppendControl(InnerTable);
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.CssClass += " sm-table";

            base.OnPreRender(e);

            this.InnerTable.Rows.AddAt(0, HeaderRow);
         
            int i = 0;
            foreach (TableRow row in InnerTable.Rows)
                if(i++>0) row.CssClass += (i % 2== 0 ? "" : "alternate");
        }
    }
}
