﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.GUI;
using System.IO;
using System.Text.RegularExpressions;

namespace NetCms.GUI
{
    public static class CssForEditor 
    {
        public static string GetCssCode(string currentNetwork)
        {
            
                // recuperare il foglio di stile della pagina

                string arrText = "";
                //string network = System.Web.HttpContext.Current.Request["network"].ToString();
                //string network = NetCms.Networks.NetworksManager.CurrentActiveNetwork.SystemName;

                string AbsoluteRoot = NetCms.Configurations.Paths.AbsoluteRoot;

                string file_trattati = "";

                // recuperare il foglio di stile alignment.css
                string cssorigine = AbsoluteRoot + "/css/" + currentNetwork + "/alignment.css";
                string css = System.Web.HttpContext.Current.Server.MapPath(cssorigine);
                if (File.Exists(css))
                {
                    file_trattati += css;

                    System.IO.StreamReader objReader = new System.IO.StreamReader(css);

                    arrText = objReader.ReadToEnd();
                    objReader.Close();
                }

                // recuperare il foglio di stile pagina.css
                cssorigine = AbsoluteRoot + "/css/" + currentNetwork + "/pagina.css";
                css = System.Web.HttpContext.Current.Server.MapPath(cssorigine);
                if (File.Exists(css))
                {
                    file_trattati += "\n" + css;

                    System.IO.StreamReader objReader = new System.IO.StreamReader(css);
                    arrText += objReader.ReadToEnd();

                    objReader.Close();
                }

                // recuperare il foglio di stile table.css
                cssorigine = AbsoluteRoot + "/css/" + currentNetwork + "/table.css";
                css = System.Web.HttpContext.Current.Server.MapPath(cssorigine);
                if (File.Exists(css))
                {
                    file_trattati += "\n" + css;

                    System.IO.StreamReader objReader = new System.IO.StreamReader(css);
                    arrText += objReader.ReadToEnd();

                    objReader.Close();
                }

                // rimuovo i riferimenti a colcx
                //arrText = arrText.Replace("body {.*}", "");

                arrText = Regex.Replace(arrText, "body {.*}", "");

                arrText = arrText.Replace("#ColCX", "");
                arrText = arrText.Replace("img/", AbsoluteRoot + "/css/" + currentNetwork + "/img/");
                arrText = arrText.Replace("text-align:left;", "");
                arrText = arrText.Replace("text-align:center;", "");
                arrText = arrText.Replace("text-align:right;", "");
                arrText += " .flashblock{ border:1px solid #ccc; background-color:#fafafa; width:300px; height:250px; padding:4px; display:block; }\n";
                arrText += " #tinymce{ font-size:1.0em; }\n";
                arrText += "/* " + file_trattati + " */";

                // ritornare il css ripulito
                return arrText;            
        }
    }
}
