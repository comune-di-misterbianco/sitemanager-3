using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure;
using NetCms.Structure.WebFS;
using System.Web.UI;

/// <summary>
/// Summary description for StructureTable
/// </summary>
namespace NetCms
{
    namespace GUI
    {
        public class StructureTable : WebControl
        {
            private StructureFolder Folder;
            private NetUtility.RequestVariable SubmitSource = new NetUtility.RequestVariable("SubmitSource", NetUtility.RequestVariable.RequestType.Form);
            private HtmlGenericControl Table;

            public StructureTable(StructureFolder folder)
                : base(System.Web.UI.HtmlTextWriterTag.Table)
            {
                Table = new HtmlGenericControl("table");
                Table.Attributes["class"] = "foldertab FoldersGUI table";
                Table.Attributes["cellspacing"] = "0";
                Folder = folder;
            }

            private string _PaginationKey;
            public string PaginationKey
            {
                get
                {
                    if (_PaginationKey == null)
                        _PaginationKey = "fpage";
                    return _PaginationKey;
                }
                set
                {
                    if (_PaginationKey == null)
                        _PaginationKey = value;
                    else
                    {
                        //throw new Exception("La chiave per la paginazione pu� essere impostata una sola volta per istanza della classe StructureTable");
                        //LogAndTrance.ExceptionManager.ExceptionLogger.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, "La chiave per la paginazione pu� essere impostata una sola volta per istanza della classe StructureTable", this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Warning);
                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "La chiave per la paginazione pu� essere impostata una sola volta per istanza della classe StructureTable", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                    }
                }
            }

            private int _RowCount;
            public int RowCount
            {
                get;
                set;
            }

            private int _TopRange;
            private int TopRange
            {
                get
                {
                    if (_TopRange == 0)
                        _TopRange = RecordPerPagina * Page;
                    return _TopRange;
                }
            }

            private int _BottomRange;
            private int BottomRange
            {
                get
                {
                    if (_BottomRange == 0)
                        _BottomRange = RecordPerPagina * (Page - 1);
                    return _BottomRange;
                }
            }

            private int _RecordPerPagina = 0;
            public int RecordPerPagina
            {
                get
                {
                    if (_RecordPerPagina == 0)
                        _RecordPerPagina = 30;//Folder.ReferedAccount.GUI.PreviewType == NetCms.Users.SitemanagerUserGUI.PreviewTypes.Floating ? 25 : 15;
                    return _RecordPerPagina;
                }
            }

            private int _Page;
            public int Page
            {
                get
                {
                    if (_Page == 0)
                    {
                        NetUtility.RequestVariable PageRequest = new NetUtility.RequestVariable(this.PaginationKey, NetUtility.RequestVariable.RequestType.Form_QueryString);
                        _Page = PageRequest != null && PageRequest.IsValidInteger && PageRequest.IntValue > 0 ? PageRequest.IntValue : 1;
                    }

                    return _Page;
                }
            }


            private bool ValueInRange
            {
                get
                {
                    return TopRange > RowCount && RowCount >= BottomRange;
                }
            }

            private int rowIndex = 0;

            public void addRow(HtmlGenericControl tr)
            {
                //Meccanismo di funzionamento modificato per risolvere grossi problemi prestazionali
                //if (ValueInRange)
                this.Controls.Add(tr);
                if (rowIndex % 2 == 0)
                    tr.Attributes["class"] = (tr.Attributes["class"] + " alternate").Trim();
                rowIndex++;
                if (rowIndex == RecordPerPagina)
                    rowIndex = 0;
                //RowCount++;
            }
            public void addTableHead(HtmlGenericControl tr)
            {
                this.Controls.AddAt(0, tr);
            }
            public void addTableFooter(HtmlGenericControl tr)
            {
                this.Controls.Add(tr);
            }

            public void addOpenTableBody()
            {
                this.Controls.Add(new LiteralControl("<tbody>"));
            }

            public void addCloseTableBody()
            {
                this.Controls.Add(new LiteralControl("</tbody>"));
            }

            protected override void OnPreRender(EventArgs e)
            {
                base.OnPreRender(e);
            //    this.Attributes.Add("cellpadding", "0");
            //    this.Attributes.Add("cellspacing", "0");
                //div.Controls.Add(Folder.SearchForm);
                if (RowCount > RecordPerPagina)
                    this.addTableFooter(Pages);
                this.CssClass = "foldertab FoldersGUI";

                //this.Controls.Add(Table);

                //if (Folder.ReferedAccount.GUI.PreviewType == NetCms.Users.SitemanagerUserGUI.PreviewTypes.Static)
                //{
                //    HtmlGenericControls.Div popup = new HtmlGenericControls.Div();
                //    popup.ID = "PopupControl";
                //    popup.Attributes.CssStyle.Add("display", "none");
                //    this.Controls.Add(popup);
                //}

                #region SetMain Field

                WebControl trHide = new WebControl(System.Web.UI.HtmlTextWriterTag.Tr);
                trHide.CssClass = "d-none";

                WebControl tdHide = new WebControl(System.Web.UI.HtmlTextWriterTag.Td);
                tdHide.Attributes["colspan"] = "6";


                HtmlGenericControl hiddens = new HtmlGenericControl("span");
                hiddens.InnerHtml = "<input ";
                hiddens.InnerHtml += "name=\"" + PaginationKey + "\" ";
                hiddens.InnerHtml += "id=\"" + PaginationKey + "\" ";
                hiddens.InnerHtml += "type=\"hidden\" ";
                hiddens.InnerHtml += "value=\"" + Page + "\" />";

                hiddens.InnerHtml += "<input ";
                hiddens.InnerHtml += "name=\"deleteDoc\" ";
                hiddens.InnerHtml += "id=\"deleteDoc\" ";
                hiddens.InnerHtml += "type=\"hidden\" ";
                hiddens.InnerHtml += "value=\"\" />";



                hiddens.InnerHtml += "<script type=\"text/javascript\">";
                hiddens.InnerHtml += @"
function SetFolderPage(page)
{ 
    oInput = document.getElementById('" + PaginationKey + @"');
    if(oInput)
    {        
        oInput.value = page;
        setSubmitSource('" + (this.SubmitSource.IsValid() ? this.SubmitSource.StringValue : "") + @"');
        document.getElementById('PageForm').submit()
    }
}
";
                hiddens.InnerHtml += "</script>";

                tdHide.Controls.Add(hiddens);
                trHide.Controls.Add(tdHide);

                this.Controls.Add(trHide);
                //this.Controls.Add(hiddens);

                #endregion
            }

            public HtmlGenericControl Pages
            {
                get
                {
                    HtmlGenericControl tr = new HtmlGenericControl("tr");
                    HtmlGenericControl td = new HtmlGenericControl("td");

                    NetUtility.PageMaker pager = new NetUtility.PageMaker(this.Page, NetUtility.PageMaker.VariabileType.Form);
                    pager.RPP = this.RecordPerPagina;
                    pager.Key = PaginationKey;
                    //Codice per postback con querystring
                    //td = pager.PageNavigation(Folder.BackofficeHyperlink, this.RowCount);
                    td = pager.PageNavigation("javascript: SetFolderPage({0});", this.RowCount);

                    td.TagName = "td";
                    td.Attributes["colspan"] = "8";
                    td.Attributes["class"] += " FolderNavigationControls";

                    tr.Controls.Add(td);

                    return tr;

                }
            }
        }
    }
}