using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms;
/// <summary>
/// Summary description for ToolbarButton
/// </summary>

namespace NetCms.GUI
{
    public class BackToolbarButton:WebControl
    {
        public string Name;
        public string Label;

        public NetCms.GUI.Css.ImagesIndexer ImagesIndexer
        {
            get { return _ImagesIndexer; }
            private set { _ImagesIndexer = value; }
        }
        private NetCms.GUI.Css.ImagesIndexer _ImagesIndexer;

        private bool Disable;

        public const string DefaultBackAddress = PageData.BackAddress;

        public BackToolbarButton(bool backDisable) : base(HtmlTextWriterTag.Div)
        { 
            Name = "Indietro";
            Label = "Indietro";
            Disable = backDisable;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.ImagesIndexer = new NetCms.GUI.Css.ImagesIndexer(this.Page);
            HtmlGenericControl back = new HtmlGenericControl("div");

            back.Attributes["class"] = "toolbarButton Back";
            back.InnerHtml = "<a title=\"Indietro\"";
            back.InnerHtml += " onmouseover=\"setToolbarCaption('Indietro')\" ";
            back.InnerHtml += " onmouseout=\"setToolbarCaption('')\" ";
            
           
            if (!Disable)
            {
                back.InnerHtml += " href=\"";
                back.InnerHtml += DefaultBackAddress;
                back.InnerHtml += "\">";
                back.InnerHtml += "<span class=\"d-none\">Torna indietro</span>";
                //back.InnerHtml += "<img alt=\"\" src=\"" + ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Back);
            }
            else
            {
                back.InnerHtml += " href=\"#\">";
                //back.InnerHtml += "<img alt=\"\" src=\"" + ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Back_Disabled);
                back.InnerHtml += "<span class=\"d-none\">Torna indietro</span>";                
            }
                back.InnerHtml += "</a>";
                this.Controls.Add(back);

            HtmlGenericControl home = new HtmlGenericControl("div");

            home.Attributes["class"] = "toolbarButton Home";
            home.InnerHtml = "<a title=\"Homepage\"";
            home.InnerHtml += " onmouseover=\"setToolbarCaption('Torna alla Homepage')\" ";
            home.InnerHtml += " onmouseout=\"setToolbarCaption('')\" ";
            home.InnerHtml += " href=\"";
            home.InnerHtml += Configurations.Paths.AbsoluteSiteManagerRoot;
            home.InnerHtml += "/default.aspx";
            home.InnerHtml += "\">";
            //home.InnerHtml += "<img src=\"" + ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Home);
            back.InnerHtml += "<span class=\"d-none\">Torna alla schermata principale</span>";
            home.InnerHtml += "</a>";

            this.Controls.Add(home);

        }

    }
}