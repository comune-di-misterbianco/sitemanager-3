using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for ToolbarButtonCollection
/// </summary>
namespace NetCms.GUI
{
    public class SideToolbarButtonCollection
    {
        private NetCollection coll;
        public int Count { get { return coll.Count; } }
        public SideToolbarButtonCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new NetCollection();
        }

        public void Add(SideToolbarButton button)
        {
            coll.Add(button.Name, button);
        }

        public SideToolbarButton this[int i]
        {
            get
            {
                SideToolbarButton button = (SideToolbarButton)coll[coll.Keys[i]];
                return button;
            }
        }

        public SideToolbarButton this[string str]
        {
            get
            {
                SideToolbarButton button = (SideToolbarButton)coll[str];
                return button;
            }
        }

        public void Clear()
        {
            coll.Clear();
        }

    }
}