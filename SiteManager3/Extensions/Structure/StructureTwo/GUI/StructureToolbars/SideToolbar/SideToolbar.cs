using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Toolbar
/// </summary>
namespace NetCms.GUI
{
    public class SideToolbar
    {

        private SideToolbarButtonCollection Buttons;
        public SideToolbarButtonCollection ButtonCollection
        {
            get
            {
                return Buttons;
            }
        }

        public SideToolbar()
        {
            //
            // TODO: Add constructor logic here
            //
            Buttons = new SideToolbarButtonCollection();
        }


        public HtmlGenericControl getControl()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.ID = "SideCommandToolbar";
            if (Buttons.Count != 0)
            {
                HtmlGenericControl ul = new HtmlGenericControl("ul");
                div.Controls.Add(ul);

                bool alternate = true;
                for (int i = 0; i < Buttons.Count; i++)
                {
                    ul.Controls.Add(Buttons[i].getControl(alternate));
                    alternate = !alternate;
                }
            }
            else
            {                
                div.InnerHtml = "<p>Nessuna operazione</p>";
            }
            
            return div;
        }
        public void addButton(SideToolbarButton btn)
        {
            Buttons.Add(btn);
        }

        
    }
}