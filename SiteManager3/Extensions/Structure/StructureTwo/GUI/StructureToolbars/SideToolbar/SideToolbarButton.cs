using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms;
/// <summary>
/// Summary description for ToolbarButton
/// </summary>

namespace NetCms.GUI
{
    public class SideToolbarButton
    {

        private string _CssClass;
        public string CssClass
        {
            get { return _CssClass; }
            set { _CssClass = value; }
        }
        public string Href;
        public string Name;
        public string Label;
        public string Icon;
        public bool AddRootDist;

        protected PageData Config;

        public SideToolbarButton(string name, string href, string icon, string label, PageData config, bool addRootDist)
        {
            Name = name;
            Href = href;
            Icon = icon;
            Label = label;
            Config = config;
            AddRootDist = addRootDist;
        }
        public SideToolbarButton(string name, string href, string icon, string label, PageData config)
            :this(name,href,icon,label,config,false)
        {
        }
        public virtual HtmlGenericControl getControl(bool alternate)
        {
            HtmlGenericControl li = new HtmlGenericControl("li");
            //li.Attributes.CssStyle.Add("list-style-image", "url(" + Config.RootDist + "css/" + Config.Skin + "/" + Icon + ")");
            li.Attributes["class"] = "SideToolbarButton";
            if(CssClass!=null && CssClass.Length>0)
                li.Attributes["class"] += " " + CssClass;
            li.InnerHtml = "<a ";
            if(alternate)
                li.InnerHtml += "class=\"alternate\"";
            li.InnerHtml += " alt=\"" + Label + "\"";
            li.InnerHtml += "href=\"";
            if (AddRootDist)
            {
                li.InnerHtml += Config.Paths.AbsoluteNetworkRoot+"/";
            }
            li.InnerHtml += Href;

            li.InnerHtml += "\">";
            string iconurl = this.Icon; //this.Icon.ToLower().Contains("webresource.axd") ? this.Icon : Config.Paths.AbsoluteRoot + "/css/default/" + this.Icon;
            li.InnerHtml += "<div style=\"background-image:url(" + iconurl + ")\" >";
            
            li.InnerHtml += "<span>" + Label + "</span></div></a>";
            return li;
        }
        public virtual HtmlGenericControl getControl()
        {
            return getControl(false);
        }

    }
}