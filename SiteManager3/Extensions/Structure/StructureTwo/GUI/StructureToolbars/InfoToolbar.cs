using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Toolbar
/// </summary>

namespace NetCms.GUI
{
    public class InfoToolbar
    {
        private string _BackAddress;
        public string BackAddress
        {
            get { return _BackAddress==null?"":_BackAddress; }
            set { _BackAddress = value; }
        }

        public Structure.WebFS.StructureFolder CurrentFolder
        {
            get
            {
                return _CurrentFolder;
            }
        }
        private Structure.WebFS.StructureFolder _CurrentFolder;

        private bool _ShowFolderPath;
        public bool ShowFolderPath
        {
            get { return _ShowFolderPath; }
            set { _ShowFolderPath = value; }
        }
	
        private string _Icon;
        public string Icon
        {
            get { return _Icon == null ? "" : _Icon; }
            set { _Icon = value; }
        } 

        private string _Title;
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        //private NetCms.PageData PageData;
        public InfoToolbar(Structure.WebFS.StructureFolder CurrentFolder, string title)
        {
            Title = title;
            _CurrentFolder = CurrentFolder;
        }

        public InfoToolbar(Structure.WebFS.StructureFolder CurrentFolder)
            : this(CurrentFolder, "")
        {
        }

        public InfoToolbar()
            : this(null, "")
        {
        }
        public HtmlGenericControl getControl()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.ID = "AppToolbar";

            HtmlGenericControl caption = GetCaption();
            div.Controls.Add(caption);

            HtmlGenericControl hr = new HtmlGenericControl("div");
            hr.Attributes["class"] = "hr";
            div.Controls.Add(hr);
            
            div.Controls.Add(new BackToolbarButton(false));
                        
            hr = new HtmlGenericControl("div");
            hr.Attributes["class"] = "hr clearleft";
            div.Controls.Add(hr);

            return div;
        }

        private HtmlGenericControl GetCaption()
        {
            HtmlGenericControl value = new HtmlGenericControl("div");

            HtmlGenericControl frontend = new HtmlGenericControl("a");
            if (CurrentFolder != null)
            {
                frontend.Attributes["class"] = "Where_FrontLink";
                frontend.Attributes["href"] = this.CurrentFolder.StructureNetwork.Paths.AbsoluteFrontRoot+"/";
                if(CurrentFolder!= null)
                    frontend.Attributes["href"] = this.CurrentFolder.FrontendHyperlink;

                frontend.Attributes["title"] = "Vai al frontend di " + this.CurrentFolder.StructureNetwork.Label;
                frontend.Attributes["target"] = "_blank";
                frontend.InnerHtml = "<span>Where_FrontLink</span>";
            }
            HtmlGenericControl jslabels = new HtmlGenericControl("a");
            jslabels.ID = "Toolbar_JSLabels";
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "Where_Back";
            div.Controls.Add(frontend);
            div.Controls.Add(jslabels);
            HtmlGenericControl left = new HtmlGenericControl("div");
            left.Attributes["class"] = "Where_Left Where_Icon_" + Icon;
            HtmlGenericControl right = new HtmlGenericControl("div");
            right.Attributes["class"] = "Where_Right";
            
            if (NetCms.Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork)
            {
                HtmlGenericControl net = new HtmlGenericControl("div");
                net.Attributes["class"] = "Where_NetworkInfo";
                net.InnerHtml = string.Format("<span>Portale Corrente: <strong>{0}</strong><span>", this.CurrentFolder.StructureNetwork.Label);
                div.Controls.Add(net);
            }

            div.Controls.Add(right);
            right.Controls.Add(left);

            HtmlGenericControl caption = new HtmlGenericControl("div");
            caption.ID = "toolbarCaption";
            caption.InnerHtml = "" + Title + "";
            left.Controls.Add(caption);

            if (this.CurrentFolder != null && ShowFolderPath)
            {
                HtmlGenericControl path = new HtmlGenericControl("div");
                path.Attributes["class"] = "Where_Path breadCrumb module";
                path.InnerHtml = this.CurrentFolder.LabelLinkPathBreadCrumbs;
                right.Controls.AddAt(0,path);
            }
            

            value.Controls.Add(div);

            return value;
        }
    }
}