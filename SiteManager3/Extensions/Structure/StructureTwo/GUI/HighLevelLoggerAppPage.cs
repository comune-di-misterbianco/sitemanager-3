﻿using System;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NetCms.Vertical;
using NetCms.GUI;

/// <summary>
/// Summary description for FilesDocument
/// </summary>
namespace NetCms.LogAndTrance
{
    public class HighLevelLoggerApplicationPage:SmPageVertical
    {
        public HighLevelLoggerApplicationPage()
        {                        
            Toolbar.Buttons.Add(new ToolbarButton("log_global.aspx", NetCms.GUI.Icons.Icons.Table, "Mostra Log SQL"));
        }
              
        public override string PageTitle
        {
            get { return "Log Verboso"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Table; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_StartPage"; }
        }
        protected override WebControl RootControl
        {
            get
            {
                return null;// new HighLevelLoggerControl(Connection);
            }
        }
    }
}
