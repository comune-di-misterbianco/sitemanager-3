﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.GUI
{
    public class Collapsable : WebControl
    {
        public enum Colors
        {
            Ligth,Dark
        }
        public virtual bool UseCookies { get{ return false;} }

        public bool StartCollapsed
        {
            get { return _StartCollapsed; }
            set { _StartCollapsed = value; }
        }
        private bool _StartCollapsed;
        
        public virtual string Title
        {
            get { return _Title; }
            /*protected*/ set { _Title = value; }
        }
        private string _Title;

        public Colors Color
        {
            get { return _Color; }
            set { _Color = value; }
        }
        private Colors _Color;
			
        public virtual WebControl TitleControl
        {
            get
            {
                if (_TitleControl == null)
                {
                    _TitleControl = new WebControl(HtmlTextWriterTag.Div);
                    _TitleControl.CssClass = "toggle-button ";
                    _TitleControl.CssClass += UseCookies ? "save-state " : "";
                    _TitleControl.CssClass += this.Color.ToString().ToLower();
                    if (!string.IsNullOrEmpty(Title))
                        _TitleControl.Controls.Add(new LiteralControl(this.Title));
                }
                return _TitleControl;
            }
        }
        private WebControl _TitleControl;

        public virtual WebControl ToggleButton
        {
            get
            {
                if (_ToggleButton == null)
                {
                    _ToggleButton = new WebControl(HtmlTextWriterTag.A);
                    _ToggleButton.Attributes.Add("href", "#");
                    _ToggleButton.Controls.Add(new LiteralControl("<span>Mostra/Nascondi</span>"));
                }
                return _ToggleButton;
            }
        }
        private WebControl _ToggleButton;

        private WebControl CollapsableControl
        {
            get { return _CollapsableControl; }
            set { _CollapsableControl = value; }
        }
        private WebControl _CollapsableControl;


        public int CollapseControlCount
        {
            get
            {
                if (CollapsableControl != null)
                {
                    return CollapsableControl.Controls.Count;
                }
                else
                    return 0;
            }
        }
        
        public Collapsable()
            : base(HtmlTextWriterTag.Div)
        {
            CollapsableControl = new WebControl(HtmlTextWriterTag.Div);
        }
        public Collapsable(string title)
            : base(HtmlTextWriterTag.Div)
        {
            Title = title;
            CollapsableControl = new WebControl(HtmlTextWriterTag.Div);
        }
        public void AppendControl(Control control)
        {
            this.CollapsableControl.Controls.Add(control);
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            CollapsableControl.CssClass += " collapsable";
            if (this.StartCollapsed)
            {
                TitleControl.CssClass += " collapsed";
                CollapsableControl.CssClass += " collapsed-block";
            }

            TitleControl.Controls.Add(ToggleButton);

            this.Controls.Add(this.TitleControl);
            this.Controls.Add(this.CollapsableControl);
        }
    }
}
