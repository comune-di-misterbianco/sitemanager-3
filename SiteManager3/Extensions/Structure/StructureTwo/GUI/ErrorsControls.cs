using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI
{
    public static class ErrorsControls
    {
        private static string BackControls()
        {
            string html = "";
            if ( NetCms.Networks.NetworksManager.CurrentFace == Faces.Admin)
            {
                if (NetCms.Users.AccountManager.Logged)
                {
                    html += @"
			        <p class=""text-center"">
                    <a class=""btn btn-secondary"" href=""" + NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL() + @""">Torna Indietro</a>
			        <a class=""btn btn-primary"" href=""" + NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + @""">Torna alla Homepage</a></p>
		            ";
                }
            }
            else
            {
                html += @"<p class=""text-center"">";
                if (HttpContext.Current.Request.UrlReferrer != null)
                    html += @"<a class=""btn btn-secondary"" href=""" + HttpContext.Current.Request.UrlReferrer + @""">Torna Indietro</a>";

                if (NetCms.Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork)
                {
                    string homeUrl = string.IsNullOrEmpty(NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot) ? "/" : NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;

                    html += @"<a class=""btn btn-primary""  href=""" + homeUrl + @""">Torna alla Homepage</a>";
                }
                html += "</p>";


            }

            return html;
        }

        public static WebControl NoGrantsControl(Exception ex = null)
        {
            //get
            //{
                WebControl control = new WebControl(HtmlTextWriterTag.Div);
                control.CssClass = "systemError noGrants  w-75 p-3 justify-content-center mx-auto";
               
                string html;
                //if (NetCms.Configurations.XmlConfig.GetNodeXml("/Portal/configs/errorNoGrants") != null)
                //    html = G2Core.Common.Utility.RemoveCDATA(NetCms.Configurations.XmlConfig.GetNodeXml("/Portal/configs/errorNoGrants"));
                //else
                    html = "<div class=\"card\">"
                         + "<div class=\"card-body\">"
                         + "<h5 class=\"card-title\">Errori rilevati</h5>"
                         + "<div class=\"alert\"><i class=\"fa fa-exclamation-triangle fa-4x red\" aria-hidden=\"true\"></i></div>"
                         + "<h6 class=\"card-subtitle\">Accesso Negato.</h6>"
                         + "<p class=\"card-text\">Non possiedi i permessi per accedere a questa risorsa. <br/>"
                         + "Se ritieni che dovresti avere accesso a questa risorsa prova a effettuare il logout e successivamente riprova ad accedere a questa risorsa.<br/>"
                         + "Se il problema dovesse persistere contatta il tuo amministratore in modo che verifichi i tuoi permessi.</p>"

                         + BackControls()
                         + "</div>";

            control.Controls.Add(new LiteralControl(html));

                return control;
            //}
        }

        public static WebControl Error404Control(Exception ex = null)
        {            
			   
							
            WebControl control = new WebControl(HtmlTextWriterTag.Div);
            control.CssClass = "systemError  w-75 p-3 justify-content-center mx-auto";

            string html;
																														 
									   

            html = "<div class=\"card\">"
                + "<div class=\"card-body\">"
                + "<div class=\"alert\"><i class=\"fa fa-exclamation-triangle fa-4x red\" aria-hidden=\"true\"></i></div>"
                 + "<h5 class=\"card-title\">Errori rilevati</h5>"

                 + "<h6 class=\"card-subtitle\">Error 404 - Pagina non trovata</h6>"
                 + "<p class=\"card-text\">La pagina non � pi� disponibile oppure l'indirizzo digitato non � corretto.</p>"
                 + BackControls()
                 + "</div>";

            control.Controls.Add(new LiteralControl(html));

            return control;            
			 
        }

        public static WebControl SystemErrorControl(Exception ex = null)
        {
            //get
            //{
                WebControl control = new WebControl(HtmlTextWriterTag.Div);
                control.CssClass = "systemError  w-75 p-3 justify-content-center mx-auto";
            // control.Controls.Add(new LiteralControl(G2Core.Common.Utility.RemoveCDATA(NetCms.Configurations.XmlConfig.GetNodeXml("/Portal/configs/errorsInfo"))));


                string html = "<div class=\"card\">"
                            + "<div class=\"card-body\">"
                            + "<div class=\"alert\"><i class=\"fa fa-exclamation-triangle fa-4x red\" aria-hidden=\"true\"></i></div>"
                            + "<h5 class=\"card-title\">Errori rilevati</h5>"

                            + "<h6 class=\"card-subtitle\">Il sistema ha rilevato un errore interno</h6>"
                            + "<ul  class=\"list-group\">";

                foreach (var log in NetCms.Diagnostics.Diagnostics.ExceptionsForThisPage)
                {
                    html += "<li class=\"list-group-item\">"
                             + ((log.LogErrorCode != null && log.LogErrorCode.Length > 0) ? "<p class=\"card-text\">Codice: " + log.LogErrorCode + "</p>" : "") 
                         + "<p class=\"card-text\">ID: " + log.LogID + "</p>"                         
                         + ((log.LogMessage != null && log.LogMessage.Length > 0) ? "<p class=\"card-text\">Errore: " + log.LogMessage + "</p>" : "")
                         + "</li>";
                }
                
               if (ex != null)
               {
                  html += "<li class=\"list-group-item\">"
                       + "<p class=\"card-text\">"
                       + "<br />"
                       + "<strong>Messaggio</strong>: " + ex.Message
                       + "<br />"
                       + "<strong>Sorgente</strong>: " + ex.Source
                       + "</p>"
                       + "</li>";
               }


                html += "</ul>" + "<br />";


                html += BackControls();

                html += "</div>";

                control.Controls.Add(new LiteralControl(html));

                return control;
            //}
        }

        public static WebControl GenericErrorControl(Exception ex = null)
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);
            control.CssClass = "systemError w-75 p-3 justify-content-center mx-auto";

            string html;

            html = "<div class=\"card\">"
                 + "<div class=\"card-body\">"
                 + "<div class=\"alert\"><i class=\"fa fa-exclamation-triangle fa-4x red\" aria-hidden=\"true\"></i></div>"
                 + "<h5 class=\"card-title\">Errori rilevati</h5>"                 
                 + "<h6 class=\"card-subtitle\">Error non riconosciuto</h6>"
                 + "<p class=\"card-text\">E' stato rilevato un errore non gestito dal sistema: si invita a contattare l'assistenza tecnica per segnalare l'accaduto.</p>";

            if (ex != null)
             {
                html += "<p  class=\"card-text\">"
                     + "<br />"
                     + "<strong>Messaggio</strong>: " + ex.Message
                     + "<br />"
                     + "<strong>Sorgente</strong>: " + ex.Source
                     + "</p>";                            
            }

            html += BackControls()
                 + "</div>";

            control.Controls.Add(new LiteralControl(html));

            return control;
        }

    }
}