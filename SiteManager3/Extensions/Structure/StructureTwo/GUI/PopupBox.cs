using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Reflection;

/// <summary>
/// Summary description for FilesDocument
/// </summary>
namespace NetCms.GUI
{
    public enum PostBackMessagesType { Normal, Success, Notify, Error }
    public class PopupBox:WebControl
    {
        public WebControl AlertBox
        {
            get
            {
                if (_AlertBox == null)
                {
                    _AlertBox = new WebControl(HtmlTextWriterTag.Div);
                    _AlertBox.ID = "alert";
                    _AlertBox.CssClass = "overlay popup";
                }
                return _AlertBox;
            }
        }
        private WebControl _AlertBox;
        
        public WebControl FrameBox
        {
            get
            {
                if (_FrameBox == null)
                {
                    _FrameBox = new WebControl(HtmlTextWriterTag.Div);
                    WebControl iframe = new WebControl(HtmlTextWriterTag.Iframe);
                    _FrameBox.ID = "alertframe";
                    _FrameBox.CssClass = "overlay popup";
                    //_FrameBox.Controls.Add(iframe);
                }
                return _FrameBox;
            }
        }
        private WebControl _FrameBox;

        public WebControl Title
        {
            get
            {
                if (_Title == null)
                {
                    _Title = new WebControl(HtmlTextWriterTag.Div);
                    _Title.Controls.Add(new LiteralControl("Informazioni"));
                }
                return _Title;
            }
        }
        private WebControl _Title;

        public bool OpenBox
        {
            get { return _OpenBox; }
            private set { _OpenBox = value; }
        }
        private bool _OpenBox;
			
		public PopupButton PopupButton
		{
			get 
			{ 
				if(_PopupButton == null)
					_PopupButton = new PopupButton(this);
				return _PopupButton; 
			} 
		}
		private PopupButton _PopupButton;

        public NetCms.GUI.PostBackMessagesType Status
        {
            get
            {
                return _Status;
            }
            set { _Status = value; }
        }
        private NetCms.GUI.PostBackMessagesType _Status;

        public PopupBox():base(HtmlTextWriterTag.Div)
        {                        
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.Controls.Add(AlertBox);
            //this.Controls.Add(FrameBox);
           /*
            switch (Status)
            {
                case BoxStates.Error:   this.CssClass += " error"; break;
                case BoxStates.Notify:  this.CssClass += " notify"; break;
                case BoxStates.Success: this.CssClass += " success"; break;
                default:                this.CssClass += " normal"; break;
            }
 */
           
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            this.ParseMessages();
            
            if(OpenBox) this.PopupButton.SetStatus(Status);
            {
                string color = "#586C8F";
                switch (Status)
                {
                    case PostBackMessagesType.Error: color = "#E5524B"; break;
                    case PostBackMessagesType.Notify: color = "#FFF5A9"; break;
                    case PostBackMessagesType.Success: color = "#549148"; break;
                }


                G2Core.XhtmlControls.JavaScript script = new G2Core.XhtmlControls.JavaScript();
                this.Controls.Add(script);
                script.ScriptText = @"
$(document).ready(function(){
        $(""#alert"").overlay({

	        finish: {top: 'center'},
	        expose: '" + color + @"'

        });" + (this.OpenBox ? @"$(""#alert"").overlay().load();" : "") + @"
        $(""#PostBackList"").click(function()
        {
            $(""#alert"").overlay().load();
        });
        $(""#alertframe"").overlay({

	        finish: {top: 'center'},
	        expose: '#586C8F'

        });
	});


";
            }
        }

        public static void AddMessage(string msg)
        {
            AddMessage(msg, PostBackMessagesType.Normal, false);
        }
        public static void AddMessage(string msg, NetCms.GUI.PostBackMessagesType state)
        {
            AddMessage(msg, state, false);
        }
        private static void AddMessage(string msg, NetCms.GUI.PostBackMessagesType state, bool session)
        {
            PopupMessage message = new PopupMessage(msg, state);

            object objValues;
            if (session)
            {
                if (HttpContext.Current.Session["PopupMessages"] == null) 
                    HttpContext.Current.Session["PopupMessages"] = new List<PopupMessage>();
                objValues = HttpContext.Current.Session["PopupMessages"];
            }
            else
            {
                if (HttpContext.Current.Items["PopupMessages"] == null)
                    HttpContext.Current.Items["PopupMessages"] = new List<PopupMessage>();
                objValues = HttpContext.Current.Items["PopupMessages"];
            }

            List<PopupMessage> PopupMessages = (List<PopupMessage>)objValues;
            PopupMessages.Add(message);
        }
        public static void AddMessageAndReload(string msg, NetCms.GUI.PostBackMessagesType state)
        {
            AddSessionMessage(msg, state, HttpContext.Current.Request.Url.ToString());
        }
        public static void AddSessionMessage(string msg, NetCms.GUI.PostBackMessagesType state)
        {
            AddMessage(msg, state, true);
        }
        public static void AddSessionMessage(string msg, NetCms.GUI.PostBackMessagesType state, bool goBack)
        {
            AddMessage(msg, state, true);
            
            if (goBack)
            {
                if (NetCms.Users.AccountManager.Logged)
                    NetCms.Diagnostics.Diagnostics.Redirect(NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                else
                    NetCms.Diagnostics.Diagnostics.Redirect(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot);
            }
        }

        public static void AddSessionMessage(string msg, NetCms.GUI.PostBackMessagesType state, string redirectToUrl)
        {
            AddMessage(msg, state, true);
           NetCms.Diagnostics.Diagnostics.Redirect(redirectToUrl);
        }

        public static void AddNoGrantsMessageAndGoBack()
        {
           NetCms.Diagnostics.Diagnostics.Redirect(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/error-no-grants.aspx");
        }

        private void ParseMessages()
        {
            AlertBox.Controls.Add(new LiteralControl("<span class=\"overlay_info_icon\">&nbsp;</span>"));

            if (HttpContext.Current.Session["PopupMessages"] != null)
            {
                List<PopupMessage> PopupMessages = (List<PopupMessage>)HttpContext.Current.Session["PopupMessages"];
                ParseMessages(PopupMessages);
                PopupMessages.Clear();

            }
            if (HttpContext.Current.Items["PopupMessages"] != null)
                ParseMessages((List<PopupMessage>)HttpContext.Current.Items["PopupMessages"]);
        }
        private void ParseMessages(List<PopupMessage> PopupMessages)
        {
            foreach (PopupMessage msg in PopupMessages)
                this.ParseMessage(msg.Message, msg.State);
        }
        
        private void ParseMessage(string msg, NetCms.GUI.PostBackMessagesType state)
        {
            OpenBox = true;

            LiteralControl control = new LiteralControl("<p>" + msg +"</p>");
            this.AlertBox.Controls.Add(control);

            if (this.Status == PostBackMessagesType.Normal)
                this.Status = state;
            else
                if (this.Status == PostBackMessagesType.Success && state != PostBackMessagesType.Normal)
                    this.Status = state;
                else
                    if (this.Status == PostBackMessagesType.Notify && state != PostBackMessagesType.Normal && state != PostBackMessagesType.Success)
                        this.Status = state;
                    else
                        if (this.Status == PostBackMessagesType.Notify && state != PostBackMessagesType.Normal && state != PostBackMessagesType.Success && state != PostBackMessagesType.Error)
                            this.Status = state;
        }       
    }

    public class PopupButton : WebControl
    {
        public PopupBox PopupBox
        {
            get { return _PopupBox; }
            private set { _PopupBox = value; }
        }
        private PopupBox _PopupBox;		

        public PopupButton(PopupBox popupBox) 
            : base(HtmlTextWriterTag.A)
        {
            PopupBox = popupBox;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            this.Attributes.Add("href","javascript:;");
            this.CssClass = "open_alert_button";
            this.Controls.Add(new LiteralControl("<span>Informazioni sugli eventi legati a questa pagina</span>"));
        }

        public void SetStatus(PostBackMessagesType Status)
        {
            switch (Status)
            {
                case PostBackMessagesType.Error: this.CssClass += " open_alert_button_red"; break;
                case PostBackMessagesType.Success: this.CssClass += " open_alert_button_green"; break;
                case PostBackMessagesType.Notify: this.CssClass += " open_alert_button_yellow"; break;
                default : this.CssClass += " open_alert_button_blue"; break;
            }
        }
}


    public class PopupMessage
    {
        public string Message
        {
            get { return _Message; }
            private set { _Message = value; }
        }
        private string _Message;

        public NetCms.GUI.PostBackMessagesType State
        {
            get { return _State; }
            private set { _State = value; }
        }
        private NetCms.GUI.PostBackMessagesType _State;

        public PopupMessage(string msg, NetCms.GUI.PostBackMessagesType state)
        {
            State = state;
            Message = msg;
        }
    }
}
