﻿using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.GUI.UploadManager.Model;
using System.Web.Http;
using NHibernate;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

namespace NetCms.Structure.GUI.UploadManager.Controller
{
    public class UploadManagerController : ApiController
    {
        [HttpPost]
        [ActionName("checkfile")]
        public DocumentStatus CheckDocumentInFolder([FromBody] JObject jobj) //public DocumentStatus CheckDocumentInFolder([FromBody] int folderId, [FromBody] string filename)
        {
            using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                DocumentStatus docstatus = new DocumentStatus();

                string caratterinonvalidi;
                bool exist = false;
                bool error = false;
                string message = string.Empty;

                int folderId = (jobj["folderId"] != null && jobj["folderId"].ToObject<int>() > 0) ? jobj["folderId"].ToObject<int>() : -1;
                string filename = (jobj["filename"].ToObject<string>() != null && jobj["filename"].ToObject<string>().Length > 0) ? jobj["filename"].ToObject<string>() : "";

                try
                {
                    //  string filenameCheck = (jobj["filenamecheck"] != null) ? jobj["filename"].ToObject<bool>() : false;

                    //string filenameCheck = (jobj["filename"].ToObject<string>() != null && jobj["filename"].ToObject<string>().Length > 0) ? jobj["filenamecheck"].ToObject<string>() : "";
                    //if (bool.Parse(filenameCheck))

                    CheckFileName(filename, out docstatus.filenameIsValid, out caratterinonvalidi);
                    if (!docstatus.filenameIsValid)
                    {
                        message = "Il nome del file presenta dei caratteri non conformi (" + caratterinonvalidi + "). Si invita a rinominarlo e riprovare col trasferimento.";
                        error = true;
                    }

                    
                }
                catch(System.Exception ex)
                {
                    Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, ex.Message);

                }

                // verifica esistenza documento nella folder di cms o nel path assoluto su disco
                if (!error)
                {
                    if (folderId != -1)
                    {
                        StructureFolder folder = FolderBusinessLogic.GetById(sess, folderId);

                        Document doc = DocumentBusinessLogic.GetDocumentByPhysicalNameAndFolder(filename, folder, sess);
                        if (doc != null)
                        {
                            exist = true;
                        }
                    }
                    docstatus.exist = exist;
                }

                if (!error)
                {
                    if (!exist)
                        docstatus.message = "Il documento selezionato non esiste e può essere trasferito.";
                    else
                        docstatus.message = "Attenzione: il documento " + filename + " è già presente; per trasferirlo modificare il nome.";
                }
                else
                {
                    docstatus.message = message;
                }

                return docstatus;
            }
        }


        [HttpPost]
        [ActionName("checkfile_status")]
        public DocumentStatus CheckDocument([FromBody] JObject jobj) //public DocumentStatus CheckDocumentInFolder([FromBody] int folderId, [FromBody] string filename)
        {
            using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                bool error = false;
                bool exist = false;
                bool checkNameFormat = false;
                string message = string.Empty;

                int folderId = (jobj["folderId"] != null && jobj["folderId"].ToObject<int>() > 0) ? jobj["folderId"].ToObject<int>() : -1;
                string filename = (jobj["filename"].ToObject<string>() != null && jobj["filename"].ToObject<string>().Length > 0) ? jobj["filename"].ToObject<string>() : "";
                string folderPath = (jobj["folderPath"].ToObject<string>() != null && jobj["folderPath"].ToObject<string>().Length > 0) ? jobj["folderPath"].ToObject<string>() : "";

                try
                {                    
                    string filenameCheck = (jobj["filenamecheck"].ToObject<string>() != null && jobj["filenamecheck"].ToObject<string>().Length > 0) ? jobj["filenamecheck"].ToObject<string>() : "";
                    if (bool.Parse(filenameCheck))
                        checkNameFormat = true;
                }
                catch (System.Exception ex)
                {
                    Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, ex.Message);

                }

                DocumentStatus docstatus = new DocumentStatus();

                // aggiungere regular expression per il controllo del nomefile 
                // se non conforme lo rigetta e blocca l'upload avvisando l'utente

                if (checkNameFormat)
                {                    
                    string caratterinonvalidi;

                    CheckFileName(filename, out docstatus.filenameIsValid, out caratterinonvalidi);
                    if (!docstatus.filenameIsValid)
                    {
                        message = "Il nome del file presenta dei caratteri non conformi ("+ caratterinonvalidi + "). Si invita a rinominarlo e riprovare col trasferimento.";
                        error = true;
                    }
                }

                // verifica esistenza documento nella folder di cms o nel path assoluto su disco
                if (!error)
                {
                    if (folderId != -1)
                    {
                        StructureFolder folder = FolderBusinessLogic.GetById(sess, folderId);

                        Document doc = DocumentBusinessLogic.GetDocumentByPhysicalNameAndFolder(filename, folder, sess);
                        if (doc != null)
                        {
                            exist = true;
                        }                        
                    }
                    docstatus.exist = exist;
                }

                if (!error)
                {
                    if (!exist)
                        docstatus.message = "Il documento selezionato non esiste e può essere trasferito.";
                    else
                        docstatus.message = "Attenzione: il documento " + filename + " è già presente; per trasferirlo modificare il nome.";
                }
                else
                {
                    docstatus.message = message;
                }

                return docstatus;

            }
        }

        public void CheckFileName(string filename, out bool esito, out string caratteriNonValidi)
        {
            caratteriNonValidi = string.Empty;
            string illegalChars = new string(System.IO.Path.GetInvalidFileNameChars()) + new string(System.IO.Path.GetInvalidPathChars());

            var invalidChars = System.Text.RegularExpressions.Regex.Escape(illegalChars) + "," + "”" + "’" + "/";
            
            string invalidRegStr = string.Format(@"^(CON|,|PRN|AUX|NUL|CLOCK\$|COM[1-9]|LPT[‌​1-9])(?=\..|$)|(^(\.‌​+|\s+)$)|((\.+|\s+)$‌​)|([{0}])", invalidChars);

            esito = !System.Text.RegularExpressions.Regex.IsMatch(filename, invalidRegStr, RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.CultureInvariant);
            System.Text.RegularExpressions.Match res2 = System.Text.RegularExpressions.Regex.Match(filename, invalidRegStr, RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.CultureInvariant);

            foreach(char result in res2.Value)
            {
                caratteriNonValidi += result;
            }
            
        }
     
    }
}
