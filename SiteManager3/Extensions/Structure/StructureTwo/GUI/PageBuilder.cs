using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Connections;
using NetCms;
using NetCms.GUI;
/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms
{
    public class PageBuilder
    {
        private Structure.Side Side;

        private Control _Corpo;
        public Control Corpo
        {
            get { return _Corpo; }
            set { _Corpo = value; }
        }

        public PopupBox PopupBox
        {
            get
            {
                if (_PopupBox == null)
                    _PopupBox = new PopupBox();
                return _PopupBox;
            }
        }
        private PopupBox _PopupBox;

        public NetCms.GUI.Scripts.ScriptsIndexer Scripts
        {
            get
            {
                if (_Scripts == null)
                    _Scripts = new NetCms.GUI.Scripts.ScriptsIndexer(SystemPage);
                return _Scripts;
            }
        }
        private NetCms.GUI.Scripts.ScriptsIndexer _Scripts;
        
        private System.Web.UI.Page SystemPage
        {
            get { return _SystemPage; }
            set { _SystemPage = value; }
        }
        private System.Web.UI.Page _SystemPage;

        private string InternalScripts
        {
            get
            {
                string HeadHtml = "";
                HeadHtml += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "dtree.css\" /><!--dtree.csx-->\n";
                HeadHtml += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "jquery-treeview.css\" /><!--jquery-treeview.csx-->\n";
                HeadHtml += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "homepage.css\" /><!--homepage.csx-->\n";
                HeadHtml += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "newsletter.css\" /><!--newsletter.csx-->\n";
                HeadHtml += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "side.css\" /><!--side.csx-->\n";  
                
                if (PageData.Account != null)
                {
                    if (PageData.Account.GUI.PreviewType == NetCms.Users.SitemanagerUserGUI.PreviewTypes.Floating)
                    {
                        HeadHtml += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "floating_popup.css\" /><!--Floating_Popup.csx-->\n";
                        HeadHtml += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Floating_Popup) + "\" ></script><!--Floating_Popup.js-->\n";
                    }

                    if (PageData.Account.GUI.PreviewType == NetCms.Users.SitemanagerUserGUI.PreviewTypes.Static)
                    {
                        HeadHtml += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "static_popup.css\" /><!--Static_Popup.csx-->\n";
                        HeadHtml += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Static_Popup) + "\" ></script><!--Static_Popup.js-->\n";
                    }
                }

                HeadHtml += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.xTreeView) + "\" ></script><!--xTreeView.js-->\n";
                HeadHtml += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Homepage) + "\" ></script><!--Homepage.js-->\n";
                HeadHtml += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.DTree) + "\" ></script><!--DTree.js-->\n";
                HeadHtml += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Clock) + "\" ></script><!--Clock.js-->\n";
                HeadHtml += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.FreeTextJsIntegration) + "\" ></script><!--FreeTextJsIntegration.js-->\n";
                HeadHtml += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.JScripts) + "\" ></script><!--JScripts.js-->\n";
                
                HeadHtml += @"
<script type=""text/javascript""><!--//--><![CDATA[//><!--
<!--
function setSubmitSource(value)
{
    if(document.getElementById('SubmitSource')!=null)
    {
        document.getElementById('SubmitSource').value = value;
    } 
}
//--><!]]></script>";
                HeadHtml += @"
                	            
                <script src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery) + @""" type=""text/javascript""></script><!--Jquery.js-->
                <script src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Cookie) + @""" type=""text/javascript""></script><!--Jquery_Cookie.js-->
                <script src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Treeview) + @""" type=""text/javascript""></script><!--Jquery_Treeview.js-->
                <script src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Treeview_Async) + @""" type=""text/javascript""></script><!--Jquery_Treeview_Async.js-->
                <script src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Treeview_Edit) + @""" type=""text/javascript""></script><!--Jquery_Treeview_Edit.js-->
	
                <script src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Tools) + @""" type=""text/javascript""></script><!--Tools.js-->
                <script type=""text/javascript"" src=""" + Configurations.Paths.AbsoluteSiteManagerRoot + @"/test.js""></script><!--DebugJS.js-->
                <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteSiteManagerRoot + @"/test.css"" /><!--test.csx-->
        ";

                return HeadHtml;
            }
        }
        
        private PageData PageData;
        private HtmlHead PageHeader;

        private string _HeaderText;
        public string HeaderText
        {
            get { return _HeaderText; }
            set { _HeaderText = value; }
        }

        public PageBuilder(PageData pagedata)
            : this(pagedata, new Structure.Side(pagedata))
        {
            
        }
        public PageBuilder(PageData pagedata, Structure.Side side)
        {
            PageData = pagedata;
            SystemPage = this.PageData.SystemPage;
            PageHeader = SystemPage.Header;
            Side = side;
            this.PageData.GUIBuilder = this;
        }

        public void BuildPage(HtmlForm Form)
        {
            Form.Method = "post";
            Form.Name = "PageForm";
            Form.ID = "PageForm";
            
            Form.Controls.Add(BuildHead());
            Form.Controls.Add(BuildSide());
            Form.Controls.Add(BuildBody(false));
            Form.Controls.Add(new GUI.SmPageFooter()); 

            PageHeader.InnerHtml = GetJavaScripts();
            PageHeader.InnerHtml += GetStyleSheets();
            PageHeader.InnerHtml += HeaderText;
            PageHeader.Title = Configurations.PortalData.TitoloCms;
        }
        public void BuildCommonPage(HtmlForm Form)
        {
            Form.Method = "post";
            Form.Name = "PageForm";
            Form.ID = "PageForm";

            Form.Controls.Add(BuildHead());
            Form.Controls.Add(BuildCommonSide());
            Form.Controls.Add(BuildBody(false));
            Form.Controls.Add(new GUI.SmPageFooter()); 

            PageHeader.InnerHtml = "";
            PageHeader.InnerHtml = GetJavaScripts();
            PageHeader.InnerHtml += GetStyleSheets();
            PageHeader.InnerHtml += HeaderText;
            PageHeader.Title = Configurations.PortalData.TitoloCms;
        }        
        public void BuildHomePage(HtmlForm Form)
        {
            Form.Method = "post";
            Form.Name = "PageForm";
            Form.ID = "PageForm";
            
            Form.Controls.Add(BuildHead());
            Form.Controls.Add(BuildHomeSide());
            Form.Controls.Add(BuildBody(false));
            Form.Controls.Add(new GUI.SmPageFooter()); 

            PageHeader.InnerHtml = "";
            PageHeader.InnerHtml = GetJavaScripts();
            PageHeader.InnerHtml += GetStyleSheets();
            PageHeader.InnerHtml += HeaderText;
            PageHeader.Title = Configurations.PortalData.TitoloCms;           
        }
        public void BuildPageLite(HtmlForm Form)
        {
            Form.Method = "post";
            Form.Name = "PageForm";
            Form.ID = "PageForm";

            Form.Controls.Add(BuildBody(true));

            PageHeader.InnerHtml = GetJavaScripts();
            PageHeader.InnerHtml = GetStyleSheetsLite();
            PageHeader.Title = Configurations.PortalData.TitoloCms;
        }
        public void BuildPageClean(HtmlForm Form)
        {
            if (Form != null)
            {
                Form.Method = "post";
                Form.Name = "PageForm";
                Form.ID = "PageForm";

                PageHeader.InnerHtml = "";
                PageHeader.InnerHtml = GetJavaScripts();
                PageHeader.InnerHtml += GetLoginStyleSheets();
                PageHeader.InnerHtml += HeaderText;
                PageHeader.Title = Configurations.PortalData.TitoloCms;

                Form.Controls.Add(BuildHead());
                Form.Controls.Add(BuildBody(true));
                Form.Controls.Add(new GUI.SmPageFooter());
            }
        }
        public void BuildPageDebug(HtmlForm Form)
        {
            Form.Method = "post";
            Form.Name = "PageForm";
            Form.ID = "PageForm";

            Form.Controls.Add(BuildBody(true));

            PageHeader.InnerHtml = GetJavaScripts();
            PageHeader.InnerHtml = GetStyleSheetsDebug();
            PageHeader.Title = Configurations.PortalData.TitoloCms;
        }
        public void BuildPagePopup(HtmlForm Form)
        {
            Form.Method = "post";
            Form.Name = "PageForm";
            Form.ID = "PageForm";

            Form.Controls.Add(BuildBody(true));

            PageHeader.InnerHtml = "";
            PageHeader.InnerHtml = GetJavascriptPopup();
            PageHeader.InnerHtml += GetStyleSheetPopup();

            PageHeader.Title = Configurations.PortalData.TitoloCms;
        }

        public void BuildPageThickBox(HtmlForm Form) 
        {
            Form.Method = "post";
            Form.Name = "PageForm";
            Form.ID = "PageForm";

            HtmlGenericControl mydiv = new HtmlGenericControl("div");
            mydiv.ID = "corpo2";
            if (Corpo != null)
                mydiv.Controls.Add(Corpo);
            mydiv.Controls.Add(PopupBox);

            Form.Controls.Add(mydiv);

            PageHeader.InnerHtml = "";
            PageHeader.InnerHtml = GetJavascriptThickBox();
            PageHeader.InnerHtml += GetStyleSheetThickBox();

            PageHeader.Title = Configurations.PortalData.TitoloCms;     
        }

        private HtmlGenericControl BuildHead()
        {
            HtmlGenericControl head  = new HtmlGenericControl("div");
            head.ID = "int";
            
            HtmlGenericControl title = new HtmlGenericControl("div");
            title.ID = "title";
            head.Controls.Add(title);

            HtmlGenericControl h1 = new HtmlGenericControl("h1");
            title.Controls.Add(h1);

            if (PageData.Account != null)
            {
                HtmlGenericControl info = new HtmlGenericControl("div");
                info.ID = "UserLoginInfo";
                title.Controls.Add(info);
                
                info.InnerHtml += "<span class=\"User\">Salve, <strong>" + Users.AccountManager.CurrentAccount.UserName + "</strong></span>";
                info.InnerHtml += " | ";
                info.InnerHtml += "<span class=\"Logout\"><a href=\"javascript: Submit('Logout');\">Logout</a></span>";                                


                if (NetCms.Configurations.Debug.GenericEnabled)
                {
                    info.InnerHtml += "| ";
                    string debugAnchor = @"<a href="""+Configurations.Paths.AbsoluteSiteManagerRoot+@"/debug.aspx?keepThis=true&TB_iframe=true&height=600&width=800"" title="""" class=""thickbox"">";
                    info.InnerHtml += "<span class=\"Debug\">" + debugAnchor + "<strong>Men� di Debug</strong></a></span>";
                }
            }
            
            return head;
        }
        private HtmlGenericControl BuildSide()
        {
            return Side.getMenu();
        }
        private HtmlGenericControl BuildCommonSide()
        {
            return Side.getCommonMenu();
        }
        private HtmlGenericControl BuildHomeSide()
        {
            return Side.getHomeMenu();
        }
        private HtmlGenericControl BuildBody(bool Lite)
        {
            HtmlGenericControl body = new HtmlGenericControl("div");
            body.ID = "corpo";
            
            if (!Lite)
                body.Controls.Add(PageData.InfoToolbar.getControl());

            if (Corpo != null)
                body.Controls.Add(Corpo);
            
            if (!Lite)
                body.Controls.Add(PopupBox);

            return body;
        }
        

        private string GetJavaScripts()
        {
            return this.InternalScripts + this.PageData.OuterScripts;
        }
        private string GetStyleSheets()
        {
            string customCss = Configurations.XmlConfig.GetAttribute("/Portal/configs/admin-gui", "css");

            string value = "";
            value = "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "style.css\" />\n"; ;
            value += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "smtable.css\" />\n";
            value += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.AbsoluteSiteManagerRoot + "/css/" + customCss + "/custom.css\" />\n";
            value += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.AbsoluteSiteManagerRoot + "/test.css\" />\n";
            return value;
        }
        private string GetLoginStyleSheets()
        {
            string customCss = Configurations.XmlConfig.GetAttribute("/Portal/configs/admin-gui", "css");

            string value = "";
            value = "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "loginpage.css\" />\n"; ;
            value += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.AbsoluteSiteManagerRoot + "/css/"+customCss+"/login.css\" />\n"; ;
            return value;
        }

        private string GetStyleSheetsLite()
        {

            string value = "";
            value = "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "lite.css\" />\n"; ;
            return value;
        }

        private string GetStyleSheetsDebug()
        {

            string value = "";
            value = "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "debug.css\" />\n"; ;
            value += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "axt.css\" />\n"; ;
            value += NetCms.Configurations.Scripts.JQuery.HtmlHeadCode;            
            value += NetCms.Configurations.Scripts.JQueryUI.HtmlHeadCode;
            value += @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteSiteManagerRoot + @"/test.css"" /><!--test.csx-->";
            value += @" <script src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Cookie) + @""" type=""text/javascript""></script><!--Jquery_Cookie.js-->
	
	<script type=""text/javascript"">
	$(document).ready(function(){
		$(function() {
		    $(""#tabs"").tabs();
	    })
	});
	</script>
        ";
            return value;
        }

        private string GetStyleSheetPopup() 
        {
            string value = "";
            value += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "gallery.css\" /><!--side.css-->";
            value += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "side.css\" /><!--side.css-->";
            value += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "jquery-treeview.css\" /><!--tree.csx-->";
            value += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "template.css\" /><!--template.csx-->";
            return value;
        }
        private string GetJavascriptPopup()
        {
            string value = "";
            value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.FreeTextJsIntegration) + "\" ></script><!--FreeTextJsIntegration.js-->\n";
            value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.JScripts) + "\" ></script><!--Scripts.js-->\n";

            value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery) + "\" ></script><!--Jquery.js-->\n";
            value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Cookie) + "\"></script><!--Jquery_Cookie.js-->\n";
            value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Treeview) + "\"></script><!--Jquery_Treeview.js-->\n";
            value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Treeview_Async) + "\"></script><!--Jquery_Treeview_Async.js-->\n";
			value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Treeview_Edit) + "\"></script><!--Jquery_Treeview_Edit.js-->\n";

            value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Tools) + "\"></script><!--Jquery_Tools.js-->\n";

            string idCurrentFolder = PageData.CurentFolderID.ToString();
            NetUtility.RequestVariable SelFolderID = new NetUtility.RequestVariable("StructureFolderSideTree", PageData.Request.Form);
            if (SelFolderID.IsValid(NetUtility.RequestVariable.VariableType.Integer) && SelFolderID.IntValue > 0)
            {
                idCurrentFolder = SelFolderID.StringValue;
            }
            
            value += @"<script type=""text/javascript"">
	$(document).ready(function(){
		$(""#FolderPickerAjaxTree"").treeview({
			url: """ + PageData.Paths.AbsoluteNetworkRoot + @"/cms/ajaxfolderpopup.aspx?folder=" + idCurrentFolder + @"""
		})
	});
	</script>
        ";  
            return value;
        }

        private string GetStyleSheetThickBox()
        {
            string value = "";
            value = "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "litethickbox.css\" />\n";
            value += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "axt.css\" />\n"; ;
            return value;
        }
        private string GetJavascriptThickBox()
        {
            string value = "";
            value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.FreeTextJsIntegration) + "\" ></script><!--FreeTextJsIntegration.js-->\n";
            value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.JScripts) + "\" ></script><!--Scripts.js-->\n";
            return value;
        }


        private string[] Mesi = { "Gennaio", "Febbario", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre" };
        private string[] Settimana = { "Luned�", "Marted�", "Mercoled�", "Gioved�", "Venerd�", "Sabato", "Domenica"};

        private string GiornoSettimana(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    return Settimana[0];
                case DayOfWeek.Tuesday:
                    return Settimana[1];
                case DayOfWeek.Wednesday:
                    return Settimana[2];
                case DayOfWeek.Thursday:
                    return Settimana[3];
                case DayOfWeek.Friday:
                    return Settimana[4];
                case DayOfWeek.Sunday:
                    return Settimana[6];
                case DayOfWeek.Saturday:
                    return Settimana[5];
            }
            return null;
        }
    }
}