using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Reflection;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Networks;
using System.Linq;
using NetCms.Structure.WebFS;

namespace NetCms.GUI.Startpage
{
    public class ModBookmarksPage : SmPageVertical
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.HomeSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public ModBookmarksPage()
        {
            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " accede alla pagina di modifica dei preferiti", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
        }

        public override string PageTitle
        {
            get { return "Modifica Preferiti"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Star; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_StartPage"; }
        }

        private WebControl _RootControl;
        protected override WebControl RootControl
        {
            get
            {
                if (_RootControl == null)
                {
                    _RootControl = new WebControl(HtmlTextWriterTag.Div);

                    foreach (BasicNetwork net in this.Account.AssociatedNetworks.Select(x => x.Network).Where(x=>x.CmsStatus == NetworkCmsStates.Enabled))
                    {
                        var ctr = FolderBusinessLogic.BookmarksEditableByNetwork(new BasicStructureNetwork(net), Account);
                        if (ctr != null) _RootControl.Controls.Add(ctr);
                    }
                }
                return _RootControl;
            }
        }
    }
}
