﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;
using System.Collections.Generic;
using NetCms.Vertical;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar
{
    public class VappPackageSideBar : SideBar
    {
       
        public override SideBarBox[] Boxes
        {
            get
            {
                List<SideBarBox> boxes = new List<SideBarBox>();

                boxes.Add(SmPage.PostbackBox);
                boxes.Add(new NetCms.GUI.SideBar.Boxes.InfoSideBox(this.SmPage.InfoBox));

                if (ShowToolbarOperations)
                    boxes.Add(OperationsBox);
                //bool InsideNetwork = NetCms.Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork;

                foreach(var item in RelatedAppsBoxes)
                    boxes.Add(item.Value);

                boxes.Add(new NetCms.GUI.SideBar.Boxes.ExtendedNetworksSideBox());
                boxes.Add(new NetCms.GUI.SideBar.Boxes.FooterSideBox());
                
                return boxes.ToArray();
            }
        }

        public Boxes.ToolbarSideBox OperationsBox
        {
            get
            {
                if (_OperationsBox == null)
                {
                    Toolbar toolbar  = new GUI.GenericToolbar();
                    _OperationsBox = new GUI.SideBar.Boxes.ToolbarSideBox(toolbar);
                }
                return _OperationsBox;
            }
            set
            {
                _OperationsBox = value;
            }
        }
        private Boxes.ToolbarSideBox _OperationsBox;

        public IDictionary<int, Boxes.VappPackageToolbarSideBox> RelatedAppsBoxes
        {
            get
            {
                if (_relatedAppsBoxes == null)
                {
                    _relatedAppsBoxes = new Dictionary<int, Boxes.VappPackageToolbarSideBox>();
                    bool InsideNetwork = NetCms.Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork;
                    foreach (int id in ContainedApps)
                    {
                        VerticalApplication application = VerticalApplicationsPool.ActiveAssemblies[id.ToString()];
                        if (application.Grant)
                        {
                            Boxes.VappPackageToolbarSideBox box = new Boxes.VappPackageToolbarSideBox(application.Label, application.SystemName + "-" + application.ID.ToString(), (InsideNetwork ? NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteAdminRoot : NetCms.Configurations.Paths.AbsoluteSiteManagerRoot) + "/applications/" + this.VAppSystemName + "/" + application.SystemName + application.BackofficeUrl);
                            box.StartCollapsed = true;
                            //box.Toolbar.Buttons.Add(new ToolbarButton((InsideNetwork ? NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteAdminRoot : NetCms.Configurations.Paths.AbsoluteSiteManagerRoot) + "/applications/" + this.VAppSystemName + "/" + application.SystemName + application.BackofficeUrl, NetCms.GUI.Icons.Icons.Script, application.Label));
                            _relatedAppsBoxes.Add(application.ID, box);
                        }
                    }
                }
                return _relatedAppsBoxes;
            }
            set
            {
                _relatedAppsBoxes = value;
            }
        }

        private IDictionary<int, Boxes.VappPackageToolbarSideBox> _relatedAppsBoxes;

        public SmPage SmPage
        {
            get { return _SmPage; }
            private set { _SmPage = value; }
        }
        private SmPage _SmPage;

        public int[] ContainedApps
        {
            get { return _ContainedApps; }
            private set { _ContainedApps = value; }
        }
        private int[] _ContainedApps;

        public string VAppSystemName
        {
            get { return _VAppSystemName; }
            private set { _VAppSystemName = value; }
        }
        private string _VAppSystemName;

        public bool ShowToolbarOperations
        {
            get { return _ShowToolbarOperations; }
            set { _ShowToolbarOperations = value; }
        }
        private bool _ShowToolbarOperations;

        public VappPackageSideBar(SmPage page, string vappSystemName, int[] containedApps) : base(page) { SmPage = page; ContainedApps = containedApps; VAppSystemName = vappSystemName; ShowToolbarOperations = false; }

    }
}



