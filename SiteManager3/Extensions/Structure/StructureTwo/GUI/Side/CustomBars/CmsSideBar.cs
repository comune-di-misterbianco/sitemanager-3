using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar
{
    public class CmsSideBar:SideBar
    {
        public override SideBarBox[] Boxes 
        {
            get
            {
                SideBarBox[] boxes = { 
                                         SmPage.PostbackBox ,
                                         new NetCms.GUI.SideBar.Boxes.InfoSideBox(this.SmPage.InfoBox), 
                                         new NetCms.GUI.SideBar.Boxes.ToolbarSideBox(this.SmPage.Toolbar), 
                                         new NetCms.GUI.SideBar.Boxes.FolderSideBox(),
                                         new NetCms.GUI.SideBar.Boxes.BookmarksSideBox() ,
                                         //new NetCms.GUI.SideBar.Boxes.LoginSideBox() ,
                                         //new NetCms.GUI.SideBar.Boxes.MessengerSideBox() ,
                                         //new NetCms.GUI.SideBar.Boxes.SearchSideBox(),
                                         //new NetCms.GUI.SideBar.Boxes.VersionSideBox() 
                                         new NetCms.GUI.SideBar.Boxes.FooterSideBox() 
                                     };
                return boxes;
            }
        }

        public CmsSideBar(SmPage page) : base(page) { }


    }
}



