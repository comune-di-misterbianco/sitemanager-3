using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar
{
    public abstract class SideBar:WebControl
    {
        public GUI.Toolbar ButtonsToolbar
        {
            get
            {
                if (_ButtonsToolbar == null)
                    _ButtonsToolbar = new GUI.GenericToolbar();
                return _ButtonsToolbar;
            }
        }
        private GUI.Toolbar _ButtonsToolbar;

        public Info.InfoBox InformationsBox
        {
            get
            {
                if (_InformationsBox == null)
                {
                    _InformationsBox = new Info.CmsInfoBox();
                }
                return _InformationsBox;
            }
        }
        private Info.InfoBox  _InformationsBox;

        public abstract SideBarBox[] Boxes { get; }

        public SmPage SmPage
        {
            get { return _SmPage; }
            private set { _SmPage = value; }
        }
        private SmPage _SmPage;
        
        public SideBar(SmPage page)
            : base(HtmlTextWriterTag.Div) 
        {
            SmPage = page;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            this.ID = "side";

            WebControl container = new WebControl(HtmlTextWriterTag.Div);
            container.CssClass = "sideContainer";
            foreach (SideBarBox box in this.Boxes)
                if(box.RenderBox) container.Controls.Add(box);

            this.Controls.Add(container);
        }
    }
}



