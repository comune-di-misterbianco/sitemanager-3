﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar.Boxes
{
    public class VappPackageToolbarSideBox : SideBarBox
    {
        public override string HtmlID { get; set; }
        public override string LocalCssClass { get { return "Commands"; } }

        public string BoxBaseLink { get; set; }
        public override bool UseCookies { get { return true; } }

        public override WebControl TitleControl
        {
            get
            {
                if (_TitleControl == null)
                {
                    _TitleControl = new WebControl(HtmlTextWriterTag.Div);
                    _TitleControl.CssClass = "toggle-button ";
                    _TitleControl.CssClass += UseCookies ? "save-state " : "";
                    _TitleControl.CssClass += this.Color.ToString().ToLower();
                    if (!string.IsNullOrEmpty(Title))
                    {
                        //WebControl link = new WebControl(HtmlTextWriterTag.A);
                        //link.Attributes.Add("href", BoxBaseLink);
                        //link.Controls.Add(new LiteralControl(this.Title));
                        //_TitleControl.Controls.Add(link);
                        _TitleControl.Attributes.Add("onclick", "javascript: window.location.href = \"" + BoxBaseLink + "\";");
                        _TitleControl.Controls.Add(new LiteralControl(this.Title));
                    }
                }
                return _TitleControl;
            }
        }
        private WebControl _TitleControl;

        //public override WebControl ToggleButton
        //{
        //    get
        //    {
        //        if (_ToggleButton == null)
        //        {
        //            _ToggleButton = new WebControl(HtmlTextWriterTag.A);
        //            _ToggleButton.Attributes.Add("href", BoxBaseLink);
        //            _ToggleButton.Controls.Add(new LiteralControl("<span>Mostra/Nascondi</span>"));
        //        }
        //        return _ToggleButton;
        //    }
        //}
        //private WebControl _ToggleButton;

        public override bool RenderBox
        {
            get
            {
                return true;
            }
        }

        public override Control Content
        {
            get { return this.Toolbar; }
        }

        public Toolbar Toolbar
        {
            get { return _Toolbar; }
            set { _Toolbar = value; }
        }
        private Toolbar _Toolbar;

        public VappPackageToolbarSideBox(Toolbar toolbar, string title, string htmlId)
        {
            Toolbar = toolbar;
            Title = title;
            HtmlID = htmlId;
        }

        public VappPackageToolbarSideBox(string title, string htmlId, string baseLink = "")
        {
            if (Toolbar == null)
                Toolbar = new GenericToolbar();
            Title = title;
            HtmlID = htmlId;
            BoxBaseLink = baseLink;
        }
    }
}



