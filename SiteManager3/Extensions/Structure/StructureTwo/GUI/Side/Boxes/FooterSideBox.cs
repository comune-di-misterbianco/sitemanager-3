using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar.Boxes
{
    public class FooterSideBox : SideBarBox
    {
        public override string Title { get { return "About"; } }
        public override string HtmlID { get { return "About"; } set { HtmlID = value; } }
        public override string LocalCssClass { get { return "CurentObjectSideInfo"; } }

        public override Control Content
        {
            get { return BuildControl(); }
        }

        public string FooterContentHtml
        {
            get
            {
                if (_FooterContentHtml == null)
                {
                    _FooterContentHtml = Configurations.XmlConfig.GetNodeXml("/Portal/configs/admin-gui/footer");
                    if (_FooterContentHtml != null)
                        _FooterContentHtml = G2Core.Common.Utility.RemoveCDATA(_FooterContentHtml);
                }
                return _FooterContentHtml;
            }
        }
        private string _FooterContentHtml;

        public FooterSideBox()
        { 
        }

        protected WebControl BuildControl()
        {
            WebControl Content = new WebControl(HtmlTextWriterTag.Div);
            Content.ID = HtmlID + "_Content";
            Content.CssClass = "content";


            if (this.FooterContentHtml != null)
                Content.Controls.Add(new LiteralControl(FooterContentHtml));

            return Content;
        }
    }
}



