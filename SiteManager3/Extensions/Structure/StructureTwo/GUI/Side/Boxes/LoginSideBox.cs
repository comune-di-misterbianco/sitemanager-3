using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar.Boxes
{
    public class LoginSideBox : SideBarBox
    {
        public override string Title { get { return "Account"; } }
        public override string HtmlID { get { return "AccountControl"; } set { HtmlID = value; } }
        public override string LocalCssClass { get { return "CurentObjectSideInfo"; } }

        public override bool UseCookies { get { return true; } }

        public override Control Content
        {
            get { return BuildControl(); }
        }

        public LoginSideBox() { }

        protected Control BuildControl()
        {/*
            #region Forms

            string forms = "<select name=\"Accounts\" id=\"Accounts\">";
            string li = "<option {0} value=\"{2}\">{1}</option>";

            DataTable users = this.Connection.SqlQuery("SELECT * FROM Users WHERE State_User = 1 AND Deleted_User = 0");

            foreach (DataRow row in users.Rows)
            {
                string selected = this.Account != null && row["id_User"].ToString() == this.Account.ID.ToString() ? "selected=\"selected\"" : "";
                forms += string.Format(li, selected, row["Name_User"].ToString(), row["id_User"].ToString());
            }

            forms += "</select>";

            forms += "<input id=\"DebugLoginButton\" name=\"DebugLoginButton\" type=\"button\" value=\"Login\" onclick=\"Submit('DebugLogin')\" />";

            #endregion
            */
            WebControl Content = new WebControl(HtmlTextWriterTag.Div);
            Content.ID = HtmlID + "_Content";
            Content.CssClass = "content";

            //Content.Controls.Add(new LiteralControl(forms));
            Content.Controls.Add(Info);

            return Content;
        }


        #region Info

        private WebControl Info
        {
            get
            {
                WebControl Info = new WebControl(HtmlTextWriterTag.Div);
                Info.CssClass = " CurentObjectSideInfo";

                HtmlGenericControl dati = new HtmlGenericControl("ul");

                dati.InnerHtml += "<li>Account Corrente: " + this.Account.UserName + "</li>";
                //dati.InnerHtml += "<li><a href=\"" + Configurations.Paths.AbsoluteSiteManagerRoot + "/user_pwd.aspx\">Modifica Password</a></li>";
                //dati.InnerHtml += "<li><a href=\"" + Configurations.Paths.AbsoluteSiteManagerRoot + "/user_data.aspx\">Modifica Dati Anagrafici</a></li>";
                //dati.InnerHtml += "<li><a href=\"" + Configurations.Paths.AbsoluteSiteManagerRoot + "/user_opt.aspx\">Opzioni Grafiche</a></li>";

                /*
                HtmlGenericControl output = new HtmlGenericControl("li");

                Button bt = new Button();
                bt.Text = "Logout";
                bt.OnClientClick = "setSubmitSource('Logout')";
                bt.ID = "Logout";
                output.Controls.Add(bt);

                dati.Controls.Add(output);*/

                Info.Controls.Add(dati);

                return Info;
            }
        }

        #endregion
    }
}



