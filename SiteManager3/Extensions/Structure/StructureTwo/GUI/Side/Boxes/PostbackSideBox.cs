using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar.Boxes
{
    public class PostbackBox: SideBarBox
    {
        public override string Title { get { return "Avviso"; } }
        public override string HtmlID { get { return "MsgBox"; } set { HtmlID = value; } }
        public override string LocalCssClass 
        { get { return "Notice "; } }

        public override bool UseCookies { get { return false; } }

        private Control _Content;
        public override Control Content
        {
            get
            {
                if (_Content == null) BuildContent();
                return _Content;
            }
        }

        private WebControl _List;
        public WebControl List
        {
            get
            {
                return _List;
            }
        }

        private WebControl _AlertList;
        public WebControl AlertList
        {
            get
            {
                return _AlertList;
            }
        }

        private WebControl _AlertControl;
        public WebControl AlertControl
        {
            get
            {
                return _AlertControl;
            }
        }


        private Info.InfoBox _InfoBox;
        public Info.InfoBox InfoBox
        {
            get
            {
                return _InfoBox;
            }
        }

        public PostbackBox(Info.InfoBox infoBox) 
        {
            this._InfoBox = infoBox;
            this.RenderBox = false;
            _AlertList = new WebControl(HtmlTextWriterTag.Ul);
            _List = new WebControl(HtmlTextWriterTag.Ul);

            _AlertControl = new WebControl(HtmlTextWriterTag.Div);
            WebControl alertTitle = new WebControl(HtmlTextWriterTag.H2);
            alertTitle.Controls.Add(new LiteralControl("Informazioni"));
            _AlertControl.Controls.Add(alertTitle);
            _AlertControl.ID = "alertC";
            _AlertControl.CssClass = "overlay";
            _AlertControl.Controls.Add(AlertList);
            /*
            if (this.InfoBox.PostbackMessages.Count > 0)
            {
                this.AddPostbackInfoLine("<ul>");
                foreach (string info in this.InfoBox.PostbackMessages)
                    this.AddPostbackInfoLine(info);
                this.AddPostbackInfoLine("</ul>");
            }*/
        }

        public void AddPostbackInfoLine(string info)
        {
            List.Controls.Add(new LiteralControl(info));
            List.ID = "PostBackList";
            AlertList.Controls.Add(new LiteralControl(info));
            if (this.RenderBox == false)
            {
                G2Core.XhtmlControls.JavaScript script = new G2Core.XhtmlControls.JavaScript();
                AlertList.Controls.Add(script);
                script.ScriptText = @"
$(document).ready(function(){
        $(""#alertC"").overlay({

	        finish: {top: 'center'},
	        expose: '#586C8F'

        });
		$(""#alertC"").overlay().load();
        
        $(""#PostBackList"").click(function()
        {
            $(""#alertC"").overlay().load();
        });
	});


";
            }
            this.RenderBox = true;
        }

        private void BuildContent()
        {
            _Content = new WebControl(HtmlTextWriterTag.Div);

            _Content.Controls.Add(List);
            
        }
    }
}



