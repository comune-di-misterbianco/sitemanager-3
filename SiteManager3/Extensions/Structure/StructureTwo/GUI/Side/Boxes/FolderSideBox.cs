using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar.Boxes
{
    public class FolderSideBox : SideBarBox
    {
        public override string Title { get { return "Cartelle"; } }
        public override string HtmlID { get { return "Tree"; } set { HtmlID = value; } }
        public override string LocalCssClass { get { return "SideFolders"; } }


        public override Control Content
        {
            get { return BuildControl(); }
        }

        public FolderSideBox(){ }

        protected WebControl BuildControl()
        {
            WebControl Tree_Content = new WebControl(HtmlTextWriterTag.Div);
            Tree_Content.ID = "Tree_Content";
            Tree_Content.CssClass = "content";

            WebControl list = new WebControl(HtmlTextWriterTag.Ul);
            list.ID = "FoldersTreeAjax";
            list.CssClass = "treeview";
            Tree_Content.Controls.Add(list);

            return Tree_Content;
        }
    }
}



