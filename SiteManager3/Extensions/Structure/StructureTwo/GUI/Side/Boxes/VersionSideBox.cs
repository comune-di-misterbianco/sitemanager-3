using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar.Boxes
{
    public class VersionSideBox : SideBarBox
    {
        public override string Title { get { return "Informazioni su SiteManager 2"; } }
        public override string HtmlID { get { return "Version"; } set { HtmlID = value; } }
        public override string LocalCssClass { get { return "CurentObjectSideInfo"; } }

        public override bool UseCookies { get { return true; } }

        public override Control Content
        {
            get { return BuildControl(); }
        }

        public VersionSideBox() { }

        protected Control BuildControl()
        {
            WebControl Content = new WebControl(HtmlTextWriterTag.Div);
            Content.ID = HtmlID + "_Content";
            Content.CssClass = "content";

            WebControl list = new WebControl(HtmlTextWriterTag.Ul);
            list.Controls.Add(new LiteralControl("<span class=\"net-service-logo\"></span>"));
            //list.Controls.Add(new LiteralControl("<li>SiteManager v." + PageData.ChangeLog.Version + "</li>"));

            Content.Controls.Add(list);
            return Content;
        }
    }
}



