using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar.Boxes
{
    public class ExtendedNetworksSideBox : SideBarBox
    {
        public const int MaxPortalsButtonsToShow = 10;

        public override string Title { get { return "Gestione Portali"; } }
        public override string HtmlID { get { return "ExtendedNetworks"; } set { HtmlID = value; } }
        public override string LocalCssClass { get { return "Commands"; } }

        public override bool UseCookies { get { return true; } }

        private Control _Content;
        public override Control Content
        {
            get { return _Content ?? (_Content = new NetworksUpdatePanel(Account, MaxPortalsButtonsToShow)); }
        }
        
        public ExtendedNetworksSideBox()
        {
        }
    }

}



