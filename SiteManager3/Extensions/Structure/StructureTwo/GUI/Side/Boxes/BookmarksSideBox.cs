using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;
using System.Collections.Generic;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Networks.WebFS;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar.Boxes
{
    public class BookmarksSideBox : SideBarBox
    {
        public override string Title { get { return "Preferiti"; } }
        public override string HtmlID { get { return "Preferiti"; } set { HtmlID = value; } }
        public override string LocalCssClass { get { return "Commands"; } }

        public override bool UseCookies { get { return true; } }

        public override Control Content
        {
            get { return BuildControl(); }
        }

        public BookmarksSideBox() {}

        protected WebControl BuildControl()
        {
            //string sql = "SELECT * FROM Favorites";
            //sql += " INNER JOIN Folders ON id_Folder = Folder_Favorite";
            //sql += " INNER JOIN FoldersLang ON id_Folder = Folder_FolderLang";
            //sql += " INNER JOIN Lang ON id_Lang = Lang_FolderLang";
            //sql += " INNER JOIN Networks ON id_Network = Network_Folder";
            //sql += " WHERE User_Favorite = " + this.Account.ID;
            //sql += " AND Network_Folder = " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.ID;
            //sql += " AND Default_Lang = 1 ORDER BY Label_FolderLang";
            //DataTable table = this.Connection.SqlQuery(sql);

            string favoritesString = "";
            WebControl control = null;
            try
            {
                //ICollection<NetworkFolder> favorites = FolderBusinessLogic.FindFavoritesByUserAndNetwork(this.Account, NetCms.Networks.NetworksManager.CurrentActiveNetwork);

                NetCms.Users.User currentUser = NetCms.Users.UsersBusinessLogic.GetById(NetCms.Users.AccountManager.CurrentAccount.ID);
                ICollection<NetworkFolder> favorites = FolderBusinessLogic.FindFavoritesByUserAndNetwork(currentUser, NetCms.Networks.NetworksManager.CurrentActiveNetwork);

                if (favorites.Count > 0)
                {
                    control = new WebControl(HtmlTextWriterTag.Ul);
                    control.CssClass = "favorites";
                    foreach (NetworkFolder favorite in favorites)
                    {
                        string folderLabel = (favorite as StructureFolder).Label;
                        favoritesString += "<li class=\"SideToolbarButton\"><a alt=\"" + folderLabel + " \" href=\"" + Configurations.Paths.AbsoluteSiteManagerRoot + "/" + favorite.Network.SystemName + "/cms/default.aspx?folder=" + favorite.ID + "\"><div style=\"background-image:url(" + NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Star) + ")\" ><span>" + folderLabel + "</span></div></a></li>";
                    }
                    //Favorites += "<li class=\"SideToolbarButton\"><a alt=\"Gestisci Preferiti\" href=\"" + Configurations.Paths.AbsoluteSiteManagerRoot + "/favorites.aspx\"><div style=\"background-image:url(/css/default/toolbar/manage.png)\" ><span>Gestisci Preferiti</span></div></a></li>";
                    control.Controls.Add(new LiteralControl(favoritesString));

                }
                else
                {
                    control = new WebControl(HtmlTextWriterTag.P);
                    control.CssClass = "favorites p-1";
                    favoritesString += "Non ci sono preferiti <br /> Per aggiungerne uno utilizzare l'apposito tasto presente in ogni cartella del gestore";
                    control.Controls.Add(new LiteralControl(favoritesString));
                }
            }
            catch(Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "BookmarksSideBox.cs - Si � verificata l'eccezione : " + ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName);
            }
            
            //WebControl list = new WebControl(HtmlTextWriterTag.Ul);
            //list.CssClass = "favorites";
            //list.Controls.Add(new LiteralControl(favoritesString));
            return control;
        }

    }

}



