using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;
using NetCms.Connections;
using NetCms.Networks;
using System.Collections.Generic;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar.Boxes
{
    public class NetworksUpdatePanel : UpdatePanel 
    {
        //public const string WhereModel = "WHERE User_UserNetwork = {0}";
        public int MaxPortalsButtonsToShow { get; private set; }
        //public Connection Connection { get; private set; }

        //public DataTable Table
        //{
        //    get
        //    {
        //        if (_Table == null)
        //        {
                    
        //            _Table = this.Connection.SqlQuery("SELECT DISTINCT * FROM users_networks INNER JOIN networks ON id_Network = Network_UserNetwork WHERE User_UserNetwork = {0}".FormatByParameters(Account.ID));
        //        }
        //        return _Table;
        //    }
        //}
        //private DataTable _Table;

        public List<BasicNetwork> CurrentUserNetworks
        {
            get 
            {
                return Account.AssociatedNetworks.Select(x => x.Network).ToList();
            }
        }
        
        //public NetworksData NetworksData
        //{
        //    get
        //    {
                
        //        return _NetworksData ?? (_NetworksData = new NetCms.Networks.NetworksData());
        //    }
        //}
        //private NetworksData _NetworksData;

        public List<BasicNetwork> NetworksData
        {
            get
            {
                return NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.Where(x=>x.Value.Enabled).Select(x => x.Value).ToList();
            }
        }
        

        public WebControl List
        {
            get
            {
                if (_List == null)
                {
                    _List = new WebControl( HtmlTextWriterTag.Ul);
                    _List.CssClass = "networks";
                }
                return _List;
            }
        }
        private WebControl _List;

        public TextBox Text
        {
            get
            {
                return _Text ?? (_Text = new TextBox() { ID = "netSearch" });
            }
        }
        private TextBox _Text;

        protected string Where { get; private set; }
        public NetCms.Users.User Account { get; private set; }

        public NetworksUpdatePanel(NetCms.Users.User account, int maxPortalsButtonsToShow) 
        {
            //Connection = connection;
            Account = account;
            Text.TextChanged += new EventHandler(Text_TextChanged);
            MaxPortalsButtonsToShow = maxPortalsButtonsToShow;
            Where = "";
        }

        void Text_TextChanged(object sender, EventArgs e)
        {
            List.Controls.Add(new LiteralControl("<li class=\"SideNetworkButton SideNetworkButton_Network\">OK</a></li>"));
        }

        protected override void OnInit(EventArgs e)
        {
 	        base.OnInit(e);
        
            this.ContentTemplateContainer.Controls.Add(List);
            FillNetworks();
        }

        public void FillNetworks()
        {
            List.Controls.Clear();

            //if (Table.Rows.Count > 0)
            if (CurrentUserNetworks.Count > 0)
            {
                if (CurrentUserNetworks.Count > MaxPortalsButtonsToShow)
                    List.Controls.Add(GetSearchControl());

                //foreach (DataRow net in Table.Select(Where).Take(MaxPortalsButtonsToShow))
                foreach (BasicNetwork net in CurrentUserNetworks.Where(x=> (Where.Length==0 || (Where.Length!=0 && x.Label.Contains(Where)))).Take(MaxPortalsButtonsToShow))
                {
                    //int netID = int.Parse(net["Network_UserNetwork"].ToString());
                    if (NetworksData.Where(x=>x.ID == net.ID).Count()>0)
                    {
                        //NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(netID);
                        if (net.CmsStatus == NetCms.Networks.NetworkCmsStates.Enabled)
                            List.Controls.Add(new LiteralControl("<li class=\"SideNetworkButton SideNetworkButton_Network\"><a alt=\"" + net.Label + " \" href=\"" + net.Paths.AbsoluteAdminRoot + "/cms/default.aspx\"><span>" + net.Label + "</span></a></li>"));
                    }
                }
            }
            if(List.Controls.Count <1)
            {
                if (Text.Text.Length > 0)
                    List.Controls.Add(new LiteralControl("<li class=\"SideNetworkButton\"><a alt=\"Nessun Portale\" href=\"#\"><span>La ricerca non ha trovato nessun Portale</span></a></li>"));
                else
                    List.Controls.Add(new LiteralControl("<li class=\"SideNetworkButton\"><a alt=\"Nessun Portale\" href=\"#\"><span>Nessun Portale Abilitato</span></a></li>"));
            }
        }

        public WebControl GetSearchControl()
        {
            WebControl search = new WebControl(HtmlTextWriterTag.Li);
            search.CssClass = "SideNetworkSearch";
            //Text.Text = "Ricerca Portali";
            search.Controls.Add(Text);

            Button button = new Button();
            button.ID = "networkSearchButton";
            button.Click += new EventHandler(button_Click);
            button.Text = "Cerca";
            search.Controls.Add(button);

            return search;
        }

        void button_Click(object sender, EventArgs e)
        {
            if (Text.Text.Length > 0)
                Where = Text.Text;
            //Where = "Label_Network LIKE '%" + Text.Text.FilterForDB() + "%'";
            else
                Where = "";

            FillNetworks();
        }
    }
}



