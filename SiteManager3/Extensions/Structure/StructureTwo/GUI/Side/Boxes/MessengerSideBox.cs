using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar.Boxes
{
    public class MessengerSideBox : SideBarBox
    {
        public override string Title { get { return "Messaggi"; } }
        public override string HtmlID { get { return "Messaggi"; } set { HtmlID = value; } }
        public override string LocalCssClass { get { return "Commands"; } }

        public override bool UseCookies { get { return true; } }

        public override Control Content
        {
            get { return BuildControl(); }
        }

        public MessengerSideBox() { }

        public DataTable MessagesTable
        {
            get
            {
                if (_MessagesTable == null)
                {
                    string sql = "";
                    sql += "SELECT * FROM MessengerMessages";
                    sql += " INNER JOIN MessengerReceivers ON id_Message = Message_Receiver";
                    sql += " INNER JOIN Users ON id_User = Author_Message";
                    sql += " INNER JOIN Anagrafica ON id_Anagrafica = Anagrafica_User";
                    sql += " WHERE Readed_Receiver = 0 AND User_Receiver = " + this.Account.ID;
                    sql += " ORDER BY Date_Message DESC";

                    _MessagesTable = this.Conn.SqlQuery(sql);
                }
                return _MessagesTable;
            }
        }
        private DataTable _MessagesTable;

        protected Control BuildControl()
        {

            string msgs = "";
            WebControl dati = new WebControl(HtmlTextWriterTag.Div);
            string datiInnerHtml = "<div class=\"MessengerInfo\" ><a title=\"Vai a Messenger\" href=\"" + Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/messenger/default.aspx\">";
            if (MessagesTable.Rows.Count > 0)
            {
                for (int i = 0; i < MessagesTable.Rows.Count && i < 5; i++)
                {
                    DataRow row = MessagesTable.Rows[i];
                    msgs += @"
                        <li class=""SideToolbarButton"">
                            <a href=""" + Configurations.Paths.AbsoluteSiteManagerRoot + @"/messenger/message_details.aspx?mess=" + row["id_Message"] + @""" >
                                <div style=""background-image:url(" + NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Email) + @")"">
                                <span>" + row["Title_Message"] + @" da " + row["Cognome_Anagrafica"] + @" " + row["Nome_Anagrafica"] + @"</span>
                                </div>
                            </a>
                        </li>";
                }

                datiInnerHtml += "<strong>Ci sono " + MessagesTable.Rows.Count + " nuovi messaggi</strong>";
            }
            else
                datiInnerHtml += "<strong>Non ci sono nuovi messaggi</strong>";

            datiInnerHtml += "</a></div>";
            msgs += @"<li class=""SideToolbarButton"">
                            <a href=""" + Configurations.Paths.AbsoluteSiteManagerRoot + @"/applications/messenger/default.aspx"" >
                                <div style=""background-image:url(" + NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Email_Go) + @")"">
                                <span>Vai a Messenger</span>
                                </div>
                            </a>
                        </li>";
            datiInnerHtml += "<div><ul>" + msgs + "</ul></div>";
            dati.Controls.Add(new LiteralControl(datiInnerHtml));
            return dati;
        }

    }

}



