using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar.Boxes
{
    public class ToolbarSideBox: SideBarBox
    {
        public override string Title { get { return "Operazioni"; } }
        public override string HtmlID { get { return "Commands"; } set { HtmlID = value; } }
        public override string LocalCssClass { get { return "Commands"; } }

        public override bool UseCookies { get { return true; } }
        
        public override bool RenderBox
        {
            get
            {
                return this.Toolbar.Buttons.Count > 0;
            }
        }

        public override Control Content
        {
            get { return this.Toolbar; }
        }

        public Toolbar Toolbar
        {
            get { return _Toolbar; }
            private set { _Toolbar = value; }
        }
        private Toolbar _Toolbar;

        public ToolbarSideBox(Toolbar toolbar) 
        {
            Toolbar = toolbar;
        }
    }
}



