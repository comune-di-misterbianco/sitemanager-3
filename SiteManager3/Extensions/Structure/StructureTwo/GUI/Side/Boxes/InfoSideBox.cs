using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar.Boxes
{
    public class InfoSideBox: SideBarBox
    {
        public override string Title { get { return "Informazioni"; } }
        public override string HtmlID { get { return "SideInfo"; } set { HtmlID = value; } }
        public override string LocalCssClass { get { return "SideInfo"; } }

        public override bool UseCookies { get { return false; } }

        public override bool RenderBox
        {
            get
            {
                return false;
            }
        }

        public override Control Content
        {
            get { return InfoBox; }
        }

        public Info.InfoBox InfoBox
        {
            get
            {
                return _InfoBox;
            }
        }
        private Info.InfoBox _InfoBox;

        public InfoSideBox(Info.InfoBox infobox) 
        {
            _InfoBox = infobox;
        }
    }
}



