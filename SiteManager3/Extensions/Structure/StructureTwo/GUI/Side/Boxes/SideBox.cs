using System;
using System.Web;
using System.Web.UI;
using NetCms.Connections;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar
{
    public abstract class SideBarBox:Collapsable
    {
        private Connection _Conn;
        public Connection Conn
        {
            get
            {
                //if (_Conn == null)
                //    _Conn = NetCms.Connections.ConnectionsManager.CommonConnection;
                //return _Conn;
                ConnectionsManager ConnManager = new ConnectionsManager();
                return ConnManager.CommonConnection;
            }
        }

        protected NetCms.Users.User Account
        {
            get
            {
                if (_Account == null)
                    _Account = NetCms.Users.AccountManager.CurrentAccount;
                return _Account;
            }
        }
        protected NetCms.Users.User _Account;

        public abstract string HtmlID { get; set; }
        public abstract string LocalCssClass { get; }

        public virtual bool RenderBox
        {
            get { return _RenderBox; }
            protected set { _RenderBox = value; }
        }
        private bool _RenderBox = true;

        public abstract Control Content
        {
            get;
        }

        private bool Collapsed
        {
            get
            {
                return false;
            }
        }

        public SideBarBox()
        {
            this.Color = Colors.Dark;
            if (HttpContext.Current.Request.Cookies[this.HtmlID + "Box_StartClosed"] != null)
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies[this.HtmlID + "Box_StartClosed"];
                bool startCollapsed = cookie.Value == "true";
                StartCollapsed = bool.Parse(cookie.Value);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.ID = HtmlID;
            this.CssClass = ("SideBlock " + LocalCssClass).Trim();
           
            this.AppendControl(Content);
        }
    }
}



