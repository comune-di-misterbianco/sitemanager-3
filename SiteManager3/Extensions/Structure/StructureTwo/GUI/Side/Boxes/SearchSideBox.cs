using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.GUI.SideBar.Boxes
{
    public class SearchSideBox : SideBarBox
    {
        public override string Title { get { return "Cerca"; } }
        public override string HtmlID { get { return "Search"; } set { HtmlID = value; } }
        public override string LocalCssClass { get { return ""; } }

        public override bool UseCookies { get { return false; } }
        public override bool RenderBox
        {
            get
            {
                return base.RenderBox;
            }
            protected set
            {
                base.RenderBox = value;
            }
        }
        public override Control Content
        {
            get { return BuildControl(); }
        }

        public SearchSideBox() { }

        protected Control BuildControl()
        {
            WebControl Content = new WebControl(HtmlTextWriterTag.Div);
            Content.ID = HtmlID + "_Content";
            Content.CssClass = "content";
            Content.Controls.Add(SearchCriteria);
            return Content;
        }

        #region Search Criteria

        private HtmlGenericControl SearchCriteria
        {
            get
            {
                HtmlGenericControl dati = new HtmlGenericControl("div");

                NetUtility.RequestVariable Titolo_Search = new NetUtility.RequestVariable("Titolo_Search", NetUtility.RequestVariable.RequestType.Form);
                NetUtility.RequestVariable Date_Search = new NetUtility.RequestVariable("Date_Search", NetUtility.RequestVariable.RequestType.Form);
                NetUtility.RequestVariable DateType_Search = new NetUtility.RequestVariable("DateType_Search", NetUtility.RequestVariable.RequestType.Form);
                NetUtility.RequestVariable OrderBy_Search = new NetUtility.RequestVariable("OrderBy_Search", NetUtility.RequestVariable.RequestType.Form);
                NetUtility.RequestVariable Tipo_Search = new NetUtility.RequestVariable("Tipo_Search", NetUtility.RequestVariable.RequestType.Form);
                dati.InnerHtml += "";

                if (Titolo_Search.IsValidString)
                {
                    dati.InnerHtml += " che contengono il testo '" + Titolo_Search + "' nel titolo o nella descrizione";
                }
                if (Tipo_Search.IsValidInteger)
                {
                    //DataTable table = this.Connection.SqlQuery("SELECT * FROM Applicazioni");
                    //DataRow[] row = table.Select("id_Applicazione = " + Tipo_Search.IntValue);
                    //if (row.Length > 0)
                    //{
                    //    if (dati.InnerHtml.Length > 0)
                    //        dati.InnerHtml += ",";
                    //    dati.InnerHtml += " di tipo " + row[0]["SystemName_Applicazione"].ToString() + "";
                    //}

                    NetCms.Structure.Applications.Application dbApp = NetCms.Structure.Applications.ApplicationBusinessLogic.GetApplicationById(Tipo_Search.IntValue);
                    if (dbApp != null)
                    {
                        if (dati.InnerHtml.Length > 0)
                            dati.InnerHtml += ",";
                        dati.InnerHtml += " di tipo " + dbApp.SystemName + "";
                    }
                }
                if (Date_Search.IsValid(NetUtility.RequestVariable.VariableType.Date) && DateType_Search.IsValidInteger)
                {
                    if (dati.InnerHtml.Length > 0)
                        dati.InnerHtml += ",";
                    string Operator = "";
                    if (DateType_Search.IntValue == 1)
                        Operator = " precedente al";
                    if (DateType_Search.IntValue == 2)
                        Operator = " successiva al";
                    dati.InnerHtml += " la cui data di creazione sia " + Operator + " " + Date_Search.DateTimeValue.Day + "/" + Date_Search.DateTimeValue.Month + "/" + Date_Search.DateTimeValue.Year;
                }

                if (OrderBy_Search.IsValidInteger)
                {
                    string OrderBy = "";
                    switch (OrderBy_Search.IntValue)
                    {
                        case 0:
                            OrderBy += " Nome";
                            break;
                        case 1:
                            OrderBy += " Percorso";
                            break;
                        case 2:
                            OrderBy += " Tipo";
                            break;
                        case 3:
                            OrderBy += " Data crescente";
                            break;
                        case 4:
                            OrderBy += " Data decrescente";
                            break;
                        default:
                            OrderBy += " Nome";
                            break;
                    }
                    if (dati.InnerHtml.Length > 0)
                        dati.InnerHtml += ",";
                    dati.InnerHtml += " ordinati per" + OrderBy;
                }

                if (dati.InnerHtml.Length > 0)
                    dati.InnerHtml = "<p>Documenti" + dati.InnerHtml + "</p>";

                else
                    dati.InnerHtml = "<p>Nessun Criterio Selezionato</p>";
                
                return dati;
            }
        }

        #endregion
    }
}



