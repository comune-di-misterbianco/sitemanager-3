﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace NetCms.GUI.Info
{
    /// <summary>
    /// La classe GenericToolbar implementa la gestione della Toolbar generiche da utilizzare nella pagina dell'applicazione.
    /// </summary>
    public class CmsInfoBox : InfoBox
    {
        /// <summary>
        /// Inizializza una nuova istanza di NetCms.GUI.GenericInfoBox 
        /// </summary>
        /// <example>
        /// L'esempio descrive come istanziare un oggetto NetCms.GUI.InfoBox
        /// <code>
        /// InfoBox myGenericInfoBox = new GenericInfoBox();
        /// </code>
        /// </example>
        public CmsInfoBox()
        {
            Infos.ItemAdded += new Info.InfoElementsCollection.EventHandler(Infos_ItemAdded);
        }


        void Infos_ItemAdded(object sender, EventArgs e, object value)
        {
            if (value is Info.InfoElement)
            {
                Info.InfoElement info = (Info.InfoElement)value;
                NetCms.GUI.PopupBox.AddMessage(info.Text);
            }
        }

        /// <summary>
        /// Ritorna il nome tag html, relativo all'oggetto NetCms.GUI.GenericToolbar
        /// </summary>
        protected override string TagName
        {
            get
            {
                return "DIV";
            }
        }

        /// <summary>
        /// Renering html dell'oggetto NetCms.GUI.GenericInfoBox
        /// </summary>
        /// <returns>
        /// Ritorna il controllo HtmlGenericControl del NetCms.GUI.GenericInfoBox
        /// </returns>
        protected override HtmlGenericControl RenderRow(InfoElement element)
        {
            return null;
        }

        protected override void OnPreRender(EventArgs e)
        {
            
        }
    }
}
