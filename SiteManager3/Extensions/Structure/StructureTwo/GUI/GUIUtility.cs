﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;

namespace NetCms.GUI
{
    public static class GuiUtility
    {
        public static NetCms.GUI.Css.ImagesIndexer ImagesIndexer
        {
            get { return (NetCms.GUI.Css.ImagesIndexer)HttpContext.Current.Items["PageImagesIndexer"]; }
        }

        public static NetCms.GUI.Icons.IconsIndexer IconsIndexer
        {
            get { return (NetCms.GUI.Icons.IconsIndexer)HttpContext.Current.Items["PageIconsIndexer"]; }
        }

        public static void RestartApplication()
        {
            System.Web.HttpRuntime.UnloadAppDomain();

            NetCms.GUI.PopupBox.AddSessionMessage("<p>Gli aggiornamenti effettuati, richiedono il riavvio del sistema. Sarà quindi necessario rieffettuare il login.</p>", PostBackMessagesType.Notify);
        }
    }
}
