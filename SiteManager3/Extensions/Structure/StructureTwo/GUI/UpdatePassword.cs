using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Reflection;
using NetService.Utility.ValidatedFields;
using System.Web.Security;
using NetCms.Users;

/// <summary>
/// Summary description for FilesDocument
/// </summary>
namespace NetCms.GUI
{
    public class UpdatePassword:SmPageVertical
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                return null;
            }
        }

        public NetCms.Users.UsersChangePasswordControl ChangePasswordControl
        {
            get
            {
                return _ChangePasswordControl ?? (_ChangePasswordControl = new NetCms.Users.UsersChangePasswordControl(Account));
            }
        }
        private NetCms.Users.UsersChangePasswordControl _ChangePasswordControl;

        public UpdatePassword()
        {
            ChangePasswordControl.ChangePasswordForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
            //ChangePasswordControl.OnAfterPostback += new Users.UsersChangePasswordControl.AfterPostback(ChangePasswordControl_OnAfterPostback);
        }

        //void ChangePasswordControl_OnAfterPostback(bool success)
        //{
        //    if(success)
        //        NetCms.GUI.PopupBox.AddSessionMessage("La password � stata aggiornata con successo. Sar� necessario reimpostarla tra 90 giorni.", NetCms.GUI.PostBackMessagesType.Success, "default.aspx");
        //    else
        //        NetCms.GUI.PopupBox.AddSessionMessage("Impossibile effettuate l'aggiornamento delle credenziali.", NetCms.GUI.PostBackMessagesType.Notify);
        //}

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (ChangePasswordControl.ChangePasswordForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                string password = ChangePasswordControl.ChangePasswordForm.Fields.Find(x => x.Key == "nuovapassword").PostBackValue;
                string reqPwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();

                User current = AccountManager.CurrentAccount;

                if (current.Password == reqPwd || current.NuovaPassword == reqPwd)
                {
                    NetCms.GUI.PopupBox.AddMessage("E' necessario inserire una password diversa da quella correntemente impostata.", PostBackMessagesType.Notify);
                }
                else
                {
                    try
                    {
                        current.NuovaPassword = null;
                        current.Password = reqPwd;
                        current.LastUpdate = DateTime.Now;

                        UsersBusinessLogic.SaveOrUpdateUser(current);
                        NetCms.GUI.PopupBox.AddSessionMessage("La password � stata aggiornata con successo. Sar� necessario reimpostarla tra 90 giorni.", PostBackMessagesType.Success, "default.aspx");

                    }
                    catch (Exception ex)
                    {
                        NetCms.GUI.PopupBox.AddMessage("Impossibile effettuate l'aggiornamento delle credenziali.", PostBackMessagesType.Error);

                    }
                }
            }
        }

        public override string PageTitle
        {
            get { return "Aggiornamento Credenziali di Accesso"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Key; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_StartPage"; }
        }
        protected override WebControl RootControl
        {
            get
            {
                WebControl ctr = new WebControl(HtmlTextWriterTag.Div);

                ctr.Controls.Add(ChangePasswordControl);
                //ctr.Controls.Add(new NetCms.Structure.Grants.UsersGrants.UsersModPasswordView(PageData.CurrentReference).getView());

                return ctr;
            }
        }
    }
}
