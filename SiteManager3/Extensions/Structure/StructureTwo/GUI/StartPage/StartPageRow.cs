using System;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for FilesDocument
/// </summary>
namespace NetCms.GUI.Startpage
{
    public class StartpageRow
    {
        private StartpageBoxCollection boxes;
        public StartpageBoxCollection Boxes
        {
            get { return boxes; }
        }
	
        private string _Key;
        public string Key
        {
            get
            {
                if (_Key == null)
                {
                    _Key = "SiteManagerGUISheet_" + this.GetHashCode();
                }
                return _Key;
            }
        }

        private string _Title;
        public string Title
        {
            get
            {
                return _Title;
            }
        }

        public StartpageRow(string key, string title)
        {
            _Key = key;
            _Title = title;
            boxes = new StartpageBoxCollection();
        }
        
        private HtmlGenericControl control;
        public HtmlGenericControl Control
        {
            get
            {
                if (control == null)
                {
                    control = GetControl();
                }
                return control;
            }
        }
        protected virtual HtmlGenericControl GetControl()
        {
            G2Core.XhtmlControls.Div row = new G2Core.XhtmlControls.Div();
            
            G2Core.XhtmlControls.Div rowTitle = new G2Core.XhtmlControls.Div();
            rowTitle.Class = "StartpageRowTitle";
            rowTitle.InnerHtml = this.Title;
            row.Controls.Add(rowTitle);

            G2Core.XhtmlControls.Div rowContainer = new  G2Core.XhtmlControls.Div();
            rowContainer.Class = "StartpageRowContainer";
            row.Controls.Add(rowContainer);

            foreach (StartpageBox box in this.Boxes)
                rowContainer.Controls.Add(box.Control);

            row.Class = "StartpageRow StartpageRow" + rowContainer.Controls.Count + "Boxes";


            G2Core.XhtmlControls.Div rowClear = new G2Core.XhtmlControls.Div();
            rowClear.Class = "StartpageClear";
            rowContainer.Controls.Add(rowClear);

            return row;
        }
    }
}
