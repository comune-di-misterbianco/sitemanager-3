using System;
using System.Collections;
/// <summary>
/// Summary description for xNodeCollection
/// </summary>

namespace NetCms.GUI.Startpage
{
    public class StartpageRowCollection : IEnumerable
    {
        private G2Collection coll;

        public int Count { get { return coll.Count; } }

        public StartpageRowCollection()
        {
            coll = new G2Collection();
        }

        public void Add(StartpageRow row)
        {
            coll.Add(row.Key, row);
        }

        public StartpageRow this[int i]
        {
            get
            {
                StartpageRow value = (StartpageRow)coll[coll.Keys[i]];
                return value;
            }
        }
        public StartpageRow this[string key]
        {
            get
            {
                StartpageRow field = (StartpageRow)coll[key];
                return field;
            }
        }

        public bool Contains(string key)
        {
            return this[key] != null;
        }
    #region Enumerator

    public IEnumerator GetEnumerator()
    {
        return new CollectionEnumerator(this);
    }

    private class CollectionEnumerator : IEnumerator
    {
        private int CurentPos = -1;
        private StartpageRowCollection Collection;
        public CollectionEnumerator(StartpageRowCollection coll)
        {
            Collection = coll;
        }
        public object Current
        {
            get
            {
                return Collection[CurentPos];
            }
        }
        public bool MoveNext()
        {
            if (CurentPos < Collection.Count - 1)
            {
                CurentPos++;
                return true;
            }
            else
                return false;
        }
        public void Reset()
        {
            CurentPos = -1;
        }
    }
    #endregion
    }
}