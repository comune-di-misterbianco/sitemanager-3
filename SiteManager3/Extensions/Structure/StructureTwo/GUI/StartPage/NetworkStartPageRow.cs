using System;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for FilesDocument
/// </summary>
namespace NetCms.GUI.Startpage
{
    public class NetworkStartpageRow : StartpageRow
    {
        public string Href
        {
            get { return _Href; }
            private set { _Href = value; }
        }
        private string _Href;

			
        public NetworkStartpageRow(string key, string title,string href):base(key,title)
        {
            Href = href;
        }
        
        protected override HtmlGenericControl GetControl()
        {
            HtmlGenericControl row = new HtmlGenericControl("tr");

            HtmlGenericControl rowTitle = new HtmlGenericControl("td");
            rowTitle.Attributes.Add("class", "StartpageTableRowTitle");
            rowTitle.InnerHtml = "<a href=\"" + this.Href + "\">";
            rowTitle.InnerHtml +=this.Title;
            rowTitle.InnerHtml += "</a>";
            bool first = true;
            foreach (StartpageBox box in this.Boxes)
            {
                HtmlGenericControl boxControl = box.Control;
                row.Controls.Add(boxControl);
                
                if (first) row.Controls.Add(rowTitle);
                first = false;
            }
            row.Attributes.Add("class", "StartpageTableRow StartpageTableRow" + this.Boxes.Count + "Boxes");

            return row;
        }
    }
}
