using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Reflection;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Vertical;
using System.Collections.Generic;
using NetCms.Vertical.BusinessLogic;
using System.Linq;

/// <summary>
/// Summary description for FilesDocument
/// </summary>
namespace NetCms.GUI.Startpage
{
    public class Startpage : SmPageVertical
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.HomeSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        private StartpageRowCollection _NetworksRows;
        public StartpageRowCollection NetworksRows
        {
            get
            {
                if (_NetworksRows == null)
                {
                    _NetworksRows = new StartpageRowCollection();
                    Networks.NetworksData ntdata = new NetCms.Networks.NetworksData();
                    foreach (DataRow net in this.Conn.SqlQuery("SELECT * FROM users_networks WHERE User_UserNetwork = " + Account.ID).Rows)
                    {
                        int netID = int.Parse(net["Network_UserNetwork"].ToString());
                        if (ntdata.Contains(netID))
                        {
                            NetCms.Structure.WebFS.BasicStructureNetwork network = new NetCms.Structure.WebFS.BasicStructureNetwork(netID);
                            NetworkStartpageRow row = new NetworkStartpageRow(network.SystemName, network.Label, network.Paths.AbsoluteAdminRoot + "/cms/default.aspx");
                            row.Boxes.Add(new NetworkStartpageBox("Admin" + network.SystemName + " AdminIcon", "Gestione Contenuti " + network.Label, network.Paths.AbsoluteAdminRoot + "/cms/default.aspx", "Permette di gestire i contenuti del sito " + network.Label));
                            row.Boxes.Add(new NetworkStartpageBox("Front" + network.SystemName + " FrontIcon", "Frontend " + network.Label, network.Paths.AbsoluteFrontRoot, "Vai al frontend del sito " + network.Label));
                            row.Boxes.Add(new NetworkStartpageBox("NewsletterIcon" + network.SystemName + " NewsletterIcon", "Newsletter " + network.Label, network.Paths.AbsoluteAdminRoot + "/applications/newsletter/default.aspx", "Vai alla gestione della Newsletter del sito " + network.Label));
                            _NetworksRows.Add(row);
                        }
                    }
                }

                return _NetworksRows;
            }
        }

        public const int PermissionsHomeGroupID = 1;
        public const int ApplicationsHomeGroupID = 2;
        public const int ConfigsHomeGroupID = 3;

        private StartpageRow _PermissionsRow;
        public StartpageRow PermissionsRow
        {
            get
            {
                if (_PermissionsRow == null)
                    _PermissionsRow = new StartpageRow("Permissions", "Amministrazione Utenti");

                return _PermissionsRow;
            }
        }

        public List<VerticalApplication> ExternalAppz
        {
            get
            {
                if (_ExternalAppz == null)
                    _ExternalAppz = VerticalAppBusinessLogic.FindAllVerticalApplications().Where(x => x.Enabled && !x.NetworkDependant && x.AccessModeInt != 1).ToList();//Connection.SqlQuery("SELECT * FROM ExternalAppz WHERE Enabled_ExternalApp = 1 AND NetworkDependant_ExternalApp = 0 AND AccessoMode_ExternalApp <> 1");
                return _ExternalAppz;
            }
        }
        private List<VerticalApplication> _ExternalAppz;

        private StartpageRow _ApplicationsRow;
        public StartpageRow ApplicationsRow
        {
            get
            {
                if (_ApplicationsRow == null)
                    _ApplicationsRow = new StartpageRow("Applications", "Applicazioni");

                return _ApplicationsRow;
            }
        }

        private StartpageRow _ConfigsRow;
        public StartpageRow ConfigsRow
        {
            get
            {
                if (_ConfigsRow == null)
                    _ConfigsRow = new StartpageRow("Configs", "Amministrazione Avanzata");

                return _ConfigsRow;
            }
        }

        private StartpageRow _DevelopRow;
        public StartpageRow DevelopRow
        {
            get
            {
                if (_DevelopRow == null)
                    _DevelopRow = new StartpageRow("Sviluppo", "Applicazioni in fase di Sviluppo");

                return _DevelopRow;
            }
        }

        public void InitAppsBox()
        {
            //Codice aggiunto per non visualizzare le app presenti dentro una VerticalApplicationPackage
            List<int> appContained = new List<int>();
            foreach (var app in VerticalApplicationsPool.ActiveAssemblies)
                if (app is VerticalApplicationPackage)
                {
                    foreach (int id in ((VerticalApplicationPackage)app).ContainedApplicationsIDs)
                        if (!appContained.Contains(id))
                            appContained.Add(id);
                }


            //foreach (DataRow applicationData in ExternalAppz.Rows)
            foreach(VerticalApplication app in ExternalAppz)
            {
                //ExternalApplications.ExternalApplication app = new ExternalApplications.ExternalApplication(int.Parse(applicationData["id_ExternalApp"].ToString()));

                //Non vengono mostrate le app inglobate dentro una vapp package
                if (app.Grant && !appContained.Contains(app.ID))
                {
                    //Aggiunto per consentire la ridefinizione della StartPageBox all'interno di ogni applicazione, qualora sia necessario cambiare l'URL della pagina iniziale
                    StartpageBox pageBox = null;
                    //Se trova l'applicazione negli assemblies attivi usa lo StartPageBox ridefinito, altrimenti quello standard
                    //Se non � ridefinito usa quello standard
                    if (VerticalApplicationsPool.ActiveAssemblies.Contains(app.ID))
                        pageBox = VerticalApplicationsPool.ActiveAssemblies[app.ID.ToString()].StartpageBox;
                    else
                        pageBox = app.StartpageBox;
                    
                    switch (app.HomepageGroup)
                    {
                        case 1: this.PermissionsRow.Boxes.Add(pageBox); break;
                        case 2: this.ApplicationsRow.Boxes.Add(pageBox); break;
                        case 3: this.ConfigsRow.Boxes.Add(pageBox); break;
                        default: this.DevelopRow.Boxes.Add(pageBox); break;
                    }
                }
            }
        }


        public Startpage()
        {
            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo all'homepage del CMS", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            InitAppsBox();
        }

        public override string PageTitle
        {
            get { return "Pagina Iniziale"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.House; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_StartPage"; }
        }
        protected WebControl Panels
        {
            get
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                value.ID = "slider";

                WebControl buttons = new WebControl(HtmlTextWriterTag.Ul);
                buttons.CssClass = "navigation";
                value.Controls.Add(buttons);

                string activitiesBadgeClass = "";

                int numActivitiesUnread = Activities.ActivitiesTable.Count(x => !x.Letta);

                if (numActivitiesUnread > 0 && numActivitiesUnread < 10) activitiesBadgeClass += "one";
                else if (numActivitiesUnread < 100) activitiesBadgeClass += "two";
                else activitiesBadgeClass += "tree";
             
                buttons.Controls.Add(new LiteralControl(@"
            <li class=""applications""><a href=""#applications""><span>Applicazioni</span></a></li> 
            <li class=""activity""><a href=""#activity"">" + (numActivitiesUnread > 0 ? @"<span class=""count count-" + activitiesBadgeClass + @""">" + numActivitiesUnread + "</span>" : "") + @"<span>Attivit�</span></a></li> 
            <li class=""favorites""><a href=""#favorites""><span>Preferiti</span></a></li>
"));

                if (UrpIsInstalled && UserHavePortalEnable)                    
                buttons.Controls.Add(new LiteralControl("<li class=\"urp-online\"><a href=\"#urponline\"><span>Urp On-line</span></a></li>"));

                WebControl scroll = new WebControl(HtmlTextWriterTag.Div);
                scroll.CssClass = "scroll";

                WebControl scrollContainer = new WebControl(HtmlTextWriterTag.Div);
                scrollContainer.CssClass = "scrollContainer";

                value.Controls.Add(scroll);
                scroll.Controls.Add(scrollContainer);

                scrollContainer.Controls.Add(MainTab);
                scrollContainer.Controls.Add(ActivitiesTab);
                scrollContainer.Controls.Add(FavoritesTab);

                if (UrpIsInstalled && UserHavePortalEnable)
                    scrollContainer.Controls.Add(UrpTab);


                return value;
            }
        }

        protected override WebControl RootControl
        {
            get
            {
                if (_GfxContainer == null)
                {
                    _GfxContainer = new WebControl(HtmlTextWriterTag.Div);
                    _GfxContainer.CssClass = "gfxContainer";

                    WebControl gfxBase = new WebControl(HtmlTextWriterTag.Div);
                    gfxBase.ID = "gfxBase";
                    WebControl gfxLeft = new WebControl(HtmlTextWriterTag.Div);
                    gfxLeft.ID = "gfxLeft";
                    WebControl gfxRight = new WebControl(HtmlTextWriterTag.Div);
                    gfxRight.ID = "gfxRight";

                    _GfxContainer.Controls.Add(gfxBase);
                    gfxBase.Controls.Add(gfxLeft);
                    gfxLeft.Controls.Add(gfxRight);


                    WebControl gfxBase_footer = new WebControl(HtmlTextWriterTag.Div);
                    gfxBase_footer.ID = "gfxBase_footer";
                    WebControl gfxLeft_footer = new WebControl(HtmlTextWriterTag.Div);
                    gfxLeft_footer.ID = "gfxLeft_footer";
                    WebControl gfxRight_footer = new WebControl(HtmlTextWriterTag.Div);
                    gfxRight_footer.ID = "gfxRight_footer";

                    gfxRight.Controls.Add(gfxBase_footer);
                    gfxBase_footer.Controls.Add(gfxLeft_footer);
                    gfxLeft_footer.Controls.Add(gfxRight_footer);


                    gfxRight_footer.Controls.Add(Panels);
                }
                return _GfxContainer;
            }
        }
        private WebControl _GfxContainer;

        public WebControl MainTab
        {
            get
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                //WebControl networks = new WebControl(HtmlTextWriterTag.Table);
                value.CssClass = "panel applications";
                value.ID = "applications";

                //networks.CssClass = "networks networks_" + "column";//+ (this.NetworksRows.Count > 2 || Configurations.Generics.DebugMode == NetCms.Configurations.Generics.DebugModeStates.Advanced ? "column" : "fullsizerow");

                /*foreach (StartpageRow row in this.NetworksRows)
                    networks.Controls.Add(row.Control);
                value.Controls.Add(networks);
*/
                if (PermissionsRow.Boxes.Count > 0) value.Controls.Add(this.PermissionsRow.Control);
                if (ApplicationsRow.Boxes.Count > 0) value.Controls.Add(this.ApplicationsRow.Control);
                if (ConfigsRow.Boxes.Count > 0) value.Controls.Add(this.ConfigsRow.Control);
                if (NetCms.Configurations.Debug.GenericEnabled
                    && this.DevelopRow.Boxes.Count > 0) value.Controls.Add(this.DevelopRow.Control);

                return value;
            }
        }

        public NetCms.Users.Activities.Activities Activities
        {
            get
            {
                if (_Activities == null)
                    _Activities = new NetCms.Users.Activities.Activities(this.Account.Profile.ID.ToString());
                return _Activities;
            }
        }
        private NetCms.Users.Activities.Activities _Activities;

        public WebControl ActivitiesTab
        {
            get
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                value.CssClass = "panel activity";
                value.ID = "activity";

                /*WebControl fff = new NetCms.Requests.ReviewRequests().Revisioni();
                if (fff != null) value.Controls.Add(fff);
               
                HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset("Elenco Attivit� Pendenti", new HtmlGenericControl("div"));
                value.Controls.Add(set); */
                //WebControl title = new WebControl(HtmlTextWriterTag.Div);
                //title.CssClass = "title";
                //value.Controls.Add(title);
                //title.Controls.Add(new LiteralControl("Elenco Attivit� Pendenti"));

                value.Controls.Add(Activities);

                return value;
            }
        }

        public WebControl FavoritesTab
        {
            get
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                value.CssClass = "panel favorites";
                value.ID = "favorites";

                WebControl fff = FolderBusinessLogic.AllBookmarks(Account);
                if (fff != null) value.Controls.Add(fff);

                return value;
            }
        }

        public WebControl UrpTab 
        {
            get 
            {
                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                value.CssClass = "panel urponline";
                value.ID = "urponline";

                Assembly assembly = NetCms.Vertical.VerticalApplicationsPool.GetByID(2307).Assembly;
                Type ItemsControl = assembly.GetType("URPOnline.Controls.ItemsControl");               

                //var networks = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.Select(x => x.Value);
                var networks = NetCms.Users.AccountManager.CurrentAccount.AssociatedNetworks;
                foreach (Users.UserNetwork network in networks)
                {
                    Object[] args = { network.Network.ID };
                    WebControl itemsControl = Activator.CreateInstance(ItemsControl,args) as WebControl;
                    value.Controls.Add(itemsControl);
                }

                return value;
            }
        }

        protected override void AddScriptsToHeader()
        {
            base.AddScriptsToHeader();

            string scriptLink = ""
            + @"<script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_ScrollTo) + @""" ></script><!--xTreeView.js-->" + System.Environment.NewLine
            + @"<script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_LocalScroll) + @""" ></script><!--Homepage.js-->" + System.Environment.NewLine
            + @"<script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_SerialScroll) + @""" ></script><!--Homepage.js-->" + System.Environment.NewLine
            + @"<script type=""text/javascript"" src=""" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.CodaSlider) + @""" ></script><!--Thickbox.js-->" + System.Environment.NewLine;
            
            this.Page.Header.Controls.Add(new LiteralControl(scriptLink));
        }

        protected override void AddCssToHeader()
        {
            base.AddCssToHeader();

            string customCss = Configurations.XmlConfig.GetAttribute("/Portal/configs/admin-gui", "css");
            string cssLink = @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.AbsoluteSiteManagerRoot + @"/css/" + customCss + @"/startpage.css"" />" + System.Environment.NewLine;
            
            this.Page.Header.Controls.Add(new LiteralControl(cssLink));
        }

        private bool UrpIsInstalled
        {
            get
            {
                return NetCms.Vertical.VerticalApplicationsPool.ActiveAssemblies.Contains(2307);
            }
        }

        private bool UserHavePortalEnable 
        {
            get{
                return NetCms.Users.AccountManager.CurrentAccount.AssociatedNetworks.Count() > 0;                
            }        
        }

    
    }
}
