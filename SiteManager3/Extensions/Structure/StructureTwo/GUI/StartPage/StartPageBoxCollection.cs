using System;
using System.Collections;
/// <summary>
/// Summary description for xNodeCollection
/// </summary>

namespace NetCms.GUI.Startpage
{
    public class StartpageBoxCollection : IEnumerable
    {
        private G2Collection coll;

        public int Count { get { return coll.Count; } }

        public StartpageBoxCollection()
        {
            coll = new G2Collection();
        }

        public void Add(StartpageBox box)
        {
            coll.Add(box.Key, box);
        }

        public StartpageBox this[int i]
        {
            get
            {
                StartpageBox value = (StartpageBox)coll[coll.Keys[i]];
                return value;
            }
        }
        public StartpageBox this[string key]
        {
            get
            {
                StartpageBox field = (StartpageBox)coll[key];
                return field;
            }
        }

        public bool Contains(string key)
        {
            return this[key] != null;
        }
    #region Enumerator

    public IEnumerator GetEnumerator()
    {
        return new CollectionEnumerator(this);
    }

    private class CollectionEnumerator : IEnumerator
    {
        private int CurentPos = -1;
        private StartpageBoxCollection Collection;
        public CollectionEnumerator(StartpageBoxCollection coll)
        {
            Collection = coll;
        }
        public object Current
        {
            get
            {
                return Collection[CurentPos];
            }
        }
        public bool MoveNext()
        {
            if (CurentPos < Collection.Count - 1)
            {
                CurentPos++;
                return true;
            }
            else
                return false;
        }
        public void Reset()
        {
            CurentPos = -1;
        }
    }
    #endregion
    }
}