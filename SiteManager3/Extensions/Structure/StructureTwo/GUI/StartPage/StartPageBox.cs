using System;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for FilesDocument
/// </summary>
namespace NetCms.GUI.Startpage
{
    public class StartpageBox
    {
        private string _Key;
        public string Key
        {
            get
            {
                return _Key;
            }
        }

        private string _Desc;
        public string Desc
        {
            get
            {
                return _Desc;
            }
        }

        private string _Href;
        public string Href
        {
            get
            {
                return _Href;
            }
        }

        private string _Label;
        public string Label
        {
            get
            {
                return _Label;
            }
        }

        public StartpageBox(string key,string label,string href)
            : this(key, label, href,null)
        {
        }
        public StartpageBox(string key, string label, string href, string desc)
        {
            _Key = key;
            _Desc = desc;
            _Label = label;
            _Href = href;
        }


        private HtmlGenericControl control;
        public HtmlGenericControl Control
        {
            get
            {
                if (control == null)
                {
                    control = GetControl();
                }
                return control;
            }
        }
        protected virtual  HtmlGenericControl GetControl()
        {
            G2Core.XhtmlControls.Div box = new G2Core.XhtmlControls.Div();

            box.Class = "StartpageBox StartpageBox" + this.Key;
            box.InnerHtml = "<a href=\"" + this.Href + "\"><span class=\"StartpageBoxIcon\"></span>";
            box.InnerHtml += "<span>" + this.Label + "</span>";
            if(this.Desc!=null && this.Desc.Length>0)
                box.InnerHtml += "<br/><em>" + this.Desc + "</em>";
            box.InnerHtml += "</a>";

            return box;
        }
    }
}
