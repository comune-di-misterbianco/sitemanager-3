using System;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for FilesDocument
/// </summary>
namespace NetCms.GUI.Startpage
{
    public class NetworkStartpageBox : StartpageBox
    {
        public NetworkStartpageBox(string key,string label,string href)
            : base(key, label, href,null)
        {
        }
        public NetworkStartpageBox(string key, string label, string href, string desc)
            : base(key, label, href, desc)
        {       }

        protected override HtmlGenericControl GetControl()
        {
            HtmlGenericControl td = new HtmlGenericControl("td");

            td.Attributes.Add("class","StartpageCell StartpageCell" + this.Key);
            td.InnerHtml = "<a href=\"" + this.Href + "\"><span class=\"StartpageCellIcon\"></span>";
            td.InnerHtml += "<span>" + this.Label + "</span>";
            td.InnerHtml += "</a>";

            return td;
        }
    }
}
