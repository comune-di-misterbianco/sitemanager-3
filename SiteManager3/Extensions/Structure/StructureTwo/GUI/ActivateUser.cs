using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Reflection;

/// <summary>
/// Summary description for FilesDocument
/// </summary>
namespace NetCms.GUI
{
    public class ActivateUserPage:SmPageVertical
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                return null;
            }
        }

        public ActivateUserPage()
        {                        
        }

        public override string PageTitle
        {
            get { return "Attivazione Utenza"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.User; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_StartPage"; }
        }
        protected override WebControl RootControl
        {
            get
            {
                WebControl ctr = new WebControl(HtmlTextWriterTag.Div);

                ctr.Controls.Add(new NetCms.Users.ActivateUsersControl(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot));

                return ctr;
            }
        }
    }
}
