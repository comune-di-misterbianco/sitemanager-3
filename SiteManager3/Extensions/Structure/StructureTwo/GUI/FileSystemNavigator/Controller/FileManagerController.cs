﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;
using NetCms.Structure.GUI.FileSystemNavigator.Model;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS;
using System.Web.Http;
using NetCms.Users;
using NetCms.Networks.Grants;
using NetCms.Vertical.BusinessLogic;
using NetCms.Structure.Applications;
using System.Linq.Expressions;

namespace NetCms.Structure.GUI.FileSystemNavigator.Controller
{
    public class FileManagerController : ApiController
    {
        //public IEnumerable<FMDocumentModel> GetDocumentsByFolder(int folderId, int userId)
        //{
        //    using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
        //    {
        //        StructureFolder folder = FolderBusinessLogic.GetById(sess, folderId);
        //        Users.User utente = UsersBusinessLogic.GetById(userId, sess);
                
        //        var docs = (from doc in folder.Documents
        //                    let document = DocumentBusinessLogic.GetById(doc.ID, sess)
        //                    let criteria = new DocumentGrants(document,utente.Profile)
        //                    where criteria[NetCms.Grants.CriteriaTypes.Show].Allowed
        //                    select new FMDocumentModel()
        //                    {
        //                        ID = document.ID,
        //                        Nome = document.Nome,
        //                        DataCreazione = document.Data.ToShortDateString(),
        //                        DataModifica = document.Modify.ToShortDateString(),
        //                        ClasseIcona = document.Icon,
        //                        NomeFisico = document.PhysicalName
        //                    }).ToList();

        //        return docs;
        //    }
        //}

        public static NetCms.Vertical.Configuration.Configuration Configuration
        {
            get
            {
                return _Configuration ?? (_Configuration = new NetCms.Vertical.Configuration.Configuration("filemanager"));
            }
        }
        private static NetCms.Vertical.Configuration.Configuration _Configuration;

      

        #region Old_Api
        
        //public DocumentGridObject GetDocumentsByFolder(int folderId, int userId)
        //{
        //    using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
        //    {
        //        StructureFolder folder = FolderBusinessLogic.GetById(sess, folderId);
        //        Users.User utente = UsersBusinessLogic.GetById(userId, sess);

        //        var docs = (from doc in folder.Documents
        //                    let document = DocumentBusinessLogic.GetById(doc.ID, sess)
        //                    let criteria = new DocumentGrants(document, utente.Profile)
        //                    let application = ApplicationsPool.ActiveAssemblies[document.Application.ToString()]
        //                    where criteria[NetCms.Grants.CriteriaTypes.Show].Allowed
        //                    select new DocumentRow()
        //                    {
        //                        id = document.ID.ToString(),
        //                        cell = new List<string>() { document.Nome, "File " + ((document.Extension.Length > 0)? document.Extension : application.LabelSingolare), document.Modify.ToString(), document.FrontendLink }

        //                    }).ToList();

        //        DocumentGridObject gridObj = new DocumentGridObject();
        //        gridObj.page = 1;
        //        gridObj.total = docs.Count;
        //        gridObj.rows = docs;

        //        return gridObj;
        //    }
        //}

        //public DocumentGridObject GetDocumentsByFolder(int folderId, int userId,string name, string order)
        //{
        //    using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
        //    {
        //        StructureFolder folder = FolderBusinessLogic.GetById(sess, folderId);
        //        Users.User utente = UsersBusinessLogic.GetById(userId, sess);

        //        var documents = from doc in folder.Documents
        //                        let document = DocumentBusinessLogic.GetById(doc.ID, sess)
        //                        let criteria = new DocumentGrants(document, utente.Profile)
        //                        where criteria[NetCms.Grants.CriteriaTypes.Show].Allowed
        //                        select document;

        //        var docsOrdered = G2Core.Common.Utility.OrderBy(documents.AsQueryable(), name, order == "asc");

        //        var docs = (from doc in docsOrdered
        //                    let application = ApplicationsPool.ActiveAssemblies[doc.Application.ToString()]
        //                    select new DocumentRow()
        //                    {
        //                        id = doc.ID.ToString(),
        //                        cell = new List<string>() { doc.Nome, "File " + ((doc.Extension.Length > 0) ? doc.Extension : application.LabelSingolare), doc.Modify.ToString(), doc.FrontendLink }

        //                    }).ToList();
                
        //        DocumentGridObject gridObj = new DocumentGridObject();
        //        gridObj.page = 1;
        //        gridObj.total = docs.Count;
        //        gridObj.rows = docs;

        //        return gridObj;
        //    }
        //}
        #endregion

        public DocumentGridObject GetDocumentsByFolder(int folderId, int userId, int page, int rp, string name, string order, TipoSelettore tipoFiltro = TipoSelettore.All)
        {
            //if (NetCms.Users.AccountManager.CurrentAccount != null)
            //{
                using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                {
                    StructureFolder folder = FolderBusinessLogic.GetById(sess, folderId);
                    IList<Document> folder_documents = DocumentBusinessLogic.GetDocumentsPagedByFolder2(folderId, ViewOrders.CreationDateDesc, page, rp, tipoFiltro, sess);
                    int docsOrderedCount = DocumentBusinessLogic.GetDocumentsPagedByFolder2Count(folderId, tipoFiltro, sess);

                    Users.User utente = UsersBusinessLogic.GetById(userId, sess);

                    if (folder != null)
                    {
                        IEnumerable<Document> documents = null;

                        if (folder.Disable == 0)
                        {
                        documents = (from doc in folder_documents//folder.Documents
                                     orderby doc.Nome ascending
                                     let document = DocumentBusinessLogic.GetById(doc.ID, sess)
                                     let criteria = new DocumentGrants(document, utente.Profile, sess)
                                     where criteria[NetCms.Grants.CriteriaTypes.Show].Allowed  && document.Trash == 0 //&& CanShow(document.Extension, tipoFiltro)
                                     select document);//.Skip((page-1)*rp).Take(rp);
                        }
                        else
                        {
                        documents = (from doc in folder_documents //folder.Documents
                                     orderby doc.Nome ascending
                                     let document = DocumentBusinessLogic.GetById(doc.ID, sess)
                                     //let criteria = new DocumentGrants(document, utente.Profile)
                                     where  document.Trash == 0  //criteria[NetCms.Grants.CriteriaTypes.Show].Allowed && CanShow(document.Extension, tipoFiltro)
                                     select document);//.Skip((page-1) * rp).Take(rp);
                    }
                        IList<Document> documentsList = documents.ToList();

                    //var docsOrdered = G2Core.Common.Utility.OrderBy(documentsList.AsQueryable().Where(x => x.AddDoc && x.Trash == 0), name, order == "asc");

                    //IList<Document> docsOrderedList = docsOrdered.ToList();

                        //int docsOrderedCount = documentsList.Count();//docsOrderedList.Count();

                        var docsPaginated = documentsList; // docsOrderedList.Skip((page - 1) * rp).Take(rp);

                        var docs = (from doc in docsPaginated
                                    let application = ApplicationsPool.ActiveAssemblies[doc.Application.ToString()]
                                    select new DocumentRow()
                                    {
                                        id = doc.ID.ToString(),
                                        cell = new List<string>() { doc.Nome, "File " + ((doc.Extension.Length > 0) ? doc.Extension : application.LabelSingolare), (doc.Modify != DateTime.MinValue) ? doc.Modify.ToString() : doc.Date, "<strong>Seleziona</strong>", doc.FrontendLink }

                                    }).ToList();

                        //Aggiungo la selezione della cartella corrente nel caso il filtro sia impostato a "Tutti i file"
                        if (tipoFiltro == TipoSelettore.All)
                        {
                            docs.Add(new DocumentRow()
                            {
                                id = "F_" + folder.ID.ToString(),
                                cell = new List<string>() { folder.Label, "Cartella corrente", " - ", "<strong>Seleziona</strong>", folder.FrontendHyperlink }

                            });
                        }
                        DocumentGridObject gridObj = new DocumentGridObject();
                        gridObj.page = page;
                        gridObj.total = docsOrderedCount == 0 ? 1 : docsOrderedCount;
                        gridObj.rows = docs;

                        return gridObj;
                    }
                    else
                    {
                        Diagnostics.WebApiErrorHandler apiError = new Diagnostics.WebApiErrorHandler(System.Net.HttpStatusCode.NotFound, "Folder Id not found", string.Format("No folder with ID = {0}", folderId));
                        throw new HttpResponseException(apiError.HttpResponse());
                    }
                }
            //}
            //else
            //{
            //    Diagnostics.WebApiErrorHandler apiError = new Diagnostics.WebApiErrorHandler(System.Net.HttpStatusCode.Unauthorized, "Autentication is required", string.Format("No cmsuser is logged"));
            //    throw new HttpResponseException(apiError.HttpResponse());
            //}

        }

        public static List<string> ImageExtensions
        {
            get
            {
                return Configuration["imgextensions"].ToLower().Split(',').ToList();
            }
        }

        public static List<string> FlashExtensions
        {
            get
            {
                return Configuration["flashextensions"].ToLower().Split(',').ToList();
            }
        }

        public static List<string> VideoExtensions
        {
            get
            {
                return Configuration["videoextensions"].ToLower().Split(',').ToList();
            }
        }

        //private bool CanShow(string extension, TipoSelettore tipo)
        //{
        //    switch (tipo)
        //    {
        //        case TipoSelettore.All:
        //            return true;
        //            break;
        //        case TipoSelettore.Flash:
        //            return FlashExtensions.Contains(extension.ToLower());
        //            break;
        //        case TipoSelettore.Images:
        //            return ImageExtensions.Contains(extension.ToLower());
        //            break;
        //        case TipoSelettore.Video:
        //            return VideoExtensions.Contains(extension.ToLower());
        //            break;
        //        default: return true;
        //    }
        //}
    }
}
