﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.GUI.FileSystemNavigator.Model
{
    public enum TipoSelettore { All = 0, Images = 1, Flash = 2, Video = 3 };

    public class DocumentRow
    {
        public string id;
        public List<string> cell = new List<string>();
    }

    public class DocumentGridObject
    {
        public int page;
        public int total;
        public List<DocumentRow> rows = new List<DocumentRow>();
    }
}


