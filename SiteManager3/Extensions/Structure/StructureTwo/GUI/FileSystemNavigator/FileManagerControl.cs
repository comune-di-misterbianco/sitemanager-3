﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using NetCms.Structure.GUI.FileSystemNavigator.Model;
using NetCms.Structure.WebFS;

namespace NetCms.Structure.Popup
{
    public class FileManagerControl : WebControl
    {
        private string _TargetControlId;

        public FileManagerControl(string targetCtdId/*, TipoSelettore tipo = TipoSelettore.All*/) : base( HtmlTextWriterTag.Div)
        {
            _TargetControlId = targetCtdId;
            //TipoFiltro = tipo;
        }

        public TipoSelettore TipoFiltro
        {
            get
            {
                NetUtility.RequestVariable type = new NetUtility.RequestVariable("type", NetUtility.RequestVariable.RequestType.QueryString);
                if (type.IsValidInteger)
                    return (TipoSelettore)Enum.ToObject(typeof(TipoSelettore), type.IntValue);
                return TipoSelettore.All;
            }
        }

        private int Rpp
        {
            get 
            {
                return int.Parse(Configuration["rpp"]);
            }
        }

        private int Width
        {
            get
            {
                return int.Parse(Configuration["width"]);
            }
        }

        private int Height
        {
            get
            {
                return int.Parse(Configuration["height"]);
            }
        }

        private string Title
        {
            get
            {
                return Configuration["title"];
            }
        }

        public bool _ShowListView = false;
        public bool ShowListView 
        {
            get {
                return _ShowListView;
            }
            set {
                _ShowListView = value;
            }
        }

        public bool RequestFromCKEditor
        {
            get 
            {
                NetUtility.RequestVariable ckeditor = new NetUtility.RequestVariable("CKEditor", NetUtility.RequestVariable.RequestType.QueryString);
                if (ckeditor.IsValidString)
                    return true;
                return false;
            }
        }

        public int CKEditorFromNumber
        {
            get
            {
                NetUtility.RequestVariable ckeditorfuncnumber = new NetUtility.RequestVariable("CKEditorFuncNum", NetUtility.RequestVariable.RequestType.QueryString);
                if (ckeditorfuncnumber.IsValidInteger)
                    return ckeditorfuncnumber.IntValue;
                return -1;
            }
        }

        public static NetCms.Vertical.Configuration.Configuration Configuration
        {
            get
            {
                return _Configuration ?? (_Configuration = new NetCms.Vertical.Configuration.Configuration("filemanager"));
            }
        }
        private static NetCms.Vertical.Configuration.Configuration _Configuration;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            WebControl divFileManager = new WebControl(HtmlTextWriterTag.Div);
            divFileManager.ID = "FileManagerContainer";
            divFileManager.CssClass = "FileManagerClass container-fluid";
            //this.Controls.Add(divFileManager);

            WebControl divrow = new WebControl(HtmlTextWriterTag.Div);
            divrow.CssClass = "row";
            
            WebControl topBar = new WebControl(HtmlTextWriterTag.Div);
            topBar.CssClass = "fileManagerTopbar col-lg-12 border-bottom";

            //topBar.Controls.Add(new LiteralControl("" +
            //    "<div class=\"form-inline\">"+
            //        "<div class=\"col-lg-6 \"> " +
            //            "<div class=\"mb-2 mt-2\">" +
            //                "<h4 class=\"toolbarLabel\">Document picker</h4>" +                                                    
            //            "</div>" +
            //        "</div>" +
            //        "<div class=\"col-lg-6 \">"+
            //            "<div class=\"mb-2 mt-2\">"+
            //                "<div class=\"col-lg-3\">" +
            //                    "<div class=\"input-group  \">" +
            //                        "<input class=\"form-control\" placeholder=\"Nome file\" type=\"text\" id=\"search_file\" />" +
            //                        "<div class=\"input-group-append\">"+
            //                            "<button class=\"btn btn-secondary\" type=\"button\">"+
            //                            "<i class=\"fas fa-search\"></i>"+
            //                          "</button>"+
            //                        "</div>"+
            //                    "</div>" +
            //                "</div>" +

            //                 "<div class=\"input-group col-lg-3 \">" +
            //                    //  "<button type=\"button\" class=\"btn btn-light ml-1\">Cerca</button>" +
            //                    "<button type=\"button\" class=\"btn btn-secondary ml-1\">Carica</button>" +
            //                    "<button type=\"button\" id=\"select-button\" class=\"btn btn-primary ml-1\" disabled=\"disabled\">Seleziona</button>" +
            //                    "</div>" +
            //            "</div> " +
            //        "</div> " +
            //    "</div>"+
            //    ""));



            topBar.Controls.Add(new LiteralControl("" +
                "<nav class=\"navbar navbar-light bg-light justify-content-between\">" +
                    "<a class=\"navbar-brand\">File Picker</a>" +
                "<div class=\"form-inline\">" +
                //"<input class=\"form-control mr-sm-2\" type=\"search\" placeholder=\"Cerca\" aria-label=\"Cerca\">" +
               // "<button class=\"btn btn-outline-secondary my-2 my-sm-0\" type=\"submit\"><i class=\"fas fa-search pr-1\"></i>Cerca</button>" +
               // "<button type=\"button\" class=\"btn btn-secondary ml-1\"><i class=\"fas fa-upload pr-1\"></i>Carica</button>" +
               ((ShowListView) ? "<button type=\"button\" id=\"select-button\" class=\"btn btn-primary ml-1\" disabled=\"disabled\"><i class=\"fas fa-check-square pr-1\"></i>Seleziona</button>" : "") +        
                "</div> " +
                "</nav>"));

            divrow.Controls.Add(topBar);


            WebControl leftPanel = new WebControl(HtmlTextWriterTag.Div);
            leftPanel.CssClass = "fileManagerLeftPanel col-lg-3 border-right mh-100";
            divrow.Controls.Add(leftPanel);

            WebControl foldersList = new WebControl(HtmlTextWriterTag.Ul);
            foldersList.ID = "FolderPickerAjaxTree";
            leftPanel.Controls.Add(foldersList);

            WebControl rightPanel = new WebControl(HtmlTextWriterTag.Div);
            rightPanel.CssClass = "fileManagerRightPanel col-lg-9";
            divrow.Controls.Add(rightPanel);

            WebControl FolderInfo = new WebControl(HtmlTextWriterTag.P);
            FolderInfo.CssClass = "FolderInfo";
            WebControl nameFolder = new WebControl(HtmlTextWriterTag.Span);
            nameFolder.ID = "FolderName";
            FolderInfo.Controls.Add(nameFolder);
            WebControl otherFolder = new WebControl(HtmlTextWriterTag.Span);
            otherFolder.ID = "FolderOther";
            FolderInfo.Controls.Add(otherFolder);

            rightPanel.Controls.Add(FolderInfo);
            if (!ShowListView)
            {
                WebControl documentsList = new WebControl(HtmlTextWriterTag.Table);
                documentsList.ID = "DocumentPickerAjaxList";
                documentsList.CssClass = "col-lg-9";
                documentsList.Style.Add(HtmlTextWriterStyle.Display, "none");
                rightPanel.Controls.Add(documentsList);

                WebControl orderNameHidden = new WebControl(HtmlTextWriterTag.Input);
                orderNameHidden.Attributes["type"] = "hidden";
                orderNameHidden.ID = "orderNameHidden";
                rightPanel.Controls.Add(orderNameHidden);

                WebControl orderAscHidden = new WebControl(HtmlTextWriterTag.Input);
                orderAscHidden.Attributes["type"] = "hidden";
                orderAscHidden.ID = "orderAscHidden";
                rightPanel.Controls.Add(orderAscHidden);
            }
            else 
            {
                WebControl FilePickerGrid = new WebControl(HtmlTextWriterTag.Div);
                FilePickerGrid.ID = "FilePicker";
                FilePickerGrid.CssClass = "filepicker";
                rightPanel.Controls.Add(FilePickerGrid);                 
            }
            WebControl script = new WebControl( HtmlTextWriterTag.Script);

            this.Controls.Add(script);
            script.Attributes["type"] = "text/javascript";


            /*******/

            int idCurrentFolder = -1;
            string tipoFolder = "";
            string nomeFolder = "";
            bool isMainHomepage = false;
            StructureFolder currentFolder = null;

            NetUtility.RequestVariable SelFolderID = new NetUtility.RequestVariable("StructureFolderSideTree", NetUtility.RequestVariable.RequestType.Form_QueryString);
            if (SelFolderID.IsValid(NetUtility.RequestVariable.VariableType.Integer) && SelFolderID.IntValue > 0)
            {
                idCurrentFolder = SelFolderID.IntValue;
                currentFolder = NetCms.Structure.WebFS.FileSystemBusinessLogic.FolderBusinessLogic.GetById(idCurrentFolder);
                tipoFolder = currentFolder.TypeName;
                nomeFolder = currentFolder.Label.Replace("'", @"\'");
            }
            else 
            {
                isMainHomepage = false;
                idCurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.RootFolderID;
                tipoFolder = "Homepage";
                nomeFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.RootFolder.Nome.Replace("'", @"\'");
            }

            var folders = (isMainHomepage) ? NetCms.Networks.NetworksManager.CurrentActiveNetwork.RootFolder.Folders.Cast<StructureFolder>() : currentFolder.Folders.Cast<StructureFolder>();
            var docs = (isMainHomepage) ? NetCms.Networks.NetworksManager.CurrentActiveNetwork.RootFolder.Documents.Cast<Document>() : currentFolder.Documents.Cast<Document>();                   

            if (!ShowListView)
            {

                string html = @"
        $(function () {
            $('#DocumentPickerAjaxList').flexigrid({
            url: '" + NetCms.Configurations.Paths.AbsoluteRoot + @"/api/filemanager/GetDocumentsByFolder/" + idCurrentFolder + "/' + " + NetCms.Users.AccountManager.CurrentAccount.ID + @" + '/1/" + Rpp + @"/Nome/asc/" + (int)TipoFiltro + @"',
            dataType: 'json',
            method: 'get',
            colModel: [
             {display: 'Nome', name: 'Nome', width: 300, sortable: true, align: 'left'},
             {display: 'Tipo', name: 'Extension', width: 100, sortable: true, align: 'left'},
             {display: 'Data modifica', name: 'Modify', width: 100, sortable: true, align: 'left'},
             {display: 'Azioni', name: 'Action', width: 100, sortable: false, align: 'left', process: procMe},
             {display: 'Percorso', name: 'FrontendLink', width: 100, sortable: true, align: 'left', hide: true}
            ],
            onError: function (jqXHR, textStatus, errorThrown) {
                console.log('flexigrid failed ' + errorThrown);
            },        
            sortname: 'Nome',
            sortorder: 'asc',
            usepager: true,
            title: " + Title + @",
            useRp: false,
            rp: " + Rpp + @",
            showTableToggleBtn: false,
            width: 'auto', //" + Width + @"
            height: " + Height + @",
            singleSelect: true,
            pagestat: 'Visualizzo da {from} a {to} di {total} elementi',
			pagetext: 'Pagina',
			outof: 'di',
			findtext: 'Trova',
			procmsg: 'Recupero documenti, attendere ...',
			nomsg: 'Nessun elemento'           
        });

        retrieveDocuments(" + idCurrentFolder + ",'" + nomeFolder + "','" + tipoFolder + "'," + folders.Count(x => x.Trash == 0) + "," + docs.Count(x => x.AddDoc && x.Trash == 0) + @");
      });    


            function retrieveDocuments(value,name,tipo,subFolds,numFiles){
                $('#FolderOther').empty();
                $('#FolderOther').append('<p><strong>Tipologia cartella selezionata</strong>: ' + tipo + '</p>');            
                $('#FolderOther').append('<p>Continene <strong>' + subFolds + '</strong> sottocartelle e <strong>' + numFiles + '</strong> documenti</p>');                            

                $('#DocumentPickerAjaxList').flexOptions({ onChangeSort: function (name,order)
                    {
                        $('#orderNameHidden').val(name);
                        $('#orderAscHidden').val(order);
                        $('#DocumentPickerAjaxList').flexOptions({url: '" + NetCms.Configurations.Paths.AbsoluteRoot + @"/api/filemanager/GetDocumentsByFolder/' + value + '/' + " + NetCms.Users.AccountManager.CurrentAccount.ID + @" + '/1/" + Rpp + @"/' + name + '/' + order + '/' + " + (int)TipoFiltro + @"}).flexReload();
                    } 
                });                

                $('#DocumentPickerAjaxList').flexOptions({ onChangePage: function (page)
                    {
                        var name = $('#orderNameHidden').val();
                        if (!name)
                            name = 'Nome';
                        var order = $('#orderAscHidden').val();
                        if (!order)
                            order = 'asc';
                        $('#DocumentPickerAjaxList').flexOptions({url: '" + NetCms.Configurations.Paths.AbsoluteRoot + @"/api/filemanager/GetDocumentsByFolder/' + value + '/' + " + NetCms.Users.AccountManager.CurrentAccount.ID + @" + '/' + page + '/" + Rpp + @"/' + name + '/' + order + '/' + " + (int)TipoFiltro + @"}).flexReload();
                    } 
                }); 

                
                var name = $('#orderNameHidden').val();
                if (!name)
                    name = 'Nome';
                var order = $('#orderAscHidden').val();
                if (!order)
                    order = 'asc';
                $('#DocumentPickerAjaxList').flexOptions({url: '" + NetCms.Configurations.Paths.AbsoluteRoot + @"/api/filemanager/GetDocumentsByFolder/' + value + '/' + " + NetCms.Users.AccountManager.CurrentAccount.ID + @" + '/1/" + Rpp + @"/' + name + '/' + order  + '/' + " + (int)TipoFiltro + @", newp: 1}).flexReload();

                //Selezione al doppio click sulla riga
                $('#DocumentPickerAjaxList').dblclick(function(event){
                    event.preventDefault();
                    $('.trSelected', this).each( function(){";

                if (RequestFromCKEditor)
                {
                    html += @"var funcNum = " + CKEditorFromNumber + @";
                         var fileUrl = $('td[abbr=\'FrontendLink\'] >div', this).html();
                         window.opener.CKEDITOR.tools.callFunction( funcNum, fileUrl );
                         window.close();";
                }
                else
                {
                    html += @"oOpener = window;
                        oTarget = oOpener.document.getElementById('" + _TargetControlId + @"');
                        oTarget.value = $('td[abbr=\'FrontendLink\'] >div', this).html();
                        $('#uimodal-output').dialog('close');";
                }

                html += @"});
                });

                //Inibisce la selezione del testo in caso di doppio click su tag di tipo tr,td o div
                $.extend($.fn.disableTextSelect = function() {
		            return this.each(function(){
			            if($.browser.mozilla){//Firefox
				            $(this).css('MozUserSelect','none');
			            }else if($.browser.msie){//IE
				            $(this).bind('selectstart',function(){return false;});
			            }else{//Opera, etc.
				            $(this).mousedown(function(){return false;});
			            }
		            });
	            });
	            $('#DocumentPickerAjaxList tr,td,div').disableTextSelect();
            }
            
            //Funzione per il pulsante seleziona
            function procMe(celDiv,id) { 
                $(celDiv).click( 
                    function () {
                        var fileUrl = $('td[abbr=\'FrontendLink\'] >div', this.parentNode.parentNode).html();";

                if (RequestFromCKEditor)
                {
                    html += @"var funcNum = " + CKEditorFromNumber + @";                            
                            var oOpener = window.parent;                       
                            oOpener.CKEDITOR.tools.callFunction(funcNum, fileUrl );                            
                            ";
                }
                else
                {
                    html += @"oOpener = window.parent;
                        oTarget = oOpener.document.getElementById('" + _TargetControlId + @"');
                        oTarget.value = fileUrl;
                        oOpener.$('#docPicker').modal('toggle');";                        
                        
                }

                html += @"} 
                ); 
            }; 
                
            ";

                script.Controls.Add(new LiteralControl(html));
            }
            else
            {                
                string html = @"$(document).ready(function () {        
		                            $('#FilePicker').filePicker({			                            
                                       url: '" + NetCms.Configurations.Paths.AbsoluteRoot + @"/api/filemanager/GetDocumentsByFolder/" + idCurrentFolder + "/" + NetCms.Users.AccountManager.CurrentAccount.ID + @"/1/" + Rpp + @"/Nome/asc/" + (int)TipoFiltro + @"'                                       
	                            });
                              });

                              function retrieveDocuments(value, name, tipo, subFolds, numFiles) {
                                       var url = '" + NetCms.Configurations.Paths.AbsoluteRoot + @"/api/filemanager/GetDocumentsByFolder/'+value+'/" + NetCms.Users.AccountManager.CurrentAccount.ID + @"/1/" + Rpp + @"/Nome/asc/" + (int)TipoFiltro + @"';                                                                                                                     

                                       $('.FolderInfo').empty();
                                       $('.FolderInfo').append('<p><strong>Tipologia cartella selezionata</strong>: ' + tipo + '</p>');            
                                       $('#FilePicker').filePicker('update',url);
                              }
                              
                              ";                    //$('.FolderInfo').append('<p>Continene <strong>' + subFolds + '</strong> sottocartelle e <strong>' + numFiles + '</strong> documenti</p>');           

                script.Controls.Add(new LiteralControl(html));
            }

            divFileManager.Controls.Add(divrow);
            this.Controls.Add(divFileManager);
        }
    }
}
