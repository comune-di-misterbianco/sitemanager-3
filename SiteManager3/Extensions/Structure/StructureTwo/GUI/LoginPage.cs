using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Reflection;

/// <summary>
/// Summary description for FilesDocument
/// </summary>
namespace NetCms.GUI
{
    public class LoginPage:SmPageLogin
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                return null;
            }
        }

        public override string PageTitle
        {
            get { return "Pagina di Accesso"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.House; }
        }

        public override string LocalCssClass
        {
            get { return "LoginPage"; }
        }

        protected override WebControl RootControl
        {
            get
            {
                return new GUI.CmsLoginController();
            }
        }
    }
}
