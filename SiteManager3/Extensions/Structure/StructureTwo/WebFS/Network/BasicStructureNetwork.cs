﻿using System;
using System.Web;
using System.Web.Caching;
using System.Collections.Generic;
using System.Data;
using System.Text;
using NetCms.Connections;
using NetCms.Networks;

namespace NetCms
{
    namespace Structure.WebFS
    {
        public class BasicStructureNetwork : NetCms.Networks.Network //NetCms.Networks.BasicNetwork
        {
            public override NetCms.Faces Face
            {
                get { return NetCms.Faces.Admin; }
            }

            public BasicStructureNetwork(int id) : base(id) { }
            //DA ELIMINARE - DECOMMENTATO SOLO SOLO PER COMPILARE
            //public BasicStructureNetwork(DataRow row) : base(0) { }
            public BasicStructureNetwork(string name) : base(name) { }
            public BasicStructureNetwork(INetwork network) : base(network) { }


            public override Networks.WebFS.NetworkFolder BuildRootFolder()
            {
                throw new Exception("Metodo non implementato su BasicStructureNetwork");
            }
        }
    }
}
