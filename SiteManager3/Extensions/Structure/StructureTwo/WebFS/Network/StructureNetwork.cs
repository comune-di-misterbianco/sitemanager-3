﻿using System;
using System.Web;
using System.Web.Caching;
using System.Collections.Generic;
using System.Data;
using System.Text;
using NetCms.Connections;
using System.Linq;
using NetCms.Networks;
using NetCms.GUI;

namespace NetCms
{
    namespace Structure.WebFS
    {
        public class StructureNetwork : NetCms.Networks.Network
        {
            public override Networks.WebFS.NetworkFolder BuildRootFolder()
            {
                
                IList<StructureFolder> collection = FileSystemBusinessLogic.FolderBusinessLogic.FindRootFoldersByTipo(NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies.Cast<NetCms.Structure.Applications.Application>().Select(x => x.ID).ToList<int>(),this.ID);
                return collection.ElementAt(0);
                //StructureFolderCollection coll = new StructureFolderCollection(this.CacheKey,G2Core.Caching.Visibility.CachingVisibility.User,this.Connection);
                //coll.FillWithRootFolder();
             
                //return coll[0];
            }
            
            public override NetCms.Faces Face
            {
                get { return NetCms.Faces.Admin; }
            }

            private int _FolderNameMaxLenght;
            public int FolderNameMaxLenght
            {
                get
                {
                    if (_FolderNameMaxLenght == 0)
                        _FolderNameMaxLenght = Configurations.XmlConfig.GetIntegerAttribute("/Portal/Networks/" + this.SystemName, "foldernamemaxlenght");

                    if (_FolderNameMaxLenght == int.MinValue)
                        _FolderNameMaxLenght = 255;

                    return _FolderNameMaxLenght;
                }
            }

            public StructureNetwork(int id) : base(id) { }
            public StructureNetwork(string name) : base(name) { }
            public StructureNetwork(Networks.NetworkKey networkKey) : base(networkKey) { }
            public StructureNetwork(INetwork baseNetwork) : base(baseNetwork) { }


            public new StructureFolder RootFolder
            {
                get
                {
                    return (StructureFolder)base.RootFolder;
                }
            }

            public new StructureFolder CurrentFolder
            {
                get
                {
                    if (!NetCms.Users.AccountManager.CurrentAccount.NetworkEnabledForUser(this.ID))
                        PopupBox.AddNoGrantsMessageAndGoBack();

                    return (StructureFolder)base.CurrentFolder;
                }
            }

            public void Invalidate()
            {
                //SISTEMARE CON CACHE 2° LIVELLO
                //RootFolderCacheableItem.Invalidate();
                //RootFolder.OwnerCollection.DataTableManager.Invalidate();
                //RootFolder.OwnerCollection.Invalidate();
                ////_RootFolder = null;
            }
        }
    }
}
