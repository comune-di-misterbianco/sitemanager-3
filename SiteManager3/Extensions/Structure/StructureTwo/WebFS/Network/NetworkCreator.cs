﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using NetCms.Structure.Applications;
using NetCms.Connections;
using NHibernate;
using NetCms.Users;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS;
using NetCms.Networks.WebFS;
using NetCms.Grants;
using System.Linq;

namespace NetCms.Networks
{
    public class NetworkGenerator
    {
        public const string ModelSystemNetworkName = "system";

        private bool _CreateHomeRecord = true;
        public bool CreateHomeRecord
        {
            get { return _CreateHomeRecord; }
            set { _CreateHomeRecord = value; }
        }

        public bool Default
        {
            get { return _Default; }
            set { _Default = value; }
        }
        private bool _Default;

        public bool Enabled
        {
            get { return _Enabled; }
            set { _Enabled = value; }
        }
        private bool _Enabled;

        public bool HasFrontend
        {
            get { return _HasFrontend; }
            set { _HasFrontend = value; }
        }
        private bool _HasFrontend = true;

        public string SystemName
        {
            get { return _SystemName; }
            private set { _SystemName = value; }
        }
        private string _SystemName;

        public int DefaultLang
        {
            get { return _DefaultLang; }
            private set { _DefaultLang = value; }
        }
        private int _DefaultLang = 1;

        protected List<int> UsersToAllow
        {
            get
            {
                if (_UsersToAllow == null)
                    _UsersToAllow = new List<int>();
                return _UsersToAllow;
            }
        }
        private List<int> _UsersToAllow;
               

        public NetworkKey GeneratedNetworkKey
        {
            get
            {
                return _GeneratedNetworkKey;
            }
        }
        private NetworkKey _GeneratedNetworkKey;

        public string Label
        {
            get { return _Label; }
            private set { _Label = value; }
        }
        private string _Label;


        public bool GenerateCms
        {
            get { return _GenerateCms; }
            private set { _GenerateCms = value; }
        }
        private bool _GenerateCms;

        public NetworkGenerator(string label, string systemName)
            :this(label, systemName, true)
        {
        }	

        public NetworkGenerator(string label, string systemName, bool generateCms)
        {
            this.Label = label;
            this.SystemName = systemName;
            this.GenerateCms = generateCms;
        }

        public List<string> Errors
        {
            get
            {
                if (_Errors == null)
                    _Errors = new List<string>();
                return _Errors;
            }
        }
        private List<string> _Errors;


        public List<NetworkGeneratorLogItem> Logs
        {
            get
            {
                if (_Logs == null)
                    _Logs = new List<NetworkGeneratorLogItem>();
                return _Logs;
            }
        }
        private List<NetworkGeneratorLogItem> _Logs;

        public int NetworkID;

        public bool Generate()
        {
            bool ok =  this.GenerateCms ? GenerateWithCms() : GenerateWithoutCms();

            if (NetCms.Configurations.Debug.GenericEnabled)
            {
                DateTime lastTime = DateTime.MinValue;
                string model = "@{0} #{1}) - {2} [{3}]";
                foreach (var log in Logs)
                {
                    TimeSpan time = (lastTime == DateTime.MinValue) ? new TimeSpan(0) : log.ExecutionTime - lastTime;
                    NetCms.GUI.PopupBox.AddMessage(
                        string.Format(model, time.ToString(), log.Code, log.Message, (log.Ex != null ? log.Ex.Message : "")),
                        ok ? NetCms.GUI.PostBackMessagesType.Success : GUI.PostBackMessagesType.Notify
                        );
                }
            }

            return ok;
        }

        private bool GenerateWithCms()
        {
            NetworkDbGenerator dbGenerator = new NetworkDbGenerator(this);
            //Passo 1: Creo la network su database 
            int NewNetworkID = dbGenerator.Generate();
            bool ok = NewNetworkID > 0;
            NetworkID = NewNetworkID;
            //Passo 2: Genero i file di sistema e le cartelle.

            if (ok)
            {
                ok = GenerateSystemFolderAndFiles(NewNetworkID);
                if (!ok) Errors.Add("Si è verificato un errore nella creazione dei file di sistema");
            }
            if (ok)
            {
                ok = GenerateCSSFolderAndFiles(NewNetworkID);
                if (!ok) Errors.Add("Si è verificato un errore nella creazione dei file di sistema");
            }
            if (ok)
            {
                ok = GenerateStaticSystemFolderAndFiles(NewNetworkID);
                if (!ok) Errors.Add("Si è verificato un errore nella creazione dei file di sistema");
            }
            //Passo 3: Installo l'applicazione Homepage con la cartella radice della portale.
            if (ok)
            {
                ok = NetCms.Structure.Applications.ApplicationsPool.HomepageApplication.Installer.Install(NewNetworkID, CreateHomeRecord);
                if (!ok) Errors.Add("Si è verificato un errore nella creazione dei record relativi alla root folder");
            }

            ////Passo 5: Installo le altre applicazioni 
            //if (ok)
            //{
            //    for (int i = 0; ok && i < NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies.Count; i++)
            //    {
            //        if (ok && NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies[i].ID != NetCms.Structure.Applications.ApplicationsPool.HomepageApplication.ID)
            //        {
            //            ok = NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies[i].Installer.Install(NewNetworkID);
            //            if (!ok) Errors.Add("Si è verificato un errore nella creazione dell'applicazione " + NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies[i].SystemName);
            //        }
            //    }
            //}

            //Creazione dell'utente
            if (ok)
            {   
                foreach (int UserID in this.UsersToAllow)
                    InsertUser(UserID, NewNetworkID);
            }
            
            //Passo 6: Attribuisco i permessi a tutti gli utenti che sono stati inseriti nell'elenco degli utenti da ammettere.
            if (ok)
            {
                ok = GeneratePermessions(NewNetworkID);
                if (!ok) Errors.Add("Si è verificato un errore nella creazione dei permessi sulla root folder");
            }

            if (ok)
                _GeneratedNetworkKey = new NetworkKey(NewNetworkID, G2Core.Caching.Visibility.CachingVisibility.User);
            //NetCms.GUI.GuiUtility.RestartApplication();
            
            return ok;
        }
       
        private bool GenerateWithoutCms()
        {
            NetworkDbGenerator dbGenerator = new NetworkDbGenerator(this);
            //Passo 1: Creo la network su database 
            int NewNetworkID = dbGenerator.GenerateNetworkRecord();
            bool ok = NewNetworkID > 0;
            if (ok)
            {
                NetworkID = NewNetworkID;

                //Creazione dell'utente
                foreach (int UserID in this.UsersToAllow)
                {
                    InsertUser(UserID, NewNetworkID);
                }

                _GeneratedNetworkKey = new NetworkKey(NewNetworkID, G2Core.Caching.Visibility.CachingVisibility.User);
            }
            else
            {
                this.Logs.Add(new NetworkGeneratorLogItem() { Message = "Impossibile generare il database.", Code = 12, ExecutionTime = DateTime.Now });
                return false;
            }

            return ok;
        }

        private bool GenerateSystemFolderAndFiles(int NewNetworkID)
        {
            string path = HttpContext.Current.Server.MapPath(Configurations.Paths.AbsoluteRoot + "/" + this.SystemName);
            try
            {
                //Creo la cartella principale del portale
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
            }
            catch (Exception ex)
            {
                this.Logs.Add(new NetworkGeneratorLogItem() { Message = "Errore durante la creazione della cartella principale del portale.", Ex = ex, Code = 8, ExecutionTime = DateTime.Now });
                return false;
            }

            try
            {
                //Creo la default page del Portale
                string filePath = path + "\\" + Configurations.Generics.DefaultPageName;
                if (!File.Exists(filePath))
                    File.Create(filePath);
            }
            catch (Exception ex)
            {
                this.Logs.Add(new NetworkGeneratorLogItem() { Message = "Errore durante la generazione della default page del Portale.", Ex = ex, Code = 9, ExecutionTime = DateTime.Now });
                return false;
            }

            try
            {
                string originalSystemFolderPath = Configurations.Paths.AbsoluteConfigRoot + "\\" + NetworkGenerator.ModelSystemNetworkName;
                string newSystemFolderPath = Configurations.Paths.AbsoluteConfigRoot + "\\" + this.SystemName;
                DirectoryInfo sysdir = new DirectoryInfo(originalSystemFolderPath);
                if (!sysdir.Exists)
                    throw new Exception("Impossibile trovare la cartella '" + originalSystemFolderPath + "' necessaria alla creazione di una nuovo Portale.");

                //Creo la cartella di configurazione del Portale
                if (!Directory.Exists(newSystemFolderPath))
                    Directory.CreateDirectory(newSystemFolderPath);

                //Creo i file di configurazione del Portale
                foreach (FileInfo file in sysdir.GetFiles())
                    file.CopyTo(newSystemFolderPath + "\\" + file.Name);
            }
            catch (Exception ex)
            {
                this.Logs.Add(new NetworkGeneratorLogItem() { Message = "Errore durante la copia dei file di sistema.", Ex = ex, Code = 10, ExecutionTime = DateTime.Now });
                return false;
            }

            try
            {
                string systemNodeContent = Configurations.XmlConfig.GetNodeXml("/Portal/configs/Networks/" + NetworkGenerator.ModelSystemNetworkName);

                XmlNode Networks = Configurations.XmlConfig.GetNodeContent("/Portal/configs/Networks");
                XmlNode clone = Configurations.XmlConfig.Document.CreateNode(XmlNodeType.Element, this.SystemName.ToLower(), null);
                clone.InnerXml = systemNodeContent;
                Networks.AppendChild(clone);

                if (Configurations.XmlConfig.Save())
                    return true;
                else
                {
                    this.Logs.Add(new NetworkGeneratorLogItem() { Message = "Errore durante la generazione della configurazione XML.", Code = 11, ExecutionTime = DateTime.Now });
                    return false;
                }

            }

            catch (Exception ex)
            {
                this.Logs.Add(new NetworkGeneratorLogItem() { Message = "Errore durante la generazione della configurazione XML.", Ex = ex, Code = 11, ExecutionTime = DateTime.Now });
                return false;
            }

        }
        
        private bool GenerateStaticSystemFolderAndFiles(int NewNetworkID)
        {
            try
            {
                //Creo la cartella principale del Portale
                string path = HttpContext.Current.Server.MapPath(Configurations.Paths.AbsoluteRoot + "/" + this.SystemName);
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);


                string originalSystemFolderPath = Configurations.Paths.PhysicalRoot + "\\" + NetworkGenerator.ModelSystemNetworkName;
                string newSystemFolderPath = Configurations.Paths.PhysicalRoot + "\\" + this.SystemName;
                DirectoryInfo sysdir = new DirectoryInfo(originalSystemFolderPath);
                if (!sysdir.Exists)
                    throw new Exception("Impossibile trovare la cartella '" + originalSystemFolderPath + "' necessaria alla creazione di una nuovo Portale.");

                FilesCopy(sysdir, newSystemFolderPath);

                DirectoriesCopy(sysdir, newSystemFolderPath);


                return true;
            }
            catch (Exception ex)
            {
                this.Logs.Add(new NetworkGeneratorLogItem() { Message = "Errore durante la generazione dei file statici.", Ex = ex, Code = 7, ExecutionTime = DateTime.Now });
                return false;
            }

        }

        private bool GenerateCSSFolderAndFiles(int NewNetworkID)
        {
            try
            {
                //Creo la cartella principale del Portale
                string path = HttpContext.Current.Server.MapPath(Configurations.Paths.AbsoluteRoot + "/css/" + this.SystemName);
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                string originalSystemFolderPath = Configurations.Paths.PhysicalRoot + "\\css\\" + NetworkGenerator.ModelSystemNetworkName;
                string newSystemFolderPath = Configurations.Paths.PhysicalRoot + "\\css\\" + this.SystemName;
                DirectoryInfo sysdir = new DirectoryInfo(originalSystemFolderPath);
                if (!sysdir.Exists)
                    throw new Exception("Impossibile trovare la cartella '" + originalSystemFolderPath + "' necessaria alla creazione di una nuovo Portale.");

                FilesCopy(sysdir, newSystemFolderPath);

                DirectoriesCopy(sysdir, newSystemFolderPath);
                
                return true;
            }
            catch (Exception ex)
            {
                this.Logs.Add(new NetworkGeneratorLogItem() { Message = "Errore durante la generazione dei CSS.", Ex = ex, Code = 6, ExecutionTime = DateTime.Now });
                return false;
            }

        }

        private void FilesCopy(DirectoryInfo dir, string copyTo)
        {
            try
            {
                foreach (FileInfo file in dir.GetFiles())
                {
                    if (!File.Exists(copyTo + "\\" + file.Name))
                        file.CopyTo(copyTo + "\\" + file.Name);
                }
            }
            catch (Exception ex)
            {
                this.Logs.Add(new NetworkGeneratorLogItem() { Message = "Errore durante la generazione dei files su filesystem.", Ex = ex, Code = 5, ExecutionTime = DateTime.Now });
            }
        }

        private void DirectoriesCopy(DirectoryInfo dir, string copyTo)
        {
            try
            {
                DirectoryInfo source = new DirectoryInfo(copyTo);
                foreach (DirectoryInfo d in dir.GetDirectories())
                {
                    if (!Directory.Exists(copyTo + "\\" + d.Name))
                        source.CreateSubdirectory(d.Name);
                    DirectoriesCopy(d, copyTo + "\\" + d.Name);
                    FilesCopy(d, copyTo + "\\" + d.Name);
                }
            }
            catch (Exception ex)
            {
                this.Logs.Add(new NetworkGeneratorLogItem() { Message = "Errore durante la generazione delle directory su filesystem.", Ex = ex, Code = 4, ExecutionTime = DateTime.Now });
            }
        }

        public bool GeneratePermessions(int NewNetworkID)
        {
            //foreach (int UserID in this.UsersToAllow)
            //{
            //    try
            //    {
            //        DataRow user = Connection.SqlQuery("SELECT Profile_User FROM Users WHERE id_User = " + UserID).Rows[0];
            //        DataRow root = Connection.SqlQuery("SELECT id_folder FROM folders WHERE Depth_Folder = 1 AND Network_Folder = " + NewNetworkID).Rows[0];
            //        //DataRow root = Connection.SqlQuery("SELECT id_folder FROM " + this.SystemName + "_folders WHERE Depth_Folder = 1").Rows[0];

            //        //string folderprofiles = "INSERT INTO " + this.SystemName + "_folderprofiles (Folder_FolderProfile,Profile_FolderProfile,Creator_FolderProfile)";
            //        string folderprofiles = "INSERT INTO folderprofiles (Folder_FolderProfile,Profile_FolderProfile,Creator_FolderProfile)";
            //        folderprofiles += " VALUES (" + root[0].ToString() + "," + user[0].ToString() + "," + UserID + ")";

            //        int folderProfileID = Connection.ExecuteInsert(folderprofiles);

            //        var criteri = Connection.SqlQuery("SELECT * FROM criteri");

            //        //string folderprofilesCriteria = "INSERT INTO " + this.SystemName + "_folderprofilescriteria (FolderProfile_FolderProfileCriteria,Criteria_FolderProfileCriteria,Value_FolderProfileCriteria,Application_FolderProfileCriteria,Updater_FolderProfileCriteria)";
            //        string folderprofilesCriteria = "INSERT INTO folderprofilescriteria (FolderProfile_FolderProfileCriteria,Criteria_FolderProfileCriteria,Value_FolderProfileCriteria,Application_FolderProfileCriteria,Updater_FolderProfileCriteria)";
            //        folderprofilesCriteria += " VALUES (" + folderProfileID + ",{0},1,0," + UserID + ")";
            //        foreach (DataRow row in criteri.Rows)
            //            Connection.Execute(string.Format(folderprofilesCriteria, row["id_Criterio"].ToString()));
            //    }
            //    catch (Exception ex)
            //    {
            //        this.Logs.Add(new NetworkGeneratorLogItem() { Message = "Errore durante la generazione dei permessi della network.", Ex = ex, Code = 3, ExecutionTime = DateTime.Now });
            //    }
            //}

            //return true;
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                User creator = UsersBusinessLogic.GetById(NetCms.Users.AccountManager.CurrentAccount.ID, sess);
                try
                {
                    foreach (int UserID in this.UsersToAllow)
                    {
                        User utente = UsersBusinessLogic.GetById(UserID, sess);
                        BasicNetwork network = NetCms.Networks.NetworksManager.GlobalIndexer.GetNetworkByID(NewNetworkID);
                        Network net = new StructureNetwork(network);
                        StructureFolder root = FolderBusinessLogic.GetById(sess,net.RootFolder.ID);

                        FolderProfile folderProfile = new FolderProfile();
                        folderProfile.WfsObject = root;
                        folderProfile.Profile = utente.Profile;
                        folderProfile.Creator = creator;
                        

                        var criteri = GrantsBusinessLogic.FindAllCriteri(sess);
                        foreach (Criterio criterio in criteri)
                        {
                            FolderProfileCriteria folderProfileCriteria = new FolderProfileCriteria();
                            folderProfileCriteria.ObjectProfile = folderProfile;
                            folderProfileCriteria.Criteria = criterio;
                            folderProfileCriteria.Value = 1;
                            folderProfileCriteria.Application = 0;
                            folderProfileCriteria.Updater = creator;
                            folderProfile.CriteriaCollection.Add(folderProfileCriteria);
                            //GrantsBusinessLogic.SaveOrUpdateFolderProfileCriteria(folderProfileCriteria);
                        }
                        folderProfile = GrantsBusinessLogic.SaveOrUpdateFolderProfile(folderProfile, sess);
                    }
                    tx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    this.Logs.Add(new NetworkGeneratorLogItem() { Message = "Errore durante la generazione dei permessi della network.", Ex = ex, Code = 3, ExecutionTime = DateTime.Now });
                    return false;
                }
            }
        }

        public void InsertUser(int uid, int nid)
        {
            //try
            //{
            //    string delete = "DELETE FROM users_networks WHERE User_UserNetwork = " + uid + " AND Network_UserNetwork = " + nid + "";
            //    Connection.ExecuteInsert(delete);
            //}
            //catch(Exception ex)
            //{
            //    this.Logs.Add(new NetworkGeneratorLogItem() { Message = "Errore durante la pulizia degli utenti della network.", Ex = ex, Code = 1, ExecutionTime = DateTime.Now });
            //}

            //try
            //{
            //    string users_networks = "INSERT INTO users_networks (User_UserNetwork,Network_UserNetwork)";
            //    users_networks += " VALUES (" + uid + "," + nid + ")";

            //    Connection.ExecuteInsert(users_networks);
            //}
            //catch (Exception ex)
            //{
            //    this.Logs.Add(new NetworkGeneratorLogItem() { Message = "Errore durante l'associazione dell'utente "+uid+" alla network.", Ex = ex, Code = 2, ExecutionTime = DateTime.Now });
            //}

            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    User utente = UsersBusinessLogic.GetById(uid, sess);

                    if (utente.AssociatedNetworks.Count(x => x.Network.ID == nid) > 0) 
                    { 
                        bool resDel = UsersBusinessLogic.DisableNetwork(utente, nid, sess);
                        if (!resDel)
                            throw new Exception("DeleteNetworkException");
                    }
                    
                    bool resIns = UsersBusinessLogic.EnableNetwork(utente, nid, sess);
                    if (!resIns)
                        throw new Exception("InsertNetworkException");

                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    if (ex.Message == "DeleteNetworkException")
                        this.Logs.Add(new NetworkGeneratorLogItem() { Message = "Errore durante la pulizia degli utenti della network.", Ex = ex, Code = 1, ExecutionTime = DateTime.Now });
                    if (ex.Message == "InsertNetworkException")
                        this.Logs.Add(new NetworkGeneratorLogItem() { Message = "Errore durante l'associazione dell'utente " + uid + " alla network.", Ex = ex, Code = 2, ExecutionTime = DateTime.Now });
                }
            }
        }

        public void AllowUser(int uid)
        {
            if (!this.UsersToAllow.Contains(uid))
                this.UsersToAllow.Add(uid);
        }

        private class NetworkDbGenerator
        {
            //private G2Core.Common.StringCollection CreatedTables
            //{
            //    get
            //    {
            //        if (_CreatedTables == null)
            //            _CreatedTables = new G2Core.Common.StringCollection();
            //        return _CreatedTables;
            //    }
            //}
            //private G2Core.Common.StringCollection _CreatedTables;

            private G2Core.Common.StringCollection TablesToCreate
            {
                get
                {
                    if (_TablesToCreate == null)
                        _TablesToCreate = new G2Core.Common.StringCollection();
                    return _TablesToCreate;
                }
            }
            private G2Core.Common.StringCollection _TablesToCreate;

            public G2Core.Common.StringCollection ConstraintsToCreate
            {
                get
                {
                    if (_ConstraintsToCreate == null)
                        _ConstraintsToCreate = new G2Core.Common.StringCollection();
                    return _ConstraintsToCreate;
                }
            }
            private G2Core.Common.StringCollection _ConstraintsToCreate;

            public NetworkGenerator NetworkGenerator
            {
                get
                {
                    return _NetworkGenerator;
                }
            }
            private NetworkGenerator _NetworkGenerator;

            public NetworkDbGenerator(NetworkGenerator ntGen)
            {
                this._NetworkGenerator = ntGen;
            }

            public bool NetworkAlreadyExists { get; private set; } 

            public int Generate()
            {
                int networkID = GenerateNetworkRecord();
                if (networkID > 0)
                {
                    bool ok = true;
                    //ATTENZIONE: La generazione delle tabelle per singola network non esiste più
                    //if (ok)
                    //{
                    //    ok = GenerateTables(networkID);
                    //    if (!ok)
                    //    {
                    //        NetworkGenerator.Logs.Add(new NetworkGeneratorLogItem() { Message = "Impossibile generare il database della network.", Code = 201 });
                    //        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Impossibile generare il database della network.", "", "");
                    //    }
                    //}
                    if (ok)
                    {
                        ok = UpdateNetworkRecord(networkID);
                    
                        if (!ok)
                        {
                            NetworkGenerator.Logs.Add(new NetworkGeneratorLogItem() { Message = "Impossibile aggiornare il record della network.", Code = 202 });
                            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Impossibile aggiornare il record della network.", "", "");
                        }
                    }
                    if (ok)
                    {
                        ok = GenerateRoutines();

                        if (!ok)
                        {
                            NetworkGenerator.Logs.Add(new NetworkGeneratorLogItem() { Message = "Impossibile generare le routines della network.", Code = 203 });
                            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Impossibile generare le routines della network.", "", "");
                        }
                    }
                    if (ok)
                    {
                        return networkID;
                    }
                    else
                    {
                        if (!NetworkAlreadyExists)
                        {
                            NetworkRemover remover = new NetworkRemover(networkID, this.NetworkGenerator.SystemName);
                            remover.DestroyNetwork();
                        }
                    }
                }

                return 0;
            }

            public int GenerateNetworkRecord()
            {
                if (NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.ContainsKey(this.NetworkGenerator.SystemName))
                {
                    if (NetCms.Networks.NetworksManager.GlobalIndexer.WithoutCms.ContainsKey(this.NetworkGenerator.SystemName))
                    {
                        NetworkAlreadyExists = true;
                        return NetCms.Networks.NetworksManager.GlobalIndexer.WithoutCms[this.NetworkGenerator.SystemName].ID;
                    }
                    else
                        this.NetworkGenerator.Logs.Add(new NetworkGeneratorLogItem() { Message = "Impossibile trovare la network tra le network in cache.", Code = 103, ExecutionTime = DateTime.Now });
                }
                else
                {
                    if (this.NetworkGenerator.SystemName.ToLower() != ModelSystemNetworkName.ToLower())
                    {
                        using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                        using (ITransaction tx = sess.BeginTransaction())
                        {
                            try
                            {
                                BasicNetwork nuovaNetwork = new BasicNetwork();
                                nuovaNetwork.Label = this.NetworkGenerator.Label;
                                nuovaNetwork.SystemName = this.NetworkGenerator.SystemName.ToLower();
                                nuovaNetwork.RootPath = this.NetworkGenerator.SystemName.ToLower();
                                nuovaNetwork.Predefinita = this.NetworkGenerator.Default;
                                nuovaNetwork.HasFrontend = this.NetworkGenerator.HasFrontend;
                                nuovaNetwork.Enabled = this.NetworkGenerator.Enabled;
                                nuovaNetwork.DefaultLang = this.NetworkGenerator.DefaultLang;
                                nuovaNetwork = NetworksBusinessLogic.SaveOrUpdateNetwork(nuovaNetwork,sess);
                                //string sql = "INSERT INTO Networks (Label_Network,Nome_Network,WebRoot_Network,Default_Network,Frontend_Network,Enabled_Network,DefaultLang_Network)";
                                //string values = " VALUES ('" + this.NetworkGenerator.Label + "','" + this.NetworkGenerator.SystemName + "','" + this.NetworkGenerator.SystemName + "','" + (this.NetworkGenerator.Default ? 1 : 0) + "'," + (this.NetworkGenerator.HasFrontend ? 1 : 0) + "," + (this.NetworkGenerator.Enabled ? 1 : 0) + "," + this.NetworkGenerator.DefaultLang + ")";
                                 //this.NetworkGenerator.Connection.ExecuteInsert(sql + values, true);
                                tx.Commit();

                                int NewNetworkID = nuovaNetwork.ID;

                                NetCms.Networks.NetworksManager.GlobalIndexer.Update();
                                NetworkAlreadyExists = false; 
                                return NewNetworkID;
                            }
                            catch (Exception ex)
                            {
                                tx.Rollback();
                                this.NetworkGenerator.Logs.Add(new NetworkGeneratorLogItem() { Message = "Impossibile generare record della network.", Ex = ex, Code = 13, ExecutionTime = DateTime.Now });
                                return 0;
                            }
                        }
                    }
                    else
                    {
                        this.NetworkGenerator.Logs.Add(new NetworkGeneratorLogItem() { Message = "Impossibile generare il database.", Code = 14, ExecutionTime = DateTime.Now });
                        return 0;
                    }
                }
                return 0;
            }
            
            private bool UpdateNetworkRecord(int networkID)
            {
                try
                {
                    BasicNetwork network = NetworksBusinessLogic.GetById(networkID);
                    network.DisableCms();

                    //string sql = "UPDATE Networks SET Cms_Network = 2 WHERE id_Network = " + networkID;
                    //bool ok = this.NetworkGenerator.Connection.Execute(sql, true);
                    //NetCms.Networks.NetworksManager.GlobalIndexer.Update();
                    return true;
                }
                catch (Exception ex)
                {
                    this.NetworkGenerator.Logs.Add(new NetworkGeneratorLogItem() { Message = "Impossibile aggiornare record della network.", Ex = ex, Code = 15, ExecutionTime = DateTime.Now });
                    return false;
                }
            }

            private bool GenerateRoutines()
            {
                try
                {
                    GrantsProceduresInstaller routinesInstaller = new GrantsProceduresInstaller();
                    bool generated = routinesInstaller.GenerateProcedures();
                    //bool generated = routinesInstaller.GenerateNetworkProcedures(this.NetworkGenerator.SystemName);

                    if (!generated)
                        this.NetworkGenerator.Logs.Add(new NetworkGeneratorLogItem() { Message = "Impossibile generare le stored procedures per la network.", Code = 16, ExecutionTime = DateTime.Now });
                    return generated;
                }
                catch (Exception ex)
                {
                    this.NetworkGenerator.Logs.Add(new NetworkGeneratorLogItem() { Message = "Impossibile generare le stored procedures per la network.", Ex = ex, Code = 17, ExecutionTime = DateTime.Now });
                    return false;
                }
            }

            //private bool GenerateTables(int networkID)
            //{
            //    return CreateTables(networkID);
            //}

//            private bool CreateTables(int NewNetworkID)
//            {
//                try
//                {
//                    DataTable schema = this.NetworkGenerator.Connection.SqlQuery(@"
//                                 SELECT table_name
//                                 FROM information_schema.tables
//                                 WHERE table_name LIKE '" + ModelSystemNetworkName + @"_%'
//                                 AND TABLE_SCHEMA = '" + this.NetworkGenerator.Connection.DatabaseName + @"' 
//                                ORDER BY table_name DESC
//                                ");
//                    foreach (DataRow r in schema.Rows)
//                    {
//                        string newTableName = ReplaceTableName(r["table_name"].ToString());
//                        CreateTable(r["table_name"].ToString(), newTableName);
//                    }
//                    //ExecuteStoredProcedure(ExecutedQuerys);
//                    return ExecuteQuerys();
//                }
//                catch (Exception ex)
//                {
//                    NetworkRemover remover = new NetworkRemover(NewNetworkID.ToString(), this.NetworkGenerator.SystemName);
//                    remover.DestroyNetwork();
                    
//                    this.NetworkGenerator.Logs.Add(new NetworkGeneratorLogItem() { Message = "Impossibile generare una o più tabelle del database.", Ex = ex, Code = 17, ExecutionTime = DateTime.Now });

//                    return false;
//                }
//            }
            //private int FkCount = 0;
            //private bool CreateTable(string source_name, string table_name)
            //{
            //    DataTable key = this.NetworkGenerator.Connection.SqlQuery("SELECT DISTINCT * FROM information_schema.KEY_COLUMN_USAGE WHERE  TABLE_NAME = '" + source_name + "' AND CONSTRAINT_SCHEMA = '" + this.NetworkGenerator.Connection.DatabaseName + "' AND CONSTRAINT_NAME <> 'PRIMARY'");

            //    string //sqlcreate = "DROP TABLE IF EXISTS " + table_name + ";";
            //    sqlcreate = "CREATE TABLE IF NOT EXISTS " + table_name + " LIKE " + source_name + ";";
            //    this.TablesToCreate.Add(sqlcreate);

            //    foreach (DataRow r in key.Rows)
            //    {
            //        string constraintTableName = this.ReplaceTableName(r["REFERENCED_TABLE_NAME"].ToString());
            //        string constraintName = this.NetworkGenerator.SystemName + "_fk" + FkCount++;

            //        DataTable existingConstraint = this.NetworkGenerator.Connection.SqlQuery("SELECT DISTINCT * FROM information_schema.KEY_COLUMN_USAGE WHERE  TABLE_NAME = '" + table_name + "' AND CONSTRAINT_SCHEMA = '" + this.NetworkGenerator.Connection.DatabaseName + "' AND CONSTRAINT_NAME = '" + constraintName + "'");
            //        if (existingConstraint.Rows.Count == 0)
            //        {
            //            string sql = "ALTER TABLE " + table_name + " ADD CONSTRAINT " + constraintName + " FOREIGN KEY (" + r["COLUMN_NAME"] + ") REFERENCES " + constraintTableName + " (" + r["REFERENCED_COLUMN_NAME"] + ") ON UPDATE CASCADE ON DELETE CASCADE;";
            //            this.ConstraintsToCreate.Add(sql);
            //        }
            //    }
            //    //this.CreatedTables.Add(table_name);

            //    return true;
            //}

            //private void ExecuteQuerys()
            //{
            //    foreach (string sql in this.TablesToCreate)
            //        ExecuteStoredProcedure(sql);

            //    foreach (string sql in this.ConstraintsToCreate)
            //        ExecuteStoredProcedure(sql);
            //}

            //private bool ExecuteStoredProcedure(string sql)
            //{
            //    System.Data.Common.DbParameter par = this.NetworkGenerator.Connection.CreateParameter("in_script", sql, ParameterDirection.Input);
            //    List<System.Data.Common.DbParameter> parameters = new List<System.Data.Common.DbParameter>();

            //    parameters.Add(par);

            //    return this.NetworkGenerator.Connection.ExecuteCommand("CALL createtables (?)", CommandType.StoredProcedure, parameters);
            //}

            //private bool ExecuteQuerys()
            //{
            //    //string sqlScript = "SET foreign_key_checks = 0;\n";

            //    bool ok = true;
            //    foreach (string sql in this.TablesToCreate)
            //        //sqlScript += sql + "\n";
            //        ok = ok && ExecuteStoredProcedure(sql);

            //    foreach (string sql in this.ConstraintsToCreate)
            //        //sqlScript += sql + "\n";
            //        ok = ok && ExecuteStoredProcedure(sql);

            //    //sqlScript += "SET foreign_key_checks = 1;\n";
            //    return ok;
            //    //return ExecuteStoredProcedure(sqlScript);

            //}

            //private bool ExecuteStoredProcedure(string sql)
            //{
            //    System.Data.Common.DbParameter par = this.NetworkGenerator.Connection.CreateParameter("in_script", sql, ParameterDirection.Input);
            //    List<System.Data.Common.DbParameter> parameters = new List<System.Data.Common.DbParameter>();
            //    parameters.Add(par);

            //    bool ok = this.NetworkGenerator.Connection.ExecuteCommand("CALL createtables (?)", CommandType.StoredProcedure, parameters);
            //    if (!ok) Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Eseguire la stored procedure createtables, con la query '" + sql + "'.", "", "");
            //    return ok;
            //    //return this.NetworkGenerator.Connection.ExecuteCommand(sql, CommandType.Text, parameters);
            //}

            //private string ReplaceTableName(string tablename)
            //{
            //    return tablename.ToString().Replace(ModelSystemNetworkName + "_", this.NetworkGenerator.SystemName + "_");
            //}
        }
    }

    public class NetworkGeneratorLogItem
    {
        public string Message { get; set; }
        public int Code { get; set; }
        public Exception Ex { get; set; } 
        public DateTime ExecutionTime { get; set; } 
    }
}
