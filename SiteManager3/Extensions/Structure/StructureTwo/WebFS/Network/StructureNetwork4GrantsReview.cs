﻿using System;
using System.Web;
using System.Web.Caching;
using System.Collections.Generic;
using System.Data;
using System.Text;
using NetCms.Connections;

namespace NetCms
{
    namespace Structure.WebFS
    {
        public class StructureNetwork4GrantsReview : StructureNetwork
        {
            private Networks.NetworkKey _CacheKey;
            public override Networks.NetworkKey CacheKey
            {
                get 
                {
                    if (_CacheKey == null)
                        _CacheKey = new Networks.NetworkKey(this.ID, G2Core.Caching.Visibility.CachingVisibility.Global, "[AboutUser-" + Account.ID + "]");
                    return _CacheKey;
                }
            }

            public Users.User Account
            {
                get { return _Account; }
                private set { _Account = value; }
            }
            private Users.User _Account;

            public StructureNetwork4GrantsReview(int id, Users.User account) : base(id) { Account = account; }
            public StructureNetwork4GrantsReview(string name, Users.User account) : base(name) { Account = account; }
        }
    }
}
