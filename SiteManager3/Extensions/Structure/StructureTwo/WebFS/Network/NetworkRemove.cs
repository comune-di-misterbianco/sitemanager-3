﻿using System;
using System.IO;
using System.Xml;
using NetCms.Connections;
using NHibernate;
using NetCms.Users;
using NetCms.Networks.WebFS;
using NetCms.Grants;


namespace NetCms.Networks
{
    public class NetworkRemover
    {
        public int NetworkID
        {
            get { return _NetworkID; }
            private set { _NetworkID = value; }
        }
        private int _NetworkID;

        public string NetworkSystemName
        {
            get { return _NetworkSystemName; }
            private set { _NetworkSystemName = value; }
        }
        private string _NetworkSystemName;



        public NetworkRemover(string systemName)
        {
            this.NetworkSystemName = systemName;
            BasicNetwork net = NetworksBusinessLogic.GetByName(systemName);
            if (net != null)
                this.NetworkID = net.ID;
            //string sql = "SELECT id_Network FROM Networks WHERE Nome_Network = '{0}'".FormatByParameters(systemName);
            //var table = Connection.SqlQuery(sql);
            //if (table.Rows.Count > 0)
            //{
            //    this.NetworkID = int.Parse(table.Rows[0][0].ToString());
            //}
        }

        public NetworkRemover(int networkID, string systemName)
        {
            this.NetworkID = networkID;
            this.NetworkSystemName = systemName;
        }

        public bool DestroyNetwork()
        {            
            if (NetworkID != 1 && NetworkSystemName != "internet")
            {
                 using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                 using (ITransaction tx = sess.BeginTransaction())
                 {
                     try
                     {

                         //var network = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks[this.NetworkSystemName];
                         var network = NetworksBusinessLogic.GetByName(this.NetworkSystemName, sess);
                         //StructureNetwork structureNetwork = new StructureNetwork(network);
                         //NetCms.Structure.TrashBusinessLogic.TrashBusinessLogic.DeleteFolder(structureNetwork.RootFolderID, network);


                         string FolderPath = Configurations.Paths.AbsoluteConfigRoot + "\\" + this.NetworkSystemName;
                         //Cancello la cartella di configurazione della Portale
                         if (Directory.Exists(FolderPath)) Directory.Delete(FolderPath, true);


                         XmlNode Networks = Configurations.XmlConfig.GetNodeContent("/Portal/configs/Networks");
                         XmlNode Network = Configurations.XmlConfig.GetNodeContent("/Portal/configs/Networks/" + this.NetworkSystemName);
                         if (Networks != null && Network != null)
                         {
                             Networks.RemoveChild(Network);
                             Configurations.XmlConfig.Save();
                         }

                         UsersBusinessLogic.DisableAllUsersByNetwork(network.ID,sess);

                         //Cancellazione profili cartelle e documenti per i permessi
                         foreach(NetworkFolder fold in network.NetworkFolders)
                             GrantsBusinessLogic.DeleteFolderProfilesByFolder(fold.ID, network.ID, sess);

                         //Cancellazione profili applicazioni verticali di network
                         ExternalAppzGrantsBusinessLogic.DeleteExternalAppProfilesByNetwork(network.ID, sess);

                         NetworksBusinessLogic.DeleteNetwork(network,sess);
                         
                         //ATTENZIONE IMPEDIRE QUESTA AZIONE POICHE' CANCELLEREBBE LE SP DI TUTTE LE NETWORK
                         //GrantsProceduresInstaller routinesInstaller = new GrantsProceduresInstaller();
                         //routinesInstaller.DeleteProcedures();
                         //routinesInstaller.DeleteNetworkProcedures(this.NetworkSystemName);

                         G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
                         cache.RemoveAll("");
                         tx.Commit();
                     }
                     catch (Exception e)
                     {
                         tx.Rollback();
                         return false;
                     }
                 }
                //this.Connection.Execute("DELETE FROM Networks WHERE id_Network = " + this.NetworkID);
                 NetCms.Networks.NetworksManager.GlobalIndexer.Update();
                

            }
            return true;


        }

    }
}
