﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Grants.Mapping
{
    public class CriterioMapping : ClassMap<Criterio>
    {
        public CriterioMapping()
        {
            Table("criteri");
            Cache.NonStrictReadWrite();
            Id(x => x.ID).Column("id_Criterio");
            Map(x => x.Nome).Column("Nome_Criterio");
            Map(x => x.Key).Column("Key_Criterio");
            Map(x => x.Tipo).Column("Tipo_Criterio");
            Map(x => x.ForDoc).Column("ForDoc_Criterio");
        }
    }
}
