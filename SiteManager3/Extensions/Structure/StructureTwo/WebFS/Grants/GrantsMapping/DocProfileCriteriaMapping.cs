﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Grants.Mapping
{
    public class DocProfileCriteriaMapping : SubclassMap<DocProfileCriteria>
    {
        public DocProfileCriteriaMapping()
        {
            Table("doc_profiles_criteria");
            KeyColumn("id_DocProfileCriteria");

            References(x => x.ObjectProfile).Class(typeof(DocProfile))
                 .ForeignKey("object_profiles_criteria_docprofile")
                 .Column("DocProfile_ObjectProfileCriteria")
                 .Fetch.Select();

            References(x => x.Criteria).Class(typeof(Criterio))
                 .ForeignKey("doc_profiles_criteria_criterio")
                 .Column("Criteria_ObjectProfileCriteria")
                 .Fetch.Select();
        }
    }
}
