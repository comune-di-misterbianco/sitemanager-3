﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Networks.WebFS;
using NHibernate.Collection;
using NetCms.Structure.WebFS;

namespace NetCms.Grants.Mapping
{
    public class DocProfileMapping : SubclassMap<DocProfile>
    {
        public DocProfileMapping()
        {
            Table("doc_profiles");
            KeyColumn("id_DocProfile");

            References(x => x.WfsObject).Class(typeof(Document))
                 .ForeignKey("object_profiles_document")
                 .Column("Document_ObjectProfile")
                 .Fetch.Select();

            HasMany<DocProfileCriteria>(x => x.CriteriaCollection)
                .KeyColumn("DocProfile_ObjectProfileCriteria")
                .Fetch.Select().AsSet()
                .Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
