﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Grants.Mapping
{
    public class FolderProfileCriteriaMapping : SubclassMap<FolderProfileCriteria>
    {
        public FolderProfileCriteriaMapping()
        {
            Table("folder_profiles_criteria");
            KeyColumn("id_FolderProfileCriteria");

            Map(x => x.Application).Column("Application_FolderProfileCriteria");

            References(x => x.ObjectProfile).Class(typeof(FolderProfile))
                 .ForeignKey("object_profiles_criteria_folderprofile")
                 .Column("FolderProfile_ObjectProfileCriteria")
                 .Fetch.Select();

            References(x => x.Criteria).Class(typeof(Criterio))
                 .ForeignKey("folder_profiles_criteria_criterio")
                 .Column("Criteria_ObjectProfileCriteria")
                 .Fetch.Select();
        }
    }
}
