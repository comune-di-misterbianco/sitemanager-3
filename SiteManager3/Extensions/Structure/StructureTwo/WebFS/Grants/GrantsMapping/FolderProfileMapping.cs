﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Networks.WebFS;
using NetCms.Structure.WebFS;

namespace NetCms.Grants.Mapping
{
    public class FolderProfileMapping : SubclassMap<FolderProfile>
    {
        public FolderProfileMapping()
        {
            Table("folder_profiles");
            KeyColumn("id_FolderProfile");

            References(x => x.WfsObject).Class(typeof(StructureFolder))
                 .ForeignKey("object_profiles_folder")
                 .Column("Folder_ObjectProfile")
                 .Fetch.Select();

            HasMany<FolderProfileCriteria>(x => x.CriteriaCollection)
                .KeyColumn("FolderProfile_ObjectProfileCriteria")
               .Fetch.Select().AsSet()
               .Cascade.AllDeleteOrphan().Inverse();
        }
    }
}
