﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Users;
using System.Data;
using NetCms.Networks.WebFS;
using NetCms.Grants;
using NHibernate;

namespace NetCms.Networks.Grants
{
    public class DocumentGrants : GrantsFinder
    {
        private static ISession Session
        {
            get;
            set;
        }
        

        public IDocument Document { get; private set; }

        public DocumentGrants(IDocument document, Profile profile, ISession innerSession = null)
            : base(profile)
        {
            this.Document = document;
            Session = innerSession;
        }

        protected static Dictionary<string, int> Criteri
        {
            get
            {
                return _Criteri ?? (_Criteri = InitCriteri());
            }
        }

        private static Dictionary<string, int> InitCriteri()
        {
            //var dic = new Dictionary<string, int>();

            //string sql = @"SELECT * FROM criteri";

            //var result = NetCms.Connections.ConnectionsManager.CommonConnection.SqlQuery(sql);
            //foreach (System.Data.DataRow row in result.Rows) dic.Add(row["Key_Criterio"].ToString(), int.Parse(row["id_Criterio"].ToString()));

            //return dic;

            var criteri = GrantsBusinessLogic.FindAllCriteri(Session);
            return criteri.ToDictionary(x => x.Key, x => x.ID);

        }
        private static Dictionary<string, int> _Criteri;

        public static void RefreshCriteri()
        {
            _Criteri = null;
        }

        public override GrantValue CheckForGrant(string criterio)
        {
//            string sql = @"SELECT fpc.Value_FolderProfileCriteria AS val
//                           FROM ((folders INNER JOIN network_folders ON folders.id_Folder = network_folders.id_NetworkFolder) INNER JOIN folderprofiles fp ON fp.Folder_FolderProfile = folders.id_Folder) 
//                           LEFT JOIN folderprofilescriteria fpc ON fp.id_FolderProfile = fpc.FolderProfile_FolderProfileCriteria
//                           WHERE Network_Folder = {0}
//                           AND fpc.Criteria_FolderProfileCriteria = {1}
//                           AND fp.Profile_FolderProfile = {2}
//                           AND fp.Folder_FolderProfile = {3};";

            string sql = @"SELECT object_profiles_criteria.Value_ObjectProfileCriteria AS val
                            FROM (
                            (network_docs LEFT JOIN network_folders ON network_docs.Folder_Doc = network_folders.id_NetworkFolder) 
                            INNER JOIN 
                            (doc_profiles INNER JOIN object_profiles ON id_DocProfile = id_ObjectProfile) 
                            ON doc_profiles.Document_ObjectProfile = network_docs.id_NetworkDoc) 
                            LEFT JOIN (doc_profiles_criteria INNER JOIN object_profiles_criteria ON id_DocProfileCriteria = id_ObjectProfileCriteria) 
                            ON doc_profiles.id_DocProfile = doc_profiles_criteria.DocProfile_ObjectProfileCriteria
                            WHERE Network_Folder = {0}
                            AND doc_profiles_criteria.Criteria_ObjectProfileCriteria = {1}
                            AND object_profiles.Profile_ObjectProfile = {2}
                            AND doc_profiles.Document_ObjectProfile = {3};";

            var result = Conn.SqlQuery(string.Format(sql,
                                                           this.Document.NetworkID,  //0
                                                           GetIdCriterio(criterio),         //1
                                                           Profile.ID,                      //2
                                                           Document.ID                        //3
                                                           ));

            return new GrantValue(result == null || result.Rows.Count == 0 ? "0" : result.Rows[0]["val"].ToString());
        }

        protected override GrantValue GrantValueForGroup(string criterio, int network = 0)
        {
            var parDoc = Conn.CreateParameter("in_doc", Document.ID, ParameterDirection.Input);
            var parCriterio = Conn.CreateParameter("in_criterio", GetIdCriterio(criterio), ParameterDirection.Input);
            var parNetwork = Conn.CreateParameter("in_network_id", this.Document.NetworkID, ParameterDirection.Input);
            var parGroup = Conn.CreateParameter("in_group_tree", (Profile as GroupProfile).Group.Tree, ParameterDirection.Input);
            var parValue = Conn.CreateParameter("out_value", 0, ParameterDirection.Output);
         //   var parWeight = Connection.CreateParameter("out_weight", 0, ParameterDirection.Output);
            

            var pars = new[] { parDoc, parCriterio, parNetwork, parGroup,parValue/*, parWeight*/ }.ToList();

            var result = Conn.ExecuteCommand("sp_doc_grant_value_by_group", CommandType.StoredProcedure, pars, false);

            return new GrantValue(parValue.Value.ToString());
        }

        protected override int GetIdCriterio(string criterio)
        {

            int idCriterio = 0;
            if (!int.TryParse(criterio, out idCriterio))
            {
                //lock (Criteri)
                //{
                    idCriterio = Criteri[criterio];
                //}
            }

            return idCriterio;
        }

        protected override GrantValue GrantValueForUser(string criterio, int network = 0)
        {
            var parDoc = Conn.CreateParameter("in_doc", this.Document.ID, ParameterDirection.Input);
            var parCriterio = Conn.CreateParameter("in_criterio", GetIdCriterio(criterio), ParameterDirection.Input);
            var parNetwork = Conn.CreateParameter("in_network_id", this.Document.NetworkID, ParameterDirection.Input);
            var parUser = Conn.CreateParameter("in_user", (Profile as UserProfile).User.ID, ParameterDirection.Input);
            

            var parValue = Conn.CreateParameter("out_value", null, ParameterDirection.Output);

            var pars = new[] { parDoc, parCriterio, parNetwork, parUser,parValue }.ToList();

            var result = Conn.ExecuteCommand("sp_doc_grant_value_by_user", CommandType.StoredProcedure, pars, false);

            return new GrantValue(parValue.Value.ToString());
        }

        public override GrantValue NeedToShow(string criterio, int network = 0)
        {
            throw new NotImplementedException();
        }
    }
}
