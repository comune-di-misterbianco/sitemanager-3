﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Users;
using System.Data;
using NetCms.Networks.WebFS;
using NetCms.Grants;
using NHibernate;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS;

namespace NetCms.Networks.Grants
{
    public class FolderGrants : GrantsFinder
    {
        public IFolder Folder { get; private set; }

        private ISession session;

        public FolderGrants(IFolder folder, Profile profile, ISession _session = null)
            : base(profile)
        {
            if (_session == null)
                session = FolderBusinessLogic.GetCurrentSession();
            else
                session = _session;
            this.Folder = folder;
        }

        protected Dictionary<string, int> Criteri
        {
            get
            {
                return _Criteri ?? (_Criteri = InitCriteri(session));
            }
        }

        private Dictionary<string, int> InitCriteri(ISession sess)
        {
            //var dic = new Dictionary<string, int>();

            //string sql = @"SELECT * FROM criteri";

            //var result = NetCms.Connections.ConnectionsManager.CommonConnection.SqlQuery(sql);
            //foreach (System.Data.DataRow row in result.Rows) dic.Add(row["Key_Criterio"].ToString(), int.Parse(row["id_Criterio"].ToString()));

            //return dic;

            var criteri = GrantsBusinessLogic.FindAllCriteri(sess);
            return criteri.ToDictionary(x => x.Key, x => x.ID);

        }
        private static Dictionary<string, int> _Criteri;

        public static void RefreshCriteri()
        {
            _Criteri = null;
        }

        public override GrantValue CheckForGrant(string criterio)
        {
            //            string sql = @"SELECT fpc.Value_ObjectProfileCriteria AS val
            //                               FROM (
            //                                (folders INNER JOIN network_folders ON folders.id_Folder = network_folders.id_NetworkFolder) 
            //                               INNER JOIN 
            //                                (folder_profiles INNER JOIN object_profiles ON id_FolderProfile = id_ObjectProfile) fp ON fp.Folder_ObjectProfile = folders.id_Folder) 
            //                               LEFT JOIN (folder_profiles_criteria INNER JOIN object_profiles_criteria ON id_FolderProfileCriteria = id_ObjectProfileCriteria) fpc ON fp.id_FolderProfile = fpc.FolderProfile_ObjectProfileCriteria
            //                               WHERE Network_Folder = {0}
            //                               AND fpc.Criteria_ObjectProfileCriteria = {1}
            //                               AND fp.Profile_ObjectProfile = {2}
            //                               AND fp.Folder_ObjectProfile = {3};";

            string sql = @"SELECT object_profiles_criteria.Value_ObjectProfileCriteria AS val
                            FROM (network_folders INNER JOIN (folder_profiles INNER JOIN object_profiles ON id_FolderProfile = id_ObjectProfile) 
                            ON folder_profiles.Folder_ObjectProfile = network_folders.id_NetworkFolder) 
                            LEFT JOIN (folder_profiles_criteria INNER JOIN object_profiles_criteria ON id_FolderProfileCriteria = id_ObjectProfileCriteria) ON folder_profiles.id_FolderProfile = folder_profiles_criteria.FolderProfile_ObjectProfileCriteria
                            WHERE Network_Folder = {0}
                            AND folder_profiles_criteria.Criteria_ObjectProfileCriteria = {1}
                            AND object_profiles.Profile_ObjectProfile = {2}
                            AND folder_profiles.Folder_ObjectProfile = {3};";

            var result = Conn.SqlQuery(string.Format(sql,
                                                           this.Folder.NetworkID,  //0
                                                           GetIdCriterio(criterio),         //1
                                                           Profile.ID,                      //2
                                                           Folder.ID                        //3
                                                           ));

            return new GrantValue(result == null || result.Rows.Count == 0 ? "0" : result.Rows[0]["val"].ToString());
        }

        protected override GrantValue GrantValueForGroup(string criterio, int network = 0)
        {
           
            var parTreeFolder = Conn.CreateParameter("in_tree_folder", (this.Folder as StructureFolder).Tree, ParameterDirection.Input);
            var parCriterio = Conn.CreateParameter("in_criterio", GetIdCriterio(criterio), ParameterDirection.Input);
            var parNetwork = Conn.CreateParameter("in_network_id", this.Folder.NetworkID, ParameterDirection.Input);
            var parGroup = Conn.CreateParameter("in_group_tree", (Profile as GroupProfile).Group.Tree, ParameterDirection.Input);
            
            var parValue = Conn.CreateParameter("out_value", 0, ParameterDirection.Output);
            
            //var parWeight = Connection.CreateParameter("out_weight", 0, ParameterDirection.Output);

            //var pars = new[] { parFolder, parCriterio, parNetwork, parGroup, parValue, parWeight }.ToList();
            var pars = new[] { parTreeFolder, parCriterio, parNetwork, parGroup,parValue }.ToList();

            var result = Conn.ExecuteCommand("sp_folder_grant_value_by_group", CommandType.StoredProcedure, pars, false);

            return new GrantValue(parValue.Value.ToString());
        }

        protected override int GetIdCriterio(string criterio)
        {

            int idCriterio = 0;
            if (!int.TryParse(criterio, out idCriterio))
            {
                //lock (Criteri)
                //{
                    idCriterio = Criteri[criterio];
                //}
            }

            return idCriterio;
        }

        protected override GrantValue GrantValueForUser(string criterio, int network = 0)
        {
            StructureFolder currentFolder = FolderBusinessLogic.GetById(this.Folder.ID);

            //var parTreeFolder = Connection.CreateParameter("in_tree_folder", (this.Folder as StructureFolder).Tree, ParameterDirection.Input);
            var parTreeFolder = Conn.CreateParameter("in_tree_folder", currentFolder.Tree, ParameterDirection.Input);
            var parCriterio = Conn.CreateParameter("in_criterio", GetIdCriterio(criterio), ParameterDirection.Input);
            var parNetwork = Conn.CreateParameter("in_network_id", currentFolder.NetworkID, ParameterDirection.Input);
            var parUser = Conn.CreateParameter("in_user", (Profile as UserProfile).User.ID, ParameterDirection.Input);
            
            var parValue = Conn.CreateParameter("out_value", null, ParameterDirection.Output);

            var pars = new[] { parTreeFolder, parCriterio, parNetwork, parUser,  parValue }.ToList();

            //string parametri = "in_path_folder" + this.Folder.Path + "in_tree_folder" + (this.Folder as StructureFolder).Tree + "in_criterio: " + GetIdCriterio(criterio) + " - in_network_id: " + this.Folder.NetworkID + " - in_user: " + (Profile as UserProfile).User.ID + " - in_winning_value: " + (Configurations.Generics.MostValuableGrant ? 1 : 2);

            //TimeSpan tstart = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

            var result = Conn.ExecuteCommand("sp_folder_grant_value_by_user", CommandType.StoredProcedure, pars, false);

            //TimeSpan tend = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            //NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Info, "Esecuzione stored procedure GrantValueForUser con parametri: " + parametri + " impiegando: " + tend.Subtract(tstart).Seconds + "s", NetCms.Users.AccountManager.CurrentAccount != null ? NetCms.Users.AccountManager.CurrentAccount.ID.ToString() : "", NetCms.Users.AccountManager.CurrentAccount != null ? NetCms.Users.AccountManager.CurrentAccount.UserName : "");

            return new GrantValue(parValue.Value.ToString());
        }

        public override GrantValue NeedToShow(string criterio, int network = 0)
        {
                        
            var parCriterio = Conn.CreateParameter("in_criterio", GetIdCriterio(criterio), ParameterDirection.Input);
            var parUser = Conn.CreateParameter("in_user", (Profile as UserProfile).User.ID, ParameterDirection.Input);
      
            var parTreeFolder = Conn.CreateParameter("in_tree_folder", (this.Folder as StructureFolder).Tree, ParameterDirection.Input);
            var parNetwork = Conn.CreateParameter("in_network_id", this.Folder.NetworkID, ParameterDirection.Input);
            var parValue = Conn.CreateParameter("out_value", null, ParameterDirection.Output);

            var pars = new[] {  parTreeFolder, parCriterio, parNetwork, parUser, parValue }.ToList();

            //string parametri = "in_path_folder" + this.Folder.Path + "in_tree_folder" + (this.Folder as StructureFolder).Tree + "in_criterio: " + GetIdCriterio(criterio) + " - in_network_id: " + this.Folder.NetworkID + " - in_user: " + (Profile as UserProfile).User.ID + " - in_winning_value: " + (Configurations.Generics.MostValuableGrant ? 1 : 2);

            //TimeSpan tstart = new TimeSpan(DateTime.Now.Hour,DateTime.Now.Minute,DateTime.Now.Second);

            var result = Conn.ExecuteCommand("sp_folder_need_to_show", CommandType.StoredProcedure, pars, false);

            //TimeSpan tend = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

            //NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Info, "Esecuzione stored procedure NeedToShow con parametri: " + parametri + " impiegando: " + tend.Subtract(tstart).Seconds + "s", NetCms.Users.AccountManager.CurrentAccount != null ? NetCms.Users.AccountManager.CurrentAccount.ID.ToString() : "", NetCms.Users.AccountManager.CurrentAccount != null? NetCms.Users.AccountManager.CurrentAccount.UserName:"");
            return new GrantValue(parValue.Value.ToString());
        }
    }
}