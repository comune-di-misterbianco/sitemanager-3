using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using HtmlGenericControls;
using NetCms.GUI;
using NetCms.Vertical.Grants;
using NetCms.Structure.Grants;
using NetCms.Users;
using NetCms.Structure.WebFS;
using NetCms.Networks.WebFS;
using System.Linq;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Grants;
using System.Collections.Generic;
using NetCms.Vertical;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.Structure.Grants
{
    public partial class GrantsReview:NetworkPage
    {
        #region RootFolder & Account

        private StructureFolder _RootFolder;
        public StructureFolder RootFolder
        {
            get
            {
                if (_RootFolder == null)
                {
                    //NetCms.Structure.WebFS.StructureNetwork4GrantsReview network = new StructureNetwork4GrantsReview(Networks.NetworksManager.CurrentActiveNetwork.ID, this.Account);
                    //_RootFolder = network.RootFolder;
                    
                    _RootFolder = this.CurrentNetwork.RootFolder;
                }
                return _RootFolder;
            }
        }


        #endregion

        #region Profile

        private Profile _Profile;
        private Profile Profile
        {
            get
            {
                return _Profile;
            }
        }

        #endregion

        #region Selected Criteria

        private string _SelectedCriteria;
        private string SelectedCriteria
        {
            get
            {
                if (_SelectedCriteria == null)
                    InitSelectedCriteria();
                return _SelectedCriteria;
            }
        }

        private void InitSelectedCriteria()
        {
            NetUtility.RequestVariable SelectedCriteriaRequest = new NetUtility.RequestVariable("SelectedCriteria", NetUtility.RequestVariable.RequestType.Form);
            if (SelectedCriteriaRequest.IsValidString && SelectedCriteriaRequest.StringValue.Length > 0)

                _SelectedCriteria = SelectedCriteriaRequest.StringValue;
            else
                _SelectedCriteria = "show";

            //DataTable cTab = this.RootFolder.Connection.SqlQuery("SELECT * FROM Criteri WHERE Key_Criterio = '" + _SelectedCriteria + "'");
            //if (cTab.Rows.Count > 0)
            //    _SelectedCriteriaForDoc = cTab.Rows[0]["ForDoc_Criterio"].ToString() == "1";
            Criterio criterio = GrantsBusinessLogic.GetCriterioByKey(_SelectedCriteria);
            if (criterio != null)
                _SelectedCriteriaForDoc = criterio.ForDoc;
        }

        private bool _SelectedCriteriaForDoc;
        private bool SelectedCriteriaForDoc
        {
            get
            {
                if (_SelectedCriteria == null)
                    InitSelectedCriteria();
                return _SelectedCriteriaForDoc;
            }
        }
        #endregion

        public override string LocalCssClass
        {
            get { return ""; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Key; }
        }

        //private string _NomeProfilo;
        //private string NomeProfilo
        //{
        //    get
        //    {
        //        if (_NomeProfilo == null)
        //        {
        //            _NomeProfilo = Profile.IsGroup ? Profile.Group.Name: Profile.User.NomeCompleto;
        //        }
        //        return _NomeProfilo;
        //    }
        //}

        private string _NomeProfilo;
        private string NomeProfilo
        {
            get
            {
                if (_NomeProfilo == null)
                {
                    _NomeProfilo = Profile.EntityName;
                }
                return _NomeProfilo;
            }
        }

        private string SelectedEntityName
        {
            get
            {
                if (this.Profile.IsUser)
                    return "Utente";
                return "Gruppo";
            }
        }
        
        //private DataTable _FolderProfiles;
        //public DataTable FolderProfiles
        //{
        //    get
        //    {
        //        if (_FolderProfiles == null)
        //        {
        //            _FolderProfiles = PageData.Conn.SqlQuery("SELECT * FROM FolderProfiles");
        //        }
        //        return _FolderProfiles;
        //    }
        //}
        
        //private DataTable _GroupsFolderProfiles;
        //public DataTable GroupsFolderProfiles
        //{
        //    get
        //    {
        //        if (_GroupsFolderProfiles == null)
        //        {
        //            _GroupsFolderProfiles = PageData.Conn.SqlQuery("SELECT * FROM FolderProfiles INNER JOIN Groups ON Profile_Group = Profile_FolderProfile");
        //        }
        //        return _GroupsFolderProfiles;
        //    }
        //}
        
        //private DataTable _DocProfiles;
        //public DataTable DocProfiles
        //{
        //    get
        //    {
        //        if (_DocProfiles == null)
        //        {
        //            _DocProfiles = PageData.Conn.SqlQuery("SELECT * FROM DocProfiles");
        //        }
        //        return _DocProfiles;
        //    }
        //}
        
        public GrantsReview()
        {
            G2Core.Common.RequestVariable profileID = new G2Core.Common.RequestVariable("pid", G2Core.Common.RequestVariable.RequestType.QueryString);
            _Profile = ProfileBusinessLogic.GetById(profileID.IntValue);
            //ExternalAppzGrantsChecker Checker = new ExternalAppzGrantsChecker("users");
            VerticalApplication usersApp = VerticalApplicationsPool.GetBySystemName("users");
            if (!usersApp.Grant) NetCms.GUI.PopupBox.AddNoGrantsMessageAndGoBack();
        }

        protected override WebControl BuildControls()
        {
            return GetControlView();
        }
        public override string PageTitle
        {
            get { return "Riepilogo dei permessi sul CMS " + PageData.Network.Label + " " + this.Profile.EntityLabel; }
        }
        public WebControl GetControlView()
        {
            PageData.InfoToolbar.Title += "Riepilogo dei permessi sul CMS " + PageData.Network.Label + "  ";

            WebControl output = new WebControl(HtmlTextWriterTag.Div);

            //string Utente = this.Account.Label;
            //string logText = "L'utente " + Utente;

            //logText += " visualizza il riepilogo dei permessi su: " + PageData.Network.Label + " " + this.Profile.EntityLabel;
            //PageData.GuiLog.SaveLogRecord(logText, false);
            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
            string logText = "L'utente con username " + userName + " sta accedendo alla pagina di riepilogo dei permessi per " + this.Profile.EntityLabel + " relativamente al CMS" + PageData.Network.Label + ".";
            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, logText, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            
            HtmlGenericControl tabella = new HtmlGenericControl("table");
            HtmlGenericControl riga = new HtmlGenericControl("tr");
            HtmlGenericControl cell;

            tabella.Attributes["class"] = "GrantReviewTable GrantReviewInfoTable";
            tabella.Attributes["cellspacing"] = "0";
            tabella.Attributes["cellpadding"] = "0";

            riga = new HtmlGenericControl("tr");
            tabella.Controls.Add(riga);

            cell = new HtmlGenericControl("th");
            cell.InnerHtml = this.SelectedEntityName + ": ";
            riga.Controls.Add(cell);

            cell = new HtmlGenericControl("th");
            cell.InnerHtml = "Criterio Selezionato";
            riga.Controls.Add(cell);

            riga = new HtmlGenericControl("tr");
            tabella.Controls.Add(riga);

            cell = new HtmlGenericControl("td");
            cell.InnerHtml = this.NomeProfilo;
            riga.Controls.Add(cell);

            cell = new HtmlGenericControl("td");
            DropDownList list = new DropDownList();
            list.ID = "SelectedCriteria";
            list.AutoPostBack = true;
            //DataTable criteri = PageData.Conn.SqlQuery("SELECT * FROM Criteri");
            //foreach (DataRow row in criteri.Rows)
            //    list.Items.Add(new ListItem(row["Nome_Criterio"].ToString(), row["key_Criterio"].ToString()));
            IList<Criterio> criteri = GrantsBusinessLogic.FindAllCriteri();
            foreach(Criterio criterio in criteri)
                list.Items.Add(new ListItem(criterio.Nome, criterio.Key));
            cell.Controls.Add(list);
            riga.Controls.Add(cell);

            //bool SimpleCriteria = criteri.Select(" Key_Criterio = '" + SelectedCriteria + "' AND Tipo_Criterio = 0").Length > 0;

            //if (!SimpleCriteria)
            {/*
                    if (SelectedCriteria == NetCms.Structure.Grants.Criteria.CriteriaSwitch(CriteriaTypes.Bandi))
                    {
                        list = new DropDownList();
                        list.ID = "SelectedCriteriaApp";
                        list.AutoPostBack = true;
                        DataTable uffici = PageData.Conn.SqlQuery("SELECT * FROM Bandi_Uffici");
                        list.Items.Add(new ListItem("", "0"));
                        foreach (DataRow ufficio in uffici.Rows)
                            list.Items.Add(new ListItem(ufficio["Nome_ufficio"].ToString(), ufficio["id_Ufficio"].ToString()));
                        cell.Controls.Add(list);
                    }

                    if (SelectedCriteria == NetCms.Structure.Grants.Criteria.CriteriaSwitch(CriteriaTypes.NewFolder))
                    {
                        list = new DropDownList();
                        list.ID = "SelectedCriteriaApp";
                        list.AutoPostBack = true;
                        DataTable applicativi = PageData.Conn.SqlQuery("SELECT * FROM Applicazioni  WHERE Folder_Applicativo = 1");
                        list.Items.Add(new ListItem("", "0"));
                        foreach (DataRow applicativo in applicativi.Rows)
                            list.Items.Add(new ListItem(applicativo["SystemName_Applicazione"].ToString(), applicativo["id_Applicazione"].ToString()));
                        cell.Controls.Add(list);
                    }*/
            }
            output.Controls.Add(tabella);

            tabella = new HtmlGenericControl("table");
            riga = new HtmlGenericControl("tr");

            tabella.ID = "GrantReviewTable";
            tabella.Attributes["class"] = "GrantReviewTable";
            tabella.Attributes["cellspacing"] = "0";
            tabella.Attributes["cellpadding"] = "0";

            riga = new HtmlGenericControl("tr");
            tabella.Controls.Add(riga);

            cell = new HtmlGenericControl("th");
            cell.InnerHtml = "Cartella";
            riga.Controls.Add(cell);

            cell = new HtmlGenericControl("th");
            cell.InnerHtml = "Specifica";
            riga.Controls.Add(cell);

            cell = new HtmlGenericControl("th");
            cell.InnerHtml = "Permesso Registrato SP";
            riga.Controls.Add(cell);

            cell = new HtmlGenericControl("th");
            cell.InnerHtml = "Permesso Calcolato SP";
            riga.Controls.Add(cell);

            //if (NetCms.Configurations.Debug.GenericEnabled)
            //{
            //    cell = new HtmlGenericControl("th");
            //    cell.InnerHtml = "Permesso Registrato New";
            //    riga.Controls.Add(cell);

            //    cell = new HtmlGenericControl("th");
            //    cell.InnerHtml = "Permesso Calcolato New";
            //    riga.Controls.Add(cell);
            //}
            Dictionary<int, int> alternateCollection = new Dictionary<int, int>();
            IList<StructureFolder> Folders = FolderBusinessLogic.ListAllFolders(this.CurrentNetwork.ID).OrderBy(x => x.Path).ToList();
            foreach (StructureFolder fold in Folders)
                AddFolderRecursive(tabella, fold, alternateCollection);//RootFolder);

            output.Controls.Add(tabella);
            return output;
        }

        private void AddFolderRecursive(HtmlGenericControl table, StructureFolder folder,Dictionary<int, int> altCollection)
        {
            StructureFolder CurentFolder = folder;


            //Specify inheritedSpecify = folder.CriteriaProxy[Profile].SearchPermissionByInheritage(SelectedCriteria);
            //Specify notInheritedSpecify = folder.CriteriaProxy[Profile].GetRawPermission(SelectedCriteria);
            NetCms.Networks.Grants.FolderGrants grants = new Networks.Grants.FolderGrants(folder, Profile);
            var notInheritedNewSpecify = grants.CheckForGrant(SelectedCriteria);
            var inheritedNewSpecify = grants[SelectedCriteria];
            bool hasSubFolders = folder.HasSubFolders;
            bool hasDocuments = folder.HasDocuments;

            #region Riga
            HtmlGenericControl riga = new HtmlGenericControl("tr");

            HtmlGenericControl cell;
            riga = new HtmlGenericControl("tr");
            riga.ID = "Row" + folder.Tree.Replace(".", "A");
            if (folder.Depth > 2)
            {
                riga.Attributes["class"] = "GrantsReview_Hide ";
            }

            if (altCollection.ContainsKey(folder.Depth))
            {
                if (altCollection[folder.Depth] % 2 > 0)
                {
                    riga.Attributes["class"] += "alternate";
                }
                altCollection[folder.Depth]++;
            }
            else
            {
                altCollection.Add(folder.Depth, 0);
                if (folder.Depth != 0)
                    riga.Attributes["class"] += "alternate";
            }

            //if (table.Controls.Count % 2 > 0)
            //    riga.Attributes["class"] += "alternate";
            table.Controls.Add(riga);

            #endregion

            #region Cella Nome + Icona + Linee di profonditÓ

            string offset = "";
            for (int i = folder.Depth; i > 2; i--)
            {
                //string ClassName = CurentFolder == folder ? " GrantsReview_LinesBottom" : " GrantsReview_LinesVoid";
                //StructureFolder parentCurentFolder = CurentFolder.Parent as StructureFolder;
                offset = "<span class=\"GrantsReview_Lines" + /*(!CurentFolder.HasNext && !parentCurentFolder.HasDocuments && i - 1 > 0 ? ClassName : "") +*/ "\">&nbsp;</span>" + offset;
                //CurentFolder = folder.Parent as StructureFolder;
            }

            StructureFolder parentFolder = folder.Parent as StructureFolder;
            string Lines = "GrantsReview_Lines GrantsReview_" + (hasSubFolders || hasDocuments ? "Plus" : "Join");
            if (folder.Parent != null && !folder.HasNext && !parentFolder.HasDocuments) Lines += "Bottom";

            cell = new HtmlGenericControl("td");
            cell.InnerHtml = offset;
            cell.InnerHtml += "<a href=\"javascript: OpenFolder_Grants('" + table.ID + "','" + riga.ID + "')\" class=\"GrantsReview_Link\">";
            if (folder.Depth > 1)
                cell.InnerHtml += "<span id=\"" + riga.ID + "_Lines\" class=\"" + Lines + "\">&nbsp;</span>";
            cell.InnerHtml += "<span class=\"GrantsReview_FolderIcon\">&nbsp;</span>";
            cell.InnerHtml += "<span>" + folder.Label + "</span>";
            cell.InnerHtml += "</a>";
            cell.Attributes["class"] = "GrantsReview_FolderCell";
            riga.Controls.Add(cell);

            #endregion

            string link = folder.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/grants/grants.aspx?folder=" + folder.ID;
            //bool NoRegisteredCriteria = false;

            cell = new HtmlGenericControl("td");

            //if( inheritedSpecify.WfsObject.ObjectType == WebFSObjectTypes.Folder && folder.ID == inheritedSpecify.WfsObject.ID)
            if (inheritedNewSpecify.SpecifyFound)
            {
                //if (inheritedSpecify.Profile.IsUser)
                if (grants.Profile.IsUser)
                    cell.InnerHtml = "<a class=\"GrantsReview_Icon GrantsReview_User\" href=\"" + link + "\"><span><strong>Utente</strong></span></a>";
                else
                    cell.InnerHtml += "<a class=\"GrantsReview_Icon GrantsReview_Group\" href=\"" + link + "\"><span><strong>Gruppo '" + grants.Profile.EntityName /*inheritedSpecify.Profile.EntityName*/ + "'</strong></span></a>";
            }
            else
                cell.InnerHtml += "<a class=\"GrantsReview_Icon GrantsReview_Void\" href=\"" + link + "\"><span>Nessuna -> Crea</span></a>";

            //if (FolderProfiles.Select("Profile_FolderProfile = " + this.Profile.ID + " AND Folder_FolderProfile = " + folder.ID).Length == 0)
            //    NoRegisteredCriteria = true;

            riga.Controls.Add(cell);
            
            //cell = new HtmlGenericControl("td");
            //cell.InnerHtml = ShowIcon(NoRegisteredCriteria ? SpecifyTypes.NotAssociated : notInheritedSpecify.Value);
            //riga.Controls.Add(cell);

            //cell = new HtmlGenericControl("td");
            //cell.InnerHtml = ShowIcon(inheritedSpecify.Value == SpecifyTypes.Allow ? SpecifyTypes.Allow : SpecifyTypes.Deny);
            //riga.Controls.Add(cell);
            
            //if (NetCms.Configurations.Debug.GenericEnabled)
            //{
                

                
                cell = new HtmlGenericControl("td");
                cell.InnerHtml = ShowIcon(notInheritedNewSpecify.Value);
                riga.Controls.Add(cell);

                
                cell = new HtmlGenericControl("td");
                cell.InnerHtml = ShowIcon(inheritedNewSpecify.Allowed ? NetCms.Grants.GrantsValues.Allow : NetCms.Grants.GrantsValues.Deny);
                riga.Controls.Add(cell);
            //}

            //for (int i = 0; i < folder.SubFolders.Count; i++)
            //{
            //    if (!folder.SubFolders.ElementAt(i).Hidden)
            //        AddFolderRecursive(table, folder.SubFolders.ElementAt(i) as StructureFolder);
            //}

            //foreach (StructureFolder fold in folder.SubFolders.Where(x => !x.Hidden)) AddFolderRecursive(table, fold as StructureFolder);
            //for (int i = 0; i < folder.Documents.Count; i++)
            //{
            //    //if (!folder.Documents[i].)
            //    AddDoc(table, folder.Documents.ElementAt(i) as Document);
            //}

            foreach (NetworkDocument doc in folder.Documents) AddDoc(table, doc as Document); 
        }

        //private void AddDoc_bak(HtmlGenericControl table, Document doc)
        //{

        //    HtmlGenericControl riga = new HtmlGenericControl("tr");
        //    StructureFolder folderDoc = FolderBusinessLogic.GetById(doc.Folder.ID);
        //    riga.ID = "Row" + folderDoc.Tree.Replace(".", "A") + doc.ID + "A";
        //    if (folderDoc.Depth > 1)
        //        riga.Attributes["class"] = "GrantsReview_Hide ";
        //    if (table.Controls.Count % 2 > 0)
        //        riga.Attributes["class"] += "alternate";
        //    table.Controls.Add(riga);

        //    HtmlGenericControl cell;


            //string offset = "";
            //if (folderDoc.Depth > 1)
            //    offset = "<span class=\"GrantsReview_Lines\">&nbsp;</span>";
            //StructureFolder CurentFolder = folderDoc;
            //for (int i = folderDoc.Depth; i > 2; i--)
            //{
            //    string ClassName = CurentFolder == doc.Folder ? " GrantsReview_LinesBottom" : " GrantsReview_LinesVoid";
            //    StructureFolder parentCurentFolder = CurentFolder.Parent as StructureFolder;
            //    offset = "<span class=\"GrantsReview_Lines" + (!CurentFolder.HasNext && !parentCurentFolder.HasDocuments && i - 1 > 0 ? ClassName : "") + "\">&nbsp;</span>" + offset;
            //    CurentFolder = FolderBusinessLogic.GetById(doc.Folder.Parent.ID);
            //}


        //    string Lines = "GrantsReview_Join";
        //    if (!doc.HasNext) Lines += "Bottom";

        //    cell = new HtmlGenericControl("td");
        //    cell.InnerHtml += offset;
        //    cell.InnerHtml += "<span class=\"GrantsReview_Lines " + Lines + "\">&nbsp;</span>";
        //    cell.InnerHtml += "<span class=\"GrantsReview_DocIcon " + doc.Icon + "\">&nbsp;</span>";
        //    cell.InnerHtml += "<span>" + doc.Nome + "</span>";
        //    cell.Attributes["class"] = "GrantsReview_FolderCell";
        //    riga.Controls.Add(cell);

        //    cell = new HtmlGenericControl("td");
        //    string link = doc.Folder.Network.Paths.AbsoluteAdminRoot + "/cms/grants/grants.aspx?doc=" + doc.ID;
        //    bool NoRegisteredCriteria = false;
        //    if (this.SelectedCriteriaForDoc)
        //    {
        //        if (DocProfiles.Select(" Profile_DocProfile = " + Profile + " AND Doc_DocProfile = " + doc.ID).Length > 0)
        //        {
        //            if (this.Profile.IsUser)
        //                cell.InnerHtml = "<a class=\"GrantsReview_Icon GrantsReview_User\" href=\"" + link + "\"><span><strong>Utente</span></strong></a>";
        //            else
        //                cell.InnerHtml += "<a class=\"GrantsReview_Icon GrantsReview_Group\" href=\"" + link + "\"><span><strong>Gruppo '" + this.NomeProfilo + "'</strong></span></a>";

        //        }
        //        else
        //        {
        //            DataRow[] rows = GroupsFolderProfiles.Select("id_FolderProfile = " + doc.CriteriaProxy[this.Profile].ObjectProfileID);
        //            if (rows.Length > 0)
        //                cell.InnerHtml += "<a class=\"GrantsReview_Icon GrantsReview_Group\" href=\"" + link + "\"><span><strong>Gruppo '" + rows[0]["Name_Group"] + "'</strong></span></a>";
        //            else
        //            {
        //                cell.InnerHtml += "<a class=\"GrantsReview_Icon GrantsReview_Void\" href=\"" + link + "\"><span>Nessuna -> Crea</span></a>";
        //                NoRegisteredCriteria = true;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        cell.InnerHtml += "&nbsp;";
        //    }
        //    riga.Controls.Add(cell);
        //    cell = new HtmlGenericControl("td");
        //    cell.InnerHtml = ShowIcon(NoRegisteredCriteria ? SpecifyTypes.NotAssociated : doc.CriteriaProxy[Profile].GetRawPermission(SelectedCriteria).Value);
        //    riga.Controls.Add(cell);

        //    cell = new HtmlGenericControl("td");
        //    cell.InnerHtml = ShowIcon(doc.CriteriaProxy[Profile].SearchPermissionByInheritage(SelectedCriteria).Value);
        //    riga.Controls.Add(cell);

        //}
        private void AddDoc(HtmlGenericControl table, Document doc)
        {
            //StructureFolder CurentFolder = folder;
            if (doc != null)
            {
                //Specify inheritedSpecify = doc.CriteriaProxy[Profile].SearchPermissionByInheritage(SelectedCriteria);
                //Specify notInheritedSpecify = doc.CriteriaProxy[Profile].GetRawPermission(SelectedCriteria);
                NetCms.Networks.Grants.DocumentGrants grants = new Networks.Grants.DocumentGrants(doc, Profile);

                var notInheritedNewSpecify = grants.CheckForGrant(SelectedCriteria);
                var inheritedNewSpecify = grants[SelectedCriteria];

                HtmlGenericControl riga = new HtmlGenericControl("tr");
                StructureFolder folderDoc = FolderBusinessLogic.GetById(doc.Folder.ID);
                riga.ID = "Row" + folderDoc.Tree.Replace(".", "A") + doc.ID + "A";
                if (folderDoc.Depth > 1)
                    riga.Attributes["class"] = "GrantsReview_Hide ";
                if (table.Controls.Count % 2 > 0)
                    riga.Attributes["class"] += "alternate";
                table.Controls.Add(riga);

                HtmlGenericControl cell;


                string offset = "";
                if (folderDoc.Depth > 1)
                    offset = "<span class=\"GrantsReview_Lines\">&nbsp;</span>";
                StructureFolder tmpFolder = folderDoc;
                for (int i = folderDoc.Depth; i > 2; i--)
                {
                    //string ClassName = tmpFolder == doc.Folder ? " GrantsReview_LinesBottom" : " GrantsReview_LinesVoid";
                    //StructureFolder parentTmpFolder = tmpFolder.Parent as StructureFolder;
                    offset = "<span class=\"GrantsReview_Lines" + /*(!tmpFolder.HasNext && !parentTmpFolder.HasDocuments && i - 1 > 0 ? ClassName : "") +*/ "\">&nbsp;</span>" + offset;
                    //tmpFolder = FolderBusinessLogic.GetById(doc.Folder.Parent.ID);
                }

                string Lines = "GrantsReview_Join";
                if (!doc.HasNext) Lines += "Bottom";

                cell = new HtmlGenericControl("td");
                cell.InnerHtml += offset;
                cell.InnerHtml += "<span class=\"GrantsReview_Lines " + Lines + "\">&nbsp;</span>";
                cell.InnerHtml += "<span class=\"GrantsReview_DocIcon " + doc.Icon + "\">&nbsp;</span>";
                cell.InnerHtml += "<span>" + doc.Nome + "</span>";
                cell.Attributes["class"] = "GrantsReview_FolderCell";
                riga.Controls.Add(cell);

                string link = doc.Network.Paths.AbsoluteAdminRoot + "/cms/grants/grants.aspx?" + doc.QueryString4URLs;
                //bool NoRegisteredCriteria = false;

                cell = new HtmlGenericControl("td");

                //if (inheritedSpecify.WfsObject.ObjectType == WebFSObjectTypes.Document && doc.ID == inheritedSpecify.WfsObject.ID)
                if (inheritedNewSpecify.SpecifyFound)
                {
                    //if (inheritedSpecify.Profile.IsUser)
                    if (grants.Profile.IsUser)
                        cell.InnerHtml = "<a class=\"GrantsReview_Icon GrantsReview_User\" href=\"" + link + "\"><span><strong>Utente</strong></span></a>";
                    else
                        cell.InnerHtml += "<a class=\"GrantsReview_Icon GrantsReview_Group\" href=\"" + link + "\"><span><strong>Gruppo '" + grants.Profile.EntityName /*inheritedSpecify.Profile.EntityName*/ + "'</strong></span></a>";
                }
                else
                    cell.InnerHtml += "<a class=\"GrantsReview_Icon GrantsReview_Void\" href=\"" + link + "\"><span>Nessuna -> Crea</span></a>";

                //if (this.DocProfiles.Select("Profile_DocProfile = " + this.Profile.ID + " AND Doc_DocProfile = " + doc.ID).Length == 0)
                //    NoRegisteredCriteria = true;

                riga.Controls.Add(cell);

                //cell = new HtmlGenericControl("td");
                //cell.InnerHtml = ShowIcon(NoRegisteredCriteria ? SpecifyTypes.NotAssociated : notInheritedSpecify.Value);
                //riga.Controls.Add(cell);

                //cell = new HtmlGenericControl("td");
                //cell.InnerHtml = ShowIcon(inheritedSpecify.Value);
                //riga.Controls.Add(cell);

                //if (NetCms.Configurations.Debug.GenericEnabled)
                //{
                    
                    cell = new HtmlGenericControl("td");
                    cell.InnerHtml = ShowIcon(notInheritedNewSpecify.Value);
                    riga.Controls.Add(cell);

                    
                    cell = new HtmlGenericControl("td");
                    cell.InnerHtml = ShowIcon(inheritedNewSpecify.Allowed ? NetCms.Grants.GrantsValues.Allow : NetCms.Grants.GrantsValues.Deny);
                    riga.Controls.Add(cell);
                //}
            }
        }

        //private string ShowIcon(SpecifyTypes value)
        //{
        //    string output = "<span class=\"GrantsReview_Icon GrantsReview_{0}\"><span>{1}</span></span>";
        //    switch (value)
        //    {
        //        case SpecifyTypes.Inherits:
        //            return string.Format(output, "Inherits", "Eredita");
        //        case SpecifyTypes.Allow:
        //            return string.Format(output, "Allowed", "Permesso");
        //        case SpecifyTypes.Deny:
        //            return string.Format(output, "NotAllowed", "NON Permesso");
        //        default:
        //            return string.Format(output, "NoRecord", "Nessuno -> Eredita");
        //    }
        //}

        private string ShowIcon(NetCms.Grants.GrantsValues value)
        {
            string output = "<span class=\"GrantsReview_Icon GrantsReview_{0}\"><span>{1}</span></span>";
            switch (value)
            {
                case NetCms.Grants.GrantsValues.Inherits:
                    return string.Format(output, "Inherits", "Eredita");
                case NetCms.Grants.GrantsValues.Allow:
                    return string.Format(output, "Allowed", "Permesso");
                case NetCms.Grants.GrantsValues.Deny:
                    return string.Format(output, "NotAllowed", "Non Permesso");
                default:
                    return string.Format(output, "NoRecord", "Nessuno -> Eredita");
            }
        }
    }
}
