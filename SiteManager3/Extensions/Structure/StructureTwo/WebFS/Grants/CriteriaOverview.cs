﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;
using NetUtility.NetTreeViewControl;
using NetCms.GUI;
using NetTable;
using NetCms.Structure.Grants;
using NetCms.Networks.WebFS;
using NetCms.Networks.Grants;
using NetCms.Grants;
using NetCms.Grants.Interfaces;

namespace NetCms.Structure.WebFS
{
    public class CriteriaOverview:WebControl
    {
        
        public ObjectProfileAssociation Association
        {
            get { return _Association; }
            private set { _Association = value; }
        }
        private ObjectProfileAssociation _Association;

        public CriteriaOverview(IGrantsFindable wfsObject)
            : base(HtmlTextWriterTag.Div)
        {
            G2Core.Common.RequestVariable associationRequest = new G2Core.Common.RequestVariable("fp", G2Core.Common.RequestVariable.RequestType.QueryString);
            this.Association = new ObjectProfileAssociation(wfsObject,associationRequest.IntValue);
        }

        public CriteriaOverview(IGrantsFindable wfsObject, int associationID)
            : base(HtmlTextWriterTag.Div)
        {
            this.Association = new ObjectProfileAssociation(wfsObject,associationID);
        }

        public CriteriaOverview(NetCms.Networks.WebFS.CmsWebFSObject wfsObject,ObjectProfileAssociation association)
            : base(HtmlTextWriterTag.Div)
        {
            this.Association = association;
        }

        public IList<Criterio> CriteriaList
        {
            get
            {
                if (_CriteriaList == null)
                {
                    _CriteriaList = GrantsBusinessLogic.FindAllCriteri();
                }
                return _CriteriaList;
            }
        }
        private IList<Criterio> _CriteriaList;

        //public DataTable GrantsTable
        //{
        //    get
        //    {
        //        if (_GrantsTable == null)
        //            _GrantsTable = this.Connection.SqlQuery("SELECT * FROM folderprofilescriteria WHERE FolderProfile_FolderProfileCriteria = " + Association.ID);
        //        return _GrantsTable;
        //    }
        //}
        //private DataTable _GrantsTable;
        
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.Controls.Add(Overview());
        }

        private WebControl Overview()
        {
            G2Core.UI.DetailsSheet details = new G2Core.UI.DetailsSheet("Riepilogo dei permessi", @"Questa schermata mostra i permessi per " + this.Association.Profile.EntityLabel + @". La voce 'Permesso Registrato' contiene il valore attualmente registrato. La voce 'Permesso Eleborato' contiene il risultato dell'elaborazione dello specifico permesso da parte del sistema tenendo conto anche delle eredità dei gruppi.");

            details.AddColumn("Criterio", G2Core.UI.DetailsSheet.Colors.Default);
            //details.AddColumn("Permesso Registrato", G2Core.UI.DetailsSheet.Colors.Default);
            //details.AddColumn("Permesso Elaborato", G2Core.UI.DetailsSheet.Colors.Default);

            //if (NetCms.Configurations.Debug.GenericEnabled)
            //{
            details.AddColumn("New Registrato", G2Core.UI.DetailsSheet.Colors.Default);
            details.AddColumn("New Elaborato", G2Core.UI.DetailsSheet.Colors.Default);
            //}

            TableRow row;

            //TimeSpan newTimings = new TimeSpan(0);
            //TimeSpan oldTimings = new TimeSpan(0);

            //GrantsFinder grant = null;//new FolderGrants(this.Association.WfsObject as StructureFolder, this.Association.Profile);
            //if(this.Association.WfsObject is StructureFolder)
            //    grant = new FolderGrants(this.Association.WfsObject as StructureFolder, this.Association.Profile);
            //else
            //    grant = new DocumentGrants(this.Association.WfsObject as Document, this.Association.Profile);
            GrantsFinder grant = this.Association.GrantObject.CriteriaByProfile(this.Association.Profile);

            foreach (Criterio criterioItem in CriteriaList)
            {
                var element = Association.GrantObject;

                row = new TableRow();

                TableCell cell = new TableCell();
                cell.Text = criterioItem.Nome;
                row.Cells.Add(cell);

                string value = "";
                string criterio = criterioItem.Key;

                #region Calcolo Originale
                //{
                //    var start = DateTime.Now;
                //    cell = new TableCell();
                //    Specify dbvalue = element.CriteriaProxy[Association.Profile].GetRawPermission(criteriaRow["Key_Criterio"].ToString());
                //    switch (dbvalue.Value)
                //    {
                //        case SpecifyTypes.Allow: cell.Text = "Consenti"; cell.CssClass = "fill_green"; break;
                //        case SpecifyTypes.Deny: cell.Text = "Nega"; cell.CssClass = "fill_red"; break;
                //        default: cell.Text = "Eredita"; cell.CssClass = "fill_yellow"; break;
                //    }

                //    row.Cells.Add(cell);
                //    value = "";

                //    cell = new TableCell();

                //    var specify = element.CriteriaProxy[Association.Profile].SearchPermissionByInheritage(criteriaRow["Key_Criterio"].ToString());
                //    if (specify.Allow)
                //    {
                //        value += "Consenti";
                //        cell.CssClass = "fill_green";
                //    }
                //    else
                //    {
                //        value += "Nega";
                //        cell.CssClass = "fill_red";
                //    }

                //    cell.Text = value;
                //    row.Cells.Add(cell);
                //    oldTimings += DateTime.Now - start;
                //}
                #endregion

                #region Calcolo Nuovo via Stored Procedures
                
                //if (NetCms.Configurations.Debug.GenericEnabled)
                //{
                    var start = DateTime.Now;
                    cell = new TableCell();
                    var val = grant.CheckForGrant(criterio);
                    switch (val.Value)
                    {
                        case GrantsValues.Allow: cell.Text = "Consenti"; cell.CssClass = "fill_green"; break;
                        case GrantsValues.Deny: cell.Text = "Nega"; cell.CssClass = "fill_red"; break;
                        default: cell.Text = "Eredita"; cell.CssClass = "fill_yellow"; break;
                    }

                    row.Cells.Add(cell);
                    value = "";

                    cell = new TableCell();

                    val = grant[criterio];
                    if (val.Allowed)
                    {
                        value += "Consenti"; 
                        cell.CssClass = "fill_green";
                    }
                    else
                    {
                        value += "Nega"; 
                        cell.CssClass = "fill_red";
                    }

                    cell.Text = value;
                    row.Cells.Add(cell);
                    //newTimings += DateTime.Now - start;
                //}
                #endregion
                details.AddRow(row, G2Core.UI.DetailsSheet.Colors.Default);
            }

            //if (NetCms.Configurations.Debug.GenericEnabled)
            //{
            //    DetailsSheetRow dsr = new DetailsSheetRow("Tempi", "Vecchio Metodo: " + oldTimings.ToString() + " | Nuovo Metodo " + newTimings.ToString() + "");
            //    dsr.CssClassLeft = "big strong";
            //    details.AddRow("Tempi", "Vecchio Metodo: ", oldTimings.ToString(), "Nuovo Metodo", newTimings.ToString());
            //}
            /*
            DetailsSheetRow dsr = new DetailsSheetRow("Indietro", "Ritorna alla schermata di associazione dei Permessi Applicazione .", "success");
            dsr.LinkLeft = "applications.aspx?app="+this.Association.ApplicationID;
            dsr.CssClassLeft = "big strong";
            details.AddRow(dsr);

            dsr = new DetailsSheetRow("Modifica", "Vai alla pagina di modifica di questi permessi.", "notify");
            dsr.LinkLeft = "applications.aspx?aid=" + this.Association.ID;
            dsr.CssClassLeft = "big strong";
            details.AddRow(dsr);
            */
            #region codice di log
            if (!Page.IsPostBack)
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                string logText = "L'utente con username " + userName + " sta accedendo alla pagina di visualizzazione dei permessi";
                if (this.Association.GrantObject.GrantObjectType ==  GrantObjectType.Document)
                {

                    logText += " del documento con id=" + this.Association.ObjectID;
                }
                else
                {

                    logText += " della cartella con id=" + this.Association.ObjectID;
                }
                logText += " assegnati per " + this.Association.Profile.EntityLabel + ".";

                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, logText, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            }
            #endregion

            return details;
        }


    }
}
