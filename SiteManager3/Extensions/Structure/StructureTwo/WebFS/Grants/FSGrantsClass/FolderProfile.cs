﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Networks.WebFS;

namespace NetCms.Grants
{
    public class FolderProfile : ObjectProfile
    {
        public FolderProfile()
            : base()
        {
            CriteriaCollection = new HashSet<ObjectProfileCriteria>();
        }

        public override ICollection<ObjectProfileCriteria> CriteriaCollection
        {
            get;
            //{
            //    return _CriteriaCollection;
            //}
            set;
            //{
            //    _CriteriaCollection = value as HashSet<FolderProfileCriteria>;
            //}
        }
        //private HashSet<FolderProfileCriteria> _CriteriaCollection;
    }
}
