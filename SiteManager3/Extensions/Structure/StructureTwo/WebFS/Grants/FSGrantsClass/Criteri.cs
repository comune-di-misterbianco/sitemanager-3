﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Grants.Interfaces;

namespace NetCms.Grants
{
    public class Criterio : IGrantsCriteria
    {
        public Criterio()
        { }

        #region Proprietà da mappare

        public virtual int ID { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Key { get; set; }

        public virtual int Tipo { get; set; }

        public virtual bool ForDoc { get; set; }

        #endregion
    }
}
