﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Networks.WebFS;

namespace NetCms.Grants
{
    public class DocProfile : ObjectProfile
    {
        public DocProfile() :base()
        {
            CriteriaCollection = new HashSet<ObjectProfileCriteria>();
        }

        public override ICollection<ObjectProfileCriteria> CriteriaCollection
        {
            get;
            //{
            //    return _CriteriaCollection as ICollection<ObjectProfileCriteria>;
            //}
            set;
            //{
            //    _CriteriaCollection = value as HashSet<DocProfileCriteria>;
            //}
        }
        //private HashSet<DocProfileCriteria> _CriteriaCollection;
    }
}
