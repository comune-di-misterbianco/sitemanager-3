﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Grants
{
    public class FolderProfileCriteria : ObjectProfileCriteria
    {
        public FolderProfileCriteria()
            : base()
        { }

        //public virtual FolderProfile FolderProfile
        //{ get; set; }

        public override ObjectProfile ObjectProfile
        {
            get
            {
                return _ObjectProfile;
            }
            set
            {
                _ObjectProfile = value as FolderProfile;
            }
        }
        private FolderProfile _ObjectProfile;

        public virtual int Application
        { get; set; }
    }
}
