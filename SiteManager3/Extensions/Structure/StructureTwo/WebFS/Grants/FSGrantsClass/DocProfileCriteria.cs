﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Grants
{
    public class DocProfileCriteria : ObjectProfileCriteria
    {
        public DocProfileCriteria() : base()
        {}

        //public virtual DocProfile DocProfile
        //{ get; set; }
        public override ObjectProfile ObjectProfile
        {
            get
            {
                return _ObjectProfile;
            }
            set
            {
                _ObjectProfile = value as DocProfile;
            }
        }
        private DocProfile _ObjectProfile;
    }
}
