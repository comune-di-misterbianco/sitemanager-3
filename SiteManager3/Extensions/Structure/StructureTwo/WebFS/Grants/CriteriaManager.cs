﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;
using NetUtility.NetTreeViewControl;
using NetCms.GUI;
using NetTable;
using NetCms.Users;
using NetCms.Networks.WebFS;
using NetCms.Networks.Grants;
using NetCms.Grants;
using NetCms.Grants.Interfaces;
using NHibernate;
using NetCms.Structure.Applications;
using System.Threading.Tasks;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Runtime.Remoting.Messaging;
using System.Web.SessionState;

namespace NetCms.Structure.WebFS
{
    public class CriteriaManager:WebControl
    {
      
        
        public ObjectProfileAssociation Association
        {
            get { return _Association; }
            private set { _Association = value; }
        }
        private ObjectProfileAssociation _Association;
        
        public NetCms.Users.User CurrentUser
        {
            get
            {
                if (_CurrentUser == null)
                    _CurrentUser = NetCms.Users.AccountManager.CurrentAccount;
                return _CurrentUser;
            }
        }
        private NetCms.Users.User _CurrentUser;
        
        public IGrantsFindable Object
        {
            get { return _Object; }
            private set { _Object = value; }
        }
        private IGrantsFindable _Object;

        public CriteriaManager(IGrantsFindable wfsObject)
            : base(HtmlTextWriterTag.Div)
        {
            this.Object = wfsObject;
            G2Core.Common.RequestVariable associationRequest = new G2Core.Common.RequestVariable("fp", G2Core.Common.RequestVariable.RequestType.QueryString);
            this.Association = new ObjectProfileAssociation(wfsObject,associationRequest.IntValue);
        }

        public CriteriaManager(IGrantsFindable wfsObject, int associationID)
            : base(HtmlTextWriterTag.Div)
        {
            this.Object = wfsObject;
            this.Association = new ObjectProfileAssociation(wfsObject,associationID);
        }

        public CriteriaManager(StructureFolder wfsObject, ObjectProfileAssociation association)
            : base(HtmlTextWriterTag.Div)
        {
            this.Object = wfsObject;
            this.Association = association;
        }

        public IList<Criterio> CriteriaList
        {
            get
            {
                if (_CriteriaList == null)
                {
                    _CriteriaList = GrantsBusinessLogic.FindAllCriteri().OrderBy(x => x.Tipo).ThenBy(x => x.ID).ToList(); //this.Connection.SqlQuery("SELECT * FROM criteri ORDER BY Tipo_Criterio ASC, id_Criterio");
                }
                return _CriteriaList;
            }
        }
        private IList<Criterio> _CriteriaList;

        //public DataTable ApplicazioniTable
        //{
        //    get
        //    {
        //        if (_ApplicazioniTable == null)
        //        {
        //            _ApplicazioniTable = this.Connection.SqlQuery("SELECT * FROM applicazioni");
        //        }
        //        return _ApplicazioniTable;
        //    }
        //}
        //private DataTable _ApplicazioniTable;

        //public DataTable GrantsTable
        //{
        //    get
        //    {
        //        if (_GrantsTable == null)
        //            _GrantsTable =  this.Object.GetGrantsTable(Association.ID);
        //        return _GrantsTable;
        //    }
        //}
        //private DataTable _GrantsTable;
        
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if(Association.AssociationFound)
                this.Controls.Add(CriteriaView());
        }

        private ICollection<KeyValuePair<int, bool>> CollectGrants(User _user)
        {
            ICollection<KeyValuePair<int, bool>> _FoldersCanView = new List<KeyValuePair<int, bool>>();
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                User user = UsersBusinessLogic.GetById(_user.ID, sess);
                var networks = user.AssociatedNetworks;
                foreach (var net in networks)
                {
                    var folders = FolderBusinessLogic.FindNetworkFoldersForGrantsCollection(net.Network.ID, sess);
                    foreach (var folder in folders)//net.Network.NetworkFolders.Cast<StructureFolder>().Where(x => !x.Hidden && !x.IsSystemFolder && x.Depth <= 3))
                    {
                        GrantsFinder Criteria = new FolderGrants(folder, user.Profile, sess);
                        _FoldersCanView.Add(new KeyValuePair<int, bool>(folder.ID, Criteria.NeedToShow("show").Allowed));
                    }
                }
            }
            return _FoldersCanView;
            //Thread.Sleep(60000);
        }

        static void CallbackMethod(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;
            NetCms.Users.LoginControl.CollectGrantsDelegate caller = (NetCms.Users.LoginControl.CollectGrantsDelegate)result.AsyncDelegate;
            ReturnMessage mess = (ReturnMessage)result.GetReplyMessage();

            HttpSessionState session = (HttpSessionState)ar.AsyncState;
            //ICollection<KeyValuePair<int, bool>> returnValue = caller.EndInvoke(ar);

            ICollection<KeyValuePair<int, bool>> _FoldersCanView = (List<KeyValuePair<int, bool>>)mess.ReturnValue;
            if (session != null)
            {
                User currUser = (User)session[Configurations.Generics.BackAccountSessionKey];
                if (currUser != null)
                {
                    if (session["FoldersCanViewUSER_" + currUser.ID] != null)
                    {
                        lock (session["FoldersCanViewUSER_" + currUser.ID])
                        {
                            session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                        }
                    }
                    else
                        session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                }
                //currUser.FoldersCanView = _FoldersCanView;
            }
        }


        void submit_Click(object sender, EventArgs e)
        {

            bool result = true;
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    this.Object.DeleteGrantsForAssociation(this.Association.ID,sess);
                  
                    foreach (Criterio criterioItem in CriteriaList)
                    {
                        string RadioGroupID = criterioItem.ID.ToString() + "Criteria" + Association.ID;

                        G2Core.Common.RequestVariable request = new G2Core.Common.RequestVariable(RadioGroupID);
                        if (request.IsValidString)
                        {
                            string grantvalue = request.StringValue.Remove(1);
                            switch (grantvalue)
                            {
                                case "A"://caso in cui +è stato impostato come allow
                                    grantvalue = "1";
                                    break;
                                case "I"://caso in cui +è stato impostato come eredita
                                    grantvalue = "0";
                                    break;
                                case "N"://caso in cui +è stato impostato come nega
                                    grantvalue = "2";
                                    break;
                            }

                            if (grantvalue != "0")
                                this.Object.AddGrant(this.Association.ID, int.Parse(grantvalue), criterioItem.ID, Users.AccountManager.CurrentAccount.ID,sess);
                        }
                    }
                    tx.Commit();

                    //Aggiungo questo codice per forzare il caching dopo aver modificato i permessi cartella se relativi all'account dell'utente correntemente loggato
                    if (this.Object.GrantObjectType == GrantObjectType.Folder)
                    {
                        if (this.Association.Profile.IsUser && this.Association.Profile.ID == NetCms.Users.AccountManager.CurrentAccount.Profile.ID)
                        {
                            if (HttpContext.Current.Session["FoldersCanViewUSER_" + NetCms.Users.AccountManager.CurrentAccount.ID] != null)
                                HttpContext.Current.Session["FoldersCanViewUSER_" + NetCms.Users.AccountManager.CurrentAccount.ID] = null;

                            HttpSessionState webSession = HttpContext.Current.Session;

                            NetCms.Users.LoginControl.CollectGrantsDelegate myAction = new NetCms.Users.LoginControl.CollectGrantsDelegate(CollectGrants);
                            myAction.BeginInvoke(NetCms.Users.AccountManager.CurrentAccount, new AsyncCallback(CallbackMethod), webSession);

                            
                        }
                    }
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    result = false;
                }
            }
            //SISTEMARE CON CACHE 2° LIVELLO
            //this.Object.Invalidate();

            string link ="wizard.aspx?fp=" + this.Association.ID + "&show=o";
            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
            string logText = "L'utente con username " + userName + " ha salvato con successo i permessi";
            string logTextError = "L'utente con username " + userName + " ha riscontrato un errore nel salvataggio dei permessi";
            if (this.Object.GrantObjectType == GrantObjectType.Document)
            {
                link += "&folder=" + ((Document)this.Object).Folder.ID + "&doc=" + this.Object.ID;
                logText += " del documento con id=" + this.Object.ID;
                logTextError += " del documento con id=" + this.Object.ID;
            }
            else
            {
                link += "&folder=" + this.Association.ObjectID;
                logText += " della cartella con id=" + this.Association.ObjectID;
                logTextError += " della cartella con id=" + this.Association.ObjectID;
            }
            logText += " assegnati per " + this.Association.Profile.EntityLabel + ".";
            logTextError += " assegnati per " + this.Association.Profile.EntityLabel + ".";

            if (result)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, logText, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                NetCms.GUI.PopupBox.AddSessionMessage("I Permessi sono stati salvati con successo", PostBackMessagesType.Success, link);
            }
            else
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, logTextError, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                NetCms.GUI.PopupBox.AddSessionMessage("Errore inatteso durante il salvataggio dei permessi", PostBackMessagesType.Error, link);
            }
        }
        public WebControl CriteriaView()
        {
            Table table = new Table();
            table.CssClass = "tab";
            table.ID = "CriteriaTable";
            table.CssClass = "tab";
            table.CellSpacing = 0;
            table.CellPadding = 0;
            
            TableHeaderRow hrow = new TableHeaderRow();
            table.Rows.Add(hrow);

            TableHeaderCell hcell = new TableHeaderCell();
            hcell.Text = "Applicazione";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Criterio";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Permesso Registrato";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Permesso Elaborato";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Consenti";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Nega";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Eredita";
            hrow.Cells.Add(hcell);


            TableRow row = new TableRow();
            TableCell cell = new TableCell();
            bool altrenate = false;
            DataRow[] rows;
            int lastType = 0;
            string lastLabel = "Tutte";

            //GrantsFinder grant = null;
            //if (this.Association.WfsObject is StructureFolder)
            //    grant = new FolderGrants(this.Association.WfsObject as StructureFolder, this.Association.Profile);
            //else
            //    grant = new DocumentGrants(this.Association.WfsObject as Document, this.Association.Profile);
            GrantsFinder grant = this.Object.CriteriaByProfile(this.Association.Profile);

            foreach (Criterio criterioItem in CriteriaList)
            {
                string criterio = criterioItem.Key;

                var val = grant.CheckForGrant(criterio);

                //COSA DOVREBBE FARE???? NON SI CAPISCE
                //var mySpecify = this.Association.WfsObject.Criteria[criteriaRow["Key_Criterio"].ToString()];
                if (val!=null || this.Association.IsSuperAdminForThis(this.CurrentUser.Profile))
                {
                    /*if ((int)criteriaRow["Tipo_Criterio"] != lastType)
                    {
                        rows = this.ApplicazioniTable.Select("id_Applicazione = " + criteriaRow["Tipo_Criterio"]);
                        if(rows.Length>0)
                        {
                            altrenate = false;

                            lastType = (int)criteriaRow["Tipo_Criterio"];
                            row = new TableRow();
                            table.Rows.Add(row);

                            cell = new TableHeaderCell();
                            cell.ColumnSpan = 6;
                            cell.Text = "Criteri per l'Applicazione '" + rows[0]["LabelPlurale_Applicazione"].ToString() + "'";
                            row.Cells.Add(cell);
                        }
                    }*/

                    row = new TableRow();

                    if (altrenate) row.CssClass = "alternate";
                    altrenate = !altrenate;

                    cell = new TableCell();
                    if (criterioItem.Tipo != lastType)
                    {
                        //rows = this.ApplicazioniTable.Select("id_Applicazione = " + criterioItem.Tipo);
                        //if (rows.Length > 0)
                        //{
                        //    lastType = criterioItem.Tipo;
                        //    lastLabel = (string)rows[0]["LabelPlurale_Applicazione"];
                        //}

                        Application dbApp = ApplicationBusinessLogic.GetApplicationById(criterioItem.Tipo);
                        if (dbApp != null)
                        {
                            lastType = criterioItem.Tipo;
                            lastLabel = dbApp.Label;
                        }
                    }
                    cell.Text = lastLabel;

                    row.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Text = criterioItem.Nome;
                    row.Cells.Add(cell);

                    string RecorderValue = "";

                    //string sql = "Criteria_FolderProfileCriteria = " + criteriaRow["id_Criterio"] + " AND FolderProfile_FolderProfileCriteria = " + this.Association.ID;
                    //rows = GrantsTable.Select(sql);

                    //string dbvalue = rows.Length > 0 ? rows[0]["Value_FolderProfileCriteria"].ToString() : "0";
                    //switch (dbvalue)
                    //{
                    //    case "1": RecorderValue += "Consenti"; break;
                    //    case "2": RecorderValue += "Nega"; break;
                    //    default: RecorderValue += "Eredita"; break;
                    //}
                    NetCms.Grants.GrantsValues dbValue = val.Value;
                    switch (dbValue)
                    {
                        case NetCms.Grants.GrantsValues.Allow: RecorderValue += "Consenti"; break;
                        case NetCms.Grants.GrantsValues.Deny: RecorderValue += "Nega"; break;
                        default: RecorderValue += "Eredita"; break;
                    }

                    cell = new TableCell();
                    cell.Text = RecorderValue;
                    cell.CssClass = "center";
                    row.Cells.Add(cell);

                    cell = new TableCell();

                    string workedValue = "";
                    //var specify = this.Association.WfsObject.CriteriaProxy[Association.Profile].SearchPermissionByInheritage(criteriaRow["Key_Criterio"].ToString());
                    val = grant[criterio];
                    switch (val.Value)
                    {
                        case NetCms.Grants.GrantsValues.Allow: workedValue += "Consenti"; cell.CssClass = "fill_green"; break;
                        case NetCms.Grants.GrantsValues.Deny: workedValue += "Nega"; cell.CssClass = "fill_red"; break;
                        default: workedValue += "Eredita"; cell.CssClass = "fill_yellow"; break;
                    }

                    cell.Text = workedValue;
                    cell.CssClass = "center";
                    row.Cells.Add(cell);

                    string RadioGroupID = criterioItem.ID.ToString() + "Criteria" + Association.ID;

                    RadioButton allow = new RadioButton();
                    allow.ID = "A_" + RadioGroupID;
                    allow.GroupName = RadioGroupID;
                    allow.Checked = dbValue == NetCms.Grants.GrantsValues.Allow; //dbvalue == "1";

                    RadioButton negate = new RadioButton();
                    negate.ID = "N_" + RadioGroupID;
                    negate.GroupName = RadioGroupID;
                    negate.Checked = dbValue == NetCms.Grants.GrantsValues.Deny; //dbvalue == "2";

                    RadioButton inherits = new RadioButton();
                    inherits.ID = "I_" + RadioGroupID;
                    inherits.GroupName = RadioGroupID;
                    inherits.Checked = !allow.Checked && !negate.Checked;


                    cell = new TableCell();
                    cell.Controls.Add(allow);
                    cell.CssClass = "center";
                    row.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Controls.Add(negate);
                    cell.CssClass = "center";
                    row.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Controls.Add(inherits);
                    cell.CssClass = "center";
                    row.Cells.Add(cell);

                    table.Rows.Add(row);
                }
            }

            if (CriteriaList.Count > 1)
            {
                row = new TableRow();

                cell = new TableCell();
                cell.Text = "&nbsp;";
                cell.CssClass = "text";
                cell.ColumnSpan = 4;
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Text = "<a href=\"javascript: GrantsTable_All('CriteriaTable','A')\">Consenti Tutti</a>";
                cell.CssClass = "center";
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Text = "<a href=\"javascript: GrantsTable_All('CriteriaTable','N')\">Nega Tutti</a>";
                cell.CssClass = "center";
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Text = "<a href=\"javascript: GrantsTable_All('CriteriaTable','I')\">Eredita Tutti</a>";
                cell.CssClass = "center";
                row.Cells.Add(cell);

                table.Rows.Add(row);
            }

            Button submit = new Button();
            submit.Text = "Salva Permessi";
            submit.Click += new EventHandler(submit_Click);
            row = new TableRow();

            cell = new TableCell();
            cell.Text = "&nbsp;";
            cell.ColumnSpan = 4;
            cell.Attributes["class"] = "text";
            row.Cells.Add(cell);

            cell = new TableCell();
            cell.ColumnSpan = 3;
            cell.Controls.Add(submit);
            cell.CssClass = "center";
            row.Cells.Add(cell);
            table.Rows.Add(row);
            if (!Page.IsPostBack)
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                string logText = "L'utente con username " + userName + " sta accedendo alla pagina di modifica dei permessi";
                if (this.Object.GrantObjectType == GrantObjectType.Document)
                {
                    logText += " del documento con id=" + this.Object.ID;
                }
                else
                {
                    logText += " della cartella con id=" + this.Association.ObjectID;
                }
                logText += " assegnati per " + this.Association.Profile.EntityLabel + ".";
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, logText, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            }
            return table;
        }
    }
}
