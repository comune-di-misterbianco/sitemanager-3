using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetUtility.NetTreeViewControl;
using NetCms.GUI;
using NetTable;
using NetCms.Grants.Interfaces;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Grants;
using NetCms.Networks.WebFS;
/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Structure.Grants.Wizard
{
    /*public class StructureGrantsWizard
    {
        private PageData PageData;

        private GrantsObjectType Object;

        private void SetToolbars()
        {
            string link;
            if (Object.Type == GrantsType.Document)
                link = "doc=" + Object.ID;
            else
                link = "folder=" + Object.ID;
            PageData.InfoToolbar.Icon = "Grants";
            PageData.InfoToolbar.Title = "Permessi per " + (Object.Type == GrantsType.Folder ? "la cartella " : "il documento ") + ": '" + Object.Name + "'";
            //PageData.SideToolbar.addButton(new SideToolbarButton("Aggiungi Gruppo", PageData.Paths.AbsoluteNetworkRoot + "/cms/grants/group_add.aspx?" + link, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Chart_Organisation), "Aggiungi Gruppo", PageData));
            //PageData.SideToolbar.addButton(new SideToolbarButton("Aggiungi Utente", PageData.Paths.AbsoluteNetworkRoot + "/cms/grants/user_add.aspx?" + link, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Vcard), "Aggiungi Utente", PageData));

        }

        public StructureGrantsWizard(PageData pageData)
        {
            PageData = pageData;
            int ID = PageData.checkNumericGetExist("doc", "");
            if (ID != 0)
            {
                Object = new GrantsObjectType(pageData, GrantsType.Document, ID);
            }
            else
            {
                ID = PageData.checkNumericGetExist("folder", PageData.BackAddress);
                Object = new GrantsObjectType(pageData, GrantsType.Folder, ID);
            }
        }

        public HtmlGenericControl TableView()
        {
            HtmlGenericControl output = new HtmlGenericControl("div");
            if (Object.Allowed)
            {
                string Target = Object.Type == GrantsType.Document ? "Doc" : "Folder";

                string Utente =  PageData.Account.NomeCompleto + " (" + PageData.Account.UserName + ")";
                string logText = "L'utente " + Utente;

                if (Object.Type == GrantsType.Document)
                {
                    logText += " vuole Gestire i Permessi per il documento " + PageData.CurrentFolder.FindDoc(Object.ID.ToString()).Label;
                }
                else
                {
                    logText += " vuole Gestire i Permessi per la cartella  " + PageData.CurrentFolder.Nome + " (" + PageData.CurrentFolder.ID + ")";
                }

                PageData.GuiLog.SaveLogRecord(logText, false);
                SetToolbars();

                EntityView table = new EntityView(PageData, Object);
                output.Controls.Add(table.getView());
            }
            else
                PageData.GoToErrorPage();

            return output;
        }

        public HtmlGenericControl WizardView()
        {
            HtmlGenericControl output = new HtmlGenericControl("div");
            SetToolbars();

            if (Object.Type == GrantsType.Folder)
                output.Controls.Add(new FolderWizard(PageData, Object).getView());
            else
                output.Controls.Add(new DocWizard(PageData, Object).getView());

            return output;
        }

    }*/
}

namespace NetCms.Structure.Grants.Wizard
{
    public class GrantsWizardEntityView : NetCms.GUI.NetworkPage
    {
        public override string LocalCssClass
        {
            get { return ""; }
        }
        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Key; }
        }

        public override string PageTitle
        {
            get
            {
                string s = "Permessi per " + (Object.GrantObjectType == GrantObjectType.Folder ? "la cartella " : "il documento ") + " '" + ((WebFSObject)Object).Nome + "'";

                return s;
            }
        }

        private IGrantsFindable Object;

        public GrantsWizardEntityView()
        {
            int ID = PageData.checkNumericGetExist("doc", "");
            if (ID != 0)
            {
                Object = DocumentBusinessLogic.GetById(ID);
            }
            else
            {
                ID = PageData.checkNumericGetExist("folder", PageData.BackAddress);
                Object = FolderBusinessLogic.GetById(ID);
            }
        }

        protected override WebControl BuildControls()
        {
            WebControl output = new WebControl(HtmlTextWriterTag.Div);
            //Se posso gestire i permessi
            if (Object.Criteria[CriteriaTypes.Grants].Allowed)
            {
                //string Target = Object.Type == GrantsType.Document ? "Doc" : "Folder";

                //string Utente = PageData.Account.NomeCompleto + " (" + PageData.Account.UserName + ")";
                //string logText = "L'utente " + Utente;                
                /*if (Object.Type == GrantsType.Document)
                {
                    logText += " vuole Gestire i Permessi per il documento " + PageData.CurrentFolder.FindDoc(Object.ID.ToString()).Label;
                    
                }
                else
                {                    
                    logText += " vuole Gestire i Permessi per la cartella  " + PageData.CurrentFolder.Nome + " (" + PageData.CurrentFolder.ID + ")";
                }
                */
                //PageData.GuiLog.SaveLogRecord(logText, false);

                EntityView table = new EntityView(PageData, Object);
                output.Controls.Add(table.getView());
            }
            else
                PageData.GoToErrorPage();

            return output;
        }
    }
}