﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NetCms.DBSessionProvider;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;

namespace NetCms.Grants
{
    public static class GrantsBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static IList<Criterio> FindAllCriteri(ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Criterio> dao = new CriteriaNhibernateDAO<Criterio>(innerSession);
            return dao.FindAll();
        }

        public static Criterio GetCriterioByKey(string key, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            SearchParameter keySP = new SearchParameter(null, "Key", key, Finder.ComparisonCriteria.Equals, false);

            IGenericDAO<Criterio> dao = new CriteriaNhibernateDAO<Criterio>(innerSession);
            return dao.GetByCriteria(new SearchParameter[] { keySP });
        }

        public static Criterio GetCriterioById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Criterio> dao = new CriteriaNhibernateDAO<Criterio>(innerSession);
            return dao.GetById(id);
        }

        public static IList<DocProfile> GetDocProfiles(int documentId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<DocProfile> dao = new CriteriaNhibernateDAO<DocProfile>(innerSession);
            SearchParameter documentSP = new SearchParameter(null, "WfsObject.ID", documentId, Finder.ComparisonCriteria.Equals, false);
            
            return dao.FindByCriteria(new SearchParameter[] { documentSP });
        }

        public static IList<FolderProfile> GetFolderProfiles(int folderId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<FolderProfile> dao = new CriteriaNhibernateDAO<FolderProfile>(innerSession);
            SearchParameter folderSP = new SearchParameter(null, "WfsObject.ID", folderId, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[] { folderSP });
        }

        public static DocProfile SaveOrUpdateDocProfile(DocProfile docProfile, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<DocProfile> dao = new CriteriaNhibernateDAO<DocProfile>(innerSession);
            return dao.SaveOrUpdate(docProfile);
        }

        public static FolderProfile SaveOrUpdateFolderProfile(FolderProfile folderProfile, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<FolderProfile> dao = new CriteriaNhibernateDAO<FolderProfile>(innerSession);
            return dao.SaveOrUpdate(folderProfile);
        }

        public static DocProfileCriteria SaveOrUpdateDocProfileCriteria(DocProfileCriteria docProfileCriteria, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<DocProfileCriteria> dao = new CriteriaNhibernateDAO<DocProfileCriteria>(innerSession);
            return dao.SaveOrUpdate(docProfileCriteria);
        }

        public static FolderProfileCriteria SaveOrUpdateFolderProfileCriteria(FolderProfileCriteria folderProfileCriteria, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<FolderProfileCriteria> dao = new CriteriaNhibernateDAO<FolderProfileCriteria>(innerSession);
            return dao.SaveOrUpdate(folderProfileCriteria);
        }


        public static bool CheckIfProfileIsAssociatedToDoc(int documentId,int profileId,ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<DocProfile> dao = new CriteriaNhibernateDAO<DocProfile>(innerSession);
            SearchParameter documentSP = new SearchParameter(null, "WfsObject.ID", documentId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter profileSP = new SearchParameter(null, "Profile.ID", profileId, Finder.ComparisonCriteria.Equals, false);

            DocProfile founded = dao.GetByCriteria(new SearchParameter[] { documentSP, profileSP });

            return founded != null;
        }

        public static bool CheckIfProfileIsAssociatedToFolder(int folderId, int profileId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<FolderProfile> dao = new CriteriaNhibernateDAO<FolderProfile>(innerSession);
            SearchParameter folderSP = new SearchParameter(null, "WfsObject.ID", folderId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter profileSP = new SearchParameter(null, "Profile.ID", profileId, Finder.ComparisonCriteria.Equals, false);

            FolderProfile founded = dao.GetByCriteria(new SearchParameter[] { folderSP, profileSP });

            return founded != null;
        }


        public static void DeleteDocProfile(int docProfileId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<DocProfile> dao = new CriteriaNhibernateDAO<DocProfile>(innerSession);

            DocProfile docProfile = dao.GetById(docProfileId);

            dao.Delete(docProfile);
        }

        public static void DeleteFolderProfile(int folderProfileId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<FolderProfile> dao = new CriteriaNhibernateDAO<FolderProfile>(innerSession);

            FolderProfile folderProfile = dao.GetById(folderProfileId);

            dao.Delete(folderProfile);
        }

        public static void DeleteFolderProfilesByProfile(int profileId,int networkId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<FolderProfile> dao = new CriteriaNhibernateDAO<FolderProfile>(innerSession);
            SearchParameter profileSP = new SearchParameter(null, "Profile.ID", profileId, Finder.ComparisonCriteria.Equals, false);

            IList<FolderProfile> founded = dao.FindByCriteria(new SearchParameter[] { profileSP }).Where(x=>(x.WfsObject as StructureFolder).NetworkID == networkId).ToList();
            foreach (FolderProfile folderProfile in founded)
                dao.Delete(folderProfile);
        }

        public static void DeleteFolderProfilesByFolder(int folderId, int networkId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            StructureFolder folder = FolderBusinessLogic.GetById(folderId);
            IGenericDAO<DocProfile> dDao = new CriteriaNhibernateDAO<DocProfile>(innerSession);
            ////Cancellazione profili per i documenti
            //foreach (Document doc in folder.Documents)
            //{
            //    SearchParameter docSP = new SearchParameter(null, "WfsObject.ID", doc.ID, Finder.ComparisonCriteria.Equals, false);

            //    IList<DocProfile> foundedDocs = dDao.FindByCriteria(new SearchParameter[] { docSP });
            //    foreach (DocProfile docProfile in foundedDocs)
            //    {
            //        dDao.Delete(docProfile);
            //    }
            //}

            IGenericDAO<FolderProfile> fDao = new CriteriaNhibernateDAO<FolderProfile>(innerSession);
            //SearchParameter folderSP = new SearchParameter(null, "WfsObject.ID", folderId, Finder.ComparisonCriteria.Equals, false);

            //IList<FolderProfile> founded = fDao.FindByCriteria(new SearchParameter[] { folderSP }).Where(x => (x.WfsObject as StructureFolder).NetworkID == networkId).ToList();
            //foreach (FolderProfile folderProfile in founded)
            //{
            //    fDao.Delete(folderProfile);
            //}

            IList<StructureFolder> allSubFolders = FolderBusinessLogic.FindFoldersBySubTree(folder.Tree, networkId, innerSession);
            foreach (StructureFolder subFold in allSubFolders)
            {
                foreach (Document doc in subFold.Documents)
                {
                    SearchParameter docSP = new SearchParameter(null, "WfsObject.ID", doc.ID, Finder.ComparisonCriteria.Equals, false);

                    IList<DocProfile> foundedDocs = dDao.FindByCriteria(new SearchParameter[] { docSP });
                    foreach (DocProfile docProfile in foundedDocs)
                    {
                        dDao.Delete(docProfile);
                    }
                }


                SearchParameter subfolderSP = new SearchParameter(null, "WfsObject.ID", subFold.ID, Finder.ComparisonCriteria.Equals, false);

                IList<FolderProfile> subFounded = fDao.FindByCriteria(new SearchParameter[] { subfolderSP }).Where(x => (x.WfsObject as StructureFolder).NetworkID == networkId).ToList();
                foreach (FolderProfile folderProfile in subFounded)
                {
                    fDao.Delete(folderProfile);
                }
            }
        }


        public static DocProfile GetDocProfileById(int docProfileId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<DocProfile> dao = new CriteriaNhibernateDAO<DocProfile>(innerSession);

            return dao.GetById(docProfileId);
        }

        public static FolderProfile GetFolderProfileById(int folderProfileId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<FolderProfile> dao = new CriteriaNhibernateDAO<FolderProfile>(innerSession);

            return dao.GetById(folderProfileId);
        }

        public static Criterio GetCriterioByNomeAndKeyAndTipoAndForDoc(string nome, string key, int tipo, bool forDoc, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<Criterio> dao = new CriteriaNhibernateDAO<Criterio>(session);
            SearchParameter nomeSp = new SearchParameter(null, "Nome", nome, Finder.ComparisonCriteria.Equals, false);
            SearchParameter keySp = new SearchParameter(null, "Key", key, Finder.ComparisonCriteria.Equals, false);
            SearchParameter tipoSp = new SearchParameter(null, "Tipo", tipo, Finder.ComparisonCriteria.Equals, false);
            SearchParameter forDocSp = new SearchParameter(null, "ForDoc", forDoc, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { nomeSp, keySp, tipoSp, forDocSp });
        }

        public static Criterio SaveOrUpdateCriterio(Criterio criterio, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<Criterio> dao = new CriteriaNhibernateDAO<Criterio>(session);

            return dao.SaveOrUpdate(criterio);
        }

        public static ICollection<Criterio> FindCriteriByTipo(int tipo, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<Criterio> dao = new CriteriaNhibernateDAO<Criterio>(session);
            SearchParameter tipoSp = new SearchParameter(null, "Tipo", tipo, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[] { tipoSp });
        }

        public static void DeleteCriterio(Criterio criterio, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<DocProfileCriteria> dpcDao = new CriteriaNhibernateDAO<DocProfileCriteria>(session);
            SearchParameter criteriaDocSp = new SearchParameter(null, "Criteria.ID", criterio.ID, Finder.ComparisonCriteria.Equals, false);

            ICollection<DocProfileCriteria> dpcList = dpcDao.FindByCriteria(new SearchParameter[] { criteriaDocSp });
            foreach (DocProfileCriteria dpc in dpcList)
            {
                dpcDao.Delete(dpc);
            }

            IGenericDAO<FolderProfileCriteria> opcDao = new CriteriaNhibernateDAO<FolderProfileCriteria>(session);
            SearchParameter criteriaSp = new SearchParameter(null, "Criteria.ID", criterio.ID, Finder.ComparisonCriteria.Equals, false);

            ICollection<FolderProfileCriteria> opcList = opcDao.FindByCriteria(new SearchParameter[] { criteriaSp });
            foreach (FolderProfileCriteria opc in opcList)
            {
                opcDao.Delete(opc);
            }

            IGenericDAO<Criterio> dao = new CriteriaNhibernateDAO<Criterio>(session);
            dao.Delete(criterio);
        }
    }
}
