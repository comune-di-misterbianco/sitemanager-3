﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.Applications;
using NetCms.Grants.Interfaces;


/// <summary>
/// Summary description for NewsFolder_NEW_
/// </summary>
namespace NetCms.Structure.WebFS
{
    public class FolderGrantsPage : NetCms.GUI.NetworkPage
    {
        public override NetCms.GUI.Icons.Icons PageIcon
        {
	        get { return NetCms.GUI.Icons.Icons.Key; }
        }
        
        public string _PageTitle;
        public override string PageTitle
        {
            get {
            return _PageTitle;
        
}
        }
        
        public override string LocalCssClass
        {
            get { return ""; }
        }

        public G2Core.Common.RequestVariable Show
        {
            get
            {
                if (_Show == null)
                    _Show = new G2Core.Common.RequestVariable("show", G2Core.Common.RequestVariable.RequestType.QueryString);
                return _Show;
            }
        }
        private G2Core.Common.RequestVariable _Show;

        protected override WebControl BuildControls()
        {
            IGrantsFindable obj = this.CurrentDocument != null ? (IGrantsFindable)this.CurrentDocument : (IGrantsFindable)this.CurrentFolder;
            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
            //string textLog = "L'utente con username " + userName;
            if (Show.StringValue == "o")
            {
                CriteriaOverview ctr = new CriteriaOverview(obj);
                _PageTitle += " assegnati per " + ctr.Association.Profile.EntityLabel;
                //textLog += this.CurrentDocument != null ? " ha modificato i permessi assegnati per " + ctr.Association.Profile.EntityLabel + "del documento " + this.CurrentDocument.Label : " ha modificato i permessi assegnati per " + ctr.Association.Profile.EntityLabel + "della cartella " + this.CurrentFolder.Label; 
                //NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, textLog , NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                return ctr;
            }
            else
            {
                CriteriaManager ctr = new CriteriaManager(obj);
                _PageTitle += " assegnati per " + ctr.Association.Profile.EntityLabel;
                //textLog += this.CurrentDocument != null ? " sta accedendo alla pagina di modifica dei permessi assegnati per " + ctr.Association.Profile.EntityLabel + "del documento " + this.CurrentDocument.Label : " sta accedendo alla pagina di modifica dei permessi assegnati per " + ctr.Association.Profile.EntityLabel + "della cartella " + this.CurrentFolder.Label;
                //NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, textLog, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                return ctr;
            }
        }

        public FolderGrantsPage()
        {
            string action = (Show.StringValue == "o") ? "Dettaglio" : "Modifica";
            if (this.CurrentDocument != null)
                _PageTitle = action + " Permessi per il documento '" + this.CurrentDocument.Nome + "'";
            else
                _PageTitle= action + " Permessi per cartella '" + this.CurrentFolder.LabeledPath + "'";
        }
        

    }
}
