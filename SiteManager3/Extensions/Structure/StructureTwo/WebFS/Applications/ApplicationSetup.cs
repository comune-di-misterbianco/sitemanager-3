﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Xml;
using G2Core.Caching;
using NetCms.Networks;
using HibernateMappingToXml;
using System.IO;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Grants;
using NetCms.Networks.WebFS;
using NHibernate;
using NetCms.DBSessionProvider;
using NetCms.Users;
using NetCms.Networks.Grants;

namespace NetCms.Structure.Applications
{
    public abstract class Setup
    {
        private int _NetworkID;
        public int NetworkID 
        {
            get { return _NetworkID; }
            private set { _NetworkID = value; }
        }

        private bool _CreateRecord = true;
        public bool CreateRecord
        {
            get { return _CreateRecord; }
            set { _CreateRecord = value; }
        }

        private bool _FilterID;
        public bool FilterID
        {
            get { return _FilterID; }
            private set { _FilterID = value; }
        }

        public abstract string[,] Pages
        {
            get;
        }

        public IList<BasicStructureNetwork> NetworksTable
        {
            get
            {
                //return this.Connection.SqlQuery("SELECT * FROM Networks");
                if (_NetworksTable == null)
                    _NetworksTable = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.Select(x => new BasicStructureNetwork(x.Value)).ToList(); //this.Connection.SqlQuery("SELECT * FROM Networks");
                return _NetworksTable;
            }
        }
        private IList<BasicStructureNetwork> _NetworksTable;

        public NetCms.Connections.Connection Connection
        {
            get
            {
                return ApplicationBaseData.Conn;
            }
        }	

        //public const string AddApplicationSql = "INSERT INTO applicazioni (id_Applicazione,SystemName_Applicazione,LabelPlurale_Applicazione,LabelSingolare_Applicazione,EnableReviews_Applicazione,Enabled_Applicazione,EnableMove_Applicazione,EnableNewsLetter_Applicazione,EnableModule_Applicazione) VALUES ({0},'{1}','{2}','{3}',{4},{5},{6},{7},{8})";
        //public const string AddApplicationPageSql = "INSERT INTO applicazioni_pagine (Pagename_PaginaApplicazione,ClassName_PaginaApplicazione,Applicazione_PaginaApplicazione) VALUES ('{0}','{1}',{2})";
        //public const string AddApplicationCriteriaSql = "INSERT INTO criteri (Nome_Criterio,Key_Criterio,Tipo_Criterio,ForDoc_Criterio) VALUES ('{0}','{1}',{2},{3})";
        //public const string SelectApplicationCriteriaSql = "SELECT * FROM criteri WHERE Nome_Criterio = '{0}' AND Key_Criterio = '{1}' AND Tipo_Criterio = {2} AND ForDoc_Criterio = 0";

        public Application ApplicationBaseData
        {
            get { return _ApplicationBaseData; }
            private set { _ApplicationBaseData = value; }
        }
        private Application _ApplicationBaseData;

        //public Application DbApp
        //{
        //    get 
        //    {
        //        if (_DbApp == null)
        //        {
        //            _DbApp = ApplicationBusinessLogic.GetApplicationById(this.ApplicationBaseData.ID);
        //        }
        //        return _DbApp; 
        //    }
        //}
        //private Application _DbApp;

        public Setup(Application baseApp)
        {
            ApplicationBaseData = baseApp;
        }

        private string errors;
        public string Errors
        {
            get
            {
                if (errors == null)
                {
                    errors = "";
                }
                return "<ul>" + errors + "</ul>";
            }
        }
        protected void AddErrorString(string errorToAdd)
        {
            errors += "<li>" + errorToAdd + "</li>";
            //LogAndTrance.ExceptionManager.ExceptionLogger.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, errorToAdd, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Warning);
            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, errorToAdd, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
        }
        public bool Install() { return Install(0); }

        public bool Install(int netID, bool addRec)
        {
            CreateRecord = addRec;
            return Install(netID);
        }

        public bool Install(int netID)
        {
            this.NetworkID = netID;
            bool FolderClassFound = false;
            bool DocumentClassFound = false;
            bool ModuleClassFound = false;
            bool OverviewClassFound = false;
            bool ReviewClassFound = false;


            Type[] types = ApplicationBaseData.Assembly.GetTypes();
            
            Type FolderType = Type.GetType("NetCms.Structure.WebFS.StructureFolder");
            Type DocumentType = Type.GetType("NetCms.Structure.WebFS.Document");
            Type OverviewType = Type.GetType("NetCms.Structure.WebFS.DocumentOverview");
            Type ReviewType = Type.GetType("NetCms.Structure.WebFS.DocumentReviewPage");

            foreach (Type type in types)
            {
                if (type.BaseType == FolderType) FolderClassFound = true;
                if (type.BaseType == DocumentType) DocumentClassFound = true;
                if (type.BaseType == OverviewType) OverviewClassFound = true;
                if (type.BaseType == ReviewType) ReviewClassFound = true;

                if (ApplicationsPool.HomepageApplication != null && ApplicationsPool.HomepageApplication.InstallState == Application.InstallStates.Installed)
                {
                    Type ModuleType = ApplicationsPool.HomepageApplication.Assembly.GetType("NetCms.Structure.Applications.Homepage.HomepageModule");
                    if (type.BaseType == ModuleType)
                        ModuleClassFound = true;
                }
                else ModuleClassFound = true;

            }

            bool ok = FolderClassFound && 
                      DocumentClassFound &&
                /*(!this.ApplicationBaseData.EnableModule || ModuleClassFound) &&*/
                      (!this.ApplicationBaseData.EnableReviews || (OverviewClassFound && ReviewClassFound));
            if (!ok)
            {
                if (!FolderClassFound) this.AddErrorString("Non è presente la classe derivata da NetCms.Structure.WebFS.StructureFolder, che specifica il cmportamento delle cartelle dell'applicazione.");
                if (!DocumentClassFound) this.AddErrorString("Non è presente la classe derivata da NetCms.Structure.WebFS.Document, che specifica il cmportamento dei documenti dell'applicazione.");
                //if (!(!this.ApplicationBaseData.EnableModule || ModuleClassFound)) this.AddErrorString("Non è presente la classe derivata da NetCms.Structure.Applications.Homepage.HomepageModule, che specifica il comportamento dei moduli Homepage dell'applicazione.");
                if (!(!this.ApplicationBaseData.EnableReviews || OverviewClassFound && ReviewClassFound)) this.AddErrorString("Non è presente una o più delle classi derivate daNetCms.Structure.WebFS.DocumentOverview o NetCms.Structure.WebFS.DocumentReviewPage, che specificano il comportamento dei moduli delle classi di gestione delle revisioni dell'applicazione.");
            }
            else
            {
                using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = session.BeginTransaction())
                {
                    try
                    {
                        if (NetworkID <= 0)
                        {
                            this.FilterID = false;
                            Application dbApp = this.AddApplication(session);
                            this.AddPages(session, dbApp);
                            ApplicationBusinessLogic.SaveOrUpdateApplication(dbApp, session);
                        }
                        else this.FilterID = true;

                        this.AddCriteria(session);
                        tx.Commit();
                        ok = true;
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        ok = false;
                    }
                }

                if (ok)
                {

                    FolderGrants.RefreshCriteri();

                    if (ok) ok = this.LocalInstall();
                    if (ok) ok = this.CreateApplicationConfig();

                    if (ok)
                    {
                        using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())
                        using (ITransaction tx = session.BeginTransaction())
                        {
                            ApplicationBaseData.UpdateApplicationData();
                        }
                    }
                }


                //if (NetworkID <= 0)
                //{
                //    this.FilterID = false;//"";
                //    ok = this.AddApplication();
                //    if (ok) ok = this.AddPages();
                //}
                //else this.FilterID = true;//"id_Network = " + NetworkID;

                //if (ok) ok = this.AddCriteria();
                //if (ok) ok = this.LocalInstall();
                //if (ok) ok = this.CreateApplicationConfig();
                ////if (ok) ok = this.CreateTables();
                //if (ok) ApplicationBaseData.UpdateApplicationData();
            }

            return ok;
        }
        protected abstract bool LocalInstall(bool isInsidePackage = false);

        public virtual void Uninstall()
        {
            Application dbApp = null;

            if (!(NetworkID > 0))
            {
                using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = session.BeginTransaction())
                {
                    try
                    {

                        //Elimino tutte le cartelle e i documenti relativi a questa applicazione
                        IList<StructureFolder> folderToRemove = FolderBusinessLogic.FindFoldersByApplication(this.ApplicationBaseData.ID, session);
                        foreach (StructureFolder folder in folderToRemove)
                        {
                            //GrantsBusinessLogic.DeleteFolderProfilesByFolder(folder.ID, folder.NetworkID, session);

                            var network = NetCms.Networks.NetworksManager.GlobalIndexer.GetNetworkByID(folder.NetworkID);
                            folder.Parent.Folders.Remove(folder);
                            TrashBusinessLogic.TrashBusinessLogic.DeleteFolder(folder.ID, network, session);
                        }


                        //string sql = "DELETE FROM criteri WHERE Tipo_Criterio = " + this.ApplicationBaseData.ID;
                        //Connection.Execute(sql);

                        //sql = "DELETE FROM applicazioni WHERE id_Applicazione = " + this.ApplicationBaseData.ID;
                        //Connection.Execute(sql);

                        //sql = "DELETE FROM applicazioni_pagine WHERE Applicazione_PaginaApplicazione = " + this.ApplicationBaseData.ID;
                        //Connection.Execute(sql);

                        foreach (Criterio criterio in GrantsBusinessLogic.FindCriteriByTipo(this.ApplicationBaseData.ID, session))
                        {
                            GrantsBusinessLogic.DeleteCriterio(criterio, session);
                        }
                        dbApp = ApplicationBusinessLogic.GetApplicationById(this.ApplicationBaseData.ID, session);
                        ApplicationBusinessLogic.DeleteApplication(dbApp, session);

                        tx.Commit();
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                    }
                }
            }
            ApplicationBaseData.UpdateApplicationData();
        }

        protected void AddPage(string pageName, string codeClass, ISession session, Application dbApp = null)
        {
            try
            {
                //string sql = string.Format(NetCms.Structure.Applications.Setup.AddApplicationPageSql, pageName, codeClass, this.ApplicationBaseData.ID);
                //done = Connection.Execute(sql);
                //if (done == false) throw new Exception();
                //return true;

                if (dbApp == null)
                    dbApp = ApplicationBusinessLogic.GetApplicationById(this.ApplicationBaseData.ID, session);

                ApplicationPage page = new ApplicationPage();
                page.PageName = pageName;
                page.ClassName = codeClass;
                page.Application = dbApp;

                dbApp.ApplicationPages.Add(page);

                //ApplicationBusinessLogic.SaveOrUpdateApplication(dbApp, session);

            }
            catch (Exception e) 
            {
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile creare la pagina '" + pageName + "' dell'Applicazione nel database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "60");
                throw e;
            }
        }

        protected Application AddApplication(ISession session)
        {
            try
            {
                //string sql = string.Format(NetCms.Structure.Applications.Setup.AddApplicationSql, 
                //                            this.ApplicationBaseData.ID, 
                //                            this.ApplicationBaseData.SystemName, 
                //                            this.ApplicationBaseData.Label, 
                //                            this.ApplicationBaseData.LabelSingolare,
                //                            this.ApplicationBaseData.EnableReviews,
                //                            1,
                //                            this.ApplicationBaseData.EnableMove,
                //                            this.ApplicationBaseData.EnableNewsLetter,
                //                            this.ApplicationBaseData.EnableModule
                //                            );
                //done = Connection.Execute(sql);
                //if (done == false) throw new Exception();
                //return true;

                Application app = new Application();
                app.ID = this.ApplicationBaseData.ID;
                app.Enabled = true;
                app.SystemName = this.ApplicationBaseData.SystemName;
                app.Label = this.ApplicationBaseData.Label;

                //ApplicationBusinessLogic.SaveOrUpdateApplication(app,session);
                return app;
            }
            catch (Exception e)
            {
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile inserire l'Applicazione nel database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "61");
                throw e;
            }
        }

        protected void AddCriteria(ISession session)
        {
            try
            {
                //string sql = String.Format(NetCms.Structure.Applications.Setup.SelectApplicationCriteriaSql, "Creazione Cartella " + this.ApplicationBaseData.Label, "newfolder_" + this.ApplicationBaseData.SystemName, this.ApplicationBaseData.ID);
                //DataTable test = Connection.SqlQuery(sql);

                Criterio criterio = GrantsBusinessLogic.GetCriterioByNomeAndKeyAndTipoAndForDoc("Creazione Cartella " + this.ApplicationBaseData.Label, "newfolder_" + this.ApplicationBaseData.SystemName, this.ApplicationBaseData.ID, false, session);

                //sql = String.Format(NetCms.Structure.Applications.Setup.AddApplicationCriteriaSql, "Creazione Cartella " + this.ApplicationBaseData.Label, "newfolder_" + this.ApplicationBaseData.SystemName, this.ApplicationBaseData.ID, 0);
                //int idCriterio = Connection.ExecuteInsert(sql);
                //done = idCriterio > 0;

                if (criterio == null)
                {
                    criterio = new Criterio();
                    criterio.Nome = "Creazione Cartella " + this.ApplicationBaseData.Label;
                    criterio.Key = "newfolder_" + this.ApplicationBaseData.SystemName;
                    criterio.Tipo = this.ApplicationBaseData.ID;
                    criterio.ForDoc = false;

                    GrantsBusinessLogic.SaveOrUpdateCriterio(criterio, session);

                    AddGrants(criterio, session);
                }
            }
            catch (Exception e)
            {
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile inserire i criteri per l'Applicazione nel database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "140");
                throw e;
            }
        }

        public void AddPages(ISession session, Application dbApp = null)
        {
            if (this.Pages != null)
            {
                for (int i = 0; i < this.Pages.Length / 2; i++)
                    this.AddPage(this.Pages[i, 0], this.Pages[i, 1], session, dbApp);
            }
        }

        //protected bool CreateTables()
        //{
        //    // TODO aggiungere il codice per le constraints
        //    bool Failed = false;
        //    var xmlNodes = this.ApplicationBaseData.DatabaseDataXmlDocument.SelectNodes("/DatabaseMap/table");
        //    DBComparer.Database database = new DBComparer.MySql.MySqlDatabase(this.Connection.DatabaseName, this.Connection);

        //    foreach (var network in NetCms.Networks.NetworksManager.GlobalIndexer.WithCms)
        //    {
        //        if (!Failed)
        //        {
        //            foreach (XmlNode node in xmlNodes)
        //            {
        //                BasicNetwork net = network.Value;
        //                string tableName = network.Value.SystemName + "_" + node.Attributes["TableName"].InnerXml.ToString();
        //                DBComparer.Table sourceTable = new DBComparer.XML.XMLTable(tableName, node, net.SystemName);

        //                if (database.Tables.ContainsKey(tableName))
        //                {
        //                    var targetTable = database.Tables[tableName];
        //                    string script = "";

        //                    if (!targetTable.Compare(sourceTable, out script))
        //                        Failed = !this.Connection.Execute(script);
        //                }
        //                else
        //                {
        //                    string script = sourceTable.BuildCreateSQL();
        //                    Failed = !this.ApplicationBaseData.Connection.Execute(script);
        //                }
        //            }
        //        }
        //    }

        //    return !Failed;
        //}

        public bool CreateApplicationConfig()
        {
            //CODICE CHE GENERA IL FILE XML DELL'APPLICAZIONE A PARTIRE DAGLI XML DI MAPPING NHIBERNATE
   //         this.ApplicationBaseData.GenerateXmlFromHbm();
            
            return true;
        }

        public string DatabaseDataXmlFilePath
        {
            get
            {
                if (_DatabaseDataXmlFilePath == null)
                {
                    _DatabaseDataXmlFilePath = NetCms.Configurations.Paths.PhysicalRoot + "\\App_Data\\" + this.ApplicationBaseData.SystemName + ".xml";
                }
                return _DatabaseDataXmlFilePath;
            }
        }
        private string _DatabaseDataXmlFilePath;

        //private static XmlDocument document;
        //public static XmlDocument ConfigurationXmlDocument
        //{
        //    get
        //    {
        //        if (document == null)
        //        {
        //            document = new XmlDocument();
        //            try
        //            {
        //                document.Load(ConfigurationFilePath);
        //            }
        //            catch (Exception ex)
        //            {
        //                document = null;
        //                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, "Errore nel caricamento del file xml di configurazione delle applicazioni", "", "", "");
        //            }
        //        }
        //        return document;
        //    }
        //}

        public void AddGrants(Criterio criterio, ISession session)
        {
            User currentUser = UsersBusinessLogic.GetById(AccountManager.CurrentAccount.ID, session);

            foreach (BasicNetwork network in currentUser.AssociatedNetworks.Select(x => x.Network))
            {
                StructureNetwork decoratedNetwork = new StructureNetwork(network);
                StructureFolder rootFolder = FolderBusinessLogic.GetById(session, decoratedNetwork.RootFolder.ID);

                if (rootFolder.CheckIfProfileIsAssociated(currentUser.Profile.ID, session))
                {
                    FolderProfile fp = rootFolder.GetGrantObjectProfiles(session).Where(x => x.Profile.ID == currentUser.Profile.ID).First() as FolderProfile;
                    rootFolder.AddGrant(fp.ID, 1, criterio.ID, currentUser.ID, session);
                }
            }

            Cache cache = new Cache();
            cache.RemoveAll("");
        }

        //public bool AddGrants(int idCriterio)
        //{
        //    try
        //    {
        //        NetCms.Users.User currentUser = NetCms.Users.AccountManager.CurrentAccount;

        //        string sqlCheckIfAssociatedToTheRootFolder = "SELECT id_FolderProfile FROM folderprofiles WHERE Profile_FolderProfile = {0} AND Folder_FolderProfile = {1}";
        //        string sqlSelectRootFolder = "SELECT id_Folder FROM folders WHERE Network_Folder = {0} AND Depth_Folder = 1";

        //        string sqlInsert = "INSERT INTO folderprofilescriteria (FolderProfile_FolderProfileCriteria,Criteria_FolderProfileCriteria,Value_FolderProfileCriteria,Application_FolderProfileCriteria,Updater_FolderProfileCriteria) VALUES ({0},{1},1,0,{2})";
        //        //string sqlInsert = "INSERT INTO folderprofiles (Folder_FolderProfile,Profile_FolderProfile,Creator_FolderProfile) VALUES ({0},{1},{2})";
        //        //DataTable networks = Connection.SqlQuery("SELECT * FROM Networks WHERE id_Network IN ({0})".FormatByParameters(string.Join(",", currentUser.EnabledNetworks.Select(x => x.Key))));

        //        foreach (int networkID in currentUser.EnabledNetworks.Keys)
        //        {
        //            DataRow rootFolderRow = Connection.SqlQuery(sqlSelectRootFolder.FormatByParameters(networkID.ToString())).Rows.Cast<DataRow>().FirstOrDefault();
        //            if (rootFolderRow != null)
        //            {
        //                DataRow folderProfileRow = Connection.SqlQuery(sqlCheckIfAssociatedToTheRootFolder.FormatByParameters(currentUser.Profile, rootFolderRow[0].ToString())).Rows.Cast<DataRow>().FirstOrDefault();

        //                if (folderProfileRow != null)
        //                {
        //                    Connection.Execute(sqlInsert.FormatByParameters(folderProfileRow[0].ToString(), idCriterio, currentUser.ID));
        //                }
        //            }
        //        }
        //        return true;
        //    }
        //    catch { }
        //    finally
        //    {
        //        Cache cache = new Cache();
        //        cache.RemoveAll("");
        //    }
        //    return false;
        //}

        //public bool AddGrants(int idCriterio)
        //{
        //    try
        //    {
        //        NetCms.Users.User currentUser = NetCms.Users.AccountManager.CurrentAccount;

        //        string sqlCheckIfAssociatedToTheRootFolder = "SELECT id_FolderProfile FROM folderprofiles WHERE Profile_FolderProfile = {0} AND Folder_FolderProfile = {1}";
        //        string sqlSelectRootFolder = "SELECT id_Folder FROM {0}_folders WHERE Depth_Folder = 1";
        //        string sqlInsert = "INSERT INTO folderprofiles (Folder_FolderProfile,Profile_FolderProfile,Creator_FolderProfile) VALUES ({0},{1},{2})";
        //        //DataTable networks = Connection.SqlQuery("SELECT * FROM Networks WHERE id_Network IN ({0})".FormatByParameters(string.Join(",", currentUser.EnabledNetworks.Select(x => x.Key))));

        //        foreach (string networkName in currentUser.EnabledNetworks.Values)
        //        {
        //            DataRow rootFolderRow = Connection.SqlQuery(sqlSelectRootFolder.FormatByParameters(networkName)).Rows.Cast<DataRow>().FirstOrDefault();
        //            if (rootFolderRow != null)
        //            {
        //                DataRow folderProfileRow = Connection.SqlQuery(sqlCheckIfAssociatedToTheRootFolder.FormatByParameters(currentUser.Profile, rootFolderRow[0].ToString())).Rows.Cast<DataRow>().FirstOrDefault();

        //                if (folderProfileRow != null)
        //                {
        //                    Connection.Execute(sqlInsert.FormatByParameters(folderProfileRow[0].ToString(), currentUser.Profile.ID, currentUser.ID));
        //                }
        //            }
        //        }
        //        return true;
        //    }
        //    catch { }
        //    finally
        //    {
        //        Cache cache = new Cache();
        //        cache.RemoveAll("");
        //    }
        //    return false;
        //}

        //public string GetMD5Hash(string input)
        //{
        //    System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
        //    byte[] bs = System.Text.Encoding.UTF8.GetBytes(input);
        //    bs = x.ComputeHash(bs);
        //    System.Text.StringBuilder s = new System.Text.StringBuilder();
        //    foreach (byte b in bs)
        //    {
        //        s.Append(b.ToString("x2").ToLower());
        //    }
        //    string password = s.ToString();
        //    return password;
        //}
    }
}