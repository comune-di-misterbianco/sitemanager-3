﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications
{
    public class ApplicationMapping : ClassMap<Application>
    {
        // commenti inseriti per far notare che nella tabella esistono anche i campi commentati,
        // ma per le operazioni di lettura e modifica su db, usiamo solo i campi mappati
        public ApplicationMapping()
        {
            Table("applicazioni");
            Id(x => x.ID).Column("id_Applicazione").GeneratedBy.Assigned();
            Map(x => x.SystemName).Column("SystemName_Applicazione");
            Map(x => x.Label).Column("LabelPlurale_Applicazione");
            //Map(x => x.LabelSingolare).Column("LabelSingolare_Applicazione");
            //Map(x => x.EnableReviews).Column("EnableReviews_Applicazione");
            Map(x => x.Enabled).Column("Enabled_Applicazione");
            //Map(x => x.EnableMove).Column("EnableMove_Applicazione");
            //Map(x => x.EnableNewsLetter).Column("EnableNewsLetter_Applicazione");
            //Map(x => x.EnableModule).Column("EnableModule_Applicazione");
            //Map(x => x.Namespace).Column("Namespace_Applicazione");
            HasMany(x => x.ApplicationPages)
                .KeyColumn("Applicazione_PaginaApplicazione")
                .Fetch.Select().AsSet()
                .Cascade.All()
                .Inverse();
        }
    }
}
