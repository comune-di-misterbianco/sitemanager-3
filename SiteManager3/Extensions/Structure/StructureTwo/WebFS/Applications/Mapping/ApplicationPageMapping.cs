﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Structure.Applications
{
    public class ApplicationPageMapping : ClassMap<ApplicationPage>
    {
        public ApplicationPageMapping()
        {
            Table("applicazioni_pagine");
            Id(x => x.ID).Column("id_PaginaApplicazione");
            Map(x => x.PageName).Column("Pagename_PaginaApplicazione");
            Map(x => x.ClassName).Column("ClassName_PaginaApplicazione").CustomSqlType("MEDIUMTEXT");
            References(x => x.Application)
                .Column("Applicazione_PaginaApplicazione")
                .Not.Nullable()
                .Fetch.Select();
        }
    }
}
