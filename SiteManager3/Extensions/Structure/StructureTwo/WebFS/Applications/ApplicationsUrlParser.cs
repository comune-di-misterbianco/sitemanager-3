﻿using System;
using System.Web;
using System.Data;
using System.Reflection;

namespace NetCms
{
    public class ApplicationsUrlParser
    {
        public enum BasicPaths { Applications, NetworksCmsApplications }

        public string  SubPath
        {
            get { return _SubPath; }
            private set { _SubPath = value; }
        }
        private string  _SubPath;

        public ApplicationsUrlParser()
        {
        }

        public bool Parse(BasicPaths basePath)
        {
            switch (basePath)
            {
                case BasicPaths.Applications: return Parse("applications"); break;
                case BasicPaths.NetworksCmsApplications: return Parse("networks/cms/applications"); break;
            }
            return false;           
        }

        public bool Parse(string BasePath)
        {
            try
            {
                BasePath = ("/" + BasePath + "/").ToLower();
                while (BasePath.Contains("//"))
                    BasePath = BasePath.Replace("//", "/");
                string[] splitter = { BasePath };
                string url = HttpContext.Current.Request.Url.ToString().ToLower();
                url = url.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[1];
                string path = url.Split('?')[0];
                
                this.SubPath = path;

                return true;
            }
            catch (Exception ex){}

            return false;
        }

        private string[] SplitAtFirstSlash(string path)
        {
            string[] value = new string[2];
            int IndexOfSlash = path.IndexOf('/');
            value[0] = path.Substring(
                                        0,
                                        IndexOfSlash
                                     );
            value[1] = path.Substring(
                                        IndexOfSlash + 1,
                                        (path.Length - IndexOfSlash) - 1
                                     );
            return value;
        }
    }
}
