﻿using System;
using System.Data;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Collections.Generic;
using NetCms.Networks;
using NHibernate;
using NetCms.Grants;

namespace NetCms.Structure.Applications
{
    public class ApplicationFixer
    {
        #region Sql Models
        
//        protected const string SelectApplicationSql = @"SELECT * FROM applicazioni WHERE id_applicazione = {0}
//                                                    AND SystemName_Applicazione = '{1}'
//                                                    AND LabelPlurale_Applicazione = '{2}'
//                                                    AND LabelSingolare_Applicazione = '{3}'
//                                                    AND EnableReviews_Applicazione = {4}
//                                                    AND Enabled_Applicazione = {5}
//                                                    AND EnableMove_Applicazione = {6}
//                                                    AND EnableNewsLetter_Applicazione = {7}
//                                                    AND EnableModule_Applicazione = {8}";

//        protected const string SelectPageSql = @"SELECT * FROM applicazioni_pagine WHERE Pagename_PaginaApplicazione = '{0}'
//                                                    AND ClassName_PaginaApplicazione = '{1}'
//                                                    AND Applicazione_PaginaApplicazione = {2}";

//        protected const string SelectCriteriaSql = @"SELECT * FROM criteri WHERE Nome_Criterio = '{0}' 
//                                                    AND Key_Criterio = '{1}'
//                                                    AND Tipo_Criterio = {2}
//                                                    AND ForDoc_Criterio = {3}";

//        protected const string RepairApplicationSql = @"UPDATE applicazioni SET 
//                                                        SystemName_Applicazione = '{1}',
//                                                        LabelPlurale_Applicazione = '{2}',
//                                                        LabelSingolare_Applicazione = '{3}',
//                                                        EnableReviews_Applicazione = {4},
//                                                        Enabled_Applicazione = {5},
//                                                        EnableMove_Applicazione = {6},
//                                                        EnableNewsLetter_Applicazione = {7},
//                                                        EnableModule_Applicazione = {8}
//                                                        WHERE id_Applicazione = {0}";

        //protected const string DeletePagesSql = @"DELETE FROM applicazioni_pagine WHERE Applicazione_PaginaApplicazione = {0}";

//        protected const string RepairCriteriaSql = @"UPDATE criteri SET
//                                                    Nome_Criterio = '{1}',
//                                                    Key_Criterio = '{2}',
//                                                    ForDoc_Criterio = {3}
//                                                    WHERE Tipo_Criterio = {0}";

        //protected const string AddApplicationCriteriaSql = "INSERT INTO criteri (Nome_Criterio,Key_Criterio,Tipo_Criterio,ForDoc_Criterio) VALUES ('{1}','{2}',{0},{3})";
        #endregion

        public Application Application
        {
            get
            {
                return _Application;
            }
            private set
            {
                _Application = value;
            }
        }
        private Application _Application;

        public ApplicationFixer(Application app)
        {
            Application = app;
        }

        public bool Repair()
        {
            Application.ApplicationValidator.Validate();
            List<NetCms.Structure.Applications.Validation.ApplicationValidatorResult> list = Application.ApplicationValidator.ValidationResults;
            
            bool pagesJustRepaired = false;
            bool mapJustRepaired = false;

            foreach (NetCms.Structure.Applications.Validation.ApplicationValidatorResult validation in list)
            {
                if (validation.IsOk == NetCms.Structure.Applications.Validation.ApplicationValidatorResult.ResultsTypes.Error)
                {
                    switch (validation.RepairType)
                    {
                        case NetCms.Structure.Applications.Validation.ApplicationValidator.RepairTypes.Application: RepairRecord(validation); break;
                        case NetCms.Structure.Applications.Validation.ApplicationValidator.RepairTypes.Criteria: RepairRecord(validation); break;
//                        case NetCms.Structure.Applications.Validation.ApplicationValidator.RepairTypes.Custom: LocalRepair(); break;
                        case NetCms.Structure.Applications.Validation.ApplicationValidator.RepairTypes.Pool: break;
                        case NetCms.Structure.Applications.Validation.ApplicationValidator.RepairTypes.Pages:
                                if (!pagesJustRepaired && RepairSet(validation)) pagesJustRepaired = true;
                            break;
                        case NetCms.Structure.Applications.Validation.ApplicationValidator.RepairTypes.DatabaseMap:
                                if (!mapJustRepaired && RepairDatabaseMap()) mapJustRepaired = true;
                            break;
                    }
                }
            }
            
            G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
            this.Application.UpdateApplicationData();
            cache.RemoveAll("");

            bool repairSuccessfull = Application.ApplicationValidator.Validate();

            if (repairSuccessfull)
            {
                ApplicationsPool.NonActiveAssemblies.Remove(this.Application.ID.ToString());
                ApplicationsPool.ActiveAssemblies.Add(this.Application);
            }

            return repairSuccessfull;
        }
        private bool RepairRecord(NetCms.Structure.Applications.Validation.ApplicationValidatorResult validation)
        {
            bool done = true;

            using (ISession session = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    if (validation.RepairType == NetCms.Structure.Applications.Validation.ApplicationValidator.RepairTypes.Application)
                    {
                        //done = this.Application.Connection.Execute(String.Format(RepairApplicationSql,
                        //                                              this.Application.ID,
                        //                                              this.Application.SystemName,
                        //                                              this.Application.Label,
                        //                                              this.Application.LabelSingolare,
                        //                                              this.Application.EnableReviews,
                        //                                              1,
                        //                                              this.Application.EnableMove,
                        //                                              this.Application.EnableNewsLetter,
                        //                                              this.Application.EnableModule));
                        Application dbApp = ApplicationBusinessLogic.GetApplicationById(this.Application.ID, session);
                        dbApp.Enabled = true;
                        dbApp.SystemName = this.Application.SystemName;
                        dbApp.Label = this.Application.Label;
                        ApplicationBusinessLogic.SaveOrUpdateApplication(dbApp, session);
                    }

                    if (validation.RepairType == NetCms.Structure.Applications.Validation.ApplicationValidator.RepairTypes.Criteria)
                    {
                        //Così di fatto non eseguirà mai update ma sempre insert. Valutare se è il caso di fare la select con una condizione meno stringente e quindi eseguire l'update sul record ottenuto
                        //bool exist = Application.Connection.SqlQuery(String.Format(SelectCriteriaSql, "Creazione Cartella " + this.Application.Label, "newfolder_" + this.Application.SystemName, this.Application.ID, 0)).Rows.Count > 0;
                        //if (!exist)
                        //{
                        //    done = Application.Connection.Execute(String.Format(AddApplicationCriteriaSql,
                        //                                                  this.Application.ID,
                        //                                                  "Creazione Cartella " + this.Application.Label,
                        //                                                  "newfolder_" + this.Application.SystemName,
                        //                                                  0));
                        //}
                        //else
                        //{
                        //    done = Application.Connection.Execute(String.Format(RepairCriteriaSql,
                        //                                                  this.Application.ID,
                        //                                                  "Creazione Cartella " + this.Application.Label,
                        //                                                  "newfolder_" + this.Application.SystemName,
                        //                                                  0));
                        //}

                        Criterio criterio = GrantsBusinessLogic.GetCriterioByNomeAndKeyAndTipoAndForDoc("Creazione Cartella " + this.Application.Label, "newfolder_" + this.Application.SystemName, this.Application.ID, false, session);
                        if (criterio == null)
                        {
                            criterio = new Criterio();
                        }
                        criterio.Nome = "Creazione Cartella " + this.Application.Label;
                        criterio.Key = "newfolder_" + this.Application.SystemName;
                        criterio.Tipo = this.Application.ID;
                        criterio.ForDoc = false;
                        GrantsBusinessLogic.SaveOrUpdateCriterio(criterio, session);
                    }

                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    done = false;
                }
            }

            return done;
        }
        private bool RepairSet(NetCms.Structure.Applications.Validation.ApplicationValidatorResult validation)
        {
            bool done = true;

            using (ISession session = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    if (validation.RepairType == NetCms.Structure.Applications.Validation.ApplicationValidator.RepairTypes.Pages)
                    {
                        //done = Application.Connection.Execute(String.Format(DeletePagesSql, this.Application.ID));
                        //done = done & this.Application.Installer.AddPages();

                        Application dbApp = ApplicationBusinessLogic.GetApplicationById(this.Application.ID, session);
                        dbApp.ApplicationPages.Clear();
                        Application.Installer.AddPages(session, dbApp);
                        ApplicationBusinessLogic.SaveOrUpdateApplication(dbApp,session);
                    }
                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    done = false;
                }
            }

            return done;
        }
        private bool RepairDatabaseMap()
        {
            DBComparer.Database database = new DBComparer.MySql.MySqlDatabase(this.Application.Conn.DatabaseName, this.Application.Conn);

            bool repairFailed = false;

            foreach (var network in NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks)
            {
                if (!repairFailed)
                {
                    foreach (var table in this.Application.AssociatedDatabaseTables)
                    {
                        if (database.Tables.ContainsKey(table))
                        {
                            BasicNetwork net = network.Value;
                            var xmlNode = this.Application.DatabaseDataXmlDocument.SelectSingleNode("/DatabaseMap/table[@TableName=\"" + table + "\"]");

                            DBComparer.Table targetTable = database.Tables[table];
                            DBComparer.Table sourceTable = new DBComparer.XML.XMLTable(table, xmlNode, net.SystemName);

                            string script = "";

                            if (!targetTable.Compare(sourceTable, out script))
                            {
                                //script = script.Replace("REFERENCES `", "REFERENCES `" + network.Value.SystemName + "_");
                                //script = script.Replace("FOREIGN KEY `", "FOREIGN KEY `" + network.Value.SystemName + "_");
                                //script = script.Replace("ADD CONSTRAINT `", "ADD CONSTRAINT `" + network.Value.SystemName + "_");
                                repairFailed = !this.Application.Conn.Execute(script);
                            }
                        }
                    }
                }
            }

            if(!repairFailed)
                Application.Installer.CreateApplicationConfig();

            return repairFailed;
        }
    }
}