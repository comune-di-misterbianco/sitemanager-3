﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.DBSessionProvider;

namespace NetCms.Structure.Applications
{
    public static class ApplicationsPool
    {
        private const string NotInstalledApplicationsKey = "NetCms.Structure.Applications.NotInstalledApplications";
        private const string InstalledApplicationsKey = "NetCms.Structure.Applications.InstalledApplications";
        public const string FaultyApplicationsKey = "NetCms.Structure.Applications.FaultyApplications";
        public const string AllApplicationsKey = "NetCms.Structure.Applications.AllApplications";

        public static NetCms.Structure.Applications.ApplicationCollection NonActiveAssemblies
        {
            get
            {
                if (_NonActiveAssemblies == null)
                {
                    if (HttpContext.Current.Application[NotInstalledApplicationsKey] != null)
                        _NonActiveAssemblies = (NetCms.Structure.Applications.ApplicationCollection)HttpContext.Current.Application[NotInstalledApplicationsKey];
                    else
                        HttpContext.Current.Application[NotInstalledApplicationsKey] = _NonActiveAssemblies = new ApplicationCollection();
                }
                return _NonActiveAssemblies;
            }
        }
        private static NetCms.Structure.Applications.ApplicationCollection _NonActiveAssemblies;

        public static NetCms.Structure.Applications.Application HomepageApplication
        {
            get
            {
                return _HomepageApplication;
            }
            set { _HomepageApplication = value; }
        }
        private static NetCms.Structure.Applications.Application _HomepageApplication;

        public static NetCms.Structure.Applications.ApplicationCollection Assemblies
        {
            get
            {
                if (_Assemblies == null)
                {
                    if (HttpContext.Current.Application[AllApplicationsKey] != null)
                        _Assemblies = (NetCms.Structure.Applications.ApplicationCollection)HttpContext.Current.Application[AllApplicationsKey];
                    else
                        HttpContext.Current.Application[AllApplicationsKey] = _Assemblies = new ApplicationCollection();
                }

                return _Assemblies;
            }
        }
        private static NetCms.Structure.Applications.ApplicationCollection _Assemblies;

        public static NetCms.Structure.Applications.ApplicationCollection ActiveAssemblies
        {
            get
            {
                if (_ActiveAssemblies == null)
                {
                    if (HttpContext.Current.Application[InstalledApplicationsKey] != null)
                        _ActiveAssemblies = (NetCms.Structure.Applications.ApplicationCollection)HttpContext.Current.Application[InstalledApplicationsKey];
                    else
                        HttpContext.Current.Application[InstalledApplicationsKey] = _ActiveAssemblies = new ApplicationCollection();
                       
                }

                return _ActiveAssemblies;
            }
        }
        private static NetCms.Structure.Applications.ApplicationCollection _ActiveAssemblies;

        public static NetCms.Structure.Applications.ApplicationCollection FaultyAssemblies
        {
            get
            {
                if (_FaultyAssemblies == null)
                {
                    if (HttpContext.Current.Application[FaultyApplicationsKey] != null)
                        _FaultyAssemblies = (NetCms.Structure.Applications.ApplicationCollection)HttpContext.Current.Application[FaultyApplicationsKey];
                    else
                        HttpContext.Current.Application[FaultyApplicationsKey] = _FaultyAssemblies = new ApplicationCollection();

                }

                return _FaultyAssemblies;
            }
        }
        private static NetCms.Structure.Applications.ApplicationCollection _FaultyAssemblies;

        public static object BuildInstanceOf(string className, string applicationID)
        {
            return BuildInstanceOf(className, applicationID, null);
        }

        public static object BuildInstanceOf(string className, string applicationID, object[] parameters)
        {
            if (ApplicationsPool.ActiveAssemblies.Contains(applicationID))
            {
                Application application = ActiveAssemblies[applicationID];
                Type ApplicationFolderType = application.Assembly.GetType(className);

                if (parameters != null) return System.Activator.CreateInstance(ApplicationFolderType, parameters);
                else return System.Activator.CreateInstance(ApplicationFolderType);
            }
            //Trace?
            return null;
        }

        private static WebControl typesListAccordion;
        public static WebControl TypesListAccordion
        {
            get
            {
                if (typesListAccordion == null)
                {
                    typesListAccordion = new WebControl(HtmlTextWriterTag.Div);
                    typesListAccordion.ID = "TypesListAccordionContainer";

                    G2Core.XhtmlControls.JavaScript script = new G2Core.XhtmlControls.JavaScript();
                    script.ScriptText = @"
$(document).ready(function(){
	$(function() {
	    $(""#TypesListAccordion"").accordion({ autoHeight: false });
    })
});
";
                    typesListAccordion.Controls.Add(script);

                    WebControl sheets = new WebControl(HtmlTextWriterTag.Div);
                    sheets.ID = "TypesListAccordion";
                    typesListAccordion.Controls.Add(sheets);

                    foreach (NetCms.Structure.Applications.Application app in NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies)
                    {
                        WebControl h3 = new WebControl(HtmlTextWriterTag.Div);
                        h3.Controls.Add(new LiteralControl("<a href=\"" + app.SystemName + "\">" + app.ID + ") " + app.SystemName + "</a>"));
                        sheets.Controls.Add(h3);

                        WebControl div = new WebControl(HtmlTextWriterTag.Div);
                        sheets.Controls.Add(div);

                        WebControl list = new WebControl(HtmlTextWriterTag.Ul);
                        div.Controls.Add(list);
                        list.Style.Add(HtmlTextWriterStyle.Height, "400px");
                        list.Style.Add(HtmlTextWriterStyle.Overflow, "auto");

                        foreach (System.Type type in app.Assembly.GetTypes())
                            list.Controls.Add(new LiteralControl("<li><p>" + type.ToString() + "<br />" + type.BaseType.ToString() + "</p></li>"));
                    }
                }
                return typesListAccordion;
            }
        }
        
        public static void ParseType(Type type, Assembly asm)
        {
            if (type.BaseType == Type.GetType("NetCms.Structure.Applications.Application"))
            {
                Object[] paramsArray = { asm };
                NetCms.Structure.Applications.Application application = (NetCms.Structure.Applications.Application)Activator.CreateInstance(type, paramsArray);

                if (application.ID == 1)
                    HomepageApplication = application;

                Assemblies.Add(application);

                bool active = application.InstallState == Application.InstallStates.Installed && application.Enabled;
                if (active)
                {
                    ActiveAssemblies.Add(application);
                }
                else NonActiveAssemblies.Add(application);

                if (application.Enabled)
                    FluentSessionProvider.FluentAssemblies.Add(application.Assembly);
            }
        }

        public static string ActivesAppzSqlFilter(string externalKey)
        {
            //string sql = "";
            //foreach (NetCms.Structure.Applications.Application application in NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies)
            //    sql += (sql.Length > 0 ? " OR" : "") + " " + externalKey + " = " + application.ID;

            //if (sql.Length > 0)
            //    sql = " AND (" + sql + ")";

            //return sql; 
            string[] ids =  NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies.Cast<NetCms.Structure.Applications.Application>().Select(x=>x.ID.ToString()).ToArray();
            string sql = " AND " + externalKey + " IN (" + string.Join("," , ids) + ")";
            
            return sql;
        }
    }
}