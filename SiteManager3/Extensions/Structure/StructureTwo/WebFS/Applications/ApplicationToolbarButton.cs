﻿using System;
using System.Data;
using System.Reflection;
using System.Text;

namespace NetCms.Structure.Applications
{
    public class ApplicationToolbarButton : NetCms.GUI.ToolbarButton
    {

        public ApplicationToolbarButton(string pagename, NetCms.GUI.Icons.Icons icon, string label, NetCms.Networks.INetwork network, Application relatedApplication)
            : base(network.Paths.AbsoluteAdminRoot + "/cms/applications/" + relatedApplication.SystemName + "/" + pagename, icon, label)
        {
        }
        public ApplicationToolbarButton(string pagename, string iconUrl, string label, NetCms.Networks.INetwork network, Application relatedApplication)
            : base(network.Paths.AbsoluteAdminRoot + "/cms/applications/" + relatedApplication.SystemName + "/" + pagename, iconUrl, label)
        {
        }
    }

}