﻿using System;
using System.Data;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Collections.Generic;
using System.IO;
using HibernateMappingToXml;
using NHibernate;
using NetCms.Connections;

namespace NetCms.Structure.Applications
{
    public class Application
    {
        //public abstract bool EnableNewsLetter { get; }
        //public abstract bool EnableMove { get; }
        //public abstract bool EnableModule { get; }
        //public abstract bool EnableReviews { get; }
        //public abstract string SystemName { get; }
        //public abstract string Label { get; }
        //public abstract string LabelSingolare { get; }
        //public abstract int ID { get; }
        //public abstract string Namespace { get; }
        //public abstract NetCms.Structure.Applications.Setup Installer { get; }
        //public abstract bool IsSystemApplication { get; }

        ///// <summary>
        ///// SystemName dei tipi di cartella consentiti.
        ///// 'all' = tutti.
        ///// </summary>
        //public abstract string[] AllowedSubfolders { get; }

        public virtual ICollection<ApplicationPage> ApplicationPages
        {
            get;
            set;
        }

        public virtual bool EnableNewsLetter 
        {
            get
            {
                return false;
            }
        }

        public virtual bool EnableMove 
        {
            get
            {
                return false;
            }
        }

        public virtual bool EnableModule 
        {
            get
            {
                return false;
            }
        }

        public virtual bool EnableReviews 
        {
            get 
            {
                return false;
            }
        }

        public virtual string SystemName
        {
            get;
            set;
        }

        public virtual string Label
        {
            get;
            set;
        }

        public virtual string LabelSingolare 
        {
            get
            {
                return "";
            }
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Namespace 
        {
            get
            {
                return "";
            }
        }

        public virtual NetCms.Structure.Applications.Setup Installer 
        {
            get
            {
                return null;
            }
        }

        public virtual bool IsSystemApplication 
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// SystemName dei tipi di cartella consentiti.
        /// 'all' = tutti.
        /// </summary>
        public virtual string[] AllowedSubfolders 
        {
            get
            {
                return null;
            }
        }

        public virtual Validation.ApplicationValidator ApplicationValidator
        {
            get
            {
                if (_ApplicationValidator == null)
                {
                    _ApplicationValidator = new Validation.ApplicationValidator(this);
                }
                return _ApplicationValidator;
            }
        }
        private Validation.ApplicationValidator _ApplicationValidator;

        public virtual string DatabaseDataXmlFilePath
        {
            get
            {
                if (_DatabaseDataXmlFilePath == null)
                {
                    _DatabaseDataXmlFilePath = NetCms.Configurations.Paths.PhysicalRoot + "\\App_Data\\" + this.SystemName + ".xml";
                }
                return _DatabaseDataXmlFilePath;
            }
        }
        private string _DatabaseDataXmlFilePath;

        public virtual XmlDocument DatabaseDataXmlDocument
        {
            get
            {
                //if (_DatabaseDataXmlDocument == null)
                //{
                    XmlDocument _DatabaseDataXmlDocument = new XmlDocument();
                    try
                    {
                        _DatabaseDataXmlDocument.Load(NetCms.Configurations.Paths.PhysicalRoot + "\\App_Data\\" + this.SystemName + ".xml");
                    }
                    catch (Exception ex)
                    {
                        _DatabaseDataXmlDocument = null;
                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, "Errore nel caricamento del file xml di installazione del database dell'applicazione '" + this.Label + "'", "", "", "");
                    }
                //}
                return _DatabaseDataXmlDocument;
            }
        }

        public virtual List<string> AssociatedDatabaseTables 
        {
            get
            {
                List<string> _AssociatedDatabaseTables = new List<string>();
                var nodes = DatabaseDataXmlDocument.SelectNodes("/DatabaseMap/table");

                foreach (XmlNode node in nodes)
                    _AssociatedDatabaseTables.Add(node.Attributes["TableName"].Value.ToString());
                
                return _AssociatedDatabaseTables;
            }
        }

        private Connection _Conn;
        public virtual Connection Conn
        {
            get
            {
                //if (_Conn == null)
                //    _Conn = NetCms.Connections.ConnectionsManager.CommonConnection;
                //return _Conn;

                ConnectionsManager ConnManager = new ConnectionsManager();
                return ConnManager.CommonConnection;
            }
        }

        public virtual bool Enabled { get; set; }

        public virtual ApplicationClasses Classes
        {
            get { return _Classes; }
            set { _Classes = value; }
        }
        private ApplicationClasses _Classes;

        public virtual Assembly Assembly { get; protected set; }

        public Application(Assembly assembly)
        {
            this.Assembly = assembly;
            this.Classes = new ApplicationClasses(this);
            UpdateApplicationData();
        }

        public Application()
        {
            ApplicationPages = new HashSet<ApplicationPage>();
        }

        public enum InstallStates { NotLoaded, NotInstalled, Installed, Error }

        private InstallStates _InstallState = InstallStates.NotLoaded;
        public virtual InstallStates InstallState
        {
            get
            {
                if (_InstallState == InstallStates.NotLoaded)
                {
                    UpdateInstallState();
                }
                return _InstallState;
            }
        }

        public virtual void UpdateInstallState()
        {
            if (this.ApplicationValidator.IsValid)
            {
                DataTable applications = Conn.SqlQuery("SELECT * FROM applicazioni WHERE id_Applicazione = " + this.ID);
                switch (applications.Rows.Count)
                {
                    case 0: _InstallState = InstallStates.NotInstalled; break;
                    case 1: _InstallState = InstallStates.Installed; break;
                    default: _InstallState = InstallStates.Error; break;
                }
            }
            else
            {
                _InstallState = InstallStates.Error;
            }
        }

        //public void UpdateInstallState(Application dbApp = null, ISession session = null)
        //{
        //    if (this.ApplicationValidator.IsValid)
        //    {
        //        if (dbApp == null)
        //            dbApp = ApplicationBusinessLogic.GetApplicationById(this.ID, session);

        //        if (dbApp != null)
        //            _InstallState = InstallStates.Installed;
        //        else _InstallState = InstallStates.NotInstalled;
        //    }
        //    else
        //    {
        //        _InstallState = InstallStates.Error;
        //    }
        //}

        public virtual void UpdateApplicationData()
        {
            DataTable tab = Conn.SqlQuery("SELECT * FROM applicazioni");
            DataRow[] Rows = tab.Select("id_Applicazione = " + this.ID);
            if (Rows.Length > 0)
            {
                this.Enabled = Rows[0]["Enabled_Applicazione"].ToString() == "1";
                UpdateInstallState();
            }
            else
            {
                _InstallState = InstallStates.NotInstalled;
            }
        }

        //public void UpdateApplicationData(Application dbApp = null, ISession session = null)
        //{
        //    if (session == null)
        //        session = ApplicationBusinessLogic.GetCurrentSession();

        //    if (dbApp == null)
        //        dbApp = ApplicationBusinessLogic.GetApplicationById(this.ID, session);
            
        //    if (dbApp != null)
        //    {
        //        this.Enabled = dbApp.Enabled;
        //        UpdateInstallState(dbApp, session);
        //    }
        //    else
        //    {
        //        _InstallState = InstallStates.NotInstalled;
        //    }
        //}

        //public bool Disable()
        //{
        //    bool ok = Connection.Execute("UPDATE applicazioni SET Enabled_Applicazione = 0 WHERE id_Applicazione = " + this.ID);
        //    this.Enabled = false;
        //    ApplicationsPool.ActiveAssemblies.Remove(this.ID.ToString());
        //    ApplicationsPool.NonActiveAssemblies.Add(this);
        //    G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
        //    cache.RemoveAll("");
        //    return ok;
        //}

        public virtual bool Disable()
        {
            try
            {
                Application dbApp = ApplicationBusinessLogic.GetApplicationById(this.ID);
                dbApp.Enabled = false;
                ApplicationBusinessLogic.SaveOrUpdateApplication(dbApp);

                this.Enabled = false;
                ApplicationsPool.ActiveAssemblies.Remove(this.ID.ToString());
                ApplicationsPool.NonActiveAssemblies.Add(this);
                G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
                cache.RemoveAll("");
                return true;
            }
            catch
            {
                return false;
            }
        }

        //public bool Enable()
        //{
        //    bool ok = Connection.Execute("UPDATE applicazioni SET Enabled_Applicazione = 1 WHERE id_Applicazione = " + this.ID);
        //    this.Enabled = true;
        //    ApplicationsPool.NonActiveAssemblies.Remove(this.ID.ToString());
        //    ApplicationsPool.ActiveAssemblies.Add(this);
        //    G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
        //    cache.RemoveAll("");
        //    return ok;
        //}

        public virtual bool Enable()
        {
            try
            {
                Application dbApp = ApplicationBusinessLogic.GetApplicationById(this.ID);
                dbApp.Enabled = true;
                ApplicationBusinessLogic.SaveOrUpdateApplication(dbApp);

                this.Enabled = true;
                ApplicationsPool.NonActiveAssemblies.Remove(this.ID.ToString());
                ApplicationsPool.ActiveAssemblies.Add(this);
                G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
                cache.RemoveAll("");
                return true;
            }
            catch
            {
                return false;
            }
        }

        public virtual bool SwitchState()
        {
            if (this.Enabled) return Disable();
            else return this.Enable();
        }

        //Metodo che genera il file di mapping xml dal file hbm di fluent per il monitoraggio
        public virtual void GenerateXmlFromHbm()
        {
            DirectoryInfo mappingDirInfo = new DirectoryInfo(MappingsFileDirectory);
            IEnumerable<FileInfo> extraFileToMap = mappingDirInfo.EnumerateFiles(this.Namespace + ".*");
            List<string> extraFiles = new List<string>();
            foreach (FileInfo info in extraFileToMap)
                extraFiles.Add(info.FullName);

            XmlToCustom converter = new XmlToCustom(DocsContentMappingFilePath, this.SystemName, extraFiles.ToArray());

            using (StreamWriter outfile =
            new StreamWriter(DatabaseDataXmlFilePath))
            {
                outfile.Write(converter.Customize());
            }
        }

        public static string DocsContentMappingFilePath
        {
            get
            {
                if (_DocsContentMappingFilePath == null)
                {
                    _DocsContentMappingFilePath = NetCms.Configurations.Paths.PhysicalRoot + "\\App_Data\\NetCms.Structure.WebFS.DocContent.hbm.xml";
                }
                return _DocsContentMappingFilePath;
            }
        }
        private static string _DocsContentMappingFilePath;

        public static string MappingsFileDirectory
        {
            get
            {
                if (_MappingsFileDirectory == null)
                {
                    _MappingsFileDirectory = NetCms.Configurations.Paths.PhysicalRoot + "\\App_Data\\";
                }
                return _MappingsFileDirectory;
            }
        }
        private static string _MappingsFileDirectory;

        #region Operazioni extra dopo l'importazione

        /// <summary>
        /// Questo metodo consente di essere ridefinito all'interno di ogni singola application al fine di eseguire operazioni extra
        /// dopo aver importato i file. In particolare per adattare le vecchie versioni delle applicazioni ai nuovi meccanismi di
        /// funzionamento del cms 3.
        /// </summary>
        /// <returns></returns>
        public virtual bool ExtraOperationsAfterImport()
        {
            return true;
        }

        #endregion

        public static Dictionary<string, NetCms.CmsApp.Configuration.Configuration> Configs
        {
            get
            {
                if (_ApplicationsConfigurations == null)
                {
                    _ApplicationsConfigurations = new Dictionary<string, NetCms.CmsApp.Configuration.Configuration>();
                }
                return _ApplicationsConfigurations;
            }
        }
        private static Dictionary<string, NetCms.CmsApp.Configuration.Configuration> _ApplicationsConfigurations;
    
    }

}