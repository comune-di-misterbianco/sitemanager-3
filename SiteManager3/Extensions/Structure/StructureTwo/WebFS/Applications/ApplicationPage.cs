﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.Applications
{
    public class ApplicationPage
    {
        public ApplicationPage()
        {

        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string PageName
        {
            get;
            set;
        }

        public virtual string ClassName
        {
            get;
            set;
        }

        public virtual Application Application
        {
            get;
            set;
        }
    }
}
