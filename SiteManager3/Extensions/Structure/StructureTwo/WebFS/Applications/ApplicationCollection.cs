using System;
using System.Reflection;
using System.Collections;
/// <summary>
/// Summary description for xNodeCollection
/// </summary>

namespace NetCms.Structure.Applications
{
    public class ApplicationCollection : IEnumerable
    {
        private G2Collection coll;

        public int Count { get { return coll.Count; } }

        public ApplicationCollection()
        {
            coll = new G2Collection();
        }

        public void Add(Application obj)
        {
            coll.Add(obj.ID.ToString(), obj);
        }

        public Application this[int i]
        {
            get
            {
                return (Application)coll[coll.Keys[i]];
            }
        }
        public Application this[string ApplicationID]
        {
            get
            {
                return (Application)coll[ApplicationID];
            }
        }

        public bool Contains(string key)
        {
            return this[key] != null;
        }
        public void Remove(string key)
        {
            coll.Remove(key);
        }
    #region Enumerator

    public IEnumerator GetEnumerator()
    {
        return new CollectionEnumerator(this);
    }

    private class CollectionEnumerator : IEnumerator
    {
        private int CurentPos = -1;
        private ApplicationCollection Collection;
        public CollectionEnumerator(ApplicationCollection coll)
        {
            Collection = coll;
        }
        public object Current
        {
            get
            {
                return Collection[CurentPos];
            }
        }
        public bool MoveNext()
        {
            if (CurentPos < Collection.Count - 1)
            {
                CurentPos++;
                return true;
            }
            else
                return false;
        }
        public void Reset()
        {
            CurentPos = -1;
        }
    }
    #endregion
    }
}