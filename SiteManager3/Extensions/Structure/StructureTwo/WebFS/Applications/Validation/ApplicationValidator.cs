﻿using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Users;

namespace NetCms.Structure.Applications.Validation
{
    public class ApplicationValidator
    {
        public enum RepairTypes
        {
            Custom,
            Pages,
            Application,
            Pool,
            Criteria,
            DatabaseMap
        }

        private bool _IsValid;
        public bool IsValid
        {
            get
            {
                if (ValidationResults.Count == 0)
                    Validate();

                return _IsValid;
            }
            private set { _IsValid = value; }
        }

        public List<ApplicationValidatorResult> ValidationResults
        {
            get
            {
                if (_ValidationResults == null)
                {
                    _ValidationResults = new List<ApplicationValidatorResult>();
                }
                return _ValidationResults;
            }
        }
        private List<ApplicationValidatorResult> _ValidationResults;

        public List<ApplicationValidation> Validators
        {
            get
            {
                if (_Validators == null)
                {
                    _Validators = new List<ApplicationValidation>();
                }
                return _Validators;
            }
        }
        private List<ApplicationValidation> _Validators;

        public Application Application
        {
            get
            {
                return _Application;
            }
            private set
            {
                _Application = value;
            }
        }
        private Application _Application;

        public NetCms.Connections.Connection Connection
        {
            get
            {
                return _Connection;
            }
            private set
            {
                _Connection = value;
            }
        }
        private NetCms.Connections.Connection _Connection;

        public ApplicationValidator(Application Application)
        {
            this.Application = Application;
            this.Connection = Application.Conn;

            //this.Validators.Add(new PoolValidation(Application));
            this.Validators.Add(new ApplicationRecordValidation(Application));
            this.Validators.Add(new PagesValidation(Application));
            this.Validators.Add(new CriteriaValidation(Application));
            //this.Validators.Add(new VersionValidation(Application));
            this.Validators.Add(new DatabaseValidation(Application));
        }

        public bool Validate()
        {
            ValidationResults.Clear();

            bool allOK = true;
            foreach (var validator in this.Validators)
            {
                validator.ValidationResults.Clear();
                validator.Validate();

                foreach (var validation in validator.ValidationResults)
                {
                    allOK = allOK & validation.IsOk != ApplicationValidatorResult.ResultsTypes.Error; 
                    this.ValidationResults.Add(validation);
                }
            }
            IsValid = allOK;
            return allOK;
        }
    }
}
