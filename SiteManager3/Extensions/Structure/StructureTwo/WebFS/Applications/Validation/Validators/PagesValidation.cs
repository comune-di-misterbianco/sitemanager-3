﻿using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Users;

namespace NetCms.Structure.Applications.Validation
{
    public class PagesValidation : ApplicationValidation
    {
        public PagesValidation(Application Application) : base(Application) { }

        protected const string SelectPageSql = @"SELECT * FROM applicazioni_pagine WHERE Applicazione_PaginaApplicazione = {0}";

        public DataTable Pages
        {
            get
            {
                if (_Pages == null)
                {
                    _Pages = Connection.SqlQuery(String.Format(SelectPageSql, this.Application.ID));
                }
                return _Pages;
            }
        }
        private DataTable _Pages;

        public override bool Validate()
        {
            if (this.Application.Installer.Pages != null)
            {
                bool isvalid = true;
                for (int i = 0; i < this.Application.Installer.Pages.Length / 2; i++)
                {
                    ApplicationValidatorResult validation = CheckPage(this.Application.Installer.Pages[i, 0], this.Application.Installer.Pages[i, 1]);
                    this.ValidationResults.Add(validation);
                    isvalid = isvalid & validation.IsOk == ApplicationValidatorResult.ResultsTypes.Ok;
                }
                return isvalid;
            }
            else return true;
        }
        private ApplicationValidatorResult CheckPage(string pageName, string codeClass)
        {
            Connection.ClearCache();
            this._Pages = null;
            if (this.Pages.Select("Pagename_PaginaApplicazione = '" + pageName + "' AND ClassName_PaginaApplicazione = '" + codeClass + "'").Length == 0)
                return new ApplicationValidatorResult(pageName, false, "Entry non presente o errata nella tabella delle pagine dell'applicazione", ApplicationValidator.RepairTypes.Pages);
            else return new ApplicationValidatorResult(pageName, true, "", ApplicationValidator.RepairTypes.Pages);
        }
    }
}
