﻿using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Users;

namespace NetCms.Structure.Applications.Validation
{
    public class DatabaseValidation : ApplicationValidation
    {
        public DatabaseValidation(Application Application) : base(Application) { }

        public override bool Validate()
        {
            DBComparer.Database database = new DBComparer.MySql.MySqlDatabase(this.Application.Conn.DatabaseName, this.Application.Conn);

            bool overallValidity = true;

            foreach (var absoluteTable in this.Application.AssociatedDatabaseTables)
            {
                if (database.Tables.ContainsKey(absoluteTable))
                {
                    var xmlNode = this.Application.DatabaseDataXmlDocument.SelectSingleNode("/DatabaseMap/table[@TableName=\"" + absoluteTable + "\"]");

                    DBComparer.Table targetTable = database.Tables[absoluteTable];

                    bool isvalid = false;
                    string message = "";

                    if (targetTable != null)
                    {
                        DBComparer.Table sourceTable = new DBComparer.XML.XMLTable(absoluteTable, xmlNode, absoluteTable);
                        string script = "";
                        isvalid = targetTable.Compare(sourceTable, out script);
                        if (!isvalid)
                            message = "Incongruenza tra la tabella '" + absoluteTable + "' del database e la definizione nella mappa dell'applicazione.";
                    }
                    else
                        message = "La tabella '" + absoluteTable + "' non esiste sul database.";

                    string name = "Tabella del database '" + absoluteTable + "'";
                    ApplicationValidator.RepairTypes type = ApplicationValidator.RepairTypes.DatabaseMap;
                    this.ValidationResults.Add(new ApplicationValidatorResult(name, isvalid, message, type));

                    overallValidity = overallValidity && isvalid;
                }
                else
                {
                    overallValidity = false;
                    string message = "La tabella '" + absoluteTable + "' non esiste sul database.";
                    string name = "Tabella del database '" + absoluteTable + "'";
                    ApplicationValidator.RepairTypes type = ApplicationValidator.RepairTypes.DatabaseMap;
                    this.ValidationResults.Add(new ApplicationValidatorResult(name, overallValidity, message, type));
                }

            }

            return overallValidity;

        }

        //public bool ValidateNetwork(NetCms.Networks.BasicNetwork network)
        //{
        //    DBComparer.Database database = new DBComparer.MySql.MySqlDatabase(this.Application.Connection.DatabaseName, this.Application.Connection);

        //    bool overallValidity = true;
        //    foreach (var absoluteTable in this.Application.AssociatedDatabaseTables)
        //    {
        //        string networkTable = network.SystemName + "_" + absoluteTable;
        //        if (database.Tables.ContainsKey(networkTable))
        //        {
        //            var xmlNode = this.Application.DatabaseDataXmlDocument.SelectSingleNode("/DatabaseMap/table[@TableName=\"" + absoluteTable + "\"]");

        //            DBComparer.Table targetTable = database.Tables[networkTable];

        //            bool isvalid = false;
        //            string message = "";

        //            if (targetTable != null)
        //            {
        //                DBComparer.Table sourceTable = new DBComparer.XML.XMLTable(absoluteTable, xmlNode, network.SystemName);
        //                string script = "";
        //                isvalid = targetTable.Compare(sourceTable, out script);
        //                if (!isvalid)
        //                    message = "Incongruenza tra la tabella '" + networkTable + "' del database e la definizione nella mappa dell'applicazione.";
        //            }
        //            else
        //                message = "La tabella '" + networkTable + "' non esiste sul database.";

        //            string name = "Tabella del database '" + networkTable + "'";
        //            ApplicationValidator.RepairTypes type = ApplicationValidator.RepairTypes.DatabaseMap;
        //            this.ValidationResults.Add(new ApplicationValidatorResult(name, isvalid, message, type));

        //            overallValidity = overallValidity && isvalid;
        //        }
        //    }

        //    return overallValidity;
        //}
    }
}
