﻿using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Users;

namespace NetCms.Structure.Applications.Validation
{
    public class TemplateValidation : ApplicationValidation
    {
        public TemplateValidation(Application Application) : base(Application) { }

        public override bool Validate()
        {
            string name = "";
            string message = "";
            bool ok = true;
            ApplicationValidator.RepairTypes type = ApplicationValidator.RepairTypes.Criteria;


            this.ValidationResults.Add(new ApplicationValidatorResult(name, ok,message,  type));
            return ok;
        }
    }
}
