﻿using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Users;

namespace NetCms.Structure.Applications.Validation
{
    public class PoolValidation : ApplicationValidation
    {
        public PoolValidation(Application application) : base(application) { }

        public override bool Validate()
        {
            bool ok = NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies.Contains(this.Application.ID.ToString());
            this.ValidationResults.Add( new ApplicationValidatorResult(this.Application.Label, ok,
                                                          "L'applicazione non è presente nel pool delle applicazioni attive e funzionanti.", 
                                                          ApplicationValidator.RepairTypes.Pool));
            return ok;
        }
    }
}
