﻿using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Users;

namespace NetCms.Structure.Applications.Validation
{
    public class CriteriaValidation : ApplicationValidation
    {
        protected const string SelectCriteriaSql = @"SELECT * FROM criteri WHERE Nome_Criterio = '{0}' 
                                                    AND Key_Criterio = '{1}'
                                                    AND Tipo_Criterio = {2}
                                                    AND ForDoc_Criterio = {3}";
        public CriteriaValidation(Application Application) : base(Application) { }

        public override bool Validate()
        {
            bool ok = Application.Conn.SqlQuery(String.Format(SelectCriteriaSql, "Creazione Cartella " + this.Application.Label, "newfolder_" + this.Application.SystemName, this.Application.ID, 0)).Rows.Count > 0;
            this.ValidationResults.Add(new ApplicationValidatorResult("Creazione Cartella " + this.Application.Label, ok, "Entry non presente o errata nella tabella delle pagine dell'applicazione", ApplicationValidator.RepairTypes.Criteria));
            return ok;
        }
    }
}
