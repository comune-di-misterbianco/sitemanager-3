﻿using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Users;

namespace NetCms.Structure.Applications.Validation
{
    public class ApplicationRecordValidation : ApplicationValidation
    {
//        protected const string SelectApplicationSql = @"SELECT * FROM applicazioni WHERE id_applicazione = {0}
//                                                    AND SystemName_Applicazione = '{1}'
//                                                    AND LabelPlurale_Applicazione = '{2}'
//                                                    AND LabelSingolare_Applicazione = '{3}'
//                                                    AND EnableReviews_Applicazione = {4}
//                                                    AND Enabled_Applicazione = {5}
//                                                    AND EnableMove_Applicazione = {6}
//                                                    AND EnableNewsLetter_Applicazione = {7}
//                                                    AND EnableModule_Applicazione = {8}";

        protected const string SelectApplicationSql = @"SELECT * FROM applicazioni WHERE id_applicazione = {0}
                                                    AND SystemName_Applicazione = '{1}'
                                                    AND LabelPlurale_Applicazione = '{2}'
                                                    AND Enabled_Applicazione = '{3}'";

        public ApplicationRecordValidation(Application Application) : base(Application) { }

        public override bool Validate()
        {
            //DataTable application = Application.Connection.SqlQuery(String.Format(SelectApplicationSql,
            //                                                                       this.Application.ID,
            //                                                                       this.Application.SystemName,
            //                                                                       this.Application.Label,
            //                                                                       this.Application.LabelSingolare,
            //                                                                       this.Application.EnableReviews,
            //                                                                       1,
            //                                                                       this.Application.EnableMove,
            //                                                                       this.Application.EnableNewsLetter,
            //                                                                       this.Application.EnableModule));

            DataTable application = Application.Conn.SqlQuery(String.Format(SelectApplicationSql,
                                                                                   this.Application.ID,
                                                                                   this.Application.SystemName,
                                                                                   this.Application.Label,
                                                                                   1));

            bool ok = application.Rows.Count == 1;
            this.ValidationResults.Add(new ApplicationValidatorResult("Record nella tabella delle applicazioni", ok, "Entry non presente o errata nella tabella Applicazioni", ApplicationValidator.RepairTypes.Application));
            return ok;
            
            //Application dbApp = ApplicationBusinessLogic.GetApplicationForValidation(this.Application.ID, this.Application.SystemName, this.Application.Label, true);
            //bool ok = dbApp != null;
            //this.ValidationResults.Add(new ApplicationValidatorResult("Record nella tabella delle applicazioni", ok, "Entry non presente o errata nella tabella Applicazioni", ApplicationValidator.RepairTypes.Application));
            //return ok;
        }
    }
}
