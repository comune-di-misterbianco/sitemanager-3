﻿//using System;
//using System.Data;
//using System.Reflection;
//using System.Collections;
//using System.Collections.Generic;
//using System.Xml;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.HtmlControls;
//using NetCms.Structure.Grants;
//using NetCms.Users;

//namespace NetCms.Structure.Applications.Validation
//{
//    public class VersionValidation : ApplicationValidation
//    {
//        public VersionValidation(Application Application) : base(Application) { }

//        public override bool Validate()
//        {
//            string name = "Versione dell'Applicazione";
//            string message = "Si è verificato un problema con la validazione della mappa del database. I possibili problemi potrebbero essere: File xml della mappa non esistente, xml della mappa non correttamente formattato, versione dell'xml della mappa non corrispondente alla versione dell'applicazione installata.";
//            bool ok = CheckHashValidity();
//            ApplicationValidator.RepairTypes type = ApplicationValidator.RepairTypes.DatabaseMap;


//            this.ValidationResults.Add(new ApplicationValidatorResult(name, ok,message,  type));
//            return ok;
//        }

//        public bool CheckHashValidity()
//        {
//            bool result = false;
//            try
//            {
//                XmlDocument doc = new XmlDocument();
//                doc.LoadXml(this.Application.DatabaseDataXmlDocument.OuterXml);

//                XmlNode appNode = Setup.ConfigurationXmlDocument.SelectSingleNode("/applications/" + this.Application.SystemName);
//                XmlNode mapNode = this.Application.DatabaseDataXmlDocument.SelectSingleNode("/DatabaseMap");
//                XmlNode mapNodeClone = doc.SelectSingleNode("/DatabaseMap");

//                string hashApp = appNode.Attributes["databaseDataHash"].Value;
//                string hashMap = mapNode.Attributes["hash"].Value;

//                //mapNodeClone.Attributes.Remove(mapNodeClone.Attributes["hash"]);
//                mapNodeClone.Attributes["hash"].Value = "";
//                string hashMapCalculated = GetMD5Hash(doc.InnerXml);
//                result = (hashApp == hashMap) && (hashApp == hashMapCalculated);
//            }
//            catch { }
//            return result;
//        }

//        public string GetMD5Hash(string input)
//        {
//            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
//            byte[] bs = System.Text.Encoding.UTF8.GetBytes(input);
//            bs = x.ComputeHash(bs);
//            System.Text.StringBuilder s = new System.Text.StringBuilder();
//            foreach (byte b in bs)
//            {
//                s.Append(b.ToString("x2").ToLower());
//            }
//            string password = s.ToString();
//            return password;
//        }
//    }
//}
