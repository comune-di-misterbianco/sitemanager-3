﻿using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Users;

namespace NetCms.Structure.Applications.Validation
{
    public class ApplicationValidatorResult
    {
        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private ResultsTypes _IsOk;
        public ResultsTypes IsOk
        {
            get { return _IsOk; }
            set { _IsOk = value; }
        }

        public enum ResultsTypes
        {
            Ok, Error, Warning
        }

        public string RepairTypeLabel
        {
            get
            {
                switch (this.RepairType)
                {
                    case NetCms.Structure.Applications.Validation.ApplicationValidator.RepairTypes.Criteria: return "Criterio Permessi";
                    case NetCms.Structure.Applications.Validation.ApplicationValidator.RepairTypes.Application: return "Record dell'Applicazione";
                    case NetCms.Structure.Applications.Validation.ApplicationValidator.RepairTypes.Pages: return "Pagina dell'Applicazione";
                    case NetCms.Structure.Applications.Validation.ApplicationValidator.RepairTypes.DatabaseMap: return "Coerenza Database";
                    case NetCms.Structure.Applications.Validation.ApplicationValidator.RepairTypes.Custom: return "Altro";
                    case NetCms.Structure.Applications.Validation.ApplicationValidator.RepairTypes.Pool: return "Persistenza nel Pool";
                    default: return "";
                }
            }
        }

        private string _Message;
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public ApplicationValidator.RepairTypes RepairType
        {
            get
            {
                return _RepairType;
            }
            private set
            {
                _RepairType = value;
            }
        }
        private ApplicationValidator.RepairTypes _RepairType;

        public ApplicationValidatorResult(string name, bool isOk, string message)
            : this(name, isOk, message, ApplicationValidator.RepairTypes.Custom)
        {
        }

        public ApplicationValidatorResult(string name, bool isOk, string message, ApplicationValidator.RepairTypes repairType)
        {
            this.Name = name;
            this.IsOk = isOk ? ResultsTypes.Ok : ResultsTypes.Error;
            this.Message = message;
            RepairType = repairType;
        }

        public ApplicationValidatorResult(string name, ResultsTypes isOk, string message)
            : this(name, isOk, message, ApplicationValidator.RepairTypes.Custom)
        {
        }

        public ApplicationValidatorResult(string name, ResultsTypes isOk, string message, ApplicationValidator.RepairTypes repairType)
        {
            this.Name = name;
            this.IsOk = isOk;
            this.Message = message;
            RepairType = repairType;
        }
    }
}
