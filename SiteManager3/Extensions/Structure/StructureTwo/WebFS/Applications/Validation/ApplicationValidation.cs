﻿using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Users;

namespace NetCms.Structure.Applications.Validation
{
    public abstract class ApplicationValidation
    {
        public List<ApplicationValidatorResult> ValidationResults
        {
            get
            {
                if (_ValidationResults == null)
                {
                    _ValidationResults = new List<ApplicationValidatorResult>();
                }
                return _ValidationResults;
            }
        }
        private List<ApplicationValidatorResult> _ValidationResults;

        public Application Application
        {
            get
            {
                return _Application;
            }
            private set
            {
                _Application = value;
            }
        }
        private Application _Application;

        public NetCms.Connections.Connection Connection
        {
            get
            {
                return _Connection;
            }
            private set
            {
                _Connection = value;
            }
        }
        private NetCms.Connections.Connection _Connection;

        public ApplicationValidation(Application application)
        {
            this.Application = application;
            this.Connection = application.Conn;
        }

        public abstract bool Validate();
    }
}
