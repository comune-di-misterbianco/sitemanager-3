﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NetCms.DBSessionProvider;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;

namespace NetCms.Structure.Applications
{
    public static class ApplicationBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static Application GetApplicationById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Application> dao = new CriteriaNhibernateDAO<Application>(innerSession);
            return dao.GetById(id);
        }

        public static Application GetApplicationBySystemName(string systemName, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Application> dao = new CriteriaNhibernateDAO<Application>(innerSession);
            SearchParameter sysNameSP = new SearchParameter(null, "SystemName", systemName, Finder.ComparisonCriteria.Equals, false);
            return dao.GetByCriteria(new SearchParameter[]{sysNameSP});
        }

        public static Application GetApplicationForValidation(int id, string systemName, string label, bool enabled, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Application> dao = new CriteriaNhibernateDAO<Application>(innerSession);
            SearchParameter idSP = new SearchParameter(null, "ID", id, Finder.ComparisonCriteria.Equals, false);
            SearchParameter sysNameSP = new SearchParameter(null, "SystemName", systemName, Finder.ComparisonCriteria.Equals, false);
            SearchParameter labelSP = new SearchParameter(null, "Label", label, Finder.ComparisonCriteria.Equals, false);
            SearchParameter enabledSP = new SearchParameter(null, "Enabled", enabled, Finder.ComparisonCriteria.Equals, false);
            return dao.GetByCriteria(new SearchParameter[] { idSP, sysNameSP, labelSP, enabledSP });
        }

        public static Application SaveOrUpdateApplication(Application application, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Application> dao = new CriteriaNhibernateDAO<Application>(innerSession);
            return dao.SaveOrUpdate(application);
        }

        public static ApplicationPage SaveOrUpdateApplicationPage(ApplicationPage applicationPage)
        {
            IGenericDAO<ApplicationPage> dao = new CriteriaNhibernateDAO<ApplicationPage>(GetCurrentSession());
            return dao.SaveOrUpdate(applicationPage);
        }

        public static void DeleteApplication(Application application, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Application> dao = new CriteriaNhibernateDAO<Application>(innerSession);
            dao.Delete(application);
        }
    }
}
