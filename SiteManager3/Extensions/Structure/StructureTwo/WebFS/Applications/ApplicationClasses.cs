﻿using System;
using System.Data;
using System.Reflection;
using System.Text;

namespace NetCms.Structure.Applications
{
    public class ApplicationClasses
    {
        public const string BaseFolderClassName = "NetCms.Structure.WebFS.StructureFolder";
        public const string BaseOverviewClassName = "NetCms.Structure.WebFS.DocumentOverview";
        public const string BaseReviewClassName = "NetCms.Structure.WebFS.DocumentReviewPage";
        public const string BaseDocumentClassName = "NetCms.Structure.WebFS.Document";
        public const string BaseDocContentClassName = "NetCms.Structure.WebFS.DocContent";
        //public const string BaseHomepageModuleClassName = "NetCms.Structure.Applications.Homepage.HomepageModule";
        //public const string BaseHomepageFrontendApplicationModuleClassName = "NetFrontend.Application+GenericApplicationModulo";
        //public const string BaseHomepageFrontendModuleClassName = "NetFrontend.Homepage+LayoutModuloBlock";
        //public const string BaseHomepageModuleFormClassName = "NetCms.Structure.Applications.Homepage.HomepageModuleForm";
        public const string BaseDetailsClassName = "NetCms.Structure.WebFS.DocumentDetailsPage";
        public const string BaseExporterClassName = "NetCms.Exporter.ApplicationExporter";
        public const string BaseFrontContentHandlerClassName = "NetFrontend.ContentsHandler";
        public const string BaseFrontApplicationContentsHandlerClassName = "NetFrontend.ApplicationContentsHandler";
        //public const string BaseNewsletterArticleClassName = "NetCms.Newsletter2.Admin.Articolo";
        //public const string BaseNewsletterApplicationsArticleClassName = "NetCms.Newsletter2.Admin.ApplicationArticoloNewsLetter";

        private string _Folder;
        public string Folder 
        { get { return _Folder; } }

        private string _Overview;
        public string Overview 
        { get { return _Overview; } }

        private string _Review;
        public string Review 
        { get { return _Review; } }

        private string _Exporter;
        public string Exporter
        { get { return _Exporter; } }

        private string _ContentHandler;
        public string ContentHandler
        { get { return _ContentHandler; } }

        private string _Document;
        public string Document 
        { get { return _Document; } }

        private string _DocContent;
        public string DocContent
        { get { return _DocContent; } }

        //private string _HomepageModule;
        //public string HomepageModule 
        //{ get { return _HomepageModule; } }

        //private string _HomepageFrontendModule;
        //public string HomepageFrontendModule
        //{ get { return _HomepageFrontendModule; } }

        //private string _HomepageModuleForm;
        //public string HomepageModuleForm
        //{ get { return _HomepageModuleForm; } }

        //private string _NewsletterArticle;
        //public string NewsletterArticle
        //{ get { return _NewsletterArticle; } }

        private string _Details;
        public string Details { get { return _Details; } }
         
        private Application Application
        {
            get { return _Application; }
            set { _Application = value; }
        }
        private Application _Application;

        public ApplicationClasses(Application application)
        {
            this.Application = application;
            FindClasses();
        }

        public void FindClasses()
        {
            Type[] types = Application.Assembly.GetTypes();
 
            foreach (Type type in types)
            {
                switch (type.BaseType.FullName)
                {
                    case BaseFolderClassName: this._Folder = type.ToString(); break;
                    case BaseOverviewClassName: this._Overview = type.ToString(); break;
                    case BaseReviewClassName: this._Review = type.ToString(); break;
                    case BaseDocumentClassName: this._Document = type.ToString(); break;
                    case BaseDetailsClassName: this._Details = type.ToString(); break;
                    case BaseDocContentClassName: this._DocContent = type.ToString(); break;
                    //case BaseHomepageModuleClassName: this._HomepageModule = type.ToString(); break;
                    //case BaseHomepageFrontendModuleClassName: this._HomepageFrontendModule = type.ToString(); break;
                    //case BaseHomepageFrontendApplicationModuleClassName: this._HomepageFrontendModule = type.ToString(); break;
                    //case BaseHomepageModuleFormClassName: this._HomepageModuleForm = type.ToString(); break;
                    case BaseExporterClassName: this._Exporter = type.ToString(); break;
                    case BaseFrontContentHandlerClassName: this._ContentHandler = type.ToString(); break;
                    case BaseFrontApplicationContentsHandlerClassName: this._ContentHandler = type.ToString(); break;
                    //case BaseNewsletterArticleClassName: this._NewsletterArticle = type.ToString(); break;
                    //case BaseNewsletterApplicationsArticleClassName: this._NewsletterArticle = type.ToString(); break;
                }
            }
        }
    }

}