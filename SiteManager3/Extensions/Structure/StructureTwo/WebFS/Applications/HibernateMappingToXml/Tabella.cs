﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace HibernateMappingToXml
{
    public class Tabella
    {
        public string Nome { get; set; }
        public List<Colonna> Campi { get; set; }
        public List<Colonna> ForeignKeys { get; set; }
        private string id;
        public string Id
        {
            get
            {
                return id;
            }
            set
            {
                //Se l'id è già settato cancella la colonna id dalla tabella
                //if (this.id != null)
                //{
                //    foreach (Colonna c in Campi)
                //    {
                //        if (c.nome.CompareTo(this.id) == 0)
                //        {
                //            Campi.Remove(c);
                //            break;
                //        }
                //    }
                //}
                id = value;
                //Colonna idColumn = new Colonna(value, Mapper.KEY_TYPE, true, true, false);
                //Campi.Add(idColumn);
            }
        }



        public Tabella(string nome)
        {
            this.Nome = nome;
            Campi = new List<Colonna>();
            ForeignKeys = new List<Colonna>();
        }

        public void AddColumn(Colonna column)
        {
            if (column.esterna)
                ForeignKeys.Add(column);
            else
                Campi.Add(column);
        }

        public XElement ToXML(Dictionary<string,Relazione> relazioni)
        {
            //Scrittura della tabella principale
            XElement entry = new XElement("table", new XAttribute("TableName", Nome));
            XElement constraints = new XElement("constraints");
            foreach (var rel in relazioni.ToList())
            {
                string stringaConstraint = "CONSTRAINT `" + rel.Value.tabPart + "` FOREIGN KEY (`" + rel.Value.colPart + "`) REFERENCES `" + rel.Value.tabDest + "` (`" + rel.Value.colDest + "`) ON DELETE CASCADE ON UPDATE CASCADE";
                //Prendo tutte i nomi delle chiavi esterne di questa tabella
                List<string> chiavi = ForeignKeys.Select(x => x.nome).ToList();
                //aggiungo all'xml solo le relazioni (costraints) che sono foreign key in questa tabella
                if (chiavi.IndexOf(rel.Key) != -1)
                    constraints.Add(new XElement("constraint", stringaConstraint));
            }
            entry.Add(constraints);
            XElement fields = new XElement("fields");

            foreach (Colonna c in Campi)
            {
                fields.Add(c.toXElement("field"));
            }
            foreach (Colonna c in ForeignKeys)
            {
                fields.Add(c.toXElement("field"));
            }
            entry.Add(fields);
            return entry;
        }
    }

    
}
