﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace HibernateMappingToXml
{
    class XmlToCustom
    {
        private string applName;
        private string fileName;
        private string[] fileNameArray;

        private const string KEY_TYPE = "int(11)";

        public XmlToCustom(string defFileName, string nomeAppl, string[] extraFileNames)
        {
            applName = nomeAppl;
            fileName = defFileName;
            fileNameArray = extraFileNames; 
        }

        public string Customize()
        {

            List<Tabella> tabelle = new List<Tabella>();
            Dictionary<string,Relazione> relazioni = new Dictionary<string,Relazione>();

            XDocument doc = XDocument.Load(fileName);
            XNamespace ns = Mapper.NAMESPACE;
            

            tabelle = GetFromClass(doc, ns, applName, relazioni, true);
            if (fileNameArray.Length > 0)
            {
                foreach (string nomeFile in fileNameArray)
                {

                    doc = XDocument.Load(nomeFile);
                    tabelle.AddRange(GetFromClass(doc, ns, applName, relazioni, false));
                }
            }

            XElement risultato = new XElement("DatabaseMap");
            foreach(Tabella tab in tabelle)
                risultato.Add(tab.ToXML(relazioni));
            return risultato.ToString();
            
            

        }

        //Questo metodo si occupa di creare una lista di tabelle a partire dall'XDocument passato
        //Se viene settato isFirst a TRUE il metodo entra direttamente all'interno dei join-sublass
        //E NON crea una tabella per la classe, che vuol dire sarà astratta
        //Viene richiesto di passare anche un  namespace se l'elemento root ne ha uno
        //Viene costruita una tabella per ogni Joined-Subclass che ha il nome dell'applicazione passata (se non viene passato le legge tutte)
        //Le relazioni incontrate durante la scansione delle tabelle vengono inserire nel dizionario relazioni
        public List<Tabella> GetFromClass(XDocument root,XNamespace ns, string applName, Dictionary<string, Relazione> relazioni, bool isFirst)
        {
            List<Tabella> tabelle = new List<Tabella>();
            Tabella tabella;

            XElement classe = root.Descendants(ns + "class").First();

            //Verifico se devo saltare la creazione della tabella per la classe astratta
            if (!isFirst)
            {
                tabella = new Tabella(classe.Attribute("table").Value);

                //prendo nella sottochiave id->Column-> attributo: name il nome della chiave primaria
                XElement chiaveExtra = classe.Element(ns + "id");
                XElement idExtra = chiaveExtra.Element(ns + "column");
                tabella.Id=idExtra.Attribute("name").Value;

                XElement autoIncrementExtra = chiaveExtra.Element(ns + "generator");
                bool isAutoIncrement = false;
                if (autoIncrementExtra != null)
                {
                    string generatorValue = autoIncrementExtra.Attribute("class").Value;
                    if (generatorValue.Contains("identity"))
                        isAutoIncrement = true;
                }

                tabella.Campi.Add(new Colonna(idExtra.Attribute("name").Value, Mapper.KEY_TYPE, true, true, false, isAutoIncrement));
                //prendo tutte le proprietà all'interno della class
                GetProperty(classe, ns, tabella);

                GetRelationship(classe, ns, tabella, relazioni);

                GetForeignKeys(classe, ns, tabella, relazioni);
                
                tabelle.Add(tabella);
            }

            //Verifico e rilevo le subclass all'interno del documento
            List<Tabella> sottotabelle = GetFromSubclass(classe, ns, applName, relazioni);

            tabelle.AddRange(sottotabelle);

            return tabelle;

        }


        //Questo metodo si occupa di creare una lista di tabelle a partire dall'XElement passato
        //Viene costruita una tabella per ogni Joined-Subclass che ha il nome dell'applicazione passata
        //Viene richiesto di passare anche un  namespace se l'elemento root ne ha uno
        //Le relazioni incontrate durante la scansione delle tabelle vengono inserire nel dizionario relazioni
        public List<Tabella> GetFromSubclass(XElement root, XNamespace ns, string applName, Dictionary<string,Relazione> relazioni)
        {
            List<Tabella> tabelle = new List<Tabella>();
            Tabella tabella;

            //prendo l'elemento joined-subclass relativo all'applicazione di interesse
            IEnumerable<XElement> join = root.Descendants(ns + "joined-subclass").Where(x => x.Attribute("name").ToString().ToLower().Contains(", " + applName + ", "));

            //Per ognuno delle sottoclassi trovate
            foreach (XElement j in join)
            {

                tabella = new Tabella(j.Attribute("table").Value);

                //prendo nella sottochiave Key->Column-> attributo: name il nome della chiave primaria
                XElement chiave = j.Element(ns + "key");
                XElement id = chiave.Element(ns + "column");
                tabella.Id=id.Attribute("name").Value;

                XElement autoIncrementExtra = chiave.Element(ns + "generator");
                bool isAutoIncrement = false;
                if (autoIncrementExtra != null)
                {
                    string generatorValue = autoIncrementExtra.Attribute("class").Value;
                    if (generatorValue.Contains("identity"))
                        isAutoIncrement = true;
                }

                tabella.Campi.Add(new Colonna(id.Attribute("name").Value, Mapper.KEY_TYPE, true, true, false, isAutoIncrement));
                

                //prendo tutte le property e le inserisco come colonne nella tabella
                GetProperty(j, ns, tabella);

                GetRelationship(j, ns, tabella, relazioni);

                GetForeignKeys(j, ns, tabella, relazioni);

                List<Tabella> sottotabelle = GetFromSubclass(j, ns, applName, relazioni);

                tabelle.AddRange(sottotabelle);

                tabelle.Add(tabella);
            }

            return tabelle;
                
        }


        //Legge tutti i campi Property all'interno dell XElement passato
        //inserisce nell'oggetto Tabella passato tutti i campi
        //Richiede di passare il namespace in cui ricercare
        public void GetProperty(XElement root, XNamespace ns, Tabella tabella)
        {
            //cerco tutte le proprietà a meno delle subclass
            IEnumerable<XElement> proprieta = root.Descendants(ns + "property").Where(x => !root.Descendants(ns + "joined-subclass").Contains(x.Parent));
            foreach (XElement prop in proprieta)
            {
                //Prendo il nome della colonna
                XElement colonna = prop.Element(ns + "column");

                //Creo la colonna senza particolari specifiche (not null: False, primaria: False, Esterna: false)
                Colonna campo = new Colonna(colonna.Attribute("name").Value, "", false, false, false);

                //Verifico se la colonna deve essere non nulla
                if(campoNotNull(colonna))
                    campo.notNull=true;

                //Verifico se ci sono valori di default
                if (colonna.Attribute("default") != null)
                    campo.def_value = colonna.Attribute("default").Value;


                //Il tipo della colonna viene scelto dall'attribto sql-type se presente, altrimenti dalla proprietà padre
                if (colonna.Attribute("sql-type") != null)
                {
                    campo.tipo = Mapper.MapType(colonna.Attribute("sql-type").Value);
                }
                else
                {
                    campo.tipo = Mapper.MapType(prop.Attribute("type").Value.Split(new char[] { ',' }).ElementAt(0));
                }
                Console.WriteLine(colonna.Name);
                tabella.AddColumn(campo);
            }
        }

        //Legge tutti i campi set per creare le relazioni
        //Parte dall'elemento XElement
        //Utilizza il namespace passato
        //La relazione punta alla chiave primaria della tabella passata(la foreign-key conterrà l'id della tabella passata)
        //Inserisce la relazione all'interno dell'oggetto relazioni passato
        public void GetRelationship(XElement root, XNamespace ns, Tabella tabella, Dictionary<string,Relazione> relazioni)
        {
            //Creo le relazioni da completare (cerco i set) tranne che nelle subclass
            IEnumerable<XElement> sets = root.Descendants(ns + "set").Where(x => !root.Descendants(ns + "joined-subclass").Contains(x.Parent));
            foreach (XElement set in sets)
            {
                XElement key = set.Element(ns + "key");
                XElement colonna = key.Element(ns + "column");

                //La tabella di destinazione è quella che contiene l'id a cui la foreign key fa riferimento
                //La tabella di partenza è quella che contiene il campo foreign-key

                //Se la relazione è in sospeso completo i campi mancanti
                if (relazioni.ContainsKey(colonna.Attribute("name").Value))
                {
                    relazioni[colonna.Attribute("name").Value].colPart = colonna.Attribute("name").Value;
                    relazioni[colonna.Attribute("name").Value].tabDest = tabella.Nome;
                    relazioni[colonna.Attribute("name").Value].colDest = tabella.Id;
                }
                else
                {
                    //Se non è mai stato incontrata una foreign key relativa a questa relazione
                    //creo una nuova relazione
                    Relazione relazione = new Relazione();
                    relazione.colPart = colonna.Attribute("name").Value;
                    relazione.tabDest = tabella.Nome;
                    relazione.colDest = tabella.Id;
                    relazioni.Add(colonna.Attribute("name").Value, relazione);
                }
                
            }
        }

        //Legge tutti i campi many-to-one per creare le relazioni
        //Parte dall'elemento XElement
        //Utilizza il namespace passato
        //Utilizza la tabella passata per conoscere il nome della tabella che conterrà la foreign key
        //Inserisce le relazioni all'interno dell'oggetto relazioni che è stato passato
        public void GetForeignKeys(XElement root, XNamespace ns, Tabella tabella, Dictionary<string, Relazione> relazioni)
        {
            //prendo tutte le colonne che sono foreign-key a meno di quelle contenute in altre subclass
            IEnumerable<XElement> many2One = root.Descendants(ns + "many-to-one").Where(x => !root.Descendants(ns + "joined-subclass").Contains(x.Parent));
            foreach (XElement prop in many2One)
            {
                XElement colonnaExtra = prop.Element(ns + "column");

                //Creo una colonna che è di tipo chiave primaria, e che sia chiave esterna non nulla
                Colonna campo = new Colonna(colonnaExtra.Attribute("name").Value, Mapper.KEY_TYPE, false, false, true);

                //Se la relazione prevede che la foreign-key possa essere nulla modifico l'attributo relativo
                if(campoNotNull(colonnaExtra))
                    campo.notNull = true;

                
                //Se la relazione è in sospeso completo i campi mancanti
                if(relazioni.ContainsKey(colonnaExtra.Attribute("name").Value))
                    relazioni[colonnaExtra.Attribute("name").Value].tabPart = tabella.Nome;

                //se la relazione non è ancora stata creata ne inizializzo una nuova
                else
                {

                    Relazione relazione = new Relazione();
                    relazione.tabPart = tabella.Nome;
                    relazione.colPart = colonnaExtra.Attribute("name").Value;

                    relazioni.Add(colonnaExtra.Attribute("name").Value,relazione);
                }
                
                tabella.AddColumn(campo);
            }
        }


        //Legge un elemento e controlla se è prensente un attirbuto NOT NULL
        //restituisce se l'attributo può essere nullo
        public bool campoNotNull(XElement campo)
        {
            if (campo.Attribute("not-null") != null)
                if (campo.Attribute("not-null").Value.CompareTo("false") == 0)
                    return false;
                else
                    return true;
            else
                return false ;
        }
    }




}
