﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace HibernateMappingToXml
{
    public class Colonna
    {
       
        public string nome{get; set;}
        public string tipo { get; set; }
        public bool notNull { get; set; }
        public bool primaria{get; set;}
        public bool esterna { get; set; }
        public string def_value { get; set; }
        public bool isAutoIncrement { get; set; }

        public Colonna(string nome, string tipo, bool notNull, bool isprimaria, bool isesterna, bool _isAutoIncrement = false)
        {
            this.nome = nome;
            this.tipo = tipo;
            this.notNull = notNull;
            this.primaria = isprimaria;
            this.esterna = isesterna;
            this.isAutoIncrement = _isAutoIncrement;
        }

        public Colonna(string nome, string tipo, bool notNull, bool isprimaria, bool isesterna, string default_value,bool _isAutoIncrement = false)
            : this(nome, tipo, notNull, isprimaria, isesterna)
        {
            this.def_value = default_value;
            this.isAutoIncrement = _isAutoIncrement;
        }

        public XElement toXElement(string chiave)
        {
            XElement field = new XElement(chiave);

            if (primaria)
                field.Add(new XAttribute("PrimaryKey", "True"));
            else
                field.Add(new XAttribute("PrimaryKey", "False"));

            if (esterna)
                field.Add(new XAttribute("ExternalKey", "True"));
            else
                field.Add(new XAttribute("ExternalKey", "False"));

            field.Add(new XAttribute("Name", nome));
            field.Add(new XAttribute("Type", tipo));

            if(def_value!=null)
                field.Add(new XAttribute("Default", " DEFAULT '"+def_value+"'"));
            else
            {
                if (primaria)
                {
                    if (isAutoIncrement)
                        field.Add(new XAttribute("Default", " AUTO_INCREMENT"));
                    else
                        field.Add(new XAttribute("Default", " DEFAULT NULL"));
                }
                else if (notNull)
                    field.Add(new XAttribute("Default", " DEFAULT ''"));
                else
                    field.Add(new XAttribute("Default", " DEFAULT NULL"));
            }
                

            if(notNull)
                field.Add(new XAttribute("NotNull", " NOT NULL"));
            else
                field.Add(new XAttribute("NotNull", ""));

            return field;

        }
    }
}
