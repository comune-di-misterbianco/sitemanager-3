﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HibernateMappingToXml
{
    public static class Mapper
    {
        public const string KEY_TYPE = "int(11)";
        public const string NAMESPACE = "urn:nhibernate-mapping-2.2";

        private static Dictionary<string, string> mapping;
        
        public static string MapType(string type)
        {
            mapping = new Dictionary<string,string>()
            {
                {"System.String","varchar(255)"},
                {"System.Int32","int(11)"},
                {"mediumtext","mediumtext"},
                {"longtext","longtext"},
                {"text","text"},
                {"varchar(100)","varchar(100)"},
                {"varchar(1000)","varchar(1000)"},
                {"System.DateTime","datetime"},//aggiunta 
                {"System.Boolean","tinyint(1)"},//aggiunta
                {"System.Decimal","decimal(12,2)"},//aggiunta
                {"NetCms.Structure.Applications.News.Mapping.NewsStatusMapping","varchar(255)"}
            
            };

            if (mapping.ContainsKey(type) || mapping.ContainsKey(type.ToLower()))
                return mapping[type];
            else
                throw new ArgumentException("Mapping parameter not found");
        }
    }
}
