﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HibernateMappingToXml
{
    public class Relazione
    {
        //Tabella che ha il campo per la foreign-key
        public string tabPart { get; set; }
        //Tabella che non ha collegamenti con l'altra tabella
        public string tabDest { get; set; }
        //Colonna che contiene la foreign-key
        public string colPart { get; set; }
        //Colonna che contiene l'ID che viene riferita dalla foreign-key 
        public string colDest { get; set; }
    }
}
