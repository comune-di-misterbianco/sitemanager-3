﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.CmsApp.ShortCodes
{
    public abstract class ShortCode
    {
        const string tplname = "shortcode.tpl";

        public EnumTemplateType Template { get; set; }
        public int RifObj { get; set; }

        public string AppRif { get; set; }

        public EnumObjectType ObjectType { get; set; }

        public int ItemLimit
        {
            get;
            set;
        }

        public string Raw
        {
            get;
            set;
        }


        public enum EnumObjectType
        {
            folder,
            doc
        }

        public enum EnumTemplateType
        {
            list,
            gallery,
        }
       
        public NetCms.Themes.Model.Theme CurrentTheme
        {
            get;
            set;
        }


        public ShortCode()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rawValue"></param>
        /// <param name="appRif"></param>
        /// <param name="rifObj"></param>
        /// <param name="objectType"></param>
        /// <param name="currentTheme"></param>
        /// <param name="itemLimit">Limitazione item da esporre: default -1 nessun limite</param>
        public ShortCode(string rawValue, string appRif, int rifObj, EnumObjectType objectType, NetCms.Themes.Model.Theme currentTheme, int itemLimit = -1)
        {
            Raw = rawValue;
            AppRif = appRif;
            RifObj = rifObj;
            ObjectType = objectType;
            CurrentTheme = currentTheme;
            ItemLimit = itemLimit;
        }

        public abstract string GetHtml();
        
        public virtual string GetTemplatePath()
        {
            string tpl_path = string.Empty;
            string baseTplPath = NetCms.Configurations.Paths.AbsoluteRoot + NetCms.Configurations.ThemeEngine.ThemesFolder;
            tpl_path = baseTplPath + "/" + CurrentTheme.Name.ToLower() + "/templates/content/" + AppRif.ToLower() + "/" + tplname;
            tpl_path = System.Web.HttpContext.Current.Server.MapPath(tpl_path);

            if (!System.IO.File.Exists(tpl_path))
            {
                //Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore: file template " + tpl_path + " mancante.");

                tpl_path = baseTplPath + "/" + CurrentTheme.ParentTheme.Name.ToLower() + "/templates/content/" + AppRif.ToLower() + "/" + tplname;
                tpl_path = System.Web.HttpContext.Current.Server.MapPath(tpl_path);

                if (!System.IO.File.Exists(tpl_path))
                {
                    Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore: file template " + tpl_path + " mancante nel tema padre.");
                }

            }
          
            return tpl_path;
        }

    }
}
