﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace NetCms.CmsApp.ShortCodes
{
    public static class ShortCodesPool
    {
        private const string AssembliesApplicationKey = "NetCms.CmsApp.ShortCodes.ShortCodesPool";

        private static Dictionary<string, string> _Assemblies;
        public static Dictionary<string, string> Assemblies
        {
            get
            {
                if (_Assemblies == null)
                {
                    if (HttpContext.Current.Application[AssembliesApplicationKey] != null)
                        _Assemblies = (Dictionary<string, string>)HttpContext.Current.Application[AssembliesApplicationKey];
                    else
                        HttpContext.Current.Application[AssembliesApplicationKey] = _Assemblies = new Dictionary<string, string>();

                }
                return _Assemblies;
            }
        }

        public static void ParseType(Type type, Assembly asm)
        {
            if (type.BaseType == Type.GetType("NetCms.CmsApp.ShortCodes.ShortCode"))
            {
                Assemblies.Add(type.FullName, asm.FullName);
            }
        }
    }
}
