﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Structure.WebFS.FileSystemModel.Mapping
{
    public class DocContentMapping : ClassMap<DocContent>
    {
        public DocContentMapping()
        {
            Table("docs_contents");
            Cache.NonStrictReadWrite();
            Id(x => x.ID).Column("id_DocContent");

            HasOne(x => x.DocLang)
                .PropertyRef(r => r.Content)
                .Cascade.All();

            HasOne(x => x.Revisione)
                .PropertyRef(r => r.Content)
                .Cascade.All();

            HasMany(x => x.Attaches)
              .KeyColumn("DocContent_id")
              .Fetch.Select()
              .AsSet()
              .KeyNullable()
              .Cascade.All();
        }
    }
}
