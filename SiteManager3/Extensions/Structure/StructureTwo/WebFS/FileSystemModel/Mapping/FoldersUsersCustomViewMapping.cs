﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Structure.WebFS.FileSystemModel.Mapping
{
    public class FoldersUsersCustomViewMapping : ClassMap<FoldersUsersCustomView>
    {
        public FoldersUsersCustomViewMapping()
        {
            Table("folders_users_custom_view");
            Id(x => x.ID).Column("id_FolderCustomView");
            Map(x => x.ViewOrder).Column("View_FolderCustomView").Not.Nullable();

            References(x => x.Folder)
                .ForeignKey("foldersuserscustomview_folder")
                 .Column("Folder_FolderCustomView")
                 .Not.Nullable()
                 .Fetch.Select();

            References(x => x.User)
                .ForeignKey("foldersuserscustomview_user")
                 .Column("User_FolderCustomView")
                 .Not.Nullable()
                 .Fetch.Select();
        }
    }
}
