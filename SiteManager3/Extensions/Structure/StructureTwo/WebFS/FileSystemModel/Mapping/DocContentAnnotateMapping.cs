﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Structure.Search.TextAnalysis;

namespace NetCms.Structure.WebFS.FileSystemModel.Mapping
{
    public class DocContentAnnotateMapping : ClassMap<DocContentAnnotate>
    {
        public DocContentAnnotateMapping()
        {
            Table("docs_contents_annotates");
            Id(x => x.ID);
            Map(x => x.RifContent);

            HasMany<DocAnnotate>(x => x.Annotates)
               .KeyColumn("RifDocContentAnnotate")
               .Fetch.Select().AsSet()                         
               .Cascade.AllDeleteOrphan();
               
        }
    }
}
