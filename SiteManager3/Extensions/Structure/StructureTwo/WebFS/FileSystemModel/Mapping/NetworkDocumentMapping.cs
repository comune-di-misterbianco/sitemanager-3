﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Networks.WebFS;

namespace NetCms.Structure.WebFS.FileSystemModel.Mapping
{
    public class NetworkDocumentMapping : ClassMap<NetworkDocument>
    {
        public NetworkDocumentMapping()
        {
            Table("network_docs");
            Cache.NonStrictReadWrite();
            Id(x => x.ID).Column("id_NetworkDoc");

            References(x => x.Folder)
                .ForeignKey("network_docs_network_folders")
                 .Column("Folder_Doc")
                 .Not.Nullable()
                 .Fetch.Select();
        }
    }
}
