﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Structure.WebFS;

namespace NetCms.Structure.WebFS.FileSystemModel.Mapping
{
    public class DocumentAttachMapping : ClassMap<DocumentAttach>
    {
        public DocumentAttachMapping()
        {
            Table("docs_contents_attaches");
           
            Id(x => x.Id);
            
            References(x => x.Content)
                .Column("DocContent_id")
                .Fetch.Select();

            References(x => x.FileDocument)
                .Class(typeof(Document))
                .Column("Document_id")
                .Fetch.Select();
        }
    }
}
