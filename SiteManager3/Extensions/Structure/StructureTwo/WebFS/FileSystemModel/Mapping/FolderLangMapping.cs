﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Structure.WebFS.FileSystemModel.Mapping
{
    public class FolderLangMapping : ClassMap<FolderLang>
    {
        public FolderLangMapping()
        {
            Table("folderslang");
            Cache.NonStrictReadWrite();
            Id(x => x.ID).Column("id_FolderLang");
            Map(x => x.Label).Column("Label_FolderLang").CustomSqlType("text");
            Map(x => x.Descrizione).Column("Desc_FolderLang").CustomSqlType("varchar (2000)");
            Map(x => x.Lang).Column("Lang_FolderLang");

            References(x => x.Folder)
                .ForeignKey("folderslang_folders")
                 .Column("Folder_FolderLang")
                 .Not.Nullable()
                 .Fetch.Select();
        }
    }
}
