﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Networks.WebFS;
using FluentNHibernate.Mapping;
using NetCms.Networks;

namespace NetCms.Structure.WebFS.FileSystemModel.Mapping
{
    public class NetworkFolderMapping : ClassMap<NetworkFolder>
    {
        public NetworkFolderMapping()
        {
            Table("network_folders");
            Cache.NonStrictReadWrite();
            Id(x => x.ID).Column("id_NetworkFolder");
       
            Map(x => x.Path).Column("Path_Folder").CustomSqlType("varchar (2000)");
          
            Map(x => x.Sistema).Column("System_Folder");

            References(x => x.Network).Class(typeof(BasicNetwork))
                .ForeignKey("folders_networks")
                 .Column("Network_Folder")
                 .Not.Nullable()
                 .Fetch.Select();

            References(x => x.Parent)
                 .ForeignKey("network_folders_parent")
                 .Column("Parent_Folder")
                 .Nullable()
                 .Fetch.Select()
                 .LazyLoad(Laziness.False);

            HasMany(x => x.Folders)
                .KeyColumn("Parent_Folder")
               .Fetch.Select().AsSet()
               .Cascade.All();

            HasMany(x => x.Documents)
                .KeyColumn("Folder_Doc")
                .Fetch.Select().AsSet()
                .Cascade.All();

        }
    }
}