﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Structure.WebFS.Reviews;

namespace NetCms.Structure.WebFS.FileSystemModel.Mapping
{
    public class ReviewMapping : ClassMap<Review>
    {
        public ReviewMapping()
        {
            Table("revisioni");
            Cache.NonStrictReadWrite();
            Id(x => x.ID).Column("id_Revisione");
            Map(x => x.Published).Column("Published_Revisione");
            Map(x => x.Approvata).Column("Approvata_Revisione").Default("-1");
            Map(x => x.Autore).Column("Autore_Revisione");
            Map(x => x.Data).Column("Data_Revisione");
            Map(x => x.Origine).Column("Origine_Revisione");

            References(x => x.DocLang)
                .ForeignKey("revisioni_docs_lang")
                 .Column("DocLang_Revisione")
                 .Not.Nullable()
                 .Fetch.Select();

            References(x => x.Content, "Contenuto_Revisione")
                .ForeignKey("revisioni_docs_contents").Unique().Cascade.All()
                .Nullable();

            HasMany(x => x.ReviewNotifications)
                .KeyColumn("Revisione_RevisioneNotifica")
                .Fetch.Select().AsSet()
                .Cascade.All();

            HasMany(x => x.ReviewLogs)
                .KeyColumn("Revisione_RevLog")
                .Fetch.Select().AsSet()
                .Cascade.All();
        }
    }
}
