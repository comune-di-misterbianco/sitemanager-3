﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Networks;

namespace NetCms.Structure.WebFS.FileSystemModel.Mapping
{
    public class StructureFolderMapping : SubclassMap<StructureFolder>
    {
        public StructureFolderMapping()
        {
            Table("folders");
            KeyColumn("id_Folder");
            Map(x => x.Nome).Column("Name_Folder");
            Map(x => x.Tree).Column("Tree_Folder");
            Map(x => x.Tipo).Column("Tipo_Folder");
            Map(x => x.Stato).Column("Stato_Folder");
            Map(x => x.Depth).Column("Depth_Folder");
            Map(x => x.Trash).Column("Trash_Folder");
            Map(x => x.RestrictedInt).Column("Restricted_Folder");
            Map(x => x.HomeType).Column("HomeType_Folder");
            Map(x => x.Disable).Column("Disable_Folder");
            Map(x => x.TitleOffset).Column("TitleOffset_Folder");
            Map(x => x.CssClass).Column("CssClass_Folder");
            Map(x => x.Order).Column("Order_Folder");
            Map(x => x.Rss).Column("Rss_Folder");
            Map(x => x.ShowMenu).Column("ShowMenu_Folder").Default("0");
            Map(x => x.Layout).Column("Layout_Folder");          
            Map(x => x.Template).Column("Template_Folder");
            Map(x => x.ShowInTopMenu).Column("ShowInTopMenu_Folder").Default("1");
            Map(x => x.CoverImage).Column("CoverImage_Folder");

            HasMany(x => x.FolderLangs)
                .KeyColumn("Folder_FolderLang")
                .Fetch.Select().AsSet()
                .Cascade.All();

            HasMany(x => x.FolderUsersCustomViews)
                .KeyColumn("Folder_FolderCustomView")
                .Fetch.Select().AsSet()
                .Cascade.All().Inverse();

        }
    }
}
