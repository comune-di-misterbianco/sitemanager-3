﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Structure.WebFS.Reviews;

namespace NetCms.Structure.WebFS.FileSystemModel.Mapping
{
    public class ReviewLogMapping : ClassMap<ReviewLog>
    {
        public ReviewLogMapping()
        {
            Table("revisionilog");
            Cache.NonStrictReadWrite();
            Id(x => x.ID).Column("id_RevLog");
            Map(x => x.Mittente).Column("Mittente_RevLog");
            Map(x => x.Destinatario).Column("Destinatario_RevLog").Default("0");
            Map(x => x.Livello).Column("Livello_RevLog");
            Map(x => x.Data).Column("Data_RevLog");
            Map(x => x.Oggetto).Column("Oggetto_RevLog").Default("0");
            Map(x => x.Testo).Column("Testo_RevLog").CustomSqlType("varchar(2000)");

            References(x => x.Review)
                .ForeignKey("revisionilog_revisioni")
                 .Column("Revisione_RevLog")
                 .Not.Nullable()
                 .Fetch.Select();

            HasMany(x => x.ReviewNotifications)
                .KeyColumn("Log_RevisioneNotifica")
                .Fetch.Select().AsSet()
                .Cascade.All();
                        
        }
    }
}
