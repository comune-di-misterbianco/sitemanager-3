﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Structure.WebFS.FileSystemModel.Mapping
{
    public class DocumentMapping : SubclassMap<Document>
    {
        public DocumentMapping()
        {
            Table("docs");
            KeyColumn("id_Doc");
            Map(x => x.PhysicalName).Column("Name_Doc");
            Map(x => x.Stato).Column("Stato_Doc");
            Map(x => x.Data).Column("Data_Doc");
            Map(x => x.Modify).Column("Modify_Doc");
            Map(x => x.Autore).Column("Autore_Doc");
            Map(x => x.Hide).Column("Hide_Doc");
            Map(x => x.CssClass).Column("CssClass_Doc");
            Map(x => x.Application).Column("Application_Doc");
            Map(x => x.TitleOffset).Column("TitleOffset_Doc");
            Map(x => x.Trash).Column("Trash_Doc");
            Map(x => x.Create).Column("Create_Doc");
            Map(x => x.LangPickMode).Column("LangPickMode_Doc");
            Map(x => x.Order).Column("Order_Doc");
            Map(x => x.Layout).Column("Layout_Doc");
            Map(x => x.ShowInTopMenu).Column("ShowInTopMenu_Doc").Default("1");

            HasMany(x => x.DocLangs)
                .KeyColumn("Doc_DocLang")
                .Fetch.Select().AsSet()
                .Cascade.All();

            HasMany(x => x.ReviewNotifications)
                .KeyColumn("Doc_RevisioneNotifica")
                .Fetch.Select().AsSet()
                .Cascade.All();

            References(x => x.DocumentoRiferito)
                 .ForeignKey("docs_docriferito")
                 .Column("DocRiferito_Doc")
                 .Nullable()                 
                 .Fetch.Select();

            HasMany(x => x.Allegati)
                .KeyColumn("DocRiferito_Doc")
               .Fetch.Select().AsSet()               
               .Cascade.All();          

            HasMany(x => x.DocTags)                
               .KeyColumn("Doc_DocTag")
               .Fetch.Select().AsSet().Inverse()
               .Cascade.All();
        }
    }
}
