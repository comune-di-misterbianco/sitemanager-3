﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Structure.WebFS.FileSystemModel.Mapping
{
    public class DocumentLangMapping : ClassMap<DocumentLang>
    {
        public DocumentLangMapping()
        {
            Table("docs_lang");
            Cache.NonStrictReadWrite();
            Id(x => x.ID).Column("id_DocLang");
            Map(x => x.Label).Column("Label_DocLang");
            Map(x => x.Descrizione).Column("Desc_DocLang").CustomSqlType("varchar (2000)");
            Map(x => x.Lang).Column("Lang_DocLang");

            References(x => x.Document)
                 .ForeignKey("docs_lang_docs")
                 .Column("Doc_DocLang")
                 .Not.Nullable()
                 .Fetch.Select();

            References(x => x.Content, "Content_DocLang")
                .ForeignKey("docs_lang_docs_contents").Unique().Cascade.All()
                .Nullable();

            HasMany(x => x.Revisioni)
                .KeyColumn("DocLang_Revisione")
                .Fetch.Select().AsSet()
                .Cascade.All();
        }
    }
}
