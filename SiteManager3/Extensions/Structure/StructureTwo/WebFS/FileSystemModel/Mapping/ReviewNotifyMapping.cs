﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Structure.WebFS.Reviews;

namespace NetCms.Structure.WebFS.FileSystemModel.Mapping
{
    public class ReviewNotifyMapping : ClassMap<ReviewNotify>
    {
        public ReviewNotifyMapping()
        {
            Table("revisioni_notifiche");
            Cache.NonStrictReadWrite();
            Id(x => x.ID).Column("id_RevisioneNotifica");
            Map(x => x.Mittente).Column("Mittente_RevisioneNotifica");
            Map(x => x.Destinatario).Column("Destinatario_RevisioneNotifica");
            Map(x => x.Stato).Column("Stato_RevisioneNotifica").Default("0");

            References(x => x.Review)
                .ForeignKey("revisioni_notifiche_revisioni")
                 .Column("Revisione_RevisioneNotifica")
                 .Not.Nullable()
                 .Fetch.Select();

            References(x => x.Document)
                .ForeignKey("revisioni_notifiche_docs")
                 .Column("Doc_RevisioneNotifica")
                 .Not.Nullable()
                 .Fetch.Select();

            References(x => x.ReviewLog)
                .ForeignKey("revisioni_notifiche_revisionilog")
                 .Column("Log_RevisioneNotifica")
                 .Not.Nullable()
                 .Fetch.Select();

        }
    }
}
