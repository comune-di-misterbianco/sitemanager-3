﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Data;
using System.ComponentModel;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetCms.Front;
using System.Collections.Generic;
using NetCms.Networks.WebFS;
using NetCms.Networks;
using GenericDAO.DAO.Utility;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.Applications;
using NHibernate;
using NetCms.Structure.WebFS.Reviews;
using System.Reflection;
using NetCms.Grants.Interfaces;
using NetCms.Networks.Grants;
using NetCms.Grants;
using NetCms.Users;
using Structure.StructureTwo.Vertical.Importer.BusinessLogic;
using SharedUtilities;
using Structure.Importer.Model;
using Structure.StructureTwo.Vertical.Importer.Model;

namespace NetCms.Structure.WebFS
{
    public abstract partial class StructureFolder : NetworkFolder,IGrantsFindable
    {

        /// <summary>
        /// Restituisce l'ID della lingua in uso
        /// </summary>
        /// <returns></returns>
        private int _LangID = 1;
        public virtual int LangID
        {
            get
            {
                //CmsConfigs.Model.Config currentconfig = CmsConfigs.BusinessLogic.ConfigsBusinessLogic.GetConfigByIdNetwork(NetworkID); // da riadattare usando un pattern singleton
                var currentconfig = (PageData != null && PageData.CurrentConfig != null) ? PageData.CurrentConfig : CmsConfigs.BusinessLogic.ConfigsBusinessLogic.GetConfigByIdNetwork(NetworkID);
                if (currentconfig != null && currentconfig.CurrentLanguage != null)  
                    //if (PageData != null && PageData.CurrentConfig != null && PageData.CurrentConfig.CurrentLanguage != null) 
                    // l'oggetto PageData genera eccezioni in quanto non thread safe
                    _LangID = PageData.CurrentConfig.CurrentLanguage.ID;

                return _LangID;
            }
        }

        public virtual int ParentLangID
        {
            get
            {
                int parentLangID = 1;
                if (PageData != null && PageData.CurrentConfig != null && PageData.CurrentConfig.CurrentLanguage != null)
                    parentLangID = PageData.CurrentConfig.CurrentLanguage.Parent_Lang;

                return parentLangID;
            }
        }

        public StructureFolder()
            : base()
        {
            if (NetCms.Users.AccountManager.UserAlreadyLogged())
                this.ReferedAccount = NetCms.Users.AccountManager.CurrentAccount;
            
            FolderLangs = new HashSet<FolderLang>();
            FolderUsersCustomViews = new HashSet<FoldersUsersCustomView>();
            
        }

        public StructureFolder(NetworkKey networkKey)
            : base(networkKey)
        {
            if (NetCms.Users.AccountManager.UserAlreadyLogged())
                this.ReferedAccount = NetCms.Users.AccountManager.CurrentAccount;
            
            FolderLangs = new HashSet<FolderLang>();
            FolderUsersCustomViews = new HashSet<FoldersUsersCustomView>();
            
            //Folders = new HashSet<NetworkFolder>();
            //Documents = new HashSet<NetworkDocument>();

            //NetworkKey = networkKey;
        }

        public override NetworkKey NetworkKey
        {
            get
            {
                return new NetworkKey(NetworkID);
            }
        }

        // INSERITO PER IMPLEMENTARE IL COMPORTAMENTO DI DEFAULT DELLA COLLEZIONE DI SUBFOLDERS NEL VECCHIO CMS
        public virtual ICollection<StructureFolder> SubFolders
        {
            get
            {
                if (_SubFolder == null)
                {
                    _SubFolder = FolderBusinessLogic.FindSubFoldersByParentFolder(this.ID);//new HashSet<StructureFolder>();
                            

                    //foreach (NetworkFolder fold in base.Folders)
                    //{
                    //    StructureFolder folder = FolderBusinessLogic.GetById(fold.ID);
                    //    if (folder.Trash == 0)
                    //        _SubFolder.Add(folder);
                    //}

                    switch (ViewOrder)
                    {
                        case ViewOrders.ID: _SubFolder = _SubFolder.OrderByDescending(x => x.ID).ToList(); break;
                        case ViewOrders.Label: _SubFolder = _SubFolder.OrderBy(x => x.FolderLangs.ElementAt(0).Label).ToList(); break;
                        case ViewOrders.Name: _SubFolder = _SubFolder.OrderBy(x => x.Nome).ToList(); break;
                        case ViewOrders.LabelDesc: _SubFolder = _SubFolder.OrderByDescending(x => x.FolderLangs.ElementAt(0).Label).ToList(); break;
                        default: _SubFolder = _SubFolder.OrderBy(x => x.FolderLangs.ElementAt(0).Label).ToList(); break;
                    }
                }

                return _SubFolder;
            }
        }
        private ICollection<StructureFolder> _SubFolder;

        //private NetworkKey _NetworkKey;
        //public NetworkKey NetworkKey
        //{
        //    get
        //    {
        //        return _NetworkKey;
        //    }
        //    private set { _NetworkKey = value; }
        //}

        #region Proprietà da mappare

        // in più ci sono le id e name di CmsWebFSObject

        public virtual int Disable
        {
            get;
            set;
        }

        public virtual int Layout
        {
            get;
            set;
        }

        public virtual string Template
        {
            get;
            set;               
        }

        public virtual string CoverImage
        {
            get;
            set;
        }

        public virtual string CoverImageSrc
        {
            get
            {
                string srcImg = "";
                if (CoverImage != null && !string.IsNullOrEmpty(CoverImage))
                {
                    Document coverImg = NetCms.Structure.WebFS.FileSystemBusinessLogic.DocumentBusinessLogic.GetById(int.Parse(CoverImage));
                    if (coverImg != null)
                        srcImg = coverImg.FrontendLink;

                }

                return srcImg;
            }
        }

        public virtual int Stato
        {
            get;
            set;
        }

        public virtual int Rss
        {
            get;
            set;
        }

        public virtual int ShowMenu
        {
            get;
            set;
        }
        public virtual int ShowInTopMenu
        {
            get;
            set;
        }
        public virtual int Depth
        {
            get;
            set;
        }

        public virtual string Tree
        {
            get;
            set;
        }

        public virtual int RestrictedInt
        {
            get;
            set;
        }

        public virtual string CssClass
        {
            get;
            set;
        }

        // sarà l'id applicazione
        public virtual int Tipo
        {
            get;
            set;
        }

        public virtual int TitleOffset
        {
            get;
            set;
        }

        public virtual int Order
        {
            get;
            set;
        }

        public virtual int Trash
        {
            get;
            set;
        }

        public virtual int HomeType
        {
            get;
            set;
        }

        //public virtual int Sistema
        //{
        //    get;
        //    set;
        //}

        public virtual ICollection<FolderLang> FolderLangs
        {
            get;
            set;
        }

        //public override ICollection<StructureFolder> Folders
        //{
        //    get;
        //    set;
        //}

        //public override ICollection<Document> Documents
        //{
        //    get;
        //    set;
        //}

        public virtual ICollection<FoldersUsersCustomView> FolderUsersCustomViews
        {
            get;
            set;
        }

        #endregion

        #region Metodi precedentemente in NetworkFolder

        public virtual int NetworkID
        {
            get
            {
                return StructureNetwork.ID;
            }
        }

        public virtual bool HasSubFolders
        {
            get
            {
                return FolderBusinessLogic.CountSubFolder(this) > 0;
            }
        }

        public virtual bool HasDocuments
        {
            get
            {
                return DocumentBusinessLogic.CountDocumentsByFolder(this) > 0;
            }
        }

        private List<CmsWebFSObject> _Childs;
        public override List<CmsWebFSObject> Childs
        {
            get { if (_Childs == null) InitChilds(); return _Childs; }
        }

        private void InitChilds()
        {
            _Childs = new List<CmsWebFSObject>();

            foreach (CmsWebFSObject obj in this.SubFolders)
                _Childs.Add(obj);
            foreach (CmsWebFSObject obj in this.Documents)
                _Childs.Add(obj);
        }

        public override CmsWebFSObject ParentObject
        {
            get { return this.Parent; }
        }

        public override WebFSObjectTypes ObjectType
        {
            get { return WebFSObjectTypes.Folder; }
        }

        public override NetCms.Connections.Connection Connection
        {
            get
            {
                return this.StructureNetwork.Connection;
            }
        }

        public virtual bool IsRootFolder
        {
            get
            {
                return this.Depth == 1 && Parent == null;
            }
        }

        public override string QueryString4URLs
        {
            get { return "folder=" + this.ID; }
        }

        public virtual int StateValue
        {
            get
            {
                if (Parent != null)
                {
                    //return Stato >= (Parent as StructureFolder).StateValue ? Stato : (Parent as StructureFolder).StateValue;

                    int parentFolderState = (Parent as StructureFolder).StateValue;

                    //FolderStates parFolderState = (FolderStates)Enum.ToObject(typeof(FolderStates), parentFolderState);
                    //FolderStates folderState = (FolderStates)Enum.ToObject(typeof(FolderStates), Stato);

                    //if (folderState == parFolderState)
                    //    return Stato;
                    //else
                    //{
                    //    if (parFolderState == FolderStates.NotReachable)
                    //        return parentFolderState;

                    //}

                    if (Stato == parentFolderState)
                    {
                        return Stato;
                    }
                    else
                    {
                        if (parentFolderState == 2 )//|| parentFolderState == 1)
                        {
                            return parentFolderState;
                        }
                        else
                        {
                            return Stato;
                        }
                    }
                }
                else
                {
                    return Stato;
                }
            }
        }

        //public virtual string TypeName
        //{
        //    get
        //    {
        //        if (_TypeName == null)
        //        {

        //            /*DataRow[] rows = Applicativi.Select("id_Applicativo = " + Type);
        //            if (rows.Length > 0)
        //                _TypeName = rows[0]["Nome_Applicativo"].ToString();
        //            else
        //                _TypeName = "";*/
        //        }
        //        return _TypeName;
        //    }
        //}
        //protected string _TypeName;

        public virtual bool Restricted
        {
            get
            {
                if (Parent != null)
                    return RestrictedInt != 0 || (Parent as StructureFolder).Restricted;
                return RestrictedInt != 0;
            }
        }

        public virtual int RestrictedArea
        {
            get
            {
                if (RestrictedInt == 0 && Parent != null)
                    return (Parent as StructureFolder).RestrictedArea;
                return RestrictedInt;
            }
        }

        public virtual RssStatus RssValue
        {
            get
            {
                try
                {
                    switch (Rss)
                    {
                        case 0: return RssStatus.Inheriths;
                        case 1: return RssStatus.Enabled;
                        case 2: return RssStatus.Disabled;
                        default: return RssStatus.SystemError;
                    }
                }
                catch { return RssStatus.SystemError; }
            }
        }
        public enum RssStatus { SystemError, Inheriths, Disabled, Enabled }

        public virtual bool EnableRss
        {
            get
            {
                switch (RssValue)
                {
                    case RssStatus.Disabled: return false;
                    case RssStatus.Enabled: return true;
                    case RssStatus.Inheriths: return Parent != null ? (Parent as StructureFolder).EnableRss : false;
                    case RssStatus.SystemError: return false;
                    default: return false;
                }
            }
        }

        public virtual string StatusLabel
        {
            get
            {
                return StatusLabels(this.StateValue); ;
            }
        }

        public static string StatusLabels(int status)
        {
            string statlabel = "";
            switch (status)
            {
                case 0:
                    //statlabel = "Mostra nel menu principale e rendi raggiungible";
                    statlabel = EnumUtilities.GetEnumDescription(FolderStates.Menu);
                    break;
                case 1:
                    //statlabel = "Raggiungibile";
                    statlabel = EnumUtilities.GetEnumDescription(FolderStates.Reachable);
                    break;
                case 2:
                    //statlabel = "Non raggiungibile";
                    statlabel = EnumUtilities.GetEnumDescription(FolderStates.NotReachable);
                    break;
                case 3:
                    statlabel = "Nascosto";
                    break;
                case 4:
                    //statlabel = "Mostra solo nel menu contestuale e rendi raggiungibile";
                    statlabel = EnumUtilities.GetEnumDescription(FolderStates.SubMenu);                    
                    break;
            }
            return statlabel;
        }

        //public virtual FolderStates Status
        //{
        //    get
        //    {
        //        FolderStates stat = FolderStates.NotReachable;

        //        switch (StateValue)
        //        {
        //            case 0:
        //                stat = FolderStates.Menu;
        //                break;
        //            case 1:
        //                stat = FolderStates.Reachable;
        //                break;
        //            case 2:
        //                stat = FolderStates.NotReachable;
        //                break;
        //        }
        //        return stat;
        //    }
        //}

        public enum FolderStates
        {
            [Description("Mostra in tutti i menu")]
            Menu = 0,
            [Description("Mostra solo nel menu contestuale")]
            SubMenu = 4, // mostra la folder solo nel menù contestuale se attivo
            [Description("Raggiungibile")]
            Reachable = 1,
            [Description("Non raggiungibile")]
            NotReachable = 2,
            //[Description("Nascosto")]
            //Hidden = 3
        }
        #endregion

        #region Metodi implementati per contratto con la classe base "NetCms.Networks.WebFS.NetworkFolder"

        //public override G2Core.Caching.ICacheableObject BuildItem(DataRow data)
        //{
        //    return Fabricate(data, NetworkKey);//new StructureFolder(data);
        //}

        public virtual NetCms.Structure.Applications.Application RelativeApplication
        {
            get;
            set;

        }
        //private NetCms.Structure.Applications.Application _RelativeApplication;

        //public static StructureFolder Fabricate(DataRow data, Networks.NetworkKey Network)
        //{
        //    string ApplicationID = data["Tipo_Folder"].ToString();
        //    if (ApplicationsPool.ActiveAssemblies.Contains(ApplicationID))
        //    {
        //        object[] parameters = { data, Network };
        //        Application application = ApplicationsPool.ActiveAssemblies[ApplicationID];
        //        Type ApplicationFolderType = application.Assembly.GetType(application.Classes.Folder);
        //        StructureFolder folder = (StructureFolder)System.Activator.CreateInstance(ApplicationFolderType, parameters);
        //        return folder;
        //    }
        //    //Trace
        //    return null;
        //}


        //private NetCms.Structure.WebFS.StructureFolderCollection _SubFolders;
        //public override NetCms.Networks.WebFS.NetworkFoldersCollection Folders
        //{
        //    get
        //    {
        //        if (_SubFolders == null)
        //        {
        //            _SubFolders = new NetCms.Structure.WebFS.StructureFolderCollection(this.NetworkKey, G2Core.Caching.Visibility.CachingVisibility.User, this.Network.Connection);
        //            _SubFolders.FillWithChildFolders(this.ID, false, this.ViewOrder, StructureFolderCollection.FoldersOrdersDirections.Default);
        //            _SubFolders.OwnerItem = this.OwnerCollection.GetCacheableItem(this.ID);
        //        }

        //        return _SubFolders;
        //    }
        //}

        //private ICollection<StructureFolder> _SubFolders;
        //public ICollection<StructureFolder> Folders
        //{
        //    get
        //    {
        //        if (_SubFolders == null)
        //        {
        //            _SubFolders = new NetCms.Structure.WebFS.StructureFolderCollection(this.NetworkKey, G2Core.Caching.Visibility.CachingVisibility.User, this.Network.Connection);
        //            _SubFolders.FillWithChildFolders(this.ID, false, this.ViewOrder, StructureFolderCollection.FoldersOrdersDirections.Default);
        //            _SubFolders.OwnerItem = this.OwnerCollection.GetCacheableItem(this.ID);
        //        }

        //        return _SubFolders;
        //    }
        //}

        //protected override NetCms.Networks.WebFS.NetworkDocumentsCollection BuildDocumentsCollection()
        //{
        //    DocumentsCollection coll = new DocumentsCollection(this.NetworkKey, G2Core.Caching.Visibility.CachingVisibility.User, this.Network.Connection);

        //    coll.FillWithDocsFromFolder(this.ID, false, this.ViewOrder, DocumentsCollection.DocumentsOrdersDirections.Default);

        //    return coll;
        //}

        //public new DocumentsCollection Documents
        //{
        //    get
        //    {
        //        return (DocumentsCollection)base.Documents;
        //    }
        //}

        public virtual new StructureNetwork StructureNetwork
        {
            get
            {
                return new StructureNetwork(Network);
            }
        }

        public override NetworkFolder FindFolderByID(int id)
        {
            return FolderBusinessLogic.GetById(id);//FolderBusinessLogic.FindFolder(this, id.ToString(), FolderBusinessLogic.SearchFor.ID);
        }

        public override NetworkFolder FindFolderByName(string name)
        {
            return FolderBusinessLogic.FindFolder(this, name, FolderBusinessLogic.SearchFor.Name);
        }

        public override NetworkFolder FindFolderByPath(string path)
        {
            return FolderBusinessLogic.FindFolder(this, path, FolderBusinessLogic.SearchFor.Path);
        }

        public override NetworkDocument FindDocByID(int id)
        {
            return DocumentBusinessLogic.GetById(id);//DocumentBusinessLogic.FindDocument(this, id.ToString(), DocumentBusinessLogic.SearchFor.ID);
        }

        public override NetworkDocument FindDocByPhysicalName(string name)
        {
            return DocumentBusinessLogic.FindDocument(this, name, DocumentBusinessLogic.SearchFor.PhysicalName);
        }

        public override NetworkDocument GetDocumentByPhysicalName(string name)
        {
            return DocumentBusinessLogic.GetDocumentByPhysicalName(this, name);
        }

        public override NetworkFolder GetFolderByNetworkAndParentsPath(string folderPath, int networkId, List<string> paths)
        {
            return FolderBusinessLogic.GetFolderByNetworkAndParentsPath(folderPath, networkId, paths);
        }
        //public new Document FindDoc(string key)
        //{
        //    //return (Document)base.FindDoc(key);
        //    // Implementare possibilmente con il generic dao facendo find by id
        //    throw new NotImplementedException();
        //}

        #endregion


        #region Proprietà

        public override string TranslatedPath
        {
            get
            {
                return TranslatePath(LangID, ParentLangID);
            }

        }

        
        public override string TranslatePath(int langId, int parentLangId)
        {
            string path = "";
            StructureFolder temp = this;
            do
            {
                bool defaultLang = langId == LanguageManager.BusinessLogic.LanguageBusinessLogic.GetDefaultLanguage().ID;
                FolderLang tempFolderLang = temp.FolderLangs.Where(x => x.Lang == langId).FirstOrDefault();

                if (tempFolderLang == null)
                {
                    defaultLang = true;
                    tempFolderLang = temp.FolderLangs.Where(x => x.Lang == parentLangId).FirstOrDefault();
                }

                path = "/" + (temp.Path.Length > 0 ? (defaultLang ? temp.Nome : tempFolderLang.Label.ToLower().Replace(" ", "-")) + path : "");

                temp = (StructureFolder)temp.Parent;
            }
            while (temp != null && temp.Parent != null);

            return path;
        }

        public virtual string ObjectHashCode
        {
            get
            {
                if (_ObjectHashCode == null)
                    _ObjectHashCode = this.GetHashCode().ToString();
                return _ObjectHashCode;
            }
        }
        private string _ObjectHashCode;

        public virtual NetCms.Structure.CmsPathCodes Codes
        {
            get
            {
                if (_Codes == null)
                    _Codes = new NetCms.Structure.CmsPathCodes(this.StructureNetwork);
                return _Codes;
            }
        }
        private NetCms.Structure.CmsPathCodes _Codes;

        public virtual Users.User Account
        {
            get
            {
                return Users.AccountManager.CurrentAccount;
            }
        }

        //public DocumentsCollection DocumentsByAllTree
        //{
        //    get
        //    {
        //        if (_DocumentsByAllTree == null)
        //        {
        //            _DocumentsByAllTree = new DocumentsCollection(this.NetworkKey, G2Core.Caching.Visibility.CachingVisibility.User, this.Network.Connection);
        //            _DocumentsByAllTree.FillWithDocsFromFolderTree(this.ID);
        //        }
        //        return _DocumentsByAllTree;
        //    }
        //}
        //private DocumentsCollection _DocumentsByAllTree;

        // TODO: miglioare le modalità di filtro dei record da recuperare 
        public virtual ICollection<Document> DocumentsByAllTree(int recordLimit)
        {
            //get
            //{
                return DocumentBusinessLogic.FindDocumentsByFolderTree(this.Tree, recordLimit);
            //}
        }

        public virtual bool AllowMove
        {
            get
            {
                return false;
            }
        }

        public virtual string Label
        {
            get
            {
             //   return this.FolderLangs.ElementAt(0).Label;
                return CurrentFolderLang.Label;
            }
        }

        public virtual string Descrizione
        {
            get
            {
                //return this.FolderLangs.ElementAt(0).Descrizione;
                return CurrentFolderLang.Descrizione;
            }
        }

        /// <summary>
        /// Restituisce la folderlang in relazione alla lingua correntemente settata - se il record non è disponibile 
        /// lo recupera usando la lingua di default (ita).
        /// </summary>
        public virtual FolderLang CurrentFolderLang
        {
            get
            {
                FolderLang currentFolderLang = null;
                // check if exist content in current lang - if not return content from parent lang
                currentFolderLang = this.FolderLangs.Where(x => x.Lang == LangID).FirstOrDefault();
                if (currentFolderLang == null)
                    currentFolderLang = this.FolderLangs.Where(x => x.Lang == ParentLangID).FirstOrDefault();

                return currentFolderLang;
            }
        }

        public override NetworkDocument CurrentDocument
        {
            get
            {
                if (Networks.NetworksManager.NetworkStatus == Networks.NetworksStates.OutsideNetworks)
                {
                    Exception ex = new Exception("Impossibile ottenere il documento corrente quando non si è all'interno di una Network");
                    NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                    ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);
                }
                if (HttpContext.Current.Items["Networks.WebFS.NetworkDocument.CurrentDocument"] == null)
                {
                    NhRequestObject<NetworkDocument> documentRequest = new NhRequestObject<NetworkDocument>(FolderBusinessLogic.GetCurrentSession(), "doc");
                    NetworkDocument document = null;
                    if (documentRequest.Exists)
                        document = documentRequest.Instance;

                    HttpContext.Current.Items["Networks.WebFS.NetworkDocument.CurrentDocument"] = document;
                }
                return (NetworkDocument)HttpContext.Current.Items["Networks.WebFS.NetworkDocument.CurrentDocument"];
            }
        }

        public virtual bool Starred
        {
            get
            {
                if (_Starred == 0)
                {
                    InitStarred();
                }
                return _Starred == 1;
            }
        }
        protected int _Starred;
        public virtual void InitStarred()
        {
            //DataTable table = this.StructureNetwork.Connection.SqlQuery("SELECT * FROM Favorites");
            //DataRow[] rows = table.Select("Folder_Favorite = " + this.ID + " AND User_Favorite = " + this.PageData.Account.ID);
            //if (rows.Length > 0)
            //    _Starred = 1;
            //else
            //    _Starred = 2;

            //ICollection<Favorites> favorites = FolderBusinessLogic.FindFavoritesByUserAndFolder(this.PageData.Account, this);
            //if (favorites.Count > 0)
            //    _Starred = 1;
            //else _Starred = 2;

            if (this.PageData.Account.FavoritesFolders.Contains(this))
                _Starred = 1;
            else _Starred = 2;
        }

        //public new StructureFolder Parent
        //{
        //    get
        //    {
        //        return (StructureFolder)base.Parent;
        //    }
        //}
        //public StructureFolderCollection SubFolders
        //{
        //    get
        //    {
        //        return (StructureFolderCollection)this.Folders;
        //    }
        //}

        public virtual bool ReviewEnabled { get; set; }

        private PageData _PageData;
        public virtual PageData PageData
        {
            get
            {
                if (_PageData == null || _PageData.GetHashCode() != ((PageData)HttpContext.Current.Items["SM_PageData"]).GetHashCode())
                    _PageData = (PageData)HttpContext.Current.Items["SM_PageData"];
                return _PageData;
            }
        }

        private bool? _ContainsLowLevelSystemFolders;
        public virtual bool ContainsLowLevelSystemFolders
        {
            get
            {
                if (_ContainsLowLevelSystemFolders == null)
                {
                    DirectoryInfo dir = new DirectoryInfo(HttpContext.Current.Server.MapPath(PageData.Paths.AbsoluteWebRoot + this.Path));

                    bool found = false;
                    if (dir.Exists)
                    {
                        DirectoryInfo[] subs = dir.GetDirectories();
                        found = subs.Where(x => NetCms.Configurations.Generics.SystemFiles.Contains(x.Name.ToLower())).Count() > 0;
                    }
                    _ContainsLowLevelSystemFolders = new bool?(found);
                }
                return _ContainsLowLevelSystemFolders.Value;
            }
        }

        public virtual Serialization Serialized
        {
            get
            {
                if (_Serialized == null)
                    _Serialized = new Serialization(this);
                return _Serialized;
            }
        }
        private Serialization _Serialized;

        public virtual FrontLayouts FrontLayout
        {
            get
            {
                try
                {
                    switch (Layout)
                    {
                        case 0: return FrontLayouts.Inherits;
                        case 1: return FrontLayouts.LeftRight;
                        case 2: return FrontLayouts.LeftFooter;
                        case 3: return FrontLayouts.RightFooter;
                        case 4: return FrontLayouts.Left;
                        case 5: return FrontLayouts.Right;
                        case 6: return FrontLayouts.Footer;
                        case 7: return FrontLayouts.None;
                        case 8: return FrontLayouts.All;
                        default: return FrontLayouts.SystemError;
                    }
                }
                catch
                {
                    return FrontLayouts.SystemError;
                }
            }
        }

        public virtual int Homepage
        {
            get { return _Homepage; }
        }
        protected int _Homepage;


        private int _HasNext;
        public virtual bool HasNext
        {
            get
            {
                if (_HasNext == 0)
                {
                    _HasNext = 2;
                    if (Parent != null)
                    {
                        bool found = false;

                        ICollection<StructureFolder> subfolders = (Parent as StructureFolder).SubFolders;
                        for (int i = 0; i < subfolders.Count && !found; i++)
                        {
                            StructureFolder folder = subfolders.ElementAt(i) as StructureFolder;
                            if (folder == this)
                            {
                                _HasNext = i + 1 < subfolders.Count ? 1 : 2;
                                found = true;
                            }
                        }
                    }
                }
                return _HasNext == 1;
            }
        }

        public virtual bool AddKeywordsToTitle
        {
            get
            {
                return _TitleOffset == 1;
            }
        }
        private int _TitleOffset;

        public virtual string Icon
        {
            get
            {
                return "icon_folder";
            }
        }

        private int _NeedVisualization;
        public virtual bool NeedVisualization
        {
            get
            {
                /*if (_NeedVisualization == 0)
                {
                    bool show = this.Grant;

                    if (!show && this.EnableGrantsElab)
                        for (int i = 0; i < Documents.Count && !show; i++)
                            show = show || Documents[i].Grant;
                    if (!show)
                        for (int i = 0; i < SubFolders.Count && !show; i++)
                            show = show || SubFolders[i].NeedVisualization;

                    _NeedVisualization = show ? 1 : 2;
                }*/
                //return _NeedVisualization == 1;
                
                    var item = NetCms.Users.AccountManager.CurrentAccount.FoldersCanView.Where(x => x.Key == this.ID);
                    if (item.Count() > 0)
                        return item.First().Value;
                    
                    bool show = this.NeedToShow;
                    lock (NetCms.Users.AccountManager.CurrentAccount.FoldersCanView)
                    {
                        NetCms.Users.AccountManager.CurrentAccount.FoldersCanView.Add(new KeyValuePair<int, bool>(this.ID, show)); 
                    }
                    return show;
            }
        }

        #region ViewOrder

        private ViewOrders ViewOrdersSwitch(int val)
        {
            switch (val)
            {
                case 0: return ViewOrders.Undefined;
                case 1: return ViewOrders.Label;
                case 2: return ViewOrders.CreationDate;
                case 3: return ViewOrders.ModifyDate;
                case 6: return ViewOrders.LabelDesc;
                case 7: return ViewOrders.CreationDateDesc;
                default: return ViewOrders.Undefined;
            }
        }
        private ViewOrders _ViewOrder;
        public virtual ViewOrders ViewOrder
        {
            get
            {
                if (this.Account != null)
                {
                    bool loadFromPostBack = false;
                    int UserID = this.Account.ID;
                    if (this.PageData.IsPostBack)
                    {
                        NetUtility.RequestVariable sortrequest = new NetUtility.RequestVariable("SortDocsBy_" + this.ID, NetUtility.RequestVariable.RequestType.Form);
                        if (sortrequest.IsValidInteger)
                        {
                            _ViewOrder = ViewOrdersSwitch(sortrequest.IntValue);
                            int val = sortrequest.IntValue;

                            FoldersUsersCustomView customView;
                            var viewFound = this.FolderUsersCustomViews.Where(x => x.User.ID == UserID);
                            if (viewFound.Count() > 0)
                            {
                                customView = viewFound.First();
                            }
                            else
                            {
                                customView = new FoldersUsersCustomView();
                                customView.Folder = this;
                                customView.User = this.Account;
                                this.FolderUsersCustomViews.Add(customView);
                            }

                            customView.ViewOrder = ViewOrdersSwitch(val);

                            FolderBusinessLogic.SaveOrUpdateFolderUserCustomView(customView);
                            //string sql = "DELETE FROM folders_users_custom_view WHERE Folder_FolderCustomView = " + this.ID + " AND User_FolderCustomView = " + UserID + "";
                            //this.Network.Connection.Execute(sql);
                            //sql = "INSERT INTO folders_users_custom_view (Folder_FolderCustomView,User_FolderCustomView,View_FolderCustomView) VALUES (" + this.ID + "," + UserID + "," + val + ")";
                            //this.Network.Connection.Execute(sql);
                            loadFromPostBack = true;
                        }
                    }
                    if (!loadFromPostBack)
                    {
                        if (_ViewOrder == ViewOrders.Undefined)
                        {
                            _ViewOrder = ViewOrders.CreationDateDesc;
                          
                            var viewFound = this.FolderUsersCustomViews.Where(x => x.User.ID == UserID);
                            if (viewFound.Count() > 0) {
                                FoldersUsersCustomView customView = viewFound.First();
                                _ViewOrder = customView.ViewOrder;
                            }
                        }
                    }
                }
                else _ViewOrder = ViewOrders.CreationDateDesc;

                return _ViewOrder;
            }
        }

        #endregion

        #region RssManager

        private RssManager _RssManager;
        public virtual RssManager RssManager
        {
            get
            {
                if (_RssManager == null)
                    _RssManager = BuildRssManager();

                return _RssManager;
            }
        }
        protected virtual RssManager BuildRssManager()
        {
            return new RssManager("http://" + HttpContext.Current.Request.Url.Authority + this.FrontendHyperlink, this.Label, this.Descrizione, this);
        }
        public virtual void BuildRssRecursive()
        {
            RssManager.SaveXML(RssManager.RecourseSide.Bottom);
        }

        #endregion

        #endregion
         


        #region Grants

        
        //private bool enableGrantsElab;
        //public bool EnableGrantsElab
        //{
        //    get
        //    {
        //        return enableGrantsElab && (this.Parent == null ? true : this.Parent.EnableGrantsElab);
        //    }
        //    private set { enableGrantsElab = value; }
        //}

        public virtual bool EnableGrantsElab
        {
            get
            {
                bool result = Disable == 0;
                if (this.Parent != null)
                {
                    StructureFolder parentfolder = FolderBusinessLogic.GetById(this.Parent.ID);
                    result = Disable == 0 && (this.Parent == null ? true : parentfolder.EnableGrantsElab);
                }
               
              //  return Disable == 0 && (this.Parent == null ? true : parentfolder.EnableGrantsElab);
             //return Disable == 0 && (this.Parent == null ? true : (this.Parent as StructureFolder).EnableGrantsElab);
                return result;

            }
        }

        /*
        public CriteriaCollection Criteria
        {
            get
            {
                if (_Criteria == null)
                    initCriteria();
                return _Criteria;
            }
        }
        private CriteriaCollection _Criteria;

        public int[] FolderUserProfiles
        {
            get
            {
                if (_FolderUserProfiles == null)
                {
                    _FolderUserProfiles = initFolderUserProfiles();
                }
                return _FolderUserProfiles;
            }
        }
        private int[] _FolderUserProfiles;
        public int[] initFolderUserProfiles()
        {
            int[] array;
            DataTable tab = null;
            if (HttpContext.Current.Session["FolderProfilesTable"] == null)
                HttpContext.Current.Session["FolderProfilesTable"] = tab = this.Network.Connection.SqlQuery("SELECT * FROM FolderProfiles");
            else
            {
                tab = (DataTable)HttpContext.Current.Session["FolderProfilesTable"];
            }
            DataRow[] rows = tab.Select(" Folder_FolderProfile = " + this.ID);
            array = new int[rows.Length];
            for (int i = 0; i < rows.Length; i++)
                array[i] = int.Parse(rows[i]["id_FolderProfile"].ToString());

            return array;

        }

        private bool Grant_loaded;//variabile che indica se la proprietà show è stata già letta.
        private bool Grant_backstore;//variabile che contiene il valore restituito dalla proprietà Show 
        public bool Grant
        {
            get
            {
                if(!Account.NetworkEnabledForUser(this.Network.ID)) return false;
                if (!Grant_loaded)
                {
                    Grant_loaded = true;
                    if (this.EnableGrantsElab)
                        Grant_backstore = Criteria["show"].Value == 1;
                    else Grant_backstore = Parent == null ? false : ((StructureFolder)this.Parent).Grant;

                }
                return Grant_backstore;
            }
        }

        private bool Show_loaded;//variabile che indica se la proprietà show è stata già letta.
        private bool Show_backstore;//variabile che contiene il valore restituito dalla proprietà Show
        public bool Show
        {
            get
            {
                if (!Show_loaded)
                {
                    Show_loaded = true;
                    if (Grant) Show_backstore = true;
                    else
                    {
                        int i = 0;
                        while (!Show_backstore && i < Folders.Count)
                            Show_backstore = Show_backstore || ((StructureFolder)Folders[i++]).Show;
                    }
                }

                return Show_backstore;
            }
        }
        */
        private NetCms.Users.User _ReferedAccount;
        public virtual NetCms.Users.User ReferedAccount
        {
            get
            {

                if (_ReferedAccount == null && this.Parent != null)
                    _ReferedAccount = (Parent as StructureFolder).ReferedAccount;
                return _ReferedAccount;
            }
            set
            {
                _ReferedAccount = value;
            }
        }

        private void initCriteria()
        {
            //NetCms.Structure.Grants.FolderGrantsFinder finder = new NetCms.Structure.Grants.FolderGrantsFinder(ReferedAccount);
            //_Criteria = finder.Search(this);
        }


        public virtual Users.Group Groups
        {
            get
            {
                if (_Groups == null)
                    initGroups();
                return _Groups;
            }
        }
        private Users.Group _Groups;
        private void initGroups()
        {
            _Groups = new NetCms.Structure.RevisorsLoader(PageData, this).getRootGroup();
        }

        #endregion

        #region Metodi
        
        // DA SOSTITUIRE CON CACHE 2° LIVELLO NHIBERNATE
        //public void InvalidateDocs()
        //{

        //    this.Documents.Invalidate();
        //}

        //public void InvalidateFolders()
        //{
        //    this.Folders.Invalidate();
        //}

        public virtual void Invalidate()
        {
            //this.OwnerCollection.DataTableManager.Invalidate();
            //InvalidateDocs();
            //this.InvalidateFolders();

            //if (this.ID == this.Network.RootFolder.ID)
            //    this.Network.Invalidate();
            //else if (this.OwnerCollection != null)
            //    this.OwnerCollection.GetCacheableItem(this.ID).Invalidate();

            G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendMenu", this.StructureNetwork.SystemName);

            //NetCms.DBSessionProvider.FluentSessionProvider.Instance.GetSessionFactory().EvictEntity("NetCms.Networks.WebFS.NetworkFolder.Folders");
            //NetCms.DBSessionProvider.FluentSessionProvider.Instance.GetSessionFactory().EvictEntity("NetCms.Networks.WebFS.NetworkFolder.Documents");

           // NetCms.DBSessionProvider.FluentSessionProvider.Instance.GetSessionFactory().EvictEntity("NetCms.Structure.WebFS.StructureFolder", this.ID);
        }

        protected void getBackRow(StructureTable table)
        {
            HtmlGenericControl tr = new HtmlGenericControl("tr");
            table.addTableFooter(tr);

            HtmlGenericControl td = new HtmlGenericControl("td");
            if (Parent != null)
            {
                tr = new HtmlGenericControl("tr");
                table.addTableFooter(tr);

                td = new HtmlGenericControl("td");
                td.Attributes["class"] = "backrow";
                td.InnerHtml = "<a href=\"default.aspx?folder=" + Parent.ID + "\"> Torna Indietro </a>";
                td.Attributes["colspan"] = "8";
                tr.Controls.Add(td);

            }
        }

        public virtual void addSearchResult(StructureTable table, StandardSearchFields pars)
        {
            StructureFolderSearch finder = new StructureFolderSearch(this, pars, PageData, table.RecordPerPagina, table.Page);
            finder.addSearchResult(table);
        }

        private string FsFolderName(string folderName)
        {
            string FileSystemName = folderName;
            FileSystemName = FileSystemName.Trim();
            FileSystemName = FileSystemName.ToLower();

            for (int i = 0; i < FileSystemName.Length; i++)
            {
                int integerValue;
                int convertedPw = Convert.ToInt32(FileSystemName[i]);
                if (!int.TryParse(FileSystemName[i].ToString(), out integerValue) && (convertedPw < 97 || convertedPw > 122))
                    FileSystemName = FileSystemName.Replace(FileSystemName[i], '-');
            }
            //Rimuovo i doppi trattini
            while (FileSystemName.Contains("--"))
                FileSystemName = FileSystemName.Replace("--", "-");

            //Rimuovo i trattini alla fine del nome
            while (FileSystemName.EndsWith("-"))
                FileSystemName = FileSystemName.Remove(FileSystemName.Length - 1);

            //Rimuovo i trattini all inizio del nome
            while (FileSystemName.StartsWith("-"))
                FileSystemName = FileSystemName.Substring(1, FileSystemName.Length - 1);

            //Rimuovo i punti alla fine del nome
            while (FileSystemName.EndsWith("."))
                FileSystemName = FileSystemName.Remove(FileSystemName.Length - 1);

            //Taglio il nome della cartella se è più lungo di 60 caratteri
            if (FileSystemName.Length > 60)
                FileSystemName = FileSystemName.Remove(60);

            while (FileSystemName.EndsWith("-"))
                FileSystemName = FileSystemName.Remove(FileSystemName.Length - 1);
            return FileSystemName;
        }

        public virtual bool MoveFolder(StructureFolder TargetFolder)
        {
            return MoveFolder(TargetFolder, TargetFolder.Path, TargetFolder.Tree, TargetFolder.Depth);
        }

        //private bool MoveFolder(string TargetFolderID, string TargetFolderPath, string TargetFolderTree, int TargetFolderDepth)
        //{
        // string FileSystemName = this.Nome;
        // FileSystemName = FileSystemName.Replace("'", "''");

        // while (FileSystemName.EndsWith("."))
        //     FileSystemName = FileSystemName.Remove(FileSystemName.Length - 1);

        // string NewVirtualPath = TargetFolderPath + "/" + FileSystemName;
        // if (CreatePhysicalFolder(NewVirtualPath))
        // {
        //     for (int i = 0; i < Documents.Count; i++)
        //         Documents.ElementAt(i).MoveDocPhysical(NewVirtualPath, PageData);
        // }
        // #region Sposto la default

        // string path = this.Network.Paths.AbsoluteFrontRoot + this.Path + "/default.aspx";
        // path = HttpContext.Current.Server.MapPath(path);
        // System.IO.FileInfo pagefile = new FileInfo(path);
        // if (pagefile.Exists)
        // {
        //     path = this.Network.Paths.AbsoluteFrontRoot + NewVirtualPath + "/default.aspx";
        //     path = HttpContext.Current.Server.MapPath(path);
        //     System.IO.FileInfo destfile = new FileInfo(path);
        //     if (!destfile.Exists)
        //         pagefile.MoveTo(path);
        // }
        // path = this.Network.Paths.AbsoluteFrontRoot + this.Path + "/default.aspx.cs";
        // path = HttpContext.Current.Server.MapPath(path);
        // System.IO.FileInfo codefile = new FileInfo(path);
        // if (codefile.Exists)
        // {
        //     path = this.Network.Paths.AbsoluteFrontRoot + NewVirtualPath + "/default.aspx.cs";
        //     path = HttpContext.Current.Server.MapPath(path);
        //     codefile.MoveTo(path);
        // }

        // #endregion


        // string NewTree = TargetFolderTree + "" + NetUtility.TreeUtility.FormatNumber(this.ID.ToString()) + ".";
        // string sql = "UPDATE Folders SET ";
        // sql += " Tree_Folder = '" + NewTree + "'";
        // sql += ",Path_Folder = '" + NewVirtualPath + "'";
        // sql += ",Parent_Folder = " + TargetFolderID + "";
        // sql += ",Depth_Folder = " + (TargetFolderDepth + 1) + "";
        // sql += " WHERE id_Folder = " + this.ID;

        // this.Network.Connection.Execute(sql);

        // for (int i = 0; i < this.SubFolders.Count; i++)
        // {
        //     SubFolders.ElementAt(i).MoveFolder(this.ID.ToString(), NewVirtualPath, NewTree, TargetFolderDepth + 1);
        // }

        //// this.Parent.Invalidate();
        //// this.Network.RootFolder.FindFolder(TargetFolderID).Invalidate();

        // return true;
        //}

        private bool MoveFolder(StructureFolder TargetFolder, string TargetFolderPath, string TargetFolderTree, int TargetFolderDepth)
        {
            string FileSystemName = this.Nome;
            FileSystemName = FileSystemName.Replace("'", "''");

            while (FileSystemName.EndsWith("."))
                FileSystemName = FileSystemName.Remove(FileSystemName.Length - 1);

            string NewVirtualPath = TargetFolderPath + "/" + FileSystemName;
            if (CreatePhysicalFolder(NewVirtualPath))
            {
                for (int i = 0; i < Documents.Count; i++)
                    (Documents.ElementAt(i) as Document).MoveDocPhysical(NewVirtualPath, PageData);
            }
            #region Sposto la default

             string path = this.StructureNetwork.Paths.AbsoluteFrontRoot + this.Path + "/default.aspx";
             path = HttpContext.Current.Server.MapPath(path);
             System.IO.FileInfo pagefile = new FileInfo(path);
             if (pagefile.Exists)
             {
                 path = this.StructureNetwork.Paths.AbsoluteFrontRoot + NewVirtualPath + "/default.aspx";
                 path = HttpContext.Current.Server.MapPath(path);
                 System.IO.FileInfo destfile = new FileInfo(path);
                 if (!destfile.Exists)
                     pagefile.MoveTo(path);
             }
             path = this.StructureNetwork.Paths.AbsoluteFrontRoot + this.Path + "/default.aspx.cs";
             path = HttpContext.Current.Server.MapPath(path);
             System.IO.FileInfo codefile = new FileInfo(path);
             if (codefile.Exists)
             {
                 path = this.StructureNetwork.Paths.AbsoluteFrontRoot + NewVirtualPath + "/default.aspx.cs";
                 path = HttpContext.Current.Server.MapPath(path);
                 codefile.MoveTo(path);
             }

            #endregion

            string NewTree = TargetFolderTree + "" + NetUtility.TreeUtility.FormatNumber(this.ID.ToString()) + ".";

            this.Tree = NewTree;
            this.Path = NewVirtualPath;
            this.Parent = TargetFolder;
            this.Depth = TargetFolderDepth + 1;

            FolderBusinessLogic.SaveOrUpdateFolder(this);

            for (int i = 0; i < this.SubFolders.Count; i++)
            {
                SubFolders.ElementAt(i).MoveFolder(this, NewVirtualPath, NewTree, TargetFolderDepth + 1);
            }

            // SISTEMARE CON CACHE 2° LIVELLO
            // this.Parent.Invalidate();
            // this.Network.RootFolder.FindFolder(TargetFolderID).Invalidate();

            return true;
        }

        //public abstract void CreateDefaultPage();
        public virtual void CreateDefaultPage(ISession innerSession = null)
        {
            try
            {
                string path = HttpContext.Current.Server.MapPath(this.StructureNetwork.Paths.AbsoluteFrontRoot + Path);
                if (!File.Exists(path) && !File.Exists(path + "/default.aspx"))
                    File.Create(path + "/default.aspx");

            }
            catch (Exception ex) { }
        }

        //public static string ResumeFolder(int id, PageData pagedata)
        //{
        //string result = "Si è verificato un errore e non è stato possibile ripristinare la Cartella";

        //DataTable table = Networks.NetworksManager.CurrentActiveNetwork.Connection.SqlQuery("SELECT * FROM Folders ");
        //DataRow[] rows = table.Select("id_Folder = " + id);
        //if (rows.Length == 1)
        //{
        //    DataRow row = rows[0];
        //    string ParentID = row["Parent_Folder"].ToString();
        //    string Path = row["Path_Folder"].ToString();
        //    string Tree = row["Tree_Folder"].ToString();
        //    string Nome = row["Name_Folder"].ToString();
        //    rows = table.Select("Trash_Folder = 0 AND Parent_Folder = '" + ParentID + "' AND Name_Folder = '" + Nome + "'");
        //    if (rows.Length == 0)//Ripristino la cartella
        //    {

        //        Networks.NetworksManager.CurrentActiveNetwork.Connection.Execute("UPDATE Folders SET Trash_Folder = 0 WHERE Tree_Folder LIKE '" + Tree + "%'");
        //        result = "Cartella ripristinata con successo";

        //        Networks.NetworksManager.CurrentActiveNetwork.RootFolder.FindFolder(ParentID).Invalidate();
        //    }
        //    else
        //        result = "Non è stato possibile ripristinare la cartella perchè all'interno della cartella di origine esiste già una cartella con il nome della cartella che si vuole ripristinare";

        //}

        //return result;

        //}

        public static string ResumeFolder(int id)
        {
            string result = "Si è verificato un errore e non è stato possibile ripristinare la Cartella";

            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    StructureFolder folder = FolderBusinessLogic.GetById(sess, id);
                    if (folder != null)
                    {
                        if (FolderBusinessLogic.GetCartellaNonCestinataByNome((folder.Parent as StructureFolder), folder.Nome, sess) == null)
                        {
                            IList<StructureFolder> foldersToUpdate = FolderBusinessLogic.FindFoldersBySubTree(folder.Tree, folder.NetworkID, sess);

                            //PER MIGLIORARE LE PERFORMANCE E' CONSIGLIABILE UTILIZZARE IL BATCHSIZE CHE AL MOMENTO NECESSITA LA CONFIGURAZIONE DI NHIBERNATE
                            //sess.SetBatchSize(5);

                            foreach (StructureFolder folderToUpdate in foldersToUpdate)
                            {
                                folderToUpdate.Trash = 0;
                                FolderBusinessLogic.SaveOrUpdateFolder(folderToUpdate, sess);
                            }
                            result = "Cartella ripristinata con successo";

                            // SISTEMARE CON CACHE 2° LIVELLO
                            //Networks.NetworksManager.CurrentActiveNetwork.RootFolder.FindFolder(ParentID).Invalidate();
                        }
                        else
                            result = "Non è stato possibile ripristinare la cartella perchè all'interno della cartella di origine esiste già una cartella con il nome della cartella che si vuole ripristinare";
                    }

                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }

                return result;
            }
        }

        public virtual bool ResumeDoc(int id)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    Document doc = FileSystemBusinessLogic.DocumentBusinessLogic.GetById(id, sess);
                    doc.Trash = 0;

                    FileSystemBusinessLogic.DocumentBusinessLogic.SaveOrUpdateDocument(doc, sess);
                    tx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    return false;
                }
            }
            //SISTEMARE CON CACHE 2°LIVELLO
            //this.Network.Connection.Execute("UPDATE Docs SET Trash_Doc = 0 WHERE id_Doc =" + id + "");

            //DataTable table = this.Network.Connection.SqlQuery("SELECT * FROM Docs ");
            //DataRow[] rows = table.Select("id_Doc = " + id);
            //if (rows.Length == 1)
            //{
            //    DataRow row = rows[0];
            //    string FolderID = row["Folder_Doc"].ToString();
            //    StructureFolder Folder = this.Network.RootFolder.FindFolder(FolderID);
            //    Folder.Invalidate();
            //}
            //return true;
        }

        public virtual StructureFolder CreateSystemFolder(string label, string Name, string Desc, int type)
        {
            return CreateFolder(label, Name, Desc, type, 1, "", 0, 1);
        }
        public virtual StructureFolder CreateHiddenFolder(string label, string Name, string Desc, int type)
        {
            return CreateFolder(label, Name, Desc, type, 3, "", 0, 2);
        }

        public virtual StructureFolder CreateFolder(string label, string Name, string Desc, int type)
        {
            return CreateFolder(label, Name, Desc, type, 1, "", 0);
        }
        public virtual StructureFolder CreateFolder(string label, string Name, string Desc, int type, int State, string cssClass)
        {
            return CreateFolder(label, Name, Desc, type, State, cssClass, 0);
        }
        public virtual StructureFolder CreateFolder(string label, string Name, string Desc, int type, int State, string cssClass, int RestrictedAreaID)
        {
            return CreateFolder(label, Name, Desc, type, State, cssClass, RestrictedAreaID, 0);
        }
        public virtual StructureFolder CreateFolder(string label, string Name, string Desc, int type, int State, string cssClass, int RestrictedAreaID, int IsSystemFolder)
        {
            return CreateFolder(label, Name, Desc, type, State, cssClass, 0, 0, RestrictedAreaID, IsSystemFolder, 0, 0, "", true);
        }
        
        public virtual StructureFolder CreateFolder(string label, string Name, string Desc, int type, int State, string cssClass, int rss, int layout, int RestrictedAreaID, int IsSystemFolder,int showInTopMenu, int showMenuContestuale, string template)
        {
            return CreateFolder(label, Name, Desc, type, State, cssClass, rss, layout, RestrictedAreaID, IsSystemFolder, showInTopMenu, showMenuContestuale, template, true);
        }

        public virtual StructureFolder CreateFolder(string label, string Name, string Desc, int type, int State, string cssClass, int rss, int layout, int RestrictedAreaID, int IsSystemFolder, int showInTopMenu, int showMenuContestuale, string template,  bool CreateDefaultPage)
        {
            Name = Name.Trim();
            if (label == null) label = Name;

            string FileSystemName = FsFolderName(Name);

            int NewFolderID;
            bool ValidFolderName = true;

            //Controllo che il nome della cartella non sia di sistema
            ValidFolderName = !NetCms.Configurations.Generics.SystemFolders.Contains(FileSystemName.ToLower());
            //Controllo che il nome della cartella non sia di il nome di una Portale
            IList<BasicNetwork> Networks = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.Select(x => x.Value).Where(x=>x.Enabled).ToList();//new NetCms.Networks.NetworksData();
            foreach (BasicNetwork network in Networks)
            {
                //Aggiunto il controllo su Nome_Network per mantenere un corretto funzionamento anche quando non è settata la Webroot
                //if (FileSystemName.ToLower() == network["Webroot_Network"].ToString().ToLower() || FileSystemName.ToLower() == network["Nome_Network"].ToString().ToLower())
                //    ValidFolderName = false;
                if (FileSystemName.ToLower() == network.RootPath.ToLower() || FileSystemName.ToLower() == network.SystemName.ToLower())
                    ValidFolderName = false;
            }

            //Controllo che la cartella non esista già tra le cartelle attive
            for (int i = 0; i < this.SubFolders.Count; i++)
                if (SubFolders.ElementAt(i).Nome == FileSystemName)
                    ValidFolderName = false;

            //Controllo che la cartella non esista già tra le cartelle nel cestino
            IList<StructureFolder> trashed = FileSystemBusinessLogic.FolderBusinessLogic.GetCartelleCestinateByPath(this.Path.Replace("'", "''") + "/" + FileSystemName, this.NetworkID);
            if (trashed.Count > 0)
                ValidFolderName = false;

            //Controllo che il nome o la descrizione non siano vuoti
            if (FileSystemName == "" || Desc == "")
                ValidFolderName = false;

            if (ValidFolderName && this.CreatePhysicalFolder(this.Path + "/" + FileSystemName))
            {


                using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = sess.BeginTransaction())
                {
                    try
                    {
                        #region Creo la folder
                        object[] parameters = { StructureNetwork.CacheKey };
                        Application application = ApplicationsPool.ActiveAssemblies[type.ToString()];
                        Type ApplicationFolderType = application.Assembly.GetType(application.Classes.Folder);
                        StructureFolder newFolder = (StructureFolder)System.Activator.CreateInstance(ApplicationFolderType, parameters);
                        //StructureFolder newFolder = new StructureFolder(this.NetworkKey);

                        BasicNetwork network = NetworksBusinessLogic.GetById(this.Network.ID, sess);

                        newFolder.Nome = FileSystemName;
                        newFolder.Tipo = type;
                        newFolder.Network = network;
                        newFolder.ShowInTopMenu = showInTopMenu;
                        newFolder.ShowMenu = showMenuContestuale; // esposizione del menu contestuale
                        network.NetworkFolders.Add(newFolder);
                        newFolder = FileSystemBusinessLogic.FolderBusinessLogic.SaveOrUpdateFolder(newFolder, sess);
                        NewFolderID = newFolder.ID;
                        #endregion

                        #region Aggiorno i dati della folder aggiungendo i dati del Tree e del path

                        newFolder.Tree = this.Tree + NetUtility.TreeUtility.FormatNumber(NewFolderID.ToString()) + ".";
                        newFolder.Parent = this;
                        newFolder.Depth = (this.Depth + 1);
                        newFolder.Stato = State;
                        newFolder.CssClass = cssClass;
                        newFolder.TitleOffset = 0;
                        newFolder.Path = this.Path + "/" + FileSystemName;
                        newFolder.Sistema = IsSystemFolder;
                        newFolder.Rss = rss;
                        newFolder.Layout = layout;
                        newFolder.Template = template; // page layout

                        NetUtility.RequestVariable hometype = new NetUtility.RequestVariable("HomeType_Folder", PageData.Request.Form);
                        if (hometype.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                            newFolder.HomeType = hometype.IntValue;

                        newFolder.RestrictedInt = RestrictedAreaID;

                        //FileSystemBusinessLogic.FolderBusinessLogic.SaveOrUpdateFolder(newFolder);
                        #endregion

                        #region Creo la Lingua

                        FolderLang folderLang = new FolderLang();
                        folderLang.Label = label; //Nome
                        folderLang.Lang = PageData.DefaultLang; //Lingua
                        folderLang.Descrizione = Desc;

                        folderLang.Folder = newFolder;
                        newFolder.FolderLangs.Add(folderLang);

                        FileSystemBusinessLogic.FolderBusinessLogic.SaveOrUpdateFolder(newFolder, sess);

                        #endregion

                        #region Gestisco lo stato
                        //SISTEMARE CON CACHE 2° LIVELLO
                        this.Invalidate();

                        #endregion

                        if (CreateDefaultPage) newFolder.CreateDefaultPage(sess);
                        tx.Commit();
                        return newFolder;
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        return null;
                    }
                }
            }
            else
                return null;

        }

        private bool CreatePhysicalFolder(string path)
        {
            DirectoryInfo folder;
            string phisical_path;

            phisical_path = HttpContext.Current.Server.MapPath(this.StructureNetwork.Paths.AbsoluteFrontRoot + path);
            folder = new DirectoryInfo(phisical_path);

            string adaptedAbsoluteWebRoot = this.StructureNetwork.Paths.AbsoluteFrontRoot.Length > 0 ? this.StructureNetwork.Paths.AbsoluteFrontRoot : "/";
            string phisical_AbsoluteWebRoot = HttpContext.Current.Server.MapPath(adaptedAbsoluteWebRoot);

            string[] paths = path.Split('/');
            string progressive_path = "\\";

            try
            {
                for (int i = 1; i < paths.Length; i++)
                {
                    progressive_path += paths[i] + '\\';
                    folder = new DirectoryInfo(phisical_AbsoluteWebRoot + progressive_path);
                    if (!folder.Exists) folder.Create();
                }
                if (!File.Exists(phisical_path + "\\default.aspx"))
                    File.Create(phisical_path + "\\default.aspx");
            }
            catch (Exception ex) { }

            return true;
        }

        #endregion

        #region Metodi usati solo nell'importazione


        public virtual StructureFolder CreateFolder(DataRow record, bool createDefaultPage)
        {
            int NewFolderID;
            string fileSystemName = FsFolderName(record["Name_Folder"].ToString());
            if (this.CreatePhysicalFolder(this.Path + "/" + fileSystemName))
            {
                using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = sess.BeginTransaction())
                {
                    try
                    {
                        //Recupero la folder parent
                        StructureFolder parentFolder = FolderBusinessLogic.GetById(sess, this.ID);

                        #region Creo la folder
                        object[] parameters = { StructureNetwork.CacheKey };
                        Application application = ApplicationsPool.ActiveAssemblies[record["Tipo_Folder"].ToString()];
                        Type ApplicationFolderType = application.Assembly.GetType(application.Classes.Folder);
                        StructureFolder newFolder = (StructureFolder)System.Activator.CreateInstance(ApplicationFolderType, parameters);
                        //StructureFolder newFolder = new StructureFolder(this.NetworkKey);
                        newFolder.Nome = fileSystemName;
                        newFolder.Tipo = int.Parse(record["Tipo_Folder"].ToString());
                        newFolder.Network = parentFolder.Network;
                        (parentFolder.Network as BasicNetwork).NetworkFolders.Add(newFolder);
                        newFolder = FileSystemBusinessLogic.FolderBusinessLogic.SaveOrUpdateFolder(newFolder, sess);
                        NewFolderID = newFolder.ID;
                        #endregion

                        #region Aggiorno i dati della folder aggiungendo i dati del Tree e del path

                        newFolder.Tree = parentFolder.Tree + NetUtility.TreeUtility.FormatNumber(NewFolderID.ToString()) + ".";
                        newFolder.Parent = parentFolder;
                        newFolder.Depth = (parentFolder.Depth + 1);
                        if (record.Table.Columns.Contains("Stato_Folder"))
                            newFolder.Stato = int.Parse(record["Stato_Folder"].ToString());
                        if (record.Table.Columns.Contains("CssClass_Folder"))
                            newFolder.CssClass = record["CssClass_Folder"].ToString();

                        if (record.Table.Columns.Contains("TitleOffset_Folder"))
                        {
                            int offset = 0;
                            if (int.TryParse(record["TitleOffset_Folder"].ToString(), out offset))
                                newFolder.TitleOffset = offset;
                        }

                        newFolder.Path = parentFolder.Path + "/" + fileSystemName;
                        if (record.Table.Columns.Contains("System_Folder"))
                            newFolder.Sistema = int.Parse(record["System_Folder"].ToString());
                        if (record.Table.Columns.Contains("Rss_Folder"))
                            newFolder.Rss = int.Parse(record["Rss_Folder"].ToString());
                        if (record.Table.Columns.Contains("Layout_Folder"))
                            newFolder.Layout = int.Parse(record["Layout_Folder"].ToString());
                        if (record.Table.Columns.Contains("HomeType_Folder"))
                            newFolder.HomeType = int.Parse(record["HomeType_Folder"].ToString());
                        if (record.Table.Columns.Contains("Order_Folder"))
                            newFolder.Order = int.Parse(record["Order_Folder"].ToString());

                        if (record.Table.Columns.Contains("ShowMenu_Folder"))
                            newFolder.Order = int.Parse(record["ShowMenu_Folder"].ToString());
                        if (record.Table.Columns.Contains("Template_Folder"))
                            newFolder.CssClass = record["Template_Folder"].ToString();

                        newFolder.RestrictedInt = 0;

                        //FileSystemBusinessLogic.FolderBusinessLogic.SaveOrUpdateFolder(newFolder);
                        #endregion

                        #region Creo la Lingua

                        FolderLang folderLang = new FolderLang();
                        if (record.Table.Columns.Contains("Label_FolderLang"))
                            folderLang.Label = record["Label_FolderLang"].ToString(); //Nome
                        folderLang.Lang = PageData.DefaultLang; //Lingua
                        if (record.Table.Columns.Contains("Desc_FolderLang"))
                            folderLang.Descrizione = record["Desc_FolderLang"].ToString();

                        folderLang.Folder = newFolder;
                        newFolder.FolderLangs.Add(folderLang);

                        FileSystemBusinessLogic.FolderBusinessLogic.SaveOrUpdateFolder(newFolder, sess);

                        #endregion

                        #region Gestisco lo stato
                        //SISTEMARE CON CACHE 2° LIVELLO
                        this.Invalidate();

                        #endregion

                        if (createDefaultPage) newFolder.CreateDefaultPage(sess);
                        tx.Commit();
                        return newFolder;
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        return null;
                    }
                }
                //    #region Creo la folder

                //    string sql = "INSERT INTO Folders";
                //    sql += " (Name_Folder) Values ('" + fileSystemName + "') ";

                //    int NewFolderID = this.Network.Connection.ExecuteInsert(sql);

                //    #endregion

                //    #region Aggiorno i dati della folder aggiungendo i dati del Tree e del path

                //    sql = "UPDATE Folders SET ";
                //    sql += " Tree_Folder = '" + this.Tree + NetUtility.TreeUtility.FormatNumber(NewFolderID.ToString()) + ".' ";
                //    sql += ", Parent_Folder = " + this.ID + " ";
                //    sql += ", Depth_Folder = " + (this.Depth + 1) + " ";
                //    if (record.Table.Columns.Contains("Stato_Folder"))
                //        sql += ", Stato_Folder =  " + record["Stato_Folder"].ToString();
                //    if (record.Table.Columns.Contains("CssClass_Folder"))
                //        sql += ", CssClass_Folder = '" + record["CssClass_Folder"].ToString() + "' ";
                //    if (record.Table.Columns.Contains("Tipo_Folder"))
                //        sql += ", Tipo_Folder = " + record["Tipo_Folder"].ToString() + " ";
                //    if (record.Table.Columns.Contains("TitleOffset_Folder"))
                //        sql += ", TitleOffset_Folder = " + ((record["TitleOffset_Folder"].ToString().Length==0)?"''":record["TitleOffset_Folder"].ToString());
                //    if (record.Table.Columns.Contains("Name_Folder"))
                //        sql += ", Path_Folder = '" + this.Path.Replace("'", "''") + "/" + fileSystemName + "' ";
                //    if (record.Table.Columns.Contains("System_Folder"))
                //        sql += ", System_Folder = " + record["System_Folder"].ToString();
                //    sql += ", Network_Folder = " + this.Network.ID + " ";
                //    if (record.Table.Columns.Contains("HomeType_Folder"))
                //        sql += ", HomeType_Folder = " + record["HomeType_Folder"].ToString() + " ";
                //    if (record.Table.Columns.Contains("Rss_Folder"))
                //        sql += ", Rss_Folder = " + record["Rss_Folder"].ToString() + " ";
                //    if (record.Table.Columns.Contains("Layout_Folder"))
                //        sql += ", Layout_Folder = " + record["Layout_Folder"].ToString() + " ";
                //    sql += ", Restricted_Folder = 0 ";
                //    sql += "WHERE id_Folder = " + NewFolderID + " ";

                //    this.Network.Connection.Open();
                //    this.Network.Connection.Execute(sql);
                //    this.Network.Connection.Close();

                //    #endregion

                //    #region Creo la Lingua

                //    sql = "INSERT INTO FoldersLang";
                //    sql += " (Label_FolderLang,Lang_FolderLang,Desc_FolderLang,Folder_FolderLang)";
                //    sql += " Values";
                //    sql += " (";
                //    sql += " '" + record["Label_FolderLang"].ToString().Replace("'", "''") + "'";//Nome
                //    sql += ", " + this.Network.DefaultLang + "";//Lingua
                //    sql += ", '" + record["Desc_FolderLang"].ToString().Replace("'", "''") + "'";//Descrizione
                //    sql += ", " + NewFolderID + "";//Folder
                //    sql += " )";

                //    this.Network.Connection.Open();
                //    this.Network.Connection.Execute(sql);
                //    this.Network.Connection.Close();

                //    #endregion

                //    #region Gestisco lo stato

                //    this.Invalidate();

                //    #endregion

                //    StructureFolder folder = this.SubFolders[NewFolderID.ToString()];
                //    if (folder == null)
                //    {
                //        var list = this.SubFolders.Cast<StructureFolder>().ToDictionary(x => x.ID, x => x);
                //        return null;
                //    }
                //    else
                //    {
                //        if (createDefaultPage) folder.CreateDefaultPage();
                //        return folder;
                //    }
            }
            else
                return null;
        }

        public virtual bool CreateDocumentFromImport(DataRow docRecord, string[] ExistentIDs, out string[] IDs, bool isRootDocsImport, Importer.Importer imp)
        {
            return CreateDocumentFromImport(docRecord, null, ExistentIDs, out IDs, isRootDocsImport, imp);
        }
        public virtual bool CreateDocumentFromImport(DataRow docRecord, string sourceFileName, string[] ExistentIDs, out string[] IDs, bool isRootDocsImport, Importer.Importer imp)
        {
            DataSet sourceDB = imp.SourceDatabase;

            IDs = new string[2];

            int docID = ExistentIDs[0] != null ? int.Parse(ExistentIDs[0]) : -1;
            int docLangID = ExistentIDs[1] != null ? int.Parse(ExistentIDs[1]) : -1;

            int oldIdDoc = -1;

            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    //Recupero la folder parent
                    StructureFolder containerFolder = FolderBusinessLogic.GetById(sess, this.ID);

                    User currentUser = UsersBusinessLogic.GetById(Users.AccountManager.CurrentAccount.ID,sess);

                    Application application = ApplicationsPool.ActiveAssemblies[docRecord["Application_Doc"].ToString()];

                    Document newDocument = null;
                    if (docID == -1)
                    {
                        object[] parameters = { StructureNetwork.CacheKey };
                        //Application application = ApplicationsPool.ActiveAssemblies[docRecord["Application_Doc"].ToString()];
                        Type ApplicationDocumentType = application.Assembly.GetType(application.Classes.Document);
                        newDocument = (Document)System.Activator.CreateInstance(ApplicationDocumentType, parameters);

                        if (docRecord.Table.Columns.Contains("Name_Doc") && docRecord["Name_Doc"].ToString().Length > 0)
                        {
                            string nome = docRecord["Name_Doc"].ToString();
                            newDocument.PhysicalName = nome.EndsWith(".aspx") ? NetCms.Structure.Utility.ImportContentFilter.ClearFileSystemName(nome.Remove(nome.Length - 4, 4)) + ".aspx" : nome;
                        }
                        if (docRecord.Table.Columns.Contains("Stato_Doc") && docRecord["Stato_Doc"].ToString().Length > 0)
                            newDocument.Stato = int.Parse(docRecord["Stato_Doc"].ToString());
                        if (docRecord.Table.Columns.Contains("Data_Doc") && docRecord["Data_Doc"].ToString().Length > 0)
                            newDocument.Data = DateTime.Parse(docRecord["Data_Doc"].ToString());
                        if (docRecord.Table.Columns.Contains("Modify_Doc"))
                        {
                            DateTime res;
                            if (DateTime.TryParse(docRecord["Modify_Doc"].ToString(), out res))
                                newDocument.Modify = res;
                        }
                        if (docRecord.Table.Columns.Contains("Autore_Doc"))
                        {
                            int autoreID;
                            int.TryParse(docRecord["Autore_Doc"].ToString(), out autoreID);

                            User authorByOldID = Users.UsersBusinessLogic.GetUserOldID(autoreID,sess);

                            if (authorByOldID != null)
                                newDocument.Autore = authorByOldID.ID;
                            else if (authorByOldID == null) 
                            {
                                User author = Users.UsersBusinessLogic.GetById(autoreID, sess);
                                if (author != null)
                                    newDocument.Autore = author.ID;
                                else
                                    newDocument.Autore = 1;
                            }
                            else
                                newDocument.Autore = 1;
                        }
                        if (docRecord.Table.Columns.Contains("Hide_Doc") && docRecord["Hide_Doc"].ToString().Length > 0)
                            newDocument.Hide = int.Parse(docRecord["Hide_Doc"].ToString());
                        if (docRecord.Table.Columns.Contains("CssClass_Doc") && docRecord["CssClass_Doc"].ToString().Length > 0)
                            newDocument.CssClass = docRecord["CssClass_Doc"].ToString();
                        if (docRecord.Table.Columns.Contains("Application_Doc") && docRecord["Application_Doc"].ToString().Length > 0)
                            newDocument.Application = int.Parse(docRecord["Application_Doc"].ToString());
                        if (docRecord.Table.Columns.Contains("TitleOffset_Doc"))
                        {
                            int res;
                            if (int.TryParse(docRecord["TitleOffset_Doc"].ToString(), out res))
                                newDocument.TitleOffset = res;
                            else
                                newDocument.TitleOffset = 0;
                        }
                        if (docRecord.Table.Columns.Contains("Trash_Doc") && docRecord["Trash_Doc"].ToString().Length > 0)
                            newDocument.Trash = int.Parse(docRecord["Trash_Doc"].ToString());
                        if (docRecord.Table.Columns.Contains("Create_Doc"))
                        {
                            DateTime res;
                            if (DateTime.TryParse(docRecord["Create_Doc"].ToString(), out res))
                                newDocument.Create = res;
                        }
                        if (docRecord.Table.Columns.Contains("LangPickMode_Doc") && docRecord["LangPickMode_Doc"].ToString().Length > 0)
                            newDocument.LangPickMode = int.Parse(docRecord["LangPickMode_Doc"].ToString());
                        if (docRecord.Table.Columns.Contains("Order_Doc") && docRecord["Order_Doc"].ToString().Length > 0)
                            newDocument.Order = int.Parse(docRecord["Order_Doc"].ToString());
                        if (docRecord.Table.Columns.Contains("Layout_Doc") && docRecord["Layout_Doc"].ToString().Length > 0)
                            newDocument.Layout = int.Parse(docRecord["Layout_Doc"].ToString());

                        newDocument.Folder = containerFolder;
                        containerFolder.Documents.Add(newDocument);

                        if ((newDocument.Application == 6 || newDocument.Application == 3 || newDocument.Application == 8 || newDocument.Application == 10 || newDocument.Application == 14) &&
                           (docRecord.Table.Columns.Contains("id_Doc") && docRecord["id_Doc"].ToString().Length > 0))
                            oldIdDoc = int.Parse(docRecord["id_Doc"].ToString());

                        //IDs[0] = docID.ToString();
                    }
                    else
                        newDocument = DocumentBusinessLogic.GetById(docID, sess);

                    //int DocContentID = (isRootDocsImport) ? CreateRootContent(docRecord, sourceDB, sourceFileName, imp) : CreateDocContent(docRecord, sourceDB, sourceFileName, imp);


                    DocContent docContent = null;
                    if (isRootDocsImport)
                    {
                        docContent = CreateRootContent(docRecord, sess, sourceDB, imp.SourceFolderPath, newDocument);
                    }
                    else
                    {

                        //object[] contentParameters = {  };
                        object[] contentImportParameters = { docRecord, sess, sourceDB, StructureNetwork.CacheKey,imp.SourceFolderPath, newDocument };
                        Type ApplicationContentType = application.Assembly.GetType(application.Classes.DocContent);

                        MethodInfo method = ApplicationContentType.GetMethod("ImportDocContent", new Type[] { typeof(DataRow), typeof(NHibernate.ISession), typeof(DataSet), typeof(NetworkKey), typeof(string), typeof(Document).MakeByRefType() });
                        if (method != null)
                        {
                            docContent = (DocContent)System.Activator.CreateInstance(ApplicationContentType, null);
                            docContent = method.Invoke(docContent, contentImportParameters) as DocContent;
                        }
                    }


                    DocumentLang docLang = null;

                    if (docLangID == -1)
                    {
                        docLang = new DocumentLang();
                        docLang.Document = newDocument;
                        newDocument.DocLangs.Add(docLang);
                        if (docRecord.Table.Columns.Contains("Label_DocLang"))
                            docLang.Label = docRecord["Label_DocLang"].ToString();
                        if (docRecord.Table.Columns.Contains("Lang_DocLang"))
                            docLang.Lang = int.Parse(docRecord["Lang_DocLang"].ToString());
                        if (docRecord.Table.Columns.Contains("Desc_DocLang"))
                            docLang.Descrizione = docRecord["Desc_DocLang"].ToString();

                        newDocument = DocumentBusinessLogic.SaveOrUpdateDocument(newDocument, sess);

                        DocumentBusinessLogic.SaveOrUpdateDocumentLang(docLang, sess);

                        //IDs[1] = docLangID.ToString();
                    }
                    else
                        docLang = DocumentBusinessLogic.GetDocLangById(docLangID, sess);

                    bool result = false;
                    int tryCount = 0;
                    do
                    {
                        try
                        {
                            result = ImportFile(docRecord, sourceFileName);
                        }
                        catch (System.IO.IOException ex)
                        {
                            System.Threading.Thread.Sleep(250);
                        }
                    }
                    while (!result && tryCount++ < 5);
                    if (!result) NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Impossibile importare il file '" + sourceFileName + "'", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");

                    //revisione
                    //int contenuto = 0;
                    //if (DocContentID == 0)
                    //    contenuto = docID;
                    //else
                    //    contenuto = DocContentID;

                    //Se il documento è una revisione e quindi ne contiene i record
                    if (docRecord.Table.Columns.Contains("Data_Revisione"))
                    {
                        //string date = Connection.FilterDateTime(docRecord["Data_Revisione"].ToString());
                        //string oldAuthor = docRecord["Autore_Revisione"].ToString();
                        //string newAuthor = Users.AccountManager.CurrentAccount.ID.ToString();
                        //DataTable newAuthorRelation = this.Connection.SqlQuery("SELECT * FROM import_users WHERE isFrontend = 0 AND UserLegacyID = " + oldAuthor);
                        //if (newAuthorRelation.Rows.Count > 0)
                        //    newAuthor = newAuthorRelation.Rows[0]["UserNewID"].ToString();

                        //string sql = "INSERT INTO Revisioni ";
                        //sql += " (DocLang_Revisione,Contenuto_Revisione,Data_Revisione,Autore_Revisione,Origine_Revisione,Published_Revisione) VALUES ";
                        //sql += " (" + docLangID + "," + contenuto + "," + date + "," + newAuthor + "," + docRecord["Origine_Revisione"].ToString() + "," + docRecord["Published_Revisione"].ToString() + ") ";
                        //this.Network.Connection.Execute(sql);

                        string publishedInfo = "";
                        if (docRecord.Table.Columns.Contains("Publicata_Revisione"))
                            publishedInfo = docRecord["Publicata_Revisione"].ToString();
                        if (docRecord.Table.Columns.Contains("Published_Revisione"))
                            publishedInfo = docRecord["Published_Revisione"].ToString();
                        
                        //Review revisione = new Review(Users.AccountManager.CurrentAccount);
                        Review revisione = new Review(currentUser);
                        revisione.DocLang = docLang;
                        revisione.Content = docContent;
                        revisione.Data = DateTime.Parse(docRecord["Data_Revisione"].ToString());

                        if (docRecord.Table.Columns.Contains("Autore_Revisione"))
                        {
                            //int idAuthorReview;
                            //if (int.TryParse(docRecord["Autore_Revisione"].ToString(), out idAuthorReview))
                            //    revisione.Autore = idAuthorReview;
                            //else
                            //    revisione.Autore = Users.AccountManager.CurrentAccount.ID;

                            int idAuthorReview;
                            int.TryParse(docRecord["Autore_Revisione"].ToString(), out idAuthorReview);

                            User authorByOldID = Users.UsersBusinessLogic.GetUserOldID(idAuthorReview, sess);

                            if (authorByOldID != null)
                                revisione.Autore = authorByOldID.ID;
                            else if (authorByOldID == null)
                            {
                                User author = Users.UsersBusinessLogic.GetById(idAuthorReview, sess);
                                if (author != null)
                                    revisione.Autore = author.ID;
                                else
                                    revisione.Autore = 1;
                            }
                            else
                                revisione.Autore = 1;
                        }
                        //revisione.Autore = Users.AccountManager.CurrentAccount.ID;
                        
                        revisione.Origine = int.Parse(docRecord["Origine_Revisione"].ToString());
                        revisione.Published = int.Parse(publishedInfo);

                        docLang.Revisioni.Add(revisione);
                        docContent.Revisione = revisione;

                        ReviewBusinessLogic.SaveOrUpdateReview(revisione, sess);


                        if (publishedInfo == "1")
                        {
                            docLang.Content = docContent;
                            docContent.DocLang = docLang;

                            DocumentBusinessLogic.SaveOrUpdateDocumentLang(docLang, sess);

                            //string sql_lang = "UPDATE Docs_Lang SET";
                            //sql_lang += " Content_DocLang = " + contenuto;
                            //sql_lang += " WHERE id_DocLang = " + docLangID;
                            //this.Network.Connection.Execute(sql_lang);
                        }
                    }
                    else
                    {
                        //Se l'applicazione supporta le revisioni
                        if (containerFolder.RelativeApplication.EnableReviews)
                        {
                            //Creazione della revisione iniziale nel caso non ci sia

                            //Review revisione = new Review(Users.AccountManager.CurrentAccount);
                            Review revisione = new Review(currentUser);
                            revisione.DocLang = docLang;
                            revisione.Content = docContent;
                            revisione.Data = DateTime.Now;
                            revisione.Autore = Users.AccountManager.CurrentAccount.ID;
                            revisione.Origine = 0;
                            revisione.Published = 1;


                            docLang.Content = docContent;
                            docContent.DocLang = docLang;

                            docLang.Revisioni.Add(revisione);
                            docContent.Revisione = revisione;

                            ReviewBusinessLogic.SaveOrUpdateReview(revisione, sess);

                        }
                        else
                        {
                            docLang.Content = docContent;
                            docContent.DocLang = docLang;

                            DocumentBusinessLogic.SaveOrUpdateDocumentLang(docLang, sess);
                        }
                    }

                    // add id histories to db 
                    if (oldIdDoc != -1 && newDocument != null && newDocument.ID > 0)
                    {
                        DocHistoryBusinessLogic.SaveDocHistory(oldIdDoc, newDocument.ID, newDocument.Application, sess);
                    }

                    tx.Commit();
                    IDs[0] = newDocument.ID.ToString();
                    IDs[1] = docLang.ID.ToString();

                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    return false;
                }
            }

        }



        
        public virtual ImportState CreateDocumentFromImport(ImportItem docData, ISession sess = null)
        {
           
            ImportState itemImportState = new ImportState();


            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    StructureFolder containerFolder = FolderBusinessLogic.GetById(sess, this.ID);
                    itemImportState.Tile = docData.Title;

                    NetworkDocument doc = containerFolder.FindDocByPhysicalName(NetCms.Structure.Utility.ImportContentFilter.ClearFileSystemName(docData.Title));
                    // check for duplicate
                    if (doc == null)
                    {

                        int appID = this.RelativeApplication.ID;
                        int langID = containerFolder.Network.DefaultLang;
                        
                        NetCms.Users.User currentUser = NetCms.Users.UsersBusinessLogic.GetById(Users.AccountManager.CurrentAccount.ID, sess);

                        //
                        Application application = ApplicationsPool.ActiveAssemblies[appID.ToString()];
                        object[] parameters = { StructureNetwork.CacheKey };

                        Document newDocument = null;

                        Type ApplicationDocumentType = application.Assembly.GetType(application.Classes.Document);
                        newDocument = (Document)System.Activator.CreateInstance(ApplicationDocumentType, parameters);

                        // Fill Document                
                        newDocument.PhysicalName = NetCms.Structure.Utility.ImportContentFilter.ClearFileSystemName(docData.Title);
                        newDocument.Stato = 1;
                        newDocument.Create = docData.Date;
                        newDocument.Data = docData.Date;
                        newDocument.Modify = docData.Date;
                        newDocument.Autore = currentUser.ID;
                        newDocument.Hide = 0;
                        newDocument.CssClass = "";
                        newDocument.Application = appID;
                        newDocument.TitleOffset = 0;
                        newDocument.Trash = 0;
                        newDocument.LangPickMode = 0;
                        newDocument.Order = 0;
                        newDocument.Layout = 0;
                        newDocument.Folder = containerFolder;
                        containerFolder.Documents.Add(newDocument);

                        // utilizzare la reflection x docContent
                        Dictionary<string, string> docContenDictionary = new Dictionary<string, string>();
                        
                        if (!string.IsNullOrEmpty(docData.Content))
                            docContenDictionary.Add("Testo", docData.Content);

                        if (docData.CustomFields != null)
                            docContenDictionary = docContenDictionary.Concat(docData.CustomFields).ToDictionary(x=>x.Key, x=>x.Value);

                        object[] contentImportParameters = { docContenDictionary, sess, };

                        DocContent docContent = null;

                        Type ApplicationContentType = application.Assembly.GetType(application.Classes.DocContent);

                        MethodInfo method = ApplicationContentType.GetMethod("ImportDocContent", new Type[] { typeof(Dictionary<string, string>), typeof(NHibernate.ISession) });
                        if (method != null)
                        {
                            docContent = (DocContent)System.Activator.CreateInstance(ApplicationContentType, null);
                            docContent = method.Invoke(docContent, contentImportParameters) as DocContent;
                        }

                        DocumentLang docLang = new DocumentLang();
                        docLang.Document = newDocument;
                        newDocument.DocLangs.Add(docLang);
                        docLang.Label = docData.Title;
                        docLang.Descrizione = docData.Description;
                        docLang.Lang = langID;
                       
                        docLang.Content = docContent;
                        docContent.DocLang = docLang;
                      
                        newDocument = DocumentBusinessLogic.SaveOrUpdateDocument(newDocument, sess);

                        DocumentBusinessLogic.SaveOrUpdateDocumentLang(docLang, sess);                        

                        Review revisione = new Review(currentUser);
                        revisione.DocLang = docLang;
                        revisione.Content = docContent;
                        revisione.Data = docData.Date;
                        revisione.Autore = Users.AccountManager.CurrentAccount.ID;
                        revisione.Origine = 0;
                        revisione.Published = 1;

                        docLang.Revisioni.Add(revisione);
                        docContent.Revisione = revisione;

                        ReviewBusinessLogic.SaveOrUpdateReview(revisione, sess);

                        if (docData.Attaches != null && docData.Attaches.Any())
                        {
                            // TODO: migliorare il recupero dell'assembly con un metodologia più efficiente
                          
                            Application fileApp = ApplicationsPool.ActiveAssemblies["4"]; // 0 è l'applicazione core del cms "files"

                            
                            Type AttachesBusinessLogic = fileApp.Assembly.GetType("NetCms.Structure.Applications.Files.AttachesBusinessLogic");

                            MethodInfo methodGetDestAttachFolder = AttachesBusinessLogic.GetMethod("GetFolderByDate",
                                new Type[] { typeof(int), typeof(DateTime) });

                            StructureFolder destinationAttachFolder = methodGetDestAttachFolder.Invoke(null, new object[] { this.NetworkID, docData.Date }) as StructureFolder;
                            string folderOriginPath = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + "/" + "repository/contents/imports");
                            string folderDestPath = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + "/" + destinationAttachFolder.Path + "/");

                            foreach (ItemAttach attach in docData.Attaches)
                            {
                                string filenameDest = GetUniqueFileName(attach.Filename, folderDestPath);

                                string fileOriging = folderOriginPath + "/" + (!String.IsNullOrEmpty(attach.OriginPath) ? attach.OriginPath + "/" : "") + attach.Filename;

                                if (File.Exists(fileOriging))
                                    System.IO.File.Copy(fileOriging, folderDestPath + filenameDest);
                                else
                                    Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Warning, "File " + fileOriging + " non è stato copiato in quanto non disponibile nella cartella di importazione");

                                MethodInfo methodInsertAttach = AttachesBusinessLogic.GetMethod("InsertAttach",
                                                           new Type[] { typeof(Document),
                                         typeof(DocContent),
                                         typeof(string),
                                         typeof(int),
                                         typeof(int),
                                         typeof(string),
                                         typeof(StructureFolder),
                                         typeof(NHibernate.ISession) });

                                if (methodInsertAttach != null)
                                {
                                    object[] attachParameter = {
                                        newDocument,
                                        docContent,
                                        filenameDest,
                                        this.NetworkID,
                                        currentUser.ID,
                                        folderDestPath,
                                        destinationAttachFolder,
                                        sess
                                        };
                                    
                                    methodInsertAttach.Invoke(null, attachParameter);
                                }

                            }
                        }

                        tx.Commit();
                        itemImportState.Status = ImportState.StatusEnums.OK;
                    }
                    else
                    {
                        // item skipped 
                        itemImportState.Status = ImportState.StatusEnums.Skipped;

                    }
                }
                catch(Exception ex)
                {
                    tx.Rollback();
                    itemImportState.Status = ImportState.StatusEnums.Error;
                    itemImportState.Message = ex.Message;
                }

                return itemImportState;
            }
        }

        //Questo metodo dovrebbe servire a creare nella root che è di tipo homepage, dei contenuti di applicazioni differenti. Al momento vale solo per i link!
        private DocContent CreateRootContent(DataRow docRecord, ISession session, DataSet sourceDB, string sourceFolderPath, Document newDoc)
        {
            DocContent rootDocContent = null;
            Application application = ApplicationsPool.ActiveAssemblies[docRecord["Application_Doc"].ToString()];
            object[] contentImportParameters = { docRecord, session, sourceDB, StructureNetwork.CacheKey, sourceFolderPath, newDoc };
            Type ApplicationContentType = application.Assembly.GetType(application.Classes.DocContent);

            //MethodInfo method = ApplicationContentType.GetMethod("ImportDocContent");
            MethodInfo method = ApplicationContentType.GetMethod("ImportDocContent", new Type[] { typeof(DataRow), typeof(NHibernate.ISession), typeof(DataSet), typeof(NetworkKey), typeof(string), typeof(Document).MakeByRefType() });
            if (method != null)
            {
                rootDocContent = (DocContent)System.Activator.CreateInstance(ApplicationContentType, null);
                rootDocContent = method.Invoke(rootDocContent, contentImportParameters) as DocContent;
            }
            return rootDocContent;

        }

        protected bool ImportFile(DataRow docRecord, string sourceFileName)
        {
            if (!string.IsNullOrEmpty(sourceFileName))
            {
                string folderPhysicalPath = HttpContext.Current.Server.MapPath(StructureNetwork.Paths.AbsoluteFrontRoot + this.Path);

                DirectoryInfo dir = new DirectoryInfo(folderPhysicalPath);

                string nome_file = "";
                switch (this.RelativeApplication.ID)
                {
                    case 4:
                        if (docRecord.Table.Columns.Contains("Link_DocFiles"))
                            nome_file = docRecord["Link_DocFiles"].ToString();
                        else
                            nome_file = docRecord["Name_Doc"].ToString();
                        break;
                    case 227: nome_file = docRecord["Link_AreaDiScambio"].ToString();
                        break;
                    default: nome_file = docRecord["Name_Doc"].ToString();
                        break;
                };
                nome_file = nome_file.EndsWith(".aspx") ? NetCms.Structure.Utility.ImportContentFilter.ClearFileSystemName(nome_file.Remove(nome_file.Length - 4, 4)) + ".aspx" : nome_file;
                FileInfo finfo = new FileInfo(folderPhysicalPath + "\\" + nome_file);
                if (sourceFileName.EndsWith("-empty") && !finfo.Exists)
                    File.Create(folderPhysicalPath + "\\" + nome_file);
                else
                    File.Copy(sourceFileName, folderPhysicalPath + "\\" + nome_file, true);
            }
            return true;
        }

        private string GetUniqueFileName(string fileName, string absolutePath)
        {
            int count = 1;
           
            string extension = System.IO.Path.GetExtension(absolutePath + fileName);
            string path = System.IO.Path.GetDirectoryName(absolutePath);
            string newFullPath = absolutePath + fileName;

            while (System.IO.File.Exists(newFullPath))
            {
                string tempFileName = string.Format("{0}({1})", fileName.Replace(extension,""), count++);
                newFullPath = System.IO.Path.Combine(path, tempFileName + extension);
            }

            return System.IO.Path.GetFileName(newFullPath);
        }

        #endregion

        #region Metodi dell'interfaccia IGrantsFindable
        
        public virtual GrantsFinder Criteria
        {
            get 
            {
                if (_Criteria == null)
                    _Criteria = new FolderGrants(this, NetCms.Users.AccountManager.CurrentAccount.Profile);
                return _Criteria;
            }
        }
        private GrantsFinder _Criteria;

        public virtual bool Grant
        {
            get 
            {
                return this.NeedVisualization;//this.Criteria[CriteriaTypes.Show].Allowed;
            }
        }

        public virtual bool NeedToShow
        {
            get 
            {
                return this.Criteria.NeedToShow("show").Allowed; //this.Criteria[CriteriaTypes.Show].Allowed;
            }
        }


        public virtual GrantsFinder CriteriaByProfile(Users.Profile profile)
        {
            return new FolderGrants(this, profile);
        }

        public virtual NetCms.Grants.GrantValue CriteriaForApplication(CriteriaTypes criteria, string appId)
        {
            try
            {
                string appKey = NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies[appId].SystemName;
                //ApplicationsTable.Select("id_Applicazione = " + applicationID)[0]["SystemName_Applicazione"].ToString();
                string criterio = NetCms.Grants.GrantsUtility.CriteriaSwitch(criteria) + "_" + appKey;
                return Criteria[criterio];
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Eccezione applicazione con id: " + appId + " - Messaggio: " + ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName);
                return new GrantValue(GrantsValues.NotSpecified);
            }
        }

//        public virtual DataTable CriteriaTable
//        {
//            get
//            {
//                if (_CriteriaTable == null)
//                    _CriteriaTable = this.Connection.SqlQuery(@"SELECT Key_Criterio,Tipo_Criterio,id_FolderProfile AS id_ObjectProfile,Folder_FolderProfile AS Object_ObjectProfile,Profile_FolderProfile AS Profile_ObjectProfile,Value_FolderProfileCriteria AS Value_ObjectProfileCriteria FROM folderprofilescriteria
//                     INNER JOIN folderprofiles ON id_FolderProfile = FolderProfile_FolderProfileCriteria
//                     INNER JOIN criteri ON id_Criterio = Criteria_FolderProfileCriteria");

//                return _CriteriaTable;
//            }
//        }
//        private DataTable _CriteriaTable;


//        public virtual DataTable GetGrantsTable(string associationID)
//        {
//            DataTable _GrantsTable = this.Connection.SqlQuery("SELECT * FROM folderprofilescriteria WHERE FolderProfile_FolderProfileCriteria = " + associationID);
//            return _GrantsTable;
//        }

        public virtual void DeleteGrantsForAssociation(int associationID)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {

                try
                {
                    FolderProfile FolderProfile = GrantsBusinessLogic.GetFolderProfileById(associationID, sess);
                    FolderProfile.CriteriaCollection.Clear();
                    GrantsBusinessLogic.SaveOrUpdateFolderProfile(FolderProfile, sess);

                    tx.Commit();
                    
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    
                }
            }


            //string sqlDeleteAssociatedCriteria = "DELETE FROM folderprofilescriteria WHERE FolderProfile_FolderProfileCriteria = " + associationID;
            //this.Connection.Execute(sqlDeleteAssociatedCriteria);
        }

        public virtual bool AddGrant(int associationID, int grantValue, int idCriterio, int creatorUserID)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                User creator = UsersBusinessLogic.GetById(creatorUserID, sess);
                try
                {
                    FolderProfile FolderProfile = GrantsBusinessLogic.GetFolderProfileById(associationID,sess);

                    FolderProfileCriteria folderProfileCriteria = new FolderProfileCriteria();
                    folderProfileCriteria.ObjectProfile = FolderProfile;
                    folderProfileCriteria.Criteria = GrantsBusinessLogic.GetCriterioById(idCriterio,sess);
                    folderProfileCriteria.Value = grantValue;
                    folderProfileCriteria.Application = 0;
                    folderProfileCriteria.Updater = creator;
                    FolderProfile.CriteriaCollection.Add(folderProfileCriteria);

                    GrantsBusinessLogic.SaveOrUpdateFolderProfile(FolderProfile, sess);

                    tx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    return false;
                }
            }


            //string sql = "INSERT INTO folderprofilescriteria(";
            //sql += " FolderProfile_FolderProfileCriteria ";
            //sql += ", Value_FolderProfileCriteria";
            //sql += ", Criteria_FolderProfileCriteria";
            //sql += ", Updater_FolderProfileCriteria";
            //sql += ", Application_FolderProfileCriteria";
            //sql += ") VALUES ";
            //sql += "(";
            //sql += " " + associationID + " ";
            //sql += ", " + grantValue + " ";
            //sql += ", " + idCriterio + " ";
            //sql += ", " + creatorUserID + " ";
            //sql += ", 0 ";
            //sql += ") ";

            //this.Connection.Execute(sql);

        }

        public virtual void DeleteGrantsForAssociation(int associationID, ISession session)
        {
            FolderProfile FolderProfile = GrantsBusinessLogic.GetFolderProfileById(associationID, session);
            FolderProfile.CriteriaCollection.Clear();
            GrantsBusinessLogic.SaveOrUpdateFolderProfile(FolderProfile, session);
        }

        public virtual void AddGrant(int associationID, int grantValue, int idCriterio, int creatorUserID, ISession session)
        {
            User creator = UsersBusinessLogic.GetById(creatorUserID, session);

            FolderProfile FolderProfile = GrantsBusinessLogic.GetFolderProfileById(associationID, session);

            FolderProfileCriteria folderProfileCriteria = new FolderProfileCriteria();
            folderProfileCriteria.ObjectProfile = FolderProfile;
            folderProfileCriteria.Criteria = GrantsBusinessLogic.GetCriterioById(idCriterio,session);
            folderProfileCriteria.Value = grantValue;
            folderProfileCriteria.Application = 0;
            folderProfileCriteria.Updater = creator;
            FolderProfile.CriteriaCollection.Add(folderProfileCriteria);

            GrantsBusinessLogic.SaveOrUpdateFolderProfile(FolderProfile, session);
        }

        public virtual GrantObjectType GrantObjectType
        {
            get { return GrantObjectType.Folder; }
        }

        public virtual ICollection<ObjectProfile> GrantObjectProfiles
        {
            get { return GrantsBusinessLogic.GetFolderProfiles(this.ID).ToList<ObjectProfile>(); }
        }

        public virtual ICollection<ObjectProfile> GetGrantObjectProfiles(ISession session)
        {
            return GrantsBusinessLogic.GetFolderProfiles(this.ID, session).ToList<ObjectProfile>();
        }

        public virtual bool CheckIfProfileIsAssociated(int profileId, ISession session = null)
        {
            return GrantsBusinessLogic.CheckIfProfileIsAssociatedToFolder(this.ID, profileId, session);
        }

        public virtual bool AddGrantProfile(int profileId)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                User creator = UsersBusinessLogic.GetById(NetCms.Users.AccountManager.CurrentAccount.ID, sess);
                try
                {
                    FolderProfile FolderProfile = new NetCms.Grants.FolderProfile();
                    FolderProfile.Profile = ProfileBusinessLogic.GetById(profileId,sess);
                    FolderProfile.WfsObject = FolderBusinessLogic.GetById(sess, this.ID);
                    FolderProfile.Creator = creator;

                    GrantsBusinessLogic.SaveOrUpdateFolderProfile(FolderProfile, sess);

                    tx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    return false;
                }
            }
        }

        public virtual bool DeleteGrantProfile(int objectProfileId)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    GrantsBusinessLogic.DeleteFolderProfile(objectProfileId, sess);
                    tx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    return false;
                }
            }
        }

        public virtual ObjectProfile AddGrantProfile(int profileId, ISession session)
        {
            User creator = UsersBusinessLogic.GetById(NetCms.Users.AccountManager.CurrentAccount.ID, session);
               
            FolderProfile FolderProfile = new NetCms.Grants.FolderProfile();
            FolderProfile.Profile = ProfileBusinessLogic.GetById(profileId, session);
            FolderProfile.WfsObject = this;
            FolderProfile.Creator = creator;

            return GrantsBusinessLogic.SaveOrUpdateFolderProfile(FolderProfile, session);
        }

        public virtual bool DeleteGrantProfile(int objectProfileId, ISession session)
        {
            try
            {
                GrantsBusinessLogic.DeleteFolderProfile(objectProfileId, session);
                return true;
            }
            catch (Exception ex)
            {
             
                return false;
            }
        }

        public virtual GrantsFinder CriteriaByProfileAndNetwork(Profile profile, NetworkKey networkKey)
        {
            throw new NotImplementedException();
        }

        public virtual bool CheckIfProfileIsAssociated(int profileId, int networkId, ISession session = null)
        {
            return GrantsBusinessLogic.CheckIfProfileIsAssociatedToFolder(this.ID, profileId, session);
        }

        public virtual bool AddGrantProfile(int profileId, int networkId)
        {
            return this.AddGrantProfile(profileId);
        }

        public virtual ObjectProfile AddGrantProfile(int profileId, int networkId, ISession session)
        {
            return this.AddGrantProfile(profileId, session);
        }
        #endregion

        public abstract bool DeleteModuloApplicativo(ISession session);

        public abstract bool DeleteSomething(ISession session);
     

        public enum HomeTypeFolder
        {
            [Description("Nome e Descrizione")]
            NomeDescrizione = 0,
            [Description("Indice contenuti")]
            IndiceContenuti = -1,
            [Description("Pagina Personalizzata")]      
            PaginaPersonalizzata = 1,
            [Description("Landing Page")]
            LandingPage = 2,
            [Description("Section Page")]
            SectionPage = 3,

        }
        
        //public enum TemplateFolderStartPageEnum
        //{    
        //    Table, // esposizione in modalità tabellare semplificata
        //    Grid, // esposizione in modalità griglia https://italia.github.io/design-web-toolkit/components/detail/layout--grid.html
        //    Masonry, // https://italia.github.io/design-web-toolkit/components/detail/layout--masonry.html
        //    News, // https://italia.github.io/design-web-toolkit/components/detail/layout--news.html
        //    List, // esposizione in modalità lista dd/dt o ul/li in funzione dell'implentazione prevista nel tpl
        //}
        
    }
}
