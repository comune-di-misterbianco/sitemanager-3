﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Users;

namespace NetCms.Structure.WebFS
{
    public class FoldersUsersCustomView
    {
        public FoldersUsersCustomView()
        { }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual StructureFolder Folder
        {
            get;
            set;
        }

        public virtual User User
        {
            get;
            set;
        }

        public virtual ViewOrders ViewOrder
        { 
            get; 
            set; 
        }
    }

    #region Enumeratori

    public enum ViewOrders { Undefined = 0, Label = 1, CreationDate = 2, ModifyDate = 3, Name = 4, ID = 5, LabelDesc = 6, CreationDateDesc = 7 }

    #endregion
}
