﻿//using System;
//using System.IO;
//using System.Web;
//using System.Data;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.HtmlControls;
//using NetCms.GUI;
//using NetCms.Structure.Grants;
//using NetForms;
//using System.Linq;
//using NetCms.Grants;

//namespace NetCms.Structure.WebFS
//{
//    public class StructureFolderGUI
//    {
//        public StructureFolder Folder
//        {
//            get { return _Folder; }
//            private set { _Folder = value; }
//        }
//        private StructureFolder _Folder;

//        protected virtual string BuildFolderLink(StructureFolder folder)
//        {
//            string details = "<strong>" + folder.Label + "</strong><br />";
//            details += "<p><em>" + folder.FolderLangs.ElementAt(0).Descrizione.Replace(@"
//", " ") + "</em></p>";//ATTENZIONE L'accapo soprastante non è un errore di battitura ma una necessità in quanto rappresenta il carattere accapo
//            details += "Percorso: " + folder.LabeledPath + "<br />";
//            details += "Tipo: " + folder.TypeName + "<br />";
//            int max = 10;
//            int tot = 0;

//            string Contents = "Contenuto: <br />";
//            int i;
//            for (i = 0; i < folder.SubFolders.Count && tot < max; i++)
//            {
//                StructureFolder currentSubFolder = folder.SubFolders.ElementAt(i);
//                if (!currentSubFolder.Hidden)
//                {
//                    tot++;
//                    Contents += "&nbsp;+&nbsp;" + currentSubFolder.Label + "<br />";
//                }
//            }
//            details += "Num. Sottocartelle: " + tot + "<br />";
//            for (i = 0; i < folder.Documents.Count && tot < max; i++, tot++)
//            {
//                Contents += "&nbsp;-&nbsp;" + folder.Documents.ElementAt(i).Nome + "<br />";
//            }

//            if (tot == 0)
//            {
//                Contents += "Cartella Vuota <br />";
//            }

//            if (tot >= max)
//            {
//                Contents += "...";
//            }
//            if (this.Folder.Criteria[CriteriaTypes.Advanced].Allowed)
//            {
//                details += "ClasseCss: " + folder.CssClass + "<br />";
//                details += "Titolo del Frontend : " + (folder.AddKeywordsToTitle ? "Keywords" : "Path") + "<br />";

//            }
//            details += "Num. Documenti: " + folder.Documents.Count + "<br />";

//            details += "Num. Documenti: " + folder.Documents.Count + "<br />";
//            details += Contents;

//            details = details.Replace("'", "\\\'");
//            details = details.Replace("\"", "&quot;");
//            //details = details.Replace("\"", "\'");

//            string title = folder.Label + " - ";
//            title += "Num. Documenti" + folder.Documents.Count + " - ";
//            title += "Num. Sottocartelle" + folder.SubFolders.Count + "<br />";

//            string image = "";
//            /*if ( Users.AccountManager.CurrentAccount.GUI.PreviewType == Users.SitemanagerUserGUI.PreviewTypes.Static)
//                image =NetCms.GUI.GuiUtility.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Folder_Popup);*/
//            string Link = "<a onmouseover=\"Over('" + image + "','" + details + "')\" onmouseout=\"Out()\" href=\"default.aspx?folder=" + folder.ID + "\">" + folder.Label + "</a>";

//            return Link;
//        }
			
//        public TableRow FolderRow
//        {
//            get
//            {
//                string Link = BuildFolderLink(Folder);
//                string Icon = "FS_Icon " + Folder.Icon;

//                if (Folder.IsSystemFolder)
//                    Icon += "_system";
//                else
//                    if (Folder.Restricted)
//                        Icon += "_reserved_area";

//                TableRow tr = new TableRow();

//                TableCell td = new TableCell();

//                td = new TableCell();
//                td.Controls.Add(new LiteralControl("&nbsp;"));
//                td.Attributes["class"] = Icon;
//                tr.Controls.Add(td);

//                td = new TableCell();
//                if (Folder.Restricted)
//                    td.Attributes["class"] = "reserved_area";
//                td.Controls.Add(new LiteralControl(Link));
//                tr.Controls.Add(td);

//                td = new TableCell();
//                td.Controls.Add(new LiteralControl("&nbsp;"));
//                tr.Controls.Add(td);

//                td = new TableCell();
//                td.Controls.Add(new LiteralControl("Cartella " + Folder.TypeName));
//                tr.Controls.Add(td);

//                td = new TableCell();
//                if (Folder.IsSystemFolder)
//                    td.Controls.Add(new LiteralControl("Cartella di sistema"));
//                else
//                    td.Controls.Add(new LiteralControl((Folder.StatusLabel)));
//                tr.Controls.Add(td);

//                if (Users.AccountManager.CurrentAccount.GUI.FoldersGUI == Users.SitemanagerUserGUI.FoldersGUIs.Separate)
//                {

//                    td = new TableCell();
//                    if (Folder.Grant && Folder.Criteria[CriteriaTypes.Grants].Allowed)//&& this.Account.IsAdministrator)
//                    {
//                        td.Attributes["class"] = "grant";
//                        td.Controls.Add(new LiteralControl("<a href=\"" + Folder.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/grants/grants.aspx?Folder=" + Folder.ID + "\"><span>Imposta Permessi</span></a>"));
//                    }
//                    else
//                        td.Controls.Add(new LiteralControl("&nbsp;"));
//                    tr.Controls.Add(td);

//                    td = new TableCell();

//                    if (!Folder.IsSystemFolder && Folder.Grant && Folder.Criteria[CriteriaTypes.Modify].Allowed)
//                    {
//                        td.Attributes["class"] = "modify modify_Folder";
//                        td.Controls.Add(new LiteralControl("<a href=\"" + Folder.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/Folder_mod.aspx?Folder=" + Folder.ID + "\"><span>Proprietà</span></a>"));
//                    }
//                    else
//                        td.Controls.Add(new LiteralControl("&nbsp;"));
//                    tr.Controls.Add(td);


//                    td = new TableCell();
//                    if (!Folder.IsSystemFolder && Folder.Grant && Folder.Criteria[CriteriaTypes.Delete].Allowed)
//                    {
//                        td.Attributes["class"] = "delete";
//                        td.Controls.Add(new LiteralControl("<a href=\"" + Folder.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/Folder_del.aspx?Folder=" + Folder.ID + "\"><span>Elimina</span></a>"));
//                    }
//                    else
//                        td.Controls.Add(new LiteralControl("&nbsp;"));
//                }
//                if (Users.AccountManager.CurrentAccount.GUI.FoldersGUI == Users.SitemanagerUserGUI.FoldersGUIs.Colapsed)
//                {
//                    td = new TableCell();
//                    td.Attributes["colspan"] = "3";
//                    td.Attributes["class"] = "ColapseActionCell";
//                    td.Controls.Add(new LiteralControl(""));

//                    string anchorModel = "<a class=\"ColapseActionLink {1}\" title=\"{2}\" onmouseover=\"setToolbarCaption('{0}')\" onmouseout=\"setToolbarCaption('')\" href=\"{3}\"><span>{0}<span></a>";

//                    string cssclass, href, label, title;
//                    if (Folder.EnableGrantsElab && Folder.Grant && Folder.Criteria[CriteriaTypes.Grants].Allowed)//&& this.Account.IsAdministrator)
//                    {
//                        label = "Gestisci Permessi Cartella";
//                        title = "Gestisci Permessi Cartella '" + Folder.Label + "'";
//                        cssclass = "ColapseActionLink_Grants";
//                        href = Folder.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/grants/grants.aspx?Folder=" + Folder.ID;
//                        td.Controls.Add(new LiteralControl(string.Format(anchorModel, label, cssclass, title, href)));
//                    }
//                    label = "Modifica Proprietà Cartella";
//                    title = "Modifica Proprietà Cartella '" + Folder.Label + "'";
//                    cssclass = "ColapseActionLink_Prop";
//                    href = Folder.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/Folder_mod.aspx?Folder=" + Folder.ID;
//                    td.Controls.Add(new LiteralControl(string.Format(anchorModel, label, cssclass, title, href)));

//                    td.Controls.Add(new LiteralControl("<a class=\"ColapseActionLink\"><span>&nbsp;</span></a>"));

//                    label = "Elimina Cartella";
//                    title = "Elimina Cartella '" + Folder.Label + "'";
//                    cssclass = "ColapseActionLink_Delete";
//                    href = Folder.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/Folder_del.aspx?Folder=" + Folder.ID;
//                    td.Controls.Add(new LiteralControl(string.Format(anchorModel, label, cssclass, title, href)));

//                    tr.Controls.Add(td);
//                }

//                tr.Controls.Add(td);

//                return tr;
//            }
//        }
//    }
//}
