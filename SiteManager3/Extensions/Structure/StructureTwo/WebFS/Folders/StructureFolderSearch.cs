﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Data;
using NetCms.GUI;
using System.Linq;

namespace NetCms.Structure.WebFS
{
    public abstract partial class StructureFolder : NetCms.Networks.WebFS.NetworkFolder
    {
        public class StructureFolderSearch
        {
            private StructureFolder Folder;
            private StructureFolder.StandardSearchFields Parameters;
            private PageData PageData;
            private int RPP;
            private int pageNumber;

            #region Costruttore

            public StructureFolderSearch(StructureFolder folder, StructureFolder.StandardSearchFields parameters, PageData pageData, int rpp, int pageNum)
            {
                this.Folder = folder;
                this.Parameters = parameters;
                this.PageData = pageData;
                this.RPP = rpp;
                this.pageNumber = pageNum;
            }

            #endregion

            //private string BuildQuery()
            //{
            //    string sql = "SELECT * FROM Folders INNER JOIN Docs ON id_Folder = Folder_Doc INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang WHERE Lang_DocLang = " + PageData.DefaultLang + " AND System_Folder < 2 AND Tree_Folder LIKE  '" + this.Folder.Tree + "%' AND Trash_Doc = 0" + NetCms.Structure.Applications.ApplicationsPool.ActivesAppzSqlFilter("Tipo_Folder");
            //    if (Parameters.Titolo != null && Parameters.Titolo.Length > 0)
            //        sql += " AND (Name_Doc LIKE '%" + Parameters.Titolo + "%' OR Label_DocLang LIKE '%" + Parameters.Titolo + "%')";
            //    if (Parameters.Tipo > 0)
            //        sql += " AND Application_Doc  = " + Parameters.Tipo + "";
            //    if (Parameters.Data != null && Parameters.Data.Length > 5)
            //    {
            //        DateTime SearchDate;

            //        if (DateTime.TryParse(Parameters.Data, out SearchDate))
            //            switch (Parameters.TipoData)
            //            {
            //                case 0:
            //                    sql += " AND Data_Doc  = " + PageData.Conn.FilterDateTime(SearchDate.ToString()) + "";

            //                    break;
            //                case 1:
            //                    sql += " AND Data_Doc  <= " + PageData.Conn.FilterDateTime(SearchDate.ToString()) + "";

            //                    break;
            //                case 2:
            //                    sql += " AND Data_Doc  >= " + PageData.Conn.FilterDateTime(SearchDate.ToString()) + "";

            //                    break;
            //            }
            //    }
            //    if (Parameters.OrderBy > 0)
            //        switch (Parameters.OrderBy)
            //        {
            //            case 0:
            //                sql += " ORDER BY Label_DocLang";
            //                break;
            //            case 1:
            //                sql += " ORDER BY Path_Folder,Label_DocLang";
            //                break;
            //            case 2:
            //                sql += " ORDER BY Application_Doc,Label_DocLang";
            //                break;
            //            case 3:
            //                sql += " ORDER BY Data_Doc,Label_DocLang";
            //                break;
            //            case 4:
            //                sql += " ORDER BY Data_Doc DESC,Label_DocLang";
            //                break;
            //            default:
            //                sql += " ORDER BY Label_DocLang";
            //                break;
            //        }
            //    else
            //        sql += " ORDER BY Label_DocLang";
            //    return sql;

            //    // SISTEMARE CON DAO
            //    throw new NotImplementedException();
            //}

            //private string _SqlQuery;
            //private string SqlQuery
            //{
            //    get
            //    {
            //        if (_SqlQuery == null)
            //            _SqlQuery = BuildQuery();
            //        return _SqlQuery;
            //    }
            //}

            private List<Document> _Documents;
            private List<Document> Documents
            {
                get
                {
                    if (_Documents == null)
                    {
                        //if (PageData.Session[SearchCacheQueryKey] != null)
                        //{
                        //    string CachedQuery = PageData.Session[SearchCacheQueryKey].ToString();
                        //    if (SqlQuery.ToLower() == CachedQuery.ToLower())
                        //        if (PageData.Session[SearchCacheDocsKey] != null)
                        //            _Documents =( List<Document>) PageData.Session[SearchCacheDocsKey];

                        //}
                        //if (_Documents == null)
                        //{
                            //_Documents = new List<Document>();

                            //DataTable dataTable = PageData.Conn.SqlQuery(SqlQuery);

                            //for (int i = 0; i < dataTable.Rows.Count; i++)
                            //{
                            //    Document docu = this.Folder.FindDoc(dataTable.Rows[i]["id_Doc"].ToString());
                            //    if (docu != null)
                            //        Documents.Add(docu);
                            //}

                            //PageData.Session[SearchCacheQueryKey] = SqlQuery;
                            //PageData.Session[SearchCacheDocsKey] = Documents;
                            _Documents = new List<Document>();
                                
                            IList<Document> filteredDocs = FileSystemBusinessLogic.DocumentBusinessLogic.FindFolderDocumentsForSearchPaged(pageNumber,RPP,PageData.DefaultLang, this.Folder.Tree, NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies.Cast<NetCms.Structure.Applications.Application>().Select(x => x.ID).ToList<int>(), Parameters);

                            foreach (Document doc in filteredDocs)
                            {
                                Document docu = this.Folder.FindDocByID(doc.ID) as Document;
                                if (docu != null)
                                    Documents.Add(docu);
                            }

                        //}
                    }
                    return _Documents;
                }
            }

            private const string SearchCacheQueryKey = "SearchCacheQuery";
            private const string SearchCacheDocsKey = "SearchCacheDocs";

            public void addSearchResult(StructureTable Table)
            {
                Table.RowCount = FileSystemBusinessLogic.DocumentBusinessLogic.FindFolderDocumentsForSearchCount(PageData.DefaultLang, this.Folder.Tree, NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies.Cast<NetCms.Structure.Applications.Application>().Select(x => x.ID).ToList<int>(), Parameters);
                for (int i = 0; i < Documents.Count; i++)
                {
                    Document doc = Documents[i];
                    if (doc.Grant)
                        Table.addRow(doc.getRowView(true));
                }
            }
        }
    }
}
