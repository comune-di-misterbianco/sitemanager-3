﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Linq;

namespace NetCms.Structure.WebFS
{
    public abstract partial class StructureFolder : NetCms.Networks.WebFS.NetworkFolder
    {
        public class Serialization
        {
            public StructureFolder Folder
            {
                get { return _Folder; }
                private set { _Folder = value; }
            }
            private StructureFolder _Folder;

            public bool ShowInTree
            {
                get
                {
                    //Se la cartella non è di sistema, non è nascosta, e la stored procedure di visualizzazione restituisce true, devo mostrare la cartella
                    bool grantNeedView = this.Folder.NeedVisualization;
                    bool show = (!this.Folder.IsSystemFolder && !this.Folder.Hidden && grantNeedView);
                    //Se la prima analisi ha restituito false, proseguo a controllare se ho i permessi e la cartella è imghome
                    if (!show)
                    {
                        show = grantNeedView && this.Folder.Nome.ToLower() == "imghome";
                        //Se ancora è false, controllo le sottocartelle
                        if (!show)
                            show = HasChildForCurrentUser;
                    }
                    return show;
                    //return (this.Folder.Grant && ((!this.Folder.IsSystemFolder && !this.Folder.Hidden && this.Folder.NeedVisualization) || this.Folder.Nome.ToLower() == "imghome") || HasChildForCurrentUser);
                }
            }

            public Serialization(StructureFolder folder)
            {
                this.Folder = folder;
            }

            public bool HasChildForCurrentUser
            {
                get
                {
                    //Controllo se almeno una delle sottocartelle non è cestinata, non è di sistema, non è nascosta e deve essere mostrara
                    return Folder.SubFolders.Any(x => x.Trash == 0 && !x.IsSystemFolder && !x.Hidden && x.NeedVisualization);
                    //Second-OLD
                    //return !Folder.IsSystemFolder && !Folder.Hidden && Folder.SubFolders.Count(x=>x.Trash == 0) > 0 && Folder.NeedVisualization;
                    
                    //First-OLD
                    //bool ok = false;

                    //foreach (StructureFolder child in Folder.SubFolders)
                    //    ok = ok || child.Serialized.HasChildForCurrentUser || (!child.IsSystemFolder && !child.Hidden && child.Grant);

                    //return ok;
                }
            }

            public string JsonNodeCode
            {
                get
                {

                    return GetJsonNodeCode(0, Folder.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/default.aspx?folder={0}", true, false);
                }
            }
            public string JsonChildsCode
            {
                get { return GetJsonChildsCode(0, Folder.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/default.aspx?folder={0}", false, false); }
            }

            public string GetJsonNodeCode(int FolderType, string Link, bool addTrash, bool addCheckboxes, string onClick = "")
            {
                string code = "";
                string model = @"""{0}"": ""{1}""";
                string model2 = @"""{0}"": {1}";
                code += "{";
                string label = this.Folder.Label;
                StructureFolder CurrentFolder = this.Folder.StructureNetwork.CurrentFolder;
                if (CurrentFolder != null && CurrentFolder.ID == this.Folder.ID)
                    label = "<strong>" + label + "</strong>";

                string checkbox = "";
                if (addCheckboxes)
                    checkbox = "<input type=\\\"checkbox\\\" id=\\\"check" + this.Folder.ID + "\\\" name=\\\"check" + this.Folder.ID + "\\\" />";
                code += string.Format(model, "text", checkbox + "<a href=\\\"" + Link.Replace("{0}", this.Folder.ID.ToString()) + "\\\"" + ((onClick.Length > 0) ? " onclick=\\\"" + onClick.Replace("{0}", this.Folder.ID.ToString()).Replace("{1}", "'" + this.Folder.Label.Replace("'", "\\\\'").Replace("\"", "&quot;") + "'").Replace("{2}", "'" + this.Folder.TypeName + "'").Replace("{3}", this.Folder.SubFolders.Count(x => x.Trash == 0).ToString()).Replace("{4}", this.Folder.Documents.Cast<Document>().Count(x => x.AddDoc && x.Trash == 0).ToString()) + "\\\"" : "") + ">" + label.Replace("\"", "&quot;") + "</a>");
                code += ",\n";
                code += string.Format(model, "classes", "FATN FATN_" + this.Folder.ID + (this.Folder.CssClass != null && this.Folder.CssClass.Length > 0 ? " " + this.Folder.CssClass : ""));
                code += ",\n";
                if (this.Folder.Parent == null || (CurrentFolder != null && CurrentFolder.Path.Contains(this.Folder.Path)))
                {
                    code += string.Format(model2, "children", GetJsonChildsCode(FolderType, Link, addTrash, addCheckboxes, onClick));
                    code += ",\n";
                    code += string.Format(model2, "expanded", "true");
                    code += ",\n";
                }
                else
                {
                    code += string.Format(model2, "hasChildren", HasChildForCurrentUser ? "true" : "false");
                    code += ",\n";
                }
                code += string.Format(model, "id", "FATN_" + this.Folder.ID);
                //code += ",\n";
                code += "}";

                return code;
            }
            public string GetJsonChildsCode(int FolderType, string Link, bool addTrash, bool addCheckboxes, string onClick = "")
            {
                string code = "[";

                foreach (StructureFolder child in this.Folder.SubFolders)
                {
                    //if (((!child.IsSystemFolder && !child.Hidden)||child.Nome.ToLower() =="imghome" )&& child.Grant)
                    if (child.Serialized.ShowInTree && (FolderType == 0 || FolderType == child.Tipo))
                        code += (code.Length > 2 ? "," : "") + child.Serialized.GetJsonNodeCode(FolderType, Link, addTrash, addCheckboxes, onClick);
                }
                if (this.Folder.Parent == null && addTrash)
                {
                    if (code.Length > 1)
                        code += ",";
                    code += "{";
                    code += "\"text\": \"<a href=\\\"" + this.Folder.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/trash.aspx\\\">Cestino</a>\"";
                    code += ",\n";
                    code += "\"classes\": \"FATN FATN_TRASH\"";
                    code += ",\n";
                    code += "\"id\": \"FATN_TRASH\"";
                    code += "}";
                }
                code += "]";

                return code;
            }

            public string JsonFormNodeCode
            {
                get
                {

                    return GetJsonNodeCode(0, Folder.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/default.aspx?folder={0}", false, true);
                }
            }
            public string JsonFormChildsCode
            {
                get { return GetJsonChildsCode(0, Folder.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/default.aspx?folder={0}", false, true); }
            }
        }
    }
}
