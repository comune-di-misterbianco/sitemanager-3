﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.WebFS
{
    public class FolderLang
    {
        public FolderLang()
        {
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Label
        {
            get;
            set;
        }

        public virtual string Descrizione
        {
            get;
            set;
        }

        public virtual int Lang
        {
            get;
            set;
        }

        public virtual StructureFolder Folder
        {
            get;
            set;
        }
    }
}
