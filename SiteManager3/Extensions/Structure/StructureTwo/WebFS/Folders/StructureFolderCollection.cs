﻿//using System;
//using System.Web;
//using System.Web.Caching;
//using System.Collections.Generic;
//using System.Data;
//using System.Text;
//using NetCms.Connections;

//namespace NetCms.Structure.WebFS
//{
//    public class StructureFolderCollection : NetCms.Networks.WebFS.NetworkFoldersCollection
//    {
//        protected override G2Core.Caching.CacheableItem BuildElement(DataRow recordData, int index, G2Core.Caching.CacheableCollection ownerCollection)
//        {
//            return new CacheableItem(this.NetworkKey, recordData, index, ownerCollection);
//        }

//        private Networks.NetworkKey _NetworkKey;
//        public Networks.NetworkKey NetworkKey
//        {
//            get
//            {
//                return _NetworkKey;
//            }
//            private set { _NetworkKey = value; }
//        }

//        public StructureFolderCollection(Networks.NetworkKey networkKey, G2Core.Caching.Visibility.CachingVisibility Visibility, NetCms.Connections.Connection conn)
//            : base(Visibility, conn)
//        {
//            NetworkKey = networkKey;
//        }

//        public enum FoldersOrdersDirections { Default, ASC, DESC }

//        protected string FolderID
//        {
//            get { return _FolderID; }
//            private set { _FolderID = value; }
//        }
//        private string _FolderID;

//        public bool AddTrashedFolders
//        {
//            get { return _AddTrashedFolders; }
//            private set { _AddTrashedFolders = value; }
//        }
//        private bool _AddTrashedFolders;

//        public new NetCms.Structure.WebFS.StructureFolder this[int i]
//        {
//            get
//            {
//                return (NetCms.Structure.WebFS.StructureFolder)base[i];
//            }
//        }

//        public new NetCms.Structure.WebFS.StructureFolder this[string key]
//        {
//            get
//            {
//                return (NetCms.Structure.WebFS.StructureFolder)base[key];
//            }
//        }

//        public void FillWithAllFolders()
//        {
//            base.FillCollection("SELECT * FROM Folders WHERE true " + NetCms.Structure.Applications.ApplicationsPool.ActivesAppzSqlFilter("Tipo_Folder"), CommandType.Text, null);
//        }

//        protected void FillByQuery(StructureFolder.ViewOrders orderby, FoldersOrdersDirections direction)
//        {

//            string sql = "SELECT * FROM Folders INNER JOIN FoldersLang ON id_Folder = Folder_FolderLang";
//            sql += " WHERE Parent_Folder = " + FolderID + NetCms.Structure.Applications.ApplicationsPool.ActivesAppzSqlFilter("Tipo_Folder");
//            if (!AddTrashedFolders) sql += " AND Trash_Folder = 0";

//            switch (orderby)
//            {
//                case StructureFolder.ViewOrders.ID: sql += " ORDER BY id_Folder " + (direction == FoldersOrdersDirections.Default ? "DESC" : direction.ToString()); break;
//                case StructureFolder.ViewOrders.Label: sql += string.Format(" ORDER BY Label_FolderLang {0} , id_Folder {0}", (direction == FoldersOrdersDirections.Default ? "ASC" : direction.ToString())); break; break;
//                case StructureFolder.ViewOrders.Name: sql += string.Format(" ORDER BY Name_Folder {0} , id_Folder {0}", (direction == FoldersOrdersDirections.Default ? "ASC" : direction.ToString())); break; break;
//                case StructureFolder.ViewOrders.CreationDate: string.Format(" ORDER BY Date_Folder {0} , id_Folder {0}", (direction == FoldersOrdersDirections.Default ? "DESC" : direction.ToString())); break;
//                case StructureFolder.ViewOrders.ModifyDate: sql += string.Format(" ORDER BY Modify_Folder {0} , id_Folder {0}", (direction == FoldersOrdersDirections.Default ? "DESC" : direction.ToString())); break;
//            }

//            base.FillCollection(sql, CommandType.Text, null);
//        }

//        public void FillWithChildFolders(string folderID, bool addTrashedFolders, StructureFolder.ViewOrders orderby, FoldersOrdersDirections direction)
//        {
//            FolderID = folderID;
//            AddTrashedFolders = addTrashedFolders;

//            FillByQuery(orderby, direction);
//        }
//        public void FillWithRootFolder()
//        {

//            string sql = "SELECT * FROM Folders INNER JOIN FoldersLang ON id_Folder = Folder_FolderLang";
//            sql += " WHERE Depth_Folder = 1" + NetCms.Structure.Applications.ApplicationsPool.ActivesAppzSqlFilter("Tipo_Folder");

//            base.FillCollection(sql, CommandType.Text, null);
//        }


//        public StructureFolder GetByName(string key)
//        {
//            StructureFolder folder = null;
//            for (int i = 0; i < base.Count && folder == null; i++)
//            {
//                if (this[i].Nome.ToLower() == key.ToLower())
//                    folder = this[i];
//            }
//            return folder;
//        }

//        public StructureFolderCollection OrderedByID()
//        {
//            StructureFolderCollection folders = new StructureFolderCollection(this.NetworkKey, this.Visibility, this.Connection);
//            folders.FillWithChildFolders(this.FolderID, this.AddTrashedFolders, StructureFolder.ViewOrders.ID, FoldersOrdersDirections.Default);
//            return folders;
//        }
//        public StructureFolderCollection OrderedByModify()
//        {
//            StructureFolderCollection folders = new StructureFolderCollection(this.NetworkKey, this.Visibility, this.Connection);
//            folders.FillWithChildFolders(this.FolderID, this.AddTrashedFolders, StructureFolder.ViewOrders.ModifyDate, FoldersOrdersDirections.Default);
//            return folders;
//        }
//        public StructureFolderCollection OrderedByName()
//        {
//            StructureFolderCollection folders = new StructureFolderCollection(this.NetworkKey, this.Visibility, this.Connection);
//            folders.FillWithChildFolders(this.FolderID, this.AddTrashedFolders, StructureFolder.ViewOrders.Name, FoldersOrdersDirections.Default);
//            return folders;
//        }
//        public StructureFolderCollection OrderedByDate()
//        {
//            StructureFolderCollection folders = new StructureFolderCollection(this.NetworkKey, this.Visibility, this.Connection);
//            folders.FillWithChildFolders(this.FolderID, this.AddTrashedFolders, StructureFolder.ViewOrders.CreationDate, FoldersOrdersDirections.Default);
//            return folders;
//        }


//        public class CacheableItem : G2Core.Caching.CacheableItem
//        {
//            private Networks.NetworkKey _NetworkKey;
//            public Networks.NetworkKey NetworkKey
//            {
//                get
//                {
//                    return _NetworkKey;
//                }
//                private set { _NetworkKey = value; }
//            }

//            public CacheableItem(Networks.NetworkKey networkKey, DataRow recordData, int index, G2Core.Caching.CacheableCollection ownerCollection)
//                : base(recordData, index, ownerCollection)
//            {
//                _NetworkKey = networkKey;
//                this.Key += networkKey.BaseKey;
//            }
//            protected override G2Core.Caching.ICacheableObject BuildItemProcedure()
//            {
//                return StructureFolder.Fabricate(this.CurrentRecordData, NetworkKey);
//            }
//        }
//    }
//}
