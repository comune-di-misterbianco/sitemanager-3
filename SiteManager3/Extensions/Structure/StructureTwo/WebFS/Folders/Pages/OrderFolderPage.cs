﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.Applications;


/// <summary>
/// Summary description for NewsFolder_NEW_
/// </summary>
namespace NetCms.Structure.WebFS
{
    public class OrderFolderPage : NetCms.GUI.NetworkPage
    {
        public OrderFolderPage()
            : base()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton(this.CurrentNetwork.Paths.AbsoluteAdminRoot + "/cms/default.aspx", NetCms.GUI.Icons.Icons.Arrow_Left, "Indietro"));
        }
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Ordinamento contenuti della cartella '" + this.CurrentFolder.Label + "'"; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Folder_Explore; }
        }
        
        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);

            div.Controls.Add(new NetCms.Structure.StructureFoldersOrder(this.PageData).GetView());

            return div;
        }

    }
}