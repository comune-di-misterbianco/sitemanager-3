using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.Applications;


/// <summary>
/// Summary description for NewsFolder_NEW_
/// </summary>
namespace NetCms.Structure.WebFS
{
    public class ExporterPage : NetCms.GUI.NetworkPage
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Esportazione"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Arrow_Divide; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        public static WebControl FabricatePage()
        {
            if (Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork)
            {
                //Application application = ((StructureFolder)Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder).RelativeApplication;

                Document doc = (Document)Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.CurrentDocument;
                return (WebControl)ApplicationsPool.BuildInstanceOf(doc.RelativeApplication.Classes.Exporter, doc.RelativeApplication.ID.ToString());
            }
            else return new NetCms.GUI.SmPageError();
            //Trace
        }
        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            string scheda = "";
            NetUtility.RequestVariable action = new NetUtility.RequestVariable("action", NetUtility.RequestVariable.RequestType.QueryString);
            if (action.IsValidInteger)
            {                
                string[] id = { this.CurrentFolder.ID.ToString() };
                NetCms.Exporter.Exporter Exporter = new NetCms.Exporter.Exporter(this.CurrentFolder.StructureNetwork.SystemName, id);
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                switch (action.IntValue)
                {
                    case 1:
                        string packageurl = Exporter.ExportPackage();
                         #region log                             
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha creato con successo il pacchetto di esportazione della cartella'" + this.CurrentFolder.LabeledPath + "', relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        #endregion
                        scheda = "<div class=\"\"><h3>Esportazione contenuti \"" + this.CurrentFolder.Label + "\"</h3></div>";
                        scheda += "<p>Il pacchetto di esportazione � stato creato con successo.</p>";
                        scheda += "<p>Puoi effettuare il <a href=\"" + packageurl + "\">Download</a> adesso oppure troverai questo pacchetto disponibile nella <a href=\"exporter.aspx\">pagina di riepilogo dei pacchetti.</a>";
                        scheda += "<p class=\"download\"><a href=\"" + packageurl + "\"><span>Download</span>.";
                        scheda += "</p>";

                        break;
                    case 2:
                        Exporter.Export();
                        #region log                             
                            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha creato con successo un backup della cartella'" + this.CurrentFolder.LabeledPath + "', relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        #endregion
                        scheda = "<div class=\"\"><h3>Esportazione contenuti \"" + this.CurrentFolder.Label + "\"</h3></div>";
                        scheda += "<p>Il backup � stato creato con successo.</p>";
                        scheda += "<p>Troverai questo backup nella <a href=\"exporter.aspx\">pagina di riepilogo dei backup.</a>";
                        scheda += "</p>";

                        break;
                }
            }
            else
            {
                #region log
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di esportazione della cartella'" + this.CurrentFolder.LabeledPath + "', relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                #endregion
                scheda = "<div class=\"\"><h3>Esportazione contenuti \"" + this.CurrentFolder.Label + "\"</h3></div>";
                scheda += "<p>Da questa pagina � possibile esportare i contenuti della cartella \"" + this.CurrentFolder.Label + "\", al fine di effettuare un backup, o copiare il contenuto di questa cartella su un altra Network</p>";
                scheda += "<p>Seleziona una delle opzioni seguenti:";
                scheda += "<ul>";
                scheda += "<li><a href=\"export.aspx?folder=" + this.CurrentFolder.ID + "&amp;action=1\">Esporta come pacchetto di esportazione di SiteManager<a></li>";
                scheda += "<li><a href=\"export.aspx?folder=" + this.CurrentFolder.ID + "&amp;action=2\">Esporta ed aggiungi ai backup disponibili per questo Portale<a></li>";
                //scheda += "<li><a href=\"export.aspx?folder=" + this.CurrentFolder.ID + "&amp;action=3\">Duplica questa cartella all'interno di un altra cartella di questa rete.<a></li>";
                scheda += "</ul>";
                scheda += "</p>";
            }

            div.Controls.Add(new LiteralControl(scheda));

            return div;
        }
    }
}
