﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.Applications;
using NetCms.Structure.WebFS.Forms;
using System.IO;
using System.Linq;
using NetCms.Diagnostics;
using NHibernate;
using NetCms.DBSessionProvider;
using System.Collections.Generic;
using Structure.Importer.Model;
using Structure.StructureTwo.Vertical.Importer.Model;

namespace NetCms.Structure.WebFS
{
    public class ImportContentPage : NetCms.GUI.NetworkPage
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Importazione contenuti"; }
        }

        /// <summary>
        /// Tmp folder path destination
        /// </summary>
        private string destinationFolderPath = "/repository/contents/tmp/";
        
        /// <summary>
        /// Record Counter
        /// </summary>
        private int TotalRecordCounter
        {
            get;
            set;
        }


        private int FolderId
        {
            get
            {
                G2Core.Common.RequestVariable folder = new G2Core.Common.RequestVariable("folder", G2Core.Common.RequestVariable.RequestType.QueryString);
                if (folder.IsValidInteger)
                    return folder.IntValue;
                return -1;
            }

        }

        private StructureFolder currentFolder
        {
            get
            {
                return FileSystemBusinessLogic.FolderBusinessLogic.GetById(FolderId);
            }

        }



        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Arrow_Divide; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }       

        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "";

            div.Controls.Add(HelpControl());

            WebControl field = new WebControl(HtmlTextWriterTag.Fieldset);
            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);

            legend.Controls.Add(new LiteralControl("Importazione contenuti"));
            field.Controls.Add(legend);
            field.Controls.Add(ImportForm);

            div.Controls.Add(field);


            return div;
        }


        private WebControl HelpControl()
        {
            WebControl field = new WebControl(HtmlTextWriterTag.Fieldset);

            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            legend.Controls.Add(new LiteralControl("Help"));
            field.Controls.Add(legend);

            WebControl p = new WebControl(HtmlTextWriterTag.Div);
            p.CssClass = "info";
            p.Controls.Add(new LiteralControl("Inserire qui help in linea"));

            field.Controls.Add(p);

            return field;
        }

        private CmsContentImportForm _ImportForm;
        private CmsContentImportForm ImportForm
        {
            get
            {
                if (_ImportForm == null) {
                    _ImportForm = new CmsContentImportForm("import", this.IsPostBack, destinationFolderPath);
                    _ImportForm.AddSubmitButton = true;
                    _ImportForm.SubmitButton.Text = "Importa";
                    _ImportForm.AddPreLoader = true;
                    _ImportForm.PreLoaderText = "Importazione in corso ... ";
                    _ImportForm.SubmitButton.Click += SubmitButton_Click;
                }

                return _ImportForm;
            }            
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            //List<ImportItem> items = new List<ImportItem>();
            //items.Add(new ImportItem{ Title = "aa1", Date = DateTime.Now, Description = "bb1", Content = "cc1" });
            //items.Add(new ImportItem{ Title = "aa2", Date = DateTime.Now, Description = "bb2", Content = "cc2" });
            //items.Add(new ImportItem{ Title = "aa3", Date = DateTime.Now, Description = "bb3", Content = "cc3" });

            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(items, Newtonsoft.Json.Formatting.Indented);

            // check if json file is valid --> schema

            System.Text.Encoding encode = System.Text.Encoding.UTF8;            
            StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(destinationFolderPath + UploadedFile.Name), encode, true);

            List<ImportItem> items = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ImportItem>>(reader.ReadToEnd());                                    

            List<ImportState> importStateList = new List<ImportState>();

            using (ISession sess = FluentSessionProvider.Instance.OpenInnerSession())
            {
                try
                {
                    foreach (ImportItem item in items)
                    {
                        ImportState esito = currentFolder.CreateDocumentFromImport(item, sess);
                        importStateList.Add(esito);
                       
                        // al primo errore blocco l'import
                        if (esito.Status ==ImportState.StatusEnums.Error)
                            break;
                    }
                }
                catch (Exception ex)
                {

                }
            }

            // 
            string message = string.Empty;
            bool itemsWithError = importStateList.Any(x => x.Status == ImportState.StatusEnums.Error);
            bool itemSkipped = importStateList.Any(x => x.Status == ImportState.StatusEnums.Skipped);

            int itemWithErrorCount = importStateList.Count(x => x.Status == ImportState.StatusEnums.Error);
            int itemSkippedCount = importStateList.Count(x => x.Status == ImportState.StatusEnums.Skipped);
            int itemImportedCount = importStateList.Count(x => x.Status == ImportState.StatusEnums.OK);

            // rimozione del file json

            if (!itemsWithError)
            {
                //UploadedFile.Delete();
                if (!itemSkipped)
                    message += "Tutti gli elementi contenuti nel file json sono stati importati correttamente.";
            }
            else
                message += "Alcuni elementi non sono stati importati a causa di un errore.";

            if (itemSkipped)
                message +=  " elementi sono stati saltati perchè presentavano lo stesso titolo.";

            

            // visualizzare report stato import
            NetCms.GUI.PopupBox.AddSessionMessage(message, !itemsWithError ? PostBackMessagesType.Success : PostBackMessagesType.Error, !itemsWithError);
        }

        //private void SubmitButton_Click(object sender, EventArgs e)
        //{
        //    string resultValidation = FileValidation();

        //    if (string.IsNullOrEmpty(resultValidation))
        //    {
        //        if (UploadedFile != null)
        //        {
        //            System.Text.Encoding encode = System.Text.Encoding.Default;
        //            StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(destinationFolderPath + UploadedFile.Name), encode, true);

        //            string[] line;
        //            int index = 1;
        //            do
        //            {
        //                if (index != 1)
        //                {
        //                    line = reader.ReadLine().Split(';');
        //                    try
        //                    {
        //                        using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())                                
        //                        {
        //                            string Titolo = line[0].TrimStart().TrimEnd();
        //                            string Data = line[1].TrimStart().TrimEnd();
        //                            string Descrizione = line[2].TrimStart().TrimEnd();
        //                            string Contenuto = line[3].TrimStart().TrimEnd();


        //                            Dictionary<string, object> docData = new Dictionary<string, object>();
        //                            docData.Add("Titolo", line[0].TrimStart().TrimEnd());
        //                            docData.Add("Descrizione", line[2].TrimStart().TrimEnd());
        //                            docData.Add("Data", DateTime.Parse(line[1].TrimStart().TrimEnd()));
        //                            docData.Add("Contenuto", line[3].TrimStart().TrimEnd());             // => si potrebbe convertire anche questo in dictionary
        //                            docData.Add("LangID", 1); // lingua predefinita                                    

        //                            bool esito = currentFolder.CreateDocumentFromImport(docData, session);

        //                        }
        //                    }
        //                    catch(Exception ex)
        //                    {
        //                       //RecordWithError += 1;
        //                       //ErrorList.Add("Riga " + index.ToString() + ": Errore importazione del contenuto ");
        //                       Diagnostics.Diagnostics.TraceMessage(TraceLevel.Error, "Errore importazione del contenuto alla riga " + index.ToString(), NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName);
        //                       Diagnostics.Diagnostics.TraceException(ex, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName);
        //                    }
        //                }
        //                else
        //                {
        //                    reader.ReadLine();
        //                }
        //                index++;

        //            } while (!reader.EndOfStream);
        //            reader.Close();
        //        }
        //    }
        //}

        private FileInfo UploadedFile
        {
            get
            {
                try
                {
                    string sourceFolder = NetCms.Configurations.Paths.AbsoluteRoot + destinationFolderPath;

                    var files = ImportForm.UploadCSV.FileTempList.First();
                    if (files != null && files.Count > 0)
                    {
                        string path = Path.Combine("/" + sourceFolder, files["FileName"].ToString());
                        string completePath = HttpContext.Current.Server.MapPath("/" + path);
                        FileInfo fileIn = new FileInfo(completePath);
                        return fileIn;
                    }
                    else
                        return null;
                }
                catch (Exception ex)
                {
                    // diagnostica per errori sul fileInfo
                    return null;
                }
            }
        }

        //public string FileValidation()
        //{
        //    string errors = "";

        //    if (UploadedFile != null)
        //    {
        //        System.Text.Encoding encode = System.Text.Encoding.UTF8;

        //        string absoluteFilePath = System.Web.HttpContext.Current.Server.MapPath(destinationFolderPath + UploadedFile.Name);

        //        StreamReader reader = new StreamReader(absoluteFilePath, encode, true);              

        //        string[] lineIn;

        //        int index = 0;
        //        do
        //        {
        //            if (index != 0)
        //            {
        //                lineIn = reader.ReadLine().Split(';');
        //                errors += LineValidation(lineIn, index + 1);
        //                TotalRecordCounter += 1;
        //            }
        //            else
        //            {
        //                reader.ReadLine();
        //            }
        //            index++;
        //        } while (!reader.EndOfStream);

        //        reader.Close();
        //    }
        //    else
        //    {
        //        //status_import = false;
        //        Diagnostics.Diagnostics.TraceMessage(TraceLevel.Warning, "L'utente " + NetCms.Users.AccountManager.CurrentAccount.UserName + " ha tentato un'importazione massiva di contenuti: nessun file è stato selezionato per l'importazione.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName);
        //        PopupBox.AddSessionMessage("Selezionare un file da importare!!", PostBackMessagesType.Error);
        //    }

        //    return errors;

        //}

        //public string LineValidation(string[] line, int lineNumber)
        //{
        //    string errors = "";

        //    string Titolo = line[0].TrimStart().TrimEnd();
        //    string Data = line[1].TrimStart().TrimEnd();           
        //    string Descrizione = line[2].TrimStart().TrimEnd();
        //    string Contenuto = line[3].TrimStart().TrimEnd();
            
                     
        //    // potrebbe esserci qualche riga totalmente vuota. faccio saltare la validazione
        //    if (!String.IsNullOrEmpty(Titolo) && !String.IsNullOrEmpty(Data) &&
        //        !String.IsNullOrEmpty(Descrizione) && !String.IsNullOrEmpty(Contenuto)
        //        )
        //    {

        //        #region Titolo

        //        if (string.IsNullOrEmpty(Titolo))
        //            errors += "la riga " + lineNumber + " non contiente nessun Titolo ";

        //        if (!string.IsNullOrEmpty(Titolo) && (Titolo.Length == 0))
        //            errors += "la riga " + lineNumber + " non contiene un Titolo valido.<br />";

        //        #endregion

        //        #region Data

        //        DateTime DataContenuto;

        //        if (string.IsNullOrEmpty(Data))
        //            errors += "la riga " + lineNumber + " non contiente nessuna Data";

        //        if (!string.IsNullOrEmpty(Data) && !DateTime.TryParse(Data, out DataContenuto))
        //            errors += "la riga " + lineNumber + " non contiene una Data valida.<br />";

        //        #endregion

        //        #region Descrizione

        //        if (string.IsNullOrEmpty(Descrizione))
        //            errors += "la riga " + lineNumber + " non contiente nessuna Descrizione ";

        //        if (!string.IsNullOrEmpty(Descrizione) && (Descrizione.Length == 0))
        //            errors += "la riga " + lineNumber + " non contiene una Descrizione valido.<br />";

        //        #endregion

        //        #region Contenuto

        //        if (string.IsNullOrEmpty(Contenuto))
        //            errors += "la riga " + lineNumber + " non contiente nessun Contenuto ";

        //        if (!string.IsNullOrEmpty(Contenuto) && (Contenuto.Length == 0))
        //            errors += "la riga " + lineNumber + " non contiene un Contenuto valido.<br />";

        //        #endregion

        //    }
        //    return errors;
        //}



    }

}
