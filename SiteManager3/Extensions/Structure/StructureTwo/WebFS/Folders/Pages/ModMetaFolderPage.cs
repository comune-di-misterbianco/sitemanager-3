﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.Applications;


/// <summary>
/// Summary description for NewsFolder_NEW_
/// </summary>
namespace NetCms.Structure.WebFS
{
    public class ModMetaFolderPage : NetCms.GUI.NetworkPage
    {
        public ModMetaFolderPage()
            : base()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton(this.CurrentNetwork.Paths.AbsoluteAdminRoot + "/cms/folders_meta.aspx?folder=" + this.CurrentFolder.ID, NetCms.GUI.Icons.Icons.Arrow_Left, "Indietro"));
        }
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Aggiornamento Meta della cartella '" + this.CurrentFolder.Label + "'"; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Tag_Blue_Edit; }
        }
        
        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);

            div.Controls.Add(new NetCms.Structure.StructureFolderMeta(this.PageData).GetModView());

            return div;
        }

    }
}