using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.Applications;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Networks.WebFS;
using NetCms.Users;


/// <summary>
/// Summary description for NewsFolder_NEW_
/// </summary>
namespace NetCms.Structure.WebFS
{
    public class ListFolderPage : NetCms.GUI.NetworkPage
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return this.CurrentFolder.SearchCriteria.Length > 0 ? this.CurrentFolder.SearchCriteria : this.CurrentFolder.LabeledPath; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Folder; }
        }
        
        protected override WebControl BuildControls()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);

            div.Controls.Add(this.CurrentFolder.getListView());

            return div;
        }

        public ListFolderPage()
        {
            CheckAddToFavorite();

            ToolbarButtonsCollection toolbar = this.CurrentFolder.ToolbarButtons;
            if (toolbar != null)
                foreach (ToolbarButton button in toolbar)
                    this.SideBar.ButtonsToolbar.Buttons.Add(button);
            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla cartella '" + this.CurrentFolder.Label + "' del portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName, callerObject: this);                    
            
        }

        public void CheckAddToFavorite()
        {
            NetUtility.RequestVariable starred = new NetUtility.RequestVariable("star", PageData.Request.QueryString);
            if (!PageData.IsPostBack && starred.IsValid(NetUtility.RequestVariable.VariableType.Integer))
            {
                if (CurrentFolder != null && CurrentFolder.ID == starred.IntValue  && !CurrentFolder.Starred && CurrentFolder.Grant)
                {

                    User user = UsersBusinessLogic.GetById(PageData.Account.ID);
                    user.FavoritesFolders.Add(CurrentFolder);
                    UsersBusinessLogic.SaveOrUpdateUser(user);
                    NetCms.Users.AccountManager.UpdateCurrentUser();

                    //PageData.Conn.Execute("INSERT INTO Favorites (User_Favorite,Folder_Favorite) VALUES (" + PageData.Account.ID + "," + starred.IntValue.ToString() + ")");
                    
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha aggiunto ai preferiti la cartella '" + this.CurrentFolder.Label + "' del portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);                    
           
                    CurrentFolder.InitStarred();
                    PageData.Session["Favorites"] = null;

                }
            }
        }
    }
}
