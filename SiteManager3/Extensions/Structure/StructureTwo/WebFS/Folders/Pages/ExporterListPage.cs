﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.Applications;
using System.IO;
using System.Linq;

namespace NetCms.Structure.WebFS
{
    public class ExporterListPage : NetCms.GUI.NetworkPage
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Pacchetti esportazione presenti"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Page_White_Zip; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        
        protected override WebControl BuildControls()
        {
            if (!IsPostBack)
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina dell'exporter con la lista dei file.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            }


            WebControl value = new WebControl(HtmlTextWriterTag.Div);

            Table table = new Table();
            table.CssClass = "tab";
            table.CellPadding = 0;
            table.CellSpacing = 0;

            TableHeaderRow hrow = new TableHeaderRow();
            table.Controls.Add(hrow);

            TableHeaderCell hcell = new TableHeaderCell();
            hcell.Text = "Nome";
            hrow.Controls.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Tipo";
            hrow.Controls.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Data";
            hrow.Controls.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Dimensione";
            hrow.Controls.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Download";
            hrow.Controls.Add(hcell);

            DirectoryInfo directory = new DirectoryInfo(Configurations.Paths.AbsoluteExportRoot);
            //DirectoryInfo[] folders = directory.GetDirectories();
            bool alternate = false;
            //foreach (DirectoryInfo obj in folders)
            //{
            //    if (obj.Name.StartsWith("SM2-ExportData-") || obj.Name.StartsWith("SM2-ExportUserGroupData-"))
            //    {
            //        TableRow row = new TableRow();
            //        row.CssClass = alternate ? "alternate" : "normal";
            //        table.Controls.Add(row);

            //        TableCell cell = new TableCell();
            //        cell.Text = obj.Name;
            //        row.Controls.Add(cell);

            //        cell = new TableCell();
            //        cell.Text = "Cartella di Esportazione";
            //        row.Controls.Add(cell);

            //        cell = new TableCell();
            //        cell.Text = obj.CreationTime.Year + "/" + obj.CreationTime.Month + "/" + obj.CreationTime.Day + " @ " + obj.CreationTime.Hour + ":" + obj.CreationTime.Minute;
            //        row.Controls.Add(cell);

            //        cell = new TableCell();
            //        cell.Text = (this.GetDirectorySize(obj.FullName) / 1024).ToString() + "KB";
            //        row.Controls.Add(cell);

            //        cell = new TableCell();
            //        cell.CssClass = "";
            //        cell.Text = "&nbsp;";
            //        row.Controls.Add(cell);
            //    }
            //}

            FileInfo[] files = directory.GetFiles();
            foreach (FileInfo obj in files.OrderByDescending(x=>x.CreationTime))
            {
                if (obj.Extension == ".sm2pkg" && (obj.Name.StartsWith("SM2-ExportData-") || obj.Name.StartsWith("SM2-ExportUserGroupData-")))
                {
                    TableRow row = new TableRow();
                    row.CssClass = alternate ? "alternate" : "normal";
                    table.Controls.Add(row);

                    TableCell cell = new TableCell();
                    cell.Text = obj.Name;
                    row.Controls.Add(cell);

                    cell = new TableCell();
                    cell.Text = "Pacchetto di Esportazione";
                    row.Controls.Add(cell);

                    cell = new TableCell();
                    cell.Text = obj.CreationTime.ToShortDateString() + " " + obj.CreationTime.ToShortTimeString();//obj.CreationTime.Year + "/" + obj.CreationTime.Month + "/" + obj.CreationTime.Day + " @ " + obj.CreationTime.Hour + ":" + obj.CreationTime.Minute;
                    row.Controls.Add(cell);

                    cell = new TableCell();
                    cell.Text = (obj.Length / 1024).ToString() + "KB";
                    row.Controls.Add(cell);

                    cell = new TableCell();
                    cell.CssClass = "action download";
                    cell.Text = "<a href=\"" + Configurations.Paths.AbsoluteRoot + "/exported-data/" + obj.Name + "\"><span>Download</span></a>";
                    row.Controls.Add(cell);
                }
            }

            value.Controls.Add(table);

            return value;
        }

        private long GetDirectorySize(string path)
        {
            // 1
            // Get array of all file names.
            string[] a = Directory.GetFiles(path, "*.*");

            // 2
            // Calculate total bytes of all files in a loop.
            long b = 0;
            foreach (string name in a)
            {
                // 3
                // Use FileInfo to get length of each file.
                FileInfo info = new FileInfo(name);
                b += info.Length;
            }
            // 4
            // Return total size
            return b;
        }
    }
}
