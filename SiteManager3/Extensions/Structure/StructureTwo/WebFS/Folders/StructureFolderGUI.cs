﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetCms.Structure.Grants;
using NetForms;
using System.Linq;
using NetCms.Networks.WebFS;
using NetCms.Front;
using System.Collections;
using System.Collections.Generic;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Grants;
using System.Threading.Tasks;
using SharedUtilities;
using LanguageManager.Model;
using CmsConfigs.Model;

namespace NetCms.Structure.WebFS
{
    public abstract partial class StructureFolder : NetCms.Networks.WebFS.NetworkFolder
    {
        public virtual void DeletePostBack()
        {
            G2Core.Common.RequestVariable deleteDocRequest = new G2Core.Common.RequestVariable("deleteDoc", G2Core.Common.RequestVariable.RequestType.Form);
            if (this.PageData.IsPostBack && deleteDocRequest.IsValidInteger)
            {
                if (this.Documents.Where(x => x.ID == deleteDocRequest.IntValue).Count() > 0)
                    (this.Documents.First(x => x.ID == deleteDocRequest.IntValue) as Document).Delete();

                //this.Documents.Remove(deleteDocRequest.StringValue);
                //this.Invalidate();
            }
        }

        public virtual string TypeName
        {
            get
            {
                return this.RelativeApplication.Label;
            }
        }


        public virtual string KeyTypeName
        {
            get
            {
                return RelativeApplication.SystemName;
            }
        }

        public virtual HtmlGenericControl getMoveFolderView()
        {
            if (this.Criteria[CriteriaTypes.Advanced].Value == GrantsValues.Deny)
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina alla quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
        
            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerHtml = "Spostamento del Cartella";
            fieldset.Controls.Add(legend);

            StructureFolderBinder binder = new StructureFolderBinder(this.PageData);
            int[] types = new int[3];
            types[0] = 2;
            types[1] = 1;
            types[2] = this.Tipo;
            TreeNode root = binder.TreeFromStructure(this.StructureNetwork.RootFolder, types);
            NetForms.NetTreeView folders = new NetForms.NetTreeView("Seleziona la cartella di destinazione", "Folder_ModuloApplicativo", root);
            folders.NotAllowedMsg = "Attenzione! La cartella selezionata non è di tipo " + this.TypeName + " o non si possiedono i permessi di acceso";
            folders.RootDist = Configurations.Paths.AbsoluteRoot;
            folders.CheckAllows = true;
            folders.Required = true;
            fieldset.Controls.Add(folders.getControl());

            Button move = new Button();
            move.ID = "Move";
            move.Text = "Sposta";
            fieldset.Controls.Add(move);

            NetUtility.RequestVariable MoveRV = new NetUtility.RequestVariable(move.ID, NetUtility.RequestVariable.RequestType.Form);
            if (MoveRV.IsValid() && MoveRV.StringValue == move.Text)
            {
                NetUtility.RequestVariable FolderRV = new NetUtility.RequestVariable(folders.FieldName, NetUtility.RequestVariable.RequestType.Form);
                if (FolderRV.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                {
                    StructureFolder targetFolder = (StructureFolder)this.StructureNetwork.RootFolder.FindFolderByID(FolderRV.IntValue);
                   
                    // verifico che la folder di destinazione non sia una cartella figlia                                                             
                    //if (this.FindFolderByID(targetFolder.ID) == null)
                    if (!this.Folders.Any(x=>x.ID == targetFolder.ID))
                    {

                        if (targetFolder != null)
                        {
                            bool moved = this.MoveFolder(targetFolder);

                            //string Utente = this.Account.Label;
                            //string logText = "L'utente " + Utente + "vuole spostare la cartella " + this.FindFolder(targetFolder.ID.ToString());
                            //PageData.GuiLog.SaveLogRecord(logText, false);
                            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                            //if (this.FindFolderByID(targetFolder.ID) == null) non posso usare la proprietà Nome quindi è stato sostituito con targetFolder
                            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " vuole spostare la cartella '" + targetFolder.Nome/*this.FindFolderByID(targetFolder.ID).Nome*/ + "' (id=" + targetFolder.ID.ToString() + ") del portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                            if (moved)
                            {
                                PageData.MsgBox.InnerHtml = "Cartella Spostata";
                                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha spostato con successo la cartella, relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                            }
                            else
                            {
                                NetCms.GUI.PopupBox.AddMessage("Si è verificato un errore non sarà possibile spostare la cartella.", PostBackMessagesType.Error);
                                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a spostare la cartella, relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                            }
                        }
                        else
                        {
                            NetCms.GUI.PopupBox.AddMessage("Si è verificato un errore non sarà possibile spostare la cartella.", PostBackMessagesType.Error);
                            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a spostare la cartella, relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        }
                    }
                    else
                    {
                        NetCms.GUI.PopupBox.AddMessage("Non è possibile spostare la cartella all'interno di una sua sottocartella.", PostBackMessagesType.Error);
                        string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a spostare la cartella, relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    }
                }
                else
                {
                    NetCms.GUI.PopupBox.AddMessage("Si è verificato un errore non sarà possibile spostare la cartella.", PostBackMessagesType.Error);
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a spostare la cartella, relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                }
            }
            

            return fieldset;
        }
        
        protected NetFormTable FormTable
        {
            get
            {
                NetFormTable _FormTable = new NetFormTable("Folders", "id_Folder", this.StructureNetwork.Connection);

                NetDropDownList tipo = new NetDropDownList("Tipo", "Tipo_Folder");
                tipo.AutoPostBack = true;
                string tiposql = "SELECT LabelPlurale_Applicazione,id_Applicazione,Enabled_Applicazione FROM applicazioni";
                //if (this.Type > 2)
                //{
                //    tiposql += " WHERE Enabled_Applicazione = 1";
                //    tiposql += " AND (id_Applicazione = " + this.Type + " OR id_Applicazione = 4 OR id_Applicazione = 204) ";
                //}
                tiposql += " WHERE Enabled_Applicazione = 1";
                if (this.RelativeApplication.AllowedSubfolders != null && this.RelativeApplication.AllowedSubfolders.Length > 0)
                {
                    if (!this.RelativeApplication.AllowedSubfolders.Contains("all"))
                    {
                        tiposql += " AND (SystemName_Applicazione = '" + this.RelativeApplication.SystemName + "'";
                        tiposql += " OR SystemName_Applicazione LIKE '" + string.Join("' OR SystemName_Applicazione LIKE '", this.RelativeApplication.AllowedSubfolders);
                        tiposql += "')";
                    }
                }
                else
                    tiposql += " AND id_Applicazione = " + this.Tipo;

                tiposql += " ORDER BY LabelPlurale_Applicazione";
                DataTable dt = this.StructureNetwork.Connection.SqlQuery(tiposql);
                
                foreach (DataRow row in dt.Rows)
                {
                    string AppID = row["id_Applicazione"].ToString();
                    //if (this.Criteria.Application(CriteriaTypes.NewFolder, AppID))
                    if (this.CriteriaForApplication(CriteriaTypes.NewFolder, AppID).Allowed)
                        tipo.addItem(row["LabelPlurale_Applicazione"].ToString(), AppID);
                }

                _FormTable.addField(tipo);
                tipo.Required = true;
                if (this.PageData.IsPostBack)
                {
                    NetUtility.RequestVariable type = new NetUtility.RequestVariable("Tipo_Folder", HttpContext.Current.Request.Form);
                    if (type.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                    {
                        if (this.Criteria[CriteriaTypes.FoldersVisibility].Allowed)
                        {
                            NetDropDownList Stato = new NetDropDownList("Stato", "Stato_Folder");
                            Stato.ClearList();
                            //for (int i = 0; i < 3; i++)
                            //    Stato.addItem(StructureFolder.StatusLabels(i), i.ToString());
                            foreach (StructureFolder.FolderStates stato in Enum.GetValues(typeof(StructureFolder.FolderStates)))
                            {
                                int k = (int)Enum.Parse(typeof(StructureFolder.FolderStates), stato.ToString());
                                Stato.addItem(EnumUtilities.GetEnumDescription(stato), k.ToString());
                            }
                            Stato.Required = true;
                            _FormTable.addField(Stato);
                        }
                        else
                        {
                            NetHiddenField Stato = new NetHiddenField("Stato_Folder");
                            Stato.Value = "2";
                            _FormTable.addField(Stato);
                        }

                        //if (type.IntValue == 2) // comportamento per webdocs
                        //{
                        //    NetDropDownList TipoHome = new NetDropDownList("Tipo pagina principale", "HomeType_Folder");
                        //    TipoHome.ClearList();

                        //    TipoHome.addItem("Nome e Descrizione", "0");
                        //    TipoHome.addItem("Indice contenuti", "-1");
                        //    TipoHome.addItem("Pagina Personalizzata", "1");
                        //    TipoHome.addItem("Landing Page", "2");
                        //    TipoHome.addItem("Section Page", "3");
                        //    TipoHome.Required = true;

                        //    _FormTable.addField(TipoHome);                            

                        //}
                        //else
                        if (type.IntValue == 219) // applicazione verticale ???
                        {
                            NetDropDownList TipoApplicazioneVerticale = new NetDropDownList("Applicazione Associata", "HomeType_Folder");
                            TipoApplicazioneVerticale.ClearList();
                            foreach (NetCms.Vertical.VerticalApplication app in Vertical.VerticalApplicationsPool.ActiveAssemblies)
                                TipoApplicazioneVerticale.addItem(app.Label, app.ID.ToString());

                            TipoApplicazioneVerticale.Required = true;

                            _FormTable.addField(TipoApplicazioneVerticale);
                            //break;
                        }
                        //else if (type.IntValue == 3 || type.IntValue == 6 || type.IntValue == 8 || type.IntValue == 10 || type.IntValue == 11 || type.IntValue == 14)
                        //{
                        //    // in caso di news,bandi,cstampa, faq, imggallery, rassegna
                        //    NetDropDownList TipoHome = new NetDropDownList("Tipo pagina principale", "HomeType_Folder");
                        //    TipoHome.ClearList();
                        //    TipoHome.addItem("Normale", "0");
                        //    TipoHome.addItem("Mostra descrizione", "-2");
                        //    TipoHome.Required = true;

                        //    _FormTable.addField(TipoHome);
                        //}
                        //if (type.IntValue == 2 || type.IntValue == 1)
                        //{
                        //    NetDropDownList ShowInTopMenu = new NetDropDownList("Mostra nel topmenu", "ShowInTopMenu_Folder");
                        //    ShowInTopMenu.ClearList();
                        //    ShowInTopMenu.addItem("Si", "1");
                        //    ShowInTopMenu.addItem("No", "0");
                        //    _FormTable.addField(ShowInTopMenu);
                        //}

                        if (this.StructureNetwork.CurrentFolder.RssValue != NetCms.Structure.WebFS.StructureFolder.RssStatus.SystemError)
                        {
                            NetDropDownList rss = new NetDropDownList("Rss", "Rss_Folder");
                            rss.ClearList();
                            rss.addItem("Eredita", "0");
                            rss.addItem("Abilitati", "1");
                            rss.addItem("Disabilitati", "2");
                            rss.Required = true;

                            _FormTable.addField(rss);
                        }

                        if (this.StructureNetwork.CurrentFolder.FrontLayout != FrontLayouts.SystemError)
                        {
                            NetDropDownList layout = new NetDropDownList("Layout di colonne e Piè di pagina (Attenzione la visualizzazione finale del layout dipende comunque da come sono state impostate le colonne nella homepage relativa a questa cartella.)", "Layout_Folder");
                            layout.ClearList();

                            layout.addItem("Eredita", "0");
                            layout.addItem("Mostra Tutte", "8");
                            layout.addItem("Nascondi Tutte", "7");
                            layout.addItem("Mostra solo Sinistra e Destra", "1");
                            layout.addItem("Mostra solo Sinistra e Footer", "2");
                            layout.addItem("Mostra solo Destra e Footer", "3");
                            layout.addItem("Mostra solo Sinistra", "4");
                            layout.addItem("Mostra solo Destra", "5");
                            layout.addItem("Mostra solo Footer", "6");
                            layout.Required = true;

                            _FormTable.addField(layout);
                        }

                        NetDropDownList grants4docs = new NetDropDownList("Elabora permessi per i Documenti di questa cartella", "Disable_Folder");
                        grants4docs.ClearList();
                        grants4docs.addItem("Elabora", "0");
                        grants4docs.addItem("Non Elaborare", "2");
                        grants4docs.Required = true;

                        _FormTable.addField(grants4docs);
                        
                        NetDropDownList restricted = new NetDropDownList("Area Riservata", "Restricted_Folder");
                        restricted.ClearList();
                        if (this.Restricted && this.RestrictedArea > 0)
                        {
                            NetHiddenField restrictedforced = new NetHiddenField("Restricted_Folder");
                            restrictedforced.Value = "0";
                            _FormTable.addField(restrictedforced);
                        }
                        else
                        {
                            if (!this.Restricted)
                            {
                                restricted.addItem("NO (Cartella accessibile a tutto il Pubblico)", "0");
                                restricted.addItem("Riservata (Cartella accessibile solo al pubblico Autenticato e Profilato)", "-1");
                            }
                            else
                            {
                                restricted.addItem("Eredita (Conserva le impostazioni della cartella genitore)", "0");
                            }
                            restricted.Required = true;
                            _FormTable.addField(restricted);
                        }
                        if (this.Criteria[CriteriaTypes.Advanced].Allowed)
                        {
                            NetTextBoxPlus cssclass = new NetTextBoxPlus("Classe Css", "CssClass_Folder");
                            cssclass.ContentType = NetTextBoxPlus.ContentTypes.NormalText;
                            _FormTable.addField(cssclass);
                        }

                        if (NetCms.Configurations.ThemeEngine.Status == Configurations.ThemeEngine.ThemeEngineStatus.on)
                        {
                            if (type.IntValue == 2) // webdocs
                            {
                                NetDropDownList TipoHome = new NetDropDownList("Tipo pagina principale", "HomeType_Folder");
                                TipoHome.ClearList();

                                TipoHome.addItem("Nome e Descrizione", "0");
                                TipoHome.addItem("Indice contenuti", "-1");
                                TipoHome.addItem("Pagina Personalizzata", "1");
                                TipoHome.addItem("Landing Page", "2");
                                TipoHome.addItem("Section Page", "3");
                                TipoHome.Required = true;

                                _FormTable.addField(TipoHome);
                            }

                            if (type.IntValue == 3 || //news               
                            type.IntValue == 8 || // stampa
                            type.IntValue == 7) // eventi
                            {
                                NetDropDownList Template = new NetDropDownList("Template pagina", "Template_Folder");
                                foreach (string tpl in GlobalConfig.CurrentConfig.CurrentTheme.GetTplElement(new List<Themes.Model.TemplateElement.TplTypes>()
                                                                                    {
                                                                                        Themes.Model.TemplateElement.TplTypes.contentapplicativo
                                                                                    }))
                                {
                                    Template.addItem(tpl, tpl.ToLower());
                                }

                                Template.Required = true;

                                _FormTable.addField(Template);
                            }
                            if (type.IntValue == 4)
                            {
                                NetDropDownList ShowMenu = new NetDropDownList("Mostra menu contestuale", "ShowMenu_Folder");
                                ShowMenu.ClearList();
                                ShowMenu.addItem("No", "0");
                                ShowMenu.addItem("Si", "1");
                                _FormTable.addField(ShowMenu);
                            }

                            NetDropDownList ShowInTopMenu = new NetDropDownList("Mostra nel topmenu", "ShowInTopMenu_Folder");
                            ShowInTopMenu.ClearList();
                            ShowInTopMenu.addItem("Si", "1");
                            ShowInTopMenu.addItem("No", "0");
                            _FormTable.addField(ShowInTopMenu);
                        }
                        else
                        {
                            if (type.IntValue == 2)
                            {
                                NetDropDownList TipoHome = new NetDropDownList("Tipo pagina principale", "HomeType_Folder");
                                TipoHome.ClearList();

                                TipoHome.addItem("Nome e Descrizione", "0");
                                TipoHome.addItem("Indice contenuti", "-1");
                                TipoHome.addItem("Pagina Personalizzata", "1");
                                TipoHome.addItem("Landing Page", "2");
                                TipoHome.addItem("Section Page", "3");
                                TipoHome.Required = true;

                                _FormTable.addField(TipoHome);
                            }

                            if (type.IntValue == 3 || //news               
                            type.IntValue == 8 || //stampa
                            type.IntValue == 6 || //bandi
                            type.IntValue == 10 || //faq
                            type.IntValue == 11 || //imagegallery
                            type.IntValue == 14)   // rassegna                         
                            {
                                NetDropDownList TipoHome = new NetDropDownList("Tipo pagina principale", "HomeType_Folder");
                                TipoHome.ClearList();
                                TipoHome.addItem("Normale", "0");
                                TipoHome.addItem("Mostra descrizione", "-2");
                                TipoHome.Required = true;
                                _FormTable.addField(TipoHome);
                            }
                        }

                    }
                }

                return _FormTable;
            }

        }

        public virtual WebControl getNewFolderView()
        {
            if (this.Criteria[CriteriaTypes.NewFolder].Value == GrantsValues.Deny)
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());

            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "container-fluid";

            WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);

            //HtmlGenericControl percorso = new HtmlGenericControl("fieldset");

            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerHtml = "Creazione Nuova Cartella ";
            fieldset.Controls.Add(legend);

            HtmlGenericControl contentDiv = new HtmlGenericControl("div");
            contentDiv.Attributes["class"] = "contentForm newfolderform col-md-12";

            HtmlGenericControl divInfo = new HtmlGenericControl("div");
            divInfo.Attributes["class"] = "netformtable folderinfopath";

            HtmlGenericControl divInnerInfo = new HtmlGenericControl("div");
            divInnerInfo.Attributes["class"] = "innerContent";

            HtmlGenericControl datipercorso = new HtmlGenericControl("p");
            
            datipercorso.InnerHtml = "La nuova cartella sarà creata all'interno della cartella <strong>\"" + this.Label + "\"</strong> ";//<br />";
           

            StructureFolder fold = this;
            string path = "";
            while (fold != null)
            {
                path = fold.Label + "/" + path;
                fold = fold.Parent as StructureFolder;
            }
            datipercorso.InnerHtml += "(Percorso: \"" + path + "\")";
            divInnerInfo.Controls.Add(datipercorso);
            divInfo.Controls.Add(divInnerInfo);
            contentDiv.Controls.Add(divInfo);
            //fieldset.Controls.Add(percorso);

            #region Toolbar
            PageData.InfoToolbar.Title = "Creazione Nuova Cartella ";
            #endregion

            #region Lang

            NetFormTable FoldersLang = new NetFormTable("FoldersLang", "id_FolderLang", this.StructureNetwork.Connection);
            FoldersLang.CssClass = "netformtable_lang";

            NetTextBoxPlus nome = new NetTextBoxPlus("Nome", "Label_FolderLang");
            nome.Required = true;
            nome.MaxLenght = this.StructureNetwork.FolderNameMaxLenght;
            nome.ContentType = NetTextBoxPlus.ContentTypes.Folder;
            FoldersLang.addField(nome);

            NetTextBoxPlus Descrizione = new NetTextBoxPlus("Descrizione", "Desc_FolderLang");
            Descrizione.Required = true;
            Descrizione.MaxLenght = 2000;
            Descrizione.Rows = 5;
            FoldersLang.addField(Descrizione);

            NetHiddenField lang = new NetHiddenField("Lang_FolderLang");
            lang.Value = this.StructureNetwork.DefaultLang.ToString();
            FoldersLang.addField(lang);

            NetExternalField folder = new NetExternalField("Folders", "Folder_FolderLang");
            FoldersLang.addExternalField(folder);
            #endregion

            NetForm form = new NetForm();
            form.CssClass = "newfolderform";

            form.AddSubmitButton = false;
            form.AddTable(this.FormTable);
            form.SubmitLabel = "Crea Cartella";

            contentDiv.Controls.Add(FormTable.getForm());

            string errors = "";

            #region Postback

            if (this.PageData.IsPostBack)
            {
                NetUtility.RequestVariable type = new NetUtility.RequestVariable("Tipo_Folder", HttpContext.Current.Request.Form);
                if (type.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                {
                    form.AddTable(FoldersLang);
                    contentDiv.Controls.Add(FoldersLang.getForm());
                    if (type.IntValue == 1)
                    {
                        datipercorso.InnerHtml += "<p>State per creare una nuova cartella di tipo Homepage.<br />";
                        datipercorso.InnerHtml += "Le cartelle Homepage ridefiniscono il layout del sito, dopo la creazione sarà quindi necessario configurare i moduli in modo che venga gestito il layout per le pagine a partire da questo percorso: \"" + this.Label + "\"</p>";
                    }

                    form.AddSubmitButton = true;
                }
            }

            if (this.PageData.IsPostBack && this.PageData.ValidPostBack && this.PageData.SubmitSource == "send")
            {

                errors = form.serializedValidation(HttpContext.Current.Request.Form);
                //errors += FoldersLang.validateForm(HttpContext.Current.Request.Form);
                if (errors == null || errors.Length == 0)
                {
                    int Tipo_Folder = int.Parse(HttpContext.Current.Request.Form["Tipo_Folder"]);
                    string Label = HttpContext.Current.Request.Form["Label_FolderLang"];
                    string Desc = HttpContext.Current.Request.Form["Desc_FolderLang"];
                    int stato = int.Parse(HttpContext.Current.Request.Form["Stato_Folder"]);
                    int rss = int.Parse(HttpContext.Current.Request.Form["Rss_Folder"]);
                    int layout = int.Parse(HttpContext.Current.Request.Form["Layout_Folder"]);

                    string template = HttpContext.Current.Request.Form["Template_Folder"];


                    //comportamento di default per showInTopMenu in relazione al tipo folder
                    int defaultValuaShowInTopMenu = 0;
                    int defaultValuaMenuContestuale = 0;

                    if (Tipo_Folder == 1 || Tipo_Folder == 2)
                    {
                        defaultValuaShowInTopMenu = 1;
                    }

                    int showInTopMenu =  (HttpContext.Current.Request.Form["ShowInTopMenu_Folder"]!=null) ? int.Parse(HttpContext.Current.Request.Form["ShowInTopMenu_Folder"]) : defaultValuaShowInTopMenu;                    
                    //dropdown menu contestuale
                    int menucontestuale = (HttpContext.Current.Request.Form["ShowMenu_Folder"]!=null) ? int.Parse(HttpContext.Current.Request.Form["ShowMenu_Folder"]) : defaultValuaMenuContestuale;
                    

                    NetUtility.RequestVariable CssClass = new NetUtility.RequestVariable("CssClass_Folder");
                    int RestrictedArea = int.Parse(HttpContext.Current.Request.Form["Restricted_Folder"]);

                    var NewFOlder = this.CreateFolder(null, Label, Desc, Tipo_Folder, stato, CssClass.StringValue, rss, layout, RestrictedArea, ((Tipo_Folder == 219) ? 5 : 0), showInTopMenu, menucontestuale, template);
                    if (NewFOlder != null)
                    {
                        string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha creato con successo la cartella '" + Label + "', relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);                    
                        if (Tipo_Folder == 1)
                            NetCms.GUI.PopupBox.AddSessionMessage("La cartella '" + Label + "' è stata creata con successo. Le consigliamo di configurare subito il layout di questa Homepage, sarà comuque possibile farlo anche successivamente.", PostBackMessagesType.Success, "default.aspx?folder=" + this.ID);
                        else
                            NetCms.GUI.PopupBox.AddSessionMessage("La cartella '" + Label + "' è stata creata con successo.", PostBackMessagesType.Success, "default.aspx?folder=" + this.ID);
                        
                    }
                    else
                    {
                        errors += "<strong>Non è possibile creare una cartella con questo nome n.b. è possibile che esista già un altra cartella con questo nome, o il nome è stato riservato dal sistema.";
                        string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a creare la cartella '" + Label + "', relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);                    
                    }

                }
                //this.StructureNetwork.Connection.Close();
                if (errors.Length > 0)
                    NetCms.GUI.PopupBox.AddMessage(errors, PostBackMessagesType.Error);

            }
          
            if (!this.PageData.IsPostBack) {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di creazione di una cartella, relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);                    
                        
            }

            #endregion


            //fieldset.Controls.Add(form.getForms("Dati nuova Cartella "));
            //fieldset.Controls.Add(form.getForms());
            HtmlGenericControl pButtons = new HtmlGenericControl("p");
            pButtons.Attributes["class"] = "buttonscontainer";

            Button bt = new Button();
            bt.Text = "Crea Cartella";
            bt.ID = "send";
            bt.CssClass = "button";
            bt.OnClientClick = "javascript: setSubmitSource('send')";
            pButtons.Controls.Add(bt);

            contentDiv.Controls.Add(pButtons);

            WebControl row = new WebControl(HtmlTextWriterTag.Div);
            row.CssClass = "row";

            row.Controls.Add(contentDiv);

            div.Controls.Add(fieldset);

            fieldset.Controls.Add(row);

            div.Controls.Add(fieldset);
            return div;
        }
        
        protected HtmlGenericControl SearchHeadRow
        {
            get
            {
                if (_SearchHeadRow == null)
                {
                    HtmlGenericControl th;
                    HtmlGenericControl tr = new HtmlGenericControl("tr");

                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "&nbsp;";
                    tr.Controls.Add(th);

                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "Nome";
                    tr.Controls.Add(th);

                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "Percorso";
                    th.Attributes["class"] = "FolderSearch_PercorsoCell";
                    tr.Controls.Add(th);

                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "Data di Creazione";
                    tr.Controls.Add(th);

                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "Tipo";
                    tr.Controls.Add(th);

                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "Stato";
                    tr.Controls.Add(th);

                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "Opzioni";
                    tr.Controls.Add(th);

                    _SearchHeadRow = tr;
                }
                return _SearchHeadRow;
            }
        }
        private HtmlGenericControl _SearchHeadRow;

        protected virtual HtmlGenericControl HeadRow
        {
            get
            {
                HtmlGenericControl th;
                HtmlGenericControl tr = new HtmlGenericControl("tr");

                th = new HtmlGenericControl("th");
                th.InnerHtml = "&nbsp;";
                th.InnerHtml += "<input type=\"hidden\" id=\"SortDocsBy_" + this.ID + "\" name=\"SortDocsBy_" + this.ID + "\" />";
                tr.Controls.Add(th);

                th = new HtmlGenericControl("th");
                th.Attributes["class"] = "nameColl columnLimit";
                th.InnerHtml = "<a class=\"HeadRowSortLink" + (ViewOrder == ViewOrders.LabelDesc ? " HeadRowSortLinkSelected" : " HeadRowSortLinkSelectedTop") + " \" href=\"javascript: setFormValue(" + (this.ViewOrder == ViewOrders.Label ? "6" : "1") + ",'SortDocsBy_" + this.ID + "',1);\">Nome</a>";
                tr.Controls.Add(th);

                th = new HtmlGenericControl("th");
                th.InnerHtml = "<a class=\"HeadRowSortLink" + (ViewOrder == ViewOrders.CreationDateDesc ? " HeadRowSortLinkSelected" : (ViewOrder == ViewOrders.CreationDate)? " HeadRowSortLinkSelectedTop" : "") + "\" href=\"javascript: setFormValue(" + (this.ViewOrder == ViewOrders.CreationDate ? "7" : "2") + ",'SortDocsBy_" + this.ID + "',1);\">Data</a>";
                tr.Controls.Add(th);

                th = new HtmlGenericControl("th");
                th.InnerHtml = "Tipo";
                tr.Controls.Add(th);

                th = new HtmlGenericControl("th");
                th.InnerHtml = "Stato";
                tr.Controls.Add(th);

                if (Users.AccountManager.CurrentAccount.GUI.FoldersGUI == NetCms.Users.SitemanagerUserGUI.FoldersGUIs.Separate)
                {
                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "Permessi";
                    tr.Controls.Add(th);

                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "Modifica";
                    tr.Controls.Add(th);

                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "Elimina";
                    tr.Controls.Add(th);
                }
                if (Users.AccountManager.CurrentAccount.GUI.FoldersGUI == NetCms.Users.SitemanagerUserGUI.FoldersGUIs.Colapsed)
                {
                    th = new HtmlGenericControl("th");
                    th.InnerHtml = "Opzioni";
                    //th.Attributes["colspan"] = "3";
                    tr.Controls.Add(th);
                }
                return tr;
            }
        }

        public virtual HtmlGenericControl getView()
        {
            this.DeletePostBack();
            if (this.PageData.PageName == "folder_del.aspx")
                return getDeleteFolderView();

            if (this.PageData.PageName == "folder_mod.aspx")
                return getModFolderView();
            if (this.PageData.PageName == "folder_move.aspx")
                return getMoveFolderView();
            /*
            if (this.PageData.SubmitSource == "SearchButton")
                return getSearchResultView();
            */
            HtmlGenericControl div = new HtmlGenericControl("div");
            //???
            string Utente = this.Account.Label;
            string logText = "L'utente " + Utente + "visita i contenuti della cartella " + this.Label + " (" + this.ID + ")";
            this.PageData.GuiLog.SaveLogRecord(logText, false);


            StructureTable table = new StructureTable(this);

            // serve una row con le info della cartella padre (se disponibile)

            getTableFolderView(table);
            getTableDocsView(table);
            getBackRow(table);
            //div.Controls.Add(Toolbar.getControl());
            div.Controls.Add(table);
           
            return div;
        }

        protected virtual void getTableDocsView(StructureTable table)
        {


            //Meccanismo di funzionamento modificato per risolvere grossi problemi prestazionali

            int numFolder = this.SubFolders.Where(x => !x.Hidden && x.NeedVisualization).Count();
            int numDoc = DocumentBusinessLogic.CountDocumentsByFolder(this.ID);
            int recordTot = numFolder + numDoc;

            table.RowCount = recordTot; 

            table.RowCount = recordTot;

            int recordsToSkip = numFolder;
            int recordsToTake = table.RecordPerPagina;
           
            int currentRecordTaken = (table.Page) * table.RecordPerPagina;
            if (currentRecordTaken >= recordsToSkip)
            {
                recordsToTake = currentRecordTaken - recordsToSkip;
                if (recordsToTake > table.RecordPerPagina)
                    recordsToTake = table.RecordPerPagina;

                recordsToSkip = ((table.Page - 1) * table.RecordPerPagina) - numFolder;
                
                if (recordsToSkip < 0) recordsToSkip = 0;


                //IEnumerable<Document> docs;
                //switch (this.ViewOrder)
                //{
                //    case NetCms.Structure.WebFS.ViewOrders.Label: docs = this.Documents.Cast<Document>().Where(x => x.Trash == 0 && x.AddDoc).OrderBy(x => x.Nome); break;
                //    case NetCms.Structure.WebFS.ViewOrders.CreationDate: docs = this.Documents.Cast<Document>().Where(x => x.Trash == 0 && x.AddDoc).OrderByDescending(x => x.Create); break;
                //    case NetCms.Structure.WebFS.ViewOrders.ModifyDate: docs = this.Documents.Cast<Document>().Where(x => x.Trash == 0 && x.AddDoc).OrderByDescending(x => x.Modify); break;
                //    default: docs = this.Documents.Cast<Document>().Where(x => x.Trash == 0 && x.AddDoc).OrderByDescending(x => x.Create); break;
                //}


                //var docsPaged = (from doc in docs
                //                 where doc != null && doc.AddDoc && doc.Trash == 0
                //                 select doc).Skip(recordsToSkip).Take(recordsToTake);

                //Rilevato bug sul penultimo parametro - viene passato recordsToSkip mentre la funzione si aspetta la pagina corrente della paginazione
                //IList<Document> docsPaged = DocumentBusinessLogic.GetDocumentsPagedByFolder(this.ID, this.ViewOrder, recordsToSkip, recordsToTake);

                IList<Document> docsPaged = DocumentBusinessLogic.GetDocumentsPagedByFolder(this.ID, this.ViewOrder, recordsToSkip, recordsToTake);

                foreach (Document doc in docsPaged)
                    table.addRow(doc.getRowView());

                // al momento non implementabile in quanto "doc" non è tread safe cioè ci sono degli accessi al HttpContex.current  che esiste solo nel tread principale 
                // il problema riguarda doc.Grant e CurrentActiveNetwork 
                /*  Parallel.ForEach(docsPaged, (doc) =>
                {
                    table.addRow(doc.getRowView());
                });*/
            }
        }


        #region Search

        public struct StandardSearchFields
        {
            private string _Titolo;
            public string Titolo
            {
                get { return _Titolo; }
                set { _Titolo = value; }
            }

            private string _Testo;
            public string Testo
            {
                get { return _Testo; }
                set { _Testo = value; }
            }

            private int _Tipo;
            public int Tipo
            {
                get { return _Tipo; }
                set { _Tipo = value; }
            }

            private string _Data;
            public string Data
            {
                get { return _Data; }
                set { _Data = value; }
            }

            private int _TipoData;
            public int TipoData
            {
                get { return _TipoData; }
                set { _TipoData = value; }
            }


            private int _OrderBy;
            public int OrderBy
            {
                get { return _OrderBy; }
                set { _OrderBy = value; }
            }

        }
        private StandardSearchFields _SearchFields;
        public virtual StandardSearchFields SearchFields
        {
            get { return _SearchFields; }
            set { _SearchFields = value; }
        }

        protected WebControl _SearchForm;
        public virtual WebControl SearchForm
        {
            get
            {
                if (_SearchForm == null)
                    _SearchForm = initSearchForm();
                return _SearchForm;
            }

        }
        public virtual WebControl initSearchForm()
        {
            string infotext = "";
            if (this.IsRootFolder) infotext = "Consente di cercare contenuti su tutta il Portale '" + this.StructureNetwork.Label + "'";
            else infotext = "Consente di cercare contenuti nella cartella corrente e nelle sue sottocartelle";
            DetailsSheet searchform = new DetailsSheet("Cerca", infotext);

            #region Titolo

            TextBox Titolo_Search = new TextBox();
            Titolo_Search.ID = "Titolo_Search";
            Titolo_Search.Columns = 100;
            
            DetailsSheetRow row = new DetailsSheetRow("<label for=\"Titolo_Search\">Testo</label>");
            row.ControlsToAppendRight.Add(Titolo_Search);
            searchform.AddRow(row);

            #endregion
           
            #region Tipo

            

            DropDownList tipoddl = new DropDownList();
            tipoddl.ID = "Tipo_Search";

            string tiposql = "SELECT SystemName_Applicazione,LabelPlurale_Applicazione,id_Applicazione FROM Applicazioni WHERE Enabled_Applicazione = 1 ";
            if (this.Tipo > 2)
            {
                tiposql += " AND ";
                tiposql += "(id_Applicazione = " + this.Tipo + " ";
                tiposql += "OR id_Applicazione = 4) ";                
            }

            tiposql += " ORDER BY LabelPlurale_Applicazione";
            DataTable dt = this.StructureNetwork.Connection.SqlQuery(tiposql);

            tipoddl.Items.Add(new ListItem("Tutti", "0"));
            foreach (DataRow drow in dt.Rows)
                tipoddl.Items.Add(new ListItem(drow["LabelPlurale_Applicazione"].ToString(), drow["id_Applicazione"].ToString()));

            row = new DetailsSheetRow("<label for=\"Tipo_Search\">Tipo Documento</label>");
            row.ControlsToAppendRight.Add(tipoddl);
            searchform.AddRow(row);

            #endregion

            #region Data

            TextBox dateBox = new TextBox();
            dateBox.ID = "Date_Search";
            dateBox.Columns = 10;
            dateBox.CssClass = "date";

            //Label lb = new Label();
            //lb.AssociatedControlID = "Date_Search";
            //lb.Text = "Data&nbsp;&nbsp;";

            DropDownList dateDdl = new DropDownList();
            dateDdl.ID = "DateType_Search";
            dateDdl.Items.Add(new ListItem("Data Esatta", "0"));
            dateDdl.Items.Add(new ListItem("Prima del", "1"));
            dateDdl.Items.Add(new ListItem("Dopo il", "2"));

            HtmlGenericControl date = new HtmlGenericControl("div");
            //date.Controls.Add(lb);
            date.Controls.Add(dateDdl);
            date.Controls.Add(dateBox);


            row = new DetailsSheetRow("<label for=\"Date_Search\">Data</label>");
            row.ControlsToAppendRight.Add(date);
            searchform.AddRow(row);


            #endregion

            #region Ordine

            DropDownList ddl = new DropDownList();
            ddl.ID = "OrderBy_Search";

            ddl.Items.Add(new ListItem("Nome", "0"));
            ddl.Items.Add(new ListItem("Percorso", "1"));
            ddl.Items.Add(new ListItem("Tipo", "2"));
            ddl.Items.Add(new ListItem("Data Crescente", "3"));
            ddl.Items.Add(new ListItem("Data Decrescente", "4"));

            row = new DetailsSheetRow("<label for=\"OrderBy_Search\">Ordina per</label>");
            row.ControlsToAppendRight.Add(ddl);
            searchform.AddRow(row);

            #endregion

            #region Button

            HtmlGenericControl searchdiv = new HtmlGenericControl("p");
            Button search = new Button();
            search.ID = "SearchButton";
            search.Text = "Cerca";
            search.OnClientClick = "setSubmitSource('SearchButton')";
            searchdiv.Controls.Add(search);

            row = new DetailsSheetRow("");
            row.ControlsToAppendRight.Add(searchdiv);
            searchform.AddRow(row);

            #endregion
            
            return searchform;

        }

        #endregion

        protected virtual void InitSearchFields()
        {
            _SearchFields = new StandardSearchFields();

            if (HttpContext.Current.Request.Form["Titolo_Search"] != null)
            {
                _SearchFields.Titolo = HttpContext.Current.Request.Form["Titolo_Search"].ToString().Trim().Replace("'", "''");
            }
            if (HttpContext.Current.Request.Form["Tipo_Search"] != null)
            {
                _SearchFields.Tipo = int.Parse(HttpContext.Current.Request.Form["Tipo_Search"].ToString().Trim().Replace("'", "''"));
            }
            if (HttpContext.Current.Request.Form["Date_Search"] != null)
            {
                _SearchFields.Data = HttpContext.Current.Request.Form["Date_Search"].ToString().Trim().Replace("'", "''");
            }
            if (HttpContext.Current.Request.Form["DateType_Search"] != null)
            {
                _SearchFields.TipoData = int.Parse(HttpContext.Current.Request.Form["DateType_Search"].ToString().Trim().Replace("'", "''"));
            }
            if (HttpContext.Current.Request.Form["OrderBy_Search"] != null)
            {
                _SearchFields.OrderBy = int.Parse(HttpContext.Current.Request.Form["OrderBy_Search"].ToString());
            }
        }
        public virtual WebControl getSearchResultView()
        {
            InitSearchFields();
            WebControl output = new WebControl(HtmlTextWriterTag.Div);

            StructureTable table = new StructureTable(this);
            table.PaginationKey = "SearchPage";
            HtmlGenericControl th = new HtmlGenericControl("th");
            th.InnerHtml = "Cartella";

            table.addTableHead(SearchHeadRow);

            this.addSearchResult(table, SearchFields);
            output.Controls.Add(table);
            return output;
        }

        public virtual HtmlGenericControl getDeleteFolderView()
        {
            if (this.Criteria[CriteriaTypes.Delete].Value == GrantsValues.Deny || this.ID == this.StructureNetwork.RootFolder.ID)
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
        
            HtmlGenericControl output = new HtmlGenericControl("div");

            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            output.Controls.Add(fieldset);

            HtmlGenericControl legend = new HtmlGenericControl("legend");
            fieldset.Controls.Add(legend);
            if (!this.ContainsLowLevelSystemFolders)
            {

                if (PageData.IsPostBack && PageData.SubmitSource == "DeleteFolder")
                {
                    HtmlGenericControl div = new HtmlGenericControl("div");
                    fieldset.Controls.Add(div);

                    //string Utente = this.Account.Label;
                    //string logText = "L'utente " + Utente + " elimina la cartella " + this.Nome + " (" + this.ID + ")";
                    //PageData.GuiLog.SaveLogRecord(logText);
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha eliminato la cartella '" + this.Nome + "' (id=" + this.ID + ") del portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);                    
                        
                    div = new HtmlGenericControl("div");
                    div.InnerHtml = "<a href=\"" + PageData.BackAddress + "\">Indietro</a>";
                    fieldset.Controls.Add(div);


                    // LA TABELLA homepages_moduli_documenti SEMBRA deprecata

                    //string sql = "DELETE homepages_moduli FROM homepages_moduli";
                    //sql += " INNER JOIN homepages_moduli_documenti ON id_Modulo = Modulo_ModuloDocumento";
                    //sql += " INNER JOIN Docs ON Doc_ModuloDocumento = id_Doc";
                    //sql += " INNER JOIN Folders ON id_Folder = Folder_Doc ";
                    //sql += " WHERE Tree_Folder LIKE '" + Tree + "%'";

                    //this.Network.Connection.Execute(sql);


                    //this.Network.Connection.Execute("DELETE homepages_moduli FROM homepages_moduli INNER JOIN homepages_moduli_applicativi ON id_Modulo = Modulo_ModuloApplicativo INNER JOIN Folders ON id_Folder = Folder_ModuloApplicativo WHERE Tree_Folder LIKE '" + Tree + "%'");
                    string sql = "SELECT Modulo_ModuloApplicativo FROM homepages_moduli_applicativi INNER JOIN Folders ON id_Folder = Folder_ModuloApplicativo WHERE Tree_Folder LIKE '" + Tree + "%'";
                    DataTable ModuliToDelete = this.StructureNetwork.Connection.SqlQuery(sql);
                    this.StructureNetwork.Connection.Execute("DELETE homepages_moduli_applicativi FROM homepages_moduli_applicativi INNER JOIN Folders ON id_Folder = Folder_ModuloApplicativo WHERE Tree_Folder LIKE '" + Tree + "%'");
                    foreach (DataRow row in ModuliToDelete.Rows)
                    {
                        this.StructureNetwork.Connection.Execute("DELETE FROM homepages_moduli WHERE id_Modulo = " + row["Modulo_ModuloApplicativo"]);
                    }

                    //this.Network.Connection.Execute("UPDATE Folders SET Trash_Folder = -" + this.Account.ID + " WHERE Tree_Folder LIKE '" + Tree + "%'");
                    //this.Network.Connection.Execute("UPDATE Folders SET Stato_Folder = 2 , Trash_Folder = " + this.Account.ID + " WHERE id_Folder = " + ID);

                    FolderBusinessLogic.TrashFolder(this);

                    legend.InnerHtml = "La Cartella è stata spostata nel cestino";

                    string pid = this.Parent.ID.ToString();
                    //PageData.CurentFolder = this.Parent;


                    //SISTEMARE CON LA CACHE 2° LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                    //this.OwnerCollection.Remove(this.ID);
                    (this.Parent as StructureFolder).Invalidate();


                    PageData.Redirect(this.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/default.aspx?folder=" + pid);

                }
                else
                {
                    legend.InnerHtml = "Sei sicuro di voler procedere con l'eliminazione della cartella?";

                    HtmlGenericControl div = new HtmlGenericControl("div");
                    div.Style.Add(HtmlTextWriterStyle.Padding, "10px");
                    div.InnerHtml = "<p>Eliminare una cartella comporta l'eliminazione di:</p>";
                    div.InnerHtml += "<ul class=\"list-unstyled\">";
                    div.InnerHtml += "<li>Tutti i documenti contenuti nella cartella</li>";
                    div.InnerHtml += "<li>Tutte le sottocartelle contenute nella cartella</li>";
                    div.InnerHtml += "<li>Tutti i documenti contenuti nelle sottocartelle contenute</li>";
                    div.InnerHtml += "</ul>";
                    fieldset.Controls.Add(div);

                    Button bt = new Button();
                    bt.ID = "DeleteFolder";
                    bt.CssClass = "btn btn-primary";
                    bt.Text = "Si sono sicuro";
                    bt.OnClientClick = "setSubmitSource('DeleteFolder')";
                    fieldset.Controls.Add(bt);
                                                          
                    fieldset.Controls.Add(new LiteralControl("<a class=\"btn btn-secondary\" href=\"" + PageData.BackAddress+ "\">Indietro</a>"));
                }
            }
            else
            {
                legend.InnerHtml = "Non è possibile procedere con l'eliminazione della cartella";

                HtmlGenericControl div = new HtmlGenericControl("div");
                div.InnerHtml += "Non è possibile eliminare questa cartella perchè contiene una cartella di sistema non registrata dal CMS";
                div.InnerHtml += "<br />";
                div.InnerHtml += "per ulteriori informazioni contattare l'assistenza";
                fieldset.Controls.Add(div);

                div = new HtmlGenericControl("div");
                div.InnerHtml = "<a href=\"" + PageData.BackAddress + "\">Indietro</a>";
                fieldset.Controls.Add(div);
            }

            return output;
        }

        private string _LabeledPath;
        public virtual string LabeledPath
        {
            get
            {
                if (_LabeledPath == null)
                {
                    StructureFolder dir = this;
                    while (dir != null)
                    {
                        _LabeledPath = dir.Label + (string.IsNullOrEmpty(_LabeledPath) ? "" :" / ") + _LabeledPath;
                        dir = dir.Parent as StructureFolder;
                    }
                }
                return _LabeledPath;
            }
        }

        //Costruisce il percorso corretto, comprensivo di link esclusivamente per la cartella corrente, da usare per la stampa del percorso dei documenti nelle ricerche
        private string _LabelLinkPath;
        public virtual string LabelLinkPath
        {
            get
            {
                if (_LabelLinkPath == null)
                {
                    StructureFolder dir = this;
                    while (dir != null)
                    {
                        string ThisFolderLabel;
                        string CurentUrl = this.PageData.Request.Url.ToString().ToLower();
                        if (dir != this)
                            ThisFolderLabel = dir.Label;
                        else
                            ThisFolderLabel = "<a class=\"Where_PathLink\" href=\"" + dir.BackofficeHyperlink + "\">" + dir.Label + "</a>";

                        _LabelLinkPath = ThisFolderLabel + " / " + _LabelLinkPath;
                        dir = dir.Parent as StructureFolder;
                    }
                }
                return _LabelLinkPath;
            }
        }

        //Costruisce il percorso corretto, comprensivo di link eccetto per la cartella corrente, da usare per le breadcrumbs
        private string _LabelLinkPathBreadCrumbs;
        public virtual string LabelLinkPathBreadCrumbs
        {
            get
            {
                if (_LabelLinkPathBreadCrumbs == null)
                {
                    StructureFolder dir = this;
                  
                    while (dir != null)
                    {

                        string CurentUrl = this.PageData.Request.Url.ToString().ToLower();
                        string ThisFolderLabel;
                        if (dir == this)
                            ThisFolderLabel = "<li class=\"lastB\">" + dir.Label + "</li>";
                        else
                            ThisFolderLabel = "<li><a class=\"Where_PathLink\" href=\"" + dir.BackofficeHyperlink + "\">" + dir.Label + "</a></li>";

                        _LabelLinkPathBreadCrumbs = ThisFolderLabel + _LabelLinkPathBreadCrumbs;
                        dir = dir.Parent as StructureFolder;
                    }
                    _LabelLinkPathBreadCrumbs = "<ul>" + _LabelLinkPathBreadCrumbs + "</ul>";
                }
                return _LabelLinkPathBreadCrumbs;
            }
        }


        public virtual NetCms.GUI.ToolbarButtonsCollection ToolbarButtons 
        {
            get
            {
                NetCms.GUI.ToolbarButtonsCollection Buttons = new ToolbarButtonsCollection();
                if (!this.IsSystemFolder)
                {
                    //Aggiunta cartella ai preferiti
                    if (this.Grant && !this.Starred)
                        Buttons.Add(new NetCms.GUI.ToolbarButton(this.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/default.aspx?folder=" + ID + "&star=" + ID, NetCms.GUI.Icons.Icons.Star, "Aggiungi ai Preferiti"));

                    if (NetCms.Configurations.Generics.EnableSematic)
                    {
                        IndexConfig currentconfig = CmsConfigs.BusinessLogic.ConfigsBusinessLogic.GetIndexConfig();
                        if (currentconfig.Mode == IndexConfig.IndexMode.solr)
                            Buttons.Add(new NetCms.GUI.ToolbarButton(this.StructureNetwork.Paths.AbsoluteAdminRoot + "/search/search_solr.aspx", NetCms.GUI.Icons.Icons.Find, "Ricerca"));
                        else
                            Buttons.Add(new NetCms.GUI.ToolbarButton(this.StructureNetwork.Paths.AbsoluteAdminRoot + "/search/search.aspx", NetCms.GUI.Icons.Icons.Find, "Ricerca"));
                    }

                    //Creazione nuova Cartella
                    //bool ShowNewFolderButton = false;
                    //for (int i = 0; !ShowNewFolderButton && i < this.Criteria.ApplicationsCount; i++)
                    //    ShowNewFolderButton = ShowNewFolderButton || this.Criteria.Application(NetCms.Structure.Grants.CriteriaTypes.NewFolder, i);
                    //if (ShowNewFolderButton)
                    if (this.Criteria[CriteriaTypes.NewFolder].Allowed)
                        Buttons.Add(new NetCms.GUI.ToolbarButton(this.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/" + "folder_new.aspx?folder=" + ID, NetCms.GUI.GuiUtility.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Folder_Add), "Nuova Cartella"));

                    //Eliminazione Cartella
                    if (this.Criteria[CriteriaTypes.Delete].Allowed && this != this.StructureNetwork.RootFolder && !this.IsSystemFolder)
                        Buttons.Add(new NetCms.GUI.ToolbarButton(this.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/folder_del.aspx?folder=" + ID, NetCms.GUI.GuiUtility.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Folder_Delete), "Elimina Cartella Corrente"));

                    //Prprietà Cartella
                    if (this.Criteria[CriteriaTypes.Modify].Allowed)// && this != this.StructureNetwork.RootFolder)
                        Buttons.Add(new NetCms.GUI.ToolbarButton(this.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/folder_mod.aspx?folder=" + ID, NetCms.GUI.Icons.Icons.Cog_Edit, "Proprietà Cartella Corrente"));

                    bool advancedPermission = this.Criteria[CriteriaTypes.Advanced].Allowed;
                    //Meta Cartella
                    if (advancedPermission && this != this.StructureNetwork.RootFolder && !this.ReviewEnabled || this.Criteria[CriteriaTypes.Publisher].Allowed)
                        Buttons.Add(new NetCms.GUI.ToolbarButton(this.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/folders_meta.aspx?folder=" + this.ID, NetCms.GUI.Icons.Icons.Page_White_Code, "Imposta Meta Tag"));

                    //Spostamento Cartella
                    if (advancedPermission && this != this.StructureNetwork.RootFolder && this.AllowMove)
                       Buttons.Add(new NetCms.GUI.ToolbarButton(this.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/folder_move.aspx?folder=" + ID, NetCms.GUI.GuiUtility.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Folder_Move), "Sposta Cartella Corrente"));

                    //Importazione contenuti - al momento solo per news e webdocs
                    if (advancedPermission && (this.RelativeApplication.ID == 2 || this.RelativeApplication.ID == 3 || this.RelativeApplication.ID == 8001 || this.RelativeApplication.ID == 8002))
                        Buttons.Add(new NetCms.GUI.ToolbarButton(this.StructureNetwork.Paths.AbsoluteAdminRoot + "/folders/import_content.aspx?folder=" + ID, NetCms.GUI.Icons.Icons.Folder_Table, "Importazione contenuti"));

                    //Esporta Cartella
                    if (/*NetCms.Configurations.Debug.GenericEnabled &&*/ advancedPermission)
                        Buttons.Add(new NetCms.GUI.ToolbarButton(this.StructureNetwork.Paths.AbsoluteAdminRoot + "/folders/export.aspx?folder=" + ID, NetCms.GUI.Icons.Icons.Arrow_Divide, "Esportazione e Backup"));

                    //Ordinamento Sottocartelle
                    if (advancedPermission)
                    {
                        bool haveDocPublished = false;
                        // il codice sottostante recupera il numero di documenti pubblicati nella cartella corrente
                        //if (this.HasDocuments && this.Documents.Count > 1)
                        //{
                        //    IList<Document> docs = DocumentBusinessLogic.FindDocumentsByFolderID(this.ID);
                            
                        //    if (docs != null)
                        //    {
                        //        // rimuovo dai documenti la pagina indice
                        //        Document index = docs.Where(x => x.Nome == "Pagina Indice").ElementAtOrDefault(0);
                        //        if (index != null)
                        //            docs.Remove(index);
                        //        // counto i documenti con la revisione pubblicata
                        //        int totDocs = docs.SelectMany(x => x.DocLangs).SelectMany(o => o.Revisioni).Count(y => y.Pubblicata == true);                                
                        //        haveDocPublished = (totDocs > 1);
                        //    }
                        //}
                        if (this.HasDocuments && this.Documents.Count > 1)
                        {
                            //IList<Document> docs = DocumentBusinessLogic.FindDocumentsByFolderID(this.ID);

                            //if (docs != null)
                            //{
                            //    // rimuovo dai documenti la pagina indice
                            //    Document index = docs.Where(x => x.Nome == "Pagina Indice").ElementAtOrDefault(0);
                            //    if (index != null)
                            //        docs.Remove(index);
                            //    // counto i documenti con la revisione pubblicata
                                int totDocs = DocumentBusinessLogic.CountDocumentsRecordsPublishedByFolder(this.ID);//docs.SelectMany(x => x.DocLangs).SelectMany(o => o.Revisioni).Count(y => y.Pubblicata == true);
                                haveDocPublished = (totDocs > 1);
                            //}
                        }


                        //La query sottostante controlla che ci siano almeno 2 sottocartelle.
                        //if (this.Network.Connection.SqlQuery("SELECT Name_Folder,id_Folder FROM Folders WHERE System_Folder = 0 AND Depth_Folder = " + (this.Depth + 1) + " AND Tree_Folder LIKE '" + this.Tree + "%' ORDER BY Tree_Folder").Rows.Count > 1)
                        if (this.HasSubFolders && (this.SubFolders.Where(x => x.Sistema == 0).Count() > 1) || (haveDocPublished))
                            Buttons.Add(new NetCms.GUI.ToolbarButton(this.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/order.aspx?folder=" + ID, NetCms.GUI.GuiUtility.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Arrow_Updown), "Ordina cartelle e documenti"));                            
                    }
                    //Permessi Cartella
                    if (this.EnableGrantsElab && this.Criteria[CriteriaTypes.Grants].Allowed /*&& this.Account.IsAdministrator*/)
                    {
                        Buttons.Add(new NetCms.GUI.ToolbarButton(this.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/grants/grants.aspx?folder=" + ID, NetCms.GUI.Icons.Icons.Key, "Imposta Permessi Cartella Corrente"));
                    }
                }

                return Buttons;
            }
        }

        private string _BackofficeHyperlink;
        public virtual string BackofficeHyperlink
        {
            get
            {
                if (_BackofficeHyperlink == null)
                {
                    _BackofficeHyperlink = PageData.Paths.AbsoluteNetworkRoot + "/cms/default.aspx?folder=" + this.ID;
                }
                return _BackofficeHyperlink;
            }
        }

        //Aggiunto il controllo sulla CurrentActiveNetwork nel caso in cui non ci sia un contesto Http (ad esempio per le WebApi)
        //Andando a leggere la network da quella di appartenenza della cartella, caso verosimile visto che non devo accedere a cartelle di altre networks
        private string _FrontendHyperlink;
        public virtual string FrontendHyperlink
        {
            get
            {
                if (_FrontendHyperlink == null)
                {
                    if (NetCms.Networks.NetworksManager.CurrentActiveNetwork != null)
                        _FrontendHyperlink = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + this.Path + "/";
                    else
                        _FrontendHyperlink = this.Network.Paths.AbsoluteFrontRoot + this.Path + "/";
                }
                return _FrontendHyperlink;
            }
        }

        protected virtual void getTableFolderView(StructureTable table)
        {
            if (!this.NeedVisualization)
                PageData.GoBack();

            HtmlGenericControl tr = new HtmlGenericControl("tr");

            #region Header

            table.addTableHead(HeadRow);

            #endregion

            HtmlGenericControl td;

            string AbsAdminRoot = this.StructureNetwork.Paths.AbsoluteAdminRoot;

            IList<StructureFolder> subFolders = this.SubFolders.Where(x => x.Hidden != true).Skip((table.Page - 1) * table.RecordPerPagina).Take(table.RecordPerPagina).ToList();
            //for (int i = 0; i < Folders.Count; i++)
            foreach (StructureFolder folder in /*(StructureFolderCollection)*/subFolders)
            {
                bool needVisualization = folder.NeedVisualization;
                if (!folder.Hidden && needVisualization)
                {
                    bool grant = needVisualization;//folder.Grant;
                    bool canManageGrants = folder.Criteria[CriteriaTypes.Grants].Allowed;
                    bool canModify = folder.Criteria[CriteriaTypes.Modify].Allowed;
                    bool canDelete = folder.Criteria[CriteriaTypes.Delete].Allowed;
                    bool IsSistemFolder = folder.IsSystemFolder;
                    bool IsRestricted = folder.Restricted;
                    int folderId = folder.ID;
                    string folderLabel = folder.Label;

                    string Link = BuildFolderLink(folder);
                    string Icon = "FS_Icon " + folder.Icon;

                    if (IsSistemFolder)
                        Icon += "_system";
                    else
                        if (IsRestricted)
                        Icon += "_reserved_area";

                    tr = new HtmlGenericControl("tr");
                    table.addRow(tr);

                    td = new HtmlGenericControl("td");
                    td.InnerHtml = "&nbsp;";
                    td.Attributes["class"] = Icon;
                    tr.Controls.Add(td);

                    td = new HtmlGenericControl("td");
                    if (IsRestricted)
                        td.Attributes["class"] = "reserved_area";
                    td.InnerHtml = Link;
                    tr.Controls.Add(td);

                    td = new HtmlGenericControl("td");
                    td.InnerHtml = "&nbsp;";
                    tr.Controls.Add(td);

                    td = new HtmlGenericControl("td");
                    if (NetCms.Configurations.Debug.GenericEnabled)
                        td.InnerHtml += "[" + folder.GetHashCode() + "] ";
                    td.InnerHtml += "Cartella " + folder.TypeName;

                    tr.Controls.Add(td);

                    td = new HtmlGenericControl("td");
                    if (IsSistemFolder)
                        td.InnerHtml = "Cartella di sistema";
                    else
                        td.InnerHtml = (folder.StatusLabel);// + " " + (folder.Parent != null ? "<br>Stato cartella padre: " + StatusLabels(((StructureFolder)folder.Parent).Stato) + "<br>":"") + " Stato cartella: " + StatusLabels(folder.Stato);
                    tr.Controls.Add(td);

                    if (this.Account.GUI.FoldersGUI == Users.SitemanagerUserGUI.FoldersGUIs.Separate)
                    {

                        td = new HtmlGenericControl("td");
                        if (grant && canManageGrants)//&& this.Account.IsAdministrator)
                        {
                            td.Attributes["class"] = "grant";
                            td.InnerHtml = "<a href=\"" + AbsAdminRoot + "/cms/grants/grants.aspx?folder=" + folderId + "\"><span>Imposta Permessi</span></a>";
                        }
                        else
                            td.InnerHtml = "&nbsp;";
                        tr.Controls.Add(td);

                        td = new HtmlGenericControl("td");

                        if (!IsSistemFolder && grant && canModify)
                        {
                            td.Attributes["class"] = "modify modify_folder";
                            td.InnerHtml = "<a href=\"" + AbsAdminRoot + "/cms/folder_mod.aspx?folder=" + folderId + "\"><span>Proprietà</span></a>";
                        }
                        else
                            td.InnerHtml = "&nbsp;";
                        tr.Controls.Add(td);


                        td = new HtmlGenericControl("td");
                        if (canDelete && !IsSistemFolder)
                        {
                            td.Attributes["class"] = "delete";
                            td.InnerHtml = "<a href=\"" + AbsAdminRoot + "/cms/folder_del.aspx?folder=" + folderId + "\"><span>Elimina</span></a>";
                        }
                        else
                            td.InnerHtml = "&nbsp;";

                        if (LanguageManager.Utility.Config.EnableMultiLanguage)
                        {
                            tr.Controls.Add(td);

                            IList<Language> enabledLangs = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages();
                            int countTrad = 0;
                            foreach (Language lang in enabledLangs)
                            {
                                if (folder.FolderLangs.Count(x => x.Lang == lang.ID) > 0)
                                    countTrad++;
                            }

                            td = new HtmlGenericControl("td");
                            if (countTrad == enabledLangs.Count)
                                td.Attributes["class"] = "icon_flag_all";
                            else
                                td.Attributes["class"] = "icon_flag";
                            td.InnerHtml = "<a href=\"#\"><span>Traduzioni (" + countTrad + "/" + enabledLangs.Count + ")</span></a>"; ;
                        }
                    }
                    if (this.Account.GUI.FoldersGUI == Users.SitemanagerUserGUI.FoldersGUIs.Colapsed)
                    {
                        td = new HtmlGenericControl("td");
                        //td.Attributes["colspan"] = "3";
                        td.Attributes["class"] = "ColapseActionCell";
                        td.InnerHtml = "";

                        string anchorModel = "<a class=\"ColapseActionLink {1}\" title=\"{2}\" onmouseover=\"setToolbarCaption('{0}')\" onmouseout=\"setToolbarCaption('')\" href=\"{3}\"><span>{0}</span></a>";

                        string cssclass, href, label, title;
                        if (folder.EnableGrantsElab && grant && canManageGrants)//&& this.Account.IsAdministrator)
                        {
                            label = "Gestisci Permessi Cartella";
                            title = "Gestisci Permessi Cartella '" + folderLabel + "'";
                            cssclass = "ColapseActionLink_Grants";
                            href = AbsAdminRoot + "/cms/grants/grants.aspx?folder=" + folderId;
                            td.InnerHtml += string.Format(anchorModel, label, cssclass, title, href);
                        }
                        else
                            td.InnerHtml += "<a class=\"ColapseActionLink\"><span>&nbsp;</span></a>";

                        if (!IsSistemFolder && grant && canModify)
                        {
                            label = "Modifica Proprietà Cartella";
                            title = "Modifica Proprietà Cartella '" + folderLabel + "'";
                            cssclass = "ColapseActionLink_Prop";
                            href = AbsAdminRoot + "/cms/folder_mod.aspx?folder=" + folderId;
                            td.InnerHtml += string.Format(anchorModel, label, cssclass, title, href);
                        }
                        else
                            td.InnerHtml += "<a class=\"ColapseActionLink\"><span>&nbsp;</span></a>";

                        //td.InnerHtml += "<a class=\"ColapseActionLink\"><span>&nbsp;</span></a>";

                        if (canDelete && !IsSistemFolder)
                        {
                            label = "Elimina Cartella";
                            title = "Elimina Cartella '" + folderLabel + "'";
                            cssclass = "ColapseActionLink_Delete";
                            href = AbsAdminRoot + "/cms/folder_del.aspx?folder=" + folderId;
                            td.InnerHtml += string.Format(anchorModel, label, cssclass, title, href);
                        }
                        else
                            td.InnerHtml += "<a class=\"ColapseActionLink\"><span>&nbsp;</span></a>";

                        if (LanguageManager.Utility.Config.EnableMultiLanguage)
                        {
                            IList<Language> enabledLangs = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages();
                            int countTrad = 0;
                            foreach (Language lang in enabledLangs)
                            {
                                if (folder.FolderLangs.Count(x => x.Lang == lang.ID) > 0)
                                    countTrad++;
                            }

                            label = "Traduzioni (" + countTrad + "/" + enabledLangs.Count + ")";
                            title = "Traduzioni cartella '" + folderLabel + "': " + countTrad + "/" + enabledLangs.Count;
                            if (countTrad == enabledLangs.Count)
                                cssclass = "ColapseActionLink_MultiLang_All";
                            else
                                cssclass = "ColapseActionLink_MultiLang";
                            href = "#";
                            td.InnerHtml += string.Format(anchorModel, label, cssclass, title, href); ;                            
                        }
                        else
                            td.InnerHtml += "<a class=\"ColapseActionLink\"><span>&nbsp;</span></a>";

                        tr.Controls.Add(td);
                    }

                    tr.Controls.Add(td);
                                        
                }
            }
        }

        protected virtual string BuildFolderLink(StructureFolder folder)
        {
            //
            /*
            string details = "<strong>" + folder.Label + "</strong><br />";
            details += "<p><em>" + folder.Descrizione.Replace(@"
", " ") + "</em></p>";//ATTENZIONE L'accapo soprastante non è un errore di battitura ma una necessità in quanto rappresenta il carattere accapo
            details += "Percorso: " + folder.LabeledPath + "<br />";
            details += "Tipo: " + folder.TypeName + "<br />";
            int max = 10;
            int tot = 0;

            string Contents = "Contenuto: <br />";
            int i;
            for (i = 0; i < folder.SubFolders.Count && tot < max; i++)
            {
                if (!folder.SubFolders[i].Hidden)
                {
                    tot++;
                    Contents += "&nbsp;+&nbsp;" + folder.SubFolders[i].Label + "<br />";
                }
            }
            details += "Num. Sottocartelle: " + tot + "<br />";
            for (i = 0; i < folder.Documents.Count && tot < max; i++, tot++)
            {
                Contents += "&nbsp;-&nbsp;" + folder.Documents[i].Label + "<br />";
            }

            if (tot == 0)
            {
                Contents += "Cartella Vuota <br />";
            }

            if (tot >= max)
            {
                Contents += "...";
            }
            if (this.Criteria[CriteriaTypes.Advanced])
            {
                details += "ClasseCss: " + folder.CssClass + "<br />";
                details += "Titolo del Frontend : " + (folder.AddKeywordsToTitle ? "Keywords" : "Path") + "<br />";

            }
            details += "Num. Documenti: " + folder.Documents.Count + "<br />";

            details += "Num. Documenti: " + folder.Documents.Count + "<br />";
            details += Contents;

            details = details.Replace("'", "\\\'");
            details = details.Replace("\"", "&quot;");
            //details = details.Replace("\"", "\'");

            string title = folder.Label + " - ";
            title += "Num. Documenti" + folder.Documents.Count + " - ";
            title += "Num. Sottocartelle" + folder.SubFolders.Count + "<br />";

            string image = "";
            //if (this.Account.GUI.PreviewType == Users.SitemanagerUserGUI.PreviewTypes.Static)
              //  image =NetCms.GUI.GuiUtility.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Folder_Popup);*/
            string Link = "<a href=\"default.aspx?folder=" + folder.ID + "\">" + folder.Label + "</a>";

            return Link;
        }
        public virtual HtmlGenericControl getModFolderView()
        {          
            if (!this.Grant || this.Criteria[CriteriaTypes.Modify].Value == GrantsValues.Deny)
                PageData.GoToErrorPage();

            #region Toolbar
            PageData.InfoToolbar.Title = "Modifica Proprietà Cartella '" + this.Nome + "'";
            PageData.InfoToolbar.Icon = "Folder";
            PageData.InfoToolbar.ShowFolderPath = true;
            #endregion

            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "container-fluid";

            //string Utente = this.Account.Label;
            //string logText = "L'utente " + Utente + " vuole aggiornare le proprietà per la cartella " + this.Network.CurrentFolder.Label + " (" + Network.CurrentFolder.ID + ")";
            //PageData.GuiLog.SaveLogRecord(logText);

            NetFormTable table = new NetFormTable("Folders", "id_Folder", this.StructureNetwork.Connection, this.ID);
            table.CssClass = "netformtable_folder";

            NetFormTable lang = new NetFormTable("FoldersLang", "id_FolderLang", this.StructureNetwork.Connection, this.FolderLangs.ElementAt(0).ID);
            lang.CssClass = "netformtable_lang";

            NetTextBoxPlus nome = new NetTextBoxPlus("Nome", "Label_FolderLang");
            nome.MaxLenght = this.StructureNetwork.FolderNameMaxLenght;
            nome.Required = true;
            nome.ContentType = NetTextBoxPlus.ContentTypes.Folder;
            if (this.Parent != null)
                lang.addField(nome);

            NetDropDownList tipo = new NetDropDownList("Tipo", "Tipo_Folder");
            if (this.Criteria[CriteriaTypes.FoldersVisibility].Allowed)
            {
                NetDropDownList Stato = new NetDropDownList("Stato", "Stato_Folder");
                //for (int i = 0; i < 3; i++)
                //    Stato.addItem(StructureFolder.StatusLabels(i), i.ToString());
                foreach (StructureFolder.FolderStates stato in Enum.GetValues(typeof(StructureFolder.FolderStates)))
                {
                    int k = (int)Enum.Parse(typeof(StructureFolder.FolderStates), stato.ToString());
                    Stato.addItem(EnumUtilities.GetEnumDescription(stato), k.ToString());
                }
                Stato.Required = true;
                if (this.Parent != null)
                    table.addField(Stato);
            }

            NetDropDownList restricted = new NetDropDownList("Area Riservata", "Restricted_Folder");
            restricted.ClearList();
            restricted.AutoPostBack = true;

            if (this.IsRootFolder)
            {
                restricted.addItem("NO (Cartella accessibile a tutto il Pubblico)", "0");
                restricted.addItem("Generale (Cartella accessibile da tutto il pubblico autenticato)", "-1");
                restricted.Required = true;
                table.addField(restricted);
            }
            else if (this.Parent != null)
            {
                if ((this.Parent as StructureFolder).Restricted)
                {
                    NetHiddenField restrictedforced = new NetHiddenField("Restricted_Folder");
                    restrictedforced.Value = "0";
                    table.addField(restrictedforced);
                }
                else
                {
                    restricted.addItem("NO (Cartella accessibile a tutto il Pubblico)", "0");
                    restricted.addItem("Generale (Cartella accessibile da tutto il pubblico autenticato)", "-1");
                    restricted.Required = true;
                    table.addField(restricted);
                }
            }

            //if (PageData.IsPostBack && restricted.PostBackValue == "-1")
            //{
            //    NetDropDownList loginType = new NetDropDownList("Modalita di accesso", "LoginType");
            //    loginType.Required = true;
            //    restricted.addItem("Standard", "1");
            //    restricted.addItem("SSO", "2");
            //}


            NetTextBoxPlus Descrizione = new NetTextBoxPlus("Descrizione", "Desc_FolderLang");
            Descrizione.Required = true;
            Descrizione.MaxLenght = 2000;
            Descrizione.Rows = 5;
            lang.addField(Descrizione);

            NetImagePicker coverImageStart = new NetImagePicker("Immagine di copertina", "CoverImage_Folder", this.PageData);
            coverImageStart.CurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID;
            table.addField(coverImageStart);

            NetDropDownList TitleOffset = new NetDropDownList("Titolo del Frontend", "TitleOffset_Folder");
            TitleOffset.ClearList();
            TitleOffset.addItem("Mostra solo il titolo", "0");
            TitleOffset.addItem("Aggiungi path in coda al titolo", "-1");
            TitleOffset.addItem("Aggiungi KEYWORDS in coda al titolo", "1");
            TitleOffset.addItem("Mostra solo KEYWORDS", "2");
            TitleOffset.Required = true;
            table.addField(TitleOffset);

            NetTextBoxPlus cssclass = new NetTextBoxPlus("Classe Css", "CssClass_Folder");
            cssclass.ContentType = NetTextBoxPlus.ContentTypes.NormalText;

            if (this.Criteria[CriteriaTypes.Advanced].Allowed && this.Parent != null)
            {
                

                table.addField(cssclass);
            }

            //if (PageData.CurrentConfig.Layout != CmsConfigs.Model.Config.LayoutType.Responsive)
            if (NetCms.Configurations.ThemeEngine.Status == Configurations.ThemeEngine.ThemeEngineStatus.on)
            {
                if (this.StructureNetwork.CurrentFolder.Tipo == 3 || //news               
                this.StructureNetwork.CurrentFolder.Tipo == 8 || // stampa
                this.StructureNetwork.CurrentFolder.Tipo == 7) // eventi
                {
                    NetDropDownList Template = new NetDropDownList("Template pagina", "Template_Folder");                   
                    foreach (string tpl in GlobalConfig.CurrentConfig.CurrentTheme.GetTplElement(new List<Themes.Model.TemplateElement.TplTypes>() 
                                                                                    {
                                                                                        Themes.Model.TemplateElement.TplTypes.contentapplicativo
                                                                                    })) 
                    {
                        Template.addItem(tpl, tpl.ToLower());
                    }

                    Template.Required = true;

                    table.addField(Template);                   
                }
                if (this.StructureNetwork.CurrentFolder.Tipo == 4)
                {
                    NetDropDownList ShowMenu = new NetDropDownList("Mostra menu contestuale", "ShowMenu_Folder");
                    ShowMenu.ClearList();
                    ShowMenu.addItem("No", "0");
                    ShowMenu.addItem("Si", "1");
                    table.addField(ShowMenu);
                }

                NetDropDownList ShowInTopMenu = new NetDropDownList("Mostra nel topmenu", "ShowInTopMenu_Folder");
                ShowInTopMenu.ClearList();
                ShowInTopMenu.addItem("Si", "1");
                ShowInTopMenu.addItem("No", "0");                
                table.addField(ShowInTopMenu);
            }
            else
            {
                if (this.StructureNetwork.CurrentFolder.Tipo == 3 || //news               
                this.StructureNetwork.CurrentFolder.Tipo == 8 || //stampa
                 this.StructureNetwork.CurrentFolder.Tipo == 6 || //bandi
                this.StructureNetwork.CurrentFolder.Tipo == 10 || //faq
                this.StructureNetwork.CurrentFolder.Tipo == 11 || //imagegallery
                this.StructureNetwork.CurrentFolder.Tipo == 14)   // rassegna                         
                {
                    NetDropDownList TipoHome = new NetDropDownList("Tipo pagina principale", "HomeType_Folder");
                    TipoHome.ClearList();
                    TipoHome.addItem("Normale", "0");
                    TipoHome.addItem("Mostra descrizione", "-2");
                    TipoHome.Required = true;
                    table.addField(TipoHome);
                }
            }            

            if (this.StructureNetwork.CurrentFolder.RssValue != RssStatus.SystemError)
            {
                NetDropDownList rss = new NetDropDownList("Rss", "Rss_Folder");
                rss.addItem("Eredita", "0");
                rss.addItem("Abilitati", "1");
                rss.addItem("Disabilitati", "2");
                rss.Required = true;

                table.addField(rss);
            }
            NetDropDownList layout = new NetDropDownList("Layout di colonne e Piè di pagina (Attenzione la visualizzazione finale del layout dipende comunque da come sono state impostate le colonne nella homepage relativa a questa cartella.)", "Layout_Folder");
            if (this.StructureNetwork.CurrentFolder.FrontLayout != FrontLayouts.SystemError) //&& this.Parent != null && this.Tipo != 1 
            {
                layout.ClearList();
                layout.addItem("Eredita", "0");
                layout.addItem("Mostra Tutte", "8");
                layout.addItem("Nascondi Tutte", "7");
                layout.addItem("Mostra solo Sinistra e Destra", "1");
                layout.addItem("Mostra solo Sinistra e Footer", "2");
                layout.addItem("Mostra solo Destra e Footer", "3");
                layout.addItem("Mostra solo Sinistra", "4");
                layout.addItem("Mostra solo Destra", "5");
                layout.addItem("Mostra solo Footer", "6");
                layout.Required = true;

                table.addField(layout);
            }

            NetDropDownList grants4docs = new NetDropDownList("Elabora permessi per i Documenti di questa cartella", "Disable_Folder");

            if (this.Parent != null)
            {
                grants4docs.ClearList();
                grants4docs.addItem("Elabora", "0");
                grants4docs.addItem("Non Elaborare", "2");
                grants4docs.Required = true;

                table.addField(grants4docs);
            }

            NetForm form = new NetForm();

            if (this.Parent != null)
                form.AddTable(lang);

            //Inserimento codice per il supporto multilingua
            #region Supporto multilingua
            WebControl langContols = new WebControl(HtmlTextWriterTag.Div);
            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                IList<LanguageManager.Model.Language> languages = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages();
                if (languages != null && languages.Count > 0)
                {
                    string insertAfterId = "FoldersLang";
                    foreach (LanguageManager.Model.Language l in languages.Where(x=>x.Default_Lang == 0))
                    {
                        #region Lang
                        Collapsable colTrad = new Collapsable("Traduzione " + l.Nome_Lang);
                        colTrad.ID = "FoldersLang#" + l.LangTwoLetterCode;
                        colTrad.StartCollapsed = true;

                        //NetFormTable FoldersLang = new NetFormTable("FoldersLang_" + l.LangTwoLetterCode, "id_FolderLang_" + l.LangTwoLetterCode);

                        NetTextBoxPlus nomeTrad = new NetTextBoxPlus("Nome " + l.Nome_Lang, "Label_FolderLang_" + l.LangTwoLetterCode);
                        nomeTrad.Required = false;
                        nomeTrad.MaxLenght = this.StructureNetwork.FolderNameMaxLenght;
                        nomeTrad.ContentType = NetTextBoxPlus.ContentTypes.Folder;
                        
                        //FoldersLang.addField(nomeTrad);

                        NetTextBoxPlus DescrizioneTrad = new NetTextBoxPlus("Descrizione " + l.Nome_Lang, "Desc_FolderLang_" + l.LangTwoLetterCode);
                        DescrizioneTrad.Required = false;
                        DescrizioneTrad.MaxLenght = 2000;
                        DescrizioneTrad.Rows = 5;

                        if (this.FolderLangs.Count(x => x.Lang == l.ID) > 0) {
                            FolderLang curLang = this.FolderLangs.Where(x => x.Lang == l.ID).First();
                            nomeTrad.Value = curLang.Label;
                            DescrizioneTrad.Value = curLang.Descrizione;
                        }

                        colTrad.AppendControl(nomeTrad.getControl());
                        colTrad.AppendControl(DescrizioneTrad.getControl());

                        langContols.Controls.Add(colTrad);

                        //FoldersLang.addField(DescrizioneTrad);

                        //NetHiddenField langTrad = new NetHiddenField("Lang_FolderLang_" + l.LangTwoLetterCode);
                        //langTrad.Value = l.ID.ToString();
                        //FoldersLang.addField(langTrad);

                        //NetHiddenField folderTrad = new NetHiddenField("Folder_FolderLang_" + l.LangTwoLetterCode);
                        //folderTrad.Value = this.ID.ToString();
                        //FoldersLang.addField(folderTrad);

                        #endregion

                        form.ControlToInsertAfter.Add(new KeyValuePair<string,WebControl>(insertAfterId, colTrad));
                        insertAfterId = colTrad.ID;
                    }
                }
            }
            #endregion 

            form.AddTable(table);
            form.SubmitLabel = "Salva";

            string errors = "";

            if (PageData.IsPostBack && PageData.SubmitSource == "send")
            {
                if (this.Parent != null)
                {
                    errors = lang.validateForm(HttpContext.Current.Request.Form);
                    errors += table.validateForm(HttpContext.Current.Request.Form);

                    //Controllo che non esistano documenti nella cartella genitore con lo stesso nome nuovo
                    bool notValidName = (this.Parent as StructureFolder).SubFolders
                                     .Count(folder => folder.Label.ToLower().Trim() == nome.RequestVariable.StringValue.ToLower().Trim() && folder.ID != this.ID) > 0;
                    if(notValidName)
                    {
                        string messaggio = "Non è possibile creare una cartella con questo nome in quanto esiste già un altra cartella con questo nome.";
                        if (errors != null && errors.Length > 0)
                            errors = errors.Replace("<ul>", "<ul><li>" + messaggio + "</li>");
                        else
                            errors = "<div class=\"form-error\"><ul><li>" + messaggio + "</li></ul></div>";
                    }
                }
                if (LanguageManager.Utility.Config.EnableMultiLanguage)
                {
                    IList<LanguageManager.Model.Language> languages = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages();
                    if (languages != null && languages.Count > 0)
                    {
                        foreach (LanguageManager.Model.Language l in languages.Where(x => x.Default_Lang == 0))
                        {
                            string nameTrad = HttpContext.Current.Request.Form["Label_FolderLang_" + l.LangTwoLetterCode].ToString();
                            string descTrad = HttpContext.Current.Request.Form["Desc_FolderLang_" + l.LangTwoLetterCode].ToString();

                            if (!string.IsNullOrEmpty(nameTrad) && !string.IsNullOrEmpty(descTrad))
                            {
                                //Verifico che il nome non contenga lo slash o altri caratteri speciali che manderebbero in errore l'url generato al volo
                                if (NetUtility.NameControls.CheckName(nameTrad))
                                {
                                    //Entrambi popolati, il nome non contiene lo slash, posso inserire la traduzione
                                    //Se Esiste modifico il record
                                    if (this.FolderLangs.Count(x => x.Lang == l.ID) > 0)
                                    {
                                        FolderLang langToModify = this.FolderLangs.First(x => x.Lang == l.ID);
                                        langToModify.Label = nameTrad;
                                        langToModify.Descrizione = descTrad;

                                        FolderBusinessLogic.SaveOrUpdateFolderLang(langToModify);
                                    }
                                    else
                                    {
                                        //Se non esiste lo creo
                                        FolderLang langToCreate = new FolderLang();
                                        langToCreate.Label = nameTrad;
                                        langToCreate.Descrizione = descTrad;
                                        langToCreate.Folder = this;
                                        langToCreate.Lang = l.ID;
                                        this.FolderLangs.Add(langToCreate);

                                        FolderBusinessLogic.SaveOrUpdateFolder(this);
                                    }
                                }
                                else
                                {
                                    //Mostro un messaggio per avvisare l'utente che il nome non può contenere caratteri speciali
                                    string messaggio = "Il campo '<strong>Nome</strong>' per la lingua " + l.Nome_Lang + " non può contenere caratteri speciali";
                                    if (errors != null && errors.Length > 0)
                                        errors = errors.Replace("<ul>", "<ul><li>" + messaggio + "</li>");
                                    else
                                        errors = "<div class=\"form-error\"><ul><li>" + messaggio + "</li></ul></div>";
                                }
                            }
                            else
                            {
                                //Se solo uno è popolato mostro un messaggio di errore affinchè vengano valorizzati entrambi
                                if (!string.IsNullOrEmpty(nameTrad) || !string.IsNullOrEmpty(descTrad))
                                {
                                    string messaggio = "I campi '<strong>Nome</strong>' e '<strong>Descrizione</strong>' devono essere compilati entrambi o nessuno per la lingua " + l.Nome_Lang;
                                    if (errors != null && errors.Length > 0)
                                        errors = errors.Replace("<ul>", "<ul><li>" + messaggio + "</li>");
                                    else
                                        errors = "<div class=\"form-error\"><ul><li>" + messaggio + "</li></ul></div>";
                                }
                                else
                                {
                                    //Nessuno dei due è popolato, rimuovo la traduzione
                                    if (this.FolderLangs.Count(x => x.Lang == l.ID) > 0)
                                    {
                                        FolderLang langToDelete = this.FolderLangs.First(x => x.Lang == l.ID);
                                        this.FolderLangs.Remove(langToDelete);
                                        langToDelete.Folder = null;
                                        FolderBusinessLogic.DeleteFolderLang(langToDelete);
                                    }
                                }
                            }                 
                        }
                    }
                }

                if (errors == "" || this.Parent == null)
                {
                    lang.updateQuery(HttpContext.Current.Request.Form);

                    string label = HttpContext.Current.Request.Form["Label_FolderLang"];

                    if (this.Parent != null)
                    {
                        if (this.Criteria[CriteriaTypes.FoldersVisibility].Allowed && HttpContext.Current.Request.Form["Stato_Folder"] != null && HttpContext.Current.Request.Form["Stato_Folder"].ToString() != "")
                            this.Stato = int.Parse(HttpContext.Current.Request.Form["Stato_Folder"].ToString());                        
                    }
                    
                    this.RestrictedInt = int.Parse(HttpContext.Current.Request.Form["Restricted_Folder"].ToString());

                    NetUtility.RequestVariable rss = new NetUtility.RequestVariable("Rss_Folder", NetUtility.RequestVariable.RequestType.Form);
                    if (!rss.IsValidInteger ||
                        (rss.IntValue == 2) ||
                        (rss.IntValue == 0 && this.StructureNetwork.CurrentFolder.Parent != null && (this.StructureNetwork.CurrentFolder.Parent as StructureFolder).EnableRss)
                        )
                    {
                        FileInfo rssFile = new FileInfo(HttpContext.Current.Server.MapPath(this.StructureNetwork.Paths.AbsoluteFrontRoot + this.StructureNetwork.CurrentFolder.Path + "/rss.xml"));
                        if (rssFile.Exists) rssFile.Delete();
                    }
                    else this.StructureNetwork.CurrentFolder.BuildRssRecursive();

                    //rss era considerato String e si verificava se era una stringa valida, perchè??? Prima sembra essere trattato come int
                    if (this.StructureNetwork.CurrentFolder.RssValue != RssStatus.SystemError && rss.IsValidInteger)
                        this.Rss = rss.IntValue;

                    if (this.StructureNetwork.CurrentFolder.FrontLayout != FrontLayouts.SystemError && layout.RequestVariable.IsValidInteger) //this.Parent != null &&  this.Tipo != 1
                        this.Layout = layout.RequestVariable.IntValue;

                    if (this.Parent != null && this.Criteria[CriteriaTypes.Advanced].Allowed)
                    {
                        if (TitleOffset.RequestVariable.IsValidInteger)
                            this.TitleOffset = TitleOffset.RequestVariable.IntValue;
                        
                        this.CssClass = cssclass.RequestVariable.StringValue;
                    }
                    if (this.Parent != null)
                        this.Disable = grants4docs.RequestVariable.IntValue;

                    //NetUtility.RequestVariable hometype = new NetUtility.RequestVariable("HomeType_Folder", PageData.Request.Form);
                    //if (hometype.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                    //    this.HomeType = hometype.IntValue;


                    //coverimage
                    NetUtility.RequestVariable coverImg = new NetUtility.RequestVariable("CoverImage_Folder", PageData.Request.Form);
                    if (coverImg.IsValid(NetUtility.RequestVariable.VariableType.String))
                        this.CoverImage = coverImg.StringValue;
                    else
                        this.CoverImage = null;

                    //template
                    NetUtility.RequestVariable tpl = new NetUtility.RequestVariable("Template_Folder", PageData.Request.Form);
                    if (tpl.IsValid(NetUtility.RequestVariable.VariableType.String))
                        this.Template = tpl.StringValue;

                    if (HttpContext.Current.Request.Form["ShowMenu_Folder"] != null)
                        this.ShowMenu = int.Parse(HttpContext.Current.Request.Form["ShowMenu_Folder"].ToString());
                    if (HttpContext.Current.Request.Form["ShowInTopMenu_Folder"] != null)
                        this.ShowInTopMenu = int.Parse(HttpContext.Current.Request.Form["ShowInTopMenu_Folder"].ToString());

                    FileSystemBusinessLogic.FolderBusinessLogic.SaveOrUpdateFolder(this);


                    //string sql = "";
                    //sql += "UPDATE Folders SET ";
                    //if (this.Parent != null)
                    //{
                    //    if (this.Criteria[CriteriaTypes.FoldersVisibility] && HttpContext.Current.Request.Form["Stato_Folder"] != null && HttpContext.Current.Request.Form["Stato_Folder"].ToString() != "")
                    //        sql += " Stato_Folder = " + HttpContext.Current.Request.Form["Stato_Folder"] + ",";

                    //    sql += " Restricted_Folder = " + HttpContext.Current.Request.Form["Restricted_Folder"] + ", ";
                    //}
                    //NetUtility.RequestVariable rss = new NetUtility.RequestVariable("Rss_Folder", NetUtility.RequestVariable.RequestType.Form);
                    //if (!rss.IsValidInteger ||
                    //    (rss.IntValue == 2) ||
                    //    (rss.IntValue == 0 && this.Network.CurrentFolder.Parent != null && this.Network.CurrentFolder.Parent.EnableRss)
                    //    )
                    //{
                    //    FileInfo rssFile = new FileInfo(HttpContext.Current.Server.MapPath(this.Network.Paths.AbsoluteFrontRoot + this.Network.CurrentFolder.Path + "/rss.xml"));
                    //    if (rssFile.Exists) rssFile.Delete();
                    //}
                    //else this.Network.CurrentFolder.BuildRssRecursive();

                    //if (this.Network.CurrentFolder.RssValue != RssStatus.SystemError && rss.IsValidString)
                    //    sql += " Rss_Folder = " + rss.StringValue;

                    //if (this.Parent != null && this.Network.CurrentFolder.FrontLayout != FrontLayouts.SystemError && this.Tipo != 1 && layout.RequestVariable.IsValidString)
                    //    sql += ", Layout_Folder = " + layout.RequestVariable.StringValue;

                    //if (this.Parent != null && this.Criteria[CriteriaTypes.Advanced])
                    //{
                    //    if (TitleOffset.RequestVariable.IsValidString)
                    //        sql += ", TitleOffset_Folder = " + TitleOffset.RequestVariable.StringValue;
                    //    if (cssclass.RequestVariable.IsValidString)
                    //        sql += ", CssClass_Folder = '" + cssclass.RequestVariable.StringValue + "' ";
                    //}
                    //if (this.Parent != null)
                    //    sql += ", Disable_Folder = " + grants4docs.RequestVariable.StringValue;

                    //sql += " WHERE id_Folder = " + this.ID + " ";
                    //this.Network.Connection.Execute(sql);

                    //SISTEMARE CON CACHE 2° LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                    this.Invalidate();
                    if (this.Parent != null)
                        ((StructureFolder)this.Parent).Invalidate();                  

                    errors = "Aggiornamento Effettuato";
                    
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha mofificato le proprietà della cartella '" + this.StructureNetwork.CurrentFolder.Label + "' (id=" + this.StructureNetwork.CurrentFolder.ID + ") del portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    NetCms.GUI.PopupBox.AddSessionMessage(errors, PostBackMessagesType.Success, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL()); 
                }
                else
                {
                    NetCms.GUI.PopupBox.AddSessionMessage(errors, PostBackMessagesType.Error);
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + "non è riuscito a modificare le proprietà della cartella '" + this.StructureNetwork.CurrentFolder.Label + "' (id=" + this.StructureNetwork.CurrentFolder.ID + ") del portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);                    
            
                }
            }
            else {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di modifica delle proprietà della cartella '" + this.StructureNetwork.Label + "' (id=" + this.StructureNetwork.CurrentFolder.ID + ") del portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);                                
            }

            //div.Controls.Add(form.getForms("Modifica Proprietà Cartella '" + this.Nome + "'"));
            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");

            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerText = "Proprietà Cartella ";
            fieldset.Controls.Add(legend);

            HtmlGenericControl contentDiv = new HtmlGenericControl("div");
            contentDiv.Attributes["class"] = "contentForm foldermod col-md-12";

            //if (this.Parent != null)
            //{
                contentDiv.Controls.Add(lang.getForm());
                if (LanguageManager.Utility.Config.EnableMultiLanguage)
                {
                    contentDiv.Controls.Add(langContols);
                }
            //}
            contentDiv.Controls.Add(table.getForm());

            HtmlGenericControl pButtons = new HtmlGenericControl("p");
            pButtons.Attributes["class"] = "buttonscontainer";

            Button bt = new Button();
            bt.Text = "Salva";
            bt.ID = "send";
            bt.CssClass = "button";
            bt.OnClientClick = "javascript: setSubmitSource('send')";
            pButtons.Controls.Add(bt);

            contentDiv.Controls.Add(pButtons);

            WebControl row = new WebControl(HtmlTextWriterTag.Div);
            row.CssClass = "row";

            row.Controls.Add(contentDiv);

            fieldset.Controls.Add(row);

            div.Controls.Add(fieldset);
            return div;
        }

        public virtual WebControl getListView()
        {
            //Lancio il codice di controllo eliminazione dei documenti
            DeletePostBack();

            //NetCms.GUI.PopupBox.AddMessage("Si è verificato un errore non sarà possibile spostare la cartella.", PostBackMessagesType.Error);
            WebControl container = new WebControl(HtmlTextWriterTag.Div);

            //Creo la tabella con l'elenco delle applicazioni verticali di Portale
            Vertical.VerticalCmsApplicationListControl vapplist = new NetCms.Vertical.VerticalCmsApplicationListControl(this.StructureNetwork.ID);
            vapplist.NetworkDependant = true;
            vapplist.ShowOnlyGroups = 0;
            container.Controls.Add(vapplist);

            //Creo la form di ricerca e la insirisco in un controllo a scomparsa.
            NetCms.GUI.Collapsable SearchControl = new Collapsable("Pannello di ricerca");
            SearchControl.StartCollapsed = true;
            SearchControl.AppendControl(this.initSearchForm());
            SearchControl.AppendControl(new WebControl(HtmlTextWriterTag.P));
            container.Controls.Add(SearchControl);

            NetCms.GUI.Collapsable collapsableControl = new Collapsable("Cartelle e Documenti");

            //Controllo se è stata effettuata una ricerca
            if (this.PageData.SubmitSource == "SearchButton")
            {
                //Se è stata effettuata una ricerca, costruisco la tabella coi criteri di ricerca selezionati e la tabella coi risultati della ricerca
                //container.Controls.Add(SearchCriteria);
                //SearchControl.AppendControl(SearchCriteria);
                SearchControl.StartCollapsed = false;
                collapsableControl.AppendControl(getSearchResultView());
            }
            else
            {
                //Se NON è stata effettuata una ricerca, costruisco la tabella relativa alla cartella corrente

                WebControl divFolderInfo = new WebControl(HtmlTextWriterTag.Div);
                divFolderInfo.CssClass = "folderInfo";

                string tpl = ((this.Template != null && !string.IsNullOrEmpty(this.Template)) ? @"<span>Template pagina principale</span>: " + this.Template + @" <br />" : "");


                string folderInfo = @"
                <div class=""container-fluid"">
                    <div class=""row"">
                    <div class=""col-sm"">
                        <p>
                                      <span>Cartella corrente</span>: " + this.Label + @" <br /> 
                                      <span>Tipologia</span>: " + this.RelativeApplication.LabelSingolare + @" <br />                                       
                        </p>
                    </div>
                    <div class=""col-sm"">
                        <p>
                                    <span>Stato</span>: " + this.StatusLabel + @"<br /> 
                                    <span>Top Menu</span>: " + ((this.ShowInTopMenu != 0)?"Si":"No") + @"<br /> 
                                    <span>Menu contestuale</span>: " + ((this.ShowMenu != 0) ? "Si" : "No") + @"
                        </p>
                    </div>
                    <div class=""col-sm"">
                        <p>"+
                                     (((this.RelativeApplication.ID == 2)) ? @"<span>Tipo pagina principale</span>: " +  ((StructureFolder.HomeTypeFolder)this.HomeType).ToDescriptionString()  + @"<br />" : "") +
                                     tpl +
                                     @"<span>Front layout</span>:"+ this.FrontLayout.ToDescriptionString()+ @"
                        </p>
                    </div>
                    </div>
                </div>";
   

                divFolderInfo.Controls.Add(new LiteralControl(folderInfo));
                collapsableControl.AppendControl(divFolderInfo);

                StructureTable table = new StructureTable(this);               
                getTableFolderView(table);
                getTableDocsView(table);
                getBackRow(table);                
                collapsableControl.AppendControl(table);
            }
            container.Controls.Add(collapsableControl);

            return container;
        }

        public virtual string SearchCriteria
        {
            get
            {
                string datiInnerHtml = "";
                if (this.PageData.SubmitSource == "SearchButton")
                {
                    NetUtility.RequestVariable Titolo_Search = new NetUtility.RequestVariable("Titolo_Search", NetUtility.RequestVariable.RequestType.Form);
                    NetUtility.RequestVariable Date_Search = new NetUtility.RequestVariable("Date_Search", NetUtility.RequestVariable.RequestType.Form);
                    NetUtility.RequestVariable DateType_Search = new NetUtility.RequestVariable("DateType_Search", NetUtility.RequestVariable.RequestType.Form);
                    NetUtility.RequestVariable OrderBy_Search = new NetUtility.RequestVariable("OrderBy_Search", NetUtility.RequestVariable.RequestType.Form);
                    NetUtility.RequestVariable Tipo_Search = new NetUtility.RequestVariable("Tipo_Search", NetUtility.RequestVariable.RequestType.Form);
                    datiInnerHtml += "";

                    if (Titolo_Search.IsValidString)
                    {
                        datiInnerHtml += " che contengono il testo '" + Titolo_Search + "' nel titolo o nella descrizione";
                    }
                    if (Tipo_Search.IsValidInteger)
                    {
                        //DataTable table = this.StructureNetwork.Connection.SqlQuery("SELECT * FROM Applicazioni");
                        //DataRow[] row = table.Select("id_Applicazione = " + Tipo_Search.IntValue);
                        //if (row.Length > 0)
                        //{
                        //    if (datiInnerHtml.Length > 0)
                        //        datiInnerHtml += ",";
                        //    datiInnerHtml += " di tipo " + row[0]["SystemName_Applicazione"].ToString() + "";
                        //}

                        NetCms.Structure.Applications.Application dbApp = NetCms.Structure.Applications.ApplicationBusinessLogic.GetApplicationById(Tipo_Search.IntValue);
                        if (dbApp != null)
                        {
                            if (datiInnerHtml.Length > 0)
                                datiInnerHtml += ",";
                            datiInnerHtml += " di tipo " + dbApp.SystemName + "";
                        }
                    }
                    if (Date_Search.IsValid(NetUtility.RequestVariable.VariableType.Date) && DateType_Search.IsValidInteger)
                    {
                        if (datiInnerHtml.Length > 0)
                            datiInnerHtml += ",";
                        string Operator = "";
                        if (DateType_Search.IntValue == 1)
                            Operator = " precedente al";
                        if (DateType_Search.IntValue == 2)
                            Operator = " successiva al";
                        datiInnerHtml += " la cui data di creazione sia " + Operator + " " + Date_Search.DateTimeValue.Day + "/" + Date_Search.DateTimeValue.Month + "/" + Date_Search.DateTimeValue.Year;
                    }

                    if (OrderBy_Search.IsValidInteger)
                    {
                        string OrderBy = "";
                        switch (OrderBy_Search.IntValue)
                        {
                            case 0:
                                OrderBy += " Nome";
                                break;
                            case 1:
                                OrderBy += " Percorso";
                                break;
                            case 2:
                                OrderBy += " Tipo";
                                break;
                            case 3:
                                OrderBy += " Data crescente";
                                break;
                            case 4:
                                OrderBy += " Data decrescente";
                                break;
                            default:
                                OrderBy += " Nome";
                                break;
                        }
                        if (datiInnerHtml.Length > 0)
                            datiInnerHtml += ",";
                        datiInnerHtml += " ordinati per" + OrderBy;
                    }

                    if (datiInnerHtml.Length > 0)
                        datiInnerHtml = "Documenti" + datiInnerHtml + "";

                }
                return datiInnerHtml;
            }
        }
    }
}
