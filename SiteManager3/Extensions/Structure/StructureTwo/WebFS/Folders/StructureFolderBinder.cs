using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Connections;
using NetCms.Users;
using System.Linq;


/// <summary>
/// Summary description for StructureFolderBind
/// </summary>
/// 
namespace NetCms
{
    namespace Structure.WebFS
    {
        public class StructureFolderBinder
        {
            public StructureFolderBinder(PageData pageData)
            {

            }

            public TreeNode TreeFromStructure(StructureFolder Folder)
            {
                return TreeFromStructure(Folder, 0,true);

            }
            public TreeNode TreeFromStructure(StructureFolder Folder, bool ShowSystemFolders)
            {
                return TreeFromStructure(Folder, 0,ShowSystemFolders);

            }
            public TreeNode TreeFromStructure(StructureFolder Folder, int[] FilterTypes)
            {
                return TreeFromStructure(Folder, FilterTypes, false);
            }
            public TreeNode TreeFromStructure(StructureFolder Folder, int FilterType)
            {
                return TreeFromStructure(Folder, FilterType, false);
            }
            public TreeNode TreeFromStructure(StructureFolder Folder, int FilterType, bool ShowSystemFolders)
            {
                int[] types = new int[1];
                types[0] = FilterType;
                return TreeFromStructure(Folder, types, ShowSystemFolders);
            }
            public TreeNode TreeFromStructure(StructureFolder Folder, int[] FilterTypes,bool ShowSystemFolders)
            {

                TreeNode node = new TreeNode(Folder.Label, Folder.ID.ToString());
                node.ToolTip = Folder.Tipo.ToString();
                node.ImageUrl = (Folder.StateValue<2?1:0).ToString();
                node.Expanded = Folder.StateValue == 0 ;
                node.Target = Folder.Path;

                bool check = FilterTypes[0] == 0;
                int i=-1;
                while (!check && ++i < FilterTypes.Length)
                    check = check || Folder.Tipo == FilterTypes[i];
                
                node.Checked = check;

                for (i = 0; i < Folder.SubFolders.Count; i++)
                {
                    StructureFolder folder = Folder.SubFolders.ElementAt(i);
                    if (ShowSystemFolders || (!folder.IsSystemFolder && !folder.Hidden))
                        node.ChildNodes.Add(TreeFromStructure(folder, FilterTypes, ShowSystemFolders));
                }

                return node;

            }
        }
    }
}