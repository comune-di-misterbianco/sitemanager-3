﻿using NetService.Utility.ValidatedFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.WebFS.Forms
{
    public class CmsContentImportForm : VfManager
    {
        private string _TmpUploadFolderPath;
        public CmsContentImportForm(string controlID, bool isPostBack, string tmpFolderPath)
            :base(controlID, isPostBack)
        {
            _TmpUploadFolderPath = tmpFolderPath;
            this.Fields.Add(UploadCSV);            
        }

        public VfMultiUpload UploadCSV
        {
            get
            {
                if (_UploadCSV == null)
                {
                    _UploadCSV = new VfMultiUpload("allegati", "File Json", _TmpUploadFolderPath, null, new string[] { "json" }, 1);
                    _UploadCSV.CssClass = "VfUploadify";
                    _UploadCSV.AllowDelete = false;
                    _UploadCSV.Required = true;                  
                }
                return _UploadCSV;
            }

        }
        private VfMultiUpload _UploadCSV;

    }
}
