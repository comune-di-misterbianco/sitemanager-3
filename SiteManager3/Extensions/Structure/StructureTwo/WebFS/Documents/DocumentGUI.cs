﻿using System;
using System.Web;
using System.Data;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetForms;
using NetCms.Front;
using NetCms.Grants;
using LanguageManager.Model;
using System.Collections.Generic;
using System.Linq;


namespace NetCms.Structure.WebFS
{
    public partial class Document
    {
        public static NetFormTable DocsFormTable(PageData PageData, int Application)
        {
            return DocsFormTable(PageData, Application,null);
        }

        public static NetFormTable NetworkDocsFormTable(PageData PageData)
        {
            NetFormTable networkDocs = new NetFormTable("network_docs", "id_NetworkDoc", PageData.Conn);

            #region Campi NetworkDocs

            NetHiddenField folder = new NetHiddenField("Folder_Doc");
            folder.Value = PageData.CurrentFolder.ID.ToString();
            networkDocs.addField(folder);  

            #endregion

            return networkDocs;
        }

        public static NetFormTable DocsContentFormTable(PageData PageData)
        {
            NetFormTable docsContent = new NetFormTable("docs_contents", "id_DocContent", PageData.Conn);

            return docsContent;
        }

        public static NetFormTable DocsFormTable(PageData PageData, int Application, Document Document)
        {
            NetFormTable docs = new NetFormTable("Docs", "id_Doc", PageData.Conn);
            if (Document!=null)
                docs.ID =  Document.ID;

            #region Campi Docs

            if (Document == null)
            {
                //DocNameTextBox Titolo = new DocNameTextBox("Titolo", "Name_Doc", NetTextBoxPlus.ContentTypes.File, PageData);
                DocNameTextBox Titolo = new DocNameTextBox("Titolo", "Name_Doc", NetTextBoxPlus.ContentTypes.NormalText, PageData);
                Titolo.Required = true;
                docs.addField(Titolo);
            }
            if (Application > 1)//Disabilita per la modifica delle Homepages il campo stato
            {
                NetDropDownList Stato = new NetDropDownList("Stato", "Stato_Doc");
                //for (int i = 0; i < 3; i++)
                //Stato.addItem(Document.StatusLabels(i), i.ToString());

                if (Application == 2 || Application == 9)
                {
                    Stato.addItem(Document.StatusLabels(0), "0");
                    //Stato.addItem(Document.StatusLabels(4), "4");
                }
                Stato.addItem(Document.StatusLabels(1), "1");
                Stato.addItem(Document.StatusLabels(2), "2");
                
                Stato.Required = true;
                docs.addField(Stato);

                if (PageData.CurrentFolder.RelativeApplication.ID == 2 || PageData.CurrentFolder.RelativeApplication.ID == 9)
                {
                    NetDropDownList ShowInTopMenu = new NetDropDownList("Mostra nel topmenu", "ShowInTopMenu_Doc");
                    ShowInTopMenu.ClearList();
                    ShowInTopMenu.addItem("Si", "1");
                    ShowInTopMenu.addItem("No", "0");
                    docs.addField(ShowInTopMenu);
                }
            }
            
            NetTags Tags = new NetTags("Parole chiave", "Tags");
            Tags.Required = false;
            Tags.JumpField = true;
            Tags.PlaceholderText = "Inserire singole parole per classificare il contenuto: esempio casa, animali, sport";
            // sintassi keyword separate da virgole 'foo', 'bar'
            Tags.TagList = NetCms.Structure.WebFS.Documents.Tags.BusinessLogic.TagsBusinessLogic.GetAllTags();
            Tags.MaxTag = 6;

            if (Document != null)
                Tags.Value = Document.Tags;

            docs.addField(Tags);

            if (Document == null)
            {

                NetHiddenField data = new NetHiddenField("Data_Doc");
                data.Value = PageData.Conn.FilterDate(DateTime.Today.Date.ToString());
                data.ValidValue = true;
                docs.addField(data);

                NetHiddenField create = new NetHiddenField("Create_Doc");
                create.Value = data.Value;//PageData.Conn.FilterDate(DateTime.Today.Date.ToString());
                create.ValidValue = true;
                docs.addField(create);

                NetHiddenField autore = new NetHiddenField("Autore_Doc");
                autore.Value = PageData.Account.ID.ToString();
                docs.addField(autore);

                NetHiddenField app = new NetHiddenField("Application_Doc");
                app.Value = Application.ToString();
                docs.addField(app);
            }
            else
            {
                //Controllo se la modifica della data di creazione per l'applicazione è abilitata
                string AppName = NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies[Application.ToString()].SystemName;
                string attribute = NetCms.Configurations.XmlConfig.GetAttribute("/Portal/configs/Networks/" + PageData.Network.SystemName + "/applications/" + AppName, "editdate");
                if (attribute == "true")
                {
                    NetDateField data = new NetDateField("Data", "Data_Doc");
                    data.Required = true;
                    docs.addField(data);
                }
            }

            if (PageData.CurrentFolder.Criteria[CriteriaTypes.Advanced].Allowed)
            {
                if (Document == null || Document.EnableCssEdit)
                {
                    NetTextBoxPlus cssClass = new NetTextBoxPlus("Classe CSS", "cssclass_doc");
                    docs.addField(cssClass);
                }

                if (Document == null || Document.EnableCustomTitleEdit)
                {
                    NetDropDownList TitleOffset = new NetDropDownList("Titolo del Frontend", "TitleOffset_Doc");
                    TitleOffset.ClearList();
                    TitleOffset.addItem("Mostra solo il titolo", "0");
                    TitleOffset.addItem("Aggiungi path in coda al titolo", "-1");
                    TitleOffset.addItem("Aggiungi KEYWORDS in coda al titolo", "1");
                    TitleOffset.addItem("Mostra solo KEYWORDS", "2");
                    TitleOffset.Required = true;
                    docs.addField(TitleOffset);
                }
            }
            else
            {
                NetHiddenField HiddenTitleOffset = new NetHiddenField("TitleOffset_Doc");
                HiddenTitleOffset.Value = "0";

                docs.addField(HiddenTitleOffset);
            }

            if ((Document == null || Document.EnableCustomLayoutEdit) &&
                ((StructureFolder)Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder).FrontLayout != FrontLayouts.SystemError)
            {
                NetDropDownList layout = new NetDropDownList("Layout di colonne e Piè di pagina (Attenzione la visualizzazione finale del layout dipende comunque da come sono state impostate le colonne nella homepage relativa a questa cartella.)", "Layout_Doc");
                layout.ClearList();

                layout.addItem("Eredita", "0");
                layout.addItem("Mostra Tutte", "8");
                layout.addItem("Nascondi Tutte", "7");
                layout.addItem("Mostra solo Sinistra e Destra", "1");
                layout.addItem("Mostra solo Sinistra e Footer", "2");
                layout.addItem("Mostra solo Destra e Footer", "3");
                layout.addItem("Mostra solo Sinistra", "4");
                layout.addItem("Mostra solo Destra", "5");
                layout.addItem("Mostra solo Footer", "6");
                layout.Required = true;

                docs.addField(layout);
            }
            NetHiddenField modify = new NetHiddenField("Modify_Doc");
            modify.Value = PageData.Conn.FilterDate(DateTime.Today.Date.ToString());
            docs.addField(modify);

            #endregion

            return docs;
        }
        public static NetFormTable ReviewFormTable(PageData PageData)
        {
            NetFormTable review = new NetFormTable("Revisioni", "id_Revisione", PageData.Conn);

            NetHiddenField autore = new NetHiddenField("Autore_Revisione");
            autore.Value = PageData.Account.ID.ToString();
            review.addField(autore);

            NetHiddenField Approvata = new NetHiddenField("Approvata_Revisione");
            Approvata.Value = PageData.Account.Depth.ToString();
            review.addField(Approvata);


            NetHiddenField Data = new NetHiddenField("Data_Revisione");
            Data.Value = PageData.Conn.FilterDate(DateTime.Today.Date.ToString());
            review.addField(Data);

            NetExternalField doc = new NetExternalField("Docs_Lang", "DocLang_Revisione");
            review.ExternalFields.Add(doc);

            return review;
        }
        public static NetFormTable LanguageFormTable(PageData PageData)
        {
            NetFormTable Language = new NetFormTable("Docs_Lang", "id_DocLang", PageData.Conn);

            NetHiddenField Lang = new NetHiddenField("Lang_DocLang");
            Lang.Value = PageData.DefaultLang.ToString();
            Language.addField(Lang);

            NetTextBoxPlus Descrizione = new NetTextBoxPlus("Descrizione", "Desc_DocLang");
            Descrizione.Required = true;
            Descrizione.Rows = 5;
            Descrizione.MaxLenght = 2000;
            Language.addField(Descrizione);

            NetExternalField doc = new NetExternalField("network_docs", "Doc_DocLang");
            Language.ExternalFields.Add(doc);

            return Language;
        }
        public static NetFormTable LanguageFormTable(PageData PageData, string id)
        {
            NetFormTable Language = new NetFormTable("Docs_Lang", "id_DocLang", PageData.Conn, int.Parse(id));

            NetTextBoxPlus label = new NetTextBoxPlus("Titolo", "Label_DocLang");
            label.Required = true;
            label.Size = 60;

            Language.addField(label);

            NetTextBoxPlus Descrizione = new NetTextBoxPlus("Descrizione", "Desc_DocLang");
            Descrizione.Required = true;
            Descrizione.Rows = 5;
            Descrizione.Size = 60;
            Descrizione.MaxLenght = 2000;
            Language.addField(Descrizione);

            return Language;
        }           

        protected NetUtility.HtmlGenericControlCollection OptionsControls(string AppName)
        {
            return OptionsControls(AppName, AppName + "_mod");
        }
        protected NetUtility.HtmlGenericControlCollection OptionsControls(string AppName, string ModPageName)
        {
            return OptionsControls(AppName, ModPageName, "Contenuti");
        }
        protected NetUtility.HtmlGenericControlCollection OptionsControls(string AppName, string ModPageName, string ModLinkLabel)
        {
            if (PageData.Account.GUI.FoldersGUI == Users.SitemanagerUserGUI.FoldersGUIs.Separate)
            {
                NetUtility.HtmlGenericControlCollection controls = new NetUtility.HtmlGenericControlCollection();
                HtmlGenericControl td = new HtmlGenericControl("td");

                #region Tasto di collegamento ai permessi
                //***************************************************
                td = new HtmlGenericControl("td");
                controls.Add(td, "DocGrantsButton" + this.ID);

                if ((this.Folder as StructureFolder).EnableGrantsElab && Grant && Criteria[CriteriaTypes.Grants].Allowed)
                {
                    td.Attributes["class"] = "grant";
                    td.InnerHtml = "<a href=\"" + PageData.Paths.AbsoluteNetworkRoot + "/cms/grants/grants.aspx?doc=" + ID + "\"><span>Imposta Permessi</span></a>";
                }
                else
                    td.InnerHtml = "&nbsp;";

                //***************************************************
                #endregion

                #region Tasto di collegamento alla pagina di modifica

                td = new HtmlGenericControl("td");
                controls.Add(td, "DocModifyButton" + this.ID);
                if (Grant && Criteria[CriteriaTypes.Modify].Allowed)
                {
                    td.Attributes["class"] = "modify";
                    if ((Folder as StructureFolder).ReviewEnabled)
                    {
                        if (this.Criteria[CriteriaTypes.Publisher].Allowed)// && (this.PhysicalName != "default.aspx" || NetCms.Users.AccountManager.CurrentAccount.God == 1))
                        {
                            td.Attributes["class"] += " half";
                            td.InnerHtml = "<a  class=\"HalfProperies\"  href=\"" + PageData.Paths.AbsoluteNetworkRoot + "/docs/docs_mod.aspx?folder=" + this.Folder.ID + "&amp;doc=" + this.ID + "\"><span>Proprietà</span></a>";
                        }
                        string href = PageData.Paths.AbsoluteNetworkRoot + "/docs/overview.aspx?folder=" + this.Folder.ID + "&amp;doc=" + this.ID;
                        td.InnerHtml += "<a href=\"" + href + "\"><span>Revisioni</span></a>";
                    }
                    else
                    {
                        td.Attributes["class"] += " half";
                        td.InnerHtml = "<a class=\"HalfProperies\" href=\"" + PageData.Paths.AbsoluteNetworkRoot + "/docs/docs_mod.aspx?folder=" + this.Folder.ID + "&amp;doc=" + this.ID + "\"><span>Proprietà</span></a>";
                        td.InnerHtml += "<a href=\"" + PageData.Paths.AbsoluteNetworkRoot + "/" + AppName + "/modify.aspx?folder=" + this.Folder.ID + "&amp;doc=" + this.ID + "\"><span>" + ModLinkLabel + "</span></a>";
                    }
                }
                else
                    td.InnerHtml = "&nbsp;";

                #endregion

                #region Tasto di eliminazione del record

                td = new HtmlGenericControl("td");
                controls.Add(td, "DocDeleteButton" + this.ID);
                if (Grant && Criteria[CriteriaTypes.Delete].Allowed && this.Application != 1 && (this.PhysicalName != "default.aspx" || NetCms.Users.AccountManager.CurrentAccount.God == 1))
                {
                    td.Attributes["class"] = "delete";
                    td.InnerHtml = "<a href=\"javascript: DeleteDoc('" + this.ID + "','deleteDoc','" + this.PhysicalName.Replace("'", "\\\'") + "')\"><span>Elimina</span></a>";
                }
                else
                    td.InnerHtml = "&nbsp;";

                #endregion

                #region Informazione sullo stato della traduzione in caso di multilingua
                if (LanguageManager.Utility.Config.EnableMultiLanguage)
                {
                    IList<Language> enabledLangs = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages();
                    int countTrad = 0;
                    foreach (Language lang in enabledLangs)
                    {
                        if (this.DocLangs.Count(x => x.Lang == lang.ID && x.Content != null) > 0)
                            countTrad++;
                    }

                    td = new HtmlGenericControl("td");
                    controls.Add(td, "DocMultiLanguageInfo" + this.ID);
                    if (countTrad == enabledLangs.Count)
                        td.Attributes["class"] = "icon_flag_all";
                    else
                        td.Attributes["class"] = "icon_flag";
                    td.InnerHtml = "<a href=\"#\"><span>Traduzioni (" + countTrad + "/" + enabledLangs.Count + ")</span></a>";
                }
                #endregion
                return controls;
            }
            if (PageData.Account.GUI.FoldersGUI == Users.SitemanagerUserGUI.FoldersGUIs.Colapsed)
            {
                NetUtility.HtmlGenericControlCollection controls = new NetUtility.HtmlGenericControlCollection();

                HtmlGenericControl td = new HtmlGenericControl("td");

                controls.Add(td, td.ID);
                td.InnerHtml = "";
                td.Attributes["class"] = "ColapseActionCell";
                string anchorModel = "<a class=\"ColapseActionLink {1}\" title=\"{2}\" onmouseover=\"setToolbarCaption('{4}')\" onmouseout=\"setToolbarCaption('')\" href=\"{3}\"><span>{0}</span></a>";

                string cssclass, href, label, title;

                #region Tasto di collegamento ai permessi
                if ((this.Folder as StructureFolder).EnableGrantsElab && Grant && Criteria[CriteriaTypes.Grants].Allowed/* && PageData.Account.IsAdministrator*/)
                {
                    label = "Gestisci Permessi Documento";
                    title = HttpUtility.HtmlAttributeEncode("Gestisci Permessi Documento '" + this.Nome + "'");
                    cssclass = "ColapseActionLink_Grants";
                    href = PageData.Paths.AbsoluteNetworkRoot + "/cms/grants/grants.aspx?folder=" + this.Folder.ID + "&amp;doc=" + ID;
                    td.InnerHtml += string.Format(anchorModel, label, cssclass, title, href, label.Replace("'", "\'"));
                }
                #endregion

                #region Tasto di collegamento alla pagina di modifica

                if ((this.Folder as StructureFolder).ReviewEnabled)
                {
                    if (this.Criteria[CriteriaTypes.Publisher].Allowed && Math.Abs((this.Folder as StructureFolder).Homepage) != this.ID && AllowProperties && (this.PhysicalName != "default.aspx" || NetCms.Users.AccountManager.CurrentAccount.God == 1))
                    {
                        label = "Modifica Proprietà Documento";
                        title = HttpUtility.HtmlAttributeEncode("Modifica Proprietà Documento '" + this.Nome + "'");
                        cssclass = "ColapseActionLink_Prop";
                        href = PageData.Paths.AbsoluteNetworkRoot + "/docs/docs_mod.aspx?folder=" + this.Folder.ID + "&amp;doc=" + this.ID;
                        td.InnerHtml += string.Format(anchorModel, label, cssclass, title, href, label.Replace("'", "\'"));
                    }
                    else
                        td.InnerHtml += "<a class=\"ColapseActionLink\"><span>&nbsp;</span></a>";

                    if (AllowModify)
                    {
                        title = HttpUtility.HtmlAttributeEncode("Gestisci Revisioni Documento '" + this.Nome + "'");
                        label = "Gestisci Revisioni Documento";
                        cssclass = "ColapseActionLink_Reviews";
                        href = PageData.Paths.AbsoluteNetworkRoot + "/docs/overview.aspx?folder=" + this.Folder.ID + "&amp;doc=" + this.ID;
                        td.InnerHtml += string.Format(anchorModel, label, cssclass, title, href, label.Replace("'", "\'"));
                    }
                    else
                        td.InnerHtml += "<a class=\"ColapseActionLink\"><span>&nbsp;</span></a>";

                }
                else
                {
                    if (Math.Abs((this.Folder as StructureFolder).Homepage) != this.ID && AllowProperties)
                    {
                        label = "Modifica Proprietà Documento \\\'" + this.Nome + "\\\'";
                        title = HttpUtility.HtmlAttributeEncode("Modifica Proprietà Documento '" + this.Nome + "'");
                        cssclass = "ColapseActionLink_Prop";
                        href = PageData.Paths.AbsoluteNetworkRoot + "/docs/docs_mod.aspx?folder=" + this.Folder.ID + "&amp;doc=" + this.ID;
                        td.InnerHtml += string.Format(anchorModel, label, cssclass, title, href, label.Replace("'", "\'"));
                    }
                    else
                        td.InnerHtml += "<a class=\"ColapseActionLink\"><span>&nbsp;</span></a>";

                    if (AllowModify)
                    {
                        label = "Modifica Contenuto Documento";
                        title = HttpUtility.HtmlAttributeEncode("Modifica Contenuto Documento '" + this.Nome + "'");
                        cssclass = "ColapseActionLink_Modify";
                        href = PageData.Paths.AbsoluteNetworkRoot + "/cms/applications/" + AppName + "/modify.aspx?folder=" + this.Folder.ID + "&amp;doc=" + this.ID;
                        td.InnerHtml += string.Format(anchorModel, label, cssclass, title, href, label.Replace("'", "\'"));
                    }
                    else
                        td.InnerHtml += "<a class=\"ColapseActionLink\"><span>&nbsp;</span></a>";
                }
                #endregion

                #region Tasto di eliminazione del record
                if (this.AllowDelete && (this.PhysicalName != "default.aspx" || NetCms.Users.AccountManager.CurrentAccount.God == 1))
                {
                    title = HttpUtility.HtmlAttributeEncode("Elimina Documento '" + this.Nome + "'");
                    label = "Elimina Documento";
                    cssclass = "ColapseActionLink_Delete";                    
                    href = HttpUtility.HtmlAttributeEncode("javascript: DeleteDoc('" + this.ID + "','deleteDoc','" + HttpUtility.JavaScriptStringEncode(this.PhysicalName) + "')");//.Replace("\'", "\\'")
                    td.InnerHtml += string.Format(anchorModel, label, cssclass, title, href, label.Replace("'", "\'"));
                
                
                }
                #endregion

                #region Informazione sullo stato della traduzione in caso di multilingua
                if (LanguageManager.Utility.Config.EnableMultiLanguage && this.Application != 1 && this.Application != 6)
                {
                    IList<Language> enabledLangs = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages();
                    int countTrad = 0;
                    foreach (Language lang in enabledLangs)
                    {
                        if (this.DocLangs.Count(x => x.Lang == lang.ID && x.Content != null) > 0)
                            countTrad++;
                    }

                    title = HttpUtility.HtmlAttributeEncode("Traduzioni documento '" + this.Nome + "' (" + countTrad + "/" + enabledLangs.Count + ")");
                    label = "Traduzioni documento: " + countTrad + "/" + enabledLangs.Count;
                    if (countTrad == enabledLangs.Count)
                        cssclass = "ColapseActionLink_MultiLang_All";
                    else
                        cssclass = "ColapseActionLink_MultiLang";
                    href = "#";
                    td.InnerHtml += string.Format(anchorModel, label, cssclass, title, href, label.Replace("'", "\'"));
                }
                #endregion

                return controls;
            }

            
            return null;
        }
    }
}
