﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCms.Structure.WebFS
{
    public class DocumentAttach
    {
        public DocumentAttach()
        {

        }

        public virtual int Id { get; set; }

        public virtual DocContent Content
        {
            get;
            set;
        }

        public virtual Document FileDocument
        {
            get;
            set;
        }

        public enum Column
        {
            Id,
            DocContent,
            FileDocument
        }
    }
}
