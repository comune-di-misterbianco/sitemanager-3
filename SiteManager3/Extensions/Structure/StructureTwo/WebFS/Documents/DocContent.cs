﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Structure.WebFS.Reviews;
using System.Data;
using NHibernate;
using NetCms.Networks;

namespace NetCms.Structure.WebFS
{
    public abstract class DocContent
    {
        public DocContent()
        { 
        
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual DocumentLang DocLang
        {
            get;
            set;
        }

        public virtual Review Revisione
        {
            get;
            set;
        }

        public virtual ICollection<DocumentAttach> Attaches
        {
            get;
            set;
        }

        public abstract DocContent ImportDocContent(DataRow docRecord, ISession session, DataSet sourceDB, NetworkKey netKey, string sourceFolderPath, ref Document newDoc);

        public abstract DocContent ImportDocContent(Dictionary<string, string> docRecord, ISession session);

        public virtual bool EnableIndexContent
        {
            get;
            protected internal set;
        }

        public abstract CmsConfigs.Model.IndexDocContent GetContentToIndex(ISession session);
    }
}
