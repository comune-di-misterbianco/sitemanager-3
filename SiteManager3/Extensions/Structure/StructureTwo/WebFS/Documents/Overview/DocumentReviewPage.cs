using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.Applications;
using NetCms.Structure.WebFS.Reviews;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;


/// <summary>
/// Summary description for NewsFolder_NEW_
/// </summary>
namespace NetCms.Structure.WebFS
{
    public abstract class DocumentReviewPage : NetCms.GUI.NetworkPage
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }

        public int ReviewID
        {
            get { return _ReviewID; }
            private set { _ReviewID = value; }
        }
        private int _ReviewID;

        private int _ContentID = -1;
        public int ContentID
        {
            get
            {
                if (_ContentID == -1)
                {
                    if (Review != null)
                        _ContentID = Review.Content.ID;
                    else _ContentID = 0; 
                }

                return _ContentID;
            }
        }

        public Review Review
        {
            get
            {
                if (_Review == null)
                {
                    _Review = ReviewBusinessLogic.GetReviewById(ReviewID);
                }
                return _Review;
            }

        }
        private Review _Review;

        //public DataRow ReviewRow
        //{
        //    get
        //    {
        //        if (_ReviewRow == null)
        //        {
        //            DataTable rev = this.Connection.SqlQuery("SELECT * FROM Revisioni WHERE id_Revisione = " + ReviewID);
        //            if (rev.Rows.Count > 0)
        //                _ReviewRow = rev.Rows[0];
        //        }
        //        return _ReviewRow;
        //    }
        //}
        //private DataRow _ReviewRow;

			
        public int DocLang
        {
            get { return _DocLang; }
            private set { _DocLang = value; }
        }
        private int _DocLang;
        			

        protected void InitByDoc()
        {
            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                G2Core.Common.RequestVariable languageSelected = new G2Core.Common.RequestVariable("lang", G2Core.Common.RequestVariable.RequestType.QueryString);
                
                if (languageSelected.IsValidInteger)                                    
                    _DocLang = int.Parse(this.CurrentDocument.DocLangsContents[languageSelected.IntValue.ToString()]);                                   
                else
                    _DocLang = int.Parse(this.CurrentDocument.DocLangsContents[this.PageData.SelectedLang.ToString()]);
            }
            else
                _DocLang = int.Parse(this.CurrentDocument.DocLangsContents[this.PageData.SelectedLang.ToString()]);
        }
			
        public DocumentReviewPage()
        {
            G2Core.Common.RequestVariable requestReview = new G2Core.Common.RequestVariable("review",G2Core.Common.RequestVariable.RequestType.QueryString);

            if (requestReview != null && requestReview.IsValidInteger && requestReview.IntValue > 0)
            {
                this._ReviewID = requestReview.IntValue;
                _DocLang = Review.DocLang.ID;
            }
            else InitByDoc();
        }
        public static WebControl FabricatePage()
        {
            if (Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork)
            {
                //Application application = ((Document)Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.CurrentDocument).Folder.RelativeApplication;

                Document doc = (Document)Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.CurrentDocument;
                return (WebControl)ApplicationsPool.BuildInstanceOf(doc.RelativeApplication.Classes.Review, doc.RelativeApplication.ID.ToString());
            }
            else return new NetCms.GUI.SmPageError();
            //Trace
        }

    }
}
