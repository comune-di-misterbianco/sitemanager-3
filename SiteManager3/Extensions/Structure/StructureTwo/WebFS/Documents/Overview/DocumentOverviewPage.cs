using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using NetTable;
using NetCms.Structure.Applications;
using NetCms.Structure.WebFS.Reviews;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Linq;
using NetCms.Users;
using System.Collections.Generic;
using NetCms.Grants;
using NHibernate;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetCms.Structure.WebFS
{
    public abstract class DocumentOverview : NetCms.GUI.NetworkPage
    {
        protected string NewReviewLink
        {
            get
            {
                return this.CurrentFolder.StructureNetwork.Paths.AbsoluteAdminRoot + "/docs/review.aspx?folder=" + this.CurrentFolder.ID + "&doc=" + this.CurrentDocument.ID;
            }
        }
        protected int ApplicationID
        {
            get
            {
                return this.CurrentFolder.RelativeApplication.ID;
            }
        }

        protected DataTable RevLog
        {
            get
            {
                if (RevLogOBJ == null)
                {
                    string sql;
                    sql = " SELECT * FROM RevisioniLog INNER JOIN Revisioni on id_Revisione = Revisione_RevLog WHERE Oggetto_RevLog <> 3";
                    RevLogOBJ = this.Conn.SqlQuery(sql);
                }
                return RevLogOBJ;
            }
        }
        private DataTable RevLogOBJ;

        public string DocLangID
        {
            get { return this.CurrentDocument.DocLangsContents[this.PageData.DefaultLang.ToString()]; }
        }

        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Gestione revisioni del documento '" + this.CurrentDocument.Nome + "'"; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel " + NetCms.GUI.NetworkPage.BaseCssClass + "_OverviewPage "; }
        }

        public DocumentOverview()
        {
            if (this.CurrentDocument.Criteria[CriteriaTypes.Modify].Value == NetCms.Grants.GrantsValues.Deny)
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
        
            Toolbar.Buttons.Add(new ToolbarButton(NewReviewLink, NetCms.GUI.Icons.Icons.Page_White_Add, "Nuova Revisione Vuota"));

            base.AddDocumentInformationsToSide();
        }

        protected override WebControl BuildControls()
        {
            WebControl container = new WebControl(HtmlTextWriterTag.Div);
            if (this.Page.IsPostBack)
                this.PostBack();
            container.Controls.Add(getView());

            return container;
        }

        public virtual Control getView()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");

            #region Query

            string sql;

            string SELECT;
            string WHERE = "";
            SELECT = " SELECT * FROM ";
            SELECT += " docs INNER JOIN docs_Lang ON (id_Doc = Doc_DocLang)";
            SELECT += " INNER JOIN revisioni ON (id_DocLang = DocLang_Revisione)";
            SELECT += " INNER JOIN Users ON Autore_Revisione = id_User";

            WHERE += " WHERE id_DocLang =  " + this.DocLangID;

            sql = SELECT + WHERE + " ORDER BY Data_Revisione DESC,id_Revisione DESC ";

            #endregion

            #region Tabella e Colonne

            SmTable table = new SmTable(PageData.PageIndex, sql, this.Conn);

            table.NoRecordMsg = "Non ci sono revisioni disponibili per questo documento<br />Per crearne una utilizare il tasto 'Nuova Revisione Vuota'";

            SmTableActionColumn Preview = new SmTableActionColumn("Contenuto_Revisione", "preview", "&nbsp;", PreviewJS);
            table.addColumn(Preview);

            ReviewColumnName Nome = new ReviewColumnName("id_Revisione", "Codice Revisione", ID);
            table.addColumn(Nome);

            SmTableColumn Autore = new SmTableColumn("Name_User", "Autore");
            table.addColumn(Autore);

            ReviewColumnDate data = new ReviewColumnDate("Data_Revisione", "Data");
            table.addColumn(data);

            ReviewColumnAction set = new ReviewColumnAction(0 , 0, RevLog, PageData.CurrentReference, this.CurrentDocument);
            table.addColumn(set);

            SmTableActionColumn New = new SmTableActionColumn("id_Revisione", "modify", "Nuova Revisione", NewReviewLink + "&amp;review={0}");
            table.addColumn(New);
            #endregion

            #region Campi Hidden per i controlli di postback

            /**********************************************/
            HtmlInputHidden activate = new HtmlInputHidden();
            activate.ID = "publish";
            activate.EnableViewState = false;
            div.Controls.Add(activate);
            /***************************************************/
            HtmlInputHidden approve = new HtmlInputHidden();
            approve.ID = "approve";
            approve.EnableViewState = false;
            div.Controls.Add(approve);
            /**********************************************/
            HtmlInputHidden askrevision = new HtmlInputHidden();
            askrevision.ID = "askrevision";
            askrevision.EnableViewState = false;
            div.Controls.Add(askrevision);
            /**********************************************/
            HtmlInputHidden annullask = new HtmlInputHidden();
            annullask.ID = "annullask";
            annullask.EnableViewState = false;
            div.Controls.Add(annullask);
            /**********************************************/
            HtmlInputHidden refuse = new HtmlInputHidden();
            refuse.ID = "refuse";
            refuse.EnableViewState = false;
            div.Controls.Add(refuse);
            /**********************************************/

            #endregion

            #region Campo di testo per messaggi di notifica

            HtmlGenericControl notify = new HtmlGenericControl("div");
            notify.Attributes["class"] = "hide";
            notify.ID = "notify";

            Label NotifyLabel = new Label();
            NotifyLabel.Text = "Inserire messaggio di notifica";
            NotifyLabel.AssociatedControlID = "NotifyText";
            NotifyLabel.Attributes.CssStyle.Add("display", "block");
            notify.Controls.Add(NotifyLabel);

            TextBox NotifyText = new TextBox();
            NotifyText.ID = "NotifyText";
            NotifyText.Columns = 60;
            NotifyText.Rows = 5;
            NotifyText.EnableViewState = false;
            notify.Controls.Add(NotifyText);

            Button NotifyButton = new Button();
            NotifyButton.ID = "NotifyButton";
            NotifyButton.Text = "Invia";
            NotifyButton.OnClientClick = "setSubmitSource('refuse')";
            notify.Controls.Add(NotifyButton);

            div.Controls.Add(notify);

            #endregion

            div.Controls.Add(table.getTable());
            return div;
        }
        
        public static Review insertReview(DocumentLang DocLang, DocContent Content, int SourceID, PageData PageData, ISession sess = null)
        {
            User currentAccount = UsersBusinessLogic.GetById(Users.AccountManager.CurrentAccount.ID,sess);

            Review revisione = new Review(currentAccount);
            revisione.DocLang = DocLang;
            revisione.Content = Content;
            revisione.Data = DateTime.Now;
            revisione.Autore = currentAccount.ID;
            revisione.Origine = SourceID;

            return ReviewBusinessLogic.SaveOrUpdateReview(revisione,sess);
        }

        public virtual void PostBack()
        {
            DateTime ora = DateTime.Now;
            string today = "'" + ora.Year + "-" + ora.Month + "-" + ora.Day + " " + ora.Hour + "." + ora.Minute + "." + ora.Second + "'";

            string key;

            #region Pubblica

            PostBackPubblica();

            #endregion

            #region Chiedi Revisione

            key = "askrevision";
            if (PageData.SubmitSource == key &&
                HttpContext.Current.Request.Form[key] != null &&
                HttpContext.Current.Request.Form[key].Length > 0)
            {

                string value = HttpContext.Current.Request.Form[key];
                int ReviewID = int.Parse(HttpContext.Current.Request.Form[key]);

                string sql;
                string sql_tot = "";
                string Testo = this.Account.UserName + " chiede che sia revisionato il documento" + this.CurrentDocument.PhysicalName;


                //this.Connection.Open();

                sql = "UPDATE RevisioniLog INNER JOIN Revisioni ON id_Revisione = Revisione_RevLog SET";
                sql += " Oggetto_RevLog = 3";
                sql += " WHERE DocLang_Revisione = " + DocLangID;
                sql += " AND Mittente_RevLog = " + this.Account.ID;
                sql += " AND Oggetto_RevLog = 0";
                this.Conn.Execute(sql);
                sql_tot += "<br />" + sql + "<br />";
                int Livello_RevLog = this.Account.Depth;
                if (this.CurrentDocument.Criteria[CriteriaTypes.Redactor].Allowed && Livello_RevLog > 0)
                    Livello_RevLog--;
                sql = "INSERT INTO RevisioniLog ";
                sql += " (Mittente_RevLog,Livello_RevLog,Revisione_RevLog,Data_RevLog,Testo_RevLog) VALUES";
                sql += " (" + this.Account.ID + "," + Livello_RevLog + "," + value + "," + today + ",'" + Testo + "')";
                //this.Connection.Open();
                int LogID = this.Conn.ExecuteInsert(sql);
                sql_tot += "<br>" + sql + "<br>";
                //this.Connection.Close();

                value = value.Length == 1 ? "0" + value : value;
                value = value.Length == 2 ? "0" + value : value;
                NetCms.GUI.PopupBox.AddMessage("Richiesta di approvazione per la revisione \"Rev#" + this.CurrentDocument.ID + "/" + value + "\" inviata"
                    +"<br>(Ogni altra richiesta di approvazione per questo documento � stata annullata)",NetCms.GUI.PostBackMessagesType.Success);

                this.Conn.Execute("UPDATE Revisioni_Notifiche SET Stato_RevisioneNotifica = 1 WHERE Doc_RevisioneNotifica = " + this.CurrentDocument.ID);
                SendNotify(ReviewID, LogID);

                //Msg += "<br>" + sql_tot;
            }

            #endregion

            #region Annulla Richiesta

            key = "annullask";
            if (PageData.SubmitSource == key &&
                HttpContext.Current.Request.Form[key] != null &&
                HttpContext.Current.Request.Form[key].Length > 0)
            {
                string value = HttpContext.Current.Request.Form[key].Split('!')[0];
                string LogID = HttpContext.Current.Request.Form[key].Split('!')[1];
                string sql;

                sql = "UPDATE RevisioniLog SET";
                sql += " Oggetto_RevLog = 3";
                sql += " WHERE Revisione_RevLog = " + value;
                sql += " AND Mittente_RevLog = " + this.Account.ID;
                this.Conn.Execute(sql);
                value = value.Length == 1 ? "0" + value : value;
                value = value.Length == 2 ? "0" + value : value;
                NetCms.GUI.PopupBox.AddMessage("Richiesta la di approvazione per la revisione \"Rev#" + this.CurrentDocument.ID + "/" + value + "\" annullata", NetCms.GUI.PostBackMessagesType.Notify);
                ClearNotify(LogID);
                //Msg += "<br>" + sql;
            }

            #endregion

            #region Approva

            key = "approve";
            if (PageData.SubmitSource == key &&
                HttpContext.Current.Request.Form[key] != null &&
                HttpContext.Current.Request.Form[key].Length > 0)
            {
                string value = HttpContext.Current.Request.Form[key].Split('!')[0];
                string LogID = HttpContext.Current.Request.Form[key].Split('!')[1];
                string Dest = HttpContext.Current.Request.Form[key].Split('!')[2];
                string Level = HttpContext.Current.Request.Form[key].Split('!')[3];
                string sql_over = "";
                string sql;

                //this.Connection.Open();

                sql = "UPDATE Revisioni SET";
                sql += " Approvata_Revisione = " + Level;
                sql += " WHERE id_Revisione = " + value;
                this.Conn.Execute(sql);
                sql_over += "<br>" + sql + "<br>";

                sql = "INSERT INTO RevisioniLog ";
                sql += " (Mittente_RevLog,Destinatario_RevLog,Livello_RevLog,Revisione_RevLog,Data_RevLog,Testo_RevLog,Oggetto_RevLog) VALUES";
                sql += " (" + this.Account.ID + "," + Dest + "," + Level + "," + value + "," + today + ",'Richiesta approvata',1)";
                this.Conn.Execute(sql);
                sql_over += "<br>" + sql + "<br>";

                //this.Connection.Close();
                value = value.Length == 1 ? "0" + value : value;
                value = value.Length == 2 ? "0" + value : value;
                NetCms.GUI.PopupBox.AddMessage("Revisione \"Rev#" + this.CurrentDocument.ID + "/" + value + "\" approvata", NetCms.GUI.PostBackMessagesType.Success);
                ClearNotify(LogID);
                // Msg += "<br>" + sql_over;
            }

            #endregion

            #region Respingi

            key = "refuse";
            if (PageData.SubmitSource == key &&
                HttpContext.Current.Request.Form[key] != null &&
                HttpContext.Current.Request.Form[key].Length > 0)
            {
                string value = HttpContext.Current.Request.Form[key].Split('!')[0];
                string Log = HttpContext.Current.Request.Form[key].Split('!')[1];
                string Dest = HttpContext.Current.Request.Form[key].Split('!')[2];
                string Level = HttpContext.Current.Request.Form[key].Split('!')[3];
                string MsgText = HttpContext.Current.Request.Form["NotifyText"];
                MsgText = MsgText.Replace("'", "''");
                if (MsgText.Length > 2000)
                    MsgText = MsgText.Substring(0, 1995) + "...";
                string sql_over = "";
                string sql;

                //this.Connection.Open();

                sql = "INSERT INTO RevisioniLog ";
                sql += " (Mittente_RevLog,Destinatario_RevLog,Livello_RevLog,Revisione_RevLog,Data_RevLog,Testo_RevLog,Oggetto_RevLog) VALUES";
                sql += " (" + this.Account.ID + "," + Dest + "," + Level + "," + value + "," + today + ",'" + MsgText + "',2)";
                this.Conn.Execute(sql);
                sql_over += "<br>" + sql + "<br>";

                sql = "UPDATE RevisioniLog SET";
                sql += " Oggetto_RevLog = 2 ";
                sql += ", Testo_RevLog = '" + MsgText + "' ";
                sql += " WHERE Oggetto_RevLog = 1 AND Revisione_RevLog = " + value;
                this.Conn.Execute(sql);
                sql_over += "<br>" + sql + "<br>";

                sql = "UPDATE Revisioni SET";
                sql += " Approvata_Revisione = -1 ";
                sql += " WHERE id_Revisione = " + value;
                this.Conn.Execute(sql);
                sql_over += "<br>" + sql + "<br>";

                //this.Connection.Close();
                value = value.Length == 1 ? "0" + value : value;
                value = value.Length == 2 ? "0" + value : value;
                NetCms.GUI.PopupBox.AddMessage("Revisione \"Rev#" + this.CurrentDocument.ID + "" + value + "\" Respinta", NetCms.GUI.PostBackMessagesType.Notify);
                ClearNotify(Log);
                // Msg += "<br>" + sql_over;
            }

            #endregion
        }

        public virtual void PostBackPubblica()
        {
            string today = this.Conn.FilterDate(DateTime.Today.Date.ToString());
            string key;

            #region Pubblica

            key = "publish";
            G2Core.Common.RequestVariable request = new G2Core.Common.RequestVariable(key);
            if (PageData.SubmitSource == key && request.IsValidString)
            {
                string value = request.StringValue;
                string sql_over = "";

                //this.Connection.Open();

                string sql = "UPDATE Revisioni SET";
                sql += " Published_Revisione = 0";
                sql += " WHERE DocLang_Revisione = " + DocLangID + " AND Published_Revisione = 1";
                this.Conn.Execute(sql);
                sql_over += sql;

                sql = "UPDATE Revisioni SET";
                sql += " Published_Revisione = 1";
                sql += " WHERE id_Revisione = " + value;
                this.Conn.Execute(sql);
                sql_over += sql;

                sql = "SELECT * FROM Revisioni";
                sql += " WHERE id_Revisione = " + value;
                DataTable dt = this.Conn.SqlQuery(sql);
                sql_over += sql;
                string Content_Doc = "0";
                if (dt.Rows.Count > 0)
                    Content_Doc = dt.Rows[0]["Contenuto_Revisione"].ToString();

                sql = "UPDATE Docs_Lang SET";
                sql += " Content_DocLang = " + Content_Doc;
                sql += " WHERE id_DocLang = " + this.DocLangID;
                this.Conn.Execute(sql);
                sql_over += sql;

                //this.Connection.Close();
                value = value.Length == 1 ? "0" + value : value;
                value = value.Length == 2 ? "0" + value : value;
                NetCms.GUI.PopupBox.AddMessage("Revisione \"Rev#" + this.CurrentDocument.ID + "/" + value + "\" Impostata come Attiva", NetCms.GUI.PostBackMessagesType.Success);

                //SISTEMARE CON CACHE 2� LIVELLO
                //this.CurrentFolder.InvalidateDocs();
                
                
                ////PageData.CurrentFolder.Invalidate();

                if (this.CurrentFolder.EnableRss)
                    this.CurrentFolder.RssManager.SaveXML();
                //Msg += "<br>"+sql_over;
            }
            else
            {
                NetCms.GUI.PopupBox.AddSessionMessage("Spiaciente ma non � stato possibile pubblicare la Revisione \"Rev#" + this.CurrentDocument.ID + "\" a causa di un problema tecnico, se il problema persiste contattate l'amministratore. ", NetCms.GUI.PostBackMessagesType.Success);
            }

            #endregion

        }

        #region Colonne Personalizate

        private class ReviewColumnName : SmTableColumn
        {
            private string Doc;
            public ReviewColumnName(string fieldName, string label, string doc)
                : base(fieldName, label)
            {
                Doc = doc;
            }

            public override HtmlGenericControl getField(DataRow row)
            {
                string value = row[this.FieldName].ToString();
                value = value.Length == 1 ? "0" + value : value;
                value = value.Length == 2 ? "0" + value : value;
                HtmlGenericControl td = new HtmlGenericControl("td");

                string OutputValue = "Rev#" + Doc + "/" + value;

                td.InnerHtml = OutputValue;
                return td;
            }
        }

        private class ReviewColumnDate : SmTableColumn
        {

            public ReviewColumnDate(string fieldName, string label)
                : base(fieldName, label)
            {
            }

            public override HtmlGenericControl getField(DataRow row)
            {
                string value = row[this.FieldName].ToString();
                HtmlGenericControl td = new HtmlGenericControl("td");

                string OutputValue = value;
                if (value.Length > 10)
                    OutputValue = OutputValue.Replace(" ", " ore ");

                td.InnerHtml = OutputValue;
                return td;
            }
        }

        private class ReviewColumnAction : SmTableColumn
        {
            private NetCms.Connections.Connection _Conn;
            private NetCms.Connections.Connection Conn
            {
                get
                {
                    //if (_Conn == null)
                    //    _Conn = NetCms.Connections.ConnectionsManager.CurrentNetworkConnection;
                    //return _Conn;
                    NetCms.Connections.ConnectionsManager ConnManager = new NetCms.Connections.ConnectionsManager();
                    return ConnManager.CommonConnection;
                }
            }

            public Users.User _Account;
            public Users.User Account
            {
                get
                {
                    if (_Account == null) _Account = Users.AccountManager.CurrentAccount;
                        return _Account;
                }
            }

            private string Href = "javascript: setFormValue({0},'{1}',true)";
            private int Redactor;
            private int Publisher;
            private PageData PageData;
            private DataTable RevLog;
            private Document CurrentDocument;
            public ReviewColumnAction(int redactor, int publisher, DataTable revLog, PageData pageData, Document selectedDoc)
                : base("id_Revisione", "</th><th>")
            {
                Redactor = redactor;
                Publisher = publisher;
                RevLog = revLog;
                PageData = pageData;
                this.CurrentDocument = selectedDoc;
            }

            public override HtmlGenericControl getField(DataRow row)
            {
                HtmlGenericControl td = new HtmlGenericControl("td");

                int Autore = int.Parse(row["Autore_Revisione"].ToString());
                int Approvata = int.Parse(row["Approvata_Revisione"].ToString());
                int Publicata = int.Parse(row["Published_Revisione"].ToString());

                string html = "&nbsp;";
                string findLevel = "0";

                #region Se � stata publicata o per me � stata approvata(livello di approvazione arrivato al mio livello)
                //Se � stata publicata o per me � stata approvata(livello di approvazione arrivato al mio livello)
                if (Publicata == 1 || (Approvata != -1 && Approvata < this.Account.Depth))
                {
                    if (Publicata == 1)
                    {
                        html = "Revisione Pubblicata";
                        html += "</td><td>&nbsp;";
                    }
                    else
                    {
                        html = "Revisione Approvata ";
                        if (Publisher == 1)
                        {
                            html += "</td><td class=\"action web\">";
                            html += getPublishAction(row);
                        }
                        else
                            /*if (Redactor == 1)
                            {
                                html += "</td><td class=\"action ok\">";
                                html += getRequestAction(row);
                            }
                            else*/
                            html += "</td><td>&nbsp;";
                    }
                }
                #endregion

                #region Se(non � stata ancora publicata e non � stata appaovata da i livelli superiori al mio
                else//Se(non � stata ancora publicata e non � stata appaovata da i livelli superiori al mio
                {
                    DataRow[] Logs = RevLog.Select("Revisione_RevLog = " + row["id_Revisione"].ToString(), " id_RevLog DESC");

                    DataRow LastLog = null;
                    int i = 0;
                    bool xMe = false;

                    #region Cerco se esistono messaggi nella tabella revlog xme
                    int Mittente = 0;
                    while (Logs.Length > i && !xMe)
                    {
                        LastLog = Logs[i];
                        Mittente = int.Parse(LastLog["Mittente_RevLog"].ToString());
                        int Destinatario = int.Parse(LastLog["Destinatario_RevLog"].ToString());

                        if (!xMe && Mittente == this.Account.ID)
                            xMe = true;

                        if (!xMe && Destinatario == this.Account.ID)
                            xMe = true;

                        #region if (!xMe && Redactor == 1)

                        if (!xMe && Redactor == 1)
                        {
                            int LivelloPartenza = int.Parse(LastLog["Livello_RevLog"].ToString());

                            string sql = "SELECT * FROM Groups ORDER by Tree_Group";
                            DataTable Gruppi = this.Conn.SqlQuery(sql);

                            for (int j = 0; j < this.Account.Groups.Count; j++)
                            {
                                //NetCms.Users.User AccountMittente = new NetCms.Users.User(Mittente);
                                Users.User AccountMittente = Users.UsersBusinessLogic.GetById(Mittente);

                                #region this.Account.Groups[j].Depth == LivelloPartenza
                                //*****************************************************************************

                                Group cuurentGroup = this.Account.Groups.ElementAt(j);
                                if (cuurentGroup.Depth == LivelloPartenza)
                                {
                                    if (AccountMittente.Groups.Count > 0)
                                    {
                                        //devo controllare che parta da uno dei miei gruppi
                                        //sql = "Depth_Group<=" + LivelloPartenza +"AND";
                                        sql = "  Tree_Group LIKE '" + this.CurrentDocument.Groups.FindGroup(cuurentGroup.ID).Tree + "%'";

                                        for (int u = 0; u < AccountMittente.Groups.Count; u++)
                                        {
                                            if (u == 0)
                                                sql += " AND (";
                                            else
                                                sql += " OR ";
                                            string tree = this.CurrentDocument.Groups.FindGroup(AccountMittente.Groups.ElementAt(u).ID).Tree;
                                            sql += "  Tree_Group = '" + tree + "'";

                                            if (u + 1 == AccountMittente.Groups.Count)
                                                sql += " ) ";
                                        }
                                        DataRow[] rows = Gruppi.Select(sql, "Depth_Group Desc ");
                                        if (rows.Length > 0)
                                        {
                                            xMe = true;
                                            findLevel = cuurentGroup.Depth.ToString();
                                        }
                                    }

                                }
                                //*****************************************************************************
                                #endregion

                                #region this.Account.Groups[j].Depth < LivelloPartenza && !xMe

                                Group currentGroup = this.Account.Groups.ElementAt(j);

                                if (currentGroup.Depth < LivelloPartenza && !xMe)
                                {
                                    #region Prima devo trovare i miei gruppi che siano nel ramo di uno dei gruppi del mittente

                                    //Prima devo trovare i miei gruppi che siano nel ramo di uno dei gruppi del mittente
                                    string Tree_Group = this.CurrentDocument.Groups.FindGroup(currentGroup.ID).Tree;
                                    sql = "Tree_Group LIKE '" + Tree_Group + "%' ";
                                    bool first = true;
                                    for (int u = 0; u < AccountMittente.Groups.Count; u++)
                                    {
                                        string tree = this.CurrentDocument.Groups.FindGroup(AccountMittente.Groups.ElementAt(u).ID).Tree;
                                        tree = tree.Replace(Tree_Group, "");
                                        string[] ids = tree.Split('.');
                                        for (int e = 0; e < ids.Length - 1; e++)
                                        {
                                            if (ids[e].Length > 0)
                                            {
                                                if (first)
                                                {
                                                    sql += " AND (";
                                                    first = false;
                                                }
                                                else
                                                    sql += " OR ";
                                                sql += "  id_Group = " + ids[e] + "";
                                            }

                                        }
                                        if (u + 1 == AccountMittente.Groups.Count)
                                            sql += " ) ";
                                    }
                                    #endregion

                                    //Se ne faccio parte allora:
                                    //Devo controllare che nei gruppi che ci sono di mezzo non esistano revisori 
                                    DataRow[] rows = Gruppi.Select(sql, " Depth_Group Desc ");
                                    if (rows.Length > 0)
                                    {

                                        bool HasRevisors = false;

                                        for (int g = 0; g < rows.Length && !HasRevisors; g++)
                                        {
                                            int id = int.Parse(rows[g]["id_Group"].ToString());
                                            NetCms.Users.Group Gruppo = this.CurrentDocument.Groups.FindGroup(id);

                                            while (Gruppo != null && this.Account.Groups.ElementAt(j).Depth < Gruppo.Depth)
                                            {
                                                if (Gruppo.Depth <= LivelloPartenza)
                                                    for (int u = 0; u < Gruppo.Users.Count && !HasRevisors; u++)
                                                    {
                                                        HasRevisors = Gruppo.Users.ElementAt(u).Revisor == 1 ? true : false;
                                                    }
                                                Gruppo = Gruppo.Parent;
                                            }
                                        }
                                        xMe = !HasRevisors;

                                    }
                                    if (xMe)
                                        findLevel = this.Account.Groups.ElementAt(j).Depth.ToString();
                                }
                                #endregion

                            }

                        }
                        #endregion

                        i++;
                    }

                    #endregion
                    if (LastLog != null)
                    {
                        Mittente = int.Parse(LastLog["Mittente_RevLog"].ToString());
                    }

                    #region Messaggio NON xMe me
                    if (!xMe /*||//DA CONTROPLLARE
                            ( Approvata == this.Account.Depth && LastLog ==null)
                            */
                          )
                        if (Autore == this.Account.ID)
                        {
                            if (Publisher == 1)
                            {
                                html = "Archivato";
                                html += "</td><td class=\"action web\">" + getPublishAction(row);
                            }
                            else
                            {
                                html = "Archivato";
                                html += "</td><td class=\"action web\">" + getRequestAction(row);
                            }
                        }
                        else
                            this.ShowOwnerRow = false;
                    else
                    #endregion

                    #region Messaggio xMe me
                    {
                        int Oggetto = int.Parse(LastLog["Oggetto_RevLog"].ToString());

                        int Destinatario = int.Parse(LastLog["Destinatario_RevLog"].ToString());
                        if (Oggetto == 1)
                        {
                            if (Destinatario == 0 || Destinatario == this.Account.ID || Mittente == this.Account.ID)
                            {
                                html = "Approvata";
                                if (Publisher == 1)
                                {
                                    html += "</td><td class=\"action web\">";
                                    html += getPublishAction(row);
                                }
                                else
                                    if (Redactor == 1)
                                    {
                                        html += "</td><td class=\"action ok\">";
                                        html += getRequestAction(row);
                                    }
                            }
                            else
                                this.ShowOwnerRow = false;
                        }

                        if (Oggetto == 2)
                        {
                            if (Destinatario == 0 || Destinatario == this.Account.ID || Mittente == this.Account.ID)
                            {
                                if ((Publisher == 1 || Redactor == 1) && Mittente == this.Account.ID)
                                    html = "<a title=\"" + LastLog["Testo_RevLog"].ToString() + "\" href=\"javascript: alert('" + LastLog["Testo_RevLog"].ToString() + "','Motivazioni allegate alla Respinta')\">Hai Respinto Questa Revisione</a>";

                                else
                                    html = "<a title=\"" + LastLog["Testo_RevLog"].ToString() + "\" href=\"javascript: alert('" + LastLog["Testo_RevLog"].ToString() + "','Motivazioni allegate alla Respinta')\">Respinta</a>";

                                html += "</td><td>&nbsp";
                            }
                            else
                                this.ShowOwnerRow = false;
                        }
                        if (Oggetto == 0)
                            if (Mittente == this.Account.ID)
                            {
                                html = "Hai effettuato una richiesta </td><td class=\"action refuse\">" + getAnnullRequestAction(row, LastLog["id_RevLog"].ToString());
                            }
                            else
                            {
                                html = "Attendono la tua Approvazione " + findLevel + "</td><td class=\"action half\">" + getApproveRefuseAction(row, Mittente.ToString(), findLevel, int.Parse(LastLog["id_RevLog"].ToString())); ;
                            }
                    }
                    #endregion

                }

                #endregion
                td.InnerHtml = html;
                return td;
            }

            public string getPublishAction(DataRow row)
            {
                string value = row[this.FieldName].ToString();

                string html = "";
                html += "<a href=\"";
                html += Href.Replace("{0}", value);
                html = html.Replace("{1}", "publish");
                html += "\" title=\"" + "Pubblica" + "\"><span>";
                html += "Pubblica";
                html += "</span></a>";

                return html;
            }

            public string getApproveRefuseAction(DataRow row, string Destinatario, string MyLevel, int LogID)
            {
                string value = row[this.FieldName].ToString();

                string html = "";
                html += "<div class=\"approve\" >";
                html += "<a href=\"";
                html += Href.Replace("{0}", "'" + value + "!" + LogID + "!" + Destinatario + "!" + MyLevel + "'");
                html = html.Replace("{1}", "approve");
                html += "\" title=\"" + "Approva" + "\"><span>";
                html += "Approva";
                html += "</span></a></div>";

                string refact = "javascript: RefuseAction({0},'{1}',false)";
                string html2 = "";
                html += "<div class=\"refuse\" >";
                html2 += "<a href=\"";
                html2 += refact.Replace("{0}", "'" + value + "!" + LogID + "!" + Destinatario + "!" + MyLevel + "'");
                html2 = html2.Replace("{1}", "refuse");
                html2 += "\" title=\"" + "Respingi" + "\"><span>";
                html2 += "Respingi";
                html2 += "</span></a></div>";

                return html + html2;
            }

            public string getRequestAction(DataRow row)
            {
                string value = row[this.FieldName].ToString();

                string html = "";
                html += "<a href=\"";
                html += Href.Replace("{0}", value);
                html = html.Replace("{1}", "askrevision");
                html += "\" title=\"" + "Richiedi Approvazione" + "\"><span>";
                html += "Richiedi Approvazione";
                html += "</span></a>";

                return html;
            }

            public string getAnnullRequestAction(DataRow row, string LogID)
            {
                string value = row[this.FieldName].ToString();

                string html = "";
                html += "<a href=\"";
                html += Href.Replace("{0}", "'" + value + "!" + LogID + "'");
                html = html.Replace("{1}", "annullask");
                html += "\" title=\"" + "Annulla Richiesta" + "\"><span>";
                html += "Annulla Richiesta";
                html += "</span></a>";

                return html;
            }
        }

        protected abstract string PreviewJS
        {
            get;
        }


        public class OverviewsActionColumn : SmTableColumn
        {
            public string Classe;
            public string Label;
            public string Href;

            public OverviewsActionColumn(string fieldName, string classe, string label, string href)
                : base(fieldName, label)
            {
                Classe = classe;
                Label = label;
                Href = href;
            }

            public override HtmlGenericControl getField(DataRow row)
            {
                string value = row[this.FieldName].ToString();
                string folder = row["Table_Applicativo"].ToString();
                HtmlGenericControl td = new HtmlGenericControl("td");
                td.Attributes["class"] = Classe;
                string HrefTemp = Href;
                HrefTemp = HrefTemp.Replace("{0}", value);
                HrefTemp = HrefTemp.Replace("{1}", folder);

                string html = "";
                html += "<a href=\"";
                html += HrefTemp;
                html += "\" title=\"" + Label + "\"><span>";
                html += Label;
                html += "</span></a>";

                td.InnerHtml = html;
                return td;
            }
        }

        public class OverviewsCodeColumn : SmTableColumn
        {

            public OverviewsCodeColumn(string fieldName, string caption)
                : base(fieldName, caption)
            {
            }

            public override HtmlGenericControl getField(DataRow row)
            {
                string rev = row["Revisione_RevisioneNotifica"].ToString();
                string doc = row[this.FieldName].ToString();
                if (rev.Length == 1)
                    rev = "00" + rev;
                else if (rev.Length == 2)
                    rev = "0" + rev;
                HtmlGenericControl td = new HtmlGenericControl("td");

                td.InnerHtml = "&nbsp;Rev#" + doc + "/" + rev;

                return td;
            }

        }

        #endregion

        public virtual void SendNotify(int RevID, int LogID)
        {
            string Notified = ".";
            string Notifies = ".";
            int Max = 0;
            //NetCms.Users.UserCollection Users = PageData.Groups.AllUsers;
            ICollection<User> Users = PageData.Groups.AllUsers;
            for (int i = 0; i < Users.Count; i++)
            {
                User currentUser = Users.ElementAt(i);
                if (currentUser.ID != this.Account.ID && !Notified.Contains("." + currentUser.ID + "."))
                {
                    int val = Is4ME(currentUser.ID, RevID, Max);
                    Notifies += val + ".";
                    if (val > Max)
                        Max = val;
                    Notified += currentUser.ID + ".";
                }
            }

            string[] arrayNotifies = Notifies.Split('.');
            string[] arrayNotified = Notified.Split('.');

            string sqlmodel = "INSERT INTO Revisioni_Notifiche ";
            sqlmodel += " (Doc_RevisioneNotifica,Log_RevisioneNotifica,Revisione_RevisioneNotifica,Mittente_RevisioneNotifica,Destinatario_RevisioneNotifica,Stato_RevisioneNotifica) Values";
            int j = 0;
            for (j = 1; j < arrayNotifies.Length - 1; j++)
            {
                if (int.Parse(arrayNotifies[j]) == Max)
                {
                    //SendNotify(notifyUser);
                    string sql = "(";

                    sql += this.CurrentDocument.ID + ",";//Doc
                    sql += LogID + ",";//Log
                    sql += RevID + ",";//Revisione
                    sql += this.Account.ID + ",";//Mittente
                    sql += PageData.Groups.FindUser(int.Parse(arrayNotified[j])).ID + ",";//Destinatario
                    sql += "0";//Stato

                    sql += ")";
                    this.Conn.Execute(sqlmodel + sql);

                    string title = "Richiesta di revisione per il documento ''"+this.CurrentDocument.Nome.Replace("'","''")+"''";
                    string Text = "L''Utente " + this.Account.NomeCompleto.Replace("'", "''") + "(" + this.Account.UserName.Replace("'", "''") + ") ha inviato una richiesta di revisone per il documento ''" + this.CurrentDocument.Nome.Replace("'", "''") + "''";
                    NetCms.Users.User destinatario = PageData.Groups.FindUser(int.Parse(arrayNotified[j]));

                    //NetCms.Users.Activities.Activity.CreateNewActivity(title, Text, NetCms.Users.NotificaBackoffice.NotificaBackofficeType.Revisioni, destinatario.Profile.ID, this.Account.Profile.ID, "folder=" + this.CurrentFolder.ID + "&doc=" + CurrentDocument.ID, CurrentDocument.Network.ID);
                    NetCms.Users.NotificaBackoffice.CreateNewNotificaBackoffice(title, Text, NetCms.Users.NotificaBackoffice.NotificaBackofficeType.Revisioni, destinatario.Profile.ID, this.Account.Profile.ID, "folder=" + this.CurrentFolder.ID + "&doc=" + CurrentDocument.ID, CurrentDocument.Network.ID);
                }
            }
        }


        public virtual int Is4ME(int AccountID, int RevID, int MinDepth)
        {
            int output = 0;
            /*NetCms.Users.User Account = new NetCms.Users.User(AccountID);

            bool IsRedactor = false;

            #region Scopro se l'utente selezionato ha il permesso come redattore per questo documento

            Grants.DocGrantsFinder finder = new Grants.DocGrantsFinder( Account);
            NetCms.Structure.Grants.CriteriaCollection criteri = finder.Search(this.CurrentDocument);

            if (criteri["redactor"].Value > 0)
                IsRedactor = criteri["redactor"].Value == 1;
            else
            {
                StructureFolder folder = this.CurrentDocument.Folder;
                int specify = 0; Grants.FolderGrantsFinder folderfinder;
                while (specify == 0 && folder != null)
                {
                    folderfinder = new Grants.FolderGrantsFinder(Account);
                    criteri = folderfinder.Search(folder);

                    if (criteri["redactor"].Value > 0)
                        IsRedactor = criteri["redactor"].Value == 1;
                    specify = criteri["redactor"].Value;
                    folder = folder.Parent;
                }
            }
            #endregion

            DataRow[] Logs = RevLog.Select("Revisione_RevLog = " + RevID, " id_RevLog DESC");

            DataRow LastLog = Logs[0];
            bool xMe = false;
            string findLevel = "0";
            if (IsRedactor)
            {
                #region Cerco se esistono messaggi nella tabella revlog xme

                int LivelloPartenza = int.Parse(LastLog["Livello_RevLog"].ToString());

                string sql = "SELECT * FROM Groups ORDER by Tree_Group";
                DataTable Gruppi = this.Connection.SqlQuery(sql);

                for (int j = 0; j < Account.Groups.Count; j++)
                {
                    if (Account.Groups[j].Depth >= MinDepth)
                    {
                        NetCms.Users.User AccountMittente = this.Account;
                        //StructureGroups.Group Group = PageData.Groups.FindGroup(

                        #region NetCms.Users.SitemanagerUserGUI.Groups[j].Depth == LivelloPartenza
                        //*****************************************************************************
                        if (Account.Groups[j].Depth == LivelloPartenza)
                        {
                            if (AccountMittente.Groups.Count > 0)
                            {
                                //devo controllare che parta da uno dei miei gruppi
                                sql = "Depth_Group<=" + LivelloPartenza + " AND Tree_Group LIKE '" + this.CurrentDocument.Groups.FindGroup(Account.Groups[j].ID).Tree + "%'";

                                for (int u = 0; u < AccountMittente.Groups.Count; u++)
                                {
                                    if (u == 0)
                                        sql += " AND (";
                                    else
                                        sql += " OR ";
                                    string tree = this.CurrentDocument.Groups.FindGroup(AccountMittente.Groups[u].ID).Tree;
                                    sql += "  Tree_Group = '" + tree + "'";

                                    if (u + 1 == AccountMittente.Groups.Count)
                                        sql += " ) ";
                                }
                                DataRow[] rows = Gruppi.Select(sql, "Depth_Group Desc ");
                                if (rows.Length > 0)
                                {
                                    xMe = true;
                                    findLevel = rows[0]["Depth_Group"].ToString();
                                }
                            }

                        }
                        //*****************************************************************************
                        #endregion

                        #region NetCms.Users.SitemanagerUserGUI.Groups[j].Depth < LivelloPartenza && !xMe
                        if (Account.Groups[j].Depth < LivelloPartenza && !xMe)
                        {
                            //Prima devo controllare che un mio gruppo sia nel ramo di uno dei gruppi del mittente

                            sql = "Tree_Group LIKE '" + this.CurrentDocument.Groups.FindGroup(Account.Groups[j].ID).Tree + "%' ";
                            for (int u = 0; u < AccountMittente.Groups.Count; u++)
                            {
                                if (u == 0)
                                    sql += " AND (";
                                else
                                    sql += " OR ";
                                string tree = this.CurrentDocument.Groups.FindGroup(AccountMittente.Groups[u].ID).Tree;
                                sql += "  Tree_Group = '" + tree + "'";

                                if (u + 1 == AccountMittente.Groups.Count)
                                    sql += " ) ";
                            }
                            DataRow[] rows = Gruppi.Select(sql, " Depth_Group Desc ");
                            if (rows.Length > 0)
                            {
                                //sql = "Depth_Group<" + LivelloPartenza + " AND " + sql;
                                //if (Gruppi.Select(sql).Length==0)
                                //   xMe = true;
                                //else
                                {
                                    bool HasRevisors = false;

                                    for (int g = 0; g < rows.Length && !HasRevisors; g++)
                                    {
                                        int id = int.Parse(rows[g]["id_Group"].ToString());
                                        NetCms.Users.Group Gruppo = this.CurrentDocument.Groups.FindGroup(id);

                                        while (Gruppo != null && Account.Groups[j].Depth < Gruppo.Depth)
                                        {
                                            if (Gruppo.Depth < LivelloPartenza)
                                                for (int u = 0; u < Gruppo.Users.Count && !HasRevisors; u++)
                                                {
                                                    HasRevisors = ((Users.User)Gruppo.Users[u]).Revisor == 1 ? true : false;
                                                }
                                            Gruppo = Gruppo.Parent;
                                        }
                                    }
                                    //Se ne faccio parte allora:
                                    //Devo controllare che nei gruppi che ci sono di mezzo non esistano revisori 
                                    xMe = !HasRevisors;
                                }
                            }
                            if (xMe)
                                findLevel = Account.Groups[j].Depth.ToString();

                        }
                        #endregion

                    }

                }
                #endregion
            }

            if (xMe && output < 2)
                output = 1;

            return output == 1 ? int.Parse(findLevel) : -1;*/
            return 0;
        }

        public HtmlGenericControl GetView(PageData pageData)
        {

            HtmlGenericControl output = new HtmlGenericControl("div");
            //output.Controls.Add(Toolbar.getControl());

            string sql = "SELECT * FROM Revisioni_Notifiche";
            sql += " INNER JOIN Docs ON id_Doc = Doc_RevisioneNotifica";
            sql += " INNER JOIN Folders ON id_Folder = Folder_Doc";
            sql += " INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang";
            sql += " INNER JOIN Applicazioni  ON Application_Doc = id_Applicazione";
            sql += " INNER JOIN Users ON id_User = Mittente_RevisioneNotifica";
            sql += " WHERE Destinatario_RevisioneNotifica = " + pageData.Account.ID;
            sql += " AND Stato_RevisioneNotifica = 0";
            sql += " AND Lang_DocLang = " + pageData.DefaultLang;
            sql += " AND Network_Folder = " + pageData.Network.ID;
            sql += " ORDER BY id_RevisioneNotifica DESC";

            SmTable table = new SmTable(pageData.PageIndex, sql, pageData.Conn);

            #region Campi della tabella

            SmTableColumn col;

            col = new SmTableColumn("Label_DocLang", "Documento");
            table.addColumn(col);

            col = new SmTableColumn("Name_User", "Richiedente");
            table.addColumn(col);

            col = new OverviewsCodeColumn("id_Doc", "Codice Revisione");
            table.addColumn(col);

            string href = pageData.Paths.AbsoluteNetworkRoot + "/{1}/{1}_overview.aspx?id={0}";

            col = new OverviewsActionColumn("id_Doc", "action web", "Revisiona", href);
            table.addColumn(col);

            #endregion

            output.Controls.Add(table.getTable());
            return output;
        }


        public virtual void ClearNotify(string LogID)
        {
            this.Conn.Execute("UPDATE Revisioni_Notifiche SET Stato_RevisioneNotifica = 1 WHERE Log_RevisioneNotifica = " + LogID);
        }

        public static WebControl FabricatePage()
        {
            if (Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork)
            {
                //Application application = ((StructureFolder)Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder).RelativeApplication;

                Document doc = (Document)Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.CurrentDocument;
                return (DocumentOverview)ApplicationsPool.BuildInstanceOf(doc.RelativeApplication.Classes.Overview, doc.RelativeApplication.ID.ToString());
            }
            else return new NetCms.GUI.SmPageError();
        }
    }

}