using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using NetCms.Grants;

/// <summary>
/// Summary description for StructureDocument
/// </summary>
namespace NetCms.Structure
{
    public class DocumentMeta
    {
        private int _Meta = -1;
        private int Meta
        {
            get
            {
                if (_Meta < 0)
                {
                    NetUtility.RequestVariable RequestMeta = new NetUtility.RequestVariable("meta", NetUtility.RequestVariable.RequestType.QueryString);
                    if (RequestMeta.IsValidInteger)
                        _Meta = RequestMeta.IntValue;
                    else
                        _Meta = 0;
                }
                return _Meta;
            }
        }

        private PageData PageData;
        public string getMetaName(string id)
        {
            string sql = "SELECT * FROM Docs_Meta";
            sql += " INNER JOIN Meta ON id_Meta = Meta_DocMeta ";
            sql += " WHERE id_DocMeta = " + id;
            DataTable reader = PageData.Conn.SqlQuery(sql);
            if (reader.Rows.Count > 0)
                return reader.Rows[0]["Name_Meta"].ToString();
            return "";
        }
        public string getMetaType(string id)
        {
            string sql = "SELECT * FROM Docs_Meta";
            sql += " INNER JOIN Meta ON id_Meta = Meta_DocMeta ";
            sql += " WHERE id_DocMeta = " + id;
            DataTable reader = PageData.Conn.SqlQuery(sql);
            if (reader.Rows.Count > 0)
                return reader.Rows[0]["id_Meta"].ToString();
            return "";
        }
        private Document _Document;
        public Document Document
        {
            get
            {
                if (_Document == null)
                {
                    NetUtility.QueryStringRequestVariable id = new NetUtility.QueryStringRequestVariable("doc");
                    if (id.IsValidInteger)
                        _Document = PageData.CurrentFolder.FindDocByID(id.IntValue) as Document;
                }
                if (_Document == null || !_Document.Grant || _Document.Criteria[CriteriaTypes.Advanced].Value == GrantsValues.Deny)
                    PageData.GoToErrorPage(_Document);

                return _Document;
            }
        }

        public DocumentMeta(PageData pageData)
        {
            PageData = pageData;
        }

        private NetFormTable _FormTable;
        private NetFormTable FormTable
        {
            get
            {
                if (_FormTable == null)
                {
                    _FormTable = new NetFormTable("Docs_Meta", "id_DocMeta", PageData.Conn);

                    string MetaTypesSql;

                    if (_Meta > 0)
                    {
                        _FormTable.ID = Meta;
                    }


                    #region Campi

                    //MetaTypesSql = "SELECT * FROM Docs_Meta WHERE Doc_DocMeta = " + Document.ID + "";
                    //DataTable tab = PageData.Conn.SqlQuery(MetaTypesSql);

                    MetaTypesSql = "SELECT Name_Meta,id_Meta FROM Meta ORDER BY Name_Meta";
                    DataTable MetaTable = PageData.Conn.SqlQuery(MetaTypesSql);

                    NetDropDownList meta = new NetDropDownList("Meta", "Meta_DocMeta");
                    meta.ClearList();
                    foreach (DataRow row in MetaTable.Rows)
                    {
                        //var selectedRows = tab.Select("Meta_DocMeta = " + row["id_Meta"]);
                        //if (selectedRows.Length == 0 || Meta.ToString() == selectedRows[0]["id_DocMeta"].ToString())
                        meta.addItem(row["Name_Meta"].ToString(), row["id_Meta"].ToString());
                    }
                    meta.Required = true;
                    _FormTable.addField(meta);

                    NetDropDownList Stato = new NetDropDownList("Stato", "Stato_DocMeta");
                    Stato.ClearList();
                    Stato.addItem("Attivo", "1");
                    Stato.addItem("Non Attivo", "0");
                    Stato.Required = true;
                    _FormTable.addField(Stato);

                    NetTextBoxPlus content = new NetTextBoxPlus("Content", "Content_DocMeta");
                    content.Required = true;
                    content.MaxLenght = 0;
                    content.Rows = 7;
                    content.Size = 70;
                    _FormTable.addField(content);

                    NetHiddenField doc = new NetHiddenField("Doc_DocMeta");
                    doc.Value = Document.ID.ToString();
                    _FormTable.addField(doc);

                    #endregion
                }
                return _FormTable;
            }
        }

        public HtmlGenericControl GetNewView()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");

            G2Core.Common.RequestVariable meta = new G2Core.Common.RequestVariable("Meta_DocMeta", G2Core.Common.RequestVariable.RequestType.Form);

            G2Core.XhtmlControls.Div divset = new G2Core.XhtmlControls.Div();

            NetForm form = new NetForm();
            form.AddTable(FormTable);
            form.SubmitLabel = "Aggiungi Meta";
            string errors = "";

            PageData.InfoToolbar.Icon = "Meta";
            PageData.InfoToolbar.Title = "Aggiunta Meta al Documento '" + Document.Nome + "'";
            PageData.InfoToolbar.ShowFolderPath = true;

            //string Utente =  PageData.Account.NomeCompleto + " (" + PageData.Account.UserName + ")";
            //string logText = "L'utente " + Utente + "vuole aggiungere meta per il documento  " + Document.Label;
            //PageData.GuiLog.SaveLogRecord(logText, false);

            bool presente = false;

            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
            if (PageData.IsPostBack)
            {
                string sql = "SELECT * FROM Docs_Meta WHERE Doc_DocMeta = " + Document.ID + " AND Meta_DocMeta = " + meta.StringValue;
                DataTable tab = PageData.Conn.SqlQuery(sql);
                G2Core.Common.RequestVariable confermaOk = new G2Core.Common.RequestVariable("conferma");
                if (tab.Rows.Count > 0 && !confermaOk.StringValue.Equals("Sovrascrivi"))
                {
                    presente = true;
                }
                else
                {
                    errors = form.serializedValidation();
                    if (errors == null)
                    {
                        if (confermaOk.StringValue.Equals("Sovrascrivi"))
                        {
                            sql = "DELETE FROM Docs_Meta WHERE Doc_DocMeta = " + Document.ID + " AND Meta_DocMeta = " + meta.StringValue;
                            PageData.Conn.Execute(sql);
                        }

                        form.serializedInsert();
                        if (form.LastOperation == NetForm.LastOperationValues.OK)
                        {
                            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha aggiunto con successo un meta tag al documento '" + Document.Nome + "' (id=" + Document.ID + "), relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                            NetCms.GUI.PopupBox.AddSessionMessage("Meta aggiunto con successo.", PostBackMessagesType.Success, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());

                            HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset(PageData.InfoToolbar.Title);
                            HtmlGenericControls.Div setcontent = new HtmlGenericControls.Div();
                            set.Controls.Add(setcontent);
                            setcontent.InnerHtml = "Meta aggiunto con successo.<br /><a href=\"" + PageData.BackAddress + "\">Torna Indietro</a>";
                        }
                    }
                    else
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non � riuscito ad aggiungere un meta tag al documento '" + Document.Nome + "' (id=" + Document.ID + "), relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        NetCms.GUI.PopupBox.AddSessionMessage(errors, PostBackMessagesType.Error);
                    }
                }
            }
            else
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di aggiunta Meta al documento '" + Document.Nome + "' (id=" + Document.ID + ").", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            }

            if (presente)
                form.AddSubmitButton = false;

            divset.Controls.Add(form.getForms(PageData.InfoToolbar.Title));
            div.Controls.Add(divset);

            if (presente)
            {
                divset = new G2Core.XhtmlControls.Div();
                Label labelPresente = new Label();
                labelPresente.Text = "Meta della tipologia selezionata gi� presente. Vuoi sovrascriverlo?";
                Button conferma = new Button();
                conferma.ID = "conferma";
                conferma.Text = "Sovrascrivi";
                divset.Controls.Add(labelPresente);
                divset.Controls.Add(conferma);
                G2Core.XhtmlControls.Fieldset set = new G2Core.XhtmlControls.Fieldset("Conferma", divset);
                set.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#ffffee");
                div.Controls.Add(set);
            }

            return div;
        }
        public HtmlGenericControl GetModView()
        {
            if (Meta < 1)
                PageData.GoBack();

            HtmlGenericControl div = new HtmlGenericControl("div");
            HtmlGenericControl divset = new G2Core.XhtmlControls.Div();

            G2Core.Common.RequestVariable metaDropDown = new G2Core.Common.RequestVariable("Meta_DocMeta", G2Core.Common.RequestVariable.RequestType.Form);

            NetForm form = new NetForm();
            form.AddTable(FormTable);
            form.SubmitLabel = "Aggiorna Meta";
            string errors = "";

            PageData.InfoToolbar.Icon = "Meta";
            PageData.InfoToolbar.Title = "Aggiornamento Meta del Documento '" + Document.Nome + "'";
            PageData.InfoToolbar.ShowFolderPath = true;

            bool presente = false;

            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
            if (PageData.IsPostBack)
            {
                DataTable tab = null;
                G2Core.Common.RequestVariable confermaOk = new G2Core.Common.RequestVariable("conferma");
                if (metaDropDown.StringValue != getMetaType(Meta.ToString()))
                {
                    string sql = "SELECT * FROM Docs_Meta WHERE Doc_DocMeta = " + Document.ID + " AND Meta_DocMeta = " + metaDropDown.StringValue;
                    tab = PageData.Conn.SqlQuery(sql);
                    if (tab.Rows.Count > 0 && !confermaOk.StringValue.Equals("Sovrascrivi"))
                    {
                        presente = true;
                    }
                }

                if (!presente)
                {
                    errors = form.serializedValidation();
                    if (errors == null)
                    {
                        if (confermaOk.StringValue.Equals("Sovrascrivi"))
                        {
                            string sql = "DELETE FROM Docs_Meta WHERE Doc_DocMeta = " + Document.ID + " AND Meta_DocMeta = " + metaDropDown.StringValue;
                            PageData.Conn.Execute(sql);
                        }

                        form.serializedUpdate();
                        if (form.LastOperation == NetForm.LastOperationValues.OK)
                        {
                            HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset(PageData.InfoToolbar.Title);
                            HtmlGenericControls.Div setcontent = new HtmlGenericControls.Div();
                            set.Controls.Add(setcontent);

                            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha aggiornato con successo il Meta del documento '" + Document.Nome + "' (id=" + Document.ID + ").", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                            setcontent.InnerHtml = "Meta aggiornato con successo.<br /><a href=\"" + PageData.BackAddress + "\">Torna Indietro</a>";
                            NetCms.GUI.PopupBox.AddSessionMessage("Meta aggiornato con successo.", PostBackMessagesType.Success, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                        }
                    }
                    else
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non � riuscito ad aggiungere un meta tag al documento '" + Document.Nome + "' (id=" + Document.ID + "), relativamente al portale '" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        NetCms.GUI.PopupBox.AddSessionMessage(errors, PostBackMessagesType.Error);

                    }
                }
            }
            else
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di aggiornamento Meta del documento '" + Document.Nome + "' (id=" + Document.ID + ").", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

            }

            if (presente)
                form.AddSubmitButton = false;

            divset.Controls.Add(form.getForms(PageData.InfoToolbar.Title));
            div.Controls.Add(divset);

            if (presente)
            {
                divset = new G2Core.XhtmlControls.Div();
                Label labelPresente = new Label();
                labelPresente.Text = "Meta della tipologia selezionata gi� presente. Vuoi sovrascriverlo?";
                Button conferma = new Button();
                conferma.ID = "conferma";
                conferma.Text = "Sovrascrivi";
                divset.Controls.Add(labelPresente);
                divset.Controls.Add(conferma);
                G2Core.XhtmlControls.Fieldset set = new G2Core.XhtmlControls.Fieldset("Conferma", divset);
                set.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#ffffee");
                div.Controls.Add(set);
            }

            return div;
        }
        public HtmlGenericControl GetListView()
        {
            PageData.SideToolbar.addButton(new SideToolbarButton("Meta", "docs/docs_meta_add.aspx?doc=" + Document.ID, NetCms.GUI.GuiUtility.ImagesIndexer.GetImageURL(NetCms.GUI.Css.ImagesIndexer.Images.Toolbar_Meta_Add), "Aggiungi Meta", PageData, true));

            PageData.InfoToolbar.Icon = "Meta";
            PageData.InfoToolbar.Title = "Meta del Documento '" + Document.Nome + "'";
            PageData.InfoToolbar.ShowFolderPath = true;

            HtmlGenericControl div = new HtmlGenericControl("div");
            //string Utente =  PageData.Account.NomeCompleto + " (" + PageData.Account.UserName + ")";
            //string logText = "L'utente " + Utente + "vuole gestire i meta per il documento  " + Document.Label;
            //PageData.GuiLog.SaveLogRecord(logText, false);

            #region Eliminazione Record

            if (PageData.IsPostBack)
            {
                NetUtility.RequestVariable DeleteRequest = new NetUtility.RequestVariable("del", NetUtility.RequestVariable.RequestType.Form);
                if (DeleteRequest.IsValidInteger)
                {
                    PageData.Conn.Execute("DELETE FROM Docs_Meta WHERE id_DocMeta = " + DeleteRequest.IntValue);
                    //Utente = PageData.Account.NomeCompleto + " (" + PageData.Account.UserName + ")";
                    //logText = "L'utente " + Utente + " vuole eliminare il meta" + getMetaName(DeleteRequest.ToString()) + "per il documento  " + Document.Label;
                    //PageData.GuiLog.SaveLogRecord(logText);
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha eliminato il meta con id=" + DeleteRequest.IntValue + " del documento '" + Document.Nome + "' (id=" + Document.ID + ").", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                }
            }
            else
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di gestione dei meta del documento '" + Document.Nome + "' (id=" + Document.ID + ").", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

            }

            #endregion

            string sql = "SELECT * FROM Docs_Meta";
            sql += " INNER JOIN Meta ON id_Meta = Meta_DocMeta ";
            sql += " INNER JOIN Docs ON id_Doc = Doc_DocMeta";
            sql += " INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang";
            sql += " WHERE id_Doc = " + Document.ID + " AND Lang_DocLang = " + PageData.DefaultLang;

            NetTable.SmTable table = new NetTable.SmTable(sql, PageData.Conn);

            NetTable.SmTableColumn nome = new NetTable.SmTableColumn("Name_Meta", "Name");
            table.addColumn(nome);

            NetTable.SmTableColumn content = new NetTable.SmTableColumn("Content_DocMeta", "Content");
            content.CutTextAt = 150;
            table.addColumn(content);

            NetTable.SmTableBoleanColumn stato = new NetTable.SmTableBoleanColumn("Stato_DocMeta", "Stato", "Attivo", "Non Attivo");
            table.addColumn(stato);

            NetTable.SmTableActionColumn modifica = new NetTable.SmTableActionColumn("id_DocMeta", "action modify", "Modifica", "docs_meta_mod.aspx?meta={0}&amp;doc=" + Document.ID + "&amp;folder=" + Document.Folder.ID);
            table.addColumn(modifica);

            NetTable.SmTableActionColumn elimina = new NetTable.SmTableActionColumn("id_DocMeta", "action delete", "Elimina", "javascript: setdel({0});");
            table.addColumn(elimina);

            div.Controls.Add(table.getTable());

            return div;
        }

    }
}