﻿using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetService.Utility.ValidatedFields;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace NetCms.Structure.WebFS
{
    public class AttachesGui
    {
        private const string tmpPathPattern = "/repository/contents/{year}/{month}/{day}";
          
        private string TmpFolderPath
        {
            get
            {
                string tmpPath = NetCms.Configurations.Paths.AbsoluteRoot + tmpPathPattern;
                tmpPath = tmpPath.Replace("{year}", DateTime.Today.Year.ToString())
                                   .Replace("{month}", DateTime.Today.Month.ToString())
                                   .Replace("{day}", DateTime.Today.Day.ToString());
                return tmpPath;
            }
        }

        private String[] AllowedExtentions
        {
            get;
        }

        //public int DocID
        //{
        //    get; 
        //    set;
        //}

        private int DocContentID
        {
            get;
            set;
        }


        private DocContent currentDocContent;
        private DocContent CurrentDocContent
        {
            get
            {
                if (currentDocContent == null)                
                    currentDocContent = DocumentBusinessLogic.GetDocContentById(DocContentID);                                  
                return currentDocContent;
            }
        }

        public int ApplicationId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationId">Key of cms application</param>
        /// <param name="allowedExtentions">An string array such as { "doc", "docx", "pdf", "p7m", "rtf", "zip", "xlsx", "xls" }</param>
        public AttachesGui(int applicationId, int docContentID, String[] allowedExtentions)
        {
            ApplicationId = applicationId;
            // DocID = docID;
            DocContentID = docContentID;
            AllowedExtentions = allowedExtentions;
        }

        public WebControl GetControl()
        {
            WebControl uploadForm = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
            uploadForm.Attributes["class"] = "netformtable attachesctrl";

            WebControl upload = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
            upload.Attributes["class"] = "innerContent";
            upload.Controls.Add(UploadField);

            uploadForm.Controls.Add(upload);

            return uploadForm;
        }

        private List<FileUploadedV2> _FileUploaded;
        private List<FileUploadedV2> FileUploaded
        {
            get
            {
                if (_FileUploaded == null)
                {
                    _FileUploaded = new List<FileUploadedV2>();
                    if (CurrentDocContent != null && CurrentDocContent.Attaches.Any())
                    {
                        FileUploadedV2 file;
                        foreach (DocumentAttach attach in CurrentDocContent.Attaches)
                        {
                            Document allegato = attach.FileDocument;

                            file = new FileUploadedV2();
                            file.ID = attach.Id;
                            file.FileName = allegato.Nome;
                            file.Ext = allegato.Nome.Substring(allegato.Nome.LastIndexOf('.') + 1, 3);
                            _FileUploaded.Add(file);
                        }
                    }
                }

                return _FileUploaded;
            }
        }

        private VfMultiUpload2 uploadField;
        public VfMultiUpload2 UploadField
        {
            get
            {
                if (uploadField == null)
                {
                    uploadField = new VfMultiUpload2("allegati",
                                                    "Documenti allegati",
                                                    TmpFolderPath,
                                                    null,
                                                    AllowedExtentions,
                                                    0,
                                                    true,
                                                    FileUploaded
                                                    );

                    uploadField.Mode = VfMultiUpload2.FieldMode.FileSystemWithJavascript;
                    uploadField.Required = false;
                    uploadField.BindField = false;
                    uploadField.DisplayFileList = true;
                    uploadField.AllowDelete = true;//??
                }
                return uploadField;
            }
        }
           
    }

}
