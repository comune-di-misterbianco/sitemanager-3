﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using NetCms.Structure.WebFS;
using System.Linq;
using System.Collections.Generic;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;

/// <summary>
/// Summary description for StructureDocument
/// </summary>
namespace NetCms
{
    namespace Structure
    {
        public class DocumentModView : NetworkPage
        {
            public override NetCms.GUI.SideBar.SideBar SideBar
            {
                get { return _SideBar; }
            }
            private NetCms.GUI.SideBar.SideBar _SideBar;

            public override string PageTitle
            {
                get { return "Modifica proprietà documento '" + this.CurrentDocument.Nome + "'"; }
            }

            public override NetCms.GUI.Icons.Icons PageIcon
            {
                get { return NetCms.GUI.Icons.Icons.Cog_Edit; }
            }

            public override string LocalCssClass
            {
                get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
            }

            public DocumentModView()
            {
                _SideBar = new NetCms.GUI.SideBar.CmsSideBar(this);
            }

            protected override WebControl BuildControls()
            {
                WebControl div = new WebControl(HtmlTextWriterTag.Div);
                div.CssClass = "container-fluid";

                HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");

                HtmlGenericControl legend = new HtmlGenericControl("legend");
                legend.InnerText = "Proprietà Documento '" + this.CurrentDocument.Nome + "'";
                fieldset.Controls.Add(legend);

                HtmlGenericControl contentDiv = new HtmlGenericControl("div");
                contentDiv.Attributes["class"] = "contentForm docform col-md-12";


                //string Utente = this.Account.Label;
                //string logText = "L'utente " + Utente + " vuole aggiornare le proprietà per il documento '" + this.CurrentDocument.Label + "'";
                //this.GuiLog.SaveLogRecord(logText);

                NetFormTable docs = Document.DocsFormTable(PageData.CurrentReference, this.CurrentDocument.Application, this.CurrentDocument);
                docs.CssClass = "netformtable_docsform";
                NetFormTable lang = Document.LanguageFormTable(PageData.CurrentReference, this.CurrentDocument.DocLangs.ElementAt(0).ID.ToString());
                lang.CssClass = "netformtable_langform";

                NetForm form = new NetForm();
                form.AddTable(lang);

                //Inserimento codice per il supporto multilingua
                #region Supporto multilingua
                WebControl langContols = new WebControl(HtmlTextWriterTag.Div);

                //Aggiunta l'esclusione dei documenti di tipo homepage in quanto non hanno il supporto alla traduzione multilingua
                if (LanguageManager.Utility.Config.EnableMultiLanguage && this.CurrentDocument.Application != 1 && this.CurrentDocument.Application != 6)
                {
                    IList<LanguageManager.Model.Language> languages = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages();
                    if (languages != null && languages.Count > 0)
                    {
                        string insertAfterId = "Docs_Lang";
                        foreach (LanguageManager.Model.Language l in languages.Where(x => x.Default_Lang == 0))
                        {
                            #region Lang
                            Collapsable colTrad = new Collapsable("Traduzione " + l.Nome_Lang);
                            colTrad.ID = "Docs_Lang#" + l.LangTwoLetterCode;
                            colTrad.StartCollapsed = true;

                            //NetFormTable FoldersLang = new NetFormTable("FoldersLang_" + l.LangTwoLetterCode, "id_FolderLang_" + l.LangTwoLetterCode);

                            NetTextBoxPlus nomeTrad = new NetTextBoxPlus("Titolo " + l.Nome_Lang, "Label_DocLang_" + l.LangTwoLetterCode);
                            nomeTrad.Required = false;
                            nomeTrad.Size = 60;
                            

                            //FoldersLang.addField(nomeTrad);

                            NetTextBoxPlus DescrizioneTrad = new NetTextBoxPlus("Descrizione " + l.Nome_Lang, "Desc_DocLang_" + l.LangTwoLetterCode);
                            DescrizioneTrad.Required = false;
                            DescrizioneTrad.MaxLenght = 2000;
                            DescrizioneTrad.Rows = 5;
                            DescrizioneTrad.Size = 60;
                            

                            if (this.CurrentDocument.DocLangs.Count(x => x.Lang == l.ID) > 0)
                            {
                                DocumentLang curLang = this.CurrentDocument.DocLangs.Where(x => x.Lang == l.ID).First();
                                nomeTrad.Value = curLang.Label;
                                DescrizioneTrad.Value = curLang.Descrizione;
                            }

                            colTrad.AppendControl(nomeTrad.getControl());
                            colTrad.AppendControl(DescrizioneTrad.getControl());

                            langContols.Controls.Add(colTrad);
                            #endregion

                            form.ControlToInsertAfter.Add(new KeyValuePair<string, WebControl>(insertAfterId, colTrad));
                            insertAfterId = colTrad.ID;
                        }
                    }
                }
                #endregion


                form.AddTable(docs);

                string errors = "";
                form.SubmitLabel = "Aggiorna Dati";

                if (docs.IsPostBack && form.CheckSubmitSource)
                {
                    bool languageError = false;
                    //Aggiunta l'esclusione dei documenti di tipo homepage in quanto non hanno il supporto alla traduzione multilingua
                    if (LanguageManager.Utility.Config.EnableMultiLanguage && this.CurrentDocument.Application != 1 && this.CurrentDocument.Application != 6)
                    {
                        IList<LanguageManager.Model.Language> languages = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages();
                        if (languages != null && languages.Count > 0)
                        {
                            foreach (LanguageManager.Model.Language l in languages.Where(x => x.Default_Lang == 0))
                            {
                                string nameTrad = HttpContext.Current.Request.Form["Label_DocLang_" + l.LangTwoLetterCode].ToString();
                                string descTrad = HttpContext.Current.Request.Form["Desc_DocLang_" + l.LangTwoLetterCode].ToString();

                                if (!string.IsNullOrEmpty(nameTrad) && !string.IsNullOrEmpty(descTrad))
                                {
                                    //Verifico che il nome non contenga lo slash o altri caratteri speciali che manderebbero in errore l'url generato al volo
                                    if (NetUtility.NameControls.CheckName(nameTrad))
                                    {
                                        //Entrambi popolati, posso inserire la traduzione
                                        //Se Esiste modifico il record
                                        if (this.CurrentDocument.DocLangs.Count(x => x.Lang == l.ID) > 0)
                                        {
                                            DocumentLang langToModify = this.CurrentDocument.DocLangs.First(x => x.Lang == l.ID);
                                            langToModify.Label = nameTrad;
                                            langToModify.Descrizione = descTrad;

                                            DocumentBusinessLogic.SaveOrUpdateDocumentLang(langToModify);
                                        }
                                        else
                                        {
                                            //Se non esiste lo creo
                                            DocumentLang langToCreate = new DocumentLang();
                                            langToCreate.Label = nameTrad;
                                            langToCreate.Descrizione = descTrad;
                                            langToCreate.Document = this.CurrentDocument;
                                            langToCreate.Lang = l.ID;
                                            this.CurrentDocument.DocLangs.Add(langToCreate);

                                            DocumentBusinessLogic.SaveOrUpdateDocument(this.CurrentDocument);
                                        }
                                    }
                                    else
                                    {
                                        //Mostro un messaggio per avvisare l'utente che il nome non può contenere caratteri speciali
                                        string messaggio = "Il campo '<strong>Nome</strong>' per la lingua " + l.Nome_Lang + " non può contenere caratteri speciali";
                                        if (errors != null && errors.Length > 0)
                                            errors = errors.Replace("<ul>", "<ul><li>" + messaggio + "</li>");
                                        else
                                            errors = "<div class=\"form-error\"><ul><li>" + messaggio + "</li></ul></div>";
                                    }
                                }
                                else
                                {
                                    //Se solo uno è popolato mostro un messaggio di errore affinchè vengano valorizzati entrambi
                                    if (!string.IsNullOrEmpty(nameTrad) || !string.IsNullOrEmpty(descTrad))
                                    {
                                        string messaggio = "I campi '<strong>Nome</strong>' e '<strong>Descrizione</strong>' devono essere compilati entrambi o nessuno per la lingua " + l.Nome_Lang;
                                        if (errors != null && errors.Length > 0)
                                            errors = errors.Replace("<ul>", "<ul><li>" + messaggio + "</li>");
                                        else
                                            errors = "<div class=\"form-error\"><ul><li>" + messaggio + "</li></ul></div>";
                                        languageError = true;
                                    }
                                    else
                                    {
                                        //Nessuno dei due è popolato, rimuovo la traduzione
                                        //if (this.CurrentDocument.DocLangs.Count(x => x.Lang == l.ID) > 0)
                                        //{
                                        //    DocumentLang langToDelete = this.CurrentDocument.DocLangs.First(x => x.Lang == l.ID);
                                        //    this.CurrentDocument.DocLangs.Remove(langToDelete);
                                        //    langToDelete.Document = null;
                                        //    FolderBusinessLogic.DeleteFolderLang(langToDelete);
                                        //}
                                    }
                                }
                            }
                        }
                    }


                    errors += form.serializedValidation();
                    if (string.IsNullOrEmpty(errors))
                        errors = form.serializedUpdate(HttpContext.Current.Request.Form);

                    if (form.LastOperation == NetForm.LastOperationValues.OK && !languageError)
                    {
                        //SISTEMARE CON CACHE 2° LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                        
                        this.CurrentFolder.Invalidate();
                        if (this.CurrentFolder.Parent != null)
                            ((StructureFolder)this.CurrentFolder.Parent).Invalidate();

                        //this.PostbackBox.AddPostbackInfoLine(errors);

                        G2Core.Common.RequestVariable tags = new G2Core.Common.RequestVariable("Tags");
                        // NetCms.Structure.WebFS.Tags.AddTagsForDoc(tags.StringValue, this.CurrentDocument.ID.ToString(), this.CurrentFolder.StructureNetwork.Connection);

                        // Gestione dei tag del documento
                        if (tags.IsValidString)
                        {
                            //todo: deserialize tags
                            NetCms.Structure.WebFS.Documents.Tags.BusinessLogic.TagsBusinessLogic.AddTagForDoc(tags.OriginalValue, this.CurrentDocument.ID);
                        }


                        if (CmsConfigs.Model.GlobalConfig.IndexConfig != null && CmsConfigs.Model.GlobalConfig.IndexConfig.SolrEnabled)
                        {
                            CmsConfigs.BusinessLogic.ContentIndex.IndexContentSolr.AddUpdateDocument(this.CurrentDocument.ID);
                        }  

                        string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha modificato con successo le proprietà del documento '" + this.CurrentDocument.Nome + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                        NetCms.GUI.PopupBox.AddSessionMessage(errors, PostBackMessagesType.Success, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
                    }
                    else
                    {
                        string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a modificare le proprietà del documento '" + this.CurrentDocument.Nome + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);    
                        NetCms.GUI.PopupBox.AddSessionMessage(errors, PostBackMessagesType.Error, HttpContext.Current.Request.Url.OriginalString);
                    }
                }
                else {
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di modifica proprietà del documento '" + this.CurrentDocument.Nome + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);    
                }
                
                //div.Controls.Add(form.getForms("Proprietà Documento '" + this.CurrentDocument.Nome + "'"));

                contentDiv.Controls.Add(lang.getForm());

                if (LanguageManager.Utility.Config.EnableMultiLanguage)
                {
                    contentDiv.Controls.Add(langContols);
                }

                contentDiv.Controls.Add(docs.getForm());

                HtmlGenericControl pButtons = new HtmlGenericControl("p");
                pButtons.Attributes["class"] = "buttonscontainer";

                Button bt = new Button();
                bt.Text = "Aggiorna Dati";
                bt.ID = "send";
                bt.CssClass = "button";
                bt.OnClientClick = "javascript: setSubmitSource('send')";
                pButtons.Controls.Add(bt);

                contentDiv.Controls.Add(pButtons);

                WebControl row = new WebControl(HtmlTextWriterTag.Div);
                row.CssClass = "row";

                row.Controls.Add(contentDiv);

                fieldset.Controls.Add(row);

                div.Controls.Add(fieldset);

                return div;
            }
        }
    }
}