﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Structure.Search.TextAnalysis;

namespace NetCms.Structure.WebFS
{
    public class DocContentAnnotate
    {
        public DocContentAnnotate()
        {
            Annotates = new HashSet<DocAnnotate>();
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual int RifContent
        {
            get;
            set;
        }
        public virtual ICollection<DocAnnotate> Annotates
        {
            get;
            set;
        }
    }
}
