﻿using FluentNHibernate.MappingModel.Collections;
using G2Core.Common;
using GenericDAO.DAO;
using NetCms.DBSessionProvider;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetService.Utility.Controls;
using NetService.Utility.RecordsFinder;
using Newtonsoft.Json;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Structure.WebFS.Documents.Tags.BusinessLogic
{
    public static class TagsBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static CmsTag FindTagByLabel(string label, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            CmsTag tag = null;

            CriteriaNhibernateDAO<CmsTag> daoTag = new CriteriaNhibernateDAO<CmsTag>(innerSession);

            SearchParameter spLabel = new SearchParameter("", CmsTag.Column.Label.ToString(), label, Finder.ComparisonCriteria.Equals, false);

            tag = daoTag.FindByCriteria(new SearchParameter[] { spLabel }).FirstOrDefault();

            return tag;
        }

        public static CmsTag FindTagByKey(string key, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            CmsTag tag = null;

            CriteriaNhibernateDAO<CmsTag> daoTag = new CriteriaNhibernateDAO<CmsTag>(innerSession);

            SearchParameter spKey = new SearchParameter("", CmsTag.Column.Key.ToString(), key, Finder.ComparisonCriteria.Equals, false);

            tag = daoTag.FindByCriteria(new SearchParameter[] { spKey }).FirstOrDefault();

            return tag;
        }

        public static void AddTagForDoc(string tags, int idDoc)
        {
            // deserialize json tags array 
            tags = DeserealizeTags(tags);

            char[] separator = { ',' };
            string[] tagsArray = tags.Replace(" ", "-").Split(separator, StringSplitOptions.RemoveEmptyEntries);

            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    CriteriaNhibernateDAO<DocTag> dao = new CriteriaNhibernateDAO<DocTag>(session);
                    CriteriaNhibernateDAO<CmsTag> daoTag = new CriteriaNhibernateDAO<CmsTag>(session);

                    Document currentDoc = FileSystemBusinessLogic.DocumentBusinessLogic.GetById(idDoc, session);
                    string[] currentTag = currentDoc.Tags.Split(separator, StringSplitOptions.RemoveEmptyEntries);

                    // tag da rimuovere 
                    IEnumerable<string> tagDaAggiungere = tagsArray.Except(currentTag);

                    // tag comuni
                    IEnumerable<string> tagComuni = tagsArray.Intersect(currentTag);

                    // tag da rimuovere
                    IEnumerable<string> tagsDaRimuovere = currentTag.Except(tagComuni);

                    DocTag doctag = null;
                    CmsTag tag = null;

                    foreach (string strTag in tagDaAggiungere)
                    {
                        string labelTag = strTag.Trim().Replace("-", " ");

                        tag = FindTagByLabel(labelTag, session);

                        if (tag == null)
                        {
                            tag = new CmsTag();
                            tag.Label = labelTag;
                            tag.Key = FilterTag(labelTag);
                            daoTag.SaveOrUpdate(tag);
                        }

                        doctag = new DocTag();
                        doctag.RifDoc = currentDoc;
                        doctag.RifTag = tag;

                        tag.DocTags.Add(doctag);
                        currentDoc.DocTags.Add(doctag);

                        dao.SaveOrUpdate(doctag);
                    }

                    foreach (string strTag in tagsDaRimuovere)
                    {
                        string labelTag = strTag.Trim().Replace("-", " ");
                        tag = FindTagByLabel(labelTag, session);

                        if (tag != null)
                        {
                            DocTag tagDaRimuovere = tag.DocTags.Single(x => x.RifTag.Label == labelTag);
                            tag.DocTags.Remove(tagDaRimuovere);
                            currentDoc.DocTags.Remove(tagDaRimuovere);
                            dao.Delete(tagDaRimuovere);
                        }

                    }

                    tx.Commit();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message);
                    tx.Rollback();
                }
            }
        }

        public static void AddTagForDoc(string tags, int idDoc, ISession session)
        {
            char[] separator = { ',' };
            string[] tagsArray = tags.Replace(" ", "").Split(separator, StringSplitOptions.RemoveEmptyEntries);

            try
            {
                CriteriaNhibernateDAO<DocTag> dao = new CriteriaNhibernateDAO<DocTag>(session);
                CriteriaNhibernateDAO<CmsTag> daoTag = new CriteriaNhibernateDAO<CmsTag>(session);

                Document currentDoc = FileSystemBusinessLogic.DocumentBusinessLogic.GetById(idDoc, session);
                string[] currentTag = currentDoc.Tags.Replace(" ", "").Split(separator, StringSplitOptions.RemoveEmptyEntries);

                // tag da rimuovere 
                IEnumerable<string> tagDaAggiungere = tagsArray.Except(currentTag);

                // tag comuni
                IEnumerable<string> tagComuni = tagsArray.Intersect(currentTag);

                // tag da rimuovere
                IEnumerable<string> tagsDaRimuovere = currentTag.Except(tagComuni);

                DocTag doctag = null;
                CmsTag tag = null;

                foreach (string strTag in tagDaAggiungere)
                {
                    string labelTag = strTag.Trim();

                    tag = FindTagByLabel(labelTag, session);

                    if (tag == null)
                    {
                        tag = new CmsTag();
                        tag.Label = labelTag;
                        tag.Key = FilterTag(labelTag);
                        daoTag.SaveOrUpdate(tag);
                    }

                    doctag = new DocTag();
                    doctag.RifDoc = currentDoc;
                    doctag.RifTag = tag;

                    tag.DocTags.Add(doctag);
                    currentDoc.DocTags.Add(doctag);

                    dao.SaveOrUpdate(doctag);
                }

                foreach (string strTag in tagsDaRimuovere)
                {
                    string labelTag = strTag.Trim();
                    tag = FindTagByLabel(labelTag, session);

                    if (tag != null)
                    {
                        DocTag tagDaRimuovere = tag.DocTags.Single(x => x.RifTag.Label == labelTag);
                        tag.DocTags.Remove(tagDaRimuovere);
                        currentDoc.DocTags.Remove(tagDaRimuovere);
                        dao.Delete(tagDaRimuovere);
                    }

                }
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message);

            }

        }


        public static List<string> GetTagsByDocs(int idDoc, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            CriteriaNhibernateDAO<DocTag> dao = new CriteriaNhibernateDAO<DocTag>(innerSession);

            Document currentDoc = FileSystemBusinessLogic.DocumentBusinessLogic.GetById(idDoc, innerSession);

            SearchParameter spDocument = new SearchParameter("", DocTag.Column.RifDoc.ToString(), currentDoc, Finder.ComparisonCriteria.Equals, false);

            ICollection<DocTag> result = dao.FindByCriteria(new SearchParameter[] { spDocument });

            List<string> tags = result.Select(x => x.RifTag.Label).ToList<string>();

            return tags;
        }

        public static List<string> GetAllTags(int limit, int networkID, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            #region MyRegion
            //CriteriaNhibernateDAO<DocTag> dao = new CriteriaNhibernateDAO<DocTag>(innerSession);

            //SearchParameter networkSP = new SearchParameter(null, "Network.ID", networkID, Finder.ComparisonCriteria.Equals, false);
            //SearchParameter folderSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { networkSP });
            //SearchParameter documentSP = new SearchParameter("", DocTag.Column.RifDoc.ToString(), SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { folderSP });

            //SearchParameter spTag = new SearchParameter(null, DocTag.Column.RifTag.ToString(), Finder.GroupingCriteria.Group, false);
            //SearchParameter tagsJoin = new SearchParameter("", DocTag.Column.RifTag.ToString(), SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { spTag });

            //ICollection<DocTag> result = dao.FindByCriteria(0,limit, new SearchParameter[] { documentSP, spTag });

            //List<string> tags = result.Select(x => x.RifTag.Label).ToList<string>(); 
            #endregion

            ICriteria criteria = innerSession.CreateCriteria<DocTag>();
            criteria.SetMaxResults(limit);

            var join = criteria.CreateCriteria(DocTag.Column.RifDoc.ToString());
            var subJoing = join.CreateCriteria("Folder");
            subJoing.Add(Restrictions.Eq("Network.ID", networkID));

            var joinTags = criteria.CreateCriteria("RifTag", "tag");
            joinTags.SetProjection(
                Projections.Distinct(
                    Projections.ProjectionList()
                    .Add(Projections.Alias(Projections.Property("tag.Label"), "tag.Label")))
                ).AddOrder(Order.Asc("tag.Label"));

            TextInfo tInfo = CultureInfo.CurrentCulture.TextInfo;//new CultureInfo("it-IT", false).TextInfo;

            List<string> tags = criteria.List<string>()
                                .ToList()
                                .ConvertAll(x => x.Trim())
                                .ConvertAll(x => x.ToLower())
                                .ConvertAll(x => tInfo.ToTitleCase(x));
            return tags;

            // TODO ripulire il codice e aggiungere l'ordinamento (applicare il camel case alle label dei tags)
        }

        public static string GetAllTags(ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            CriteriaNhibernateDAO<CmsTag> dao = new CriteriaNhibernateDAO<CmsTag>(innerSession);
            return String.Join(" ,", dao.FindAll().Select(x => string.Format("'{0}'", RemoveSpecialCharacters(x.Label).Replace("-", " "))));
        }

        public static string FilterTag(string tagToFilter)
        {
            return tagToFilter.Trim().Replace(" ", "-").ToLower();
        }

        public static string RemoveSpecialCharacters(this string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_') //|| c == ' '
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static WebControl GetDocs(RequestVariable tag, RequestVariable currentpage, bool isPostBack)
        {
            WebControl control = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
            control.CssClass = "results";
            control.ID = "argumentresults";

            int rpp = 15;
            string paginationKey = "pag";
            int currentPage = 1;

            if (currentpage.IsValidInteger)
                currentPage = currentpage.IntValue;

            int docsCount = GetCurrentSession().CreateCriteria<Document>()
                            .Add(Restrictions.Not(Restrictions.Eq("Stato", 2)))
                            .Add(Restrictions.Eq("Trash", 0))
                            .CreateCriteria("DocTags")
                            .CreateCriteria("RifTag")
                            .Add(Restrictions.Eq("Label", tag.OriginalValue))
                            .SetProjection(Projections.RowCount()).UniqueResult<int>();


            PaginationHandler pagination = new PaginationHandler(rpp, docsCount, paginationKey, isPostBack);
            pagination.GoToFirstPageOnSearch = true;
            pagination.BaseLink = "argomenti.aspx?" + pagination.PaginationKey + "={0}";
            if (tag.IsValidString)
                pagination.BaseLink += "&tag=" + tag.OriginalValue;
            pagination.CssClass = "pagination";



            IDictionary<string, bool> orders = new Dictionary<string, bool>();
            orders.Add("Modify", true);

            var docs = GetCurrentSession().CreateCriteria<Document>()
                            .Add(Restrictions.Not(Restrictions.Eq("Stato", 2)))
                            .Add(Restrictions.Eq("Trash", 0))
                            .CreateCriteria("DocTags")
                            .CreateCriteria("RifTag")
                            .Add(Restrictions.Eq("Label", tag.OriginalValue))
                            .SetFirstResult((pagination.CurrentPage - 1) * pagination.PageSize).SetMaxResults(pagination.PageSize).List<Document>();

            string strResults = string.Empty;

            if (docs != null && docs.Count() > 0)
            {

                foreach (Document doc in docs)
                {
                    string docTitle = "";
                    if (doc.DocLangs != null && doc.DocLangs.FirstOrDefault() != null)
                    {
                        DocumentLang docLang = doc.DocLangs.FirstOrDefault();
                        if (docLang.Label.ToLower() == "Pagina Indice".ToLower())
                        {
                            StructureFolder currentFolder = FolderBusinessLogic.GetById(GetCurrentSession(), doc.Folder.ID);
                            if (doc.Folder != null)
                                docTitle = currentFolder.Label;
                        }
                        else
                        {
                            docTitle = docLang.Label;
                        }
                    }

                    string desc = (doc.Descrizione == "Pagina Indice della cartella") ? "" : "<p class=\"desc\">" + doc.Descrizione + "</p>";

                    strResults += @"
                                    <div class=""item"">
                                        <h3><a href=""" + doc.FullName + @""">" + docTitle + @"</a></h3>
                                        <div class=""itemdata"">
                                            " + desc + @"
                                            <p class=""ultimamodifica"">" + doc.Modify.ToLongDateString() + @"</p>                                            
                                        </div>
                                    </div>
                                ";
                    //< p class=""url""><a href = """+doc.FullName+@""">" + doc.FullName + @"</a></p>
                }

            }

            control.Controls.Add(new LiteralControl(strResults));
            control.Controls.Add(pagination);

            return control;
        }

        public static int GetItemsCount(string tag)
        {
            int docsCount = GetCurrentSession().CreateCriteria<Document>()
                            .Add(Restrictions.Not(Restrictions.Eq("Stato", 2)))
                            .Add(Restrictions.Eq("Trash", 0))
                            .CreateCriteria("DocTags")
                            .CreateCriteria("RifTag")
                            .Add(Restrictions.Eq(CmsTag.Column.Key.ToString(), tag))
                            .SetProjection(Projections.RowCount()).UniqueResult<int>();

            return docsCount;
        }

        public static Object GetItems(string tag, int currentpage, int pagesize, bool isPostBack)
        {

            IDictionary<string, bool> orders = new Dictionary<string, bool>();
            orders.Add("Modify", true);

            var docs = GetCurrentSession().CreateCriteria<Document>()
                            .Add(Restrictions.Not(Restrictions.Eq("Stato", 2)))
                            .Add(Restrictions.Eq("Trash", 0))
                            .Add(Restrictions.Not(Restrictions.Eq("Application", 11)))
                            .CreateCriteria("DocTags")
                            .CreateCriteria("RifTag")
                            .Add(Restrictions.Eq(CmsTag.Column.Key.ToString(), tag))
                            .SetFirstResult((currentpage - 1) * pagesize).SetMaxResults(pagesize).List<Document>();

            string strResults = string.Empty;


            IList<Object> items = new List<Object>();

            if (docs != null && docs.Count() > 0)
            {
                foreach (Document doc in docs)
                {
                    string docTitle = "";
                    if (doc.DocLangs != null && doc.DocLangs.FirstOrDefault() != null)
                    {
                        DocumentLang docLang = doc.DocLangs.FirstOrDefault();
                        if (docLang.Label.ToLower() == "Pagina Indice".ToLower())
                        {
                            StructureFolder currentFolder = FolderBusinessLogic.GetById(GetCurrentSession(), doc.Folder.ID);
                            if (doc.Folder != null)
                                docTitle = currentFolder.Label;
                        }
                        else
                        {
                            docTitle = docLang.Label;
                        }
                    }

                    string desc = (doc.Descrizione == "Pagina Indice della cartella") ? "" : "<p class=\"desc\">" + doc.Descrizione + "</p>";

                    var item = new
                    {
                        id = doc.ID,
                        titolo = docTitle,
                        descrizione = desc,
                        url = doc.FrontendLink,
                        data = doc.Modify,
                        folderlabel = doc.Folder.Nome.Replace("-", " "),
                        folderurl = doc.Folder.Path + "/",
                    };
                    items.Add(item);
                }

            }

            return items;
        }


        private static string DeserealizeTags(string tags)
        {
            List<Tagify> json = JsonConvert.DeserializeObject<List<Tagify>>(tags);

            return string.Join(",", json.Select(x => x.Value));
        }
    }
    public class Tagify
    {
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
