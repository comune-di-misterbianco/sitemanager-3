﻿using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.WebFS.Documents.Tags.BusinessLogic
{
    public class TagsManager
    {
        public TagsManager(ISession session)
        {
            currentSession = session;
        }

        private ISession currentSession;

        public CmsTag FindTagByLabel(string label, ISession innerSession = null)
        {
            CmsTag tag = null;

            CriteriaNhibernateDAO<CmsTag> daoTag = new CriteriaNhibernateDAO<CmsTag>(innerSession);

            SearchParameter spLabel = new SearchParameter("", CmsTag.Column.Label.ToString(), label, Finder.ComparisonCriteria.Equals, false);

            tag = daoTag.FindByCriteria(new SearchParameter[] { spLabel }).FirstOrDefault();

            return tag;
        }

        public string FilterTag(string tagToFilter)
        {
            return tagToFilter.Trim().Replace(" ", "-").ToLower();
        }

        public void AddTagForDoc(string[] tagsArray, int idDoc)
        {
            char[] separator = { ',' };

            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {

                try
                {
                    CriteriaNhibernateDAO<DocTag> dao = new CriteriaNhibernateDAO<DocTag>(session);
                    CriteriaNhibernateDAO<CmsTag> daoTag = new CriteriaNhibernateDAO<CmsTag>(session);

                    Document currentDoc = FileSystemBusinessLogic.DocumentBusinessLogic.GetById(idDoc, session);
                   // string[] currentTag = currentDoc.Tags.Replace(" ", "").Split(separator, StringSplitOptions.RemoveEmptyEntries);
                    string[] currentTag = currentDoc.Tags.Split(separator, StringSplitOptions.RemoveEmptyEntries);

                    // tag da rimuovere 
                    IEnumerable<string> tagDaAggiungere = tagsArray.Except(currentTag);

                    // tag comuni
                    IEnumerable<string> tagComuni = tagsArray.Intersect(currentTag);

                    // tag da rimuovere
                    IEnumerable<string> tagsDaRimuovere = currentTag.Except(tagComuni);

                    DocTag doctag = null;
                    CmsTag tag = null;

                    foreach (string strTag in tagDaAggiungere)
                    {
                        string labelTag = strTag.Trim();

                        tag = FindTagByLabel(labelTag, session);

                        if (tag == null)
                        {
                            tag = new CmsTag();
                            tag.Label = labelTag;
                            tag.Key = FilterTag(labelTag);
                            daoTag.SaveOrUpdate(tag);
                        }

                        doctag = new DocTag();
                        doctag.RifDoc = currentDoc;
                        doctag.RifTag = tag;

                        tag.DocTags.Add(doctag);
                        currentDoc.DocTags.Add(doctag);

                        dao.SaveOrUpdate(doctag);
                    }

                    foreach (string strTag in tagsDaRimuovere)
                    {
                        string labelTag = strTag.Trim();
                        tag = FindTagByLabel(labelTag, session);

                        if (tag != null)
                        {
                            DocTag tagDaRimuovere = tag.DocTags.Single(x => x.RifTag.Label == labelTag);
                            tag.DocTags.Remove(tagDaRimuovere);
                            currentDoc.DocTags.Remove(tagDaRimuovere);
                            dao.Delete(tagDaRimuovere);
                        }

                    }
                    tx.Commit();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message);
                    tx.Rollback();
                }
            }
        }
    }
}
