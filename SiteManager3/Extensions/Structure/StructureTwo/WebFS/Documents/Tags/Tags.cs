﻿using System;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using NetCms.Structure.Grants;
using NetCms.Structure.Applications;
using NetForms;

namespace NetCms.Structure.WebFS
{
    public class Tags
    {
        public static void AddTagsForDoc(string tags,string docID, NetCms.Connections.Connection conn)
        {
            string sqlDeleteExisting = "DELETE FROM docs_tags WHERE Doc_DocTag = " + docID;
            string sqlAddTag = "INSERT INTO tags (Label_Tag,Key_Tag) VALUES ('{0}','{1}')";
            string sqlAddAssociation = "INSERT INTO docs_tags (Tag_DocTag,Doc_DocTag) VALUES";

            DataTable tagsTable = conn.SqlQuery("SELECT * FROM tags");

            char[] separator = { ',' };
            string[] tagsArray = tags.Split(separator, StringSplitOptions.RemoveEmptyEntries);

            bool atLeastOneAdded = false;
            foreach (string tag in tagsArray)
            {
                int tagID = 0;
                DataRow[] rows = tagsTable.Select("Key_Tag = '" + FilterTag(tag) + "'");
                if (rows.Length == 0)
                    tagID = conn.ExecuteInsert(string.Format(sqlAddTag, tag, FilterTag(tag)));
                else
                    tagID = (int)rows[0]["id_Tag"];

                sqlAddAssociation += (!atLeastOneAdded ? "" : ",") + " (" + tagID + "," + docID + ")";
                atLeastOneAdded = true;
            }

            conn.ExecuteInsert(sqlDeleteExisting);
            if(atLeastOneAdded)
                conn.ExecuteInsert(sqlAddAssociation);

            //Trace
        }

        public static string FilterTag(string tagToFilter)
        {
            return tagToFilter.Trim().Replace(" ", "-").ToLower();
        }
    }
}
