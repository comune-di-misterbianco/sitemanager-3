﻿using NetCms.Structure.WebFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.WebFS.Documents.Tags
{
    public class CmsTag
    {
        public CmsTag()
        {
            DocTags = new HashSet<DocTag>();
        }

        public virtual int Id
        {
            get;
            set;
        }

        public virtual string Label
        {
            get;
            set;
        }

        public virtual string Key
        {
            get;
            set;
        }

        public virtual ICollection<DocTag> DocTags
        {
            get;
            set;
        }

        public enum Column
        {
            Id,
            Label,
            Key,
            DocTags,
        }
    }
}
