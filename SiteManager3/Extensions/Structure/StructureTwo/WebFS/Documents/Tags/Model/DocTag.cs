﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.WebFS.Documents.Tags
{
    public class DocTag
    {
        public DocTag()
        {

        }

        public virtual int Id
        {
            get;
            set;
        }

        public virtual Document RifDoc
        {
            get;
            set;
        }

        public virtual CmsTag RifTag
        {
            get;
            set;
        }

        public enum Column
        {
            Id,
            RifDoc,
            RifTag,
        }
    }
}
