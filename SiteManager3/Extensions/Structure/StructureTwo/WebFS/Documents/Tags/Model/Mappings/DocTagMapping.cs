﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.WebFS.Documents.Tags.Mapping
{
    public class DocTagMapping: ClassMap<DocTag>
    {
        public DocTagMapping()
        {
            Table("docs_tags");
            Id(x => x.Id).Column("id_DocTag");

            References(x => x.RifDoc)
                .Class(typeof(Document))
                .Column("Doc_DocTag")
                .Fetch.Select()
                .Not.Nullable();

            References(x => x.RifTag)
                .Class(typeof(CmsTag))
                .Column("Tag_DocTag")
                .Fetch.Join()
                .Not.Nullable();

        }
    }
}
