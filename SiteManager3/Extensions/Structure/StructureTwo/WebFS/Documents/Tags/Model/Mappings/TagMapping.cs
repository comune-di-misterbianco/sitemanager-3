﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.WebFS.Documents.Tags.Mapping
{
    public class TagMapping : ClassMap<CmsTag>
    {
        public TagMapping()
        {
            Table("tags");
            Id(x=>x.Id).Column("id_Tag");
            Map(x => x.Label).Column("Label_Tag");
            Map(x => x.Key).Column("Key_Tag");

            HasMany(x => x.DocTags)
              .KeyColumn("Tag_DocTag")
              .Fetch.Select().AsSet().Inverse()
              .Cascade.All();
        }
    }
}
