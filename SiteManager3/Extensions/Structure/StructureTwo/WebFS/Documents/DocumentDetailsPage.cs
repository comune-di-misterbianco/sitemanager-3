using System;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.Applications;


/// <summary>
/// Summary description for NewsFolder_NEW_
/// </summary>
namespace NetCms.Structure.WebFS
{
    public abstract class DocumentDetailsPage : NetCms.GUI.NetworkPage
    {
        protected abstract ToolbarButton ReserveForNewsletter { get; }

        protected abstract ToolbarButton ModifyButton { get; }

        public static WebControl FabricatePage()
        {
            G2Core.XhtmlControls.InputField deleteDocControl = new G2Core.XhtmlControls.InputField("DeleteDocument");
            if (deleteDocControl.RequestVariable.IsPostBack && 
                deleteDocControl.RequestVariable.IsValidInteger &&
                NetCms.Networks.NetworksManager.CurrentActiveNetwork != null &&
                NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder != null &&
                NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.CurrentDocument != null
                )
            {
                var folder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder;
                Document doc = folder.CurrentDocument as Document;
                doc.Delete();

                if (CmsConfigs.Model.GlobalConfig.IndexConfig != null && CmsConfigs.Model.GlobalConfig.IndexConfig.SolrEnabled)
                {
                    CmsConfigs.BusinessLogic.ContentIndex.IndexContentSolr.DeleteDocument(doc.ID);
                }

                //ATTENZIONE: NON DOVREBBE SERVIRE
                //folder.Documents.Remove(deleteDocControl.RequestVariable.StringValue);
                //SISTEMARE CON CACHE 2░ LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
                (folder as StructureFolder).Invalidate();
            }

            if (Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork)
            {
                //Application application = ApplicationsPool.ActiveAssemblies[((Document)Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.CurrentDocument).Application.ToString()];

                Document doc = (Document)Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.CurrentDocument;

                WebControl control = (WebControl)ApplicationsPool.BuildInstanceOf(doc.RelativeApplication.Classes.Details, doc.RelativeApplication.ID.ToString());
                
                control.Controls.Add(deleteDocControl.Control);
                return control;
            }
            else return new NetCms.GUI.SmPageError();
            //Trace
        }

        public DocumentDetailsPage() 
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("docs_meta.aspx?doc=" + this.CurrentDocument.ID + "&folder=" + this.CurrentFolder.ID, NetCms.GUI.Icons.Icons.Page_White_Code, "Imposta Meta tag"));

            if (this.CurrentDocument.AllowDelete)
                this.Toolbar.Buttons.Add(new ToolbarButton("javascript: DeleteDoc('" + this.CurrentDocument.ID + "','DeleteDocument','" + this.CurrentDocument.PhysicalName + "')", NetCms.GUI.Icons.Icons.Page_White_Delete, "Elimina " + this.CurrentFolder.RelativeApplication.LabelSingolare));

            //if (this.CurrentDocument.Criteria["modify"])
                //this.Toolbar.Buttons.Add(new ToolbarButton(this.CurrentFolder.Network.Paths.AbsoluteAdminRoot + "/cms/docs_move.aspx?doc=" + CurrentDocument.ID + "&folder=" + this.CurrentFolder.ID, NetCms.GUI.Icons.Icons.Page_Go, "Sposta " + this.CurrentFolder.RelativeApplication.LabelSingolare));
            if (this.CurrentDocument.Criteria[NetCms.Grants.CriteriaTypes.Grants].Allowed && PageData.Account.IsAdministrator)
                this.Toolbar.Buttons.Add(new ToolbarButton(this.CurrentFolder.StructureNetwork.Paths.AbsoluteAdminRoot + "/cms/grants/grants.aspx?folder=" + this.CurrentFolder.ID + "&doc=" + CurrentDocument.ID, NetCms.GUI.Icons.Icons.Key, "Imposta Permessi " + this.CurrentFolder.RelativeApplication.LabelSingolare));

            //this.Toolbar.Buttons.Add(new ToolbarButton(this.CurrentFolder.Network.Paths.AbsoluteAdminRoot + "/crm/crm_doc.aspx?doc=" + CurrentDocument.ID, NetCms.GUI.Icons.Icons.World, "Leggi traccie CRM"));
            if (this.CurrentDocument.AllowModify)
                this.Toolbar.Buttons.Add(new ToolbarButton("docs_mod.aspx?doc=" + CurrentDocument.ID + "&folder=" + this.CurrentFolder.ID, NetCms.GUI.Icons.Icons.Page_Edit, "ProprietÓ Documento"));
            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta visualizzando i dettagli del documento '" + CurrentDocument.Nome + "' (id=" + CurrentDocument.ID + ").", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
        }
    }
}
