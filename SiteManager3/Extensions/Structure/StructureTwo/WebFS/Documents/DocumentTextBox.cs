﻿using System;
using System.Web;
using System.Data;
using NetCms.Structure.Grants;
using NetForms;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;

namespace NetCms.Structure.WebFS
{
    public partial class Document
    {
        private class DocNameTextBox : NetTextBoxPlus
        {
            private PageData PageData;
            public DocNameTextBox(string label, string fieldname, ContentTypes type, PageData Pagedata)
                : base(label, fieldname, type)
            {
                PageData = Pagedata;
            }
            public override string validateInput(string input)
            {
                string errors = base.validateInput(input);

                string name = input.Replace(".aspx", "");
                name = base.validateValue(name);
                //name = name.Replace("'", "''");               

                if (DocumentBusinessLogic.GetDocumentByPhysicalNameAndFolder(name,PageData.CurrentFolder) != null)
                {
                    errors += "<li>";
                    errors += "Esiste già un documento con questo nome in questa cartella";
                    errors += "</li>"; ;
                }

                ////SISTEMARE CON DAO UNA VOLTA IMPLEMENTATO L'OR
                //string sql = "SELECT * FROM Docs INNER JOIN network_docs ON Docs.id_Doc = network_docs.id_NetworkDoc";
                //sql += " WHERE Folder_Doc = " + PageData.CurrentFolder.ID + "";
                //sql += " AND (Name_Doc LIKE '" + name + "' OR Name_Doc LIKE '" + name + ".aspx')";
                ////sql += " OR Name_Doc LIKE '" + fsname + "' OR Name_Doc LIKE '" + fsname + ".aspx'";

                //DataTable reader = PageData.Conn.SqlQuery(sql);
                //if (reader.Rows.Count > 0)
                //{
                //    errors += "<li>";
                //    errors += "Il esiste già un documento con questo nome in questa cartella";
                //    errors += "</li>"; ;
                //}

                return errors;
            }
        }
    }
}
