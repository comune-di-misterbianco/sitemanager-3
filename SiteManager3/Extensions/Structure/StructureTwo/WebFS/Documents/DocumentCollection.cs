﻿//using System;
//using System.Web;
//using System.Web.Caching;
//using System.Collections.Generic;
//using System.Data;
//using System.Text;
//using NetCms.Connections;

//namespace NetCms.Structure.WebFS
//{
//    public class DocumentsCollection : NetCms.Networks.WebFS.NetworkDocumentsCollection
//    {
//        protected override G2Core.Caching.CacheableItem BuildElement(DataRow recordData, int index, G2Core.Caching.CacheableCollection ownerCollection)
//        {
//            return new CacheableItem(NetworkKey, recordData, index, ownerCollection);
//        }

//        private Networks.NetworkKey _NetworkKey;
//        public Networks.NetworkKey NetworkKey
//        {
//            get
//            {
//                return _NetworkKey;
//            }
//            private set { _NetworkKey = value; }
//        }

//        public enum DocumentsOrdersDirections { Default, ASC, DESC }

//        private string StartingQuery;

//        public DocumentsCollection(Networks.NetworkKey networkKey, G2Core.Caching.Visibility.CachingVisibility Visibility, NetCms.Connections.Connection conn) : base(Visibility, conn) { this.NetworkKey = networkKey; }

//        public void FillWithDocsFromFolder(string folderID, bool AddTrashedDocuments)
//        {
//            FillWithDocsFromFolder(folderID, AddTrashedDocuments, StructureFolder.ViewOrders.Undefined, DocumentsOrdersDirections.Default);
//        }
//        public void FillWithDocsFromFolder(string folderID, bool AddTrashedDocuments, StructureFolder.ViewOrders orderby)
//        {
//            FillWithDocsFromFolder(folderID, AddTrashedDocuments, orderby, DocumentsOrdersDirections.Default);
//        }
//        public void FillWithDocsFromFolder(string folderID, bool AddTrashedDocuments, StructureFolder.ViewOrders orderby, DocumentsOrdersDirections direction)
//        {
//            string sql = "SELECT * FROM Docs INNER JOIN docs_lang ON id_Doc = Doc_DocLang WHERE Lang_DocLang = " + Networks.NetworksManager.GetNetwork(NetworkKey).DefaultLang + " AND Folder_Doc = " + folderID;
//            if (!AddTrashedDocuments) sql += " AND Trash_Doc = 0";

//            this.StartingQuery = sql;

//            FillWithDocs(sql, orderby, direction);
//        }

//        public void FillWithDocsFromFolderTree(string folderID)
//        {
//            string sql = "SELECT * FROM Folders";
//            sql += " INNER JOIN Docs ON id_Folder = Folder_Doc";
//            sql += " INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang";
//            sql += " WHERE Trash_Doc = 0 AND Tree_Folder LIKE '%." + folderID + ".%'";

//            this.StartingQuery = sql; FillWithDocs(sql, StructureFolder.ViewOrders.ModifyDate, DocumentsOrdersDirections.DESC);
//        }

//        private void FillWithDocs(string sql, StructureFolder.ViewOrders orderby, DocumentsOrdersDirections direction)
//        {
//            switch (orderby)
//            {
//                case StructureFolder.ViewOrders.ID: sql += " ORDER BY id_Doc " + (direction == DocumentsOrdersDirections.Default ? "DESC" : direction.ToString()); break;
//                case StructureFolder.ViewOrders.Label: sql += " ORDER BY Label_DocLang " + (direction == DocumentsOrdersDirections.Default ? "ASC" : direction.ToString()); break;
//                case StructureFolder.ViewOrders.Name: sql += string.Format(" ORDER BY Name_Doc {0} , id_Doc {0}", (direction == DocumentsOrdersDirections.Default ? "ASC" : direction.ToString())); break; break;
//                case StructureFolder.ViewOrders.CreationDate: string.Format(" ORDER BY Date_Doc {0} , id_Doc {0}", (direction == DocumentsOrdersDirections.Default ? "DESC" : direction.ToString())); break;
//                case StructureFolder.ViewOrders.ModifyDate: sql += string.Format(" ORDER BY Modify_Doc {0} , id_Doc {0}", (direction == DocumentsOrdersDirections.Default ? "DESC" : direction.ToString())); break;
//                case StructureFolder.ViewOrders.Undefined: sql += string.Format(" ORDER BY Name_Doc {0} , id_Doc {0}", (direction == DocumentsOrdersDirections.Default ? "ASC" : direction.ToString())); break;
//            }

//            base.FillCollection(sql, CommandType.Text, null);
//        }

//        public DocumentsCollection OrderedByID()
//        {
//            DocumentsCollection docs = new DocumentsCollection(this.NetworkKey, this.Visibility, this.Connection);
//            docs.FillWithDocs(this.StartingQuery, StructureFolder.ViewOrders.ID, DocumentsOrdersDirections.Default);
//            return docs;
//        }
//        public DocumentsCollection OrderedByModify()
//        {
//            DocumentsCollection docs = new DocumentsCollection(this.NetworkKey, this.Visibility, this.Connection);
//            docs.FillWithDocs(this.StartingQuery, StructureFolder.ViewOrders.ModifyDate, DocumentsOrdersDirections.Default);
//            return docs;
//        }
//        public DocumentsCollection OrderedByName()
//        {
//            DocumentsCollection docs = new DocumentsCollection(this.NetworkKey, this.Visibility, this.Connection);
//            docs.FillWithDocs(this.StartingQuery, StructureFolder.ViewOrders.Name, DocumentsOrdersDirections.Default);
//            return docs;
//        }
//        public DocumentsCollection OrderedByDate()
//        {
//            DocumentsCollection docs = new DocumentsCollection(this.NetworkKey, this.Visibility, this.Connection);
//            docs.FillWithDocs(this.StartingQuery, StructureFolder.ViewOrders.CreationDate, DocumentsOrdersDirections.Default);
//            return docs;
//        }

//        public new Document this[int i]
//        {
//            get
//            {
//                return (Document)base[i];
//            }
//        }

//        public new Document this[string key]
//        {
//            get
//            {
//                return (Document)base[key];
//            }
//        }

//        public class CacheableItem : G2Core.Caching.CacheableItem
//        {
//            private Networks.NetworkKey _NetworkKey;
//            public Networks.NetworkKey NetworkKey
//            {
//                get
//                {
//                    return _NetworkKey;
//                }
//                private set { _NetworkKey = value; }
//            }

//            public CacheableItem(Networks.NetworkKey networkKey, DataRow recordData, int index, G2Core.Caching.CacheableCollection ownerCollection)
//                : base(recordData, index, ownerCollection)
//            {
//                this.NetworkKey = networkKey;
//                this.Key += networkKey.BaseKey;
//            }
//            protected override G2Core.Caching.ICacheableObject BuildItemProcedure()
//            {
//                return Document.Fabricate(this.CurrentRecordData, NetworkKey);
//            }
//        }
//    }
//}
