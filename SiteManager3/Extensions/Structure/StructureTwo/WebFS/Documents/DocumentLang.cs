﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Structure.WebFS.Reviews;

namespace NetCms.Structure.WebFS
{
    public class DocumentLang
    {
        public DocumentLang()
        {
            //Content = null;
            Revisioni = new HashSet<Review>();
        }

        public virtual int ID
        {
            get;
            set;
        }


        public virtual string Descrizione
        {
            get;
            set;
        }


        public virtual string Label
        {
            get;
            set;
        }


        public virtual int Lang
        {
            get;
            set;
        }

        public virtual DocContent Content
        {
            get;
            set;
        }

        public virtual Document Document
        {
            get;
            set;
        }

        public virtual ICollection<Review> Revisioni
        {
            get;
            set;
        }
    }
}
