﻿using System;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Applications;
using NetForms;
using NetCms.Networks.WebFS;
using NetCms.Networks;
using System.Collections.Generic;
using System.Linq;
using NetCms.Connections;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS.Reviews;
using NetCms.Users;
using NetCms.Grants;
using NetCms.Grants.Interfaces;
using NetCms.Networks.Grants;
using NHibernate;


namespace NetCms.Structure.WebFS
{
    public abstract partial class Document : NetworkDocument,IGrantsFindable
    {
        #region Proprietà da mappare

        public virtual int Content
        {
            get
            {
                //if (this.DocLangs.ElementAt(0).Content != null)
                //    return this.DocLangs.ElementAt(0).Content.ID;
                //else
                //    return 0;

                int content = 0;

                if (this.DocLangs != null && this.DocLangs.Any() && CurrentDocLang.Content != null)
                {
                    // find doclang by current id lang 
                    content = CurrentDocLang.Content.ID;
                }
                //Aggiungo questo ulteriore controllo per evitare una eccezione che si scatena quando ho inserito la traduzione di titolo e descrizione in una lingua diversa da quella di default, ma non ho ancora inserito il contenuto.
                //In questo caso prendo il contenuto della lingua di default.
                if (CurrentDocLang.Content == null && CurrentDocLang.Lang != ParentLangID)
                {
                    DocumentLang tempDocLang = this.DocLangs.Where(x => x.Lang == LanguageManager.BusinessLogic.LanguageBusinessLogic.GetDefaultLanguage().ID).FirstOrDefault();
                    if (tempDocLang.Content != null)
                        content = tempDocLang.Content.ID;
                }

                return content;
            }
        }

        /// <summary>
        /// Restituisce l'ID della lingua in uso
        /// </summary>
        /// <returns></returns>
        private int _LangID = 1;
        public virtual int LangID
        {
            get
            {
                if (PageData != null && PageData.CurrentConfig != null && PageData.CurrentConfig.CurrentLanguage != null)
                    _LangID = PageData.CurrentConfig.CurrentLanguage.ID;

                return _LangID;
            }
        }

        public virtual int ParentLangID
        {
            get
            {
                int parentLangID = 1;
                if (PageData != null && PageData.CurrentConfig != null && PageData.CurrentConfig.CurrentLanguage != null)
                    parentLangID = PageData.CurrentConfig.CurrentLanguage.Parent_Lang;

                return parentLangID;
            }
        }


        public virtual DocumentLang CurrentDocLang
        {
            get
            {
                DocumentLang currentDocLang = null;

                currentDocLang = this.DocLangs.Where(x => x.Lang == LangID).FirstOrDefault();
                if (currentDocLang == null)
                    currentDocLang = this.DocLangs.Where(x => x.Lang == ParentLangID).FirstOrDefault();
                return currentDocLang;
            }
        }

        public virtual StructureFolder StructureFolder
        {
            get {
                return FolderBusinessLogic.GetFolderRecordById(Folder.ID);              
            }
    }

        public virtual string TranslateName(int langId, int parentLangId)
        {
            string name = "";
            
            bool defaultLang = langId == LanguageManager.BusinessLogic.LanguageBusinessLogic.GetDefaultLanguage().ID;
            DocumentLang tempDocLang = this.DocLangs.Where(x => x.Lang == langId).FirstOrDefault();

            if (tempDocLang == null)
            {
                //defaultLang = true;
                tempDocLang = this.DocLangs.Where(x => x.Lang == parentLangId).FirstOrDefault();
            }

            name = (defaultLang ? this.PhysicalName : tempDocLang.Label.ToLower().Replace(" ", "-") + (Extension.Length > 0 ? "." + Extension : ""));

            return name;
        }

        public override string TranslatedName
        {
            get
            {
                
                return TranslateName(LangID, ParentLangID);
            }

        }


        /// <summary>
        /// Rappresenta il nome fisico del documento memorizzato sulla tabella docs, non la label!!!!
        /// </summary>
        public virtual string PhysicalName
        {
            get;
            set;
        }

        public virtual int Hide
        {
            get;
            set;
        }

        public virtual int Autore
        {
            get;
            set;
        }

        public virtual string CssClass
        {
            get;
            set;
        }

        public virtual int Application
        {
            get;
            set;
        }

        public virtual DateTime Data
        {
            get;
            set;
        }

        public virtual DateTime Create
        {
            get;
            set;
        }

        public virtual int TitleOffset
        {
            get;
            set;
        }


        public virtual DateTime Modify
        {
            get;
            set;
        }

        public virtual int Stato
        {
            get;
            set;
        }

        public virtual int Trash
        {
            get;
            set;
        }

        public virtual int Layout
        {
            get;
            set;
        }

        public virtual int Order
        {
            get;
            set;
        }

        public virtual int LangPickMode
        {
            get;
            set;
        }

        public virtual ICollection<DocumentLang> DocLangs
        {
            get;
            set;
        }

        public virtual ICollection<ReviewNotify> ReviewNotifications
        {
            get;
            set;
        }

        public virtual ICollection<Document> Allegati
        {
            get;
            set;
        }

        public virtual Document DocumentoRiferito
        {
            get;
            set;
        }

        #endregion

        //public override G2Core.Caching.ICacheableObject BuildItem(DataRow data)
        //{
        //    return Document.Fabricate(data,this.NetworkKey);
        //}

        //public static Document Fabricate(DataRow data, Networks.NetworkKey Network)
        //{
        //    string ApplicationID = data["Application_Doc"].ToString();
        //    if (ApplicationsPool.ActiveAssemblies.Contains(ApplicationID))
        //    {
        //        object[] parameters = { data, Network };
        //        Application application = ApplicationsPool.ActiveAssemblies[ApplicationID];
        //        Type ApplicationFolderType = application.Assembly.GetType(application.Classes.Document);
        //        Document folder = (Document)System.Activator.CreateInstance(ApplicationFolderType, parameters);
        //        return folder;
        //    }
        //    //Trace
        //    return null;
        //}
        
        public virtual new StructureNetwork Network
        {
            get
            {
                return new StructureNetwork(base.Network);
            }
        }

        private PageData _PageData;
        public virtual PageData PageData
        {
            get
            {
                if (_PageData == null || _PageData.GetHashCode() != ((PageData)HttpContext.Current.Items["SM_PageData"]).GetHashCode())
                    _PageData = (PageData)HttpContext.Current.Items["SM_PageData"];
                return _PageData;
            }
        }

        public virtual string ObjectHashCode
        {
            get
            {
                if (_ObjectHashCode == null)
                    _ObjectHashCode = this.GetHashCode().ToString();
                return _ObjectHashCode;
            }
        }
        private string _ObjectHashCode;

        public virtual Application RelativeApplication
        {
            get 
            {
                return ApplicationsPool.ActiveAssemblies[((Document)Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.CurrentDocument).Application.ToString()];
            }
        }

        //public new StructureFolder Folder
        //{
        //    get
        //    {
        //        return (StructureFolder)base.Folder;
        //    }
        //}
        //protected new StructureFolder _Folder;
    
        //private StructureFolder _CurrentFolder;
        //protected StructureFolder CurrentFolder
        //{
        //    get
        //    {
        //        if (_CurrentFolder == null)
        //        {
        //            if (DocumentBusinessLogic.GetCurrentSession() != null)
        //                _CurrentFolder = DocumentBusinessLogic.GetCurrentSession().GetSessionImplementation().PersistenceContext.Unproxy(this.Folder) as StructureFolder;
        //        }
        //        return _CurrentFolder;
        //    }
        //}

        public Document() : base() 
        {
            DocLangs = new HashSet<DocumentLang>();
            ReviewNotifications = new HashSet<ReviewNotify>();
            Allegati = new HashSet<Document>();
            DocTags = new HashSet<Documents.Tags.DocTag>();
            TitleOffset = -1;
            Stato = -1;
        }

        public Document(Networks.NetworkKey networkKey)
            : base(networkKey) 
        {
            //InitDocumentData(record);
            DocLangs = new HashSet<DocumentLang>();
            ReviewNotifications = new HashSet<ReviewNotify>();
            Allegati = new HashSet<Document>();
            TitleOffset = -1;
            Stato = -1;
            //NetworkKey = networkKey;
        }

        public override string QueryString4URLs
        {
            get { return "folder=" + this.Folder.ID + "&doc=" + this.ID; }
        }

        public virtual string GetFolderLabel
        {
            get
            {
                string label = "";
                if (this.Folder != null)
                    label = NetCms.Structure.WebFS.FileSystemBusinessLogic.FolderBusinessLogic.GetById(this.Folder.ID).Label;
                return label;
            }
        }

        public virtual string GetFolderPath
        {
            get
            {
                string folderPath = "";

                if (this.Folder != null)
                    folderPath = this.Folder.Path;
                return folderPath;                    
            }
        }

        //private NetworkKey _NetworkKey;
        //public NetworkKey NetworkKey
        //{
        //    get
        //    {
        //        return _NetworkKey;
        //    }
        //    private set { _NetworkKey = value; }
        //}

        //private void InitDocumentData(DataRow record)
        //{
        //    Hide_Doc = record["Hide_Doc"].ToString();
        //    int.TryParse(record["Autore_Doc"].ToString(), out _Author);
        //}

        public abstract string FrontendLink
        {
            get;
        }
        public virtual string SiteManagerLink
        {
            get
            {
                //return (this.Folder as StructureFolder).Network.Paths.AbsoluteAdminRoot + "/docs/details.aspx?folder=" + this.Folder.ID + "&amp;doc=" + this.ID;
                return this.Network.Paths.AbsoluteAdminRoot + "/docs/details.aspx?folder=" + this.Folder.ID + "&amp;doc=" + this.ID;
            }
        }
        public abstract string Icon { get; }

        protected DocContent _ContentRecord;
        public abstract DocContent ContentRecord { get; }

        //protected DataRow _ContentRecord;
        //public abstract DataRow ContentRecord { get; }

        public virtual bool AllowDelete
        {
            get { return Grant && Criteria[CriteriaTypes.Delete].Allowed && this.Application != 1; }
        }
        public virtual bool AllowProperties
        {
            get { return Grant && Criteria[CriteriaTypes.Modify].Allowed; }
        }
        public virtual bool AllowModify
        {
            get { return Grant && Criteria[CriteriaTypes.Modify].Allowed; }
        }
        
        public virtual bool EnableCssEdit
        {
            get { return true; }
        }
        public virtual bool EnableCustomTitleEdit
        {
            get { return true; }
        }
        public virtual bool EnableCustomLayoutEdit
        {
            get { return true; }
        }

        public override string Nome
        {
            get
            {
                //return this.DocLangs.ElementAt(0).Label;
                return CurrentDocLang.Label;
            }
            //set;
        }

        public virtual string Descrizione
        {
            get
            {
                //return this.DocLangs.ElementAt(0).Descrizione;
                return CurrentDocLang.Descrizione;
            }

        }

        public virtual string Date
        {
            get
            {
                return (Data.ToShortDateString().Length > 10) ? Data.ToShortDateString().Remove(10) : Data.ToShortDateString();
            }
        }

        public virtual string LastModify
        {
            get
            {
                return (Modify.ToShortDateString().Length > 10) ? Modify.ToShortDateString().Remove(10) : Modify.ToShortDateString();
            }
        }

        public virtual bool AddKeywordsToTitle
        {
            get
            {
                return TitleOffset == 1;
            }
        }

        public virtual bool AddDoc
        {
            get
            {
                return (Hide == 0);
            }
        }
        
        

        //private string _AuthorName;
        //public virtual string AuthorName
        //{
        //    get
        //    {
        //        if (_AuthorName == null)
        //        {
        //            DataTable anagrafica = PageData.Conn.SqlQuery("SELECT * FROM Users INNER JOIN Anagrafica on id_Anagrafica = Anagrafica_User");
        //            DataRow[] rows = anagrafica.Select("id_User = " + this.Autore);
        //            if (rows.Length > 0)
        //                _AuthorName = rows[0]["Cognome_Anagrafica"].ToString() + " " + rows[0]["Nome_Anagrafica"].ToString();
        //            else _AuthorName = "Sconosciuto";
        //        }
        //        return _AuthorName;
        //    }
        //}

        private string _AuthorName;
        public virtual string AuthorName
        {
            get
            {
                if (_AuthorName == null)
                {
                    User user = UsersBusinessLogic.GetById(this.Autore);
                    if (user != null)
                        _AuthorName = user.UserName;
                    else _AuthorName = "Sconosciuto";
                }
                return _AuthorName;
            }
        }
        
        //private string _ReviewAuthorName;
        //public virtual string ReviewAuthorName
        //{
        //    get
        //    {
        //        if (_ReviewAuthorName == null)
        //        {
        //            string sql = "SELECT * FROM Users INNER JOIN Anagrafica on id_Anagrafica = Anagrafica_User";
        //            sql += " INNER JOIN Revisioni ON id_User = Autore_Revisione";
        //            sql += " INNER JOIN Docs_Lang ON id_DocLang = DocLang_Revisione";

        //            DataTable tab = PageData.Conn.SqlQuery(sql);
        //            DataRow[] rows = tab.Select("Contenuto_Revisione = Content_DocLang AND Doc_DocLang = " + this.ID);
        //            if (rows.Length > 0)
        //                _ReviewAuthorName = rows[0]["Cognome_Anagrafica"].ToString() + " " + rows[0]["Nome_Anagrafica"].ToString();
        //            else _ReviewAuthorName = "Sconosciuto";
        //        }
        //        return _ReviewAuthorName;
        //    }
        //}

        private string _ReviewAuthorName;
        public virtual string ReviewAuthorName
        {
            get
            {
                if (_ReviewAuthorName == null)
                {
                    try
                    {
                        DocumentLang doclang = this.DocLangs.ElementAt(0);
                        _ReviewAuthorName = doclang.Revisioni.First(x=>x.Content == doclang.Content).Author.UserName;
                        
                    }
                    catch
                    {
                        _ReviewAuthorName = "Sconosciuto";
                    }
                }
                return _ReviewAuthorName;
            }
        }

        #region Stato del Documento

        public virtual string StatusLabel
        {
            get
            {
                var statofolder = (this.Folder as StructureFolder).Stato;
               return StatusLabels(Stato);
            }
        }
        public static string StatusLabels(int status)
        {
            string statlabel = "";
            switch (status)
            {
                case 0:
                    statlabel = "Mostra in tutti i menu";
                    break;
                case 1:
                    statlabel = "Raggiungibile";
                    break;
                case 2:
                    statlabel = "Non Raggiungibile";
                    break;
                case 3:
                    statlabel = "Nascosto";
                    break;

                //case 4:
                //    statlabel = "Mostra solo nel menu contestuale";
                //    break; 
            }
            return statlabel;
        }
        public virtual DocumentStates Status
        {
            get
            {
                DocumentStates stat = DocumentStates.NotReachable;

                switch (Stato)
                {
                    case 0:
                        stat = DocumentStates.Menu;
                        break;
                    case 1:
                        stat = DocumentStates.Reachable;
                        break;
                    case 2:
                        stat = DocumentStates.NotReachable;
                        break;
                }
                return stat;
            }
        }
        public enum DocumentStates
        {
            Menu = 0,
            Reachable = 1,
            NotReachable = 2
        }

        #endregion


        #region Criteri e Permessi
        /*
        public CriteriaCollection Criteria
        {
            get
            {
                if (_Criteria == null)
                    initCriteria();

                return _Criteria;
            }
        }
        private CriteriaCollection _Criteria;
        private void initCriteria()
        {
            NetCms.Structure.Grants.DocGrantsFinder finder = new NetCms.Structure.Grants.DocGrantsFinder();
            _Criteria = finder.Search(this);
        }
        public bool Grant
        {
            get
            {
                bool Output = false;
                if (Criteria["show"].Value != 0)
                    Output = Criteria["show"].Value == 1 ? true : false;
                return Output;
            }
        }
        */

        //protected DataTable _DocProfiles;
        //protected DataTable DocProfiles
        //{
        //    get
        //    {
        //        if (_DocProfiles == null)
        //        {
        //            if (PageData.Session["DocProfilesTable"] == null)
        //                PageData.Session["DocProfilesTable"] = _DocProfiles = PageData.Conn.SqlQuery("SELECT * FROM DocProfiles");
        //            else
        //                _DocProfiles = (DataTable)PageData.Session["DocProfilesTable"];
        //        }

        //        return _DocProfiles;
        //    }
        //}

        //public virtual int[] DocUserProfiles
        //{
        //    get
        //    {
        //        if (_DocUserProfiles == null)
        //        {


        //            //string sql = "SELECT * FROM DocProfiles";
        //            //sql += " WHERE Doc_DocProfile = " + this.ID;
        //            //DataTable tab = PageData.Conn.SqlQuery(sql);
        //            DataRow[] rows = DocProfiles.Select(" Doc_DocProfile = " + this.ID);
        //            _DocUserProfiles = new int[rows.Length];
        //            for (int i = 0; i < rows.Length; i++)
        //                _DocUserProfiles[i] = int.Parse(rows[i]["id_DocProfile"].ToString());
        //        }
        //        return _DocUserProfiles;
        //    }
        //}
        //private int[] _DocUserProfiles;

        #endregion

        #region Gruppi

        public virtual Users.Group Groups
        {
            get
            {
                if (_Root == null)
                    initGroups();
                return _Root;
            }
        }
        private Users.Group _Root;
        private void initGroups()
        {
            _Root = new RevisorsLoader(PageData, this).getRootGroup();
        }

        #endregion
             

        public override Connection Connection
        {
            get { return this.Network.Connection; }
        }

        public override WebFSObjectTypes ObjectType
        {
            get { return WebFSObjectTypes.Document; }
        }

        public override List<CmsWebFSObject> Childs
        {
            get { return null; }
        }

        public virtual string FullName
        {
            get { return Folder.Path + "/" + PhysicalName; }
        }

        private int _HasNext;
        public virtual bool HasNext
        {
            get
            {
                if (_HasNext == 0)
                {
                    _HasNext = 2;

                    bool found = false;
                    for (int i = 0; i < Folder.Documents.Count && !found; i++)
                    {
                        if (Folder.Documents.ElementAt(i) == this)
                        {
                            _HasNext = i + 1 < Folder.Documents.Count ? 1 : 2;
                            found = true;
                        }
                    }

                }
                return _HasNext == 1;
            }
        }

        private string _Extension;
        public virtual string Extension
        {
            get
            {
                if (_Extension == null)
                {
                    string[] exts = PhysicalName.ToLower().Split('.');
                    if (exts.Length > 1)
                        _Extension = exts[exts.Length - 1];
                    else
                        _Extension = "";
                }
                return _Extension;
            }
        }

        public virtual G2Core.Common.StringCollection DocLangsContents
        {
            get
            {
                if (_DocLangsContents == null)
                {
                    _DocLangsContents = new G2Core.Common.StringCollection();

                    foreach (DocumentLang docLang in DocLangs)
                        _DocLangsContents.Add(docLang.ID.ToString(), docLang.Lang.ToString());
                }
                return _DocLangsContents;
            }
        }
        private G2Core.Common.StringCollection _DocLangsContents;

        public abstract string Link
        {
            get;
        }

        public override CmsWebFSObject ParentObject
        {
            get { return this.Folder; }
        }

        public virtual void Delete()
        {
            StructureFolder folder = FolderBusinessLogic.GetById(this.Folder.ID);
            this.Trash = folder.ReferedAccount.ID;
            if (FileSystemBusinessLogic.DocumentBusinessLogic.SaveOrUpdateDocument(this) != null)
            {

                // Riadattare quando si mapperà la newsletter
                //PageData.Conn.Execute("DELETE FROM newsletter_prenotazioni WHERE doc_prenotazione = " + this.ID);
                //PageData.Conn.Execute("DELETE FROM newsletter_articoli WHERE doc_articolo = " + this.ID);

                string link = folder.BackofficeHyperlink;
                //CAPIRE A COSA SERVE
                //Serve a cancellare la cache della struttura del menu, che così è obbligato a ricreare
                G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
                cache.RemoveAll("");

                NetCms.GUI.PopupBox.AddSessionMessage("Documento Eliminato", NetCms.GUI.PostBackMessagesType.Success, link);
            }
            //if (PageData.Conn.Execute("UPDATE Docs SET Trash_Doc = " + this.Folder.ReferedAccount.ID + " WHERE id_Doc = " + this.ID))
            //{
            //    PageData.Conn.Execute("DELETE FROM newsletter_prenotazioni WHERE doc_prenotazione = " + this.ID);
            //    PageData.Conn.Execute("DELETE FROM newsletter_articoli WHERE doc_articolo = " + this.ID);

            //    string link = this.Folder.BackofficeHyperlink;
            //    G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
            //    cache.RemoveAll("");
            //    NetCms.GUI.PopupBox.AddSessionMessage("Documento Eliminato", NetCms.GUI.PostBackMessagesType.Success, link);
            //}
        }

        public virtual bool PublishContent()
        {
            //SISTEMARE CON IL CONTROLLO SE SONO ATTIVE O MENO LE REVISIONI
            if (this.DocLangs != null && this.DocLangs.Count() > 0)
                return PublishContent(this.DocLangs.ElementAt(0).Content.ID);
            else
                return false;
        }
        public virtual bool PublishContent(int ContentID)
        {
            return true;
        }

        #region Metodi per lo spostamento del Documento


        public virtual HtmlGenericControl MoveDocView(PageData ExternalData)
        {
            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerHtml = "Spostamento del Documento";
            fieldset.Controls.Add(legend);

            StructureFolderBinder binder = new StructureFolderBinder(ExternalData);
            TreeNode root = binder.TreeFromStructure(this.Network.RootFolder, this.Application);
            NetTreeView folders = new NetTreeView("Seleziona la cartella di destinazione", "Folder_ModuloApplicativo", root);
            folders.NotAllowedMsg = "Attenzione! La cartella selezionata non è di tipo Pagine Web o non si possiedono i permessi di acceso";
            folders.RootDist = ExternalData.RootDist;
            folders.CheckAllows = true;
            folders.Required = true;
            fieldset.Controls.Add(folders.getControl());

            Button move = new Button();
            move.ID = "Move";
            move.Text = "Sposta";
            fieldset.Controls.Add(move);

            if (ExternalData.IsPostBack)
            {
                NetUtility.RequestVariable MoveRV = new NetUtility.RequestVariable(move.ID, NetUtility.RequestVariable.RequestType.Form);
                if (MoveRV.IsValid() && MoveRV.StringValue == move.Text)
                {
                    NetUtility.RequestVariable FolderRV = new NetUtility.RequestVariable(folders.FieldName, NetUtility.RequestVariable.RequestType.Form);
                    if (FolderRV.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                    {
                        StructureFolder targetFolder = (StructureFolder)this.Network.RootFolder.FindFolderByID(FolderRV.IntValue);
                        if (targetFolder != null)
                        {
                            bool moved = MoveDocDB(targetFolder) && MoveDocPhysical(targetFolder.Path, ExternalData);
                            if (moved)
                                PageData.MsgBox.InnerHtml = "Documento Spostato";
                            else
                                PageData.MsgBox.InnerHtml = "Si è verificato un errore non sarà possibile spostare il documento.";
                        }
                        else
                            PageData.MsgBox.InnerHtml = "Si è verificato un errore non sarà possibile spostare il documento.";
                    }
                    else
                        PageData.MsgBox.InnerHtml = "Si è verificato un errore non sarà possibile spostare il documento.";
                }
            }

            return fieldset;
        }
        public virtual bool MoveDocDB(int TargetFolderID)
        {
            StructureFolder targetFolder = (StructureFolder)this.Network.RootFolder.FindFolderByID(TargetFolderID);
            if (targetFolder != null)
            {
                return MoveDocDB(targetFolder);
            }
            return false;
        }
        public virtual bool MoveDocDB(StructureFolder TargetFolder)
        {
            this.Folder.Documents.Remove(this);
            TargetFolder.Documents.Add(this);
            this.Folder = FileSystemBusinessLogic.FolderBusinessLogic.GetById(TargetFolder.ID);

            FileSystemBusinessLogic.DocumentBusinessLogic.SaveOrUpdateDocument(this);

            //SISTEMARE CON CACHE 2° LIVELLO - ADESSO CONTIENE SOLO L'INVALIDAZIONE DEL FRONTEND
            Network.CurrentFolder.Invalidate();
            TargetFolder.Invalidate();

            return true;
        }
        public virtual bool MoveDocPhysical(string TargetFolder, PageData ExternalData)
        {
            return true;
        }
        #endregion

        public abstract HtmlGenericControl getRowView();
        public abstract HtmlGenericControl getRowView(bool SearchView);

        private string _Tags;
        public virtual string Tags
        {
            get
            {
                if (string.IsNullOrEmpty(_Tags))
                {
                    _Tags = string.Empty;

                    if (DocTags != null && DocTags.Count > 0)
                    {
                        IEnumerable<string> taglist = DocTags.Select(x => x.RifTag.Label);
                        _Tags = string.Join(",", taglist);
                    }                   
                }

                return _Tags;
            }
        }

        public virtual object DocumentTags
        {
            get
            {
                object doctags;
                if (this.DocTags != null && this.DocTags.Any())
                {
                    doctags = from tag in this.DocTags
                              select new
                              {
                                  label = tag.RifTag.Label,
                                  key = tag.RifTag.Key
                              };
                }
                else
                {
                    doctags = new { };
                }
                return doctags;
            }
        }

        //public virtual object DocumentAttaches
        //{
        //    get
        //    {
        //        object docAttaches;

        //        if (this.)
        //    }
        //}

        public virtual ICollection<Documents.Tags.DocTag> DocTags
        {
            get;
            set;
        }

        public virtual int ShowInTopMenu
        {
            get;
            set;
        }
       
        #region Metodi dell'interfaccia IGrantsFindable
        
        public virtual GrantsFinder Criteria
        {
            get
            {
                if (_Criteria == null)
                    _Criteria = new DocumentGrants(this, NetCms.Users.AccountManager.CurrentAccount.Profile);
                return _Criteria;
            }
        }
        private GrantsFinder _Criteria;

        public virtual bool Grant
        {
            get
            {
                return this.Criteria[CriteriaTypes.Show].Allowed;
            }
        }

        public virtual bool NeedToShow
        {
            get
            {
                return this.Criteria[CriteriaTypes.Show].Allowed;
            }
        }

        public virtual GrantsFinder CriteriaByProfile(Users.Profile profile)
        {
            return new DocumentGrants(this, profile);
        }


        public virtual NetCms.Grants.GrantValue CriteriaForApplication(CriteriaTypes criteria, string appId)
        {
            string appKey = NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies[appId].SystemName;
                //ApplicationsTable.Select("id_Applicazione = " + applicationID)[0]["SystemName_Applicazione"].ToString();
            string criterio = NetCms.Grants.GrantsUtility.CriteriaSwitch(criteria) + "_" + appKey;
            return Criteria[criterio];
        }

//        public virtual DataTable CriteriaTable
//        {
//            get
//            {
//                if (_CriteriaTable == null)
//                    _CriteriaTable = this.Connection.SqlQuery(@"SELECT Key_Criterio,Tipo_Criterio,id_DocProfile AS id_ObjectProfile,doc_docProfile AS Object_ObjectProfile,Profile_docProfile AS Profile_ObjectProfile,Value_docProfileCriteria AS Value_ObjectProfileCriteria FROM docprofilescriteria
//                     INNER JOIN docprofiles ON id_DocProfile = docProfile_docProfileCriteria
//                     INNER JOIN criteri ON id_Criterio = Criteria_docProfileCriteria");

//                return _CriteriaTable;
//            }
//        }
//        private DataTable _CriteriaTable;

//        public virtual DataTable GetGrantsTable(string associationID)
//        {
//            DataTable _GrantsTable = this.Connection.SqlQuery("SELECT * FROM folderprofilescriteria WHERE FolderProfile_FolderProfileCriteria = " + associationID);
//            return _GrantsTable;
//        }

        public virtual void DeleteGrantsForAssociation(int associationID)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    DocProfile DocProfile = GrantsBusinessLogic.GetDocProfileById(associationID, sess);
                    DocProfile.CriteriaCollection.Clear();
                    GrantsBusinessLogic.SaveOrUpdateDocProfile(DocProfile, sess);

                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
            }

            //string sqlDeleteAssociatedCriteria = "DELETE FROM docprofilescriteria WHERE DocProfile_DocProfileCriteria = " + associationID;
            //this.Connection.Execute(sqlDeleteAssociatedCriteria);
        }

        public virtual bool AddGrant(int associationID, int grantValue, int idCriterio, int creatorUserID)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                User creator = UsersBusinessLogic.GetById(creatorUserID, sess);
                try
                {
                    DocProfile DocProfile = GrantsBusinessLogic.GetDocProfileById(associationID, sess);

                    DocProfileCriteria docProfileCriteria = new DocProfileCriteria();
                    docProfileCriteria.ObjectProfile = DocProfile;
                    docProfileCriteria.Criteria = GrantsBusinessLogic.GetCriterioById(idCriterio,sess);
                    docProfileCriteria.Value = grantValue;
                    docProfileCriteria.Updater = creator;
                    DocProfile.CriteriaCollection.Add(docProfileCriteria);

                    GrantsBusinessLogic.SaveOrUpdateDocProfile(DocProfile, sess);

                    tx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    return false;
                }
            }
            //string sql = "INSERT INTO docprofilescriteria(";
            //sql += " DocProfile_DocProfileCriteria ";
            //sql += ", Value_DocProfileCriteria";
            //sql += ", Criteria_DocProfileCriteria";
            //sql += ", Updater_DocProfileCriteria";
            //sql += ") VALUES ";
            //sql += "(";
            //sql += " " + associationID + " ";
            //sql += ", " + grantValue + " ";
            //sql += ", " + idCriterio + " ";
            //sql += ", " + creatorUserID + " ";
            //sql += ") ";

            //this.Connection.Execute(sql);
        }

        public virtual void DeleteGrantsForAssociation(int associationID, ISession session)
        {
            DocProfile DocProfile = GrantsBusinessLogic.GetDocProfileById(associationID, session);
            DocProfile.CriteriaCollection.Clear();
            GrantsBusinessLogic.SaveOrUpdateDocProfile(DocProfile, session);
        }

        public virtual void AddGrant(int associationID, int grantValue, int idCriterio, int creatorUserID, ISession session)
        {
            User creator = UsersBusinessLogic.GetById(creatorUserID, session);
            DocProfile DocProfile = GrantsBusinessLogic.GetDocProfileById(associationID, session);

            DocProfileCriteria docProfileCriteria = new DocProfileCriteria();
            docProfileCriteria.ObjectProfile = DocProfile;
            docProfileCriteria.Criteria = GrantsBusinessLogic.GetCriterioById(idCriterio,session);
            docProfileCriteria.Value = grantValue;
            docProfileCriteria.Updater = creator;
            DocProfile.CriteriaCollection.Add(docProfileCriteria);

            GrantsBusinessLogic.SaveOrUpdateDocProfile(DocProfile, session);
        }


        public virtual GrantObjectType GrantObjectType
        {
            get { return GrantObjectType.Document; }
        }

        public virtual ICollection<ObjectProfile> GrantObjectProfiles
        {
            get { return GrantsBusinessLogic.GetDocProfiles(this.ID).ToList<ObjectProfile>(); }
        }

        public virtual bool CheckIfProfileIsAssociated(int profileId, ISession session = null)
        {
            return GrantsBusinessLogic.CheckIfProfileIsAssociatedToDoc(this.ID, profileId, session);
        }

        public virtual bool AddGrantProfile(int profileId)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                User creator = UsersBusinessLogic.GetById(NetCms.Users.AccountManager.CurrentAccount.ID, sess);
                try
                {
                    DocProfile DocProfile = new NetCms.Grants.DocProfile();
                    DocProfile.Profile = ProfileBusinessLogic.GetById(profileId, sess);
                    DocProfile.WfsObject = DocumentBusinessLogic.GetById(this.ID,sess);
                    DocProfile.Creator = creator;

                    GrantsBusinessLogic.SaveOrUpdateDocProfile(DocProfile, sess);

                    tx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    return false;
                }
            }
        }

        public virtual bool DeleteGrantProfile(int objectProfileId)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    GrantsBusinessLogic.DeleteDocProfile(objectProfileId, sess);
                    tx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    return false;
                }
            }
        }

        public virtual ObjectProfile AddGrantProfile(int profileId, ISession session)
        {
            User creator = UsersBusinessLogic.GetById(NetCms.Users.AccountManager.CurrentAccount.ID, session);
                
            DocProfile DocProfile = new NetCms.Grants.DocProfile();
            DocProfile.Profile = ProfileBusinessLogic.GetById(profileId, session);
            DocProfile.WfsObject = this;
            DocProfile.Creator = creator;

            return GrantsBusinessLogic.SaveOrUpdateDocProfile(DocProfile, session);
        }

        public virtual bool DeleteGrantProfile(int objectProfileId, ISession session)
        {
            try
            {
                GrantsBusinessLogic.DeleteDocProfile(objectProfileId, session);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public virtual GrantsFinder CriteriaByProfileAndNetwork(Profile profile, NetworkKey networkKey)
        {
            throw new NotImplementedException();
        }

        public virtual bool CheckIfProfileIsAssociated(int profileId, int networkId, ISession session = null)
        {
            return GrantsBusinessLogic.CheckIfProfileIsAssociatedToDoc(this.ID, profileId, session);
        }

        public virtual bool AddGrantProfile(int profileId, int networkId)
        {
            return this.AddGrantProfile(profileId);
        }

        public virtual ObjectProfile AddGrantProfile(int profileId, int networkId, ISession session)
        {
            return this.AddGrantProfile(profileId, session);
        }
        #endregion
    }
}
