using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using NetTable;
using NetCms.Users;

namespace NetCms.Structure.WebFS.Reviews
{
    public class ReviewLog
    {
        #region proprietÓ da mappare

        public virtual int ID
        {
            get;
            set;
        }

        public virtual Review Review
        {
            get;
            set;
        }

        public virtual int Mittente
        {
            get;
            set;
        }

        public virtual int Destinatario
        {
            get;
            set;
        }

        public virtual int Livello
        {
            get;
            set;
        }

        public virtual int GroupID
        {
            get
            {
                return Destinatario;
            }
        }

        public virtual int Oggetto
        {
            get;
            set;
        }

        public virtual DateTime Data
        {
            get;
            set;
        }

        public virtual string Testo
        {
            get;
            set;
        }

        public virtual ICollection<ReviewNotify> ReviewNotifications
        {
            get;
            set;
        }

        #endregion

        //public User Author
        //{
        //    get { return _Author; }
        //    private set { _Author = value; }
        //}
        //private User _Author;

        public virtual User Author
        {
            get
            {
                //return new NetCms.Users.User(Mittente);
                return UsersBusinessLogic.GetById(Mittente);
            }
        }

        public virtual ReviewsChecker Checker
        {
            get 
            {
                return new ReviewsChecker(this.Review.User, this.ActualStepGroup, this.Review.Document);
            }
        }

        //public string ReviewID
        //{
        //    get { return _ReviewID; }
        //    private set { _ReviewID = value; }
        //}
        //private string _ReviewID;

        //public string GroupID
        //{
        //    get { return _GroupID; }
        //    private set { _GroupID = value; }
        //}
        //private string _GroupID;

        //public string ID
        //{
        //    get { return _ID; }
        //    private set { _ID = value; }
        //}
        //private string _ID;
        
        //public int Step
        //{
        //    get { return _Step; }
        //    private set { _Step = value; }
        //}
        //private int _Step;
        
        public virtual Group TargetGroup
        {
            get
            {
                return GroupsHandler.RootGroup.FindGroup(this.GroupID);
            }
        }

        public virtual Group ActualStepGroup
        {
            get
            {
                int i = Livello;
                _ActualStepGroup = GroupsHandler.RootGroup.FindGroup(this.GroupID);
                while (_ActualStepGroup != null && _ActualStepGroup.Parent != null && i-- > 0)
                    _ActualStepGroup = _ActualStepGroup.Parent;

                return _ActualStepGroup;
            }
        }
        private Group _ActualStepGroup;

        public virtual ReviewLogStates Subject
        {
            get 
            {
                return ReviewLog.Parse(Oggetto);
            }
        }
        

        public virtual bool LogExist
        {
            get { return _LogExist; }
            protected internal set { _LogExist = value; }
        }
        private bool _LogExist;

        public ReviewLog()
        {
            ReviewNotifications = new HashSet<ReviewNotify>();

            this.LogExist = true;

            //this.TargetGroup = this.ActualStepGroup = GroupsHandler.RootGroup.FindGroup(this.GroupID);

            //int i = Livello;
            //while (ActualStepGroup != null && ActualStepGroup.Parent != null && i-- > 0)
            //    ActualStepGroup = ActualStepGroup.Parent;

            //_Checker = new ReviewsChecker(this.Review.User, this.ActualStepGroup, this.Review.Document);
        }

        //public ReviewLog(Review review, DataRow reviewLogRecord)
        //{
        //    Init(review, reviewLogRecord);
        //}
        //public ReviewLog(Review review)
        //{

        //    string sqlLogs = " SELECT * FROM revisionilog WHERE Revisione_RevLog = ";
        //    DataTable logs = review.Document.Connection.SqlQuery(sqlLogs + review.ID + " ORDER BY id_RevLog DESC");
        //    if (logs.Rows.Count > 0) Init(review, logs.Rows[0]);
        //}

        //private void Init(Review review, DataRow reviewLogRecord)
        //{
        //    this.LogExist = true;
            //this.Author = new NetCms.Users.User(reviewLogRecord["Mittente_RevLog"].ToString());
            //this.ReviewID = reviewLogRecord["Revisione_RevLog"].ToString();
            //this.Step = (int)reviewLogRecord["Livello_RevLog"];
            //this.GroupID = reviewLogRecord["Destinatario_RevLog"].ToString();
            //this.ID = reviewLogRecord["id_RevLog"].ToString();
            //this.Subject = ReviewLog.Parse(reviewLogRecord["Oggetto_RevLog"].ToString());
        //    this.TargetGroup = this.ActualStepGroup = GroupsHandler.RootGroup.FindGroup(this.GroupID);

        //    int i = Step;
        //    while ( ActualStepGroup != null && ActualStepGroup.Parent != null && i-- > 0)
        //        ActualStepGroup = ActualStepGroup.Parent;

        //    _Checker = new ReviewsChecker(review.User, this.ActualStepGroup, review.Document);
        //}

        public static ReviewLogStates Parse(int val)
        {
            switch(val)
            {
                case 0: return ReviewLogStates.Request;
                case 1: return ReviewLogStates.Approved;
                case 2: return ReviewLogStates.Rejected;
                case 3: return ReviewLogStates.Cancelled;
                case 5: return ReviewLogStates.Published;
                default: return ReviewLogStates.Archivied;
            }
        }
    }
}