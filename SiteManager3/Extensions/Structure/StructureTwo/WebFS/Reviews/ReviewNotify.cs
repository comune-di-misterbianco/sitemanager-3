﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Structure.WebFS.Reviews
{
    public class ReviewNotify
    {
        public ReviewNotify()
        { }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual Document Document
        {
            get;
            set;
        }

        public virtual Review Review
        {
            get;
            set;
        }

        public virtual ReviewLog ReviewLog
        {
            get;
            set;
        }

        public virtual int Mittente
        {
            get;
            set;
        }

        public virtual int Destinatario
        {
            get;
            set;
        }

        public virtual int Stato
        {
            get;
            set;
        }
    }
}
