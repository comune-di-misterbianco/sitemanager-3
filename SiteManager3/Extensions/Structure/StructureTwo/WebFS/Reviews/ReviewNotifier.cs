using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using NetTable;
using NetCms.Networks.WebFS;
using NetCms.Structure.WebFS;
using NetCms.Users;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Grants;
using NHibernate;
using NetCms.DBSessionProvider;
using System.Linq;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Structure.WebFS.Reviews
{
    public class ReviewNotifier
    {
        

        public  Dictionary<string, Users.GroupAssociation> UsersToNotify
        {
            get
            {
                if (_UsersToNotify == null)
                    _UsersToNotify = new Dictionary<string, Users.GroupAssociation>();
                return _UsersToNotify;
            }
        }
        private Dictionary<string, Users.GroupAssociation> _UsersToNotify;

        public Review Review
        {
            get
            {
                return _Review;
            }
            private set { _Review = value; }
        }
        private Review _Review;
        
        public User User
        {
            get { return _User; }
            private set { _User = value; }
        }
        private User _User;
        
        public ReviewNotifier()
        {
            G2Core.Common.RequestVariable userReq = new G2Core.Common.RequestVariable("uid", G2Core.Common.RequestVariable.RequestType.QueryString);
            G2Core.Common.RequestVariable review = new G2Core.Common.RequestVariable("rid", G2Core.Common.RequestVariable.RequestType.QueryString);

            //User = new User(user.StringValue);
            //User user = UsersBusinessLogic.GetById(userReq.IntValue);
            User = UsersBusinessLogic.GetById(userReq.IntValue);

            Review = FileSystemBusinessLogic.ReviewBusinessLogic.GetReviewById(review.IntValue);
            Review.User = User;
            //Review = new Review(review.StringValue, User, Connection);
        }
        public ReviewNotifier(Review review, NetCms.Users.User user)
        {
            this._Review = review;
            this.User = user;
        }

        public void SendNotifies()
        {
            foreach (Group group in this.User.Groups)
                SendNotifiesForGroup(group);

            SendNotifiesToUsers();
        }

        private void SendNotifiesForGroup(Group group)
        {
            var revisors = group.GetRevisorsAssociations();
            bool atLeastOnFound = false;
            foreach (var association in revisors)
            {
                if (
                    this.Review.Document.CriteriaByProfile(association.User.Profile)[CriteriaTypes.Redactor].Allowed ||
                    this.Review.Document.CriteriaByProfile(association.User.Profile)[CriteriaTypes.Publisher].Allowed
                    )
                {
                    if (!UsersToNotify.ContainsKey(association.User.ID.ToString()) && association.User.ID!= this.User.ID)
                    {
                        this.UsersToNotify.Add(association.User.ID.ToString(), association);
                        atLeastOnFound = true;
                    }
                }
            }
            if (!atLeastOnFound && group.Parent != null)
                SendNotifiesForGroup(group.Parent);
        }

//        private void SendNotifiesToUsers()
//        {
//            string sql = @"INSERT INTO revisioni_notifiche
//                            (
//	                             Doc_RevisioneNotifica
//	                            ,Destinatario_RevisioneNotifica
//	                            ,Mittente_RevisioneNotifica
//	                            ,Revisione_RevisioneNotifica
//	                            ,Stato_RevisioneNotifica
//	                            ,Log_RevisioneNotifica
//                            )
//                            VALUES ";
//            string vals = @"
//                            (
//	                             '{0}'
//	                            ,'{1}'
//	                            ,'{2}'
//	                            ,'{3}'
//	                            ,'{4}'
//	                            ,'{5}'
//                            )";
//            bool first = true;

//            if (NetCms.Configurations.Debug.GenericEnabled && this.UsersToNotify.Count>0)
//                NetCms.GUI.PopupBox.AddSessionMessage("Sono state inviate le notifiche di revisione ai seguenti utenti: ", NetCms.GUI.PostBackMessagesType.Normal);
            
//            foreach (var userVal in this.UsersToNotify)
//            {
//                foreach (var log in this.Review.ActiveLogs)
//                    //if (log.GroupID == userVal.Value.GroupID)
//                    {
//                        if (first) first = false;
//                        else sql += ",";
//                        sql += string.Format(vals, this.Review.Document.ID, userVal.Value.User.ID, this.User.ID, this.Review.ID, "0", log.ID);

//                        if (NetCms.Configurations.Debug.GenericEnabled)
//                        {
//                            string debugLog = "Utente: " + userVal.Value.User.UserName + " - " + userVal.Value.User.NomeCompleto + " - " + userVal.Value.User.ID + ". Per la richiesta al gruppo " + userVal.Value.Group.Name;
//                            NetCms.GUI.PopupBox.AddSessionMessage(debugLog, NetCms.GUI.PostBackMessagesType.Normal);
//                        }
//                    }
//            }

//            this.Connection.Execute(sql);
//        }

        private void SendNotifiesToUsers()
        {
            if (NetCms.Configurations.Debug.GenericEnabled && this.UsersToNotify.Count > 0)
                NetCms.GUI.PopupBox.AddSessionMessage("Sono state inviate le notifiche di revisione ai seguenti utenti: ", NetCms.GUI.PostBackMessagesType.Normal);
            
            using (ISession sess = FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    User mittente = UsersBusinessLogic.GetById(this.User.ID, sess);

                    foreach (var userVal in this.UsersToNotify)
                    {
                        User destinatario = UsersBusinessLogic.GetById(userVal.Value.User.ID, sess);

                        foreach (var log in this.Review.ActiveLogs)
                        {
                            Document doc = DocumentBusinessLogic.GetById(this.Review.Document.ID, sess);
                            Review review = ReviewBusinessLogic.GetReviewById(this.Review.ID, sess);

                            ReviewNotify notify = new ReviewNotify();
                            notify.Document = doc;
                            notify.Destinatario = destinatario.ID;
                            notify.Mittente = mittente.ID;
                            notify.Review = review;
                            notify.Stato = 0;
                            notify.ReviewLog = log;

                            ReviewBusinessLogic.SaveOrUpdateReviewNotify(notify, sess);

                            string title = "L'Utente '" + mittente.NomeCompleto + "' chiede che sia revisionato il documento '" + notify.Document.DocLangs.ElementAt(0).Label + "' del portale '" + notify.Document.Network.Label + "'.";
                            string Text = title;

                            string url = "/" + notify.Document.Network.SystemName + "/docs/overview.aspx?folder=" + notify.Document.Folder.ID + "&doc=" + notify.Document.ID + "&review=" + notify.Review.ID;

                            NetCms.Users.NotificaBackoffice.CreateNewNotificaBackoffice(title, Text, NetCms.Users.NotificaBackoffice.NotificaBackofficeType.Revisioni, destinatario.Profile.ID, mittente.Profile.ID, url, notify.Document.Network.ID, sess);

                            if (NetCms.Configurations.Debug.GenericEnabled)
                            {
                                string debugLog = "Utente: " + userVal.Value.User.UserName + " - " + userVal.Value.User.NomeCompleto + " - " + userVal.Value.User.ID + ". Per la richiesta al gruppo " + userVal.Value.Group.Name;
                                NetCms.GUI.PopupBox.AddSessionMessage(debugLog, NetCms.GUI.PostBackMessagesType.Normal);
                            }
                        }
                    }
                    tx.Commit();

                }
                catch (Exception ex)
                {
                    tx.Rollback();

                }
            }
        }
    }
}