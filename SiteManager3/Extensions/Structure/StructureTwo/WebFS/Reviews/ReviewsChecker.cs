using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using NetTable;
using NetCms.Users;
using NetCms.Networks.Grants;
using NetCms.Grants;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Structure.WebFS.Reviews
{
    public class ReviewsChecker
    {
        public User User
        {
            get { return _User; }
            private set { _User = value; }
        }
        private User _User;

        private Group _Group;
        public Group Group
        {
            get
            {
                return _Group;
            }
            private set { _Group= value; }
        }

       /* public ReviewLog RevLog
        {
            get { return _RevLog; }
            private set { _RevLog= value; }
        }
        private ReviewLog _RevLog;
        */

        public Document Document
        {
            get { return _Document; }
            private set { _Document = value; }
        }
        private Document _Document;

        public ReviewsChecker(User user, Group group, Document doc)
        {
            this.Document = doc;
            this.User = user;
            this.Group = group;
        }

        public bool Is4me(ContentAdministrationTypes role)
        {
            return FindReviewPositions(role).Count>0;
        }

        public List<Group> FindReviewPositions(ContentAdministrationTypes role)
        {
            List<Group> groups = new List<Group>();

            //NUOVO SISTEMA DI CALCOLO PERMESSI
            
            //if (this.Document.CriteriaProxy[this.User.Profile][NetCms.Structure.Grants.CriteriaTypes.Redactor] && this.Group!=null)
            if (this.Document.CriteriaByProfile(this.User.Profile)[CriteriaTypes.Redactor].Allowed && this.Group != null)    
                foreach (GroupAssociation groupAssociation in this.User.AssociatedGroupsOrdered)
                    if (groupAssociation.ContentsRole >= role)
                        if( groupAssociation.Group.ID == Group.ID || Group.IsMyParent(groupAssociation.GroupID.ToString()))
                            groups = AddGroup(groups, groupAssociation);

            return groups;
        }

        public List<Group> AddGroup(List<Group> groups, GroupAssociation groupToAdd)
        {
            List<Group> newGroups = new List<Group>();
            bool addGroup = true;
            if (groups.Count > 0)
            {
                foreach (Group group in groups)
                {
                    if (group.IsMyParent(groupToAdd.GroupID.ToString()))
                    {
                        addGroup = false;
                        break;
                    }
                    
                    if (!group.IsMyChild(groupToAdd.GroupID.ToString()))
                        newGroups.Add(group);
                }
            }

            if (addGroup) newGroups.Add(groupToAdd.Group);

            return newGroups;
        }

        private List<Group> FilterReviewPositions(List<Group> groups)
        {
            List<Group> newGroups = new List<Group>();

            //questa funzione deve eseguire il "taglio" dei gruppi dalla lista dei gruppi 
            //dal quale l'utente pu� revisonare (groups) il documento corrente. 
            //Va quindi effettuata una ricerca di eventuali revisori nei gruppi presenti tra: il gruppo nel quale � stata effettuata la richiesta
            //e il gruppo dal quale io potrei revisionare.
            //Non implementando questa funzione il sistema permette di revisionare un documento scavalcando utenti di grado inferiore.

            return newGroups;
        }
    }
}