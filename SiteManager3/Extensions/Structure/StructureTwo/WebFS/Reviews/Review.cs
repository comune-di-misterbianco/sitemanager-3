using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using NetTable;
using NetCms.Users;
using System.Linq;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NHibernate;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Structure.WebFS.Reviews
{
    public class Review
    {
        #region Propriet� da mappare

        public virtual int ID
        {
            get;
            set;
        }

        public virtual DocContent Content
        {
            get;
            set;
        }
        
        public virtual int Published
        {
            get;
            set;
        }

        public virtual int Autore
        {
            get;
            set;
        }

        public virtual int Origine
        {
            get;
            set;
        }

        public virtual int Approvata
        {
            get;
            set;
        }

        public virtual DateTime Data
        {
            get;
            set;
        }

        public virtual DocumentLang DocLang
        {
            get;
            set;
        }

        public virtual ICollection<ReviewLog> ReviewLogs
        {
            get;
            set;
        }

        public virtual ICollection<ReviewNotify> ReviewNotifications
        {
            get;
            set;
        }

        #endregion

        //public virtual User Author
        //{
        //    get { return new NetCms.Users.User(Autore); }
        //}

        public virtual User Author
        {
            get 
            { 
                return UsersBusinessLogic.GetById(Autore); 
            }
        }

        public virtual User User
        {
            get { return _User; }
            /*private*/ set { _User = value; }
        }
        private User _User;

        public virtual bool Pubblicata
        {
            get { return Published == 1; }
        }


        public virtual string DatailsPageURL
        {
            get
            {
                if (_DatailsPageURL == null)
                    _DatailsPageURL = Document.Network.Paths.AbsoluteAdminRoot + "/docs/overview.aspx?folder=" + this.Document.Folder.ID + "&amp;doc=" + this.Document.ID + "&amp;review=" + this.ID;
                return _DatailsPageURL;
            }
        }
        private string _DatailsPageURL;

        public virtual string Code
        {
            get
            {
                if (_Code == null)
                {
                    _Code = "#REV/" + G2Core.Common.Utility.FormatNumber(ID.ToString(), 3);
                }
                return _Code;
            }
        }
        private string _Code;

        public virtual string PreviewJS
        {
            get
            {
                StructureFolder folder = FolderBusinessLogic.GetById(this.Document.Folder.ID);
                return "javascript: Preview('" + folder.FrontendHyperlink + "'," + this.Content.ID + ",'" + folder.Path + "')";
            }
        }



        public virtual string StatusLabel
        {
            get
            {
                string GlobalStatus = Pubblicata ? "Pubblicata" : (ActiveLogs.Count == 0 ? "Bozza" : "Workflow");
                return GlobalStatus;
            }
        }

        //public DataRow ReviewRecord
        //{
        //    get { return _ReviewRecord; }
        //    private set { _ReviewRecord = value; }
        //}
        //private DataRow _ReviewRecord;

        public virtual Document Document
        {
            get { return DocLang.Document; }
        }

        //public virtual DataTable Logs
        //{
        //    get
        //    {
        //        if (_Logs == null)
        //        {
        //            string sqlLogs = " SELECT * FROM revisionilog WHERE Revisione_RevLog = " + this.ID;
        //            _Logs = this.Document.Connection.SqlQuery(sqlLogs);
        //        }
        //        return _Logs;
        //    }
        //}
        //private DataTable _Logs;

        //public virtual List<ReviewLog> ActiveLogs
        //{
        //    get
        //    {
        //        if (_ActiveLogs == null)
        //        {
        //            _ActiveLogs = new List<ReviewLog>();

        //            DataRow[] rootLogs = Logs.Select("Livello_RevLog = 0");
        //            foreach (DataRow revlogData in rootLogs)
        //            {
        //                DataRow currentLog = Logs.Select("Destinatario_RevLog = " + revlogData["Destinatario_RevLog"], "id_RevLog DESC")[0];

        //                _ActiveLogs.Add(new ReviewLog(this, currentLog));
        //            }
        //        }
        //        return _ActiveLogs;
        //    }
        //}
        //private List<ReviewLog> _ActiveLogs;

        public virtual List<ReviewLog> ActiveLogs
        {
            get
            {
                if (_ActiveLogs == null)
                {
                    _ActiveLogs = new List<ReviewLog>();
                    List<ReviewLog> RootLogs = new List<ReviewLog>();
                    //_ActiveLogs.AddRange(ReviewLogs.Where(x=>x.Livello==0));

                    RootLogs.AddRange(ReviewLogs.Where(x => x.Livello == 0));

                    var DestinatariRoot = (from dest in RootLogs select dest.Destinatario).Distinct();


                    foreach (int dest in DestinatariRoot)
                    {
                       _ActiveLogs.Add(ReviewLogs.Where(x => x.Destinatario == dest).OrderByDescending(x => x.ID).First());
                    }

                }
                return _ActiveLogs;
            }
        }
        private List<ReviewLog> _ActiveLogs;

        public virtual void RefreshActiveLogs()
        {
            _ActiveLogs = null;
        }

        //public ReviewsManager ReviewsManager
        //{
        //    get { return _ReviewsManager; }
        //    private set { _ReviewsManager = value; }
        //}
        //private ReviewsManager _ReviewsManager;

        //public Review(ReviewsManager reviewsManager, DataRow reviewRecord)
        //{
        //    this.User = reviewsManager.User;
        //    this.Document = reviewsManager.CurrentDocument;
        //    this.ReviewsManager = reviewsManager;
        //    Init(reviewRecord);
        //}
        //public Review(DataRow reviewRecord, NetCms.Users.User user, Document doc)
        //{
        //    this.User = user;
        //    this.Document = doc;
        //    Init(reviewRecord);
        //}
        //public Review(string reviewID, NetCms.Users.User user, Document doc)
        //{
        //    this.User = user;
        //    this.Document = doc;
        //    string sql = ReviewsManager.SqlQueryReviews + " WHERE id_Revisione = " + reviewID;
        //    Init(doc.Connection.SqlQuery(sql).Rows[0]);
        //}
        //public Review(string reviewID, NetCms.Users.User user, NetCms.Connections.Connection conn)
        //{
        //    this.User = user;
        //    string sql = ReviewsManager.SqlQueryReviews + " WHERE id_Revisione = " + reviewID;
        //    Init(conn.SqlQuery(sql).Rows[0]);
        //}

        public Review()
        {
            ReviewLogs = new HashSet<ReviewLog>();
            ReviewNotifications = new HashSet<ReviewNotify>();
        }

        public Review(NetCms.Users.User user)
        {
            this.User = user;
            ReviewLogs = new HashSet<ReviewLog>();
            ReviewNotifications = new HashSet<ReviewNotify>();
        }

        public virtual Review GetRebuildedReview()
        {
            //Verificare che sia effettivamente return this
            
            ReviewBusinessLogic.GetCurrentSession().Refresh(this);
            this.RefreshActiveLogs();
            return this;//new Review(this.ID, this.User, this.Document);
        }
        
        private void Init(DataRow reviewRecord)
        {
            //this.ReviewRecord = reviewRecord;
            //this.Author = new NetCms.Users.User(reviewRecord["Autore_Revisione"].ToString());
            //this.ContentID = reviewRecord["Contenuto_Revisione"].ToString();
            //this.ID = reviewRecord["id_Revisione"].ToString();
            //this.Pubblicata = reviewRecord["Published_Revisione"].ToString() == "1";
            //if(Document == null)
            //    this.Document =(Document) NetCms.Networks.NetworksManager.CurrentActiveNetwork.RootFolder.FindDoc(reviewRecord["id_Doc"].ToString());
        }

        //public virtual void AskReview()
        //{
        //    string requestSql = "INSERT INTO revisionilog (Revisione_RevLog,Mittente_RevLog,Livello_RevLog,Oggetto_RevLog,Testo_RevLog,Data_RevLog,Destinatario_RevLog) VALUES (";
        //    requestSql += this.ID + ",";
        //    requestSql += this.User.ID + ",";
        //    requestSql += 0 + ",";//Livello di partenza della richiesta (� sempre 0)
        //    requestSql += 0 + ",";//Essendo una richiesta di revisione l'oggetto � 0
        //    requestSql += "'L''utente " + this.User.NomeCompleto + " richiede che sia revisionato il documento " + this.Document.Nome + "." + "',";
        //    requestSql += this.Document.Connection.FilterDateTime(DateTime.Now) + ",";
        //    requestSql += "{0})";

        //    foreach (Group group in this.User.Groups)
        //        this.Document.Connection.Execute(string.Format(requestSql, group.ID.ToString()));
        //}

        public virtual void AskReview()
        {
            int idCurrentReview = this.ID;

            using(ISession session = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                   
                    Review currentReview = ReviewBusinessLogic.GetReviewById(idCurrentReview, session);
                    currentReview.User = this.User;

                    foreach (Group group in currentReview.User.Groups)
                    {
                        ReviewLog revisioneLog = new ReviewLog();
                        revisioneLog.Review = currentReview;
                        revisioneLog.Mittente = currentReview.User.ID;
                        revisioneLog.Livello = 0;//Livello di partenza della richiesta (� sempre 0)
                        revisioneLog.Oggetto = 0;//Essendo una richiesta di revisione l'oggetto � 0
                        revisioneLog.Testo = "L'utente " + currentReview.User.NomeCompleto + " richiede che sia revisionato il documento " + currentReview.Document.Nome + ".";
                        revisioneLog.Data = DateTime.Now;
                        revisioneLog.Destinatario = group.ID;

                        ReviewBusinessLogic.SaveOrUpdateReviewLog(revisioneLog,session);
                        currentReview.ReviewLogs.Add(revisioneLog);
                    }

                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
            }
            this.RefreshActiveLogs();
        }

        //public virtual void AskReview(ReviewLog Log)
        //{
        //    string requestSql = "INSERT INTO revisionilog (Revisione_RevLog,Mittente_RevLog,Livello_RevLog,Oggetto_RevLog,Testo_RevLog,Data_RevLog,Destinatario_RevLog) VALUES (";
        //    requestSql += this.ID + ",";
        //    requestSql += this.User.ID + ",";
        //    requestSql += Log.Livello + ",";//Livello di partenza della richiesta (� sempre 0)
        //    requestSql += 0 + ",";//Essendo una richiesta di revisione l'oggetto � 0
        //    requestSql += "'L''utente " + this.User.NomeCompleto + " richiede che sia revisionato il documento " + this.Document.Nome + "." + "',";
        //    requestSql += this.Document.Connection.FilterDateTime(DateTime.Now) + ",";
        //    requestSql += Log.GroupID + ")";

        //    this.Document.Connection.Execute(requestSql);
        //}

        public virtual void AskReview(ReviewLog Log)
        {
            int idCurrentReview = this.ID;

            using(ISession session = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    
                    Review currentReview = ReviewBusinessLogic.GetReviewById(idCurrentReview, session);
                    currentReview.User = this.User;

                    ReviewLog revisioneLog = new ReviewLog();
                    revisioneLog.Review = currentReview;
                    revisioneLog.Mittente = currentReview.User.ID;
                    revisioneLog.Livello = Log.Livello;//Livello di partenza della richiesta (� sempre 0)
                    revisioneLog.Oggetto = 0;//Essendo una richiesta di revisione l'oggetto � 0
                    revisioneLog.Testo = "L'utente " + currentReview.User.UserName + " richiede che sia revisionato il documento " + currentReview.Document.Nome + ".";
                    revisioneLog.Data = DateTime.Now;
                    revisioneLog.Destinatario = Log.GroupID;

                    ReviewBusinessLogic.SaveOrUpdateReviewLog(revisioneLog,session);

                    currentReview.ReviewLogs.Add(revisioneLog);

                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
            }
            this.RefreshActiveLogs();
        }

        //public virtual void Reject(ReviewLog Log)
        //{
        //    string requestSql = "INSERT INTO revisionilog (Revisione_RevLog,Mittente_RevLog,Livello_RevLog,Oggetto_RevLog,Testo_RevLog,Data_RevLog,Destinatario_RevLog) VALUES (";
        //    requestSql += this.ID + ",";
        //    requestSql += this.User.ID + ",";
        //    requestSql += (Log.Livello + 1) + ",";
        //    requestSql += 2 + ",";//Essendo un respinta di revisione l'oggetto � 2
        //    requestSql += "'L''utente " + this.User.NomeCompleto + " ha respinto la revisione per il documento " + this.Document.Nome + "." + "',";
        //    requestSql += this.Document.Connection.FilterDateTime(DateTime.Now) + ",";
        //    requestSql += Log.GroupID + ")";

        //    this.Document.Connection.Execute(requestSql);

        //    string updateSql = "UPDATE revisioni_notifiche SET Stato_RevisioneNotifica = 1 WHERE Revisione_RevisioneNotifica = " + this.ID + " AND Log_RevisioneNotifica = " + this.ID;
        //    this.Document.Connection.Execute(updateSql);
        //}

        public virtual void Reject(ReviewLog Log)
        {
            int idCurrentReview = this.ID;

            using(ISession session = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {                    
                    Review currentReview = ReviewBusinessLogic.GetReviewById(idCurrentReview, session);
                    currentReview.User = this.User;

                    ReviewLog revisioneLog = new ReviewLog();
                    revisioneLog.Review = currentReview;
                    revisioneLog.Mittente = currentReview.User.ID;
                    revisioneLog.Livello = Log.Livello + 1;
                    revisioneLog.Oggetto = 2;//Essendo un respinta di revisione l'oggetto � 2
                    revisioneLog.Testo = "L'utente " + currentReview.User.UserName + " ha respinto la revisione per il documento " + currentReview.Document.Nome + ".";
                    revisioneLog.Data = DateTime.Now;
                    revisioneLog.Destinatario = Log.GroupID;

                    revisioneLog = ReviewBusinessLogic.SaveOrUpdateReviewLog(revisioneLog,session);
                    currentReview.ReviewLogs.Add(revisioneLog);
                    //ReviewBusinessLogic.UpdateStatoRevisioneNotificaByRevisioneAndLog(currentReview, revisioneLog, session);
                    foreach (ReviewNotify notify in currentReview.ReviewNotifications)
                    {
                        notify.Stato = 1;
                        ReviewBusinessLogic.SaveOrUpdateReviewNotify(notify, session);
                    }

                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
            }
            this.RefreshActiveLogs();
        }

        //public virtual void Approve(ReviewLog Log)
        //{
        //    string requestSql = "INSERT INTO revisionilog (Revisione_RevLog,Mittente_RevLog,Livello_RevLog,Oggetto_RevLog,Testo_RevLog,Data_RevLog,Destinatario_RevLog) VALUES (";
        //    requestSql += Log.Review.ID + ",";
        //    requestSql += this.User.ID + ",";
        //    requestSql += (Log.Livello + 1) + ",";//Livello di partenza della richiesta (� sempre 0)
        //    requestSql += 1 + ",";
        //    requestSql += "'L''utente " + this.User.NomeCompleto + " ha approvato la revisione per il documento " + this.Document.Nome + "." + "',";
        //    requestSql += this.Document.Connection.FilterDateTime(DateTime.Now) + ",";
        //    requestSql += Log.GroupID + ")";

        //    this.Document.Connection.Execute(requestSql);

        //    string updateSql = "UPDATE revisioni_notifiche SET Stato_RevisioneNotifica = 1 WHERE Revisione_RevisioneNotifica = " + this.ID;// +" AND Log_RevisioneNotifica = " + Log.ID;
        //    this.Document.Connection.Execute(updateSql);
        //}

        public virtual void Approve(ReviewLog Log)
        {
            int idCurrentReview = this.ID;

            using(ISession session = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {

                    Review currentReview = ReviewBusinessLogic.GetReviewById(idCurrentReview, session);
                    currentReview.User = this.User;

                    ReviewLog revisioneLog = new ReviewLog();
                    revisioneLog.Review = ReviewBusinessLogic.GetReviewById(Log.Review.ID,session);
                    revisioneLog.Mittente = currentReview.User.ID;
                    revisioneLog.Livello = Log.Livello + 1;
                    revisioneLog.Oggetto = 1;
                    revisioneLog.Testo = "L'utente " + currentReview.User.UserName + " ha approvato la revisione per il documento " + currentReview.Document.Nome + ".";
                    revisioneLog.Data = DateTime.Now;
                    revisioneLog.Destinatario = Log.GroupID;

                    revisioneLog = ReviewBusinessLogic.SaveOrUpdateReviewLog(revisioneLog,session);
                    currentReview.ReviewLogs.Add(revisioneLog);
                    foreach (ReviewNotify notify in currentReview.ReviewNotifications)
                    {
                        notify.Stato = 1;
                        ReviewBusinessLogic.SaveOrUpdateReviewNotify(notify,session);
                    }

                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
            }
            this.RefreshActiveLogs();
        }

        //public virtual void Annull(ReviewLog Log)
        //{
        //    string requestSql = "INSERT INTO revisionilog (Revisione_RevLog,Mittente_RevLog,Livello_RevLog,Oggetto_RevLog,Testo_RevLog,Data_RevLog,Destinatario_RevLog) VALUES (";
        //    requestSql += this.ID + ",";
        //    requestSql += this.User.ID + ",";
        //    requestSql += (Log.Livello + 1) + ",";
        //    requestSql += 3 + ",";
        //    requestSql += "'L''utente " + this.User.NomeCompleto + " ha annullato la revisione per il documento " + this.Document.Nome + "." + "',";
        //    requestSql += this.Document.Connection.FilterDateTime(DateTime.Now) + ",";
        //    requestSql += Log.GroupID + ")";

        //    this.Document.Connection.Execute(requestSql);

        //    string updateSql = "UPDATE revisioni_notifiche SET Stato_RevisioneNotifica = 1 WHERE Revisione_RevisioneNotifica = " + this.ID + " AND Log_RevisioneNotifica = " + this.ID;
        //    this.Document.Connection.Execute(updateSql);
        //}

        public virtual void Annull(ReviewLog Log)
        {
            int idCurrentReview = this.ID;

            using(ISession session = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    Review currentReview = ReviewBusinessLogic.GetReviewById(idCurrentReview, session);
                    currentReview.User = this.User;

                    ReviewLog revisioneLog = new ReviewLog();
                    revisioneLog.Review = currentReview;
                    revisioneLog.Mittente = currentReview.User.ID;
                    revisioneLog.Livello = Log.Livello + 1;
                    revisioneLog.Oggetto = 3;
                    revisioneLog.Testo = "L'utente " + currentReview.User.UserName + " ha annullato la revisione per il documento " + currentReview.Document.Nome + ".";
                    revisioneLog.Data = DateTime.Now;
                    revisioneLog.Destinatario = Log.GroupID;

                    revisioneLog = ReviewBusinessLogic.SaveOrUpdateReviewLog(revisioneLog,session);
                    currentReview.ReviewLogs.Add(revisioneLog);
                    //ReviewBusinessLogic.UpdateStatoRevisioneNotificaByRevisioneAndLog(currentReview, revisioneLog, session);
                    foreach (ReviewNotify notify in currentReview.ReviewNotifications)
                    {
                        notify.Stato = 1;
                        ReviewBusinessLogic.SaveOrUpdateReviewNotify(notify, session);
                    }

                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
            }
            this.RefreshActiveLogs();
        }

        //public virtual bool Publish()
        //{
        //    bool result = true;
        //    try
        //    {
        //        if (ActiveLogs.Count > 0)
        //        {
        //            foreach (ReviewLog Log in this.ActiveLogs)
        //            {
        //                string requestSql = "INSERT INTO revisionilog (Revisione_RevLog,Mittente_RevLog,Livello_RevLog,Oggetto_RevLog,Testo_RevLog,Data_RevLog,Destinatario_RevLog) VALUES (";
        //                requestSql += this.ID + ",";
        //                requestSql += this.User.ID + ",";
        //                requestSql += (Log.Livello + 1) + ",";
        //                requestSql += 5 + ",";
        //                requestSql += "'L''utente " + NetCms.Connections.Connection.FilterString(this.User.NomeCompleto) + " ha Pubblicato la revisione per il documento " + NetCms.Connections.Connection.FilterString(this.Document.Nome) + "." + "',";
        //                requestSql += this.Document.Connection.FilterDateTime(DateTime.Now) + ",";
        //                requestSql += Log.GroupID + ")";
        //                result = result && this.Document.Connection.Execute(requestSql);

        //            }
        //        }
        //        else
        //        {
        //            string requestSql = "INSERT INTO revisionilog (Revisione_RevLog,Mittente_RevLog,Livello_RevLog,Oggetto_RevLog,Testo_RevLog,Data_RevLog,Destinatario_RevLog) VALUES (";
        //            requestSql += this.ID + ",";
        //            requestSql += this.User.ID + ",";
        //            requestSql += "0,";
        //            requestSql += 5 + ",";
        //            requestSql += "'L''utente " + NetCms.Connections.Connection.FilterString(this.User.NomeCompleto) + " ha Pubblicato la revisione per il documento " + NetCms.Connections.Connection.FilterString(this.Document.Nome) + "." + "',";
        //            requestSql += this.Document.Connection.FilterDateTime(DateTime.Now) + ",";
        //            requestSql += this.User.Groups[0].ID + ")";
        //            result = result && this.Document.Connection.Execute(requestSql);
        //        }

        //        string updateSql = "UPDATE revisioni SET Published_Revisione = 0 WHERE Published_Revisione = 1 AND DocLang_Revisione = " + this.Document.DocLangs.ElementAt(0).ID;
        //        result = result && this.Document.Connection.Execute(updateSql);

        //        updateSql = "UPDATE revisioni SET Published_Revisione = 1 WHERE id_Revisione = " + this.ID;
        //        result = result && this.Document.Connection.Execute(updateSql);

        //        updateSql = "UPDATE revisioni_notifiche SET Stato_RevisioneNotifica = 1 WHERE Revisione_RevisioneNotifica = " + this.ID;
        //        result = result && this.Document.Connection.Execute(updateSql);

        //        string sql = "UPDATE Docs_Lang SET";
        //        sql += " Content_DocLang = " + this.Content.ID;
        //        sql += " WHERE id_DocLang = " + this.Document.DocLangs.ElementAt(0).ID;
        //        result = result && this.Document.Connection.Execute(sql);

        //        G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendMenu", this.Document.Network.SystemName);

        //        //SISTEMARE CON CACHE 2� LIVELLO
        //        //this.Document.Folder.InvalidateDocs();
        //    }
        //    catch(Exception ex)
        //    { 
        //        result = false;
        //        Diagnostics.Tracer.TraceMessage(Diagnostics.TraceLevel.Error, "Errore nella pubblicazione della revisione relativa " + this.Content.ID + " al documento" + this.Document.ID);
        //        Diagnostics.Diagnostics.TraceException(ex, null, null);
        //    }

        //    return result;
        //}

        public virtual bool Publish()
        {
            bool result = true;
            int idCurrentReview = this.ID;

            using(ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {                    
                    Review currentReview = ReviewBusinessLogic.GetReviewById(idCurrentReview,sess);
                    currentReview.User = this.User;

                    if (currentReview.ActiveLogs.Count > 0)
                    {
                        foreach (ReviewLog Log in currentReview.ActiveLogs)
                        {
                            ReviewLog revisioneLog = new ReviewLog();
                            //revisioneLog.Review = this;
                            revisioneLog.Review = currentReview;
                            //revisioneLog.Mittente = this.User.ID;
                            revisioneLog.Mittente = currentReview.User.ID;
                            revisioneLog.Livello = Log.Livello + 1;
                            revisioneLog.Oggetto = 5;
                            //revisioneLog.Testo = "'L''utente " + this.User.NomeCompleto + " ha Pubblicato la revisione per il documento " + this.Document.Nome + ".";
                            revisioneLog.Testo = "L'utente " + currentReview.User.UserName + " ha pubblicato la revisione per il documento " + currentReview.Document.Nome + ".";
                            revisioneLog.Data = DateTime.Now;
                            revisioneLog.Destinatario = Log.GroupID;

                            ReviewBusinessLogic.SaveOrUpdateReviewLog(revisioneLog, sess);
                            if (result)
                                //this.ReviewLogs.Add(revisioneLog);
                                currentReview.ReviewLogs.Add(revisioneLog);
                        }
                    }
                    else
                    {
                        ReviewLog revisioneLog = new ReviewLog();
                        //revisioneLog.Review = this;
                        revisioneLog.Review = currentReview;
                        //revisioneLog.Mittente = this.User.ID;
                        revisioneLog.Mittente = currentReview.User.ID;
                        revisioneLog.Livello = 0;
                        revisioneLog.Oggetto = 5;
                        //revisioneLog.Testo = "'L''utente " + this.User.NomeCompleto + " ha Pubblicato la revisione per il documento " + this.Document.Nome + ".";
                        revisioneLog.Testo = "L'utente " + currentReview.User.NomeCompleto + " ha pubblicato la revisione per il documento " + currentReview.Document.Nome + ".";
                        revisioneLog.Data = DateTime.Now;
                        //revisioneLog.Destinatario = this.User.Groups.ElementAt(0).ID;
                        revisioneLog.Destinatario = currentReview.User.Groups.ElementAt(0).ID;

                        ReviewBusinessLogic.SaveOrUpdateReviewLog(revisioneLog, sess);
                        if (result)
                            //this.ReviewLogs.Add(revisioneLog);
                            currentReview.ReviewLogs.Add(revisioneLog);
                    }

                    //ReviewBusinessLogic.UpdatePublishedRevisioniByDocLang(this.Document.DocLangs.ElementAt(0), sess);
                    ReviewBusinessLogic.UpdatePublishedRevisioniByDocLang(currentReview.DocLang, sess);

                    currentReview.Published = 1;

                    //foreach (ReviewNotify rn in this.ReviewNotifications)
                    foreach (ReviewNotify rn in currentReview.ReviewNotifications)
                    {
                        rn.Stato = 1;
                    }

                    //ReviewBusinessLogic.SaveOrUpdateReview(this, sess);
                    ReviewBusinessLogic.SaveOrUpdateReview(currentReview, sess);

                    //this.Document.DocLangs.ElementAt(0).Content = this.Content;
                    currentReview.DocLang.Content = currentReview.Content;

                    //DocumentBusinessLogic.SaveOrUpdateDocumentLang(this.Document.DocLangs.ElementAt(0), sess);
                    DocumentBusinessLogic.SaveOrUpdateDocumentLang(currentReview.DocLang, sess);

                    //StructureFolder currentFolder = this.Document.Folder as StructureFolder;
                    StructureFolder currentFolder = FolderBusinessLogic.GetById(sess, this.Document.Folder.ID); //currentReview.Document.Folder as StructureFolder;

                    //if (currentFolder.EnableRss)
                    //    currentFolder.RssManager.SaveXML();

                   
                    //SISTEMARE CON CACHE 2� LIVELLO
                    //this.Document.Folder.InvalidateDocs();

                    tx.Commit();
                    //Questa istruzione serve ad invalidare il men� di frontend ed � necessaria
                    //G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendMenu", this.Document.Network.SystemName);
                    G2Core.Caching.Invalidation.InvalidationHandler.Invalidate("FrontendMenu", currentReview.Document.Network.SystemName);

                    if (currentFolder.EnableRss)
                        currentFolder.RssManager.SaveXML(sess);

                    result = true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    result = false;
                    Diagnostics.Tracer.TraceMessage(Diagnostics.TraceLevel.Error, "Errore nella pubblicazione della revisione relativa " + this.Content.ID + " al documento" + this.Document.ID);
                    Diagnostics.Diagnostics.TraceException(ex, null, null);
                }
            }

            this.RefreshActiveLogs();
            return result;
        }
    }
}