﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetForms;
using NetCms.Structure.Applications;
using NetCms.Grants;
using System.Linq;


/// <summary>
/// Summary description for NewsFolder_NEW_
/// </summary>
namespace NetCms.Structure.WebFS.Reviews
{
    public class ReviewsManagerPage : NetCms.GUI.NetworkPage
    {
        public string NewReviewLink
        {
            get
            {
                if (LanguageManager.Utility.Config.EnableMultiLanguage)
                {
                    int lang = TempCurrentLang;//this.SelectedLang.IsValidInteger ? this.SelectedLang.IntValue : LanguageManager.BusinessLogic.LanguageBusinessLogic.GetDefaultLanguage().ID;
                    return this.CurrentFolder.StructureNetwork.Paths.AbsoluteAdminRoot + "/docs/review.aspx?folder=" + this.CurrentFolder.ID + "&doc=" + this.CurrentDocument.ID + "&lang=" + lang;
                }
                return this.CurrentFolder.StructureNetwork.Paths.AbsoluteAdminRoot + "/docs/review.aspx?folder=" + this.CurrentFolder.ID + "&doc=" + this.CurrentDocument.ID;
            }
        }
        
        public override NetCms.GUI.Icons.Icons PageIcon
        {
	        get { return NetCms.GUI.Icons.Icons.Accept; }
        }

        public override string PageTitle
        {
            get { return "Revisioni per il Documento '" + this.CurrentDocument.Nome + "'"; }
        }
        
        public override string LocalCssClass
        {
            get { return ""; }
        }

        public G2Core.Common.RequestVariable Show
        {
            get
            {
                if (_Show == null)
                    _Show = new G2Core.Common.RequestVariable("show", G2Core.Common.RequestVariable.RequestType.QueryString);
                return _Show;
            }
        }
        private G2Core.Common.RequestVariable _Show;

        public G2Core.Common.RequestVariable SelectedLang
        {
            get
            {
                if (_SelectedLang == null)
                {
                    
                    if (LanguageManager.Utility.Config.EnableMultiLanguage)
                    {
                        _SelectedLang = new G2Core.Common.RequestVariable("Language_DocReview", G2Core.Common.RequestVariable.RequestType.Form_QueryString);
                        
                    }
                }
                return _SelectedLang;
            }
        }
        private G2Core.Common.RequestVariable _SelectedLang;
        private int _TempCurrLang;
        public int TempCurrentLang
        {
            get
            {
                if (_TempCurrLang == 0)
                {
                    if (LanguageManager.Utility.Config.EnableMultiLanguage)
                    {
                        if (!HttpContext.Current.Request.UrlReferrer.AbsoluteUri.Contains("review.aspx") && 
                            ((!HttpContext.Current.Request.Url.AbsoluteUri.Contains("overview.aspx") && !HttpContext.Current.Request.UrlReferrer.AbsoluteUri.Contains("overview.aspx")) || 
                            (HttpContext.Current.Request.Url.AbsoluteUri.Contains("overview.aspx") && HttpContext.Current.Request.UrlReferrer.AbsoluteUri.Contains("overview.aspx") && SelectedLang.IsPostBack) || HttpContext.Current.Request.UrlReferrer.AbsoluteUri.Contains("default.aspx")))
                        {
                            HttpContext.Current.Session["TempCurrLanguage"] = (SelectedLang != null && SelectedLang.IsValidInteger) ? SelectedLang.IntValue : LanguageManager.BusinessLogic.LanguageBusinessLogic.GetDefaultLanguage().ID;
                        }
                    }
                    else
                        HttpContext.Current.Session["TempCurrLanguage"] = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetDefaultLanguage().ID;
                    _TempCurrLang = (HttpContext.Current.Session["TempCurrLanguage"] != null)? int.Parse(HttpContext.Current.Session["TempCurrLanguage"].ToString()) : LanguageManager.BusinessLogic.LanguageBusinessLogic.GetDefaultLanguage().ID;
                }
                return _TempCurrLang;
            }
        }
        protected override WebControl BuildControls()
        {
            return new ReviewsManager(this.Account,this.CurrentDocument, TempCurrentLang);
        }


        private bool CheckFirstDocLangRecord
        {
            get
            {
                bool checkFirstDocLangRecord = false;
                if (LanguageManager.Utility.Config.EnableMultiLanguage)
                {                    
                    if (SelectedLang.IsValidInteger)                        
                        checkFirstDocLangRecord = this.CurrentDocument.DocLangs.Any(x => x.Lang == SelectedLang.IntValue);
                    else
                        checkFirstDocLangRecord = this.CurrentDocument.DocLangs.Any(x => x.Lang == this.PageData.SelectedLang);
                }
                else
                    checkFirstDocLangRecord = true; // do per scontato che il record esiste

                return checkFirstDocLangRecord;
            }

        }

        public ReviewsManagerPage()
        {
            if (this.CurrentDocument.Criteria[CriteriaTypes.Modify].Value == GrantsValues.Deny)
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", PostBackMessagesType.Error, NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());

            if (CheckFirstDocLangRecord)
            Toolbar.Buttons.Add(new ToolbarButton(NewReviewLink, NetCms.GUI.Icons.Icons.Page_White_Add, "Nuova Revisione Vuota"));

            base.AddDocumentInformationsToSide();

        }
        

    }
}
