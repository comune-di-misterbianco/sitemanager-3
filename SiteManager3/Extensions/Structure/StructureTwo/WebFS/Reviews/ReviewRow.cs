using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using NetTable;
using NetCms.Networks.WebFS;
using NetCms.Structure.WebFS;
using NetCms.Users;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Structure.WebFS.Reviews
{
    public class ReviewRow:TableRow
    {
        public Review Review
        {
            get { return _Review; }
            private set { _Review = value; }
        }
        private Review _Review;

        private bool CanBePublished = false;

        //public ReviewRow(ReviewsManager reviewsManager, DataRow reviewRecord)
        //{
        //    this.Review = new Review(reviewsManager, reviewRecord);
        //}

        public ReviewRow(ReviewsManager reviewsManager, Review review)
        {
            this.Review = review;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
                                    
            #region Costruzione delle celle della riga

            TableCell cell;

            cell = new TableCell();
            cell.Text = this.Review.Code;
            this.Cells.Add(cell);

            cell = new TableCell();
            cell.Text = "<a href=\""+this.Review.Author.AdminDetailLink+"\" title=\""+this.Review.Author.NomeCompleto+" ("+this.Review.Author.UserName+")\">"+ this.Review.Author.NomeCompleto + "</a>";
            this.Cells.Add(cell);

            cell = new TableCell();
            cell.Text = this.Review.Data.ToString().Replace(" "," ore ");
            int lastIndexOfColumn = cell.Text.LastIndexOf(':');
            if(cell.Text.Length < lastIndexOfColumn)
                cell.Text = cell.Text.Substring(0, cell.Text.LastIndexOf(':'));
            this.Cells.Add(cell);

            cell = new TableCell();
            cell.Text = this.Review.StatusLabel;
            this.Cells.Add(cell);

            cell = new TableCell();
            cell.CssClass = "action details";
            cell.Text = "<a href=\"" + this.Review.PreviewJS + "\"><span>Anteprima</span></a>";
            this.Cells.Add(cell);

            cell = new TableCell();
            cell.CssClass = "action details";
            cell.Text = "<a href=\"" + Review.DatailsPageURL + "\"><span>Scheda e Opzioni</span></a>";
            this.Cells.Add(cell);

            cell = new TableCell();
            cell.CssClass = "action modify";
            NetCms.Networks.Network net = this.Review.Document.Network;
            string Link = net.Paths.AbsoluteAdminRoot + "/docs/review.aspx?";
            Link += "folder=" + this.Review.Document.Folder.ID;
            Link += "&amp;doc=" + this.Review.Document.ID;
            Link += "&amp;review=" + this.Review.ID;
            cell.Text = "<a href=\"" + Link + "\" title=\"Crea una nuova revisione a partire dalla revisione " + this.Review.Code + "\"><span>Nuova Revisione</span></a>";
            this.Cells.Add(cell); 

            #endregion
        }

    }
}