using System;
using System.Data;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using NetTable;
using NetCms.Networks.WebFS;
using NetCms.Structure.WebFS;
using NetCms.Users;
using NetCms.Grants;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Structure.WebFS.Reviews
{
    public class ReviewPage:WebControl
    {
        public User User
        {
            get { return _User; }
            private set { _User = value; }
        }
        private User _User;

        public Review Review
        {
            get { return _Review; }
            private set { _Review = value; }
        }
        private Review _Review;

        public G2Core.XhtmlControls.InputField LogRequest
        {
            get
            {
                if (_LogRequest == null)
                    _LogRequest = new G2Core.XhtmlControls.InputField("logReview_U" + this.User.ID);
                return _LogRequest;
            }
        }
        private G2Core.XhtmlControls.InputField _LogRequest;

        public G2Core.XhtmlControls.InputField ActionRequest
        {
            get
            {
                if (_ActionRequest == null)
                    _ActionRequest = new G2Core.XhtmlControls.InputField("actionReview_U" + this.User.ID);
                return _ActionRequest;
            }
        }
        private G2Core.XhtmlControls.InputField _ActionRequest;

        public G2Core.XhtmlControls.InputField ReviewRequest
        {
            get
            {
                if (_ReviewRequest == null)
                    _ReviewRequest = new G2Core.XhtmlControls.InputField("reviewReview_U" + this.User.ID);
                return _ReviewRequest;
            }
        }
        private G2Core.XhtmlControls.InputField _ReviewRequest;

        public G2Core.XhtmlControls.InputField GroupRequest
        {
            get
            {
                if (_GroupRequest == null)
                    _GroupRequest = new G2Core.XhtmlControls.InputField("groupReview_U" + this.User.ID);
                return _GroupRequest;
            }
        }
        private G2Core.XhtmlControls.InputField _GroupRequest;

        private bool CanBePublished
        {
            get
            {
                return _CanBePublished && this.Review.Document.CriteriaByProfile(User.Profile)[CriteriaTypes.Publisher].Allowed;
            }
            set { _CanBePublished = value; }
        }
        private bool _CanBePublished = false;

        public ReviewPage(Review review, NetCms.Users.User user, Document doc):base( HtmlTextWriterTag.Div)
        {
            this.User = user;
            this.Review = review;
            this.Review.User = User;

            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();
            this.NotifiesAddress = "http://" + ip + this.Review.Document.Network.Paths.AbsoluteAdminRoot + "/docs/notify.aspx?uid=" + this.User.ID + "&rid=" + this.Review.ID;
        }

        protected override void OnInit(EventArgs e)
        {
            #region PostBack

            if (Page.IsPostBack)
            {
                if (this.Review.ActiveLogs.Count > 0) //Se non ci sono record di log vuol dire che non � stata mai fatta alcuna richiesta di revisione per la revisione
                    foreach (ReviewLog ActiveLog in this.Review.ActiveLogs)
                        CheckPostback(ActiveLog);
                else
                    CheckPostback();
                this.Review = this.Review.GetRebuildedReview();
            }
            #endregion

            base.OnInit(e);
            this.AppendControls();
            this.Controls.Add(this.ActionRequest.Control);
            this.Controls.Add(this.LogRequest.Control);
            this.Controls.Add(this.ReviewRequest.Control);
            this.Controls.Add(this.GroupRequest.Control);
            this.Controls.Add(new WebControl(HtmlTextWriterTag.P));
        }

        public string[] GetCurrentAction(ReviewRole role, ReviewLog ActiveLog)
        {
            string label = "";
            string action = "";
            string action2 = "";
            string from = "";
            ReviewLogStates state = ActiveLog.Subject;

            #region if (role == ReviewRole.Author)

            if (role == ReviewRole.CurrentLogAuthor)
            {
                if (state == ReviewLogStates.Published) { label = "Pubblicata"; action = ""; }
                if (state == ReviewLogStates.Approved) { label = "Approvata"; action = RequestLink(ActiveLog); }
                if (state == ReviewLogStates.Cancelled) { label = "Annullata Da Te"; action = RequestLink(ActiveLog); }
                if (state == ReviewLogStates.Rejected) { label = "Respinta"; action = ""; }
                if (state == ReviewLogStates.Request)
                {
                    label = "Richiesta Pendente";
                    action = "<a class=\"action stop\" href=\"" + this.BuildActionLink("2", ActiveLog) + "\"><span>Annulla Richiesta</span></a>";
                }
                if (state == ReviewLogStates.Archivied) { label = "Bozza"; action = RequestLink(ActiveLog); }
            }
            if (role == ReviewRole.ReviewAuthor)
            {
                if (state == ReviewLogStates.Published) { label = "Pubblicata"; action = ""; }
                if (state == ReviewLogStates.Approved) { label = "Approvata"; action = ""; }
                if (state == ReviewLogStates.Cancelled) { label = "Annullata Da Te"; action = RequestLink(ActiveLog); }
                if (state == ReviewLogStates.Rejected) { label = "Respinta"; action = ""; }
                if (state == ReviewLogStates.Request)
                {
                    if (ActiveLog.Livello == 0)
                    {
                        label = "Richiesta Pendente";
                        action = "<a class=\"action stop\" href=\"" + this.BuildActionLink("2", ActiveLog) + "\"><span>Annulla Richiesta</span></a>";
                    }
                    else
                    {
                        label = "Workflow"; action = "";
                    }
                }
                if (state == ReviewLogStates.Archivied) 
                { 
                    label = "Bozza"; action = RequestLink(ActiveLog); 
                }
            }
            #endregion

            #region if (role == ReviewRole.Reviewer || role == ReviewRole.Publisher)

            if (role == ReviewRole.Reviewer || role == ReviewRole.Publisher)
            {

                if (state == ReviewLogStates.Approved)
                {
                    label = "Approvata";
                    if (role != ReviewRole.Publisher)
                    {
                        action = RequestLink(ActiveLog);
                    }
                }
                if (state == ReviewLogStates.Published) { label = "Pubblicata"; action = ""; }
                if (state == ReviewLogStates.Cancelled) { label = "Ferma"; action = ""; }
                if (state == ReviewLogStates.Rejected) { label = "Respinta"; action = ""; }
                if (state == ReviewLogStates.Request)
                {
                    label = "Richiesta Pendente";
                    action = "";
                    string Approva = "<a class=\"action ok\" href=\"" + this.BuildActionLink("4", ActiveLog) + "\"><span>Approva</span></a>";
                    string Respingi = "<a class=\"action stop\" href=\"" + this.BuildActionLink("3", ActiveLog) + "\"><span>Respingi</span></a>";

                    var gruppiDalQualeRevisiono = ActiveLog.Checker.FindReviewPositions(NetCms.Users.ContentAdministrationTypes.Review);
                    foreach (Group group in gruppiDalQualeRevisiono)
                    {
                        from = group.Name;
                        action = string.Format(Approva, group.ID);
                        action2 = string.Format(Respingi, group.ID);
                    }
                }
                if (state == ReviewLogStates.Archivied) { label = "Ferma"; action = ""; }
            }
            
            #endregion

            #region if (role == ReviewRole.NoOne)
            if (role == ReviewRole.NoOne)
            {
                if (state == ReviewLogStates.Approved) 
                { label = "Approvata"; action = ""; }
                if (state == ReviewLogStates.Published) { label = "Pubblicata"; action = ""; }
                if (state == ReviewLogStates.Cancelled) { label = "Workflow"; action = ""; }
                if (state == ReviewLogStates.Rejected) { label = "Workflow"; action = ""; }
                if (state == ReviewLogStates.Request) { label = "Workflow"; action = ""; }
                if (state == ReviewLogStates.Archivied)
                {
                    if (this.Review.User.ID == this.Review.Author.ID)
                    {
                        label = "Ferma";
                        action = RequestLink(ActiveLog);
                    }
                    else
                    {
                        action = "";
                        label = "Workflow";
                    }
                }
            } 
            #endregion

            this.CanBePublished = this.CanBePublished || ((role == ReviewRole.Publisher) && (state == ReviewLogStates.Approved || state == ReviewLogStates.Request || state == ReviewLogStates.Archivied));

            string[] value = { label, action, action2, from };
            return value;
        }

        private string PublishLink(ReviewLog Log)
        {
            return "<a class=\"action web\" href=\"" + this.BuildActionLink("5", Log) + "\">Pubblica</a>";
        }
        private string RequestLink(ReviewLog Log)
        {
            return "<a class=\"action ask\" href=\"" + this.BuildActionLink("1", Log) + "\">Richiedi Revisione</a>";
        }

        public void CheckPostback(ReviewLog Log)
        {
            if (this.LogRequest.RequestVariable.IsValidInteger && Log.ID == this.LogRequest.RequestVariable.IntValue)
            {
                if ((this.ActionRequest.RequestVariable.IsValidInteger) && (this.ReviewRequest.RequestVariable.IsValidInteger))
                {
                    switch (this.ActionRequest.RequestVariable.IntValue)
                    {
                        case 1:
                            this.Review.AskReview(Log);
                            this.Controls.Add(SendNotifies());
                            break;
                        case 2: this.Review.Annull(Log); break;
                        case 3: this.Review.Reject(Log); break;
                        case 4: this.Review.Approve(Log); break;
                        case 5: 
                            bool ok = this.Review.Publish();
                            if (ok) NetCms.GUI.PopupBox.AddMessageAndReload("Revisione Pubblicata con Successo", NetCms.GUI.PostBackMessagesType.Success);
                            else NetCms.GUI.PopupBox.AddMessageAndReload("Non � stato possibile pubblicare la revisione a causa di un problema tecnico, se il problema persiste contattare l'amministratore. ", NetCms.GUI.PostBackMessagesType.Error);
                            break;
                    }
                }
            }
        }
        public void CheckPostback()
        {
            if (this.LogRequest.RequestVariable.IntValue == 0)
                if ((this.ActionRequest.RequestVariable.IsValidInteger) && (this.ReviewRequest.RequestVariable.IsValidInteger))
                {
                    switch (this.ActionRequest.RequestVariable.IntValue)
                    {
                        case 1:
                            this.Review.AskReview(); 
                            this.Controls.Add(SendNotifies());
                            break;
                        case 2: bool ok = this.Review.Publish(); 
                            if (ok) NetCms.GUI.PopupBox.AddMessageAndReload("Revisione Pubblicata con Successo", NetCms.GUI.PostBackMessagesType.Success);
                            else NetCms.GUI.PopupBox.AddMessageAndReload("Non � stato possibile pubblicare la revisione a causa di un problema tecnico, se il problema persiste contattare l'amministratore. ", NetCms.GUI.PostBackMessagesType.Error);
                            break;
                    }
                }
        }

        public string BuildActionLink(string action, ReviewLog log)
        {
            return BuildActionLink(action, log.Review.ID.ToString(), log.ID.ToString(), log.GroupID.ToString());
        }

        public string BuildActionLink(string action, string ReviewID, string logID, string GroupID)
        {
            string link = "javascript: ";
            link += "setFormValue('{0}','" + this.ActionRequest.RequestVariable.Key + "',0);";
            link += "setFormValue('{1}','" + this.LogRequest.RequestVariable.Key + "',0);";
            link += "setFormValue('{2}','" + this.GroupRequest.RequestVariable.Key + "',0);";
            link += "setFormValue('{3}','" + this.ReviewRequest.RequestVariable.Key + "',1);";

            return string.Format(link, action, logID != null ? logID : "0", GroupID != null ? GroupID : "0", ReviewID);
        }

        protected void AppendControls()
        {
            #region Stato Attuale della Revisione

            /*
            if (!Review.Pubblicata)
            {
                if (this.Review.ActiveLogs.Count == 0) //Se non ci sono record di log vuol dire che non � stata mai fatta alcuna richiesta di revisione per la revisione
                {
                    CurrentStatus += "&nbsp;";
                    GlobalStatus = "Bozza";
                    if (Review.Author.ID == Review.User.ID)
                        CurrentAction += "<li><a href=\"" + this.BuildActionLink("1", Review.ID, null, null) + "\">Richiedi Revisione</a></li>";
                    else
                        CurrentAction += "&nbsp;";
                }
                else
                {
                    GlobalStatus = "Workflow";
                    foreach (ReviewLog ActiveLog in this.Review.ActiveLogs)
                    {
                        ReviewRole actualRole = ReviewRole.NoOne;
                        if ((ActiveLog.LogExist && this.Review.User.ID == ActiveLog.Author.ID) || (this.Review.User.ID == this.Review.Author.ID)) actualRole = ReviewRole.Author;
                        else if (ActiveLog.Checker.Is4me(NetCms.Users.ContentAdministrationTypes.Publish)) actualRole = ReviewRole.Publisher;
                        else if (ActiveLog.Checker.Is4me(NetCms.Users.ContentAdministrationTypes.Review)) actualRole = ReviewRole.Reviewer;

                        var actions = GetCurrentAction(actualRole, ActiveLog);

                        CurrentAction += actions[1].Length > 0 ? "<li>" + actions[1] + "</li>" : "";
                        CurrentStatus += actions[0].Length > 0 ? "<li>" + actions[0] + "</li>" : "";
                        CurrentFrom += actions[2].Length > 0 ? "<li>" + actions[2] + "</li>" : "";
                    }
                    if (this.CanBePublished)
                        CurrentAction += PublishLink(this.Review.ActiveLogs[0]);
                }
            }*/
            #endregion
            
            G2Core.UI.DetailsSheet details = new G2Core.UI.DetailsSheet("Riepilogo revisione", @"Questa pagina mostra le informazioni relative ad una revisione, e ne permette la sua gestione.");

            details.AddTitleTextRow("Codice", this.Review.Code);
            details.AddTitleTextRow("Autore", "<a class=\"action details\" href=\"" + this.Review.Author.AdminDetailLink + "\" title=\"" + this.Review.Author.NomeCompleto + " (" + this.Review.Author.UserName + ")\">" + this.Review.Author.NomeCompleto + "</a>");

            details.AddTitleTextRow("Documento", this.Review.Document.Nome);
            details.AddTitleTextRow("Stato Globale", this.Review.StatusLabel);
            
            TableRow l2row = new TableRow();

            TableCell cell2 = new TableCell();
            cell2.Text = "<a class=\"action details\" href=\"" + this.Review.PreviewJS+ "\"><span>Mostra Anteprima</span></a>";
            cell2.ColumnSpan = 2;
            l2row.Cells.Add(cell2);

            details.AddRow(l2row, G2Core.UI.DetailsSheet.Colors.Green);

            G2Core.UI.DetailsSheet logs = new G2Core.UI.DetailsSheet("Stato Workflow", "Questa tabella mostra l'elenco delle richieste di revisione e il loro attuale stato, ogni riga di questa tabella rappresenta lo stato attuale di un workflow inviato al gruppo indicato nella colonna 'Inviata al Gruppo'.");
            logs.SeparateColumns = G2Core.UI.DetailsSheet.SeparateColumnsOptions.All;
            logs.AddColumn("Autore", G2Core.UI.DetailsSheet.Colors.Default);
            logs.AddColumn("Inviata al Gruppo", G2Core.UI.DetailsSheet.Colors.Default);
            logs.AddColumn("Stato", G2Core.UI.DetailsSheet.Colors.Default);
            logs.AddColumn("Azioni", G2Core.UI.DetailsSheet.Colors.Default,2);

            bool addWorkflow = false;
            TableRow row = new TableRow();
            if (!Review.Pubblicata)
            {
                if (this.Review.ActiveLogs.Count == 0) //Se non ci sono record di log vuol dire che non � stata mai fatta alcuna richiesta di revisione per la revisione
                {
                    string action = "";
                    if (Review.Author.ID == Review.User.ID)
                    {
                        if(Review.User.Publisher && this.Review.Document.CriteriaByProfile(Review.User.Profile)[CriteriaTypes.Publisher].Allowed)
                            action += "<a class=\"action ok\" href=\"" + this.BuildActionLink("2", Review.ID.ToString(), null, null) + "\"><span>Pubblica</span></a>";
                        else
                            action += "<a class=\"action ok\" href=\"" + this.BuildActionLink("1", Review.ID.ToString(), null, null) + "\"><span>Richiedi Revisione</span></a>";
                    }
                    else
                        action += "&nbsp;";

                    TableRow lrow = new TableRow(); 

                    TableCell cell = new TableCell();
                    cell.Text = action;
                    cell.ColumnSpan = 2;
                    lrow.Cells.Add(cell);

                    details.AddRow(lrow, G2Core.UI.DetailsSheet.Colors.Green);
                }
                else
                {
                    foreach (ReviewLog ActiveLog in this.Review.ActiveLogs)
                    {
                        ReviewRole actualRole = BuildReviewRole(ActiveLog);

                        var actions = GetCurrentAction(actualRole, ActiveLog);

                        string Action1 = actions[1].Length > 0 ? "" + actions[1] + "" : "";
                        string Action2 = actions[2].Length > 0 ? "" + actions[2] + "" : "";
                        string CurrentStatus = actions[0].Length > 0 ? "" + actions[0] + "" : "";
                        string CurrentFrom = actions[2].Length > 0 ? "" + actions[3] + "" : "";

                        logs.AddRow("<a class=\"action details\" href=\"" + ActiveLog.Author.AdminDetailLink + "\" title=\"" + ActiveLog.Author.NomeCompleto + " (" + ActiveLog.Author.UserName + ")\">" + ActiveLog.Author.NomeCompleto + "</a>",
                                        CurrentFrom,
                                        CurrentStatus,
                                        Action1,
                                        Action2);
                        addWorkflow = true;
                    }
                    if (this.CanBePublished)
                    {
                        TableRow lrow = new TableRow();

                        TableCell cell = new TableCell();
                        cell.Text = PublishLink(this.Review.ActiveLogs[0]);
                        cell.ColumnSpan = 2;
                        lrow.Cells.Add(cell);

                        details.AddRow(lrow, G2Core.UI.DetailsSheet.Colors.Green);
                    }
                }
            }

            this.Controls.Add(details);
            if (addWorkflow) this.Controls.Add(logs);
        }

        private ReviewRole BuildReviewRole(ReviewLog ActiveLog)
        {
            ReviewRole actualRole = ReviewRole.NoOne;

            if (!ActiveLog.LogExist)
            {
                if (this.Review.User.ID == this.Review.Author.ID)
                    actualRole = ReviewRole.ReviewAuthor;
            }
            else
            {
                if (ActiveLog.Checker.Is4me(NetCms.Users.ContentAdministrationTypes.Publish)) 
                    actualRole = ReviewRole.Publisher;

                else if (this.Review.User.ID == ActiveLog.Author.ID) 
                    actualRole = ReviewRole.CurrentLogAuthor;

                else if (ActiveLog.Checker.Is4me(NetCms.Users.ContentAdministrationTypes.Review)) 
                    actualRole = ReviewRole.Reviewer;
            }

            return actualRole;
        }

        public string NotifiesAddress
        {
            get { return _NotifiesAddress; }
            private set { _NotifiesAddress = value; }
        }
        private string _NotifiesAddress;
        
        private WebControl SendNotifies()
        {
            WebControl ctr = new WebControl(HtmlTextWriterTag.Iframe);
            ctr.Attributes.Add("height", "0px");
            ctr.Attributes.Add("width", "0px");
            ctr.Attributes.Add("src", NotifiesAddress);
            ctr.Style.Add(HtmlTextWriterStyle.Visibility, "hidden");
            return ctr;
        }
    }
}