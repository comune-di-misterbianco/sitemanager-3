using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using NetTable;
using NetCms.Users;
using NetForms;
using System.Linq;

/// <summary>
/// Summary description for StructureGrants
/// </summary>
/// 
namespace NetCms.Structure.WebFS.Reviews
{
    public enum ReviewRole { NoOne, ReviewAuthor, CurrentLogAuthor, Reviewer, Publisher }
    public enum ReviewLogStates { Archivied, Request, Approved, Rejected, Cancelled, Published }

    public class ReviewsManager : WebControl
    {
        public const string SqlQueryReviews = @" SELECT * FROM
             docs INNER JOIN docs_Lang ON (id_Doc = Doc_DocLang)
             INNER JOIN revisioni ON (id_DocLang = DocLang_Revisione)
             INNER JOIN Users ON Autore_Revisione = id_User";

        public User User
        {
            get { return _User; }
            private set { _User = value; }
        }
        private User _User;
        
        public Document CurrentDocument
        {
            get
            {
                return _CurrentDocument;
            }
        }
        private Document _CurrentDocument;

        public ReviewsManager(User user, Document document, int selectedLang)
            : base(HtmlTextWriterTag.Div)
        {
            this.User = user;
            _CurrentDocument = document;
            _SelectedLang = selectedLang;
        }

        private int _SelectedLang;
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!CheckFirstDocLangRecord)
            {
                this.Controls.Add(new LiteralControl("<p class=\"alert alert-warning\" role=\"alert\">Attenzione: prima di inserire una nuova revisione nella lingua selezionata � necessario tradurre il titolo" +
                        " e la descrizione per il contenuto corrente accedendo alla pagina <a href=\"docs_mod.aspx?folder=" + this.CurrentDocument.Folder.ID + "&doc=" + this.CurrentDocument.ID + "\"><i class=\"fa fa-cog\" aria-hidden=\"true\"></i><strong>Propriet� Documento</strong></p>"));
            }
            else 
            { 
                G2Core.Common.RequestVariable reviewRequest = new G2Core.Common.RequestVariable("review", G2Core.Common.RequestVariable.RequestType.QueryString);
                if (reviewRequest.IsValidInteger)
                {                    
                        
                        Review review = FileSystemBusinessLogic.ReviewBusinessLogic.GetReviewById(reviewRequest.IntValue);
                        //Settare l'user della revisione
                        review.User = this.User;
                        ReviewPage page = new ReviewPage(review, this.User, CurrentDocument);
                        #region log
                        string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di gestione della revisione con id=" + reviewRequest + " del documento '" + this.CurrentDocument.Nome + "'.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        #endregion
                        this.Controls.Add(page);                                     
                }
                else InitReviewsManager();
            }
        }

        private bool CheckFirstDocLangRecord
        {
            get
            {
                bool checkFirstDocLangRecord = false;
                if (LanguageManager.Utility.Config.EnableMultiLanguage)                
                   checkFirstDocLangRecord = this.CurrentDocument.DocLangs.Any(x => x.Lang == _SelectedLang);                                   
                else
                    checkFirstDocLangRecord = true; // do per scontato che il record esiste

                return checkFirstDocLangRecord;
            }

        }

        protected void InitReviewsManager()
        {

            IList<Review> reviews = FileSystemBusinessLogic.ReviewBusinessLogic.FindReviewsByDocId(this.CurrentDocument.ID, new List<string>() { "Data", "ID" }, true);

            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                NetFormTable tableLangChooser = new NetFormTable("ReviewLanguage", "LanguagesForReview");

                NetDropDownList LanguageChooser = new NetDropDownList("Revisioni per la lingua:", "Language_DocReview");
                LanguageChooser.ClearList();

                var languages = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages();
                foreach (LanguageManager.Model.Language language in languages)
                {
                    //LanguageManager.Model.Language language = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetLanguageById(docLang.Lang);
                    LanguageChooser.addItem(language.Nome_Lang, language.ID.ToString(), language.ID == _SelectedLang);
                }
                LanguageChooser.Required = true;
                tableLangChooser.addField(LanguageChooser);

                NetForm formLangChooser = new NetForm();
                formLangChooser.AddTable(tableLangChooser);
                formLangChooser.AddBackButton = false;
                formLangChooser.AddSubmitButton = true;
                this.Controls.Add(formLangChooser.getForms("Scelta lingua del documento '" + this.CurrentDocument.Nome + "'"));
  
            }
            reviews = reviews.Where(x => x.DocLang.Lang == _SelectedLang).ToList();
            this.Controls.Add(new WebControl(HtmlTextWriterTag.P));

            

           
            #region Table & Header Row
            Table table = new Table();
            table.CellPadding = 3;
            table.CellSpacing = 0;
            table.CssClass = "tab";
            table.Attributes["border"] = "1px";
            table.BorderStyle = BorderStyle.Solid;
            table.Style.Add(HtmlTextWriterStyle.Width, "100%");
            table.Style.Add("border", "1px solid #cccccc");
            table.Style.Add(HtmlTextWriterStyle.FontSize, "14px");
            table.Style.Add(HtmlTextWriterStyle.FontFamily, "verdana");

            TableHeaderCell hcell;

            TableHeaderRow hrow = new TableHeaderRow();
            table.Rows.Add(hrow);

            hcell = new TableHeaderCell();
            hcell.Text = "Codice";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Autore";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Data";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Stato Generale";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Anteprima";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Scheda e Opzioni";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Nuova Revisione";
            hrow.Cells.Add(hcell);
                       
            #endregion

            foreach (Review review in reviews)
            {
                review.User = this.User;
                //Settare l'user della revisione
                ReviewRow row = new ReviewRow(this, review);
                table.Rows.Add(row);
            }
                        
            this.Controls.Add(table);
            this.Controls.Add(new WebControl(HtmlTextWriterTag.P));
            #region log
            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di gestione delle revisioni per il documento '" + this.CurrentDocument.Nome + "', relativamente al Portale " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Label + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            #endregion
        }
    }
}