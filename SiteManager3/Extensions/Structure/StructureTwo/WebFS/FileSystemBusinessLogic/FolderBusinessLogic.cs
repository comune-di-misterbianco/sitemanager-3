﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using System.Threading.Tasks;
using NHibernate.Criterion;
using NHibernate.Transform;
using System.Threading;
using NetCms.DBSessionProvider;
using NetCms.Users;
using System.Web.UI.WebControls;
using System.Web.UI;
using NetCms.Networks;
using NetCms.Networks.WebFS;
using NetService.Utility.ArTable;
using NetCms;
using NetCms.GUI;

namespace NetCms.Structure.WebFS.FileSystemBusinessLogic
{
    public static class FolderBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static StructureFolder GetById(int id)
        {
            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(GetCurrentSession());
            return dao.GetById(id);
        }

        public static StructureFolder GetById(ISession session, int id)
        {
            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(session);
            return dao.GetById(id);
        }

        public static StructureFolder GetByName(StructureFolder parent, string key)
        {
            StructureFolder folder = null;
            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(GetCurrentSession());
            SearchParameter nomeSearchParameter = new SearchParameter(null,"Nome",key, Finder.ComparisonCriteria.EqualsIgnoreCase,false);
            SearchParameter parentSearchParameter = new SearchParameter(null, "Parent", parent, Finder.ComparisonCriteria.Equals, false);
            SearchParameter[] searchParameters = new SearchParameter[] { nomeSearchParameter, parentSearchParameter };
            folder = dao.GetByCriteria(searchParameters);

            return folder;
        }

        public static StructureFolder GetByIdFromParent(StructureFolder parent, int id)
        {
            StructureFolder folder = null;
            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(GetCurrentSession());
            SearchParameter nomeSearchParameter = new SearchParameter(null, "ID", id, Finder.ComparisonCriteria.Equals, false);
            SearchParameter parentSearchParameter = new SearchParameter(null, "Parent", parent, Finder.ComparisonCriteria.Equals, false);
            SearchParameter[] searchParameters = new SearchParameter[] { nomeSearchParameter, parentSearchParameter };
            folder = dao.GetByCriteria(searchParameters);

            return folder;
        }

        public static StructureFolder GetCartellaNonCestinataByNome(StructureFolder parent, string key, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(innerSession);
            SearchParameter nomeSearchParameter = new SearchParameter(null, "Nome", key, Finder.ComparisonCriteria.EqualsIgnoreCase, false);
            SearchParameter parentSearchParameter = new SearchParameter(null, "Parent", parent, Finder.ComparisonCriteria.Equals, false);
            SearchParameter nonCestinataSearchParameter = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter[] searchParameters = new SearchParameter[] { nomeSearchParameter, parentSearchParameter, nonCestinataSearchParameter };
            return dao.GetByCriteria(searchParameters);
        }

        public static IList<StructureFolder> GetCartelleCestinateByPath(string path,int networkId)
        {
            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(GetCurrentSession());
            SearchParameter pathSearchParameter = new SearchParameter(null, "Path", path, Finder.ComparisonCriteria.Equals, false);
            SearchParameter cestinataSearchParameter = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Not, false);
            SearchParameter networkSP = new SearchParameter(null, "Network.ID", networkId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter[] searchParameters = new SearchParameter[] { cestinataSearchParameter, pathSearchParameter,networkSP };
            return dao.FindByCriteria(searchParameters);
        }

        public static IList<StructureFolder> FindFoldersBySubTree(string treeStart, int networkId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(innerSession);
            SearchParameter treeStartSP = new SearchParameter(null, "Tree", treeStart, Finder.ComparisonCriteria.LikeStart, false);
            SearchParameter networkSP = new SearchParameter(null, "Network.ID", networkId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter[] searchParameters = new SearchParameter[] { treeStartSP,networkSP };
            return dao.FindByCriteria(searchParameters);
        }

        public static StructureFolder SaveOrUpdateFolder(StructureFolder folder, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(innerSession);

            return dao.SaveOrUpdate(folder);
        }

        //public static StructureFolder SaveOrUpdateFolder(ISession session, StructureFolder folder)
        //{
        //    IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(session);
        //    return dao.SaveOrUpdate(folder);
        //}

        public static FolderLang SaveOrUpdateFolderLang(FolderLang folderLang)
        {
            IGenericDAO<FolderLang> dao = new CriteriaNhibernateDAO<FolderLang>(GetCurrentSession());
            return dao.SaveOrUpdate(folderLang);
        }

        public static void DeleteFolderLang(FolderLang folderLang)
        {
            IGenericDAO<FolderLang> dao = new CriteriaNhibernateDAO<FolderLang>(GetCurrentSession());
            dao.Delete(folderLang);
        }

        public static void DeleteFolder(StructureFolder folder)
        {
            ICollection<User> usersFavorites = UsersBusinessLogic.FindUsersByFolderFavorite(folder);

            foreach (User user in usersFavorites)
            {
                user.FavoritesFolders = user.FavoritesFolders.Where(x => x.ID != folder.ID).ToList();
                UsersBusinessLogic.SaveOrUpdateUser(user);
            }

            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(GetCurrentSession());
            dao.Delete(folder);
        }

        public static void DeleteFolder(ISession session, StructureFolder folder)
        {
            ICollection<User> usersFavorites = UsersBusinessLogic.FindUsersByFolderFavorite(folder);

            foreach (User user in usersFavorites)
            {
                user.FavoritesFolders = user.FavoritesFolders.Where(x => x.ID != folder.ID).ToList();
                UsersBusinessLogic.SaveOrUpdateUser(user);
            }

            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(session);
            dao.Delete(folder);
        }

        public static StructureFolder SaveOrUpdateFolder(StructureFolder folder, int batchSizeForSession)
        {
            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(GetCurrentSession().SetBatchSize(batchSizeForSession));
            return dao.SaveOrUpdate(folder);
        }

        public static int CountSubFolder(StructureFolder folder)
        {
            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(GetCurrentSession());
            SearchParameter SP = new SearchParameter(null, "Parent", folder, Finder.ComparisonCriteria.Equals, false);
            return dao.FindByCriteriaCount(new SearchParameter[] {SP});
        }
        
        
        
        public static IList<StructureFolder> FindRootFoldersByTipo(ICollection<int> tipi, int networkId)
        {
            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(GetCurrentSession());
            SearchParameter tipiSP = new SearchParameter(null, "Tipo", tipi, Finder.ComparisonCriteria.In, false);
            SearchParameter depthSP = new SearchParameter(null, "Depth", 1, Finder.ComparisonCriteria.Equals, false);
            SearchParameter networkSP = new SearchParameter(null, "Network.ID", networkId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter[] searchParameters = new SearchParameter[] { networkSP, depthSP, tipiSP };
            return dao.FindByCriteria(searchParameters);
        }
        public static StructureFolder FindRootFoldersByNetwork(int networkId,ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(innerSession);            

            SearchParameter depthSP = new SearchParameter(null, "Depth", 1, Finder.ComparisonCriteria.Equals, false);
            SearchParameter networkSP = new SearchParameter(null, "Network.ID", networkId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter[] searchParameters = new SearchParameter[] { networkSP, depthSP };
            
            var results = dao.FindByCriteria(searchParameters);
            
            return results.FirstOrDefault();
        }

        public enum SearchFor { ID,Name,Path };

        public static StructureFolder FindFolder(StructureFolder startFolder, string key, SearchFor searchFor)
        {
            StructureFolder foundNode = null;

            switch (searchFor)
            {
                case SearchFor.ID:
                    {
                        foundNode = FindFolderByID(startFolder, key);
                        break;
                    }

                case SearchFor.Name:
                    {
                        foundNode = FindFolderByName(startFolder, key);
                        break;
                    }

                case SearchFor.Path:
                    {
                        foundNode = FindFolderByPath(startFolder, key);
                        break;
                    }
            }

            if (foundNode != null)
                return foundNode;

            IList<StructureFolder> list = GetCurrentSession().CreateCriteria(typeof(StructureFolder)).Add(Expression.Eq("Parent", startFolder)).SetFetchMode("Folders", FetchMode.Join)
                .SetResultTransformer(new DistinctRootEntityResultTransformer())
                .List<StructureFolder>();

            return TraverseFolderTree(list, key, searchFor);

            #region Implementazione 1
            //Non tiene conto delle cartelle contenute nelle sub folder elencate in list
            //switch (searchFor)
            //{ 
            //    case SearchFor.ID:
            //        Parallel.ForEach<StructureFolder>(list, node =>
            //        {
            //            foundNode = FindFolderByID(node, key);
            //        });
            //        break;
            //    case SearchFor.Name:
            //        Parallel.ForEach<StructureFolder>(list, node =>
            //        {
            //            foundNode = FindFolderByName(node, key);
            //        });
            //        break;
            //    case SearchFor.Path:
            //        Parallel.ForEach<StructureFolder>(list, node =>
            //        {
            //            foundNode = FindFolderByPath(node, key);
            //        });
            //        break;
            //    default:
            //        break;
            //}

            //return foundNode;
            #endregion

            #region Implementazione 2 -- Da TESTARE
            //Sembra essere molto lento
            //var tokenSource = new CancellationTokenSource();
            //CancellationToken ct = tokenSource.Token;

           
            //Func<StructureFolder, Task> createTask = null;
            //createTask = n =>
            //{           
            //    Task<StructureFolder> nt = Task.Factory.StartNew<StructureFolder>(() =>
            //    {
            //        if (ct.IsCancellationRequested)
            //        {
            //            ct.ThrowIfCancellationRequested();
            //        }

            //        if (n.Nome == key)
            //        {
            //            //foundNode = n;
            //            tokenSource.Cancel();
            //            return n;
            //        }
            //        return null;
            //    },ct);

            
            //        var nts = (new[] { nt, })
            //            .Concat(n.Folders.Select(cn => createTask(cn as StructureFolder)))
            //            .ToArray();

            //        return Task.Factory.ContinueWhenAll(nts, ts => { }, ct);
            //};

            //createTask(startFolder).Wait();

            //return foundNode;

            #endregion

            #region Implementazione 3

            //Func<StructureFolder, String, StructureFolder> findNode = null; // satisfy recursion re-use
            //findNode = (n, value) =>
            //{
            //    if (n == null) return n;
            //    if (n.Nome == value) return n;

            //    foreach (var subNode in n.Folders.Cast<StructureFolder>())
            //    {
            //        StructureFolder foundNode = findNode(subNode, value);
            //        if (foundNode != null) return foundNode;
            //    }
            //    return null;
            //};

            //return findNode(startFolder, key);
            #endregion

            #region Implementazione 4 - Sembra essere la più performante

            //NetworkFolder result = null;

            //List<NetworkFolder> list = start.Folders.Cast<NetworkFolder>().ToList();

            //CancellationTokenSource cts = new CancellationTokenSource();

            //ParallelOptions po = new ParallelOptions();
            //po.MaxDegreeOfParallelism = 20; // max threads
            //po.CancellationToken = cts.Token;

            //while (result == null && list.Count > 0)
            //{
            //    List<NetworkFolder> tempList = new List<NetworkFolder>();
            //    Parallel.ForEach<NetworkFolder>(list, po, (node, loopState) =>
            //    {
            //        if (node.ID == key || node.Path == key || node.Nome == key)
            //        {
            //            result = node;
            //            loopState.Stop();
            //        }
            //    });
            //    if (result == null)
            //    {
            //        list.ForEach(x => tempList.AddRange(x.Folders.Cast<NetworkFolder>().ToList()));
            //        list = tempList;
            //    }
            //}

            //return result;

            #endregion
        }

        public static StructureFolder FindFolder(StructureFolder startFolder, string key, SearchFor searchFor, ISession session)
        {
            StructureFolder foundNode = null;

            switch (searchFor)
            {
                case SearchFor.ID:
                    {
                        foundNode = FindFolderByID(startFolder, key);
                        break;
                    }

                case SearchFor.Name:
                    {
                        foundNode = FindFolderByName(startFolder, key);
                        break;
                    }

                case SearchFor.Path:
                    {
                        foundNode = FindFolderByPath(startFolder, key);
                        break;
                    }
            }

            if (foundNode != null)
                return foundNode;

            IList<StructureFolder> list = session.CreateCriteria(typeof(StructureFolder)).Add(Expression.Eq("Parent", startFolder)).SetFetchMode("Folders", FetchMode.Join)
                .SetResultTransformer(new DistinctRootEntityResultTransformer())
                .List<StructureFolder>();

            return TraverseFolderTree(list, key, searchFor);

            #region Implementazione 1
            //Non tiene conto delle cartelle contenute nelle sub folder elencate in list
            //switch (searchFor)
            //{ 
            //    case SearchFor.ID:
            //        Parallel.ForEach<StructureFolder>(list, node =>
            //        {
            //            foundNode = FindFolderByID(node, key);
            //        });
            //        break;
            //    case SearchFor.Name:
            //        Parallel.ForEach<StructureFolder>(list, node =>
            //        {
            //            foundNode = FindFolderByName(node, key);
            //        });
            //        break;
            //    case SearchFor.Path:
            //        Parallel.ForEach<StructureFolder>(list, node =>
            //        {
            //            foundNode = FindFolderByPath(node, key);
            //        });
            //        break;
            //    default:
            //        break;
            //}

            //return foundNode;
            #endregion

            #region Implementazione 2 -- Da TESTARE
            //Sembra essere molto lento
            //var tokenSource = new CancellationTokenSource();
            //CancellationToken ct = tokenSource.Token;


            //Func<StructureFolder, Task> createTask = null;
            //createTask = n =>
            //{           
            //    Task<StructureFolder> nt = Task.Factory.StartNew<StructureFolder>(() =>
            //    {
            //        if (ct.IsCancellationRequested)
            //        {
            //            ct.ThrowIfCancellationRequested();
            //        }

            //        if (n.Nome == key)
            //        {
            //            //foundNode = n;
            //            tokenSource.Cancel();
            //            return n;
            //        }
            //        return null;
            //    },ct);


            //        var nts = (new[] { nt, })
            //            .Concat(n.Folders.Select(cn => createTask(cn as StructureFolder)))
            //            .ToArray();

            //        return Task.Factory.ContinueWhenAll(nts, ts => { }, ct);
            //};

            //createTask(startFolder).Wait();

            //return foundNode;

            #endregion

            #region Implementazione 3

            //Func<StructureFolder, String, StructureFolder> findNode = null; // satisfy recursion re-use
            //findNode = (n, value) =>
            //{
            //    if (n == null) return n;
            //    if (n.Nome == value) return n;

            //    foreach (var subNode in n.Folders.Cast<StructureFolder>())
            //    {
            //        StructureFolder foundNode = findNode(subNode, value);
            //        if (foundNode != null) return foundNode;
            //    }
            //    return null;
            //};

            //return findNode(startFolder, key);
            #endregion

            #region Implementazione 4 - Sembra essere la più performante

            //NetworkFolder result = null;

            //List<NetworkFolder> list = start.Folders.Cast<NetworkFolder>().ToList();

            //CancellationTokenSource cts = new CancellationTokenSource();

            //ParallelOptions po = new ParallelOptions();
            //po.MaxDegreeOfParallelism = 20; // max threads
            //po.CancellationToken = cts.Token;

            //while (result == null && list.Count > 0)
            //{
            //    List<NetworkFolder> tempList = new List<NetworkFolder>();
            //    Parallel.ForEach<NetworkFolder>(list, po, (node, loopState) =>
            //    {
            //        if (node.ID == key || node.Path == key || node.Nome == key)
            //        {
            //            result = node;
            //            loopState.Stop();
            //        }
            //    });
            //    if (result == null)
            //    {
            //        list.ForEach(x => tempList.AddRange(x.Folders.Cast<NetworkFolder>().ToList()));
            //        list = tempList;
            //    }
            //}

            //return result;

            #endregion
        }

        private static StructureFolder TraverseFolderTree(IList<StructureFolder> list, string key, SearchFor searchFor)
        {
            StructureFolder foundNode = null;

            foreach (StructureFolder folder in list)
            {
                switch (searchFor)
                {
                    case SearchFor.ID:
                        {
                            foundNode = FindFolderByID(folder, key);
                            break;
                        }

                    case SearchFor.Name:
                        {
                            foundNode = FindFolderByName(folder, key);
                            break;
                        }

                    case SearchFor.Path:
                        {
                            foundNode = FindFolderByPath(folder, key);
                            break;
                        }
                }

                if (foundNode != null)
                    return foundNode;

                StructureFolder result = null;
                if (folder.SubFolders.Count > 0)
                    result = TraverseFolderTree(folder.SubFolders.ToList(), key, searchFor);

                if (result != null)
                    return result;
            }
            return null;
        }

        private static StructureFolder FindFolderByID(StructureFolder folder, string key)
        {
            if (folder.ID == int.Parse(key)) return folder;
            return null;
        }

        private static StructureFolder FindFolderByName(StructureFolder folder, string key)
        {

            if(folder.Nome.ToLower() == key.ToLower())
                return folder;
            return null;
        }

        private static StructureFolder FindFolderByPath(StructureFolder folder, string key)
        {
            if (folder.Path == key) return folder;
            return null;
        }

        public static void TrashFolder(StructureFolder folder)
        {
            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(GetCurrentSession());

            ICollection<StructureFolder> foldersToTrash = FindFoldersBySubTree(folder.Tree,folder.NetworkID);

            foreach (StructureFolder folderToTrash in foldersToTrash)
            {
                folderToTrash.Trash = -(folder.Account.ID);
                dao.SaveOrUpdate(folderToTrash);
            }

            folder.Stato = 2;
            folder.Trash = folder.Account.ID;
            dao.SaveOrUpdate(folder);
        }

        #region Metodi di Folders Users Custom View

        public static FoldersUsersCustomView GetFolderUserCustomViewById(int id)
        {
            IGenericDAO<FoldersUsersCustomView> dao = new CriteriaNhibernateDAO<FoldersUsersCustomView>(GetCurrentSession());
            return dao.GetById(id);
        }

        public static FoldersUsersCustomView GetFolderUserCustomViewByFolderAndUser(StructureFolder folder, int userId)
        {
            IGenericDAO<FoldersUsersCustomView> dao = new CriteriaNhibernateDAO<FoldersUsersCustomView>(GetCurrentSession());
            SearchParameter userIdSearchParameter = new SearchParameter(null, "User.ID", userId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter folderSearchParameter = new SearchParameter(null, "Folder", folder, Finder.ComparisonCriteria.Equals, false);
            SearchParameter[] searchParameters = new SearchParameter[] { userIdSearchParameter, folderSearchParameter };
            
            return dao.GetByCriteria(searchParameters);
        }

        public static FoldersUsersCustomView SaveOrUpdateFolderUserCustomView(FoldersUsersCustomView customView)
        {
            IGenericDAO<FoldersUsersCustomView> dao = new CriteriaNhibernateDAO<FoldersUsersCustomView>(GetCurrentSession());
            return dao.SaveOrUpdate(customView);
        }

        public static void DeleteFolderUserCustomView(FoldersUsersCustomView customView)
        {
            IGenericDAO<FoldersUsersCustomView> dao = new CriteriaNhibernateDAO<FoldersUsersCustomView>(GetCurrentSession());
            dao.Delete(customView);
        }

        #endregion


        #region Metodi di FrontEnd

        public static IList<StructureFolder> FindSubFolderOrdered(int parentId, string orderBy, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(innerSession);

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter parentSP = new SearchParameter(null, "Parent.ID", parentId, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(orderBy, false, new SearchParameter[] { trashSP, parentSP });

        }

        public static StructureFolder GetFolderRecordById(int folderId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(innerSession);

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter idSP = new SearchParameter(null, "ID", folderId, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { trashSP, idSP });

        }

        public static bool IsFolderRestricted(int folderId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(innerSession);

            SearchParameter IdSP = new SearchParameter(null, "ID", folderId, Finder.ComparisonCriteria.Equals, false);
            StructureFolder folder = dao.GetByCriteria(new SearchParameter[] { IdSP });

            if (folder != null)
            {
                SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);
                SearchParameter restrictedSP = new SearchParameter(null, "RestrictedInt", 0, Finder.ComparisonCriteria.Not, false);
                //SearchParameter treeSP = new SearchParameter(null, "Tree", folder.Tree, Finder.ComparisonCriteria.LikeStart, false);
                IList<StructureFolder> results = dao.FindByCriteria(new SearchParameter[] { trashSP, restrictedSP });

                if (results.Count > 0)
                {
                    var filteredResult = from f in results
                                         where folder.Tree.Contains(f.Tree)
                                         select f;

                    return filteredResult.Count() > 0;
                }
            }
            return false;
        }

        public static StructureFolder GetFolderByPathCheckingIfNetworkOrApplication(string folderPath,int networkId,List<string> paths)
        {
            Disjunction dis = new Disjunction();

            //Controllo se questa network contiene una cartella col path che arriva per paramentro
            Conjunction networkCheck = new Conjunction();
            networkCheck.Add(Restrictions.Eq("Network.ID", networkId));
            networkCheck.Add(Restrictions.Like("Path", folderPath));

            //In caso contrario verifico il path sia un path relativo ad una Cartella di Applicazione verticale
            Conjunction applicationCheck = BuildFolderByParentsPathConjunction(folderPath, networkId, paths);
            //Conjunction applicationCheck = new Conjunction();
            //applicationCheck.Add(Restrictions.Eq("NetworkID", networkId));
            //applicationCheck.Add(Restrictions.Gt("HomeType", 0));
            //applicationCheck.Add(Restrictions.Eq("Sistema", 5));

            //Disjunction pathDisjunction = new Disjunction();
            //foreach(string path in paths)
            //    pathDisjunction.Add(Restrictions.Like("Path", path));
            //if (folderPath.Length > 0 && paths.Count > 0)
            //    applicationCheck.Add(pathDisjunction);

            dis.Add(networkCheck);
            if (folderPath.Length > 0 && paths.Count > 0)
                dis.Add(applicationCheck);

            return GetCurrentSession().CreateCriteria(typeof(StructureFolder)).Add(dis).UniqueResult<StructureFolder>();
        }

        public static StructureFolder GetFolderRecordByPath(string path,int networkId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(innerSession);

            SearchParameter networkSP = new SearchParameter(null, "Network.ID", networkId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter pathSP = new SearchParameter(null, "Path", path, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { trashSP, pathSP, networkSP });

        }

        public static StructureFolder GetFolderRecordByNameAndPath(string name, string path, int networkId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();
            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(innerSession);

            SearchParameter networkSP = new SearchParameter(null, "Network.ID", networkId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter pathSP = new SearchParameter(null, "Path", path, Finder.ComparisonCriteria.Equals, false);
            SearchParameter nameSP = new SearchParameter(null, "Nome", name, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { trashSP, pathSP,nameSP, networkSP });

        }

        public static IList<StructureFolder> ListChildsFoldersRecords(string tree, int depth, bool useOR = false)
        {
            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(GetCurrentSession());

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter statoSP = null;
            if (!useOR)
                statoSP = new SearchParameter(null, "Stato", 0, Finder.ComparisonCriteria.Equals, false);
            else
            {
                //serve per recuperare tutti le cartelle con stato a 0 o a 4(menu o sub menu) necessario
                /// per visualizzare le voci di menu nel menu contestuale alla sezione che lo prevede
                SearchParameter statoSPSubMenu = new SearchParameter(null, "Stato", 4, Finder.ComparisonCriteria.Equals, false);
                SearchParameter statoSPMenu = new SearchParameter(null, "Stato", 0, Finder.ComparisonCriteria.Equals, false);

                statoSP = new SearchParameter(null, SearchParameter.BehaviourClause.Or, new SearchParameter[] { statoSPMenu, statoSPSubMenu });
            }

            SearchParameter sistemaSP = new SearchParameter(null, "Sistema", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter depthSP = new SearchParameter(null, "Depth", depth, Finder.ComparisonCriteria.Equals, false);
            SearchParameter treeSP = new SearchParameter(null, "Tree", tree, Finder.ComparisonCriteria.LikeStart, false);
            
            return dao.FindByCriteria(new SearchParameter[] { trashSP, statoSP,sistemaSP,depthSP,treeSP });

        }       

        public static int CountListChildsFoldersRecords(string tree, int depth, bool useOR = false)
        {
            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(GetCurrentSession());

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter statoSP = null;
            if (!useOR)
                statoSP = new SearchParameter(null, "Stato", 0, Finder.ComparisonCriteria.Equals, false);
            else
            {
                //serve per recuperare tutti le cartelle con stato a 0 o a 4(menu o sub menu) necessario
                /// per visualizzare le voci di menu nel menu contestuale alla sezione che lo prevede
                SearchParameter statoSPSubMenu = new SearchParameter(null, "Stato", 4, Finder.ComparisonCriteria.Equals, false);
                SearchParameter statoSPMenu = new SearchParameter(null, "Stato", 0, Finder.ComparisonCriteria.Equals, false);

                statoSP = new SearchParameter(null, SearchParameter.BehaviourClause.Or, new SearchParameter[] { statoSPMenu, statoSPSubMenu });
            }

            SearchParameter sistemaSP = new SearchParameter(null, "Sistema", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter depthSP = new SearchParameter(null, "Depth", depth, Finder.ComparisonCriteria.Equals, false);
            SearchParameter treeSP = new SearchParameter(null, "Tree", tree, Finder.ComparisonCriteria.LikeStart, false);

            return dao.FindByCriteriaCount(new SearchParameter[] { trashSP, statoSP, sistemaSP, depthSP, treeSP });

        }

        public static IList<StructureFolder> ListChildsFoldersRecordsForRoot(string tree, int depth, bool useOR = false)
        {
            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(GetCurrentSession());

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter statoSP = null;
            if (!useOR)
                statoSP = new SearchParameter(null, "Stato", 0, Finder.ComparisonCriteria.Equals, false);
            else
            {
                //serve per recuperare tutti le cartelle con stato a 0 o a 4(menu o sub menu) necessario
                /// per visualizzare le voci di menu nel menu contestuale alla sezione che lo prevede
                SearchParameter statoSPSubMenu = new SearchParameter(null, "Stato", 4, Finder.ComparisonCriteria.Equals, false);
                SearchParameter statoSPMenu = new SearchParameter(null, "Stato", 0, Finder.ComparisonCriteria.Equals, false);

                statoSP = new SearchParameter(null, SearchParameter.BehaviourClause.Or, new SearchParameter[] { statoSPMenu, statoSPSubMenu });
            }

            //SearchParameter sistemaSP = new SearchParameter(null, "Sistema", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter depthSP = new SearchParameter(null, "Depth", depth, Finder.ComparisonCriteria.Equals, false);
            SearchParameter treeSP = new SearchParameter(null, "Tree", tree, Finder.ComparisonCriteria.LikeStart, false);

            IList<StructureFolder> result = dao.FindByCriteria(new SearchParameter[] { trashSP, statoSP, depthSP, treeSP });
            return result.Where(x => x.Sistema == 0 || x.Sistema == 5).ToList();
        }

        public static int CountListChildsFoldersRecordsForRoot(string tree, int depth, bool useOR = false)
        {
            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(GetCurrentSession());

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);
            //SearchParameter statoSP = new SearchParameter(null, "Stato", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter statoSP = null;
            if (!useOR)
                statoSP = new SearchParameter(null, "Stato", 0, Finder.ComparisonCriteria.Equals, false);
            else
            {
                //serve per recuperare tutti le cartelle con stato a 0 o a 4(menu o sub menu) necessario
                /// per visualizzare le voci di menu nel menu contestuale alla sezione che lo prevede
                SearchParameter statoSPSubMenu = new SearchParameter(null, "Stato", 4, Finder.ComparisonCriteria.Equals, false);
                SearchParameter statoSPMenu = new SearchParameter(null, "Stato", 0, Finder.ComparisonCriteria.Equals, false);

                statoSP = new SearchParameter(null, SearchParameter.BehaviourClause.Or, new SearchParameter[] { statoSPMenu, statoSPSubMenu });
            }

            //SearchParameter sistemaSP = new SearchParameter(null, "Sistema", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter depthSP = new SearchParameter(null, "Depth", depth, Finder.ComparisonCriteria.Equals, false);
            SearchParameter treeSP = new SearchParameter(null, "Tree", tree, Finder.ComparisonCriteria.LikeStart, false);

            IList<StructureFolder> result = dao.FindByCriteria(new SearchParameter[] { trashSP, statoSP, depthSP, treeSP });
            return result.Where(x => x.Sistema == 0 || x.Sistema == 5).Count();
        }

        public static IList<StructureFolder> ListFoldersRecordsByPaths(int networkId, string[] paths,string order,bool ascending)
        {

            Conjunction conjunction = new Conjunction();
            conjunction.Add(Restrictions.Eq("Network.ID", networkId));
            conjunction.Add(Restrictions.Eq("Trash", 0));
            conjunction.Add(Restrictions.Lt("Sistema", 2));
            conjunction.Add(Restrictions.Lt("Stato", 2));
            conjunction.Add(Restrictions.Eq("Tipo", 1));

            if (paths.Length > 0)
            {
                Disjunction pathDisjunction = new Disjunction();
                foreach (string path in paths)
                    pathDisjunction.Add(Restrictions.Eq("Path", path));
                conjunction.Add(pathDisjunction);
            }

            return GetCurrentSession().CreateCriteria(typeof(StructureFolder)).SetCacheable(true).SetCacheMode(CacheMode.Normal).Add(conjunction).AddOrder(new Order(order,ascending)).List<StructureFolder>();
        }

        public static IList<StructureFolder> ListAllFolders(int networkId)
        {

            Conjunction conjunction = new Conjunction();
            conjunction.Add(Restrictions.Eq("Network.ID", networkId));
            conjunction.Add(Restrictions.Eq("Trash", 0));
            conjunction.Add(Restrictions.Eq("Sistema", 0));
            conjunction.Add(Restrictions.Le("Stato", 2));
            
            return GetCurrentSession().CreateCriteria(typeof(StructureFolder)).SetCacheable(true).SetCacheMode(CacheMode.Normal).Add(conjunction).List<StructureFolder>();
        }

        public static IList<StructureFolder> ListFoldersRecordsByPaths(string[] paths)
        {

            Conjunction conjunction = new Conjunction();
            
            conjunction.Add(Restrictions.Eq("Trash", 0));
       
            if (paths.Length > 0)
            {
                Disjunction pathDisjunction = new Disjunction();
                foreach (string path in paths)
                    pathDisjunction.Add(Restrictions.Like("Path", path));
                conjunction.Add(pathDisjunction);
            }

            return GetCurrentSession().CreateCriteria(typeof(StructureFolder)).SetCacheable(true).SetCacheMode(CacheMode.Normal).Add(conjunction).List<StructureFolder>();
        }

        public static IList<StructureFolder> FindFoldersByApplication(int applicationId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(innerSession);

            SearchParameter tipoSP = new SearchParameter(null, "Tipo", applicationId, Finder.ComparisonCriteria.Equals, false);

            SearchParameter tipoParentSP = new SearchParameter(null, "Tipo", applicationId, Finder.ComparisonCriteria.Not, false);
            SearchParameter parentSP = new SearchParameter(null, "Parent", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { tipoParentSP });

            return dao.FindByCriteria(new SearchParameter[] { tipoSP, parentSP });
        }

        #endregion

        #region Metodi per le network

        private static Conjunction BuildFolderByParentsPathConjunction(string folderPath, int networkId, List<string> paths)
        {
            //Verifico il path sia un path relativo ad una Cartella di Applicazione verticale
            Conjunction applicationCheck = new Conjunction();
            applicationCheck.Add(Restrictions.Eq("Network.ID", networkId));
            applicationCheck.Add(Restrictions.Gt("HomeType", 0));
            applicationCheck.Add(Restrictions.Eq("Sistema", 5));

            Disjunction pathDisjunction = new Disjunction();
            foreach (string path in paths)
                pathDisjunction.Add(Restrictions.Like("Path", path));
            if (folderPath.Length > 0 && paths.Count > 0)
                applicationCheck.Add(pathDisjunction);

            return applicationCheck;
        }

        public static StructureFolder GetFolderByNetworkAndParentsPath(string folderPath, int networkId, List<string> paths)
        {           
            Conjunction applicationCheck = BuildFolderByParentsPathConjunction(folderPath,networkId,paths);

            return GetCurrentSession().CreateCriteria(typeof(StructureFolder)).Add(applicationCheck).UniqueResult<StructureFolder>();
        }


        #endregion

        #region Metodi per le folder preferite

        public static void DeleteFavorite(User user, int favoriteId)
        {
            user.FavoritesFolders = user.FavoritesFolders.Where(x => x.ID != favoriteId).ToList();
            UsersBusinessLogic.SaveOrUpdateUser(user);
        }

        public static WebControl AllBookmarks(User currentUser)
        {
            WebControl richestePerRete = new WebControl(HtmlTextWriterTag.Div);
            Networks.NetworksData ntdata = new NetCms.Networks.NetworksData();

            foreach (BasicNetwork net in UsersBusinessLogic.FindNetworksByUser(currentUser).Select(x => x.Network))
            {
                if (NetworksManager.GlobalIndexer.GetNetworkByID(net.ID) != null)
                {
                    if (net.CmsStatus == NetworkCmsStates.Enabled)
                    {
                        WebControl ctr = BookmarksByNetwork(net, currentUser);
                        if (ctr != null)
                        {
                            Collapsable col = new NetCms.GUI.Collapsable("Preferiti per il portale " + net.Label);
                            col.AppendControl(ctr);
                            richestePerRete.Controls.Add(col);
                        }
                    }
                }
            }

            string Favorites = "<li class=\"SideToolbarButton\"><a alt=\"Modifica Preferiti\" href=\"preferiti.aspx\"><div style=\"background-image:url(" + NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Pencil) + ")\" ><span>Modifica Preferiti</span></div></a></li>";

            WebControl list = new WebControl(HtmlTextWriterTag.Ul);
            list.CssClass = "favorites";
            list.Controls.Add(new LiteralControl(Favorites));
            richestePerRete.Controls.Add(list);

            return richestePerRete;
        }

        public static WebControl BookmarksByNetwork(BasicNetwork Network, User currentUser)
        {
            ICollection<NetworkFolder> favorites =  UsersBusinessLogic.GetById(currentUser.ID).FavoritesFolders.Where(x=>x.NetworkID == Network.ID).ToList();

            string favoritesString = "";

            if (favorites.Count > 0)
            {
                foreach (NetworkFolder fav in favorites.OrderBy(x => (x as StructureFolder).Label))
                {
                    favoritesString += "<li class=\"SideToolbarButton\"><a alt=\"" + (fav as StructureFolder).Label + " \" href=\"" + Configurations.Paths.AbsoluteSiteManagerRoot + "/" + fav.Network.SystemName + "/cms/default.aspx?folder=" + fav.ID + "\"><div style=\"background-image:url(" + NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Star) + ")\" ><span>" + (fav as StructureFolder).Label + "</span></div></a></li>";
                }
            }
            else return null;

            WebControl list = new WebControl(HtmlTextWriterTag.Ul);
            list.CssClass = "favorites";
            list.Controls.Add(new LiteralControl(favoritesString));
            return list;
        }

        public static WebControl BookmarksEditableByNetwork(BasicStructureNetwork Network, User currentUser)
        {
            currentUser = UsersBusinessLogic.GetById(currentUser.ID);

            G2Core.XhtmlControls.InputField network = new G2Core.XhtmlControls.InputField("n" + Network.ID);
            G2Core.XhtmlControls.InputField favorite = new G2Core.XhtmlControls.InputField("f" + Network.ID);

            if (network.RequestVariable.IsValidInteger && favorite.RequestVariable.IsValidInteger && network.RequestVariable.IntValue == Network.ID)
            {
                FolderBusinessLogic.DeleteFavorite(currentUser, favorite.RequestVariable.IntValue);
            }

            Collapsable collapsable = new Collapsable("Preferiti per il Portale '" + Network.Label + "'");
            collapsable.ID = "fn" + Network.ID;

            ICollection<NetworkFolder> favorites = currentUser.FavoritesFolders.Where(x=>x.NetworkID == Network.ID).ToList();

            favorites = favorites.OrderBy(x => (x as StructureFolder).Label).ToList();

            ICollection<StructureFolder> structureFolders = favorites.ToList().ConvertAll(x => (StructureFolder)x);

            ArTable atable = new ArTable(favorites);

            atable.CssClass = "tab";

            atable.AddColumn(new ArTextColumn("Cartella", "Label"));
            atable.AddColumn(new ArActionColumn("Elimina", ArActionColumn.Icons.Delete, "javascript: " + network.getSetFormValueJScript("'" + Network.ID + "'", false) + ";" + favorite.getSetFormValueConfirmJScript("'{ID}'", "Sicuro di voler eliminare questo preferito?")));

            collapsable.AppendControl(network.Control);
            collapsable.AppendControl(favorite.Control);
            collapsable.AppendControl(atable);
            return collapsable;
        }

        //public static ICollection<Favorites> FindFavoritesByUserAndNetwork(User user, Network network)
        //{
        //    IGenericDAO<Favorites> dao = new CriteriaNhibernateDAO<Favorites>(GetCurrentSession());
        //    SearchParameter userSp = new SearchParameter(null, "User", user, Finder.ComparisonCriteria.Equals, false);

        //    SearchParameter networkSp = new SearchParameter(null, "Network", network, Finder.ComparisonCriteria.Equals, false);
        //    SearchParameter folderSp = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToOne, new SearchParameter[] { networkSp });

        //    return dao.FindByCriteria(new SearchParameter[] { userSp, folderSp }).OrderBy(x => (x.Folder as StructureFolder).Label).ToList();
        //}

        public static ICollection<NetworkFolder> FindFavoritesByUserAndNetwork(User user, Network network)
        {
            user = UsersBusinessLogic.GetById(user.ID);
            return user.FavoritesFolders.Where(x => x.Network.ID == network.ID).ToList();
        }

        #endregion

        public static IList<StructureFolder> GetTrashedFolders(int lang, int networkId, int userId, bool showAllUserTrash = false)
        {
            List<SearchParameter> searchParameters = new List<SearchParameter>();

            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(GetCurrentSession());
            SearchParameter langSP = new SearchParameter(null, "Lang", lang, Finder.ComparisonCriteria.Equals, false);

            SearchParameter folderLangsSP = new SearchParameter(null,"FolderLangs", SearchParameter.RelationTypes.OneToMany, langSP);
            searchParameters.Add(folderLangsSP);

            SearchParameter networkSP = new SearchParameter(null, "Network.ID", networkId, Finder.ComparisonCriteria.Equals, false);
            searchParameters.Add(networkSP);

            if (!showAllUserTrash)
            {
                SearchParameter cestinataSearchParameter = new SearchParameter(null, "Trash", userId, Finder.ComparisonCriteria.Equals, false);
                searchParameters.Add(cestinataSearchParameter);
            }
            else
            {
                SearchParameter cestinataSearchParameter = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Not, false);
                searchParameters.Add(cestinataSearchParameter);
            }

            //SearchParameter[] searchParameters = new SearchParameter[] { cestinataSearchParameter, folderLangsSP, networkSP };            

            return dao.FindByCriteria(searchParameters.ToArray());
        }


        public static ICollection<NetworkFolder> FindNetworkFoldersForGrantsCollection(int networkId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();


            IGenericDAO<NetworkFolder> dao = new CriteriaNhibernateDAO<NetworkFolder>(innerSession);
            SearchParameter hiddenSP = new SearchParameter(null, "Sistema", 2, Finder.ComparisonCriteria.Not, false);
            SearchParameter systemSP = new SearchParameter(null, "Sistema", 1, Finder.ComparisonCriteria.Not, false);
            SearchParameter depthSP = new SearchParameter(null, "Depth", 3, Finder.ComparisonCriteria.LessEquals, false);
            SearchParameter networkSP = new SearchParameter(null, "Network.ID", networkId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter[] searchParameters = new SearchParameter[] { networkSP, depthSP, hiddenSP, systemSP };
            return dao.FindByCriteria(searchParameters);
        }


        public static IList<StructureFolder> FindSubFoldersByParentFolder(int parentId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(innerSession);

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter parentSP = new SearchParameter(null, "Parent.ID", parentId, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[] { trashSP, parentSP });

        }


        public static string TranslateNameOnTheFly(int lang, int networkId, string name, int depth, string parentName)
        {
            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(GetCurrentSession());
            SearchParameter langSP = new SearchParameter(null, "Lang", lang, Finder.ComparisonCriteria.Equals, false);
            SearchParameter labelSP = new SearchParameter(null, "Label", name.Replace("/", "").Replace("-", " "), Finder.ComparisonCriteria.EqualsIgnoreCase, false);

            SearchParameter folderLangsSP = new SearchParameter(null, "FolderLangs", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { labelSP, langSP });

            SearchParameter depthSP = new SearchParameter(null, "Depth", depth, Finder.ComparisonCriteria.LessEquals, false);
            SearchParameter networkSP = new SearchParameter(null, "Network.ID", networkId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter[] searchParameters = new SearchParameter[] { folderLangsSP, networkSP, depthSP };
            IList<StructureFolder> folders = dao.FindByCriteria(searchParameters);
            if (folders != null && folders.Count > 0)
            {
                if (folders.Count == 1)
                    return "/" + folders.First().Nome;
                else if (folders.Count(x => x.Parent.Nome == parentName) > 0)
                    return "/" + folders.First(x => x.Parent.Nome == parentName).Nome;
            }                
            return (name.Length > 0 && !name.StartsWith("/")? "/" : "") + name;

        }

        public static string TranslatePathOnTheFly(int lang, int networkId, string path)
        {
            string pathTranslated = "";
                       
            string[] PathArray = path.Split('/');
            string previousTranslated = "";
            for (int i = 1; i < PathArray.Length; i++)
            {
                if (PathArray[i].Length > 0) {
                    previousTranslated = FolderBusinessLogic.TranslateNameOnTheFly(lang, networkId, PathArray[i], i + 1, previousTranslated.Replace("/", ""));
                    pathTranslated += previousTranslated;
                }
            }

            return pathTranslated;
        }
    }
}
