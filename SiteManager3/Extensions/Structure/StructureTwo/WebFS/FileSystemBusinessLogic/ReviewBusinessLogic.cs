﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NetCms.Structure.WebFS.Reviews;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using NetCms.DBSessionProvider;

namespace NetCms.Structure.WebFS.FileSystemBusinessLogic
{
    public static class ReviewBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static IList<Review> FindReviewsByDocId(int IdDoc, List<string> sortBy, bool descending)
        {
            IGenericDAO<Review> dao = new CriteriaNhibernateDAO<Review>(GetCurrentSession());
            SearchParameter searchDocIdParameter = new SearchParameter(null,"ID",IdDoc, Finder.ComparisonCriteria.Equals,false);
            SearchParameter searchDocLangParameter = new SearchParameter(null,"Document", SearchParameter.RelationTypes.OneToMany,searchDocIdParameter);
            SearchParameter searchDocParameter = new SearchParameter(null, "DocLang", SearchParameter.RelationTypes.OneToMany, searchDocLangParameter);
            return dao.FindByCriteria(sortBy, descending, new SearchParameter[] { searchDocParameter });
        }

        public static Review GetReviewById(int Id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Review> dao = new CriteriaNhibernateDAO<Review>(innerSession);
            return dao.GetById(Id);
        }

        public static Review SaveOrUpdateReview(Review review, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Review> dao = new CriteriaNhibernateDAO<Review>(innerSession);
            return dao.SaveOrUpdate(review);
        }

        public static ReviewLog SaveOrUpdateReviewLog(ReviewLog reviewLog, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<ReviewLog> dao = new CriteriaNhibernateDAO<ReviewLog>(session);
            return dao.SaveOrUpdate(reviewLog);
        }

        public static ReviewNotify SaveOrUpdateReviewNotify(ReviewNotify reviewNotify, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<ReviewNotify> dao = new CriteriaNhibernateDAO<ReviewNotify>(session);
            return dao.SaveOrUpdate(reviewNotify);
        }

        public static void UpdateStatoRevisioneNotificaByRevisioneAndLog(Review review, ReviewLog log, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<ReviewNotify> dao = new CriteriaNhibernateDAO<ReviewNotify>(session);

            SearchParameter reviewSP = new SearchParameter(null,"Review.ID",review.ID, Finder.ComparisonCriteria.Equals, false);
            SearchParameter logSP = new SearchParameter(null,"ReviewLog.ID", log.ID, Finder.ComparisonCriteria.Equals,false);

            var reviewNotificationsToUpdate = dao.FindByCriteria(new SearchParameter[] { reviewSP, logSP });
            if(reviewNotificationsToUpdate != null)
            {
                foreach (ReviewNotify rn in reviewNotificationsToUpdate)
                {
                    rn.Stato = 1;
                    SaveOrUpdateReviewNotify(rn, session);
                }
            }
        }

        public static void UpdatePublishedRevisioniByDocLang(DocumentLang docLang, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<Review> dao = new CriteriaNhibernateDAO<Review>(session);

            SearchParameter pubSP = new SearchParameter(null,"Published",1, Finder.ComparisonCriteria.Equals,false);
            SearchParameter docLangSP = new SearchParameter(null,"DocLang.ID", docLang.ID, Finder.ComparisonCriteria.Equals, false); 

            var revisioni = dao.FindByCriteria(new SearchParameter[]{pubSP,docLangSP});

            foreach (Review revisione in revisioni)
            {
                revisione.Published = 0;
                SaveOrUpdateReview(revisione,session);
            }
        }

        public static ICollection<ReviewNotify> FindNotifyByUserId(int userId, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<ReviewNotify> dao = new CriteriaNhibernateDAO<ReviewNotify>(session);

            SearchParameter statoSP = new SearchParameter(null, "Stato", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter logSP = new SearchParameter(null, "Destinatario", userId, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[] { statoSP, logSP });
        }
    }
}
