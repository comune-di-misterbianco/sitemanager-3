﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using GenericDAO.DAO;
using NHibernate.Transform;
using NHibernate.Criterion;
using NetService.Utility.RecordsFinder;
using NetCms.DBSessionProvider;
using NetCms.Networks.WebFS;
using System.IO;
using System.Web;
using NetCms.Structure.Applications;
using NetCms.Structure.GUI.FileSystemNavigator.Model;

namespace NetCms.Structure.WebFS.FileSystemBusinessLogic
{
    public static class DocumentBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static Document GetById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(innerSession);
            return dao.GetById(id);
        }

        //funzione che torna il numero di occorrenze di un file filtrato per applicazione
        public static int GetNumberOfInstanceFile(string nome, string estensione, int id_application, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(innerSession);
            SearchParameter SP_Application = new SearchParameter(null, "Application", id_application, Finder.ComparisonCriteria.Equals, false);
            SearchParameter SP_Name = new SearchParameter(null, "PhysicalName", nome + "%." + estensione, Finder.ComparisonCriteria.Like, false);
            return dao.FindByCriteriaCount(new SearchParameter[] { SP_Application, SP_Name });
        }

        public static DocumentLang GetDocLangById(int id)
        {
            IGenericDAO<DocumentLang> dao = new CriteriaNhibernateDAO<DocumentLang>(GetCurrentSession());
            return dao.GetById(id);
        }

        public static DocumentLang GetDocLangById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<DocumentLang> dao = new CriteriaNhibernateDAO<DocumentLang>(innerSession);
            return dao.GetById(id);
        }

        public static DocContent GetDocContentById(int id)
        {
            IGenericDAO<DocContent> dao = new CriteriaNhibernateDAO<DocContent>(GetCurrentSession());
            return dao.GetById(id);
        }

        public static DocContent GetDocContentById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<DocContent> dao = new CriteriaNhibernateDAO<DocContent>(innerSession);
            return dao.GetById(id);
        }

        public static DocumentLang GetDocLangByIdDoc(int idDoc)
        {
            IGenericDAO<DocumentLang> dao = new CriteriaNhibernateDAO<DocumentLang>(GetCurrentSession());
            SearchParameter docSP = new SearchParameter(null, "Document.ID", idDoc, Finder.ComparisonCriteria.Equals, false);
            return dao.GetByCriteria(new SearchParameter[] { docSP });
        }

        public static Document SaveOrUpdateDocument(Document doc, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(innerSession);
            return dao.SaveOrUpdate(doc);
        }

        public static DocumentLang SaveOrUpdateDocumentLang(DocumentLang docLang, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<DocumentLang> dao = new CriteriaNhibernateDAO<DocumentLang>(innerSession);
            return dao.SaveOrUpdate(docLang);
        }

        public static void DeleteDocument(Document doc, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            if (doc.DocumentoRiferito != null)
            {
                FileInfo file = new FileInfo(HttpContext.Current.Server.MapPath(NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + doc.Folder.Path + "/" + doc.PhysicalName));
                if (file.Exists)
                    file.Delete();
            }

            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(session);
            dao.Delete(doc);
        }

        /// <summary>
        /// Deprecata
        /// </summary>
        /// <param name="treeStart"></param>
        /// <returns></returns>
        public static IList<Document> FindDocumentsByFolderTree(string treeStart)
        {
            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(GetCurrentSession());
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter treeStartSP = new SearchParameter(null, "Tree", treeStart, Finder.ComparisonCriteria.LikeStart, false);
            SearchParameter folderSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { treeStartSP });

            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, folderSP };
            return dao.FindByCriteria(new List<string>() { "Modify", "ID" }, true, searchParameters);
        }

        public static IList<Document> FindDocumentsByFolderTree(string treeStart, int recordLimit)
        {
            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(GetCurrentSession());
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);
         
            SearchParameter spStato = new SearchParameter("null", "Stato", 2, Finder.ComparisonCriteria.Not, false);            

            SearchParameter treeStartSP = new SearchParameter(null, "Tree", treeStart, Finder.ComparisonCriteria.LikeStart, false);
            SearchParameter folderSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { treeStartSP });

            SearchParameter appAllegatiSP = new SearchParameter(null, "Application", 4, Finder.ComparisonCriteria.Not, false);
            SearchParameter appLinkSP = new SearchParameter(null, "Application", 9, Finder.ComparisonCriteria.Not, false);

            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, spStato, folderSP, appAllegatiSP, appLinkSP };

            Dictionary<string, bool> order = new Dictionary<string, bool>();
            order.Add("Modify", true);
            order.Add("ID", true);

            //return dao.FindByCriteria(new List<string>() { "Modify", "ID" }, true, searchParameters);
            return dao.FindByCriteria(0, recordLimit, order, searchParameters, false);
        }

        public static IList<Document> GetDocumentsByIdFolderAndTree(int folderId, string treeStart, int recordLimit, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(session);
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);
       
            SearchParameter spStato = new SearchParameter("null", "Stato", 2, Finder.ComparisonCriteria.Not, false);
         
            SearchParameter folderIdSP = new SearchParameter(null, "ID", folderId, Finder.ComparisonCriteria.Equals, false);            
            SearchParameter treeStartSP = new SearchParameter(null, "Tree", treeStart, Finder.ComparisonCriteria.LikeStart, false);
            SearchParameter folderSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { folderIdSP, treeStartSP });

            SearchParameter appAllegatiSP = new SearchParameter(null, "Application", 4, Finder.ComparisonCriteria.Not, false);
            SearchParameter appLinkSP = new SearchParameter(null, "Application", 9, Finder.ComparisonCriteria.Not, false);           

            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, spStato, folderSP, appAllegatiSP, appLinkSP };

            Dictionary<string, bool> order = new Dictionary<string, bool>();
            order.Add("Modify", true);
            order.Add("ID", true);

            //return dao.FindByCriteria(new List<string>() { "Modify", "ID" }, true, searchParameters);
            return dao.FindByCriteria(0, recordLimit, order, searchParameters, false);
        }

        public static IList<Document> FindDocumentsByFolderID(int idFolder)
        {
            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(GetCurrentSession());
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter treeStartSP = new SearchParameter(null, "ID", idFolder, Finder.ComparisonCriteria.Equals, false);
            SearchParameter folderSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { treeStartSP });

            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, folderSP };
            return dao.FindByCriteria(searchParameters);
        }




        public static Document GetDocumentByPhysicalName(StructureFolder startFolder, string name)
        {
            Document foundNode = null;
            if (((startFolder.Documents) as ICollection<Document>).Where(x => x.PhysicalName == name).Count() > 0)
                foundNode = ((startFolder.Documents) as ICollection<Document>).First(x => x.PhysicalName == name) as Document;
            return foundNode;
        }

        //DA TESTARE SE FUNZIONA
        public static IList<Document> FindFolderDocumentsForSearch(int defaultLang, string tree, ICollection<int> tipi, StructureFolder.StandardSearchFields parameters)
        {
            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(GetCurrentSession());

            Dictionary<string, bool> orders = new Dictionary<string, bool>();
            if (parameters.OrderBy > 0)
                switch (parameters.OrderBy)
                {
                    case 0:
                        orders.Add("Label", false);//DocLangs.Label
                        //sql += " ORDER BY Label_DocLang";
                        break;
                    case 1:
                        orders.Add("Folder.Path", false);
                        orders.Add("Label", false);
                        //sql += " ORDER BY Path_Folder,Label_DocLang";
                        break;
                    case 2:
                        orders.Add("Application", false);
                        orders.Add("Label", false);//DocLangs.Label
                        //sql += " ORDER BY Application_Doc,Label_DocLang";
                        break;
                    case 3:
                        orders.Add("Data", false);
                        orders.Add("Label", false);//DocLangs.Label
                        //sql += " ORDER BY Data_Doc,Label_DocLang";
                        break;
                    case 4:
                        orders.Add("Data", true);
                        orders.Add("Label", false);//DocLangs.Label
                        //sql += " ORDER BY Data_Doc DESC,Label_DocLang";
                        break;
                    default:
                        orders.Add("Label", false);//DocLangs.Label
                        //sql += " ORDER BY Label_DocLang";
                        break;
                }
            else
                orders.Add("Label", false);//DocLangs.Label
            //sql += " ORDER BY Label_DocLang";
            
            List<SearchParameter> documentParameters = new List<SearchParameter>();

            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);
            documentParameters.Add(nonCestinatiSP);

            SearchParameter tipiSP = new SearchParameter(null, "Tipo", tipi, Finder.ComparisonCriteria.In, false);
            //documentParameters.Add(tipiSP);

            SearchParameter systemSP = new SearchParameter(null, "Sistema", 2, Finder.ComparisonCriteria.LessThan, false);//System
            SearchParameter treeStartSP = new SearchParameter(null, "Tree", tree, Finder.ComparisonCriteria.LikeStart, false);
            SearchParameter folderSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { treeStartSP, systemSP, tipiSP });
            documentParameters.Add(folderSP);


            List<SearchParameter> LangParameters = new List<SearchParameter>();
            SearchParameter idLangSP = new SearchParameter(null, "Lang", defaultLang, Finder.ComparisonCriteria.Equals, false);
            LangParameters.Add(idLangSP);


            if (parameters.Titolo != null && parameters.Titolo.Length > 0)
            {
                SearchParameter labelSP = new SearchParameter(null, "Label", parameters.Titolo, Finder.ComparisonCriteria.Like, false);
                LangParameters.Add(labelSP);
                //sql += " AND (Name_Doc LIKE '%" + Parameters.Titolo + "%' OR Label_DocLang LIKE '%" + Parameters.Titolo + "%')";
            }
            //SearchParameter langSP = new SearchParameter(null, "DocLangs", SearchParameter.RelationTypes.OneToMany, LangParameters.ToArray());
            SearchParameter langSP = new SearchParameter(null,"DocLangs",SearchParameter.RelationTypes.OneToMany,orders,LangParameters.ToArray());
            documentParameters.Add(langSP);

            if (parameters.Tipo > 0)
            {
                SearchParameter applicationSP = new SearchParameter(null, "Application", parameters.Tipo, Finder.ComparisonCriteria.Equals, false);
                documentParameters.Add(applicationSP);
                //sql += " AND Application_Doc  = " + Parameters.Tipo + "";
            }
            if (parameters.Data != null && parameters.Data.Length > 5)
            {
                DateTime SearchDate;

                if (DateTime.TryParse(parameters.Data, out SearchDate))
                    switch (parameters.TipoData)
                    {
                        case 0:
                            SearchParameter dataEqSP = new SearchParameter(null, "Data", SearchDate, Finder.ComparisonCriteria.Equals, false);
                            documentParameters.Add(dataEqSP);
                            //sql += " AND Data_Doc  = " + PageData.Conn.FilterDateTime(SearchDate.ToString()) + "";

                            break;
                        case 1:
                            SearchParameter dataLeSP = new SearchParameter(null, "Data", SearchDate, Finder.ComparisonCriteria.LessEquals, false);
                            documentParameters.Add(dataLeSP);
                            //sql += " AND Data_Doc  <= " + PageData.Conn.FilterDateTime(SearchDate.ToString()) + "";

                            break;
                        case 2:
                            SearchParameter dataGeSP = new SearchParameter(null, "Data", SearchDate, Finder.ComparisonCriteria.GreaterEquals, false);
                            documentParameters.Add(dataGeSP);
                            //sql += " AND Data_Doc  >= " + PageData.Conn.FilterDateTime(SearchDate.ToString()) + "";

                            break;
                    }
            }
            //Dictionary<string, bool> orders = new Dictionary<string, bool>();
            //if (parameters.OrderBy > 0)
            //    switch (parameters.OrderBy)
            //    {
            //        case 0:
            //            orders.Add("DocLangs.Label", false);
            //            //sql += " ORDER BY Label_DocLang";
            //            break;
            //        case 1:
            //            orders.Add("Folder.Path", false);
            //            orders.Add("Label", false);
            //            //sql += " ORDER BY Path_Folder,Label_DocLang";
            //            break;
            //        case 2:
            //            orders.Add("Application", false);
            //            orders.Add("DocLangs.Label", false);
            //            //sql += " ORDER BY Application_Doc,Label_DocLang";
            //            break;
            //        case 3:
            //            orders.Add("Data", false);
            //            orders.Add("DocLangs.Label", false);
            //            //sql += " ORDER BY Data_Doc,Label_DocLang";
            //            break;
            //        case 4:
            //            orders.Add("Data", true);
            //            orders.Add("DocLangs.Label", false);
            //            //sql += " ORDER BY Data_Doc DESC,Label_DocLang";
            //            break;
            //        default:
            //            orders.Add("DocLangs.Label", false);
            //            //sql += " ORDER BY Label_DocLang";
            //            break;
            //    }
            //else
            //    orders.Add("DocLangs.Label", false);
            ////sql += " ORDER BY Label_DocLang";


            //SearchParameter networkSP = new SearchParameter(null, "NetworkID", networkId, Finder.ComparisonCriteria.Equals, false);
            //SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP,tipiSP,folderSP,langSP };

            return dao.FindByCriteria(documentParameters.ToArray());//orders, 
        }

        public static int FindFolderDocumentsForSearchCount(int defaultLang, string tree, ICollection<int> tipi, StructureFolder.StandardSearchFields parameters)
        {
            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(GetCurrentSession());

            Dictionary<string, bool> orders = new Dictionary<string, bool>();
            if (parameters.OrderBy > 0)
                switch (parameters.OrderBy)
                {
                    case 0:
                        orders.Add("Label", false);//DocLangs.Label
                        //sql += " ORDER BY Label_DocLang";
                        break;
                    case 1:
                        orders.Add("Folder.Path", false);
                        orders.Add("Label", false);
                        //sql += " ORDER BY Path_Folder,Label_DocLang";
                        break;
                    case 2:
                        orders.Add("Application", false);
                        orders.Add("Label", false);//DocLangs.Label
                        //sql += " ORDER BY Application_Doc,Label_DocLang";
                        break;
                    case 3:
                        orders.Add("Data", false);
                        orders.Add("Label", false);//DocLangs.Label
                        //sql += " ORDER BY Data_Doc,Label_DocLang";
                        break;
                    case 4:
                        orders.Add("Data", true);
                        orders.Add("Label", false);//DocLangs.Label
                        //sql += " ORDER BY Data_Doc DESC,Label_DocLang";
                        break;
                    default:
                        orders.Add("Label", false);//DocLangs.Label
                        //sql += " ORDER BY Label_DocLang";
                        break;
                }
            else
                orders.Add("Label", false);//DocLangs.Label
            //sql += " ORDER BY Label_DocLang";

            List<SearchParameter> documentParameters = new List<SearchParameter>();

            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);
            documentParameters.Add(nonCestinatiSP);

            SearchParameter tipiSP = new SearchParameter(null, "Tipo", tipi, Finder.ComparisonCriteria.In, false);
            //documentParameters.Add(tipiSP);

            SearchParameter systemSP = new SearchParameter(null, "Sistema", 2, Finder.ComparisonCriteria.LessThan, false);//System
            SearchParameter treeStartSP = new SearchParameter(null, "Tree", tree, Finder.ComparisonCriteria.LikeStart, false);
            SearchParameter folderSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { treeStartSP, systemSP, tipiSP });
            documentParameters.Add(folderSP);


            List<SearchParameter> LangParameters = new List<SearchParameter>();
            SearchParameter idLangSP = new SearchParameter(null, "Lang", defaultLang, Finder.ComparisonCriteria.Equals, false);
            LangParameters.Add(idLangSP);


            if (parameters.Titolo != null && parameters.Titolo.Length > 0)
            {
                SearchParameter labelSP = new SearchParameter(null, "Label", parameters.Titolo, Finder.ComparisonCriteria.Like, false);
                LangParameters.Add(labelSP);
                //sql += " AND (Name_Doc LIKE '%" + Parameters.Titolo + "%' OR Label_DocLang LIKE '%" + Parameters.Titolo + "%')";
            }
            //SearchParameter langSP = new SearchParameter(null, "DocLangs", SearchParameter.RelationTypes.OneToMany, LangParameters.ToArray());
            SearchParameter langSP = new SearchParameter(null, "DocLangs", SearchParameter.RelationTypes.OneToMany, orders, LangParameters.ToArray());
            documentParameters.Add(langSP);

            if (parameters.Tipo > 0)
            {
                SearchParameter applicationSP = new SearchParameter(null, "Application", parameters.Tipo, Finder.ComparisonCriteria.Equals, false);
                documentParameters.Add(applicationSP);
                //sql += " AND Application_Doc  = " + Parameters.Tipo + "";
            }
            if (parameters.Data != null && parameters.Data.Length > 5)
            {
                DateTime SearchDate;

                if (DateTime.TryParse(parameters.Data, out SearchDate))
                    switch (parameters.TipoData)
                    {
                        case 0:
                            SearchParameter dataEqSP = new SearchParameter(null, "Data", SearchDate, Finder.ComparisonCriteria.Equals, false);
                            documentParameters.Add(dataEqSP);
                            //sql += " AND Data_Doc  = " + PageData.Conn.FilterDateTime(SearchDate.ToString()) + "";

                            break;
                        case 1:
                            SearchParameter dataLeSP = new SearchParameter(null, "Data", SearchDate, Finder.ComparisonCriteria.LessEquals, false);
                            documentParameters.Add(dataLeSP);
                            //sql += " AND Data_Doc  <= " + PageData.Conn.FilterDateTime(SearchDate.ToString()) + "";

                            break;
                        case 2:
                            SearchParameter dataGeSP = new SearchParameter(null, "Data", SearchDate, Finder.ComparisonCriteria.GreaterEquals, false);
                            documentParameters.Add(dataGeSP);
                            //sql += " AND Data_Doc  >= " + PageData.Conn.FilterDateTime(SearchDate.ToString()) + "";

                            break;
                    }
            }

            return dao.FindByCriteriaCount(documentParameters.ToArray());//orders, 
        }

        //DA TESTARE SE FUNZIONA
        public static IList<Document> FindFolderDocumentsForSearchPaged(int page, int rpp, int defaultLang, string tree, ICollection<int> tipi, StructureFolder.StandardSearchFields parameters)
        {
            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(GetCurrentSession());

            Dictionary<string, bool> orders = new Dictionary<string, bool>();
            if (parameters.OrderBy > 0)
                switch (parameters.OrderBy)
                {
                    case 0:
                        orders.Add("Label", false);//DocLangs.Label
                        //sql += " ORDER BY Label_DocLang";
                        break;
                    case 1:
                        orders.Add("Folder.Path", false);
                        orders.Add("Label", false);
                        //sql += " ORDER BY Path_Folder,Label_DocLang";
                        break;
                    case 2:
                        orders.Add("Application", false);
                        orders.Add("Label", false);//DocLangs.Label
                        //sql += " ORDER BY Application_Doc,Label_DocLang";
                        break;
                    case 3:
                        orders.Add("Data", false);
                        orders.Add("Label", false);//DocLangs.Label
                        //sql += " ORDER BY Data_Doc,Label_DocLang";
                        break;
                    case 4:
                        orders.Add("Data", true);
                        orders.Add("Label", false);//DocLangs.Label
                        //sql += " ORDER BY Data_Doc DESC,Label_DocLang";
                        break;
                    default:
                        orders.Add("Label", false);//DocLangs.Label
                        //sql += " ORDER BY Label_DocLang";
                        break;
                }
            else
                orders.Add("Label", false);//DocLangs.Label
            //sql += " ORDER BY Label_DocLang";

            List<SearchParameter> documentParameters = new List<SearchParameter>();

            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);
            documentParameters.Add(nonCestinatiSP);

            SearchParameter tipiSP = new SearchParameter(null, "Tipo", tipi, Finder.ComparisonCriteria.In, false);
            //documentParameters.Add(tipiSP);

            SearchParameter systemSP = new SearchParameter(null, "Sistema", 2, Finder.ComparisonCriteria.LessThan, false);//System
            SearchParameter treeStartSP = new SearchParameter(null, "Tree", tree, Finder.ComparisonCriteria.LikeStart, false);
            SearchParameter folderSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { treeStartSP, systemSP, tipiSP });
            documentParameters.Add(folderSP);


            List<SearchParameter> LangParameters = new List<SearchParameter>();
            SearchParameter idLangSP = new SearchParameter(null, "Lang", defaultLang, Finder.ComparisonCriteria.Equals, false);
            LangParameters.Add(idLangSP);


            if (parameters.Titolo != null && parameters.Titolo.Length > 0)
            {
                SearchParameter labelSP = new SearchParameter(null, "Label", parameters.Titolo, Finder.ComparisonCriteria.Like, false);
                LangParameters.Add(labelSP);
                //sql += " AND (Name_Doc LIKE '%" + Parameters.Titolo + "%' OR Label_DocLang LIKE '%" + Parameters.Titolo + "%')";
            }
            //SearchParameter langSP = new SearchParameter(null, "DocLangs", SearchParameter.RelationTypes.OneToMany, LangParameters.ToArray());
            SearchParameter langSP = new SearchParameter(null, "DocLangs", SearchParameter.RelationTypes.OneToMany, orders, LangParameters.ToArray());
            documentParameters.Add(langSP);

            if (parameters.Tipo > 0)
            {
                SearchParameter applicationSP = new SearchParameter(null, "Application", parameters.Tipo, Finder.ComparisonCriteria.Equals, false);
                documentParameters.Add(applicationSP);
                //sql += " AND Application_Doc  = " + Parameters.Tipo + "";
            }
            if (parameters.Data != null && parameters.Data.Length > 5)
            {
                DateTime SearchDate;

                if (DateTime.TryParse(parameters.Data, out SearchDate))
                    switch (parameters.TipoData)
                    {
                        case 0:
                            SearchParameter dataEqSP = new SearchParameter(null, "Data", SearchDate, Finder.ComparisonCriteria.Equals, false);
                            documentParameters.Add(dataEqSP);
                            //sql += " AND Data_Doc  = " + PageData.Conn.FilterDateTime(SearchDate.ToString()) + "";

                            break;
                        case 1:
                            SearchParameter dataLeSP = new SearchParameter(null, "Data", SearchDate, Finder.ComparisonCriteria.LessEquals, false);
                            documentParameters.Add(dataLeSP);
                            //sql += " AND Data_Doc  <= " + PageData.Conn.FilterDateTime(SearchDate.ToString()) + "";

                            break;
                        case 2:
                            SearchParameter dataGeSP = new SearchParameter(null, "Data", SearchDate, Finder.ComparisonCriteria.GreaterEquals, false);
                            documentParameters.Add(dataGeSP);
                            //sql += " AND Data_Doc  >= " + PageData.Conn.FilterDateTime(SearchDate.ToString()) + "";

                            break;
                    }
            }
            
            return dao.FindByCriteria((page - 1)*rpp,rpp,documentParameters.ToArray());//orders, 
        }

        public enum SearchFor { ID, LabelDocLang, PhysicalName };

        public static Document FindDocument(StructureFolder startFolder, string key, SearchFor searchFor)
        {
            Document foundNode = null;

            if (startFolder.Documents != null)
            {
                switch (searchFor)
                {
                    case SearchFor.ID:
                        {
                            var nodes = startFolder.Documents.Where(x => x.ID == key.ToInt());
                            if (nodes.Count() > 0)
                                foundNode = nodes.ElementAt(0) as Document;
                            break;
                        }

                    case SearchFor.LabelDocLang:
                        {
                            var nodes = startFolder.Documents.Where(x => x.Nome == key);
                            if (nodes.Count() > 0)
                                foundNode = nodes.ElementAt(0) as Document;
                            break;
                        }

                    case SearchFor.PhysicalName:
                        {
                            ICollection<Document> nodes = new List<Document>();
                            foreach (NetworkDocument nd in startFolder.Documents)
                            {
                                Document doc = nd as Document;
                                if (doc.PhysicalName == key)
                                    nodes.Add(doc);
                            }

                            if (nodes.Count() > 0)
                                foundNode = nodes.ElementAt(0) as Document;
                            break;
                        }
                }
            }

            if (foundNode != null)
                return foundNode;

            IList<StructureFolder> list = GetCurrentSession().CreateCriteria(typeof(StructureFolder)).Add(Restrictions.Eq("Parent", startFolder)).SetFetchMode("Folders", FetchMode.Join)
                .SetResultTransformer(new DistinctRootEntityResultTransformer())
                .List<StructureFolder>();

            return TraverseFolderTree(list, key, searchFor);
        }

        private static Document TraverseFolderTree(IList<StructureFolder> list, string key, SearchFor searchFor)
        {
            Document foundNode = null;

            foreach (StructureFolder folder in list)
            {
                foreach (Document document in folder.Documents)
                {
                    switch (searchFor)
                    {
                        case SearchFor.ID:
                            {
                                var nodes = folder.Documents.Where(x => x.ID == key.ToInt());
                                if (nodes.Count() > 0)
                                    foundNode = nodes.ElementAt(0) as Document;
                                break;
                            }

                        case SearchFor.LabelDocLang:
                            {
                                var nodes = folder.Documents.Where(x => x.Nome == key);
                                if (nodes.Count() > 0)
                                    foundNode = nodes.ElementAt(0) as Document;
                                break;
                            }

                        case SearchFor.PhysicalName:
                            {
                                ICollection<Document> nodes = new List<Document>();
                                foreach (NetworkDocument nd in folder.Documents)
                                {
                                    Document doc = nd as Document;
                                    if (doc.PhysicalName == key)
                                        nodes.Add(doc);
                                }
                                if (nodes.Count() > 0)
                                    foundNode = nodes.ElementAt(0) as Document;
                                break;
                            }
                    }

                    if (foundNode != null)
                        return foundNode;
                }

                Document result = null;
                if (folder.SubFolders.Count > 0)
                    result = TraverseFolderTree(folder.SubFolders.ToList(), key, searchFor);

                if (result != null)
                    return result;
            }
            return null;
        }

        public static Document GetDocumentByPhysicalNameAndFolder(string name, StructureFolder folder, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            Disjunction dis = new Disjunction();
            dis.Add(Restrictions.Eq("PhysicalName", name));
            dis.Add(Restrictions.Eq("PhysicalName", name + ".aspx"));
            return innerSession.CreateCriteria(typeof(Document)).Add(Restrictions.Eq("Folder", folder)).Add(dis).UniqueResult<Document>();

        }

        public static Document GetByNameAndLink(string docName, string folderName, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(innerSession);
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter docNameSP = new SearchParameter(null, "PhysicalName", docName, Finder.ComparisonCriteria.Equals, false);

            SearchParameter NameSP = new SearchParameter(null, "Nome", "%" + folderName + "%", Finder.ComparisonCriteria.Like, false);
            //SearchParameter folderNameSP = new SearchParameter(null, "Folder.Path", "%" + folderName + "%", Finder.ComparisonCriteria.Like, false);
            SearchParameter folderSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { NameSP });

            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, docNameSP, folderSP };
            IList<Document> docsToReturn = dao.FindByCriteria(searchParameters);
            return docsToReturn.FirstOrDefault();
        }


        public static IList<Document> GetDocumentByName(string docName, int netId, string path, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(innerSession);
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter pathSP = new SearchParameter(null, "Path", path, Finder.ComparisonCriteria.Equals, false);
            SearchParameter networkSP = new SearchParameter(null, "Network.ID", netId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter folderSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { networkSP, pathSP });

            SearchParameter docNameSP = new SearchParameter(null, "PhysicalName", docName, Finder.ComparisonCriteria.Equals, false);

            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, folderSP, docNameSP };
            return dao.FindByCriteria(searchParameters);
        }

        #region Metodi di FrontEnd

        public static Document GetDocumentRecordById(int documentID)
        {
            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(GetCurrentSession());
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter statoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.Not, false);

            SearchParameter idSP = new SearchParameter(null, "ID", documentID, Finder.ComparisonCriteria.Equals, false);

            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, statoSP, idSP };
            IList<Document> docs = dao.FindByCriteria(searchParameters);

            IList<Document> filteredDocs =docs.Where(x => x.Application == 4 || x.DocLangs.Where(c => (c.Content != null && c.Content.ID > 0)).Count() > 0).ToList();

            if (filteredDocs.Count > 0)
                return filteredDocs.First();
            else
                return null;
        }

        public static bool IsDocumentRestricted(int documentID)
        {
            IGenericDAO<StructureFolder> dao = new CriteriaNhibernateDAO<StructureFolder>(GetCurrentSession());

            SearchParameter docIdSP = new SearchParameter(null, "ID", documentID, Finder.ComparisonCriteria.Equals, false);
            SearchParameter docSP = new SearchParameter(null, "Documents", SearchParameter.RelationTypes.OneToMany, docIdSP);
            StructureFolder folder = dao.GetByCriteria(new SearchParameter[] { docSP });

            SearchParameter trashSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter restrictedSP = new SearchParameter(null, "RestrictedInt", 0, Finder.ComparisonCriteria.Not, false);
            //SearchParameter treeSP = new SearchParameter(null, "Tree", folder.Tree, Finder.ComparisonCriteria.LikeStart, false);
            IList<StructureFolder> results = dao.FindByCriteria(new SearchParameter[] { trashSP, restrictedSP }).ToList();

            if (results.Count > 0)
            {
                var filteredResult = from f in results
                                     where folder.Tree.Contains(f.Tree)
                                     select f;

                return filteredResult.Count() > 0;
            }
            return false;
        }

        public static List<Document> FindDocumentRecordByFolderOrderedByLabel(int folderID, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(innerSession);
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter statoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.Not, false);

            SearchParameter folderSP = new SearchParameter(null, "Folder.ID", folderID, Finder.ComparisonCriteria.Equals, false);

            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, statoSP, folderSP };
            IList<Document> docs = dao.FindByCriteria(searchParameters);

            IList<Document> filteredDocs = docs.Where(x => x.Application == 4 || x.DocLangs.Where(c => (c.Content != null && c.Content.ID > 0)).Count() > 0).ToList();

            if (filteredDocs.Count > 0)
                return filteredDocs.OrderBy(x => x.PhysicalName).ToList();
            else
                return null;
        }

        public static Document GetDocumentRecordByFolderAndName(int folderId, string nomeDoc)
        {
            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(GetCurrentSession());
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter statoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.Not, false);

            SearchParameter nomeSP = new SearchParameter(null, "PhysicalName", nomeDoc, Finder.ComparisonCriteria.EqualsIgnoreCase, false);

            SearchParameter folderSP = new SearchParameter(null, "Folder.ID", folderId, Finder.ComparisonCriteria.Equals, false);

            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, statoSP, folderSP, nomeSP };
            IList<Document> docs = dao.FindByCriteria(searchParameters);

            IList<Document> filteredDocs = docs.Where(x => x.Application == 4 || x.DocLangs.Where(c => (c.Content != null && c.Content.ID > 0)).Count() > 0).ToList();

            if (filteredDocs.Count > 0)
                return filteredDocs.First();
            else
                return null;
        }

        public static Document GetDocumentRecordByFolderAndName(int folderId, string nomeDoc, ISession session)
        {
            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(session);

            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter statoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.Not, false);

            SearchParameter nomeSP = new SearchParameter(null, "PhysicalName", nomeDoc, Finder.ComparisonCriteria.EqualsIgnoreCase, false);

            SearchParameter folderSP = new SearchParameter(null, "Folder.ID", folderId, Finder.ComparisonCriteria.Equals, false);

            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, statoSP, folderSP, nomeSP };

            IList<Document> docs = dao.FindByCriteria(searchParameters);

            IList<Document> filteredDocs = docs.Where(x => x.Application == 4 || x.DocLangs.Where(c => (c.Content != null && c.Content.ID > 0)).Count() > 0).ToList();

            if (filteredDocs.Count > 0)
                return filteredDocs.First();
            else
                return null;
        }

        public static Document GetDocumentReachableRecord(int idDoc, string path)
        {
            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(GetCurrentSession());
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter statoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.LessThan, false);

            SearchParameter idSP = new SearchParameter(null, "ID", idDoc, Finder.ComparisonCriteria.Equals, false);

            SearchParameter pathSP = new SearchParameter(null, "Path", path, Finder.ComparisonCriteria.Equals, false);
            SearchParameter folderSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { pathSP });

            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, statoSP, folderSP, idSP };
            IList<Document> docs = dao.FindByCriteria(searchParameters);

            IList<Document> filteredDocs = docs.Where(x => x.Application == 4 || x.DocLangs.Where(c => (c.Content != null && c.Content.ID > 0)).Count() > 0).ToList();

            if (filteredDocs.Count > 0)
                return filteredDocs.First();
            else
                return null;
        }

        public static IList<Document> ListChildsDocumentsRecords(int idFolder)
        {
            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(GetCurrentSession());
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter statoSP = new SearchParameter(null, "Stato", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter idFolderSP = new SearchParameter(null, "Folder.ID", idFolder, Finder.ComparisonCriteria.Equals, false);

            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, statoSP, idFolderSP };
            IList<Document> docs = dao.FindByCriteria(searchParameters);

            return docs.Where(x => x.Application == 4 || x.DocLangs.Where(c => (c.Content != null && c.Content.ID > 0)).Count() > 0).ToList();

        }

        public static int CountDocumentsByFolder(StructureFolder folder, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(innerSession);

            SearchParameter folderSP = new SearchParameter(null, "Folder", folder, Finder.ComparisonCriteria.Equals, false);

            SearchParameter[] searchParameters = new SearchParameter[] { folderSP };
            return dao.FindByCriteriaCount(searchParameters);

        }
        public static int CountDocumentsByFolder(int idFolder, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(innerSession);

            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter idFolderSP = new SearchParameter(null, "Folder.ID", idFolder, Finder.ComparisonCriteria.Equals, false);

            SearchParameter addDocSP = new SearchParameter(null, "Hide", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, idFolderSP, addDocSP };
            return dao.FindByCriteriaCount(searchParameters);

        }

        public static int CountListChildsDocumentsRecords(int idFolder)
        {
            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(GetCurrentSession());
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter statoSP = new SearchParameter(null, "Stato", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter idFolderSP = new SearchParameter(null, "Folder.ID", idFolder, Finder.ComparisonCriteria.Equals, false);

            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, statoSP, idFolderSP };
            IList<Document> docs = dao.FindByCriteria(searchParameters);

            return docs.Where(x => x.Application == 4 || x.DocLangs.Where(c => (c.Content != null && c.Content.ID > 0)).Count() > 0).Count();

        }

        public static Document GetDocumentRecordByPathNetworkAndName(string path, string nomeDoc, int networkId)
        {
            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(GetCurrentSession());
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter statoSP = new SearchParameter(null, "Stato", 2, Finder.ComparisonCriteria.LessThan, false);

            SearchParameter nomeSP = new SearchParameter(null, "PhysicalName", nomeDoc, Finder.ComparisonCriteria.Like, false);

            SearchParameter pathSP = new SearchParameter(null, "Path", path, Finder.ComparisonCriteria.Equals, false);

            SearchParameter networkSP = new SearchParameter(null, "Network.ID", networkId, Finder.ComparisonCriteria.Equals, false);

            SearchParameter folderSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { networkSP, pathSP });

            SearchParameter nascostoSP = new SearchParameter(null, "Hide", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, statoSP, folderSP, nomeSP, nascostoSP };

            IList<Document> docs = dao.FindByCriteria(searchParameters);

            IList<Document> filteredDocs = docs.Where(x => (x.Application == 4 || x.DocLangs.Where(c => (c.Content != null && c.Content.ID > 0)).Count() > 0) && ((x.Folder as StructureFolder).Tipo == 1 || (x.Folder as StructureFolder).Tipo == 2)).ToList();

            if (filteredDocs.Count > 0)
                return filteredDocs.First();
            else
                return null;
        }

        #endregion


        public static IList<Document> GetTrashedDocuments(int networkId, int userId, bool showAllUserTrash = false)
        {
            List<SearchParameter> searchParameters = new List<SearchParameter>();

            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(GetCurrentSession());
            if (!showAllUserTrash)
            {
                SearchParameter cestinatoSearchParameter = new SearchParameter(null, "Trash", userId, Finder.ComparisonCriteria.Equals, false);
                searchParameters.Add(cestinatoSearchParameter);
            }
            else
            {
                SearchParameter cestinatoSearchParameter = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Not, false);
                searchParameters.Add(cestinatoSearchParameter);
            }


            SearchParameter networkSP = new SearchParameter(null, "Network.ID", networkId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter folderSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { networkSP });
            searchParameters.Add(folderSP);

            //SearchParameter[] searchParameters = new SearchParameter[] { cestinatoSearchParameter, folderSP };

            return dao.FindByCriteria(searchParameters.ToArray());
        }


        public static int CountDocumentsRecordsPublishedByFolder(int idFolder)
        {
            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(GetCurrentSession());
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter idFolderSP = new SearchParameter(null, "Folder.ID", idFolder, Finder.ComparisonCriteria.Equals, false);

            SearchParameter labelSP = new SearchParameter(null, "Label", "Pagina Indice", Finder.ComparisonCriteria.Not, false);

            SearchParameter pubblicataSP = new SearchParameter(null, "Published", 1, Finder.ComparisonCriteria.Equals, false);

            SearchParameter RevSP = new SearchParameter(null, "Revisioni", SearchParameter.RelationTypes.OneToMany, pubblicataSP);
            
            SearchParameter langSP = new SearchParameter(null, "DocLangs", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { labelSP, RevSP });


            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, idFolderSP, langSP };
            //Aggiunta la distinct in quanto con l'attivazione di più lingue duplicava i risultati a seconda del numero di doclangs
            return dao.FindByCriteriaCount(searchParameters, true);

        }

       
        // TODO: Verificare il comportamento in modalità multilingua
        public static IList<Document> GetDocumentsPagedByFolder(int idFolder, NetCms.Structure.WebFS.ViewOrders order, int page, int rpp, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(innerSession);
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter idFolderSP = new SearchParameter(null, "Folder.ID", idFolder, Finder.ComparisonCriteria.Equals, false);

            SearchParameter addDocSP = new SearchParameter(null, "Hide", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter[] searchParameters = new SearchParameter[] { nonCestinatiSP, idFolderSP, addDocSP };

            bool descending = false;
            string orderCollection = null;
            string orderProperty = "Create";
            switch (order)
            {
                case NetCms.Structure.WebFS.ViewOrders.Label: orderCollection = "DocLangs"; orderProperty = "Label"; break;
                case NetCms.Structure.WebFS.ViewOrders.CreationDate: orderProperty = "Create"; break;
                case NetCms.Structure.WebFS.ViewOrders.ModifyDate: orderProperty = "Modify"; break;
                case ViewOrders.LabelDesc: orderCollection = "DocLangs"; orderProperty = "Label"; descending = true; break;
                case ViewOrders.CreationDateDesc: orderProperty = "Create"; descending = true; break;
                default: orderProperty = "Create"; break;
            }
            if (orderCollection == null)
                return dao.FindByCriteria(page,rpp,orderProperty,descending,searchParameters);
            //Aggiunta la distinct in quanto con l'attivazione di più lingue duplicava i risultati a seconda del numero di doclangs
            return dao.FindByCriteria(page, rpp, orderCollection, orderProperty, descending, searchParameters, true);

        }

        public static IList<Document> GetDocumentsPagedByFolder2(int idFolder, NetCms.Structure.WebFS.ViewOrders order, int page, int rpp, TipoSelettore tipoFiltro = TipoSelettore.All, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(innerSession);

            SearchParameter[] searchParameters; 
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter idFolderSP = new SearchParameter(null, "Folder.ID", idFolder, Finder.ComparisonCriteria.Equals, false);

            SearchParameter addDocSP = new SearchParameter(null, "Hide", 0, Finder.ComparisonCriteria.Equals, false);



            if (tipoFiltro != TipoSelettore.All)
            {
                List<string> Extensions = new List<string>();
                switch (tipoFiltro)
                {
                     case TipoSelettore.Images:
                        Extensions = NetCms.Structure.GUI.FileSystemNavigator.Controller.FileManagerController.ImageExtensions;
                        break;
                    case TipoSelettore.Flash:
                        Extensions = NetCms.Structure.GUI.FileSystemNavigator.Controller.FileManagerController.FlashExtensions;
                        break;
                    case TipoSelettore.Video:
                        Extensions = NetCms.Structure.GUI.FileSystemNavigator.Controller.FileManagerController.VideoExtensions;
                        break;                    
                }
                
                List<SearchParameter> spExt = new List<SearchParameter>();
                SearchParameter docFilter;

                foreach (string ext in Extensions)
                {
                    docFilter = new SearchParameter(null, "PhysicalName", ext, Finder.ComparisonCriteria.LikeEnd, false);
                    spExt.Add(docFilter);
                }
                
                SearchParameter orExt = new SearchParameter(null, SearchParameter.BehaviourClause.Or, spExt.ToArray());

                searchParameters = new SearchParameter[] { nonCestinatiSP, idFolderSP, addDocSP, orExt };
            }
            else
            {
                searchParameters = new SearchParameter[] { nonCestinatiSP, idFolderSP, addDocSP };
            }

            

            bool descending = false;
            string orderCollection = null;
            string orderProperty = "Create";
           
            switch (order)
            {
                case NetCms.Structure.WebFS.ViewOrders.Label: orderCollection = "DocLangs"; orderProperty = "Label"; break;
                case NetCms.Structure.WebFS.ViewOrders.CreationDate: orderProperty = "Create"; break;
                case NetCms.Structure.WebFS.ViewOrders.ModifyDate: orderProperty = "Modify"; break;
                case ViewOrders.LabelDesc: orderCollection = "DocLangs"; orderProperty = "Label"; descending = true; break;
                case ViewOrders.CreationDateDesc: orderProperty = "Create"; descending = true; break;
                default: orderProperty = "Create"; break;
            }

            if (orderCollection == null)
                return dao.FindByCriteria((page - 1) * rpp, rpp, orderProperty, descending, searchParameters);

            //Aggiunta la distinct in quanto con l'attivazione di più lingue duplicava i risultati a seconda del numero di doclangs
            return dao.FindByCriteria((page-1)*rpp, rpp, orderCollection, orderProperty, descending, searchParameters, true);

        }

        public static int GetDocumentsPagedByFolder2Count(int idFolder, TipoSelettore tipoFiltro = TipoSelettore.All, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(innerSession);

            SearchParameter[] searchParameters;
            SearchParameter nonCestinatiSP = new SearchParameter(null, "Trash", 0, Finder.ComparisonCriteria.Equals, false);

            SearchParameter idFolderSP = new SearchParameter(null, "Folder.ID", idFolder, Finder.ComparisonCriteria.Equals, false);

            SearchParameter addDocSP = new SearchParameter(null, "Hide", 0, Finder.ComparisonCriteria.Equals, false);



            if (tipoFiltro != TipoSelettore.All)
            {
                List<string> Extensions = new List<string>();
                switch (tipoFiltro)
                {
                    case TipoSelettore.Images:
                        Extensions = NetCms.Structure.GUI.FileSystemNavigator.Controller.FileManagerController.ImageExtensions;
                        break;
                    case TipoSelettore.Flash:
                        Extensions = NetCms.Structure.GUI.FileSystemNavigator.Controller.FileManagerController.FlashExtensions;
                        break;
                    case TipoSelettore.Video:
                        Extensions = NetCms.Structure.GUI.FileSystemNavigator.Controller.FileManagerController.VideoExtensions;
                        break;
                }

                List<SearchParameter> spExt = new List<SearchParameter>();
                SearchParameter docFilter;

                foreach (string ext in Extensions)
                {
                    docFilter = new SearchParameter(null, "PhysicalName", ext, Finder.ComparisonCriteria.LikeEnd, false);
                    spExt.Add(docFilter);
                }

                SearchParameter orExt = new SearchParameter(null, SearchParameter.BehaviourClause.Or, spExt.ToArray());

                searchParameters = new SearchParameter[] { nonCestinatiSP, idFolderSP, addDocSP, orExt };
            }
            else
            {
                searchParameters = new SearchParameter[] { nonCestinatiSP, idFolderSP, addDocSP };
            }

            //Aggiunta la distinct in quanto con l'attivazione di più lingue duplicava i risultati a seconda del numero di doclangs
            return dao.FindByCriteriaCount(searchParameters, true);
        }


        public static string TranslateNameOnTheFly(int lang, int networkId, string name, string folderPath)
        {
            string ext = "";
            string[] exts = name.ToLower().Split('.');
            if (exts.Length > 1)
                ext = exts[exts.Length - 1];
            

            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(GetCurrentSession());
            SearchParameter langSP = new SearchParameter(null, "Lang", lang, Finder.ComparisonCriteria.Equals, false);
            SearchParameter labelSP = new SearchParameter(null, "Label", name.Replace("/", "").Replace("-", " ").Replace("." + ext,""), Finder.ComparisonCriteria.EqualsIgnoreCase, false);

            SearchParameter DocLangsSP = new SearchParameter(null, "DocLangs", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { labelSP, langSP });

            SearchParameter pathSP = new SearchParameter(null, "Path", folderPath, Finder.ComparisonCriteria.Equals, false);

            SearchParameter networkSP = new SearchParameter(null, "Network.ID", networkId, Finder.ComparisonCriteria.Equals, false);

            SearchParameter folderSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { pathSP, networkSP });

            //SearchParameter networkSP = new SearchParameter(null, "Network.ID", networkId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter[] searchParameters = new SearchParameter[] { DocLangsSP, folderSP };
            Document document = dao.GetByCriteria(searchParameters);
            if (document != null)
                return document.PhysicalName;
            return name;

        }


        public static string TranslatePageName(int lang, int networkId, string name, string folderPath)
        {
            string ext = "";
            string[] exts = name.ToLower().Split('.');
            if (exts.Length > 1)
                ext = exts[exts.Length - 1];


            IGenericDAO<Document> dao = new CriteriaNhibernateDAO<Document>(GetCurrentSession());
            //SearchParameter langSP = new SearchParameter(null, "Lang", lang, Finder.ComparisonCriteria.Equals, false);
            SearchParameter labelSP = new SearchParameter(null, "Label", name.Replace("/", "").Replace("-", " ").Replace("." + ext, ""), Finder.ComparisonCriteria.EqualsIgnoreCase, false);

            SearchParameter DocLangsSP = new SearchParameter(null, "DocLangs", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { labelSP/*, langSP */});

            SearchParameter pathSP = new SearchParameter(null, "Path", folderPath, Finder.ComparisonCriteria.Equals, false);

            SearchParameter networkSP = new SearchParameter(null, "Network.ID", networkId, Finder.ComparisonCriteria.Equals, false);

            SearchParameter folderSP = new SearchParameter(null, "Folder", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { pathSP, networkSP });

            //SearchParameter networkSP = new SearchParameter(null, "Network.ID", networkId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter[] searchParameters = new SearchParameter[] { DocLangsSP, folderSP };
            Document document = dao.GetByCriteria(searchParameters);
            if (document != null && document.DocLangs.Count(x=>x.Lang == lang) > 0)
                return document.TranslateName(lang,1);
            return name;

        }

    }
}
