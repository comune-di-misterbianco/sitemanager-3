﻿DROP PROCEDURE IF EXISTS `sp_doc_grant_value_by_user`;

CREATE DEFINER = 'cmsdbuser'@'%' PROCEDURE `sp_doc_grant_value_by_user`(
        IN `in_criterio` INTEGER,
        IN `in_user` INTEGER,
        IN `in_doc` INTEGER,
        IN `in_network_id` INTEGER,
        OUT `out_value` INTEGER
    )
    DETERMINISTIC
    READS SQL DATA
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN

CALL sp_dhuh(in_criterio, in_user, in_doc,in_network_id, true, out_value);

END;