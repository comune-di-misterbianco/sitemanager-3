﻿DROP PROCEDURE IF EXISTS `sp_cms_vertical_grant_value_by_user`;

CREATE DEFINER = 'cmsdbuser'@'%' PROCEDURE `sp_cms_vertical_grant_value_by_user`(
        IN `in_criterio` INTEGER,
        IN `in_user` INTEGER,
        IN `in_network` INTEGER,
        OUT `out_value` INTEGER
    )
    DETERMINISTIC
    READS SQL DATA
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  
DECLARE done INTEGER DEFAULT 0;

DECLARE grantValueUsers INTEGER DEFAULT 0;
DECLARE grantValueGroups INTEGER DEFAULT 0;

DECLARE newValue INTEGER DEFAULT 0;

DECLARE tree VARCHAR(255);
DECLARE cursorUserGroups CURSOR FOR 	
										SELECT groups.Tree_Group 
                                        FROM usersgroups 
                                        INNER JOIN groups ON groups.id_Group = usersgroups.Group_UserGroup
								      	WHERE usersgroups.`User_UserGroup` = in_user;

DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN SET done = 1; END;

IF ISNULL(in_network) THEN
	SET in_network = 0;
END IF;

/* Verifico se l'utente ha il permesso direttamente impostato per l'applicazione. */
SELECT object_profiles_criteria.`Value_ObjectProfileCriteria` INTO grantValueUsers
FROM users u
LEFT JOIN (externalapp_profiles INNER JOIN `object_profiles` ON object_profiles.`id_ObjectProfile` = externalapp_profiles.`id_ExternalAppzProfile`) ON u.Profile_User = object_profiles.`Profile_ObjectProfile`
LEFT JOIN (externalapp_grants INNER JOIN object_profiles_criteria ON externalapp_grants.`id_ExternalAppGrant` = id_ObjectProfileCriteria) ON id_ExternalAppzProfile = ExternalAppProfile_ObjectProfileCriteria
WHERE externalapp_grants.Criteria_ObjectProfileCriteria = in_criterio
AND u.id_User = in_user
AND in_network = externalapp_profiles.`Network_ExternalAppzProfile`
AND object_profiles_criteria.`Value_ObjectProfileCriteria` > 0;

/* Se l'utente non ha il permesso impostato, procedo a scansionare i gruppi a cui è associato, alla ricerca di un consenti. */
IF grantValueUsers = 0 OR ISNULL(grantValueUsers) THEN

  SET done = 0;
	
  OPEN cursorUserGroups;

  ciclo:LOOP
    FETCH cursorUserGroups INTO tree;
    IF done THEN
    	CLOSE cursorUserGroups;
        LEAVE ciclo;
    END IF;
    
   	
    	CALL sp_cms_vertical_grant_value_by_group(in_criterio, tree, in_network, newValue);
        
    	IF newValue > 0 THEN
			SET grantValueGroups = newValue;
            /* Esco dal ciclo al primo consenti trovato, altrimenti continuo a cercare nei gruppi. */
            IF newValue = 1 THEN
            	SET done = 1;
            END IF;
  		END IF;
		
  END LOOP;

	SET out_value = grantValueGroups;
ELSE
	SET out_value = grantValueUsers;
END IF;

END;