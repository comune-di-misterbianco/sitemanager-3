﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using System.IO;
using System.Xml;
using NetCms.Structure.Applications;

namespace NetCms.Installation
{
    public class CmsApplicationsControl : WebControl
    {
        private int PopupBoxesCount = 0;

        public enum RenderModes
        {
            Installer, Monitoring
        }
        public RenderModes RenderMode
        {
            get
            {
                return _RenderMode;
            }
            private set
            {
                _RenderMode = value;
            }
        }
        private RenderModes _RenderMode; 
        public CmsApplicationsControl(RenderModes renderMode)
            : base(HtmlTextWriterTag.Div)
        {
            this.RenderMode = renderMode;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Collapsable cmsapps = new Collapsable("Applicazioni di Portale");
            cmsapps.AppendControl(ListView());

            this.Controls.Add(cmsapps);
        }

        private WebControl ListView()
        {
            WebControl value = new WebControl(HtmlTextWriterTag.Div);

            value.Controls.Add(InstallControl);
            value.Controls.Add(UninstallControl);
            value.Controls.Add(StateControl);
            value.Controls.Add(RepairControl);

            Table table = new Table();
            table.CssClass = "tab";
            table.CellPadding = 0;
            table.CellSpacing = 0;

            TableHeaderCell hcell;
            TableHeaderRow hrow = new TableHeaderRow();
            table.Controls.Add(hrow);

            if (NetCms.Configurations.Debug.GenericEnabled)
            {
                hrow.Controls.Add(BuildHeaderCell("ID", 1));
            }

            hrow.Controls.Add(BuildHeaderCell("Nome", 1));
            
            hrow.Controls.Add(BuildHeaderCell("Tipo", 1));

            if (NetCms.Configurations.Debug.GenericEnabled)
                hrow.Controls.Add(BuildHeaderCell("Key", 1));

            hrow.Controls.Add(BuildHeaderCell("Versione", 1));

            hrow.Controls.Add(BuildHeaderCell("Stato Installazione", 2));
            if (RenderMode == RenderModes.Installer)
                hrow.Controls.Add(BuildHeaderCell("&nbsp;", 1));
            
            bool alternate = false;

            foreach (Application application in ApplicationsPool.Assemblies)
            {
                TableRow row = new TableRow();
                if (alternate) row.CssClass = "alternate";
                table.Controls.Add(row);
                TableCell cell = new TableCell();

                if (NetCms.Configurations.Debug.GenericEnabled)
                {
                    cell = new TableCell();
                    cell.Text = application.ID.ToString();
                    row.Controls.Add(cell);
                }

                cell = new TableCell();
                cell.Text = application.Label;
                row.Controls.Add(cell);

                cell = new TableCell();
                cell.Text = application.IsSystemApplication ? "Di Sistema" : "Estensione";
                row.Controls.Add(cell);

                if (NetCms.Configurations.Debug.GenericEnabled)
                {
                    cell = new TableCell();
                    cell.Text = application.SystemName;
                    row.Controls.Add(cell);
                }

                cell = new TableCell();
                cell.Text = application.Assembly.GetName().Version.ToString();
                row.Controls.Add(cell);

                row.Controls.Add(BuildCell_InstallState(application));
                row.Controls.Add(BuildCell_Status(application));
                if (RenderMode == RenderModes.Installer)
                    row.Controls.Add(BuildCell_Actions(application));
                
                alternate = !alternate;

            }
            value.Controls.Add(table);
            return (value);
        }

        private TableCell BuildHeaderCell(string text, int colSpan)
        {
            TableHeaderCell hcell = new TableHeaderCell();
            hcell.Text = text;
            hcell.ColumnSpan = colSpan;
            return hcell;
        }

        private TableCell BuildCell(string text)
        {
            TableCell cell = new TableCell();

            return cell;
        }
        private TableCell BuildCell_InstallState(Application application)
        {
            TableCell cell = new TableCell();

            cell.Text = application.InstallState == Application.InstallStates.NotInstalled ? "Non Installata" : "Installata";

            return cell;
        }
        private TableCell BuildCell_Status(Application application)
        {
            TableCell cell = new TableCell();
            if (application.InstallState == Application.InstallStates.NotInstalled) cell.Text = "&nbsp;";
            else
            {
                bool ok = application.ApplicationValidator.Validate();

                string checks = "";
                if (ok)
                {
                    cell.Text = "Funzionante";
                    cell.CssClass = "action ok";
                }
                else
                {

                    cell.Text = "<strong class=\"red\">Guasta</strong>";
                    cell.CssClass = "action alert";
                }
                checks += "<tr>";
                checks += "<th>Tipo Verifica</th><th>Descrizione</th><th>Errore</th><th>Risultato</th></li>";
                checks += "</tr>";
                foreach (var check in application.ApplicationValidator.ValidationResults)
                {
                    string classe = "";
                    string message = "";
                    string status = "";

                    switch (check.IsOk)
                    {
                        case NetCms.Structure.Applications.Validation.ApplicationValidatorResult.ResultsTypes.Ok:
                            status = "Ok";
                            message = "";
                            classe = "";
                            break;
                        case NetCms.Structure.Applications.Validation.ApplicationValidatorResult.ResultsTypes.Error:
                            status = "Error";
                            message = check.Message;
                            classe = "class=\"red\"";
                            break;
                        case NetCms.Structure.Applications.Validation.ApplicationValidatorResult.ResultsTypes.Warning:
                            status = "Warning";
                            message = check.Message;
                            classe = "";
                            break;
                    }

                    checks += "<tr " + classe + ">";
                    checks += "<td>" + check.RepairTypeLabel + " </td><td>" + check.Name + " </td><td>" + message + "</td><td>" + status + "</td></li>";
                    checks += "</tr>";
                }

                cell.Text = @"<a href=""#TB_inline?height=600&width=800&inlineId=FaultDetailsCms" + PopupBoxesCount + @""" title=""Stato dell'Applicazione '" + application.Label + @"'"" class=""thickbox"" ><span>" + cell.Text + "</span></a>";
                cell.Text += @"
                                <div style=""display:none;"">
                                <div id=""FaultDetailsCms" + PopupBoxesCount + @""">

                                <table border=""0"" cellpadding=""0"" cellspacing=""0"" class=""tab"">" + checks + @"</table>
                                </div></div>
                             ";
                PopupBoxesCount++;
            }


            return cell;
        }
        private TableCell BuildCell_Actions(Application application)
        {
            TableCell cell = new TableCell();

            if (application.InstallState == Application.InstallStates.NotInstalled)
            {
                string confirm = "Sei sicuro di voler installare l\\\'applicazione \\\'{0}\\\'?".FormatByParameters(application.Label);
                cell.Text = "<a href=\"javascript: setFormValueConfirm(" + application.ID + ",'install',1,'{0}');\"><span>Installa</span></a>".FormatByParameters(confirm);
                cell.CssClass = "action add";
            }
            else
            {

                if (application.ApplicationValidator.IsValid)
                {
                    if (!application.IsSystemApplication && NetCms.Configurations.Debug.ApplicationsInstallationEnabled)
                    {
                        cell.CssClass = "action delete";
                        string confirm = "Sei sicuro di voler rimuovere l\\\'applicazione \\\'{0}\\\'?".FormatByParameters(application.Label);
                        cell.Text = "<a href=\"javascript: setFormValueConfirm(" + application.ID + ",'uninstall',1,'{0}');\"><span>Rimuovi</span></a>".FormatByParameters(confirm);
                    }
                    else cell.Text = "&nbsp;";
                }
                else
                {
                    string confirm = "Sei sicuro di voler riparare l\\\'applicazione \\\'{0}\\\'?".FormatByParameters(application.Label);
                    cell.Text = "<a href=\"javascript: setFormValueConfirm(" + application.ID + ",'repair',1,'{0}');\"><span>Ripara</span></a>".FormatByParameters(confirm);
                    cell.CssClass = "action edit";
                }
            }

            return cell;
        }

        private HtmlGenericControl InstallControl
        {
            get
            {
                G2Core.XhtmlControls.InputField install = new G2Core.XhtmlControls.InputField("install");
                if (install.RequestVariable.IsValidInteger)//&& applicationsPool.NotInstalledAssemblies.Contains(install.RequestVariable.StringValue))
                {
                    
                    NetCms.Structure.Applications.Application app = ApplicationsPool.NonActiveAssemblies[install.RequestVariable.StringValue];

                    NetCms.Users.User utente= NetCms.Users.AccountManager.CurrentAccount;
                    string userId = utente.ID.ToString();
                    string userName = utente.UserName;

                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta provando ad installare l'applicazione di portale " + app.Label +"." ,userId, userName);
                    
                    bool done = app.Installer.Install();
                    if (done)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha installato correttamente l'applicazione di portale " + app.Label + ".", userId, userName);
                        NetCms.GUI.PopupBox.AddMessage("L'applicazione " + app.Label + " è stata installata correttamente");
                        ApplicationsPool.NonActiveAssemblies.Remove(app.ID.ToString());
                        ApplicationsPool.ActiveAssemblies.Add(app);
                        G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
                        cache.RemoveAll("");
                    }
                    else
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito ad installare l'applicazione di portale " + app.Label + ".", userId, userName);
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile installare l'applicazione " + app.Label);
                        NetCms.GUI.PopupBox.AddMessage(app.Installer.Errors);
                    }
                }
                return install.Control;
            }
        }
        private HtmlGenericControl StateControl
        {
            get
            {
                G2Core.XhtmlControls.InputField state = new G2Core.XhtmlControls.InputField("state");
                if (state.RequestVariable.IsValidInteger)//&& applicationsPool.NotInstalledAssemblies.Contains(install.RequestVariable.StringValue))
                {
                    NetCms.Structure.Applications.Application app;
                    if (ApplicationsPool.ActiveAssemblies.Contains(state.RequestVariable.StringValue))
                        app = ApplicationsPool.ActiveAssemblies[state.RequestVariable.StringValue];
                    else
                        app = ApplicationsPool.NonActiveAssemblies[state.RequestVariable.StringValue];

                    NetCms.Users.User utente = NetCms.Users.AccountManager.CurrentAccount;
                    string userId = utente.ID.ToString();
                    string userName = utente.UserName;

                    if (app.Enabled)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta provando a disabilitare l'applicazione di portale " + app.Label + ".", userId, userName);
                    }
                    else 
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta provando ad abilitare l'applicazione di portale " + app.Label + ".", userId, userName);
                    }

                    bool done = app.SwitchState();
                    if (done)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha modificato correttamente lo stato dell'applicazione di portale " + app.Label + ".", userId, userName);
                        NetCms.GUI.PopupBox.AddMessage("Lo stato dell'applicazione " + app.Label + " è stato cambiato correttamente");
                    }
                    else
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a modificare lo stato dell'applicazione di portale " + app.Label + ".", userId, userName);
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile cambiare lo stato dell'applicazione " + app.Label);
                        NetCms.GUI.PopupBox.AddMessage(app.Installer.Errors);
                    }
                }
                return state.Control;
            }
        }
        private HtmlGenericControl UninstallControl
        {
            get
            {
                G2Core.XhtmlControls.InputField uninstall = new G2Core.XhtmlControls.InputField("uninstall");
                if (uninstall.RequestVariable.IsValidInteger)//&& applicationsPool.NotInstalledAssemblies.Contains(install.RequestVariable.StringValue))
                {
                    NetCms.Structure.Applications.Application app = null;
                    if (ApplicationsPool.ActiveAssemblies.Contains(uninstall.RequestVariable.StringValue))
                        app = ApplicationsPool.ActiveAssemblies[uninstall.RequestVariable.StringValue];
                    else app = ApplicationsPool.FaultyAssemblies[uninstall.RequestVariable.StringValue];

                    NetCms.Users.User utente = NetCms.Users.AccountManager.CurrentAccount;
                    string userId = utente.ID.ToString();
                    string userName = utente.UserName;

                    app.Installer.Uninstall();
                    NetCms.GUI.PopupBox.AddMessage("L'applicazione " + app.Label + " è stata rimossa correttamente");
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha disinstallato con successo l'applicazione di portale " + app.Label + ".", userId, userName);

                    if (ApplicationsPool.ActiveAssemblies.Contains(uninstall.RequestVariable.StringValue))
                        ApplicationsPool.ActiveAssemblies.Remove(app.ID.ToString());
                    else ApplicationsPool.FaultyAssemblies.Remove(app.ID.ToString());

                    ApplicationsPool.NonActiveAssemblies.Add(app);
                    G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
                    cache.RemoveAll("");
                }
                return uninstall.Control;
            }
        }
        private HtmlGenericControl RepairControl
        {
            get
            {
                G2Core.XhtmlControls.InputField repair = new G2Core.XhtmlControls.InputField("repair");
                if (repair.RequestVariable.IsValidInteger)
                {
                    NetCms.Structure.Applications.Application app = ApplicationsPool.Assemblies[repair.RequestVariable.StringValue];
                    
                    NetCms.Users.User utente = NetCms.Users.AccountManager.CurrentAccount;
                    string userId = utente.ID.ToString();
                    string userName = utente.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta provando a riparare l'applicazione di portale " + app.Label + ".", userId, userName);

                    ApplicationFixer fixer = new ApplicationFixer(app);
                    bool done = fixer.Repair();
                    if (done)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha riparato con successo l'applicazione di portale " + app.Label + ".", userId, userName);
                        NetCms.GUI.PopupBox.AddMessage("L'applicazione " + app.Label + " è stata riparata correttamente");
                    }
                    else
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a riparare in modo completo l'applicazione di portale " + app.Label + ".", userId, userName);
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile riparare in modo completo l'applicazione " + app.Label);
                    }
                }
                return repair.Control;
            }
        }
    }
}