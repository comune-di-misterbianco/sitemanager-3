﻿using System.IO;
using System.Linq;
using System.Data;
using NetCms.GUI;
using System.Collections.Generic;
using System;
using G2Core.Common;

namespace NetCms.Structure.Applications
{
    public class GrantsProceduresInstaller
    {
        private NetCms.Connections.Connection _Conn;
        public NetCms.Connections.Connection Conn
        {
            get
            {
                //if (_Conn == null)
                //    _Conn = NetCms.Connections.ConnectionsManager.CommonMySqlConnection;
                //return _Conn;
                NetCms.Connections.ConnectionsManager ConnManager = new NetCms.Connections.ConnectionsManager();
                return ConnManager.CommonConnection;
            }
        }

        public Dictionary<string,string> ResourceNames
        {
            get
            {
                if (_ResourceNames == null)
                {
                    string selector = ".SqlScripts.";
                    _ResourceNames = Assembly.GetManifestResourceNames()
                                    .Where(x => x.Contains(selector) && x.EndsWith(".txt"))
                                    //.Select(x => x.SubstringAtBetween(selector,".txt"))
                                    .ToDictionary(x => x.SubstringAtBetween(selector, ".txt"), x => x);
                }
                return _ResourceNames;
            }
        }
        private Dictionary<string, string> _ResourceNames;

        public System.Reflection.Assembly Assembly
        {
            get
            {
                return _Assembly ?? (_Assembly = this.GetType().Assembly);
            }
        }
        private System.Reflection.Assembly _Assembly;

        public DataTable RoutinesOnDB
        {
            get
            {
                if (_RoutinesOnDB == null)
                {
                    string sqlQueryString = "SELECT * FROM `information_schema`.`ROUTINES` WHERE routine_schema = '" + Conn.DatabaseName + "';";
                    DataTable dt = new DataTable(sqlQueryString);
                    Conn.FillDataTable(sqlQueryString, dt);
                    _RoutinesOnDB = dt;
                }
                return _RoutinesOnDB;
            }
        }
        private DataTable _RoutinesOnDB;

        public GrantsProceduresInstaller()
        {
            //if(!NetCms.Configurations.Debug.GenericEnabled)
            //    throw new NetCms.Exceptions.PageNotFound404Exception();
        }

        public bool DatabaseAvailable()
        {
            //var datatable = new NetCms.Networks.NetworksData(false);
            //bool result = true;
            //foreach (DataRow row in datatable.Rows)
            //    result &= CheckNetworkProceduresStatus(row["Nome_Network"].ToString());
            return RoutinesOnDB != null && RoutinesOnDB.Columns.Count>0;
        }

        public bool CheckAllNetworksProceduresStatus()
        {
            //var datatable = new NetCms.Networks.NetworksData(false);
            bool result = true;
            //foreach (DataRow row in datatable.Rows)
            result &= CheckProceduresStatus();
            return result;
        }

        public bool CheckProceduresStatus()
        {
            bool result = true;
            foreach (string sqlName in ResourceNames.Keys)
            {
                result &= RoutinesOnDB.Rows
                          .Cast<DataRow>()
                          .Count(x => x["specific_name"].ToString() == sqlName) == 1;
            }
            return result;
        }

        //RINOMINATA IN CheckProceduresStatus
        //public bool CheckNetworkProceduresStatus(string networkSystemName)
        //{
        //    bool result = true;
        //    foreach (string sqlName in ResourceNames.Keys)
        //    {
        //        result &= RoutinesOnDB.Rows
        //                  .Cast<DataRow>()
        //                  .Count(x => x["specific_name"].ToString() == sqlName.Replace("network_", networkSystemName + "_")) == 1;
        //    }
        //    return result;
        //}

        //public bool FixAllNetworksProcedures()
        //{
        //    return GenerateAllNetworksProcedures();
        //}

        public bool FixProcedures()
        {
            return GenerateProcedures();
        }

        //RINOMINATO IN FixProcedures
        //public bool FixNetworkProcedures(string networkSystemName)
        //{
        //    return GenerateNetworkProcedures(networkSystemName);
        //}

        //public bool GenerateAllNetworksProcedures()
        //{
        //    var datatable = new NetCms.Networks.NetworksData(false);
        //    bool result = true;
        //    foreach (DataRow row in datatable.Rows)
        //        result &= GenerateNetworkProcedures(row["Nome_Network"].ToString());
        //    return result;
        //}

        public bool GenerateProcedures()
        {
            bool result = true;
            foreach (string fileName in ResourceNames.Values)
            {
                var manifest = Assembly.GetManifestResourceStream(fileName);
                using (StreamReader reader = new StreamReader(manifest))
                {
                    bool ok = false;
                    try
                    {
                        string script = reader.ReadToEnd();
                        //script = script.Replace("network_", networkSystemName + "_");
                        ok = Conn.ExecuteCommand(script, CommandType.Text, null);
                    }
                    catch (Exception ex)
                    {
                        ok = false;
                        Diagnostics.Diagnostics.TraceException(ex, null, null);
                        Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, string.Format("Impossibile creare la Stored Procedure {0}", fileName), null, null);
                    }
                    result &= ok;
                }
            }

            return result;
        }

        //RINOMINATO IN GenerateProcedures
        //public bool GenerateNetworkProcedures(string networkSystemName)
        //{
        //    bool result = true;
        //    foreach (string fileName in ResourceNames.Values)
        //    {
        //        var manifest = Assembly.GetManifestResourceStream(fileName);
        //        using (StreamReader reader = new StreamReader(manifest))
        //        {
        //            bool ok = false;
        //            try
        //            {
        //                string script = reader.ReadToEnd();
        //                script = script.Replace("network_", networkSystemName + "_");
        //                ok = Conn.ExecuteCommand(script, CommandType.Text, null);
        //            }
        //            catch(Exception ex)
        //            {
        //                ok = false;
        //                Diagnostics.Diagnostics.TraceException(ex, null, null);
        //                Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, string.Format("Impossibile creare la Stored Procedure {0}", fileName), null, null);
        //            }
        //            result &= ok;
        //        }
        //    }

        //    return result;
        //}

        public bool DeleteProcedures()
        {
            bool result = true;
            foreach (string fileName in ResourceNames.Keys)
            {
                bool ok = false;
                try
                {
                    string script = string.Format("DROP PROCEDURE IF EXISTS `{0}`", fileName);
                    //script = script.Replace("network_", networkSystemName + "_");
                    ok = Conn.ExecuteCommand(script, CommandType.Text, null);
                }
                catch (Exception ex)
                {
                    ok = false;
                    Diagnostics.Diagnostics.TraceException(ex, null, null);
                    Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, string.Format("Impossibile rimuovere la Stored Procedure {0}", fileName), null, null);
                }
                result &= ok;
            }

            return result;
        }

        //RINOMINATO IN DeleteProcedures
        //public bool DeleteNetworkProcedures(string networkSystemName)
        //{
        //    bool result = true;
        //    foreach (string fileName in ResourceNames.Keys)
        //    {
        //        bool ok = false;
        //        try
        //        {
        //            string script = string.Format("DROP PROCEDURE IF EXISTS `{0}`", fileName);
        //            script = script.Replace("network_", networkSystemName + "_");
        //            ok = Conn.ExecuteCommand(script, CommandType.Text, null);
        //        }
        //        catch (Exception ex)
        //        {
        //            ok = false;
        //            Diagnostics.Diagnostics.TraceException(ex, null, null);
        //            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, string.Format("Impossibile rimuovere la Stored Procedure {0}", fileName), null, null);
        //        }
        //        result &= ok;
        //    }

        //    return result;
        //}
    }
}