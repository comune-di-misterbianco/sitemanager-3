﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetCms.Vertical;
using NetCms.Installation;
using NetCms.GUI.Icons;
using G2Core.AdvancedXhtmlForms;
using G2Core.AdvancedXhtmlForms.Fields;

namespace NetCms.Structure.Applications
{
    public class Installer : SmPageVertical
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.HomeSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Gestione Applicazioni di Portale e Verticali"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Plugin; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_Installer"; }
        }

        public Installer()
        {
            if (NetCms.Configurations.Debug.GenericEnabled)
                this.Toolbar.Buttons.Add(new ToolbarButton("networks.aspx", Icons.World, "Gestione Stato Networks"));
        }

        protected override WebControl RootControl
        {
            get
            {
                VerticalApplication installerApp = VerticalApplicationsPool.GetBySystemName("installer");
                //NetCms.Vertical.Grants.ExternalAppzGrants grants = new NetCms.Vertical.Grants.ExternalAppzGrants("installer");
                if (!installerApp.Grant) NetCms.GUI.PopupBox.AddSessionMessage("Non hai i permessi per accedere alla risorsa.", PostBackMessagesType.Error, true);

                G2Core.Common.RequestVariable request = new G2Core.Common.RequestVariable("vid", G2Core.Common.RequestVariable.RequestType.QueryString);
                if (request.IsValidInteger && VerticalApplicationsPool.ActiveAssemblies.Contains(request.StringValue))
                {
                    return this.VerticalApplicationConfig(request.IntValue);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di CMS di gestione delle applicazioni di portale e verticali.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    }

                    WebControl control = new WebControl(HtmlTextWriterTag.Div);

                    control.Controls.Add(new CmsApplicationsControl(CmsApplicationsControl.RenderModes.Installer));
                    control.Controls.Add(new VerticalApplicationsControl(VerticalApplicationsControl.RenderModes.Installer));

                    return control;
                }
            }
        }

        private WebControl VerticalApplicationConfig(int ID)
        {
            if (!IsPostBack)
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di modifica configurazione dell'applicazione verticale con id: " + ID.ToString()+ ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            }
            
            WebControl value = new WebControl(HtmlTextWriterTag.Div);

            AxfGenericTable table = new AxfGenericTable("externalappz", "id_ExternalApp", this.Conn);
            table.RelatedRecordID = ID;

            AxfTextBox Path_ExternalApp = new AxfTextBox("Percorso di Esecuzione.", "Path_ExternalApp");
            Path_ExternalApp.InfoText = @"
                Il percorso di default si ottiene lasciando vuoto questo campo.<br /> 
                In questo caso il percorso sarà del tipo '/root/portale/applications/nomeapp'.<br /> 
                <br /> 
                Per configurare un percorso personalizzato e necessario scrivere un percorso del tipo, '/nomeapp' e l'applicazione sarà raggiungibile al'indirizzo '/root/portale/nomeapp/default.aspx'.<br /> 
                E' anche possibile specificare percorsi più complessi del tipo  '/xxxxxxxx/yyyyyyy/xxxxxxxx/nome/app'. E l'applicazione sarà raggiungibile al'indirizzo '/root/portale/xxxxxxxx/yyyyyyy/xxxxxxxx/nome/app/default.aspx'.<br />              
            ";
            Path_ExternalApp.Required = false;
            table.addField(Path_ExternalApp);

            AxfDropDownList layout = new AxfDropDownList("Layout delle Colonne.", "FrontLayout_ExternalApp");
            layout.InfoText = "La visualizzazione finale del layout dipende comunque da come sono state impostate le colonne nella homepage principale del portale dal quale l'applicazione è stata richiamata.";
            layout.addItem("Mostra Tutte", "0");
            layout.addItem("Nascondi Tutte", "7");
            //layout.addItem("Mostra solo Sinistra e Destra", "1");
            //layout.addItem("Mostra solo Sinistra e Footer", "2");
            //layout.addItem("Mostra solo Destra e Footer", "3");
            layout.addItem("Mostra solo Sinistra", "4");
            layout.addItem("Mostra solo Destra", "5");
            //layout.addItem("Mostra solo Footer", "6");
            layout.Required = true;

            table.addField(layout);


            AxfForm forms = new AxfForm();
            forms.Tables.Add(table);

            if (this.IsPostBack)
            {
                var result = forms.serializedUpdate();
                if (forms.LastOperation == AxfForm.LastOperationValues.OK)
                {
                    VerticalApplicationsPool.ActiveAssemblies[ID.ToString()].UpdateApplicationData();
                    PopupBox.AddSessionMessage("Configurazione salvata con successo.", PostBackMessagesType.Success, true);
                }
                else
                    PopupBox.AddMessage("Si sono verificati alcuni errori nell'aggiornamento dei dati. verifica i dati inseriti e riprova.", PostBackMessagesType.Notify);
            }

            value.Controls.Add(forms.getControl());

            return value;
        }
    }
}