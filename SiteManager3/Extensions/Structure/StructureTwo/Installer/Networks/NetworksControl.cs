﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using System.IO;
using System.Xml;
using NetCms.Structure.Applications;
using NetCms.Networks;

namespace NetCms.Installation
{
    public class NetworksControl : WebControl
    {
        private int PopupBoxesCount = 0;

        public enum RenderModes
        {
            Installer, Monitoring
        }
        public RenderModes RenderMode
        {
            get
            {
                return _RenderMode;
            }
            private set
            {
                _RenderMode = value;
            }
        }
        private RenderModes _RenderMode;

        public GrantsProceduresInstaller GrantsProceduresInstaller
        {
            get
            {
                return _GrantsProceduresInstaller ?? (_GrantsProceduresInstaller = new GrantsProceduresInstaller());
            }
        }
        private GrantsProceduresInstaller _GrantsProceduresInstaller;

        public NetworksControl(RenderModes renderMode)
            : base(HtmlTextWriterTag.Div)
        {
            this.RenderMode = renderMode;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!GrantsProceduresInstaller.DatabaseAvailable())
            {
                WebControl fieldsetContent = new WebControl(HtmlTextWriterTag.Fieldset);
                WebControl legendContent = new WebControl(HtmlTextWriterTag.Legend);
                legendContent.Controls.Add(new LiteralControl("Stato Portali"));
                               
                fieldsetContent.Controls.Add(legendContent);
                fieldsetContent.Controls.Add(new LiteralControl("<p>Database non disponibile, si prega di provare a fare un aggiornamento della pagina.</p>"));
                this.Controls.Add(fieldsetContent);
            }

            Collapsable cmsapps = new Collapsable("Stato Portali");
            cmsapps.AppendControl(ListView());
            this.Controls.Add(cmsapps);
        }

        private WebControl ListView()
        {
            WebControl value = new WebControl(HtmlTextWriterTag.Div);

            value.Controls.Add(RepairControl);

            Table table = new Table();
            table.CssClass = "tab";
            table.CellPadding = 0;
            table.CellSpacing = 0;

            TableHeaderRow hrow = new TableHeaderRow();
            table.Controls.Add(hrow);

            if (NetCms.Configurations.Debug.GenericEnabled)
                hrow.Controls.Add(BuildHeaderCell("ID", 1));

            hrow.Controls.Add(BuildHeaderCell("Nome", 1));

            if (NetCms.Configurations.Debug.GenericEnabled)
                hrow.Controls.Add(BuildHeaderCell("Key", 1));

            hrow.Controls.Add(BuildHeaderCell("Stato Abilitazione", 1));
            hrow.Controls.Add(BuildHeaderCell("Stato Cms", 1));
            //hrow.Controls.Add(BuildHeaderCell("Stato Istallazione", 1));

            if (RenderMode == RenderModes.Installer)
                hrow.Controls.Add(BuildHeaderCell("Stato Files", 2));
            else
                hrow.Controls.Add(BuildHeaderCell("Stato Files", 1));

            if (GrantsProceduresInstaller.DatabaseAvailable())
            {
                if (RenderMode == RenderModes.Installer)
                    hrow.Controls.Add(BuildHeaderCell("Stato Routines", 2));
                else
                    hrow.Controls.Add(BuildHeaderCell("Stato Routines", 1));
            }

            bool alternate = false;

            //var datatable = new NetCms.Networks.NetworksData(false);
            foreach (var network in NetCms.Networks.NetworksManager.GlobalIndexer.WithCms.Values)
            {
                TableRow row = new TableRow();
                if (alternate) row.CssClass = "alternate";
                table.Controls.Add(row);
                TableCell cell = new TableCell();

                if (NetCms.Configurations.Debug.GenericEnabled)
                {
                    cell = new TableCell();
                    cell.Text = network.ID.ToString();
                    row.Controls.Add(cell);
                }

                cell = new TableCell();
                cell.Text = network.Label;
                row.Controls.Add(cell);
                
                if (NetCms.Configurations.Debug.GenericEnabled)
                {
                    cell = new TableCell();
                    cell.Text = network.SystemName;
                    row.Controls.Add(cell);
                }

                row.Controls.Add(BuildCell_ActivationState(network));
                row.Controls.Add(BuildCell_Status(network));
                row.Controls.Add(BuildCell_InstallState_Folders(network));
                if (RenderMode == RenderModes.Installer)
                    row.Controls.Add(BuildCell_Actions_Folders(network));
                if (GrantsProceduresInstaller.DatabaseAvailable())
                {
                    row.Controls.Add(BuildCell_InstallState_Routines(network));
                    if (RenderMode == RenderModes.Installer)
                        row.Controls.Add(BuildCell_Actions_Routines(network));
                }
                alternate = !alternate;

            }
            value.Controls.Add(table);
            return (value);
        }

        private TableCell BuildHeaderCell(string text, int colSpan)
        {
            TableHeaderCell hcell = new TableHeaderCell();
            hcell.Text = text;
            hcell.ColumnSpan = colSpan;
            return hcell;
        }

        private TableCell BuildCell(string text)
        {
            TableCell cell = new TableCell();

            return cell;
        }
        private TableCell BuildCell_ActivationState(BasicNetwork network)
        {
            TableCell cell = new TableCell();

            cell.Text = !network.Enabled ? "Disablilitato" : "Abilitato";

            return cell;
        }
        private TableCell BuildCell_Status(BasicNetwork network)
        {
            TableCell cell = new TableCell();
            
            switch (network.CmsStatus)
            {
                case NetworkCmsStates.Disabled: cell.Text = "Disablilitato"; break;
                case NetworkCmsStates.Enabled: cell.Text = "Creato"; break;
                case NetworkCmsStates.NotCreated: cell.Text = "Non Esistente"; break;
            }
            return cell;
        }

        private TableCell BuildCell_InstallState_Folders(BasicNetwork network)
        {
            TableCell cell = new TableCell();

            var filesChecker = new NetworksFilesChecker(network.SystemName, network.ID);
            bool ok = filesChecker.CheckStatus();

            string checks = "";
            if (ok)
            {
                cell.Text = "Funzionante";
                cell.CssClass = "action ok";
            }
            else
            {
                cell.Text = "<strong class=\"red\">Guasta</strong>";
                cell.CssClass = "action alert";

                checks += "<tr>";
                checks += "<th>Descrizione</th><th>Risultato</th>";
                checks += "</tr>";

                string classe = "class=\"red\"";
                string status = "Warning";
                
                foreach (var message in filesChecker.Log)
                {
                    classe = "class=\"red\"";
                    status = "Warning";
                    checks += "<tr " + classe + ">";
                    checks += "<td>" + message + "</td><td>" + status + "</td>";
                    checks += "</tr>";
                }
                cell.Text = @"<a href=""#TB_inline?height=600&width=800&inlineId=FaultDetailsCms" + PopupBoxesCount + @""" title=""Stato '" + network.Label + @"'"" class=""thickbox"" ><span>" + cell.Text + "</span></a>";
                cell.Text += @"
                                <div style=""display:none;"">
                                    <div id=""FaultDetailsCms" + PopupBoxesCount + @""">
                                        <table border=""0"" cellpadding=""0"" cellspacing=""0"" class=""tab"">" + checks + @"</table>
                                    </div>
                                </div>
                             ";
                PopupBoxesCount++;
            }

            return cell;
        }
        private TableCell BuildCell_Actions_Folders(BasicNetwork network)
        {
            TableCell cell = new TableCell();

            cell.Text = "&nbsp;";
            if (network.CmsStatus != NetworkCmsStates.NotCreated)
            {
                var filesChecker = new NetworksFilesChecker(network.SystemName, network.ID);
                bool ok = filesChecker.CheckStatus();
                if (!ok)
                {
                    cell.Text = "<a href=\"javascript: setFormValue('" + network.SystemName + "','repairGrants',1);\"><span>Ripara</span></a>";
                    cell.CssClass = "action edit";
                }
            }
            return cell;
        }

        private TableCell BuildCell_InstallState_Routines(BasicNetwork network)
        {
            TableCell cell = new TableCell();

            bool ok = GrantsProceduresInstaller.CheckProceduresStatus(); ;
            //bool ok = GrantsProceduresInstaller.CheckNetworkProceduresStatus(network.SystemName); ;

            if (ok)
            {
                cell.Text = "Funzionante";
                cell.CssClass = "action ok";
            }
            else
            {
                cell.Text = "<strong class=\"red\">Guasta</strong>";
                cell.CssClass = "action alert";
            }

            return cell;
        }
        private TableCell BuildCell_Actions_Routines(BasicNetwork network)
        {
            TableCell cell = new TableCell();

            if (network.CmsStatus != NetworkCmsStates.NotCreated)
            {
                bool ok = GrantsProceduresInstaller.CheckProceduresStatus(); ;
                //bool ok = GrantsProceduresInstaller.CheckNetworkProceduresStatus(network.SystemName); ;
                if (!ok)
                {
                    cell.Text = "<a href=\"javascript: setFormValue('" + network.SystemName + "','repairGrants',1);\"><span>Ripara</span></a>";
                    cell.CssClass = "action edit";
                }
                else
                {
                    cell.Text = "&nbsp;";

                    if (NetCms.Configurations.Debug.GenericEnabled)
                    {
                        cell.Text = "<a href=\"javascript: setFormValue('" + network.SystemName + "','repairGrants',1);\"><span>Forza Aggiornamento</span></a>";
                        cell.CssClass = "action edit";
                    }
                }
            }
            else
            {
                cell.Text = "&nbsp;";
            }
            return cell;
        }

        private HtmlGenericControl RepairControl
        {
            get
            {
                G2Core.XhtmlControls.InputField repair = new G2Core.XhtmlControls.InputField("repairGrants");
                if (repair.RequestVariable.IsValidString)
                {
                    var network = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks[repair.RequestVariable.StringValue];
                    bool done = GrantsProceduresInstaller.FixProcedures();
                    //bool done = GrantsProceduresInstaller.FixNetworkProcedures(network.SystemName);
                    var filesChecker = new NetworksFilesChecker(network.SystemName,network.ID);
                    filesChecker.Fix();
                    if (done)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username ha riparato con successo la network " + network.SystemName + ".", null, null);
                        NetCms.GUI.PopupBox.AddMessage("La Network " + network.SystemName + " è stata riparata correttamente");
                    }
                    else
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username non è riuscito a riparare in modo completo la network " + network.SystemName + ".", null, null);
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile riparare in modo completo l'applicazione la network " + network.SystemName );
                    }
                }
                return repair.Control;
            }
        }
    }

}