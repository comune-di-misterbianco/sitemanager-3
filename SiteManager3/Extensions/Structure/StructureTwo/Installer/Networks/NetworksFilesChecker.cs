﻿using System.Data;
using System.Linq;
using NetCms.Connections;
using System.Collections.Generic;
using NetCms.Front.Static;

namespace NetCms.Installation
{
    public class NetworksFilesChecker
    {
        public Connection Conn
        {
            get
            {
                if (_Conn == null)
                {
                    _Conn = new MySqlConnection(Configurations.ConnectionsStrings.MainDB);
                    _Conn.CacheEnabled = false;
                }
                return _Conn;
            }
        }
        private Connection _Conn;

       

        public List<string> Log
        {
            get
            {
                return _Log ?? (_Log = new List<string>());
            }
        }
        private List<string> _Log;

        public string NetworkName { get; private set; }

        public string NetworkWebRootFolder
        {
            get
            {                
                NetCms.Networks.BasicNetwork network = NetCms.Networks.NetworksBusinessLogic.GetById(NetworkID);                
                return (network != null) ? network.RootPath : "";                   
            }
        }
        

        //Aggiunto successivamente
        public int NetworkID { get; private set; }

        public NetworksFilesChecker(string networkName, int networkId)
        {
            NetworkName = networkName;
            NetworkID = networkId;            
        }

        //public NetworksFilesChecker(string networkName)
        //{
        //    NetworkName = networkName;
        //}

        public DataTable Folders
        {
            get
            {
                if (_Folders == null)
                {
                    _Folders = Conn.SqlQuery("SELECT * FROM (Folders INNER JOIN network_folders ON id_Folder = id_NetworkFolder) WHERE Network_Folder = " + NetworkID);
                }
                return _Folders;
            }
        }
        private DataTable _Folders;

        public bool CheckStatus()
        {
            Log.Clear();

            var table = Folders.Select("Depth_Folder = 1");
            if (table.Length == 1)
            {
                CheckFolderRecursive(table[0]);
                CheckStaticPages();
                return Log.Count == 0;
            }
            else
            {
                Log.Add("La rete '" + NetworkName + "' è gravemente compromessa, per la riparazione è necessario l'intervento di un tecnico.");
                return false;
            }
        }

        private void CheckFolderRecursive(DataRow row)
        {
            CheckDirectoryPhysical(row);
            CheckFilesPhysical(row);
            var childFolders = Folders.Select("Parent_Folder = " + row["id_Folder"].ToString());
            foreach (DataRow childFolder in childFolders)
                CheckFolderRecursive(childFolder);
        }

        private bool CheckDirectoryPhysical(DataRow row)
        {     
            try
            {
                string path = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + "/" + NetworkWebRootFolder + row["Path_Folder"].ToString());

                if (!System.IO.Directory.Exists(path))
                {
                    Log.Add("La cartella '" + path + "' non è stato trovata sulla rete '" + NetworkName + "'");
                    return false;
                }
                return true;
            }
            catch 
            {
                Log.Add("La cartella '" + NetCms.Configurations.Paths.AbsoluteRoot + "/" + NetworkName + row["Path_Folder"].ToString() + "' sulla rete '" + NetworkName + "' presente un nome troppo lungo.");
                return false;
            }
        }

        private bool CheckFilesPhysical(DataRow row)
        {
            try
            {
                string path = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + "/" + NetworkWebRootFolder + row["Path_Folder"].ToString());

                string defaultAspxPath = path + "\\default.aspx";
                if (!System.IO.File.Exists(defaultAspxPath))
                {
                    Log.Add("Il file '" + defaultAspxPath + "' non è stato trovato sulla rete '" + NetworkName + "'");
                    return false;
                }
                return true;
            }
            catch 
            {
                Log.Add("La cartella '" + NetCms.Configurations.Paths.AbsoluteRoot + "/" + NetworkWebRootFolder + row["Path_Folder"].ToString() + "' sulla rete '" + NetworkName + "' presente un nome troppo lungo.");
                return false;
            }
        }

        private void CheckStaticPages()
        {
            foreach (var page in StaticPagesCollector.ClassesKeysAssociations.Keys.Where(x=>x.EndsWith("default.aspx")))
            {
                string[] pathParts = page.Split(new []{"/"}, System.StringSplitOptions.RemoveEmptyEntries);
                string pathOverall = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + "/" + NetworkWebRootFolder);

                foreach (string part in pathParts)
                {
                    pathOverall += "\\" + part;

                    if (pathOverall.Contains(".aspx"))
                    {
                        if (!System.IO.File.Exists(pathOverall))
                        {
                            Log.Add("Il file statico '" + pathOverall + "' non è stato trovato sulla rete '" + NetworkName + "'");
                        }
                    }
                    else
                    {
                        if (!System.IO.Directory.Exists(pathOverall))
                        {
                            Log.Add("La cartella '" + pathOverall + "' (facente parte di un percorso statico) non è stato trovata sulla rete '" + NetworkName + "'");
                        }
                    }
                }
            }
        }

        private void FixFolderPhysical(DataRow row)
        {
            try
            {
                string path = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + "/" + NetworkWebRootFolder + row["Path_Folder"].ToString());

                string Path = path;
                if (!System.IO.Directory.Exists(Path))
                    System.IO.Directory.CreateDirectory(Path);
            }
            catch
            {
                Log.Add("La cartella '" + NetCms.Configurations.Paths.AbsoluteRoot + "/" + NetworkName + row["Path_Folder"].ToString() + "' sulla rete '" + NetworkName + "' presente un nome troppo lungo.");                
            }
        }

        private void FixFilesPhysical(DataRow row)
        {
            try
            {
                string path = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + "/" + NetworkWebRootFolder + row["Path_Folder"].ToString());

                string defaultAspxPath = path + "\\default.aspx";
                if (!System.IO.File.Exists(defaultAspxPath))
                    System.IO.File.Create(defaultAspxPath);
            }
            catch
            {
                Log.Add("La cartella '" + NetCms.Configurations.Paths.AbsoluteRoot + "/" + NetworkWebRootFolder + row["Path_Folder"].ToString() + "' sulla rete '" + NetworkName + "' presente un nome troppo lungo.");
            }
        }

        public void Fix()
        {
            Log.Clear();

            var table = Folders.Select("Depth_Folder = 1");
            if (table.Length == 1)
            {
                FixFolderRecursive(table[0]);
                FixStaticPages();
            }
            else
            {
                Log.Add("La rete '" + NetworkName + "' è gravemente compromessa, per la riparazione è necessario l'intervento di un tecnico.");
            }
        }
        
        private void FixFolderRecursive(DataRow row)
        {
            FixFolderPhysical(row);
            FixFilesPhysical(row);
            var childFolders = Folders.Select("Parent_Folder = " + row["id_Folder"].ToString());
            foreach (DataRow childFolder in childFolders)
                FixFolderRecursive(childFolder);
        }

        private bool FixStaticPages()
        {
            foreach (var page in StaticPagesCollector.ClassesKeysAssociations.Keys.Where(x => x.EndsWith("default.aspx")))
            {
                string[] pathParts = page.Split(new[] { "/" }, System.StringSplitOptions.RemoveEmptyEntries);
                string pathOverall = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + "/" + NetworkWebRootFolder);

                foreach (string part in pathParts)
                {
                    pathOverall += "\\" + part;

                    if (pathOverall.Contains(".aspx"))
                    {
                        if (!System.IO.File.Exists(pathOverall))
                            System.IO.File.Create(pathOverall);
                    }
                    else
                    {
                        if (!System.IO.Directory.Exists(pathOverall))
                            System.IO.Directory.CreateDirectory(pathOverall);
                    }
                }
            }

            return true;
        }
    }
}