﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetCms.Vertical;

namespace NetCms.Installation
{
    public class VerticalApplicationsControl : WebControl
    {
        private int PopupBoxesCount = 0;

        public enum RenderModes
        {
            Installer, Monitoring
        }

        public bool ThereAreBrokenApps { get; private set; } 

        public RenderModes RenderMode
        {
            get
            {
                return _RenderMode;
            }
            private set
            {
                _RenderMode = value;
            }
        }
        private RenderModes _RenderMode;
        public VerticalApplicationsControl(RenderModes renderMode)
            : base(HtmlTextWriterTag.Div)
        {
            this.RenderMode = renderMode;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Collapsable cmsapps = new Collapsable("Applicazioni Verticali");
            cmsapps.AppendControl(ListView());

            this.Controls.Add(cmsapps);
        }

        private WebControl ListView()
        {
            WebControl value = new WebControl(HtmlTextWriterTag.Div);

            value.Controls.Add(InstallControl);
            value.Controls.Add(UninstallControl);
            value.Controls.Add(StateControl);
            value.Controls.Add(RepairControl);

            Table table = new Table();
            table.CssClass = "tab";
            table.CellPadding = 0;
            table.CellSpacing = 0;

            TableHeaderCell hcell;
            TableHeaderRow hrow = new TableHeaderRow();
            table.Controls.Add(hrow);

            if (NetCms.Configurations.Debug.GenericEnabled)
            {
                hrow.Controls.Add(BuildHeaderCell("ID", 1));
            }

            hrow.Controls.Add(BuildHeaderCell("Nome", 1));

            if (NetCms.Configurations.Debug.GenericEnabled)
                hrow.Controls.Add(BuildHeaderCell("Key", 1));

            hrow.Controls.Add(BuildHeaderCell("Versione", 1));

            hrow.Controls.Add(BuildHeaderCell("Stato Installazione", 2));
            if (RenderMode == RenderModes.Installer)
            {
                hrow.Controls.Add(BuildHeaderCell("Stato Abilitazione", 2));
                hrow.Controls.Add(BuildHeaderCell("&nbsp;", 3));
            }
            bool alternate = false;

            var appz = VerticalApplicationsPool.Assemblies.Cast<VerticalApplication>()
                                                          .Where(x => !x.IsSystemApplication || NetCms.Configurations.Debug.ApplicationsInstallationEnabled || !x.Validator.Validate()).OrderBy(x => x.Description)
                                                          .Distinct();

            foreach (VerticalApplication application in appz)
            {
                TableRow row = new TableRow();
                if (alternate) row.CssClass = "alternate";
                table.Controls.Add(row);
                TableCell cell = new TableCell();

                if (NetCms.Configurations.Debug.GenericEnabled)
                {
                    cell = new TableCell();
                    cell.Text = application.ID.ToString();
                    row.Controls.Add(cell);
                }

                cell = new TableCell();
                cell.Text = application.Label;
                row.Controls.Add(cell);

                if (NetCms.Configurations.Debug.GenericEnabled)
                {
                    cell = new TableCell();
                    if(application.IsSystemApplication)
                        cell.Text = "<strong>{0}</strong>".FormatByParameters(application.SystemName);
                    else
                        cell.Text = application.SystemName;
                    row.Controls.Add(cell);
                }

                cell = new TableCell();
                cell.Text = application.Assembly.GetName().Version.ToString();
                row.Controls.Add(cell);

                row.Controls.Add(BuildCell_InstallState(application));
                row.Controls.Add(BuildCell_Monitoring(application));

               

                if (RenderMode == RenderModes.Installer)
                {
                    row.Controls.Add(BuildCell_Status(application));
                    row.Controls.Add(BuildCell_StatusChange(application));
                    row.Controls.Add(BuildCell_Configure(application));
                    row.Controls.Add(BuildCell_Actions(application));
                }
                
                alternate = !alternate;
            }
            if (ThereAreBrokenApps)
            {
                TableRow repairRow = new TableRow();
                if (alternate) repairRow.CssClass = "alternate";
                table.Controls.Add(repairRow);
                TableCell repairCell = new TableCell();

                repairCell = new TableCell();
                repairCell.ColumnSpan = NetCms.Configurations.Debug.GenericEnabled ? 9 : 7;
                string confirm = "Sei sicuro di voler riparare tutte le applicazioni guaste?";
                repairCell.Text = "<a href=\"javascript: setFormValueConfirm('all','repair_v',1,'{0}');\"><span>Ripara tutte le applicazioni guaste.</span></a>".FormatByParameters(confirm);
                repairCell.CssClass = "action alert";
                repairRow.Controls.Add(repairCell);
            }
            value.Controls.Add(table);
            return (value);
        }


        private TableCell BuildHeaderCell(string text, int colSpan)
        {
            TableHeaderCell hcell = new TableHeaderCell();
            hcell.Text = text;
            hcell.ColumnSpan = colSpan;
            return hcell;
        }

        private TableCell BuildCell(string text)
        {
            TableCell cell = new TableCell();

            return cell;
        }
        private TableCell BuildCell_InstallState(VerticalApplication application)
        {
            TableCell cell = new TableCell();

            cell.Text = application.InstallState == VerticalApplication.InstallStates.NotInstalled ? "Non Installata" : "Installata";

            return cell;
        }
        private TableCell BuildCell_Status(VerticalApplication application)
        {
            TableCell cell = new TableCell();

            if (application.InstallState == VerticalApplication.InstallStates.NotInstalled) cell.Text = "&nbsp;";
            else
            {
                if (application.Enabled)
                {
                    cell.Text = "Abilitata";
                }
                else
                {

                    cell.Text = "Disabilitata";
                }
            }
            return cell;
        }
        private TableCell BuildCell_Monitoring(VerticalApplication application)
        {
            TableCell cell = new TableCell();
            if (application.InstallState == VerticalApplication.InstallStates.NotInstalled) cell.Text = "&nbsp;";
            else
            {
                bool ok = application.Validator.Validate();
                if (!ok) ThereAreBrokenApps = true;
                string checks = "";
                if (ok)
                {
                    cell.Text = "Funzionante";
                    cell.CssClass = "action ok";
                }
                else
                {

                    cell.Text = "<strong class=\"red\">Guasta</strong>";
                    cell.CssClass = "action alert";
                }
                checks += "<tr>";
                checks += "<th>Tipo Verifica</th><th>Descrizione</th><th>Errore</th><th>Risultato</th></li>";
                checks += "</tr>";
                foreach (var check in application.Validator.ValidationResults)
                {
                    checks += "<tr " + (check.IsOk ? "" : "class=\"red\"") + ">";
                    checks += "<td>" + check.RepairTypeLabel + " </td><td>" + check.Name + " </td><td>" + ((check.IsOk ? "" : check.Message)) + "</td><td>" + ((check.IsOk ? "Ok" : "Errore")) + "</td></li>";
                    checks += "</tr>";
                }

                cell.Text = @"<a href=""#TB_inline?height=600&width=800&inlineId=FaultDetailsVertical" + PopupBoxesCount + @""" title=""Stato dell'Applicazione '" + application.Label + @"'"" class=""thickbox"" ><span>" + cell.Text + "</span></a>";
                cell.Text += @"
                                <div style=""display:none;"">
                                <div id=""FaultDetailsVertical" + PopupBoxesCount + @""">

                                <table border=""0"" cellpadding=""0"" cellspacing=""0"" class=""tab"">" + checks + @"</table>
                                </div></div>
                             ";
                PopupBoxesCount++;
            }


            return cell;
        }
        private TableCell BuildCell_StatusChange(VerticalApplication application)
        {
            TableCell cell = new TableCell();

            if (application.InstallState == VerticalApplication.InstallStates.Installed)
            {
                if (application.Enabled)
                {
                    cell.Text = "<a href=\"javascript: setFormValue(" + application.ID + ",'state_v',1);\"><span>Disabilita</span></a>";
                    cell.CssClass = "action Disabilita";
                }
                else
                {
                    cell.Text = "<a href=\"javascript: setFormValue(" + application.ID + ",'state_v',1);\"><span>Abilita</span></a>";
                    cell.CssClass = "action Abilita";
                }
            }
            else cell.Text = "&nbsp;";
            return cell;
        }
        private TableCell BuildCell_Actions(VerticalApplication application)
        {
            TableCell cell = new TableCell();

            if (application.InstallState == VerticalApplication.InstallStates.NotInstalled)
            {
                string confirm = "Sei sicuro di voler installare l\\\'applicazione \\\'{0}\\\'?".FormatByParameters(application.Label);
                cell.Text = "<a href=\"javascript: setFormValueConfirm(" + application.ID + ",'install_v',1,'{0}');\"><span>Installa</span></a>".FormatByParameters(confirm);
                cell.CssClass = "action add";
            }
            else
            {

                if (application.Validator.IsValid)
                {
                    if (application.AllowUninstall || NetCms.Configurations.Debug.ApplicationsInstallationEnabled)
                    {
                        cell.CssClass = "action delete";
                        string confirm = "Sei sicuro di voler rimuovere l\\\'applicazione \\\'{0}\\\'?".FormatByParameters(application.Label);
                        cell.Text = "<a href=\"javascript: setFormValueConfirm(" + application.ID + ",'uninstall_v',1,'{0}');\"><span>Rimuovi</span></a>".FormatByParameters(confirm);
                    }
                    else
                    {                        
                        cell.CssClass = "";
                        cell.Text = "&nbsp;";
                    }
                }
                else
                {
                    cell.CssClass = "action delete";
                    string confirm = "Sei sicuro di voler rimuovere l\\\'applicazione \\\'{0}\\\'?".FormatByParameters(application.Label);
                    cell.Text = "<a href=\"javascript: setFormValueConfirm(" + application.ID + ",'uninstall_v',1,'{0}');\"><span>Rimuovi</span></a>".FormatByParameters(confirm);
                }
            }

            return cell;
        }
     
        private TableCell BuildCell_Configure(VerticalApplication application)
        {
            TableCell cell = new TableCell();

            if (application.InstallState != VerticalApplication.InstallStates.NotInstalled && !application.Validator.IsValid)
            {
                string confirm = "Sei sicuro di voler riparare l\\\'applicazione \\\'{0}\\\'?".FormatByParameters(application.Label);
                cell.Text = "<a href=\"javascript: setFormValueConfirm(" + application.ID + ",'repair_v',1,'{0}');\"><span>Ripara</span></a>".FormatByParameters(confirm);
                cell.CssClass = "action edit";
            }
            else if (application.InstallState == VerticalApplication.InstallStates.Installed && application.AccessMode != VerticalApplication.AccessModes.Admin)
            {
                cell.Text = "<a href=\"?vid=" + application.ID + "\"><span>Configura</span></a>";
                cell.CssClass = "action prop";
            }
            else cell.Text = "&nbsp;";

            return cell;
        }

        private HtmlGenericControl StateControl
        {
            get
            {
                G2Core.XhtmlControls.InputField state = new G2Core.XhtmlControls.InputField("state_v");
                if (state.RequestVariable.IsValidInteger)//&& applicationsPool.NotInstalledAssemblies.Contains(install.RequestVariable.StringValue))
                {
                    VerticalApplication app;
                    if (VerticalApplicationsPool.ActiveAssemblies.Contains(state.RequestVariable.StringValue))
                        app = VerticalApplicationsPool.ActiveAssemblies[state.RequestVariable.StringValue];
                    else
                        app = VerticalApplicationsPool.NonActiveAssemblies[state.RequestVariable.StringValue];

                    NetCms.Users.User utente = NetCms.Users.AccountManager.CurrentAccount;
                    string userId = utente.ID.ToString();
                    string userName = utente.UserName;

                    if (app.Enabled)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta provando a disabilitare l'applicazione verticale " + app.Label + ".", userId, userName);
                    }
                    else
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta provando ad abilitare l'applicazione verticale " + app.Label + ".", userId, userName);
                    }

                    bool done = app.SwitchState();
                    if (done)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha modificato correttamente lo stato dell'applicazione verticale " + app.Label + ".", userId, userName);
                        NetCms.GUI.PopupBox.AddMessage("Lo stato dell'applicazione " + app.Label + " è stato cambiato correttamente");
                    }
                    else
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a modificare lo stato dell'applicazione verticale " + app.Label + ".", userId, userName);
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile cambiare lo stato dell'applicazione " + app.Label);
                        NetCms.GUI.PopupBox.AddMessage(app.Installer.Errors);
                    }
                }
                return state.Control;
            }
        }
        private HtmlGenericControl RepairControl
        {
            get
            {
                G2Core.XhtmlControls.InputField repair = new G2Core.XhtmlControls.InputField("repair_v");
                if (repair.RequestVariable.IsValidInteger)
                {
                    VerticalApplication app = VerticalApplicationsPool.Assemblies[repair.RequestVariable.StringValue];
                    
                    NetCms.Users.User utente = NetCms.Users.AccountManager.CurrentAccount;
                    string userId = utente.ID.ToString();
                    string userName = utente.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta provando a riparare l'applicazione verticale " + app.Label + ".", userId, userName);

                    bool done = false;

                    ///Aggiunta logica di gestione riparazione packages
                    if (app is VerticalApplicationPackage)
                        done = (app as VerticalApplicationPackage).Fixer.Repair();
                    else
                        done = app.Fixer.Repair();

                    if (done)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha riparato con successo l'applicazione verticale " + app.Label + ".", userId, userName);
                        NetCms.GUI.PopupBox.AddMessage("L'applicazione " + app.Label + " è stata riparata correttamente");
                    }
                    else
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a riparare in modo completo l'applicazione verticale " + app.Label + ".", userId, userName);
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile riparare in modo completo l'applicazione " + app.Label);
                    }
                }
                else if (repair.RequestVariable.IsValidString && repair.RequestVariable.StringValue == "all")
                {
                    NetCms.Users.User utente = NetCms.Users.AccountManager.CurrentAccount;
                    string userId = utente.ID.ToString();
                    string userName = utente.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta provando a riparare tutte le applicazioni vericali guaste.", userId, userName);
                    foreach (VerticalApplication app in VerticalApplicationsPool.Assemblies.Cast<VerticalApplication>().Where(v=> v.InstallState != VerticalApplication.InstallStates.NotInstalled  && !v.Validator.Validate()))
                    {
                        bool done = app.Fixer.Repair();
                        if (done)
                        {
                            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha riparato con successo l'applicazione verticale " + app.Label + ".", userId, userName);
                            NetCms.GUI.PopupBox.AddMessage("L'applicazione " + app.Label + " è stata riparata correttamente");
                        }
                        else
                        {
                            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a riparare in modo completo l'applicazione verticale " + app.Label + ".", userId, userName);
                            NetCms.GUI.PopupBox.AddMessage("Non è stato possibile riparare in modo completo l'applicazione " + app.Label);
                        }
                    }
                }

                return repair.Control;
            }
        }
        private HtmlGenericControl UninstallControl
        {
            get
            {
                G2Core.XhtmlControls.InputField uninstall = new G2Core.XhtmlControls.InputField("uninstall_v");
                if (uninstall.RequestVariable.IsValidInteger)//&& applicationsPool.NotInstalledAssemblies.Contains(install.RequestVariable.StringValue))
                {
                    //VerticalApplication app = VerticalApplicationsPool.ActiveAssemblies[uninstall.RequestVariable.StringValue];
                    VerticalApplication app;
                    if (VerticalApplicationsPool.ActiveAssemblies.Contains(uninstall.RequestVariable.StringValue))
                        app = VerticalApplicationsPool.ActiveAssemblies[uninstall.RequestVariable.StringValue];
                    else
                        app = VerticalApplicationsPool.NonActiveAssemblies[uninstall.RequestVariable.StringValue];

                    NetCms.Users.User utente = NetCms.Users.AccountManager.CurrentAccount;
                    string userId = utente.ID.ToString();
                    string userName = utente.UserName;


                    bool done = false;

                    if (app is VerticalApplicationPackage)
                        done = (app as VerticalApplicationPackage).Installer.Uninstall();
                    else
                        done = app.Installer.Uninstall();

                    if (done)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha disinstallato con successo l'applicazione verticale " + app.Label + ".", userId, userName);
                        NetCms.GUI.PopupBox.AddMessage("L'applicazione " + app.Label + " è stata rimossa correttamente");
                        VerticalApplicationsPool.ActiveAssemblies.Remove(app.ID.ToString());
                        VerticalApplicationsPool.NonActiveAssemblies.Add(app);
                        G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
                        cache.RemoveAll("");
                    }
                    else
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha disinstallato senza successo l'applicazione verticale " + app.Label + ".", userId, userName);
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile rimuovere l'applicazione " + app.Label + "!");
                        
                    }
                }
                return uninstall.Control;
            }
        }
        private HtmlGenericControl InstallControl
        {
            get
            {
                G2Core.XhtmlControls.InputField install = new G2Core.XhtmlControls.InputField("install_v");
                if (install.RequestVariable.IsValidInteger)//&& applicationsPool.NotInstalledAssemblies.Contains(install.RequestVariable.StringValue))
                {
                    Vertical.VerticalApplication app = Vertical.VerticalApplicationsPool.NonActiveAssemblies[install.RequestVariable.StringValue];

                    NetCms.Users.User utente = NetCms.Users.AccountManager.CurrentAccount;
                    string userId = utente.ID.ToString();
                    string userName = utente.UserName;

                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta provando ad installare l'applicazione verticale " + app.Label + ".", userId, userName);


                    bool done = false;

                    if (app is VerticalApplicationPackage)
                        done = (app as VerticalApplicationPackage).Installer.Install();
                    else
                        done = app.Installer.Install();

                    if (done)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha installato correttamente l'applicazione verticale " + app.Label + ".", userId, userName);
                        NetCms.GUI.PopupBox.AddMessage("L'applicazione " + app.Label + " è stata installata correttamente");
                        //Le seguenti operazioni sono commentate poichè di default l'applicazione non è abilitata e quindi finchè non viene abilitata
                        //non può essere ammessa tra gli active assemblies
                        //Vertical.VerticalApplicationsPool.NonActiveAssemblies.Remove(app.ID.ToString());
                        //Vertical.VerticalApplicationsPool.ActiveAssemblies.Add(app);
                        G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
                        cache.RemoveAll("");
                    }
                    else
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito ad installare l'applicazione verticale " + app.Label + ".", userId, userName);
                        NetCms.GUI.PopupBox.AddMessage("Non è stato possibile installare l'applicazione " + app.Label);
                        NetCms.GUI.PopupBox.AddMessage(!(app is VerticalApplicationPackage) ? app.Installer.Errors : (app as VerticalApplicationPackage).Installer.Errors);
                    }
                }
                return install.Control;
            }
        }
    }

}