﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetCms.Vertical;
using NetCms.Installation;

namespace NetCms.Structure.Applications
{
    public class InstallerNetworks : SmPageVertical
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.HomeSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return "Gestione Stato Networks"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Plugin; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_Installer"; }
        }

        protected override WebControl RootControl
        {
            get
            {
                VerticalApplication installerApp = VerticalApplicationsPool.GetBySystemName("installer");
                //NetCms.Vertical.Grants.ExternalAppzGrants grants = new NetCms.Vertical.Grants.ExternalAppzGrants("installer");
                if (!installerApp.Grant) NetCms.GUI.PopupBox.AddSessionMessage("Non hai i permessi per accedere alla risorsa.", PostBackMessagesType.Error);

                if (!IsPostBack)
                {
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di CMS di gestione delle applicazioni di portale e verticali.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                }

                WebControl control = new WebControl(HtmlTextWriterTag.Div);
                
                control.Controls.Add(new NetworksControl(NetworksControl.RenderModes.Installer));
                return control;

            }
        }
    }
}