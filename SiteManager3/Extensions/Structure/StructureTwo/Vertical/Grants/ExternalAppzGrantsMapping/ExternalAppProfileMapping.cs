﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Networks.WebFS;
using NHibernate.Collection;
using NetCms.Vertical;

namespace NetCms.Grants.Mapping
{
    public class ExternalAppProfileMapping : SubclassMap<ExternalAppProfile>
    {
        public ExternalAppProfileMapping()
        {
            Table("externalapp_profiles");
            KeyColumn("id_ExternalAppzProfile");
            Map(x => x.Network).Column("Network_ExternalAppzProfile");

            References(x => x.WfsObject).Class(typeof(VerticalApplication))
                 .ForeignKey("object_profiles_externalapp")
                 .Column("Application_ObjectProfile")
                 .Fetch.Select();

            HasMany<ExternalAppGrant>(x => x.CriteriaCollection)
                .KeyColumn("ExternalAppProfile_ObjectProfileCriteria")
                .Fetch.Select().AsSet()
                .Cascade.AllDeleteOrphan().Inverse();
        }
    }
}