﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Grants.Mapping
{
    public class ExternalAppGrantMapping : SubclassMap<ExternalAppGrant>
    {
        public ExternalAppGrantMapping()
        {
            Table("externalapp_grants");
            KeyColumn("id_ExternalAppGrant");

            References(x => x.ObjectProfile).Class(typeof(ExternalAppProfile))
                 .ForeignKey("object_profiles_criteria_externalappprofile")
                 .Column("ExternalAppProfile_ObjectProfileCriteria")
                 .Fetch.Select();

            References(x => x.Criteria).Class(typeof(ExternalAppzCriteria))
                 .ForeignKey("externalapp_profiles_criteria_criterio")
                 .Column("Criteria_ObjectProfileCriteria")
                 .Fetch.Select();
        }
    }
}