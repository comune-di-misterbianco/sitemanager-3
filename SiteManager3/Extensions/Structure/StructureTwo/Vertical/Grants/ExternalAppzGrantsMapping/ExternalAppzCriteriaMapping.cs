﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Grants.Mapping
{
    public class ExternalAppzCriteriaMapping : ClassMap<ExternalAppzCriteria>
    {
        public ExternalAppzCriteriaMapping()
        {
            Table("externalappzcriteria");
            Id(x => x.ID).Column("id_ExternalAppCriteria");
            Map(x => x.Nome).Column("Nome_ExternalAppCriteria");
            Map(x => x.Key).Column("Key_ExternalAppCriteria");
            Map(x => x.IsDynamic).Column("Dynamic_ExternalAppCriteria");

            References(x => x.Application)
                 .ForeignKey("externalapp_criteria")
                 .Column("App_ExternalAppCriteria")
                 .Fetch.Select();
        }
    }
}
