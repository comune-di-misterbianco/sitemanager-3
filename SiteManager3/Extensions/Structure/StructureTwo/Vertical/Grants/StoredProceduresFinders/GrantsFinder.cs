﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using NetCms.Users;
using NetCms.Grants;
using NetCms.Networks.Grants;

namespace NetCms.Vertical.Grants
{
    public class GrantElement : GrantsFinder
    {
        private NetCms.Connections.Connection _Conn;
        private NetCms.Connections.Connection Conn
        {
            get
            {
                //if (_Connection == null)
                //    _Connection = NetCms.Connections.ConnectionsManager.CommonMySqlConnection;
                //return _Connection;
                NetCms.Connections.ConnectionsManager ConnManager = new NetCms.Connections.ConnectionsManager();
                return ConnManager.CommonConnection;
            }
        }
        
        public Networks.NetworkKey NetworkKey { get; private set; }
        public VerticalApplication Application { get; private set; }
        public Profile Profile { get; private set; }


        public Dictionary<string, int> Criteri
        {
            get
            {
                return _Criteri ?? (_Criteri = InitCriteri());
            }
        }

        private Dictionary<string, int> InitCriteri()
        {
            //var dic = new Dictionary<string, int>();

            //string sql = @"SELECT * FROM externalappzcriteria WHERE App_ExternalAppCriteria = " + Application.ID;

            //var result = Connection.SqlQuery(sql);
            //foreach (DataRow row in result.Rows) dic.Add(row["Key_ExternalAppCriteria"].ToString(), int.Parse(row["id_ExternalAppCriteria"].ToString()));

            //return dic;

            var criteri = ExternalAppzGrantsBusinessLogic.FindCriteriByApp(Application.ID);
            return criteri.ToDictionary(x => x.Key, x => x.ID);
        }
        private Dictionary<string, int> _Criteri;

        /// <summary>
        /// GrantElement
        /// </summary>
        /// <param name="application">Application ID or SystemName</param>
        /// <param name="profile">Profile instance</param>
        public GrantElement(string applicationID, Profile profile)
            : base(profile)
        {
            this.Application = Vertical.VerticalApplicationsPool.GetBySystemNameOrID(applicationID);
            this.Profile = profile;
        }

        /// <summary>
        /// GrantElement
        /// </summary>
        /// <param name="application">Application</param>
        /// <param name="profile">Profile instance</param>
        public GrantElement(VerticalApplication application, Profile profile) : base(profile)
        {
            this.Application = application;//Vertical.VerticalApplicationsPool.GetBySystemNameOrID(application);
            this.Profile = profile;
        }

        /// <summary>
        /// GrantElement
        /// </summary>
        /// <param name="application">Application ID or SystemName</param>
        /// <param name="profile">Profile instance</param>
        /// <param name="networkKey">NetCms.Networks.NetworkKey</param>
        public GrantElement(VerticalApplication application, Profile profile, NetCms.Networks.NetworkKey networkKey)
            : base(profile)
        {
            this.Application = application;//Vertical.VerticalApplicationsPool.GetBySystemNameOrID(application);
            this.Profile = profile;
            this.NetworkKey = networkKey;
        }
        
        public override GrantValue this[string criterio]
        {
            get
            {
                NetCms.Grants.GrantValue value;

                if (Criteri.Count > 0)
                {
                    int network = NetworkKey == null ? 0 : NetworkKey.NetworkID;
                    value = this.Profile.IsUser ? GrantValueForUser(criterio, network) :
                                                  GrantValueForGroup(criterio, network);

                    if (!value.SpecifyFound)
                    {
                        value = this.Profile.IsUser ? GrantValueForUser(criterio, 0) :
                                                  GrantValueForGroup(criterio, 0);
                    }
                }
                else
                    value = new GrantValue(GrantsValues.Deny);
                return value;
            }
        }

        public override GrantValue CheckForGrant(string criterio)
        {
            string sql = @"SELECT Value_ObjectProfileCriteria as val 
                           FROM (externalapp_profiles INNER JOIN object_profiles ON object_profiles.id_ObjectProfile = externalapp_profiles.id_ExternalAppzProfile)
                           LEFT JOIN (externalapp_grants INNER JOIN object_profiles_criteria ON externalapp_grants.id_ExternalAppGrant = id_ObjectProfileCriteria) ON id_ExternalAppzProfile = ExternalAppProfile_ObjectProfileCriteria 
                           WHERE Criteria_ObjectProfileCriteria = {0}
                           AND Network_ExternalAppzProfile = {1}
                           AND Profile_ObjectProfile = {2}
                           AND Application_ObjectProfile = {3}";

            var result = Conn.SqlQuery(string.Format(sql,
                                                           Criteri[criterio], 
                                                           this.NetworkKey == null ? "0": this.NetworkKey.NetworkID.ToString(),
                                                           Profile.ID,
                                                           Application.ID
                                                           ));

            return new NetCms.Grants.GrantValue(result == null || result.Rows.Count == 0 ? 0 : int.Parse(result.Rows[0]["val"].ToString()));
        }

        protected override GrantValue GrantValueForGroup(string criterio, int network)
        {
            var parCriterio = Conn.CreateParameter("in_criterio", GetIdCriterio(criterio), ParameterDirection.Input);
            var parNetwork = Conn.CreateParameter("in_network", network, ParameterDirection.Input);
            var parGroup = Conn.CreateParameter("in_group_tree", (Profile as GroupProfile).Group.Tree, ParameterDirection.Input);
            var parValue = Conn.CreateParameter("out_value", 0, ParameterDirection.Output);
            
            var pars = new[] { parCriterio, parNetwork, parGroup, parValue }.ToList();

            var result = Conn.ExecuteCommand("sp_cms_vertical_grant_value_by_group", CommandType.StoredProcedure, pars);

            return new NetCms.Grants.GrantValue((int)parValue.Value);
        }

        protected override int GetIdCriterio(string criterio)
        {

            int idCriterio = 0;
            if (!int.TryParse(criterio, out idCriterio) && Criteri.Count > 0) idCriterio = Criteri[criterio];

            return idCriterio;
        }

        protected override GrantValue GrantValueForUser(string criterio, int network)
        {
            var parCriterio = Conn.CreateParameter("in_criterio", GetIdCriterio(criterio), ParameterDirection.Input);
            var parNetwork = Conn.CreateParameter("in_network", network, ParameterDirection.Input);
            var parUser = Conn.CreateParameter("in_user", (Profile as UserProfile).User.ID, ParameterDirection.Input);
            var parValue = Conn.CreateParameter("out_value", null, ParameterDirection.Output);

            var pars = new[] { parCriterio, parNetwork, parUser, parValue }.ToList();

            var result = Conn.ExecuteCommand("sp_cms_vertical_grant_value_by_user", CommandType.StoredProcedure, pars);

            return new NetCms.Grants.GrantValue(parValue.Value != DBNull.Value ? (int)parValue.Value : 0);
        }

        public bool Grant
        {
            get
            {
                return this["enable"].Value == NetCms.Grants.GrantsValues.Allow;
            }
        }

        public override GrantValue NeedToShow(string criterio, int network = 0)
        {
            throw new NotImplementedException();
        }
    }

    public class CurrentGrantElement:GrantElement
    {
        /// <summary>
        /// GrantElement
        /// </summary>
        /// <param name="application">Application ID or SystemName</param>
        /// <param name="profile">Profile instance</param>
        public CurrentGrantElement(VerticalApplication application, Profile profile)
            : base(application, profile, NetCms.Networks.NetworksManager.CurrentActiveNetwork.CacheKey)
        {
        }

        /// <summary>
        /// GrantElement
        /// </summary>
        /// <param name="application">Application ID or SystemName</param>
        public CurrentGrantElement(VerticalApplication application)
            : base(application, NetCms.Users.AccountManager.CurrentAccount.Profile, NetCms.Networks.NetworksManager.CurrentActiveNetwork.CacheKey)
        {
        }

        /// <summary>
        /// GrantElement
        /// </summary>
        /// <param name="application">Application ID or SystemName</param>
        /// <param name="profile">Profile instance</param>        /// <param name="networkKey">NetCms.Networks.NetworkKey</param>
        public CurrentGrantElement(VerticalApplication application, NetCms.Networks.NetworkKey networkKey)
            : base(application, NetCms.Users.AccountManager.CurrentAccount.Profile)
        {
        }
    }
}
