﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Structure.WebFS;
using NetCms.Grants.Interfaces;
using NetCms.Grants;

namespace NetCms.Vertical
{
    public class ApplicationProfileAssociation : ObjectProfileAssociation
    {
        public int ApplicationID
        {
            get { return _ApplicationID; }
            private set { _ApplicationID = value; }
        }
        private int _ApplicationID;

        public Networks.NetworkKey NetworkKey
        {
            get
            {
                return _NetworkKey;
            }
            private set { _NetworkKey = value; }
        }
        private Networks.NetworkKey _NetworkKey;

        public bool NetworkDependant
        {
            get { return _NetworkDependant; }
            private set { _NetworkDependant = value; }
        }
        private bool _NetworkDependant;

        public ApplicationProfileAssociation(IGrantsFindable Object, int associationID)
            :base(Object, associationID)
        {
            if (Object.GrantObjectProfiles != null)
            {
                var associations = Object.GrantObjectProfiles.Where(x => x.ID == associationID);
                if (associations.Count() > 0)
                {
                    
                    ExternalAppProfile associationProfile = associations.Cast<ExternalAppProfile>().First();
                    this.ApplicationID = associationProfile.WfsObject.ID;
                    this.NetworkDependant = (associationProfile.WfsObject as VerticalApplication).NetworkDependant;
                    if (NetworkDependant && associationProfile.Network > 0)
                        this.NetworkKey = new NetCms.Networks.NetworkKey(associationProfile.Network);
                    
                }
            }
        }
    }
}
