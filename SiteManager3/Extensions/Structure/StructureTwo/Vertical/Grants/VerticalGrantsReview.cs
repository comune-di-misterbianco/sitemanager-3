﻿using System;
using System.Web;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Linq;
using NetCms.GUI;
using NetCms.Grants;
using NetCms.Vertical.BusinessLogic;

namespace NetCms.Vertical.Grants
{
    public class VericalGrantsReview: SmPageVertical
    {
        public override string PageTitle
        {
            get { return "Riepilogo permessi relativi alle applicazioni verticali per " + this.Profile.EntityLabel; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Key; }
        }

        public override string LocalCssClass
        {
            get { return string.Empty; }
        }

        public G2Core.Common.RequestVariable SelectedNetwork
        {
            get
            {
                if (_SelectedNetwork == null)
                    _SelectedNetwork = new G2Core.Common.RequestVariable("nid", G2Core.Common.RequestVariable.RequestType.QueryString);
                return _SelectedNetwork;
            }
        }
        private G2Core.Common.RequestVariable _SelectedNetwork;

        public G2Core.Common.RequestVariable NotInNetwork
        {
            get
            {
                if (_NotInNetwork == null)
                    _NotInNetwork = new G2Core.Common.RequestVariable("show", G2Core.Common.RequestVariable.RequestType.QueryString);
                return _NotInNetwork;
            }
        }
        private G2Core.Common.RequestVariable _NotInNetwork;

        public bool NetworkDependant
        {
            get
            {
                return SelectedNetwork.IsValidInteger && SelectedNetwork.IntValue > 0;
            }
        }

        #region Profile

        public G2Core.Common.RequestVariable ProfileID
        {
            get
            {
                if (_ProfileID == null)
                    _ProfileID = new G2Core.Common.RequestVariable("pid", G2Core.Common.RequestVariable.RequestType.QueryString);
                return _ProfileID;
            }
        }
        private G2Core.Common.RequestVariable _ProfileID;

        public G2Core.Common.RequestVariable AppID
        {
            get
            {
                if (_AppID == null)
                    _AppID = new G2Core.Common.RequestVariable("app", G2Core.Common.RequestVariable.RequestType.QueryString);
                return _AppID;
            }
        }
        private G2Core.Common.RequestVariable _AppID;

        private NetCms.Users.Profile _Profile;
        private NetCms.Users.Profile Profile
        {
            get
            {
                if (_Profile == null)
                    //_Profile = new NetCms.Users.Profile(this.ProfileID.StringValue);
                    _Profile = Users.ProfileBusinessLogic.GetById(this.ProfileID.IntValue);
                return _Profile;
            }
        }

        #endregion

        public VericalGrantsReview()
        {
            VerticalApplication grantsApp = VerticalApplicationsPool.GetByID(NetCms.Users.VerticalGrantsApplication.ApplicationID);
            //ExternalApplications.ExternalApplication app = new ExternalApplications.ExternalApplication(NetCms.Users.VerticalGrantsApplication.ApplicationID);
            if (!grantsApp.Grant)
                PopupBox.AddNoGrantsMessageAndGoBack();
        }

        protected override WebControl RootControl
        {
            get { return GetApplicationsGrantsReview(); }
        }

        public WebControl GetApplicationsGrantsReview()
        {
            #region Info
            WebControl output = new WebControl(HtmlTextWriterTag.Div);
            string title = "Permessi Generici per le Applicazioni";            
            if (NetworkDependant)
            {
                title = "Permessi per le Applicazioni del Portale '" + NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks[NetCms.Networks.NetworksManager.GlobalIndexer.NetworkKeys[SelectedNetwork.IntValue]].Label + "'";
            }
            Collapsable col = new Collapsable(title);

            G2Core.UI.DetailsSheet sheet = new G2Core.UI.DetailsSheet("Informazioni", "Questa pagina contiene l'elenco dei permessi impostati per le Applicazioni Verticali, relativamente all'utente '" + this.Profile.EntityName + "'" + ".");
            sheet.SeparateColumns = G2Core.UI.DetailsSheet.SeparateColumnsOptions.No;
            sheet.AddColumn("Seleziona un'applicazione per vedere la tabella dei permessi.", G2Core.UI.DetailsSheet.Colors.Default, 2);

            //string sql = "SELECT * FROM externalappz";
            //if (NetworkDependant)
            //{   
            //    sql += " WHERE NetworkDependant_ExternalApp = 1";
            //}

            //Seleziono tutte le network che sono installate, anzichè come faceva prima selezionandole tutte
            var externalAppz = VerticalAppBusinessLogic.FindAllVerticalApplications().Where(x => VerticalApplicationsPool.ActiveAssemblies.Contains(x.ID));
            //Se è attivo il flag network dependant le filtro
            if (NetworkDependant)
                externalAppz = externalAppz.Where(x => x.NetworkDependant);

            string qsLink = "";
            if (!NetworkDependant && NotInNetwork.IsValidString && NotInNetwork.StringValue.Length > 0 && NotInNetwork.StringValue == "outside")
            {
                qsLink = "&show=outside";
                externalAppz = externalAppz.Where(x => !x.NetworkDependant);
            }

            //DataTable table = Connection.SqlQuery(sql);
            
            //foreach (DataRow row in table.Rows)
            foreach(VerticalApplication app in externalAppz)
            {
                TableRow l2row = new TableRow();

                TableCell cell2 = new TableCell();
                cell2.Text = "<a class=\"action key\" href=\"review.aspx?pid=" + Profile.ID + "&amp;app=" + app.ID + (NetworkDependant ? "&amp;nid=" + this.SelectedNetwork.StringValue : "") + qsLink + "\"><span>" + app.Label + "</span></a>";
                cell2.ColumnSpan = 2;
                l2row.Cells.Add(cell2);

                sheet.AddRow(l2row, G2Core.UI.DetailsSheet.Colors.Default);
            }

            col.AppendControl(sheet);
            output.Controls.Add(col);
            #endregion
            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
            if (this.AppID.IsValidInteger)
            {
                //DataTable atable = Connection.SqlQuery("SELECT * FROM externalappz WHERE id_ExternalApp = " + AppID.StringValue);
                VerticalApplication app = VerticalAppBusinessLogic.GetVerticalAppById(AppID.IntValue);
                //if (atable.Rows.Count > 0)
                if (app != null)
                {
                    output.Controls.Add(GetApplicationGrantsReview(app));
                    output.Controls.Add(new LiteralControl("<p>&nbsp;</p>"));
                    if (NetworkDependant)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta visualizzando i permessi per l'Applicazione " + app.Label + " del Portale " + NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks[NetCms.Networks.NetworksManager.GlobalIndexer.NetworkKeys[SelectedNetwork.IntValue]].Label + ", relativamente " + this.Profile.EntityLabel + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    }
                    else 
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta visualizzando i permessi per l'Applicazione " + app.Label + ", relativamente " + this.Profile.EntityLabel + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    }
                }
            }
            else if (NetworkDependant)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina contenente l'elenco dei permessi per le Applicazioni Verticali del Portale " + NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks[NetCms.Networks.NetworksManager.GlobalIndexer.NetworkKeys[SelectedNetwork.IntValue]].Label + ", relativamente " + this.Profile.EntityLabel + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            }
            else {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina contenente l'elenco dei permessi per le Applicazioni Verticali, relativamente " + this.Profile.EntityLabel + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            }
            return output;
        }

        public WebControl GetApplicationGrantsReview(VerticalApplication application)
        {
            Collapsable collasable = new Collapsable(application.Label);

            string Utente = this.Account.Label;
            string logText = "L'utente " + Utente;

            HtmlGenericControl tabella = new HtmlGenericControl("table");
            HtmlGenericControl riga = new HtmlGenericControl("tr");
            HtmlGenericControl cell;

            tabella = new HtmlGenericControl("table");
            riga = new HtmlGenericControl("tr");

            tabella.ID = "GrantReviewTable";
            tabella.Attributes["class"] = "GrantReviewTable";
            tabella.Attributes["cellspacing"] = "0";
            tabella.Attributes["cellpadding"] = "0";

            riga = new HtmlGenericControl("tr");
            tabella.Controls.Add(riga);

            cell = new HtmlGenericControl("th");
            cell.InnerHtml = "Criterio";
            riga.Controls.Add(cell);

            cell = new HtmlGenericControl("th");
            cell.InnerHtml = "Specifica";
            riga.Controls.Add(cell);

            cell = new HtmlGenericControl("th");
            cell.InnerHtml = "Permesso Registrato";
            riga.Controls.Add(cell);

            cell = new HtmlGenericControl("th");
            cell.InnerHtml = "Permesso Calcolato";
            riga.Controls.Add(cell);

            //string sql = "SELECT * FROM externalappzcriteria WHERE App_ExternalAppCriteria = " + applicationID;

            IList<ExternalAppzCriteria> criteri = ExternalAppzGrantsBusinessLogic.FindCriteriByApp(application.ID);

            //DataTable table = Connection.SqlQuery(sql);

            //foreach (DataRow row in table.Rows)
            foreach(ExternalAppzCriteria criterio in criteri)
            {
                riga = BuildCrieriaRow(criterio,application);
                tabella.Controls.Add(riga);
                riga.Attributes["class"] += "alternate";
            }
            collasable.AppendControl(tabella);
            return collasable;
        }

        public HtmlGenericControl BuildCrieriaRow(ExternalAppzCriteria criterio,VerticalApplication app)
        {
            string criteraKey = criterio.Key;//record["Key_ExternalAppCriteria"].ToString();
            
            GrantElement element;

            if (SelectedNetwork.IsValidInteger && SelectedNetwork.IntValue > 0)
                element = new GrantElement(app, this.Profile, new Networks.NetworkKey(this.SelectedNetwork.IntValue));
            else
                element = new GrantElement(app, this.Profile);



            GrantValue notInheritedSpecify = element.CheckForGrant(criteraKey);
            GrantValue inheritedSpecify = element[criteraKey];

            #region Riga
            HtmlGenericControl riga = new HtmlGenericControl("tr");

            HtmlGenericControl cell;
            riga = new HtmlGenericControl("tr");

            #endregion

            #region Cella Nome

            cell = new HtmlGenericControl("td");
            cell.InnerHtml += "<span>" + criterio.Nome + "</span>";
            cell.Attributes["style"] = "padding:4px;";
            riga.Controls.Add(cell);

            #endregion

            cell = new HtmlGenericControl("td");

            string link = "";

            /*if (inheritedSpecify.Value != GrantsValues.NotSpecified)
            {
                if (inheritedSpecify.Profile.IsUser)
                    cell.InnerHtml = "<a class=\"GrantsReview_Icon GrantsReview_User\" href=\"" + link + "\"><span><strong>Utente</strong></span></a>";
                else
                    cell.InnerHtml += "<a class=\"GrantsReview_Icon GrantsReview_Group\" href=\"" + link + "\"><span><strong>Gruppo '" + inheritedSpecify.Profile.EntityName + "'</strong></span></a>";
            }
            else
                cell.InnerHtml += "<a class=\"GrantsReview_Icon GrantsReview_Void\" href=\"" + link + "\"><span>Nessuna -> Crea</span></a>";
            */

            if (inheritedSpecify.Value != GrantsValues.NotSpecified)
            {
                if (element.Profile.IsUser)
                    cell.InnerHtml = "<a class=\"GrantsReview_Icon GrantsReview_User\"><span><strong>Utente</strong></span></a>";
                else
                    cell.InnerHtml += "<a class=\"GrantsReview_Icon GrantsReview_Group\"><span><strong>Gruppo '" + element.Profile.EntityName + "'</strong></span></a>";
            }
            else
                cell.InnerHtml += "<a class=\"GrantsReview_Icon GrantsReview_Void\"><span>Nessuna</span></a>";
            

            riga.Controls.Add(cell);

            cell = new HtmlGenericControl("td");
            cell.InnerHtml = ShowIcon(notInheritedSpecify.Value);
            riga.Controls.Add(cell);

            string valueToShow = inheritedSpecify.Allowed ? "Consenti" : "Nega";
            
            cell = new HtmlGenericControl("td");
            cell.InnerHtml = ShowIcon(inheritedSpecify.Value == GrantsValues.Allow ? GrantsValues.Allow: GrantsValues.Deny);
            riga.Controls.Add(cell);

            return riga;
        }

        private string ShowIcon(GrantsValues value)
        {
            string output = "<span class=\"GrantsReview_Icon GrantsReview_{0}\"><span>{1}</span></span>";
            switch (value)
            {
                case GrantsValues.Inherits:
                    return string.Format(output, "Inherits", "Eredita");
                case GrantsValues.Allow:
                    return string.Format(output, "Allowed", "Permesso");
                case GrantsValues.Deny:
                    return string.Format(output, "NotAllowed", "NON Permesso");
                default:
                    return string.Format(output, "NoRecord", "Nessuno -> Eredita");
            }
        }
    }
}
