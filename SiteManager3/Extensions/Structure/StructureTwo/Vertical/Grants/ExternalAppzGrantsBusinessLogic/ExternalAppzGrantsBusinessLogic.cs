﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NetCms.DBSessionProvider;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;

namespace NetCms.Grants
{
    public static class ExternalAppzGrantsBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static IList<ExternalAppzCriteria> FindAllCriteri(ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ExternalAppzCriteria> dao = new CriteriaNhibernateDAO<ExternalAppzCriteria>(innerSession);
            return dao.FindAll();
        }

        public static IList<ExternalAppzCriteria> FindCriteriByApp(int vappId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            SearchParameter vappSP = new SearchParameter(null, "Application.ID", vappId, Finder.ComparisonCriteria.Equals, false);

            IGenericDAO<ExternalAppzCriteria> dao = new CriteriaNhibernateDAO<ExternalAppzCriteria>(innerSession);
            return dao.FindByCriteria(new SearchParameter[] { vappSP });
        }

        public static ExternalAppzCriteria GetCriterioByKey(string key, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            SearchParameter keySP = new SearchParameter(null, "Key", key, Finder.ComparisonCriteria.Equals, false);

            IGenericDAO<ExternalAppzCriteria> dao = new CriteriaNhibernateDAO<ExternalAppzCriteria>(innerSession);
            return dao.GetByCriteria(new SearchParameter[] { keySP });
        }

        public static ExternalAppzCriteria GetCriterioByAppAndKey(string key, int vappId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            SearchParameter vappSP = new SearchParameter(null, "Application.ID", vappId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter keySP = new SearchParameter(null, "Key", key, Finder.ComparisonCriteria.Equals, false);

            IGenericDAO<ExternalAppzCriteria> dao = new CriteriaNhibernateDAO<ExternalAppzCriteria>(innerSession);
            return dao.GetByCriteria(new SearchParameter[] { keySP, vappSP });
        }

        public static ExternalAppzCriteria GetCriterioById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ExternalAppzCriteria> dao = new CriteriaNhibernateDAO<ExternalAppzCriteria>(innerSession);
            return dao.GetById(id);
        }

        public static IList<ExternalAppProfile> GetExternalAppProfiles(int vappId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ExternalAppProfile> dao = new CriteriaNhibernateDAO<ExternalAppProfile>(innerSession);
            SearchParameter vappSP = new SearchParameter(null, "WfsObject.ID", vappId, Finder.ComparisonCriteria.Equals, false);
            
            return dao.FindByCriteria(new SearchParameter[] { vappSP });
        }

        public static ExternalAppProfile GetExternalAppProfileByAppAndProfile(int vappId,int profileId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ExternalAppProfile> dao = new CriteriaNhibernateDAO<ExternalAppProfile>(innerSession);
            SearchParameter vappSP = new SearchParameter(null, "WfsObject.ID", vappId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter proSP = new SearchParameter(null, "Profile.ID", profileId, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { vappSP, proSP });
        }

        public static void DeleteExternalAppProfileByAppAndProfile(int vappId, int profileId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ExternalAppProfile> dao = new CriteriaNhibernateDAO<ExternalAppProfile>(innerSession);
            SearchParameter vappSP = new SearchParameter(null, "WfsObject.ID", vappId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter proSP = new SearchParameter(null, "Profile.ID", profileId, Finder.ComparisonCriteria.Equals, false);

            ExternalAppProfile vappProfile = dao.GetByCriteria(new SearchParameter[] { vappSP, proSP });
           if (vappProfile!=null)
                dao.Delete(vappProfile);
        }

        public static ExternalAppzCriteria SaveOrUpdateExternalAppCriteria(ExternalAppzCriteria vappCriteria, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ExternalAppzCriteria> dao = new CriteriaNhibernateDAO<ExternalAppzCriteria>(innerSession);
            return dao.SaveOrUpdate(vappCriteria);
        }

        public static ExternalAppProfile SaveOrUpdateExternalAppProfile(ExternalAppProfile vappProfile, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ExternalAppProfile> dao = new CriteriaNhibernateDAO<ExternalAppProfile>(innerSession);
            return dao.SaveOrUpdate(vappProfile);
        }

        public static ExternalAppGrant SaveOrUpdateExternalAppGrant(ExternalAppGrant vappProfileCriteria, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ExternalAppGrant> dao = new CriteriaNhibernateDAO<ExternalAppGrant>(innerSession);
            return dao.SaveOrUpdate(vappProfileCriteria);
        }


        public static void DeleteExternalAppProfile(int vappProfileId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ExternalAppProfile> dao = new CriteriaNhibernateDAO<ExternalAppProfile>(innerSession);

            ExternalAppProfile vappProfile = dao.GetById(vappProfileId);

            dao.Delete(vappProfile);
        }


        public static ExternalAppProfile GetExternalAppProfileById(int vappProfileId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ExternalAppProfile> dao = new CriteriaNhibernateDAO<ExternalAppProfile>(innerSession);

            return dao.GetById(vappProfileId);
        }

        public static bool CheckIfProfileIsAssociatedToVerticalApp(int appId, int profileId, int network, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ExternalAppProfile> dao = new CriteriaNhibernateDAO<ExternalAppProfile>(innerSession);
            SearchParameter folderSP = new SearchParameter(null, "WfsObject.ID", appId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter profileSP = new SearchParameter(null, "Profile.ID", profileId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter networkSP = new SearchParameter(null, "Network", network, Finder.ComparisonCriteria.Equals, false);

            ExternalAppProfile founded = dao.GetByCriteria(new SearchParameter[] { folderSP, profileSP, networkSP });

            return founded != null;
        }

        public static bool CheckIfProfileIsAssociatedToVerticalApp(int appId, int profileId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ExternalAppProfile> dao = new CriteriaNhibernateDAO<ExternalAppProfile>(innerSession);
            SearchParameter folderSP = new SearchParameter(null, "WfsObject.ID", appId, Finder.ComparisonCriteria.Equals, false);
            SearchParameter profileSP = new SearchParameter(null, "Profile.ID", profileId, Finder.ComparisonCriteria.Equals, false);
            

            ExternalAppProfile founded = dao.GetByCriteria(new SearchParameter[] { folderSP, profileSP });

            return founded != null;
        }

        public static void DeleteExternalAppProfilesByNetwork(int networkId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ExternalAppProfile> dao = new CriteriaNhibernateDAO<ExternalAppProfile>(innerSession);
            SearchParameter netSP = new SearchParameter(null, "Network", networkId, Finder.ComparisonCriteria.Equals, false);


            IList<ExternalAppProfile> vappProfiles = dao.FindByCriteria(new SearchParameter[] { netSP });
            foreach(ExternalAppProfile profile in vappProfiles)
                dao.Delete(profile);
        }

    }
}
