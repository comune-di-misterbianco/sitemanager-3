﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Grants.Interfaces;
using NetCms.Vertical;

namespace NetCms.Grants
{
    public class ExternalAppzCriteria : IGrantsCriteria
    {
        public ExternalAppzCriteria()
        { }

        #region Proprietà da mappare

        public virtual int ID { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Key { get; set; }

        public virtual VerticalApplication Application { get; set; }

        public virtual bool IsDynamic { get; set; }

        #endregion
    }
}
