﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Grants
{
    public class ExternalAppGrant : ObjectProfileCriteria
    {
        public ExternalAppGrant()
            : base()
        {}

        public override ObjectProfile ObjectProfile
        {
            get
            {
                return _ObjectProfile;
            }
            set
            {
                _ObjectProfile = value as ExternalAppProfile;
            }
        }
        private ExternalAppProfile _ObjectProfile;
    }
}