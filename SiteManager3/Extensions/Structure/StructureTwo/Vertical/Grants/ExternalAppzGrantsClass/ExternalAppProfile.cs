﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Networks;

namespace NetCms.Grants
{
    public class ExternalAppProfile : ObjectProfile
    {
        public ExternalAppProfile()
            : base()
        {
            CriteriaCollection = new HashSet<ObjectProfileCriteria>();
        }

        public override ICollection<ObjectProfileCriteria> CriteriaCollection
        {
            get;
            
            set;
            
        }

        public virtual int Network
        { get; set; }
    }
}