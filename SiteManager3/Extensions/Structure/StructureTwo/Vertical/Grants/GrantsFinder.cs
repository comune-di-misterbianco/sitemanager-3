﻿//using System;
//using System.IO;
//using System.Web;
//using System.Data;
//using System.Xml;
//using System.Collections;
//using System.Collections.Generic;

//namespace NetCms.Users
//{
//    public enum GrantsValues { NotSpecified,Allow,Deny,Inherits }

//    public class GrantElement:IGrantElement
//    {
//        private NetCms.Connections.Connection _Connection;
//        private NetCms.Connections.Connection Connection
//        {
//            get
//            {
//                if (_Connection == null)
//                    _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
//                return _Connection;
//            }
//        }

//        public ApplicationProfileAssociation Association
//        {
//            get { return _Association; }
//            private set { _Association = value; }
//        }
//        private ApplicationProfileAssociation _Association;
        
//        public Profile Profile
//        {
//            get { return this.Association.Profile; }
//        }
        
//        public List<IGrantElement> ParentsElements
//        {
//            get 
//            {
//                if (_ParentsElements == null)
//                {
//                    _ParentsElements = new List<IGrantElement>();
//                    if (Profile != null)
//                        foreach (Group group in Profile.ParentsGroups)
//                        _ParentsElements.Add(new GrantElement(new ApplicationProfileAssociation(this.Association.ApplicationID, group.Profile, this.Association.NetworkKey)));
//                }
//                return _ParentsElements; 
//            }
//        }
//        private List<IGrantElement> _ParentsElements;

//        /*
//        public bool AssociationFound
//        {
//            get { return this.Association.AssociationFound; }
//        }
//        */
//        public  Networks.NetworkKey  NetworkKey
//        {
//            get
//            {
//                return this.Association.NetworkKey;
//            }
//        }

//        /// <summary>
//        /// Restituisce i permessi relativi ad un criterio di un applicazione
//        /// </summary>
//        public GrantElement(ApplicationProfileAssociation association)
//        {
//            this.Association = association;
//        }
//        /// <summary>
//        /// Restituisce i permessi relativi ad un criterio di un applicazione
//        /// </summary>
//        /// <param name="application">ID o Key dell'applicazione di cui controllare i permessi</param>
//        /// <param name="Profile">Profile del gruppo o dell'utente  di cui controllare i permessi</param>
//        /// <param name="criterio">ID o Key del criterio di cui controllare i permessi</param>
//        public GrantElement(string application,Profile Profile)
//            : this(new ApplicationProfileAssociation(application, Profile, null))
//        {
//        }
//        /// <summary>
//        /// Restituisce i permessi relativi ad un criterio di un applicazione
//        /// </summary>
//        /// <param name="application">ID o Key dell'applicazione di cui controllare i permessi</param>
//        /// <param name="Profile">Profile del gruppo o dell'utente  di cui controllare i permessi</param>
//        /// <param name="criterio">ID o Key del criterio di cui controllare i permessi</param>
//        /// <param name="networkKey">NetCms.Networks.NetworkKey relativo alla rete nel quale cercare i permessi</param>
//        public GrantElement(string application, Profile Profile, NetCms.Networks.NetworkKey networkKey)
//            : this(new ApplicationProfileAssociation(application, Profile, networkKey))
//        {
//        }

//        public virtual GrantValue CheckForGrant(string criterio)
//        {
//            if (Association.AssociationFound)
//            {
//                string sql = "SELECT * FROM externalappzgrants INNER JOIN externalappzcriteria ON id_ExternalAppCriteria = Criteria_ExternalAppGrant";

//                DataTable table = Connection.SqlQuery(sql);

//                string sqlect = "ProfileAssociation_ExternalAppGrant = " + Association.ID + "";

//                int criteriaID = 0;
//                if (int.TryParse(criterio, out criteriaID)) sqlect += " AND (Criteria_ExternalAppGrant =  " + criterio + ")";
//                else sqlect += " AND Key_ExternalAppCriteria LIKE '" + criterio + "'";

//                DataRow[] rows = table.Select(sqlect);
//                if (rows.Length > 0)
//                {
//                    switch (rows[0]["Value_ExternalAppGrant"].ToString())
//                    {
//                        case "1": return new GrantValue(GrantsValues.Allow, this.Profile); ;
//                        case "2": return new GrantValue(GrantsValues.Deny, this.Profile); ;
//                        default: return new GrantValue(GrantsValues.Inherits, this.Profile); ;
//                    }
//                }
//            }
//            return new GrantValue(GrantsValues.NotSpecified, this.Profile);
//        }

//        public GrantValue this[string criterio]
//        {
//            get
//            {
//                GrantValue value = GrantValue(criterio);
//                if (!value.SpecifyFound && this.Association.NetworkKey != null)
//                {
//                    GrantElement globalPermissions = new GrantElement(this.Association.ApplicationID, this.Association.Profile);
//                    value = globalPermissions[criterio];
//                }

//                if (!value.SpecifyFound && this.ParentsElements.Count == 0)
//                {
//                    value = new GrantValue(GrantsValues.Deny, this.Profile);
//                }

//                return value;
//            }
//        }

//        public GrantValue GrantValue(string criterio)
//        {
//            GrantValue localValue = CheckForGrant(criterio);
//            if (!localValue.SpecifyFound)
//            {
//                GrantsValues reference = NetCms.Configurations.Generics.MostValuableGrant ? GrantsValues.Allow : GrantsValues.Deny;//Se impostato a true la ricerca continuerà fino a che non viene trovata una impostazione del premesso a true
//                foreach (IGrantElement element in this.ParentsElements)
//                {
//                    //Prima cerco nei permessi specifici della network
//                    GrantValue grant = element.GrantValue(criterio);
//                    if (grant.SpecifyFound &&
//                            (
//                                (!localValue.SpecifyFound) ||
//                                (grant.Weight > localValue.Weight) ||
//                                (grant.Weight == localValue.Weight && reference == grant.Value)
//                            )
//                        ) localValue = grant;
//                }                
//            }

//            return localValue;
//        }

//        public bool Grant
//        {
//            get
//            {
//                return this["enable"].Value == GrantsValues.Allow;
//            }
//        }
//    }
//}
