﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;
using NetUtility.NetTreeViewControl;
using NetCms.GUI;
using NetTable;
using NetCms.Structure.Grants;
using NetCms.Vertical;
using NetCms.Grants;
using NetCms.Vertical.Grants;
using NetCms.Networks.Grants;
using NetCms.Vertical.BusinessLogic;

namespace NetCms.Users
{
    public class CriteriaOverview:WebControl
    {
        //private NetCms.Connections.Connection _Connection;
        //private NetCms.Connections.Connection Connection
        //{
        //    get
        //    {
        //        if (_Connection == null)
        //            _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
        //        return _Connection;
        //    }
        //}
        
        public ApplicationProfileAssociation Association
        {
            get { return _Association; }
            private set { _Association = value; }
        }
        private ApplicationProfileAssociation _Association;
        
        //public CriteriaOverview(string associationID)
        //    : base(HtmlTextWriterTag.Div)
        //{
        //    this.Association = new ApplicationProfileAssociation(associationID);
        //    CheckGrants();
        //}

        public CriteriaOverview(ApplicationProfileAssociation association)
            : base(HtmlTextWriterTag.Div)
        {
            this.Association = association;
            CheckGrants();
        }

        public VerticalApplication ApplicationLocal
        {
            get
            {
                if (_ApplicationLocal == null)
                    _ApplicationLocal = Association.GrantObject as VerticalApplication;//VerticalApplicationsPool.GetByID(Association.ApplicationID);
                return _ApplicationLocal;
            }
        }
        private VerticalApplication _ApplicationLocal;

        public void CheckGrants()
        {
            VerticalApplication grantsApp = VerticalApplicationsPool.GetByID(NetCms.Users.VerticalGrantsApplication.ApplicationID);
            //ExternalApplications.ExternalApplication app = new ExternalApplications.ExternalApplication(NetCms.Users.VerticalGrantsApplication.ApplicationID);
            if (!grantsApp.Grant)
                PopupBox.AddNoGrantsMessageAndGoBack();

            //ExternalApplications.ExternalApplication appLocal = new ExternalApplications.ExternalApplication(int.Parse(Association.ApplicationID));
            if (!ApplicationLocal.Criteria["grants"].Allowed)
                PopupBox.AddNoGrantsMessageAndGoBack();
        }

        //public DataTable CriteriaTable
        //{
        //    get
        //    {
        //        if (_CriteriaTable == null)
        //        {
        //            _CriteriaTable = this.Connection.SqlQuery("SELECT * FROM externalappzcriteria WHERE App_ExternalAppCriteria = " + Association.ApplicationID);
        //        }
        //        return _CriteriaTable;
        //    }
        //}
        //private DataTable _CriteriaTable;


        //public DataTable GrantsTable
        //{
        //    get
        //    {
        //        if (_GrantsTable == null)
        //            _GrantsTable = this.Connection.SqlQuery("SELECT * FROM externalappzgrants WHERE ProfileAssociation_ExternalAppGrant = " + Association.ID);
        //        return _GrantsTable;
        //    }
        //}
        //private DataTable _GrantsTable;
        
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.Controls.Add(Overview());

            if (VerticalApplicationsPool.ActiveAssemblies[ApplicationLocal.ID.ToString()] is VerticalApplicationPackage)
            {
                VerticalApplicationPackage vAppPack = VerticalApplicationsPool.ActiveAssemblies[ApplicationLocal.ID.ToString()] as VerticalApplicationPackage;
                foreach (int id in vAppPack.ContainedApplicationsIDs)
                {
                    VerticalApplication vApp = VerticalAppBusinessLogic.GetVerticalAppById(id);
                    GUI.Collapsable collapsable = new NetCms.GUI.Collapsable("Riepilogo permessi per l'applicazione '" + vApp.Label + "'");
                    collapsable.StartCollapsed = true;
                    collapsable.AppendControl(Overview(vApp));
                    this.Controls.Add(collapsable);
                }
            }
        }

        private WebControl Overview()
        {
            G2Core.UI.DetailsSheet details = new G2Core.UI.DetailsSheet("Riepilogo dei permessi", @"Questa schermata mostra i permessi relativi all'applicazione per " + this.Association.Profile.EntityLabel + @". La voce 'Permesso Registrato' contiene il valore attualmente registrato. La voce 'Permesso Eleborato' contiene il risultato dell'elaborazione dello specifico permesso da parte del sistema tenendo conto anche delle eredità dei gruppi.");

            details.AddColumn("Criterio", G2Core.UI.DetailsSheet.Colors.Default);
            details.AddColumn("Permesso Registrato", G2Core.UI.DetailsSheet.Colors.Default);
            details.AddColumn("Permesso Elaborato SP", G2Core.UI.DetailsSheet.Colors.Default);
            //if(NetCms.Configurations.Debug.GenericEnabled)
            //    details.AddColumn("Permesso Elaborato New", G2Core.UI.DetailsSheet.Colors.Default);

            TableRow row;

            //foreach (DataRow criteriaRow in this.CriteriaTable.Rows)
            foreach(ExternalAppzCriteria criteria in ApplicationLocal.AppzCriteria)
            {
                row = new TableRow();

                TableCell cell = new TableCell();
                cell.Text = criteria.Nome; //criteriaRow["Nome_ExternalAppCriteria"].ToString();
                row.Cells.Add(cell);

                string value = "";

                //string sql = "Criteria_ExternalAppGrant = " + criteriaRow["id_ExternalAppCriteria"] + " AND ProfileAssociation_ExternalAppGrant = " +this.Association.ID;
                //DataRow[] rows = GrantsTable.Select(sql);

                GrantsFinder grant = ApplicationLocal.CriteriaByProfileAndNetwork(Association.Profile, Association.NetworkKey);
                GrantValue dbValue = grant.CheckForGrant(criteria.Key); 

                cell = new TableCell();
                //string dbvalue = rows.Length>0? rows[0]["Value_ExternalAppGrant"].ToString():"0";
                switch (dbValue.Value)
                {
                    case  GrantsValues.Allow : cell.Text = "Consenti"; cell.CssClass = "fill_green"; break;
                    case  GrantsValues.Deny : cell.Text = "Nega"; cell.CssClass = "fill_red"; break;
                    default: cell.Text = "Eredita"; cell.CssClass = "fill_yellow"; break;
                }

                row.Cells.Add(cell);

                value = "";
                //cell = new TableCell();
                //GrantElement element = new GrantElement(Association);
                //switch(element[ criteriaRow["id_ExternalAppCriteria"].ToString()].Value)
                //{
                //    case GrantsValues.Allow: value += "Consenti"; cell.CssClass = "fill_green"; break;
                //    case GrantsValues.Deny: value += "Nega"; cell.CssClass = "fill_red"; break;
                //    default:
                //        if (NetCms.Configurations.Generics.MostValuableGrant)
                //        {
                //            value += "Consenti"; 
                //            cell.CssClass = "fill_green"; 
                //        }
                //        else
                //        {
                //            value += "Nega";
                //            cell.CssClass = "fill_red";
                //        }
                //        break;
                //}

                //cell.Text = value;
                //row.Cells.Add(cell);

                //if (NetCms.Configurations.Debug.GenericEnabled)
                //{
                    cell = new TableCell();
                    //var appName = Vertical.VerticalApplicationsPool.Assemblies[Association.ApplicationID].SystemName;
                    //NetCms.Vertical.Grants.GrantElement newElement = new NetCms.Vertical.Grants.GrantElement(Association.GrantObject as VerticalApplication, Association.Profile, Association.NetworkKey);
                    //switch (newElement[criteriaRow["id_ExternalAppCriteria"].ToString()].Value)
                    switch(grant[criteria.Key].Value)
                    {
                        case GrantsValues.Allow: cell.Text = "Consenti"; cell.CssClass = "fill_green"; break;
                        case GrantsValues.Deny: cell.Text = "Nega"; cell.CssClass = "fill_red"; break;
                        default:
                            if (NetCms.Configurations.Generics.MostValuableGrant)
                            {
                                cell.Text = "Consenti";
                                cell.CssClass = "fill_green";
                            }
                            else
                            {
                                cell.Text = "Nega";
                                cell.CssClass = "fill_red";
                            }
                            break;
                    }

                    row.Cells.Add(cell);
                //}

                details.AddRow(row, G2Core.UI.DetailsSheet.Colors.Default);
            }

            /*
            DetailsSheetRow dsr = new DetailsSheetRow("Indietro", "Ritorna alla schermata di associazione dei Permessi Applicazione .", "success");
            dsr.LinkLeft = "applications.aspx?app="+this.Association.ApplicationID;
            dsr.CssClassLeft = "big strong";
            details.AddRow(dsr);

            dsr = new DetailsSheetRow("Modifica", "Vai alla pagina di modifica di questi permessi.", "notify");
            dsr.LinkLeft = "applications.aspx?aid=" + this.Association.ID;
            dsr.CssClassLeft = "big strong";
            details.AddRow(dsr);*/

            return details;
        }

        private WebControl Overview(VerticalApplication vApp)
        {
            G2Core.UI.DetailsSheet details = new G2Core.UI.DetailsSheet("Riepilogo dei permessi '" + vApp.Label + "'", @"Questa schermata mostra i permessi relativi all'applicazione " + vApp.Label + " per " + this.Association.Profile.EntityLabel + @". La voce 'Permesso Registrato' contiene il valore attualmente registrato. La voce 'Permesso Eleborato' contiene il risultato dell'elaborazione dello specifico permesso da parte del sistema tenendo conto anche delle eredità dei gruppi.");

            details.AddColumn("Criterio", G2Core.UI.DetailsSheet.Colors.Default);
            details.AddColumn("Permesso Registrato", G2Core.UI.DetailsSheet.Colors.Default);
            details.AddColumn("Permesso Elaborato SP", G2Core.UI.DetailsSheet.Colors.Default);
            //if(NetCms.Configurations.Debug.GenericEnabled)
            //    details.AddColumn("Permesso Elaborato New", G2Core.UI.DetailsSheet.Colors.Default);

            TableRow row;

            //foreach (DataRow criteriaRow in this.CriteriaTable.Rows)
            foreach (ExternalAppzCriteria criteria in vApp.AppzCriteria)
            {
                row = new TableRow();

                TableCell cell = new TableCell();
                cell.Text = criteria.Nome; //criteriaRow["Nome_ExternalAppCriteria"].ToString();
                row.Cells.Add(cell);

                string value = "";

                //string sql = "Criteria_ExternalAppGrant = " + criteriaRow["id_ExternalAppCriteria"] + " AND ProfileAssociation_ExternalAppGrant = " +this.Association.ID;
                //DataRow[] rows = GrantsTable.Select(sql);

                GrantsFinder grant = vApp.CriteriaByProfileAndNetwork(Association.Profile, Association.NetworkKey);
                GrantValue dbValue = grant.CheckForGrant(criteria.Key);

                cell = new TableCell();
                //string dbvalue = rows.Length>0? rows[0]["Value_ExternalAppGrant"].ToString():"0";
                switch (dbValue.Value)
                {
                    case GrantsValues.Allow: cell.Text = "Consenti"; cell.CssClass = "fill_green"; break;
                    case GrantsValues.Deny: cell.Text = "Nega"; cell.CssClass = "fill_red"; break;
                    default: cell.Text = "Eredita"; cell.CssClass = "fill_yellow"; break;
                }

                row.Cells.Add(cell);

                value = "";
                //cell = new TableCell();
                //GrantElement element = new GrantElement(Association);
                //switch(element[ criteriaRow["id_ExternalAppCriteria"].ToString()].Value)
                //{
                //    case GrantsValues.Allow: value += "Consenti"; cell.CssClass = "fill_green"; break;
                //    case GrantsValues.Deny: value += "Nega"; cell.CssClass = "fill_red"; break;
                //    default:
                //        if (NetCms.Configurations.Generics.MostValuableGrant)
                //        {
                //            value += "Consenti"; 
                //            cell.CssClass = "fill_green"; 
                //        }
                //        else
                //        {
                //            value += "Nega";
                //            cell.CssClass = "fill_red";
                //        }
                //        break;
                //}

                //cell.Text = value;
                //row.Cells.Add(cell);

                //if (NetCms.Configurations.Debug.GenericEnabled)
                //{
                cell = new TableCell();
                //var appName = Vertical.VerticalApplicationsPool.Assemblies[Association.ApplicationID].SystemName;
                //NetCms.Vertical.Grants.GrantElement newElement = new NetCms.Vertical.Grants.GrantElement(Association.GrantObject as VerticalApplication, Association.Profile, Association.NetworkKey);
                //switch (newElement[criteriaRow["id_ExternalAppCriteria"].ToString()].Value)
                switch (grant[criteria.Key].Value)
                {
                    case GrantsValues.Allow: cell.Text = "Consenti"; cell.CssClass = "fill_green"; break;
                    case GrantsValues.Deny: cell.Text = "Nega"; cell.CssClass = "fill_red"; break;
                    default:
                        if (NetCms.Configurations.Generics.MostValuableGrant)
                        {
                            cell.Text = "Consenti";
                            cell.CssClass = "fill_green";
                        }
                        else
                        {
                            cell.Text = "Nega";
                            cell.CssClass = "fill_red";
                        }
                        break;
                }

                row.Cells.Add(cell);
                //}

                details.AddRow(row, G2Core.UI.DetailsSheet.Colors.Default);
            }

            /*
            DetailsSheetRow dsr = new DetailsSheetRow("Indietro", "Ritorna alla schermata di associazione dei Permessi Applicazione .", "success");
            dsr.LinkLeft = "applications.aspx?app="+this.Association.ApplicationID;
            dsr.CssClassLeft = "big strong";
            details.AddRow(dsr);

            dsr = new DetailsSheetRow("Modifica", "Vai alla pagina di modifica di questi permessi.", "notify");
            dsr.LinkLeft = "applications.aspx?aid=" + this.Association.ID;
            dsr.CssClassLeft = "big strong";
            details.AddRow(dsr);*/

            return details;
        }
    }
}
