﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;
using NetUtility.NetTreeViewControl;
using NetCms.GUI;
using NetTable;
using NetCms.Structure.Grants;
using NetCms.Vertical.Grants;
using NetCms.Grants;
using NetCms.Vertical;
using NetCms.Networks.Grants;
using NHibernate;
using NetCms.Vertical.BusinessLogic;
using System.Linq;

namespace NetCms.Users
{
    public class CriteriaManager:WebControl
    {
        //private NetCms.Connections.Connection _Connection;
        //private NetCms.Connections.Connection Connection
        //{
        //    get
        //    {
        //        if (_Connection == null)
        //            _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
        //        return _Connection;
        //    }
        //}
       
        public ApplicationProfileAssociation Association
        {
            get { return _Association; }
            private set { _Association = value; }
        }
        private ApplicationProfileAssociation _Association;
        
        //public CriteriaManager(string associationID)
        //    : base(HtmlTextWriterTag.Div)
        //{
        //        this.Association = new ApplicationProfileAssociation(associationID);
        //        CheckGrants();
        //}

        public VerticalApplication ApplicationLocal
        {
            get
            {
                if (_ApplicationLocal == null)
                    _ApplicationLocal = Association.GrantObject as VerticalApplication; //VerticalApplicationsPool.GetByID(Association.ApplicationID);
                return _ApplicationLocal;
            }
        }
        private VerticalApplication _ApplicationLocal;

        public CriteriaManager(ApplicationProfileAssociation association)
            : base(HtmlTextWriterTag.Div)
        {
            this.Association = association;
            CheckGrants();
        }

        public void CheckGrants()
        {
            //ExternalApplications.ExternalApplication app = new ExternalApplications.ExternalApplication(NetCms.Users.VerticalGrantsApplication.ApplicationID);
            //if (!app.Grant)
            //    PopupBox.AddNoGrantsMessageAndGoBack();

            //ExternalApplications.ExternalApplication appLocal = new ExternalApplications.ExternalApplication(int.Parse(Association.ApplicationID));
            //if (!appLocal["grants"])
            //    PopupBox.AddNoGrantsMessageAndGoBack();

            VerticalApplication grantsApp = VerticalApplicationsPool.GetByID(NetCms.Users.VerticalGrantsApplication.ApplicationID);
            if (!grantsApp.Grant)
                PopupBox.AddNoGrantsMessageAndGoBack();

            if (!ApplicationLocal.Criteria["grants"].Allowed)
                PopupBox.AddNoGrantsMessageAndGoBack();
        }


        //public DataTable CriteriaTable
        //{
        //    get
        //    {
        //        if (_CriteriaTable == null)
        //        {
        //            _CriteriaTable = this.Connection.SqlQuery("SELECT * FROM externalappzcriteria WHERE App_ExternalAppCriteria = " + Association.ApplicationID);
        //        }
        //        return _CriteriaTable;
        //    }
        //}
        //private DataTable _CriteriaTable;

        //public DataTable GrantsTable
        //{
        //    get
        //    {
        //        if (_GrantsTable == null)
        //            _GrantsTable = this.Connection.SqlQuery("SELECT * FROM externalappzgrants WHERE ProfileAssociation_ExternalAppGrant = " + Association.ID);
        //        return _GrantsTable;
        //    }
        //}
        //private DataTable _GrantsTable;
        
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (Association.AssociationFound)
            {
                
                if (VerticalApplicationsPool.ActiveAssemblies[ApplicationLocal.ID.ToString()] is VerticalApplicationPackage)
                    this.Controls.Add(CriteriaPackageAppView(VerticalApplicationsPool.ActiveAssemblies[ApplicationLocal.ID.ToString()] as VerticalApplicationPackage));
                else
                    this.Controls.Add(CriteriaView());
            }
                
        }

        void submit_Click(object sender, EventArgs e)
        {
            bool result = true;
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    this.ApplicationLocal.DeleteGrantsForAssociation(this.Association.ID, sess);

                    foreach (ExternalAppzCriteria criterioItem in this.ApplicationLocal.AppzCriteria)
                    {
                        string RadioGroupID = criterioItem.ID.ToString() + "Criteria" + Association.ID;

                        G2Core.Common.RequestVariable request = new G2Core.Common.RequestVariable(RadioGroupID);
                        if (request.IsValidString)
                        {
                            string grantvalue = request.StringValue.Remove(1);
                            switch (grantvalue)
                            {
                                case "A"://caso in cui +è stato impostato come allow
                                    grantvalue = "1";
                                    break;
                                case "I"://caso in cui +è stato impostato come eredita
                                    grantvalue = "0";
                                    break;
                                case "N"://caso in cui +è stato impostato come nega
                                    grantvalue = "2";
                                    break;
                            }

                            if (grantvalue != "0")
                                this.ApplicationLocal.AddGrant(this.Association.ID, int.Parse(grantvalue), criterioItem.ID, Users.AccountManager.CurrentAccount.ID, sess);
                        }
                    }
                    tx.Commit();

                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    result = false;
                }
            }


            //string sqlDeleteAssociatedCriteria = "DELETE FROM externalappzgrants WHERE ProfileAssociation_ExternalAppGrant = "+this.Association.ID;

            //this.Connection.Execute(sqlDeleteAssociatedCriteria);

            //foreach (DataRow criteriaRow in this.CriteriaTable.Rows)
            //{
            //    string RadioGroupID = criteriaRow["id_ExternalAppCriteria"].ToString() + "Criteria" + Association.ID;

            //    G2Core.Common.RequestVariable request = new G2Core.Common.RequestVariable(RadioGroupID);
            //    if (request.IsValidString)
            //    {
            //        string sql = "INSERT INTO ExternalAppzGrants(";
            //        sql += " ProfileAssociation_ExternalAppGrant ";
            //        sql += ", Value_ExternalAppGrant";
            //        sql += ", Criteria_ExternalAppGrant";
            //        sql += ", Author_ExternalAppGrant";
            //        sql += ") VALUES ";

            //        string grantvalue = request.StringValue.Remove(1);
            //        switch (grantvalue)
            //        {
            //            case "A"://caso in cui +è stato impostato come allow
            //                grantvalue = "1";
            //                break;
            //            case "I"://caso in cui +è stato impostato come eredita
            //                grantvalue = "0";
            //                break;
            //            case "N"://caso in cui +è stato impostato come nega
            //                grantvalue = "2";
            //                break;
            //        }
            //        if (grantvalue != "0")
            //        {
            //            sql += "(";
            //            sql += " " + this.Association.ID + " ";
            //            sql += ", " + grantvalue + " ";
            //            sql += ", " + criteriaRow["id_ExternalAppCriteria"].ToString() + " ";
            //            sql += ", " + Users.AccountManager.CurrentAccount.ID + " ";
            //            sql += ") ";

            //            this.Connection.Execute(sql);
            //        }
            //    }
            //}
            string link = "default.aspx?nid=" + (Association.NetworkKey == null ? "0" : Association.NetworkKey.NetworkID.ToString()) + "&aid=" + this.Association.ID + "&show=o";
            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
            //NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha modificato i permessi con successo.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
            //NetCms.GUI.PopupBox.AddSessionMessage("I Permessi sono stati salvati con successo", PostBackMessagesType.Success, link);
            if (result)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha modificato i permessi con successo.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                NetCms.GUI.PopupBox.AddSessionMessage("I Permessi sono stati salvati con successo", PostBackMessagesType.Success, link);
            }
            else
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "L'utente con username " + userName + " ha riscontrato un errore nella modifica dei permessi.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                NetCms.GUI.PopupBox.AddSessionMessage("Errore inatteso durante il salvataggio dei permessi", PostBackMessagesType.Error, link);
            }

        }
        public WebControl CriteriaView()
        {
            Table table = new Table();
            table.ID = "CriteriaTable";
            table.CssClass = "tab";
            table.CellSpacing = 0;
            table.CellPadding = 0;
            TableHeaderRow hrow = new TableHeaderRow();
            table.Rows.Add(hrow);

            TableHeaderCell hcell = new TableHeaderCell();
            hcell.Text = "Criterio";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Permesso Registrato";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Permesso Elaborato";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Consenti";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Nega";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Eredita";
            hrow.Cells.Add(hcell);


            TableRow row = new TableRow();
            TableCell cell = new TableCell();
            bool altrenate = false;
            GrantsFinder element = ApplicationLocal.CriteriaByProfileAndNetwork(Association.Profile, Association.NetworkKey); //new GrantElement(Association.GrantObject as VerticalApplication, Association.Profile, Association.NetworkKey);
            GrantsFinder myElement = ApplicationLocal.Criteria;//new GrantElement(this.Association.GrantObject as VerticalApplication, NetCms.Users.AccountManager.CurrentAccount.Profile, element.NetworkKey);
            //foreach (DataRow criteriaRow in this.CriteriaTable.Rows)
            foreach (ExternalAppzCriteria criteria in ApplicationLocal.AppzCriteria)
            {
                var mySpecify = myElement[criteria.Key].Allowed;
                if (mySpecify || this.Association.IsSuperAdminForThis(myElement.Profile as UserProfile))
                {
                    string workedValue = "";
                    switch (element[criteria.Key].Value)
                    {
                        case GrantsValues.Allow: workedValue += "Consenti"; break;
                        case GrantsValues.Deny: workedValue += "Nega"; break;
                        default: workedValue += "Eredita"; break;
                    }

                    row = new TableRow();

                    if (altrenate) row.CssClass = "alternate";
                    altrenate = !altrenate;

                    cell = new TableCell();
                    cell.Text = criteria.Nome;
                    row.Cells.Add(cell);

                    string RecorderValue = "";

                    //string sql = "Criteria_ExternalAppGrant = " + criteriaRow["id_ExternalAppCriteria"] + " AND ProfileAssociation_ExternalAppGrant = " + this.Association.ID;
                    //DataRow[] rows = GrantsTable.Select(sql);

                    //string dbvalue = rows.Length > 0 ? rows[0]["Value_ExternalAppGrant"].ToString() : "0";
                    GrantValue dbvalue = element.CheckForGrant(criteria.Key);
                    switch (dbvalue.Value)
                    {
                        case  GrantsValues.Allow : RecorderValue += "Consenti"; break;
                        case  GrantsValues.Deny : RecorderValue += "Nega"; break;
                        default: RecorderValue += "Eredita"; break;
                    }

                    cell = new TableCell();
                    cell.Text = RecorderValue;
                    cell.CssClass = "center";
                    row.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Text = workedValue;
                    cell.CssClass = "center";
                    row.Cells.Add(cell);

                    string RadioGroupID = criteria.ID + "Criteria" + Association.ID;

                    RadioButton allow = new RadioButton();
                    allow.ID = "A_" + RadioGroupID;
                    allow.GroupName = RadioGroupID;
                    allow.Checked = dbvalue.Value == GrantsValues.Allow;

                    RadioButton negate = new RadioButton();
                    negate.ID = "N_" + RadioGroupID;
                    negate.GroupName = RadioGroupID;
                    negate.Checked = dbvalue.Value == GrantsValues.Deny;

                    RadioButton inherits = new RadioButton();
                    inherits.ID = "I_" + RadioGroupID;
                    inherits.GroupName = RadioGroupID;
                    inherits.Checked = !allow.Checked && !negate.Checked;


                    cell = new TableCell();
                    cell.Controls.Add(allow);
                    cell.CssClass = "center";
                    row.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Controls.Add(negate);
                    cell.CssClass = "center";
                    row.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Controls.Add(inherits);
                    cell.CssClass = "center";
                    row.Cells.Add(cell);

                    table.Rows.Add(row);
                }
            }
            if (ApplicationLocal.AppzCriteria.Count>0)
            {
                row = new TableRow();

                cell = new TableCell();
                cell.Text = "&nbsp;";
                cell.CssClass = "text";
                cell.ColumnSpan = 3;
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Text = "<a href=\"javascript: GrantsTable_All('CriteriaTable','A')\">Consenti Tutti</a>";
                cell.CssClass = "center";
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Text = "<a href=\"javascript: GrantsTable_All('CriteriaTable','N')\">Nega Tutti</a>";
                cell.CssClass = "center";
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Text = "<a href=\"javascript: GrantsTable_All('CriteriaTable','I')\">Eredita Tutti</a>";
                cell.CssClass = "center";
                row.Cells.Add(cell);

                table.Rows.Add(row);
            }
            
            Button submit = new Button();
            submit.Text = "Salva Permessi";
            submit.Click += new EventHandler(submit_Click);
            row = new TableRow();

            cell = new TableCell();
            cell.Text = "&nbsp;";
            cell.ColumnSpan = 3;
            cell.Attributes["class"] = "text";
            row.Cells.Add(cell);

            cell = new TableCell();
            cell.ColumnSpan = 3;
            cell.Controls.Add(submit);
            cell.CssClass = "center";
            row.Cells.Add(cell);
            table.Rows.Add(row);
            
            return table;
        }

        public WebControl CriteriaPackageAppView(VerticalApplicationPackage vAppPack)
        {
            WebControl divContainer = new WebControl(HtmlTextWriterTag.Div);
            VerticalApplication vAppBase = VerticalAppBusinessLogic.GetVerticalAppById(vAppPack.ID);
            divContainer.Controls.Add(CriteriaTablePackageApp(vAppBase));
            foreach (int id in vAppPack.ContainedApplicationsIDs)
            {
                VerticalApplication vApp = VerticalAppBusinessLogic.GetVerticalAppById(id);
                GUI.Collapsable collapsable = new NetCms.GUI.Collapsable("Permessi per l'applicazione '" + vApp.Label + "'");
                collapsable.StartCollapsed = true;
                collapsable.AppendControl(CriteriaTablePackageApp(vApp));
                divContainer.Controls.Add(collapsable);
            }
            return divContainer;
        }

        void submitVapp_Click(object sender, EventArgs e)
        {
            string id = ((Button)sender).ID;
            string appId = id.Replace("Save-", "");

            bool result = true;
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    VerticalApplication vApp = VerticalAppBusinessLogic.GetVerticalAppById(int.Parse(appId),sess);
                    ObjectProfile associatedProfile = null;

                    if (!vApp.CheckIfProfileIsAssociated(this.Association.Profile.ID, (this.Association.NetworkKey != null) ? this.Association.NetworkKey.NetworkID : -1, sess))
                    {
                        if (this.Association.NetworkKey != null)
                            associatedProfile = vApp.AddGrantProfile(this.Association.Profile.ID, this.Association.NetworkKey.NetworkID, sess);
                        else
                            associatedProfile = vApp.AddGrantProfile(this.Association.Profile.ID, sess);
                    }
                    else
                        associatedProfile = vApp.GrantObjectProfiles.Where(x => x.Profile.ID == this.Association.Profile.ID).First();
                         
                    vApp.DeleteGrantsForAssociation(associatedProfile.ID, sess);

                    foreach (ExternalAppzCriteria criterioItem in vApp.AppzCriteria)
                    {
                        string RadioGroupID = criterioItem.ID.ToString() + "Criteria" + Association.ID;

                        G2Core.Common.RequestVariable request = new G2Core.Common.RequestVariable(RadioGroupID);
                        if (request.IsValidString)
                        {
                            string grantvalue = request.StringValue.Remove(1);
                            switch (grantvalue)
                            {
                                case "A"://caso in cui +è stato impostato come allow
                                    grantvalue = "1";
                                    break;
                                case "I"://caso in cui +è stato impostato come eredita
                                    grantvalue = "0";
                                    break;
                                case "N"://caso in cui +è stato impostato come nega
                                    grantvalue = "2";
                                    break;
                            }

                            if (grantvalue != "0")
                                vApp.AddGrant(associatedProfile.ID, int.Parse(grantvalue), criterioItem.ID, Users.AccountManager.CurrentAccount.ID, sess);
                        }
                    }
                    tx.Commit();

                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    result = false;
                }
            }
        }

        private WebControl CriteriaTablePackageApp(VerticalApplication vApp)
        {
            Table table = new Table();
            table.ID = "CriteriaTable-" + vApp.ID;
            table.CssClass = "tab";
            table.CellSpacing = 0;
            table.CellPadding = 0;
            TableHeaderRow hrow = new TableHeaderRow();
            table.Rows.Add(hrow);

            TableHeaderCell hcell = new TableHeaderCell();
            hcell.Text = "Criterio";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Permesso Registrato";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Permesso Elaborato";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Consenti";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Nega";
            hrow.Cells.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Eredita";
            hrow.Cells.Add(hcell);


            TableRow row = new TableRow();
            TableCell cell = new TableCell();
            bool altrenate = false;
            GrantsFinder element = vApp.CriteriaByProfileAndNetwork(Association.Profile, Association.NetworkKey); //new GrantElement(Association.GrantObject as VerticalApplication, Association.Profile, Association.NetworkKey);
            GrantsFinder myElement = vApp.Criteria;//new GrantElement(this.Association.GrantObject as VerticalApplication, NetCms.Users.AccountManager.CurrentAccount.Profile, element.NetworkKey);
            //foreach (DataRow criteriaRow in this.CriteriaTable.Rows)
            foreach (ExternalAppzCriteria criteria in vApp.AppzCriteria)
            {
                var mySpecify = myElement[criteria.Key].Allowed;
                if (mySpecify || this.Association.IsSuperAdminForThis(myElement.Profile as UserProfile))
                {
                    string workedValue = "";
                    switch (element[criteria.Key].Value)
                    {
                        case GrantsValues.Allow: workedValue += "Consenti"; break;
                        case GrantsValues.Deny: workedValue += "Nega"; break;
                        default: workedValue += "Eredita"; break;
                    }

                    row = new TableRow();

                    if (altrenate) row.CssClass = "alternate";
                    altrenate = !altrenate;

                    cell = new TableCell();
                    cell.Text = criteria.Nome;
                    row.Cells.Add(cell);

                    string RecorderValue = "";

                    //string sql = "Criteria_ExternalAppGrant = " + criteriaRow["id_ExternalAppCriteria"] + " AND ProfileAssociation_ExternalAppGrant = " + this.Association.ID;
                    //DataRow[] rows = GrantsTable.Select(sql);

                    //string dbvalue = rows.Length > 0 ? rows[0]["Value_ExternalAppGrant"].ToString() : "0";
                    GrantValue dbvalue = element.CheckForGrant(criteria.Key);
                    switch (dbvalue.Value)
                    {
                        case GrantsValues.Allow: RecorderValue += "Consenti"; break;
                        case GrantsValues.Deny: RecorderValue += "Nega"; break;
                        default: RecorderValue += "Eredita"; break;
                    }

                    cell = new TableCell();
                    cell.Text = RecorderValue;
                    cell.CssClass = "center";
                    row.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Text = workedValue;
                    cell.CssClass = "center";
                    row.Cells.Add(cell);

                    string RadioGroupID = criteria.ID + "Criteria" + Association.ID;

                    RadioButton allow = new RadioButton();
                    allow.ID = "A_" + RadioGroupID;
                    allow.GroupName = RadioGroupID;
                    allow.Checked = dbvalue.Value == GrantsValues.Allow;

                    RadioButton negate = new RadioButton();
                    negate.ID = "N_" + RadioGroupID;
                    negate.GroupName = RadioGroupID;
                    negate.Checked = dbvalue.Value == GrantsValues.Deny;

                    RadioButton inherits = new RadioButton();
                    inherits.ID = "I_" + RadioGroupID;
                    inherits.GroupName = RadioGroupID;
                    inherits.Checked = !allow.Checked && !negate.Checked;


                    cell = new TableCell();
                    cell.Controls.Add(allow);
                    cell.CssClass = "center";
                    row.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Controls.Add(negate);
                    cell.CssClass = "center";
                    row.Cells.Add(cell);

                    cell = new TableCell();
                    cell.Controls.Add(inherits);
                    cell.CssClass = "center";
                    row.Cells.Add(cell);

                    table.Rows.Add(row);
                }
            }
            if (vApp.AppzCriteria.Count > 0)
            {
                row = new TableRow();

                cell = new TableCell();
                cell.Text = "&nbsp;";
                cell.CssClass = "text";
                cell.ColumnSpan = 3;
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Text = "<a href=\"javascript: GrantsTable_All('CriteriaTable-" + vApp.ID + "','A')\">Consenti Tutti</a>";
                cell.CssClass = "center";
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Text = "<a href=\"javascript: GrantsTable_All('CriteriaTable-" + vApp.ID + "','N')\">Nega Tutti</a>";
                cell.CssClass = "center";
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Text = "<a href=\"javascript: GrantsTable_All('CriteriaTable-" + vApp.ID + "','I')\">Eredita Tutti</a>";
                cell.CssClass = "center";
                row.Cells.Add(cell);

                table.Rows.Add(row);
            }

            Button submit = new Button();
            submit.ID = "Save-" + vApp.ID;
            submit.Text = "Salva Permessi";
            submit.Click += new EventHandler(submitVapp_Click);
            row = new TableRow();

            cell = new TableCell();
            cell.Text = "&nbsp;";
            cell.ColumnSpan = 3;
            cell.Attributes["class"] = "text";
            row.Cells.Add(cell);

            cell = new TableCell();
            cell.ColumnSpan = 3;
            cell.Controls.Add(submit);
            cell.CssClass = "center";
            row.Cells.Add(cell);
            table.Rows.Add(row);

            return table;
        }
    }

    //public class ApplicationProfileAssociation
    //{
    //   private NetCms.Connections.Connection _Connection;
    //    private NetCms.Connections.Connection Connection
    //    {
    //        get
    //        {
    //            if (_Connection == null)
    //                _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
    //            return _Connection;
    //        }
    //    }

    //    public string ID
    //    {
    //        get { return _ID; }
    //        private set { _ID = value; }
    //    }
    //    private string _ID;

    //    public Networks.NetworkKey NetworkKey
    //    {
    //        get
    //        {
    //            return _NetworkKey;
    //        }
    //        private set { _NetworkKey = value; }
    //    }
    //    private Networks.NetworkKey _NetworkKey;

    //    public Profile Profile
    //    {
    //        get { return _Profile; }
    //        private set { _Profile = value; }
    //    }
    //    private Profile _Profile;

    //    public bool NetworkDependant
    //    {
    //        get { return _NetworkDependant; }
    //        private set { _NetworkDependant = value; }
    //    }
    //    private bool _NetworkDependant;

    //    public string ApplicationID
    //    {
    //        get { return _ApplicationID; }
    //        private set { _ApplicationID = value; }
    //    }
    //    private string _ApplicationID;
        
    //    public bool AssociationFound
    //    {
    //        get { return _AssociationFound; }
    //    }
    //    private bool _AssociationFound;

    //    public ApplicationProfileAssociation(string associationID)
    //    {
    //        DataTable queryResult = this.Connection.SqlQuery("SELECT * FROM externalappz_profiles INNER JOIN externalappz ON id_ExternalApp = Application_ExternalAppzProfile WHERE id_ExternalAppzProfile = " + associationID);

    //        if (queryResult.Rows.Count > 0)
    //        {
    //            Init(queryResult.Rows[0]);
    //        }
    //    }
        
    //    public ApplicationProfileAssociation(string applicationID, Profile profile)
    //        :this(applicationID,profile,null)
    //    {
    //    }
        
    //    public ApplicationProfileAssociation(string applicationID,Profile profile, Networks.NetworkKey NetworkKey)
    //    {
    //        if (profile != null)
    //        {
    //            DataTable queryResult = this.Connection.SqlQuery("SELECT * FROM externalappz_profiles INNER JOIN externalappz ON id_ExternalApp = Application_ExternalAppzProfile WHERE Network_ExternalAppzProfile = " + (NetworkKey == null ? "0" : NetworkKey.NetworkID.ToString()) + " AND Profile_ExternalAppzProfile = " + profile.ID + " AND (Application_ExternalAppzProfile = '" + applicationID + "' OR Key_ExternalApp LIKE '" + applicationID + "') ");
    //            if (queryResult.Rows.Count > 0)
    //            {
    //                this.ID = queryResult.Rows[0]["id_ExternalAppzProfile"].ToString();
    //                this.NetworkDependant = (int)queryResult.Rows[0]["NetworkDependant_ExternalApp"] == 1;
    //            }
    //        }
    //        this.Profile = profile;
    //        this.ApplicationID = applicationID;
    //        this.NetworkKey = NetworkKey;
    //        this._AssociationFound = this.ID != null;
    //    }
        
    //    public ApplicationProfileAssociation(DataRow data)
    //    {
    //        Init(data);
    //    }

    //    private void Init(DataRow data)
    //    {
    //        this._AssociationFound = true;
    //        this.ID = data["id_ExternalAppzProfile"].ToString();
    //        //this.Profile = new Profile(data["Profile_ExternalAppzProfile"].ToString());
    //        this.Profile = ProfileBusinessLogic.GetById(int.Parse(data["Profile_ExternalAppzProfile"].ToString()));
    //        this.NetworkDependant = (int)data["NetworkDependant_ExternalApp"] == 1;
    //        if (NetworkDependant && (int)data["Network_ExternalAppzProfile"] > 0)
    //            this.NetworkKey = new NetCms.Networks.NetworkKey(int.Parse(data["Network_ExternalAppzProfile"].ToString()));
    //        this.ApplicationID = data["Application_ExternalAppzProfile"].ToString();
    //    }


    //    public bool IsSuperAdminForThis(UserProfile ProfileToCheck)
    //    {
    //        return (!Profile.IsUser) && (Profile as GroupProfile).Group.ImSuperAdmin(ProfileToCheck.User);
    //    }
    //}
}
