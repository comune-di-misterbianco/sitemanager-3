﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using G2Core;
using NHibernate;
using NetCms.Users.Search;
using NetCms.Networks;
using NetService.Utility.ArTable;
using System.Linq;
using NetCms.Vertical.BusinessLogic;
using NetCms.Grants;

namespace NetCms.Vertical.Grants
{
    public class GrantsEntitiesPage_Vertical:NetCms.GUI.SmPageVertical
    {
        public NetCms.Users.CriteriaManager CriteriaManager
        {
            get
            {
                if (_CriteriaManager == null && Association!=null)
                {

                    _CriteriaManager = new NetCms.Users.CriteriaManager(Association);
                }
                return _CriteriaManager;
            }
        }
        private NetCms.Users.CriteriaManager _CriteriaManager;

        public ApplicationProfileAssociation Association
        {
            get
            {
                if (_Association == null)
                {
                    G2Core.Common.RequestVariable associationRequest = new G2Core.Common.RequestVariable("aid", G2Core.Common.RequestVariable.RequestType.QueryString);
                    int associationID = associationRequest.IntValue;
                    int applicationID = SelectedApplicationID.IntValue;
                    if (applicationID > 0 || associationID > 0)
                    {
                        VerticalApplication app = null;
                        if (applicationID > 0)
                            app = VerticalAppBusinessLogic.GetVerticalAppById(applicationID);
                        else
                        {
                            ObjectProfile profile = ExternalAppzGrantsBusinessLogic.GetExternalAppProfileById(associationID);
                            app = VerticalAppBusinessLogic.GetVerticalAppById(profile.WfsObject.ID);
                        }
                        //Aggiunta la clausola che in caso associationID non sia settato in seguito a postback per l'eliminazione, considera il delete profile id
                        ApplicationProfileAssociation tmp = new ApplicationProfileAssociation(app, (associationID > 0 ? associationID : DeleteProfile.RequestVariable.IntValue));
                        if (this.SelectedNetwork.IntValue == 0 || (tmp.NetworkKey != null && this.SelectedNetwork.IntValue == tmp.NetworkKey.NetworkID))
                            _Association = tmp;
                    }
                }
                return _Association;
            }
        }
        private ApplicationProfileAssociation _Association;

        public NetCms.Users.CriteriaOverview CriteriaOverview
        {
            get
            {
                if (_CriteriaOverview == null && Association != null)
                    _CriteriaOverview = new NetCms.Users.CriteriaOverview(Association);
                return _CriteriaOverview;
            }
        }
        private NetCms.Users.CriteriaOverview _CriteriaOverview;

        public G2Core.Common.RequestVariable SelectedApplicationID
        {
            get
            {
                if (_SelectedApplicationID == null)
                    _SelectedApplicationID = new G2Core.Common.RequestVariable("app",G2Core.Common.RequestVariable.RequestType.QueryString);
                return _SelectedApplicationID;
            }
        }
        private G2Core.Common.RequestVariable _SelectedApplicationID;

        public G2Core.Common.RequestVariable ShowOverview
        {
            get
            {
                if (_ShowOverview == null)
                    _ShowOverview = new G2Core.Common.RequestVariable("show", G2Core.Common.RequestVariable.RequestType.QueryString);
                return _ShowOverview;
            }
        }
        private G2Core.Common.RequestVariable _ShowOverview;

        public G2Core.Common.RequestVariable SelectedNetwork
        {
            get
            {
                if (_SelectedNetwork == null)
                    _SelectedNetwork = new G2Core.Common.RequestVariable("nid", G2Core.Common.RequestVariable.RequestType.QueryString);
                return _SelectedNetwork;
            }
        }
        private G2Core.Common.RequestVariable _SelectedNetwork;

        public G2Core.XhtmlControls.InputField SelectedObject
        {
            get
            {
                if (_SelectedObject == null)
                {
                    _SelectedObject = new G2Core.XhtmlControls.InputField("SelectedObject");
                }
                return _SelectedObject;
            }
        }
        private G2Core.XhtmlControls.InputField _SelectedObject;

        public G2Core.XhtmlControls.InputField DeleteProfile
        {
            get
            {
                if (_DeleteProfile == null)
                {
                    _DeleteProfile = new G2Core.XhtmlControls.InputField("DeleteProfile");
                }
                return _DeleteProfile;
            }
        }
        private G2Core.XhtmlControls.InputField _DeleteProfile;

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_NetworkPageModel"; }
        }
        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Table_Key; }
        }
        public override string PageTitle
        {
            get 
            {
                if (_PageTitle == null)
                {
                    _PageTitle = "Permessi Applicazione ";
                    if (SelectedApplicationID.IsValidInteger)
                        _PageTitle += GetApplicationName(SelectedApplicationID.IntValue);
                    
                }
                return _PageTitle; 
            }
        }
        private string _PageTitle;

        private string GetApplicationName(int id)
        {
            //DataTable vapps = this.Connection.SqlQuery("SELECT * FROM externalappz");
            //DataRow[] rows = vapps.Select("id_ExternalApp = " + id);
            //return rows[0]["Nome_ExternalApp"].ToString();
            return VerticalAppBusinessLogic.GetVerticalAppById(id).Label;
        }

        private string GetNetworkLabelForLog(int id)
        {
            if (id == 0)
            {
                return "generici";
            }
            else
            {
                string networkSystemName = NetCms.Networks.NetworksManager.GlobalIndexer.NetworkKeys[id];
                NetCms.Networks.BasicNetwork network = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks[networkSystemName];
                return "del portale " + network.Label;
            }
        }

        public GrantsEntitiesPage_Vertical()
        {
            VerticalApplication grantsApp = VerticalApplicationsPool.GetByID(NetCms.Users.VerticalGrantsApplication.ApplicationID);
            //ExternalApplications.ExternalApplication app = new ExternalApplications.ExternalApplication(NetCms.Users.VerticalGrantsApplication.ApplicationID);
            if (!grantsApp.Grant)
                PopupBox.AddNoGrantsMessageAndGoBack();

            if (Association != null && Association.ApplicationID > 0)//!string.IsNullOrEmpty(Association.ApplicationID))            
            {
                VerticalApplication appLocal = VerticalApplicationsPool.GetByID(Association.ApplicationID);
                //ExternalApplications.ExternalApplication appLocal = new ExternalApplications.ExternalApplication(int.Parse(Association.ApplicationID));
                if (!appLocal.Criteria["grants"].Allowed)
                    PopupBox.AddNoGrantsMessageAndGoBack();
            }
        }

        protected override WebControl RootControl
        {
            get
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;

                if (string.IsNullOrEmpty( this.SelectedNetwork.StringValue) &&( this.SelectedNetwork.IntValue == 0 || Account.NetworkEnabledForUser(this.SelectedNetwork.IntValue)))
                {
                    if (!IsPostBack)
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di gestione dei permessi per le applicazioni.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    return BuildNetworksTable();
                }

                if (Association != null && Association.AssociationFound && ShowOverview.StringValue == "o")
                {
                    if (!IsPostBack)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta visualizzando i permessi " + GetNetworkLabelForLog(SelectedNetwork.IntValue) +" per " + CriteriaOverview.Association.Profile.EntityLabel + " relativo all'applicazione " + GetApplicationName(CriteriaOverview.Association.ApplicationID) +  ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    }

                    this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.PageName + "?nid="+SelectedNetwork+"&app=" + CriteriaOverview.Association.ApplicationID, NetCms.GUI.Icons.Icons.Table_Key, "Elenco Associazioni"));
                    this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.PageName + "?nid=" + SelectedNetwork + "&aid=" + CriteriaOverview.Association.ID, NetCms.GUI.Icons.Icons.Key, "Modifica Permessi"));
                    _PageTitle += GetApplicationName(CriteriaOverview.Association.ApplicationID) + " - Riepilogo permessi per " + CriteriaOverview.Association.Profile.EntityLabel;
                    return CriteriaOverview;
                }

                if (Association != null && Association.AssociationFound && (DeleteProfile.RequestVariable.IntValue == 0))
                {
                    if (!IsPostBack)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di modifica dei permessi " + GetNetworkLabelForLog(SelectedNetwork.IntValue) + " per " + CriteriaOverview.Association.Profile.EntityLabel + " relativo all'applicazione " + GetApplicationName(CriteriaOverview.Association.ApplicationID) + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    }
                    else {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta modificando i permessi " + GetNetworkLabelForLog(SelectedNetwork.IntValue) + " per " + CriteriaOverview.Association.Profile.EntityLabel + " relativo all'applicazione " + GetApplicationName(CriteriaOverview.Association.ApplicationID) + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    }


                    this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.PageName + "?nid=" + SelectedNetwork + "&app=" + CriteriaOverview.Association.ApplicationID, NetCms.GUI.Icons.Icons.Table_Key, "Elenco Associazioni"));
                    this.Toolbar.Buttons.Add(new ToolbarButton(NetCms.Configurations.Paths.PageName + "?nid=" + SelectedNetwork + "&show=o&aid=" + CriteriaManager.Association.ID, NetCms.GUI.Icons.Icons.Page_Key, "Mostra Scheda Permessi"));
                    _PageTitle += GetApplicationName(CriteriaManager.Association.ApplicationID) + " - Per " + CriteriaManager.Association.Profile.EntityLabel;
                    return CriteriaManager;
                }

                var vapp = VerticalAppBusinessLogic.GetVerticalAppById(this.SelectedApplicationID.IntValue);
                //if (this.Connection.SqlQuery("SELECT * FROM externalappz WHERE id_ExternalApp = " + this.SelectedApplicationID.IntValue + " AND Enabled_ExternalApp = 1").Rows.Count == 1)
                if (vapp != null && vapp.Enabled)
                {
                    if (!IsPostBack)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina dei permessi " + GetNetworkLabelForLog(SelectedNetwork.IntValue) + " per l'applicazione " + vapp.Label + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    }

                    WebControl container = new WebControl(HtmlTextWriterTag.Div);

                    Collapsable col = new Collapsable("Informazioni sulla gestione dei Permessi");
                    string text = @"<p class=""info"">Questa pagina permette di creare associazioni tra gruppi, utenti e Applicazioni al fine di impostare i permessi.</p>";
                    col.AppendControl(new LiteralControl(text));
                    container.Controls.Add(col);

                    //string sqlInsertModel = "INSERT INTO externalappz_profiles (Application_ExternalAppzProfile,Profile_ExternalAppzProfile,Creator_ExternalAppzProfile,Network_ExternalAppzProfile) Values (" + this.SelectedApplicationID + ",{0}," + this.Account.ID + "," + this.SelectedNetwork.StringValue + ")";
                    //string sqlSelectModel = "SELECT * FROM externalappz_profiles WHERE Application_ExternalAppzProfile = " + this.SelectedApplicationID + " AND Profile_ExternalAppzProfile = {0} AND Network_ExternalAppzProfile = " + this.SelectedNetwork.StringValue + "";

                    //NetCms.Structure.Grants.ProfilesPicker picker = new NetCms.Structure.Grants.ProfilesPicker("Gruppi e Utenti che è possibile aggiungere alle specifiche della cartella", this.Account, this.Connection);
                    //picker.SqlExecuteModel = sqlInsertModel;
                    //picker.SqlSelectModel = sqlSelectModel;
                    ////picker.FilterGroups = true;

                    ProfilesPickerForGrants picker = new ProfilesPickerForGrants("Gruppi e Utenti che è possibile aggiungere alle specifiche della cartella", this.Account, vapp,this.SelectedNetwork.IntValue);


                    container.Controls.Add(picker.Control);
                    container.Controls.Add(BuildTable());
                    if (picker.AddProfileField.RequestVariable.IsPostBack && picker.AddProfileField.RequestVariable.IsValidInteger && picker.AddProfileField.RequestVariable.IntValue > 0)
                    {
                        if (picker.ProfileInserted)
                        {
                            //NetCms.Users.Profile profile = new Users.Profile(picker.AddProfileField.RequestVariable.StringValue);
                            Users.Profile profile = Users.ProfileBusinessLogic.GetById(picker.AddProfileField.RequestVariable.IntValue);
                            PopupBox.AddMessage("" + profile.EntityLabel + " è stato aggiunto con successo.", PostBackMessagesType.Success);
                            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha aggiunto il profilo con id " + picker.AddProfileField.RequestVariable.StringValue + " alle specifiche dell'applicazione " + vapp.Label + " (id=" + this.SelectedApplicationID + ") " + GetNetworkLabelForLog(SelectedNetwork.IntValue) + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        }
                        else
                        {
                            PopupBox.AddMessage("Impossibile aggiungere il profilo.", PostBackMessagesType.Notify);
                            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito ad aggiungere il profilo con id " + picker.AddProfileField.RequestVariable.StringValue + " alle specifiche dell'applicazione " + vapp.Label + " (id=" + this.SelectedApplicationID + ") " + GetNetworkLabelForLog(SelectedNetwork.IntValue) + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                        }
                    }
                    return container;
                }
                else
                {
                    if (!IsPostBack)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina di impostazione dei permessi " + GetNetworkLabelForLog(SelectedNetwork.IntValue) + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    }

                    WebControl container = new WebControl(HtmlTextWriterTag.Div);

                    //Creo la tabella con l'elenco delle applicazioni verticali di Portale
                    Vertical.VerticalApplicationListControl vapplist = new NetCms.Vertical.VerticalApplicationListControl(this.Account.Profile,SelectedNetwork.IntValue);
                    vapplist.InnerTable.CssClass = "tab";
                    vapplist.NetworkDependant = this.SelectedNetwork.IntValue > 0;
                    vapplist.ShowOnlyGroups = -1;

                    container.Controls.Add(vapplist);

                    return container;
                }

                return null;
            }
        }

        public void DeletePostBack()
        {
            if (DeleteProfile.RequestVariable.IsValidInteger)
            {
                string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;

                if (Association.GrantObject.DeleteGrantProfile(DeleteProfile.RequestVariable.IntValue))
                {
                    if (VerticalApplicationsPool.ActiveAssemblies[Association.GrantObject.ID.ToString()] is VerticalApplicationPackage)
                    {
                        VerticalApplicationPackage vAppPack = VerticalApplicationsPool.ActiveAssemblies[Association.GrantObject.ID.ToString()] as VerticalApplicationPackage;
                        foreach (int appId in vAppPack.ContainedApplicationsIDs)
                        {
                            VerticalApplication vApp = VerticalAppBusinessLogic.GetVerticalAppById(appId);
                            if (vApp != null)
                            {
                                ObjectProfile associatedProfile = vApp.GrantObjectProfiles.Where(x => x.Profile.ID == this.Association.Profile.ID).First();
                                if (associatedProfile != null)
                                    vApp.DeleteGrantProfile(associatedProfile.ID);
                            }
                        }
                    }

                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha rimosso con successo il profilo con id " + DeleteProfile.RequestVariable.IntValue + " dai profili dell'applicazione " + GetApplicationName(SelectedApplicationID.IntValue) + " relativa ai permessi " + GetNetworkLabelForLog(SelectedNetwork.IntValue) + " .", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                    NetCms.GUI.PopupBox.AddMessage("Rimozione effettuata con successo", PostBackMessagesType.Success);
                }
                else
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a rimuovere il profilo con id " + DeleteProfile.RequestVariable.IntValue + " dai profili dell'applicazione " + GetApplicationName(SelectedApplicationID.IntValue) + " relativa ai permessi " + GetNetworkLabelForLog(SelectedNetwork.IntValue) + " .", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                }       


                //if (this.Connection.Execute("DELETE FROM externalappz_profiles WHERE id_ExternalAppzProfile = " + DeleteProfile.RequestVariable.IntValue))
                //{
                //    //string Utente = this.Account.Label;
                //    //string logText = "L'utente " + Utente;
                //    //logText += " rimuove il profilo " + DeleteProfile.RequestVariable.IntValue + " dai profili associati all'applicazione " + SelectedApplicationID;

                //    //this.GuiLog.SaveLogRecord(logText);
                    

                //    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha rimosso con successo il profilo con id " + DeleteProfile.RequestVariable.IntValue + " dai profili dell'applicazione " + GetApplicationName(SelectedApplicationID.IntValue) + " relativa ai permessi " + GetNetworkLabelForLog(SelectedNetwork.IntValue) + " .", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                //    NetCms.GUI.PopupBox.AddMessage("Rimozione effettuata con successo", PostBackMessagesType.Success);
                //}
                //else 
                //{
                //    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " non è riuscito a rimuovere il profilo con id " + DeleteProfile.RequestVariable.IntValue + " dai profili dell'applicazione " + GetApplicationName(SelectedApplicationID.IntValue) + " relativa ai permessi " + GetNetworkLabelForLog(SelectedNetwork.IntValue) + " .", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                //}
            }

        }
        private HtmlGenericControl BuildTable()
        {
            if (this.IsPostBack) DeletePostBack();

            HtmlGenericControls.Div set = new HtmlGenericControls.Div();
            set.Class = "FullSizeFieldset";

            HtmlGenericControls.Div title = new HtmlGenericControls.Div();
            title.Class = "FullSizeFieldset_Title";
            set.Controls.Add(title);
            title.InnerHtml = "Gruppi e Utenti associati";


            HtmlGenericControl table = new HtmlGenericControl("table");
            table.Attributes["cellspacing"] = "0";
            table.Attributes["cellpadding"] = "00";
            table.Attributes["class"] = "tab";

            NetCms.Users.Group Group = NetCms.Users.GroupsHandler.RootGroup;

            HtmlGenericControl tr;
            //****************************************************************
            //lista di grppi e utenti

            #region Header della tabella
            tr = new HtmlGenericControl("tr");
            HtmlGenericControl th;

            th = new HtmlGenericControl("th");
            th.InnerHtml = "&nbsp;";
            tr.Controls.Add(th);

            th = new HtmlGenericControl("th");
            th.InnerHtml = "Nome";
            tr.Controls.Add(th);

            th = new HtmlGenericControl("th");
            th.InnerHtml = "Tipo";
            tr.Controls.Add(th);

            th = new HtmlGenericControl("th");
            th.InnerHtml = "Eredita da";
            tr.Controls.Add(th);

            th = new HtmlGenericControl("th");
            th.InnerHtml = "Mostra";
            tr.Controls.Add(th);

            th = new HtmlGenericControl("th");
            th.InnerHtml = "Imposta";
            tr.Controls.Add(th);

            th = new HtmlGenericControl("th");
            th.InnerHtml = "Rimuovi";
            tr.Controls.Add(th);

            table.Controls.Add(tr);

            #endregion

            #region Corpo della tabella (Elenco Gruppi)
            
            //string additionalSqlJoins = " INNER JOIN externalappz_profiles ON (profiles.id_Profile = Profile_ExternalAppzProfile)";
            //string additionalSqlConditions = "Network_ExternalAppzProfile = " + this.SelectedNetwork + " AND Application_ExternalAppzProfile = " + this.SelectedApplicationID;
           
            NetCms.Users.Search.UserRelated st = new NetCms.Users.Search.UserRelated(this.Account);
            ICollection<Users.Group> groupsList = st.GroupsIAdmin(0,st.GroupsIAdminCount(null,null), null,null);
            foreach (Users.Group gruppo in groupsList)
            {
                //ISQLQuery externalAppsQuery = Users.UsersBusinessLogic.GetCurrentSession().CreateSQLQuery("SELECT * FROM externalappz_profiles WHERE Network_ExternalAppzProfile = " + this.SelectedNetwork + " AND Application_ExternalAppzProfile = " + this.SelectedApplicationID + " AND Profile_ExternalAppzProfile = " + gruppo.Profile.ID)
                //    .AddScalar("id_ExternalAppzProfile", NHibernateUtil.Int32);
                VerticalApplication vapp = VerticalAppBusinessLogic.GetVerticalAppById(SelectedApplicationID.IntValue);
                var externalAppsQuery = vapp.GrantObjectProfiles.Where(x => x.Profile.ID == gruppo.Profile.ID && (x as ExternalAppProfile).Network == this.SelectedNetwork.IntValue);

                int idExternalAppProfile = 0;
                if (externalAppsQuery.Count() > 0)
                    idExternalAppProfile = externalAppsQuery.First().ID;

                if (idExternalAppProfile > 0)
                {

                    tr = new HtmlGenericControl("tr");

                    HtmlGenericControl td;
                    td = new HtmlGenericControl("td");
                    td.InnerHtml = "&nbsp;";
                    td.Attributes["class"] = "icon_group";
                    tr.Controls.Add(td);

                    td = new HtmlGenericControl("td");
                    td.InnerHtml = gruppo.Name; //row["Name_Group"].ToString();
                    tr.Controls.Add(td);

                    td = new HtmlGenericControl("td");
                    td.InnerHtml = "Gruppo";
                    tr.Controls.Add(td);

                    string family = "&nbsp;";
                    //NetCms.Users.Group parent = Groups.Groups.FindGroup(row["Parent_Group"].ToString());
                    NetCms.Users.Group parent = gruppo.Parent; //Group.FindGroup(int.Parse(row["Parent_Group"].ToString()));
                    while (parent != null)
                    {
                        family = parent.Name + "/" + family;
                        if (parent.Parent != null)
                            //parent = Groups.Groups.FindGroup(parent.Parent.ID.ToString());
                            parent = Group.FindGroup(parent.Parent.ID);
                        else
                            parent = null;
                    }

                    td = new HtmlGenericControl("td");
                    td.InnerHtml = family;
                    tr.Controls.Add(td);

                    td = new HtmlGenericControl("td");
                    td.Attributes["class"] = "action details";
                    //td.InnerHtml = "<a href=\" javascript: setFormValue(" + row["id_" + Object.Extension + "Profile"] + ",'SelectedObject',1)\" >";
                    td.InnerHtml = "<a href=\"?nid=" + SelectedNetwork + "&amp;show=o&amp;aid=" + idExternalAppProfile;
                    td.InnerHtml += "\" >";
                    td.InnerHtml += "<span>Mostra</span>";
                    td.InnerHtml += "</a>";
                    tr.Controls.Add(td);

                    td = new HtmlGenericControl("td");
                    td.Attributes["Class"] = "action grant";
                    td.InnerHtml = "<a href=\"?nid=" + SelectedNetwork + "&amp;aid=" + idExternalAppProfile;
                    td.InnerHtml += "\" >";
                    td.InnerHtml += "<span>Imposta</span>";
                    td.InnerHtml += "</a>";
                    tr.Controls.Add(td);

                    td = new HtmlGenericControl("td");
                    td.Attributes["Class"] = "delete";
                    //td.InnerHtml = "<a href=\"grants.aspx?remove=" + row["id_" + Object.Extension + "Profile"] + "&" + PageData.ObjectQueryString + "\" >";
                    string href = "javascript: setFormValueConfirm('" + idExternalAppProfile + "','DeleteProfile',1,'Sei sicuro di voler rimuovere questa specifica?')";
                    td.InnerHtml = "<a href=\"" + href + "\" >";
                    td.InnerHtml += "<span>Rimuovi</span>";
                    td.InnerHtml += "</a>";
                    tr.Controls.Add(td);


                    table.Controls.Add(tr);
                }
            }

            #endregion

            #region Corpo della tabella  (Elenco Utenti)
            /*
            string sql = "SELECT id_User,Name_User,id_ExternalAppzProfile FROM Users";
            sql += " INNER JOIN Profiles ON (Profile_User = id_Profile)";
            sql += " INNER JOIN UsersGroups ON (User_UserGroup = id_User)";
            sql += " INNER JOIN Groups ON (Group_UserGroup = id_Group)";
            sql += " INNER JOIN externalappz_profiles ON (profiles.id_Profile = Profile_ExternalAppzProfile)";
            sql += " WHERE  Network_ExternalAppzProfile = "+this.SelectedNetwork+" AND Application_ExternalAppzProfile = " + this.SelectedApplicationID;

            string conditions = "";
            for (int i = 0; i < this.Account.Groups.Count; i++)
            {
                NetCms.Users.GroupAssociation GroupAssociation = this.Account.AssociatedGroups[i];

                if (GroupAssociation.GrantsRole != NetCms.Users.GrantAdministrationTypes.User)
                {
                    conditions += " OR ";
                    conditions += " Tree_Group LIKE '" + GroupAssociation.Group.Tree + "%'";
                }
            }
            //La condizione 1=0 serve ad esculudere tutti i record tranne quelli selezionati dalle
            //condizioni specificate nella variabile conditions
            sql += " AND (1=0" + conditions + ")";

            sql += " GROUP BY id_User,Name_User,id_ExternalAppzProfile";
            dataTable = this.Connection.SqlQuery(sql);
            */
            //additionalSqlJoins = " INNER JOIN externalappz_profiles ON (profiles.id_Profile = Profile_ExternalAppzProfile)";
            //additionalSqlConditions = "Network_ExternalAppzProfile = " + this.SelectedNetwork + " AND Application_ExternalAppzProfile = " + this.SelectedApplicationID;
            //string additionalSqlFields = "id_ExternalAppzProfile";

            //DA MODIFICARE
            //ICollection<Users.User> usersList = st.UsersIAdmin(0,st.UsersIAdminCount(null,null),null,null);

            //foreach (Users.User user in usersList)
            //{
            //    //ISQLQuery externalAppsQuery = Users.UsersBusinessLogic.GetCurrentSession().CreateSQLQuery("SELECT * FROM externalappz_profiles WHERE Network_ExternalAppzProfile = " + this.SelectedNetwork + " AND Application_ExternalAppzProfile = " + this.SelectedApplicationID + " AND Profile_ExternalAppzProfile = " + user.Profile.ID)
            //    //    .AddScalar("id_ExternalAppzProfile", NHibernateUtil.Int32);

            //    //int idExternalAppProfile = externalAppsQuery.UniqueResult<int>();
            //    VerticalApplication vapp = VerticalAppBusinessLogic.GetVerticalAppById(SelectedApplicationID.IntValue);
            //    var externalAppsQuery = vapp.GrantObjectProfiles.Where(x => x.Profile.ID == user.Profile.ID && (x as ExternalAppProfile).Network == this.SelectedNetwork.IntValue);

            //    int idExternalAppProfile = 0;
            //    if (externalAppsQuery.Count() > 0)
            //        idExternalAppProfile = externalAppsQuery.First().ID;

            //    if (idExternalAppProfile > 0)
            //    {

            //        tr = new HtmlGenericControl("tr");

            //        HtmlGenericControl td;
            //        td = new HtmlGenericControl("td");
            //        td.InnerHtml = "&nbsp;";
            //        td.Attributes["class"] = "icon_user";
            //        tr.Controls.Add(td);

            //        td = new HtmlGenericControl("td");
            //        td.InnerHtml = user.UserName; //row["Name_User"].ToString();
            //        tr.Controls.Add(td);

            //        td = new HtmlGenericControl("td");
            //        td.InnerHtml = "Utente";
            //        tr.Controls.Add(td);

            //        //DataTable dt = this.Connection.SqlQuery("SELECT * From UsersGroups WHERE User_UserGroup = " + row["id_User"].ToString());
            //        string families = "";
            //        foreach (Users.Group group in user.Groups)
            //        {
            //            string family = "";
            //            //NetCms.Users.Group parent = Group.ChildGroups.FindGroup(group["Group_UserGroup"].ToString());
            //            NetCms.Users.Group parent = group;//Group.FindGroup(int.Parse(group["Group_UserGroup"].ToString()));
            //            while (parent != null)
            //            {
            //                family = parent.Name + "/" + family;
            //                if (parent.Parent != null)
            //                    //parent = Group.ChildGroups.FindGroup(parent.Parent.ID.ToString());
            //                    parent = parent.Parent; //Group.FindGroup(parent.Parent.ID);
            //                else
            //                    parent = null;
            //            }
            //            if (families != "")
            //                families += "<br />";
            //            families += family;

            //        } td = new HtmlGenericControl("td");
            //        td.InnerHtml = families;
            //        tr.Controls.Add(td);

            //        td = new HtmlGenericControl("td");
            //        td.Attributes["class"] = "action details";
            //        //td.InnerHtml = "<a href=\" javascript: setFormValue(" + row["id_" + Object.Extension + "Profile"] + ",'SelectedObject',1)\" >";
            //        td.InnerHtml = "<a href=\"?nid=" + SelectedNetwork + "&amp;show=o&amp;aid=" + idExternalAppProfile;
            //        td.InnerHtml += "\" >";
            //        td.InnerHtml += "<span>Mostra</span>";
            //        td.InnerHtml += "</a>";
            //        tr.Controls.Add(td);

            //        td = new HtmlGenericControl("td");
            //        td.Attributes["class"] = "action grant";
            //        //td.InnerHtml = "<a href=\" javascript: setFormValue(" + row["id_" + Object.Extension + "Profile"] + ",'SelectedObject',1)\" >";
            //        td.InnerHtml = "<a href=\"?nid=" + SelectedNetwork + "&amp;aid=" + idExternalAppProfile;
            //        td.InnerHtml += "\" >";
            //        td.InnerHtml += "<span>Imposta</span>";
            //        td.InnerHtml += "</a>";
            //        tr.Controls.Add(td);

            //        td = new HtmlGenericControl("td");
            //        td.Attributes["class"] = "delete";
            //        string href = "javascript: setFormValueConfirm('" + idExternalAppProfile + "','DeleteProfile',1,'Sei sicuro di voler rimuovere questa specifica?')";
            //        td.InnerHtml = "<a href=\"" + href + "\" >";
            //        td.InnerHtml += "<span>Rimuovi</span>";
            //        td.InnerHtml += "</a>";
            //        tr.Controls.Add(td);

            //        table.Controls.Add(tr);
            //    }
            //}

            VerticalApplication vapplication = VerticalAppBusinessLogic.GetVerticalAppById(SelectedApplicationID.IntValue);
            var externalAppsProfileQuery = vapplication.GrantObjectProfiles.Where(x=>(x as ExternalAppProfile).Network == this.SelectedNetwork.IntValue);

            var usersToFilter = externalAppsProfileQuery.Select(x => x.Profile.ID);

            SearchCondition[] additionalConditions = new SearchCondition[] { new SearchCondition("ID", usersToFilter.ToList<int>(), SearchCondition.ComparisonType.In, SearchCondition.CriteriaType.Profile) };

            ICollection<Users.User> usersList = st.UsersIAdmin(0, st.UsersIAdminCount(null, additionalConditions), null, additionalConditions);

            foreach (Users.User user in usersList)
            {
                
                
                int idExternalAppProfile = 0;
                if (externalAppsProfileQuery.Count() > 0)
                    idExternalAppProfile = externalAppsProfileQuery.Where(x=>x.Profile.ID == user.Profile.ID).First().ID;

                if (idExternalAppProfile > 0)
                {

                    tr = new HtmlGenericControl("tr");

                    HtmlGenericControl td;
                    td = new HtmlGenericControl("td");
                    td.InnerHtml = "&nbsp;";
                    td.Attributes["class"] = "icon_user";
                    tr.Controls.Add(td);

                    td = new HtmlGenericControl("td");
                    td.InnerHtml = user.UserName; //row["Name_User"].ToString();
                    tr.Controls.Add(td);

                    td = new HtmlGenericControl("td");
                    td.InnerHtml = "Utente";
                    tr.Controls.Add(td);

                    //DataTable dt = this.Connection.SqlQuery("SELECT * From UsersGroups WHERE User_UserGroup = " + row["id_User"].ToString());
                    string families = "";
                    foreach (Users.Group group in user.Groups)
                    {
                        string family = "";
                        //NetCms.Users.Group parent = Group.ChildGroups.FindGroup(group["Group_UserGroup"].ToString());
                        NetCms.Users.Group parent = group;//Group.FindGroup(int.Parse(group["Group_UserGroup"].ToString()));
                        while (parent != null)
                        {
                            family = parent.Name + "/" + family;
                            if (parent.Parent != null)
                                //parent = Group.ChildGroups.FindGroup(parent.Parent.ID.ToString());
                                parent = parent.Parent; //Group.FindGroup(parent.Parent.ID);
                            else
                                parent = null;
                        }
                        if (families != "")
                            families += "<br />";
                        families += family;

                    } td = new HtmlGenericControl("td");
                    td.InnerHtml = families;
                    tr.Controls.Add(td);

                    td = new HtmlGenericControl("td");
                    td.Attributes["class"] = "action details";
                    //td.InnerHtml = "<a href=\" javascript: setFormValue(" + row["id_" + Object.Extension + "Profile"] + ",'SelectedObject',1)\" >";
                    td.InnerHtml = "<a href=\"?nid=" + SelectedNetwork + "&amp;show=o&amp;aid=" + idExternalAppProfile;
                    td.InnerHtml += "\" >";
                    td.InnerHtml += "<span>Mostra</span>";
                    td.InnerHtml += "</a>";
                    tr.Controls.Add(td);

                    td = new HtmlGenericControl("td");
                    td.Attributes["class"] = "action grant";
                    //td.InnerHtml = "<a href=\" javascript: setFormValue(" + row["id_" + Object.Extension + "Profile"] + ",'SelectedObject',1)\" >";
                    td.InnerHtml = "<a href=\"?nid=" + SelectedNetwork + "&amp;aid=" + idExternalAppProfile;
                    td.InnerHtml += "\" >";
                    td.InnerHtml += "<span>Imposta</span>";
                    td.InnerHtml += "</a>";
                    tr.Controls.Add(td);

                    td = new HtmlGenericControl("td");
                    td.Attributes["class"] = "delete";
                    string href = "javascript: setFormValueConfirm('" + idExternalAppProfile + "','DeleteProfile',1,'Sei sicuro di voler rimuovere questa specifica?')";
                    td.InnerHtml = "<a href=\"" + href + "\" >";
                    td.InnerHtml += "<span>Rimuovi</span>";
                    td.InnerHtml += "</a>";
                    tr.Controls.Add(td);

                    table.Controls.Add(tr);
                }
            }

            #endregion

            if (table.Controls.Count == 1)//Non ci sono specifiche
            {
                tr = new HtmlGenericControl("tr");

                HtmlGenericControl td;
                td = new HtmlGenericControl("td");
                td.InnerHtml = "&nbsp;Non ci sono specifiche per questo oggetto.<br />Aggiungere un gruppo o un utente per specificare dei permessi per questo oggetto";
                td.Attributes["colspan"] = "6";
                tr.Controls.Add(td);

                table.Controls.Add(tr);
            }

            set.Controls.Add(table);
            //****************************************************************
            set.Controls.Add(SelectedObject.Control);

            set.Controls.Add(DeleteProfile.Control);
            //****************************************************************

            return set;
        }
        private WebControl BuildNetworksTable()
        {
            WebControl container = new WebControl(HtmlTextWriterTag.Div);
            
            #region Info
            Collapsable col = new Collapsable("Permessi per le Applicazioni");
                      
            G2Core.UI.DetailsSheet sheet = new G2Core.UI.DetailsSheet("Informazioni", "Questa permette di accedere alla gestione dei permessi per le singole Applicazioni.");
            sheet.SeparateColumns = G2Core.UI.DetailsSheet.SeparateColumnsOptions.No;
            sheet.AddColumn("&nbsp;", G2Core.UI.DetailsSheet.Colors.Default, 2);

            sheet.AddTitleTextRow("", @"<p class=""big""><strong>'Imposta Permessi Generici' consente di impostare permessi per:</strong>
                                         <ul><li>Applicazioni che non sono correlate ad uno specifico portale ma la cui valenza è generica.</li>
                                         <li>Applicazioni che sono legate ai portali. I permessi assegnati saranno valevoli su tutti i portali presenti nel sistema.</li></ul></p>");
            //sheet.AddTitleTextRow("", "");
            sheet.AddTitleTextRow("", @"<p class=""big""><strong>'Permessi di portale' consente di impostare permessi per:</strong>
                                        <ul><li>Applicazioni correlate ad uno specifico portale. I permessi assegnati consentono di utilizzare l'applicazione solo in riferimento al portale selezionato.</li></ul></p>");
            //sheet.AddRow(new DetailsSheetRow("Generici","<a class=\"action grant key\"><span>Imposta Permessi</span></a>"));

            TableRow l2row = new TableRow();

            TableCell cell2 = new TableCell();
            cell2.Text = "<a class=\"action big key\" href=\"default.aspx?nid=0\"><span>Imposta Permessi Generici</span></a>";
            cell2.ColumnSpan = 2;
            l2row.Cells.Add(cell2);

            sheet.AddRow(l2row, G2Core.UI.DetailsSheet.Colors.Green);


            col.AppendControl(sheet);
            container.Controls.Add(col); 
            #endregion

            #region Networks
            col = new Collapsable("Gestione dei Permessi per le Applicazioni");

            IList<BasicNetwork> networks = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.Select(x => x.Value).Where(x=>x.Enabled).ToList();//new NetCms.Networks.NetworksData();

            List<BasicNetwork> rows = new List<BasicNetwork>();
            
            foreach (BasicNetwork rowNet in networks)
            {
                if (this.Account.NetworkEnabledForUser(rowNet.ID))
                    rows.Add(rowNet);
            }

            ArTable table = new ArTable(rows);

            //G2Core.AdvancedXhtmlTable.AxtTable table = new G2Core.AdvancedXhtmlTable.AxtTable(rows);
            table.InnerTableCssClass = "tab";
            table.ID = "Networks";

            ArTextColumn nome = new ArTextColumn("Nome", "Label");
            table.AddColumn(nome);
            ArTextColumn codice = new ArTextColumn("Codice", "SystemName");
            table.AddColumn(codice);
            ArOptionsColumn stato = new ArOptionsColumn("Stato", "Enabled", new string[] { "Disablilitata", "Abilitata" });
            table.AddColumn(stato);
            ArActionColumn impostaPermessi = new ArActionColumn("Imposta Permessi", ArActionColumn.Icons.Disable, "default.aspx?nid={ID}");
            impostaPermessi.CssClass = "action grant key";
            table.AddColumn(impostaPermessi);
            //table.Columns.Add(new G2Core.AdvancedXhtmlTable.AxtGenericColumn("Label_Network", "Nome"));
            //table.Columns.Add(new G2Core.AdvancedXhtmlTable.AxtGenericColumn("Nome_Network", "Codice"));
            //table.Columns.Add(new G2Core.AdvancedXhtmlTable.AxtBooleanColumn("Enabled_Network", "Stato", "Disablilitata,Abilitata"));
            //table.Columns.Add(new G2Core.AdvancedXhtmlTable.AxtActionColumn("id_Network", "Imposta Permessi", "action grant key", "default.aspx?nid={id_Network}"));

            col.AppendControl(table);

            container.Controls.Add(col);

            #endregion

            return container;
        }
    }

}
