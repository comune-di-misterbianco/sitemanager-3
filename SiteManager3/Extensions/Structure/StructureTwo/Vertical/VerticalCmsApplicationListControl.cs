﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using NetCms.Structure.Grants;
using NetCms.Users;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Vertical.Grants;
using NetCms.Vertical.BusinessLogic;
using System.Net;
using System.Collections.Generic;

namespace NetCms.Vertical
{
    public class VerticalCmsApplicationListControl : WebControl //GUI.SiteManagerHtmlTable
    {
        //private NetCms.Connections.Connection _Connection;
        //private NetCms.Connections.Connection Connection
        //{
        //    get
        //    {
        //        if (_Connection == null)
        //            _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
        //        return _Connection;
        //    }
        //}
        
        public int ShowOnlyGroups
        {
            get { return _ShowOnlyGroups; }
            set { _ShowOnlyGroups = value; }
        }
        private int _ShowOnlyGroups = -1;
        
        public bool NetworkDependant
        {
            get { return _NetworkDependant; }
            set { _NetworkDependant = value; }
        }
        private bool _NetworkDependant;

        //public NetCms.Users.User CurrentUser
        //{
        //    get
        //    {
        //        if (_CurrentUser == null)
        //            _CurrentUser = NetCms.Users.AccountManager.CurrentAccount;
        //        return _CurrentUser;
        //    }
        //}
        //private NetCms.Users.User _CurrentUser;

        public NetCms.Networks.Network CurrentNetwork
        {
            get
            {
                if (_CurrentNetwork == null)
                    _CurrentNetwork = NetCms.Networks.NetworksManager.CurrentActiveNetwork;
                return _CurrentNetwork;
            }
        }
        private NetCms.Networks.Network _CurrentNetwork;

        public bool InsideNetwork
        {
            get { return _InsideNetwork; }
            private set { _InsideNetwork = value; }
        }
        private bool _InsideNetwork;

        public string HrefToApplicationLink
        {
            get { return _HrefToApplicationLink; }
            private set { _HrefToApplicationLink = value; }
        }
        private string _HrefToApplicationLink;

        public VerticalCmsApplicationListControl(int networkID):base(HtmlTextWriterTag.Div)
        {
            HrefToApplicationLink = NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/grants/default.aspx?nid=" + networkID + "&amp;app={0}";
            this.InsideNetwork = NetCms.Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork;
            this.CssClass = "VerticalCmsApplicationListControl";
            //StructureFolder currentFolder = FolderBusinessLogic.GetById(CurrentNetwork.CurrentFolder.ID);
            //this.StartCollapsed = InsideNetwork && !(currentFolder.IsRootFolder);
        }

        protected override void OnPreRender(EventArgs e)
        {

            WebControl divWrapper = new WebControl(HtmlTextWriterTag.Div);
            divWrapper.ID = "netApplicationBar";
            divWrapper.CssClass = "wrapper";
            this.Controls.Add(divWrapper);
            WebControl divContent = new WebControl(HtmlTextWriterTag.Div);
            divContent.ID = "content_1";
            divContent.CssClass = "content";
            divWrapper.Controls.Add(divContent);

            WebControl lista = new WebControl(HtmlTextWriterTag.Ul);
            divContent.Controls.Add(lista);

            //Codice aggiunto per non visualizzare le app presenti dentro una VerticalApplicationPackage
            List<int> appContained = new List<int>();
            foreach (var app in VerticalApplicationsPool.ActiveAssemblies)
                if (app is VerticalApplicationPackage)
                {
                    foreach (int id in ((VerticalApplicationPackage)app).ContainedApplicationsIDs)
                        if (!appContained.Contains(id))
                            appContained.Add(id);
                }


            var externalAppz = VerticalAppBusinessLogic.FindAllVerticalApplications().AsParallel().Where(x=>VerticalApplicationsPool.ActiveAssemblies.Contains(x.ID) && x.Enabled && x.NetworkDependant && x.AccessMode != VerticalApplication.AccessModes.Front);
            //string sql = "SELECT * FROM ExternalAppz WHERE Enabled_ExternalApp = 1 AND NetworkDependant_ExternalApp = 1 AND AccessoMode_ExternalApp <> 1";//Filtra e applicazioni che dipendono da una Network
            //if(ShowOnlyGroups>-1)
                //sql += " AND HomepageGroup_ExternalApp = 0";//Filtra che vanno mostrate in questa tabella di riepilogo

            //DataTable datatable = this.Connection.SqlQuery(sql);
            //foreach (DataRow app in datatable.Rows)
            int i = 1;
            foreach(VerticalApplication vapp in externalAppz)
            {
                //int vappId = int.Parse(app["id_ExternalApp"].ToString());
                //GrantElement grantElement = new GrantElement(vappId.ToString(), this.CurrentUser.Profile, this.CurrentNetwork.CacheKey);
                //if(grantElement.Grant)

                //Non vengono mostrate le app inglobate dentro una vapp package
                if (vapp.Grant && !appContained.Contains(vapp.ID))
                {
                    //VerticalApplication va = VerticalApplicationsPool.GetByID(vappId);

                    string Icon = "FS_Icon application_" + vapp.SystemName;//va.SystemName;

                    //TableRow tr = new TableRow();
                    ////this.Rows.Add(tr);

                    //TableCell td = new TableCell();
                    //td.Text = "&nbsp;";
                    //td.Attributes["class"] = Icon;
                    //td.Attributes.CssStyle.Add(HtmlTextWriterStyle.BackgroundImage, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Table_Gear));
                    //tr.Cells.Add(td);

                    //Devo leggerlo perchè per il meccanismo dell'ereditarietà non verrebbe considerato con un lettura nhibernate
                    string backofficeUrl = VerticalApplicationsPool.ActiveAssemblies[vapp.ID.ToString()].BackofficeUrl;

                    //td = new TableCell();
                    string href = (InsideNetwork ? CurrentNetwork.Paths.AbsoluteAdminRoot : Configurations.Paths.AbsoluteSiteManagerRoot) + "/applications/" + vapp.SystemName + backofficeUrl;
                    //td.Text = "<a href=\"" + href + "\">" + vapp.Label + "</a>";
                    //tr.Cells.Add(td);

                    //td = new TableCell();
                    //td.CssClass = "action grant";

                    //if (vapp.Criteria["grants"].Allowed)
                    //{
                    //    td.Text = "<a href=\"" + string.Format(this.HrefToApplicationLink, vapp.ID) + "\"><span>Imposta Permessi</span></a>";
                    //}
                    //else
                    //    td.Text = "&nbsp;";
                    //tr.Cells.Add(td);

                    //Nuovo codice e grafica
                    WebControl li = new WebControl(HtmlTextWriterTag.Li);
                    li.CssClass = "app-" + i;

                    WebControl divApp = new WebControl(HtmlTextWriterTag.Div);
                    divApp.CssClass = "app";
                    li.Controls.Add(divApp);

                    WebControl divCapSlide = new WebControl(HtmlTextWriterTag.Div);
                    divCapSlide.CssClass = "capslide_img_cont ic_container";
                    divApp.Controls.Add(divCapSlide);

                    WebControl spanIcon = new WebControl(HtmlTextWriterTag.Span);
                    spanIcon.CssClass = Icon;
                    //spanIcon.Attributes.CssStyle.Add(HtmlTextWriterStyle.BackgroundImage, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Table_Gear));
                    divCapSlide.Controls.Add(spanIcon);

                    WebControl spanTitle = new WebControl(HtmlTextWriterTag.Span);
                    spanTitle.CssClass = "title";
                    spanTitle.Controls.Add(new LiteralControl(vapp.Label));
                    divCapSlide.Controls.Add(spanTitle);

                    WebControl divOverlay = new WebControl(HtmlTextWriterTag.Div);
                    divOverlay.CssClass = "overlay";
                    divOverlay.Style["display"] = "none";
                    divCapSlide.Controls.Add(divOverlay);

                    WebControl divIcCaption = new WebControl(HtmlTextWriterTag.Div);
                    divIcCaption.CssClass = "ic_caption";
                    divCapSlide.Controls.Add(divIcCaption);

                    //Link per andare all'applicazione
                    WebControl pGo = new WebControl(HtmlTextWriterTag.P);
                    pGo.CssClass = "ic_category";
                    WebControl linkGo = new WebControl(HtmlTextWriterTag.A);
                    linkGo.Attributes["href"] = href;
                    linkGo.Attributes["title"] = "Vai all'applicazione";
                    linkGo.Controls.Add(new LiteralControl("Accedi"));
                    pGo.Controls.Add(linkGo);
                    divIcCaption.Controls.Add(pGo);

                    //Link per impostare i permessi
                    WebControl pGrant = new WebControl(HtmlTextWriterTag.P);
                    pGrant.CssClass = "ic_category";
                    WebControl linkGrant = new WebControl(HtmlTextWriterTag.A);

                    VerticalApplication grantsApp = VerticalApplicationsPool.GetByID(NetCms.Users.VerticalGrantsApplication.ApplicationID);

                    if (vapp.Criteria["grants"].Allowed && grantsApp.Grant)
                    {
                        linkGrant.Attributes["href"] = WebUtility.HtmlDecode(string.Format(this.HrefToApplicationLink, vapp.ID));
                        linkGrant.Attributes["title"] = "Imposta i permessi";
                        linkGrant.Controls.Add(new LiteralControl("Imposta permessi"));
                    }
                    else
                    {
                        linkGrant.Attributes["href"] = "#";
                        linkGrant.Attributes["title"] = "Imposta permessi non consentito";
                        linkGrant.Controls.Add(new LiteralControl("&nbsp;"));
                    }
                    
                    pGrant.Controls.Add(linkGrant);
                    divIcCaption.Controls.Add(pGrant);

                    lista.Controls.Add(li);
                    i++;
                }
            }
            base.OnPreRender(e);
        }
    }
}
