﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using NetCms.Vertical.BusinessLogic;
using NetCms.Networks.Grants;
using System.Collections.Generic;

namespace NetCms.Vertical
{
    public class VerticalApplicationListControl : GUI.SiteManagerHtmlTable
    {
        
        public int ShowOnlyGroups
        {
            get { return _ShowOnlyGroups; }
            set { _ShowOnlyGroups = value; }
        }
        private int _ShowOnlyGroups = -1;
        
        public bool NetworkDependant
        {
            get { return _NetworkDependant; }
            set { _NetworkDependant = value; }
        }
        private bool _NetworkDependant;

        public NetCms.Users.Profile Profile
        {
            get { return _Profile; }
            private set { _Profile = value; }
        }
        private NetCms.Users.Profile _Profile;
        
        public int NetworkID
        {
            get { return _NetworkID; }
            private set { _NetworkID = value; }
        }
        private int _NetworkID;
        
					
        public VerticalApplicationListControl(NetCms.Users.Profile profile,int networkID)
        {
            this.NetworkID = networkID;
            this.Profile = profile;
        }

        protected override void OnPreRender(EventArgs e)
        {
            TableHeaderCell hcell = new TableHeaderCell();
            hcell.Text = "Elenco Applicazioni di Portale";
            this.TitleControl.Controls.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "&nbsp;";
            this.HeaderRow.Controls.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Applicazione";
            this.HeaderRow.Controls.Add(hcell);

            hcell = new TableHeaderCell();
            hcell.Text = "Imposta Permessi";
            this.HeaderRow.Controls.Add(hcell);

            //Codice aggiunto per non visualizzare le app presenti dentro una VerticalApplicationPackage
            List<int> appContained = new List<int>();
            foreach (var app in VerticalApplicationsPool.ActiveAssemblies)
                if (app is VerticalApplicationPackage)
                {
                    foreach (int id in ((VerticalApplicationPackage)app).ContainedApplicationsIDs)
                        if (!appContained.Contains(id))
                            appContained.Add(id);
                }

            //Seleziono tutte le network che sono installate, anzichè come faceva prima selezionandole tutte
            var externalAppz = VerticalAppBusinessLogic.FindAllVerticalApplications().Where(x => VerticalApplicationsPool.ActiveAssemblies.Contains(x.ID));
            //Se è attivo il flag network dependant le filtro
            if (NetworkDependant)
                externalAppz = externalAppz.Where(x => x.NetworkDependant);

            //string sql = "SELECT * FROM externalappz";
            //if (NetworkDependant) sql += " WHERE NetworkDependant_ExternalApp = 1";//Filtra e applicazioni che dipendono da una Network
            ////if(ShowOnlyGroups>-1)
            //    //sql += " AND HomepageGroup_ExternalApp = 0";//Filtra che vanno mostrate in questa tabella di riepilogo

            //DataTable datatable = this.Connection.SqlQuery(sql);
            //foreach (DataRow app in datatable.Rows)
            //{
            foreach(VerticalApplication vapp in externalAppz)
            {
                GrantsFinder grants ;
                if (NetworkDependant)
                    grants = vapp.CriteriaByProfileAndNetwork(this.Profile, new Networks.NetworkKey(this.NetworkID)); //new GrantElement(vapp, this.Profile, new Networks.NetworkKey(this.NetworkID));
                else
                    grants = vapp.CriteriaByProfile(this.Profile);

                if (grants[NetCms.Vertical.VerticalApplicationSetup.GrantsCriteriaName].Allowed && !appContained.Contains(vapp.ID))
                {
                    string Icon = "FS_Icon application_" + vapp.SystemName;//app["Key_ExternalApp"];

                    TableRow tr = new TableRow();
                    this.Rows.Add(tr);

                    TableCell td = new TableCell();
                    td.Text = "&nbsp;";
                    td.Attributes["class"] = Icon;
                    td.Attributes.CssStyle.Add(HtmlTextWriterStyle.BackgroundImage, NetCms.GUI.GuiUtility.IconsIndexer.GetImageURL(NetCms.GUI.Icons.Icons.Table_Gear));
                    tr.Cells.Add(td);

                    td = new TableCell();
                    td.Text = vapp.Label;//app["Nome_ExternalApp"].ToString();
                    tr.Cells.Add(td);

                    td = new TableCell();
                    td.CssClass = "action grant";

                    //http://localhost:52904/website/cms/applications/grants/default.aspx?nid=1

                    td.Text = "<a href=\"" + NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/grants/default.aspx?nid=" + NetworkID + "&amp;app=" + vapp.ID + "\"><span>Imposta Permessi</span></a>";

                    tr.Cells.Add(td);
                }
            }
            base.OnPreRender(e);
        }
    }
}
