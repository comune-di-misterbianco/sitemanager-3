﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using NetCms.Vertical;

namespace NetCms.Structure.Applications
{
    public class InstallersApplication:VerticalApplication
    {
        public override int HomepageGroup
        {
            get { return 3; }
        }
        public override AccessModes AccessMode
        {
            get { return AccessModes.Admin; }
        }

        public override string Description
        {
            get { return "Gestisce l'istallazione delle applicazioni."; }
        }

        public override bool CheckForDefaultGrant
        {
            get { return true; }
        }

        public override int ID
        {
            get { return 20; }
        }

        public override string Label
        {
            get { return "Installer"; }
        }

        public override bool NetworkDependant
        {
            get { return false; }
        }

        public override int SecurityLevel
        {
            get { return 0; }
        }

        public override string SystemName
        {
            get { return "installer"; }
        }

        public override VerticalApplicationSetup Installer
        {
            get
            {
                return null;
            }
        }

        public override bool DoAdditionalConfigurationForExternalDB()
        {
            throw new NotImplementedException();
        }

        public override bool NeedAdditionalConfiguration
        {
            get { return false; }
        }

        public InstallersApplication(Assembly assembly)
            : base(assembly)
        {            
        }

        public override bool UseCmsSessionFactory
        {
            get { return false; }
        }
    }
}
