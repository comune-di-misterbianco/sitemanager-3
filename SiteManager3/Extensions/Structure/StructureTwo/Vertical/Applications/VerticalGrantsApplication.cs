﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using NetCms.Vertical;

namespace NetCms.Users
{
    public class VerticalGrantsApplication:VerticalApplication
    {
        public const int ApplicationID = 4;

        public override VerticalApplication.AccessModes AccessMode
        {
            get { return AccessModes.Admin; }
        }
        public override string Description
        {
            get { return "Gestione dei permessi delle applicazioni"; }
        }

        public override int HomepageGroup
        {
            get { return 1; }
        }

        public override int ID
        {
            get { return ApplicationID; }
        }

        public override bool CheckForDefaultGrant
        {
            get { return true; }
        }

        public override string Label
        {
            get { return "Permessi Applicazioni"; }
        }

        public override bool NetworkDependant
        {
            get { return false; }
        }

        public override int SecurityLevel
        {
            get { return 0; }
        }

        public override string SystemName
        {
            get { return "grants"; }
        }

        public override VerticalApplicationSetup Installer
        {
            get
            {
                return null;
            }
        }

        public override bool DoAdditionalConfigurationForExternalDB()
        {
            throw new NotImplementedException();
        }

        public override bool NeedAdditionalConfiguration
        {
            get { return false; }
        }

        public VerticalGrantsApplication(Assembly assembly)
            : base(assembly)
        {            
        }

        public override bool UseCmsSessionFactory
        {
            get { return false; }
        }
    }
}
