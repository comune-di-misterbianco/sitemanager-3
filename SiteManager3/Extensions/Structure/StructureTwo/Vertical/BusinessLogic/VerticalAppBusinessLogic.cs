﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.DBSessionProvider;
using NHibernate;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using NHibernate.Criterion;
using NetCms.Users;
using NetCms.Users.CustomAnagrafe;

namespace NetCms.Vertical.BusinessLogic
{
    public static class VerticalAppBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static VerticalApplication GetVerticalAppByKey(string key, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            SearchParameter keySP = new SearchParameter(null, "SystemName", key, Finder.ComparisonCriteria.Equals, false);

            IGenericDAO<VerticalApplication> dao = new CriteriaNhibernateDAO<VerticalApplication>(innerSession);
            return dao.GetByCriteria(new SearchParameter[] { keySP });
        }

        public static VerticalApplication GetVerticalAppForFixer(int id, string nome, string key, int homeGroup,string descr,bool netwDep,int access, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            SearchParameter idSP = new SearchParameter(null, "ID", id, Finder.ComparisonCriteria.Equals, false);
            SearchParameter nomeSP = new SearchParameter(null, "Label", nome, Finder.ComparisonCriteria.Equals, false);
            SearchParameter keySP = new SearchParameter(null, "SystemName", key, Finder.ComparisonCriteria.Equals, false);
            SearchParameter homeSP = new SearchParameter(null, "HomepageGroup", homeGroup, Finder.ComparisonCriteria.Equals, false);
            SearchParameter descSP = new SearchParameter(null, "Description", descr, Finder.ComparisonCriteria.Equals, false);
            SearchParameter netSP = new SearchParameter(null, "NetworkDependant", netwDep, Finder.ComparisonCriteria.Equals, false);
            SearchParameter accessSP = new SearchParameter(null, "AccessModeInt", access, Finder.ComparisonCriteria.Equals, false);

            IGenericDAO<VerticalApplication> dao = new CriteriaNhibernateDAO<VerticalApplication>(innerSession);
            return dao.GetByCriteria(new SearchParameter[] { idSP,nomeSP,keySP,homeSP,descSP,netSP,accessSP });
        }

        public static IList<VerticalApplication> FindAllVerticalApplications(ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<VerticalApplication> dao = new CriteriaNhibernateDAO<VerticalApplication>(innerSession);
            return dao.FindAll();
        }

        public static VerticalApplication GetVerticalAppById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<VerticalApplication> dao = new CriteriaNhibernateDAO<VerticalApplication>(innerSession);
            return dao.GetById(id);
        }


        public static bool EnableVerticalApp(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<VerticalApplication> dao = new CriteriaNhibernateDAO<VerticalApplication>(innerSession);
            VerticalApplication vapp = dao.GetById(id);
            vapp.Enabled = true;

            return SaveOrUpdateVerticalApplication(vapp, innerSession) != null;
        }

        public static bool DisableVerticalApp(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<VerticalApplication> dao = new CriteriaNhibernateDAO<VerticalApplication>(innerSession);
            VerticalApplication vapp = dao.GetById(id);
            vapp.Enabled = false;

            return SaveOrUpdateVerticalApplication(vapp, innerSession) != null;
        }

        public static VerticalApplication SaveOrUpdateVerticalApplication(VerticalApplication vapp, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<VerticalApplication> dao = new CriteriaNhibernateDAO<VerticalApplication>(innerSession);
            return dao.SaveOrUpdate(vapp);
        }

        public static bool CheckExternalAppPage(int idApplication, string pageName, string className, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            SearchParameter pageNameSP = new SearchParameter(null, "PageName", pageName, Finder.ComparisonCriteria.Equals, false);
            SearchParameter classNameSP = new SearchParameter(null, "ClassName", className, Finder.ComparisonCriteria.Equals, false);
            SearchParameter appSP = new SearchParameter(null, "Application.ID", idApplication, Finder.ComparisonCriteria.Equals, false);

            IGenericDAO<ExternalAppPage> dao = new CriteriaNhibernateDAO<ExternalAppPage>(innerSession);
            return dao.FindByCriteriaCount(new SearchParameter[] { pageNameSP, classNameSP, appSP }) > 0;
        }

        //DA TESTARE
        public static ExternalAppPage GetExternalAppPageByPageName(int idApplication, string pageName)
        {
            //ICriteria criteria = GetCurrentSession().CreateCriteria(typeof(ExternalAppPage));
            //criteria.Add(Restrictions.Eq("Application.ID", idApplication));
            //Disjunction or = new Disjunction();
            //or.Add(Restrictions.Eq("PageName", pageName));
            //or.Add(Restrictions.Eq("PageName", "/" + pageName));
            //criteria.Add(or);

            //ExternalAppPage pageWithCriteria = (ExternalAppPage)criteria.UniqueResult();


            IGenericDAO<ExternalAppPage> dao = new CriteriaNhibernateDAO<ExternalAppPage>(GetCurrentSession());
            SearchParameter appSP = new SearchParameter(null, "Application.ID", idApplication, Finder.ComparisonCriteria.Equals, false);

            SearchParameter spPageName1 = new SearchParameter("", "PageName", pageName, Finder.ComparisonCriteria.Equals, false);
            SearchParameter spPageName2 = new SearchParameter("", "PageName", "/" + pageName, Finder.ComparisonCriteria.Equals, false);
            SearchParameter spOR = new SearchParameter("", SearchParameter.BehaviourClause.Or, new SearchParameter[] { spPageName1, spPageName2 });

            IList<ExternalAppPage> list = dao.FindByCriteria(new SearchParameter[] { appSP, spOR }, true);
            //ExternalAppPage pageWithSP = list.FirstOrDefault();

            //if (pageWithCriteria != null && pageWithSP != null)
            //{
            //    if (pageWithCriteria.PageName != pageWithSP.PageName || pageWithCriteria.ID != pageWithSP.ID)
            //        NetCms.GUI.PopupBox.AddMessage("Risultato non congruente tra query fatta con i criteria e quella fatta coi searchParameter nel metodo GetExternalAppPageByPageName");
            //}
            return list.FirstOrDefault();
        }

        //DA TESTARE
        public static ExternalAppPage GetExternalAppPageByPageNameAndKey(string keyApplication, string pageName)
        {
            //ICriteria criteria = GetCurrentSession().CreateCriteria(typeof(ExternalAppPage));
            //criteria.CreateAlias("Application", "app");
            //criteria.Add(Restrictions.Eq("app.SystemName", keyApplication));
            //Disjunction or = new Disjunction();
            //or.Add(Restrictions.Eq("PageName", pageName));
            //or.Add(Restrictions.Eq("PageName", "/" + pageName));
            //criteria.Add(or);

            //ExternalAppPage pageWithCriteria = (ExternalAppPage)criteria.UniqueResult();


            IGenericDAO<ExternalAppPage> dao = new CriteriaNhibernateDAO<ExternalAppPage>(GetCurrentSession());
            SearchParameter spName = new SearchParameter("", "SystemName", keyApplication, Finder.ComparisonCriteria.Equals, false);
            SearchParameter spSysName = new SearchParameter("", "Application", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { spName });

            SearchParameter spPageName1 = new SearchParameter("", "PageName", pageName, Finder.ComparisonCriteria.Equals, false);
            SearchParameter spPageName2 = new SearchParameter("", "PageName", "/" + pageName, Finder.ComparisonCriteria.Equals, false);
            SearchParameter spOR = new SearchParameter("", SearchParameter.BehaviourClause.Or, new SearchParameter[] { spPageName1,spPageName2 });

            IList<ExternalAppPage> list = dao.FindByCriteria(new SearchParameter[] { spSysName, spOR }, true);
            //ExternalAppPage pageWithSP = list.FirstOrDefault();

            //if (pageWithCriteria != null && pageWithSP != null)
            //{
            //    if (pageWithCriteria.PageName != pageWithSP.PageName || pageWithCriteria.ID != pageWithSP.ID)
            //        NetCms.GUI.PopupBox.AddMessage("Risultato non congruente tra query fatta con i criteria e quella fatta coi searchParameter nel metodo GetExternalAppPageByPageNameAndKey");
            //}
            return list.FirstOrDefault();
        }

        public static void DeleteVerticalApp(VerticalApplication vapp, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<VerticalApplication> dao = new CriteriaNhibernateDAO<VerticalApplication>(innerSession);
            dao.Delete(vapp);
        }

        public static VerticalApplicationServiceUser SaveOrUpdateVerticalApplicationServiceUser(VerticalApplicationServiceUser assoc, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<VerticalApplicationServiceUser> dao = new CriteriaNhibernateDAO<VerticalApplicationServiceUser>(innerSession);
            return dao.SaveOrUpdate(assoc);
        }

        public static VerticalApplicationServiceUser GetVerticalApplicationServiceUserById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<VerticalApplicationServiceUser> dao = new CriteriaNhibernateDAO<VerticalApplicationServiceUser>(innerSession);
            return dao.GetById(id);
        }

        public static VerticalApplicationServiceUser GetVerticalApplicationServiceUserByUserAndVApp(User user, VerticalApplication vapp, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<VerticalApplicationServiceUser> dao = new CriteriaNhibernateDAO<VerticalApplicationServiceUser>(innerSession);
            SearchParameter userSP = new SearchParameter(null, "User", user, Finder.ComparisonCriteria.Equals, false);
            SearchParameter vappSP = new SearchParameter(null, "VerticalApplication", vapp, Finder.ComparisonCriteria.Equals, false);
            SearchParameter scadenzaSp = new SearchParameter(null, "ScadenzaServizio", DateTime.Now, Finder.ComparisonCriteria.GreaterThan, false);

            return dao.GetByCriteria(new SearchParameter[] { userSP, vappSP, scadenzaSp });
        }

        public static void DeleteVerticalApplicationServiceUser(VerticalApplicationServiceUser service, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<VerticalApplicationServiceUser> dao = new CriteriaNhibernateDAO<VerticalApplicationServiceUser>(innerSession);
            dao.Delete(service);
        }

        public static ICollection<VerticalApplicationWithService> FindAllServicesByCustomAnagrafe(CustomAnagrafe ca)
        {

            ICollection<VerticalApplication> applicazioniDisponibili = ca.Applicazioni.Select(x => VerticalApplicationsPool.GetByID(x.IdApplicazione)).ToList();
            ICollection<VerticalApplicationWithService> serviziDisponibili = applicazioniDisponibili.ToList().ConvertAll(x => x as VerticalApplicationWithService);

            return serviziDisponibili;
        }

        public static ICollection<VerticalApplicationWithService> FindAllServicesByCustomAnagrafe(CustomAnagrafe ca, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();


            ICollection<VerticalApplication> applicazioniDisponibili = ca.Applicazioni.Select(x => VerticalApplicationsPool.GetByID(x.IdApplicazione)).ToList();
            ICollection<VerticalApplicationWithService> serviziDisponibili = applicazioniDisponibili.Where(x => x.Enabled).ToList().ConvertAll(x => x as VerticalApplicationWithService);

            return serviziDisponibili;
        }

        public static ICollection<VerticalApplicationServiceUser> FindServicesByUser(User user, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<VerticalApplicationServiceUser> dao = new CriteriaNhibernateDAO<VerticalApplicationServiceUser>(innerSession);
            SearchParameter userSP = new SearchParameter(null, "User", user, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[] { userSP });
        }

        public static bool UserHasService(User user, VerticalApplication vapp, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<VerticalApplicationServiceUser> dao = new CriteriaNhibernateDAO<VerticalApplicationServiceUser>(innerSession);
            SearchParameter userSP = new SearchParameter(null, "User", user, Finder.ComparisonCriteria.Equals, false);
            SearchParameter vappSP = new SearchParameter(null, "VerticalApplication", vapp, Finder.ComparisonCriteria.Equals, false);
            SearchParameter scadenzaSp = new SearchParameter(null, "ScadenzaServizio", DateTime.Now, Finder.ComparisonCriteria.GreaterThan, false);

            return dao.GetByCriteria(new SearchParameter[] { userSP, vappSP, scadenzaSp }) != null;
        }

        public static ICollection<VerticalApplicationServiceUser> FindServiceUsersByVerticalApp(VerticalApplication vapp, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<VerticalApplicationServiceUser> dao = new CriteriaNhibernateDAO<VerticalApplicationServiceUser>(innerSession);
            SearchParameter vappSP = new SearchParameter(null, "VerticalApplication", vapp, Finder.ComparisonCriteria.Equals, false);
            //SearchParameter scadenzaSp = new SearchParameter(null, "ScadenzaServizio", DateTime.Now, Finder.ComparisonCriteria.GreaterThan, false);

            return dao.FindByCriteria(new SearchParameter[] { vappSP });
        }
    }
}
