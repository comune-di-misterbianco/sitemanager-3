﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using NHibernate;
using NetCms.Vertical.BusinessLogic;
using NetCms.Grants;

namespace NetCms.Vertical
{
    public abstract class VerticalApplicationSetup
    {
        public const string DefaultCriteriaName = "enable";
        public const string GrantsCriteriaName = "grants";

        public NetCms.Connections.Connection Connection
        {
            get
            {
                return ApplicationBaseData.Conn;
            }
        }
        /*
        public const string AddApplicationSql = @"
    INSERT INTO externalappz 
    (
        id_ExternalApp,
        Nome_ExternalApp,
        Key_ExternalApp,
        HomepageGroup_ExternalApp,
        Description_ExternalApp,
        NetworkDependant_ExternalApp,
        AccessoMode_ExternalApp,
        Enabled_ExternalApp
    ) 
    VALUES 
    (
        {0}
        ,'{1}'
        ,'{2}'
        ,{3}
        ,'{4}'
        ,{5}
        ,{6}
        ,{7}
    )
";  */
       // public const string AddApplicationPageSql = "INSERT INTO externalappz_pages (PageName_ExternalAppPage,ClassName_ExternalAppPage,ExternalApp_ExternalAppPage) VALUES ('{0}','{1}',{2})";
       // public const string CheckApplicationPageSql = "SELECT * FROM externalappz_pages WHERE PageName_ExternalAppPage LIKE '{0}' AND ClassName_ExternalAppPage LIKE '{1}' AND ExternalApp_ExternalAppPage = {2}";
        //public const string AddApplicationCriteriaSql = "INSERT INTO externalappzcriteria (Nome_ExternalAppCriteria,Key_ExternalAppCriteria,App_ExternalAppCriteria,Dynamic_ExternalAppCriteria) VALUES ('{0}','{1}',{2},{3})";
        
        /// <summary>
        /// (Deprecated)Questo Array contiene l'elenco delle pagine facenti parte dell'applicazione.
        /// Questo sistema di indicizzazione delle pagine è deprecato, per definire gli indirizzi ella pagine è sufficente
        /// appendere l'attributo PageControl alla pagina stessa es:
        /// [DynamicUrl.PageControl("/details.aspx")]
        /// public class DetailsPage : ApplicationPage
        /// 
        /// é quindi consigliabile effettuare l'override di questa propietà in questo modo:
        /// 
        /// public override string[,] Pages
        ///{
        ///    get
        ///    {
        ///       return null;
        ///    }
        ///}
        /// </summary>
        public abstract string[,] Pages
        {
            get;
        }

        /// <summary>
        /// Questa proprietà definisce i criteri dei permessi per l'applicazione,
        /// deve essere riempita con coppie label/nome_criterio. Es:
        /// public override string[,] Criteria
        /// {
        ///     get
        ///     {
        ///        string[,] criteria = {
        ///                                 {"Abilita gli utenti a fare qualcosa." , "criterio1" } ,
        ///                                 {"Abilita gli utenti a fare qualcos'altro.", "criterio2"},
        ///         return criteria;
        ///     }
        /// }
        /// Se non ci sono criteri personalizzati per l'applicazione restituire l'array vuoto.
        /// Il criterio 'enable' non va messo nell'array in quanto viene creato automaticamente
        /// </summary>
        public abstract string[,] Criteria { get; }

        /// <summary>
        /// Se impostata a true l'appllicazione avrà una diversa istanza per ogni network, e i permessi per l'applicazione saranno gestiti per network
        /// Se impostata a false l'applicazione avrà una sola istanza per tutte le network
        /// </summary>

        public abstract bool IsNetworkDependant { get; }

        public VerticalApplication ApplicationBaseData
        {
            get { return _ApplicationBaseData; }
            private set { _ApplicationBaseData = value; }
        }
        private VerticalApplication _ApplicationBaseData;

        public VerticalApplicationSetup(VerticalApplication baseApp)
        {
            ApplicationBaseData = baseApp;
        }

        private string errors;
        public string Errors
        {
            get
            {
                if (errors == null)
                {
                    errors = "";
                }
                return "<ul>" + errors + "</ul>";
            }
        }
        protected void AddErrorString(string errorToAdd)
        {
            errors += "<li>" + errorToAdd + "</li>";

            //LogAndTrance.ExceptionManager.ExceptionLogger.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, errorToAdd, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Warning);
            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, errorToAdd, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
        }
        public bool Install(bool isInsidePackage = false)
        {
            bool ok = true;
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try 
                {
                    VerticalApplication app = this.AddApplication(sess);
                    if (app != null)
                    {
                        if (ok) ok = AddMainPage(app, sess);
                        if (ok) ok = this.AddPages(app, sess);
                        if (ok) ok = this.AddCriteria(app, sess);
                    }
                    else
                        throw new Exception();
                    tx.Commit();
                    
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    ok = false;
                    return ok;
                }
            }

            if (ok) ok = this.LocalInstall(isInsidePackage);
            if (ok) ok = this.InstallGrants();
            if (ok) ok = this.ApplicationBaseData.UpdateApplicationData();
            if (ok) ok = this.ApplicationBaseData.UpdateInstallationData();

            if (!ok) Uninstall();


            return ok;
        }
        /// <summary>
        /// Definisce le politiche di istallazione custom per l'applicazione.
        /// E' possibile usare questo metodo per creare tabelle su database o file propedeutici per l'applicazione
        /// Se queste non sono necessarie è sufficente che il metodo restituisca true
        /// </summary>
        /// <returns></returns>
        protected abstract bool LocalInstall(bool isInsidePackage = false);

        public virtual bool InstallGrants()
        {
            try
            {
                NetCms.Users.User user = NetCms.Users.AccountManager.CurrentAccount;
                NetCms.Users.UserPermissionRepair fixer = new NetCms.Users.UserPermissionRepair(user.Profile, user);
                return fixer.FixPermission_VerticalApplication(this.ApplicationBaseData.ID);
                
            }
            catch (Exception e)
            {
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Associare i permessi relativi all'applicazione verticale che si cercava di installare.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                return false;
            }
        }

        public virtual bool Uninstall()
        {
            //string sql = "DELETE FROM externalappz WHERE id_ExternalApp = " + this.ApplicationBaseData.ID;
            //Connection.Execute(sql);

            //sql = "DELETE FROM externalappz_pages WHERE ExternalApp_ExternalAppPage = " + this.ApplicationBaseData.ID;
            //Connection.Execute(sql);


            //sql = "DELETE FROM externalappzcriteria WHERE App_ExternalAppCriteria = " + this.ApplicationBaseData.ID;
            //Connection.Execute(sql);
            bool result = true;
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try 
                {
                    VerticalApplication vapp = VerticalAppBusinessLogic.GetVerticalAppById(this.ApplicationBaseData.ID, sess);
                    vapp.Pages.Clear();
                    vapp.AppzCriteria.Clear();
                    
                    IList<ExternalAppProfile> profiles = ExternalAppzGrantsBusinessLogic.GetExternalAppProfiles(vapp.ID,sess);

                    foreach (ExternalAppProfile profile in profiles)
                        vapp.DeleteGrantProfile(profile.ID, sess);

                    VerticalAppBusinessLogic.DeleteVerticalApp(vapp,sess);

                    tx.Commit();
                }
                catch (Exception ex)
                {
                    result = false;
                    tx.Rollback();
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                }
            }

            if (result)
            {
                ApplicationBaseData.UpdateApplicationData();
                ApplicationBaseData.UpdateInstallationData();
            }
            return result;
        }

        public bool AddCriterion(string criteriaLabel, string criteriaKey, VerticalApplication vapp, ISession session)
        {
            ExternalAppzCriteria criteria = new ExternalAppzCriteria();
            criteria.Nome = criteriaLabel;
            criteria.Key = criteriaKey.ToLower();
            criteria.Application = vapp;
            criteria.IsDynamic = vapp.NetworkDependant;
            vapp.AppzCriteria.Add(criteria);

            return ExternalAppzGrantsBusinessLogic.SaveOrUpdateExternalAppCriteria(criteria, session) != null;
            //bool done = false;
            //try
            //{
            //    string sql = string.Format(AddApplicationCriteriaSql, criteriaLabel.Replace("'","''"), criteriaKey.ToLower(), this.ApplicationBaseData.ID, this.IsNetworkDependant?"1":"0");
            //    done = Connection.Execute(sql);
            //    if (done == false) throw new Exception();
            //    return true;
            //}
            //catch (Exception e) 
            //{
            //    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile creare il criterio '" + criteriaKey + "' dell'Applicazione nel database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "58");
            //    return false; 
            //}
        }
        public bool AddMainPage(VerticalApplication vapp, ISession session)
        {
            Type[] types = ApplicationBaseData.Assembly.GetTypes();
            Type MainpageType = Type.GetType(VerticalApplicationClasses.Mainpage_BaseClassName);

            bool MainpageClassFound = types.Count(x => x.BaseType == MainpageType) > 0;

            //Se non esiste la mainpage come classe restiruisco true, altrimenti se esiste restituisco il risultato della creazione della mainpage stessa
            return !MainpageClassFound || this.AddPage(Configurations.Generics.DefaultPageName, this.ApplicationBaseData.Classes.Mainpage, vapp,session);
        }
        protected bool AddPage(string pageName, string codeClass, VerticalApplication vapp, ISession session)
        {
            //bool done = false;
            //try
            //{
                //var res = Connection.SqlQuery(CheckApplicationPageSql.FormatByParameters(pageName.ToLower(), codeClass, applicationID));
                //if (res.Rows.Count == 0)
                if (!VerticalAppBusinessLogic.CheckExternalAppPage(vapp.ID, pageName.ToLower(), codeClass, session))
                {
                    ExternalAppPage page = new ExternalAppPage();
                    page.PageName = pageName.ToLower();
                    page.ClassName = codeClass;
                    page.Application = vapp;
                    vapp.Pages.Add(page);

                    VerticalAppBusinessLogic.SaveOrUpdateVerticalApplication(vapp,session);
                    //string sql = string.Format(AddApplicationPageSql, pageName.ToLower(), codeClass, applicationID);
                    //done = Connection.Execute(sql);
                    //if (done == false) throw new Exception();
                }
                return true;
            //}
            //catch (Exception e)
            //{
            //    /*this.AddErrorString("Non è stato possibile creare la pagina '" + pageName + "' dell'Applicazione nel database.");
            //    NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
            //    ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, e.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, e);*/
            //    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile creare la pagina '" + pageName + "' dell'Applicazione nel database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "57");
            //    return false;
            //}
        }
        public bool AddPages(VerticalApplication vapp, ISession session)
        {
            bool done = true;
            
            if (Pages != null) 
                for (int i = 0; i < this.Pages.Length / 2; i++)
                    done = this.AddPage(this.Pages[i, 0], this.Pages[i, 1], vapp,session);

            var types = this.ApplicationBaseData.Assembly.GetTypes();
            foreach (System.Type t in types)
            {
                System.Attribute[] attrs = System.Attribute.GetCustomAttributes(t);  // reflection

                foreach (System.Attribute attr in attrs)
                {
                    if (attr is DynamicUrl.PageControl)
                    {
                        DynamicUrl.PageControl pageAddress = (DynamicUrl.PageControl)attr;
                        if (!pageAddress.OnlyForDebugPurpose && (this.ApplicationBaseData.ID == pageAddress.ApplicationID || pageAddress.ApplicationID == 0))
                            done = done & this.AddPage(pageAddress.Address, t.FullName, vapp, session);//pageAddress.ApplicationID == 0 ? this.ApplicationBaseData.ID : pageAddress.ApplicationID);
                    }
                }

            }

            return done;
        }
        private bool AddCriteria(VerticalApplication vapp, ISession session)
        {
            bool done = false;

            done = this.AddCriterion("Consenti Utilizzo", DefaultCriteriaName,vapp,session);
            done = this.AddCriterion("Permetti gestione dei permessi", GrantsCriteriaName, vapp, session);
            if (Criteria != null)
                for (int i = 0; i < this.Criteria.Length / 2; i++)
                    done = this.AddCriterion(this.Criteria[i, 0], this.Criteria[i, 1], vapp, session);

            return done;
        }
        protected VerticalApplication AddApplication(ISession session)
        {
            VerticalApplication vapp = new VerticalApplication();
            vapp.ID = this.ApplicationBaseData.ID;
            vapp.Label = this.ApplicationBaseData.Label;
            vapp.SystemName = this.ApplicationBaseData.SystemName;
            vapp.HomepageGroup = this.ApplicationBaseData.HomepageGroup;
            vapp.Description = this.ApplicationBaseData.Description;
            vapp.NetworkDependant = this.ApplicationBaseData.NetworkDependant;
            vapp.AccessModeInt =  (int) this.ApplicationBaseData.AccessMode;

            return VerticalAppBusinessLogic.SaveOrUpdateVerticalApplication(vapp, session);

            //bool done = false;

            //string accessModeValue = "0";
            //switch (this.ApplicationBaseData.AccessMode)
            //{
            //    case VerticalApplication.AccessModes.Admin: accessModeValue = "0"; break;
            //    case VerticalApplication.AccessModes.Front: accessModeValue = "1"; break;
            //    case VerticalApplication.AccessModes.Both: accessModeValue = "2"; break;
            //}

            //try
            //{
            //    string sql = string.Format(AddApplicationSql, 
            //                                this.ApplicationBaseData.ID, 
            //                                this.ApplicationBaseData.Label,
            //                                this.ApplicationBaseData.SystemName,
            //                                this.ApplicationBaseData.HomepageGroup,//5,
            //                                this.ApplicationBaseData.Description,
            //                                this.ApplicationBaseData.NetworkDependant,
            //                                accessModeValue ,
            //                                1
            //                                );
            //    done = Connection.Execute(sql);
            //    if (done == false) throw new Exception();
            //    return true;
            //}
            //catch (Exception e)
            //{
            //    /*this.AddErrorString("Non è stato possibile inserire l'Applicazione nel database.");
            //    NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
            //    ExLog.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, e.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, e);*/
            //    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile inserire l'Applicazione nel database", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "59");
            //    return false;
            //}
        }
    }
}