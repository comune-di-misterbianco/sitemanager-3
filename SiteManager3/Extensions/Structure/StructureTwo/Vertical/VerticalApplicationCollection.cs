using System;
using System.Reflection;
using System.Collections;
/// <summary>
/// Summary description for xNodeCollection
/// </summary>

namespace NetCms.Vertical
{
    public class VerticalApplicationCollection : IEnumerable
    {
        private G2Collection coll;

        public int Count { get { return coll.Count; } }

        public VerticalApplicationCollection()
        {
            coll = new G2Collection();
        }

        public void Add(VerticalApplication obj)
        {
            coll.Add(obj.ID.ToString(), obj);
        }

        public VerticalApplication this[int i]
        {
            get
            {
                return (VerticalApplication)coll[coll.Keys[i]];
            }
        }
        public VerticalApplication this[string VerticalApplicationID]
        {
            get
            {
                return (VerticalApplication)coll[VerticalApplicationID];
            }
        }

        public bool Contains(int id)
        {
            return this[id.ToString()] != null;
        }
        public bool Contains(string key)
        {
            return this[key] != null;
        }
        public void Remove(string key)
        {
            coll.Remove(key);
        }
    #region Enumerator

    public IEnumerator GetEnumerator()
    {
        return new CollectionEnumerator(this);
    }

    private class CollectionEnumerator : IEnumerator
    {
        private int CurentPos = -1;
        private VerticalApplicationCollection Collection;
        public CollectionEnumerator(VerticalApplicationCollection coll)
        {
            Collection = coll;
        }
        public object Current
        {
            get
            {
                return Collection[CurentPos];
            }
        }
        public bool MoveNext()
        {
            if (CurentPos < Collection.Count - 1)
            {
                CurentPos++;
                return true;
            }
            else
                return false;
        }
        public void Reset()
        {
            CurentPos = -1;
        }
    }
    #endregion
    }
}