﻿using System;
using System.IO;
using System.Xml;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.Networks;
using NetCms.Networks.WebFS;
using NetCms.Structure.WebFS;
using NetCms.Structure.Applications;
using NetCms.Grants;

namespace NetCms.Exporter
{
    public abstract class ApplicationExporter
    {
        protected const string Network_Docs = @"SELECT * FROM (Docs INNER JOIN network_docs ON id_Doc = id_NetworkDoc)
            INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang {0}
            WHERE {2} Folder_Doc = {1} AND Application_Doc = {3} AND Trash_Doc = 0";

        public FolderExporter FolderExporter
        {
            get
            {
                return _FolderExporter;
            }
            private set { _FolderExporter = value; }
        }
        private FolderExporter _FolderExporter;

        public abstract DataTable DocsTable { get; }
        public abstract DataTable[] Tables { get; }

        public abstract bool HasFiles { get; }

        public string FolderID
        {
            get { return _FolderID; }
            private set { _FolderID = value; }
        }
        private string _FolderID;


        /// <summary>
        /// Tabella di tutti i documenti non eliminati
        /// </summary>
        private DataTable _RootDocsTable;
        private DataTable RootDocsTable
        {
            get
            {
                if (_RootDocsTable == null)
                {

                    _RootDocsTable = new DataTable();
                    string sql = @"SELECT DISTINCT Application_Doc FROM (Docs INNER JOIN network_docs ON id_Doc = id_NetworkDoc)
            INNER JOIN Docs_Lang ON id_Doc = Doc_DocLang
            WHERE Folder_Doc = " + FolderID + " AND Trash_Doc = 0";
                    this.FolderExporter.Exporter.Conn.FillDataTable(sql, _RootDocsTable);
                    _RootDocsTable.TableName = "docs";
                }
                return _RootDocsTable;
            }
        }

        public ApplicationExporter(string FolderID, FolderExporter folderExporter)
        {
            StructureFolder current = (StructureFolder)NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder;
            if (current.Criteria[CriteriaTypes.Advanced].Value == GrantsValues.Deny)
                NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la pagina al quale hai provato ad accedere.", NetCms.GUI.PostBackMessagesType.Error,NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
        
            FolderExporter = folderExporter;
            this.FolderID = FolderID;
        }

        public bool Export2SystemFolder()
        {
            return Export();
        }

        //public abstract bool Export2NetworkFolder();

        /// <summary>
        /// Metodo per l'esportazione dei documenti di una folder
        /// </summary>
        /// <returns>true</returns>
        public bool Export()
        {
            //Analisi della tabella di docs specifica per l'applicazione
            if (this.DocsTable != null)
            {
                //Esportazione dei file relativi alla tabella
                ExportFiles(this.DocsTable);
                //Esportazione documenti diversi dalla tipologia di cartella
                //if (this.FolderExporter.IsRoot)
                ExportDifferentTypeDocs();
                //Aggiunta della DocsTable al file xml per la successiva reimportazione dei record
                this.DocsTable.TableName = "docs-" + FolderID;
                this.FolderExporter.Exporter.ExportDatabase.Tables.Add(this.DocsTable);
            }
            //Analisi delle Tables accessorie dell'applicazione
            if (Tables != null)
                foreach (DataTable table in Tables)
                {
                    if (table != null)
                    {
                        //table.TableName = "docs-" + FolderID + "[" + table.TableName + "]";
                        //Aggiunta, se non contenute, delle Tables specifiche al file xml
                        if (!this.FolderExporter.Exporter.ExportDatabase.Tables.Contains(table.TableName))
                            this.FolderExporter.Exporter.ExportDatabase.Tables.Add(table);
                    }
                }

            return true;
        }

        private void ExportDifferentTypeDocs()
        {
            StructureFolder folder = (StructureFolder)Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.FindFolderByID(int.Parse(FolderID));
            foreach (DataRow doc in this.RootDocsTable.Rows)
            {

                if (folder.RelativeApplication.ID != int.Parse(doc["Application_Doc"].ToString()))
                {
                    object[] param = { FolderID, this.FolderExporter };
                    Application application = NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies[doc["Application_Doc"].ToString()];
                    ApplicationExporter exporter = (ApplicationExporter)ApplicationsPool.BuildInstanceOf(application.Classes.Exporter, application.ID.ToString(), param);
                    ExportFiles(exporter.DocsTable);
                    exporter.DocsTable.TableName = application.SystemName + "RootDocs-" + FolderID;
                    this.FolderExporter.Exporter.ExportDatabase.Tables.Add(exporter.DocsTable);
                }
            }
        }

        /// <summary>
        /// Metodo di esportazione dei files per una applicazione/folder
        /// </summary>
        /// <param name="table">DocsTable relativa alla folder/applicazione</param>
        public void ExportFiles(DataTable table)
        {

            foreach (DataRow row in table.Rows)
            {
                DataRow row2 = this.FolderExporter.FoldersTable.Select("id_Folder = " + row["Folder_Doc"].ToString())[0];
                StructureFolder folder = (StructureFolder)Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.FindFolderByID(int.Parse(FolderID));
                Application application = folder.RelativeApplication;
                switch (application.ID)
                {
                    case 4: ExportFile(row["id_Doc"].ToString(), row["Link_DocFiles"].ToString(), row2["Path_Folder"].ToString());
                        break;
                    case 227: ExportFile(row["id_Doc"].ToString(), row["Link_AreaDiScambio"].ToString(), row2["Path_Folder"].ToString());
                        break;
                    default: ExportFile(row["id_Doc"].ToString(), row["Name_Doc"].ToString(), row2["Path_Folder"].ToString());
                        break;
                };


            }

        }

        /// <summary>
        /// Metodo per l'esportazione fisica del file
        /// </summary>
        /// <param name="DocumentID">ID Documento</param>
        /// <param name="docname">Nome Documento</param>
        /// <param name="folderpath">Percorso della folder</param>
        /// <returns></returns>
        public string ExportFile(string DocumentID, string docname, string folderpath)
        {
            string FolderNetPath = folderpath;
            string FolderSystemPath = HttpContext.Current.Server.MapPath(this.FolderExporter.Exporter.Network.Paths.AbsoluteFrontRoot + "\\" + FolderNetPath);

            string DocumentSystemPath = FolderSystemPath + "\\" + docname;

            if (File.Exists(DocumentSystemPath))
            {
                string newFileName = "File-" + DocumentID;
                string targetPath = FolderExporter.LocalExportPath + newFileName;

                if (!File.Exists(targetPath))
                {
                    FileInfo info = new FileInfo(DocumentSystemPath);
                    if (info.Length > 0)
                    {
                        //Copia del file opportunamente rinominato per la sua successiva individuazione in base all'id del documento
                        File.Copy(DocumentSystemPath, targetPath);
                        File.SetAttributes(targetPath, FileAttributes.Normal);
                    }
                    else
                    {
                        //Caso in cui il file sia vuoto, ma la presenza necessaria all'applicazione
                        Byte[] b = BitConverter.GetBytes(10);

                        File.WriteAllBytes(targetPath + "-empty", b);
                        File.SetAttributes(targetPath + "-empty", FileAttributes.Offline);
                    }
                }
                return newFileName;
            }
            else return null;

        }

        /// <summary>
        /// Metodo statico per la creazione, tramite reflection, degli oggetti capaci di esportare i contenuti
        /// dell'applicazione a cui appartiene la folder individuata da FolderID
        /// </summary>
        /// <param name="FolderID">ID della folder da cui esportare il contenuto</param>
        /// <param name="folderExporter">oggetto FolderExporter</param>
        /// <returns></returns>
        public static ApplicationExporter Fabricate(string FolderID, FolderExporter folderExporter)
        {
            if (Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork)
            {
                object[] param = { FolderID, folderExporter };
                StructureFolder folder = (StructureFolder)Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.FindFolderByID(int.Parse(FolderID));
                Application application = folder.RelativeApplication;
                return (ApplicationExporter)ApplicationsPool.BuildInstanceOf(application.Classes.Exporter, application.ID.ToString(), param);
            }
            else return null;
        }
    }
}
