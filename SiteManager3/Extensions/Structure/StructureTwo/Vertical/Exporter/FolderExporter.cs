﻿using System;
using System.IO;
using System.Xml;
using System.Web;
using System.Data;
using NetCms.Networks;
using NetCms.Networks.WebFS;
namespace NetCms.Exporter
{
    public  class FolderExporter
    {
        /// <summary>
        /// Oggetto Exporter corrente
        /// </summary>
        public Exporter Exporter
        {
            get
            {
                return _Exporter;
            }
            private set { _Exporter = value; }
        }
        private Exporter _Exporter;

        /// <summary>
        /// Tabella di tutte le Subfolders non eliminate compresa la Folder iniziale
        /// </summary>
        private DataTable _FoldersTable;
        public DataTable FoldersTable
        {
            get
            {
                if (_FoldersTable == null)
                {

                    _FoldersTable = new DataTable();
                    string sql = "SELECT * FROM (Folders INNER JOIN network_folders ON id_Folder = id_NetworkFolder) INNER JOIN FoldersLang ON id_Folder = Folder_FolderLang WHERE Tree_Folder LIKE '%." + G2Core.Common.Utility.FormatNumber(this.ID) + ".%' AND Trash_Folder = 0 ORDER BY Path_Folder ASC";
                    this.Exporter.Conn.FillDataTable(sql, _FoldersTable);
                    _FoldersTable.TableName = "folders";
                }
                return _FoldersTable;
            }
        }

        public ApplicationExporter ApplicationExporter
        {
            get
            {
                if (_ApplicationExporter == null)
                    _ApplicationExporter = ApplicationExporter.Fabricate(this.ID.ToString(),this);
                return _ApplicationExporter;
            }
        }
        private ApplicationExporter _ApplicationExporter;
        
        public string LocalExportPath
        {
            get
            {
                return Exporter.ExportRootPath;
            }
        }

        /// <summary>
        /// ID della folder da esportare
        /// </summary>
        public string ID
        {
            get { return _ID; }
            private set { _ID = value; }
        }
        private string _ID;
        
        /// <summary>
        /// Flag che indica se la folder è Root
        /// </summary>
        public bool IsRoot
        {
            get { return _IsRoot; }
            private set { _IsRoot = value; }
        }
        private bool _IsRoot;

        public FolderExporter(string folderID, Exporter exporter)
            :this(folderID,exporter,false)
        {
        }

        public FolderExporter(string folderID,Exporter exporter,bool isRoot)
        {
            Exporter = exporter;
            this.ID = folderID;
            this.IsRoot = true;
        }

        /// <summary>
        /// Metodo di esportazione della Folders
        /// </summary>
        /// <returns>true</returns>
        public bool Export()
        {
            //Se la Folder è root inclusione dell'elenco delle sub folders nel file xml
            if (this.IsRoot)
                this.Exporter.ExportDatabase.Tables.Add(this.FoldersTable);

            //Scansione delle subfolders
            foreach (DataRow row in this.FoldersTable.Rows)
            {
                //Creazione oggetto ApplicationExporter tramite reflection e metodo statico Fabricate per ciascuna applicazione
                //a cui appartiene la subfolder e relativa esportazione
                ApplicationExporter docsExporter = ApplicationExporter.Fabricate(row["id_Folder"].ToString(), this);
                docsExporter.Export();
            }
            
            //ApplicationExporter.Export();

            return true;
        }        
    }

}