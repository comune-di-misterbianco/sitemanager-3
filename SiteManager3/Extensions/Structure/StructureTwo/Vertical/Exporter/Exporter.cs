﻿using System;
using System.IO;
using System.Xml;
using System.Web;
using NetCms.Networks;
using NetCms.Networks.WebFS;

namespace NetCms.Exporter
{
    public class Exporter
    {
        private NetCms.Connections.Connection _Conn;
        public NetCms.Connections.Connection Conn
        {
            get
            {
                //if (_Conn == null)
                //    _Conn = NetCms.Connections.ConnectionsManager.GetNetworkConnection(this.NetworkName);
                //return _Conn;
                NetCms.Connections.ConnectionsManager ConnManager = new NetCms.Connections.ConnectionsManager();
                return ConnManager.CommonConnection;
            }
        }
        
        protected NetCms.Users.User Account
        {
            get
            {
                if (_Account == null)
                    _Account = NetCms.Users.AccountManager.CurrentAccount;
                return _Account;
            }
        }
        private NetCms.Users.User _Account;

        public System.Data.DataSet ExportDatabase
        {
            get
            {
                if (_ExportDatabase == null)
                    _ExportDatabase = new System.Data.DataSet();
                return _ExportDatabase;
            }
        }
        private System.Data.DataSet _ExportDatabase;

        public DateTime DateTime
        {
            get
            {
                if (_DateTime == System.DateTime.MinValue)
                    _DateTime = DateTime.Now;
                return _DateTime;
            }
        }
        private DateTime _DateTime;

        public string ExportName
        {
            get
            {
                if (_ExportName == null)
                {
                    string date = G2Core.Common.Utility.FormatNumber(DateTime.Year.ToString(),2) +
                                  G2Core.Common.Utility.FormatNumber(DateTime.Month.ToString(), 2) +
                                  G2Core.Common.Utility.FormatNumber(DateTime.Day.ToString(), 2) +
                                  G2Core.Common.Utility.FormatNumber(DateTime.Hour.ToString(), 2) +
                                  G2Core.Common.Utility.FormatNumber(DateTime.Minute.ToString(), 2) +
                                  G2Core.Common.Utility.FormatNumber(DateTime.Millisecond.ToString(), 3);
                    _ExportName = "SM2-ExportData-" + date;
                }
                return _ExportName;
            }
        }
        private string _ExportName;

        public string ExporterPath
        {
            get
            {
                return Configurations.Paths.AbsoluteExportRoot;
            }
        }
        private string _ExporterPath;

        public string ExportRootPath
        {
            get
            {
                if (_ExportRootPath == null)
                {
                    _ExportRootPath = ExporterPath + ExportName + "\\";
                    if (!Directory.Exists(_ExportRootPath)) Directory.CreateDirectory(_ExportRootPath);
                }
                return _ExportRootPath;
            }
        }
        private string _ExportRootPath;

        /// <summary>
        /// Array di stringhe contenente gli/l'ID delle/a folder di cui effettuare l'esportazione
        /// </summary>
        public string[] Folders2ImportIDs
        {
            get { return _Folders2ImportIDs; }
            private set { _Folders2ImportIDs = value; }
        }
        private string[]  _Folders2ImportIDs;

        public string NetworkName
        {
            get { return _NetworkName; }
            private set { _NetworkName = value; }
        }
        private string _NetworkName;

        public NetCms.Structure.WebFS.BasicStructureNetwork Network
        {
            get
            {
                if (_Network == null)
                    _Network = new NetCms.Structure.WebFS.BasicStructureNetwork(this.NetworkName);
                return _Network;
            }
        }
        private NetCms.Structure.WebFS.BasicStructureNetwork _Network;

        public string ID
        {
            get { return _ID; }
            private set { _ID = value; }
        }
        private string _ID;

        public Exporter(string networkName,string[] folders2ImportIDs)
        {
            NetworkName = networkName;
            Folders2ImportIDs = folders2ImportIDs;
        }

        /// <summary>
        /// Metodo per l'esportazione dei contenuti
        /// </summary>
        public void Export()
        {
            foreach (string id in this.Folders2ImportIDs)
            {
                FolderExporter exporter = new FolderExporter(id, this, true);
                exporter.Export();
            }
            this.ExportDatabase.WriteXmlSchema(ExportRootPath + "schema.xsd");
            this.ExportDatabase.WriteXml(ExportRootPath + "data.xml", System.Data.XmlWriteMode.IgnoreSchema);

            SaveInformationsFile();
        }
        
        /// <summary>
        /// Metodo per l'esportazione sotto forma di pacchetto compresso zip
        /// </summary>
        /// <returns>percorso pacchetto esportazione</returns>
        public string ExportPackage()
        {
            Export();

            G2Core.Common.G2Zip archiver = new G2Core.Common.G2Zip(ExporterPath + ExportName + ".sm2pkg", this.ExportRootPath);
            archiver.Save();

            Directory.Delete(ExportRootPath, true);

            return Configurations.Paths.AbsoluteRoot + "/exported-data/" + ExportName + ".sm2pkg";
        }

        /// <summary>
        /// Metodo per la creazione di un file xml contenente un insieme di informazioni per il pacchetto esportato
        /// </summary>
        private void SaveInformationsFile()
        {
            XmlDocument xmldoc = new XmlDocument();

            XmlNode xmlnode = xmldoc.CreateNode(XmlNodeType.XmlDeclaration, "", "");

            xmldoc.AppendChild(xmlnode);

            XmlElement root = xmldoc.CreateElement("", "packageinformations", "");
            xmldoc.AppendChild(root);


            XmlNode author = xmldoc.CreateNode(XmlNodeType.Element, "author", "");
            root.AppendChild(author);

            XmlAttribute att = xmldoc.CreateAttribute("id");
            att.Value = Account.ID.ToString();
            author.Attributes.Append(att);

            att = xmldoc.CreateAttribute("username");
            att.Value = Account.UserName;
            author.Attributes.Append(att); 

            att = xmldoc.CreateAttribute("name");
            att.Value =  Account.NomeCompleto;
            author.Attributes.Append(att);

            XmlNode date = xmldoc.CreateNode(XmlNodeType.Element, "date", "");
            root.AppendChild(date);

            att = xmldoc.CreateAttribute("value");
            att.Value = G2Core.Common.Utility.FormatNumber(DateTime.Day.ToString(), 2) + "/" +
                                  G2Core.Common.Utility.FormatNumber(DateTime.Month.ToString(), 2) + "/" +
                                  G2Core.Common.Utility.FormatNumber(DateTime.Year.ToString(), 2) + " " +
                                  G2Core.Common.Utility.FormatNumber(DateTime.Hour.ToString(), 2) + ":" +
                                  G2Core.Common.Utility.FormatNumber(DateTime.Minute.ToString(), 2);
            date.Attributes.Append(att);

            xmldoc.Save(ExportRootPath + "informations.xml");
        }
    }
}