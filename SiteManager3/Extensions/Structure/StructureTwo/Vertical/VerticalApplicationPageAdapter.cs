﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using G2Core;
using NetCms.Vertical.BusinessLogic;

namespace NetCms.Vertical
{
    public class VerticalApplicationPageAdapter : NetCms.GUI.SmPage
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                {
                    if (this.RelativeApplication is VerticalApplicationPackage)
                        _SideBar = new NetCms.GUI.SideBar.VappPackageSideBar(this,this.RelativeApplication.SystemName, ContainedApps);
                    else
                        _SideBar = new NetCms.GUI.SideBar.HomeSideBar(this);
                }
                    
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public override string PageTitle
        {
            get { return ApplicationPage.PageTitle; }
        }
        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return ApplicationPage.PageIcon; }
        }
        public override string LocalCssClass
        {
            get { return RelativeApplication.SystemName.ToLower(); }
        }

        public ApplicationPage ApplicationPage
        {
            get { return _ApplicationPage; }
            private set { _ApplicationPage = value; }
        }
        private ApplicationPage _ApplicationPage;

        public VerticalApplication RelativeApplication
        {
            get { return _RelativeApplication; }
            private set { _RelativeApplication = value; }
        }
        private VerticalApplication _RelativeApplication;

        public int[] ContainedApps
        {
            get { return _ContainedApps; }
            private set { _ContainedApps = value; }
        }
        private int[] _ContainedApps;

        public string ClassName
        {
            get { return _ClassName; }
            private set { _ClassName = value; }
        }
        private string _ClassName;

        public VerticalApplicationPageAdapter(Vertical.VerticalApplication application, string className, StateBag viewState, bool isPostBack)
        {
            this.AddScriptManager = true;
            NetCms.Users.AccountManager.CurrentAccount.History.UpdateHistoryData();

            this.RelativeApplication = application;
            this.ClassName = className;
            NetCms.Networks.NetworkKey key = null;
            if (NetCms.Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork)
                key = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CacheKey;       
     
            if (application is VerticalApplicationPackage)
            {
                ContainedApps = ((VerticalApplicationPackage)application).ContainedApplicationsIDs;
                object[] parameters = { viewState, isPostBack, this.Toolbar, this.InfoBox, key };
                this.ApplicationPage = (Vertical.ApplicationPage)VerticalApplicationsPool.BuildInstanceOf(className, application.ID.ToString(), parameters, ContainedApps);
                int appId = FindRelatedApp(className);

                bool InsideNetwork = key != null; 
                ((GUI.SideBar.VappPackageSideBar)this.SideBar).ShowToolbarOperations = true;
                ((GUI.SideBar.VappPackageSideBar)this.SideBar).OperationsBox.Toolbar.Buttons.Add(new ToolbarButton((InsideNetwork ? NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteAdminRoot : NetCms.Configurations.Paths.AbsoluteSiteManagerRoot) + "/applications/" + application.SystemName + application.BackofficeUrl, GUI.Icons.Icons.Application_Home, "Home"));
                
                if (((GUI.SideBar.VappPackageSideBar)this.SideBar).RelatedAppsBoxes.ContainsKey(appId))
                {
                    ((GUI.SideBar.VappPackageSideBar)this.SideBar).RelatedAppsBoxes[appId].StartCollapsed = false;
                    ((GUI.SideBar.VappPackageSideBar)this.SideBar).RelatedAppsBoxes[appId].Toolbar = this.Toolbar;
                }
                else
                {                    
                    foreach (ToolbarButton button in this.Toolbar.Buttons)
                        ((GUI.SideBar.VappPackageSideBar)this.SideBar).OperationsBox.Toolbar.Buttons.Add(button);
                }
            }
            else {
                object[] parameters = { viewState, isPostBack, this.Toolbar, this.InfoBox, key };
                this.ApplicationPage = (Vertical.ApplicationPage)VerticalApplicationsPool.BuildInstanceOf(className, application.ID.ToString(), parameters);
            }
        }


        private int FindRelatedApp(string className)
        {
            if (ContainedApps != null)
            {
                foreach (int id in ContainedApps)
                {
                    VerticalApplication application = VerticalApplicationsPool.ActiveAssemblies[id.ToString()];
                    Type typeToInvoke = application.Assembly.GetType(className);
                    if (typeToInvoke != null)
                        return application.ID;

                }
            }
            return this.RelativeApplication.ID;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (ApplicationPage.AddScriptManager && Page != null && ScriptManager.GetCurrent(Page) == null)
            {
                if (ApplicationPage.ScriptManager != null)
                    Page.Form.Controls.AddAt(0, ApplicationPage.ScriptManager);
                //Page.Form.Controls.AddAt(0, new ScriptManager());                        
            }
            
            if (this.ApplicationPage.PageLayout == Vertical.ApplicationPage.PageLayouts.Default)
            {
                this.Controls.Add(this.Header);
                if (SideBar != null)
                {
                    //if (this.RelativeApplication is VerticalApplicationPackage) {
                        ////this.SideBar.ButtonsToolbar.Buttons.Clear();

                        ////foreach (ToolbarButton button in ((VerticalApplicationPackage)this.RelativeApplication).ToolbarMaster.Buttons)
                        ////    this.SideBar.ButtonsToolbar.Buttons.Add(button);

                        ////foreach (ToolbarButton button in this.ApplicationPage.Toolbar.Buttons)
                        ////    this.SideBar.ButtonsToolbar.Buttons.Add(button);
                    //}
                    this.Controls.Add(this.SideBar);
                }
                this.Controls.Add(this.InfoToolbar);
                this.Controls.Add(this.ContentBody);
                this.Controls.Add(this.Footer);
            }
            else
            {
                this.CssClass = "lite";
                this.ContentBody.ID = "corpo_lite";
                this.Controls.Add(this.ContentBody);
            }
            base.AddScriptsToHead = this.ApplicationPage.AddScriptsToHead;
            
        }

        protected override WebControl RootControl
        {
            get
            {
                this.DisableContent = ApplicationPage.DisableContent;

                bool granted = string.IsNullOrEmpty(this.ApplicationPage.Criteria);
                if(!granted)
                {
                    int appId = FindRelatedApp(this.ClassName);
                    VerticalApplication vApp = VerticalAppBusinessLogic.GetVerticalAppById(appId);
                    //NetCms.Vertical.Grants.ExternalAppzGrantsChecker Checker = new NetCms.Vertical.Grants.ExternalAppzGrantsChecker(RelativeApplication.SystemName, this.ApplicationPage.Criteria);
                    //granted = (Checker.Grant);
                    granted = vApp.Criteria[this.ApplicationPage.Criteria].Allowed;
                }

                if (granted)
                {
                    var ctr = this.ApplicationPage.Control;

                    this.ApplicationPage.OuterScripts.ForEach(x => { this.Page.Header.Controls.Add(new LiteralControl(x)); });

                    return ctr;
                }
                else
                {
                    NetCms.GUI.PopupBox.AddSessionMessage("Non sei autorizzato a vedere la risorsa a cui hai tentato di accedere.", PostBackMessagesType.Error, Account.History.GetBackURL());
                    return null;
                }
            }
        }
        
        protected override void AddCssToHeader()
        {
            if (this.ApplicationPage.PageLayout == Vertical.ApplicationPage.PageLayouts.Default)
                base.AddCssToHeader();
            else
            {
                string cssLink = @"<link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"lite.css"" /><!--lite.csx-->"
                               + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"smtable.css"" /><!--smtable.csx-->"
                               + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"axf.css"" /><!--axf.csx-->"
                               + @" <link rel=""stylesheet"" type=""text/css"" href=""" + Configurations.Paths.BackOfficeSkinPath + @"axt.css"" /><!--axt.csx-->";
                this.Page.Header.Controls.Add(new LiteralControl(cssLink));
            }
        }      
    }
}
