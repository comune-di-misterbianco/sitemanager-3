﻿using System;
using System.Data;
using System.Reflection;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Users;
using NetCms.Front;
using NetCms.Vertical.BusinessLogic;
using NHibernate;
using NetCms.Grants.Interfaces;
using NetCms.Grants;
using NetCms.Networks.Grants;
using NetCms.Vertical.Grants;
using System.Linq;
using NetCms.Connections;

namespace NetCms.Vertical
{              
    public class VerticalApplication: IGrantsFindable
    {
        public enum AccessModes { Admin = 0, Front = 1, Both = 2 }

        #region Proprietà da mappare

        /// <summary>
        /// Definisce l'ID dell'applicazione, questo deve essere un intero e deve essere diverso dagli ID di tutte le altre applicazioni fin ora create.
        /// </summary>
        public virtual int ID { get; set; }

        /// <summary>
        /// Nome di sistema dell'applicazione, dovrebbe essere scritto in minuscolo e senza spazi
        /// </summary>
        public virtual string SystemName { get; set; }

        /// <summary>
        /// La label associata all'applicazione, è quella che vedranno gli utenti finali
        /// </summary>
        public virtual string Label { get; set; }

        /// <summary>
        /// Definisce la riga in cui verrà messa l'icona dell'applicazione nella homepage del CMS
        /// </summary>
        public virtual int HomepageGroup
        {
            get;
            set;
        }
        
        /// <summary>
        /// Non utilizzata ma mantenuta per retrocompatibilità
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Se impostata a true l'appllicazione avrà una diversa istanza per ogni network, e i permessi per l'applicazione saranno gestiti per network
        /// Se impostata a false l'applicazione avrà una sola istanza per tutte le network
        /// </summary>
        public virtual bool NetworkDependant { get; set; }

        public virtual bool Enabled
        {
            get;
            set;
        }

        //Utilizzata da FrontLayout
        public virtual int Layout
        {
            get;
            set;
        }
        //Utilizzata da FrontendPath
        public virtual string Path
        {
            get;
            set;
        }

        public virtual int AccessModeInt
        {
            get;
            set;
        }

        public virtual ICollection<ExternalAppPage> Pages
        { get; set; }

        public virtual ICollection<ExternalAppzCriteria> AppzCriteria
        { get; set; }

        public virtual ICollection<VerticalApplicationServiceUser> ServiceUsers
        { get; set; }

        #endregion


        /// <summary>
        /// Determina se l'applicazione è fruibile da frontend, backoffice o da entrambi
        /// </summary>
        public virtual AccessModes AccessMode
        {
            get
            {
                switch (AccessModeInt)
                {
                    case 0: return AccessModes.Admin;
                    case 1: return AccessModes.Front;
                    case 2: return AccessModes.Both;
                    default: return AccessModes.Admin;
                }
            }
        }

        public virtual bool HasService
        {
            get
            {
                return false;
            }
        }

        
        private const string DefaultCriteria = "enable";

        public virtual Connection Conn
        {
            get
            {
                ConnectionsManager ConnManager = new ConnectionsManager();
                return ConnManager.CommonConnection;
            }
        }

        /// <summary>
        /// Permette di specificare l'indirizzo di base per la pagina di accesso da backoffice nelle applicazioni verticali.
        /// </summary>
        public virtual string BackofficeUrl
        {
            get
            {
                return "/admin/default.aspx";
            }
        }

        //Indica se aggiungere il progetto alla session factory del CMS o utilizzare una sessione differente. In genere deve essere true.
        public virtual bool UseCmsSessionFactory
        {
            get { return false; }
        }

        //Elenco di assembly da cui dipende l'applicazione verticale. Permette di usare gli assembly di altre applicazione anche se queste non
        //sono installate, in caso di dipendenze.
        public virtual ICollection<Assembly> FluentAssembliesRelated
        {
            get
            {
                return new List<Assembly>();
            }
        }

        public virtual bool NeedAdditionalConfiguration
        {
            get { return false; }
        }

        public virtual bool DoAdditionalConfigurationForExternalDB()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Non utilizzata ma mantenuta per retrocompatibilità
        /// </summary>
        public virtual int SecurityLevel { get { return 0; } }
                               
        /// <summary>
        /// Se impostata a true gli utenti di frontend dovranno avere il permesso "enable" per poter accedere ad ogni singola pagina
        /// Se impostata a false gli utenti potranno accedere liberamente alle pagine anche se non loggati, eventuali criteri specifici per pagina saranno controllati comunque
        /// </summary>
        public virtual bool CheckForDefaultGrant { get { return false; } }

        /// <summary>
        /// Se impostato a true non sarà possibile disinstallare l'applicazione se non in debug mode
        /// </summary>
        public virtual bool IsSystemApplication { get { return Installer == null; } }

        /// <summary>
        /// Se impostato a true non sarà possibile disinstallare l'applicazione se non in debug mode
        /// </summary>
        public virtual bool AllowUninstall { get { return true; } }

        public virtual List<string> CustomCssStyles
        {
            get
            {
                if (_CustomCssStyles == null)
                    _CustomCssStyles = new List<string>();
                return _CustomCssStyles;
            }
        }
        private List<string> _CustomCssStyles;
                         
        public virtual Validation.VerticalApplicationValidator Validator
        {
            get
            {
                if (_Validator == null)
                {
                    _Validator = new Validation.VerticalApplicationValidator(this);
                }
                return _Validator;
            }
        }
        private Validation.VerticalApplicationValidator _Validator;

        public virtual VerticalApplicationFixer Fixer
        {
            get
            {
                if (_Fixer == null)
                {
                    _Fixer = new VerticalApplicationFixer(this);
                }
                return _Fixer;
            }
        }
        private VerticalApplicationFixer _Fixer;

        public virtual NetCms.GUI.Icons.Icons Icon
        {
            get
            {
                return NetCms.GUI.Icons.Icons.Application;
            }
        }

        public virtual string FrontendPath
        {
            get
            {
                if (string.IsNullOrEmpty(Path)) return "/applications/" + this.SystemName;
                return Path;
            }
        }

        public virtual FrontLayouts FrontLayout
        {
            get
            {
                switch (Layout)
                {
                    case 0: return FrontLayouts.All; 
                    case 1: return FrontLayouts.LeftRight; 
                    case 2: return FrontLayouts.LeftFooter;
                    case 3: return FrontLayouts.RightFooter;
                    case 4: return FrontLayouts.Left;
                    case 5: return FrontLayouts.Right;
                    case 6: return FrontLayouts.Footer;
                    case 7: return FrontLayouts.None;
                    case 8: return FrontLayouts.All;
                    default: return FrontLayouts.All;
                }
            }
        }
       
        public virtual string FrontendURL
        {
            get 
            {
                string currentFakeNetwork = NetCms.Networks.NetworksManager.CurrentFakeNetwork;
                if (NetCms.Networks.NetworksManager.CurrentActiveNetwork.SystemName != currentFakeNetwork)
                    return NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/" + currentFakeNetwork + this.FrontendPath;
                else
                    return NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + this.FrontendPath;
            }
        }

        /// <summary>
        /// Definisce la classe che effettua l'istallazione dell'applicazione
        /// </summary>
        public virtual VerticalApplicationSetup Installer { get { return null; } }
                    
        public virtual Assembly Assembly
        {
            get { return _Assembly; }
            set { _Assembly = value; }
        }
        private Assembly _Assembly;

        public virtual VerticalApplicationClasses Classes
        {
            get { return _Classes; }
            set { _Classes = value; }
        }
        private VerticalApplicationClasses _Classes;

        public enum InstallStates { NotLoaded, NotInstalled, Installed, Error }

        private InstallStates _InstallState = InstallStates.NotLoaded;
        public virtual InstallStates InstallState
        {
            get
            {
                if (_InstallState == InstallStates.NotLoaded)
                {
                    UpdateInstallationData();
                }
                return _InstallState;
            }
        }

        
        private GUI.Startpage.StartpageBox _StartpageBox;
        public virtual GUI.Startpage.StartpageBox StartpageBox
        {
            get
            {
                if (_StartpageBox == null)
                    _StartpageBox = new GUI.Startpage.StartpageBox(this.SystemName.ToUpper(), this.Label, Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/" + this.SystemName + "/" + Configurations.Generics.DefaultPageName);

                return _StartpageBox;
            }
        }

        public VerticalApplication(Assembly assembly)
        {
            this.Assembly = assembly;
            this.Classes = new VerticalApplicationClasses(this);
            UpdateApplicationData();
            Pages = new HashSet<ExternalAppPage>();
        }
        
        public VerticalApplication()
        {
            UpdateApplicationData();
            Pages = new HashSet<ExternalAppPage>();
            AppzCriteria = new HashSet<ExternalAppzCriteria>();
            ServiceUsers = new HashSet<VerticalApplicationServiceUser>();
        }

                                    
        //private Vertical.Grants.ExternalAppzGrants Grants
        //{
        //    get
        //    {
        //        if (_Grants == null)
        //            _Grants = new Vertical.Grants.ExternalAppzGrants(this.ID);
        //        return _Grants;
        //    }
        //}
        //private Vertical.Grants.ExternalAppzGrants _Grants;
        //public bool this[string key]
        //{
        //    get
        //    {
        //        return this.Grants[key];
        //    }
        //}

        //public bool Grant
        //{
        //    get
        //    {
        //        return this.Grants[DefaultCriteria];
        //    }
        //}

        public virtual VerticalApplicationFrontPageAdapter GetFrontAdapter(NetCms.Networks.NetworkKey networkKey, StateBag viewState, bool IsPostBack)
        {
//            DataTable PagesTable = Connection.SqlQuery(@"
//                                                                SELECT * FROM externalappz 
//                                                                INNER JOIN externalappz_pages ON (externalappz.id_ExternalApp = externalappz_pages.ExternalApp_ExternalAppPage) 
//                                                                WHERE Key_ExternalApp = '" + this.SystemName + "' AND (PageName_ExternalAppPage = '" + Parser.SubPath + "' OR PageName_ExternalAppPage = '/" + Parser.SubPath + "')"
//                                                          );
            ExternalAppPage page = VerticalAppBusinessLogic.GetExternalAppPageByPageName(this.ID, Parser.SubPath);

            //if (PagesTable.Rows.Count > 0)
            if (page != null)
            {
                string appID = page.Application.ID.ToString();//PagesTable.Rows[0]["id_ExternalApp"].ToString();

                string className = page.ClassName; //PagesTable.Rows[0]["ClassName_ExternalAppPage"].ToString();

                object[] adapterparameters = { appID, className, viewState, IsPostBack, networkKey };
                VerticalApplicationFrontPageAdapter adapter = (VerticalApplicationFrontPageAdapter)NetCms.Vertical.VerticalApplicationsPool.BuildInstanceOf(this.Classes.FrontAdapter, appID, adapterparameters);

                return adapter;
            }
            //Trace


            return null;
        }

        public virtual WebControl FrontControls_new(NetCms.Networks.NetworkKey networkKey, StateBag viewState, bool IsPostBack)
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);

            VerticalApplicationFrontPageAdapter adapter = GetFrontAdapter(networkKey, viewState, IsPostBack);
            if (adapter != null &&
                adapter.ApplicationPage.PageAvailability == ApplicationPage.PageAvailableFor.Both ||
                adapter.ApplicationPage.PageAvailability == ApplicationPage.PageAvailableFor.Front)
            {
                bool grant = string.IsNullOrEmpty(adapter.ApplicationPage.Criteria);
                if (NetCms.Users.AccountManager.Logged && !grant)
                {
                    //NetCms.Vertical.Grants.ExternalAppzGrantsChecker Checker = new NetCms.Vertical.Grants.ExternalAppzGrantsChecker(this.SystemName, adapter.ApplicationPage.Criteria);
                    grant = this.Criteria[adapter.ApplicationPage.Criteria].Allowed; //Checker.Grant;
                }
                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                if (grant)
                    value.Controls.Add(adapter);
                else
                {
                    if (NetCms.Users.AccountManager.Logged)
                        throw new Exception("Not granted");
                    else
                        value.Controls.Add(new NetCms.Users.LoginControl());
                }
            }
            else return null;
            
//            DataTable PagesTable = Connection.SqlQuery(@"
//                                                                SELECT * FROM externalappz 
//                                                                INNER JOIN externalappz_pages ON (externalappz.id_ExternalApp = externalappz_pages.ExternalApp_ExternalAppPage) 
//                                                                WHERE Key_ExternalApp = '" + this.SystemName + "' AND (PageName_ExternalAppPage = '" + Parser.SubPath + "' OR PageName_ExternalAppPage = '/" + Parser.SubPath + "')"
//                                                          );
//            if (PagesTable.Rows.Count > 0)
//            {
//                string appID = PagesTable.Rows[0]["id_ExternalApp"].ToString();

//                string className = PagesTable.Rows[0]["ClassName_ExternalAppPage"].ToString();

//                object[] adapterparameters = { appID, className, viewState, IsPostBack, networkKey };
//                VerticalApplicationFrontPageAdapter adapter = (VerticalApplicationFrontPageAdapter)NetCms.Vertical.VerticalApplicationsPool.BuildInstanceOf(this.Classes.FrontAdapter, appID, adapterparameters);

//                if (adapter.ApplicationPage.PageAvailability == ApplicationPage.PageAvailableFor.Both ||
//                    adapter.ApplicationPage.PageAvailability == ApplicationPage.PageAvailableFor.Front)
//                {
//                    bool grant = string.IsNullOrEmpty(adapter.ApplicationPage.Criteria);
//                    if (NetCms.Users.AccountManager.Logged && !grant)
//                    {
//                        NetCms.Vertical.Grants.ExternalAppzGrantsChecker Checker = new NetCms.Vertical.Grants.ExternalAppzGrantsChecker(this.SystemName, adapter.ApplicationPage.Criteria);
//                        grant = Checker.Grant;
//                    }
//                    WebControl value = new WebControl(HtmlTextWriterTag.Div);
//                    if (grant) value.Controls.Add(adapter);
//                    else if (NetCms.Users.AccountManager.Logged)
//                        throw new Exception("Not granted");
//                    else value.Controls.Add(new NetCms.Users.LoginControl());
//                    return value;
//                }
//                return null;
//            }
//            //Trace


            return control;
        }
        
        public virtual WebControl FrontControls(NetCms.Networks.NetworkKey networkKey, StateBag viewState, bool IsPostBack)
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);

//            DataTable PagesTable = Connection.SqlQuery(@"
//                                                                            SELECT * FROM externalappz 
//                                                                            INNER JOIN externalappz_pages ON (externalappz.id_ExternalApp = externalappz_pages.ExternalApp_ExternalAppPage) 
//                                                                            WHERE Key_ExternalApp = '" + this.SystemName + "' AND (PageName_ExternalAppPage = '" + Parser.SubPath + "' OR PageName_ExternalAppPage = '/" + Parser.SubPath + "')"
//                                                          );

            ExternalAppPage page = VerticalAppBusinessLogic.GetExternalAppPageByPageName(this.ID, Parser.SubPath);

            //if (PagesTable.Rows.Count > 0)
            if (page != null)
            {
                string appID = page.Application.ID.ToString();//PagesTable.Rows[0]["id_ExternalApp"].ToString();

                string className = page.ClassName;//PagesTable.Rows[0]["ClassName_ExternalAppPage"].ToString();

                object[] adapterparameters = { appID, className, viewState, IsPostBack, networkKey };
                VerticalApplicationFrontPageAdapter adapter = (VerticalApplicationFrontPageAdapter)NetCms.Vertical.VerticalApplicationsPool.BuildInstanceOf(this.Classes.FrontAdapter, appID, adapterparameters);
                ApplicationContext.SetCurrentComponent(adapter.Application.Label);

                if (adapter.ApplicationPage.PageAvailability == ApplicationPage.PageAvailableFor.Both || adapter.ApplicationPage.PageAvailability == ApplicationPage.PageAvailableFor.Front)
                {
                    //NetCms.Vertical.Grants.ExternalAppzGrants Checker = new NetCms.Vertical.Grants.ExternalAppzGrants(this.ID);
                    bool Check1, Check2;
                    if (NetCms.Users.AccountManager.Logged)
                    {
                        Check1 = !this.CheckForDefaultGrant || this.Grant;//Checker.Grant;
                        Check2 = string.IsNullOrEmpty(adapter.ApplicationPage.Criteria) || this.Criteria[adapter.ApplicationPage.Criteria].Allowed;//Checker[adapter.ApplicationPage.Criteria];
                    }
                    else
                    {
                        Check1 = !this.CheckForDefaultGrant;
                        Check2 = true;
                    }
                  
                    //WebControl value = new WebControl(HtmlTextWriterTag.Div);
                  
                    if (Check1 && Check2)
                    {
                        return adapter; //value.Controls.Add(adapter);
                    }
                    else
                    {
                        //if (NetCms.Users.AccountManager.Logged)
                            throw new NetCms.Exceptions.NotGrantedException();
                        //else
                            //value.Controls.Add(new NetCms.Users.LoginControl());
                    }

                    //return value;
                }
                return null;
            }

            return control;
        }

        public virtual VerticalApplicationsUrlParser Parser
        {
            get
            {
                if(!HttpContext.Current.Items.Contains("VerticalApplicationsUrlParser_"+this.ID))
                    HttpContext.Current.Items["VerticalApplicationsUrlParser_" + this.ID] = new VerticalApplicationsUrlParser(this, NetCms.Networks.NetworksManager.CurrentActiveNetwork);
                return (VerticalApplicationsUrlParser)HttpContext.Current.Items["VerticalApplicationsUrlParser_" + this.ID];
            }
        }

        public virtual bool UpdateInstallationData()
        {
            bool result = false;
            try
            {
                if (this.ID > 0)
                {
                    DataTable applications = Conn.SqlQuery("SELECT * FROM externalappz WHERE id_ExternalApp = " + this.ID);
                    switch (applications.Rows.Count)
                    {
                        case 0: _InstallState = InstallStates.NotInstalled; break;
                        case 1: _InstallState = InstallStates.Installed; break;
                        default: _InstallState = InstallStates.Error; break;
                    }
                    result = true;
                }
            }
            catch
            {
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile effettuare il refresh dello stato di installazione della applicazione.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }

            return result;
        }
        public virtual bool UpdateApplicationData()
        {
            bool result = false;
            try
            {
                //VerticalApplication app = VerticalAppBusinessLogic.GetVerticalAppById(this.ID);
                //if (app != null)
                //{
                //    this.Enabled = app.Enabled;
                //    this.Path = app.Path;
                //    this.Layout = app.Layout;
                //    result = true;
                //}
                if (this.ID > 0)
                {
                    DataTable tab = Conn.SqlQuery("SELECT * FROM externalappz WHERE id_ExternalApp = " + this.ID);
                    if (tab.Rows.Count == 1)
                    {
                        this.Enabled = tab.Rows[0]["Enabled_ExternalApp"].ToString() == "1";
                        this.Path = tab.Rows[0]["Path_ExternalApp"].ToString();
                        this.Layout = int.Parse(tab.Rows[0]["FrontLayout_ExternalApp"].ToString());
                        result = true;
                    }
                }
            }
            catch
            {
                //NetCms.Users.AccountManager.CurrentAccount.ID.ToString()
                //NetCms.Users.AccountManager.CurrentAccount.UserName
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non è stato possibile effettuare il refresh della applicazione.", "", "", "");

            }

            return result;
        }

        public virtual NetCms.Users.User CurrentUser { get { return NetCms.Users.AccountManager.CurrentAccount; } }

        public virtual bool Disable()
        {
            bool ok = false;
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    VerticalAppBusinessLogic.DisableVerticalApp(this.ID, sess);
                    tx.Commit();
                    ok = true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    ok = false;
                }  
            }
            if (ok)
            {
                VerticalApplicationsPool.ActiveAssemblies.Remove(this.ID.ToString());
                VerticalApplicationsPool.NonActiveAssemblies.Add(this);
                G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
                cache.RemoveAll("");
                this.Enabled = false;
            }
            return ok;
        }
        public virtual bool Enable()
        {
            bool ok = false;
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    VerticalAppBusinessLogic.EnableVerticalApp(this.ID, sess);
                    tx.Commit();
                    ok = true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    ok = false;
                }  
            }
            if (ok)
            {
                VerticalApplicationsPool.NonActiveAssemblies.Remove(this.ID.ToString());
                VerticalApplicationsPool.ActiveAssemblies.Add(this);
                G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
                cache.RemoveAll("");
                this.Enabled = true;
            }
            return ok;
        }
        public virtual bool SwitchState()
        {
            if (this.Enabled) return Disable();
            else return this.Enable();
        }

        public static Dictionary<string,NetCms.Vertical.Configuration.Configuration> Configs
        {
            get
            {
                if (_ApplicationsConfigurations == null)
                {
                    _ApplicationsConfigurations = new Dictionary<string, NetCms.Vertical.Configuration.Configuration>();
                }
                return _ApplicationsConfigurations;
            }
        }
        private static Dictionary<string, NetCms.Vertical.Configuration.Configuration> _ApplicationsConfigurations;


        #region  Metodi dell'interfaccia IGrantsFindable
        
        public virtual GrantObjectType GrantObjectType
        {
            get { return GrantObjectType.VerticalApplication; }
        }
        //Il metodo cerca sulla network corrente se network dependant
        public virtual GrantsFinder Criteria
        {
            get
            {
                //if (_Criteria == null) {
                    NetCms.Networks.NetworkKey NetworkKey = null;
                    if (this.NetworkDependant)
                    {
                        if (NetCms.Networks.NetworksManager.CurrentFace == Faces.Admin)
                        {
                            if (NetCms.Networks.NetworksManager.CurrentActiveNetwork != null)
                                NetworkKey = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CacheKey;
                        }
                        else
                        {
                            int id = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks[NetCms.Networks.NetworksManager.CurrentFakeNetwork].ID;
                            NetworkKey = new NetCms.Networks.NetworkKey(id, G2Core.Caching.Visibility.CachingVisibility.User);
                        }
                    }
                    _Criteria = new GrantElement(this, NetCms.Users.AccountManager.CurrentAccount.Profile,NetworkKey);
                //}
                return _Criteria;
            }
        }
        private GrantsFinder _Criteria;

        public virtual void RefreshCriteria()
        {
            _Criteria = null;
        }

        public virtual bool Grant
        {
            get { return this.Criteria[DefaultCriteria].Allowed; }
        }

        public virtual bool NeedToShow
        {
            get { throw new NotImplementedException(); }
        }
        //Il metodo cerca sulla network corrente se network dependant
        public virtual GrantsFinder CriteriaByProfile(Profile profile)
        {
            NetCms.Networks.NetworkKey NetworkKey = null;
            if (this.NetworkDependant)
            {
                if (NetCms.Networks.NetworksManager.CurrentFace == Faces.Admin)
                {
                    if (NetCms.Networks.NetworksManager.CurrentActiveNetwork != null)
                        NetworkKey = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CacheKey;
                }
                else
                {
                    int id = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks[NetCms.Networks.NetworksManager.CurrentFakeNetwork].ID;
                    NetworkKey = new NetCms.Networks.NetworkKey(id, G2Core.Caching.Visibility.CachingVisibility.User);
                }
            }
            return new GrantElement(this, profile, NetworkKey);
        }

        public virtual GrantsFinder CriteriaByProfileAndNetwork(Profile profile, Networks.NetworkKey networkKey)
        {
            return new GrantElement(this, profile, networkKey);
        }

        public virtual GrantValue CriteriaForApplication(CriteriaTypes criteria, string appId)
        {
            throw new NotImplementedException();
        }

        public virtual ICollection<ObjectProfile> GrantObjectProfiles
        {
            get { return ExternalAppzGrantsBusinessLogic.GetExternalAppProfiles(this.ID).ToList<ObjectProfile>(); }
        }

        public virtual void DeleteGrantsForAssociation(int associationID)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    ExternalAppProfile vappProfile = ExternalAppzGrantsBusinessLogic.GetExternalAppProfileById(associationID, sess);
                    vappProfile.CriteriaCollection.Clear();
                    ExternalAppzGrantsBusinessLogic.SaveOrUpdateExternalAppProfile(vappProfile, sess);

                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
            }
        }

        public virtual bool AddGrant(int associationID, int grantValue, int idCriterio, int creatorUserID)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                User creator = UsersBusinessLogic.GetById(creatorUserID, sess);
                try
                {
                    ExternalAppProfile vappProfile = ExternalAppzGrantsBusinessLogic.GetExternalAppProfileById(associationID, sess);

                    ExternalAppGrant vappCriteria = new ExternalAppGrant();
                    vappCriteria.ObjectProfile = vappProfile;
                    vappCriteria.Criteria = ExternalAppzGrantsBusinessLogic.GetCriterioById(idCriterio,sess);
                    vappCriteria.Value = grantValue;
                    vappCriteria.Updater = creator;
                    vappProfile.CriteriaCollection.Add(vappCriteria);

                    ExternalAppzGrantsBusinessLogic.SaveOrUpdateExternalAppProfile(vappProfile, sess);

                    tx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    return false;
                }
            }
        }

        public virtual void DeleteGrantsForAssociation(int associationID, ISession session)
        {
            ExternalAppProfile vappProfile = ExternalAppzGrantsBusinessLogic.GetExternalAppProfileById(associationID, session);
            vappProfile.CriteriaCollection.Clear();
            ExternalAppzGrantsBusinessLogic.SaveOrUpdateExternalAppProfile(vappProfile, session);
        }

        public virtual void AddGrant(int associationID, int grantValue, int idCriterio, int creatorUserID, ISession session)
        {
            User creator = UsersBusinessLogic.GetById(creatorUserID, session);
            ExternalAppProfile vappProfile = ExternalAppzGrantsBusinessLogic.GetExternalAppProfileById(associationID, session);

            ExternalAppGrant vappCriteria = new ExternalAppGrant();
            vappCriteria.ObjectProfile = vappProfile;
            vappCriteria.Criteria = ExternalAppzGrantsBusinessLogic.GetCriterioById(idCriterio, session);
            vappCriteria.Value = grantValue;
            vappCriteria.Updater = creator;
            vappProfile.CriteriaCollection.Add(vappCriteria);

            ExternalAppzGrantsBusinessLogic.SaveOrUpdateExternalAppProfile(vappProfile, session);
        }

        public virtual bool CheckIfProfileIsAssociated(int profileId, ISession session)
        {
            return ExternalAppzGrantsBusinessLogic.CheckIfProfileIsAssociatedToVerticalApp(this.ID, profileId, session);
        }



        //public virtual ExternalAppProfile AddGrantProfile(int profileId, int networkId, ISession session)
        //{

        //    User creator = UsersBusinessLogic.GetById(NetCms.Users.AccountManager.CurrentAccount.ID, session);

        //    ExternalAppProfile vappProfile = new ExternalAppProfile();
        //    vappProfile.Profile = ProfileBusinessLogic.GetById(profileId, session);
        //    vappProfile.WfsObject = VerticalAppBusinessLogic.GetVerticalAppById(this.ID, session);
        //    vappProfile.Network = networkId;
        //    vappProfile.Creator = creator;

        //    return ExternalAppzGrantsBusinessLogic.SaveOrUpdateExternalAppProfile(vappProfile, session);

        //}

        public virtual bool DeleteGrantProfile(int objectProfileId)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    ExternalAppzGrantsBusinessLogic.DeleteExternalAppProfile(objectProfileId, sess);
                    tx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    return false;
                }
            }
        }

        public virtual bool AddGrantProfile(int profileId)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                User creator = UsersBusinessLogic.GetById(NetCms.Users.AccountManager.CurrentAccount.ID, sess);
                try
                {
                    ExternalAppProfile vappProfile = new ExternalAppProfile();
                    vappProfile.Profile = ProfileBusinessLogic.GetById(profileId, sess);
                    vappProfile.WfsObject = VerticalAppBusinessLogic.GetVerticalAppById(this.ID, sess);
                    vappProfile.Creator = creator;

                    ExternalAppzGrantsBusinessLogic.SaveOrUpdateExternalAppProfile(vappProfile, sess);

                    tx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    return false;
                }
            }
        }

        public virtual bool AddGrantProfile(int profileId, int networkId)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                User creator = UsersBusinessLogic.GetById(NetCms.Users.AccountManager.CurrentAccount.ID, sess);
                try
                {
                    ExternalAppProfile vappProfile = new ExternalAppProfile();
                    vappProfile.Profile = ProfileBusinessLogic.GetById(profileId, sess);
                    vappProfile.WfsObject = VerticalAppBusinessLogic.GetVerticalAppById(this.ID, sess);
                    vappProfile.Network = networkId;
                    vappProfile.Creator = creator;

                    ExternalAppzGrantsBusinessLogic.SaveOrUpdateExternalAppProfile(vappProfile, sess);

                    tx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    return false;
                }
            }
        }

        public virtual ObjectProfile AddGrantProfile(int profileId, int networkId, ISession session)
        {
            User creator = UsersBusinessLogic.GetById(NetCms.Users.AccountManager.CurrentAccount.ID, session);

            ExternalAppProfile vappProfile = new ExternalAppProfile();
            vappProfile.Profile = ProfileBusinessLogic.GetById(profileId, session);
            vappProfile.WfsObject = VerticalAppBusinessLogic.GetVerticalAppById(this.ID, session);
            vappProfile.Network = networkId;
            vappProfile.Creator = creator;

            return ExternalAppzGrantsBusinessLogic.SaveOrUpdateExternalAppProfile(vappProfile, session);
        }

        public virtual ObjectProfile AddGrantProfile(int profileId, ISession session)
        {
            User creator = UsersBusinessLogic.GetById(NetCms.Users.AccountManager.CurrentAccount.ID, session);
            
            ExternalAppProfile vappProfile = new ExternalAppProfile();
            vappProfile.Profile = ProfileBusinessLogic.GetById(profileId, session);
            vappProfile.WfsObject = VerticalAppBusinessLogic.GetVerticalAppById(this.ID, session);
            vappProfile.Creator = creator;

            return ExternalAppzGrantsBusinessLogic.SaveOrUpdateExternalAppProfile(vappProfile, session);
        }

        public virtual bool DeleteGrantProfile(int objectProfileId, ISession session)
        {
            try
            {
                ExternalAppzGrantsBusinessLogic.DeleteExternalAppProfile(objectProfileId, session);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public virtual bool CheckIfProfileIsAssociated(int profileId, int networkId = -1, ISession session = null)
        {
            if (networkId != -1)
                return ExternalAppzGrantsBusinessLogic.CheckIfProfileIsAssociatedToVerticalApp(this.ID, profileId, networkId, session);
            return ExternalAppzGrantsBusinessLogic.CheckIfProfileIsAssociatedToVerticalApp(this.ID, profileId, session);
        }

        

        #endregion

        private HtmlGenericControl _ErrorControl;
        public virtual HtmlGenericControl ErrorControl
        {
            get
            {
                if (_ErrorControl == null)
                {
                    _ErrorControl = new HtmlGenericControl("div");
                    HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset("Errore!");
                    HtmlGenericControls.Div div = new HtmlGenericControls.Div();
                    div.Class = "ErrorAlert";
                    div.InnerHtml = "Non possiedi i permessi per accedere a questa risorsa.";
                    set.Controls.Add(div);

                    _ErrorControl.Controls.Add(set);
                }
                return _ErrorControl;
            }
        }

        #region Proprietà nuova elaborazione Frontend

        /// <summary>
        /// Questa proprietà indica se l'applicazione verticale fa uso di un modello di pagina personalizzato, nel quale è possibile modificare la 
        /// grafica e la struttura in maniera indipendente e differente dalle altre applicazioni verticali e dal cms
        /// </summary>
        public virtual bool CustomPageModel
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Questa proprietà specifica l'indirizzo in cui si trova la pagina personalizzata da usare come modello per tutte le pagine dell'applicazione
        /// verticale. Di default è la pagina "/models/vapp.aspx", ma può essere sostituita con un'altra pagina ad esempio: ""/models/customVappPages/assegnifamiglia/vapp.aspx""
        /// </summary>
        public virtual string CustomPageLocation
        {
            get
            {
                return "/models/vapp.aspx";
            }
        }


        #endregion
        
    }
    public class VerticalApplicationsUrlParser
    {
        public string SubPath
        {
            get { return _SubPath; }
            private set { _SubPath = value; }
        }
        private string _SubPath;

        private bool _IsCurrentActiveApplication;
        public bool IsCurrentActiveApplication
        {
            get
            {
                return _IsCurrentActiveApplication;
            }
        }

        public NetCms.Networks.INetwork CurrentNetwork
        {
            get { return _CurrentNetwork; }
            private set { _CurrentNetwork = value; }
        }
        private NetCms.Networks.INetwork _CurrentNetwork;

        public VerticalApplication Application
        {
            get { return _Application; }
            private set { _Application = value; }
        }
        private VerticalApplication _Application;

        public VerticalApplicationsUrlParser(VerticalApplication application, NetCms.Networks.INetwork currentNetwork)
        {
            CurrentNetwork = currentNetwork;
            Application = application;

            Parse();
        }

        private void Parse()
        {
            Uri currentUri = HttpContext.Current.Request.Url;
            string currentURL = currentUri.AbsolutePath;

            //string appPath = this.CurrentNetwork.Paths.AbsoluteFrontRoot + this.Application.FrontendPath + "/";

            string regexAbsoluteFrontRoot = this.CurrentNetwork.Paths.AbsoluteFrontRoot.Replace("/", @"\/");
            string regexFrontendPath = this.Application.FrontendPath.Replace("/", @"\/");

            if (regexFrontendPath.StartsWith(@"\/"))
                regexFrontendPath = regexFrontendPath.Substring(2, regexFrontendPath.Length - 2);

            Regex regexFakeNetwork = new Regex(regexAbsoluteFrontRoot + @"\/[a-zA-Z0-9]*?\/" + regexFrontendPath + @"\/", RegexOptions.IgnoreCase);
            Regex regexCurrentNetwork = new Regex(regexAbsoluteFrontRoot + @"\/" + regexFrontendPath + @"\/", RegexOptions.IgnoreCase);

            if (this.Application.AccessMode != VerticalApplication.AccessModes.Admin && (regexFakeNetwork.IsMatch(currentURL) || regexCurrentNetwork.IsMatch(currentURL)))
            {
                string[] split = { this.Application.FrontendPath+"/" };
                string appInternalPath = currentURL.Split(split, StringSplitOptions.None)[1];
                _IsCurrentActiveApplication = true;
                _SubPath = appInternalPath;

                string p = NetCms.Networks.NetworksManager.CurrentFakeNetwork;
            }
            else
            {
                _SubPath = "";
                _IsCurrentActiveApplication = false;
            }
        }

    }
}
