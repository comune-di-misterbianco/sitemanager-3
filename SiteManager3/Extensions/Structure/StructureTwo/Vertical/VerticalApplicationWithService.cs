﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using NetCms.Users;

namespace NetCms.Vertical
{
    public abstract class VerticalApplicationWithService : VerticalApplication
    {
        public VerticalApplicationWithService() : base()
        {

        }

        public VerticalApplicationWithService(Assembly assembly)
            : base(assembly)
        {

        }

        public abstract Servizio Servizio
        {
            get;
        }

        public override bool HasService
        {
            get
            {
                return true;
            }
        }

        public string ActivationPage 
        {
            get
            {
                return NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/activate_service.aspx?vapp=" + this.ID;
            }
            
        }
    }
}
