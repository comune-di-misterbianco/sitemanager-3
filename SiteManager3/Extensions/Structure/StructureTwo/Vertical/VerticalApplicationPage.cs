﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using System.Collections.Generic;

namespace NetCms.Vertical
{
    public abstract class ApplicationPageEx : ApplicationPage
    {
        private NetCms.GUI.Info.InfoBox _InformationBox;
        public override NetCms.GUI.Info.InfoBox InformationBox
        {
            get { return _InformationBox; }
        }

        private Toolbar _Toolbar;
        public override Toolbar Toolbar
        {
            get { return _Toolbar; }
        }

        public StateBag ViewState
        {
            get { return _ViewState; }
            private set { _ViewState = value; }
        }
        private StateBag _ViewState;


        public virtual NetCms.Networks.NetworkKey NetworkKey
        {
            get
            {
                return _NetworkKey;
            }
            private set { _NetworkKey = value; }
        }
        private NetCms.Networks.NetworkKey _NetworkKey;

        public bool IsPostBack
        {
            get { return _IsPostBack; }
            private set { _IsPostBack = value; }
        }
        private bool _IsPostBack;
        
        public ApplicationPageEx(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
        {
            NetworkKey = networkKey;
            _Toolbar = toolbar;
            _InformationBox = informationBox;
            this._ViewState = viewState;
            this.IsPostBack = isPostBack;
        }
    }
}
