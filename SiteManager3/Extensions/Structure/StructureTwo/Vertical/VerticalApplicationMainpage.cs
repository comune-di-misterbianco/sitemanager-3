﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;

namespace NetCms.Vertical
{
    public abstract class VerticalApplicationMainPage : ApplicationPageEx
    {
        public virtual bool EnableGrants
        {
            get
            {
                return false;
            }
        }

        public VerticalApplicationMainPage(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey){}
    }
}