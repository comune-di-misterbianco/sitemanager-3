﻿using NetCms.Grants;
using NetCms.Vertical.BusinessLogic;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Vertical
{
    /// <summary>
    /// Routines di installazione di applicativi di tipo package
    /// </summary>
    public abstract class VerticalApplicationPackageSetup : VerticalApplicationSetup
    {
        

        public VerticalApplicationPackageSetup(VerticalApplicationPackage baseApp)
            : base(baseApp)
        {
        }

        


        public new bool Install()
        {
            bool ok = true;
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    VerticalApplication app = this.AddApplication(sess);
                    if (app != null)
                    {
                        //Aggiungo in automatico tutte le applicazioni collegate
                        foreach (int ChildAppID in (this.ApplicationBaseData as VerticalApplicationPackage).ContainedApplicationsIDs)
                        {
                            Vertical.VerticalApplication appToInstall = null;
                            if (Vertical.VerticalApplicationsPool.ActiveAssemblies.Contains(ChildAppID.ToString()))
                                appToInstall = Vertical.VerticalApplicationsPool.ActiveAssemblies[ChildAppID.ToString()];
                            if (appToInstall == null)
                                appToInstall = Vertical.VerticalApplicationsPool.NonActiveAssemblies[ChildAppID.ToString()];

                            if (appToInstall != null && appToInstall.InstallState != VerticalApplication.InstallStates.Installed)
                            {
                                appToInstall.Installer.Install(true);
                                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Info, "Installata applicazione: " + appToInstall.Label, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                            }
                            if (appToInstall != null && !appToInstall.Enabled)
                                appToInstall.Enable();
                            
                        }

                        if (ok) ok = AddMainPage(app, sess);
                        if (ok) ok = this.AddPages(app, sess);
                        if (ok) ok = this.AddCriteria(app, sess);
                    }
                    else
                        throw new Exception();
                    tx.Commit();

                }
                catch (Exception ex)
                {
                    AddErrorString(ex.Message);
                    tx.Rollback();
                    ok = false;
                    return ok;
                }
            }

            if (ok) ok = this.LocalInstall();
            if (ok) ok = this.InstallGrants();
            if (ok) ok = this.ApplicationBaseData.UpdateApplicationData();
            if (ok) ok = this.ApplicationBaseData.UpdateInstallationData();

            if (!ok) Uninstall();


            return ok;
        }

        

        public new bool AddPages(VerticalApplication vapp, ISession session)
        {
            bool done = true;

            if (Pages != null)
                for (int i = 0; i < this.Pages.Length / 2; i++)
                    done = this.AddPage(this.Pages[i, 0], this.Pages[i, 1], vapp, session);


            ///Funzionamento regolare
            ///
            var types = this.ApplicationBaseData.Assembly.GetTypes();
            foreach (System.Type t in types)
            {
                System.Attribute[] attrs = System.Attribute.GetCustomAttributes(t);  // reflection

                foreach (System.Attribute attr in attrs)
                {
                    if (attr is DynamicUrl.PageControl)
                    {
                        DynamicUrl.PageControl pageAddress = (DynamicUrl.PageControl)attr;
                        if (!pageAddress.OnlyForDebugPurpose && (this.ApplicationBaseData.ID == pageAddress.ApplicationID || pageAddress.ApplicationID == 0))
                            done = done & this.AddPage(pageAddress.Address, t.FullName, vapp, session);
                    }
                }

            }

            ///Assorbimento pagine delle applicazioni contenute. La procedura memorizza le pagine degli applicativi contenuti come proprie pagine
            ///effettuando però un override del dynamic url aggiungendo come prefix il system name dell'applicativo in questione.
            ///
            foreach(int ChildAppID in (this.ApplicationBaseData as VerticalApplicationPackage).ContainedApplicationsIDs)
            {
                var app = VerticalApplicationsPool.GetByID(ChildAppID);

                types = app.Assembly.GetTypes();
                foreach (System.Type t in types)
                {
                    System.Attribute[] attrs = System.Attribute.GetCustomAttributes(t);  

                    foreach (System.Attribute attr in attrs)
                    {
                        if (attr is DynamicUrl.PageControl)
                        {
                            DynamicUrl.PageControl pageAddress = (DynamicUrl.PageControl)attr;

                            
                            if (!pageAddress.OnlyForDebugPurpose && (app.ID == pageAddress.ApplicationID || pageAddress.ApplicationID == 0))
                                done = done & this.AddPage("/" + app.SystemName + pageAddress.Address, t.FullName, vapp, session);
                        }
                    }
                }
            }

            return done;
        }


        private bool AddCriteria(VerticalApplication vapp, ISession session)
        {
            bool done = false;

            done = this.AddCriterion("Consenti Utilizzo", DefaultCriteriaName, vapp, session);
            done = this.AddCriterion("Permetti gestione dei permessi", GrantsCriteriaName, vapp, session);

            ///Criteri di base dell'applicazione
            if (Criteria != null)
                for (int i = 0; i < this.Criteria.Length / 2; i++)
                    done = this.AddCriterion(this.Criteria[i, 0], this.Criteria[i, 1], vapp, session);



            ///Assorbimento criteri applicativi contenuti - NON SERVE / NON FUNZIONA
            //foreach (int ChildAppID in (this.ApplicationBaseData as VerticalApplicationPackage).ContainedApplicationsIDs)
            //{
            //    var app = VerticalApplicationsPool.GetByID(ChildAppID);

            //    if(app.Installer.Criteria != null)
            //    {
            //        for (int i = 0; i < app.Installer.Criteria.Length / 2; i++)
            //            done = this.AddCriterion(app.Description + " - " + app.Installer.Criteria[i, 0], app.Installer.Criteria[i, 1], vapp, session);
            //    }
            //}

            return done;
        }

    }
}
