﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.GUI;
using G2Core;
using NetCms.Users;
using System.Linq;
using System.Web.UI.HtmlControls;

namespace NetCms.Vertical
{
    public abstract class VerticalApplicationFrontPageAdapter : WebControl
    {

        #region AreaContentControls
        public abstract WebControl[] PreContentControls { get; }
        public abstract WebControl[] PostContentControls { get; }
        public abstract WebControl[] ContentControls { get; }


        /// <summary>
        /// Notify Area 
        /// </summary>
        //public abstract WebControl[] NotifyContentControls { get; }
        /// <summary>
        /// Page Area
        /// </summary>
        //public abstract WebControl[] PageContentControls { get; }
        /// <summary>
        /// Side Menu Area
        /// </summary>
        //public abstract WebControl[] SideMenuContentControls{ get; }


        /// <summary>
        /// Proprietà da implementare nella classe derivata per ottenere il Box Informazioni NetCms.GUI.Info.InfoBox della pagina.
        /// </summary>
        public abstract NetCms.GUI.Info.InfoBox InfoBox { get; }

        /// <summary>
        /// Proprietà da implementare nella classe derivata per ottenere la Toolbar NetCms.GUI.Toolbar della pagina.
        /// Per evitare di utilizzarla e necessario restituire NULL
        /// </summary>
        public abstract Toolbar Toolbar { get; }

        #endregion

        #region Property

        public ApplicationPage ApplicationPage
        {
            get { return _ApplicationPage; }
            private set { _ApplicationPage = value; }
        }
        private ApplicationPage _ApplicationPage;
               
        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID
        {
            get { return _ApplicationID; }
            private set { _ApplicationID = value; }
        }
        private string _ApplicationID;

        public string ClassName
        {
            get { return _ClassName; }
            private set { _ClassName = value; }
        }
        private string _ClassName;

        protected NetCms.Networks.NetworkKey NetworkKey
        {
            get { return _NetworkKey; }
        }
        private NetCms.Networks.NetworkKey _NetworkKey;

        public StateBag InherithedViewState
        {
            get { return _InherithedViewState; }
            private set { _InherithedViewState = value; }
        }
        private StateBag _InherithedViewState;
                      
        /// <summary>
        /// Application
        /// </summary>
        public VerticalApplication Application
        {
            get
            {
                if (_Application == null)
                    _Application = VerticalApplicationsPool.ActiveAssemblies[this.ApplicationID];
                return _Application;
            }
        }
        private VerticalApplication _Application;
                      
        /// <summary>
        /// Enum Menu Toolbar Position
        /// </summary>
        /// <remarks>Default Position is Left</remarks>
        public enum PositionMenu
        {
            Top,
            /// <summary>
            /// Default
            /// </summary>
            Left,
            Right
        }
              
        /// <summary>
        /// Toolbar Position 
        /// </summary>
        /// <remarks> Default Position is Left</remarks>
        public PositionMenu MenuPosition
        {
            get { return _MenuPosition;  }
            set { _MenuPosition = value; }
        }
        private PositionMenu _MenuPosition = PositionMenu.Left;

        #endregion
    
        public VerticalApplicationFrontPageAdapter(string applicationID, string className, StateBag viewState, bool IsPostBack, NetCms.Networks.NetworkKey networkKey)
            : base(HtmlTextWriterTag.Div)
        {
            ApplicationID = applicationID;
            ClassName = className;
            _NetworkKey = networkKey;
            this.InherithedViewState = viewState;
            object[] parameters = { this.InherithedViewState, IsPostBack, this.Toolbar, this.InfoBox, networkKey };
            ApplicationPage = (Vertical.ApplicationPage)VerticalApplicationsPool.BuildInstanceOf(ClassName, ApplicationID, parameters);
        }

        protected override void OnInit(EventArgs e)
        {
            //Se l'applicazione è dotata di servizio e quindi richiede l'attivazione dello stesso, verifico se l'utente è loggato;
            //Dopodichè se la custom anagrafe dell'utente corrente contiene l'applicazione in esame e il servizio non richiede una attivazione differita
            //e l'utente non è attivo, faccio un redirect alla pagina di attivazione
            bool showPage = true;
            if (Application.HasService)
            {
                if (NetCms.Users.AccountManager.Logged)
                {
                    VerticalApplicationWithService vapp = (VerticalApplicationWithService)Application;
                    User user = NetCms.Users.AccountManager.CurrentAccount;
                    if (vapp != null && user != null && user.CustomAnagrafe != null && user.CustomAnagrafe.Applicazioni.Count(x => x.IdApplicazione == vapp.ID) > 0)
                    {
                        if (!vapp.Servizio.AttivazioneDifferita && !vapp.Servizio.UtenteAttivo(user) && !user.IsAdministrator)
                            NetCms.Diagnostics.Diagnostics.Redirect(vapp.ActivationPage);
                    }
                    else
                        showPage = false;
                }
                
            }

            //NetCms.Vertical.Grants.ExternalAppzGrantsChecker Checker = new NetCms.Vertical.Grants.ExternalAppzGrantsChecker(Application.SystemName);
            //if (Checker.Grant)

            if(!ApplicationPage.DisableContent)
            {
                if (ApplicationPage.AddScriptManager && Page != null && ScriptManager.GetCurrent(Page) == null)
                {   

                   // if (ApplicationPage.ScriptManager != null)
                        Page.Form.Controls.AddAt(0, ApplicationPage.ScriptManager);
                        //Page.Form.Controls.AddAt(0, new ScriptManager());                        
                }

                
                this.ID = "VApp";
                this.CssClass = "row";
                base.OnInit(e);
                BuildControls();

                WebControl SideContent = new WebControl(HtmlTextWriterTag.Div);
                if (NetCms.Configurations.ThemeEngine.Status == Configurations.ThemeEngine.ThemeEngineStatus.off)
                    SideContent.CssClass = "col-md-3 hidden-xs hidden-sm";
                else
                    SideContent.CssClass = "col-md-3 d-none d-md-block";

                SideContent.ID = "SideContentVapp";
                                                                        
                WebControl MainContent = new WebControl(HtmlTextWriterTag.Div);
                MainContent.CssClass = "col-md-9";
                MainContent.ID = "MainContentVapp";
                
                //Aggiunto questo codice per correggere un comportamento anomalo nell'assegnazione delle classi nel caso in cui l'utente non è ancora loggato. Senza questo, stampa ugualmente la colonna del menù in quanto non verifica l'effettiva esistenza del menù.
                //Nel FrontAdapter di ogni vertical app va inserito un codice che controlla e appende il menù solo se ci sono effettivamente item, altrimenti no. Vedi RefertiOnLine.
                if(this.PostContentControls == null || (this.PostContentControls != null && !(PostContentControls.Count() > 0)))
                {  
                    SideContent.CssClass = "";
                    MainContent.CssClass = "col-md-12";
                }


                if (this.ContentControls != null)
                    foreach (WebControl ctr in this.ContentControls)
                        if (ctr != null)
                            MainContent.Controls.Add(ctr);

                if (showPage)
                {
                    if (this.PostContentControls != null)
                    {
                        if (MenuPosition == PositionMenu.Left)
                        {
                            WebControl sideButton = new WebControl(HtmlTextWriterTag.Button);
                            sideButton.ID = "ToolbarSxButton";
                            sideButton.Attributes.Add("type", "button");
                            sideButton.CssClass = "d-block d-md-none btn btn-default sxToolbarButton";
                            sideButton.Controls.Add(new LiteralControl("<i class=\"fa fa-bars\"></i><span>" + Application.Label + "</span>"));

                            this.Controls.Add(sideButton);
                        }

                        foreach (WebControl ctr in this.PostContentControls)
                            if (ctr != null)
                                SideContent.Controls.Add(ctr);
                    }
                }

                switch (MenuPosition)
                {
                    case PositionMenu.Top:
                        this.Controls.Add(SideContent);
                        this.Controls.Add(MainContent);
                        break;
                    case PositionMenu.Left:
                        this.Controls.Add(SideContent);
                        this.Controls.Add(MainContent); 
                        break;
                    case PositionMenu.Right:
                        this.Controls.Add(MainContent);
                        this.Controls.Add(SideContent);
                        break;
                    default:
                        this.Controls.Add(SideContent);
                        this.Controls.Add(MainContent);
                        break;
                }


                #region OldCode

                /*foreach (WebControl ctr in this.PreContentControls)
                    this.Controls.Add(ctr);*/

                //if (this.ContentControls != null)
                //    foreach (WebControl ctr in this.ContentControls)
                //        if (ctr != null)
                //            this.Controls.Add(ctr);
                //if (showPage)
                //{
                //    if (this.PostContentControls != null)
                //        foreach (WebControl ctr in this.PostContentControls)
                //            if (ctr != null)
                //                this.Controls.Add(ctr);
                //}

                #endregion


                this.ApplicationPage.OuterScripts.ForEach(x => { this.Page.Header.Controls.Add(new LiteralControl(x)); });

                if (this.ApplicationPage.AddPingSessionControl)
                {
                    PingSession.PingSession pingS = new PingSession.PingSession();
                    this.Page.Header.Controls.Add(pingS.getLiteralControl());
                }
            }
            //else throw new Exception("Not Granted");
        }

     
        protected abstract void BuildControls();
         
        public string CssFolder(int networkID)
        {
            
            CmsConfigs.Model.Config currentConfig = CmsConfigs.Model.GlobalConfig.GetConfigByNetworkID(NetCms.Networks.NetworksManager.CurrentActiveNetwork.ID);
            if (NetCms.Configurations.ThemeEngine.Status == Configurations.ThemeEngine.ThemeEngineStatus.on && currentConfig != null)                
                // TODO: da testare
                return currentConfig.CurrentTheme.CssFolder.ToLower();
            else
                return NetCms.Networks.NetworksManager.CurrentActiveNetwork.CssFolder;
        }

    }
}
