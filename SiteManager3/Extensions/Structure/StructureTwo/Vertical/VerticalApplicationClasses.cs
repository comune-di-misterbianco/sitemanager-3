﻿using System;
using System.Data;
using System.Reflection;
using System.Text;

namespace NetCms.Vertical
{
    public class VerticalApplicationClasses
    {
        public const string Mainpage_BaseClassName = "NetCms.Vertical.VerticalApplicationMainPage";
        public const string FrontAdapter_BaseClassName = "NetCms.Vertical.VerticalApplicationFrontPageAdapter";

        private string _Mainpage;
        public string Mainpage
        { get { return _Mainpage; } }

        private string _FrontAdapter;
        public string FrontAdapter
        { get { return _FrontAdapter; } }

        private VerticalApplication Application
        {
            get { return _Application; }
            set { _Application = value; }
        }
        private VerticalApplication _Application;

        public VerticalApplicationClasses(VerticalApplication application)
        {
            this.Application = application;
            FindClasses();
        }

        public void FindClasses()
        {
            Type[] types = Application.Assembly.GetTypes();

            foreach (Type type in types)
            {
                if (type.BaseType != null)
                {
                    switch (type.BaseType.FullName)
                    {
                        case Mainpage_BaseClassName: this._Mainpage = type.ToString(); break;
                        case FrontAdapter_BaseClassName: this._FrontAdapter = type.ToString(); break;
                    }
                }
            }
        }
    }

}