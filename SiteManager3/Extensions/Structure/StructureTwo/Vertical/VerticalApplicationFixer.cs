﻿using System;
using System.Data;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Collections.Generic;
using NHibernate;
using NetCms.Vertical.BusinessLogic;
using NetCms.Grants;

namespace NetCms.Vertical
{
    public class VerticalApplicationFixer
    {
        #region Sql Models
        /*
        protected const string SelectApplicationSql = @"SELECT * 
                                                        FROM 
                                                          externalappz
                                                        WHERE
                                                          id_ExternalApp = {0}   AND
                                                          Nome_ExternalApp = '{1}'   AND
                                                          Key_ExternalApp = '{2}'   AND
                                                          HomepageGroup_ExternalApp = {3}  AND
                                                          Description_ExternalApp = '{4}'  AND
                                                          NetworkDependant_ExternalApp = {5}  AND
                                                          AccessoMode_ExternalApp = {6}
                                                        "; 
          

        protected const string SelectPageSql = @"SELECT * FROM applicazioni_pagine WHERE Pagename_PaginaApplicazione = '{0}'
                                                    AND ClassName_PaginaApplicazione = '{1}'
                                                    AND Applicazione_PaginaApplicazione = {2}";
        
        protected const string SelectCriteriaSql = @"SELECT * FROM externalappzcriteria WHERE Key_ExternalAppCriteria = '{0}'";
        
        protected const string RepairApplicationSql = @"UPDATE externalappz SET 
                                                        Nome_ExternalApp = '{1}',
                                                        Key_ExternalApp = '{2}',
                                                        HomepageGroup_ExternalApp = '{3}',
                                                        Description_ExternalApp = '{4}',
                                                        NetworkDependant_ExternalApp = {5},
                                                        AccessoMode_ExternalApp = {6}
                                                        WHERE id_ExternalApp = {0}";
        
        protected const string DeletePagesSql = @"DELETE FROM externalappz_pages WHERE ExternalApp_ExternalAppPage = {0}";
        
        protected const string RepairCriteriaSql = @"
UPDATE 
  externalappzcriteria  
SET 
  Nome_ExternalAppCriteria = '{1}',
  Dynamic_ExternalAppCriteria = '{3}'
WHERE 
  Key_ExternalAppCriteria = '{2}' AND
  App_ExternalAppCriteria = '{4}'
;
";
          */
        #endregion

        public VerticalApplication Application
        {
            get
            {
                return _Application;
            }
            private set
            {
                _Application = value;
            }
        }
        private VerticalApplication _Application;

        public Dictionary<string,string> ApplicationCriteria
        {
            get
            {
                if (_ApplicationCriteria == null)
                {
                    _ApplicationCriteria = new Dictionary<string,string>();

                    _ApplicationCriteria.Add(VerticalApplicationSetup.DefaultCriteriaName, "Consenti Utilizzo");
                    _ApplicationCriteria.Add(VerticalApplicationSetup.GrantsCriteriaName, "Permetti gestione dei permessi");

                    VerticalApplicationSetup Installer = (this.Application is VerticalApplicationPackage) ? (this.Application as VerticalApplicationPackage).Installer : this.Application.Installer;

                    for (int i = 0; i < Installer.Criteria.Length / 2; i++)
                        _ApplicationCriteria.Add(Installer.Criteria[i, 1], Installer.Criteria[i, 0]);
                }
                return _ApplicationCriteria;
            }
        }
        private Dictionary<string,string> _ApplicationCriteria;

        public VerticalApplicationFixer(VerticalApplication app)
        {
            Application = app;
        }
        
        public bool Repair()
        {
            Application.Validator.Validate();
            List<NetCms.Vertical.Validation.VerticalApplicationValidatorResult> list = Application.Validator.ValidationResults;

            bool pagesJustRepaired = false;

            foreach (NetCms.Vertical.Validation.VerticalApplicationValidatorResult validation in list)
            {
                if (!validation.IsOk)
                {
                    switch (validation.RepairType)
                    {
                        case NetCms.Vertical.Validation.VerticalApplicationValidator.RepairTypes.Application: RepairApplication(validation); break;
                        case NetCms.Vertical.Validation.VerticalApplicationValidator.RepairTypes.Criteria: RepairCriteria(validation); break;
                        case NetCms.Vertical.Validation.VerticalApplicationValidator.RepairTypes.Pages:
                            if (!pagesJustRepaired)
                            {
                                RepairPage(validation);
                                pagesJustRepaired = true;
                            }
                            break;
                    }
                }
            }

            G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
            this.Application.UpdateApplicationData();
            cache.RemoveAll("");

            bool repairSuccessfull = Application.Validator.Validate();

            if (repairSuccessfull)
            {
                if(VerticalApplicationsPool.NonActiveAssemblies.Contains(this.Application.ID.ToString()))
                    VerticalApplicationsPool.NonActiveAssemblies.Remove(this.Application.ID.ToString());
                if(!VerticalApplicationsPool.ActiveAssemblies.Contains(this.Application.ID.ToString()))
                    VerticalApplicationsPool.ActiveAssemblies.Add(this.Application);
            }

            return repairSuccessfull;
        }

        public void RepairApplication(NetCms.Vertical.Validation.VerticalApplicationValidatorResult validation)
        {
            int accessmode = 0;

            switch (this.Application.AccessMode)
            {
                case VerticalApplication.AccessModes.Admin: accessmode = 0; break;
                case VerticalApplication.AccessModes.Front: accessmode = 1; break;
                case VerticalApplication.AccessModes.Both: accessmode = 2; break;
            }

            //this.Application.Connection.Execute(String.Format(RepairApplicationSql,
            //                                              this.Application.ID,
            //                                              this.Application.Label.FilterForDB(),
            //                                              this.Application.SystemName.FilterForDB(),
            //                                              this.Application.HomepageGroup,
            //                                              this.Application.Description.FilterForDB(),
            //                                              this.Application.NetworkDependant ? 1 : 0,
            //                                              accessmode));

            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {

                    VerticalApplication app = VerticalAppBusinessLogic.GetVerticalAppById(this.Application.ID,sess);

                    app.Label = this.Application.Label;
                    app.SystemName = this.Application.SystemName;
                    app.HomepageGroup = this.Application.HomepageGroup;
                    app.Description = this.Application.Description;
                    app.NetworkDependant = this.Application.NetworkDependant;
                    app.AccessModeInt = accessmode;

                    VerticalAppBusinessLogic.SaveOrUpdateVerticalApplication(app,sess);
                    tx.Commit();

                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la riparazione dei record principali dell'applicazione verticale " + Application.Label + " con identificativo " + Application.ID + ". " + ex.Message);
                }
            }
        }
        public void RepairPage(NetCms.Vertical.Validation.VerticalApplicationValidatorResult validation)
        {
            //Application.Connection.Execute(String.Format(DeletePagesSql, this.Application.ID));
            //this.Application.Installer.AddMainPage();
            //this.Application.Installer.AddPages();

            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try 
                {

                    VerticalApplication app = VerticalAppBusinessLogic.GetVerticalAppById(this.Application.ID,sess);
                    app.Pages.Clear();
                    VerticalAppBusinessLogic.SaveOrUpdateVerticalApplication(app,sess);

                    ///Introduzione logica di riparazione pagine applicativi package
                    if (this.Application is VerticalApplicationPackage)
                    {
                        (this.Application as VerticalApplicationPackage).Installer.AddMainPage(app, sess);
                        (this.Application as VerticalApplicationPackage).Installer.AddPages(app, sess);
                    }
                    else
                    {
                        this.Application.Installer.AddMainPage(app, sess);
                        this.Application.Installer.AddPages(app, sess);
                    }
                    
                    tx.Commit();
                    
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la riparazione delle pagina dell'applicazione verticale " + Application.Label + " con identificativo " + Application.ID + ". " + ex.Message);
                }
            }
        }
        public void RepairCriteria(NetCms.Vertical.Validation.VerticalApplicationValidatorResult validation)
        {
            string LabelCriterio = ApplicationCriteria[validation.Name];

            VerticalApplicationSetup Installer = (this.Application is VerticalApplicationPackage) ? (this.Application as VerticalApplicationPackage).Installer : this.Application.Installer;
            //DataTable criterion = this.Application.Connection.SqlQuery(string.Format(SelectCriteriaSql, validation.Name));

            ExternalAppzCriteria criterion = ExternalAppzGrantsBusinessLogic.GetCriterioByAppAndKey(validation.Name, Application.ID);

            //if (criterion.Rows.Count > 0)
            if (criterion != null)
            {
                //Application.Connection.Execute(String.Format(RepairCriteriaSql,
                //                                          this.Application.ID,
                //                                          LabelCriterio.FilterForDB(),
                //                                          validation.Name.FilterForDB().ToLower(),
                //                                          Application.NetworkDependant ? 1 : 0,
                //                                          Application.ID.ToString()));

                using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = sess.BeginTransaction())
                {
                    try
                    {
                        ExternalAppzCriteria criterionToRepair = ExternalAppzGrantsBusinessLogic.GetCriterioByAppAndKey(validation.Name, Application.ID,sess);
                        criterionToRepair.Nome = LabelCriterio;
                        criterionToRepair.IsDynamic = Application.NetworkDependant;
                        ExternalAppzGrantsBusinessLogic.SaveOrUpdateExternalAppCriteria(criterionToRepair, sess);

                        tx.Commit();

                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la riparazione dei criteri dell'applicazione verticale " + Application.Label + " con identificativo " + Application.ID + ". " + ex.Message);
                    }
                }
            }
            else
            {
                using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = sess.BeginTransaction())
                {
                    try
                    {
                        VerticalApplication app = VerticalAppBusinessLogic.GetVerticalAppById(this.Application.ID,sess);

                        Installer.AddCriterion(LabelCriterio, validation.Name, app, sess);

                        VerticalAppBusinessLogic.SaveOrUpdateVerticalApplication(app,sess);

                        tx.Commit();

                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la riparazione dei criteri dell'applicazione verticale " + Application.Label + " con identificativo " + Application.ID + ". " + ex.Message);
                    }
                }

                //Application.Installer.AddCriterion(LabelCriterio, validation.Name);
            }
        }        
    }
}