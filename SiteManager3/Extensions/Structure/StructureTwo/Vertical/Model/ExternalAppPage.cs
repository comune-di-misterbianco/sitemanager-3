﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Vertical
{
    public class ExternalAppPage
    {
        public ExternalAppPage()
        { }

        public virtual int ID
        { get; set; }

        public virtual string PageName
        { get; set; }

        public virtual string ClassName
        { get; set; }

        public virtual VerticalApplication Application
        { get; set; }
    }
}
