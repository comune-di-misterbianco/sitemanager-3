﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Vertical.Mapping
{
    public class VerticalApplicationServiceUserMapping : ClassMap<VerticalApplicationServiceUser>
    {
        public VerticalApplicationServiceUserMapping()
        {
            Table("user_externalappzService");
            Cache.NonStrictReadWrite();
            Id(x => x.ID);
            Map(x => x.DataAttivazioneServizio);
            Map(x => x.ScadenzaServizio);

            References(x => x.User)
                .Column("User")
                .Fetch.Select();

            References(x => x.VerticalApplication)
                .Column("VerticalApplication")
                .Fetch.Select();
        }
    }
}
