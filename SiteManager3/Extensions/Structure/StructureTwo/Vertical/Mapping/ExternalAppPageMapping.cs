﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Vertical.Mapping
{
    public class ExternalAppPageMapping : ClassMap<ExternalAppPage>
    {
        public ExternalAppPageMapping()
        {
            Table("externalappz_pages");
            Cache.NonStrictReadWrite();
            Id(x => x.ID).Column("id_ExternalAppPage");
            Map(x => x.PageName).Column("PageName_ExternalAppPage").CustomSqlType("varchar(1024)");
            Map(x => x.ClassName).Column("ClassName_ExternalAppPage").CustomSqlType("text"); ;

            References(x => x.Application)
                 .ForeignKey("externalapp_pages")
                 .Column("ExternalApp_ExternalAppPage")
                 .Fetch.Select();
        }
    }
}