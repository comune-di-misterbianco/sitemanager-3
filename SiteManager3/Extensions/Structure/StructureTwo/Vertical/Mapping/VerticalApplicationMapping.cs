﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NetCms.Grants;

namespace NetCms.Vertical.Mapping
{
    public class VerticalApplicationMapping : ClassMap<VerticalApplication>
    {
        public VerticalApplicationMapping()
        {
            Table("externalappz");
            Cache.NonStrictReadWrite();
            Id(x => x.ID).Column("id_ExternalApp").GeneratedBy.Assigned();
            Map(x => x.Label).Column("Nome_ExternalApp");
            Map(x => x.SystemName).Column("Key_ExternalApp");
            Map(x => x.HomepageGroup).Column("HomepageGroup_ExternalApp");
            Map(x => x.Description).Column("Description_ExternalApp");
            Map(x => x.NetworkDependant).Column("NetworkDependant_ExternalApp");
            Map(x => x.Enabled).Column("Enabled_ExternalApp");
            Map(x => x.AccessModeInt).Column("AccessoMode_ExternalApp");
            Map(x => x.Path).Column("Path_ExternalApp");
            Map(x => x.Layout).Column("FrontLayout_ExternalApp");

            HasMany<ExternalAppPage>(x => x.Pages)
                .KeyColumn("ExternalApp_ExternalAppPage")
                .Fetch.Select().AsSet()
                .Cascade.AllDeleteOrphan().Inverse();

            HasMany<ExternalAppzCriteria>(x => x.AppzCriteria)
                .KeyColumn("App_ExternalAppCriteria")
                .Fetch.Select().AsSet()
                .Cascade.All().Inverse();

            HasMany<VerticalApplicationServiceUser>(x => x.ServiceUsers)
                .KeyColumn("VerticalApplication")
                .Fetch.Select().AsSet()
                .Cascade.All()
                .Inverse();
        }
    }
}
