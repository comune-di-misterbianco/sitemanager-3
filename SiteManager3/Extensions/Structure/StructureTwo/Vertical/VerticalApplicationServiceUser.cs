﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Users;

namespace NetCms.Vertical
{
    public class VerticalApplicationServiceUser
    {
        public VerticalApplicationServiceUser()
        {

        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual User User
        {
            get;
            set;
        }

        public virtual VerticalApplication VerticalApplication
        {
            get;
            set;
        }

        public virtual DateTime ScadenzaServizio
        {
            get;
            set;
        }

        public virtual DateTime DataAttivazioneServizio
        {
            get;
            set;
        }
    }
}
