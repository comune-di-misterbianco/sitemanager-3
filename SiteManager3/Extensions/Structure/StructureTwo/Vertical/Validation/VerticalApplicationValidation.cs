﻿using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Users;

namespace NetCms.Vertical.Validation
{
    public abstract class VerticalApplicationValidation
    {
        public List<VerticalApplicationValidatorResult> ValidationResults
        {
            get
            {
                if (_ValidationResults == null)
                {
                    _ValidationResults = new List<VerticalApplicationValidatorResult>();
                }
                return _ValidationResults;
            }
        }
        private List<VerticalApplicationValidatorResult> _ValidationResults;

        public VerticalApplication VerticalApplication
        {
            get
            {
                return _VerticalApplication;
            }
            private set
            {
                _VerticalApplication = value;
            }
        }
        private VerticalApplication _VerticalApplication;

        public NetCms.Connections.Connection Connection
        {
            get
            {
                return _Connection;
            }
            private set
            {
                _Connection = value;
            }
        }
        private NetCms.Connections.Connection _Connection;

        public VerticalApplicationValidation(VerticalApplication verticalApplication)
        {
            this.VerticalApplication = verticalApplication;
            this.Connection = verticalApplication.Conn;
        }

        public bool Validate()
        {
            this.ValidationResults.Clear();
            return DoValidation();
        }
        protected abstract bool DoValidation();
    }
}
