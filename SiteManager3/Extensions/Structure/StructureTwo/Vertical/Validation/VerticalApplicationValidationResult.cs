﻿using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Users;

namespace NetCms.Vertical.Validation
{
    public class VerticalApplicationValidatorResult
    {
        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private bool _IsOk;
        public bool IsOk
        {
            get { return _IsOk; }
            set { _IsOk = value; }
        }

        public string RepairTypeLabel
        {
            get
            {
                switch (this.RepairType)
                {
                    case NetCms.Vertical.Validation.VerticalApplicationValidator.RepairTypes.Criteria: return "Criterio Permessi";
                    case NetCms.Vertical.Validation.VerticalApplicationValidator.RepairTypes.Application: return "Record dell'Applicazione";
                    case NetCms.Vertical.Validation.VerticalApplicationValidator.RepairTypes.Pages: return "Pagina dell'Applicazione";
                    case NetCms.Vertical.Validation.VerticalApplicationValidator.RepairTypes.DatabaseMap: return "Coerenza Database";
                    case NetCms.Vertical.Validation.VerticalApplicationValidator.RepairTypes.Custom: return "Altro";
                    case NetCms.Vertical.Validation.VerticalApplicationValidator.RepairTypes.Pool: return "Persistenza nel Pool";
                    default: return "";
                }
            }
        }

        private string _Message;
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public VerticalApplicationValidator.RepairTypes RepairType
        {
            get
            {
                return _RepairType;
            }
            private set
            {
                _RepairType = value;
            }
        }
        private VerticalApplicationValidator.RepairTypes _RepairType;

        public VerticalApplicationValidatorResult(string name, bool isOk, string message)
            : this(name, isOk, message, VerticalApplicationValidator.RepairTypes.Custom)
        {
        }
        public VerticalApplicationValidatorResult(string name, bool isOk, string message, VerticalApplicationValidator.RepairTypes repairType)
        {
            this.Name = name;
            this.IsOk = isOk;
            this.Message = message;
            RepairType = repairType;
        }
    }
}
