﻿using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Users;
using System.Linq;
using NetCms.Vertical.BusinessLogic;
using NHibernate;

namespace NetCms.Vertical.Validation
{
    public class CriteriaValidation : VerticalApplicationValidation
    {
        public CriteriaValidation(VerticalApplication verticalApplication) : base(verticalApplication) { }
        //        private const string SelectSql = @"
        //SELECT * FROM externalappzcriteria WHERE App_ExternalAppCriteria = {0} ";


        //        public DataTable Criteri
        //        {
        //            get
        //            {
        //                if (_Criteri == null) BuildCriteriTable();
        //                return _Criteri;
        //            }
        //        }
        //        private DataTable _Criteri;

        //        private void BuildCriteriTable()
        //        {
        //            _Criteri = Connection.SqlQuery(String.Format(SelectSql, this.VerticalApplication.ID));
        //        }

        protected override bool DoValidation()
        {
            VerticalApplicationSetup Installer = (this.VerticalApplication is VerticalApplicationPackage) ? (this.VerticalApplication as VerticalApplicationPackage).Installer : this.VerticalApplication.Installer;

            //BuildCriteriTable();
            if (Installer != null && Installer.Criteria != null)
            {
                bool isvalid = true;

                VerticalApplicationValidatorResult validation = CheckCriterio(VerticalApplicationSetup.DefaultCriteriaName, "Consenti Utilizzo");
                this.ValidationResults.Add(validation);
                isvalid = isvalid & validation.IsOk;

                validation = CheckCriterio(VerticalApplicationSetup.GrantsCriteriaName, "Permetti gestione dei permessi");
                this.ValidationResults.Add(validation);
                isvalid = isvalid & validation.IsOk;

                for (int i = 0; i < Installer.Criteria.Length / 2; i++)
                {
                    validation = CheckCriterio(Installer.Criteria[i, 1], Installer.Criteria[i, 0]);
                    this.ValidationResults.Add(validation);
                    isvalid = isvalid & validation.IsOk;
                }
                return isvalid;
            }
            else return true;
        }

        private VerticalApplicationValidatorResult CheckCriterio(string name, string label)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                VerticalApplication vapp = VerticalAppBusinessLogic.GetVerticalAppById(this.VerticalApplication.ID,sess);
                VerticalApplicationValidatorResult result = new VerticalApplicationValidatorResult(name, vapp.AppzCriteria.Where(x => x.Nome == label && x.Key == name.ToLower()).Count() > 0, "Entry non presente o errata nella tabella dei criteri dell'applicazione", VerticalApplicationValidator.RepairTypes.Criteria);
                return result;
            }
        }
    }
}
