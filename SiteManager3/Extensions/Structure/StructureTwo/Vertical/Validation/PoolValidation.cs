﻿using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Users;

namespace NetCms.Vertical.Validation
{
    public class PoolValidation : VerticalApplicationValidation
    {
        public PoolValidation(VerticalApplication verticalApplication) : base(verticalApplication) { }

        protected override bool DoValidation()
        {
            bool ok = NetCms.Vertical.VerticalApplicationsPool.ActiveAssemblies.Contains(this.VerticalApplication.ID.ToString());
            this.ValidationResults.Add( new VerticalApplicationValidatorResult(VerticalApplication.Label, ok,
                                                          "L'applicazione non è presente nel pool delle applicazioni attive e funzionanti.", 
                                                          VerticalApplicationValidator.RepairTypes.Pool));
            return ok;
        }
    }
}
