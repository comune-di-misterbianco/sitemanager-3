﻿using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Users;
using System.Linq;
using NetCms.Vertical.BusinessLogic;
using NHibernate;

namespace NetCms.Vertical.Validation
{
    public class PagesValidation : VerticalApplicationValidation
    {
        public PagesValidation(VerticalApplication verticalApplication) : base(verticalApplication) { }

//        private const string SelectPageSql = @"
//SELECT 
//  *
//FROM 
//  externalappz_pages
//WHERE
//  ExternalApp_ExternalAppPage = {0}
//";

//        public DataTable Pages
//        {
//            get
//            {
//                if (_Pages == null) BuildPagesTable();
//                return _Pages;
//            }
//        }
//        private DataTable _Pages;

//        private void BuildPagesTable()
//        {
//            _Pages = Connection.SqlQuery(String.Format(SelectPageSql, this.VerticalApplication.ID));
//        }

        protected override bool DoValidation()
        {
            //BuildPagesTable();
            if (this.VerticalApplication.Installer != null || VerticalApplication is VerticalApplicationPackage)
            {
                bool isvalid = true;
                //if (this.VerticalApplication.Installer.Pages != null)
                //{
                //    for (int i = 0; i < this.VerticalApplication.Installer.Pages.Length / 2; i++)
                //    {
                //        VerticalApplicationValidatorResult validation = CheckPage(this.VerticalApplication.Installer.Pages[i, 0], this.VerticalApplication.Installer.Pages[i, 1]);
                //        this.ValidationResults.Add(validation);
                //        isvalid = isvalid & validation.IsOk;
                //    }
                //}

                //var types = this.VerticalApplication.Assembly.GetTypes();
                //foreach (System.Type t in types)
                //{
                //    System.Attribute[] attrs = System.Attribute.GetCustomAttributes(t);  // reflection

                //    foreach (System.Attribute attr in attrs)
                //    {
                //        if (attr is DynamicUrl.PageControl)
                //        {
                //            DynamicUrl.PageControl pageAddress = (DynamicUrl.PageControl)attr;
                //            VerticalApplicationValidatorResult validation = CheckPage(pageAddress.Address, t.FullName);
                //            this.ValidationResults.Add(validation);
                //            isvalid = isvalid & validation.IsOk;
                //        }
                //    }

                //}
                ///codice spostato sul metodo apposito
                ///
                isvalid = ValidateVerticalAppPages(this.VerticalApplication);

                ///Controllo relativo alle pagine delle applicazioni contenute
                if (this.VerticalApplication is VerticalApplicationPackage)
                {
                    foreach(int appID in (this.VerticalApplication as VerticalApplicationPackage).ContainedApplicationsIDs)
                    {
                        VerticalApplication app = VerticalApplicationsPool.Assemblies[appID.ToString()];
                        isvalid &= ValidateVerticalAppPages(app, true);
                    }
                }

                return isvalid;
            }
            
            
            else return true;
        }
        private VerticalApplicationValidatorResult CheckPage(string pageName, string codeClass)
        {
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                VerticalApplication vapp = VerticalAppBusinessLogic.GetVerticalAppById(this.VerticalApplication.ID,sess);
                if (vapp.Pages.Where(x => x.PageName.ToLower() == pageName.ToLower() && x.ClassName == codeClass).Count() == 0)
                    return new VerticalApplicationValidatorResult(pageName, false, "Entry non presente o errata nella tabella delle pagine dell'applicazione", VerticalApplicationValidator.RepairTypes.Pages);
                else return new VerticalApplicationValidatorResult(pageName, true, "", VerticalApplicationValidator.RepairTypes.Pages);
            }
        }

        /// <summary>
        /// Effettua una validazione delle pagine attualmente salvate, rispetto a quelle impostate sull'installer
        /// </summary>
        /// <param name="application">Applicazione da verificare</param>
        /// <param name="IsPackagedApp">Se true, l'applicazione da validare è contestualmente interna ad un pacchetto</param>
        /// <returns></returns>
        private bool ValidateVerticalAppPages(VerticalApplication application, bool IsPackagedApp = false)
        {
            bool isvalid = true;

            VerticalApplicationSetup Installer = application.Installer;

            if (application is VerticalApplicationPackage)
                Installer = (application as VerticalApplicationPackage).Installer;

            if (Installer.Pages != null)
            {
                for (int i = 0; i < Installer.Pages.Length / 2; i++)
                {
                    VerticalApplicationValidatorResult validation = CheckPage(Installer.Pages[i, 0], Installer.Pages[i, 1]);

                    ///Se l'applicazione è contenuta in un pacchetto il pagename conterrà come prefix il system name dell'applicazione per costruzione
                    if (IsPackagedApp)
                        validation.Name = "/" + application.SystemName + validation.Name;

                    this.ValidationResults.Add(validation);
                    isvalid = isvalid & validation.IsOk;
                }
            }

            var types = application.Assembly.GetTypes();

            foreach (System.Type t in types)
            {
                System.Attribute[] attrs = System.Attribute.GetCustomAttributes(t);  // reflection

                foreach (System.Attribute attr in attrs)
                {
                    if (attr is DynamicUrl.PageControl)
                    {
                        DynamicUrl.PageControl pageAddress = (DynamicUrl.PageControl)attr;

                        ///Se l'applicazione è contenuta in un pacchetto il pagename conterrà come prefix il system name dell'applicazione per costruzione
                        VerticalApplicationValidatorResult validation = CheckPage(IsPackagedApp ? "/" + application.SystemName + pageAddress.Address : pageAddress.Address, t.FullName);
                        this.ValidationResults.Add(validation);
                        isvalid = isvalid & validation.IsOk;
                    }
                }

            }

            return isvalid;
        }
    }
}
