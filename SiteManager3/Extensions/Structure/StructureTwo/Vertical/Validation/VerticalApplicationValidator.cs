﻿using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Users;

namespace NetCms.Vertical.Validation
{
    public class VerticalApplicationValidator
    {
        public enum RepairTypes
        {
            Custom,
            Pages,
            Application,
            Pool,
            Criteria,
            DatabaseMap
        }
        
        private bool _IsValid;
        public bool IsValid
        {
            get
            {
                if (ValidationResults.Count == 0)
                    Validate();

                return _IsValid;
            }
            private set { _IsValid = value; }
        }

        public List<VerticalApplicationValidatorResult> ValidationResults
        {
            get
            {
                if (_ValidationResults == null)
                {
                    _ValidationResults = new List<VerticalApplicationValidatorResult>();
                }
                return _ValidationResults;
            }
        }
        private List<VerticalApplicationValidatorResult> _ValidationResults;

        public List<VerticalApplicationValidation> Validators
        {
            get
            {
                if (_Validators == null)
                {
                    _Validators = new List<VerticalApplicationValidation>();
                }
                return _Validators;
            }
        }
        private List<VerticalApplicationValidation> _Validators;

        public VerticalApplication VerticalApplication
        {
            get
            {
                return _VerticalApplication;
            }
            private set
            {
                _VerticalApplication = value;
            }
        }
        private VerticalApplication _VerticalApplication;

        public NetCms.Connections.Connection Connection
        {
            get
            {
                return _Connection;
            }
            private set
            {
                _Connection = value;
            }
        }
        private NetCms.Connections.Connection _Connection;

        public VerticalApplicationValidator(VerticalApplication verticalApplication)
        {
            this.VerticalApplication = verticalApplication;
            this.Connection = verticalApplication.Conn;

            this.Validators.Add(new ApplicationValidation(verticalApplication));
            this.Validators.Add(new PagesValidation(verticalApplication));
            this.Validators.Add(new CriteriaValidation(verticalApplication));
        }

        public bool Validate()
        {
            ValidationResults.Clear();

            bool allOK = true;
            foreach (var validator in this.Validators)
            {
                validator.Validate();

                foreach (var validation in validator.ValidationResults)
                {
                    allOK = allOK & validation.IsOk; 
                    this.ValidationResults.Add(validation);
                }
            }
            IsValid = allOK;

            return IsValid;
        }
    }
}
