﻿using System;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Grants;
using NetCms.Users;
using NetCms.Vertical.BusinessLogic;
using NHibernate;

namespace NetCms.Vertical.Validation
{
    public class ApplicationValidation : VerticalApplicationValidation
    {
        
        public ApplicationValidation(VerticalApplication verticalApplication) : base(verticalApplication) { }

        protected override bool DoValidation()
        {
            int accessmode = 0;

            switch (this.VerticalApplication.AccessMode)
            {
                case VerticalApplication.AccessModes.Admin: accessmode = 0; break;
                case VerticalApplication.AccessModes.Front: accessmode = 1; break;
                case VerticalApplication.AccessModes.Both: accessmode = 2; break;
            }
            bool ok = false;
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                ok = VerticalAppBusinessLogic.GetVerticalAppForFixer(this.VerticalApplication.ID, this.VerticalApplication.Label, this.VerticalApplication.SystemName, this.VerticalApplication.HomepageGroup, this.VerticalApplication.Description, this.VerticalApplication.NetworkDependant, accessmode,sess) != null;
                this.ValidationResults.Add(new VerticalApplicationValidatorResult(VerticalApplication.Label, ok,
                                                              (!ok) ? "Il record principale dell'applicazione è corrotto." : "Il record principale dell'applicazione è funzionante.",
                                                              VerticalApplicationValidator.RepairTypes.Application));

                if(VerticalApplication  is VerticalApplicationPackage)
                {
                    foreach (int appzID in (VerticalApplication as VerticalApplicationPackage).ContainedApplicationsIDs)
                    {
                        ok &= VerticalApplicationsPool.Assemblies.Contains(appzID);
                        this.ValidationResults.Add(new VerticalApplicationValidatorResult("L'applicativo '" + VerticalApplicationsPool.Assemblies[appzID.ToString()].Description + "' è stato rilvato correttamente.", ok,
                                                              (!ok) ? "Non risulta presente l'applicativo, necessario, con identificatore " + appzID + "." : "L'applicativo '" + VerticalApplicationsPool.Assemblies[appzID.ToString()].Description + "' è stato rilvato correttamente.",
                                                              VerticalApplicationValidator.RepairTypes.Application));
                    }

                }
                
            }
            return ok;
        }
    }
}
