﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Users;
using NetCms.Vertical.BusinessLogic;

namespace NetCms.Vertical.Validation
{
    public class ServiceValidator : ApplicationPageStatusValidator
    {
        public User User { get; private set; }
        public VerticalApplication VerticalApplication { get; private set; }
        public ServiceValidator(User user, VerticalApplication vapp)
        {
            this.User = user;
            this.VerticalApplication = vapp;
        }

        public override string ErrorMessage
        {
            get { return "L'utente " + User.NomeCompleto + " non risulta associato al servizio " + VerticalApplication.Label; }
        }

        public override string ErrorTitle
        {
            get { return "Utente non autorizzato"; }
        }

        public override bool Validate()
        {
            return VerticalAppBusinessLogic.UserHasService(User, VerticalApplication);
        }
    }
}
