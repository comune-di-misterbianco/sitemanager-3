﻿using NetCms.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NetCms.Vertical
{
    /// <summary>
    /// Classe rappresentante una applicazione verticale container di altre applicazioni verticali
    /// Un package è di per se un'applicazione verticale, da qui la scelta dell'ereditarietà
    /// Un package contiene più applicazioni verticali, la stessa applicazione verticale può fare parte di più pacchetti,
    /// ne nascerebbe una relazione many to many, ma per evitare di modificare il workflow di installazione globale, si definisce 
    /// un pacchetto a partire da un array di interi contenente gli identificatori delle applicazioni componenti.
    /// </summary>
    public abstract class VerticalApplicationPackage : VerticalApplication
    {
        /// <summary>
        /// Array contenente gli identificatori delle applicazioni verticali contenute nel pacchetto
        /// </summary>
        public abstract int[] ContainedApplicationsIDs
        {
            get;
        }

        /// <summary>
        /// Se true, tutte le dll necessarie al funzionamento del pacchetto sono state rilevate dal sistema
        /// </summary>
        public virtual bool IsVerified
        {
            get
            {
                bool verified = true;

                foreach (int appzID in ContainedApplicationsIDs)
                    verified &= VerticalApplicationsPool.Assemblies.Contains(appzID);               

                return verified;
            }
        }

        /// <summary>
        /// Definisce la classe che effettua l'istallazione dell'applicazione
        /// </summary>
        public new virtual VerticalApplicationPackageSetup Installer { get { return null; } }

        public VerticalApplicationPackage()
            :base()
        {
        }

        public VerticalApplicationPackage(Assembly assembly)
            : base(assembly)
        {
        }

        public override bool IsSystemApplication { get { return false; } }

        //public abstract Toolbar ToolbarMaster
        //{
        //    get;
        //}

    }
}
