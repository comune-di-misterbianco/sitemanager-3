﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.GUI;
using NetCms.Structure.WebFS;

/// <summary>
/// Summary description for StructureDocument
/// </summary>
namespace NetCms.Importer
{
    public class DocumentImporter
    {
        public string SourceFilePath
        {
            get 
            {
                if (_SourceFilePath == null)
                {
                    _SourceFilePath = Importer.SourceFolderPath + "\\" + "File-" + this.SourceData["id_Doc"].ToString();
                    if (!System.IO.File.Exists(_SourceFilePath))
                    {
                        //_SourceFilePath = string.Empty;
                        _SourceFilePath = Importer.SourceFolderPath + "\\" + "File-" + this.SourceData["id_Doc"].ToString() + "-empty";
                        if (!System.IO.File.Exists(_SourceFilePath)) _SourceFilePath = string.Empty;
                    }
                }
                return _SourceFilePath; 
            }
        }
        private string _SourceFilePath;

        public DataRow SourceData
        {
            get { return _SourceData; }
            private set { _SourceData = value; }
        }
        private DataRow _SourceData;

        public Importer Importer
        {
            get { return _Importer; }
            private set { _Importer = value; }
        }
        private Importer _Importer;

        public StructureFolder OwnerFolder
        {
            get { return _OwnerFolder; }
            private set { _OwnerFolder = value; }
        }
        private StructureFolder _OwnerFolder;

        public bool HasFile
        {
            get
            {
                return this.SourceFilePath.Length>0;
            }
        }
        
        public DocumentImporter(StructureFolder ownerFolder, DataRow sourceData, Importer importer)
        {
            this.OwnerFolder = ownerFolder;
            this.SourceData = sourceData;
            this.Importer = importer;
            
        }

        public bool Import(string[] ExistentIDs, out string[] IDs, bool isRootDocsImport = false)
        {
            if (HasFile)
                return this.OwnerFolder.CreateDocumentFromImport(SourceData, SourceFilePath, ExistentIDs, out IDs, isRootDocsImport, Importer);
            else
                return this.OwnerFolder.CreateDocumentFromImport(SourceData, ExistentIDs, out IDs, isRootDocsImport, Importer);
            IDs = null;
            return true;
        }
    }
}