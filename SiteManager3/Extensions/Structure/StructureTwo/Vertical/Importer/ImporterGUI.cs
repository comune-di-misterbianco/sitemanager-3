﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.GUI;
using NetCms.Networks;
using System.Collections.Generic;
using System.Linq;
using NetCms.Vertical;

namespace NetCms.Structure.Applications.Importer
{
    public class ImporterGUI : SmPageVertical
    {
        public override NetCms.GUI.SideBar.SideBar SideBar
        {
            get
            {
                if (_SideBar == null)
                    _SideBar = new NetCms.GUI.SideBar.HomeSideBar(this);
                return _SideBar;
            }
        }
        private NetCms.GUI.SideBar.SideBar _SideBar;

        public ImporterGUI()
        {
            if (NetCms.Configurations.Debug.GenericEnabled)
            {
                this.Toolbar.Buttons.Add(new ToolbarButton("import.aspx?op=extra", NetCms.GUI.Icons.Icons.Script_Gear, "Operazioni di adattamento Extra"));
                this.Toolbar.Buttons.Add(new ToolbarButton("cleancontent.aspx", NetCms.GUI.Icons.Icons.Script_Gear, "Pulizia riferimenti vecchi id"));
            }
        }

        public override string PageTitle
        {
            get { return "Importer"; }
        }

        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get { return NetCms.GUI.Icons.Icons.Arrow_Divide; }
        }

        public override string LocalCssClass
        {
            get { return NetCms.GUI.NetworkPage.BaseCssClass + "_Importer"; }
        }

        private HtmlGenericControl ImportPostbackControl
        {
            get
            {
                G2Core.XhtmlControls.InputField import = new G2Core.XhtmlControls.InputField("import");
                G2Core.Common.RequestVariable folder = new G2Core.Common.RequestVariable("folder",G2Core.Common.RequestVariable.RequestType.Form);
                G2Core.Common.RequestVariable network = new G2Core.Common.RequestVariable("network", G2Core.Common.RequestVariable.RequestType.Form);
                if (import.RequestVariable.IsValidString && network.IsValidInteger && folder.IsValidInteger)
                {
                    
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta cercando di importare il file con nome: " +import.RequestVariable.StringValue + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    
                    NetCms.Networks.NetworkKey NetworkKey = new NetCms.Networks.NetworkKey(network.IntValue, G2Core.Caching.Visibility.CachingVisibility.Global);
                    NetCms.Importer.Importer Importer = new NetCms.Importer.Importer(NetworkKey, folder.StringValue, import.RequestVariable.StringValue);
                    Importer.Import();

                    if (!Importer.Error)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " ha importato con successo il file con nome: " + import.RequestVariable.StringValue + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                    }

                    NetCms.GUI.PopupBox.AddMessage(Importer.LastImportResult);
                }
                return import.Control;
            }
        }
        private long GetDirectorySize(string path)
        {
            // 1
            // Get array of all file names.
            string[] a = Directory.GetFiles(path, "*.*");

            // 2
            // Calculate total bytes of all files in a loop.
            long b = 0;
            foreach (string name in a)
            {
                // 3
                // Use FileInfo to get length of each file.
                FileInfo info = new FileInfo(name);
                b += info.Length;
            }
            // 4
            // Return total size
            return b;
        }

        protected override WebControl RootControl
        {
            get
            {
                VerticalApplication importerApp = VerticalApplicationsPool.GetBySystemName("importer");
                //NetCms.Vertical.Grants.ExternalAppzGrants grants = new NetCms.Vertical.Grants.ExternalAppzGrants("importer");
                if (!importerApp.Grant) NetCms.GUI.PopupBox.AddSessionMessage("Non hai i permessi per accedere alla risorsa.", PostBackMessagesType.Error);

                if (!IsPostBack)
                {
                    string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta accedendo alla pagina dell'importer.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                }

                WebControl value = new WebControl(HtmlTextWriterTag.Div);
                value.Controls.Add(ImportPostbackControl);

                Table table = new Table();
                table.CssClass = "tab";
                table.CellPadding = 0;
                table.CellSpacing = 0;

                TableHeaderRow hrow = new TableHeaderRow();
                table.Controls.Add(hrow);

                TableHeaderCell hcell = new TableHeaderCell();
                hcell.Text = "Nome";
                hrow.Controls.Add(hcell);

                hcell = new TableHeaderCell();
                hcell.Text = "Tipo";
                hrow.Controls.Add(hcell);

                hcell = new TableHeaderCell();
                hcell.Text = "Data";
                hrow.Controls.Add(hcell);

                hcell = new TableHeaderCell();
                hcell.Text = "Dimensione";
                hrow.Controls.Add(hcell);

                hcell = new TableHeaderCell();
                hcell.Text = "Info";
                hrow.Controls.Add(hcell);

                hcell = new TableHeaderCell();
                hcell.Text = "Importa";
                hrow.Controls.Add(hcell);

                DirectoryInfo directory = new DirectoryInfo(Configurations.Paths.AbsoluteExportRoot);
                DirectoryInfo[] folders = directory.GetDirectories();
                bool alternate = false;
                foreach (DirectoryInfo obj in folders)
                {
                    if (obj.Name.StartsWith("SM2-ExportData-"))
                    {
                        TableRow row = new TableRow();
                        row.CssClass = alternate ? "alternate" : "normal";
                        table.Controls.Add(row);

                        TableCell cell = new TableCell();
                        cell.Text = obj.Name;
                        row.Controls.Add(cell);

                        cell = new TableCell();
                        cell.Text = "Cartella di Esportazione";
                        row.Controls.Add(cell);

                        cell = new TableCell();
                        cell.Text = obj.CreationTime.Year + "/" + obj.CreationTime.Month + "/" + obj.CreationTime.Day + " @ " + obj.CreationTime.Hour + ":" + obj.CreationTime.Minute;
                        row.Controls.Add(cell);

                        cell = new TableCell();
                        cell.Text = (this.GetDirectorySize(obj.FullName) / 1024).ToString() + "Mb";
                        row.Controls.Add(cell);

                        cell = new TableCell();
                        cell.CssClass = "action info";
                        cell.Text = "<a href=\"info.aspx?path=" + obj.FullName + "\\informations.xml\"><span>Info</span></a>";
                        row.Controls.Add(cell);

                        cell = new TableCell();
                        cell.CssClass = "action add";
                        cell.Text = "<a href=\"javascript: setFormValue('" + obj.Name + "','import',1);\"><span>Importa</span></a>";
                        row.Controls.Add(cell);
                    }
                }

                FileInfo[] files = directory.GetFiles();
                foreach (FileInfo obj in files)
                {
                    if (obj.Extension == ".sm2pkg" && obj.Name.StartsWith("SM2-ExportData-"))
                    {
                        TableRow row = new TableRow();
                        row.CssClass = alternate ? "alternate" : "normal";
                        table.Controls.Add(row);

                        TableCell cell = new TableCell();
                        cell.Text = obj.Name;
                        row.Controls.Add(cell);

                        cell = new TableCell();
                        cell.Text = "Pacchetto di Esportazione";
                        row.Controls.Add(cell);

                        cell = new TableCell();
                        cell.Text = obj.CreationTime.Year + "/" + obj.CreationTime.Month + "/" + obj.CreationTime.Day + " @ " + obj.CreationTime.Hour + ":" + obj.CreationTime.Minute;
                        row.Controls.Add(cell);

                        cell = new TableCell();
                        cell.Text = (obj.Length / 1024).ToString() + "Mb";
                        row.Controls.Add(cell);

                        cell = new TableCell();
                        cell.CssClass = "action download";
                        cell.Text = "<a href=\"" + Configurations.Paths.AbsoluteRoot + "/exported-data/" + obj.Name + "\"><span>Download</span></a>";
                        row.Controls.Add(cell);

                        cell = new TableCell();
                        cell.CssClass = "action add";
                        cell.Text = "<a href=\"javascript: setFormValue('" + obj.Name + "','import',1);\"><span>Importa</span></a>";
                        row.Controls.Add(cell);
                    }
                }

                value.Controls.Add(table);

                WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
                WebControl legend = new WebControl(HtmlTextWriterTag.Legend);

                fieldset.Controls.Add(legend);
                legend.Controls.Add(new LiteralControl("Selezionare Network e Cartella"));

                List<BasicNetwork> networks = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.Select(x => x.Value).ToList();//new NetCms.Networks.NetworksData();
                
                string options = "<label for=\"network\">Network</label><select id=\"network\" name=\"network\">";
                foreach (BasicNetwork row in networks)
                {
                    options += "<option value=\"" + row.ID + "\">" + row.Label + " [" + row.SystemName + "]</option>";
                }
                options += "</select>";
                options += "<label for=\"folder\">Target Folder ID</label><input type=\"text\" id=\"folder\" name=\"folder\" value=\"1\">";

                fieldset.Controls.Add(new LiteralControl(options));
                value.Controls.Add(fieldset);

                return value;
            }
        }
    }
}