﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Structure.StructureTwo.Vertical.Importer.Model
{
    public class ImportState
    {
        public int ID { get; set; }

        public string Tile { get; set; }

        public StatusEnums Status { get; set; }

        public string Message { get; set; }

        public enum StatusEnums
        {
            OK,
            Skipped,
            Error            
        }
    }
}
