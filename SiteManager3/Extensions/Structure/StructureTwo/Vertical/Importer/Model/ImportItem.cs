﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Structure.Importer.Model
{
    public class ImportItem
    {
        [JsonProperty("title", Required = Required.Always)]
        public string Title { get; set; }

        [JsonProperty("description", Required = Required.AllowNull)]
        public string Description { get; set; }

        
        [JsonProperty("date", Required = Required.Always)]
        public DateTime Date { get; set; }

        [JsonProperty("content", Required = Required.Always)]
        public string Content { get; set; }

        [JsonProperty("customFields", Required = Required.AllowNull)]
        public Dictionary<string,string> CustomFields
        {
            get;
            set;
        }

        [JsonProperty("attaches", Required = Required.AllowNull)]
        public List<ItemAttach> Attaches { get; set; }
    }

    public class ItemAttach
    {
        [JsonProperty("filename")]
        public string Filename { get; set; }

        [JsonProperty("ext")]
        public string Ext { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("originPath")]
        public string OriginPath { get; set; }
    }
}
