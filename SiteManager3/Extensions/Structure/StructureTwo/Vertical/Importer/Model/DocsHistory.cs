﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Structure.StructureTwo.Vertical.Importer.Model
{
    public class DocHistory 
    {
        public DocHistory() 
        {
        
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual int OldID
        {
            get;
            set;
        }

        public virtual int NewID
        {
            get;
            set;
        }

        public virtual int Type
        {
            get;
            set;
        }
    }
}
