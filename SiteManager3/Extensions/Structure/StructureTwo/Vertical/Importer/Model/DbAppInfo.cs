﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Structure.StructureTwo.Vertical.Importer.Model
{
    public class DbAppInfo
    {
        public DbAppInfo(string tab, string textField,string idField, int appId)
        {
            Tabella = tab;
            TextField = textField;
            IdField = idField;
            AppId = appId;

        }

        public string Tabella;
        public string TextField;
        public string IdField;
        public int AppId;
    }
}
