﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Structure.StructureTwo.Vertical.Importer.Model;

namespace Structure.StructureTwo.Vertical.Importer.Mapping
{
    public class DocsHistoryMapping : ClassMap<DocHistory>
    {
        public DocsHistoryMapping() 
        {
            Table("docs_histories_for_import");
            Id(x => x.ID);
            Map(x => x.NewID);
            Map(x => x.OldID);
            Map(x => x.Type);
        }
    }
}
