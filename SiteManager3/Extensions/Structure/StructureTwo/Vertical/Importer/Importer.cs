﻿using System;
using System.IO;
using System.Xml;
using System.Web;
using System.Data;
using NetCms.Networks;
using NetCms.Networks.WebFS;
using NetCms.Structure.WebFS;
using System.Xml.Linq;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using NetCms.Structure.Applications;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Text;

namespace NetCms.Importer
{
    public class Importer
    {
        protected NetCms.Users.User Account
        {
            get
            {
                if (_Account == null)
                    _Account = NetCms.Users.AccountManager.CurrentAccount;
                return _Account;
            }
        }
        private NetCms.Users.User _Account;

        /// <summary>
        /// Network sulla quale avverrà l'importazione
        /// </summary>
        public NetCms.Structure.WebFS.StructureNetwork Network
        {
            get
            {
                return _Network;
            }
            private set { _Network = value; }
        }
        private NetCms.Structure.WebFS.StructureNetwork _Network;

        /// <summary>
        /// Cartella di partenza per l'importazione dei dati
        /// </summary>
        public Structure.WebFS.StructureFolder TargetFolder
        {
            get
            {
                return _TargetFolder;
            }
            private set { _TargetFolder = value; }
        }
        private Structure.WebFS.StructureFolder _TargetFolder;

        public string SourceFolderName
        {
            get { return _SourceFolderName; }
            set { _SourceFolderName = value; }
        }
        private string _SourceFolderName;

        public string LastImportResult
        {
            get
            {
                if (_LastImportResult == null)
                    _LastImportResult = "";
                return "<ul>" + _LastImportResult + "</ul>";
            }
        }
        private string _LastImportResult;

        public void AddMessageInfo(string msg)
        {
            _LastImportResult += "<li>" + msg + "</li>";
        }

        public bool Error
        {
            get
            {
                return !string.IsNullOrEmpty(_LastImportResult);
            }
        }

        public string SourceFolderPath
        {
            get
            {
                if (_SourceFolderPath == null)
                    _SourceFolderPath = Configurations.Paths.AbsoluteExportRoot + SourceFolderName;
                return _SourceFolderPath;
            }
        }
        private string _SourceFolderPath;

        public DataTable FoldersTable
        {
            get
            {
                if (_FoldersTable == null)
                    _FoldersTable = SourceDatabase.Tables["folders"];
                return _FoldersTable;
            }
        }
        private DataTable _FoldersTable;

        /// <summary>
        /// Flag utilizzato per indicare se bisogna importare la cartella di root o meno.
        /// E' necessario impostarlo a false qualora l'importazione avvenga su di un portale che presenta già una root folder.
        /// (Caso tipico visto che l'installazione dell'Homepage installa anche il root record.)
        /// <value>true</value>
        /// </summary>
        public bool RootImport
        {
            get { return _RootImport; }
            set { _RootImport = value; }
        }
        private bool _RootImport;

        public DataSet SourceDatabase
        {
            get
            {
                if (_SourceDatabase == null)
                {
                    //_SourceDatabase = new DataSet();
                    //_SourceDatabase.ReadXml(SourceFolderPath + "\\" + "data.xml");
                    InitSourceDataBase();
                }
                return _SourceDatabase;
            }
        }
        private DataSet _SourceDatabase;

        public void InitSourceDataBase() 
        {
            _SourceDatabase = new DataSet();
            _SourceDatabase.ReadXmlSchema(SourceFolderPath + "\\" + "schema.xsd");
            _SourceDatabase.ReadXml(SourceFolderPath + "\\" + "data.xml");
        }

        private void PerformFieldNormalization(string sourceExportName)
        {
            // normalizzazione dei nomi dei campi tra le versioni del cms
            // Id_Opera           --> id_Opera            
            // Id_Procedura       --> id_Procedura
            // Id_Ufficio         --> id_Ufficio
            // Tipo_EventoBando   --> id_Tipo
            // Id_Bando           --> id_Bando
            // Id_BandoEventoTipo --> id_BandoEventoTipo          



            using (StreamReader reader = new StreamReader(sourceExportName + "\\" + "data.xml"))
            {
                string strFile = reader.ReadToEnd();

                reader.Close();
                reader.Dispose();

                strFile = strFile.Replace("Id_Opera", "id_Opera");
                strFile = strFile.Replace("Id_Procedura", "id_Procedura");
                strFile = strFile.Replace("Id_Ufficio", "id_Ufficio");
                strFile = strFile.Replace("Id_Bando", "id_Bando");
                strFile = strFile.Replace("Id_BandoEventoTipo", "id_BandoEventoTipo");

                File.WriteAllText(sourceExportName + "\\" + "data.xml", strFile);

                strFile = "";
            }
        }


        public Importer(NetworkKey networkKey, string targetFolderID, string sourceExportName)
        {            
            InitSourceData(sourceExportName);
            

            //Network = (Structure.WebFS.StructureNetwork)Networks.NetworksManager.GetNetwork(networkKey);
            Network = new NetCms.Structure.WebFS.StructureNetwork(networkKey);
            TargetFolder = Network.RootFolder.FindFolderByID(int.Parse(targetFolderID)) as StructureFolder;

            if (Network == null)
            {                
                _RootImport = true;
            }
            else
            {
                _RootImport = false;
            }            
        }

        private void InitSourceData(string sourceExportName)
        {
            if (sourceExportName.EndsWith(".sm2pkg"))
            {
                try
                {
                    this.SourceFolderName = sourceExportName.Replace(".sm2pkg", "");
                    G2Core.Common.G2Zip package = new G2Core.Common.G2Zip(this.SourceFolderPath + ".sm2pkg", this.SourceFolderPath + "\\");
                    package.Estract();

                    // normalizzazione nome campi 
                    PerformFieldNormalization(this.SourceFolderPath);
                }
                catch (Exception e)
                {
                    /*this.AddMessageInfo("Impossibile estrarre il contenuto del pacchetto '" + sourceExportName + "'");
                    NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                    ExLog.SaveLog(this.Account.ID, e.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, e);*/
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Impossibile estrarre il contenuto del pacchetto durante l'importazione", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "56");
                }
            }
            else
                this.SourceFolderName = sourceExportName;
        }

        public void Import()
        {
            if (!Error)
            {
                //Lettura nome prima Folder nell'elenco presente nel pacchetto di importazione
                string targetFolderName = this.FoldersTable.Rows[0]["Name_Folder"].ToString();
                //if (!RootImport) 
                //{
                   
                //    if (this.FoldersTable.Rows[1]["Name_Folder"] != null)
                //        targetFolderName = this.FoldersTable.Rows[1]["Name_Folder"].ToString();
                //    else
                //        return;
                   
                //}
                //Verifica se la Folder non è una subFolder della TargetFolder

                //if (this.TargetFolder.Folders.GetByName(targetFolderName) == null)
                if (FolderBusinessLogic.GetByName(TargetFolder,targetFolderName) == null)
                {
                    bool import = true;
                    if (!RootImport)
                        import = false;
                    //Creazione della prima Folder specificando se deve essere importata la Root (import = true) o meno (import = false)
                    Hashtable FolderCorrispondence = new Hashtable();
                    CreateFolder(this.FoldersTable.Rows[0], this.TargetFolder, import,FolderCorrispondence);

                    CreateDocuments(this.FoldersTable.Rows[0], this.TargetFolder,FolderCorrispondence);
                    //new G2Core.Caching.Cache().RemoveAll("");
                    _LastImportResult = "Importazione effettuata con successo!";
                }
                else
                    this.AddMessageInfo("Impossibile importare il contenuto nella cartella di destinazione scelta, in quanto la cartella '" + this.TargetFolder.Nome + "' contiene già una cartella con nome '" + targetFolderName + "'");
            }
        }
        public void CreateFolder(DataRow sourceData, StructureFolder RecursiveTargetFolder, bool rootImport,Hashtable folderCorrisp)
        {
            //Filtro per la creazione delle cartelle le cui tipologie appartengono ad applicazioni realmente installate
            string tipoFolder = sourceData["Tipo_Folder"].ToString();
            Application app = NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies[tipoFolder];
            if (app == null)
            {
                folderCorrisp.Add(sourceData["id_Folder"].ToString(), null);
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "L'applicazione con ID = " + tipoFolder + " non risulta installata e pertanto la cartella '" + sourceData["Path_Folder"].ToString() + "' non sarà importata!", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                return;
            }

            StructureFolder newFolder = null;
            //Se deve essere importata la root, creazione della Folder (sourceData) all'interno di RecursiveTargetFolder
            //altrimenti posizionamento in RecursiveTargetFolder
            if (rootImport)
            {
                newFolder = RecursiveTargetFolder.CreateFolder(sourceData, false);
            }
            else
                newFolder = RecursiveTargetFolder;
            if (newFolder == null)
            {
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Errore nella creazione della cartella '" + sourceData["Path_Folder"].ToString() + "' non sarà importata!", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                throw new Exception();
            }

            folderCorrisp.Add(sourceData["id_Folder"].ToString(), newFolder.ID);
            Dictionary<string, string[]> importedDocs = new Dictionary<string, string[]>();
            //Importazione dei documenti di root che hanno una tipologia diversa da quella della cartella
            //if (newFolder.IsRootFolder)
            //{
                foreach (Application application in NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies)
                {
                    
                    if (newFolder.RelativeApplication.ID != application.ID)
                    {
                        DataTable docRootTable = SourceDatabase.Tables[application.SystemName + "RootDocs-" + sourceData["id_Folder"]];
                        if (docRootTable != null)
                        {
                            foreach (DataRow doc in docRootTable.Rows)
                            {
                                string[] outIds = null;
                                string[] existentIds = new string[2];
                                //Se il documento è già stato importato, recupero degli ids e passaggio all'importer
                                if (importedDocs.ContainsKey(doc["id_Doc"].ToString()))
                                    existentIds = importedDocs[doc["id_Doc"].ToString()];
                                DocumentImporter docImporter = new DocumentImporter(newFolder, doc, this);
                                if (!docImporter.Import(existentIds, out outIds,true))
                                    throw new Exception();
                                //Se non è stato importato prima, memorizzazione  degli ids appena importati
                                if (!importedDocs.ContainsKey(doc["id_Doc"].ToString()) && outIds != null)
                                    importedDocs.Add(doc["id_Doc"].ToString(), outIds);
                            }
                        }
                    }
                }
            //}

            string pr = "Parent_Folder = " + sourceData["id_Folder"];           

            //string xmlFileDataPath = SourceFolderPath + "\\" + "data.xml";

            //TextReader reader = new System.IO.StreamReader(xmlFileDataPath);
            //string xml = reader.ReadToEnd();
            //xml = xml.Replace("\u0000", System.String.Empty);
            
            //Selezione di tutte le Folder figlie (sub folders)
            //var all = XDocument.Parse(xml).Descendants("NewDataSet");
            var all = XDocument.Load(SourceFolderPath + "\\" + "data.xml").Descendants("NewDataSet");
 
            var folders = from fold in all.Descendants("folders")
                          orderby fold.Element("Tree_Folder").Value
                          where fold.Element("Parent_Folder") != null && fold.Element("Parent_Folder").Value == sourceData["id_Folder"].ToString()
                          select fold;


            DataTable tab = new DataTable();
            foreach (var cartella in folders)
            {
                //Creazione record DataRow con i dati delle sub folders letti dal file xml
                DataRow folder = tab.NewRow();
                foreach (XElement field in cartella.Descendants())
                {
                    if (!tab.Columns.Contains(field.Name.ToString()))
                        tab.Columns.Add(field.Name.ToString());
                    folder[field.Name.ToString()] = field.Value;
                }
                //Chiamata ricorsiva del metodo CreateFolder specificando come sourceData il nuovo record di Folder (folder)
                //come RecursiveTargetFolder l'oggetto newFolder ed impostando import = true poichè stavolta è necessario
                //creare la folder (folder) in quanto non esistente
                CreateFolder(folder, newFolder, true,folderCorrisp);
            }


            ////Creazione dei documenti relativi alla Folder con verifica se hanno un file o meno ed importazione
            ////Preparo una collezione dei documenti importati in cui la chiave è l'id doc e il valore un array di stringhe con id doc e id docs lang

            //DataTable documentsTable = SourceDatabase.Tables["docs-" + sourceData["id_Folder"]];
            //if (documentsTable != null)
            //{
            //    foreach (DataRow doc in documentsTable.Rows)
            //    {
            //        string[] outIds = null;
            //        string[] existentIds = new string[2];
            //        //Se il documento è già stato importato, recupero degli ids e passaggio all'importer
            //        if (importedDocs.ContainsKey(doc["id_Doc"].ToString()))
            //            existentIds = importedDocs[doc["id_Doc"].ToString()];
            //        DocumentImporter docImporter = new DocumentImporter(newFolder, doc, this);
            //        if (!docImporter.Import(existentIds, out outIds))
            //            throw new Exception();
            //        //Se non è stato importato prima, memorizzazione  degli ids appena importati
            //        if (!importedDocs.ContainsKey(doc["id_Doc"].ToString()) && outIds != null)
            //            importedDocs.Add(doc["id_Doc"].ToString(), outIds);
            //    }
            //}
        }


        public void CreateDocuments(DataRow sourceData, StructureFolder RecursiveTargetFolder, Hashtable folderCorrisp)
        {
            StructureFolder newFolder = null;
            //Se deve essere importata la root, creazione della Folder (sourceData) all'interno di RecursiveTargetFolder
            //altrimenti posizionamento in RecursiveTargetFolder
            newFolder = RecursiveTargetFolder;
            if (newFolder == null)
            {
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Cartella '" + newFolder.Path + "' non trovata!", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                throw new Exception();
            }
            
            string pr = "Parent_Folder = " + sourceData["id_Folder"];
            //Selezione di tutte le Folder figlie (sub folders)
            var all = XDocument.Load(SourceFolderPath + "\\" + "data.xml").Descendants("NewDataSet");
            var folders = from fold in all.Descendants("folders")
                          orderby fold.Element("Tree_Folder").Value
                          where fold.Element("Parent_Folder") != null && fold.Element("Parent_Folder").Value == sourceData["id_Folder"].ToString()
                          select fold;

            DataTable tab = new DataTable();
            foreach (var cartella in folders)
            {
                //Creazione record DataRow con i dati delle sub folders letti dal file xml
                DataRow folder = tab.NewRow();
                foreach (XElement field in cartella.Descendants())
                {
                    if (!tab.Columns.Contains(field.Name.ToString()))
                        tab.Columns.Add(field.Name.ToString());
                    folder[field.Name.ToString()] = field.Value;
                }
                //Chiamata ricorsiva del metodo CreateDocuments specificando come sourceData il nuovo record di Folder (folder)
                //come RecursiveTargetFolder l'oggetto newFolder

               
                if (folderCorrisp[folder["id_Folder"].ToString()] != null)
                    CreateDocuments(folder, RecursiveTargetFolder.FindFolderByID(int.Parse(folderCorrisp[folder["id_Folder"].ToString()].ToString())) as StructureFolder, folderCorrisp);
                
            }

            //if (!newFolder.HasSubFolders)
            //{
                //Creazione dei documenti relativi alla Folder con verifica se hanno un file o meno ed importazione
                //Preparo una collezione dei documenti importati in cui la chiave è l'id doc e il valore un array di stringhe con id doc e id docs lang
                Dictionary<string, string[]> importedDocs = new Dictionary<string, string[]>();
                DataTable documentsTable = SourceDatabase.Tables["docs-" + sourceData["id_Folder"]];
                if (documentsTable != null)
                {
                    foreach (DataRow doc in documentsTable.Rows)
                    {
                        string[] outIds = null;
                        string[] existentIds = new string[2];
                        //Se il documento è già stato importato, recupero degli ids e passaggio all'importer
                        if (importedDocs.ContainsKey(doc["id_Doc"].ToString()))
                            existentIds = importedDocs[doc["id_Doc"].ToString()];
                        DocumentImporter docImporter = new DocumentImporter(newFolder, doc, this);
                        if (!docImporter.Import(existentIds, out outIds))
                            throw new Exception();
                        //Se non è stato importato prima, memorizzazione  degli ids appena importati
                        if (!importedDocs.ContainsKey(doc["id_Doc"].ToString()) && outIds != null)
                            importedDocs.Add(doc["id_Doc"].ToString(), outIds);
                    }
                }
            //}
        }
    }
}