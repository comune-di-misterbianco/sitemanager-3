﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.DBSessionProvider;
using NHibernate;
using GenericDAO.DAO;
using Structure.StructureTwo.Vertical.Importer.Model;
using NetService.Utility.RecordsFinder;
using NetCms.Structure.WebFS;
using NHibernate.Criterion;
using System.Text.RegularExpressions;

namespace Structure.StructureTwo.Vertical.Importer.BusinessLogic
{
    public static class DocHistoryBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static void SaveDocHistory(int oldId, int newId, int doctype, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<DocHistory> dao = new CriteriaNhibernateDAO<DocHistory>(innerSession);

            DocHistory docHistory = new DocHistory();
            docHistory.OldID = oldId;
            docHistory.NewID = newId;
            docHistory.Type = doctype;

            dao.SaveOrUpdate(docHistory);
        }

        public static int GetID(int oldID, int doctype, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            int docID = -1;

            SearchParameter oldIdSP = new SearchParameter(null, "OldID", oldID, Finder.ComparisonCriteria.Equals, false);
            SearchParameter typeSP = new SearchParameter(null, "Type", doctype, Finder.ComparisonCriteria.Equals, false);

            IGenericDAO<DocHistory> dao = new CriteriaNhibernateDAO<DocHistory>(innerSession);

            DocHistory result = dao.GetByCriteria(new SearchParameter[] { oldIdSP, typeSP });
            if (result != null)
                docID = result.NewID;
            return docID;

        }

        public static List<DbAppInfo> GetAppsInfo()
        {
            List<DbAppInfo> apps = new List<DbAppInfo>();

            // apps.Add(new DbAppInfo("homepages_moduli_statici", "link_modulo_statico", "rif_modulo_modulo_statico", 1));
            apps.Add(new DbAppInfo("news_docs_contents", "Testo_News", "id_news", 3));
            // apps.Add(new DbAppInfo("webdocs_docs_contents", "Testo_WebDocs", "id_WebDocs", 2));
            //apps.Add(new DbAppInfo("stampa_docs_contents", "Testo_Comunicato", "id_DocStampa", 8));

            return apps;
        }

        public static int CleanContent(ISession innerSession = null)
        {
            int status = 0;
            string idDoc = "";

            if (innerSession == null)
                innerSession = GetCurrentSession();


            Dictionary<int, string> appPatterns = new Dictionary<int, string>();
            appPatterns.Add(3, "aspx?news=");
            //appPatterns.Add(8, "?cs=");
            //appPatterns.Add(14, "?rs=");
            //appPatterns.Add(10, "?Faq=");
            //appPatterns.Add(6, "?ente=");
            // appPatterns.Add(11, "default.aspx?detail=");

            foreach (DbAppInfo app in GetAppsInfo()) // check app is installed
            {
                foreach (KeyValuePair<int, string> appPattern in appPatterns)
                {                   
                    try
                    {
                        string sqlQuery_new = "SELECT * FROM " + app.Tabella + " WHERE " + app.TextField + " LIKE '%" + appPattern.Value + "%';";

                        IList<object[]> result = innerSession.CreateSQLQuery(sqlQuery_new).List<object[]>();

                        string documentText = "";

                        foreach (object[] record in result)
                        {
                            bool contentChanged = false;

                            if (app.AppId == 1)
                                documentText = record[4].ToString();
                            else
                                documentText = record[1].ToString();

                            if (app.AppId == 1)
                            {
                                string strOLDURL = documentText;
                                string[] strURLArr = strOLDURL.Split(new string[] { appPattern.Value }, StringSplitOptions.None);

                                if (strURLArr.Length > 0)
                                {
                                    idDoc = strURLArr[1].Replace("\"", "").Replace("/", "");

                                    int newID = GetID(int.Parse(idDoc), appPattern.Key, innerSession);

                                    if (newID != -1)
                                    {
                                        string newUrl = strOLDURL.Replace(idDoc, newID.ToString());
                                        documentText = documentText.Replace(strOLDURL, newUrl);
                                        contentChanged = true;
                                    }
                                }
                            }
                            else
                            {
                                // cerco le istanze dei pattern
                                MatchCollection urls = Regex.Matches(documentText, @"href=\""(.*?)\""", RegexOptions.IgnoreCase);
                                foreach (Match url in urls)
                                {
                                    string strOLDURL = url.Value;
                                    if (strOLDURL.Contains(appPattern.Value))
                                    {
                                        string[] strURLArr = strOLDURL.Split(new string[] { appPattern.Value }, StringSplitOptions.None);
                                        if (strURLArr.Length > 0)
                                        {
                                            idDoc = strURLArr[1].Replace("\"", "").Replace("/", "");

                                            int newID = GetID(int.Parse(idDoc), appPattern.Key, innerSession);

                                            if (newID != -1)
                                            {
                                                string newUrl = strOLDURL.Replace(idDoc, newID.ToString());

                                                documentText = documentText.Replace(strOLDURL, newUrl);
                                                contentChanged = true;
                                            }
                                        }
                                    }
                                }
                            }
                            if (contentChanged)
                            {
                                string sqlUpdate = "UPDATE " + app.Tabella + " SET " + app.TextField + " = '" + MySql.Data.MySqlClient.MySqlHelper.EscapeString(documentText) + "' WHERE " + app.IdField + " = " + record[0];
                                var query = innerSession.CreateSQLQuery(sqlUpdate);
                                query.ExecuteUpdate();                               
                                status = 1;
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        string error = "Errore durante la pulizia dei vecchi riferimenti IdContent " + idDoc + " " + ex.Message;                  
                        status = -1;
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, error);
                    }
                }               
            }

            return status;
        }
    }
}
