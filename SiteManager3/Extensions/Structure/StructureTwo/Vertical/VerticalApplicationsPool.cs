﻿using System;
using System.Linq;
using System.Web;
using System.Data;
using System.Reflection;
using NetCms.DBSessionProvider;

namespace NetCms.Vertical
{
    public static class VerticalApplicationsPool
    {
        private const string NotInstalledApplicationsKey = "NetCms.Vertical.VerticalApplications.NotInstalledApplications";
        private const string InstalledApplicationsKey = "NetCms.Vertical.VerticalApplications.InstalledApplications";
        private const string AllApplicationsKey = "NetCms.Vertical.VerticalApplications.AllApplications";

        public static VerticalApplicationCollection NonActiveAssemblies
        {
            get
            {
                if (_NotInstalledAssemblies == null)
                {
                    if (HttpContext.Current.Application[NotInstalledApplicationsKey] != null)
                        _NotInstalledAssemblies = (VerticalApplicationCollection)HttpContext.Current.Application[NotInstalledApplicationsKey];
                    else
                        FindAppToInstall();
                }
                return _NotInstalledAssemblies;
            }
        }
        private static VerticalApplicationCollection _NotInstalledAssemblies;

        public static VerticalApplicationCollection ActiveAssemblies
        {
            get
            {
                if (_InstalledAssemblies == null)
                {
                    if (HttpContext.Current.Application[InstalledApplicationsKey] != null)
                        _InstalledAssemblies = (VerticalApplicationCollection)HttpContext.Current.Application[InstalledApplicationsKey];
                    else
                        FindAppToInstall();
                }

                return _InstalledAssemblies;
            }
        }
        private static VerticalApplicationCollection _InstalledAssemblies;

        public static VerticalApplicationCollection Assemblies
        {
            get
            {
                if (_Assemblies == null)
                {
                    if (HttpContext.Current.Application[AllApplicationsKey] != null)
                        _Assemblies = (VerticalApplicationCollection)HttpContext.Current.Application[AllApplicationsKey];
                    else
                        FindAppToInstall();
                }

                return _Assemblies;
            }
        }
        private static VerticalApplicationCollection _Assemblies;

        private static void FindAppToInstall()
        {
            _InstalledAssemblies = new VerticalApplicationCollection();
            _NotInstalledAssemblies = new VerticalApplicationCollection();
            _Assemblies = new VerticalApplicationCollection();
            /*DirectoryInfo binFolder = new DirectoryInfo(HttpContext.Current.Server.MapPath(Configurations.Paths.AbsoluteRoot + "/bin/"));

            foreach (FileInfo file in binFolder.GetFiles())
            {
                if (file.Extension == ".dll")
                {
                    Assembly asm = Assembly.LoadFrom(file.FullName);
                    Type[] types = asm.GetTypes();
                    foreach (Type type in types)
                    {
                        
                    }
                }
            }*/

            HttpContext.Current.Application[NotInstalledApplicationsKey] = NonActiveAssemblies;
            HttpContext.Current.Application[InstalledApplicationsKey] = ActiveAssemblies;
            HttpContext.Current.Application[AllApplicationsKey] = ActiveAssemblies;
        }

        public static object BuildInstanceOf(string className, string applicationID)
        {
            return BuildInstanceOf(className, applicationID, null);
        }
        public static object BuildInstanceOf(string className, string applicationID, object[] parameters, int[] relatedApps = null)
        {
            try
            {
                return Invoke(className, applicationID, parameters, relatedApps);
            }
            catch (Exception ex)
            {
                if(ex is System.Reflection.TargetInvocationException && ex.InnerException !=null)
                    throw ex.InnerException;                     
            }
            return null;
        }
       
        private static object Invoke(string className, string applicationID, object[] parameters, int[] relatedApps = null)
        {
            if (VerticalApplicationsPool.ActiveAssemblies.Contains(applicationID))
            {
                VerticalApplication application = ActiveAssemblies[applicationID];
                Type typeToInvoke = application.Assembly.GetType(className);
                
                //Codice aggiunto per gestire le app contenute nell'applicazione package
                if (typeToInvoke == null)
                {
                    foreach (int id in relatedApps)
                    {
                        application = ActiveAssemblies[id.ToString()];
                        typeToInvoke = application.Assembly.GetType(className);
                        if (typeToInvoke != null)
                            break;
                    }
                }

                Type[] array = application.Assembly.GetTypes();
                ConstructorInfo[] c = typeToInvoke.GetConstructors();
                int ParamethersCount = c[0].GetParameters().Length;
                if (parameters != null)
                //{                    
                //    if(typeToInvoke.BaseType.FullName ==  typeof(NetCms.Vertical.ApplicationPageEx).FullName)
                //    {
                //        System.Type[] types = { typeof(StateBag), typeof(bool), typeof(NetCms.GUI.Toolbar), typeof(NetCms.GUI.Info.InfoBox), typeof(NetCms.Networks.NetworkKey) };
                //        for (int i = 0; i < types.Length; i++)
                //            if (parameters[i] != null && types[i] != parameters[i].GetType())
                //                throw new WrongParamethersException("Errore nel cercare di istanziare l'applicazione verticale. Parametro Errato " + i, HttpContext.Current.Request.Url.ToString());
                //    }
                    return System.Activator.CreateInstance(typeToInvoke, parameters);
                //}
                else return System.Activator.CreateInstance(typeToInvoke);
            }
            //Trace?
            return null;
        }
        
        public static void ParseType(Type type, Assembly asm)
        {
            try
            {
                Object[] paramsArray = { asm };
                VerticalApplication application = (VerticalApplication)Activator.CreateInstance(type, paramsArray);
                Assemblies.Add(application);

                if (application.InstallState == VerticalApplication.InstallStates.Installed && application.Enabled)
                {
                    ActiveAssemblies.Add(application);
                }
                else
                    NonActiveAssemblies.Add(application);

                if (application.UseCmsSessionFactory && application.Enabled)
                {
                    foreach (Assembly ass in application.FluentAssembliesRelated)
                    {
                        if (!FluentSessionProvider.FluentAssemblies.Contains(ass))
                            FluentSessionProvider.FluentAssemblies.Add(ass);
                    }
                }
            }
            catch { Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Warning, "Non è stato possibile inizializzare l'applicazione '" + type.Name + "'", "", ""); }
        }

        public static NetCms.Vertical.VerticalApplication GetCurrentActiveApplication()
        {
            foreach (NetCms.Vertical.VerticalApplication vapp in NetCms.Vertical.VerticalApplicationsPool.ActiveAssemblies)
                if (vapp.Parser.IsCurrentActiveApplication)
                    return vapp;

            return null;
        }

        public static VerticalApplication GetByID(int id)
        {
            return Assemblies[id.ToString()];
        }
        public static VerticalApplication GetBySystemName(string systemName)
        {
            return Assemblies.Cast<VerticalApplication>().Where(x => x.SystemName == systemName).First();
        }

        public static VerticalApplication GetBySystemNameOrID(string value)
        {
            return Vertical.VerticalApplicationsPool.Assemblies.Cast<VerticalApplication>().Where(x => x.SystemName == value || x.ID.ToString() == value).First();
        }
    }
    public class WrongParamethersException : System.Exception
    {
        public string Url { get; private set; }
        public WrongParamethersException(string message, string url) : base(message) { Url = url; }
    }
}