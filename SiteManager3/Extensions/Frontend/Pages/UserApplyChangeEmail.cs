using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.Front.Static;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Users.Front
{
    [FrontendStaticPage(new[] { "/user/confirm_emailchange.aspx", "/en/user/confirm_emailchange.aspx" }, new[] { "Conferma cambio indirizzo email", "Confirm change email address" }, NetCms.Front.FrontLayouts.All, OnlyLoggedUsers = true)]
    public class UserApplyChangeEmail : WebControl
    {
        private NetFrontend.PageData PageData;
        private User _Account;
        public User Account
        {
            get
            {
                if (_Account == null && NetCms.Users.AccountManager.Logged)
                    _Account = NetCms.Users.AccountManager.CurrentAccount;
                return _Account;
            }
        }

        public UserApplyChangeEmail(NetFrontend.PageData pageData)
            : base(HtmlTextWriterTag.Div)
        {
            PageData = pageData;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (NetCms.Users.AccountManager.Logged)
            {
                this.CssClass = "form-user";
                
                this.Controls.Add(
                    NetCms.Users.AccountManager.CurrentAccount.Anagrafica.UpdateEmailByConfirmationLink()
                    );
            }
            else
            {
                new NetCms.Users.LoginControl();
            }
        }
    }
}