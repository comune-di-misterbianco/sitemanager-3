﻿using NetCms.Front.Static;
using NetCms.Networks.Grants;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Users
{
    [FrontendStaticPage(new[] { "/sso/default.aspx" , "/en/sso/default.aspx" }, new[] { "SSO Login page" }, NetCms.Front.FrontLayouts.None, OnlyLoggedUsers = false)]
    public class SSOLoginPage : WebControl
    {
        public SSOLoginPage() : base(HtmlTextWriterTag.Div)
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        static void CallbackMethod(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;

            NetCms.Users.LoginControl.CollectGrantsDelegate caller = 
                (NetCms.Users.LoginControl.CollectGrantsDelegate)result.AsyncDelegate;            

            ReturnMessage mess = (ReturnMessage)result.GetReplyMessage();

            HttpSessionState session = (HttpSessionState)ar.AsyncState;

            ICollection<KeyValuePair<int, bool>> _FoldersCanView = (List<KeyValuePair<int, bool>>)
                                                                        mess.ReturnValue;
            if (session != null)
            {
                User currUser = (User)session[Configurations.Generics.BackAccountSessionKey];
                if (currUser != null)
                {
                    if (session["FoldersCanViewUSER_" + currUser.ID] != null)
                    {
                        lock (session["FoldersCanViewUSER_" + currUser.ID])
                        {
                            session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                        }
                    }
                    else
                        session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                }
            }
        }

        private ICollection<KeyValuePair<int, bool>> CollectGrants(User _user)
        {
            ICollection<KeyValuePair<int, bool>> _FoldersCanView = new List<KeyValuePair<int, bool>>();
            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                User user = UsersBusinessLogic.GetById(_user.ID, sess);
                var networks = user.AssociatedNetworks;
                foreach (var net in networks)
                {
                    var folders = FolderBusinessLogic.FindNetworkFoldersForGrantsCollection
                        (net.Network.ID, sess);
                    foreach (var folder in folders)
                    {
                        GrantsFinder Criteria = new FolderGrants(folder, user.Profile, sess);
                        _FoldersCanView.Add(new KeyValuePair<int, bool>
                            (folder.ID, Criteria.NeedToShow("show").Allowed));
                    }
                }
            }
            return _FoldersCanView;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            string previousPage = System.Web.HttpContext.Current.Session["PreviousPage"] as string;

            this.Controls.Add(new LiteralControl(Labels[LoginLabels.LabelsList.WelcomeSSO]));

            if (!NetCms.Users.AccountManager.Logged)
            {
                this.Controls.Add(new LiteralControl(Labels[LoginLabels.LabelsList.UnauthenticatedUser]));
               
                // check AD mode is enable

                if (NetCms.Configurations.Generics.LoginMode == Configurations.Generics.LoginModeType.Standard)
                {
                    NetCms.Diagnostics.Diagnostics.Redirect(previousPage);
                }
                else
                {
                    System.Security.Principal.WindowsIdentity wiUserInfo = System.Web.HttpContext.Current.Request.LogonUserIdentity;
                    string userName = wiUserInfo.Name;

                    // check user logged in AD                    
                    if (userName != "NT AUTHORITY\\IUSR" && userName.Contains(NetCms.Configurations.Generics.ActiveDirectoryDomain + "\\"))
                    {
                        // recuper lo username usato in AD
                        string AD_UserName = userName.Replace(NetCms.Configurations.Generics.ActiveDirectoryDomain + "\\", "");

                        // login user on cms
                        User currentUser = NetCms.Users.UsersBusinessLogic.GetActiveUserByUserName(AD_UserName);
                        if (currentUser != null)
                        {
                            NetCms.Users.LoginResult result = NetCms.Users.AccountManager.TryLoginUser(currentUser, "");
                            if (result.Result == LoginResult.LoginResults.Logged)
                            {
                                HttpSessionState webSession = HttpContext.Current.Session;

                                NetCms.Users.LoginControl.CollectGrantsDelegate myAction = new NetCms.Users.LoginControl.CollectGrantsDelegate(CollectGrants);
                                myAction.BeginInvoke(currentUser, new AsyncCallback(CallbackMethod), webSession);

                                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente Active Directory '" + currentUser + "' ha effettuato il login.");
                                var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();

                                if (previousPage.Length == 0)
                                    previousPage = "/";

                                NetCms.Diagnostics.Diagnostics.Redirect(httpsHandler.ConvertPageAddressAsNonSecure(previousPage));
                            }
                        }
                        else // utente non presente in db
                        {
                            // add new user
                            currentUser = ADBusinessLogic.AddNewUser(AD_UserName);
                            if (currentUser == null)
                            {
                                this.Controls.Add(new LiteralControl(Labels[LoginLabels.LabelsList.UserNotFoundAD]));
                                // notify admin user new user 
                                // inviare notifica al gruppo di amministrazione per l'eventuale ulteriore profilazione
                            }
                            else
                            {
                                // eseguo l'autenticazione
                                NetCms.Users.LoginResult result = NetCms.Users.AccountManager.TryLoginUser(currentUser, "");
                                if (result.Result == LoginResult.LoginResults.Logged)
                                {
                                    HttpSessionState webSession = HttpContext.Current.Session;

                                    NetCms.Users.LoginControl.CollectGrantsDelegate myAction = new NetCms.Users.LoginControl.CollectGrantsDelegate(CollectGrants);
                                    myAction.BeginInvoke(currentUser, new AsyncCallback(CallbackMethod), webSession);

                                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente Active Directory '" + currentUser + "' ha effettuato il login.");
                                    var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();

                                    if (previousPage.Length == 0)
                                        previousPage = "/";

                                    NetCms.Diagnostics.Diagnostics.Redirect(httpsHandler.ConvertPageAddressAsNonSecure(previousPage));
                                }
                            }
                        }
                    }
                }
            }
        }

        public static LabelsManager.Labels Labels
        {
            get
            {
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(LoginLabels)) as LabelsManager.Labels;
            }
        }
    }
}
