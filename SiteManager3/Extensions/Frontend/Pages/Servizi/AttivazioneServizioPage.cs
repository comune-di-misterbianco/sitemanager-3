﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.Front.Static;
using System.Web;
using NetCms.Vertical;
using System.Linq;
using NetService.Utility.ValidatedFields;
using NHibernate;
using NetCms.DBSessionProvider;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Users.Front
{
    [FrontendStaticPage(new[] { "/user/activate_service.aspx", "/en/user/service_activation.aspx" }, new[] { "Attivazione Servizio", "Service Activation" }, NetCms.Front.FrontLayouts.All, OnlyLoggedUsers = true)]
    public class AttivazioneServizioPage : WebControl
    {
        private NetFrontend.PageData PageData;
        private User _Account;
        public User Account
        {
            get
            {
                if (_Account == null && NetCms.Users.AccountManager.Logged)
                    _Account = NetCms.Users.AccountManager.CurrentAccount;
                return _Account;
            }
        }

        public VerticalApplicationWithService AppWithService
        {
            get
            {
                if (_AppWithService == null)
                {
                    G2Core.Common.RequestVariable vappReq = new G2Core.Common.RequestVariable("vapp", G2Core.Common.RequestVariable.RequestType.QueryString);
                    if (vappReq.IsValidInteger)
                    {
                        int idVapp = vappReq.IntValue;
                        if (Account.CustomAnagrafe.Applicazioni.Select(x => x.IdApplicazione).Contains(idVapp))
                        {
                            VerticalApplication vapp = VerticalApplicationsPool.GetByID(idVapp);
                            _AppWithService = (VerticalApplicationWithService)vapp;
                        }
                    }
                }
                return _AppWithService;
            }
        }
        private VerticalApplicationWithService _AppWithService;

        public AttivazioneServizioPage(NetFrontend.PageData pageData)
            : base(HtmlTextWriterTag.Div)
        {
            PageData = pageData;
            FormAttivazione.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        public VfManager FormAttivazione
        {
            get
            {
                if (_FormAttivazione == null)
                {
                    switch (AppWithService.Servizio.TipoServizio)
                    {
                        case Servizio.TipiServizi.Automatico:
                            { 
                                break; 
                            }
                        case Servizio.TipiServizi.Semplice:
                            {
                                // semplice form con pulsante attiva
                                _FormAttivazione = new VfManager("activate");
                                _FormAttivazione.ShowMandatoryInfo = false;
                                _FormAttivazione.TitleControl.Controls.Add(new LiteralControl(AppWithService.Servizio.Nome));
                                _FormAttivazione.InfoControl.Controls.Add(new LiteralControl(AppWithService.Servizio.Descrizione));
                                _FormAttivazione.SubmitButton.Text = Labels[UserActivateLabels.LabelList.Activation];
                                _FormAttivazione.CssClass = "Axf";
                                break;
                            }
                        case Servizio.TipiServizi.Differenziato:
                            {
                                // la form specifica del servizio
                                _FormAttivazione = (AppWithService.Servizio as ServizioConDatiAggiuntivi).FormAttivazione;
                                break;
                            }
                        case Servizio.TipiServizi.Forte:
                            {
                                // una form semplice con le istruzioni per l'attivazione e senza pulsante
                                ServizioForte serv = AppWithService.Servizio as ServizioForte;
                                _FormAttivazione = new VfManager("activate");
                                _FormAttivazione.TitleControl.Controls.Add(new LiteralControl(serv.Nome));

                                WebControl descrizioneP = new WebControl(HtmlTextWriterTag.P);
                                descrizioneP.Controls.Add(new LiteralControl(serv.Descrizione));

                                WebControl istruzioniP = new WebControl(HtmlTextWriterTag.P);
                                istruzioniP.Controls.Add(new LiteralControl(serv.IstruzioniAttivazione));

                                _FormAttivazione.InfoControl.Controls.Add(descrizioneP);
                                _FormAttivazione.InfoControl.Controls.Add(istruzioniP);
                                _FormAttivazione.CssClass = "Axf";
                                _FormAttivazione.AddSubmitButton = false;
                                _FormAttivazione.ShowMandatoryInfo = false;
                                if (serv.AutomaticStrongService)
                                {
                                    _FormAttivazione.InfoControl.Controls.Add(AutomaticStrongService());
                                    _FormAttivazione.AddSubmitButton = true;
                                    _FormAttivazione.SubmitButton.Text = Labels[UserActivateLabels.LabelList.CheckCitizenship];
                                }
                                break;
                            }
                    }
                }
                return _FormAttivazione;
            }
        }
        private VfManager _FormAttivazione;

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (FormAttivazione.IsValid == VfGeneric.ValidationStates.Valid && AppWithService.Servizio.TipoServizio != Servizio.TipiServizi.Forte)
            {
                ActivateService();
            }
            if (AppWithService.Servizio.TipoServizio == Servizio.TipiServizi.Forte)
            {
                ServizioForte serv = AppWithService.Servizio as ServizioForte;
                if (serv.AutomaticStrongService)
                {
                    ActivateStrongService();

                }
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (NetCms.Users.AccountManager.Logged)
            {
                if (AppWithService != null)
                {
                    if (!AppWithService.Servizio.UtenteAttivo(Account))
                    {
                        if (AppWithService.Servizio.TipoServizio == Servizio.TipiServizi.Automatico)
                        {
                            // non richede interazione
                            ActivateService();
                        }
                        else
                            this.Controls.Add(FormAttivazione);
                    }
                    else this.Controls.Add(new LiteralControl(Labels[UserActivateLabels.LabelList.AlreadyActive]));
                }
                else this.Controls.Add(new LiteralControl(Labels[UserActivateLabels.LabelList.UnableActivate]));
            }
            else
            {
                new NetCms.Users.LoginControl();
            }
        }

        private void ActivateService()
        {
            using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
                try
                {
                    
                    User user = UsersBusinessLogic.GetById(Account.ID, session);
                    if (AppWithService.Servizio.TipoServizio == Servizio.TipiServizi.Forte)
                    {
                        ServizioForte serv = AppWithService.Servizio as ServizioForte;
                        if (serv.AutomaticStrongService)
                        {

                            AppWithService.Servizio.EseguiConVerificaCittadino(user, true, session, FormAttivazione);
                        }
                    }
                    else
                        AppWithService.Servizio.Esegui(user, true, session, FormAttivazione);
                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
                finally
                {
                    if (AppWithService.Servizio.AttivazioneDifferita)
                        NetCms.Diagnostics.Diagnostics.Redirect(System.Web.HttpContext.Current.Request.UrlReferrer.ToString());
                    else NetCms.Diagnostics.Diagnostics.Redirect(AppWithService.FrontendURL + "/default.aspx");
                }
        }

        
        private void ActivateStrongService()
        {
            User us = UsersBusinessLogic.GetById(Account.ID);
            bool ok = false;
            bool anagr = false;
            string msg = "";
            string codFiscale = "";
            foreach (CustomAnagrafe.CustomField field in us.CustomAnagrafe.CustomFields)
            {
                if (field.Name == "Codice Fiscale")
                {
                    codFiscale = us.AnagraficaMapped["Codice Fiscale"].ToString();
                    anagr = true;
                }   
            
            }
                        
            this.Controls.Add(new LiteralControl(msg));
        }
        private WebControl AutomaticStrongService()
        {
            string message = Labels[UserActivateLabels.LabelList.AutomaticActivation];
            WebControl control = new WebControl(HtmlTextWriterTag.Div);
            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            legend.Controls.Add(new LiteralControl(message));
            control.Controls.Add(legend);

            return control;
            
        
        }

        public static LabelsManager.Labels Labels
        {
            get
            {
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(UserActivateLabels)) as LabelsManager.Labels;
            }
        }

    }
}