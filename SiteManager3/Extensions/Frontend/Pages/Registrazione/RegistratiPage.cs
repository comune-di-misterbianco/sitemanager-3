﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.Front.Static;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using NetService.Utilities;
using NetCms.Users.CustomAnagrafe;
using NHibernate;
using NetCms.DBSessionProvider;
using Messaggistica;
using Messaggistica.Model;

namespace NetCms.Users.Front.Registrazione
{
    [FrontendStaticPage(new[] { "/registrazione/registrati.aspx", "/en/registration/signin.aspx" }, new[] { "Registrazione al portale", "Portal registration" }, NetCms.Front.FrontLayouts.None)]
    public class RegistratiPage : WebControl
    {
        public static LabelsManager.Labels Labels
        {
            get
            {
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(RegistrazioneLabels)) as LabelsManager.Labels;
            }
        }

        public RegistratiPage()
            : base(HtmlTextWriterTag.Div)
        {
            this.RegistrazioneForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);           
        }

        private CustomAnagrafe.CustomAnagrafe _RequestedAnagrafica;
        private CustomAnagrafe.CustomAnagrafe RequestedAnagrafica
        {
            get
            {
                if (_RequestedAnagrafica == null)
                {
                    G2Core.Common.RequestVariable req = new G2Core.Common.RequestVariable("ca", G2Core.Common.RequestVariable.RequestType.QueryString);
                    if (req.IsValidString)
                        _RequestedAnagrafica = CustomAnagrafe.CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(req.StringValue);
                }

                return _RequestedAnagrafica;
            }
        }

        public RegistrazioneForm RegistrazioneForm
        {
            get
            {
                if (_RegistrazioneForm == null)
                {
                    _RegistrazioneForm = new RegistrazioneForm("userRegister", RequestedAnagrafica,RichiesteDiRegistrazioneGestite);                  
                }
                return _RegistrazioneForm;
            }
        }
        private RegistrazioneForm _RegistrazioneForm;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.CssClass = "row registratipage";

            WebControl colPage = new WebControl(HtmlTextWriterTag.Div);
            colPage.CssClass = "col-md-12";

            WebControl phblock = new WebControl(HtmlTextWriterTag.Div);
            phblock.CssClass = "page-header";

            WebControl title = new WebControl(HtmlTextWriterTag.H1);
            title.ID = "userRegisterTitle";
            title.Controls.Add(new LiteralControl(Labels[RegistrazioneLabels.RegistrazioneLabelsList.UserRegisterTitle]));

            phblock.Controls.Add(title);

            colPage.Controls.Add(phblock);


            if (RequestedAnagrafica != null )
            {                
                if (!RequestedAnagrafica.VisibileInFrontend)
                    throw new System.Web.HttpException(404, "File Not Found");
                else
                colPage.Controls.Add(RegistrazioneForm);
            }
            else
            {
                WebControl errorcontrol = new WebControl(HtmlTextWriterTag.Div);
                errorcontrol.CssClass = "validation-result";
                     
                WebControl alertError = new WebControl(HtmlTextWriterTag.Div);
                alertError.CssClass = "alert alert-danger";
                alertError.Attributes.Add("role", "alert");
                alertError.Controls.Add(new LiteralControl(Labels[RegistrazioneLabels.RegistrazioneLabelsList.CustomAnagrafeNotFoundError]));

                errorcontrol.Controls.Add(alertError);
                colPage.Controls.Add(errorcontrol);
            }

            this.Controls.Add(colPage);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (RequestedAnagrafica != null)
            {
                if (UtenteInseritoConSuccesso)
                {
                    Control ctrRegistrazioneTitle = this.FindControl("userRegisterTitle");
                    if (ctrRegistrazioneTitle != null)
                        ctrRegistrazioneTitle.Visible = false;

                    Control ctrRegistrazione = this.FindControl(RegistrazioneForm.ID);
                    if (ctrRegistrazione != null)
                        ctrRegistrazione.Visible = false;
                }  
            }
           
        }

        public bool UtenteInseritoConSuccesso
        {
            get { return _UtenteInseritoConSuccesso; }
            set { _UtenteInseritoConSuccesso = value; }
        }
        private bool _UtenteInseritoConSuccesso;

        public bool RichiesteDiRegistrazioneGestite
        {
            get 
            {
                return System.Web.Configuration.WebConfigurationManager.AppSettings["ManageRegRequests"] == "1";
            }
        }

        public bool IndirizzoEmailUnivoco
        {
            get
            {
                return System.Web.Configuration.WebConfigurationManager.AppSettings["UniqueMailAddress"] == "1";
            }
        }

        public bool AttivazioneViaSMS
        {
            get
            {
                bool cellulareValid = false;
                CustomAnagrafe.CustomField cellulareField = RequestedAnagrafica.CustomFields.ToList().Find(x => x.Name.ToLower() == "cellulare");
                if (cellulareField != null)
                {
                    cellulareValid = cellulareField.Required;
                }
                return System.Web.Configuration.WebConfigurationManager.AppSettings["ActivationViaSMS"] == "1" && cellulareValid;
            }
        }

        public static ResultEnhanced SendSMS(IList<string> destinatariSms, string testoSms, string stato, ISession innerSession = null)
        {
            Canali canalesms = Messaggistica.BusinessLogic.GetCanaleSMS(innerSession);

            IList<string> destinatari = destinatariSms;
            if (destinatari != null && destinatari.Count > 0)
            {
                string messaggio = "";

                messaggio = testoSms;

                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                ResultEnhanced result = Messaggistica.BusinessLogic.SendMessageWithGenericProviders(canalesms, messaggio, destinatari, dictionary, innerSession);
                return result;
            }
            return new ResultEnhanced(new Result(Labels[RegistrazioneLabels.RegistrazioneLabelsList.NoRecipientForSms]));
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if ((this.RegistrazioneForm.UserForm.IsValid == NetService.Utility.ValidatedFields.VfGeneric.ValidationStates.Valid || RichiesteDiRegistrazioneGestite) &&
                this.RegistrazioneForm.AnagraficaForm.IsValid == NetService.Utility.ValidatedFields.VfGeneric.ValidationStates.Valid)
            {
                bool codiceFiscaleIsValid = true;
                #region Codice che effettua il controllo del codice fiscale se presente e se sono presenti anche i campi relativi
                
                string codiceFiscale = "";
                if (this.RegistrazioneForm.AnagraficaForm.Fields.Count(x => x.Key.ToLower() == RequestedAnagrafica.Nome.ToLower() + "_codice_fiscale") > 0)
                    codiceFiscale = this.RegistrazioneForm.AnagraficaForm.Fields.Find(x => x.Key.ToLower() == RequestedAnagrafica.Nome.ToLower() + "_codice_fiscale").PostBackValue;

                //Se il codice fiscale è presente, passo a controllare l'esistenza degli altri campi necessari
                if (codiceFiscale.Length > 0)
                {

                    string nome = "";
                    if (this.RegistrazioneForm.AnagraficaForm.Fields.Count(x => x.Key.ToLower() == RequestedAnagrafica.Nome.ToLower() + "_nome") > 0)
                        nome = this.RegistrazioneForm.AnagraficaForm.Fields.Find(x => x.Key.ToLower() == RequestedAnagrafica.Nome.ToLower() + "_nome").PostBackValue;

                    string cognome = "";
                    if (this.RegistrazioneForm.AnagraficaForm.Fields.Count(x => x.Key.ToLower() == RequestedAnagrafica.Nome.ToLower() + "_cognome") > 0)
                        cognome = this.RegistrazioneForm.AnagraficaForm.Fields.Find(x => x.Key.ToLower() == RequestedAnagrafica.Nome.ToLower() + "_cognome").PostBackValue;

                    string datanascita = "";
                    if (this.RegistrazioneForm.AnagraficaForm.Fields.Count(x => x.Key.ToLower() == RequestedAnagrafica.Nome.ToLower() + "_data_di_nascita") > 0)
                        datanascita = this.RegistrazioneForm.AnagraficaForm.Fields.Find(x => x.Key.ToLower() == RequestedAnagrafica.Nome.ToLower() + "_data_di_nascita").PostBackValue;

                    string sesso = "";
                    if (this.RegistrazioneForm.AnagraficaForm.Fields.Count(x => x.Key.ToLower() == RequestedAnagrafica.Nome.ToLower() + "_sesso") > 0)
                        sesso = this.RegistrazioneForm.AnagraficaForm.Fields.Find(x => x.Key.ToLower() == RequestedAnagrafica.Nome.ToLower() + "_sesso").PostBackValue;

                    string comune = "";
                    if (this.RegistrazioneForm.AnagraficaForm.Fields.Count(x => x.Key.ToLower() == RequestedAnagrafica.Nome.ToLower() + "_comune_di_nascita") > 0)
                        comune = this.RegistrazioneForm.AnagraficaForm.Fields.Find(x => x.Key.ToLower() == RequestedAnagrafica.Nome.ToLower() + "_comune_di_nascita").PostBackValue;

                    //Se sono presenti nome, cognome, data di nascita e comune, il controllo può avvenire
                    if (nome.Length > 0 && cognome.Length > 0 && datanascita.Length > 0 && comune.Length > 0)
                    {
                        //Se l'anagrafica contiene anche il sesso, faccio il calcolo diretto
                        if (sesso.Length > 0)
                        {
                            CodiceFiscaleGenerator CodeGenerator = new CodiceFiscaleGenerator(nome,
                                                                                        cognome,
                                                                                        DateTime.Parse(datanascita),
                                                                                        sesso == "M" ? true : false,
                                                                                        CustomAnagrafeBusinessLogic.GetComuneById(int.Parse(comune)).CodiceCatastale);

                            if (!CodeGenerator.CheckCodiceFiscale(codiceFiscale))
                            {
                                codiceFiscaleIsValid = codiceFiscaleIsValid & false;
                            }
                        }
                        else
                        {
                            //Se l'anagrafica non contiene il sesso, considero entrambi i casi ed effettuo la verifica
                            CodiceFiscaleGenerator MaleCodeGenerator = new CodiceFiscaleGenerator(nome,
                                                                                cognome,
                                                                                DateTime.Parse(datanascita),
                                                                                true,
                                                                                CustomAnagrafeBusinessLogic.GetComuneById(int.Parse(comune)).CodiceCatastale);

                            CodiceFiscaleGenerator FemaleCodeGenerator = new CodiceFiscaleGenerator(nome,
                                                                                            cognome,
                                                                                            DateTime.Parse(datanascita),
                                                                                            false,
                                                                                            CustomAnagrafeBusinessLogic.GetComuneById(int.Parse(comune)).CodiceCatastale);

                            if (!MaleCodeGenerator.CheckCodiceFiscale(codiceFiscale) && !FemaleCodeGenerator.CheckCodiceFiscale(codiceFiscale))
                            {
                                codiceFiscaleIsValid = codiceFiscaleIsValid & false;
                            }
                        }
                    }
                }
                #endregion

                //Questo valore sarà true se il codice fiscale è valido, oppure se l'anagrafica non contiene dei campi adatti ad effettuare il controllo
                //che quindi sarà bypassato.
                if (codiceFiscaleIsValid)
                {

                    string emailInserita = this.RegistrazioneForm.AnagraficaForm.Fields.Find(x => x.Key == "Email").PostBackValue;
                    //codice prima della mia modifica
                    //CustomAnagrafe.CustomField usernameField = RequestedAnagrafica.CustomFields.First(x=>x.IsUsername);
                    string userNameUtenteInserito = "";
                    CustomAnagrafe.CustomField usernameField = RequestedAnagrafica.CustomFields.ToList().Find(x => x.IsUsername);

                    if (usernameField != null)
                    {
                        userNameUtenteInserito = this.RegistrazioneForm.AnagraficaForm.Fields.Find(x => x.Key == RequestedAnagrafica.DatabaseName + "_" + usernameField.Name.Replace(' ', '_')).PostBackValue;
                    }
                    else
                    {
                        if (RequestedAnagrafica.EmailAsUserName)
                            userNameUtenteInserito = emailInserita;
                    }

                    string password = "";
                    if (!RichiesteDiRegistrazioneGestite)
                        password = this.RegistrazioneForm.UserForm.Fields.Find(x => x.Key == "Password").PostBackValue;
                    else
                    {
                        G2Core.Common.PasswordGenerator gen = new G2Core.Common.PasswordGenerator(8);
                        password = gen.Generate();
                    }


                    string hashedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();
                    string anagraficaType = "Anagrafica" + RequestedAnagrafica.Nome;

                    User utenteRegistrante = UsersBusinessLogic.GetUserByUserName(userNameUtenteInserito,0); //verifica presenza utente e se presente verifica se non eliminato

                    if (utenteRegistrante == null ) // verifica stato utente se presente o marcato come eliminato  || utenteRegistrante.Deleted
                    {
                        if (!IndirizzoEmailUnivoco || !UsersBusinessLogic.CheckEmailIsUsed(emailInserita)) //|| UsersBusinessLogic.GetUserByEmail(emailInserita) == null
                        {
                            User utenteCreato = UsersBusinessLogic.CreateUser(userNameUtenteInserito, hashedPassword, this.RegistrazioneForm.AnagraficaForm, anagraficaType, RequestedAnagrafica.DefaultGroup.ID, true, new Random().Next(9999999).ToString(), CurrentNetworkPath);
                            if (utenteCreato != null)
                            {
                                UtenteInseritoConSuccesso = true;
                                // vuol dire che non vengono gestite le richieste di registrazione
                                if (utenteCreato.Registrato)
                                {
                                    // current url  
                                    Uri url = System.Web.HttpContext.Current.Request.Url;
                                    string baseUrl = url.GetLeftPart(UriPartial.Authority);

                                    string activateURl = baseUrl + NetCms.Configurations.Generics.ActivationUrl;

                                    UsersBusinessLogic.SendConfirmMail(utenteCreato, password, activateURl, AttivazioneViaSMS);
                                  
                                    bool activationSent = true;
                                    if (AttivazioneViaSMS)
                                    {
                                        using (ISession sess = FluentSessionProvider.Instance.OpenInnerSession())
                                        using (ITransaction tx = sess.BeginTransaction())
                                        {
                                            try
                                            {
                                                IList<string> destinatario = new List<string>();

                                                string cellulare = this.RegistrazioneForm.AnagraficaForm.Fields.Find(x => x.Key.ToLower() == RequestedAnagrafica.Nome.ToLower() + "_cellulare").PostBackValue;
                                                if (activationSent = (cellulare.Length > 0))
                                                {
                                                    destinatario.Add(cellulare);
                                                    string MsgBody = Labels[RegistrazioneLabels.RegistrazioneLabelsList.SmsActivationText];

                                                    MsgBody = MsgBody.Replace("{USERNAME}", utenteCreato.UserName);
                                                    MsgBody = MsgBody.Replace("{PORTAL_NAME}", NetCms.Configurations.PortalData.Nome);
                                                    ResultEnhanced result = SendSMS(destinatario, MsgBody + utenteCreato.ActivationKey, Labels[RegistrazioneLabels.RegistrazioneLabelsList.SmsActivationTitle], sess);
                                                    activationSent &= result.Result.IsOk;

                                                    if (result.Result.IsOk)
                                                        tx.Commit();
                                                    else
                                                    {
                                                        tx.Rollback();
                                                    }
                                                }

                                            }
                                            catch (Exception ex)
                                            {
                                                tx.Rollback();
                                            }

                                        }
                                    }
                                    if (activationSent)
                                        NetFrontend.NotifyBox.AddMessage(Labels[RegistrazioneLabels.RegistrazioneLabelsList.RegistrationSuccess], NetFrontend.PostBackMessagesType.success);
                                    else
                                        NetFrontend.NotifyBox.AddMessage(Labels[RegistrazioneLabels.RegistrazioneLabelsList.RegistrationError], NetFrontend.PostBackMessagesType.warning);
                                }
                                // la registrazione è al vaglio di un operatore
                                else
                                {
                                    utenteCreato.SendRegistrationRequestMail();

                                    #region Codice per l'invio delle notifiche agli operatori associati

                                    using (ISession sess = FluentSessionProvider.Instance.OpenInnerSession())
                                    using (ITransaction tx = sess.BeginTransaction())
                                    {
                                        User creato = UsersBusinessLogic.GetById(utenteCreato.ID, sess);
                                        try
                                        {

                                            string titolo = Labels[RegistrazioneLabels.RegistrazioneLabelsList.ManagerNotifyTitle];
                                            titolo = titolo.Replace("{NOME_COMPLETO}", creato.NomeCompleto);

                                            string testo = Labels[RegistrazioneLabels.RegistrazioneLabelsList.ManagerNotifyText];
                                            testo = testo.Replace("{USERNAME}", creato.UserName);

                                            string link = "/applications/users/user_details.aspx?id=" + creato.ID;

                                            User administrator = UsersBusinessLogic.GetUserByUserName("Administrator", sess);
                                            bool containsAdmin = false;

                                            string eMailTo = "";

                                            CustomAnagrafe.CustomAnagrafe anagrafica = CustomAnagrafeBusinessLogic.GetCustomAnagrafeById(RequestedAnagrafica.ID, sess);

                                            if (anagrafica.RegRequestNotifyGroup != null)
                                            {
                                                string eMailCCToSend = "";
                                                //Invio notifica a tutti gli utenti del gruppo gestore delle richieste di registrazione per l'anagrafica scelta
                                                foreach (User Gestore in anagrafica.RegRequestNotifyGroup.Users)
                                                {
                                                    eMailCCToSend += Gestore.Anagrafica.Email + "; ";

                                                    NetCms.Users.NotificaBackoffice.CreateNewNotificaBackoffice(titolo, testo, NotificaBackoffice.NotificaBackofficeType.Notifica, Gestore.Profile.ID, creato.Profile.ID, link, NetCms.Networks.NetworksManager.CurrentActiveNetwork.ID, sess);

                                                }
                                                //Se eMailCCToSend.Length > 0 vuol dire che sono state inviate notifiche, che ci sono utenti nel gruppo e che è necessario inviare le e-Mail
                                                if (eMailCCToSend.Length > 0)
                                                {
                                                    //Verifica che sia inclusa o meno la mail dell'amministratore di sistema dopo aver verificato che l'utente amministratore esista
                                                    containsAdmin = administrator != null && anagrafica.RegRequestNotifyGroup.Users.Count(x => x.Anagrafica.Email == administrator.Anagrafica.Email) > 0;
                                                    eMailTo = eMailCCToSend.Split(';')[0].Trim();
                                                    eMailCCToSend = eMailCCToSend.Replace(eMailTo + "; ", "");
                                                    if (eMailCCToSend.Length > 0)
                                                        eMailCCToSend = eMailCCToSend.Remove(eMailCCToSend.Length - 2, 2);
                                                    UsersBusinessLogic.SendNotifyRegistrationRequestMail(eMailTo, eMailCCToSend.Length > 0 ? eMailCCToSend : null, creato.NomeCompleto, creato.UserName);
                                                }
                                            }

                                            //Se esiste l'amministratore e non gli è ancora stata inviata la notifica, procedo ad inviarla
                                            if (administrator != null)
                                            {
                                                if (!containsAdmin)
                                                {
                                                    eMailTo = administrator.Anagrafica.Email;
                                                    NetCms.Users.NotificaBackoffice.CreateNewNotificaBackoffice(titolo, testo, NotificaBackoffice.NotificaBackofficeType.Notifica, administrator.Profile.ID, creato.Profile.ID, link, NetCms.Networks.NetworksManager.CurrentActiveNetwork.ID, sess);
                                                    UsersBusinessLogic.SendNotifyRegistrationRequestMail(eMailTo, null, creato.NomeCompleto, creato.UserName);
                                                }
                                            }

                                            tx.Commit();

                                            NetFrontend.NotifyBox.AddMessage(Labels[RegistrazioneLabels.RegistrazioneLabelsList.RegistrationSuccessMsg], NetFrontend.PostBackMessagesType.success);

                                        }
                                        catch (Exception ex)
                                        {
                                            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore nell'invio delle notifiche agli utenti: " + ex.Message);

                                            tx.Rollback();

                                        }

                                    }
                                    #endregion

                                }
                            }
                            else
                            {
                                string error = Labels[RegistrazioneLabels.RegistrazioneLabelsList.RegistrationErrorMsg];
                                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, error);
                                NetFrontend.NotifyBox.AddMessage(error, NetFrontend.PostBackMessagesType.error);
                            }
                        }                       
                        else
                        {
                            NetFrontend.NotifyBox.AddMessage(Labels[RegistrazioneLabels.RegistrazioneLabelsList.EmailAlreadyExists], NetFrontend.PostBackMessagesType.notify);
                        }
                    }
                    else
                    {
                        string Msg = Labels[RegistrazioneLabels.RegistrazioneLabelsList.UsernameIsUsed];
                        Msg = Msg.Replace("{USERNAME}", userNameUtenteInserito);
                        NetFrontend.NotifyBox.AddMessage(Msg, NetFrontend.PostBackMessagesType.notify);
                    }
                }
                else
                {
                    NetFrontend.NotifyBox.AddMessage(Labels[RegistrazioneLabels.RegistrazioneLabelsList.CFNotMatchWithInputData], NetFrontend.PostBackMessagesType.error);
                }
            }
        }

        

        public string CurrentNetworkPath
        {
            get
            {
                return NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;
            }
        }
    }
}