﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using NetService.Utility.ValidatedFields;
using NetCms.Users.CustomAnagrafe;
using G2Core.Common;
using System.Web.UI;

namespace NetCms.Users.Front.Registrazione
{
    public class RegistrazioneForm : WebControl
    {
        public VfManager UserForm
        {
            get
            {
                if (_UserForm == null)
                {
                    _UserForm = new VfManager(this.ID + "_user",true) { AddSubmitButton = false };
                    _UserForm.IsPostBack = FormIsPostBack;

                    _UserForm.ShowMandatoryInfo = false; // nascondo il msg campi obbligatori

                    //VfTextBox nomeUtente = new VfTextBox("UserName", "Nome Utente", "UserName", InputTypes.Text);
                    //nomeUtente.MaxLenght = 60;
                    //nomeUtente.Required = true;
                    //_UserForm.Fields.Add(nomeUtente);

                    VfPassword password = new VfPassword("Password");                    
                    password.Required = true;
                    password.UseBootstrapItalia = ((CmsConfigs.Model.GlobalConfig.CurrentConfig.CurrentTheme.ParentTheme != null && CmsConfigs.Model.GlobalConfig.CurrentConfig.CurrentTheme.ParentTheme.Name.ToLower() == "Agid2019".ToLower())
                        || CmsConfigs.Model.GlobalConfig.CurrentConfig.CurrentTheme.Name.ToLower() == "Agid2019".ToLower());
                    _UserForm.Fields.Add(password);

                }
                return _UserForm;
            }
        }
        private VfManager _UserForm;

        private static LabelsManager.Labels Labels
        {
            get
            {
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(RegistrazioneLabels)) as LabelsManager.Labels;
            }
        }

        public AnagraficaVfManager AnagraficaForm
        {
            get
            {
                if (_AnagraficaForm == null)
                {
                    _AnagraficaForm = new AnagraficaVfManager(this.ID + "_anagrafica", FormIsPostBack, AnagraficaCustom, false, null, true);
                    _AnagraficaForm.UseBootstrapItalia = ((CmsConfigs.Model.GlobalConfig.CurrentConfig.CurrentTheme.ParentTheme != null && CmsConfigs.Model.GlobalConfig.CurrentConfig.CurrentTheme.ParentTheme.Name.ToLower() == "Agid2019".ToLower())
                        || CmsConfigs.Model.GlobalConfig.CurrentConfig.CurrentTheme.Name.ToLower() == "Agid2019".ToLower());
                }
                return _AnagraficaForm;
            }
        }
        private AnagraficaVfManager _AnagraficaForm;

        public Button SubmitButton
        {
            get
            {
                if (_SubmitButton == null)
                {
                    _SubmitButton = new Button();
                    _SubmitButton.ID = this.ID + "_submit";
                    _SubmitButton.CssClass = "btn btn-primary";
                    _SubmitButton.Text = Labels[RegistrazioneLabels.RegistrazioneLabelsList.Registration];
                }
                return _SubmitButton;
            }
        }
        private Button _SubmitButton;

        public bool FormIsPostBack { get; private set; }

        public CustomAnagrafe.CustomAnagrafe AnagraficaCustom
        {
            get;
            private set;
        }        

        public RegistrazioneForm(string controlID, CustomAnagrafe.CustomAnagrafe anagraficaCustom, bool RegRequestsManaged)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = controlID;
            RequestVariable buttonPostback = new RequestVariable(SubmitButton.ID);
            FormIsPostBack = (buttonPostback.IsPostBack && buttonPostback.StringValue == SubmitButton.Text);
            AnagraficaCustom = anagraficaCustom;
            _RegRequestsON = RegRequestsManaged;            
        }

        private bool _RegRequestsON;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.CssClass = "row";

            WebControl colPage = new WebControl(HtmlTextWriterTag.Div);
            colPage.CssClass = "col-md-12";

            #region Anagrafe
            WebControl rowAnagrafe = new WebControl(HtmlTextWriterTag.Div);
            rowAnagrafe.CssClass = "row";

            WebControl colAnagrafe = new WebControl(HtmlTextWriterTag.Div);
            colAnagrafe.CssClass = "col-md-12";

            colAnagrafe.Controls.Add(AnagraficaForm);

            rowAnagrafe.Controls.Add(colAnagrafe);

            colPage.Controls.Add(rowAnagrafe);

            #endregion

            #region Login

            if (!_RegRequestsON)
            {
                WebControl panel = new WebControl(HtmlTextWriterTag.Div);
                panel.CssClass = "panel panel-default";

                WebControl panelBody = new WebControl(HtmlTextWriterTag.Div);
                panelBody.CssClass = "panel-body";

                panelBody.Controls.Add(UserForm);
                panel.Controls.Add(panelBody);


                colPage.Controls.Add(panel);

            }

            #endregion
          
            #region Button
            WebControl rowButton = new WebControl(HtmlTextWriterTag.Div);
            rowButton.CssClass = "row";

            WebControl colButton = new WebControl(HtmlTextWriterTag.Div);
            colButton.CssClass = "col-md-12";

            WebControl submitButton_Container = new WebControl(HtmlTextWriterTag.Div);
            submitButton_Container.CssClass = "vf submit-vf text-center mb-4";
            submitButton_Container.Controls.Add(SubmitButton);

            colButton.Controls.Add(submitButton_Container);

            rowButton.Controls.Add(colButton);
            
            colPage.Controls.Add(rowButton);
            #endregion
            
            this.Controls.Add(colPage);
        }
    }
}