﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.Front.Static;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Users.Front.Registrazione
{
    [FrontendStaticPage(new[] { "/registrazione/scelta.aspx", "/en/registration/selection.aspx" }, new[] { "Scelta tipologia di registrazione", "Choice of registration type" }, NetCms.Front.FrontLayouts.None)]
    public class SceltaRegistrazionePage : WebControl
    {
        public SceltaRegistrazionePage()
            : base(HtmlTextWriterTag.Div)
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.CssClass = "row elenco_registrazioni";

            WebControl colPage = new WebControl(HtmlTextWriterTag.Div);
            colPage.CssClass = "col-md-12";

            WebControl phblock = new WebControl(HtmlTextWriterTag.Div);
            phblock.CssClass = "page-header";

            WebControl title = new WebControl(HtmlTextWriterTag.H1);
            title.Controls.Add(new LiteralControl(Labels[RegistrazioneLabels.RegistrazioneLabelsList.TypesOfRegistrations]));

            phblock.Controls.Add(title);

            colPage.Controls.Add(phblock);
                 
            WebControl ol = new WebControl(HtmlTextWriterTag.Ul);

            ICollection<CustomAnagrafe.CustomAnagrafe> anagrafiche = CustomAnagrafe.CustomAnagrafeBusinessLogic.FindAllCustomAnagrafe();

            if (anagrafiche.Count == 1)
                NetCms.Diagnostics.Diagnostics.Redirect(Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot  + "/registrazione/registrati.aspx?ca=" + anagrafiche.ElementAt(0).Nome);

            foreach (CustomAnagrafe.CustomAnagrafe ca in anagrafiche.Where(x=>x.VisibileInFrontend))
            {
                string url = Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/registrazione/registrati.aspx?ca=" + ca.Nome;
                WebControl li = new WebControl(HtmlTextWriterTag.Li);
                li.Controls.Add(new LiteralControl("<a href=\"" + url + "\"><span>" + Labels[RegistrazioneLabels.RegistrazioneLabelsList.AnagraficaRegistration] + ca.Nome + "</span></a>"));
                ol.Controls.Add(li);
            }

            colPage.Controls.Add(ol);

            this.Controls.Add(colPage);
        }

        public static LabelsManager.Labels Labels
        {
            get
            {
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(RegistrazioneLabels)) as LabelsManager.Labels;
            }
        }
    }
}