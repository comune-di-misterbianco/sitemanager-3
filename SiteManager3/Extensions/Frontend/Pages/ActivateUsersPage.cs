using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using G2Core.Common;
using NetCms.Front.Static;

namespace NetCms.Users.Front
{
    [FrontendStaticPage(new[] { "/attivazione/default.aspx", "/en/activation/default.aspx" }, new[] { "Attivazione account utente" , "User account activation" }, NetCms.Front.FrontLayouts.None)]
    public class ActivateUsersPage : WebControl
    {
        public ActivateUsersPage()
            : base(HtmlTextWriterTag.Div)
        {
            Uid = new RequestVariable("kid", RequestVariable.RequestType.QueryString);
        }

        private RequestVariable Uid;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.Controls.Add(new LiteralControl("<h1>" + Labels[UserActivateLabels.LabelList.PageTitle] + "</h1>"));

            if (Uid != null && Uid.IsValidInteger)
            {
                if (AccountManager.Logged)
                {
                    this.Controls.Add(new LiteralControl("<p class=\"alert alert-warning\">" + Labels[UserActivateLabels.LabelList.UserAlreadyActive] + "</p>"));
                }
                else
                {

                    this.Controls.Add(new LiteralControl("<p class=\"description\">" + Labels[UserActivateLabels.LabelList.PageDescription] + "</p>"));
                    string messages;
                    bool esito = NetCms.Users.UsersBusinessLogic.ActivateUser(Uid.IntValue, out messages);

                    if (esito)
                        NetFrontend.NotifyBox.AddSessionMessage(Labels[UserActivateLabels.LabelList.ActivationSuccessMsg], NetFrontend.PostBackMessagesType.success, NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/default.aspx");
                    else
                        NetFrontend.NotifyBox.AddSessionMessage(messages, NetFrontend.PostBackMessagesType.error);
        
                }
            }
            else
            {
                this.Controls.Add(new LiteralControl("<p class=\"description\">" + Labels[UserActivateLabels.LabelList.ActivationError] + "</p>"));
            }
        }

        public static LabelsManager.Labels Labels
        {
            get
            {
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(UserActivateLabels)) as LabelsManager.Labels;
            }
        }


    }
}