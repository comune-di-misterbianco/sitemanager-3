﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Front.Static;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetService.Utility.ArTable;
using NetCms.Networks;
using static NetCms.Front.Static.FrontendStaticPage;

namespace NetCms.Users.Front.Notifiche
{
    [FrontendStaticPage(new[] { "/notifiche/elenco.aspx",  "/en/notifications/list.aspx" }, new[] { "Elenco Notifiche Utente", "User Notification List" }, NetCms.Front.FrontLayouts.None, "", "", StaticPagesLabelsList.NotificheLabelLink, OnlyLoggedUsers = true)]
    public class NotificheElencoPage : WebControl
    {
        private NetFrontend.PageData PageData;
        private User _Account;
        public User Account
        {
            get
            {
                if (_Account == null && NetCms.Users.AccountManager.Logged)
                    _Account = NetCms.Users.AccountManager.CurrentAccount;
                return _Account;
            }
        }

        public NotificheElencoPage(NetFrontend.PageData pageData)
            : base(HtmlTextWriterTag.Div)
        {
            PageData = pageData;
        }

        public ArTable NotificheTable
        {
            get
            {
                if (_NotificheTable == null)
                {

                    List<Notifica> notifiche = NetCms.Users.UsersBusinessLogic.GetNotifiche(NetCms.Users.AccountManager.CurrentAccount, Notifica.TipoNotifica.Frontend, NetCms.Networks.NetworksManager.CurrentActiveNetwork.ID);
                    if (notifiche != null && notifiche.Count > 0)
                    {
                        _NotificheTable = new ArTable(notifiche);
                        _NotificheTable.NoRecordMsg = Labels[NotificheLabels.LabelsList.NoNotification];
                        ArTextColumn titolo = new ArTextColumn(Labels[NotificheLabels.LabelsList.Title], Labels[NotificheLabels.LabelsList.Title]);
                        ArTextColumn testo = new ArTextColumn(Labels[NotificheLabels.LabelsList.Text], Labels[NotificheLabels.LabelsList.Text]);
                        ArTextColumn data = new ArTextColumn(Labels[NotificheLabels.LabelsList.Date], Labels[NotificheLabels.LabelsList.Date]);
                        data.DataType = ArTextColumn.DataTypes.DateTime;
                        ArActionColumn link = new ArActionColumn("Link", "Link", "{Link}");

                        _NotificheTable.AddColumns(titolo, testo, data, link);
                    }
                }
                return _NotificheTable;
            }
        }
        private ArTable _NotificheTable;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (NetCms.Users.AccountManager.Logged)
            {

                WebControl h1 = new WebControl(HtmlTextWriterTag.H1);
                h1.Controls.Add(new LiteralControl(Labels[NotificheLabels.LabelsList.List]));
                this.Controls.Add(h1);
                
                if (NotificheTable != null)
                    this.Controls.Add(NotificheTable);
            }
            else     
            {
                this.Controls.Add(new NetCms.Users.LoginControl());
            }
        }

        public static LabelsManager.Labels Labels
        {
            get
            {
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(NotificheLabels)) as LabelsManager.Labels;
            }
        }
    }
}