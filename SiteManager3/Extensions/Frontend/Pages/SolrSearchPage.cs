﻿using G2Core.Common;
using NetCms.Front.Static;
using NetCms.Structure.Search.Forms;
using NetCms.Structure.Search.Models;
using NetCms.Users;
using NetService.Utility.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Frontend.Pages
{
    [FrontendStaticPage(new[] { "SolrSearch/ricerca/search.aspx", "/en/SolrSearch/search/search.aspx" }, new[] { "Ricerca" , "Search"}, NetCms.Front.FrontLayouts.All)]
    public class SolrSearchPage : WebControl
    {
        private NetFrontend.PageData PageData;

        public SolrSearchPage(NetFrontend.PageData pageData)
            : base(HtmlTextWriterTag.Div)
        {
            PageData = pageData;
            SearchForm.SubmitButton.Click += SubmitButton_Click;
        }

        public RequestVariable SearchTexts
        {
            get
            {
                RequestVariable searchTexts = new RequestVariable
                    ("searchBox", RequestVariable.RequestType.Form_QueryString);
                return searchTexts;
            }

        }
        public RequestVariable CurrentPage
        {
            get
            {
                RequestVariable CurrentPage = new RequestVariable
                    ("searchPage", RequestVariable.RequestType.QueryString);
                return CurrentPage;
            }
        }


        private CmsSearchForm _SearchForm;
        public CmsSearchForm SearchForm
        {
            get
            {
                if (_SearchForm == null)
                {
                    _SearchForm = new CmsSearchForm("SolrSearch");
                    _SearchForm.ShowMandatoryInfo = false;
                    _SearchForm.CssClass = "Axf";
                    _SearchForm.SubmitButton.Text = "Cerca";
                    _SearchForm.EnableInLineButtonsStyle = true;

                }
                return _SearchForm;
            }
        }

        private WebControl _SearchResults;
        public WebControl SearchResults
        {
            get
            {
                if (_SearchResults == null)
                {
                    _SearchResults = new WebControl(HtmlTextWriterTag.Div);
                    _SearchResults.CssClass = "searchResults";
                    _SearchResults.ID = "searchResults";
                }
                return _SearchResults;
            }
            set { _SearchResults = value; }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.ID = "searchpage";

            // searchBar
            this.Controls.Add(SearchForm);

            // searchResult
            this.Controls.Add(SearchResults);

            if (SearchTexts.IsValidString && CurrentPage.IsValidInteger)
                Search(SearchTexts.StringValue);

        }

        void Search(string searchTerm)
        {
            CmsConfigs.DAO.CmsSolrDao solrDao = new CmsConfigs.DAO.CmsSolrDao();

            if (solrDao.GetStatus())
            {
                int rpp = 10;
                string paginationKey = "searchPage";
                int currentPage = 1;

                if (CurrentPage.IsValidInteger)
                    currentPage = CurrentPage.IntValue;

                string qt = "edismax";
                string qf = "document_content_text^0.5+document_title^1.0+document_description^1.1";

                Dictionary<string, string> filters = new Dictionary<string, string>();

                Dictionary<string, CmsConfigs.DAO.CmsSolrDao.Order> orders = new Dictionary<string, CmsConfigs.DAO.CmsSolrDao.Order>();
                orders.Add(CmsDocument.Fields.score.ToString(), CmsConfigs.DAO.CmsSolrDao.Order.DESC);

                int resultCount = unchecked((int)(solrDao.SearchEdismaxCount(searchTerm, qt, qf, true, true, filters)));

                PaginationHandler pagination = new PaginationHandler(rpp,
                                                                         resultCount,
                                                                         paginationKey,
                                                                         SearchForm.IsPostBack);

                pagination.PaginationKey = paginationKey;
                pagination.CssClass = "pagination";
                pagination.BaseLink = "search.aspx?" + pagination.PaginationKey + "={0}&searchBox=" + searchTerm;
                pagination.GoToFirstPageOnSearch = true;

                int startNumRecord = (CurrentPage.IntValue > 1) ? ((CurrentPage.IntValue - 1) * rpp) : 1;

                SearchResults results = solrDao.SearchEdismax(searchTerm, startNumRecord, rpp, qt, qf, true, true, false, filters, orders); 

                if (results != null && results.CmsDocuments.Count > 0)
                {
                    #region Risultati ricerca

                    string strResults = "";
                    string strItem = "";

                    string portalAddress = NetCms.Configurations.PortalData.PortalAddress.Remove
                        (NetCms.Configurations.PortalData.PortalAddress.Length - 1);

                    foreach (CmsDocument doc in results.CmsDocuments)
                    {
                        strItem += @" <div class=""item"">
                                         <h3>
                                            <a href=""" + portalAddress + doc.FrontUrl + @""" rel=""esterno"">" + doc.Title + @"</a>
                                         </h3>                                         
                                         <div class=""itemdata"">
                                              <div class=""fronturl""><a href=""" + portalAddress + doc.FrontUrl + @""">" + portalAddress + doc.FrontUrl + @"</a></div>                                                  
                                              <div class=""description"">
                                                   <p><span class=""score"">" + doc.Score + @"</span> - <span class=""lastmodify"">" + doc.LastModify + @" - </span> " + doc.Description + @"</p>                                                                                                   
                                              </div>                                              
                                         </div>
                                    </div>
                                     ";
                    }

                    strResults = @"<p class=""resultscount"">" + Labels[SolrSearchLabels.SolrSearchLabelsList.Approximately] + resultCount  + Labels[SolrSearchLabels.SolrSearchLabelsList.ResultsFound] + @"</p>
                               <div id=""results-container"">
                                   " + strItem + @"
                               </div>
                             ";

                    this.Controls.Add(SearchForm);
                    this.Controls.Add(new LiteralControl(strResults));
                    this.Controls.Add(pagination);

                    #endregion

                }
                else
                {
                    this.Controls.Add(new LiteralControl("<p>" + Labels[SolrSearchLabels.SolrSearchLabelsList.NoResult] + "</p>"));

                }
            }
            else
            {
                this.Controls.Add(new LiteralControl("<p>" + Labels[SolrSearchLabels.SolrSearchLabelsList.ServiceUnavailable] + "</p>"));
            }
        }

        void ResetSearchResults()
        {
            this.Controls.Clear();
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (SearchTexts.IsValidString)
            {
                ResetSearchResults();
                Search(SearchTexts.StringValue);
            }
        }

        public static LabelsManager.Labels Labels
        {
            get
            {
                return LabelsManager.LabelsCache.GetTypedLabels
                    (typeof(SolrSearchLabels)) as LabelsManager.Labels;
            }
        }
    }
}
