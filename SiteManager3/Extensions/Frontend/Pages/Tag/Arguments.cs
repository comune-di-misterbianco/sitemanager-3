﻿using CmsConfigs.Model;
using G2Core.Common;
using LabelsManager;
using NetCms.Front.Static;
using NetCms.Structure.WebFS.Documents.Tags;
using NetCms.Structure.WebFS.Documents.Tags.BusinessLogic;
using NetFrontend;
using NetService.Utility.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Frontend.Pages.Tag
{
    [FrontendStaticPage(new[] { "/tags/argomenti.aspx", "/en/tags/topics.aspx" }, new[] { "Argomenti", "Topics" }, NetCms.Front.FrontLayouts.All)]
    public class Arguments : WebControl
    {
        private NetFrontend.PageData PageData;

        public Arguments(NetFrontend.PageData pageData) : base(HtmlTextWriterTag.Div)
        {
            PageData = pageData;
            CssClass = "container";
        }

        public RequestVariable CurrentTag
        {
            get
            {
                RequestVariable currentTag = new RequestVariable("tag", RequestVariable.RequestType.QueryString);
                return currentTag;
            }
        }

        public RequestVariable CurrentPage
        {
            get
            {
                RequestVariable CurrentPage = new RequestVariable("currpage", RequestVariable.RequestType.QueryString);
                return CurrentPage;
            }
        }

        private CmsTag Tag { get; set; }

        private Labels _CommonLabels;
        private Labels CommonLabels
        {
            get
            {
                if (_CommonLabels == null)
                {
                    _CommonLabels = LabelsCache.GetTypedLabels(typeof(CommonLabels)) as Labels;
                }
                return _CommonLabels;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (CurrentTag.IsValidString)
            {

                Tag = TagsBusinessLogic.FindTagByKey(CurrentTag.OriginalValue);
                if (Tag != null)
                {
                    string code = PageContentByTpl();
                    this.Controls.Add(new LiteralControl(code));
                }
                else
                    throw new System.Web.HttpException(404, "File Not Found");
            }
        }


        private string PageContentByTpl()
        {
            string tplPath = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + GlobalConfig.CurrentConfig.CurrentTheme.TemplateFolder + "commons/tags-page.tpl");

            int recordPP = 12;
            int numRecord = TagsBusinessLogic.GetItemsCount(Tag.Key);

            PaginationHandler pager = new PaginationHandler(recordPP, numRecord, "page", false, 1, true);
            pager.GoToFirstPageOnSearch = true;
            pager.BaseLink = "/tags/argomenti.aspx?" + pager.PaginationKey + "={0}";
            pager.BaseLink += "&tag=" + Tag.Key;

            var items = TagsBusinessLogic.GetItems(Tag.Key, pager.CurrentPage, pager.PageSize, false);

            var sezione = new
            {
                titolo = this.CommonLabels[NetFrontend.CommonLabels.CommonLabelsList.tagstitle] + ": " + char.ToUpper(Tag.Label[0]) + Tag.Label.Substring(1),
                descrizione = "",
                url = "/tags",
                pagename = "argomenti.aspx"
            };

            int pageLimit = 12;
            int range = 5;
            int minRange = Math.Max(1, pager.CurrentPage - range);
            int maxRange = Math.Min(pager.PagesCount, pager.CurrentPage + range);

            Object paging = new
            {
                pages = pager.PagesCount,
                pagekey = pager.PaginationKey,
                current = pager.CurrentPage,
                pagelimit = pageLimit,
                lowrange = minRange,
                toprange = maxRange,
                pageandquerystring = pager.BuildPaginationBaseAddress()
            };

            NetFrontend.TemplateEngine.TemplateUtility tplParser = new NetFrontend.TemplateEngine.TemplateUtility();
            string code = tplParser.TemplateParser(items, sezione, tplPath, paging);

            return code;
        }
    }
}
