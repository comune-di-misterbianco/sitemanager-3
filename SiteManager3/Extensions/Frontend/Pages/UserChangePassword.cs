﻿using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetForms;
using NetCms.Front.Static;
using NetService.Utility.ValidatedFields;
using LabelsManager;
using NetFrontend;
using static NetCms.Front.Static.FrontendStaticPage;

namespace NetCms.Users.Front
{
    [FrontendStaticPage(new[] { "/user/changepw.aspx" , "/en/user/changepw.aspx"}, new[] { "Aggiornamento Credenziali" , "Credentials update" }, NetCms.Front.FrontLayouts.None, "", "", StaticPagesLabelsList.EditPasswordLabelLink, OnlyLoggedUsers=true)]
    public class UserChangePassword : WebControl
    {
          
        #region Label
        /// <summary>
        /// Users Labels
        /// </summary>
        public static Labels Labels
        {
            get
            {
                return LabelsCache.GetTypedLabels(typeof(UsersLabels)) as Labels;
            }
        }

        #endregion

        private User _Account;
        public User Account
        {
            get
            {
                if (_Account == null && NetCms.Users.AccountManager.Logged)
                    _Account = NetCms.Users.AccountManager.CurrentAccount;
                return _Account;
            }
        }

        public UserChangePassword(): base(HtmlTextWriterTag.Div)
        {
            ChangePasswordForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        public VfManager ChangePasswordForm
        {
            get
            {
                if (_ChangePasswordForm == null)
                {
                    _ChangePasswordForm = new VfManager("changePassword", VfManager.VfManagerRenderType.Frontend);
                    _ChangePasswordForm.SubmitButton.Text = Labels[UsersLabels.UsersLabelsList.UserPasswordUpdateBtn];               
                    _ChangePasswordForm.ShowMandatoryInfo = true;

                    VfPassword password = new VfPassword("password");
                    password.Required = true;                    
                    _ChangePasswordForm.Fields.Add(password);
                }
                return _ChangePasswordForm;
            }
        }
        private VfManager _ChangePasswordForm;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (NetCms.Users.AccountManager.Logged)
            {
                this.CssClass = "panel form-changepw";

                WebControl panelHeading = new WebControl(HtmlTextWriterTag.Div);
                panelHeading.CssClass = "panel-heading NetCms_UpdatePasswordForm";

                panelHeading.Controls.Add(new LiteralControl("<h1> " + Labels[UsersLabels.UsersLabelsList.UserPasswordUpdateTitle] + " </h1>"));

                this.Controls.Add(panelHeading);

                WebControl panelBody = new WebControl(HtmlTextWriterTag.Div);
                panelBody.CssClass = "panel-body";               

                WebControl formgroupDescription = new WebControl(HtmlTextWriterTag.Div);
                formgroupDescription.CssClass = "form-group smallDescription";
                formgroupDescription.Controls.Add(new LiteralControl(Labels[UsersLabels.UsersLabelsList.UserPasswordUpdateDescription]));

                panelBody.Controls.Add(formgroupDescription);

                panelBody.Controls.Add(ChangePasswordForm);

                this.Controls.Add(panelBody);

            }
            else
            {
                    new NetCms.Users.LoginControlv3();
            }
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (ChangePasswordForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                string password = ChangePasswordForm.Fields.Find(x=>x.Key == "password").PostBackValue;
                string reqPwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();

                User current = UsersBusinessLogic.GetById(NetCms.Users.AccountManager.CurrentAccount.ID);

                if (current.Password == reqPwd || current.NuovaPassword == reqPwd)
                {
                    NetFrontend.NotifyBox.AddMessage(Labels[UsersLabels.UsersLabelsList.UserDifferentPassword], NetFrontend.PostBackMessagesType.error);
                }
                else
                {
                    current.Password = reqPwd;
                    current.LastUpdate = DateTime.Now;

                    UsersBusinessLogic.MergeUser(current);
                    NetCms.Users.AccountManager.LoginUser(current.ID.ToString());

                    NetFrontend.NotifyBox.AddSessionMessage(Labels[UsersLabels.UsersLabelsList.UserPasswordUpdated], NetFrontend.PostBackMessagesType.success, NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot);
                }
            }
        }
       
    }
}
