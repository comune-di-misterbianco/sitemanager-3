﻿ using System;
using System.Text;
using NetCms.Front.Static;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web;
using System.Diagnostics;
using System.Web.UI;
using LabelsManager;
using NetFrontend;

namespace NetCms.Users.Front
{
    [FrontendStaticPage(new[] { "/ricerca/ricerca.aspx" , "/en/search/search.aspx"}, new[] { "Ricerca" , "Search"}, NetCms.Front.FrontLayouts.All)]
    public class ZoomSearchPage:WebControl
    {
        private NetFrontend.PageData PageData;

        public ZoomSearchPage(NetFrontend.PageData pageData)
            : base(HtmlTextWriterTag.Div)
        {
            PageData = pageData;
        }               

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            
            Label n = new Label();
            Label m = new Label();
            string paramStr = "";

            this.CssClass = "wrapper";


            HtmlGenericControl searchPage = new HtmlGenericControl("div");
            searchPage.Attributes["class"] = "SearchPageZoom";

            #region Title          

            HtmlGenericControl h1 = new HtmlGenericControl("h1");
            h1.InnerHtml = this.CommonLabels[NetFrontend.CommonLabels.CommonLabelsList.SearchPageTitle];
            searchPage.Controls.Add(h1);          

            #endregion

            HtmlGenericControl ht = new HtmlGenericControl("div");
            ht.Attributes["class"] = "SearchDiv";

            if (!CheckIndexProcess())
            {
                HtmlGenericControl field_form = new HtmlGenericControl("div");
                field_form.Attributes["class"] = "form-inline";

                G2Core.Common.RequestVariable zoom_and = new G2Core.Common.RequestVariable("zoom_and", G2Core.Common.RequestVariable.RequestType.Form);
                
                field_form.InnerHtml = "<div class=\"form-group\">"
                     + "<label class=\"control-label\" for=\"search_term\">"+ this.CommonLabels[NetFrontend.CommonLabels.CommonLabelsList.LabelTermSearch] + ":   </label>"
                                 + "<input class = \"form-control\" type=\"text\" name=\"search_term\" id=\"search_term\" size=\"20\" />"
                                            + "</div>"
                                           + "<div class=\"form-group\">"
                                 + "<script type=\"text/javascript\">"
                                    + "function Search()"
                                    + "{"
                                    + "	var search_term = document.getElementById(\"search_term\");"
                                    + "	var search_term_value = search_term.value;"
                                    + "	window.location='ricerca.aspx?zoom_query=' + search_term_value;		"
                                    + "}"
                                 + "</script>"

                                 + "<input class=\"btn btn-default\" type=\"button\" value=\""+ this.CommonLabels[NetFrontend.CommonLabels.CommonLabelsList.SearchButtonText] + "\" id=\"search\" name=\"search\" "
                                 + " onClick=\"Search();\" />"
                                 + "</div>";               

                searchPage.Controls.Add(field_form);
          
                int parampos = int.Parse(HttpContext.Current.Request.RawUrl.IndexOf("?").ToString());
                if (parampos >= 0)
                {
                    paramStr = HttpContext.Current.Request.RawUrl.Substring(parampos + 1);
                }
                n.Text = paramStr;

                ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = HttpContext.Current.Server.MapPath("/ricerca/search.cgi");
                psi.EnvironmentVariables["REQUEST_METHOD"] = "GET";
                psi.EnvironmentVariables["QUERY_STRING"] = paramStr;
                psi.RedirectStandardInput = true;
                psi.RedirectStandardOutput = true;
                psi.UseShellExecute = false;
                Process proc = Process.Start(psi);
                proc.StandardOutput.ReadLine(); // skip the HTTP header line                       

                string zoom_results = "";

                zoom_results = proc.StandardOutput.ReadToEnd();

                Encoding enc = proc.StandardOutput.CurrentEncoding;
                byte[] output = enc.GetBytes(zoom_results);

                zoom_results = Encoding.GetEncoding("UTF-8").GetString(output);

                proc.WaitForExit();

                if (zoom_results.Contains("No such file or directory") && zoom_results.Contains("Check that you specified the correct log filename in your Indexer settings"))
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, this.CommonLabels[NetFrontend.CommonLabels.CommonLabelsList.NoFileOrFolderMsg], "", "", "212");
                }

                if (zoom_results.Contains("Permission denied") && zoom_results.Contains("Check that you specified the correct log filename in your Indexer settings"))
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, this.CommonLabels[NetFrontend.CommonLabels.CommonLabelsList.PermDeniedMsg], "", "", "213");
                }

                if (zoom_results.Contains("<!--The ZOOMSEARCH-->"))
                    zoom_results = zoom_results.Substring(zoom_results.IndexOf("<!--The ZOOMSEARCH-->"));

                zoom_results = zoom_results.Replace(@"<!--Zoom Search Engine Version 6.0 (1023) ENT-->", "");
                zoom_results = zoom_results.Replace(@"search.cgi?zoom_query=", "ricerca.aspx?zoom_query=");
                zoom_results = zoom_results.Replace("<center><p><small>Ricerca powered by <a href=\"http://www.wrensoft.com/zoom/\" target=\"_blank\"><b>Zoom Search Engine</b></a></small></p></center>", "");
                zoom_results = zoom_results.Replace("<form method=\"get\" action=\"search.cgi\" class=\"zoom_searchform\">", "");
                zoom_results = zoom_results.Replace("</form>", "");
                zoom_results = zoom_results.Replace("<b>...</b>", "&hellip;");

                n.Text = zoom_results;

                ht.InnerHtml = n.Text;
               
            }
            else
            {
                ht.InnerHtml = this.CommonLabels[NetFrontend.CommonLabels.CommonLabelsList.NoSearchServiceMessage];
                
            }

            searchPage.Controls.Add(ht);       
           
            this.Controls.Add(searchPage);//.Add(div);
        }

        private bool CheckIndexProcess()
        {
            Process[] runningProcess = Process.GetProcesses();

            foreach (Process p in runningProcess)
            {
                if (p.ProcessName.Contains("ZoomIndexer.exe"))
                    return true;
            }
            return false;
        }


        private Labels _CommonLabels;
        private Labels CommonLabels
        {
            get
            {
                if (_CommonLabels == null)
                {
                    _CommonLabels = LabelsCache.GetTypedLabels(typeof(CommonLabels)) as Labels;
                }
                return _CommonLabels;
            }
        }
    }
}
