using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.Front.Static;
using NetService.Utility.ValidatedFields;
using System.Web.UI.HtmlControls;
using System.Linq;
using NetCms.Users.CustomAnagrafe;
using LabelsManager;
using NetFrontend;
using static NetCms.Front.Static.FrontendStaticPage;

/// <summary>
/// Update User Profile Control
/// </summary>
namespace  NetCms.Users.Front
{
    [FrontendStaticPage(new[] { "/user/modifica_profilo.aspx" , "/en/user/editprofile.aspx" }, new[] { "Modifica dati profilo" , "Edit profile"}, NetCms.Front.FrontLayouts.None, "", "", StaticPagesLabelsList.EditProfiloLabelLink, OnlyLoggedUsers = true)]
    public class UserProfileEdit : WebControl
    {

        #region Label

        /// <summary>
        /// Users Labels
        /// </summary>
        public static Labels Labels
        {
            get
            {
                return LabelsCache.GetTypedLabels(typeof(UsersLabels)) as Labels;
            }
        }
        #endregion


        private NetFrontend.PageData PageData;
        private User _Account;
        public User Account
        {
            get
            {
                if (_Account == null && AccountManager.Logged)
                    _Account = AccountManager.CurrentAccount;
                return _Account;
            }
        }

        public UserProfileEdit(NetFrontend.PageData _PageData)
            : base(HtmlTextWriterTag.Div)
        {
            PageData = _PageData;
            ModProfileForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }

        void SubmitButton_Click(object sender, EventArgs e)
        {
            if (ModProfileForm.IsValid == VfGeneric.ValidationStates.Valid)
            {
                bool sendupdateemailconfirmation = false;
                string oldemail = Account.Anagrafica.Email;
                string newemail = "";

                newemail = ModProfileForm.Fields.Where(x => x.Key == "Email").First().PostBackValue;

                string oldIsUsername = "";
                string newIsUsername = "";
                string ifIsUsernameChanged = "";               
                
                if (string.Compare(oldemail, newemail, true) != 0)                
                    sendupdateemailconfirmation = true;
                
                HtmlGenericControl div_info = new HtmlGenericControl("div");
                div_info.Attributes["class"] = "form-info";

                string userNameUtenteInserito = "";
                if (Account.CustomAnagrafe.EmailAsUserName)
                    userNameUtenteInserito = newemail;
                else 
                {
                    CustomField customFieldIsUsername = Account.CustomAnagrafe.CustomFields.Where(x => x.IsUsername).FirstOrDefault();
                    userNameUtenteInserito = ModProfileForm.Fields.Where(x => x.Key == customFieldIsUsername.CampoInAnagrafe.Key).First().PostBackValue;
                }

                User utente = UsersBusinessLogic.GetUserByUserName(userNameUtenteInserito);
                if (utente == null || (utente != null && utente.ID == Account.ID))
                {

                    if (UsersBusinessLogic.UpdateUserAnagrafica(ModProfileForm, Account.Anagrafica.ID, Account.AnagraficaType))
                    {
                        CustomField customFieldIsUsername = Account.CustomAnagrafe.CustomFields.Where(x => x.IsUsername).FirstOrDefault();
                        if (customFieldIsUsername != null)
                        {
                            bool userAndProfileUpdated = false;
                            oldIsUsername = Account.AnagraficaMapped[customFieldIsUsername.Name].ToString();
                            newIsUsername = ModProfileForm.Fields.Where(x => x.Key == customFieldIsUsername.CampoInAnagrafe.Key).First().PostBackValue;
                            if (string.Compare(oldIsUsername, newIsUsername, true) != 0)
                            {
                                ifIsUsernameChanged = "Il campo '" + customFieldIsUsername.Name + "' usato come User Name � stato modificato. A seguito di questa operazione la nuova UserName � stata cambiata da '" + oldIsUsername + "' a '" + newIsUsername + "'";
                            }
                        }
                        AccountManager.UpdateCurrentUser();

                        string message = "Profilo aggiornato con successo." + ifIsUsernameChanged;
                        if (sendupdateemailconfirmation)
                        {
                            NetCms.Users.AccountManager.SendConfirmationEmailUpdate(Account.NomeCompleto, oldemail, newemail, Account.ID.ToString(), NetCms.Networks.NetworksManager.CurrentActiveNetwork);
                            if (newemail != null && newemail.Length > 0)
                                message += " Per attuare la modifica dell'email a \"" + newemail + "\", seguite le indicazioni inviateVi per posta elettronica all'indirizzo fornito.";
                        }
                        NetFrontend.NotifyBox.AddMessage(message, NetFrontend.PostBackMessagesType.success);
                    }
                    else
                    {
                        NetFrontend.NotifyBox.AddMessage(Labels[UsersLabels.UsersLabelsList.UserProfileUpdateError], NetFrontend.PostBackMessagesType.error);
                    }
                }
                else
                {
                    NetFrontend.NotifyBox.AddMessage(Labels[UsersLabels.UsersLabelsList.UsernameUsed], NetFrontend.PostBackMessagesType.notify);
                }
                this.Controls.Add(div_info);
            }
        }


        public AnagraficaVfManager ModProfileForm
        {
            get
            {
                if (_ModProfileForm == null)
                {
                    NetCms.Users.CustomAnagrafe.CustomAnagrafe strutturaAnagrafe = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(Account.AnagraficaName);
                    _ModProfileForm = new AnagraficaVfManager(this.ID + "_anagrafica", PageData.IsPostBack, strutturaAnagrafe, true, Account, true);
                    _ModProfileForm.SubmitButton.Text = Labels[UsersLabels.UsersLabelsList.UserProfileUpdateErrorBtn];
                }
                return _ModProfileForm;
            }
        }
        private AnagraficaVfManager _ModProfileForm;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (NetCms.Users.AccountManager.Logged && !NetCms.Users.AccountManager.CurrentAccount.CustomAnagrafe.UseSpid)
            {
                this.CssClass = "panel form-user";

                WebControl panelHeading = new WebControl(HtmlTextWriterTag.Div);
                panelHeading.CssClass = "panel-heading NetCms_profileForm";

                panelHeading.Controls.Add(new LiteralControl("<h1> " + Labels[UsersLabels.UsersLabelsList.UserProfileUpdatePageTitle] + " </h1>"));

                this.Controls.Add(panelHeading);

                WebControl panelBody = new WebControl(HtmlTextWriterTag.Div);
                panelBody.CssClass = "panel-body";


                WebControl formgroupDescription = new WebControl(HtmlTextWriterTag.Div);
                formgroupDescription.CssClass = "form-group smallDescription";
                formgroupDescription.Controls.Add(new LiteralControl(Labels[UsersLabels.UsersLabelsList.UserProfileEditDescription]));

                panelBody.Controls.Add(formgroupDescription);

                panelBody.Controls.Add(ModProfileForm);

                this.Controls.Add(panelBody);                
            }
            else
            {
                this.Controls.Add(new NetCms.Users.LoginControl());
            }
        }
    }
}