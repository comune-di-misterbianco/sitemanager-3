using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.Front.Static;
using static NetCms.Front.Static.FrontendStaticPage;

/// <summary>
/// User Profile Frontend static control
/// </summary>
namespace NetCms.Users.Front
{
    [FrontendStaticPage(new[] { "/user/profilo.aspx", "/en/user/profile.aspx" }, new[] { "Profilo utente", "User profile" }, NetCms.Front.FrontLayouts.None, "", "", StaticPagesLabelsList.ProfiloLabelLink, OnlyLoggedUsers = true)] 
    public class UserProfile:WebControl
    {
        private NetFrontend.PageData PageData;
        private User _Account;
        public User Account
        {
            get
            {
                if (_Account == null && NetCms.Users.AccountManager.Logged)
                    _Account = NetCms.Users.AccountManager.CurrentAccount;
                return _Account;
            }
        }

        public UserProfile(NetFrontend.PageData pageData)
            : base(HtmlTextWriterTag.Div)
        {
            PageData = pageData;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (NetCms.Users.AccountManager.Logged)
            {
                this.CssClass = "panel form-user";

                this.Controls.Add(AccountManager.CurrentAccount.Anagrafica.GetProfileDetails("/user/profilo.aspx"));
            }
            else     
            {
                this.Controls.Add(new LoginControlv3());
            }
        }
    }
}