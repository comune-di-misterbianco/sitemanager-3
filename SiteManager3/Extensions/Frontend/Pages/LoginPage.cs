﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.Front.Static;
using NetUtility;

namespace NetCms.Users.Front
{
    [FrontendStaticPage(new[] { "/user/accesso.aspx",  "/en/user/signin.aspx" }, new[] { "Accesso area riservata" , "Restricted area access" }, NetCms.Front.FrontLayouts.Footer)]
    public class LoginPage : WebControl
    {
        private NetFrontend.PageData PageData;

        public LoginPage(NetFrontend.PageData pageData)
            : base(HtmlTextWriterTag.Div)
        {
            PageData = pageData;
        }

        private string LinkRegistrazione = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + Labels[LoginLabels.LabelsList.RegistrationPath];

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!NetCms.Users.AccountManager.Logged)
            {
                if (!NetCms.Configurations.Generics.LoginManager)
                {
                    this.CssClass = "container";

                    WebControl control = new WebControl(HtmlTextWriterTag.Div);
                    control.CssClass = "form-user";

                    WebControl h1 = new WebControl(HtmlTextWriterTag.H1);
                    h1.Controls.Add(new LiteralControl(
                        Labels[LoginLabels.LabelsList.ReservedArea]
                        ));

                    control.Controls.Add(h1);

                    control.Controls.Add(new NetCms.Users.LoginControlv3(NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + Labels[LoginLabels.LabelsList.DefaultPath], true, true, LinkRegistrazione));
                    
                    this.Controls.Add(control);                    
                }
                else
                {
                    this.Controls.Add(new LoginManager.Controls.LoginBox(NetCms.Networks.NetworksManager.CurrentActiveNetwork.ID));
                }
            }  
            else
            {
                try
                {                    
                    AccountManager.RemoveSharedCookie("sharedCookie", NetCms.Configurations.Generics.SsoSharedCookieDomain);
                    AccountManager.AddSharedCookie("sharedCookie", NetCms.Configurations.Generics.SsoSharedCookieDomain); //"net-serv.it"

                    string urlPage = null;
                    if (!string.IsNullOrEmpty(NetCms.Configurations.Generics.ExternalAppQueryParam))
                    {
                        RequestVariable redirectVar = new RequestVariable(NetCms.Configurations.Generics.ExternalAppQueryParam, RequestVariable.RequestType.QueryString);
                        if (redirectVar != null && !string.IsNullOrEmpty(redirectVar.StringValue))
                        {
                            bool redirectFlag = redirectVar.StringValue == "1";
                            if (redirectFlag)
                            {
                                urlPage = NetCms.Configurations.Generics.ExternalAppUrl;                               
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(urlPage))
                        urlPage = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + Labels[LoginLabels.LabelsList.DefaultPath];

                    /// Redirect to page
                    /// 
                    Diagnostics.Diagnostics.Redirect(urlPage);
                }
                catch (Exception ex)
                {
                    throw ex;
                }               
            }
        }

        public static LabelsManager.Labels Labels
        {
            get
            {
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(LoginLabels)) as LabelsManager.Labels;
            }
        }
    }
}
