using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for LayoutBlock
/// </summary>
namespace NetFrontend.Homepages
{
    public abstract class LayoutBlock
    {
        public virtual string Nome { get; protected set; }
        public virtual bool Stato { get; protected set; }
        public virtual int ID { get; protected set; }
        public virtual int Pos { get; protected set; }
        public virtual int Order { get; protected set; }
        public abstract int StateValue { get; }
        public bool IsValid { get; protected set; }
        public virtual bool HasModuli { get; protected set; }
        public virtual string CssClass { get; protected set; }

        public virtual string WrapperCssClass { get; protected set; }

        public virtual string Mode { get; protected set; }

        public virtual bool AddBlock(PageData PageData)
        {
            return IsValid && ( StateValue == 1 || (StateValue == 2 && NetCms.Users.AccountManager.Logged) || (StateValue == 3 && !NetCms.Users.AccountManager.Logged));
        }

        public virtual bool IsBlock
        {
            get
            {
                return true;
            }
        }

        public LayoutBlockCollection ChildBlocks { get; private set; }
        public LayoutBlock Parent
        {
            get
            {
                return _Parent;
            }
            set
            {
                _Parent = value;
            }
        }
        private LayoutBlock _Parent;

     

        public int Depth
        {
            get
            {
                return _Depth;
            }
            set
            {
                _Depth = value;
            }
        }
        private int _Depth;

        public bool HasChild
        {
            get
            {
                return ChildBlocks.Count > 0;
            }
        }
        
        public LayoutBlock()
        {
            IsValid = true;
            ChildBlocks = new LayoutBlockCollection(this);
        }

        public abstract HtmlGenericControl GetLayoutBlock(PageData PageData);
        //public abstract void GetLayoutBlock(PageData PageData, Control container);       

        public LayoutBlock Find(int id)
        {
            return ChildBlocks.Find(id);
        }

        public string TplPath { get; set; }

    }
}