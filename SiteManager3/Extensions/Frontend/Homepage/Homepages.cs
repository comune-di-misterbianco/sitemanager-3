using System.Web.UI.HtmlControls;
using G2Core.Caching;
using NetCms.Homepages;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetFrontend.Homepages
{
    public partial class Homepage
    {
        private PageData PageData { get; set; }
        
        public PersistentCacheObject<HomepageContainersTree> TreeCache
        {
            get
            {
                return _TreeCache ?? (_TreeCache = new PersistentCacheObject<HomepageContainersTree>(BuildCacheKey(PageData.Network.SystemName, PageData.Homepage.ID.ToString()), StoreTypes.Cache, PageData.Homepage.ID,this.PageData));
            }
        }
        private PersistentCacheObject<HomepageContainersTree> _TreeCache;

        //public PersistentCacheObject<HomepageContainersTree> TreeCacheParent
        //{
        //    get
        //    {
        //        //_TreeCacheParent = BuildTreeCache();
        //        if (this.PageData.Homepage.ParentHomepage != null)
        //            return _TreeCacheParent ?? (_TreeCacheParent = new PersistentCacheObject<HomepageContainersTree>(BuildCacheKey(PageData.Network.SystemName, PageData.Homepage.ParentHomepage.ID.ToString()), StoreTypes.Cache, PageData.Homepage.ParentHomepage.ID,PageData.Homepage.ParentHomepage._PageData));
        //        else return null; 
        //        //return _TreeCacheParent;
        //    }
        //}
        //private PersistentCacheObject<HomepageContainersTree> _TreeCacheParent;

        public HomepageContainersTree Tree
        {
            get
            {
                //Aggiunto questo codice per forzare l'invalidazione della cache di homepage nel caso in cui si cambia lingua di frontend
                //if (_NeedToBeRefreshed)
                //{
                //    _Tree = null;
                //    PersistentCacheObject<HomepageContainersTree>.Invalidate(TreeCache.Key, StoreTypes.Cache);
                //    _NeedToBeRefreshed = false;
                //}
                if (_Tree == null)
                {
                    _Tree = TreeCache.Instance;
                }                
                return _Tree;
            }
        }
        private HomepageContainersTree _Tree;
        //public HomepageContainersTree TreeParent
        //{
        //    get
        //    {
        //        if (_TreeParent == null)
        //        {
        //            _TreeParent = TreeCacheParent == null ? null : TreeCacheParent.Instance ;
        //        }
        //        return _TreeParent;
        //    }
        //}
        //private HomepageContainersTree _TreeParent;

        public Homepage(PageData pagedata)
        {
            PageData = pagedata;
        }
        
        public HtmlGenericControl ColSx
        {
            get
            {
                LayoutBlock Colsx = Tree["Colsx"];
             
                return Colsx.Stato ? Colsx.GetLayoutBlock(PageData) : null;
            }
        }
        public HtmlGenericControl ColCx
        {
            get
            {
                LayoutBlock colcx =Tree["Colcx"];
                HtmlGenericControl control = colcx.GetLayoutBlock(PageData);
                
                if (PageData.Folder.CssClass.Length > 0)
                    control.Attributes["class"] += " " + PageData.Folder.CssClass;

                if (PageData.Homepage.CssClass.Length > 0)
                {
                    control.Attributes["class"] += " " + PageData.Homepage.CssClass;
                    control.Attributes["class"] = control.Attributes["class"].Trim();
                }

                return control;
            }
        }
        public HtmlGenericControl ColDx
        {
            get
            {
                LayoutBlock Coldx = Tree["Coldx"];
                return Coldx.Stato ? Coldx.GetLayoutBlock(PageData) : null;
            }
        }
        public HtmlGenericControl Footer
        {
            get
            {
                LayoutBlock footer = Tree["Footer"];

                #region MyRegion
                //if (TreeParent != null && (this.PageData.Homepage.ParentHomepage != null) && Tree["Footer"].ChildBlocks.Count() == 0)
                //{

                //    //foreach (LayoutBlock l in TreeParent["Footer"].ChildBlocks.Values)
                //    //{
                //    //    KeyValuePair<string, LayoutBlock> k = new KeyValuePair<string, LayoutBlock>(l.Nome, l);

                //    //    if (!footer.ChildBlocks.Contains(k))
                //    //        // if (footer.ChildBlocks.Find(l.ID) == null)
                //    //        footer.ChildBlocks.Add(l);


                //    //}
                //    footer = TreeParent["Footer"];

                //}
                //else
                //{


                //}
                //if (Tree.ContainsKey("Footer"))
                //    footer = Tree["Footer"];
                //else
                //{


                //}
                //else
                //    if (TreeParent.ContainsKey("Footer"))
                //        footer = TreeParent["Footer"]; 
                #endregion

                return footer.Stato ? footer.GetLayoutBlock(PageData) : null;
            }
        }

        public static LayoutModuloBlock BlocksFactory(Modulo modulo)
        {
            return BlocksFactory(modulo.Tipo, modulo);
        }

        public static LayoutModuloBlock BlocksFactory(int TipoModulo, Modulo modulo)
        {
            string TypeID = TipoModulo.ToString();
            if (ModuliHomepageClassesPool.FrontendModuleClasses.ContainsKey(TypeID))
            {
                object[] pars = { modulo };
                var type = ModuliHomepageClassesPool.FrontendModuleClasses[TypeID];
                return System.Activator.CreateInstance(type, pars) as LayoutModuloBlock;
            }

            return null;
        }

        [G2Core.Caching.Invalidation.InvalidationMethod("FrontendHomepage")]
        public static void Invalidate(string networkSystemName, string homepageID)
        {
            string key = BuildCacheKey(networkSystemName, homepageID);
            PersistentCacheObject<HomepageContainersTree>.Invalidate(key, StoreTypes.Cache);
        }

        public static string BuildCacheKey(string networkSystemName, string homepageID)
        {
            //Aggiungendo un caching separato per ogni lingua, non devo invalidare ogni volta le homepage ed evito un bug causato dalla difficolt� a mantenere attivo il cambio lingua dopo lo switch all'interno della navigazione
            //Ad esempio se parto dalla home (primo caching), vado in una sottocartella, cambio la lingua ad esempio ad inglese (settando il booleano con la necessit� di invalidare la cache),
            //torno alla pagina home viene mostrata la homepage in cache che era quella della lingua precedente in quanto non avendo fatto switch l'invalidazione non avviene. Con il caching separato risolvo il problema e non ho pi� bisogno del booleano.
            if (LanguageManager.Utility.Config.EnableMultiLanguage)
                return "SM2_FrontHomeTree_Net#" + networkSystemName.ToUpper() + "_Home#" + homepageID + "#" + LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode;
            return "SM2_FrontHomeTree_Net#" + networkSystemName.ToUpper() + "_Home#" + homepageID;
        }

        //public PersistentCacheObject<HomepageContainersTree> BuildTreeCache()
        //{

        //    PersistentCacheObject<HomepageContainersTree> TreeEreditato;





        //}
    }
}
