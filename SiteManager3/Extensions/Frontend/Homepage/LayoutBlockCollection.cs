using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

/// <summary>
/// Summary description for xNodeCollection
/// </summary>

namespace NetFrontend.Homepages
{
    public class LayoutBlockCollection : Dictionary<string, LayoutBlock> 
    {
        private LayoutBlock Owner;
        public LayoutBlockCollection(LayoutBlock owner)
        {
            Owner = owner;
        }
        public void Add(LayoutBlock layoutblock)
        {
            this.Add(layoutblock.Nome, layoutblock);
            layoutblock.Parent = Owner;
        }

        public LayoutBlock Find(int dbId)
        {
            LayoutBlock node = null;

            if (dbId == Owner.ID) /* cambiare con nome */
                node = Owner;

            if (node == null)
                node = this.Values.Where(x => x.ID == dbId).FirstOrDefault();

            var enumerator = this.GetEnumerator();
            while (node == null && enumerator.MoveNext())
                node = enumerator.Current.Value.ChildBlocks.Find(dbId);

            return node;
        }
    }
    //public class LayoutBlockCollection
    //{
    //    private NetCollection coll;

    //    public int Count { get { return coll.Count; } }

    //    private LayoutBlock Owner;

    //    public LayoutBlockCollection(LayoutBlock owner)
    //    {
    //        coll = new NetCollection();
    //        Owner = owner;
    //    }

    //    public void Add(LayoutBlock layoutblock)
    //    {
    //        coll.Add(layoutblock.Nome, layoutblock);
    //        layoutblock.Parent = Owner;
    //    }

    //    public LayoutBlock this[int i]
    //    {
    //        get
    //        {
    //            LayoutBlock value = (LayoutBlock)coll[coll.Keys[i]];
    //            return value;
    //        }
    //    }

    //    public LayoutBlock this[string key]
    //    {
    //        get
    //        {
    //            LayoutBlock field = (LayoutBlock)coll[key];
    //            return field;
    //        }
    //    }
    //    public LayoutBlock Find(string key)
    //    {
    //        LayoutBlock node = null;

    //        if (key == Owner.Nome) /* cambiare con nome */
    //            node = Owner;

    //        if (node == null)
    //            node = this[key];

    //        int i = 0;
    //        while (node == null && i < this.Count)
    //            node = this[i++].ChildBlocks.Find(key);

    //        return node;
    //    }

    //    public LayoutBlock Find(int dbId)
    //    {
    //        LayoutBlock node = null;

    //        if (dbId == Owner.ID) /* cambiare con nome */
    //            node = Owner;

    //        if (node == null)
    //            for (int i = 0; i < this.Count && node == null; i++)
    //            {
    //                node = this[i].ID == dbId ? this[i] : null;
    //            }

    //        int j = 0;
    //        while (node == null && j < this.Count)
    //            node = this[j++].ChildBlocks.Find(dbId);

    //        return node;
    //    }
    //}
}