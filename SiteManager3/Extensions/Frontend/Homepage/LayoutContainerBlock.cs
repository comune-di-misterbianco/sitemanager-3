using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Applications.Homepage.Model;

/// <summary>
/// Summary description for LayoutContainerBlock
/// </summary>
namespace NetFrontend.Homepages
{
    public class LayoutContainerBlock : LayoutBlock
    {
        public override int StateValue { get { return 1; } }

        public bool IsSystemContainer
        {
            get
            {
                return   this.Nome == "Row" + ID ||
                         this.Nome == "Colcx" ||
                         this.Nome == "Coldx" ||
                         this.Nome == "Colsx" ||
                         this.Nome == "Footer";
            }
        }

        public LayoutContainerBlock(Contenitore contenitore)
        {
            Nome = contenitore.Name;//row["nome_contenitore"].ToString();
            ID = contenitore.ID;//int.Parse(row["id_contenitore"].ToString());
            HasModuli = contenitore.HasModuli;//row["havemoduli_contenitore"].ToString() == "1";
            Stato = contenitore.Stato == 1;//row["stato_contenitore"].ToString() == "1";
           
            Pos = contenitore.Posizione;//int.Parse(row["posizione_contenitore"].ToString());
            Order = contenitore.Order;
            
            CssClass = contenitore.CssClass;//row["cssclass_contenitore"].ToString();
            WrapperCssClass = contenitore.WrapperCssClass;
            Depth = contenitore.Depth;//row.IntValue("depth_contenitore");
            Mode = contenitore.Mode;
        }

        public override HtmlGenericControl GetLayoutBlock(PageData PageData)
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            if (NetCms.Configurations.ThemeEngine.Status == NetCms.Configurations.ThemeEngine.ThemeEngineStatus.off)
            //if (PageData.CurrentConfig != null && PageData.CurrentConfig.Layout == CmsConfigs.Model.Config.LayoutType.Responsive)
            {


                if (string.IsNullOrEmpty(Mode))
                    div.Attributes["class"] = "Block Block" + this.ID;
                else
                {
                    if (Mode == "custom")
                        div.Attributes["class"] += " " + CssClass;
                    else
                        div.Attributes["class"] += " " + Mode;
                }


                if (CssClass != null && CssClass.Length > 0)
                    div.Attributes["class"] += " " + CssClass;

                int numeroFigli = ChildBlocks.Count;

                if (!IsSystemContainer)
                {
                    HtmlGenericControl name = new HtmlGenericControl("div");
                    name.InnerHtml = Nome;
                    name.Attributes["class"] = "BlockTitle";
                    div.Controls.Add(name);
                }

                #region Contenitore con moduli a schede
                if (this.Pos == 1 && Depth > 1) // pos � una trasposizione errata che indica la modalit� di rendering del contenitore pos=1 tabs e pos=0 inline
                {
                    NetUtility.SheetsControl sheets = new NetUtility.SheetsControl("Sheets_" + this.ID);
                    sheets.AddScriptCode = false;
                    foreach (var child in this.ChildBlocks.Values.Where(x => x.AddBlock(PageData)))
                    {
                        LayoutModuloBlock modulo = (LayoutModuloBlock)child;
                        HtmlGenericControl control = new HtmlGenericControl("div");

                        control.Controls.Add(modulo.GetLayoutBlock(PageData));

                        NetUtility.HtmlSheet sheet = new NetUtility.HtmlSheet();
                        sheet.Control = control;
                        sheet.Label = modulo.Title;

                        sheets.Sheets.Add(sheet);
                    }
                    div.Controls.Add(sheets.GetControl());
                }

                #endregion

                #region Contenitore con moduli affiancati
                else
                {
                    //for (int i = 0; i < ChildBlocks.Count; i++)
                    foreach (var child in this.ChildBlocks.Values.Where(x => x.AddBlock(PageData)))
                    {
                        HtmlGenericControl content = child.GetLayoutBlock(PageData);
                        if (content != null)
                        {
                            //HtmlGenericControl modulo = new HtmlGenericControl("div");

                            //modulo.Controls.Add(content);
                            //div.Controls.Add(modulo);
                            //div.Controls.Add(content);

                            if (!child.IsBlock && this.Nome != "Colsx" && this.Nome != "Coldx")
                            {
                                HtmlGenericControl modulo = new HtmlGenericControl("div");

                                modulo.Controls.Add(content);
                                div.Controls.Add(modulo);
                                switch (numeroFigli)
                                {
                                    //case 1: modulo.Attributes["class"] = "c100"; break;
                                    //case 2: modulo.Attributes["class"] = "c50"; break;
                                    //case 3: modulo.Attributes["class"] = "c33"; break;
                                    //case 4: modulo.Attributes["class"] = "c25"; break;
                                    case 1: modulo.Attributes["class"] = "col-md-12"; break;
                                    case 2: modulo.Attributes["class"] = "col-md-6"; break;
                                    case 3: modulo.Attributes["class"] = "col-md-4"; break;
                                    case 4: modulo.Attributes["class"] = "col-md-3"; break;
                                }
                            }
                            else
                            {
                                div.Controls.Add(content);
                            }
                        }
                    }
                }

                if (this.ChildBlocks.Count > 0 && this.Depth != 0)
                {
                    HtmlGenericControl clr = new HtmlGenericControl("div");
                    //clr.Attributes["class"] = "clr";
                    clr.Attributes["class"] = "clearfix";
                    div.Controls.Add(clr);
                }
                #endregion


            }
            else
            {
                if (Depth == 1)
                    div.Attributes["class"] = "wrapper-container";             
                if (Depth == 2)
                    div.Attributes["class"] = "wrapper-section" + (!string.IsNullOrEmpty(this.WrapperCssClass) ? " " + this.WrapperCssClass : "");

                // controllo web per contenere i moduli affiancati
                HtmlGenericControl container = new HtmlGenericControl("div");
                container.Attributes["class"] = Mode + " " + CssClass;

                
                HtmlGenericControl row = new HtmlGenericControl("div");
                row.Attributes["class"] = "row";

                var childBlocks = this.ChildBlocks.Values.Where(x => x.AddBlock(PageData)).OrderBy(x=>x.Order);

                foreach (var child in childBlocks)
                {      
                    HtmlGenericControl content = child.GetLayoutBlock(PageData);

                    if (content != null)
                    {                       
                        if (!child.IsBlock && this.Nome != "Colsx" && this.Nome != "Coldx")
                        {
                            HtmlGenericControl modulo = new HtmlGenericControl("div");
                            modulo.Controls.Add(content);

                            switch (ChildBlocks.Count)
                            {
                                case 1: modulo.Attributes["class"] = Mode + " " + CssClass; break;
                                case 2: modulo.Attributes["class"] = "col-md-6"; break;
                                case 3: modulo.Attributes["class"] = "col-md-4"; break;
                                case 4: modulo.Attributes["class"] = "col-md-3"; break;
                            }

                            if (ChildBlocks.Count > 1)
                            {

                                row.Controls.Add(modulo);
                                
                                container.Controls.Add(row);
                                
                                div.Controls.Add(container);
                            }
                            else
                                div.Controls.Add(modulo);
                        }
                        else
                        {
                            div.Controls.Add(content);
                        }
                    }
                }
            }
            return div;
        }
    }
}
