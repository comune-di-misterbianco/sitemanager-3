using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.Structure.Applications.Homepage;

/// <summary>
/// Summary description for LayoutModuloBlock
/// </summary>
/// 
namespace NetFrontend.Homepages
{
    [NetCms.Homepages.HomepageModuleDefiner(2, "Menu", -1, NetCms.Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public class ModuloMenu : LayoutModuloBlock
    {
        protected override string LocalTypeCssClass
        {
            get { return "modulo_menu"; }
           // get { return "modulo menu row"; }
            
        }

        public override string Nome
        {
            get
            {
                return "menu" + ID;
            }
        }

        public override bool HasModuli
        {
            get { return false; }
        }

        public override string Title
        {
            get { return "Menu"; }
        }

        private int Starting;
        private int Ending;
        private bool Exploded;        
        private bool UseMenuAgid = false;
        private bool AlwaysFirst;
        private bool OnlyLocal = false;

        public ModuloMenu(Modulo modulo)
            : base(modulo)
        {

            NetCms.Structure.Applications.Homepage.Model.ModuloMenu moduloMenu = modulo as NetCms.Structure.Applications.Homepage.Model.ModuloMenu;

            //DataRow localRow = NetFrontend.DAL.HomepageDAL.GetModuloMenu("Modulo_ModuloMenu = " + row.IntValue("id_Modulo"));

            Starting = moduloMenu.Starting; //int.Parse(localRow["Starting_ModuloMenu"].ToString());
            AlwaysFirst = moduloMenu.Starting == 1; //localRow["Starting_ModuloMenu"].ToString() == "1";
            Ending = moduloMenu.Ending; //int.Parse(localRow["Ending_ModuloMenu"].ToString());

            Exploded = moduloMenu.Type == 1;  //int.Parse(localRow["Type_ModuloMenu"].ToString()) == 1;
            UseMenuAgid = moduloMenu.Type == 2;
            OnlyLocal = moduloMenu.Type == 3;

        }

        public override HtmlGenericControl GetLayoutBlock(PageData PageData)
        {
            HtmlGenericControl div = null;

            try
            {

                if (PageData.PageDepth >= Starting)
                {
                    var Proxy = new NetCms.Front.NetworkMenu();                    
                    Proxy.FirstLevelAllwaysVisible = AlwaysFirst;

                    NetFrontend.Homepages.LayoutBlock parent = Parent;

                    while (parent.Parent != null && parent.Parent.Parent != null)
                        parent = parent.Parent;

                    LiteralControl menu;
                    if (!UseMenuAgid && !OnlyLocal)
                    {                        
                        menu = Proxy.GetMenu(parent.Nome, Starting, Ending, Exploded) as LiteralControl;
                    }
                    else if (OnlyLocal)
                    {
                        int endingLevel = Starting + 1;

                        menu = Proxy.GetMenu(parent.Nome, PageData.PageDepth, endingLevel, Exploded) as LiteralControl;

                        if (menu.Text.Length == 0)
                            menu = new LiteralControl("<a class=\"btn\" href=\"" + PageData.Folder.Parent.FrontendUrl + "default.aspx\" role=\"button\" title=\"Torna alla pagina precedente\">Indietro</a>");
                    }
                    else
                    {
                        menu = Proxy.GetMobileMenu(parent.Nome, Starting, Ending, true, true, true, this.CssClass) as LiteralControl;

                        string navContainer = "<nav id=\"menu-contextual-" + this.ID + "\" aria-labelledby=\"menu-contextual-" + this.ID + "\">"
                                            + "<h2 id=\"nav-menu-contextual-" + this.ID + "\" class=\"u-hiddenVisually\">Menu contestuale</h2>"
                                            + menu
                                            + "</nav>";
                    }

                    if (menu.Text.Length > 0) 
                    {
                        div = new HtmlGenericControl("div");
                        div.Attributes["class"] = CssClass;

                        if (NetCms.Configurations.ThemeEngine.Status == NetCms.Configurations.ThemeEngine.ThemeEngineStatus.on)
                        {
                            LiteralControl button = new LiteralControl("<button type=\"button\" class=\"d-block d-md-none btn btn-toggle\" data-toggle=\"\" data-target=\"#md-" + this.ID+"\">Menu contestuale</button>");

                            HtmlGenericControl modalContainer = new HtmlGenericControl("div");
                            modalContainer.Attributes["class"] = "menu-container d-none d-md-block"; //"modal left fade";
                            modalContainer.ID = "md-" + this.ID;
                            modalContainer.Attributes["role"] = "dialog";
                            //aria

                            string strModalContent = @"<div class=""menu-content"">
					                                    " + menu.Text + @"
			                                           </div>";

                            modalContainer.Controls.Add(new LiteralControl(strModalContent));

                            div.Controls.Add(button);
                            div.Controls.Add(modalContainer);
                        }
                        else
                        {
                           
                            div.Controls.Add(menu);
                        }
                    }
                    
                }
            }
            catch(Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la generazione del menu: " + ex.Message);
            }

            return div;
        }
    }
}
