﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using LabelsManager;
using LanguageManager.BusinessLogic;
using NetCms.Structure.Applications.Homepage;
using NetCms.Structure.Applications.Homepage.Model;

namespace NetFrontend.Homepages
{
    [NetCms.Homepages.HomepageModuleDefiner(88, "Tags", -1, NetCms.Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public class ModuloTags : LayoutModuloBlock
    {
        /// <summary>
        /// CommonLabels
        /// </summary>
        public static Labels CommonLabel
        {
            get
            {
                return LabelsCache.GetTypedLabels(typeof(CommonLabels), LanguageBusinessLogic.CurrentLanguage.CultureCode_Lang) as Labels;
            }
        }

        protected override string LocalTypeCssClass { get => "modulo_tags"; }

        public override string Nome { get => "Tags" + ID; }

        public override string Title { get => "Tags"; }

        public override bool AddBlock(PageData PageData)
        {
            return PageData.ContentsHandler != null && PageData.ContentsHandler.CurrentDocument != null;
        }

        private int MaxDocs;
        private int NetworkId;
        public ModuloTags(Modulo modulo)
            : base(modulo)
        {
            NetCms.Structure.Applications.Homepage.Model.ModuloTags module = HomepageModulesBusinessLogic<NetCms.Structure.Applications.Homepage.Model.ModuloTags>.GetModuloById(modulo.ID);
            
            MaxDocs = module.Records;
            NetworkId = module.NetworkID;
        }
        public override HtmlGenericControl GetLayoutBlock(PageData PageData)
        {
            HtmlGenericControl divmodulo = new HtmlGenericControl("div");

            LiteralControl cSrc = GetTemplateContent();

            divmodulo.Controls.Add(cSrc);

            return divmodulo;
        }

        public LiteralControl GetTemplateContent()
        {
            

            string tpl_path = GetModuleTemplatePath("modulo-tags", "");

            var currentmodulo = new
            {
                label = CommonLabel[CommonLabels.CommonLabelsList.tagstitle],
                tagspageurl = GetArgumentPageUrl(this.NetworkId)
            };

            var items = NetCms.Structure.WebFS.Documents.Tags.BusinessLogic.TagsBusinessLogic.GetAllTags(this.MaxDocs, this.NetworkId);
                        
            Object dati = new { modulo = currentmodulo, items = items };

            string src = string.Empty;
            src = base.RenderTemplate(dati, tpl_path, null);
            LiteralControl srcCntr = new LiteralControl(src);
            return srcCntr;
        }

        private string GetArgumentPageUrl(int NetworkId)
        {
            // 0 network rootpath            
            string strUrl = "{0}/tags/argomenti.aspx?tag=";

            NetCms.Networks.BasicNetwork network = NetCms.Networks.NetworksBusinessLogic.GetById(NetworkId);

            return string.Format(strUrl, network.RootPath);
        }
    }
}
