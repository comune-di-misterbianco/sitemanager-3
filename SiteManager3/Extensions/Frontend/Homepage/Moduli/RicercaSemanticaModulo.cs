using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS;
using CmsConfigs.BusinessLogic.Annotate;
using NetCms.Structure.Applications.Homepage;

/// <summary>
/// Summary description for LayoutModuloBlock
/// </summary>
/// 
namespace NetFrontend.Homepages
{
    [NetCms.Homepages.HomepageModuleDefiner(66, "Ricerca Semantica", -1, NetCms.Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public class RicercaSemanticaBlock : LayoutModuloBlock
    {
        protected override string LocalTypeCssClass
        {
            get { return "modulo_rsem"; }
        }
        public override string Nome
        {
            get
            {
                return "RicercaSemantica" + ID;
            }
        }
        public override string Title
        {
            get { return "Ricerca Semantica"; }
        }

        public override bool AddBlock(PageData PageData)
        {
            return PageData.ContentsHandler != null && PageData.ContentsHandler.CurrentDocument != null;
        }
        
        public RicercaSemanticaBlock(Modulo modulo)
            : base(modulo)
        {
            ModuloRicercaSemantica module = HomePageBusinessLogic.GetModuloRicercaSemaById(modulo.ID);
            ShowTitle = module.ShowTitle;
            MaxDocs = module.MaxDocument;
        }

        private bool ShowTitle;
        private int MaxDocs;

        public override HtmlGenericControl GetLayoutBlock(PageData PageData)
        {
            bool HasReated = PageData.ContentsHandler != null && PageData.ContentsHandler.CurrentDocument != null;
            HtmlGenericControl div = new HtmlGenericControl("div");
            if (HasReated)
            {
                int currDoc = PageData.ContentsHandler.CurrentDocumentID;

                div.Attributes["class"] = CssClass;

                Document doc = DocumentBusinessLogic.GetById(currDoc);

                if (doc != null && doc.Application == 2 && doc.DocTags != null && doc.DocTags.Count() > 0) //doc.DocLangs != null && doc.DocLangs.FirstOrDefault() != null
                {
                   

                    DocumentLang docLang = doc.DocLangs.FirstOrDefault();
                    int idDocContent = docLang.Content.ID;                   
                    //string[] arrayTags = CmsConfigs.BusinessLogic.Annotate.AnnotateBusinessLogic.GetDocContentTags(idDocContent);

                    char[] separator = { ',' };
                    string[] arrayTags = doc.Tags.Replace(" ", "").Split(separator, StringSplitOptions.RemoveEmptyEntries);

                    if (arrayTags != null && arrayTags.Length > 0)
                    {
                        //div.Controls.Add(new LiteralControl("Tags:" + string.Join(", ", arrayTags)));
                        #region MyRegion
                        //DataRow[] docs_tags = NetFrontend.DAL.FileSystemDAL.ListDocumentsTagsRecordsBySqlConditions("Doc_DocTag = " + currDoc);
                        //string lista_tag = "";
                        //if (docs_tags.Length > 0)
                        //{
                        //    foreach (DataRow tag in docs_tags)
                        //        lista_tag += "'" + tag["Key_Tag"].ToString() + "',";
                        //    if (lista_tag.Length > 1)
                        //        lista_tag = lista_tag.Remove(lista_tag.Length - 1);

                        //    DataRow[] correlati = NetFrontend.DAL.FileSystemDAL.ListDocumentsTagsRecordsBySqlConditions("Key_Tag IN (" + lista_tag + ") AND Doc_DocTag <> " + currDoc);

                        //    List<DataRow> rows = new List<DataRow>(correlati);

                        //    var correlati_count = (from c in rows
                        //                           group c by c["Doc_DocTag"] into g
                        //                           orderby g.Count() descending
                        //                           select new
                        //                           {
                        //                               DocID = g.Key,
                        //                               Count = g.Count()
                        //                           }).Take(10); 
                        #endregion
                                              
                        // recupero da solr i documenti correlati 
                        List<AnnotateBusinessLogic.SemResult> results = AnnotateBusinessLogic.GetDocsByTags(arrayTags, idDocContent, MaxDocs);

                        if (results != null && results.Count > 0)
                        {
                            if (ShowTitle)
                            {
                                HtmlGenericControl h3 = new HtmlGenericControl("h3");
                                h3.InnerHtml = "Articoli Correlati";
                                div.Controls.Add(h3);
                            }

                            HtmlGenericControl lista = new HtmlGenericControl("ul");

                            HtmlGenericControl li;
                            foreach (AnnotateBusinessLogic.SemResult correlato in results)
                            {
                                li = new HtmlGenericControl("li");
                                li.InnerHtml = "<a href=\"" + correlato.Url + "\">" + correlato.Titolo + "</a>";
                                lista.Controls.Add(li);
                            }

                            div.Controls.Add(lista);
                        }

                        #region MyRegion
                        //foreach (var correlato in correlati_count)
                        //{
                        //    Document doc = DocumentBusinessLogic.GetDocumentRecordById(int.Parse(correlato.DocID.ToString()));
                        //    //DataRow doc = NetFrontend.DAL.FileSystemDAL.GetDocumentRecordByID(int.Parse(correlato.DocID.ToString()));
                        //    if (doc!= null)
                        //    {
                        //        //var document = (NetCms.Structure.WebFS.Document)NetCms.Networks.NetworksManager.CurrentActiveNetwork.RootFolder.FindDoc(doc[0]["id_Doc"].ToString());
                        //        //HtmlGenericControl li = new HtmlGenericControl("li");
                        //        //li.InnerHtml = "<a href=\"" + document.FrontendLink + "\">" + doc[0]["Label_DocLang"].ToString() + "</a>";
                        //        //lista.Controls.Add(li);
                        //        HtmlGenericControl li = new HtmlGenericControl("li");
                        //        li.InnerHtml = "<a href=\"" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/docs/doc.aspx?id=" + doc.ID + "\">" + doc.Nome + "</a>";
                        //        lista.Controls.Add(li);
                        //    }
                        //}
                        //correlati_count = null;
                        //rows.Clear();
                        //rows = null;
                        //correlati = null;

                        #endregion


                       
                    }
                }               
            }
        
            
            return div;
        }
    }
}

