﻿using System.Web.UI.HtmlControls;
using NetCms.Structure.Applications.Homepage.Model;
using G2Core.XhtmlControls;
using NetCms.Users;
using NetCms.Users.CustomAnagrafe;
using NetCms.Vertical;
using System.Web.UI.WebControls;
using System.Linq;
using NetCms.Structure.Applications.Homepage;
using System.Web.UI;

namespace NetFrontend.Homepages
{
    [NetCms.Homepages.HomepageModuleDefiner(55, "Notifiche", -1, NetCms.Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public class ModuloNotificheBlock : LayoutModuloBlock
    {
        protected override string LocalTypeCssClass
        {
            get { return "modulo_notifiche"; }
        }
        public override string Nome
        {
            get
            {
                return "Notifiche" + ID;
            }
        }
        private string _Title;
        public override string Title
        {
            get { return _Title; }
        }

        private bool ShowTile;

        public ModuloNotificheBlock(Modulo modulo)
            : base(modulo)
        {
            _Title = "Notifiche";

            ModuloNotifiche moduloNotifiche = HomePageBusinessLogic.GetModuloNotificheById(modulo.ID);
            ShowTile = moduloNotifiche.ShowTitle;
        }

        private string urlNotifichePage = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/notifiche/elenco.aspx";

        public override HtmlGenericControl GetLayoutBlock(PageData PageData)
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = this.CssClass;

            if (ShowTile)
            {
                WebControl h3 = new WebControl(HtmlTextWriterTag.H3);
                h3.Controls.Add(new LiteralControl("<a href=\"" + urlNotifichePage + "\">" + this.Title + "</a> "));
                div.Controls.Add(h3);
            }

            if (AccountManager.Logged)
            {
                WebControl containerCounter = new WebControl(System.Web.UI.HtmlTextWriterTag.Span);
                containerCounter.CssClass = "containerCounter";

                WebControl notificheCounter = new WebControl(System.Web.UI.HtmlTextWriterTag.Span);
                notificheCounter.CssClass = "notificheCounter";

                User user = AccountManager.CurrentAccount;

                Button button = new Button();
                button.Text = "Mostra le notifiche";
                button.Click += new System.EventHandler(button_Click);
              
                notificheCounter.Controls.Add(new LiteralControl(UsersBusinessLogic.CountNotificheFrontendNonLette(user).ToString()));

                containerCounter.Controls.Add(notificheCounter);
                                              
                div.Controls.Add(containerCounter);
                div.Controls.Add(button);               
            }

            return div;
        }

        void button_Click(object sender, System.EventArgs e)
        {
            //User user = AccountManager.CurrentAccount;
            //foreach (NotificaFrontend notifica in user.Notifiche.Where(x => x.Letta == false))
            //{
            //    notifica.Letta = true;
            //    UsersBusinessLogic.SaveOrUpdateNotifica(notifica);
            //}

            NetCms.Diagnostics.Diagnostics.Redirect(urlNotifichePage);
        }

    }
}
