﻿using System.Web.UI.HtmlControls;
using NetCms.Structure.Applications.Homepage.Model;
using G2Core.XhtmlControls;
using NetCms.Users;
using NetCms.Users.CustomAnagrafe;
using NetCms.Vertical;
using System.Web.UI;
using NetCms.Networks.Grants;
using LabelsManager;
using System.Web.UI.WebControls;
using LanguageManager.BusinessLogic;

namespace NetFrontend.Homepages
{
    [NetCms.Homepages.HomepageModuleDefiner(45, "Servizi", -1, NetCms.Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public class ModuloServiziBlock : LayoutModuloBlock
    {

        #region Labels

        /// <summary>
        /// CommonLabels
        /// </summary>
        public static Labels CommonLabel
        {
            get
            {
                return LabelsCache.GetTypedLabels(typeof(CommonLabels), LanguageBusinessLogic.CurrentLanguage.CultureCode_Lang) as Labels;
            }
        }


        /// <summary>
        /// Module Title
        /// </summary>
        public override string Title
        {
            get { return _Title; }
        }
        private string _Title = CommonLabel[CommonLabels.CommonLabelsList.ServicesModuleTitle];



        #endregion
         
        protected override string LocalTypeCssClass
        {
            get { return "modulo_servizi"; }
        }
        public override string Nome
        {
            get
            {
                return "Servizi" + ID;
            }
        }

        public ModuloServiziBlock(Modulo modulo)
            : base(modulo)
        {
           
        }

        public override HtmlGenericControl GetLayoutBlock(PageData PageData)
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "panel panel-default " + CssClass;

            Div panelHeading = new Div();
            panelHeading.Attributes["class"] = "panel-heading";

            HtmlGenericControl h3 = new HtmlGenericControl("h4");
            h3.InnerHtml = Title;
            h3.Attributes["class"] = "login-title";

            panelHeading.Controls.Add(h3);
            div.Controls.Add(panelHeading);

            User user = AccountManager.CurrentAccount;
            
            if (user != null && user.CustomAnagrafe != null)
            {  
                if (user.CustomAnagrafe.Applicazioni.Count > 0)
                {
                    
                    HtmlGenericControl ul = new HtmlGenericControl("ul");
                    ul.Attributes.Add("class", "list-group");


                    foreach (CustomAnagrafeApplicazioni app in user.CustomAnagrafe.Applicazioni)
                    {
                        //HtmlGenericControl ul = new HtmlGenericControl("ul");
                        //ul.Attributes.Add("class", "list-group");

                        VerticalApplicationWithService vapp = (VerticalApplicationWithService)VerticalApplicationsPool.GetByID(app.IdApplicazione);
                        if (vapp != null)
                        {
                            bool notShowLink = false;
                            //Controllo se per l'utente è stata impostata una specifica che nega i permessi per visualizzare l'applicazione anche se la sua custom anagrafe ha l'associazione
                            GrantsFinder grants = vapp.CriteriaByProfile(user.Profile);
                            if (grants != null)
                            {
                                notShowLink = grants["enable"].SpecifyFound && !grants["enable"].Allowed;
                            }

                            if (!notShowLink)
                            {
                                ul.InnerHtml += BuildServiceLink(vapp, user);

                            }
                        }
                    }

                    if (ul.HasControls())
                        div.Controls.Add(ul);

                    
                }
                else
                {
                    WebControl panelbody = new WebControl(HtmlTextWriterTag.Div);
                    panelbody.CssClass = "panel-body";


                    WebControl notify = new WebControl(HtmlTextWriterTag.Div);
                    notify.CssClass = "alert alert-info";
                    notify.Attributes.Add("role", "alert");
                    notify.Controls.Add(new LiteralControl(CommonLabel[CommonLabels.CommonLabelsList.NoServiceForUserMessage]));
                    panelbody.Controls.Add(notify);

                    div.Controls.Add(panelbody);

                    Div panelFooter = new Div();
                    panelFooter.Attributes["class"] = "panel-footer";
                    div.Controls.Add(panelFooter);
                }
            }
            else
            {
                WebControl panelbody = new WebControl(HtmlTextWriterTag.Div);
                panelbody.CssClass = "panel-body";

                WebControl notify = new WebControl(HtmlTextWriterTag.Div);
                notify.CssClass = "alert alert-info";
                notify.Attributes.Add("role", "alert");
                notify.Controls.Add(new LiteralControl(CommonLabel[CommonLabels.CommonLabelsList.NoServiceForUserMessage]));
                panelbody.Controls.Add(notify);

                div.Controls.Add(panelbody);

                Div panelFooter = new Div();
                panelFooter.Attributes["class"] = "panel-footer";
                div.Controls.Add(panelFooter);

            }
            
            

            return div;
        }

        private string BuildServiceLink(VerticalApplicationWithService vapp, User user)
        {
            if (vapp.Servizio.AttivazioneDifferita || vapp.Servizio.UtenteAttivo(user))
                return BuildElementHtml(vapp.Label, vapp.FrontendURL + "/default.aspx", "list-group-item ");
            else
            {
                return BuildElementHtml(vapp.Label, vapp.ActivationPage, "list-group-item " + vapp.SystemName);
            }
        }

        private string BuildElementHtml(string label, string href, string cssClass)
        {
            string template = @"<li{2}><a href=""{1}"">{0}</a></li>";
            if (!string.IsNullOrEmpty(cssClass))
                cssClass = " class=\"" + cssClass + "\"";
            return string.Format(template, label, href, cssClass);
        }
    }
}
