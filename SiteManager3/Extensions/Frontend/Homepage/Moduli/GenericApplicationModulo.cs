using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.Structure.Applications.Homepage;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Diagnostics;
using LabelsManager;
using LanguageManager.BusinessLogic;

/// <summary>
/// Summary description for LayoutModuloBlock
/// </summary>
/// 
namespace NetFrontend
{
    public abstract class GenericApplicationModulo : Homepages.LayoutModuloBlock
    {
        protected virtual string BaseSqlQuery
        {
            get
            {
                return "";
            }
        }
        public override string Nome
        {
            get
            {
                return "Modulo" + ID;
            }
        }

        public virtual string Label
        {
            get
            {
                return Folder.Label;
            }
        }

        protected bool SubFolders;
        protected int RecordsToShow;
        protected bool ShowDesc;
        protected int DescLength;
        protected bool ShowMoreLink;
        protected bool HideImages;
        protected string Template;

        protected StructureFolder Folder;
        protected int FolderID;
        protected string FolderLabel;
        protected string FolderPath;

        // CommonLabels
        
        public Labels FrontCommonLabels
        {
            get
            {
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(CommonLabels), LanguageBusinessLogic.CurrentLanguage.CultureCode_Lang) as Labels;
                
            }
        }

        public override bool AddBlock(PageData PageData)
        {
            string sql = BuildSelectQuery();
            return base.AddBlock(PageData) && CountRecords(sql)>0;
        }

        protected DataRow[] InitRecords(int limit = 15)
        {
            string sql = BuildSelectQuery();
            return SelectRecords(sql,0,limit, order: " Data_Doc DESC , ID_Doc DESC");
        }

        private string BuildSelectQuery()
        {
            string sql = this.BaseSqlQuery + (this.BaseSqlQuery.Length > 0 ? " AND " : "");

            StructureFolder folderData = FolderBusinessLogic.GetFolderRecordById(FolderID);
            if (SubFolders && folderData!= null)
                sql += "Tree_Folder LIKE '" + folderData.Tree + "%'";
            else
                sql += "id_Folder = " + FolderID;
            return sql;
        }

        public GenericApplicationModulo(Modulo modulo)
            : base(modulo)
        {
            ModuloApplicativo moduloApp = NetCms.Structure.Applications.Homepage.HomepageModulesBusinessLogic<ModuloApplicativo>.GetModuloById(modulo.ID); // modulo as ModuloApplicativo;

            int id = 0;
            if (moduloApp != null)
            {
                id = moduloApp.FolderId;
                
                SubFolders = moduloApp.Subfolders == 1; 
                RecordsToShow = moduloApp.Records; 
                ShowDesc = moduloApp.ShowDesc == 1;
                DescLength = moduloApp.DescLength; 
                ShowMoreLink = moduloApp.ShowMoreLink;
                HideImages = moduloApp.HideImages;
                Template = moduloApp.TemplateMode; // TODO: approfondire se necessario

                if (id > 0)
                {
                    Folder = FolderBusinessLogic.GetById(id);
                    FolderID = id;
                    IsValid = Folder != null;
                    FolderLabel = Folder.Label;
                    if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang == 0)
                        FolderPath = Folder.TranslatedPath;
                    else
                        FolderPath = Folder.Path;                
                }
            }
            else IsValid = false;
        }

        public override HtmlGenericControl GetLayoutBlock(PageData PageData)
        {
            return BlockContent();
        }

        ///// <summary>
        ///// Nuova implementazione per recuperare il contentuto del LayoutBlock
        ///// </summary>
        ///// <param name="PageData">Instance of PageData</param>
        ///// <param name="container">Web control dove appendere il contenuto</param>
        //public override void GetLayoutBlock(PageData PageData, Control container)
        //{            
        //    Content(container);
        //}

        protected abstract HtmlGenericControl BlockContent();
      

        //protected abstract void Content(Control container);

        /// Restituisce un set di documenti corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <param name="page">Un intero che rappresenta la pagina di database da restituire in caso di paginazione. La prima pagina � rappresentata dallo 0</param>
        /// <param name="recordCount">Un intero che rappresenta la il numero di record massimo da restituire per ogni pagina</param>
        /// <returns></returns>
        public abstract DataRow[] SelectRecords(string sqlConditions, int page = 0, int recordCount = 0, string order = null);

        /// Restituisce il documento corrispondente ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <returns></returns>
        public abstract DataRow GetRecord(string sqlConditions);

        /// Conta i documenti corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <returns></returns>
        public abstract int CountRecords(string sqlConditions);

        
    }
}