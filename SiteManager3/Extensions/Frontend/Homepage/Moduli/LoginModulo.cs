using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Applications.Homepage.Model;
using G2Core.XhtmlControls;
using NetCms.Users;
using LabelsManager;
using LanguageManager.BusinessLogic;

/// <summary>
/// Summary description for LayoutModuloBlock
/// </summary>
/// 
namespace NetFrontend.Homepages
{
    [NetCms.Homepages.HomepageModuleDefiner(21, "Login", -1, NetCms.Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public class ModuloLoginBlock : LayoutModuloBlock
    {
        #region Label

        /// <summary>
        /// LoginLabels
        /// </summary>
        public static Labels Labels
        {
            get
            {
                return LabelsCache.GetTypedLabels(typeof(LoginLabels), LanguageBusinessLogic.CurrentLanguage.CultureCode_Lang) as Labels;
            }
        }

        /// <summary>
        /// CommonLabels
        /// </summary>
        public static Labels CommonLabel
        {
            get
            {
                return LabelsCache.GetTypedLabels(typeof(CommonLabels), LanguageBusinessLogic.CurrentLanguage.CultureCode_Lang) as Labels;
            }
        }

        /// <summary>
        /// Module Title
        /// </summary>
        public override string Title
        {
            get { return _Title; }        
        }
        private string _Title = Labels[LoginLabels.LabelsList.LoginTitleLabel];

        #endregion

        protected override string LocalTypeCssClass
        {
            get { return "modulo_login"; }
        }
        public override string Nome
        {
            get
            {
                return "Login" + ID;
            }
        }

        private bool MostraRegistrazione;
        private bool MostraRecuperoPassword;

        private string LinkRegistrazione = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/registrazione/scelta.aspx";

        public ModuloLoginBlock(Modulo modulo)
            : base(modulo)
        {
            //_Title = "Login";
            MostraRegistrazione = (modulo as ModuloLogin).MostraLinkRegistrazione;
            MostraRecuperoPassword = (modulo as ModuloLogin).MostraRecuperaPassword;
        }

        public override HtmlGenericControl GetLayoutBlock(PageData PageData)
        {
            string redirecturl = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;
            Div div = new Div();
            div.Attributes["class"] = "panel panel-default " + CssClass;
            if (!NetCms.Users.AccountManager.Logged)
            {
                var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
                if (httpsHandler.IsSecureConnection)
                {
                    Div panelHeading = new Div();
                    panelHeading.Attributes["class"] = "panel-heading";

                    HtmlGenericControl h4 = new HtmlGenericControl("h4");
                    h4.InnerHtml = Title;
                    h4.Attributes["class"] = "login-title";
                    panelHeading.Controls.Add(h4);
                 
                    div.Controls.Add(panelHeading);
                 
                    div.Controls.Add(new LoginControlv3(redirecturl, MostraRegistrazione, MostraRecuperoPassword, LinkRegistrazione, true));
                 
                    Div panelFooter = new Div();
                    panelFooter.Attributes["class"] = "panel-footer";
                    div.Controls.Add(panelFooter);
                
                    return div;

                    //div.Controls.Add(new NetCms.Users.LoginControl(redirecturl, MostraRegistrazione, MostraRecuperoPassword, LinkRegistrazione));
                    //HtmlGenericControl ul = new HtmlGenericControl("ul"); 
                    //ul.InnerHtml += this.RegistrazioneUtente;
                    //div.Controls.Add(ul);
                }
                else
                {

                    Div panelHeading = new Div();
                    panelHeading.Attributes["class"] = "panel-heading";

                    string address = httpsHandler.GetCurrentPageAddressAsSecure();
                    HtmlGenericControl h4 = new HtmlGenericControl("h4");
                    h4.InnerHtml = "<a class=\"to-secure-connection\" href=\"{0}\"> " + Labels[LoginLabels.LabelsList.AccessButtonLabel] + " </a>".FormatByParameters(address);
                    h4.Attributes["class"] = "login-title";
                    panelHeading.Controls.Add(h4);

                    div.Controls.Add(panelHeading);

                    Div panelFooter = new Div();
                    panelFooter.Attributes["class"] = "panel-footer";
                    div.Controls.Add(panelFooter);

                }
            }
            else
            {
                NetCms.Users.User datiuser = NetCms.Users.AccountManager.CurrentAccount;

                string nomeCompleto = (!string.IsNullOrEmpty(datiuser.NomeCompleto)) ? datiuser.NomeCompleto : datiuser.UserName;
                
                div.Attributes["class"] += " Logged";

                Div panelHeading = new Div();
                panelHeading.Attributes["class"] = "panel-heading";

                WebControl LoggedTitle = new WebControl(HtmlTextWriterTag.H4);

                string welcomMessage = CommonLabel[CommonLabels.CommonLabelsList.WellcomeMsg].Replace("%nomeutente%", nomeCompleto);

                LoggedTitle.Controls.Add(new LiteralControl(welcomMessage));
                               
                panelHeading.Controls.Add(LoggedTitle);

                div.Controls.Add(panelHeading);
                
                HtmlGenericControl ul = new HtmlGenericControl("ul");
                ul.Attributes.Add("class", "list-group");
                ul.InnerHtml += this.Profilo;
                ul.InnerHtml += this.EditProfilo;
                ul.InnerHtml += this.ModificaPassword;

                if (NetCms.Configurations.Generics.NotifyEnabled) 
                    ul.InnerHtml += this.ElencoNotifiche; // Aggiungere un parametro che ne permette la visualizzazione
               
                if (ChiarimentiIsInstalled) // Se l'app chiarimenti � installata allora aggiungo il link
                    ul.InnerHtml += this.ElencoChiarimenti;

                ul.InnerHtml += this.Logout;

                div.Controls.Add(ul);

                Div panelFooter = new Div();
                panelFooter.Attributes["class"] = "panel-footer";
                div.Controls.Add(panelFooter);
            }
            return div;
        }

        public string Logout
        {
            get
            {
                string href = "?do=logout&amp;goto=" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageBusinessLogic.CurrentLanguage.ID != LanguageBusinessLogic.GetDefaultLanguage().ID)
                {
                    href = "?do=logout&amp;goto=" + "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.Length == 0 || NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.StartsWith("/") ? "" : "/") + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;
                }
                return BuildElementHtml("Logout", href, "list-group-item LogoutLink");
            }
        }

        //public string RegistrazioneUtente
        //{
        //    get
        //    {
        //        string href = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/registrazione/scelta.aspx";
        //        return BuildElementHtml("Registrazione Utente", href, "RegistrazioneUtenteLink"); ;
        //    }
        //}

        public string Profilo
        {
            get
            {
                string href = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/profilo.aspx";
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageBusinessLogic.CurrentLanguage.ID != LanguageBusinessLogic.GetDefaultLanguage().ID)
                {
                    href = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.Length == 0 || NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.StartsWith("/")? "" : "/") +  NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/profilo.aspx";
                }
                return BuildElementHtml(CommonLabel[CommonLabels.CommonLabelsList.ProfiloLabelLink] /*"Profilo Personale"*/, href, "list-group-item ProfiloLink");
            }
        }

        public string EditProfilo
        {
            get
            {
                string href = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/modifica_profilo.aspx";
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageBusinessLogic.CurrentLanguage.ID != LanguageBusinessLogic.GetDefaultLanguage().ID)
                {
                    href = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.Length == 0 || NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.StartsWith("/") ? "" : "/") + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/modifica_profilo.aspx";
                }
                return BuildElementHtml(CommonLabel[CommonLabels.CommonLabelsList.EditProfiloLabelLink] /*"Modifica Profilo Personale"*/, href, "list-group-item ModificaProfiloLink");
            }
        }

        public string ModificaPassword
        {
            get
            {
                string href = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/changepw.aspx";
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageBusinessLogic.CurrentLanguage.ID != LanguageBusinessLogic.GetDefaultLanguage().ID)
                {
                    href = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.Length == 0 || NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.StartsWith("/") ? "" : "/") + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/changepw.aspx";
                }
                return BuildElementHtml(CommonLabel[CommonLabels.CommonLabelsList.EditPasswordLabelLink] /*"Modifica Password"*/, href, "list-group-item ModificaPasswordLink");
            }
        }

        public string ElencoNotifiche
        {
            get {
                string href = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/notifiche/elenco.aspx";
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageBusinessLogic.CurrentLanguage.ID != LanguageBusinessLogic.GetDefaultLanguage().ID)
                {
                    href = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.Length == 0 || NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.StartsWith("/") ? "" : "/") + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/notifiche/elenco.aspx";
                }
                return BuildElementHtml(CommonLabel[CommonLabels.CommonLabelsList.NotificheLabelLink] /*"Elenco notifiche"*/, href, "list-group-item ElencoNotificheLink");
            }
        }

        public string ElencoChiarimenti
        {
            get
            {
                string href = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/chiarimenti/default.aspx";
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageBusinessLogic.CurrentLanguage.ID != LanguageBusinessLogic.GetDefaultLanguage().ID)
                {
                    href = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.Length == 0 || NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.StartsWith("/") ? "" : "/") + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/chiarimenti/default.aspx";
                }
                return BuildElementHtml(CommonLabel[CommonLabels.CommonLabelsList.ChiarimentiLabelLink] /*"Elenco chiarimenti"*/, href, "list-group-item ElencoChiarimentiLink");
            }
        }

        public string BuildElementHtml(string label, string href, string cssClass)
        {
            string template = @"<li{2}><a href=""{1}"">{0}</a></li>";
            if (!string.IsNullOrEmpty(cssClass))
                cssClass = " class=\"" + cssClass + "\"";
            return string.Format(template, label, href, cssClass);
        }


        private bool ChiarimentiIsInstalled
        {
            get
            {
                return NetCms.Vertical.VerticalApplicationsPool.ActiveAssemblies.Contains(240);
            }
        }
    
    }


}
