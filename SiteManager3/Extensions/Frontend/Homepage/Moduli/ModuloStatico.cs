using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS;

/// <summary>
/// Static Module
/// </summary>
/// <remarks>Modules can be: collapsible, thumbnail or simple panel</remarks>
namespace NetFrontend.Homepages
{
    [NetCms.Homepages.HomepageModuleDefiner(1, "Statico", -1, NetCms.Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public class ModuloStatico : LayoutModuloBlock
    {
        protected override string LocalTypeCssClass
        {
            get
            {
                return "modulo-statico";
            }                           
        }

        public override string Title
        {
            get { return Titolo; }
        }
        private string Titolo;

        private string Testo;
        private string Link;
        private string LinkTitle;
        private string Img;
        private string ImgDesc;
        private string ImgAlt = "";
        private string ImgAlign;
        private string Template;

        public BlockLayout blockLayout
        {
            get
            {       
                if (_blockLayout == BlockLayout.Default)
                {
                    if (!String.IsNullOrEmpty(this.CssClass))
                    {
                        if (this.CssClass.Contains("panel") && !this.CssClass.Contains("collapsible"))
                            _blockLayout = BlockLayout.Panel;
                        if (this.CssClass.Contains("thumbnail"))
                            _blockLayout = BlockLayout.Thumbnail;
                        if (this.CssClass.Contains("collapsible"))
                            _blockLayout = BlockLayout.PanelCollapse;
                    }
                }
                return _blockLayout;
            }
        }
        
        private BlockLayout _blockLayout;
      

        private string _Nome;
        public override string Nome
        {
            get
            {
                return _Nome;
            }
        }

        public ModuloStatico(Modulo modulo)
            : base(modulo)
        {
            //NetCms.Structure.Applications.Homepage.Model.ModuloStatico moduloStatico = (NetCms.Structure.Applications.Homepage.Model.ModuloStatico)modulo;
            NetCms.Structure.Applications.Homepage.Model.ModuloStatico moduloStatico = NetCms.Structure.Applications.Homepage.HomepageModulesBusinessLogic<NetCms.Structure.Applications.Homepage.Model.ModuloStatico>.GetModuloById(modulo.ID);

            //DataRow row_moduli_statici = NetFrontend.DAL.HomepageDAL.GetModuloStaticoBySqlConditions("rif_modulo_modulo_statico = " + row.IntValue("ID_modulo"));

            if (moduloStatico != null)
            {                
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang == 0)
                {
                    if (moduloStatico.ModuloLangs.Count(x=>x.Lang == LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.ID) > 0)
                    {
                        ModuloStaticoLang moduloLang = moduloStatico.ModuloLangs.First(x => x.Lang == LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.ID);
                        InitFromModuloLangData(moduloLang);
                    }
                    else
                        InitFromModuloData(moduloStatico);
                }
                else
                {
                    InitFromModuloData(moduloStatico);
                }
            }

        }

        private void InitFromModuloData(Modulo modulo)
        {
            NetCms.Structure.Applications.Homepage.Model.ModuloStatico moduloStatico = modulo as NetCms.Structure.Applications.Homepage.Model.ModuloStatico;

            _Nome = "ModuloStatico" + moduloStatico.ID; //row_moduli_statici["rif_modulo_modulo_statico"].ToString();

            Titolo = moduloStatico.Titolo;//row_moduli_statici["titolo_modulo_statico"].ToString();
            Testo = moduloStatico.Testo; //row_moduli_statici["testo_modulo_statico"].ToString();
            Link = moduloStatico.Link;//row_moduli_statici["link_modulo_statico"].ToString();
            LinkTitle = moduloStatico.Title;//row_moduli_statici["title_modulo_statico"].ToString();
            Template = moduloStatico.TemplateMode;

            int id_img = -1;
            if (moduloStatico.Img != null && moduloStatico.Img.Length > 0)
                id_img = int.Parse(moduloStatico.Img); //row_moduli_statici.IntValue("img_modulo_statico");

            if (id_img > 0)
            {
                Document doc = DocumentBusinessLogic.GetDocumentRecordById(id_img);
                //DataRow doc = NetFrontend.DAL.FileSystemDAL.GetDocumentRecordByID(id_img);
                if (doc != null)
                {
                    string nome_file = doc.PhysicalName;//doc["Name_Doc"].ToString();
                    this.ImgDesc = doc.Descrizione; //doc["Desc_DocLang"].ToString();
                    int Folder_doc = doc.Folder.ID; //doc.IntValue("Folder_doc");
                    string path_Folder = null;

                    //DataRow fold = NetFrontend.DAL.FileSystemDAL.GetFolderRecordByID(Folder_doc);

                    if (doc.Folder != null)
                    {

                        path_Folder = doc.Folder.Path; //fold["Path_Folder"].ToString();

                        if (LanguageManager.Utility.Config.EnableMultiLanguage)
                        {
                            this.Img = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetUrlWithoutLanguage(NetCms.Front.FrontendNetwork.GetAbsoluteWebRoot() + path_Folder + "/" + nome_file);
                        }
                        else
                        {
                            this.Img = NetCms.Front.FrontendNetwork.GetAbsoluteWebRoot() + path_Folder + "/" + nome_file;
                        }

                        this.ImgAlt = moduloStatico.ImgAlt; //row_moduli_statici["imgAlt_modulo_statico"].ToString();

                        this.ImgAlign = moduloStatico.ImgAll; //row_moduli_statici["imgAll_modulo_statico"].ToString();
                    }
                }
            }
        }

        private void InitFromModuloLangData(ModuloStaticoLang moduloLang)
        {
            
            _Nome = "ModuloStatico" + moduloLang.ModuloParent.ID; //row_moduli_statici["rif_modulo_modulo_statico"].ToString();

            Titolo = moduloLang.Titolo;//row_moduli_statici["titolo_modulo_statico"].ToString();
            Testo = moduloLang.Testo; //row_moduli_statici["testo_modulo_statico"].ToString();
            Link = moduloLang.Link;//row_moduli_statici["link_modulo_statico"].ToString();
            LinkTitle = moduloLang.Title;//row_moduli_statici["title_modulo_statico"].ToString();
            Template = moduloLang.ModuloParent.TemplateMode;

            int id_img = -1;
            if (moduloLang.Img != null && moduloLang.Img.Length > 0)
                id_img = int.Parse(moduloLang.Img); //row_moduli_statici.IntValue("img_modulo_statico");

            if (id_img > 0)
            {
                Document doc = DocumentBusinessLogic.GetDocumentRecordById(id_img);
                //DataRow doc = NetFrontend.DAL.FileSystemDAL.GetDocumentRecordByID(id_img);
                if (doc != null)
                {
                    string nome_file = doc.PhysicalName;//doc["Name_Doc"].ToString();
                    this.ImgDesc = doc.Descrizione; //doc["Desc_DocLang"].ToString();
                    int Folder_doc = doc.Folder.ID; //doc.IntValue("Folder_doc");
                    string path_Folder = null;

                    //DataRow fold = NetFrontend.DAL.FileSystemDAL.GetFolderRecordByID(Folder_doc);

                    if (doc.Folder != null)
                    {

                        path_Folder = doc.Folder.Path; //fold["Path_Folder"].ToString();

                        this.Img = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetUrlWithoutLanguage(NetCms.Front.FrontendNetwork.GetAbsoluteWebRoot() + path_Folder + "/" + nome_file);

                        this.ImgAlt = moduloLang.ImgAlt; //row_moduli_statici["imgAlt_modulo_statico"].ToString();

                        this.ImgAlign = moduloLang.ImgAll; //row_moduli_statici["imgAll_modulo_statico"].ToString();
                    }
                }
            }
        }

        public override HtmlGenericControl GetLayoutBlock(PageData PageData)
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = this.CssClass;

            if (this.Template == "No_Template" || string.IsNullOrEmpty(this.Template))
            {
                #region EmbeddedHtml
                switch (blockLayout)
                {
                    case BlockLayout.Panel:

                        BuildPanel(div, PageData.AbsoluteWebRoot);

                        break;
                    case BlockLayout.PanelCollapse:
                        string scriptIcon = @"$(document).on('click', 'span.clickable', function(e){
                                var $this = $(this);
                                if(!$this.hasClass('collapsed')) {
                                        $this.addClass('collapsed');
                                    	$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                                }
                                else
                                {
                                        $this.removeClass('collapsed');
                                        $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');

                                }
                                })";

                        LiteralControl script = new LiteralControl("<script> " + scriptIcon + " </script>");
                        script.ID = "scriptcollapsible";
                        // Da ottimizzare 
                        Control r = PageData.Body.FindControl(script.ID);

                        if (r == null)
                            PageData.Body.Controls.Add(script);

                        BuildPanelCollapse(div, PageData.AbsoluteWebRoot);
                        break;
                    case BlockLayout.Thumbnail:

                        BuildThumbnail(div, PageData.AbsoluteWebRoot);
                        break;
                    default:                                                    
                        BuildPanel(div, PageData.AbsoluteWebRoot);
                        break;                        
                }


                foreach (var child in this.ChildBlocks.Values.Where(x => x.AddBlock(PageData)))
                    div.Controls.Add(child.GetLayoutBlock(PageData)); 
                #endregion
            }
            else
            {
                return GetTemplateContent();               
            }
            return div;
        }

        public void BuildPanel(HtmlGenericControl div,string pageWebRoot)
        {
            #region Panel Heading
            div.Attributes["class"] = "panel panel-default " + this.CssClass;
            if (this.Titolo != null && this.Titolo.Length > 0)
            {
                HtmlGenericControl divPanelHeading = new HtmlGenericControl("div");
                divPanelHeading.Attributes["class"] = "panel-heading";
                HtmlGenericControl h4 = new HtmlGenericControl("h4");
                if (this.Link != null && this.Link != "")
                {

                    HtmlGenericControl a = new HtmlGenericControl("a");
                    a.Attributes["href"] = this.Link.Replace(NetCms.Structure.Utility.ImportContentFilter.WebRoot, pageWebRoot);
                    if (this.LinkTitle != null && this.Link != "")
                    {
                        a.Attributes["title"] = this.LinkTitle;
                    }
                    a.InnerHtml = this.Titolo;
                    h4.Controls.Add(a);
                    divPanelHeading.Controls.Add(h4);
                }
                else
                {
                    h4.InnerHtml = this.Titolo;
                    divPanelHeading.Controls.Add(h4);
                }
                div.Controls.Add(divPanelHeading);
            }
            #endregion
            #region Image
            if (this.Img != null && this.Img != "")
            {
                HtmlGenericControl divimg = new HtmlGenericControl("div");
                divimg.Attributes["class"] = "thumbnail clearfix";
                string image = "<img src=\"" + this.Img + "\"";

                if (this.ImgAlt != null && this.ImgAlt != "")
                {
                    image += " alt=\"" + this.ImgAlt + "\"";
                }

                #region allineamento_img
                if (this.ImgAlign != null && this.ImgAlign != "")
                {
                    if (this.ImgAlign == "0")
                        image += " class=\"pull-left\""; 

                    //if (this.ImgAlign == "1")
                    //    divimg.Attributes["class"] = "text-center";

                    if (this.ImgAlign == "2")
                        image += " class=\"pull-right\"";
                }
                #endregion

                image += " />";

                divimg.InnerHtml = image;

                div.Controls.Add(divimg);
            }
            #endregion
            #region Body
            
            if (this.Testo != null)
            {
                HtmlGenericControl p = new HtmlGenericControl("div");
                p.InnerHtml = this.Testo.Replace(NetCms.Structure.Utility.ImportContentFilter.WebRoot, pageWebRoot);
                p.Attributes["class"] = "panel-body modulo_content ";
                div.Controls.Add(p);

                HtmlGenericControl clr = new HtmlGenericControl("div");
                clr.Attributes["class"] = "clr";
                div.Controls.Add(clr);
            }
            #endregion
            #region Panel Footer
            HtmlGenericControl divFooter = new HtmlGenericControl("div");
            divFooter.Attributes["class"] = "panel-footer";
            div.Controls.Add(divFooter);

            #endregion
        }

        public void BuildThumbnail(HtmlGenericControl div,string pageWebRoot)
        {
            HtmlGenericControl figure = new HtmlGenericControl("figure");
            figure.Attributes["class"] = "figure_modulo_statico";

            #region Image
            if (this.Img != null && this.Img != "")
            {
                HtmlGenericControl divimg = new HtmlGenericControl("img");
                divimg.Attributes["src"] =  this.Img;

                if (this.ImgAlt != null && this.ImgAlt != "")
                {
                    divimg.Attributes["alt"] = this.ImgAlt;
                   
                }
                #region allineamento_img
                if (this.ImgAlign != null && this.ImgAlign != "")
                {
                    if (this.ImgAlign == "0")
                    {
                        divimg.Attributes["class"] += "pull-left";
                        div.Attributes["class"] += " clearfix";
                    }
                    //if (this.ImgAlign == "1")
                    //    divimg.Attributes["class"] = "center";

                    if (this.ImgAlign == "2")
                    {
                        divimg.Attributes["class"] += "pull-right";
                        div.Attributes["class"] += " clearfix";
                    }
                }
                #endregion

                //image += " />";

                //divimg.InnerHtml = image;

                divimg.Attributes["class"] += " img-responsive ";
                
                figure.Controls.Add(divimg);
            }
            #endregion

            HtmlGenericControl figcaption = new HtmlGenericControl("figcaption");
            figcaption.Attributes["class"] = "figcaption_modulo_statico";

            HtmlGenericControl divCaption = new HtmlGenericControl("div");
            divCaption.Attributes["class"] = "caption";
            if (this.Titolo != null && this.Titolo.Length > 0)
            {
                HtmlGenericControl h4 = new HtmlGenericControl("h4");
                if (this.Link != null && this.Link != "")
                {
                    HtmlGenericControl a = new HtmlGenericControl("a");
                    a.Attributes["href"] =  this.Link.Replace(NetCms.Structure.Utility.ImportContentFilter.WebRoot, pageWebRoot);
                    if (this.LinkTitle != null && this.Link != "")
                    {
                        a.Attributes["title"] = this.LinkTitle;
                    }
                    a.InnerHtml = this.Titolo;
                    h4.Controls.Add(a);
                    divCaption.Controls.Add(h4);
                }
                else
                {
                    h4.InnerHtml = this.Titolo;
                    divCaption.Controls.Add(h4);
                }
            }

            #region Body

            if (this.Testo != null)
            {
                HtmlGenericControl p = new HtmlGenericControl("div");
                p.InnerHtml = this.Testo.Replace(NetCms.Structure.Utility.ImportContentFilter.WebRoot, pageWebRoot);
                p.Attributes["class"] = "modulo_content";
                divCaption.Controls.Add(p);

                HtmlGenericControl clr = new HtmlGenericControl("div");
                clr.Attributes["class"] = "clr";
                divCaption.Controls.Add(clr);
              
            }

            figcaption.Controls.Add(divCaption);
            figure.Controls.Add(figcaption);


            div.Controls.Add(figure);
            #endregion
        }

        public void BuildPanelCollapse(HtmlGenericControl div, string pageWebRoot)
        {
            #region Panel Heading
            
            div.Attributes["class"] = "panel panel-default " + this.CssClass;
            if (this.Titolo != null && this.Titolo.Length > 0)
            {
                HtmlGenericControl divPanelHeading = new HtmlGenericControl("div");
                divPanelHeading.Attributes["class"] = "panel-heading";
                HtmlGenericControl h4 = new HtmlGenericControl("h4");
                if (this.Link != null && this.Link != "")
                {

                    HtmlGenericControl a = new HtmlGenericControl("a");
                    a.Attributes["href"] = this.Link.Replace(NetCms.Structure.Utility.ImportContentFilter.WebRoot, pageWebRoot);
                    if (this.LinkTitle != null && this.Link != "")
                    {
                        a.Attributes["title"] = this.LinkTitle;
                    }
                    a.InnerHtml = this.Titolo + " ";
                    h4.Controls.Add(a);
                    
                }
                else
                {
                    h4.InnerHtml = this.Titolo + " ";
                   
                }
                
                HtmlGenericControl span = new HtmlGenericControl("span");
                span.Attributes["class"] = "clickable";
                HtmlGenericControl i = new HtmlGenericControl("i");
                i.Attributes["class"] = "glyphicon glyphicon-chevron-up";
                i.Attributes["data-toggle"] = "collapse";
                i.Attributes["data-target"] = "#"  + this.ID.ToString();
                span.Controls.Add(i);
                h4.Controls.Add(span);
                divPanelHeading.Controls.Add(h4);
                
                div.Controls.Add(divPanelHeading);
            }
            #endregion
            HtmlGenericControl collDiv = new HtmlGenericControl("div");
            collDiv.Attributes["id"] = this.ID.ToString();
            collDiv.Attributes["aria-expanded"] = "true";
            collDiv.Attributes["class"] = "collapse in";

            #region Image
            if (this.Img != null && this.Img != "")
            {
                HtmlGenericControl divimg = new HtmlGenericControl("div");
                divimg.Attributes["class"] = "thumbnail clearfix";
                string image = "<img src=\"" + this.Img + "\"";

                if (this.ImgAlt != null && this.ImgAlt != "")
                {
                    image += " alt=\"" + this.ImgAlt + "\"";
                }

                #region allineamento_img
                if (this.ImgAlign != null && this.ImgAlign != "")
                {
                    if (this.ImgAlign == "0")
                        image += " class=\"pull-left\"";

                    //if (this.ImgAlign == "1")
                    //    divimg.Attributes["class"] = "text-center";

                    if (this.ImgAlign == "2")
                        image += " class=\"pull-right\"";
            }
                #endregion

                image += " />";

                divimg.InnerHtml = image;
                collDiv.Controls.Add(divimg);
                //div.Controls.Add(divimg);
            }
            #endregion
            #region Body

            if (this.Testo != null)
            {
                HtmlGenericControl p = new HtmlGenericControl("div");
                p.InnerHtml = this.Testo.Replace(NetCms.Structure.Utility.ImportContentFilter.WebRoot, pageWebRoot);
                p.Attributes["class"] = "panel-body modulo_content ";
                collDiv.Controls.Add(p);
                //div.Controls.Add(p);

                HtmlGenericControl clr = new HtmlGenericControl("div");
                clr.Attributes["class"] = "clearfix";
                collDiv.Controls.Add(clr);
            }
            #endregion
            div.Controls.Add(collDiv);
            #region Panel Footer
            HtmlGenericControl divFooter = new HtmlGenericControl("div");
            divFooter.Attributes["class"] = "panel-footer";
            div.Controls.Add(divFooter);

            #endregion


        }
        public enum BlockLayout
        {
            Default,
            Thumbnail, 
            Panel, 
            PanelCollapse 
        }


        private string GetImgAlign(string imgAlign)
        {
            string align = "";

            if (imgAlign == "0")
                align = "pull-left";
            if (imgAlign == "2")
                align = "pull-right";
            if (imgAlign == "1")
                align = "center";
            return align;
        }

        public HtmlGenericControl GetTemplateContent()
        {

            Object img = new { imgsrc = (!string.IsNullOrEmpty(Img) ? Img : "false"),
                               imgalt = ImgDesc,
                               imgalign = (!string.IsNullOrEmpty(ImgAlign) ? GetImgAlign(ImgAlign)  : "false")};

            Object currentmodulo = new { titolo = (!string.IsNullOrEmpty(Titolo) ? Titolo : "false"),
                                         testo = Testo,
                                         url = (!string.IsNullOrEmpty(Link) ? Link : "false"),
                                         urltitle = LinkTitle,
                                         cssclass = (!string.IsNullOrEmpty(CssClass) ? CssClass : "false"),
                                         img = img };
            Object dati = new { modulo = currentmodulo };

            string tpl_path = GetModuleTemplatePath(Template, "");

            string src = string.Empty;
            src = RenderTemplate(dati, tpl_path, null);


            HtmlGenericControl cSrc = new HtmlGenericControl("div");
            cSrc.Attributes["class"] = this.CssClass;
            cSrc.InnerHtml = src;

            return cSrc;
        }
    }
}