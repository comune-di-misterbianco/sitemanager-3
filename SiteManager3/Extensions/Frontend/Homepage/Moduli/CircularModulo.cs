using System.Web.UI.HtmlControls;
using NetCms.Structure.Applications.Homepage.Model;

/// <summary>
/// Summary description for LayoutModuloBlock
/// </summary>
/// 
namespace NetFrontend.Homepages
{
    [NetCms.Homepages.HomepageModuleDefiner(20, "Circolare", -1, NetCms.Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public class CircularModuloBlock : LayoutModuloBlock
    {
        protected override string LocalTypeCssClass
        {
            get { return "modulo_circolare"; }
        }
        public override string Nome
        {
            get
            {
                return "Circular" + ID;
            }
        }
        public override string Title
        {
            get { return "Circular"; }
        }

        private ModuloCircolare DbData;

        public CircularModuloBlock(Modulo modulo)
            : base(modulo)
        {
            ModuloCircolare moduloCircolare = modulo as ModuloCircolare;
            
            if (moduloCircolare != null)
            {            
                DbData = moduloCircolare;
            }
        }

        public override HtmlGenericControl GetLayoutBlock(PageData PageData)
        {
            LayoutBlock modulo = Homepage.BlocksFactory(DbData.ModuloCollegato);
            modulo.Parent = this.Parent;
            
            return modulo.GetLayoutBlock(PageData);
        }
    }


}
