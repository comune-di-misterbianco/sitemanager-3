﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using NetCms.Structure.Applications.Homepage.Model;
using G2Core.XhtmlControls;
using LabelsManager;
using LanguageManager.BusinessLogic;

/// <summary>
/// Summary description for LayoutModuloBlock
/// </summary>
/// 
namespace NetFrontend.Homepages
{
    [NetCms.Homepages.HomepageModuleDefiner(29, "LoginAdvanced", -1, NetCms.Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public class ModuloLoginAdvancedBlock : LayoutModuloBlock
    {
        private Labels Labels
        {
            get
            {
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(CommonLabels), LanguageBusinessLogic.CurrentLanguage.CultureCode_Lang) as Labels;
            }
        }

        protected override string LocalTypeCssClass
        {
            get { return "modulo_login"; }
        }
        public override string Nome
        {
            get
            {
                return "Login" + ID;
            }
        }
        private string _Title;
        public override string Title
        {
            get { return _Title; }
        }

        //private bool MostraRegistrazione;
        //private bool MostraRecuperoPassword;
        //private bool ShowOnlyADCtrl;
        //private string SSOPageUrl;

        private string LinkRegistrazione = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/registrazione/scelta.aspx";

        public ModuloLoginAdvancedBlock(Modulo modulo)
            : base(modulo)
        {
            _Title = "Login";
            //MostraRegistrazione = (modulo as ModuloLoginAdvanced).MostraLinkRegistrazione;
            //MostraRecuperoPassword = (modulo as ModuloLoginAdvanced).MostraRecuperaPassword;
            //ShowOnlyADCtrl = (modulo as ModuloLoginAdvanced).MostraSoloControlliAD;
            //SSOPageUrl = (modulo as ModuloLoginAdvanced).SSOPageUrl;
        }

        public override HtmlGenericControl GetLayoutBlock(PageData PageData)
        {
            string redirecturl = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;
            Div div = new Div();
            div.Attributes["class"] = CssClass;
            //if (!NetCms.Users.AccountManager.Logged)
            //{
            //    var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();
            //    if (httpsHandler.IsSecureConnection)
            //    {
            //        HtmlGenericControl h3 = new HtmlGenericControl("h3");
            //        h3.InnerHtml = "Login";
            //        h3.Attributes["class"] = "login-title";
            //        div.Controls.Add(h3);

            //        div.Controls.Add(new NetCms.Users.LoginControlv2_AD(redirecturl, SSOPageUrl, MostraRegistrazione, MostraRecuperoPassword, ShowOnlyADCtrl, LinkRegistrazione));
            //    }
            //    else
            //    {
            //        string address = httpsHandler.GetCurrentPageAddressAsSecure();
            //        HtmlGenericControl h3 = new HtmlGenericControl("h3");
            //        h3.InnerHtml = "<a class=\"to-secure-connection\" href=\"{0}\">Login</a>".FormatByParameters(address);
            //        h3.Attributes["class"] = "login-title";
            //        div.Controls.Add(h3);
            //    }
            //}
            if (NetCms.Users.AccountManager.Logged)
            {
                NetCms.Users.User datiuser = NetCms.Users.AccountManager.CurrentAccount;

                string nomeCompleto = (!string.IsNullOrEmpty(datiuser.NomeCompleto)) ? datiuser.NomeCompleto : datiuser.UserName;

                div.Attributes["class"] += " Logged";
                HtmlGenericControl p = new HtmlGenericControl("p");
                div.Controls.Add(p);

                string strWellcomeMsg = Labels[CommonLabels.CommonLabelsList.WellcomeMsg].Replace("%nomeutente%", nomeCompleto);

                p.InnerHtml = "<span class=\"userinfo\">" + strWellcomeMsg + "</span>";

                HtmlGenericControl ul = new HtmlGenericControl("ul");

                //ul.InnerHtml += this.Profilo;

                //if (!datiuser.UseExternalAuthSystem)
                //    ul.InnerHtml += this.EditProfilo;
                //if (!datiuser.UseExternalAuthSystem)
                //    ul.InnerHtml += this.ModificaPassword;

                if (NetCms.Configurations.Generics.NotifyEnabled)
                    ul.InnerHtml += this.ElencoNotifiche; // Aggiungere un parametro che ne permette la visualizzazione

                if (ChiarimentiIsInstalled) // Se l'app chiarimenti è installata allora aggiungo il link
                    ul.InnerHtml += this.ElencoChiarimenti;

                ul.InnerHtml += this.Logout;

                div.Controls.Add(ul);
            }
            return div;
        }

        // ha senso per la modalità standard e mixed 
        // verificare quando nasconderlo e o come eseguire il logout per l'utente ad 
        public string Logout
        {
            get
            {
                string href = "?do=logout&amp;goto=" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageBusinessLogic.CurrentLanguage.ID != LanguageBusinessLogic.GetDefaultLanguage().ID)
                {
                    href = "?do=logout&amp;goto=" + "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.Length == 0 || NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.StartsWith("/") ? "" : "/") + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;
                }
                return BuildElementHtml(Labels[CommonLabels.CommonLabelsList.LogoutLabelLink], href, "LogoutLink");
            }
        }

        //public string Profilo
        //{
        //    get
        //    {
        //        string href = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/profilo.aspx";
        //        return BuildElementHtml(Labels[CommonLabels.CommonLabelsList.ProfiloLabelLink], href, "ProfiloLink");
        //    }
        //}

        //public string EditProfilo
        //{
        //    get
        //    {
        //        string href = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/modifica_profilo.aspx";
        //        return BuildElementHtml(Labels[CommonLabels.CommonLabelsList.EditProfiloLabelLink], href, "ModificaProfiloLink");
        //    }
        //}

        //public string ModificaPassword
        //{
        //    get
        //    {
        //        string href = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/changepw.aspx";
        //        return BuildElementHtml(Labels[CommonLabels.CommonLabelsList.EditPasswordLabelLink], href, "ModificaPasswordLink");
        //    }
        //}

        public string ElencoNotifiche
        {
            get
            {
                string href = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/notifiche/elenco.aspx";
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageBusinessLogic.CurrentLanguage.ID != LanguageBusinessLogic.GetDefaultLanguage().ID)
                {
                    href = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.Length == 0 || NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.StartsWith("/") ? "" : "/") + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/notifiche/elenco.aspx";
                }
                return BuildElementHtml(Labels[CommonLabels.CommonLabelsList.NotificheLabelLink], href, "ElencoNotificheLink");
            }
        }

        public string ElencoChiarimenti
        {
            get
            {
                string href = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/chiarimenti/default.aspx";
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageBusinessLogic.CurrentLanguage.ID != LanguageBusinessLogic.GetDefaultLanguage().ID)
                {
                    href = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.Length == 0 || NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.StartsWith("/") ? "" : "/") + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/chiarimenti/default.aspx";
                }
                return BuildElementHtml(Labels[CommonLabels.CommonLabelsList.ChiarimentiLabelLink], href, "ElencoChiarimentiLink");
            }
        }

        public string BuildElementHtml(string label, string href, string cssClass)
        {
            string template = @"<li{2}><a href=""{1}"">{0}</a></li>";
            if (!string.IsNullOrEmpty(cssClass))
                cssClass = " class=\"" + cssClass + "\"";
            return string.Format(template, label, href, cssClass);
        }

        private bool ChiarimentiIsInstalled
        {
            get
            {
                return NetCms.Vertical.VerticalApplicationsPool.ActiveAssemblies.Contains(240);
            }
        }

    }


}
