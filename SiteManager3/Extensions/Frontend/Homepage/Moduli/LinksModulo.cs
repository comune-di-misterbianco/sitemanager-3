//using System;
//using System.Data;
//using System.Configuration;
//using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
//using System.Xml;
//using NetCms.Structure.Applications.Homepage.Model;
//using NetCms.Structure.Applications.Homepage;
//using NetCms.Structure.WebFS.FileSystemBusinessLogic;
//using NetCms.Structure.WebFS;
//using NetCms.Networks.WebFS;
//using System.Collections.Generic;
//using System.Linq;

///// <summary>
///// Summary description for LayoutModuloBlock
///// </summary>
///// 
//namespace NetFrontend.Homepages
//{
//    public class LayoutModuloLinkBlock : LayoutModuloBlock
//    {
//        protected override string LocalTypeCssClass
//        {
//            get { return "modulo_link"; }
//        }
        
//        public override string Nome
//        {
//            get
//            {
//                return Folder.Label;
//            }
//        }

//        public override string Title
//        {
//            get { return Nome; }
//        }

//        public override bool HasModuli
//        {
//            get { return false; }
//        }

//        private string OrderString
//        {
//            get
//            {
//                string output = "";
//                switch (Order)
//                {
//                    case 0:
//                        output = " Data_Doc DESC";
//                        break;
//                    case 1:
//                        output = " Clicks_Link DESC";
//                        break;
//                    case 2:
//                        output = " Label_DocLang";
//                        break;
//                }
//                return output;
//            }
//        }
//        private int Order;
//        private bool SubFolders;
//        private int Records;
//        private bool ShowDesc;
//        private int DescLength;

//        private StructureFolder Folder;
//        private int FolderID;
//        private String FolderTree;

//        public LayoutModuloLinkBlock(Modulo modulo)
//            : base(modulo)
//        {
//            ModuloApplicativo moduloApp = HomePageBusinessLogic.GetModuloById(modulo.ID) as ModuloApplicativo;
//            //DataRow rowModuloApplicativo = NetFrontend.DAL.HomepageDAL.GetModuloApplicativoByID(row.IntValue("id_Modulo"));
//            int id = moduloApp.FolderId; //int.Parse(rowModuloApplicativo["Folder_ModuloApplicativo"].ToString());

//            Order = moduloApp.Order; //int.Parse(rowModuloApplicativo["Order_ModuloApplicativo"].ToString());
//            SubFolders = moduloApp.Subfolders == 1; //rowModuloApplicativo["SubFolders_ModuloApplicativo"].ToString() == "1";
//            Records = moduloApp.Records; //int.Parse(rowModuloApplicativo["Records_ModuloApplicativo"].ToString());
//            ShowDesc = moduloApp.ShowDesc == 1; //rowModuloApplicativo["ShowDesc_ModuloApplicativo"].ToString() == "1";
//            DescLength = moduloApp.DescLength; //int.Parse(rowModuloApplicativo["DescLength_ModuloApplicativo"].ToString());

//            Folder = FolderBusinessLogic.GetById(id); //NetFrontend.DAL.FileSystemDAL.GetFolderRecordByID(id);
//            FolderID = id;
//            FolderTree = Folder.Tree; //Folder["Tree_Folder"].ToString();

//        }

//        public override HtmlGenericControl GetLayoutBlock(PageData PageData)
//        {
//            HtmlGenericControl div = new HtmlGenericControl("div");
//            div.Attributes["class"] = CssClass;

//            HtmlGenericControl contentModulo = new HtmlGenericControl("div");
//            List<int> fIds = new List<int>();
//            fIds.Add(FolderID);
//            //string sql = "Folder_Doc = " + FolderID;
//            if (SubFolders)
//            {
                
//                //DataRow[] folders = NetFrontend.DAL.FileSystemDAL.ListFoldersRecordsBySqlConditions("Tree_Folder LIKE '%." + FolderTree + ".%' AND id_Folder <> " + FolderID);
//                ////sql = "";
//                //for (int i = 0; i < folders.Length; i++)
//                List<StructureFolder> subfolders = Folder.Folders as List<StructureFolder>;
//                foreach(StructureFolder folder in subfolders.Where(x=>x.Trash == 0))
//                {
//                    fIds.Add(folder.ID);
//                    //sql += " OR ";
//                    //sql += " Folder_Doc = " + folders[i]["id_Folder"].ToString();
//                }
//            }
            
//            DataRow[] Links = NetFrontend.DAL.LinksDAL.ListDocumentRecordsBySqlConditions(sql, order: OrderString);

//            for (int i = 0; i < (Links.Length <= Records ? Links.Length : Records); i++)
//            {
//                //contentModulo.InnerHtml += "<li><a href=\"" + PageData.PageName+"?extlink=" + Links[i]["id_Link"].ToString() + "\"><span>" + Links[i]["Label_DocLang"] + "</span></a>";
//                contentModulo.InnerHtml += "<li><a href=\"" + Links[i]["Href_Link"].ToString() + "\"><span>" + Links[i]["Label_DocLang"] + "</span></a>";
//                if (ShowDesc)
//                {
//                    string desc = Links[i]["Desc_DocLang"].ToString();
//                    if (desc.Length > DescLength)
//                        desc = desc.Remove(DescLength) + "...";
//                    contentModulo.InnerHtml += "<p>" + desc + "</p>";
//                }
//                contentModulo.InnerHtml += "</li>";
//            }
//            if (contentModulo.InnerHtml.Length != 0)
//            {
//                contentModulo.InnerHtml = "<ul>" + contentModulo.InnerHtml + "</ul>";
//            }
//            HtmlGenericControl h3 = new HtmlGenericControl("h3");
//            h3.InnerHtml = Nome;
//            div.Controls.Add(h3);
//            div.Controls.Add(contentModulo);

//            return div;


//        }
//    }


//}
