using System;
using System.Collections.Generic;
using CmsConfigs.Model;
using LabelsManager;
using LanguageManager.BusinessLogic;
using NetCms.Diagnostics;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.Themes.Model;

/// <summary>
/// Summary description for LayoutModuloBlock
/// </summary>
/// 
namespace NetFrontend.Homepages
{
    public abstract class LayoutModuloBlock : LayoutBlock
    {

        private int _ID;
        private bool _Stato;
        private int _Pos;
        private string _CssClass;
        private int _Tipo;
        private int _StateValue;
        private string _ShowOnlyInUrl;

        //protected PageData PageData;

        protected abstract string LocalTypeCssClass
        {
            get;
            
        }
        public abstract string Title
        {
            get;
        }

        public override int ID
        {
            get
            {
                return _ID;
            }
        }
        public override int Pos
        {
            get { return _Pos; }
        }
        public override bool HasModuli
        {
            get { return false; }
        }
        public override bool Stato
        {
            get { return _Stato; }
        }
        public override int StateValue
        {
            get { return _StateValue; }
        }
        public override string CssClass
        {
            get
            {
                return "modulo " + LocalTypeCssClass + " " + _CssClass;
            }
        }
        public override bool IsBlock
        {
            get
            {
                return false;
            }
        }

        //private string _ShowOnlyInUrl;
        //public virtual string ShowOnlyInUrl
        //{
        //    get { return _ShowOnlyInUrl; }
        //    set { _ShowOnlyInUrl = value; }
        //}

        //(ShowOnlyInUrl.ToLower() == PageData.FolderPath.ToLower()

        public LayoutModuloBlock(Modulo modulo)
        {
            _ID = modulo.ID; //int.Parse(row["ID_modulo"].ToString());
            _Pos = modulo.Posizione; //int.Parse(row["posizione_modulo"].ToString());
            _CssClass = modulo.CssClass; //row["cssclass_modulo"].ToString();
            _Stato = modulo.Stato == 1; //row["Stato_Modulo"].ToString() == "1";           
            _Tipo = modulo.Tipo; //int.Parse(row["tipo_modulo"].ToString());
            _ShowOnlyInUrl = modulo.ShowOnlyInUrl;
            _StateValue = modulo.Stato; //int.Parse(row["Stato_Modulo"].ToString());
        }

        public override bool AddBlock(PageData PageData)
        {
            bool baseValue = base.AddBlock(PageData);

            if (_ShowOnlyInUrl != null)
            {
                //Diagnostics.TraceMessage(TraceLevel.Warning, "ShowOnlyInUrl: " + _ShowOnlyInUrl.ToLower() + " - current PageData.FolderPath: " + PageData.FolderPath.ToLower());
                return (_ShowOnlyInUrl.ToLower() == PageData.FolderPath.ToLower());
            }                   
            return baseValue;

            //return base.AddBlock(PageData) && (_ShowOnlyInUrl.ToLower() == PageData.FolderPath.ToLower());
        }

        // CommonLabels
        private Labels _FrontCommonLabels;
        public Labels FrontCommonLabels
        {
            get
            {
                if (_FrontCommonLabels == null)
                    _FrontCommonLabels = LabelsManager.LabelsCache.GetTypedLabels(typeof(CommonLabels), LanguageBusinessLogic.CurrentLanguage.CultureCode_Lang) as Labels;
                return _FrontCommonLabels;
            }
        }

        private Dictionary<string, string> _CommonTemplateLabels;
        public Dictionary<string, string> CommonTemplateLabels
        {
            get
            {
                if (_CommonTemplateLabels == null)
                {
                    _CommonTemplateLabels = new Dictionary<string, string>();
                    foreach (CommonLabels.CommonLabelsList label in Enum.GetValues(typeof(CommonLabels.CommonLabelsList)))
                        _CommonTemplateLabels.Add(label.ToString().ToLower(), FrontCommonLabels[label].ToString());
                }
                return _CommonTemplateLabels;
            }
        }

        /// <summary>
        /// Restituisce il sorgente html del template con i dati renderizzati
        /// </summary>
        /// <param name="data">Dati da passare al template in formato anonimo</param>
        /// <param name="tplPath">Percorso assoluto del template da utilizzare</param>
        /// <param name="labels">Oggetto anonimo contenente le labels per la lingua corrente</param>
        /// <returns></returns>
        public string RenderTemplate(Object data, string tplPath, Object labels)
        {
            return NetCms.Themes.TemplateEngine.RenderTpl(data, tplPath, labels, CommonTemplateLabels);            
        }

        // TODO: Cambiare la logica del metodo - il nome file va estrapolato dal Assets del tema filtrando i templates per nome 
        /// <summary>
        /// Restituisce il path assoluto del template impostato nel modulo corrente
        /// </summary>
        /// <param name="templateName">Template scelto tra quelli disponibili in cref="Modulo.TemplateType"</param>
        /// <param name="basePath">Percorso assoluto path della cartella che contiene i tpl</param>
        /// <returns></returns>
        public string GetModuleTemplatePath(string templateName, string basePath)
        {
            string tpl_path = basePath;           

            //GetTplFileNameByLabel
            tpl_path += GlobalConfig.CurrentConfig.CurrentTheme.GetTplPathByLabel(templateName, 
                                                                                             new List<TemplateElement.TplTypes>()
                                                                                             { 
                                                                                                 TemplateElement.TplTypes.modulo,
                                                                                                 TemplateElement.TplTypes.moduloapplicativo,
                                                                                                 TemplateElement.TplTypes.systemmodulo,
                                                                                             });

            return tpl_path;
        }
    }
}