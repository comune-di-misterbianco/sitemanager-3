using System;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using NetCms.Structure.Applications.Homepage.Model;
using System.Net;
using System.Xml;
using System.ServiceModel.Syndication;
using System.Collections.Generic;

/// <summary>
/// Summary description for LayoutModuloBlock
/// </summary>
/// 
namespace NetFrontend.Homepages
{
    [NetCms.Homepages.HomepageModuleDefiner(4, "Rss", -1, NetCms.Homepages.HomepageModuleDefiner.ModuleType.Static)]
    public class LayoutModuloRssBlock : LayoutModuloBlock
    {
        protected override string LocalTypeCssClass
        {
            get { return "modulo_rss"; }
        }
        public override string Nome
        {
            get
            {
                return "Rss" + ID;
            }
        }
        private string _Title;
        public override string Title
        {
            get { return _Title; }
        }

        private string Href;
        private int Records;
        private bool ShowDesc;
        private int DescLength;

        public LayoutModuloRssBlock(Modulo modulo)
            : base(modulo)
        {

            ModuloRSS moduloRss = modulo as ModuloRSS;
            if (moduloRss != null)
            {
                Records = moduloRss.Records;
                Href = moduloRss.Link;
                ShowDesc = moduloRss.Descrizione == 1;
                DescLength = moduloRss.DescLen;
            }
        }

        private string RemoveHTML(string html)
        {
            return Regex.Replace(html, @"<(.|\n)*?>", "");
        }

        private SyndicationFeed GetRss(PageData PageData)
        {
            SyndicationFeed feed = null;
            bool success = false; ;

            if (PageData.Application["RSS_" + Href] != null && PageData.Application["RSSTime_" + Href] != null)
            {
                DateTime created = (DateTime)PageData.Application["RSSTime_" + Href];

                TimeSpan diff = DateTime.Now - created;
                if (diff.TotalMinutes < 15)
                    feed = (SyndicationFeed)PageData.Application["RSS_" + Href];
            }

            if (feed == null)
            {
                try
                {
                    var client = new WebClient();
                    client.Headers.Add("user-agent", "Mozilla/5.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                    XmlReaderSettings settings = new XmlReaderSettings();
                    settings.ValidationType = ValidationType.None;
                    settings.ConformanceLevel = ConformanceLevel.Auto;
                    settings.IgnoreComments = true;
                    settings.CheckCharacters = false;

                    XmlReader reader = XmlReader.Create(Href, settings);
                    feed = SyndicationFeed.Load(reader);
                    reader.Close();

                    if (feed.Items.Any())
                    {
                        success = true;
                    }
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante il recupero del feed rss " + this.Href + " associato al modulo rss con id " + ID, "", "", ex);
                }
                if (success)
                {
                    PageData.Application["RSS_" + Href] = feed;
                    PageData.Application["RSSTime_" + Href] = DateTime.Now;
                }

            }

            return feed;
        }


        public HtmlGenericControl LayoutBlockControl;
        public override HtmlGenericControl GetLayoutBlock(PageData PageData)
        {

            HtmlGenericControl divmodulo = new HtmlGenericControl("div");
            divmodulo.Attributes["class"] = CssClass;

            SyndicationFeed feed = GetRss(PageData);
            if (feed != null)
            {

                string name = feed.Title.Text;
                string link = ((feed.BaseUri != null) ? feed.BaseUri.AbsoluteUri : ((feed.Links != null && feed.Links.Any()) ? feed.Links.FirstOrDefault().Uri.AbsoluteUri : ""));

                string title = feed.Description.Text;
                _Title = name;

                #region Rss Title
                HtmlGenericControl divheading = new HtmlGenericControl("div");
                divheading.Attributes["class"] = "row";

                HtmlGenericControl h3 = new HtmlGenericControl("h3");
                h3.Attributes["class"] = "mb-4 block-title text-primary";
                h3.InnerHtml = "<a  title=\"" + title + "\" href=\"" + link + "\">" + name + "</a>";

                divheading.Controls.Add(h3);
                divmodulo.Controls.Add(divheading);

                #endregion

                #region Rss List 
                HtmlGenericControl divbody = new HtmlGenericControl("div");
                divbody.Attributes["class"] = "modulo-content";               

                var items = feed.Items.Take(Records);

                foreach (SyndicationItem item in items)
                {
                    string data = string.Empty;

                    if (item.PublishDate != null)
                        data = "<span class=\"data_feed\">" + item.PublishDate.Date.ToShortDateString() + "</span>";

                    string tempLink = item.Links.FirstOrDefault().Uri.AbsoluteUri;

                    divbody.InnerHtml += "<li class=\"media \">"
                        + "<div class=\"media-body post-holder \">"
                        + "<svg class=\"icon icon-primary icon-sm\"><use xlink:href=\"/bootstrap-italia/dist/svg/sprite.svg#it-calendar\"></use></svg>"
                        + "<span class=\"date\"><small>"+ data + "</small></span><br>"
                        + "<a href=\"" + tempLink + "\">"+ item.Title.Text + "</a>";

                    if (ShowDesc)
                    {
                        string desc = item.Summary.Text;
                        desc = RemoveHTML(desc);

                        if (desc.Length > DescLength)
                            desc = desc.Remove(DescLength) + "...";
                        divbody.InnerHtml += "<p>" + desc + "</p>";
                    }
                    divbody.InnerHtml += "</div></li>";
                }

                if (divbody.InnerHtml.Length != 0)
                    divbody.InnerHtml = "<ul class=\"it-list\">" + divbody.InnerHtml + "</ul>";

                divmodulo.Controls.Add(divbody);
                #endregion
            }
            return divmodulo;
        }



    }
}
