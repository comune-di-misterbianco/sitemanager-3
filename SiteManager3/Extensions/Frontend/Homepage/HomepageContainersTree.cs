using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.Applications.Homepage.Model;
using NetCms.Structure.Applications.Homepage;

namespace NetFrontend.Homepages
{
    public class HomepageContainersTree:Dictionary<string, LayoutBlock> 
    {
        private int HomepageID{ get; set; }
        private PageData PD
        {
            get;
            set;
        }

        public HomepageContainersTree(int homepageID,PageData pd )
        {
            HomepageID = homepageID;
            PD = pd;
            BuildFullTree();            
        }

        private string TplPath
        {
            get { return PD.CurrentConfig.CurrentTheme.TemplateFolder;  }
        }

        private LayoutBlock BuildContenitore(List<Contenitore> containersTree, List<Modulo> allModules)
        {
            LayoutBlock root = null;
            LayoutBlock lastblock = null;

            foreach (Contenitore recordContenitore in containersTree)
            {
                //Contenitore container = HomePageBusinessLogic.GetContenitoreById(int.Parse(recordContenitore["ID_contenitore"].ToString()));
                LayoutBlock block = new LayoutContainerBlock(recordContenitore);

                if (root == null)
                    root = block;
                else
                {
                    if (block.Depth == lastblock.Depth)
                    {
                        lastblock.Parent.ChildBlocks.Add(block);
                        block.Parent = lastblock.Parent;
                    }
                    if (block.Depth > lastblock.Depth)
                    {
                        lastblock.ChildBlocks.Add(block);
                        block.Parent = lastblock;
                    }
                    if (block.Depth < lastblock.Depth)
                    {
                        int blockGap = lastblock.Depth - block.Depth;
                        LayoutBlock parent = lastblock;
                        for (int g = 0; g <= blockGap; g++)
                            parent = parent.Parent;
                        parent.ChildBlocks.Add(block);
                        block.Parent = parent;
                    }
                }
                
                //bool HasModuli = recordContenitore.IntValue("havemoduli_contenitore") == 1;
                //if (HasModuli)
                if (recordContenitore.HasModuli)
                {
                    //Seleziono tutti i moduli del contenitore
                    var recordsModuliDelContenitore = allModules.Where(row => row.Contenitore.ID == block.ID);//.OrderBy("posizione_modulo");

                    //Per ogni record genero l'oggetto associato
                    var moduli = recordsModuliDelContenitore.Select(x => Homepage.BlocksFactory(x)).OrderBy(x=>x.Pos);

                    //Scorro l'elenco dei moduli da aggiungere alla gerarchia e li aggiungo
                    foreach (var modulo in moduli.Where(x => x != null && x.StateValue != 0))
                    {
                        modulo.TplPath = this.TplPath;
                        block.ChildBlocks.Add(modulo);
                    }
                }
                lastblock = block;
            }

            return root;
        }

        private enum StateContainer
        {
            Disabilitato,
            Abilitato
        }
             

        private void BuildFullTree()
        {
            List<Contenitore> allContainers = HomePageBusinessLogic.FindContenitoriByHomePageAndStato(HomepageID, 1).ToList();
            
            //List<Contenitore> allParentContaners = HomePageBusinessLogic.FindContenitoriByHomepageAndStato();
            //DataRow[] allContainers = NetFrontend.DAL.HomepageDAL.ListContenitori("stato_contenitore = 1 AND homepage_contenitore = " + HomepageID);
            List<Modulo> allModules = HomePageBusinessLogic.FindModuliByHomepageOrderByPosition(HomepageID).ToList();
           
            //DataRow[] allModules = NetFrontend.DAL.HomepageDAL.ListModuli("homepage_contenitore = " + HomepageID, order: "posizione_modulo");
            List<Contenitore> mainContainers = allContainers.Where(x => x.Depth == 1).ToList();
            //DataRow[] mainContainers = allContainers.Where(x => x["depth_contenitore"].ToString() == "1").ToArray();

            List<Contenitore> allParentContainers;
            List<Contenitore> parentMainContainers;
            List<Modulo> allParentModules;
            
            foreach(var container in mainContainers)
            {
                    string tree ="";
                    string nome ="";
                
                    StateContainer cont_state = StateContainer.Abilitato; // stato contenitore ereditato

                if (!container.Eredita  /*|| !(container.ChildsBlocks.Count() == 0)*/)
                {

                    tree = container.Tree; //container["tree_contenitore"].ToString();
                    nome = container.Name; //container["nome_contenitore"].ToString();

                    var containersTree = allContainers.Where(x => x.Tree.StartsWith(tree))
                                                      .OrderBy(x => x.Tree)
                                                      .ToArray();

                    this.Add(nome, BuildContenitore(containersTree.ToList(), allModules));
                }
                else
                {
                    HomepageData hp = PD.Homepage;
                    int id = hp.ParentHomepage.ID;

                    // verificare se il container ereditato � attivo!!!


                    do
                    {

                        allParentContainers = HomePageBusinessLogic.FindContenitoriByHomePageAndStato(id, 1).ToList();
                        parentMainContainers = allParentContainers.Where(x => x.Depth == 1).ToList();
                        var cont = parentMainContainers.Where(x => x.Name == container.Name).ToList();

                        // condizione in cui il contenitore padre sia disattivato
                        if (!cont.Any())
                        {                            
                            cont_state = StateContainer.Disabilitato;
                            break;
                        }

                        Contenitore contenitore = cont[0];

                        if (!contenitore.Eredita)
                        {
                            allParentModules = HomePageBusinessLogic.FindModuliByHomepageOrderByPosition(id).ToList();
                            tree = contenitore.Tree; //container["tree_contenitore"].ToString();
                            nome = contenitore.Name;
                            var containersTree = allParentContainers.Where(x => x.Tree.StartsWith(tree))
                                                    .OrderBy(x => x.Tree)
                                                    .ToArray();

                            this.Add(nome, BuildContenitore(containersTree.ToList(), allParentModules));
                        }

                        //PD = PD.Homepage.ParentHomepage.ParentHomepage._PageData;
                        if (hp.ParentHomepage != null)
                        {
                            hp = hp.ParentHomepage;
                            if (hp.ParentHomepage != null)
                                id = hp.ParentHomepage.ID;
                        }


                    } while (!this.ContainsKey(container.Name));


                    // se il contenitore parent � disabilitato appendo il contenitore previsto dalla homepage corrente
                    if (cont_state == StateContainer.Disabilitato)
                    {
                        tree = container.Tree; //container["tree_contenitore"].ToString();
                        nome = container.Name; //container["nome_contenitore"].ToString();

                        var containersTree = allContainers.Where(x => x.Tree.StartsWith(tree))
                                                          .OrderBy(x => x.Tree)
                                                          .ToArray();

                        this.Add(nome, BuildContenitore(containersTree.ToList(), allModules));
                    }
                }
                   
            }
        }
    }
}
