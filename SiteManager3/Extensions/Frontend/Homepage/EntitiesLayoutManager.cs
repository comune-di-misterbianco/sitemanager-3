using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Front;
/// <summary>
/// Summary description for LayoutBlock
/// </summary>
namespace NetFrontend
{

    public class LayoutData
    {
        //public enum FrontLayouts { 
        //    SystemError = -1, 
        //    Inherits = 0, 
        //    All = 8, 
        //    LeftRight = 1, 
        //    LeftFooter = 2,
        //    RightFooter = 3, 
        //    Left = 4, 
        //    Right = 5, 
        //    Footer = 6, 
        //    None = 7 }

        private FrontLayouts layout;
        public FrontLayouts Layout
        {
            get
            {
                return layout;
            }
        }

        private LayoutData parentLayout;
        public LayoutData ParentLayout
        {
            get
            {
                return parentLayout;
            }
        }

        public bool ShowColSX
        {
            get
            {
                if (Layout == FrontLayouts.Inherits && parentLayout != null)
                    return ParentLayout.ShowColSX;
                else
                    return Layout == FrontLayouts.All ||
                           Layout == FrontLayouts.Left ||
                           Layout == FrontLayouts.LeftFooter ||
                           Layout == FrontLayouts.LeftRight;
            }
        }
        public bool ShowColDX
        {
            get
            {
                if (Layout == FrontLayouts.Inherits && parentLayout != null)
                    return ParentLayout.ShowColDX;
                return Layout == FrontLayouts.All ||
                       Layout == FrontLayouts.Right ||
                       Layout == FrontLayouts.RightFooter||
                       Layout == FrontLayouts.LeftRight;
            }
        }
        public bool ShowFooter
        {
            get
            {

                if (Layout == FrontLayouts.Inherits && parentLayout != null)
                    return ParentLayout.ShowFooter;
                return Layout == FrontLayouts.All ||
                       Layout == FrontLayouts.Footer ||
                       Layout == FrontLayouts.RightFooter ||
                       Layout == FrontLayouts.LeftFooter;
            }
        }

        public LayoutData(FrontLayouts layoutValue)
        {
            this.layout = layoutValue;
        }
        public LayoutData(string layoutValue)
        {
            InitLayout(layoutValue);

        }
        public LayoutData(FrontLayouts layoutValue,LayoutData parent)
        {
            this.parentLayout = parent;
            this.layout = layoutValue;
        }
        public LayoutData(string layoutValue, LayoutData parent)
        {
            this.parentLayout = parent;
            InitLayout(layoutValue);

        }
        public void InitLayout(string layoutValue)
        {
            switch (layoutValue)
            {
                case "0": this.layout = FrontLayouts.Inherits; break;
                case "1": this.layout = FrontLayouts.LeftRight; break;
                case "2": this.layout = FrontLayouts.LeftFooter; break;
                case "3": this.layout = FrontLayouts.RightFooter; break;
                case "4": this.layout = FrontLayouts.Left; break;
                case "5": this.layout = FrontLayouts.Right; break;
                case "6": this.layout = FrontLayouts.Footer; break;
                case "7": this.layout = FrontLayouts.None; break;
                case "8": this.layout = FrontLayouts.All; break;
                case "-1": this.layout = FrontLayouts.SystemError; break;
                default: this.layout = FrontLayouts.Inherits; break;
            }
        }
    }
    /*
    public static class EntitiesLayoutManager
    {
        public enum FrontLayouts { SystemError = -1, Inherits = 0, All = 8, LeftRight = 1, LeftFooter = 2, RightFooter = 3, Left = 4, Right = 5, Footer = 6, None = 7 }
        public static FrontLayouts GetLayout(string layoutValue)
        {
            switch (layoutValue)
            {
                case "0": return FrontLayouts.Inherits; 
                case "1": return FrontLayouts.LeftRight; 
                case "2": return FrontLayouts.LeftFooter;
                case "3": return FrontLayouts.RightFooter; 
                case "4": return FrontLayouts.Left; 
                case "5": return FrontLayouts.Right; 
                case "6": return FrontLayouts.Footer;
                case "7": return FrontLayouts.None; 
                case "8": return FrontLayouts.All;
                default: return FrontLayouts.SystemError;
            }
        }
    }    */
}
