using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;

namespace NetFrontend.Homepages
{
    public class HomepageData
    {
        private PageData PageData;
        public PageData _PageData
        {
            get
            {
                return PageData;
            }
        }
        public HomepageData(PageData pagedata)
        {
            PageData = pagedata;
            Homepage = new Homepage(pagedata);
            
        }

        public void ClearCache()
        {
            if (PageData.NeedToClearFrontCache)
            {
                Homepage.Invalidate(PageData.Network.SystemName, this.ID.ToString());
            }
        }

        public HomepageData ParentHomepage { get; set;}

        public bool IsHomepage { get; set;}
        
        public int ID { get; set;}

        public int DocumentID { get; set;}

        public bool ShowColDX { get; set;}
        public bool ShowColSX { get; set;}
        public bool ShowFooter { get; set;}

        private string _CssClass;
        public string CssClass
        {
            get 
            {
                if (_CssClass == null)
                {
                    //DataRow row =NetFrontend.DAL.FileSystemDAL.GetDocumentRecordByID(DocumentID);
                    Document doc = DocumentBusinessLogic.GetDocumentRecordById(DocumentID);
                    if (doc != null)
                        _CssClass = doc.CssClass != null ? doc.CssClass : "";
                    else
                        _CssClass = "";
                }
                return _CssClass; 
            }
        }
	

        private Homepage Homepage { get; set;}

        private HtmlGenericControl _ColCX;
        public HtmlGenericControl ColCX
        {
            get {
                if (_ColCX == null)
                    _ColCX = Homepage.ColCx;
                return _ColCX; 
            }
        }

        private HtmlGenericControl _ColDX;
        public HtmlGenericControl ColDX
        {
            get
            {
                if (this.ShowColDX && _ColDX == null)
                    _ColDX = Homepage.ColDx;
                return _ColDX;
            }
        }

        private HtmlGenericControl _ColSX;
        public HtmlGenericControl ColSX
        {
            get
            {
                if (this.ShowColSX && _ColSX == null)
                    _ColSX = Homepage.ColSx;
                return _ColSX;
            }
        }


        private HtmlGenericControl _Footer;
        public HtmlGenericControl Footer
        {
            get
            {
                if (this.ShowFooter && _Footer == null)
                    _Footer = Homepage.Footer;
                return _Footer;
            }
        }
	
    }
}
