using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Front;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS;
using NetCms.Structure.Applications.Homepage;
using NetCms.Structure.Applications.Homepage.Model;
using System.Linq;

namespace NetFrontend.Homepages
{
    public partial class HomepageFinder
    {
        private PageData PageData;              

        private IList<StructureFolder> Folders;

        public HomepageFinder(PageData pagedata)
        {
            PageData = pagedata;
        }

        public HomepageData GetHomepage()
        {
            StructureFolder row = GetNextFolder();

            if (row != null)
            {
                int id_folder = 0;

                if (row != null)
                    id_folder = row.ID;

                Document docData = DocumentBusinessLogic.GetDocumentRecordByFolderAndName(id_folder, "default.aspx");
                //DataRow docData = NetFrontend.DAL.FileSystemDAL.GetDocumentRecordBySqlConditions("Name_Doc = 'default.aspx' AND Folder_Doc = " + id_folder);

                HomepageData data = new HomepageData(PageData);
                if (docData != null)
                {
                    data.ID = docData.Content; //int.Parse(docData["Content_DocLang"].ToString());
                    //Aggiunto per provare a risolvere un problema di caching delle homepage
                    data.ClearCache();
                    data.DocumentID = docData.ID;//int.Parse(docData["id_Doc"].ToString());
                    string WebFolderPath = FrontendNetwork.GetCurrentFolderPathTranslated();
                    string DBFolderPath = row.Path; //row["Path_Folder"].ToString();//+ "/";
                    if (WebFolderPath == DBFolderPath && PageData.PageName == "default.aspx")
                        data.IsHomepage = true;

                    ICollection<Contenitore> contenitori = HomePageBusinessLogic.FindContenitoriByHomepage(data.ID,1);
                    //DataRow[] rows = NetFrontend.DAL.HomepageDAL.ListContenitori("Homepage_Contenitore = " + data.ID + " AND Depth_Contenitore = 1");

                    data.ShowColSX = contenitori.Count > 0 && contenitori.ElementAt(0).Stato == 1;
                    data.ShowColDX = contenitori.Count > 2 && contenitori.ElementAt(2).Stato == 1;//rows[2]["Stato_Contenitore"].ToString() == "1";
                    data.ShowFooter = contenitori.Count > 3 && contenitori.ElementAt(3).Stato == 1;//rows[3]["Stato_Contenitore"].ToString() == "1";

                    FrontLayouts? FrontLayout = null;

                    if (FrontLayout == null && PageBodyHandler.CurrentPageHandler != null && PageBodyHandler.CurrentPageHandler.ForceLayout != null)
                        FrontLayout = new FrontLayouts?(PageBodyHandler.CurrentPageHandler.ForceLayout.Value);

                    if (FrontLayout == null && PageData.CurrentVerticalApplication != null)
                        FrontLayout = new FrontLayouts?( PageData.CurrentVerticalApplication.FrontLayout);

                    if (FrontLayout == null && NetCms.Front.Static.StaticPagesCollector.CurrentStaticPage != null)
                        FrontLayout = new FrontLayouts?( NetCms.Front.Static.StaticPagesCollector.CurrentStaticPage.Attribute.PageLayout);


                    if (FrontLayout != null)
                    {
                        bool showDx = FrontLayout == FrontLayouts.All ||
                                      FrontLayout == FrontLayouts.LeftRight ||
                                      FrontLayout == FrontLayouts.RightFooter ||
                                      FrontLayout == FrontLayouts.Right;
                        bool showSx = FrontLayout == FrontLayouts.All ||
                                      FrontLayout == FrontLayouts.Left ||
                                      FrontLayout == FrontLayouts.LeftRight ||
                                      FrontLayout == FrontLayouts.LeftFooter;
                        bool showFx = FrontLayout == FrontLayouts.All ||
                                      FrontLayout == FrontLayouts.Footer ||
                                      FrontLayout == FrontLayouts.RightFooter ||
                                      FrontLayout == FrontLayouts.LeftFooter;

                        data.ShowColSX = data.ShowColSX && showSx;
                        data.ShowColDX = data.ShowColDX && showDx;
                        data.ShowFooter = data.ShowFooter && showFx;
                    }
                    else
                    {
                        /* Nel Caso in cui la cartella corrente non sia di tipo Homepage controllo le impostazioni di layout */
                        if (PageData.Folder != null) // && PageData.Folder.Type != 1
                        {
                            //Viene prima analizzato il layout del documento, per vedere se ne � stato specificato uno.
                            if (PageData.ContentsHandler != null && PageData.ContentsHandler.CurrentDocument != null && PageData.ContentsHandler.CurrentDocument.FrontLayout.Layout != NetCms.Front.FrontLayouts.Inherits)
                            {
                                data.ShowColSX = data.ShowColSX && PageData.ContentsHandler.CurrentDocument.FrontLayout.ShowColSX;
                                data.ShowColDX = data.ShowColDX && PageData.ContentsHandler.CurrentDocument.FrontLayout.ShowColDX;
                                data.ShowFooter = data.ShowFooter && PageData.ContentsHandler.CurrentDocument.FrontLayout.ShowFooter;
                            }
                            //Nel caso in cui non fosse stato specificato per il documento, viene analizzato quello della sua cartella
                            else if (PageData.Folder.FrontLayout != null && PageData.Folder.FrontLayout.Layout != NetCms.Front.FrontLayouts.SystemError)
                            {
                                data.ShowColSX = data.ShowColSX && PageData.Folder.FrontLayout.ShowColSX;
                                data.ShowColDX = data.ShowColDX && PageData.Folder.FrontLayout.ShowColDX;
                                data.ShowFooter = data.ShowFooter && PageData.Folder.FrontLayout.ShowFooter;
                            }
                        }
                    }
                    data.ParentHomepage = GetHomepage();
                }
                return data;
            }
            else 
                return null;
        }

        private StructureFolder GetNextFolder()
        {
            if (Folders == null)
                initFolders();

            if (TargetFolder >= Folders.Count || (TargetFolder>0 && PageData.PageDepth-1 == 0))
                return null;
            else
                return Folders.ElementAt(TargetFolder++);
        }
        private int TargetFolder;

        private string _Path;
        private string Path
        {
            get
            {
                if (_Path == null)
                {
                    if (PageData.Request.QueryString["path"] != null && PageData.Request.QueryString["path"].Length > 0)
                        _Path = PageData.Request.QueryString["path"].ToLower();
                    else
                        _Path = FrontendNetwork.GetCurrentFolderPath();
                    _Path = _Path.Replace("'", "''");
                }
                return _Path;
            }
        }

        private void initFolders()
        {
            TargetFolder = 0;

            string webroot = PageData.AbsoluteWebRoot.ToLower();

            string filepath = Path.ToLower();
            if(webroot.Length>0)
                filepath = filepath.Replace(webroot, "");
            //filepath = filepath.Length == 1 ? "" : "/" + filepath.Replace("/", "");
            string[] filepathArray = filepath.Split('/');

            //string sql = "";
            
            List<string> paths = new List<string>();
            string previousTranslated = "";
            string filepathTot = "";
            for (int i = 0; i < filepathArray.Length; i++)
            if(filepathArray[i].Length>0)
            {
                if (filepathTot.Length <= 0)
                {
                    paths.Add(String.Empty);
                }

                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang == 0)
                {
                    previousTranslated = FolderBusinessLogic.TranslateNameOnTheFly(LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.ID, PageData.Network.ID, filepathArray[i].Replace("'", "''"), i + 1, previousTranslated.Replace("/", ""));
                    
                    filepathTot += previousTranslated;
                }
                else
                    filepathTot += "/" + filepathArray[i].Replace("'", "''");

                paths.Add(filepathTot);

            }

            string[] pathsArray = new string[paths.Count + 1];
            pathsArray[0] = "";
            int p = 1;
            foreach (string path in paths) 
            {
                pathsArray[p] = path;
                p++;
            }


            string order = "Tree";
            //if (paths.Count > 0)
            //{
                //order += "DESC";
            //}
            //else
                //order += "ASC";

            Folders = FolderBusinessLogic.ListFoldersRecordsByPaths(PageData.Network.ID, pathsArray, order, paths.Count <= 0);//NetFrontend.DAL.FileSystemDAL.ListFoldersRecordsBySqlConditions(sql + "Trash_Folder = 0 AND Stato_Folder < 2 AND System_Folder <2 AND Network_Folder = "+PageData.Network.ID+" AND Tipo_Folder = 1", order: order);
        }        
    }
}
