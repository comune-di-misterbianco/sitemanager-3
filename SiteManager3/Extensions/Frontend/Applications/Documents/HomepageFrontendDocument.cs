using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Xml;
using System.Text;
using NetCms.Networks.WebFS;
using NetFrontend;
using NetCms.Structure.WebFS;


/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Structure.Applications.Homepage
{
    [FrontendDocumentsDefiner(1, "Homepage")]
    public class HomepageFrontendDocument : FrontendDocument
    {
        public override string FrontendUrl
        {
            get { return this.Folder.FrontendUrl; }
        }

        public HomepageFrontendDocument(Document doc, FrontendFolder folder):base(doc, folder) {}
        public HomepageFrontendDocument(int docID, FrontendFolder folder) : base(docID, folder) { }
    }
}