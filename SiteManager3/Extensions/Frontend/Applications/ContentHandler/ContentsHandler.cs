
using System;
using System.Data;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using NetCms.Front;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using NetCms.Structure.Applications;
using NetCms.CmsApp.ShortCodes;
using static NetCms.CmsApp.ShortCodes.ShortCode;
using System.Reflection;
using LabelsManager;
using LanguageManager.BusinessLogic;
using NetCms.Themes.Model;
using CmsConfigs.Model;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    public abstract class ContentsHandler
    {
        protected PageData PageData;
        
        public virtual int CurrentDocumentID 
        { 
            get 
            {
 
                return CurrentDocument!=null ? CurrentDocument.ID : 0; 
            } 
        }

        public enum FrontModeEnum
        {
            Normal,
            Template
        }

        private FrontendDocument currentDocument;
        public FrontendDocument CurrentDocument
        { 
            get 
            {
                if (currentDocument == null && PageData.Network.HasCurrentDocument)
                    currentDocument = PageData.Network.CurrentDocument;
                return currentDocument; 
            } 
        }

        public int ContentID
        {
            get 
            {
                if (_ContentID == 0 )
                {
                    if (this.IsPreview)
                    {
                        G2Core.Common.QueryStringRequestVariable contentID = new G2Core.Common.QueryStringRequestVariable("id");
                        if (contentID.IsValidInteger && contentID.IntValue > 0)
                            _ContentID = contentID.IntValue;
                    }
                    
                    if (_ContentID == 0)
                        throw new Exception("Impossibile caricare l'ID del contenuto per l'anteprima");
                }

                return _ContentID; 
            }
        }
        private int _ContentID;

        public bool IsPreview
        {
            get
            {
                return _IsPreview;
            }
        }
        private bool _IsPreview;

        public ContentsHandler(PageData pagedata)
        {
            PageData = pagedata;
            _Toolbar = new NetCms.GUI.GenericToolbar();
            _InformationBox = new NetCms.GUI.Info.GenericInfoBox();

            _IsPreview = pagedata.PageName == "preview.aspx";
        }

        public abstract Control Control { get; }
        public abstract Control PreviewControl { get; }

        private NetCms.GUI.Info.InfoBox _InformationBox;
        public NetCms.GUI.Info.InfoBox InformationBox
        {
            get { return _InformationBox; }
        }

        private NetCms.GUI.Toolbar _Toolbar;
        public NetCms.GUI.Toolbar Toolbar
        {
            get { return _Toolbar; }
        }

        private Labels _CommonLabels;
        private Labels CommonLabels
        {
            get
            {
                if (_CommonLabels == null)
                {
                    _CommonLabels = LabelsCache.GetTypedLabels(typeof(CommonLabels)) as Labels;
                }
                return _CommonLabels;
            }
        }

        private Dictionary<string, string> _TemplateCommonLabels;
        public Dictionary<string, string> TemplateCommonLabels
        {
            get
            {
                if (_TemplateCommonLabels == null)
                {
                    _TemplateCommonLabels = new Dictionary<string, string>();
                    foreach (CommonLabels.CommonLabelsList label in Enum.GetValues(typeof(CommonLabels.CommonLabelsList)))
                        _TemplateCommonLabels.Add(label.ToString().ToLower(), CommonLabels[label].ToString());
                }
                return _TemplateCommonLabels;
            }
        }


        public static ContentsHandler GetSpecificHandler(PageData PageData)
        {
            ContentsHandler handler = null;
            int _ContentID = 0;
            G2Core.Common.QueryStringRequestVariable contentID = new G2Core.Common.QueryStringRequestVariable("id");
            if (contentID.IsValidInteger && contentID.IntValue > 0)
                _ContentID = contentID.IntValue;

            if (_ContentID > 0)
            {
                DocContent docContent = DocumentBusinessLogic.GetDocContentById(_ContentID);
                Document doc = null;
                if (docContent.Revisione != null)
                    doc = docContent.Revisione.Document;
                else
                    doc = docContent.DocLang.Document;
                if (doc != null && handler == null && NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies.Contains(doc.Application.ToString()))
                {
                    NetCms.Structure.Applications.Application app = NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies[doc.Application.ToString()];
                    object[] pars = { PageData };
                    handler = (ContentsHandler)NetCms.Structure.Applications.ApplicationsPool.BuildInstanceOf(app.Classes.ContentHandler, doc.Application.ToString(), pars);
                }
            }


            if (handler == null && PageData.Folder != null)
            {
                switch (PageData.Folder.Type)
                {
                    case 1: handler = new HomepageContentsHandler(PageData); break;
                    //case 2: handler = new WebdocsContentsHandler(PageData); break;
                    //case 4: handler = new FilesContentsHandler(PageData); break;
                    //case 9: handler = new LinksContentsHandler(PageData); break;
                }

                if (handler == null && NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies.Contains(PageData.Folder.Type.ToString()))
                {
                    NetCms.Structure.Applications.Application app = NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies[PageData.Folder.Type.ToString()];
                    object[] pars = { PageData };
                    handler = (ContentsHandler)NetCms.Structure.Applications.ApplicationsPool.BuildInstanceOf(app.Classes.ContentHandler, PageData.Folder.Type.ToString(), pars);
                }
            }
            return handler;
        }

        // CommonLabels
        private Labels _FrontCommonLabels;
        public Labels FrontCommonLabels
        {
            get
            {
                if (_FrontCommonLabels == null)
                    _FrontCommonLabels = LabelsManager.LabelsCache.GetTypedLabels(typeof(CommonLabels), LanguageBusinessLogic.CurrentLanguage.CultureCode_Lang) as Labels;
                return _FrontCommonLabels;
            }
        }

        private Dictionary<string, string> _CommonTemplateLabels;
        public Dictionary<string, string> CommonTemplateLabels
        {
            get
            {
                if (_CommonTemplateLabels == null)
                {
                    _CommonTemplateLabels = new Dictionary<string, string>();
                    foreach (CommonLabels.CommonLabelsList label in Enum.GetValues(typeof(CommonLabels.CommonLabelsList)))
                        _CommonTemplateLabels.Add(label.ToString().ToLower(), FrontCommonLabels[label].ToString());
                }
                return _CommonTemplateLabels;
            }
        }


        public virtual string GetShareContentBlock(string doctitle, string docurl, string docdescription)
        {
            string code = string.Empty;

            string webSiteBaseAddress = System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority;
            
            object document = new {title = doctitle,  docurl = webSiteBaseAddress + docurl, descforquerystring  = docdescription };

            string tpl_path = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + PageData.CurrentConfig.CurrentTheme.TemplateFolder + "commons/share-content.tpl");

            var template = Scriban.Template.Parse(System.IO.File.ReadAllText(tpl_path), tpl_path);

            if (template.HasErrors)
            {
                if (NetCms.Configurations.Debug.DebugLoginEnabled)
                    foreach (var error in template.Messages)
                       code += error.Message + System.Environment.NewLine;
                else
                    foreach (var error in template.Messages)
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, error.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else
            {
                var context = new Scriban.TemplateContext();
                context.PushCulture(System.Globalization.CultureInfo.CurrentCulture);

                var scriptObj = new Scriban.Runtime.ScriptObject();                
                scriptObj.Add("document", document);
                scriptObj.Add("commonlabels", TemplateCommonLabels);

                context.PushGlobal(scriptObj);

                code = template.Render(context);
            }


            return code;
        }

        /// <summary>
        /// Ritorna il codice html minimo per implementare i tag meta definiti dal protocollo open graph (https://ogp.me/)
        /// </summary>
        /// <param name="title">Titolo del documento</param>
        /// <param name="description">Descrizione </param>
        /// <param name="type"> </param>
        /// <param name="documentUrl">Indirizzo del documento</param>
        /// <param name="imageThumbUrl">Immagine in evidenza associata al documento (se disponibile)</param>
        /// <returns></returns>
        public virtual void SetOpenGraph(string title, string description, string type, string documentUrl, string imageThumbUrl)
        {
            string code = string.Empty;

            string webSiteBaseAddress = System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority;


            bool usehttps  = System.Web.HttpContext.Current.Request.IsSecureConnection;

            string url = (!documentUrl.StartsWith("http") || !documentUrl.StartsWith("https")) ? webSiteBaseAddress + documentUrl : documentUrl;
            string imageurl = (!imageThumbUrl.StartsWith("http") || !imageThumbUrl.StartsWith("https")) ? webSiteBaseAddress + imageThumbUrl : imageThumbUrl;

            string Title = HttpUtility.HtmlEncode(title); 
            string Description = HttpUtility.HtmlEncode(description);
            string websitename = PageData.CurrentConfig.PortalName;

            var opengraph = new { Title, Description, type, url, imageurl, websitename, usehttps };

            string tpl_path = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + PageData.CurrentConfig.CurrentTheme.TemplateFolder + "commons/opengraph.tpl");
            var template = Scriban.Template.Parse(System.IO.File.ReadAllText(tpl_path), tpl_path);
            if (template.HasErrors)
            {
                if (NetCms.Configurations.Debug.DebugLoginEnabled)
                    foreach (var error in template.Messages)
                        code += error.Message + System.Environment.NewLine;
                else
                    foreach (var error in template.Messages)
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, error.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else
            {
                var context = new Scriban.TemplateContext();
                context.PushCulture(System.Globalization.CultureInfo.CurrentCulture);

                var scriptObj = new Scriban.Runtime.ScriptObject();
                scriptObj.Add("og", opengraph);                

                context.PushGlobal(scriptObj);

                code = template.Render(context);
            }

            LiteralControl opengraphControls = new LiteralControl(code);
            PageData.HtmlHead.Controls.Add(opengraphControls);

            
        }

        public string ReplaceShortCode(string strInput)
        {
            string content = strInput;           

            Dictionary<string, string> assemblies = NetCms.CmsApp.ShortCodes.ShortCodesPool.Assemblies;
            
            // Pattern di regex shortcode
            string pattern = @"\[shortcode (.*?)\]"; // migliorare il sistema di riconoscimento dello shortcode in quanto se presente [1] il sistema va in errore

            object results = null;
            string AppRif = string.Empty;
            EnumObjectType ObjectType;  
            int RifObj = -1;
            int ItemLimit = -1;

            Application application = null;
            StructureFolder folder = null;
            Document doc = null;

            Regex regex = new Regex(pattern);
            MatchCollection coll = regex.Matches(content);
            foreach (Match res in coll)
            {
                string[] value = res.Value.Substring(1, res.Value.Length - 2).Split('-');
                if (value != null && value.Length > 2) 
                {
                    AppRif = value[0].ToLower().Replace("shortcode ","");
                    ObjectType = (EnumObjectType)Enum.Parse(typeof(EnumObjectType), value[1].ToLower());
                    RifObj = int.Parse(value[2].ToString());

                    if (value.Length > 3)
                        ItemLimit = int.Parse(value[3].ToString());               

                    switch (ObjectType)
                    {
                        case EnumObjectType.folder:
                            {
                                folder = FolderBusinessLogic.GetById(RifObj);
                                if (folder == null)
                                    break;
                                application = folder.RelativeApplication;
                            }
                            break;
                        case EnumObjectType.doc:
                            {
                                doc = DocumentBusinessLogic.GetById(RifObj);
                                application = doc.RelativeApplication;
                            }
                            break;
                    }
                    // recupero la folder correlata
                    // in funzione al nuovo algoritmo va istanziato lo shorcode tipizzato cercandolo
                    // nella collection di tipi di shortcode che viene valorizzata al bootstrap del cms
                    if (folder != null)
                    {
                        if (!string.IsNullOrEmpty(AppRif))
                        {
                            IEnumerable<KeyValuePair<string, string>> app_assemblies = assemblies.Where(x => x.Key.ToLower().Contains(AppRif.ToLower()));
                            Type type = application.Assembly.GetType(app_assemblies.FirstOrDefault().Key);
                            if (type != null)
                            {
                                object[] param = new object[] { res.Value, AppRif, RifObj, ObjectType, PageData.CurrentConfig.CurrentTheme, ItemLimit };
                                object istance = Activator.CreateInstance(type, param);

                                MethodInfo mt = type.GetMethod("GetHtml");
                                results = mt.Invoke(istance, null);
                                if (results != null)
                                {
                                    // sostituzione dello short code con l'html
                                    content = content.Replace(res.Value, results.ToString());
                                }
                            }
                        }
                    }                    
                    else
                    {
                        content = content.Replace(res.Value, "Entity "+ RifObj + " not found");
                    }

                }
            }

            return content;
        }


        //private string GetHtmlFromShortCode(ShortCode item)
        //{
        //    string html = "";

        //    string tpl_path = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + "/templates/content/shortcode/" + item.Template.ToString() + ".tpl");


        //    Application currentCmsApplication = null;

        //    // invoke via reflection object 
        //    switch (item.ObjectType)
        //    {
        //        case ShortCode.EnumObjectType.folder:
        //            {
        //               // StructureFolder folder = FolderBusinessLogic.GetById(item.RifObj);

        //                FrontendFolder folder = new FrontendFolder(item.RifObj);

        //                if (folder != null)
        //                {

        //                    if (folder.HasDocuments)
        //                    {
        //                        var doc = folder.Documents;

                                
        //                    }
                           

        //                    //if (folder.HasDocuments)
        //                    //{
        //                    //  currentCmsApplication = folder.RelativeApplication;
        //                    //    Type ApplicationFolderType = currentCmsApplication.Assembly.GetType(currentCmsApplication.Classes.Folder);

        //                    //    // var data = System.Activator.CreateInstance(ApplicationFolderType);

        //                    //    //Document docFolder = (Document)System.Activator.CreateInstance(ApplicationFolderType, parameters);
        //                    //}
        //                }
        //            }
        //            break;
        //        case ShortCode.EnumObjectType.doc:
        //            {
        //                Document doc = DocumentBusinessLogic.GetById(item.RifObj);
        //                if (doc != null)
        //                {
        //                    //currentCmsApplication = doc.RelativeApplication;
                         
        //                    //Type ApplicationDocumentType = currentCmsApplication.Assembly.GetType(currentCmsApplication.Classes.Document);
        //                    //Document newDocument = (Document)System.Activator.CreateInstance(ApplicationDocumentType, parameters);
        //                }
        //            }
        //            break;                
        //    }

          




        //    //var template = Scriban.Template.Parse(System.IO.File.ReadAllText(tpl_path), tpl_path);



        //    //public static Document Fabricate(DataRow data, Networks.NetworkKey Network)
        //    //{
        //    //    string ApplicationID = data["Application_Doc"].ToString();
        //    //    if (ApplicationsPool.ActiveAssemblies.Contains(ApplicationID))
        //    //    {
        //    //        object[] parameters = { data, Network };
        //    //        Application application = ApplicationsPool.ActiveAssemblies[ApplicationID];
        //    //        Type ApplicationFolderType = application.Assembly.GetType(application.Classes.Document);
        //    //        Document folder = (Document)System.Activator.CreateInstance(ApplicationFolderType, parameters);
        //    //        return folder;
        //    //    }


        //    return html;
        //}

        public string ControltoHtml(Control temp)
        {
            if (temp == null)
                return "";
            else
            {
                System.IO.TextWriter tw = new System.IO.StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(tw);
                temp.RenderControl(hw);
                return tw.ToString();
            }
        }
    

        public string GetContentTemplatePath(string templateName, string applicationName = null)
        {
            string tplPath =  GlobalConfig.CurrentConfig.CurrentTheme.GetTplPathByLabel(templateName,
                                                                                            new List<TemplateElement.TplTypes>()
                                                                                            {
                                                                                                TemplateElement.TplTypes.content,
                                                                                                TemplateElement.TplTypes.contentapplicativo
                                                                                            }, applicationName);
            return tplPath;
        }                                           
        
        public void RenderTemplate() { 
        
        }

    
    }   
}
