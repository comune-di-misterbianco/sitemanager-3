using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;


/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    public class HomepageContentsHandler:ContentsHandler
    {
        public override int CurrentDocumentID
        {
            get
            {
                //string sql = "Name_Doc LIKE 'default.aspx' AND Folder_Doc = " + PageData.Folder.ID;
                return DocumentBusinessLogic.GetDocumentRecordByFolderAndName(PageData.Folder.ID, "default.aspx").ID; //NetFrontend.DAL.FileSystemDAL.GetDocumentRecordBySqlConditions(sql).IntValue("id_Doc");
            }
        }
        public HomepageContentsHandler(PageData pagedata):base(pagedata)
        {
            PageData = pagedata;
        }
        public override Control PreviewControl
        {
            get { throw new NotImplementedException(); }
        }
        public override Control Control
        {
            get
            {
                return PageData.Homepage.ColCX;
            }
        }
    }   
}