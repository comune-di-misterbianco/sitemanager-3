using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using NetCms;
using G2Core.Common;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using LabelsManager;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    public abstract class ApplicationContentsHandler : ContentsHandler
    {
        public RequestVariable ShowRequest
        {
            get
            {
                return _ShowRequest ?? (_ShowRequest = new G2Core.Common.RequestVariable("show", G2Core.Common.RequestVariable.RequestType.QueryString));
            }
        }
        private RequestVariable _ShowRequest;

        #region DocumentReachable Property

        protected virtual bool DocumentReachable
        {
            get
            {
                if (_DocumentReachable == null)
                {
                    _DocumentReachable = false;
                    Document rows = DocumentBusinessLogic.GetDocumentReachableRecord(CurrentDocumentID, PageData.Folder.Path.Replace("'", "''")); //NetFrontend.DAL.FileSystemDAL.GetDocumentRecordBySqlConditions("Stato_Doc < 2 AND Path_Folder = '" + PageData.Folder.Path.Replace("'", "''") + "' AND id_Doc = " + CurrentDocumentID);
                    _DocumentReachable = rows != null;
                }
                return _DocumentReachable.Value;
            }
        }
        protected bool? _DocumentReachable;

        #endregion

        protected string SearchQS
        {
            get; set;
        }

        // Deprecata
        //private string _PrintOrder;
        //protected string PrintOrder
        //{
        //    get
        //    {
        //        if (_PrintOrder == null)
        //        {
        //            XmlNodeList oNodeList = this.PageData.XmlConfigs.SelectNodes("/Portal/configs/Networks/" + this.PageData.Network.SystemName + "/applications/App3/frontend");
        //            if (oNodeList != null && oNodeList.Count > 0 && oNodeList[0].Attributes["order"] != null)
        //                _PrintOrder = oNodeList[0].Attributes["order"].Value;
        //            else
        //                _PrintOrder = "Data_Doc DESC,id_Doc DESC";
        //        }
        //        return _PrintOrder;
        //    }
        //}        

        protected HtmlGenericControl SearchInfo;

        private Views _Show;
        protected Views Show
        {
            get
            {
                if (_Show == Views.NotLoaded)
                {
                    if (ShowRequest.IsValid())
                    {
                        switch (ShowRequest.StringValue)
                        {
                            case "all":
                                _Show = Views.ListAll;
                                break;
                            case "search":
                                if (PageData.IsPostBack)
                                    _Show = Views.ListSearch;
                                else
                                    _Show = Views.Search;
                                break;
                            case "finded":
                                _Show = Views.ListSearch;
                                break;
                            case "question":
                                _Show = Views.Question;
                                break;
                            default:
                                _Show = Views.ListThis;
                                break;
                        }
                    }

                }
                return _Show;
            }
        }

        protected enum Views
        {
            NotLoaded,
            ListThis,
            ListAll,
            ListSearch,
            Search,
            Question
        }

        private string _CssClass;
        protected virtual string CssClass
        {
            get
            {
                if (_CssClass == null)
                {
                    _CssClass = " ";
                    if (this.PageData.Folder.CssClass.Length > 0)
                        _CssClass += this.PageData.Folder.CssClass;

                    if (CurrentDocumentID > 0 && DocumentReachable)
                    {
                        Document doc = DocumentBusinessLogic.GetDocumentRecordById(CurrentDocumentID); //NetFrontend.DAL.FileSystemDAL.GetDocumentRecordByID(CurrentDocumentID);
                        if (doc!=null)
                        {
                            if (_CssClass.Length > 1)
                                _CssClass += " ";
                            _CssClass += (doc.CssClass != null ? doc.CssClass : "");
                        }
                    }
                }
                return _CssClass;
            }
        }

        protected abstract string ApplicationCSSClass
        { get;}
        protected abstract string QueryStringKey
        { get;}

        public bool HasCurrentDocument
        {
            get
            {
                return CurrentDocumentID > 0;
            }
        }

        private int? _CurrentDocumentID;
        public override int CurrentDocumentID
        {
            get
            {
                if (_CurrentDocumentID == null)
                {
                    G2Core.Common.RequestVariable doc = new G2Core.Common.RequestVariable(QueryStringKey, G2Core.Common.RequestVariable.RequestType.QueryString);
                    if (doc.IsValidInteger)
                    {
                        _CurrentDocumentID = doc.IntValue;
                    }
                    else
                        _CurrentDocumentID = PageData.Network.HasCurrentDocument ? PageData.Network.CurrentDocument.ID: 0;
                }
                return _CurrentDocumentID.Value;
            }
        }
        
        //protected DataRow DocRecord;
        
        public ApplicationContentsHandler(PageData pagedata):base(pagedata)
        {
            PageData = pagedata;
        }
        
        ///deprecata
        //protected string ChildsFolders()
        //{
        //    string folders = "";
        //    StructureFolder mytree = FolderBusinessLogic.GetFolderRecordByPath(PageData.Folder.Path,PageData.Folder.NetworkID); //NetFrontend.DAL.FileSystemDAL.ListFoldersRecordsBySqlConditions("Path_Folder = '" + PageData.Folder.Path + "'");
        //    if (mytree!=null)
        //        folders = "(Stato_Folder < 2 AND Tree_Folder LIKE '" + mytree.Tree + "%')";
        //    else
        //        throw new Exception();
        //    return folders;
        //}

        protected virtual bool Chiarimenti
        {
            get 
            {
                return false;
            }   
        }

        //public Labels Labels
        //{
        //    get
        //    {
        //        return LabelsManager.LabelsCache.GetTypedLabels(typeof(CommonLabels)) as Labels;
        //    }
        //}

        protected virtual WebControl ChiarimentiControl(string label,int docID)
        {
            WebControl chiarimentiControl = new WebControl(HtmlTextWriterTag.Div);
            chiarimentiControl.CssClass = "chiarimenti";

            HyperLink a = new HyperLink();
            a.NavigateUrl = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/chiarimenti/richiesta.aspx?doc="+docID;
            a.Text = string.Format(FrontCommonLabels[CommonLabels.CommonLabelsList.ServonoChiarimenti], label);
            chiarimentiControl.Controls.Add(a);

            return chiarimentiControl;
        }
    }
   
}