using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Xml;
using NetFrontend.Homepages;
using NetCms.Front;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;


/// <summary>
/// Header Initializer
/// </summary>
/// <remarks>Generate Head Tag for CMS</remarks>
namespace NetFrontend
{
    namespace FrontPiecies
    {
        public class HeaderInitializer
        {
            string cssLinkPattern = "<link type=\"{0}\" media=\"{1}\" rel=\"{2}\" href=\"{3}\" />" + System.Environment.NewLine;
            private PageData PageData;
            private HtmlHead Header;
            
            public HeaderInitializer(HtmlHead Head,PageData pagedata)
            {
                PageData = pagedata;
                Header = Head;
                PageData.HtmlHead = Head;
                
            }

            private bool _UseSimpleMeta = false;
            public bool UseSimpleMeta
            {
                get { return _UseSimpleMeta; }
                set { _UseSimpleMeta = value; }
            }

            public void Init()
            {                
                //HtmlGenericControl div = new HtmlGenericControl("div");
                // Header.Controls.Clear();
               
                PageData.Where.BuildTitle();
              
                InitMeta();

                // Aggiungere i css 
                InitCss();

                InitScripts();

//                int livello = PageData.PageDepth;


                #region oldcode
                //Css css = new Css(PageData);

                //switch (PageData.CurentStyle)
                //{
                //    case PageData.StyleTypes.AltoContrasto:
                //        Header.Controls.Add(css.GetCSS(Css.CssObjects.altocontrasto));
                //    break;

                //    case PageData.StyleTypes.Bianco:
                //        Header.Controls.Add(css.GetCSS(Css.CssObjects.bianco));



                //    break;

                //    case PageData.StyleTypes.Default:



                //        Header.Controls.Add(css.GetCSS(Css.CssObjects.theme));






                //        //Css personalizzati per cartelle
                //        foreach(HtmlLink folderCssStyleSheet in css.Folders)
                //        {
                //            if(folderCssStyleSheet!=null)
                //                Header.Controls.Add(folderCssStyleSheet);
                //        }

                //        if( PageData.CurrentVerticalApplication!=null)
                //            foreach (string vAppCssStyleSheet in PageData.CurrentVerticalApplication.CustomCssStyles)
                //            {
                //                string cssLink = string.Format(cssLinkPattern,"text/css","screen","stylesheet",PageData.AbsoluteRoot + "/css/" + PageData.Network.CssFolder + vAppCssStyleSheet);
                //                Header.Controls.Add(new LiteralControl(cssLink));
                //            }

                //    break;
                //} 
                #endregion


                if (PageData.ShowRssButton && PageData.Folder!=null)
                {
                    string rssLink = "<link rel=\"alternate\" href=\"" + PageData.AbsoluteWebRoot + PageData.Folder.Path + "/rss.xml\" type=\"application/rss+xml\" title=\""+PageData.Folder.Label + " - " + PageData.Network.Label+"\"  />" + System.Environment.NewLine;
                    Header.Controls.Add(new LiteralControl(rssLink));
                }

                if (NetCms.Configurations.ThemeEngine.Status == NetCms.Configurations.ThemeEngine.ThemeEngineStatus.off)
                {
                    // aggiunta Favicon
                    string faviconLink = "<link rel=\"shortcut icon\" href=\"" + PageData.AbsoluteWebRoot + "/imghome/favicon.ico\" type=\"image/x-icon\" />" + System.Environment.NewLine;
                    Header.Controls.Add(new LiteralControl(faviconLink));

                    string faviconLink2 = "<link rel=\"icon\" href=\"" + PageData.AbsoluteWebRoot + "/imghome/favicon.ico\" type=\"image/x-icon\" />" + System.Environment.NewLine;
                    Header.Controls.Add(new LiteralControl(faviconLink2));
                }

                // css per la stampa
                //Css css = new Css(PageData);
                // Header.Controls.Add(css.GetCSS(Css.CssObjects.stampa));
                if (PageData.PageDepth == 1)
                    PageData.Body.Attributes["class"] += " base-homepage";

                CheckForWebDocPage();
            }

            //private string _CssFolder;
            //public string CssFolder
            //{
            //    get 
            //    {
            //        if (_CssFolder == null)
            //        {
            //            string path = "/Portal/configs/Networks/" + PageData.Network + "/frontend/css";
            //            XmlNodeList oNodeList = PageData.XmlConfigs.SelectNodes(path);

            //            if (oNodeList != null && oNodeList.Count > 0)
            //            {

            //                _CssFolder = oNodeList[0].Attributes["folder"].Value + "/";
            //            }
            //        }
            //        return _CssFolder; 
            //    }
            //}

            public void InitScripts()
            {

                if (NetCms.Configurations.ThemeEngine.Status == NetCms.Configurations.ThemeEngine.ThemeEngineStatus.off)
                {
                    Script Scripts = new Script(PageData);
                    foreach (LiteralControl script in Scripts.Scripts)
                        Header.Controls.Add(script);
                }
                else
                {
                    // TODO: ThemeEngine - Da riallocare in quanto gli script vanno inserite alla fine della pagina dopo il footer
                    Header.Controls.Add(new LiteralControl(PageData.CurrentConfig.CurrentTheme.GetJSToHtml()));
                }
            }
            public void InitMeta()
            {
                #region Aggiungo i meta

                string NetworkLabel = PageData.Network.Label;

                if (!UseSimpleMeta)
                {

                    //TITLE                
                    PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta("TITLE", NetworkLabel));

                    //Content-Type
                    PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta("charset", "text/html; charset=utf-8", "Content-Type"));


                    //AUTHOR                
                    PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta("AUTHOR", NetCms.Configurations.PortalData.eMail));

                    //SUBJECT
                    PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta("SUBJECT", NetworkLabel));

                    //RATING                
                    PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta("RATING", "GENERAL"));

                    //KEYWORDS                
                    PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta("KEYWORDS", NetworkLabel));

                    //DESCRIPTION                
                    PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta("DESCRIPTION", NetworkLabel));

                    //OWNER

                    PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta("OWNER", NetworkLabel));

                    //ABSTRACT                
                    PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta("ABSTRACT", NetworkLabel));

                    //REVISIT-AFTER                
                    PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta("REVISIT-AFTER", "1 DAYS"));

                    //LANGUAGE                
                    PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta("LANGUAGE", "IT"));

                    //COPYRIGHT                
                    PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta("COPYRIGHT", NetworkLabel + " " + DateTime.Today.Year.ToString()));

                    //ROBOTS                
                    //RIMOSSO PERCHE' CREAVA PROBLEMI COI MOTORI DI RICERCA
                    //PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta("ROBOTS", "All"));
                }
                else
                {
                    //TITLE                
                    PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta("title", NetworkLabel));
                    //DESCRIPTION                
                    PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta("description", NetworkLabel));
                }

                PageData.PageControls.UpdateDataByFolders();
                #endregion

                //Aggiorno i meta in base alla gerarchia delle homepage
                HomepageData[] Homepages = new HomepageData[PageData.PageDepth+4];
                HomepageData MainHome = PageData.Homepage;
                
                int count=0;
                
                do
                {
                    Homepages[count++] = MainHome;
                    MainHome = MainHome.ParentHomepage;
                }
                while (MainHome != null);

                for (int i = count-1; i >=0; i--)
                {
                    PageData.PageControls.UpdateDataByDoc(Homepages[i].DocumentID);
                }

                if (NetCms.Front.Static.StaticPagesCollector.HasCurrentStaticPage && NetCms.Front.Static.StaticPagesCollector.CurrentStaticPage.Attribute.Meta !=null)
                {
                    foreach (var entry in NetCms.Front.Static.StaticPagesCollector.CurrentStaticPage.Attribute.Meta)
                        PageData.PageControls.Meta.Add(entry.Key.ToUpper(), entry.Value);
                }
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="checkCurrentVerticalApp">Da impostare a false nell'ErrorPageBodyHandler per evitare la ricerca dell'app verticale corrente quando si � nella pagina di errore.</param>
            public void InitCss(bool checkCurrentVerticalApp = true)
            {
                Css css = new Css(PageData);

                if (NetCms.Configurations.ThemeEngine.Status == NetCms.Configurations.ThemeEngine.ThemeEngineStatus.on)
                {
                    Header.Controls.Add(new LiteralControl(PageData.CurrentConfig.CurrentTheme.GetCssInHtml()));
                }
                else
                {
                    switch (PageData.CurentStyle)
                    {
                        case PageData.StyleTypes.AltoContrasto:
                            Header.Controls.Add(css.GetCSS(Css.CssObjects.altocontrasto));
                            break;

                        case PageData.StyleTypes.Bianco:
                            Header.Controls.Add(css.GetCSS(Css.CssObjects.bianco));

                            break;

                        case PageData.StyleTypes.Default:
                            Header.Controls.Add(css.GetCSS(Css.CssObjects.theme));
                            break;
                    }
                }

                //Css personalizzati per cartelle
                foreach (HtmlLink folderCssStyleSheet in css.Folders)
                {
                    if (folderCssStyleSheet != null)
                        Header.Controls.Add(folderCssStyleSheet);
                }
                
                // TODO: ThemeEngine - Sistemare la modalit� di caricamento dei css per le applicazioni verticali

                //Inserito per evitare di consumare risorse alla ricerca di una app verticale al fine di caricare i css, nel caso in cui siamo nella error page
                if (checkCurrentVerticalApp)
                {
                    if (PageData.CurrentVerticalApplication != null)
                        foreach (string vAppCssStyleSheet in PageData.CurrentVerticalApplication.CustomCssStyles)
                        {
                            string cssLink = string.Format(cssLinkPattern, "text/css", "screen", "stylesheet", PageData.AbsoluteRoot + PageData.Network.CssFolder + vAppCssStyleSheet);
                            Header.Controls.Add(new LiteralControl(cssLink));
                        }
                }
                
            }

            //Questa funzione controlla se la pagina navigata � una WebDocs
            //Se lo � imposta il titolo della pagina aggiungendo il titolo del documento
            private void CheckForWebDocPage()
            {
                //string where = "Path_Folder = '" + FrontendNetwork.GetCurrentFolderPath().Replace("'", "''") + "'";
                //where += " AND Name_Doc = '" + PageData.PageName.Replace("'","''") + "'";
                //where += " AND (Tipo_Folder = 2 OR Tipo_Folder = 1)";
                //where += " AND Network_Folder = "+this.PageData.Network.ID;
                //where += " AND Hide_Doc = 0 AND Stato_Doc < 2";

                Document doc = DocumentBusinessLogic.GetDocumentRecordByPathNetworkAndName(FrontendNetwork.GetCurrentFolderPathTranslated().Replace("'", "''"), PageData.PageName.Replace("'", "''"), this.PageData.Network.ID);
                //DataRow row = NetFrontend.DAL.FileSystemDAL.GetDocumentRecordBySqlConditions(where);
                if (doc != null)
                {
                    //PageData.HtmlHead.Title = row[0]["Label_DocLang"] + " � " + PageData.HtmlHead.Title;
                    int DocumentID = doc.ID; //row.IntValue("id_Doc");
                    PageData.PageControls.UpdateDataByDoc(DocumentID,true);
                }
            }
        }
    }
}