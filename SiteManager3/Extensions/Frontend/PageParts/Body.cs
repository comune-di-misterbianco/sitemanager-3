//using System;
//using System.Data;
//using System.Web.UI.HtmlControls;
//using System.Xml;
//using NetCms.Front;
//using NetCms.Structure.WebFS.FileSystemBusinessLogic;
//using NetCms.Structure.WebFS;
//using System.Collections.Generic;

///// <summary>
///// Summary description for Configs
///// </summary>
//namespace NetFrontend
//{
//    namespace FrontPiecies 
//    {
//        public class BodyParts
//        {
//            private PageData PageData;
//            public bool TopMen�Enabled
//            {
//                get
//                {
//                    bool active = true;
                     
//                    XmlNodeList oNodeList =PageData.XmlConfigs.SelectNodes("/Portal/configs/Networks/" + this.PageData.Network.SystemName + "/frontend/menu");
//                    if (oNodeList != null && oNodeList.Count > 0 && oNodeList[0].Attributes["topmenu"]!=null)
//                        active = oNodeList[0].Attributes["topmenu"].Value != "off";

//                    return active;
//                }
//            }

//            public BodyParts(PageData pagedata)
//            {
//                PageData = pagedata;

//                _Where = new HtmlGenericControl("div");
//                PageData.Where = new Where(_Where, PageData);


//                if (PageData.Folder!=null && !PageData.SystemFolders.Contains(PageData.Folder.Name))
//                {
//                    if (!PageData.Folder.NeedVisualization)
//                        throw new NetCms.Exceptions.PageNotFound404Exception();
//                }
//            }

//            public HtmlGenericControl Intestazione
//            {
//                get
//                {
//                    HtmlGenericControl div = new HtmlGenericControl("div");

//                    HtmlGenericControl title = new HtmlGenericControl("div");
//                    title.InnerHtml = IntestazioneHtml;

//                    div.Controls.Add(title);
//                    if (TopMen�Enabled)
//                    {
//                        HtmlGenericControl topmenu = new HtmlGenericControl("div");
//                        topmenu.ID = "topmenu";

//                        HtmlGenericControl topmenu_content = new HtmlGenericControl("div");
//                        topmenu_content.ID = "topmenu_content";
//                        topmenu.Controls.Add(topmenu_content);

//                        ModelMenu topmenu_obj = new NetworkMenu();
//                        topmenu_obj.FirstLevelAllwaysVisible = true;
//                        topmenu_content.Controls.Add(topmenu_obj.GetMenu("head", 1, 1));

//                        div.Controls.Add(topmenu);
//                    }

//                    return div;
//                }
//            }

//            public HtmlGenericControl _Where;
//            public HtmlGenericControl Where
//            {
//                get
//                {
//                    return _Where;
//                }
//            }

//            public HtmlGenericControl ColSX
//            {
//                get
//                {
//                    HtmlGenericControl div = PageData.Homepage.ColSX;
//                    if (div == null)
//                    {
//                        div = new HtmlGenericControl("div");
//                        div.Attributes["class"] = "nascosto";
//                    }

//                    return div;
//                }
//            }
//            public HtmlGenericControl ColDX
//            {
//                get
//                {
//                    HtmlGenericControl div = PageData.Homepage.ColDX;
//                    if (div == null)
//                    {
//                        div = new HtmlGenericControl("div");
//                        div.Attributes["class"] = "nascosto";
//                    }
//                    return div;
//                }
//            }
//            public HtmlGenericControl ColCX
//            {
//                get
//                {
//                    HtmlGenericControl div = new HtmlGenericControl("div");
                    
//                    if (PageData.Folder.CssClass.Length>0)
//                        div.Attributes["class"] = PageData.Folder.CssClass;

//                    if (PageData.Homepage.IsHomepage)
//                    {
//                        if(PageData.Homepage.ColCX!=null)
//                            div = PageData.Homepage.ColCX;
//                    }
//                    else
//                    {
//                        StructureFolder folder = FolderBusinessLogic.GetFolderRecordByPath(PageData.Folder.Path.Replace("'", "''"),PageData.Folder.NetworkID);
//                        //DataRow[] Rows = NetFrontend.DAL.FileSystemDAL.ListFoldersRecordsBySqlConditions(" Path_Folder = '" + PageData.Folder.Path.Replace("'", "''") + "'");
//                        if (folder != null)
//                        {
//                            //DataRow folder = Rows[0];
//                            string Title = folder.Label;
//                            int Depth = folder.Depth;
//                            int hometype = folder.HomeType;
//                            div.InnerHtml = "<h1>" + Title + "</h1>";


//                            div.InnerHtml += "<p>" + folder.Descrizione + "</p>";
//                            if (folder.Tipo == 2)
//                            {

//                                if (hometype < 0/*(i numeri negativi stanno per lista contenuti)*/ )
//                                {
//                                    ModelMenu topmenu_obj = new NetworkMenu();
//                                    topmenu_obj.FirstLevelAllwaysVisible = false;
//                                    topmenu_obj.PrintDescriptions = true;
//                                    div.Controls.Add(topmenu_obj.GetMenu("ColCX", Depth, 0));
//                                }
//                            }
//                            else if (folder.Tipo == 9)
//                            {

//                                IList<Document> links = DocumentBusinessLogic.ListChildsDocumentsRecords(folder.ID);
//                                //DataRow[] links = NetFrontend.DAL.LinksDAL.ListDocumentRecordsBySqlConditions("Folder_Doc = '" + folder.ID + "'");
//                                div.InnerHtml += "<p><ul>";

//                                foreach (Document link in links)
//                                    div.InnerHtml += "<li class=\"contentLink\"><a href=\"?extlink=" + link.ContentRecord.ID /*link["id_Link"]*/ + "\"><strong>" + link.Nome + "</strong></a><br /><em>" + link.Descrizione + "</em></li>";

//                                div.InnerHtml += "</ul></p>";
//                            }
//                        }
//                    }


//                    return div;
//                }
//            }

//            private XmlNode xmlData;
//            public XmlNode XmlData
//            {
//                get
//                {
//                    if (xmlData == null)
//                    {
//                        XmlDocument oXmlDoc = new XmlDocument();
//                        try
//                        {
//                            string xmlFilePath = PageData.Server.MapPath(PageData.AbsoluteRoot + "/configs/" + this.PageData.Network.SystemName.ToLower() + "/parts.xml");
//                            oXmlDoc.Load(xmlFilePath);
//                        }
//                        catch (Exception ex)
//                        {
//                        }
//                        xmlData = oXmlDoc.DocumentElement;
//                    }
//                    return xmlData;
//                }
//            }

//            private string _IntestazioneHtml;
//            private string IntestazioneHtml
//            {
//                get
//                {
//                    if (_IntestazioneHtml == null)
//                    {
//                        XmlNodeList oNodeList = XmlData.SelectNodes("/Portal/intestazione");
//                        if (oNodeList != null && oNodeList.Count > 0)
//                        {
//                            PartsFilters filter = new PartsFilters(PageData);
//                            _IntestazioneHtml = filter.FilterTags(oNodeList[0].InnerXml);
//                        }
//                    }
//                    return _IntestazioneHtml;
//                }
//            }

//        }
//    }
//}