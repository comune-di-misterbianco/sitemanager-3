
/// <summary>
/// Summary description for Configs
/// </summary>
using System.Web.UI.HtmlControls;
namespace NetFrontend
{
    public class CmsBodyHandler : BodyHandler
    {
        public CmsBodyHandler(PageData pagedata, HtmlHead pageHeader)
            : base(pagedata, pageHeader)
        {
        }
    }
}