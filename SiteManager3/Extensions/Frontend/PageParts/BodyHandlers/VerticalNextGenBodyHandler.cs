﻿using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using NetCms.Front;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    public class VerticalNextGenBodyHandler : PageBodyHandler
    {
        //DA VERIFICARE E SISTEMARE
        public NetCms.Networks.NetworkKey NetworkKey
        {
            get
            {
                if (_NetworkKey == null)
                {
                    string fakeNetSysname = NetCms.Networks.NetworksManager.CurrentFakeNetwork;
                    //DataRow[] rows = new NetCms.Networks.NetworksData().Select("Nome_Network LIKE '" + fakeNetSysname + "'");
                    //if (rows.Length == 1) _NetworkKey = new NetCms.Networks.NetworkKey(rows[0]["id_Network"].ToString());
                    var row = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.Select(x => x.Value).First(x=>x.SystemName == fakeNetSysname);
                    if (row != null) _NetworkKey = new NetCms.Networks.NetworkKey(row.ID);
                }

                return _NetworkKey;
            }
        }
        private NetCms.Networks.NetworkKey _NetworkKey;

        public VerticalNextGenBodyHandler(PageData pagedata, HtmlHead pageHeader)
            : base(pagedata, pageHeader)   
        {
            this.PageData.Network.ThemeFolder = this.PageData.CurrentConfig.CurrentTheme.TemplateFolder;
        }

        public override Control BuildControl()
        {
            Control _Control;
            bool logged = NetCms.Users.AccountManager.Logged;
            bool showPasswordRefresh = logged && NetCms.Users.AccountManager.CurrentAccount.NeedPasswordRefresh;

            if (showPasswordRefresh)
                _Control = BuildRefreshPasswordControl();
            else
            {
                try
                {
                    _Control = BuildContent();
                }
                catch (NetCms.Exceptions.NotGrantedException)
                {
                    if (!NetCms.Users.AccountManager.Logged)
                        _Control = BuildLoginControl();
                    else throw;
                }
            }
            return _Control;

            

        }

        public Control BuildContent()
        {
            this.HeaderInitializer.Init();

            Control _Content = new Control();

            _Content.Controls.Add(ZoomSearchControls.StartControl());

            _Content.Controls.Add(NotifyBox);

            if (PageContent == null)
                _Content.Controls.Add(this.PageData.CurrentVerticalApplication.FrontControls(NetworkKey, PageData.ViewState, PageData.IsPostBack));
            else
                _Content.Controls.Add(PageContent);
            _Content.Controls.Add(ZoomSearchControls.StopControl());

            return _Content;
        }

        public override Control BuildLoginControl()
        {
            this.PageData.Network.ThemeFolder = this.PageData.CurrentConfig.CurrentTheme.TemplateFolder;
            this.ForceLayout = new FrontLayouts?(FrontLayouts.None);
            var handler = new NetCms.Users.NeedToLoginControl();
            //this.PageData.ContentsHandler = handler;
            this.PageData.InitHomepage();
            this.HeaderInitializer.UseSimpleMeta = true;
            this.HeaderInitializer.Init();

            return handler;
        }

        public override Control BuildRefreshPasswordControl()
        {
            this.PageData.Network.ThemeFolder = this.PageData.CurrentConfig.CurrentTheme.TemplateFolder;
            this.ForceLayout = new FrontLayouts?(FrontLayouts.None);
            var handler = new NetCms.Users.NeedToLoginControl();
            this.PageData.InitHomepage();
            this.HeaderInitializer.UseSimpleMeta = true;
            this.HeaderInitializer.Init();

            return new NetCms.Structure.Grants.UsersGrants.FrontendUserModPasswordControl();
        }
    }
}