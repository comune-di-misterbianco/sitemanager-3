using System.Web.UI;
using System.Web.UI.HtmlControls;
using NetCms.Front;
using static NetCms.Structure.Grants.UsersGrants;

namespace NetFrontend
{
    /// <summary>
    /// Mantenuto per retrocompatibilitÓ
    /// </summary>
    public class BodyHandler : PageBodyHandler
    {
        public FrontendNetwork CurrentNetwork
        {
            get
            {
                return _CurrentNetwork ?? (_CurrentNetwork = FrontendNetwork.GetCurrentFrontendNetwork());
            }
        }
        private FrontendNetwork _CurrentNetwork;

        public BodyHandler(PageData pagedata, HtmlHead pageHeader)
            : base(pagedata, pageHeader)
        {
        }
        public BodyHandler(PageData pagedata, HtmlHead pageHeader, bool initBreadcrumbs)
            : base(pagedata, pageHeader,initBreadcrumbs)
        {
        }

        public override Control BuildControl()
        {
            if (CurrentNetwork.CurrentFolder != null && CurrentNetwork.CurrentFolder.Restricted)
            {
                if (NetCms.Users.AccountManager.Logged)
                {
                    if (CurrentNetwork.CurrentFolder.Grant)
                        return BuildGrantedControl();
                    else
                        throw new NetCms.Exceptions.NotGrantedException();
                }
                else
                {
                    throw new NetCms.Exceptions.NotGrantedException();
                }
            }
            else return BuildGrantedControl();
        }
        public Control BuildGrantedControl()
        {
            this.HeaderInitializer.Init();
            HtmlGenericControls.Div globale = new HtmlGenericControls.Div("globale");
            HtmlGenericControls.Div intestazione = new HtmlGenericControls.Div("Intestazione");
            HtmlGenericControls.Div corpo = new HtmlGenericControls.Div("corpo");
            HtmlGenericControls.Div where = new HtmlGenericControls.Div("Where");

            HtmlGenericControls.Div colCX = new HtmlGenericControls.Div("ColCX");
            HtmlGenericControls.Div colCX_Gfx1 = new HtmlGenericControls.Div();
            colCX_Gfx1.Class = "ColCX_Gfx1";
            HtmlGenericControls.Div colCX_Content = new HtmlGenericControls.Div("ColCX_Content");
            colCX_Gfx1.Class = "ColCX_Gfx2";

            HtmlGenericControls.Div colDX = new HtmlGenericControls.Div("ColDX");
            HtmlGenericControls.Div colSX = new HtmlGenericControls.Div("ColSX");
            HtmlGenericControls.Div footer = new HtmlGenericControls.Div("Footer");

            globale.Controls.Add(intestazione);
            intestazione.Controls.Add(this.Intestazione);

            globale.Controls.Add(corpo);

            corpo.Controls.Add(where);

            if (!NetCms.Users.AccountManager.Logged || (NetCms.Users.AccountManager.Logged && !NetCms.Users.AccountManager.CurrentAccount.NeedPasswordRefresh))
            {
                corpo.Controls.Add(colCX);
                colCX.Controls.Add(colCX_Gfx1);
                colCX_Gfx1.Controls.Add(colCX_Content);

                if (this.PageData.Homepage.ShowColDX)
                {
                    corpo.Controls.Add(colDX);
                    colDX.Controls.Add(this.ColDX);
                }

                if (this.PageData.Homepage.ShowColSX)
                {
                    corpo.Controls.Add(colSX);
                    colSX.Controls.Add(this.ColSX);
                }

                colCX_Content.Controls.Add(ZoomSearchControls.StartControl());

                colCX_Content.Controls.Add(NotifyBox);

                if (PageContent == null)
                {
                    var handler = this.PageData.ContentsHandler;
                    if (handler != null)
                    {
                        if (handler.IsPreview)
                            colCX_Content.Controls.Add(handler.PreviewControl);
                        else
                        {
                            if (handler.CurrentDocument == null || handler.CurrentDocument.Grant)
                                colCX_Content.Controls.Add(handler.Control);
                            else
                                throw new NetCms.Exceptions.NotGrantedException();
                        }
                    }
                    else
                        //throw new NetCms.Exceptions.PageNotFound404Exception();
                        throw new System.Web.HttpException(404, "File Not Found");
                }
                else
                    colCX_Content.Controls.Add(PageContent);
                colCX_Content.Controls.Add(ZoomSearchControls.StopControl());
            }
            else
            {
                colCX_Content.Controls.Add(new FrontendUserModPasswordControl());
            }

            where.Controls.Add(this.Where);
            footer.Controls.Add(new Footer(PageData).GetControl());
          
            globale.Controls.Add(footer);
          
            if (PageData.Folder != null && !string.IsNullOrEmpty(PageData.Folder.Path))
            {
                
                string[] bodyID = PageData.Folder.Path.Split('/');
                string css_path = "Folder_" + PageData.Folder.KeyTypeName;

                if (PageData.Body != null && bodyID.Length > 0)
                {
                    PageData.Body.ID = bodyID[1];                  
                    css_path = css_path + string.Join(" ", bodyID);

                    if (!string.IsNullOrEmpty(css_path))
                        PageData.Body.Attributes["class"] = css_path;
                }
            }
            return globale;
        }
        public Control BuildCxControl()
        {
            if (CurrentNetwork.CurrentFolder != null)
            {
                if (!CurrentNetwork.CurrentFolder.Restricted || (CurrentNetwork.CurrentFolder.Restricted && NetCms.Users.AccountManager.Logged))
                {
                    if (CurrentNetwork.CurrentFolder.Grant)
                    {
                        HtmlGenericControls.Div globale = new HtmlGenericControls.Div("");

                        if (!NetCms.Users.AccountManager.Logged || (NetCms.Users.AccountManager.Logged && !NetCms.Users.AccountManager.CurrentAccount.NeedPasswordRefresh))
                        {
                            globale.Controls.Add(ZoomSearchControls.StartControl());
                            globale.Controls.Add(NotifyBox);


                            if (PageContent == null)
                            {
                                var handler = this.PageData.ContentsHandler;
                                if (handler != null)
                                {
                                    if (this.PageData.Folder != null && this.PageData.Folder.StateValue != 2)
                                    {
                                        if (handler.IsPreview)
                                            globale.Controls.Add(handler.PreviewControl);
                                        else
                                        {
                                            if (handler.CurrentDocument == null || handler.CurrentDocument.Grant)
                                                globale.Controls.Add(handler.Control);
                                            else
                                                throw new NetCms.Exceptions.NotGrantedException();
                                        }
                                    }
                                    else
                                    {
                                        // cartella non raggiungibile
                                        if (handler.IsPreview) // verifica per anteprima
                                            globale.Controls.Add(handler.PreviewControl);
                                        else
                                            throw new System.Web.HttpException(404, "File Not Found");
                                    }
                                }
                                else
                                {
                                    //throw new NetCms.Exceptions.PageNotFound404Exception();
                                    throw new System.Web.HttpException(404, "File Not Found");
                                }
                            }
                            else
                            {
                                globale.Controls.Add(PageContent);
                            }

                            globale.Controls.Add(ZoomSearchControls.StopControl());
                        }
                        else
                        {
                            globale.Controls.Add(new FrontendUserModPasswordControl());
                        }

                        if (PageData.Folder != null && !string.IsNullOrEmpty(PageData.Folder.Path))
                        {

                            string[] bodyID = PageData.Folder.Path.Split('/');
                            string css_path = "Folder_" + PageData.Folder.KeyTypeName;

                            if (PageData.Body != null && bodyID.Length > 0)
                            {
                                PageData.Body.ID = bodyID[1];
                                css_path = css_path + string.Join(" ", bodyID);

                                if (!string.IsNullOrEmpty(css_path))
                                    PageData.Body.Attributes["class"] = css_path;
                            }
                        }
                        return globale;
                    }
                    else
                        throw new NetCms.Exceptions.NotGrantedException();
                }
                else
                {
                    return new NetCms.Users.NeedToLoginControl();
                }
            }
            else
            {
                // folder is null
                throw new System.Web.HttpException(404, "File Not Found");
            }
        }
    }
}
