using System.Web.UI.HtmlControls;
using NetCms.Front.Static;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using LabelsManager;
using NetCms.Front;
using LanguageManager.BusinessLogic;
using System.Web;
using System.Linq;

namespace NetFrontend
{
/// <summary>
    /// Static Page Body Handler
/// </summary>
    public class StaticPageBodyHandler : PageBodyHandler
    {
        public StaticPageBodyHandler(PageData pagedata, HtmlHead pageHeader)
            : base(pagedata, pageHeader)
        {
            this.PageData.Network.ThemeFolder = this.PageData.CurrentConfig.CurrentTheme.TemplateFolder;
            this.HeaderInitializer.UseSimpleMeta = true;
            this.HeaderInitializer.Init();
            
        }

        public StaticPageBodyHandler(PageData pagedata, HtmlHead pageHeader, bool needToClearCache = false)
            : base(pagedata, pageHeader)
        {
            NeedToClearFrontCache = needToClearCache;
            this.PageData.Network.ThemeFolder = this.PageData.CurrentConfig.CurrentTheme.TemplateFolder;
            this.PageData.NeedToClearFrontCache = needToClearCache;
            this.HeaderInitializer.UseSimpleMeta = true;
            this.HeaderInitializer.Init();                       
        }

        /// <summary>
        /// Inserito per forzare la cancellazione della cache in caso di cambio lingua
        /// </summary>
        public bool NeedToClearFrontCache
        {
            get; set;
        }

        #region Labels
        private LabelsManager.Labels Labels
        {
            get
            {
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(CommonLabels)) as Labels;
            }
        }
        #endregion

        #region Control2Html
        public string ControltoHtml(WebControl temp)
        {
            if (temp == null)
                return "";
            else
            {
                TextWriter tw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(tw);
                temp.RenderControl(hw);
                return tw.ToString();
            }
        }
        public string ControltoHtml(Control temp)
        {
            if (temp == null)
                return "";
            else
            {
                TextWriter tw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(tw);
                temp.RenderControl(hw);
                return tw.ToString();
            }
        }
        #endregion

        #region Header

        /// <summary>
        /// Section Header
        /// </summary>
        public HtmlGenericControl Header
        {
            get
            {
                return _Header ?? (_Header = BuildHeader());
            }
        }
        private HtmlGenericControl _Header;

        /// <summary>
        /// Header from Parts.xml
        /// </summary>
        protected string HeaderHtml
        {
            get
            {
                if (_HeaderHtml == null)
                {
                    System.Xml.XmlNodeList oNodeList = XmlData.SelectNodes("/Portal/intestazione");
                    if (oNodeList != null && oNodeList.Count > 0)
                    {
                        PartsFilters filter = new PartsFilters(PageData);
                        _HeaderHtml = filter.FilterTags(oNodeList[0].InnerXml);
                    }
                }
                return _HeaderHtml;
            }
        }
        private string _HeaderHtml;

        #region LoginControl

        /// <summary>
        /// Stabilisce se la login appesa alla navbar � di tipo modale o dropdown
        /// </summary>
        public bool LoginHeadModal
        {
            get
            {
                string value = NetCms.Configurations.XmlConfig.GetAttribute("/Portal/configs/login", "logincontrolmodal");
                bool active = !string.IsNullOrEmpty(value) && value != "off";
                return active;
            }
        }

        /// <summary>
        /// Login/Registration Access into Navbar
        /// </summary>
        /// <remarks> La login a dropdown al momento non funziona. La generazione della login come dropdown non pu� essere generata come html da aggiungere a quello prelevato dal parts.xml
        /// Si dovrebbe generare tutta la navbar come Webcontrol ed appenderla direttamente nel Default.aspx</remarks>       
        public WebControl NavLoginControlBox
        {
            get
            {
                if (_NavLoginControlBox == null)
                {
                    string[] loginUrls = new string[] { "/user/accesso.aspx", "/en/user/signin.aspx" };

                    string currentPagePath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;

                    if (LoginHeadStatus && /*!currentPagePath.Contains("user/accesso.aspx")*/ !loginUrls.Any(currentPagePath.Contains))
                    {
                        var currentNetwork = NetCms.Networks.NetworksManager.CurrentActiveNetwork;
                        string awp = (currentNetwork != null) ? currentNetwork.Paths.AbsoluteFrontRoot : NetCms.Configurations.Paths.AbsoluteRoot;
                        string strLinkReg = awp + "/" + UrlRegistrazione;

                        if (!LoginHeadModal)
                        {
                            _NavLoginControlBox = new WebControl(HtmlTextWriterTag.Ul);
                            _NavLoginControlBox.CssClass = "nav navbar-nav navbar-right";

                            if (!NetCms.Users.AccountManager.Logged)
                            {
                                if (!string.IsNullOrEmpty(UrlRegistrazione))
                                {
                                    WebControl regControl = new WebControl(HtmlTextWriterTag.Li);

                                    WebControl linkReg = new WebControl(HtmlTextWriterTag.A);
                                    linkReg.Attributes["href"] = strLinkReg;
                                    linkReg.Controls.Add(new LiteralControl(this.Labels[CommonLabels.CommonLabelsList.RegistatiLabelLink]));
                                    regControl.Controls.Add(linkReg);
                                    _NavLoginControlBox.Controls.Add(regControl);
                                }

                                WebControl LiLogin = new WebControl(HtmlTextWriterTag.Li);
                                LiLogin.CssClass = "dropdown";

                                WebControl loginButton = new WebControl(HtmlTextWriterTag.A);
                                loginButton.ID = "loginButton";
                                loginButton.Attributes["href"] = "#";
                                loginButton.CssClass = "dropdown-toggle";
                                loginButton.Attributes.Add("role", "button");
                                loginButton.Attributes.Add("data-toggle", "dropdown");
                                loginButton.Attributes.Add("aria-haspopup", "true");
                                loginButton.Attributes.Add("aria-expanded", "false");

                                loginButton.Controls.Add(new LiteralControl(this.Labels[CommonLabels.CommonLabelsList.LoginLabelLink]));// + "<span class=\"caret\"></span>"));
                                LiLogin.Controls.Add(loginButton);

                                WebControl loginBox = new WebControl(HtmlTextWriterTag.Ul);
                                loginBox.ID = "loginBox";
                                loginBox.CssClass = "dropdown-menu";

                                NetCms.Users.LoginControlv3 loginCtrl = new NetCms.Users.LoginControlv3
                                (
                                    currentNetwork.Paths.AbsoluteFrontRoot,
                                    MostraRegistrazione,
                                    MostraRecuperoPassword,
                                    strLinkReg
                                );

                                loginCtrl.IsHeadLogin = this.LoginHeadStatus;
                                loginCtrl.IsModalLogin = this.LoginHeadModal;

                                loginBox.Controls.Add(loginCtrl);

                                LiLogin.Controls.Add(loginBox);
                                _NavLoginControlBox.Controls.Add(LiLogin);
                            }
                            else
                            {
                                NetCms.Users.User datiuser = NetCms.Users.AccountManager.CurrentAccount;

                                string nomeCompleto = (!string.IsNullOrEmpty(datiuser.NomeCompleto)) ? datiuser.NomeCompleto : datiuser.UserName;

                                WebControl liUserName = new WebControl(HtmlTextWriterTag.Li);
                                liUserName.ID = "username";
                                liUserName.CssClass = "dropdown";

                                WebControl aName = new WebControl(HtmlTextWriterTag.A);
                                aName.CssClass = "dropdown-toggle";
                                aName.Attributes.Add("role", "button");
                                aName.Attributes.Add("data-toggle", "dropdown");
                                aName.Attributes.Add("aria-haspopup", "true");
                                aName.Attributes.Add("aria-expanded", "false");

                                WebControl iconUser = new WebControl(HtmlTextWriterTag.Span);
                                iconUser.CssClass = "glyphicon glyphicon-user";
                                aName.Controls.Add(iconUser);

                                aName.Controls.Add(new LiteralControl(nomeCompleto));

                                WebControl iconCaret = new WebControl(HtmlTextWriterTag.Span);
                                iconCaret.CssClass = "caret";
                                aName.Controls.Add(iconCaret);

                                liUserName.Controls.Add(aName);
                                _NavLoginControlBox.Controls.Add(liUserName);

                                WebControl loggedList = new WebControl(HtmlTextWriterTag.Ul);
                                loggedList.CssClass = "dropdown-menu";

                                loggedList.Controls.Add(new LiteralControl(this.Profilo));
                                loggedList.Controls.Add(new LiteralControl(this.EditProfilo));
                                loggedList.Controls.Add(new LiteralControl(this.ModificaPassword));

                                loggedList.Controls.Add(new LiteralControl("<li role=\"separator\" class=\"divider\"><li>"));

                                if (NetCms.Configurations.Generics.NotifyEnabled)
                                {
                                    loggedList.Controls.Add(new LiteralControl(this.ElencoNotifiche));
                                    loggedList.Controls.Add(new LiteralControl("<li role=\"separator\" class=\"divider\"><li>"));
                                }

                                if (ChiarimentiIsInstalled) // Se l'app chiarimenti � installata allora aggiungo il link
                                {
                                    loggedList.Controls.Add(new LiteralControl(this.ElencoChiarimenti));
                                    loggedList.Controls.Add(new LiteralControl("<li role=\"separator\" class=\"divider\"><li>"));
                                }

                                loggedList.Controls.Add(new LiteralControl(this.Logout));

                                _NavLoginControlBox.Controls.Add(loggedList);
                            }
                            return _NavLoginControlBox;
                        }
                        else
                        {
                            _NavLoginControlBox = new WebControl(HtmlTextWriterTag.Ul);
                            _NavLoginControlBox.CssClass = "nav navbar-nav navbar-right";

                            if (!NetCms.Users.AccountManager.Logged)
                            {
                                if (!string.IsNullOrEmpty(UrlRegistrazione))
                                {
                                    WebControl regControl = new WebControl(HtmlTextWriterTag.Li);

                                    WebControl linkReg = new WebControl(HtmlTextWriterTag.A);
                                    linkReg.Attributes["href"] = strLinkReg;
                                    linkReg.Controls.Add(new LiteralControl(this.Labels[CommonLabels.CommonLabelsList.RegistatiLabelLink]));
                                    regControl.Controls.Add(linkReg);
                                    _NavLoginControlBox.Controls.Add(regControl);
                                }

                                WebControl LiLogin = new WebControl(HtmlTextWriterTag.Li);
                                LiLogin.CssClass = "dropdown";
                                LiLogin.Attributes.Add("role", "presentation");

                                WebControl loginButton = new WebControl(HtmlTextWriterTag.A);
                                loginButton.ID = "loginButton";
                                //loginButton.Attributes["href"] = "#";
                                //loginButton.CssClass = "dropdown-toggle";
                                loginButton.Attributes.Add("role", "button");
                                loginButton.Attributes.Add("data-toggle", "modal");
                                loginButton.Attributes.Add("data-target", "LoginModal");
                                //loginButton.Attributes.Add("aria-haspopup", "true");
                                //loginButton.Attributes.Add("aria-expanded", "false");

                                loginButton.Controls.Add(new LiteralControl(this.Labels[CommonLabels.CommonLabelsList.LoginLabelLink]));// + "<span class=\"caret\"></span>"));
                                LiLogin.Controls.Add(loginButton);

                                _NavLoginControlBox.Controls.Add(LiLogin);
                            }
                            else
                            {
                                NetCms.Users.User datiuser = NetCms.Users.AccountManager.CurrentAccount;

                                string nomeCompleto = (!string.IsNullOrEmpty(datiuser.NomeCompleto)) ? datiuser.NomeCompleto : datiuser.UserName;

                                WebControl liUserName = new WebControl(HtmlTextWriterTag.Li);
                                //liUserName.ID = "username";
                                liUserName.CssClass = "dropdown";

                                WebControl aName = new WebControl(HtmlTextWriterTag.A);
                                aName.CssClass = "dropdown-toggle";
                                //aName.Attributes.Add("href", "#");
                                aName.Attributes.Add("role", "button");
                                aName.Attributes.Add("data-toggle", "dropdown");
                                aName.Attributes.Add("aria-haspopup", "true");
                                aName.Attributes.Add("aria-expanded", "false");

                                WebControl iconUser = new WebControl(HtmlTextWriterTag.Span);
                                iconUser.CssClass = "glyphicon glyphicon-user";

                                WebControl iconCaret = new WebControl(HtmlTextWriterTag.Span);
                                iconCaret.CssClass = "caret";

                                aName.Controls.Add(iconUser);
                                aName.Controls.Add(new LiteralControl(" " + nomeCompleto + " "));
                                aName.Controls.Add(iconCaret);

                                liUserName.Controls.Add(aName);

                                WebControl loggedList = new WebControl(HtmlTextWriterTag.Ul);
                                loggedList.CssClass = "dropdown-menu";
                                loggedList.Attributes.Add("aria-labelledby", "UserMenu");
                                loggedList.Attributes.Add("role", "menu");

                                loggedList.Controls.Add(new LiteralControl(this.Profilo));
                                loggedList.Controls.Add(new LiteralControl(this.EditProfilo));
                                loggedList.Controls.Add(new LiteralControl(this.ModificaPassword));

                                loggedList.Controls.Add(new LiteralControl("<li role=\"separator\" class=\"divider\"><li>"));

                                if (NetCms.Configurations.Generics.NotifyEnabled)
                                {
                                    loggedList.Controls.Add(new LiteralControl(this.ElencoNotifiche));
                                    loggedList.Controls.Add(new LiteralControl("<li role=\"separator\" class=\"divider\"><li>"));
                                }

                                if (ChiarimentiIsInstalled) // Se l'app chiarimenti � installata allora aggiungo il link
                                {
                                    loggedList.Controls.Add(new LiteralControl(this.ElencoChiarimenti));
                                    loggedList.Controls.Add(new LiteralControl("<li role=\"separator\" class=\"divider\"><li>"));
                                }

                                loggedList.Controls.Add(new LiteralControl(this.Logout));

                                liUserName.Controls.Add(loggedList);

                                _NavLoginControlBox.Controls.Add(liUserName);

                            }

                            return _NavLoginControlBox;
                        }
                    }
                    else
                        return null;
                }
                return _NavLoginControlBox;
            }
        }
        private WebControl _NavLoginControlBox;

        private LoginManager.Controls.LoginHeaderControl _LoginCtrl;
        public LoginManager.Controls.LoginHeaderControl LoginCtrl
        {
            get
            {

                if (_LoginCtrl == null)
                    _LoginCtrl = new LoginManager.Controls.LoginHeaderControl
                        (
                        NetCms.Networks.NetworksManager.CurrentActiveNetwork.ID,
                        NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot,
                        NetCms.Users.AccountManager.Logged,
                        NetCms.Users.AccountManager.CurrentAccount,
                        (this.PageData.CurrentConfig.CurrentTheme.ParentTheme != null &&
                        !string.IsNullOrEmpty(this.PageData.CurrentConfig.CurrentTheme.ParentTheme.Name)
                        ? this.PageData.CurrentConfig.CurrentTheme.ParentTheme.Name : this.PageData.CurrentConfig.CurrentTheme.Name),
                        true
                        );

                return _LoginCtrl;
            }

        }

        /// <summary>
        /// Modal Login Box
        /// </summary>
        /// <remarks>Se LoginHeadModal � on il LoginControlv3 verr� appeso all'header</remarks>
        public Control ModalLoginBox()
        {
            string currentPagePath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            var currentNetwork = NetCms.Networks.NetworksManager.CurrentActiveNetwork;
            string awp = (currentNetwork != null) ? currentNetwork.Paths.AbsoluteFrontRoot : NetCms.Configurations.Paths.AbsoluteRoot;
            string strLinkReg = awp + "/" + UrlRegistrazione;

            System.Web.UI.WebControls.WebControl divInt = new System.Web.UI.WebControls.WebControl(HtmlTextWriterTag.Div);


            NetCms.Users.LoginControlv3 loginCtrl = new NetCms.Users.LoginControlv3
               (
                    !string.IsNullOrEmpty(currentNetwork.Paths.AbsoluteFrontRoot) ? currentNetwork.Paths.AbsoluteFrontRoot + "/default.aspx" : HttpContext.Current.Request.Url.AbsolutePath + HttpContext.Current.Request.Url.Query,
                   MostraRegistrazione,
                   MostraRecuperoPassword,
                   strLinkReg
               );


            loginCtrl.IsHeadLogin = true;
            loginCtrl.IsModalLogin = true; // this.LoginHeadStatus;

            divInt.Controls.Add(loginCtrl);


            return divInt;
        }

        #endregion

        /// <summary>
        /// Network Menu
        /// </summary>
        /// <returns>Menu navbar for network's folder</returns>                                   
        private Control TopMenu()
        {
            if (TopMenuEnabled)
            {
                NetCms.Front.ModelMenu topmenu_obj = new NetCms.Front.NetworkMenu(NeedToClearFrontCache);
                topmenu_obj.FirstLevelAllwaysVisible = true;

                //if (PageData.CurrentConfig.Layout != CmsConfigs.Model.Config.LayoutType.Responsive)
                //if (NetCms.Configurations.ThemeEngine.Status == NetCms.Configurations.ThemeEngine.ThemeEngineStatus.on)
                //{                  
                //    return topmenu_obj.GetMegamenu("head", 1, 1);
                //}
                //else
                //{
                    return topmenu_obj.GetResponsiveMenu("head", 1, 1);
                //}
            }
            else return null;
        }

        //private Control MobileMenu()
        //{
        //    if (TopMenuEnabled)
        //    {
        //        ModelMenu topmenu_obj = new NetworkMenu(NeedToClearFrontCache);
        //        topmenu_obj.FirstLevelAllwaysVisible = true;

        //        string cssClassForRootElement = "Linklist Linklist--padded Treeview Treeview--default js-Treeview";

        //        switch (PageData.CurrentConfig.TopMenuStyle)
        //        {
        //            case CmsConfigs.Model.Config.MenuType.Normal:
        //                return topmenu_obj.GetMobileMenu("head", 1, 1);
        //            case CmsConfigs.Model.Config.MenuType.Megamenu:
        //                return topmenu_obj.GetMobileMenu("head", 1, 3, true, false, false, cssClassForRootElement);
        //            default:
        //                return topmenu_obj.GetMobileMenu("head", 1, 3, true, false, false, cssClassForRootElement);
        //        }
        //    }
        //    else return null;
        //}
        // TODO: da completare la gestione dinaminca del menu
        private Control MobileMenu()
        {
            if (TopMenuEnabled)
            {
                ModelMenu topmenu_obj = new NetworkMenu(NeedToClearFrontCache);
                topmenu_obj.FirstLevelAllwaysVisible = true;
                topmenu_obj.TplPath = PageData.CurrentConfig.CurrentTheme.TemplateFolder;

                string cssClassForRootElement = string.Empty;

                // TODO: ThemeEngine - da ridefinire da hard coded a parametri generici forniti dal tema
                if (PageData.CurrentConfig != null)
                {
                    if (PageData.CurrentConfig.Theme.ToLower() == "AGID".ToLower() || PageData.CurrentConfig.CurrentTheme.InheritFrom.ToLower() == "AGID".ToLower())
                        cssClassForRootElement = "Linklist Linklist--padded Treeview Treeview--default js-Treeview";
                    else if (PageData.CurrentConfig.Theme.ToLower() == "AGID2019".ToLower() || PageData.CurrentConfig.CurrentTheme.InheritFrom.ToLower() == "AGID2019".ToLower())
                        cssClassForRootElement = "navbar-nav";
                }

                if (PageData.CurrentConfig.TopMenuStyle == CmsConfigs.Model.Config.MenuType.Normal)
                {
                    // TODO: ThemeEngine - da ridefinire da hard coded -> i parametri generici devono essere forniti dal tema
                    if (PageData.CurrentConfig.Theme.ToLower() == "AGID".ToLower() || PageData.CurrentConfig.CurrentTheme.InheritFrom.ToLower() == "AGID".ToLower())
                        return topmenu_obj.GetMobileMenu("head", 1, 1, false, false, false, cssClassForRootElement);
                    else
                        return topmenu_obj.GetMenuWithTemplate("head", 1, 1, false, false, false, false, cssClassForRootElement);

                }
                else if (PageData.CurrentConfig.TopMenuStyle == CmsConfigs.Model.Config.MenuType.Megamenu)
                {
                    // TODO: ThemeEngine - da ridefinire da hard coded -> i parametri generici devono essere forniti dal tema
                    if (PageData.CurrentConfig.Theme.ToLower() == "AGID".ToLower() || PageData.CurrentConfig.CurrentTheme.InheritFrom.ToLower() == "AGID".ToLower())
                        return topmenu_obj.GetMobileMenu("head", 1, 3, true, false, false, cssClassForRootElement);
                    else
                        return topmenu_obj.GetMenuWithTemplate("head", 1, 3, true, false, false, true, cssClassForRootElement);
                }
                else
                    return null;
            }
            else
                return null;
        }


        /// <summary>
        /// Header builder
        /// </summary>
        /// <returns>Header Builder Method</returns>
        //private HtmlGenericControl BuildHeader()
        //{
        //    HtmlGenericControl tempControl = new HtmlGenericControl("div");

        //    string strTopMenu = ControltoHtml(TopMenu());

        //    string strMobileMenu = "";

        //    //if (PageData.CurrentConfig != null && PageData.CurrentConfig.Layout != CmsConfigs.Model.Config.LayoutType.Responsive)
        //    if (NetCms.Configurations.ThemeEngine.Status == NetCms.Configurations.ThemeEngine.ThemeEngineStatus.on)          
        //        strMobileMenu = ControltoHtml(MobileMenu());

        //    tempControl.InnerHtml = HeaderHtml.Replace("%%TopMenuElement%%", strTopMenu)
        //                                      .Replace("%%MobileMenu%%", strMobileMenu)
        //                                      .Replace("%%LoginControl%%", ControltoHtml(NavLoginControlBox));


        //    //tempControl.InnerHtml = HeaderHtml.Replace("%%TopMenuElement%%", ControltoHtml(TopMenu()))
        //    //                        .Replace("%%LoginControl%%", ControltoHtml(NavLoginControlBox));
        //    return tempControl;
        //}

        /// <summary>
        /// Header builder
        /// </summary>
        /// <returns>Header Builder Method</returns>
        private HtmlGenericControl BuildHeader()
        {
            HtmlGenericControl tempControl = new HtmlGenericControl("div");

            string strMobileMenu = string.Empty;
            string strTopMenu = string.Empty;

            if (TopMenuEnabled)
            {
                if (NetCms.Configurations.ThemeEngine.Status == NetCms.Configurations.ThemeEngine.ThemeEngineStatus.off)//PageData.CurrentConfig.TopMenuStyle == CmsConfigs.Model.Config.MenuType.Normal)
                    strTopMenu = ControltoHtml(TopMenu());

                if (NetCms.Configurations.ThemeEngine.Status == NetCms.Configurations.ThemeEngine.ThemeEngineStatus.on) //if (PageData.CurrentConfig != null && PageData.CurrentConfig.Layout != CmsConfigs.Model.Config.LayoutType.Responsive)
                    strMobileMenu = ControltoHtml(MobileMenu());
            }

            string strCodeLogin = string.Empty;

            if (!NetCms.Configurations.Generics.LoginManager)
                strCodeLogin = ControltoHtml(NavLoginControlBox);
            else
            {
                strCodeLogin = ControltoHtml(LoginCtrl);
            }


            tempControl.InnerHtml = HeaderHtml.Replace("%%TopMenuElement%%", strTopMenu)
                                              .Replace("%%MobileMenu%%", strMobileMenu)
                                              .Replace("%%LoginControl%%", strCodeLogin)
                                              .Replace("%%CoverIMG%%", "");


            return tempControl;
        }


        #endregion

        #region ColCx

        public Control ColCx
        {
            get
            {
                bool logged = NetCms.Users.AccountManager.Logged;
                bool showPasswordRefresh = logged && NetCms.Users.AccountManager.CurrentAccount.NeedPasswordRefresh;

                if (showPasswordRefresh)
                    return new NetCms.Structure.Grants.UsersGrants.FrontendUserModPasswordControl();
                return _ColCx ?? (_ColCx = this.PageData.Homepage.ColCX);
            }
        }
        private HtmlGenericControl _ColCx;

        #endregion

        #region Footer

        private HtmlGenericControl _Footer;
        public HtmlGenericControl Footer
        {
            get
            {
                return _Footer ?? (_Footer = BuildFooter());
            }
        }

        private string _FooterHtml;
        protected string FooterHtml
        {
            get
            {
                if (_FooterHtml == null)
                {
                    System.Xml.XmlNodeList oNodeList = XmlData.SelectNodes("/Portal/footer");
                    if (oNodeList != null && oNodeList.Count > 0)
                    {
                        PartsFilters filter = new PartsFilters(PageData);
                        _FooterHtml = filter.FilterTags(oNodeList[0].InnerXml);
                    }
                }
                return _FooterHtml;
            }
        }

        private HtmlGenericControl BuildFooter()
        {
            HtmlGenericControl tempControl = new HtmlGenericControl("div");

            tempControl.InnerHtml = FooterHtml;
            return tempControl;
        }
        #endregion

        public override Control BuildControl()
        {
            if (StaticPagesCollector.HasCurrentStaticPage)
            {
                var datiPagina = StaticPagesCollector.CurrentStaticPage.Attribute;
                var classToInstance = StaticPagesCollector.CurrentStaticPage.Type;

                Control ctr = null;
                if (!datiPagina.OnlyLoggedUsers || NetCms.Users.AccountManager.Logged)
                {
                    var costruttore = classToInstance.GetConstructor(new[] { typeof(PageData) });
                    if (costruttore != null)
                    {
                        ctr = costruttore.Invoke(new[] { PageData }) as Control;
                    }
                    else
                    {
                        costruttore = classToInstance.GetConstructor(new Type[] { });
                        if (costruttore != null)
                            ctr = costruttore.Invoke(new object[] { }) as Control;
                    }
                }
                else
                {
                    if (!NetCms.Users.AccountManager.Logged)
                        ctr = LoginControl();
                    else
                    throw new NetCms.Exceptions.NotGrantedException();
                }


                if (ctr != null)
                {
                    if (datiPagina.ShowInFullScreen)
                    {
                        HeaderInitializer.InitScripts();
                        return ctr;
                    }
                    else
                        //return BuildFullBody(ctr);
                        return BuildCxControl(ctr);
                }
            }

            // throw new NetCms.Exceptions.PageNotFound404Exception();
            throw new System.Web.HttpException(404, "File Not Found");
        }

        public Control LoginControl()
        {
            return new NetCms.Users.NeedToLoginControl();
        }

        public Control BuildCxControl(Control ctr)
        {
            
            HtmlGenericControls.Div globale = new HtmlGenericControls.Div("");

            //if (!NetCms.Users.AccountManager.Logged || (NetCms.Users.AccountManager.Logged && !NetCms.Users.AccountManager.CurrentAccount.NeedPasswordRefresh))
            //{
               
                globale.Controls.Add(ZoomSearchControls.StartControl());

                globale.Controls.Add(NotifyBox);
                globale.Controls.Add(ctr);

                //if (PageContent == null)
                //{
                    //var handler = this.PageData.ContentsHandler;
                    //if (handler != null)
                    //{
                    //    if (handler.IsPreview)
                    //        globale.Controls.Add(handler.PreviewControl);
                    //    else
                    //    {
                    //        if (handler.CurrentDocument == null || handler.CurrentDocument.Grant)
                    //            globale.Controls.Add(handler.Control);
                    //        else
                    //            throw new NetCms.Exceptions.NotGrantedException();
                    //    }
                    //}
                    //    else
                    //        throw new NetCms.Exceptions.PageNotFound404Exception();
        //}
        //        else
        //            globale.Controls.Add(PageContent);
                globale.Controls.Add(ZoomSearchControls.StopControl());
            //}
            //else
            //{
            //    globale.Controls.Add(new FrontendUserModPasswordControl());
            //}

            //where.Controls.Add(this.Where);
            //footer.Controls.Add(new Footer(PageData).GetControl());

            ////corpo.Controls.Add(footer);
            //globale.Controls.Add(footer);

            //colCX_Content.Controls.Add(ZoomSearchControls.StartControl());
            //if (PageData.Folder != null && !string.IsNullOrEmpty(PageData.Folder.Path))
            //{

            //    string[] bodyID = PageData.Folder.Path.Split('/');
            //    string css_path = "";

            //    if (PageData.Body != null && bodyID.Length > 0)
            //    {
            //        PageData.Body.ID = bodyID[1];
            //        css_path = string.Join(" ", bodyID);

            //        if (!string.IsNullOrEmpty(css_path))
            //            PageData.Body.Attributes["class"] = css_path;
            //    }
            //}
            return globale;



        }

        private HtmlGenericControl BuildFullBody(Control ctr)
        {
            this.HeaderInitializer.Init();

            HtmlGenericControls.Div globale = new HtmlGenericControls.Div("globale");
            HtmlGenericControls.Div intestazione = new HtmlGenericControls.Div("Intestazione");
            HtmlGenericControls.Div corpo = new HtmlGenericControls.Div("corpo");
            HtmlGenericControls.Div where = new HtmlGenericControls.Div("Where");

            HtmlGenericControls.Div colCX = new HtmlGenericControls.Div("ColCX");
            HtmlGenericControls.Div colCX_Gfx1 = new HtmlGenericControls.Div();
            colCX_Gfx1.Class = "ColCX_Gfx1";
            HtmlGenericControls.Div colCX_Content = new HtmlGenericControls.Div("ColCX_Content");
            colCX_Gfx1.Class = "ColCX_Gfx2";

            HtmlGenericControls.Div colDX = new HtmlGenericControls.Div("ColDX");
            HtmlGenericControls.Div colSX = new HtmlGenericControls.Div("ColSX");
            HtmlGenericControls.Div footer = new HtmlGenericControls.Div("Footer");

            globale.Controls.Add(intestazione);
            intestazione.Controls.Add(this.Intestazione);

            globale.Controls.Add(corpo);

            if (StaticPagesCollector.CurrentStaticPage.Attribute.AddBreadcrumbs)
                corpo.Controls.Add(where);

            corpo.Controls.Add(colCX);
            colCX.Controls.Add(colCX_Gfx1);
            colCX_Gfx1.Controls.Add(colCX_Content);

            if (this.PageData.Homepage.ShowColDX)
            {
                corpo.Controls.Add(colDX);
                colDX.Controls.Add(this.ColDX);
            }

            if (this.PageData.Homepage.ShowColSX)
            {
                corpo.Controls.Add(colSX);
                colSX.Controls.Add(this.ColSX);
            }

            colCX_Content.Controls.Add(ZoomSearchControls.StartControl());
            
            colCX_Content.Controls.Add(NotifyBox);

            colCX_Content.Controls.Add(ctr);
            colCX_Content.Controls.Add(ZoomSearchControls.StopControl());

            where.Controls.Add(this.Where);
            footer.Controls.Add(new Footer(PageData).GetControl());
            globale.Controls.Add(footer);
            //corpo.Controls.Add(footer);
            //colCX_Content.Controls.Add(ZoomSearchControls.StartControl);

            return globale;
        }
    }
}