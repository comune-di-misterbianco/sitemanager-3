﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using LabelsManager;
using LanguageManager.BusinessLogic;
using NetCms.Diagnostics;
using NetCms.Themes.Model;

namespace NetFrontend
{
    // <summary>
    /// Responsive Body Handler for Frontend Error Page
    /// </summary>
    public class ErrorPageBodyHandler : BodyHandler
    {
        public ErrorPageBodyHandler(PageData pagedata, HtmlHead pageHeader) : base(pagedata, pageHeader, false)
        {
            // aggiungere l'init dell'header semplificato
            this.PageData.Network.ThemeFolder = (this.PageData.CurrentConfig != null) ? this.PageData.CurrentConfig.CurrentTheme.TemplateFolder : "";
            this.HeaderInitializer.InitScripts();
            //Imposto a false per evitare di consumare risorse alla ricerca di una app verticale al fine di caricare i css, in quanto siamo nella error page
            this.HeaderInitializer.InitCss(false);
            //this.HeaderInitializer.InitMeta();
        }

        #region Labels
        private Labels Labels
        {
            get
            {
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(CommonLabels), LanguageBusinessLogic.CurrentLanguage.CultureCode_Lang) as Labels;
            }
        }
        #endregion

        #region Control2Html
        public string ControltoHtml(WebControl temp)
        {
            if (temp == null)
                return "";
            else
            {
                TextWriter tw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(tw);
                temp.RenderControl(hw);
                return tw.ToString();
            }
        }
        public string ControltoHtml(Control temp)
        {
            if (temp == null)
                return "";
            else
            {
                TextWriter tw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(tw);
                temp.RenderControl(hw);
                return tw.ToString();
            }
        }
        #endregion

        #region Header
        public HtmlGenericControl Header
        {
            get
            {
                return _Header ?? (_Header = BuildHeader());
            }
        }
        private HtmlGenericControl _Header;

        /// <summary>
        /// Header from Parts.xml
        /// </summary>
        protected string HeaderHtml
        {
            get
            {
                if (_HeaderHtml == null)
                {
                    XmlNodeList oNodeList = XmlData.SelectNodes("/Portal/intestazione");
                    if (oNodeList != null && oNodeList.Count > 0)
                    {
                        PartsFilters filter = new PartsFilters(PageData);
                        _HeaderHtml = filter.FilterTags(oNodeList[0].InnerXml);
                    }
                }
                return _HeaderHtml;
            }
        }
        private string _HeaderHtml;


        /// <summary>
        /// Header builder
        /// </summary>
        /// <returns>Header Builder Method</returns>
        private HtmlGenericControl BuildHeader()
        {
            HtmlGenericControl tempControl = new HtmlGenericControl("div");

            tempControl.InnerHtml = HeaderHtml.Replace("%%TopMenuElement%%", "")
                                              .Replace("%%MobileMenu%%", "")
                                              .Replace("%%LoginControl%%", "")
                                              .Replace("%%CoverIMG%%", "");
            return tempControl;
        }

        #endregion

        #region ColCx
        public Control ColCx
        {
            get
            {
                if (_ColCx == null)
                {
                    HandlingError();
                    string src = BuildContent();
                    _ColCx = new LiteralControl(src);
                }

                return _ColCx;
            }
        }
        private Control _ColCx;


        public int StatusCode { get; set; }
        public string Status { get; set; }

        private string Errortitle = string.Empty;
        private string Errormessage = string.Empty;
        private string BackLinktitle = string.Empty;
        private string Backlinklabel = string.Empty;

        /// <summary>
        /// Handling Status and Message error
        /// </summary>
        private void HandlingError()
        {
            StatusCode = 500;

            NetUtility.RequestVariable errorCode = new NetUtility.RequestVariable("e", NetUtility.RequestVariable.RequestType.QueryString);

            if (errorCode != null && errorCode.IsValidInteger)
                StatusCode = errorCode.IntValue;

            BackLinktitle = this.Labels[CommonLabels.CommonLabelsList.BackLinktitle];
            Backlinklabel = this.Labels[CommonLabels.CommonLabelsList.Backlinklabel];

            switch (StatusCode)
            {

                case 400:
                    {
                        Status = this.Labels[CommonLabels.CommonLabelsList.Status400];
                        Errortitle = this.Labels[CommonLabels.CommonLabelsList.Errortitle400];
                        Errormessage = this.Labels[CommonLabels.CommonLabelsList.Errormessage400];
                    }
                    break;
                case 401:
                    {
                        Status = this.Labels[CommonLabels.CommonLabelsList.Status401];
                        Errortitle = this.Labels[CommonLabels.CommonLabelsList.Errortitle401];
                        Errormessage = this.Labels[CommonLabels.CommonLabelsList.Errormessage401];
                    }
                    break;
                case 403:
                    {
                        Status = this.Labels[CommonLabels.CommonLabelsList.Status403];
                        Errortitle = this.Labels[CommonLabels.CommonLabelsList.Errortitle403];
                        Errormessage = this.Labels[CommonLabels.CommonLabelsList.Errormessage403];
                    }
                    break;
                case 404:
                    {
                        Status = this.Labels[CommonLabels.CommonLabelsList.Status404];
                        Errortitle = this.Labels[CommonLabels.CommonLabelsList.Errortitle404];
                        Errormessage = this.Labels[CommonLabels.CommonLabelsList.Errormessage404];
                    }
                    break;
                case 500:
                    {
                        Status = this.Labels[CommonLabels.CommonLabelsList.Status500];
                        Errortitle = this.Labels[CommonLabels.CommonLabelsList.Errortitle500];
                        Errormessage = this.Labels[CommonLabels.CommonLabelsList.Errormessage500];
                    }
                    break;
            //    case 503:
            //        {
            //            Status = "";
            //            Errortitle = "Portale in manutenzione";
            //            Errormessage = "Il portale è in manutenzione e sarà nuovamente online tra poco.";
            //        }
            //        break;
            }
        }


        /// <summary>
        /// Generate src from template
        /// </summary>
        /// <returns>Html src for body page</returns>
        private string BuildContent()
        {
            string src = "";
            //string templateName = string.Empty;
            //if (PageData.CurrentConfig != null)
            //    templateName = (PageData.CurrentConfig.Layout != CmsConfigs.Model.Config.LayoutType.Responsive) ? PageData.CurrentConfig.Layout.ToString() + "/": "";

            //string tpl_path = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + PageData.CurrentConfig.CurrentTheme.TemplateFolder + "commons/error-body.tpl");

            string tpl_path = CmsConfigs.Model.GlobalConfig.CurrentConfig.CurrentTheme.GetTplPathByLabel("ErrorBody",
                                                                                                             new List<TemplateElement.TplTypes>()
                                                                                                             {
                                                                                                                 TemplateElement.TplTypes.system
                                                                                                             });

            string homepagelink = "/" + PageData.AbsoluteWebRoot;

            var data = new { statuscode = StatusCode,
                             errortitle = Errortitle,
                             errormessage = Errormessage,
                             backlinktitle = BackLinktitle,
                             backlinklabel = Backlinklabel,
                             homepagelink = homepagelink
                            };

            src = NetCms.Themes.TemplateEngine.RenderTpl(data, tpl_path, null);

            return src;

            #region oldcode
            //var template = Scriban.Template.Parse(System.IO.File.ReadAllText(tpl_path), tpl_path);

            //try
            //{
            //    if (template.HasErrors)
            //    {
            //        if (NetCms.Configurations.Debug.DebugLoginEnabled)
            //            foreach (var error in template.Messages)
            //                src += error.Message + System.Environment.NewLine;
            //        else
            //            foreach (var error in template.Messages)
            //                Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, error.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            //    }
            //    else
            //    {
            //        src = template.Render(new { data = data });

            //    }
            //}
            //catch (Exception ex)
            //{
            //    Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Template parsing error", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "", ex);
            //}

            //return src; 
            #endregion
        }

        #endregion


        #region Footer

        // TODO: rimodulare la procedura di lettura e rendering del footer 

        private HtmlGenericControl _Footer;
        public HtmlGenericControl Footer
        {
            get
            {
                return _Footer ?? (_Footer = BuildFooter());
            }
        }

        private string _FooterHtml;
        protected string FooterHtml
        {
            get
            {
                if (_FooterHtml == null)
                {
                    XmlNodeList oNodeList = XmlData.SelectNodes("/Portal/footer");
                    if (oNodeList != null && oNodeList.Count > 0)
                    {
                        PartsFilters filter = new PartsFilters(PageData);
                        _FooterHtml = filter.FilterTags(oNodeList[0].InnerXml);
                    }
                }
                return _FooterHtml;
            }
        }

        private HtmlGenericControl BuildFooter()
        {
            HtmlGenericControl tempControl = new HtmlGenericControl("div");

            tempControl.InnerHtml = FooterHtml;
            return tempControl;
        }
        #endregion

    }
}
