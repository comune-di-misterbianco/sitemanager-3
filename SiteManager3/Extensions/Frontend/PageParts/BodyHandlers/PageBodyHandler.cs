using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using NetCms.Front;
using NetFrontend.FrontPiecies;
using LabelsManager;
using LanguageManager.BusinessLogic;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    public abstract class PageBodyHandler
    {
        public NotifyBox NotifyBox
        {
            get
            {
                if (_NotifyBox == null)
                    _NotifyBox = new NotifyBox();
                return _NotifyBox;
            }
        }
        private NotifyBox _NotifyBox;

        public bool AddNotifyBox
        {
            get;
            set;
        }

        protected PageData PageData;
       
        //protected CmsConfigs.Model.Config CurrentConfig
        //{
        //    get
        //    {
        //        return CmsConfigs.Model.GlobalConfig.GetConfigByNetworkID(this.PageData.Network.ID);
        //    }
        //}

        public bool TopMenuEnabled
        {
            get
            {
                //string value = NetCms.Configurations.XmlConfig.GetAttribute("/Portal/configs/Networks/" + this.PageData.Network.SystemName + "/frontend/menu", "topmenu");
                //bool active = !string.IsNullOrEmpty(value) && value != "off";
                //return active;

                bool active = false;

                if (PageData.CurrentConfig != null)
                    active = (PageData.CurrentConfig.TopMenuState == CmsConfigs.Model.Config.State.On);

                return (active);
            }
        }


        #region Propriet� correlate al blocco Login 

        public bool LoginHeadStatus
        {
            get
            {                
                string value = NetCms.Configurations.XmlConfig.GetAttribute("/Portal/configs/login", "headstatus");
                bool active = !string.IsNullOrEmpty(value) && value != "off";
                return active;
            }
        }

        public bool MostraRegistrazione
        {
            get
            {
                string value = NetCms.Configurations.XmlConfig.GetAttribute("/Portal/configs/login", "mostraRegistrazione");
                bool active = !string.IsNullOrEmpty(value) && value != "off";
                return active;
            }
        }

        public bool MostraRecuperoPassword
        {
            get
            {
                string value = NetCms.Configurations.XmlConfig.GetAttribute("/Portal/configs/login", "mostraRecuperoPassword");
                bool active = !string.IsNullOrEmpty(value) && value != "off";
                return active;
            }
        }

        public string UrlRegistrazione
        {
            
            get {                
                string value = NetCms.Configurations.XmlConfig.GetAttribute("/Portal/configs/login", "urlRegistrazione");
                return (value != null) ? value : "";
            }
            

        }


        public string Logout
        {
            get
            {
                string href = "?do=logout&amp;goto=" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;

                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageBusinessLogic.CurrentLanguage.ID != LanguageBusinessLogic.GetDefaultLanguage().ID)
                {
                    href = "?do=logout&amp;goto=" + "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.Length == 0 || NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.StartsWith("/") ? "" : "/") + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;
                }
                return BuildElementHtml("Logout", href, "LogoutLink");
            }
        }
        public string Profilo
        {
            get
            {
                string href = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/profilo.aspx";
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageBusinessLogic.CurrentLanguage.ID != LanguageBusinessLogic.GetDefaultLanguage().ID)
                {
                    href = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.Length == 0 || NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.StartsWith("/") ? "" : "/") + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/profilo.aspx";
                }
                return BuildElementHtml(Labels[CommonLabels.CommonLabelsList.ProfiloLabelLink] /*"Profilo Personale"*/, href, "ProfiloLink");
            }
        }
        public string EditProfilo
        {
            get
            {
                string href = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/modifica_profilo.aspx";
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageBusinessLogic.CurrentLanguage.ID != LanguageBusinessLogic.GetDefaultLanguage().ID)
                {
                    href = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.Length == 0 || NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.StartsWith("/") ? "" : "/") + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/modifica_profilo.aspx";
                }
                return BuildElementHtml(Labels[CommonLabels.CommonLabelsList.EditProfiloLabelLink] /*"Modifica Profilo Personale"*/, href, "ModificaProfiloLink");
            }
        }
        public string ModificaPassword
        {
            get
            {
                string href = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/changepw.aspx";
                
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageBusinessLogic.CurrentLanguage.ID != LanguageBusinessLogic.GetDefaultLanguage().ID)
                {
                    href = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.Length == 0 || NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.StartsWith("/") ? "" : "/") + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/user/changepw.aspx";
                }
                return BuildElementHtml(Labels[CommonLabels.CommonLabelsList.EditPasswordLabelLink] /*"Modifica Password"*/, href, "ModificaPasswordLink");
            }
        }

        public string BuildElementHtml(string label, string href, string cssClass)
        {
            string template = @"<li{2}><a href=""{1}"">{0}</a></li>";
            if (!string.IsNullOrEmpty(cssClass))
                cssClass = " class=\"" + cssClass + "\"";
            return string.Format(template, label, href, cssClass);
        }
        
        #endregion

        public FrontLayouts? ForceLayout { get; protected set; }

        public PageBodyHandler(PageData pagedata, HtmlHead pageHeader, bool initBreadcrumbs = true)
        {
            CurrentPageHandler = this;
            PageHeader = pageHeader;
            PageData = pagedata;

            PageData.HtmlHead = pageHeader;           

            //Where = new HtmlGenericControl("div");
            if (initBreadcrumbs)
            {
                Where = new HtmlGenericControl("ol");
                this.Where.Attributes.Add("class", "breadcrumb");
                PageData.Where = new Where(Where, PageData);
                if (PageData.Where.Blocks.Count <= 1)
                {
                    this.Where.Attributes.Add("style", "display:none;");
                }
            }
        }

        public HtmlHead PageHeader { get; private set; } 

        public HeaderInitializer HeaderInitializer
        {
            get
            {
                return _HeaderInitializer ?? (_HeaderInitializer = new HeaderInitializer(PageHeader, PageData));
            }
        }
        private HeaderInitializer _HeaderInitializer;

        public HtmlGenericControl Intestazione
        {
            get
            {
                return _Intestazione ?? (_Intestazione = BuildIntestazione());
            }
        }
        private HtmlGenericControl _Intestazione;

        public HtmlGenericControl Where { get; protected set; }

        public HtmlGenericControl ColSX
        {
            get
            {
                HtmlGenericControl div = PageData.Homepage.ColSX;
                if (div == null)
                {
                    div = new HtmlGenericControl("div");
                    div.Attributes["class"] = "nascosto";
                }

                return div;
            }
        }
        public HtmlGenericControl ColDX
        {
            get
            {
                HtmlGenericControl div = PageData.Homepage.ColDX;
                if (div == null)
                {
                    div = new HtmlGenericControl("div");
                    div.Attributes["class"] = "nascosto";
                }
                return div;
            }
        }

        public WebControl PageContent
        {
            get { return _PageContent; }
            set { _PageContent = value; }
        }
        private WebControl _PageContent;

        private LanguageManager.Model.Language _Language;
        private XmlNode xmlData;
        public XmlNode XmlData
        {
            get
            {
                #region MULTI LANG
                if(_Language == null)
                {
                    _Language = LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage;
                }

                string partsName = "";
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang != 1)
                {
                    partsName = "\\";
                    partsName += LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.CultureCode_Lang;
                    partsName += "_parts.xml";

                }
                else
                    partsName = "\\parts.xml";
                #endregion

                if (
                    xmlData == null || 
                    (xmlData != null && _Language != null && _Language.Default_Lang != LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang)
                )
                {
                    _Language = LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage;

                    XmlDocument oXmlDoc = new XmlDocument();
                    try
                    {
                        string xmlFilePath = string.Empty;

                        if (NetCms.Configurations.ThemeEngine.Status == NetCms.Configurations.ThemeEngine.ThemeEngineStatus.on)                        
                            xmlFilePath = NetCms.Configurations.Paths.AbsoluteConfigRoot + "\\" + this.PageData.Network.SystemName.ToLower() + "\\" + PageData.CurrentConfig.Theme +  partsName;
                        else
                            xmlFilePath = NetCms.Configurations.Paths.AbsoluteConfigRoot + "\\" + this.PageData.Network.SystemName.ToLower() + partsName;

                        oXmlDoc.Load(xmlFilePath);
                        xmlData = oXmlDoc.DocumentElement;
                    }
                    catch (Exception ex)
                    {
                        if (NetCms.Configurations.Debug.GenericEnabled)
                        {
                            try
                            {
                                string xmlFilePath = NetCms.Configurations.Paths.AbsoluteConfigRoot + "\\system" + partsName;
                                oXmlDoc.Load(xmlFilePath);
                                xmlData = oXmlDoc.DocumentElement;
                            }
                            catch{}
                        }
                        else
                        {
                            throw new NetCms.Exceptions.ServerError500Exception("Impossibile trovare il file di configurazione '" + partsName +"' del portale '"+this.PageData.Network.SystemName.ToLower()+"'");
                        }
                    }
                }
                return xmlData;
            }
        }

        private string _IntestazioneHtml;
        protected string IntestazioneHtml
        {
            get
            {
                if (_IntestazioneHtml == null)
                {
                    XmlNodeList oNodeList = XmlData.SelectNodes("/Portal/intestazione");
                    if (oNodeList != null && oNodeList.Count > 0)
                    {
                        PartsFilters filter = new PartsFilters(PageData);
                        _IntestazioneHtml = filter.FilterTags(oNodeList[0].InnerXml);
                    }
                }
                return _IntestazioneHtml;
            }
        }

        public Control Control
        {
            get
            {
                if (_Control == null)
                {
                    bool logged = NetCms.Users.AccountManager.Logged;
                    bool showPasswordRefresh = logged && NetCms.Users.AccountManager.CurrentAccount.NeedPasswordRefresh;

                    if (showPasswordRefresh)
                        _Control = BuildRefreshPasswordControl();
                    else
                    {
                        try
                        {
                            _Control = BuildControl();
                        }
                        catch (NetCms.Exceptions.NotGrantedException)
                        {
                            if (!logged)
                                _Control = BuildLoginControl();
                            else throw;
                        }
                    }
                }

                return _Control;
            }
        }
        private Control _Control;

        public abstract Control BuildControl();

        private Labels Labels
        {
            get
            {
                //return new NetFrontend.CommonLabels(NetCms.Configurations.Paths.AbsoluteRoot);
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(CommonLabels), LanguageBusinessLogic.CurrentLanguage.CultureCode_Lang) as Labels;
            }
        }

        private HtmlGenericControl BuildIntestazione()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "innerIntestazione";


            HtmlGenericControl title = new HtmlGenericControl("div");
            title.Attributes["class"] = "partsIntestazione";
            title.InnerHtml = IntestazioneHtml;
            div.Controls.Add(title);
         
            string currentPagePath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;

            string[] loginUrls = new string[] { "/user/accesso.aspx", "/en/user/signin.aspx" };

            if (!NetCms.Configurations.Generics.LoginManager)
            {
                if (LoginHeadStatus && /*!currentPagePath.Contains("user/accesso.aspx")*/ !loginUrls.Any(currentPagePath.Contains) ) 
                {
                    WebControl loginContainer = new WebControl(HtmlTextWriterTag.Div);
                    loginContainer.ID = "loginContainer";

                    // percorso assoluto della root con l'eventuale network
                    var currentNetwork = NetCms.Networks.NetworksManager.CurrentActiveNetwork;
                    string awp = (currentNetwork != null) ? currentNetwork.Paths.AbsoluteFrontRoot : NetCms.Configurations.Paths.AbsoluteRoot;
                    string strLinkReg = awp + "/" + UrlRegistrazione;

                    if (!NetCms.Users.AccountManager.Logged)
                    {
                        if (!string.IsNullOrEmpty(UrlRegistrazione))
                        {
                            WebControl regControl = new WebControl(HtmlTextWriterTag.Div);
                            regControl.ID = "Reg";

                            WebControl linkReg = new WebControl(HtmlTextWriterTag.A);

                            linkReg.Attributes["href"] = strLinkReg;

                            linkReg.Controls.Add(new LiteralControl("<span>" + this.Labels[CommonLabels.CommonLabelsList.RegistatiLabelLink] + "</span>"));

                            regControl.Controls.Add(linkReg);
                            loginContainer.Controls.Add(regControl);
                        }

                        WebControl loginButton = new WebControl(HtmlTextWriterTag.A);
                        loginButton.ID = "loginButton";
                        loginButton.Attributes["href"] = "#";
                        loginButton.Controls.Add(new LiteralControl("<span>" + this.Labels[CommonLabels.CommonLabelsList.LoginLabelLink] + "</span>"));
                        loginContainer.Controls.Add(loginButton);

                        WebControl clr = new WebControl(HtmlTextWriterTag.Div);
                        clr.CssClass = "clr";
                        loginContainer.Controls.Add(clr);

                        WebControl loginBox = new WebControl(HtmlTextWriterTag.Div);
                        loginBox.ID = "loginBox";

                        NetCms.Users.LoginControl loginCtrl = new NetCms.Users.LoginControl
                        (
                            currentNetwork.Paths.AbsoluteFrontRoot,
                            MostraRegistrazione,
                            MostraRecuperoPassword,
                            strLinkReg
                        );

                        loginCtrl.IsHeadLogin = true;
                        loginBox.Controls.Add(loginCtrl);

                        loginContainer.Controls.Add(loginBox);
                    }
                    else
                    {
                        NetCms.Users.User datiuser = NetCms.Users.AccountManager.CurrentAccount;

                        string nomeCompleto = (!string.IsNullOrEmpty(datiuser.NomeCompleto)) ? datiuser.NomeCompleto : datiuser.UserName;

                        //div.Attributes["class"] += " Logged";

                        WebControl list = new WebControl(HtmlTextWriterTag.Ul);

                        WebControl liUserName = new WebControl(HtmlTextWriterTag.Li);
                        liUserName.ID = "username";
                        liUserName.Controls.Add(new LiteralControl("<span>" + nomeCompleto + "</span>"));
                        list.Controls.Add(liUserName);

                        WebControl liUserNav = new WebControl(HtmlTextWriterTag.Li);
                        liUserNav.ID = "userCtrl";
                        liUserNav.Controls.Add(new LiteralControl("<span>" + this.Labels[CommonLabels.CommonLabelsList.ImpostazioniAccount] + "</span>"));

                        WebControl userNav = new WebControl(HtmlTextWriterTag.Ul);
                        userNav.ID = "userNav";
                        userNav.Controls.Add(new LiteralControl(this.Profilo));
                        userNav.Controls.Add(new LiteralControl(this.EditProfilo));
                        userNav.Controls.Add(new LiteralControl(this.ModificaPassword));

                        if (NetCms.Configurations.Generics.NotifyEnabled)
                            userNav.Controls.Add(new LiteralControl(this.ElencoNotifiche));

                        if (ChiarimentiIsInstalled) // Se l'app chiarimenti � installata allora aggiungo il link
                            userNav.Controls.Add(new LiteralControl(this.ElencoChiarimenti));

                        userNav.Controls.Add(new LiteralControl(this.Logout));

                        liUserNav.Controls.Add(userNav);

                        list.Controls.Add(liUserNav);

                        loginContainer.Controls.Add(list);
                    }

                    div.Controls.Add(loginContainer);
                }
            }
            else
            {
                div.Controls.Add(new LoginManager.Controls.LoginHeaderControl(NetCms.Networks.NetworksManager.CurrentActiveNetwork.ID, NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot, NetCms.Users.AccountManager.Logged, NetCms.Users.AccountManager.CurrentAccount));   
            }

            if (TopMenuEnabled)
            {
                HtmlGenericControl topmenu = new HtmlGenericControl("div");
                topmenu.ID = "topmenu";

                HtmlGenericControl topmenu_content = new HtmlGenericControl("div");
                topmenu_content.ID = "topmenu_content";
                topmenu.Controls.Add(topmenu_content);

                ModelMenu topmenu_obj = new NetworkMenu();
                topmenu_obj.FirstLevelAllwaysVisible = true;
                topmenu_content.Controls.Add(topmenu_obj.GetMenu("head", 1, 1));

                div.Controls.Add(topmenu);
            }           

            return div;
        }
        
        public virtual Control BuildLoginControl()
        {
            this.ForceLayout = new FrontLayouts?(FrontLayouts.None);
            var handler = new NetCms.Users.NeedToLoginControl();    
            //this.PageData.ContentsHandler = handler;
            this.PageData.InitHomepage();
            this.HeaderInitializer.Init();          

            HtmlGenericControls.Div globale = new HtmlGenericControls.Div("globale");
            HtmlGenericControls.Div intestazione = new HtmlGenericControls.Div("Intestazione");
            HtmlGenericControls.Div corpo = new HtmlGenericControls.Div("corpo");
            HtmlGenericControls.Div where = new HtmlGenericControls.Div("Where");
            HtmlGenericControls.Div colCX = new HtmlGenericControls.Div("ColCX");
            HtmlGenericControls.Div colCX_Gfx1 = new HtmlGenericControls.Div() { Class = "ColCX_Gfx2" };
            HtmlGenericControls.Div colCX_Content = new HtmlGenericControls.Div("ColCX_Content");
            HtmlGenericControls.Div colDX = new HtmlGenericControls.Div("ColDX");
            HtmlGenericControls.Div colSX = new HtmlGenericControls.Div("ColSX");
            HtmlGenericControls.Div footer = new HtmlGenericControls.Div("Footer");

            globale.Controls.Add(intestazione);
            intestazione.Controls.Add(this.Intestazione);

            globale.Controls.Add(corpo);

            corpo.Controls.Add(where);

            corpo.Controls.Add(colCX);
            colCX.Controls.Add(colCX_Gfx1);
            colCX_Gfx1.Controls.Add(colCX_Content);
            colCX_Content.Controls.Add(handler);

            where.Controls.Add(this.Where);
            footer.Controls.Add(new Footer(PageData).GetControl());
            globale.Controls.Add(footer);

            return globale;
        }
        public virtual Control BuildRefreshPasswordControl()
        {
            this.ForceLayout = new FrontLayouts?(FrontLayouts.None);
            var handler = new NetCms.Users.NeedToLoginControl();
            this.PageData.InitHomepage();
            this.HeaderInitializer.Init();

            HtmlGenericControls.Div globale = new HtmlGenericControls.Div("globale");
            HtmlGenericControls.Div intestazione = new HtmlGenericControls.Div("Intestazione");
            HtmlGenericControls.Div corpo = new HtmlGenericControls.Div("corpo");
            HtmlGenericControls.Div where = new HtmlGenericControls.Div("Where");
            HtmlGenericControls.Div colCX = new HtmlGenericControls.Div("ColCX");
            HtmlGenericControls.Div colCX_Gfx1 = new HtmlGenericControls.Div() { Class = "ColCX_Gfx2" };
            HtmlGenericControls.Div colCX_Content = new HtmlGenericControls.Div("ColCX_Content");
            HtmlGenericControls.Div colDX = new HtmlGenericControls.Div("ColDX");
            HtmlGenericControls.Div colSX = new HtmlGenericControls.Div("ColSX");
            HtmlGenericControls.Div footer = new HtmlGenericControls.Div("Footer");

            globale.Controls.Add(intestazione);
            intestazione.Controls.Add(this.Intestazione);

            globale.Controls.Add(corpo);

            corpo.Controls.Add(where);

            corpo.Controls.Add(colCX);

            colCX.Controls.Add(colCX_Gfx1);
            
            colCX_Gfx1.Controls.Add(colCX_Content);                        
           
            colCX_Content.Controls.Add(new NetCms.Structure.Grants.UsersGrants.FrontendUserModPasswordControl());           

            where.Controls.Add(this.Where);

            footer.Controls.Add(new Footer(PageData).GetControl());            

            globale.Controls.Add(footer);

            return globale;
        }

        private static G2Core.Caching.PersistentCacheObject<PageBodyHandler> CurrentPageHandlerCache
        {
            get
            {
                if (_CurrentPageHandlerCache == null)
                {
                    _CurrentPageHandlerCache = new G2Core.Caching.PersistentCacheObject<PageBodyHandler>("SM2_Front_CurrentPageHandler", G2Core.Caching.StoreTypes.Page)
                        {
                            AutoInitObjectInstance = false
                        };
                }
                return _CurrentPageHandlerCache;
            }
        }
        private static G2Core.Caching.PersistentCacheObject<PageBodyHandler> _CurrentPageHandlerCache;

        public static PageBodyHandler CurrentPageHandler
        {
            get
            {
                lock (CurrentPageHandlerCache)
                {
                    return CurrentPageHandlerCache.Instance;
                }

            }
            private set
            {
                lock (CurrentPageHandlerCache)
                {
                    CurrentPageHandlerCache.SetObjectInstance(value);
                }
            }
        }

        public bool ChiarimentiIsInstalled
        {
            get
            {
                return NetCms.Vertical.VerticalApplicationsPool.ActiveAssemblies.Contains(240);
            }
        }

        public string ElencoNotifiche
        {
            get
            {
                string href = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/notifiche/elenco.aspx";
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageBusinessLogic.CurrentLanguage.ID != LanguageBusinessLogic.GetDefaultLanguage().ID)
                {
                    href = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.Length == 0 || NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.StartsWith("/") ? "" : "/") + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/notifiche/elenco.aspx";
                }
                return BuildElementHtml(Labels[CommonLabels.CommonLabelsList.NotificheLabelLink] /*"Elenco notifiche"*/, href, "ElencoNotificheLink");
            }
        }

        public string ElencoChiarimenti
        {
            get
            {
                string href = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/chiarimenti/default.aspx";
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageBusinessLogic.CurrentLanguage.ID != LanguageBusinessLogic.GetDefaultLanguage().ID)
                {
                    href = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.Length == 0 || NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.StartsWith("/") ? "" : "/") + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "/chiarimenti/default.aspx";
                }
                return BuildElementHtml(Labels[CommonLabels.CommonLabelsList.ChiarimentiLabelLink] /*"Elenco chiarimenti"*/, href, "ElencoChiarimentiLink");
            }
        }
    }
}
