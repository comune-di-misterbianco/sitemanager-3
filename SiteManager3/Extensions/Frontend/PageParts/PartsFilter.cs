using System;
using NetCms.Front;
using LanguageManager;
using System.Xml;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using LanguageManager.Model;
using System.Collections.Generic;
using System.Web.UI;
using NetCms.Diagnostics;
using LanguageManager.BusinessLogic;
using System.Web;

/// <summary>
/// Parts filters, replacement for key,value
/// </summary>
namespace NetFrontend
{
    public class PartsFilters
    {
        private PageData PageData;

        public PartsFilters(PageData pagedata)
        {
            PageData = pagedata;
            Network = pagedata.Network;
        }

        public PartsFilters(FrontendNetwork currentNetwork)
        {
            Network = currentNetwork;
        }

        private FrontendNetwork Network
        {
            get;
            set;
        }

        private XmlNode xmlCrossWebData;
        public XmlNode XmlCrossWebData
        {
            get
            {
                if (xmlCrossWebData == null)
                {
                    XmlDocument oXmlDoc = new XmlDocument();
                    try
                    {
                        string xmlFilePath = System.Web.Hosting.HostingEnvironment.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + "/configs/" + Network.SystemName.ToLower() + "/crossweblist.xml");
                        oXmlDoc.Load(xmlFilePath);
                    }
                    catch (Exception ex)
                    {
                    }
                    xmlCrossWebData = oXmlDoc.DocumentElement;
                }
                return xmlCrossWebData;
            }
        }

        private string _Enti = "";
        private string Enti
        {
            get
            {
                if (_Enti == "" && XmlCrossWebData != null)
                {
                    if (PageData.Request.Form["Vai"] != null &&
                        PageData.Request.Form["Vai"] == "Vai" &&
                        PageData.Request.Form["CrossWebList"] != null &&
                        PageData.Request.Form["CrossWebList"].Length > 0)
                    {
                        NetCms.Diagnostics.Diagnostics.Redirect(PageData.Request.Form["CrossWebList"]);
                    }
                    _Enti = "";
                    if (PageData.Application["CrossWebList"] == null)
                    {
                        XmlNodeList oNodeList = XmlCrossWebData.SelectNodes("/CrossWeb/group/site");
                        int i = 0;
                        string group = "";
                        while (oNodeList != null && oNodeList.Count > i)
                        {
                            if (oNodeList[i].ParentNode.Attributes["name"].Value != group)
                            {
                                if (group.Length > 0)
                                    _Enti += "</optgroup>";
                                group = oNodeList[i].ParentNode.Attributes["name"].Value;

                                _Enti += "<optgroup label=\"" + group + "\">";
                            }
                            _Enti += "<option value=\"" + oNodeList[i].Attributes["href"].Value + "\"";

                            if (PageData.Network.ID.ToString() == oNodeList[i].Attributes["siteid"].Value)
                                _Enti += " selected=\"selected\" ";
                            _Enti += " >" + oNodeList[i].InnerXml + "</option>";
                            i++;
                        }
                        if (group.Length > 0)
                            _Enti += "</optgroup>";

                        #region Opzioni Sfuse
                        oNodeList = XmlCrossWebData.SelectNodes("/CrossWeb/site");
                        i = 0;
                        while (oNodeList != null && oNodeList.Count > i)
                        {

                            _Enti += "<option value=\"" + oNodeList[i].Attributes["href"].Value + "\"";

                            if (PageData.Network.ID.ToString() == oNodeList[i].Attributes["siteid"].Value)
                                _Enti += " selected=\"selected\" ";
                            _Enti += " >" + oNodeList[i].InnerXml + "</option>";
                            i++;
                        }
                        if (_Enti.Length > 0)
                        {
                            _Enti = "<label for=\"CrossWebList\" class=\"nascosto\">Cambia Sito</label><select name=\"CrossWebList\" id=\"CrossWebList\" >" + _Enti + "</select>";
                            _Enti += "<input name=\"Vai\" type=\"submit\" value=\"Vai\" class=\"vai\" />";
                        }
                        PageData.Application["CrossWebList"] = _Enti;
                        #endregion
                    }
                    else _Enti = (string)PageData.Application["CrossWebList"];
                }

                return _Enti;
            }
        }

        private string _EntiAsList = "";
        private string EntiAsList
        {
            get
            {
                if (_EntiAsList == "" && XmlCrossWebData != null)
                {
                    _Enti = "";
                    if (PageData.Application["CrossWebListAsHtmlList"] == null)
                    {
                        XmlNodeList oNodeList = XmlCrossWebData.SelectNodes("/CrossWeb/group/site");
                        int i = 0;
                        while (oNodeList != null && oNodeList.Count > i)
                        {
                            _Enti += "<li><a href=\"" + oNodeList[i].Attributes["href"].Value + "\" >" + oNodeList[i].InnerXml + "</a></li>";
                            i++;
                        }

                        #region Opzioni Sfuse

                        oNodeList = XmlCrossWebData.SelectNodes("/CrossWeb/site");
                        i = 0;
                        while (oNodeList != null && oNodeList.Count > i)
                        {
                            bool selected = oNodeList[i].Attributes["siteid"].Value == PageData.Network.ID.ToString();
                            _Enti += "<li class=\"" + (selected ? " CrossWebLabelSelected" : "") + "\"><a href=\"" + oNodeList[i].Attributes["href"].Value + "\">" + oNodeList[i].InnerXml + "</a></li>";
                            i++;
                        }
                        if (_Enti.Length > 0)
                        {

                            _Enti = "<span class=\"CrossWebLabel\">Cambia Sito</span><ul class=\"CrossWebHtmlList\">" + _Enti + "</ul>";
                        }
                        PageData.Application["CrossWebListAsHtmlList"] = _Enti;

                        #endregion
                    }
                    else _Enti = (string)PageData.Application["CrossWebListAsHtmlList"];
                }

                return _Enti;
            }
        }
        public string FilterTags(string xml)
        {
            string filtered = xml;
            string absoluteUri = System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority;

            string date = DateTime.Today.Date.ToString();
            date = date.Split(' ')[0];
            string time = NetUtility.TreeUtility.FormatNumber(DateTime.Now.Hour.ToString(), 2) + ":" + NetUtility.TreeUtility.FormatNumber(DateTime.Now.Minute.ToString(), 2);
            filtered = filtered.Replace("<!-- ##Date/Time## -->", "Oggi " + date + " ore " + time);

            filtered = filtered.Replace("<!-- ##DDLCrossWeb## -->", (PageData != null)? Enti:"");
            filtered = filtered.Replace("<!-- ##ListCrossWeb## -->", (PageData != null) ? EntiAsList : "");

            filtered = filtered.Replace("%%PortalName%%", Network.Label);

            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                if (LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang > 0)
                    filtered = filtered.Replace("%%PathToTheGlobalRootMultiLang%%", (NetCms.Configurations.Paths.AbsoluteRoot.Length > 0 ? NetCms.Configurations.Paths.AbsoluteRoot + "/" : ""));
                else
                {
                    string path = (NetCms.Configurations.Paths.AbsoluteRoot.Length > 0 ? NetCms.Configurations.Paths.AbsoluteRoot + "/" : "");
                    filtered = filtered.Replace("%%PathToTheGlobalRootMultiLang%%", "/" + LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (path.StartsWith("/") ? "" : "/") + path + (path.EndsWith("/") ? "default.aspx" : ""));
                }
            }
            else
                filtered = filtered.Replace("%%PathToTheGlobalRootMultiLang%%", (NetCms.Configurations.Paths.AbsoluteRoot.Length > 0 ? NetCms.Configurations.Paths.AbsoluteRoot + "/" : ""));

            filtered = filtered.Replace("%%PathToTheGlobalRoot%%", (NetCms.Configurations.Paths.AbsoluteRoot.Length > 0 ? NetCms.Configurations.Paths.AbsoluteRoot + "/" : ""));

            string pathToTheNetworkRoot = (Network.Paths.AbsoluteFrontRoot.Length > 0 ? Network.Paths.AbsoluteFrontRoot + "/" : "/");
            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                if (LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang > 0)
                {
                    filtered = filtered.Replace("%%PathToTheNetworkRootMultiLang%%", (Network.Paths.AbsoluteFrontRoot.Length > 0 ? Network.Paths.AbsoluteFrontRoot + "/" : "/"));
                }
                else
                {
                    string path = (Network.Paths.AbsoluteFrontRoot.Length > 0 ? Network.Paths.AbsoluteFrontRoot + "/" : "/");
                    filtered = filtered.Replace("%%PathToTheNetworkRootMultiLang%%", "/" + LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + (path.StartsWith("/") ? "" : "/") + path + (path.EndsWith("/") ? "default.aspx" : ""));
                    pathToTheNetworkRoot += LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + "/";
                }
            }
            else
            {
                filtered = filtered.Replace("%%PathToTheNetworkRootMultiLang%%", (Network.Paths.AbsoluteFrontRoot.Length > 0 ? Network.Paths.AbsoluteFrontRoot + "/" : "/"));
            }

            filtered = filtered.Replace("%%PathToTheNetworkRoot%%", pathToTheNetworkRoot) ;
            filtered = filtered.Replace("%%PortalURL%%", Network.Website);
            filtered = filtered.Replace("%%Logo%%", Network.Paths.AbsoluteFrontRoot + "/logo.png");
            filtered = filtered.Replace("%%CurrentFolderPath%%", "");
            filtered = filtered.Replace("%%CurrentFullPath%%", "");
            filtered = filtered.Replace("%%AbsoluteUri%%", absoluteUri);

            //filtered = filtered.Replace("<!-- ##RSSLink## -->", this.PageData.ShowRssButton ? "<a class=\"feedrssMain\" href=\"rss.xml\"><span>RSS</span></a>" : "");
            filtered = filtered.Replace("<!-- ##RSSLink## -->", "");
            filtered = filtered.Replace("<![CDATA[", "");
            filtered = filtered.Replace("]]>", "");

            //Aggiunto questo controllo per evitare che il language manager venisse processato ad ogni chiamata del parts filter. Deve avvenire solo dove realmente � stato posizionato.
            if (filtered.Contains("%%LanguageSwitcher%%"))
            {
                if (LanguageManager.Utility.Config.EnableMultiLanguage)
                {
                    Dictionary<string, string> translatedPageNameCollection = new Dictionary<string, string>();
                    IList<Language> languages = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages();
                    foreach (LanguageManager.Model.Language lang in languages)
                    {
                        //TODO: da attenzionare - tutto viene recuperato da PageData
                        string pageName = DocumentBusinessLogic.TranslatePageName(lang.ID, Network.ID, PageData.PageName, (PageData.Folder.TranslatedPathsCollection.Count > 0 ? PageData.Folder.TranslatedPathsCollection[LanguageManager.BusinessLogic.LanguageBusinessLogic.GetDefaultLanguage().LangTwoLetterCode] : PageData.FolderPath));
                        translatedPageNameCollection.Add(lang.LangTwoLetterCode, pageName);
                    }

                    string languageSwitcher = string.Empty;

                    //if (PageData.CurrentConfig.Layout != CmsConfigs.Model.Config.LayoutType.Responsive)
                    if (NetCms.Configurations.ThemeEngine.Status == NetCms.Configurations.ThemeEngine.ThemeEngineStatus.on)
                    {
                        Language defaultLanguage = LanguageBusinessLogic.GetLanguageFromUrl(HttpContext.Current.Request.Path.ToString());
                        var currentLang = new { label = defaultLanguage.Nome_Lang, twolettercode = defaultLanguage.LangTwoLetterCode, culturecode = defaultLanguage.CultureCode_Lang };

                        var keyAssociation = NetCms.Front.Static.StaticPagesCollector.ClassesKeysAssociations;
                        Dictionary<string, string[]> staticPageAssociation = new Dictionary<string, string[]>();
                        foreach(var kA in keyAssociation)
                        {
                            string[] addresses = kA.Value.Attribute.Addresses;
                            string module = kA.Value.Type.AssemblyQualifiedName;
                            if(!staticPageAssociation.ContainsKey(module))
                                staticPageAssociation.Add(module, addresses);
                        }

                        LanguageManager.LanguageSwitcher lan = new LanguageSwitcher
                            (
                            PageData.Folder.TranslatedPathsCollection, 
                            translatedPageNameCollection, 
                            true,
                            staticPageAssociation
                            );

                        List<Object> langs = lan.GetDataForTpl();

                        // check existance of tpl                        
                        string tpl_path = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + "/" + PageData.CurrentConfig.CurrentTheme.TemplateFolder + "commons/lang-head-selector.tpl");

                        var template = Scriban.Template.Parse(System.IO.File.ReadAllText(tpl_path), tpl_path);
                        try
                        {
                            if (!template.HasErrors)
                            {
                                var context = new Scriban.TemplateContext();
                                context.PushCulture(System.Globalization.CultureInfo.CurrentCulture);

                                var scriptObj = new Scriban.Runtime.ScriptObject();
                                scriptObj.Add("currentlang", currentLang);
                                scriptObj.Add("langs", langs);
                                context.PushGlobal(scriptObj);

                                languageSwitcher = template.Render(context);
                            }
                        }
                        catch (Exception ex)
                        {
                            Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Template parsing error", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "", ex);
                        }                            
                    }
                    else
                    {
                        LanguageManager.LanguageSwitcher lan = new LanguageSwitcher(PageData.Folder.TranslatedPathsCollection, translatedPageNameCollection);
                        languageSwitcher = lan.toHtml();
                    }

                    filtered = filtered.Replace("%%LanguageSwitcher%%", languageSwitcher);
                }
                else
                    filtered = filtered.Replace("%%LanguageSwitcher%%", "");
            }
            return filtered;
        }
    }
}