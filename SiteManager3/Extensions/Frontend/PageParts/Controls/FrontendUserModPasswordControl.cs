﻿using System;
using System.Data;
using System.Data.Common;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetForms;
using System.Web;
using NetCms.Connections;
using System.Web.UI;
using NetService.Utility.ValidatedFields;
using System.Web.Security;
using NetCms.Users;


namespace NetCms.Structure.Grants
{
    /// <summary>
    /// Partial class of UserGrants
    /// </summary>
    public partial class UsersGrants
    {
        /// <summary>
        /// Frontend User Modify Password Control
        /// </summary>
        public class FrontendUserModPasswordControl : WebControl
        {
            public User User
            {
                get
                {
                    return _User ?? (_User = NetCms.Users.AccountManager.CurrentAccount);
                }
            }
            private User _User;

            public string InformationText
            {
                get
                {
                    if (_InformationText == null)
                        // _InformationText = "La tua password è scaduta, per continuare dovrai aggiornarla. La nuova password dovrà contenere almeno 3 di questi elementi: Lettera Maiuscola, Lettera Minuscola, Numero, Simbolo. Ed essere lunga almeno 8 caratteri.";
                        _InformationText = "La tua password è scaduta, per continuare dovrai aggiornarla.";
                    return _InformationText;
                }
                set { _InformationText = value; }
            }
            private string _InformationText;


            public FrontendUserModPasswordControl()
                : base(HtmlTextWriterTag.Div)
            {
                this.CssClass = "container";
            }

            protected override void OnInit(EventArgs e)
            {
                base.OnInit(e);

                if (NetCms.Users.AccountManager.Logged)
                {
                    ChangePasswordForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
                    this.Controls.Add(getView());
                }
                else
                {
                    new NetCms.Users.LoginControl();
                }
            }

            void SubmitButton_Click(object sender, EventArgs e)
            {
                if (ChangePasswordForm.IsValid == VfGeneric.ValidationStates.Valid)
                {
                    string password = ChangePasswordForm.Fields.Find(x => x.Key == "nuovapassword").PostBackValue;
                    string reqPwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();

                    User current = UsersBusinessLogic.GetById(AccountManager.CurrentAccount.ID);

                    if (current.Password == reqPwd || current.NuovaPassword == reqPwd)
                    {
                        Control ctr = this.FindControl("div_message");
                        if (ctr != null)
                        {
                            ((HtmlGenericControl)ctr).Attributes["class"] = "alert alert-danger";
                            HtmlGenericControl a = new HtmlGenericControl("a");
                            a.Attributes["class"] = "close";
                            a.Attributes["data-dismiss"] = "alert";
                            a.Attributes["aria-label"] = "close";
                            a.Controls.Add(new LiteralControl("&times"));
                            ctr.Controls.Add(a);
                            ctr.Controls.Add(new LiteralControl("<p>E' necessario inserire una password diversa da quella correntemente impostata</p>"));
                            //((HtmlGenericControl)ctr).InnerHtml = "<p>E' necessario inserire una password diversa da quella correntemente impostata</p>";
                           
                        }
                        
                    }
                    else
                    {
                        current.NuovaPassword = null;
                        current.Password = reqPwd;
                        current.LastUpdate = DateTime.Now;

                        UsersBusinessLogic.MergeUser(current);
                        //NetCms.Users.AccountManager.LoginUser(current.ID.ToString());
                        NetCms.Users.AccountManager.DoLogOut();

                        HtmlGenericControls.Fieldset ok = new HtmlGenericControls.Fieldset("Aggiornamento password");
                        HtmlGenericControls.Div div = new HtmlGenericControls.Div();

                        div.InnerHtml = @"<p class=""alert alert-success"" role=""alert"">La password è stata aggiornata con successo. Sarà necessario reimpostarla tra 90 giorni.</p>";
                        if (NetCms.Networks.NetworksManager.CurrentFace == Faces.Front)
                        {
                            div.InnerHtml += "<p><a class=\"btn btn-primary\" href=\"" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + "\">Vai all'Homepage</a></p>";
                        }
                        ok.Controls.Add(div);
                        this.Controls.Clear();
                        this.Controls.Add(ok); 
                    }
                }
            }

            public VfManager ChangePasswordForm
            {
                get
                {
                    if (_ChangePasswordForm == null)
                    {
                        _ChangePasswordForm = new VfManager("changePassword");
                        _ChangePasswordForm.CssClass = "panel-body";
                        _ChangePasswordForm.SubmitButton.Text = "Aggiorna";
                        _ChangePasswordForm.InfoControl.CssClass = "alert alert-warning";
                        _ChangePasswordForm.SubmitButton.CssClass = "btn btn-primary";
                        _ChangePasswordForm.InfoControl.Controls.Add(new LiteralControl(InformationText));

                        VfPassword password = new VfPassword("nuovapassword");
                        password.Password.Attributes.Add("class","form-control");
                        password.Confirm.Attributes.Add("class","form-control");
                        password.Required = true;
                        
                        password.InfoControl.CssClass = "alert alert-info";
                        _ChangePasswordForm.Fields.Add(password);
                    }
                    return _ChangePasswordForm;
                }
            }
            private VfManager _ChangePasswordForm;

            public HtmlGenericControl getView()
            {

                HtmlGenericControl output = new HtmlGenericControl("div");
                output.Attributes["class"] = "refreshPWD_frontend";
               // output.Attributes["class"] = "panel-body";

                HtmlGenericControl message = new HtmlGenericControl("div");
                message.ID = "div_message";
                message.Attributes["class"] = "empty";
                output.Controls.Add(message);

                //HtmlGenericControls.Fieldset fieldset = new HtmlGenericControls.Fieldset("Dati Login");
                //fieldset.Attributes["class"] = "panel-heading";
                //output.Controls.Add(fieldset);
                HtmlGenericControls.Div div = new HtmlGenericControls.Div();
                div.Attributes["class"] = "panel panel-default";
                HtmlGenericControl head = new HtmlGenericControl("legend");
                head.Attributes["class"] = "panel-heading";
                head.Controls.Add(new LiteralControl("Dati Login"));
                div.Controls.Add(head);

                Button logout = new Button();
                logout.Text = "Logout";
                logout.ID = "BtLogout";
                logout.CssClass = "btn btn-default";
                logout.Click += new EventHandler(logout_Click);

                
                div.Controls.Add(ChangePasswordForm);
                HtmlGenericControls.Div divfoot = new HtmlGenericControls.Div();
                divfoot.Attributes["class"] = "panel-footer";
                divfoot.Controls.Add(logout);
                div.Controls.Add(divfoot);
                output.Controls.Add(div);
                return output;
            }

            void logout_Click(object sender, EventArgs e)
            {
                HttpContext.Current.Response.Redirect("default.aspx?do=logout");
            }
        }
    }
}