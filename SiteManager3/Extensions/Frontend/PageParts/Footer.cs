using System;
using System.Web;
using System.Xml;
using System.Data;
using System.Web.UI.HtmlControls;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS;
using System.Web.UI;
using Frontend.Common;


/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    public class Footer
    {
        private PageData PageData;

        public Footer(PageData pagedata)
        {
            PageData = pagedata;
        }

        private XmlNode xmlData;
        public XmlNode XmlData
        {
            get
            {
                if (xmlData == null)
                {
                    XmlDocument oXmlDoc = new XmlDocument();

                    string xmlFilePath = string.Empty;
                    
                    if (NetCms.Configurations.ThemeEngine.Status == NetCms.Configurations.ThemeEngine.ThemeEngineStatus.on)
                        //if (PageData.CurrentConfig.Layout != CmsConfigs.Model.Config.LayoutType.Responsive)
                        xmlFilePath = NetCms.Configurations.Paths.AbsoluteConfigRoot + "\\" + this.PageData.Network.SystemName.ToLower() + "\\" + PageData.CurrentConfig.Theme + "\\parts.xml";
                    else
                        xmlFilePath = NetCms.Configurations.Paths.AbsoluteConfigRoot + "\\" + this.PageData.Network.SystemName.ToLower() + "\\parts.xml";

                    try
                    {
                        oXmlDoc.Load(xmlFilePath);
                    }
                    catch (Exception ex)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Impossibile trovare il file '" + xmlFilePath + "'", "", "");
                        throw new Exception("Impossibile trovare il file '" + xmlFilePath + "'");
                    }
                    xmlData = oXmlDoc.DocumentElement;
                }
                return xmlData;
            }
        }

        public HtmlGenericControl GetControl()
        {
            StructureFolder folder = FolderBusinessLogic.GetFolderRecordByPath(PageData.FolderPath.Replace("'", "''"),PageData.Network.ID /*PageData.Folder.NetworkID*/);
            //DataRow row = NetFrontend.DAL.FileSystemDAL.GetFolderRecordBySqlConditions(" Path_Folder = '" + PageData.FolderPath.Replace("'", "''") + "'");
            if (folder != null)
            {
                if (folder.TitleOffset == 1 &&
                    this.PageData.PageControls.Meta["KEYWORDS"] != null &&
                    this.PageData.PageControls.Meta["KEYWORDS"].Content.Length > 0)// && PageData.HtmlHead.Title.Length==0)
                {
                    PageData.HtmlHead.Title = folder.Label + " - " + NetUtility.TreeUtility.CutWords(this.PageData.PageControls.Meta["KEYWORDS"].Content, 10);
                }

                if (folder.TitleOffset == 2 &&
                   this.PageData.PageControls.Meta["KEYWORDS"] != null &&
                   this.PageData.PageControls.Meta["KEYWORDS"].Content.Length > 0)// && PageData.HtmlHead.Title.Length==0)
                {
                    PageData.HtmlHead.Title = NetUtility.TreeUtility.CutWords(this.PageData.PageControls.Meta["KEYWORDS"].Content, 10); //;
                }
            }

            //foreach (HtmlMeta meta in PageData.PageControls.Meta)
            //{
            //    PageData.HtmlHead.Controls.Add(meta);                
            //}

            foreach (CmsHtmlMeta meta in PageData.PageControls.MetaTags)
            {
                PageData.HtmlHead.Controls.Add(meta.GetTag());
            }

            HtmlGenericControl footer = new HtmlGenericControl("div");
            footer.Attributes["class"] = "innerFooter";

            if (PageData.Homepage.ShowFooter)
                footer.Controls.Add(PageData.Homepage.Footer);

            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "partsFooter";
            div.InnerHtml = FooterHtml;
            footer.Controls.Add(div);

            //Script Scripts = new Script(PageData);
            //foreach (LiteralControl script in Scripts.Scripts)
            //    footer.Controls.Add(script);

            //if (PageData.BrowserData.HasData)
            //    foreach (HtmlGenericControl script in PageData.BrowserData.Scripts)
            //        footer.Controls.Add(script);
            

            //Evito di aggiungere il controllo di rimensionamento delle colonne se il css che sto caricando non � quello sdi default(grafico)
            //if (PageData.CurentStyle == PageData.StyleTypes.Default || PageData.CurentStyle == PageData.StyleTypes.Bianco)
            //    div.InnerHtml += "<script type=\"text/javascript\">$(document).ready(function(){SetColCXHeigth();});</script>"; 

            return footer;
        }

        private string _FooterHtml;
        private string FooterHtml
        {
            get
            {
                if (_FooterHtml == null)
                {
                    XmlNodeList oNodeList = XmlData.SelectNodes("/Portal/footer");
                    if (oNodeList != null && oNodeList.Count > 0)
                    {
                        PartsFilters filter = new PartsFilters(PageData);
                        _FooterHtml = filter.FilterTags(oNodeList[0].InnerXml);
                    }
                }
                return _FooterHtml;
            }
        }
    }
}