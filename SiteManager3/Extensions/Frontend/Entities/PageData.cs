using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Xml;
using System.Linq;
using NetCms.Front;
using NetFrontend.Homepages;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Reflection;
using LanguageManager.BusinessLogic;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    public class PageData
    {
        public CmsConfigs.Model.Config CurrentConfig
        {
            get
            {
                return CmsConfigs.Model.GlobalConfig.GetConfigByNetworkID(Network.ID);
            }
        }

        private PageControls _PageControls;
        public PageControls PageControls
        {
            get 
            {
                if (_PageControls == null)
                {
                    _PageControls = new PageControls(this);
                }
                return _PageControls; 
            }
        }

        private Where _Where;
        public Where Where
        {
            get
            {
                return _Where;
            }
            set
            {
                _Where = value;
            }
        }

        public NetCms.Vertical.VerticalApplication  CurrentVerticalApplication
        {
            get
            {
                if (_CurrentVerticalApplication == null)
                {
                    foreach (NetCms.Vertical.VerticalApplication vapp in NetCms.Vertical.VerticalApplicationsPool.ActiveAssemblies)
                        if (vapp.Parser.IsCurrentActiveApplication)
                            _CurrentVerticalApplication = vapp;                    
                }
                return _CurrentVerticalApplication;
            }
        }
        private NetCms.Vertical.VerticalApplication  _CurrentVerticalApplication;

        public StringCollection SystemFolders
        {
            get
            {
                if (_SystemFolders == null)
                {
                    string[] tempSystemFolders = System.Web.Configuration.WebConfigurationManager.AppSettings["SystemFolders"].ToLower().Split(',');
                    _SystemFolders = new StringCollection();
                    for (int i = 0; i < tempSystemFolders.Length; i++)
                    {
                        _SystemFolders.Add(tempSystemFolders[i]);
                    }
                }
                return _SystemFolders;
            }
        }
        private StringCollection _SystemFolders;

        public StringCollection SystemFiles
        {
            get
            {
                if (_SystemFiles == null)
                {
                    _SystemFiles = new StringCollection();
                    string[] tempSystemFiles = System.Web.Configuration.WebConfigurationManager.AppSettings["SystemFiles"].ToLower().Split(',');
                    for (int i = 0; i < tempSystemFiles.Length; i++)
                    {
                        _SystemFiles.Add(tempSystemFiles[i]);
                    }
                }
                return _SystemFiles;
            }
        }
        private StringCollection _SystemFiles;

        public Browsers.Browser BrowserData
        {
            get
            {
                if (_BrowserData == null)
                    _BrowserData = Browsers.Browsers.FabricateBrowser(this);
                
                return _BrowserData;
            }
        }
        private Browsers.Browser _BrowserData;
        public bool UpdateByMail(int ApplicationID)
        {
            bool active = false;
            string AppName = NetCms.Structure.Applications.ApplicationsPool.ActiveAssemblies[ApplicationID.ToString()].SystemName;
            if (XmlConfigs != null)
            {
                XmlNodeList oNodeList = NetCms.Configurations.XmlConfig.GetNodes("/Portal/configs/Networks/" + this.Network.SystemName + "/applications/" + AppName + "");
                if (oNodeList != null && oNodeList.Count > 0)
                    active = oNodeList[0].Attributes["updatebymail"].Value == "on";
            }

            return active;
        }

        private int _ShowRssButton;
        public bool ShowRssButton
        {
            get
            {
                if (_ShowRssButton == 0)
                {
                    System.IO.FileInfo file = new System.IO.FileInfo(HttpContext.Current.Server.MapPath("rss.xml"));
                    _ShowRssButton = file.Exists?1:2;
                }
                return _ShowRssButton == 1;
            }
        }

        public bool DebugMode
        {
            get
            {
                return NetCms.Configurations.Debug.GenericEnabled;
            }

        }

        public NetCms.Users.User Account
        {
            get
            {
                return NetCms.Users.AccountManager.CurrentAccount;
            }
        }
	
        #region Folders&Paths
        //*************************************************************

        #region Repository

        private string _RepositoryValue;
        public string RepositoryValue
        {
            get
            {
                if (_RepositoryValue == null && XmlConfigs != null)
                {
                    XmlNodeList oNodeList = XmlConfigs.SelectNodes("/Portal/configs/Networks/" + Network.SystemName + "/folders/Repository");
                    if (oNodeList != null && oNodeList.Count > 0)
                        _RepositoryValue = oNodeList[0].Attributes["name"].Value;
                }
                return _RepositoryValue;

            }
        }
        public string Repository
        {
            get
            {
                return RepositoryValue + "/";
            }
        }

        #endregion

        #region Trash

        private string _TrashValue;
        public string TrashValue
        {
            get
            {
                if (_TrashValue == null && XmlConfigs != null)
                {
                    XmlNodeList oNodeList = XmlConfigs.SelectNodes("/Portal/configs/Networks/" + Network.SystemName + "/folders/Trash");
                    if (oNodeList != null && oNodeList.Count > 0)
                        _TrashValue = oNodeList[0].Attributes["name"].Value;
                }
                return _TrashValue;

            }
        }
        public string Trash
        {
            get
            {
                return TrashValue + "/";
            }
        }

        #endregion

        #region SiteManagerRoot

        private string _SiteManagerRootValue;
        public string SiteManagerRootValue
        {
            get
            {
                if (_SiteManagerRootValue == null && XmlConfigs != null)
                {
                    XmlNodeList oNodeList = XmlConfigs.SelectNodes("/Portal/configs/Networks/" + Network.SystemName + "/folders/SiteManagerRoot");
                    if (oNodeList != null && oNodeList.Count > 0)
                        _SiteManagerRootValue = oNodeList[0].Attributes["name"].Value;
                }
                return _SiteManagerRootValue;
            }
        }
        public string SiteManagerRoot
        {
            get
            {
                return SiteManagerRootValue + "/";
            }
        }

        #endregion

        #region ApplicationRoot

        private string _ApplicationsRootValue;
        public string ApplicationsRootValue
        {
            get
            {
                if (_ApplicationsRootValue == null && XmlConfigs != null)
                {
                    XmlNodeList oNodeList = XmlConfigs.SelectNodes("/Portal/configs/Networks/" + Network.SystemName + "/folders/ApplicationRoot");
                    if (oNodeList != null && oNodeList.Count > 0)
                        _ApplicationsRootValue = oNodeList[0].Attributes["name"].Value;
                }
                return _ApplicationsRootValue;
            }
        }
        public string ApplicationsRoot
        {
            get
            {
                return this.AbsoluteRoot+ "/" + SiteManagerRoot + ApplicationsRootValue + "/";
            }
        }

        #endregion

        #region RootPath

        public string RootPathValue
        {
            get
            {
                return NetCms.Configurations.Paths.AbsoluteRoot;
            }
        }
        public string RootPath
        {
            get
            {
                return NetCms.Configurations.Paths.AbsoluteRoot;
            }
        }

        #endregion

        #region WebRoot

        private string _WebRootValue;
        public string WebRootValue
        {
            get
            {
                if (_WebRootValue == null)
                    _WebRootValue = this.Network.Path;
                return _WebRootValue;
            }
        }
        public string WebRoot
        {
            get
            {
                return WebRootValue + (WebRootValue.Length>0?"/":"");
            }
        }

        #endregion

        public string FolderPath
        {
            get
            {
                return FrontendNetwork.GetCurrentFolderPath();
            }
        }

        public  FrontendFolder Folder
        {
            get
            {                
                return Network.CurrentFolder;
            }
        }

        public string AbsoluteRoot
        {
            get
            {
                return NetCms.Configurations.Paths.AbsoluteRoot;
            }
        }
        public string AbsoluteWebRoot
        {
            get
            {
                return Network.Paths.AbsoluteFrontRoot;
            }
        }
        
        private bool PageDepthBuilded;
        private int _PageDepth = 0;
        public int PageDepth
        {
            get
            {
                if (!PageDepthBuilded)
                {
                    string[] filepath = FrontendNetwork.GetCurrentFolderPath().Split('/');
                    _PageDepth = filepath.Length;
                    /*if (this.RootPathValue.Length > 0) _PageDepth--;
                    if (this.WebRoot.Length > 0) _PageDepth--;
                    */
                    PageDepthBuilded = true;
                }

                return _PageDepth;
            }
            set
            {
                _PageDepth = value;
            }
        }

        private string _PageName;
        public string PageName
        {
            get
            {
                if (_PageName == null)
                {                    
            	    string sPath = System.Web.HttpContext.Current.Request.Url.LocalPath; 
            	    System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath); 
            	    string sRet = oInfo.Name;
                    _PageName = sRet.ToLower(); 
                }
                return _PageName;
            }
        }
        //*************************************************************
        #endregion

        /*public NetUtility.NetworkInfo _Network;
        public NetUtility.NetworkInfo Network
        {
            get
            {
                if (_Network == null)
                {
                    _Network = new NetUtility.NetworkInfo(Frontend.DataServiceProxy.Networks);
                    HttpContext.Current.Items["NetworkInfo"] = _Network;
                }
                return _Network;
            }
        }*/
        public FrontendNetwork Network
        { 
            get 
            {
                return FrontendNetwork.GetCurrentFrontendNetwork();
            } 
        }
        #region Session Application Request Server Postback Response Application
        //*************************************************************
        private HttpSessionState _Session;
        public HttpSessionState Session
        {
            get { return _Session; }
        }

        private HttpRequest _Request;
        public HttpRequest Request
        {
            get { return _Request; }
            set { _Request = value; }
        }

        private HttpServerUtility _Server;
        public HttpServerUtility Server
        {
            get { return _Server; }
        }

        public bool IsPostBack
        {
            get { return _IsPostBack; }
        }
        private bool _IsPostBack;

        private HttpResponse _Response;
        public HttpResponse Response
        {
            get { return _Response; }
        }

        private HttpApplicationState _Application;
        public HttpApplicationState Application
        {
            get { return _Application; }
        }
        //*************************************************************
        #endregion

        public enum StyleTypes
        {
            Default,
            AltoContrasto,
            Bianco
        }

        public string GetConfigAttribute(string XmlPath, string AttributeName)
        {
            string attribute = "";
            XmlNodeList oNodeList = XmlConfigs.SelectNodes(XmlPath);
            if (oNodeList != null && oNodeList.Count > 0 && oNodeList[0].Attributes[AttributeName] != null)
                attribute = oNodeList[0].Attributes[AttributeName].Value;

            return attribute;
        }

        private string _XmlLangInfo;
        public string XmlLangInfo
        {
            get
            {
                if (_XmlLangInfo == null)
                {
                    //Aggiungo il supporto multilingua
                    if (LanguageManager.Utility.Config.EnableMultiLanguage)
                    {                        
                        _XmlLangInfo = @"xml:lang=""" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + @"""";
                    }
                    else
                        _XmlLangInfo = @"xml:lang=""it""";// lang=""it"" ";

                }
                return _XmlLangInfo;
            }
        }

        private bool styleloaded = false;
        private StyleTypes _CurentStyle;
        public StyleTypes CurentStyle
        {
            get
            {
                if (!styleloaded)
                {
                    NetUtility.RequestVariable stile = new NetUtility.RequestVariable("stile", Request.QueryString);
                    if (stile.IsValid())
                    {
                        switch (stile.StringValue)
                        {
                            case "altocontrasto":
                                _CurentStyle = StyleTypes.AltoContrasto;
                                break;
                            case "bianco":
                                _CurentStyle = StyleTypes.Bianco;
                                break;
                            default:
                                _CurentStyle = StyleTypes.Default;
                                break;
                        }
                    }
                    else
                        _CurentStyle = StyleTypes.Default;
                    styleloaded = true;
                }
                return _CurentStyle;
            }
        }

        private XmlNode xmlConfigs;
        public XmlNode XmlConfigs
        {
            get
            {
                if (xmlConfigs == null)
                {
                    XmlDocument oXmlDoc = new XmlDocument();
                    try
                    {
                        string xmlFilePath = NetCms.Configurations.Paths.AbsoluteConfigRoot + "\\config.xml";
                        oXmlDoc.Load(xmlFilePath);
                    }
                    catch (Exception ex)
                    {
                        //xmlConfigs = null;
                    }
                    xmlConfigs = oXmlDoc.DocumentElement;
                }
                return xmlConfigs;
            }
        }
        
        public NetCms.Structure.CmsPathCodes Codes
        {
            get
            {
                if (_Codes == null)
                    _Codes = new NetCms.Structure.CmsPathCodes(this.AbsoluteWebRoot);
                return _Codes;
            }
        }
        private NetCms.Structure.CmsPathCodes _Codes;

        public int checkNumericGetExist(string key, string redirect)
        {
            int output = 0;
            if (Request.QueryString[key] == null && redirect.Length > 0)
                NetCms.Diagnostics.Diagnostics.Redirect(redirect);
            if (!int.TryParse(Request.QueryString[key], out output) && redirect.Length > 0)
                NetCms.Diagnostics.Diagnostics.Redirect(redirect);
            return output;
        }

        private HomepageData _Homepage;
        public HomepageData Homepage
        {
            get
            {
                if (_Homepage == null)
                    InitHomepage();
                return _Homepage;
            }
        }

        /// <summary>
        /// Inserito per forzare la cancellazione della cache in caso di cambio lingua
        /// </summary>
        public bool NeedToClearFrontCache
        {
            get; set;
        }

        private int _IsReservedArea = -2;
        public int ReservedArea
        {
            get
            {
                if (_IsReservedArea == -2)
                {
                    StructureFolder folder = FolderBusinessLogic.GetFolderRecordByPath(Folder.Path,Folder.NetworkID);
                    //DataRow row = NetFrontend.DAL.FileSystemDAL.GetFolderRecordBySqlConditions("Path_Folder = '" + Folder.Path + "'");
                    if (folder != null)
                        _IsReservedArea = folder.RestrictedInt;//int.Parse(row["Restricted_Folder"].ToString());

                }
                return _IsReservedArea;
            }
        }
        public bool IsReservedArea
        {
            get 
            {
                if (_IsReservedArea == -2)
                {
                    int Parent = 0;
                    string ParentPath = Folder.Path;
                    do
                    {
                        StructureFolder folder = FolderBusinessLogic.GetFolderRecordByPath(Folder.Path,Folder.NetworkID);
                        //DataRow row = NetFrontend.DAL.FileSystemDAL.GetFolderRecordBySqlConditions("Path_Folder = '" + Folder.Path + "'");
                        if (folder != null)
                        {
                            _IsReservedArea = folder.RestrictedInt; //int.Parse(row["Restricted_Folder"].ToString());
                            Parent = folder.Parent.ID;//int.Parse(row["Parent_Folder"].ToString());
                        }
                        if (Parent > 0)
                            ParentPath = ParentPath.Substring(0,ParentPath.LastIndexOf("/"));
                    } while (Parent > 0 && _IsReservedArea == 0);


                }
                return _IsReservedArea == -1 || _IsReservedArea > 0;
            }
        }

        private HtmlHead _HtmlHead;
        public HtmlHead HtmlHead
        {
            get
            {
                return _HtmlHead;
            }
            set
            {
                _HtmlHead = value;
            }
        }

        public void AddCssLink(string cssFileName)
        {
            HtmlLink link = new HtmlLink();

            link.Attributes["type"] = "text/css";
            link.Attributes["media"] = "screen";
            link.Attributes["rel"] = "stylesheet";
            link.Attributes["href"] = this.AbsoluteRoot + "/" + this.Network.CssFolder + "/" + cssFileName;

            this.HtmlHead.Controls.Add(link);
            
        }

        private DateTime _StartTime;
        public DateTime StartTime
        {
            get { return _StartTime; }
        }

        public StateBag ViewState
        {
            get { return _ViewState; }
            private set { _ViewState = value; }
        }
        private StateBag _ViewState;

        #region Costruttori

        public PageData(StateBag viewState, System.Web.UI.Page page):this(viewState,page.Request,page.Session,page.Application,page.Server,page.Response,page.IsPostBack)
        {
            NetCms.PageData adminPageData = new NetCms.PageData(page, viewState);
        }

        public PageData(StateBag viewState, HttpRequest request, HttpSessionState session, HttpApplicationState application, HttpServerUtility server, HttpResponse response, bool isPostBack)
        {
            ViewState = viewState;
            _Application = application;
            //LabelsCache.AbsoluteConfigsRoot = this.AbsoluteRoot + "/configs/" + this.Network.SystemName.ToLower();
            _StartTime = DateTime.Now;
            _Request = request;
            _Server = server;
            _Response = response;
            _Session = session;
            _PageDepth = 0;
            _IsPostBack = isPostBack;           
            CheckForExternalLink();            
            SearchPostBack();
        }

        //public PageData()
        //{

        //}

        #endregion

        #region Metodi

        public void InitHomepage()
        {
            HomepageFinder finder = new HomepageFinder(this);
            _Homepage = finder.GetHomepage();
        }
        private void CheckForExternalLink()
        {
            NetUtility.RequestVariable extlink = new NetUtility.RequestVariable("extlink", Request);
            if (extlink.IsValid(NetUtility.RequestVariable.VariableType.Integer))
            {
                Document link = DocumentBusinessLogic.GetDocumentRecordById(extlink.IntValue);
                //DataRow row = NetFrontend.DAL.LinksDAL.GetDocumentRecordByID(extlink.IntValue);
                if (link != null)
                {
                    PropertyInfo propHref = link.ContentRecord.GetType().GetProperty("Href");
                    PropertyInfo propClicks = link.ContentRecord.GetType().GetProperty("Clicks");
                    int clicks = int.Parse(propClicks.GetValue(link.ContentRecord, null).ToString());
                    //Frontend.DataServiceProxy.Execute("UPDATE Docs_Link SET Clicks_Link = " + (++clicks) + " WHERE id_Link = " + extlink.IntValue + "");
                    NetCms.Diagnostics.Diagnostics.Redirect(propHref.GetValue(link.ContentRecord, null).ToString());
                }

            }

        }
        
        #endregion

        private void SearchPostBack()
        {
            NetUtility.RequestVariable SearchButton = new NetUtility.RequestVariable("search", this.Request.Form);
            NetUtility.RequestVariable SearchText = new NetUtility.RequestVariable("searchtext", this.Request.Form);            
            NetUtility.RequestVariable Login_User = new NetUtility.RequestVariable("Login_User" , this.Request.Form);
            NetUtility.RequestVariable Login_Password = new NetUtility.RequestVariable("Login_Password" , this.Request.Form);
                       
            bool UserAction = false;
            //bool LoginAction = false;

            string user_search = "";
            if (IsPostBack)
            {
                if (SearchButton.IsValid() && SearchButton.StringValue == "Cerca" && SearchText.StringValue != "Parola da cercare")
                {
                    user_search = SearchText.StringValue;
                    UserAction = true;
                }
                if (SearchText.IsValid() && SearchButton.StringValue == "Find" && SearchText.StringValue != "Insert Word")
                {
                    user_search = SearchText.StringValue;
                    UserAction = true;
                }
                //if (Login_User.IsValidString && Login_Password.IsValidString && (SearchText.StringValue == "Parola da cercare" || SearchText.StringValue == "Insert Word"))
                //{ 
                //    LoginAction = true;        
                //}
            }

            if (SearchButton.IsPostBack && (UserAction || SearchText.StringValue == "Parola da cercare" || SearchText.StringValue == "Insert Word"))
            {
                if (!UserAction)
                    user_search = "";

                if (this.AbsoluteWebRoot.Contains("english"))
                {
                    NetCms.Diagnostics.Diagnostics.Redirect(this.AbsoluteWebRoot + "/search/search.aspx?zoom_query=" + user_search);
                }
                else {
                    NetCms.Diagnostics.Diagnostics.Redirect(this.AbsoluteWebRoot + "/ricerca/ricerca.aspx?zoom_query=" + user_search);
                }
            }

        }

        private ContentsHandler contentsHandler;
        public ContentsHandler ContentsHandler
        {
            get
            {
                if (contentsHandler == null) contentsHandler = ContentsHandler.GetSpecificHandler(this);
                return contentsHandler;
            }
            set { contentsHandler = value; }
        }
               
        public bool LoginStatus
        {
            get
            {
                bool active = false;

                if (XmlConfigs != null)
                {
                    XmlNodeList oNodeList = XmlConfigs.SelectNodes("/Portal/configs/externalapplications/login");
                    if (oNodeList != null && oNodeList.Count > 0)
                        active = oNodeList[0].Attributes["status"].Value == "on";
                }

                return active;
            }
        }

        public string LoginType
        {
            get
            {
                string type = "simply";

                if (XmlConfigs != null)
                {
                    XmlNodeList oNodeList = XmlConfigs.SelectNodes("/Portal/configs/externalapplications/login");
                    if (oNodeList != null && oNodeList.Count > 0)
                        type = oNodeList[0].Attributes["type"].Value;
                }

                return type;
            }
        }

        public bool LoginHeadStatus
        {
            get
            {
                bool active = false;


                if (XmlConfigs != null)
                {
                    XmlNodeList oNodeList = XmlConfigs.SelectNodes("/Portal/configs/externalapplications/login");
                    if (oNodeList != null && oNodeList.Count > 0)
                        active = oNodeList[0].Attributes["headstatus"].Value == "on";
                }

                return active;
            }
        }        

        public HtmlGenericControl GetErrorControl()
        {
            HtmlGenericControls.Fieldset fieldset = new HtmlGenericControls.Fieldset("Pagina non esistente");

            HtmlGenericControls.Par par = new HtmlGenericControls.Par();
            par.InnerHtml = "Questa pagina non esiste.";
            par.InnerHtml += "<br />E' possibile che l'indirizzo non sia corretto o che la pagina sia stata rimossa.";

            fieldset.Controls.Add(par);

            return fieldset;
        }

        private HtmlGenericControl _Body;
        public HtmlGenericControl Body 
        {
            get 
            {
                return _Body;
            }
            set
            {
                _Body = value;
            }
        }






        #region HTML5

        private string _HTML5XmlLangInfo;
        public string HTML5XmlLangInfo
        {
            get
            {
                if (_HTML5XmlLangInfo == null)
                {
                    //Aggiungo il supporto multilingua
                    if (LanguageManager.Utility.Config.EnableMultiLanguage)
                    {
                        _HTML5XmlLangInfo = @"lang=""" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + @""" ";
                    }
                    else
                        _HTML5XmlLangInfo = @"lang=""it"" ";
                }
                return _HTML5XmlLangInfo;
            }
        }
        #endregion

    }
}