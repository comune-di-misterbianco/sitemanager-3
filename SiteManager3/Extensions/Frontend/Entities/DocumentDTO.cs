﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Frontend.Entities
{
    public class DocumentDTO
    {

        public DocumentDTO() { }
        public int ID { get; set; }
        public string Titolo { get; set; }
        public string Descrizione { get; set; }

        public string Url { get; set; }
        //public DateTime Data { get; set; }
        //public string Content { get; set; }

    }
}
