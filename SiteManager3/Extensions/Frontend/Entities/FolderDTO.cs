﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Frontend.Entities
{
    public class FolderDTO
    {
        public FolderDTO() { }

        public int ID { get; set; }
        public string Titolo
        {
            get;
            set;
        }
        public string Descrizione { get; set; }
        public string Url { get; set; }
        public int Riffapp { get; set; }
        public string Coverimage { get; set; }

        public string Cssclass { get; set; }

        public int Order { get; set; }

        public IList<FolderDTO> Childfolder { get; set; }
        public IList<DocumentDTO> Documents { get; set; }

    }
}
