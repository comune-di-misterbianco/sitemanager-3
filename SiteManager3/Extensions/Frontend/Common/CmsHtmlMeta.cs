﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace Frontend.Common
{
    public class CmsHtmlMeta
    {
        public CmsHtmlMeta(string name, string content, string httpequiv, string charset) 
        {
            this.name = name;
            this.content = content;
            this.httpequiv = httpequiv;
            this.charset = charset;
        }

        public CmsHtmlMeta(string name, string content, string httpequiv)
        {
            this.name = name;
            this.content = content;
            this.httpequiv = httpequiv;            
        }

        public CmsHtmlMeta(string name, string content)
        {
            this.name = name;
            this.content = content;            
        }

        private string name;
        public string Name
        {
            get { return name; }
            
        }

        private string content;
        public string Content
        {
            get {
                return content;
            }
            set 
            {
                content = value;
            }
            
        }

        private string httpequiv;
        public string HttpEquiv
        {
            get {
                return httpequiv;
            }            
        }

        private string charset;
        public string Charset
        {
            get {
                return charset;
            }
            
        }

        private string BuildMetaTag() 
        {
            string strMeta = "<meta ";
            
            if (!string.IsNullOrEmpty(Name))
                strMeta += " name=\"" + Name + "\"";
            if (!string.IsNullOrEmpty(Content))
                strMeta += " content=\"" + Content + "\""; 
            if (!string.IsNullOrEmpty(HttpEquiv))
                strMeta += " http-equiv=\"" + HttpEquiv + "\"";
            if (!string.IsNullOrEmpty(Charset))
                strMeta += " charset=\"" + Charset + "\"";
            
            strMeta += " />" + System.Environment.NewLine;
            
            return strMeta;
        }

        public LiteralControl GetTag() 
        {
            return new LiteralControl(BuildMetaTag());
        }
    }
}
