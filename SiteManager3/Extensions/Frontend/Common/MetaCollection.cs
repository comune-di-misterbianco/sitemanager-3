using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web.UI.HtmlControls;


public class HtmlMetaCollection : IEnumerable
{
    private NetCollection coll;
    public int Count { get { return coll.Count; } }
    public HtmlMetaCollection()
    {        
        coll = new NetCollection();
    }

    public void Add(HtmlMeta meta)
    {
        coll.Add(meta.Name, meta);
    }
    public void Add(string name, string content)
    {
        Add(name, content, null);
    }
    public void Add(string name, string content, string httpequiv)
    {
        if (this.Contains(name))
        {
            this[name].Content = content;
            if (httpequiv != null)
                this[name].HttpEquiv = httpequiv;
        }
        else
        {
            HtmlMeta meta = new HtmlMeta();
            meta.Name = name;
            meta.Content = content;
            if (httpequiv != null)
                meta.HttpEquiv = httpequiv;
            this.Add(meta);
        }
    }

    public void Remove(HtmlMeta meta)
    {
        coll.Remove(meta.Name);
    }

    public void Clear()
    {
        coll.Clear();
    }

    public HtmlMeta this[int i]
    {
        get
        {
            HtmlMeta str = (HtmlMeta)coll[coll.Keys[i]];
            return str;
        }
    }

    public HtmlMeta this[string metaname]
    {
        get
        {
            HtmlMeta val = (HtmlMeta)coll[metaname];
            return val;
        }
    }


    public bool Contains(string metaname)
    {
        return coll[metaname] != null;
    }
    #region Enumerator

    public IEnumerator GetEnumerator()
    {
        return new CollectionEnumerator(this);
    }

    private class CollectionEnumerator : IEnumerator
    {
        private int CurentPos = -1;
        private HtmlMetaCollection Collection;
        public CollectionEnumerator(HtmlMetaCollection coll)
        {
            Collection = coll;
        }
        public object Current
        {
            get
            {
                return Collection[CurentPos];
            }
        }
        public bool MoveNext()
        {
            if (CurentPos < Collection.Count - 1)
            {
                CurentPos++;
                return true;
            }
            else
                return false;
        }
        public void Reset()
        {
            CurentPos = -1;
        }
    }
    #endregion
}
