﻿using LabelsManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetFrontend.TemplateEngine
{
    public class TemplateUtility
    {
        public TemplateUtility() { }

        private Labels _CommonLabels;
        private Labels CommonLabels
        {
            get
            {
                if (_CommonLabels == null)
                {
                    _CommonLabels = LabelsCache.GetTypedLabels(typeof(CommonLabels)) as Labels;
                }
                return _CommonLabels;
            }
        }

        private Dictionary<string, string> _TemplateCommonLabels;
        public Dictionary<string, string> TemplateCommonLabels
        {
            get
            {
                if (_TemplateCommonLabels == null)
                {
                    _TemplateCommonLabels = new Dictionary<string, string>();
                    foreach (CommonLabels.CommonLabelsList label in Enum.GetValues(typeof(CommonLabels.CommonLabelsList)))
                        _TemplateCommonLabels.Add(label.ToString().ToLower(), CommonLabels[label].ToString());
                }
                return _TemplateCommonLabels;
            }
        }           
    
        public string TemplateParser(Object data, Object sezione, string tplPath, Object paging = null, Object formvalue = null, Object options = null)
        {
            string code = string.Empty;

            var template = Scriban.Template.Parse(System.IO.File.ReadAllText(tplPath), tplPath);
            if (template.HasErrors)
            {
                if (NetCms.Configurations.Debug.DebugLoginEnabled)
                    foreach (var error in template.Messages)
                        code += error.Message + System.Environment.NewLine;
                else
                    foreach (var error in template.Messages)
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, error.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
            }
            else
            {
                var context = new Scriban.TemplateContext();
                context.PushCulture(System.Globalization.CultureInfo.CurrentCulture);

                var scriptObj = new Scriban.Runtime.ScriptObject();
                scriptObj.Add("items", data);
                scriptObj.Add("sezione", sezione);
                //scriptObj.Add("labels", TemplateLabels);
                scriptObj.Add("commonlabels", TemplateCommonLabels);

                if (paging != null)
                    scriptObj.Add("pager", paging);

                if (formvalue != null)
                    scriptObj.Add("form", formvalue);

                if (options != null)
                    scriptObj.Add("options", options);

                context.PushGlobal(scriptObj);

                code = template.Render(context);
            }

            return code;
        }
    }
}
