using System;
using System.IO;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Xml;
using NetCms.Front;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    public class Css
    {
        public string CssFolder
        {
            get
            {                
                return PageData.Network.CssFolder;
            }
        }

        private PageData PageData;
        public Css(PageData pagedata)
        {
            PageData = pagedata;
        }

        string cssLinkPattern = "<link type=\"{0}\" media=\"{1}\" rel=\"{2}\" href=\"{3}\" />" + System.Environment.NewLine;
        
        public LiteralControl GetCSS(CssObjects Css)
        {            
            string cssLink = "";
            string CSS = "";
            
            switch (Css)
            {
                case CssObjects.theme:
                    CSS += Theme;
                    break;
                case CssObjects.altocontrasto:
                    CSS += AltoContrasto;
                    break;
                case CssObjects.bianco:
                    CSS += Bianco;
                    break;
                case CssObjects.stampa:
                    CSS += Stampa;
                    break;

            }
            if (CSS.Length > 0)
            {                          
                string cssMedia = "";

                if (Css == CssObjects.stampa)
                    cssMedia = "print";
                else
                    cssMedia = "screen"; 
                
                string cssHref = PageData.AbsoluteRoot + this.CssFolder + "/" + CSS;
                cssLink = string.Format(cssLinkPattern, "text/css", cssMedia, "stylesheet", cssHref); 
            }
            return new LiteralControl(cssLink);
        }

        public string GetNode(CssObjects Css)
        {

            string CSS = ""; ;
            string path = "/Portal/configs/Networks/" + PageData.Network.SystemName.ToLower() + "/frontend/css/" + GetCSSString(Css);
            XmlNodeList oNodeList = NetCms.Configurations.XmlConfig.GetNodes(path);

            if (oNodeList != null && oNodeList.Count > 0)
            {

                CSS += oNodeList[0].Attributes["name"].Value;

            }
            return CSS;
        }

        public enum CssObjects
        {
            theme,  
            altocontrasto,
            bianco,
            stampa,
            folders
        }

        private string GetCSSString(CssObjects Css)
        {
            string css = "";
            switch (Css)
            {
                case CssObjects.theme:
                    css = "theme";
                    break;
                case CssObjects.altocontrasto:
                    css = "altocontrasto";
                    break;
                case CssObjects.bianco:
                    css = "bianco";
                    break;
                case CssObjects.stampa:
                    css = "stampa";
                    break;
                case CssObjects.folders:
                    css = "folders";
                    break;
            }
            return css;

        }


        private string _Theme;
        public string Theme
        {
            get
            {
                if (_Theme == null)
                    _Theme = GetNode(CssObjects.theme); 
                return _Theme;
            }
        }


        private string _Bianco;
        public string Bianco
        {
            get
            {
                if (_Bianco == null)
                    _Bianco = GetNode(CssObjects.bianco);
                return _Bianco;
            }
        }


        private string _AltoContrasto;
        public string AltoContrasto
        {
            get
            {
                if (_AltoContrasto == null)
                    _AltoContrasto = GetNode(CssObjects.altocontrasto);
                return _AltoContrasto;
            }
        }

        private string _Stampa;
        public string Stampa
        {
            get
            {
                if (_Stampa == null)
                    _Stampa = GetNode(CssObjects.stampa);
                return _Stampa;
            }
        }


        private HtmlLink[] _Folders;
        public HtmlLink[] Folders
        {
            get
            {
                if (_Folders == null)
                {
                    if (PageData.Network.CurrentFolder != null)
                    {
                        FrontendFolder folder = PageData.Network.CurrentFolder;
                        int count = folder.Depth;
                        _Folders = new HtmlLink[count];
                        while (folder != null && folder.Parent != null)
                        {
                            if (folder.NotInheritedCssClass.Trim().Length > 0)
                                _Folders[--count] = BuildCssLink("folders/" + folder.NotInheritedCssClass.ToLower() + ".css", NetCms.Configurations.ThemeEngine.Status);
                            folder = folder.Parent;
                        }
                    }
                    else _Folders = new HtmlLink[0];
                }
                return _Folders;
            }
        }

        private HtmlLink BuildCssLink(string cssFileName, NetCms.Configurations.ThemeEngine.ThemeEngineStatus themeenginestatus)
        {
            HtmlLink link = new HtmlLink();                 
            link.Attributes["rel"] = "stylesheet";            
            link.Attributes["href"] = (themeenginestatus == NetCms.Configurations.ThemeEngine.ThemeEngineStatus.off) ? PageData.AbsoluteRoot + this.CssFolder + "/" + cssFileName : PageData.CurrentConfig.CurrentTheme.CssFolder + cssFileName;
            return link;
        }        
    }
}