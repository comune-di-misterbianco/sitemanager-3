﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web;

namespace NetFrontend
{
    public enum PostBackMessagesType { normal, success, notify, error, warning }

    public class NotifyBox : WebControl
    {
        public NotifyBox()
            : base(HtmlTextWriterTag.Div)
        {
            this.CssClass = "frontend-notify";     
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            this.ParseMessages();
        }

        private void ParseMessages()
        {
            List<NotifyMessage> notifyMessages = new List<NotifyMessage>();

            if (HttpContext.Current.Session["NotifyMessages"] != null)
                notifyMessages.AddRange((List<NotifyMessage>)HttpContext.Current.Session["NotifyMessages"]);

            if (HttpContext.Current.Items["NotifyMessages"] != null)
                notifyMessages.AddRange((List<NotifyMessage>)HttpContext.Current.Items["NotifyMessages"]);

            if (notifyMessages.Count > 0)
            {
                ParseMessages(notifyMessages);
                if (HttpContext.Current.Session["NotifyMessages"] != null)
                    HttpContext.Current.Session["NotifyMessages"] = null;
            }
            else this.CssClass += " disabled";
        }

        private void ParseMessages(List<NotifyMessage> notifyMessages)
        {
            foreach (NotifyMessage msg in notifyMessages)
                this.ParseMessage(msg.Message, msg.State);
        }

        private string GetCssClass(PostBackMessagesType state)
        {
            string cssClass ="alert alert-";
            switch (state)
            {
                case PostBackMessagesType.normal:
                    cssClass = cssClass + "info ";
                    break;
                case PostBackMessagesType.success:
                    cssClass = cssClass + "success ";
                    break;
                case PostBackMessagesType.notify:
                    cssClass = cssClass + "info ";
                    break;
                case PostBackMessagesType.error:
                    cssClass = cssClass + "danger ";
                    break;
                case PostBackMessagesType.warning:
                    cssClass = cssClass + "warning ";
                    break; 
                default:
                    break;
            }
            return cssClass + "alert-dismissible fade in show";
        
        }

        private void ParseMessage(string msg, PostBackMessagesType state)
        {
            WebControl divNotify = new WebControl(HtmlTextWriterTag.Div);
            //G2Core.XhtmlControls.Div control = new G2Core.XhtmlControls.Div();
            divNotify.CssClass = GetCssClass(state);
            divNotify.Attributes.Add("role", "alert");
            divNotify.Controls.Add(new LiteralControl("<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>"));

            divNotify.Controls.Add(new LiteralControl(msg));

            //G2Core.XhtmlControls.Par par = new G2Core.XhtmlControls.Par();
            //par.InnerText = msg;
            ////control.Class = "notifyitem-" + state.ToString();
            //control.Controls.Add(new LiteralControl("<a class=\"close\">×</a>"));
            //control.Controls.Add(par);

            this.Controls.Add(divNotify);
        }

        private static void AddMessage(string msg, PostBackMessagesType state, bool session)
        {
            NotifyMessage message = new NotifyMessage(msg, state);

            object objValues;
            if (session)
            {
                if (HttpContext.Current.Session["NotifyMessages"] == null)
                    HttpContext.Current.Session["NotifyMessages"] = new List<NotifyMessage>();
                objValues = HttpContext.Current.Session["NotifyMessages"];
            }
            else
            {
                if (HttpContext.Current.Items["NotifyMessages"] == null)
                    HttpContext.Current.Items["NotifyMessages"] = new List<NotifyMessage>();
                objValues = HttpContext.Current.Items["NotifyMessages"];
            }

            List<NotifyMessage> notifyMessages = (List<NotifyMessage>)objValues;
            notifyMessages.Add(message);
        }

        public static void AddMessage(string msg)
        {
            AddMessage(msg, PostBackMessagesType.normal, false);
        }
        public static void AddMessage(string msg, PostBackMessagesType state)
        {
            AddMessage(msg, state, false);
        }

        public static void AddMessageAndReload(string msg, PostBackMessagesType state)
        {
            AddSessionMessage(msg, state, HttpContext.Current.Request.Url.ToString());
        }

        public static void AddSessionMessage(string msg, PostBackMessagesType state)
        {
            AddMessage(msg, state, true);
        }

        public static void AddSessionMessage(string msg, PostBackMessagesType state, bool goBack)
        {
            AddMessage(msg, state, true);
            if (NetCms.Users.AccountManager.Logged)
                NetCms.Diagnostics.Diagnostics.Redirect(NetCms.Users.AccountManager.CurrentAccount.History.GetBackURL());
            else
                NetCms.Diagnostics.Diagnostics.Redirect(NetCms.Configurations.Paths.AbsoluteRoot);
        }

        public static void AddSessionMessage(string msg, PostBackMessagesType state, string redirectToUrl)
        {
            AddMessage(msg, state, true);
            NetCms.Diagnostics.Diagnostics.Redirect(redirectToUrl);
        }
    }

    public class NotifyMessage
    {
        public string Message
        {
            get { return _Message; }
            private set { _Message = value; }
        }
        private string _Message;

        public PostBackMessagesType State
        {
            get { return _State; }
            private set { _State = value; }
        }
        private PostBackMessagesType _State;

        public NotifyMessage(string msg, PostBackMessagesType state)
        {
            State = state;
            Message = msg;
        }
    }
}
