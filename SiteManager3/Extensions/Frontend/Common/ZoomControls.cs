using System;
//using System.IO;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using NetCms;

/// <summary>
/// Summary description for Configs
/// </summary>

namespace NetFrontend
{
    public static class ZoomSearchControls
    {
        public static Control StartControl()
        {
            return new LiteralControl("\n<!--ZOOMRESTART-->\n");
        }
        public static Control StopControl()
        {
            return new LiteralControl("\n<!--ZOOMSTOP-->\n");
        }
    }
}