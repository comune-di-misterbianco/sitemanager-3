using System;
using System.Data;
using System.Web.UI.HtmlControls;
using NetCms.Front;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Collections.Generic;
using System.Linq;
using Frontend.Common;


namespace NetFrontend
{
    public class PageControls
    {
        private PageData PageData;

        public HtmlMetaCollection Meta { get; private set; }
        public List<CmsHtmlMeta> MetaTags { get; private set; }

        public PageControls(PageData pagedata)
        {
            PageData = pagedata;
            Meta = new HtmlMetaCollection();
            MetaTags = new List<CmsHtmlMeta>();
        }

        public void UpdateDataByDoc(int DocumentID)
        {
            UpdateDataByDoc(DocumentID, false);
        }
        public void UpdateDataByDoc(int DocumentID,bool updateTitle)
        {
            if (DocumentID > 0)
            {
                DataRow[] rows = NetFrontend.DAL.FileSystemDAL.ListDocumentsMetaRecordsBySqlConditions("Stato_DocMeta = 1 AND Doc_DocMeta = " + DocumentID);
                if (rows.Length > 0)
                {
                    foreach (DataRow row in rows)
                    {
                        string name = row["Name_Meta"].ToString();
                        string Content_Meta = row["Content_DocMeta"].ToString();

                        //PageData.PageControls.Meta.Add(name, Content_Meta);                        
                        PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta(name, Content_Meta));
                    }
                }
                Document docrow = DocumentBusinessLogic.GetDocumentRecordById(DocumentID); //NetFrontend.DAL.FileSystemDAL.GetDocumentRecordByID(DocumentID);

                if (docrow!=null && updateTitle)
                {
                    string Label = docrow.Nome;

                    /*if (PageData.Where != null &&
                        int.Parse(docrow["HomeType_Folder"].ToString()) != int.Parse(docrow["id_Doc"].ToString())
                        )
                        PageData.Where.Blocks.Add(new WhereBlock(Label));*/


                    //if (docrow.TitleOffset == 1 &&
                    //    this.Meta["KEYWORDS"] != null &&
                    //    this.Meta["KEYWORDS"].Content.Length > 0)
                    //    PageData.HtmlHead.Title = Label + " - " + this.Meta["KEYWORDS"].Content;

                    //if (docrow.TitleOffset == 2 &&
                    //    this.Meta["KEYWORDS"] != null &&
                    //    this.Meta["KEYWORDS"].Content.Length > 0)
                    //    PageData.HtmlHead.Title = NetUtility.TreeUtility.CutWords(this.Meta["KEYWORDS"].Content, 10);

                    if (docrow.TitleOffset == 1 &&
                        this.MetaTags != null &&
                        this.MetaTags.Find(x=>x.Name == "KEYWORDS").Content.Length > 0)
                        PageData.HtmlHead.Title = Label + " - " + this.MetaTags.Find(x => x.Name == "KEYWORDS").Content;

                    if (docrow.TitleOffset == 2 &&
                        this.MetaTags != null &&
                        this.MetaTags.Find(x => x.Name == "KEYWORDS").Content.Length > 0)
                        PageData.HtmlHead.Title = NetUtility.TreeUtility.CutWords(this.MetaTags.Find(x => x.Name == "KEYWORDS").Content, 10);
                }


                /*if (docrow.Modify != null)
                {
                    //string data_modify_doc = docrow["modify_doc"].ToString();
                    DateTime dataModifica = docrow.Modify;//DateTime.Parse(data_modify_doc);
                    string data = String.Format("{0:r}", dataModifica);

             
                    
                //    if (!PageData.PageControls.MetaTags.Any(x=> x.HttpEquiv == "last-modified"))
                //        PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta("", data, "last-modified"));
                } */               
            }
        }

        private string[] _Paths;
        private string[] Paths
        {
            get
            {
                if (_Paths == null)
                {
                    _Paths = FrontendNetwork.GetCurrentFolderPathTranslated().Split('/');
                }
                return _Paths;
            }
        }


        public void UpdateDataByFolders()
        {
            string sql = "Stato_FolderMeta = 1 ";
           
            sql += " AND Folder_FolderMeta = " + PageData.Folder.ID;

            DataRow[] rows = NetFrontend.DAL.FileSystemDAL.ListFoldersMetaRecordsBySqlConditions(sql, order: "Path_Folder,id_Meta");

            foreach (DataRow row in rows)
            {
                string name = row["Name_Meta"].ToString();
                string Content_Meta = row["Content_FolderMeta"].ToString();

                //if (PageData.PageControls.Meta.Contains(name))

                if (PageData.PageControls.MetaTags.Any(x=>x.Name.ToLower() == name.ToLower()))
                {
                    //PageData.PageControls.Meta[name].Content = Content_Meta;
                    int index = PageData.PageControls.MetaTags.FindIndex(x => x.Name.ToLower() == name.ToLower());                    
                    PageData.PageControls.MetaTags[index].Content = Content_Meta;                                         
                }
                else
                {                    
                    //Meta.Add(new HtmlMeta { Name = name, Content = Content_Meta });                    
                    PageData.PageControls.MetaTags.Add(new Frontend.Common.CmsHtmlMeta(name,Content_Meta));
                }
            }
        }

    }

}