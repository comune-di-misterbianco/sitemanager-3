//using System;
//using System.Collections.Generic;
//using System.Web;
//using System.Xml;
//using NetCms.Diagnostics;
//using NetCms.Networks;

///// <summary>
///// Summary description for Configs
///// </summary>
//namespace NetFrontend
//{
//    public static class LabelsCache
//    {
//        public const string LabelsCacheKey = "LabelsCacheObj";
//        //private static NetUtility.LabelsCollection labels;
//        //public static NetUtility.LabelsCollection Labels
//        //{
//        //    get
//        //    {
//        //        if (labels == null)
//        //            labels = new NetUtility.LabelsCollection();

//        //        return labels;
//        //    }
//        //}
//        private static IDictionary<int, NetUtility.LabelsCollection> _Labels;
//        public static IDictionary<int, NetUtility.LabelsCollection> Labels
//        {
//            get
//            {
//                if (_Labels == null)
//                {
//                    _Labels = new Dictionary<int, NetUtility.LabelsCollection>();

//                    //Network di base
//                    _Labels.Add(0, new NetUtility.LabelsCollection());

//                    foreach (KeyValuePair<int, string> networkKey in NetworksManager.GlobalIndexer.NetworkKeys)
//                    {
//                        NetUtility.LabelsCollection LabelsForNetwork = new NetUtility.LabelsCollection();
//                        _Labels.Add(networkKey.Key, LabelsForNetwork);
//                    }

//                }

//                return _Labels;
//            }
//        }

//        private static string absoluteConfigsRoot;
//        //public static string AbsoluteConfigsRoot
//        //{
//        //    get { return absoluteConfigsRoot; }
//        //    set { absoluteConfigsRoot = value; }
//        //}
//        public static string AbsoluteConfigsRoot
//        {
//            get 
//            { 
//                if(string.IsNullOrEmpty(absoluteConfigsRoot))
//                {
//                    absoluteConfigsRoot = HttpContext.Current.Application["LabelsPath"].ToString();
//                }
//                return absoluteConfigsRoot; 
//            }
//        }
            
//        //public static NewsletterLabels NewsletterLabels
//        //{
//        //    get
//        //    {
//        //        if (!Labels.Contains(NewsletterLabels.ApplicationKey))
//        //            Labels.Add(new NewsletterLabels(AbsoluteConfigsRoot), NewsletterLabels.ApplicationKey);

//        //        return (NewsletterLabels)Labels[NewsletterLabels.ApplicationKey];
//        //    }
//        //}
//        //public static CommonLabels CommonLabels
//        //{
//        //    get
//        //    {
//        //        //if (!Labels.Contains(CommonLabels.ApplicationKey))
//        //        //    Labels.Add(new CommonLabels(AbsoluteConfigsRoot), CommonLabels.ApplicationKey);

//        //        //return (CommonLabels)Labels[CommonLabels.ApplicationKey];

//        //        //int CurrentNetworkID = NetworksManager.CurrentActiveNetwork != null ? NetworksManager.CurrentActiveNetwork.ID : 0;
//        //        //if (!Labels[CurrentNetworkID].Contains(CommonLabels.ApplicationKey))
//        //        //    Labels[CurrentNetworkID].Add(new CommonLabels(AbsoluteConfigsRoot), CommonLabels.ApplicationKey);

//        //        //return (CommonLabels)Labels[CurrentNetworkID][CommonLabels.ApplicationKey];
//        //        return LabelsManager.LabelsCache.GetTypedLabels(typeof(CommonLabels)) as CommonLabels;

//        //    }
//        //}


//        public static object GetTypedLabels(Type type)
//        {
//            try
//            {
//                var key = type.FullName + "_LabelsCache";
//                //if (!Labels.Contains(key))
//                //{
//                //    var constructor = type.GetConstructor(new[] { typeof(string) });
//                //    var labels = constructor.Invoke(new[] { AbsoluteConfigsRoot }) as NetUtility.Labels;
//                //    Labels.Add(labels, key);
//                //}
//                //return Labels[key];

//                int CurrentNetworkID = NetworksManager.CurrentActiveNetwork != null ? NetworksManager.CurrentActiveNetwork.ID : 0;
//                if (!Labels[CurrentNetworkID].Contains(key))
//                {
//                    var constructor = type.GetConstructor(new[] { typeof(string) });

//                    string LabelsPath = CurrentNetworkID == 0 ? AbsoluteConfigsRoot : AbsoluteConfigsRoot + "\\" + NetworksManager.CurrentActiveNetwork.SystemName;
//                    var labels = constructor.Invoke(new[] { LabelsPath }) as NetUtility.Labels;
//                    Labels[CurrentNetworkID].Add(labels, key);
//                }
//                return Labels[CurrentNetworkID][key];
//            }
//            catch(Exception ex)
//            {
//                Diagnostics.TraceMessage(TraceLevel.Error, ex.Message);
//                return null;
//            }
//        }
//    }

//}