using System;
using System.IO;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Xml;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    public class Script
    {
        //private string scriptPattern = "<script type=\"{0}\" src=\"{1}\"></script>" + System.Environment.NewLine;
        private string scriptPattern = "<script src=\"{0}\"></script>" + System.Environment.NewLine;

        private string cssPattern = "<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\" />"+ System.Environment.NewLine;

        public List<LiteralControl> _Scripts; 
        public List<LiteralControl> Scripts
        {
            get
            { 
                if (_Scripts == null)
                {
                    XmlNodeList oNodeList = NetCms.Configurations.XmlConfig.GetNodes("/Portal/configs/scripts/script");
                    _Scripts = new List<LiteralControl>();

                    string strScript = "";
                    

                    foreach(XmlNode node in oNodeList)
                    {
                        string strSrc = "";

                        string strScriptPath = "";
                        
                        if (!node.Attributes["path"].Value.ToLower().Contains("http") && !node.Attributes["path"].Value.ToLower().Contains("https"))
                        {
                            //NOTA: Ho chiesto ad Angelo informazioni in merito a questo attributo ma non si ricorda, pertanto lo lascio solo per retrocompatibilitÓ o eventuali utilizzi futuri.
                            if (node.Attributes["type"] != null && node.Attributes["type"].Value == "local")
                                strSrc = PageData.AbsoluteWebRoot + "/" + ScriptsFolder + "/";
                            else
                                strSrc = PageData.AbsoluteRoot + "/" + ScriptsFolder + "/";

                            strScriptPath = strSrc + node.Attributes["path"].Value + (node.Attributes["path"].Value.Contains(".js") ? "" : ".js");
                        }
                        else
                            strScriptPath = strSrc + node.Attributes["path"].Value;
                        
                        
                        //strScript = string.Format(scriptPattern, "text/javascript", strScriptPath);
                        strScript = string.Format(scriptPattern, strScriptPath);


                        LiteralControl scriptCtrl = new LiteralControl(strScript);
                        _Scripts.Add(scriptCtrl);

                        //Aggiungo la lettura dei CSS relativi allo script in esame.
                        XmlNodeList cssNodes = null;

                        if (node.ChildNodes != null && node.ChildNodes.Count > 0)
                            cssNodes = node.ChildNodes;

                        List<string> CssScript = GetScriptCss(cssNodes);
                        foreach (string cssPath in CssScript)
                        {
                            string strCssPath = strSrc + cssPath;
                            string strCss = string.Format(cssPattern, strCssPath.Replace("//","/"));
                            LiteralControl cssCtrl = new LiteralControl(strCss);
                            _Scripts.Add(cssCtrl);
                        }
                    }
                }
                return _Scripts;
            }
        }

        private string _ScriptsFolder;
        public string ScriptsFolder
        {
            get
            {
                if (_ScriptsFolder == null)
                {
                    XmlNodeList oNodeList = NetCms.Configurations.XmlConfig.GetNodes("/Portal/configs/scripts");
                    if (oNodeList != null && oNodeList.Count > 0)
                        _ScriptsFolder = oNodeList[0].Attributes["folder"].Value;
                }

                return _ScriptsFolder;
            }
        }
        private PageData PageData;

        private static List<string> GetScriptCss(XmlNodeList cssnodes)
        {
            List<string> cssScript = new List<string>();
            if (cssnodes != null)
            {
                foreach (XmlNode node in cssnodes)
                {
                    string path = node.Attributes["path"].Value;
                    if (!path.ToLower().Contains("http") && !path.ToLower().Contains("https"))
                        cssScript.Add(NetCms.Configurations.Paths.AbsoluteRoot + "/" + node.Attributes["path"].Value);
                    else
                        cssScript.Add(path);
                }
            }
            return cssScript;
        }
        public Script(PageData pagedata)
        {
            PageData = pagedata;
        }
	
    }
}