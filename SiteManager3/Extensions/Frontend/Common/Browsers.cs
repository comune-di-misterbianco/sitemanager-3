using System;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Xml;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend.Browsers
{
    public class Browsers
    {
        public static Browser FabricateBrowser(PageData PageData)
        {
            return FabricateBrowser(HttpContext.Current.Request.Browser, PageData);
        }
        public static Browser FabricateBrowser(HttpBrowserCapabilities browserData, PageData PageData)
        {
            switch (browserData.Browser)
            {
                case "Firefox": return new Browser(Browser.Types.Firefox, browserData.MajorVersion, browserData.Version, PageData);
                case "IE": return new Browser(Browser.Types.IE, browserData.MajorVersion, browserData.Version, PageData);
                case "AppleMAC-Safari": return new Browser(Browser.Types.Safari, browserData.MajorVersion, browserData.Version, PageData);
                case "Opera": return new Browser(Browser.Types.Opera, browserData.MajorVersion, browserData.Version, PageData);
                default: return new Browser(Browser.Types.Unknown, browserData.MajorVersion, browserData.Version, PageData);
            }
        }
    }
    public class Browser
    {
        private XmlNode xmlBrowsersConfigs;
        public XmlNode XmlBrowsersConfigs
        {
            get
            {
                if (xmlBrowsersConfigs == null)
                {
                    XmlDocument oXmlDoc = new XmlDocument();
                    try
                    {
                        string xmlFilePath = HttpContext.Current.Server.MapPath(PageData.AbsoluteRoot + "/configs/" + PageData.Network.SystemName.ToLower() + "/browsers.xml");
                        oXmlDoc.Load(xmlFilePath);
                    }
                    catch (Exception ex) { }

                    xmlBrowsersConfigs = oXmlDoc.DocumentElement;
                }
                return xmlBrowsersConfigs;
            }
        }

        public enum Types
        {
            IE, Opera, Safari, Firefox, Unknown
        }
        private Types type;
        public Types Type
        {
            get { return type; }
        }

        private int majorVersion;
        public int MajorVersion
        {
            get { return majorVersion; }
        }

        private string version;
        public string Version
        {
            get { return version; }
        }
        private PageData PageData;

        public bool HasData
        {
            get { return XmlBrowsersConfigs != null; }
        }

        public Browser(Browser.Types type, int majorVersion, string version, PageData PageData)
        {
            this.type = type;
            this.majorVersion = majorVersion;
            this.version = version;
            this.PageData = PageData;
        }

        private HtmlLink[] css;
        public HtmlLink[] CSS
        {
            get
            {
                if (css == null)
                {
                    string path = "/configs/browsers/" + this.Type.ToString().ToLower() + "/version" + this.MajorVersion + "/css";
                    XmlNodeList oNodeListThisVersion = this.XmlBrowsersConfigs.SelectNodes(path);
                    path = "/configs/browsers/" + this.Type.ToString().ToLower() + "/versionall/css";
                    XmlNodeList oNodeListAllVersion = this.XmlBrowsersConfigs.SelectNodes(path);

                    css = new HtmlLink[oNodeListThisVersion.Count + oNodeListAllVersion.Count];

                    int i = 0;
                    foreach (XmlNode node in oNodeListThisVersion)
                    {
                        HtmlLink link = new HtmlLink();
                        link.Attributes["type"] = "text/css";
                        link.Attributes["media"] = "screen";
                        link.Attributes["rel"] = "stylesheet";
                        link.Attributes["href"] = PageData.AbsoluteRoot + "/css/" + PageData.Network.CssFolder + "/browsers/" + this.Type.ToString().ToLower() + "/version" + this.MajorVersion + "/" + node.InnerXml;
                        css[i++] = link;
                    }
                    foreach (XmlNode node in oNodeListAllVersion)
                    {
                        HtmlLink link = new HtmlLink();
                        link.Attributes["type"] = "text/css";
                        link.Attributes["media"] = "screen";
                        link.Attributes["rel"] = "stylesheet";
                        link.Attributes["href"] = PageData.AbsoluteRoot + "/css/" + PageData.Network.CssFolder + "/browsers/" + this.Type.ToString().ToLower() + "/versionall/" + node.InnerXml;
                        css[i++] = link;
                    }
                }
                return css;
            }
        }

        private HtmlGenericControl[] scripts;
        public HtmlGenericControl[] Scripts
        {
            get
            {
                if (scripts == null)
                {
                    string path = "/configs/browsers/" + this.Type.ToString().ToLower() + "/version" + this.MajorVersion + "/script";
                    XmlNodeList oNodeListThisVersion = this.XmlBrowsersConfigs.SelectNodes(path);
                    path = "/configs/browsers/" + this.Type.ToString().ToLower() + "/versionall/script";
                    XmlNodeList oNodeListAllVersion = this.XmlBrowsersConfigs.SelectNodes(path);

                    scripts = new HtmlGenericControl[oNodeListThisVersion.Count + oNodeListAllVersion.Count];

                    int i = 0;
                    foreach (XmlNode node in oNodeListThisVersion)
                    {
                        HtmlGenericControl script = new HtmlGenericControl("script");
                        script.Attributes["type"] = "text/javascript";
                        script.Attributes["src"] += PageData.AbsoluteRoot + "/scripts/" + PageData.Network.CssFolder + "/browsers/" + this.Type.ToString().ToLower() + "/version" + this.MajorVersion;
                        script.Attributes["src"] += "/" + (node.InnerXml.Contains(".js") ? node.InnerXml : node.InnerXml + ".js");

                        Scripts[i++] = script;

                    }
                    foreach (XmlNode node in oNodeListAllVersion)
                    {
                        HtmlGenericControl script = new HtmlGenericControl("script");
                        script.Attributes["type"] = "text/javascript";
                        script.Attributes["src"] += PageData.AbsoluteRoot + "/scripts/" + PageData.Network.CssFolder + "/browsers/" + this.Type.ToString().ToLower() + "/versionall";
                        script.Attributes["src"] += "/" + (node.InnerXml.Contains(".js") ? node.InnerXml : node.InnerXml + ".js");

                        Scripts[i++] = script;
                    }

                }
                return scripts;
            }
        }
    }
}