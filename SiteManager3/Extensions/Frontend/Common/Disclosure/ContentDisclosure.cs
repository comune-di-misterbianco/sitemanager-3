﻿using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Frontend.Common.Disclosure
{
    public static class ContentDisclosure
    {
        public static WebControl GetControl(int idDoc, int idDocContent)
        {
            WebControl disclosure = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
            disclosure.CssClass = "disclosure";

            Document doc = DocumentBusinessLogic.GetDocumentRecordById(idDoc);

            if (doc != null)
            {
                if (!string.IsNullOrEmpty(doc.Tags))
                {
                    WebControl tagCtrl = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
                    tagCtrl.CssClass = "tags";

                    char[] separator = { ',' };
                    string[] tags = doc.Tags.Split(separator, StringSplitOptions.RemoveEmptyEntries);

                    string strTag = @"<span class=""labeltag"">Tags : </span>";

                    if (tags.Length > 0)
                    {
                        strTag += @"<ul>";
                        foreach (string tag in tags)
                        {
                            strTag += @"
                                    <li><a href=""/tags/argomenti.aspx?tag=" + tag + @""">" + tag + @"</a></li>
                                ";
                        }
                        strTag += @"</ul>";
                    }
                    tagCtrl.Controls.Add(new LiteralControl(strTag));

                    disclosure.Controls.Add(tagCtrl);
                }
            }


            return disclosure;
        }
    }
}
