using System.Web;
using System.Web.UI.HtmlControls;
using NetCms.Front.Static;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    [FrontendStaticPage(new[] { "/mappa-del-portale/default.aspx" , "/en/portalmap/default.aspx"}, new[] { "Mappa del Portale", "Portal map" }, NetCms.Front.FrontLayouts.RightFooter)]
    public class SiteMap : WebControl
    {
        public const string ReloadSiteMap = "ReloadSiteMap";

        private bool ValidCache
        {
            get
            {
                if (PageData.Application[ReloadSiteMap] == null)
                    return true;
                else
                {
                    PageData.Application[ReloadSiteMap] = null;
                    return false;
                }

            }
        }

        private PageData PageData;

        private FrontendFolder _RootFolder;
        public FrontendFolder RootFolder
        {
            get
            {
                if (_RootFolder == null)
                {
                    _RootFolder = PageData.Network.RootFolder;
                }
                return _RootFolder;
            }
        }

        public FrontendFolder StructureRootFolder
        {
            get
            {
                return RootFolder;
            }
        }

        public SiteMap(PageData pagedata)
            : base(System.Web.UI.HtmlTextWriterTag.Div)
        {
            HttpContext.Current.Items["AccountNotNecessary"] = true;
            PageData = pagedata;
        }

        protected override void OnPreRender(System.EventArgs e)
        {
            base.OnPreRender(e);

            string AppKey = "SiteMap_Control_" + PageData.Network.ID;
            BuildSiteMap();
        }
        private void BuildSiteMap()
        {
            WebControl Output = this;
            Output.Attributes["class"] = "SiteMap u-text-r-xl u-padding-r-bottom  ";

            HtmlGenericControl node = new HtmlGenericControl("li");
            node.Attributes["class"] = "SiteMap_Root";

            HtmlGenericControl content = new HtmlGenericControl("span");
            content.ID = "MapRoot";
            content.InnerHtml = "<a href=\"" + (PageData.AbsoluteWebRoot.Length > 0 ? PageData.AbsoluteWebRoot : "/") + "\">";
            content.InnerHtml += PageData.Network.Label;
            content.InnerHtml += "</a>";
            node.Controls.Add(content);

            Output.Controls.Add(node);

            int count = 0;


            if (StructureRootFolder.HasSubFolders)
            {
                HtmlGenericControl list = new HtmlGenericControl("ul");

                List<FrontendFolder> foldersToAdd = StructureRootFolder.SubFolders.Where(x => x.Type != 4 && x.StateValue < 1 && !x.Restricted && !x.IsSystemFolder).AsParallel().ToList();

                foreach (FrontendFolder folder in foldersToAdd)
                    list.Controls.Add(FoldersTreeList_Hierarchy(folder, (++count).ToString()));

                if (foldersToAdd.Count > 0)
                    node.Controls.Add(list);
            }
        }

        private HtmlGenericControl FoldersTreeList_RestrictedAreas(FrontendFolder folder, string numbers)
        {
            HtmlGenericControl node = new HtmlGenericControl("li");
            node.Attributes["class"] = "folder " + folder.TypeName.ToLower().Replace(" ", "_");

            HtmlGenericControl content = new HtmlGenericControl("a");
            content.Attributes["href"] = PageData.AbsoluteWebRoot + folder.Path;
            content.Attributes["title"] = folder.Descrizione;
            content.InnerHtml = "<span class=\"SiteMap_Numbers\">" + numbers + "</span> - " + folder.Label;
            if (folder.EnableRss)
                content.InnerHtml += "<a href=\"" + PageData.AbsoluteWebRoot + folder.Path + "/rss.xml\" class=\"SiteMap_Rss\"><span> - Rss della Sezione</span></a>";
            node.Controls.Add(content);


            HtmlGenericControl sublist = new HtmlGenericControl("ul");

            int list = 0; ;

            if (folder.HasSubFolders)
                for (int i = 0; i < folder.SubFolders.Count; i++)
                    if (folder.SubFolders[i].StateValue < 1 && !folder.SubFolders[i].IsSystemFolder)
                        sublist.Controls.Add(FoldersTreeList_RestrictedAreas(folder.SubFolders[i], numbers + "." + (++list)));

            if (folder.HasDocuments && (folder.Type == 2 || folder.Type == 9))
                for (int i = 0; i < folder.Documents.Count; i++)
                    if (folder.Documents[i].Stato < 1)
                        sublist.Controls.Add(FormatDocument_RestrictedAreas(folder.Documents[i], folder, numbers + "." + (++list)));

            if (sublist.Controls.Count > 0)
                node.Controls.Add(sublist);

            return node;
        }
        private HtmlGenericControl FoldersTreeList_Hierarchy(FrontendFolder folder, string numbers)
        {
            HtmlGenericControl node = new HtmlGenericControl("li");
            node.Attributes["class"] = "folder " + folder.TypeName.ToLower().Replace(" ", "_");

            HtmlGenericControl content = new HtmlGenericControl("a");
            content.Attributes["href"] = PageData.AbsoluteWebRoot + folder.Path;
            content.Attributes["title"] = folder.Descrizione;
            content.InnerHtml = "<span class=\"SiteMap_Numbers\">" + numbers + "</span> - " + folder.Label;
            node.Controls.Add(content);


            HtmlGenericControl sublist = new HtmlGenericControl("ul");

            int list = 0; ;

            if (folder.HasSubFolders)
            {
                List<FrontendFolder> foldersToAdd = folder.SubFolders.Where(x => x.Depth <= 3 && x.Type != 4 && x.StateValue < 1 && !x.Restricted && !x.IsSystemFolder).AsParallel().ToList();

                foreach (FrontendFolder folder2Add in foldersToAdd)
                    sublist.Controls.Add(FoldersTreeList_Hierarchy(folder2Add, numbers + "." + (++list)));

                if (foldersToAdd.Count > 0)
                    node.Controls.Add(sublist);
            }
            return node;
        }
        private HtmlGenericControl FormatDocument_RestrictedAreas(FrontendDocument doc, FrontendFolder folder, string numbers)
        {
            HtmlGenericControl node = new HtmlGenericControl("li");
            node.Attributes["class"] = "doc " + folder.TypeName.ToLower().Replace(" ", "_");

            HtmlGenericControl content = new HtmlGenericControl("a");
            content.Attributes["href"] = PageData.AbsoluteWebRoot + folder.Path + "/" + doc.Name;
            content.Attributes["title"] = doc.Descrizione;
            content.InnerHtml += "<span class=\"SiteMap_Numbers\">" + numbers + "</span> - " + doc.Label;
            node.Controls.Add(content);

            return node;
        }
        private HtmlGenericControl FormatDocument_Hierarchy(FrontendDocument doc, FrontendFolder folder, string numbers)
        {
            HtmlGenericControl node = new HtmlGenericControl("li");
            node.Attributes["class"] = "doc " + folder.TypeName.ToLower().Replace(" ", "_");

            HtmlGenericControl content = new HtmlGenericControl("a");
            content.Attributes["href"] = PageData.AbsoluteWebRoot + folder.Path + "/" + doc.Name;
            content.Attributes["title"] = doc.Descrizione;
            content.InnerHtml += "<span class=\"SiteMap_Numbers\">" + numbers + "</span> - " + doc.Label;
            node.Controls.Add(content);

            return node;
        }
    }

}