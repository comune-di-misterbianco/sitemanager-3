using System;
using System.IO;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Xml;
using LanguageManager.BusinessLogic;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    public class WhereBlock
    {
        private string _Label;
        public string Label
        {
            get { return _Label; }
            set
            {
                _Label = value;
                FillControl();
            }
        }

        private string _Link;
        public string Link
        {
            get { return _Link; }
            set
            {
                _Link = value;
                FillControl();
            }
        }

        private HtmlGenericControl _Control;
        public HtmlGenericControl Control
        {
            get 
            {
                if (_Control == null)
                {
                    //_Control = new HtmlGenericControl("span");
                    //_Control.Attributes["class"] = "WherePart";
                    _Control = new HtmlGenericControl("li");

                    FillControl();
                }
                return _Control; 
            }
        }

        private bool _First;
        public bool First
        {
            get { return _First; }
            set
            {
                _First = value;
                FillControl();
            }
        }

        private bool _Last;
        public bool Last
        {
            get { return _Last; }
            set
            {
                _Last = value;
                FillControl();
            }
        }

        private void FillControl()
        {
            Control.InnerHtml = "";

            Control.InnerHtml += Label;            

            if (Last)
                //Control.InnerHtml = AddStrong(Control.InnerHtml);
                Control.Attributes.Add("class", "active");
            else
                if (_Link != null && _Link.Length > 0)
                    Control.InnerHtml = AddLink(Control.InnerHtml, Link);
            
            //if (!First)
            //    Control.InnerHtml = " � " + Control.InnerHtml;
        }
	
        public WhereBlock(string label, string link)
        {
            _Label = label;

            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                if (LanguageBusinessLogic.CurrentLanguage.Default_Lang > 0)
                    _Link = link;
                else
                    _Link = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + link + (link.EndsWith("/") ? "default.aspx" : (!link.Contains(".aspx") ? "/default.aspx" : ""));
            }
            else
                _Link = link;

        }
        public WhereBlock(string label)
        {
            _Label = label;
        }
        public WhereBlock()
        {
        }

        private string AddLink(string str, string href)
        {
            string value = null;
            if (Label == "Applicazioni")
                href = NetCms.Front.FrontendNetwork.GetAbsoluteWebRoot() + "/user/availableServices.aspx";
            
            if(NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.ContainsKey(Label.ToLower()))
            {
                var net = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks[Label.ToLower()];
                
                if (net != null && net.CmsStatus != NetCms.Networks.NetworkCmsStates.Enabled)
                {
                    //value = "<span>";
                    //value += net.Label;
                    //value += "</span>";     
                    value = "<li>";
                    value += net.Label;
                    value += "</li>";
                }
            }
            if(string.IsNullOrEmpty(value))
            {
                value = "<a href=\"" + href + "\">";
                value += str;
                value += "</a>";
            }
            return value;
        }
        private string AddStrong(string str)
        {
            string value = str;
            value = "<strong>";
            value += str;
            value += "</strong>";

            return value;
        }
    }
}