using System;
using System.Data;
using System.Configuration;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>

namespace NetFrontend
{
    public class WhereBlockCollection
    {
        private NetCollection coll;
        public int Count
        {
            get
            {
                return coll.Count;
            }
        }
        private HtmlGenericControl Where;
        private PageData PageData;

        public WhereBlockCollection(HtmlGenericControl where,PageData pagedata)
        {
            PageData = pagedata;
            this.Where = where;
            coll = new NetCollection();
        }

        public void Add(WhereBlock block)
        {
            if (Count == 0)
                block.First = true;
            else
                this[this.Count-1].Last = false;

            block.Last = true;
            coll.Add("Block" + Count, block);

            if(PageData.Where!=null)
                PageData.Where.Title = block.Label + " � " + PageData.Where.Title;

            if (coll.Count > 1)
            {
                if (coll.Count == 2)
                    Where.Controls.Add(this[0].Control);

                Where.Controls.Add(block.Control);
            }
        }

        public void Remove(int index)
        {
            PageData.Where.Title = PageData.Where.Title.Replace(this[index].Label + " � ","");
            coll.Remove(index);
        }

        public void Clear()
        {
            coll.Clear();
        }

        public WhereBlock this[int i]
        {
            get
            {
                WhereBlock str = (WhereBlock)coll[coll.Keys[i]];
                return str;
            }
        }
        public WhereBlock this[string str]
        {
            get
            {
                WhereBlock val = (WhereBlock)coll[str];
                return val;
            }
        }


        public bool Contains(string str)
        {
            return coll[str] != null;
        }

    }
}