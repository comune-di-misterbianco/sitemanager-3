using System;
using System.IO;
using System.Linq;
using System.Data;
using System.Web.UI.HtmlControls;
using NetCms.Vertical;
using NetCms.Front.Static;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Collections.Generic;
using NetCms.Structure.WebFS;
using LabelsManager;
using LanguageManager.BusinessLogic;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    public class Where
    {
        private PageData PageData;

        private WhereBlockCollection _Blocks;
        public WhereBlockCollection Blocks
        {
            get
            {
                if (_Blocks == null)
                {
                    _Blocks = new WhereBlockCollection(WhereControl,PageData);
                }
                return _Blocks;
            }
        }

        private string _Title;
        public string Title
        {
            get
            {
                return _Title;
            }
            set
            {
                _Title = value;
                //PageData.HtmlHead.Title = value;
            }
        }

        private HtmlGenericControl WhereControl;

        public Where(HtmlGenericControl wherecontrol, PageData pagedata)
        {
            WhereControl = wherecontrol;
            PageData = pagedata;
            
            Title = pagedata.Network.Label;        
            
            InitWhere();            
        }
        
        private void InitWhere()
        {
            NetCms.Vertical.VerticalApplication vapp = VerticalApplicationsPool.GetCurrentActiveApplication();
            if (vapp != null)
            {
                Console.WriteLine("init vertical app breadcrumbs");
                InitWhereVertical(vapp);
            }
            else
                if (StaticPagesCollector.HasCurrentStaticPage)
                InitWhereStaticPage(StaticPagesCollector.CurrentStaticPage.Attribute);
            else InitWhereCms();
        }

        private string BreadCrumStartLabel 
        {
            get 
            {
                return string.IsNullOrEmpty(NetCms.Configurations.PortalData.BreadCrumStartLabel) ? PageData.Network.Label : NetCms.Configurations.PortalData.BreadCrumStartLabel;
            }
        }

        private void InitWhereCms()
        {
            string Path = PageData.FolderPath.Replace("'", "''").ToLower();

            string[] PathArray = Path.Split('/');

            string previousTranslated = "";
            for (int i = 1; i < PathArray.Length; i++)
            {
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang == 0)
                {
                    previousTranslated = FolderBusinessLogic.TranslateNameOnTheFly(LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.ID, PageData.Network.ID, PathArray[i], i + 1, previousTranslated.Replace("/", ""));
                    PathArray[i] = PathArray[i - 1] + previousTranslated;
                }
                else
                    PathArray[i] = PathArray[i - 1] + "/" + PathArray[i];
            }

            WhereBlock part = new WhereBlock(BreadCrumStartLabel, PageData.AbsoluteWebRoot.Length > 0 ? PageData.AbsoluteWebRoot : "/");
            this.Blocks.Add(part);

            //string sql = "Path_Folder LIKE '" + String.Join("' OR Path_Folder LIKE '", PathArray) + "'";


            IList<StructureFolder> foldersRows = FolderBusinessLogic.ListFoldersRecordsByPaths(PathArray);
            //DataRow[] foldersRows = NetFrontend.DAL.FileSystemDAL.ListFoldersRecordsBySqlConditions(sql);
            if (foldersRows != null)
            {
                foreach (string path in PathArray)
                {
                    if (path.Length > 0)
                    {
                        string Label = string.Empty;
                        StructureFolder row = foldersRows.Where(x => x.Path.ToLower() == path).FirstOrDefault();                        
                        string pathToUse;
                        if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang == 0)
                        {
                            pathToUse = (row != null ? row.TranslatedPath : path);
                            Label = ((row != null && row.CurrentFolderLang != null) ? row.CurrentFolderLang.Label : "");
                        }
                        else
                            pathToUse = path;

                        if (string.IsNullOrEmpty(Label))
                        {
                            if (row != null)
                                Label = row.Label;
                            else
                                Label = (pathToUse.Substring(0, 1).ToUpper() + pathToUse.Substring(1, pathToUse.Length - 1).ToLower()).Replace("_", " ");
                        }
                        part = new WhereBlock(Label, PageData.AbsoluteWebRoot + pathToUse + "/");
                        this.Blocks.Add(part);

                        Title = Label + (" � " + Title);
                    }
                }
            }
        }

        //ATTENZIONE!!! ALTAMENTE PROBABILE RALLENTAMENTO RILEVATO DURANTE L'ANALISI DEL SEGUENTE CODICE
        //Questo metodo, poich� richiama il FrontAdapter che istanzia la classe della pagina per recuperare il PageTitle, provoca una duplice
        //invocazione del costruttore della classe della pagina che viene poi istanziata in seguito per renderizzare il contenuto.
        private void InitWhereVertical(NetCms.Vertical.VerticalApplication vapp )
        {
            WhereBlock part = new WhereBlock(BreadCrumStartLabel, PageData.AbsoluteWebRoot.Length > 0 ? PageData.AbsoluteWebRoot : "/");
            this.Blocks.Add(part);

            var adapter = vapp.GetFrontAdapter(PageData.Network.CacheKey, PageData.ViewState, PageData.IsPostBack);

            if (adapter != null)
            {
                string title = adapter.ApplicationPage.PageTitle;
                part = new WhereBlock(title);

                if (PageData.Network != null)
                    PageData.HtmlHead.Title = PageData.Network.Label + " - " + title;
                else
                    PageData.HtmlHead.Title = title;
            }
            this.Blocks.Add(part);

        }

        private void InitWhereStaticPage(FrontendStaticPage pageAttribute)
        {
            WhereBlock part = new WhereBlock(BreadCrumStartLabel, PageData.AbsoluteWebRoot.Length > 0 ? PageData.AbsoluteWebRoot : "/");
            this.Blocks.Add(part);

            if (pageAttribute.WhereParts != null && pageAttribute.WhereParts.Count > 0)
            {
                foreach (var entry in pageAttribute.WhereParts)
                {
                    part = string.IsNullOrEmpty(entry.Value) ? new WhereBlock(entry.Key, entry.Value) : new WhereBlock(entry.Key);
                    this.Blocks.Add(part);
                    Title = entry.Key + (" � " + Title);
                }
            }
            else
            {
                string titleLabel = string.Empty;
                if (pageAttribute.PageTitles.Length == 1)
                    titleLabel = pageAttribute.PageTitles[0];
                else
                { 
                    int parentLang = (PageData.CurrentConfig != null && PageData.CurrentConfig.CurrentLanguage != null )
                        ?
                        PageData.CurrentConfig.CurrentLanguage.Parent_Lang
                        : -1;
                    if (parentLang == -1)
                        titleLabel = pageAttribute.PageTitles.FirstOrDefault();
                    else
                    {
                        if(pageAttribute.PageTitles.Length -1 < parentLang)
                            titleLabel = pageAttribute.PageTitles.FirstOrDefault();
                        else
                            titleLabel = pageAttribute.PageTitles[parentLang];
                    }
                }
               

                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageBusinessLogic.CurrentLanguage.ID != LanguageBusinessLogic.GetDefaultLanguage().ID)
                {
                    if (!string.IsNullOrEmpty(pageAttribute.PageTitleLabel.ToString()) && pageAttribute.PageTitleLabel != FrontendStaticPage.StaticPagesLabelsList.None)
                        titleLabel = Labels[pageAttribute.PageTitleLabel];
                }
                part = new WhereBlock(titleLabel);
                this.Blocks.Add(part);
                Title = titleLabel + (" � " + Title);
            }
        }

        #region Labels
        private LabelsManager.Labels Labels
        {
            get
            {
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(CommonLabels)) as Labels;
            }
        }
        #endregion

        public void FormatWhere()
        {
            WhereControl.Controls.Clear();
            _Blocks = null;
            this.Title = PageData.Network.Label;
            WhereBlock part = new WhereBlock(BreadCrumStartLabel, PageData.AbsoluteWebRoot.Length > 0 ? PageData.AbsoluteWebRoot : "/");
            this.Blocks.Add(part);
        }

        public void BuildTitle()
        {
            if (PageData.HtmlHead!= null)
            {
                if (PageData.CurrentVerticalApplication == null)                
                {
                    if (PageData.Folder != null)
                        if (PageData.Folder.TitleOffset == 0)
                            if (!string.IsNullOrEmpty(PageData.Folder.Label))
                                PageData.HtmlHead.Title = PageData.Folder.Label;
                            else
                                PageData.HtmlHead.Title = Title;
                        else if (PageData.Folder.TitleOffset == -1)
                            PageData.HtmlHead.Title = Title;
                }
            }
        }
    }
}