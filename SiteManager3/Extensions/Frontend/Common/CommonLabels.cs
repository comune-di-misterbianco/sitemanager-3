using System;
using System.Web;
using System.Xml;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    public class CommonLabels : LabelsManager.Labels
    {
        
        public const String ApplicationKey = "common";
        
        public CommonLabels()
            : base(ApplicationKey)
        {
        }

        public enum CommonLabelsList
        {
            RichiestaDiChiarimento,
            Oggetto,
            DigitateTesto,
            RichiestaSpedita,
            RichiestaRiguardo,
            ChiContattare,
            Nome,
            Cognome,
            Email,
            RecapitoTelefonico,
            Fax,
            ContattoSpedito,
            DigitateTestoContatto,
            Contattaci,
            TestoCampiObbligatori,
            Invia,
            StoricoOperazioniPersonali,
            RelativoA,
            Dettagli,
            DataOra,
            NessunaAttivita,
            NonSpecificata,
            MappaDelSito,
            SottoscrizioneAggiornamenti,
            TestoSottoscrizione,
            SottoscrizioneCompletata,
            VaiAGestioneAccount,
            Gi�Sottoscritto,
            CancellaSottoscrizione,
            SottoscrizioneRimossa,
            Sottoscrivi,
            VuoiSottoscrivere,
            ServonoChiarimenti,
            DisclaimerContacts,
            CondizioniServizio,
            WellcomeMsg,
            LoginLabelLink,
            ImpostazioniAccount,
            ProfiloLabelLink,
            EditProfiloLabelLink,
            EditPasswordLabelLink,
            ChiarimentiLabelLink,
            NotificheLabelLink,
            LogoutLabelLink,
            RegistatiLabelLink,
            TestoAlternativoModuloLogin,
            ServicesModuleTitle,
            NoServiceForUserMessage,            
            //error page
            BackLinktitle,
            Backlinklabel,
            Status400,
            Errortitle400,
            Errormessage400,
            Status401,
            Errortitle401,
            Errormessage401,
            Status403,
            Errortitle403,
            Errormessage403,
            Status404,
            Errortitle404,
            Errormessage404,
            Status500,
            Errortitle500,
            Errormessage500,
            LabelPublishedOn,
            LabelLastRevision,
            LabelStampa,
            TitleStampaLink,
            LabelShareButton,
            LabelCloseButton,
            TitleShareClosePanel,
            TitleShareFB,
            TitleShareTW,
            TitleShareWA,
            LabelAccediSezione,
            PublishedOn,
            LastRevision,
            Page,
            PageOf,
            NextPage,
            PrevPage,
            FirstPage,
            LastPage,
            tagstitle,
            readmore,
            moreinfo,
            NoRecord,
            TimeToRead,
            //viewactions
            ActionsTitle,
            Print,
            PrintTitle,
            Send,
            Download,
            //Zoomsearch
            SearchPageTitle,
            NoSearchServiceMessage,
            LabelTermSearch,
            SearchButtonText,
            NoFileOrFolderMsg,
            PermDeniedMsg,
            //Allegati Vari
            AttachedDocumentsTitle
        }

        //public string this[CommonLabelsList label]
        //{
        //    get
        //    {
        //        if (this.LabelsCache.Contains(label.ToString()))
        //        {
        //            return this.FormatLabel(this.LabelsCache[label.ToString()]);
        //        }
        //        else
        //        {
        //            XmlNodeList oNodeList = this.XmlLabelsRoot.SelectNodes("/labels/common/"+label.ToString());
        //            if (oNodeList != null && oNodeList.Count > 0)
        //            {
        //                string value = oNodeList[0].InnerXml;
        //                LabelsCache.Add(value, label.ToString());
        //                return this.FormatLabel(value);
        //            }
        //        }
        //        return null;
        //    }
        //}
    }

}