﻿using LoginManager.Controls;
using NetCms.Front;
using NetCms.Networks;
using NetCms.Users;
using NetFrontend;
using NHibernate;
using SharedUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.UI;
using System.Xml;

namespace Frontend.Controllers
{
    public class FrontController : ApiController
    {
        private FrontendNetwork Network
        {
            get;
            set;
        }

        private CmsConfigs.Model.Config CurrentConfig
        {
            get
            {
                return CmsConfigs.Model.GlobalConfig.CurrentConfig;
            }
        }

        private LanguageManager.Model.Language _Language;
        private XmlNode xmlData;
        private XmlNode XmlData
        {
            get
            {
                #region MULTI LANG
                if (_Language == null)
                {
                    _Language = LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage;
                }
                string partsName = "";
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang != 1)
                {
                    partsName = "\\";
                    partsName += LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.CultureCode_Lang;
                    partsName += "_parts.xml";

                }
                else
                    partsName = "\\parts.xml";
                #endregion

                if (
                    xmlData == null ||
                    (xmlData != null && _Language != null && _Language.Default_Lang != LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang)
                )
                {
                    _Language = LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage;

                    XmlDocument oXmlDoc = new XmlDocument();
                    try
                    {
                        string xmlFilePath = string.Empty;

                        if (NetCms.Configurations.ThemeEngine.Status == NetCms.Configurations.ThemeEngine.ThemeEngineStatus.on)
                            xmlFilePath = NetCms.Configurations.Paths.AbsoluteConfigRoot + "\\" + this.Network.SystemName.ToLower() + "\\" + CurrentConfig.Theme + partsName;
                        else
                            xmlFilePath = NetCms.Configurations.Paths.AbsoluteConfigRoot + "\\" + this.Network.SystemName.ToLower() + partsName;

                        oXmlDoc.Load(xmlFilePath);
                        xmlData = oXmlDoc.DocumentElement;
                    }
                    catch (Exception ex)
                    {
                        if (NetCms.Configurations.Debug.GenericEnabled)
                        {
                            try
                            {
                                string xmlFilePath = NetCms.Configurations.Paths.AbsoluteConfigRoot + "\\system" + partsName;
                                oXmlDoc.Load(xmlFilePath);
                                xmlData = oXmlDoc.DocumentElement;
                            }
                            catch { }
                        }
                        else
                        {
                            throw new NetCms.Exceptions.ServerError500Exception("Impossibile trovare il file di configurazione '" + partsName +"' del portale '" + this.Network.SystemName.ToLower() + "'");
                        }
                    }
                }
                return xmlData;
            }
        }

        //[HttpGet]
        //[AcceptVerbs("GET")]
        //[ActionName("parts")]
        //public IHttpActionResult GetParts(string network)
        //{
        //    string footer = "";

        //    Network = new NetCms.Front.FrontendNetwork(network);
        //    if (Network != null)
        //    {
        //        // todo: aggiungere ai vari link il dominio e il protocollo completo se non presente 

        //        XmlNodeList oNodeList = XmlData.SelectNodes("/Portal/footer");
        //        if (oNodeList != null && oNodeList.Count > 0)
        //        {
        //            PartsFilters filter = new PartsFilters(Network);
        //            footer = filter.FilterTags(oNodeList[0].InnerXml);
        //        }

        //        return Ok(footer);
        //    }

        //    return BadRequest();

        //}

        [HttpGet]
        [AcceptVerbs("GET")]
        [ActionName("parts")]
        [NetCms.Users.Filters.AuthorizationRoleFilterAttribute(Roles = "Utenti frontend,Utenti ente")]
        public IHttpActionResult GetParts(string network)
        {
            FrontPartsDTO frontParts = new FrontPartsDTO();

            string strHead = "";
            string footer = "";

            string strMobileMenu = string.Empty;
            string strTopMenu = string.Empty;
            string cssClassForRootElement = string.Empty;
            User currentUser = null;
           
            string absoluteUri = System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority;

            using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                
                int userId = GetUserId(); // get id from claims

                if (userId <= 0)
                    return BadRequest();
                else
                {
                    currentUser = GetUser(userId, sess);
                    //currentUser.ExternalSession = sess;
                }

                Network = new NetCms.Front.FrontendNetwork(network);
                if (Network != null)
                {
                    #region intestazione                

                    if (CurrentConfig.TopMenuState == CmsConfigs.Model.Config.State.On)
                    {
                        if (NetCms.Configurations.ThemeEngine.Status == NetCms.Configurations.ThemeEngine.ThemeEngineStatus.on)
                        {
                            if (CurrentConfig.Theme.ToLower() == "AGID".ToLower() || CurrentConfig.CurrentTheme.InheritFrom.ToLower() == "AGID".ToLower())
                                cssClassForRootElement = "Linklist Linklist--padded Treeview Treeview--default js-Treeview";
                            else if (CurrentConfig.Theme.ToLower() == "AGID2019".ToLower() || CurrentConfig.CurrentTheme.InheritFrom.ToLower() == "AGID2019".ToLower())
                                cssClassForRootElement = "navbar-nav";

                            if ((CurrentConfig.Theme.ToLower() != "AGID".ToLower() && CurrentConfig.Theme.ToLower() != "AGID2019".ToLower()) &&
                                (CurrentConfig.CurrentTheme.InheritFrom.ToLower() != "AGID".ToLower() && CurrentConfig.CurrentTheme.InheritFrom.ToLower() != "AGID2019".ToLower()))
                                strTopMenu = BuildMenu(absoluteUri,cssClassForRootElement);
                            else
                                strMobileMenu = BuildMenu(absoluteUri,cssClassForRootElement);
                        }
                    }

                    

                    string strCodeLogin = string.Empty;

                    if (!NetCms.Configurations.Generics.LoginManager)
                        strCodeLogin = "";
                    else
                        strCodeLogin = BuildLoginControl(currentUser, absoluteUri);

                    string headerHtml = string.Empty;

                    XmlNodeList oNodeList = XmlData.SelectNodes("/Portal/intestazione");
                    if (oNodeList != null && oNodeList.Count > 0)
                    {
                        PartsFilters filter = new PartsFilters(Network);
                        headerHtml = filter.FilterTags(oNodeList[0].InnerXml);
                    }                              

                    strHead = headerHtml.Replace("%%TopMenuElement%%", strTopMenu)
                                        .Replace("%%MobileMenu%%", strMobileMenu)
                                        .Replace("%%LoginControl%%", strCodeLogin)
                                        .Replace("%%CoverIMG%%", "");

                    strHead = strHead.Replace("\r\n", "").Replace("\t", "");
           
                    frontParts.Head = strHead;
                    #endregion

                    #region footer
                oNodeList = XmlData.SelectNodes("/Portal/footer");
                if (oNodeList != null && oNodeList.Count > 0)
                {
                    PartsFilters filter = new PartsFilters(Network);
                    footer = filter.FilterTags(oNodeList[0].InnerXml);
                }
                frontParts.Footer = footer; 
                #endregion

                    return Ok(frontParts);
                }
            }
            return BadRequest();
        }

        protected int GetUserId()
        {
            var claimsIdentity = (System.Security.Claims.ClaimsIdentity)this.User.Identity;
            var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
            return
                (claim != null && !string.IsNullOrEmpty(claim.Value))
                ?
                Int32.Parse(claim.Value)
                :
                0;
        }

        private string BuildMenu(string absoluteUri, string cssClassForRootElement = "")
        {
            string src = string.Empty;
            // TODO: Da migliorare il modo in cui si richiede il tpl e si ricava il path
            string tpl_path = System.Web.Hosting.HostingEnvironment.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + CurrentConfig.CurrentTheme.TemplateFolder + "/commons/topmenu.tpl");

            using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                try
                {
                    // estrazione dati da db
                    NetCms.Structure.WebFS.StructureFolder structureFolder = NetCms.Structure.WebFS.FileSystemBusinessLogic.FolderBusinessLogic.GetFolderRecordByPath("", Network.ID, sess);
                    FrontendApiFolder currentFolder = new FrontendApiFolder(structureFolder.ID, sess);

                    // voci di menu di primo livello
                    var docs = currentFolder.ChildDocuments(absoluteUri, currentFolder, sess);
                   
                    var folders = (from f in currentFolder.Folders
                                 where f != null && f.ShowInTopMenu && f.GetStato(sess) == 0
                                 select new MenuItemDTO
                                 {
                                     Label = f.Label,
                                     Url = absoluteUri + f.Path,                                     
                                     Description = f.Descrizione,
                                     Cssclass = f.GetCssClass(sess),
                                     Havechild = (f.Folders != null) ? f.Folders.Any() : false,
                                     Order = f.Order,
                                     Isactive = false,
                                     Childs = (f.Folders != null && f.Folders.Any()) ? f.Childs(absoluteUri, f, sess) : null                                     
                                 });
                    var items = docs.Concat(folders).OrderByDescending(x=>x.Order);                 

                    object menu = new { items = items, usemegamenu = (CurrentConfig.TopMenuStyle == CmsConfigs.Model.Config.MenuType.Megamenu) };

                    var template = Scriban.Template.Parse(System.IO.File.ReadAllText(tpl_path), tpl_path);

                    if (template.HasErrors)
                    {
                        foreach (var error in template.Messages)
                            src += error.Message + System.Environment.NewLine;
                    }
                    else
                    {
                        var context = new Scriban.TemplateContext();
                        context.PushCulture(System.Globalization.CultureInfo.CurrentCulture);

                        var scriptObj = new Scriban.Runtime.ScriptObject();
                        scriptObj.Add("menu", menu);
                        context.PushGlobal(scriptObj);

                        src = template.Render(context);

                        return src;
                    }
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Template parsing error", "", "", "", ex);
                }
                return "";
            }
        }       

        private string BuildLoginControl(User currentUser, string absoluteUri)
        {
            string strLoginControl = string.Empty;
            WebControlToString converter = new WebControlToString();
            LoginUserControl loginControl = new LoginUserControl(currentUser, absoluteUri, CurrentConfig.CurrentTheme.ParentTheme.Name, true);
            strLoginControl = converter.ControltoHtml(loginControl);

            return strLoginControl;
        }

        private User GetUser(int idUser, ISession sess)
        {
            User currentUser = null;
            
            try
            {
                currentUser = NetCms.Users.UsersBusinessLogic.GetById(idUser, sess);
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Utente non trovato.", "", "", "", ex);
            }
           
            return currentUser;
        }
    }
}
