﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontend.Controllers
{
    public class FrontPartsDTO
    {
        public string Head
        {
            get;
            set;
        }

        public string Footer
        {
            get;
            set;
        }
    }
}
