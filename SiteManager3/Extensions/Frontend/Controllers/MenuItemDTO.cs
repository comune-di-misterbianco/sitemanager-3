﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontend.Controllers
{
    public class MenuItemDTO
    {
        public string Label { get; set; }
        public string Url { get; set; }       
        public string Description { get; set; }
        public string Cssclass { get; set; }
        public bool Havechild { get; set; }
        public bool Isactive { get; set; }
        public int Order { get; set; }
        public IEnumerable<MenuItemDTO> Childs { get; set; }
    }
}
