﻿using LanguageManager.Model;
using NetCms.Networks.WebFS;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetFrontend;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Frontend.Controllers
{
    public class FrontendApiFolder : IFolder
    {
        public int ID
        {
            get { return _ID; }
        }
        protected int _ID;
        public int ParentID
        {
            get
            {
                return _ParentID;
            }
            protected set
            {
                _ParentID = value;
            }
        }
        private int _ParentID;

        public FrontendApiFolder GetParent(ISession session)
        {
            if (ParentID > 0)
                return new FrontendApiFolder(ParentID, session);
            return null;
        }
             

        public string Path
        {
            get { return _Path; }
        }
        protected string _Path;

        public int FolderLang
        {
            get { return _FolderLang; }
        }
        protected int _FolderLang;

        public string Name
        {
            get { return _Nome; }
        }
        protected string _Nome;

        public string Label
        {
            get { return _Label; }
        }
        protected string _Label;

        public string Tree
        {
            get { return _Tree; }
        }
        protected string _Tree;

        public int Depth
        {
            get { return _Depth; }
            set { _Depth = value; }
        }
        protected int _Depth = 0;

        public int Type
        {
            get { return _Type; }
        }
        protected int _Type;

        private bool _IsSystemFolder;
        public bool IsSystemFolder
        {
            get
            {
                return _IsSystemFolder;
            }
        }

        private bool _Hidden;
        public bool Hidden
        {
            get
            {
                return _Hidden;
            }
        }

        private bool _ShowMenu;
        public bool ShowMenu
        {
            get { return _ShowMenu; }
        }

        private bool _ShowInTopMenu;
        public bool ShowInTopMenu
        {
            get { return _ShowInTopMenu; }
        }

        private string _TemplateStartPage;
        public string TemplateStartPage
        {
            get { return _TemplateStartPage; }
        }

        private string _CoverImage;
        public string CoverImage
        {
            get
            {
                return _CoverImage;
            }
        }

        private int _TitleOffset;
        public int TitleOffset
        {
            get
            {
                return _TitleOffset;
            }

        }
        protected int _Order;
        public int Order
        {
            get { return _Order; }
        }
        private string _CssClass;

        private string _Descrizione;
        public virtual string Descrizione
        {
            get
            {
                return _Descrizione;
            }
        }

        private int _Stato;
        public virtual int Stato 
        {
            get { return _Stato; }
        }


        public Dictionary<string, string> TranslatedPathsCollection
        {
            get;
            set;
        }

        public string NetworkSystemName => throw new NotImplementedException();

        public int NetworkID => throw new NotImplementedException();


        private void InitData(StructureFolder folder)
        {
            _IsSystemFolder = folder.Sistema == 1; 
            _Hidden = folder.Sistema == 2; 

            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                IList<Language> languages = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages();
                foreach (LanguageManager.Model.Language l in languages)
                {
                    TranslatedPathsCollection.Add(l.LangTwoLetterCode, folder.TranslatePath(l.ID, l.Parent_Lang));
                }
            }

            _Path = folder.Path;
            _Stato = folder.Stato;
            _Nome = folder.Nome;
            _Label = folder.Label;
            _FolderLang = folder.CurrentFolderLang.ID;
            _Depth = folder.Depth;
            _ID = folder.ID;
            _Tree = folder.Tree;
            _Descrizione = folder.Descrizione;
            _CssClass = folder.CssClass != null ? folder.CssClass : "";
            _ParentID = folder.Parent != null ? folder.Parent.ID : -1;
            _ShowMenu = folder.ShowMenu == 1;
            _Order = folder.Order;
            _TemplateStartPage = folder.Template; // template tpl associato alla pagina principale
            _ShowInTopMenu = (folder.ShowInTopMenu != 0);
        //    _CoverImage = folder.CoverImageSrc; // indirizzo img di copertina
            _TitleOffset = folder.TitleOffset;
            _Type = folder.Tipo;
        }

        private void AddFoldersToSubfolder(FrontendApiFolder folder)
        {
            if (Folders == null)
                Folders = new List<FrontendApiFolder>();
            Folders.Add(folder);
        }
        private void AddDocument(Document document)
        {
            if (Documents == null)
                Documents = new List<Document>();

            if (!Documents.Contains(document))
            Documents.Add(document);
        }
       
        public List<FrontendApiFolder> Folders
        {
            get;
            protected set;
        }
        public List<Document> Documents
        {
            get;
            protected set;
        }

        public FrontendApiFolder(int folderID, ISession innerSession = null)
        {           
            StructureFolder folder = FolderBusinessLogic.GetFolderRecordById(folderID, innerSession);
           
            if (folder != null)
            {
                InitData(folder);

                if (innerSession != null)
                {
                    InitDocs(folder, this, innerSession);
                    InitSubFolders(innerSession);
                }
            }           
        }

        private void InitSubFolders(ISession session)
        {
            if (Folders == null)
            {
                List<StructureFolder> folders = FolderBusinessLogic.FindSubFolderOrdered(this.ID, "Order", session).ToList();
                
                foreach (StructureFolder fold in folders.Where(x => x.Stato == 0 && x.Depth <= 3))
                {
                    FrontendApiFolder childFolder = new FrontendApiFolder(fold.ID, session);                   
                    AddFoldersToSubfolder(childFolder); // rimosso uso della cache per evitare complicazione tra il front (legacy) e le API.
                    childFolder.InitDocs(fold, childFolder, session);
                }
                
            }
        }
      

        private void InitDocs(StructureFolder folder, FrontendApiFolder apiFolder, ISession session)
        {
            var docs = DocumentBusinessLogic.FindDocumentRecordByFolderOrderedByLabel(folder.ID, session);

            if (docs != null && docs.Any())
            {
                foreach (Document document in docs)
                    apiFolder.AddDocument(document);
            }            
        }


        //TODO: implementazione temporanea per evitare di recuperare la parent folder dalla cache
        public string GetCssClass(ISession session)
        {
            if (ParentID > 0)
                return (((GetParent(session))._CssClass) + " " + _CssClass).Trim();
            else
                return _CssClass;
        }

        //TODO: implementazione temporanea per evitare di recuperare la parent folder dalla cache
        public int GetStato(ISession session)
        {
            if (ParentID > 0)
                return (_Stato != 4) ? _Stato : (GetParent(session).Stato);
            else
                return _Stato;
        }



        public static bool IsFolderRestricted(string folderID, ISession session)
        {
            return FolderBusinessLogic.IsFolderRestricted(int.Parse(folderID), session); //NetFrontend.DAL.FileSystemDAL.IsFolderRestricted(int.Parse(folderID));
        }
    
        public IEnumerable<MenuItemDTO> Childs(string absoluteUri, FrontendApiFolder currentFolder, ISession session)
        {            
            var items = (from folder in currentFolder.Folders
                            where folder != null && folder.ShowInTopMenu && folder.GetStato(session) == 0
                            orderby folder.Order ascending
                            select new MenuItemDTO
                            {
                                Label = folder.Label,
                                Url = absoluteUri + folder.Path,                                
                                Description = folder.Descrizione,
                                Cssclass = folder.GetCssClass(session),
                                Havechild = (folder.Folders != null && folder.Folders.Any()),
                                Order = folder.Order,
                                Isactive = false,
                                Childs = (folder.Folders != null && folder.Folders.Any() && folder.Depth <= 2) 
                                         ? Childs(absoluteUri, folder, session) : null
                            });

            if (currentFolder.Documents != null && currentFolder.Documents.Any())
            {
                var docs = ChildDocuments(absoluteUri, currentFolder, session);
                items = items.Concat(docs).OrderByDescending(x=>x.Order);
            }

            return items;         
        }

        public IEnumerable<MenuItemDTO> ChildDocuments(string absoluteUri,FrontendApiFolder currentFolder, ISession session)
        {
            var d = (from doc in currentFolder.Documents
                     where doc != null && doc.ShowInTopMenu == 1 && doc.Stato == 0
                     orderby doc.Order descending
                     select new MenuItemDTO
                     {
                         Label = doc.Nome,
                         Url = (doc.Application != 9) 
                                ? absoluteUri + doc.Folder.Path + "/" + doc.PhysicalName 
                                : GetLink(doc.ID, absoluteUri),                        
                         Description = doc.Descrizione,
                         Cssclass = "",
                         Havechild = false,
                         Order = doc.Order,
                         Isactive = false,
                         Childs = null,
                     });
            return d;
        }  
        


        private string GetLink(int idDoc, string absoluteUri)
        {
            string link = string.Empty;
            Document doc = DocumentBusinessLogic.GetDocumentRecordById(idDoc);
            PropertyInfo prop = doc.ContentRecord.GetType().GetProperty("Href");
            string linkValue = prop.GetValue(doc.ContentRecord, null).ToString();

            string tmpLinkValue = linkValue.Replace(NetCms.Structure.Utility.ImportContentFilter.WebRoot, "");

            if (tmpLinkValue.StartsWith("http://") || tmpLinkValue.StartsWith("https://"))
                link = tmpLinkValue;
            else
                link = absoluteUri + tmpLinkValue;

            return link;
        }
    }
}
