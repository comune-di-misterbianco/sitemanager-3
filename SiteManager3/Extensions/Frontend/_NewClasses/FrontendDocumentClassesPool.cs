﻿using System;
using System.Web;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace NetFrontend
{
    public static class FrontendDocumentsClassesPool
    {
        private const string ApplicationKey = "NetFrontend.FrontendDocumentsClassesPool.ApplicationKey";

        public static Dictionary<string,Type> Classes
        {
            get
            {
                if (_Classes == null)
                {
                    if (HttpContext.Current.Application[ApplicationKey] != null)
                        _Classes = (Dictionary<string, Type>)HttpContext.Current.Application[ApplicationKey];
                    else
                        HttpContext.Current.Application[ApplicationKey] = _Classes = new Dictionary<string, Type>();
                }

                return _Classes;
            }
        }
        private static Dictionary<string, Type> _Classes;

        public static void ParseType(Type type)
        {
            FrontendDocumentsDefiner attribute = System.Attribute.GetCustomAttributes(type).FirstOrDefault(x => x is FrontendDocumentsDefiner) as FrontendDocumentsDefiner;
            if (attribute != null)
            {
                if (type.HinheritsFrom("NetCms.Networks.WebFS.IDocument"))
                    Classes.Add(attribute.ApplicationID.ToString(), type);
            }
        }

        public static bool HinheritsFrom(this Type childType, string parentType)
        {
            var type = childType;

            while (type != null)
            {
                if (type.FullName == parentType) return true;
                if (type.GetInterfaces().Count(i => i.FullName == parentType)>0) return true;
                type = type.BaseType;
            }
            
            return false;
        }
    }

    [System.AttributeUsage(System.AttributeTargets.Class)]
    public class FrontendDocumentsDefiner : System.Attribute
    {
        public FrontendDocumentsDefiner(int applicationId, string name)
        {
            ApplicationID = applicationId;
            Name = name;
        }

        public int ApplicationID { get; private set; }
        public string Name { get; private set; }
    }
}