using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Xml;
using System.Text;
using NetCms.Networks.WebFS;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System.Linq;


/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    public abstract class FrontendDocument: IDocument
    {
        #region Properties

        private LayoutData _FrontLayout;
        public LayoutData FrontLayout
        {
            get
            {
                return _FrontLayout;
            }
        }

        public int ID
        {
            get { return _ID; }
        }
        protected int _ID;

        public int DocLang
        {
            get { return _DocLang; }
        }
        protected int _DocLang;

        public string Name
        {
            get { return _Nome; }
        }
        protected string _Nome;
        
        public string NetworkSystemName
        {
            get
            {
                if (_NetworkSystemName == null)
                {
                    _NetworkSystemName = this.Folder != null ? this.Folder.NetworkSystemName : NetCms.Networks.NetworksManager.CurrentActiveNetwork.SystemName;
                }
                return _NetworkSystemName;
            }
        }
        private string _NetworkSystemName;

        public int NetworkID
        {
            get
            {
                if (_NetworkID == 0)
                {
                    _NetworkID = this.Folder != null ? this.Folder.NetworkID : NetCms.Networks.NetworksManager.CurrentActiveNetwork.ID;
                }
                return _NetworkID;
            }
        }
        private int _NetworkID;

        public bool Grant
        {
            get
            {
                return CurrentUserHasGrants(this.ID.ToString());
            }
        }
        
        public bool Restricted
        {
            get
            {
                if (_Restricted == null)
                {
                    _Restricted = new bool?(IsDocumentRestricted(this.ID.ToString()));
                }
                return _Restricted.Value;
            }
        }
        private bool? _Restricted;

        public string CssClass
        {
            get { return _CssClass; }
        }
        protected string _CssClass;

        public string Label
        {
            get { return _Label; }
        }
        protected string _Label;

        public string ContentID
        {
            get { return _ContentID; }
        }
        protected string _ContentID;

        public int Type
        {
            get { return _Type; }
        }
        protected int _Type;
        
        public virtual int Stato
        {
            get
            {
                return _Stato;
            }
        }
        public int _Stato = -1;

        private bool _ShowInTopMenu;
        public bool ShowInTopMenu
        {
            get { return _ShowInTopMenu; }
        }

        private string _Descrizione;
        public virtual string Descrizione
        {
            get
            {
                return _Descrizione;
            }
        }

        private DateTime _Data;
        public DateTime Data 
        { 
            get 
            { 
                return _Data; 
            } 
        }

        private int _Order;

        public int Order
        {
            get { return _Order; }            
        }


        #endregion

        public int FolderID { get; private set; }
        public FrontendFolder Folder { get; private set; }
        public abstract string FrontendUrl { get; }

        //private DataRow DbData;
        private void InitData(Document doc)
        {
            _CssClass = doc.CssClass;
            //_ContentID = doc.DocLangs.ElementAt(0).Content.ID.ToString(); //DbData["Content_DocLang"].ToString();
            _ContentID = doc.CurrentDocLang.Content.ID.ToString();
            _Nome = doc.PhysicalName; //DbData["Name_Doc"].ToString();
            _Label = doc.Nome; //DbData["Label_DocLang"].ToString();
           // _DocLang = doc.DocLangs.ElementAt(0).ID; //int.Parse(DbData["id_DocLang"].ToString());
            _DocLang = doc.CurrentDocLang.ID; 
            _Type = doc.Application; //int.Parse(DbData["Application_Doc"].ToString());
            _ID = doc.ID; //int.Parse(DbData["id_Doc"].ToString());
            _Descrizione = doc.Descrizione; //DbData["Desc_DocLang"].ToString();
            _Stato = doc.Stato; //int.TryParse(DbData["Stato_Doc"].ToString(), out _Stato);
            _ShowInTopMenu = doc.ShowInTopMenu != 0;
            
            _Data = doc.Data;
            _Order = doc.Order;

            if (doc.Layout != -1)                
                _FrontLayout = new LayoutData(doc.Layout.ToString(), (this.Folder != null ? this.Folder.FrontLayout : null));
            else
                _FrontLayout = new LayoutData(NetCms.Front.FrontLayouts.SystemError);
        }

        public FrontendDocument(Document row, FrontendFolder folder)
        {
            Folder = folder;
            FolderID =  folder.ID;
            InitData(row);
        }
        public FrontendDocument(int docID, FrontendFolder folder)
        {
            Folder = folder;
            FolderID =  folder.ID;
            Document doc = DocumentBusinessLogic.GetDocumentRecordById(docID);
            //DataRow rows = NetFrontend.DAL.FileSystemDAL.GetDocumentRecordByID(docID);
            if (doc != null)
            {
                InitData(doc);
            }
            else throw new Exception("Impossibile trovare il record del documento associato all'ID " + docID);
        }

        public static FrontendDocument Fabricate(Document row, FrontendFolder folder)
        {
            string type = row.Application.ToString(); //row["Application_Doc"].ToString();

            try
            {                
                if (FrontendDocumentsClassesPool.Classes.ContainsKey(type))
                {
                    object[] pars = { row, folder };
                    var typeClass = FrontendDocumentsClassesPool.Classes[type];
                    return System.Activator.CreateInstance(typeClass, pars) as FrontendDocument;
                }
                else
                {
                    throw new Exception("Tipo folder non riconoscibile, " + type);
                }
            }
            catch(Exception ex)
            {
                //Trace
                
                if (System.Web.HttpContext.Current != null && NetCms.Users.AccountManager.CurrentAccount != null)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "134");
                }
                else
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, "", "", "134");
                }

                throw new Exception("Tipo folder non riconoscibile, " + type);
            }           
           // return null;
        }
        public static FrontendDocument Fabricate(int docID, FrontendFolder folder)
        {
            Document doc = DocumentBusinessLogic.GetDocumentRecordById(docID);
            //DataRow rows = NetFrontend.DAL.FileSystemDAL.GetDocumentRecordByID(docID);
            if (doc != null)
            {
                Fabricate(doc, folder);
            }
            return null;
        }

        public static FrontendDocument Fabricate(Document doc)
        {
            var folder = NetCms.Front.FrontendPageCache.GetFolder(doc.Folder.ID);
            return Fabricate(doc, folder);
        }
        public static FrontendDocument Fabricate(int docID)
        {
            Document doc = DocumentBusinessLogic.GetDocumentRecordById(docID);
            //DataRow rows = NetFrontend.DAL.FileSystemDAL.GetDocumentRecordByID(docID);
            if (doc != null)
            {
                var folder = NetCms.Front.FrontendPageCache.GetFolder(doc.Folder.ID);
                return Fabricate(doc, folder);
            }
            return null;
        }

        public static bool CurrentUserHasGrants(string docID)
        {
            if (IsDocumentRestricted(docID))
            {
                if (NetCms.Users.AccountManager.Logged)
                {
                    CriteriaDocument doc = new CriteriaDocument(int.Parse(docID), NetCms.Front.FrontendNetwork.GetCurrentFrontendNetwork().SystemName, NetCms.Front.FrontendNetwork.GetCurrentFrontendNetwork().ID);
                    var grant = new NetCms.Networks.Grants.DocumentGrants(doc, NetCms.Users.AccountManager.CurrentAccount.Profile)["frontend"].Allowed;
                    return grant;
                }
                else 
                    return false;
            }
            else
                return true;
            
        }
        public static bool IsDocumentRestricted(string docID)
        {
            return DocumentBusinessLogic.IsDocumentRestricted(int.Parse(docID)); //NetFrontend.DAL.FileSystemDAL.IsDocumentRestricted(int.Parse(docID));
        }
    }
}