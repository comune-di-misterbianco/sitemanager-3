using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Xml;
using System.Text;
using NetCms.Networks.WebFS;


/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    public class CriteriaFolder: IFolder
    {
        public int ID { get; private set; }
        public string NetworkSystemName { get; private set; }
        public string Path { get; private set; }

        public CriteriaFolder(int id, string path, string networkSystemName,int networkId)
        {
            this.ID = id;
            this.Path = path;
            this.NetworkSystemName = networkSystemName;
            this.NetworkID = networkId;
        }


        public int NetworkID
        {
            get;
            private set;
        }
    }
}