using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using NetCms.Networks.WebFS;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using LanguageManager.BusinessLogic;
using LanguageManager.Model;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetFrontend
{
    public class FrontendFolder: IFolder
    {
        private string  _CssClass;
        public string  CssClass
        {
            get 
            {
                if (Parent != null)
                    return (Parent.CssClass + " " + _CssClass).Trim();
                else
                    return _CssClass; 
            }

        }

        public string NotInheritedCssClass
        {
            get
            {     
                return _CssClass;
            }

        }

        private FrontendFolder _Parent;
        public FrontendFolder Parent
        {
            get
            {
                if (_Parent == null && ParentID>0)
                    _Parent = NetCms.Front.FrontendPageCache.GetFolder(ParentID);

                return _Parent;
            }
        }

        private LayoutData _FrontLayout;
        public LayoutData FrontLayout
        {
            get
            {
                return _FrontLayout;
            }
        }

        public int ID
        {
            get { return _ID; }
        }
        protected int _ID;

        public int ParentID
        {
            get
            {
                return _ParentID;
            }
            protected set
            {
                _ParentID = value;
            }
        }
        private int _ParentID; 

        public string Path
        {
            get { return _Path; }
        }
        protected string _Path;

        public int FolderLang
        {
            get { return _FolderLang; }
        }
        protected int _FolderLang;

        public string Name
        {
            get { return _Nome; }
        }
        protected string _Nome;

        public string Label
        {
            get { return _Label; }
        }
        protected string _Label;

        public string Tree
        {
            get { return _Tree; }
        }
        protected string _Tree;

        public int Depth
        {
            get { return _Depth; }
            set { _Depth = value; }
        }
        protected int _Depth = 0;

        public int Type
        {
            get { return _Type; }
        }
        protected int _Type;
        
        private bool _IsSystemFolder;
        public bool IsSystemFolder
        {
            get
            {
                return _IsSystemFolder;
            }
        }

        private bool _Hidden;
        public bool Hidden
        {
            get
            {
                return _Hidden;
            }
        }
        
        public bool Restricted
        {
            get
            {
                if (_Restricted == null)
                    _Restricted = new bool?(IsFolderRestricted(ID.ToString()));

                return _Restricted.Value;
            }
        }
        private bool? _Restricted;

        public bool Pubblica
        {
            get { return !Restricted; }
        }

        private bool _ShowMenu;
        public bool ShowMenu
        {
            get { return _ShowMenu;  }
        }

        private bool _ShowInTopMenu;
        public bool ShowInTopMenu
        {
            get { return _ShowInTopMenu; }
        }

        private string _TemplateStartPage;
        public string TemplateStartPage
        {
            get { return _TemplateStartPage; }
        }

        private string _CoverImage;
        public string CoverImage
        {
            get
            {
                return _CoverImage;
            }
        }

        protected int _Order;
        public int Order
        {
            get { return _Order; }           
        }

        public enum MainpageTypes
        {
            Simple,List,Custom,Section,Landing
        }

        private MainpageTypes _Mainpage;
        public MainpageTypes Mainpage
        {
            get
            {
                return _Mainpage;
            }
        }

        public enum RssStatus { SystemError, Inheriths, Disabled, Enabled }

        private RssStatus _RssValue;
        public RssStatus RssValue
        {
            get
            {
                return _RssValue;
            }
        }

        private bool _EnableRss;
        public bool EnableRss
        {
            get
            {
                switch (RssValue)
                {
                    case RssStatus.Disabled: return false;
                    case RssStatus.Enabled: return true;
                    case RssStatus.Inheriths: return Parent != null ? Parent.EnableRss : false;
                    case RssStatus.SystemError: return false;
                    default: return false;
                }
            }
        }

        private int _MainpageID;
        public int MainpageID
        {
            get
            {
                return _MainpageID;
            }
        }

        public string TypeName
        {
            get
            {
                if (_TypeName == null)
                {
                    _TypeName = NetCms.Structure.Applications.ApplicationsPool.Assemblies[Type.ToString()].Label;
                    //DataRow[] rows = Applicativi.Select("id_Applicativo = " + Type);
                    //if (rows.Length > 0)
                    //    _TypeName = rows[0]["Nome_Applicativo"].ToString();
                    //else
                    //_TypeName = "";
                }
                return _TypeName;
            }
        }
        protected string _TypeName;


       public string KeyTypeName
        {
            get
            {
                if (_KeyTypeName == null)
                {
                    _KeyTypeName = NetCms.Structure.Applications.ApplicationsPool.Assemblies[Type.ToString()].SystemName;
                }
                return _KeyTypeName;
            }
        }
        protected string _KeyTypeName;

        private string _Descrizione;
        public virtual string Descrizione
        {
            get
            {
                return _Descrizione;
            }
        }

        public int NetworkID
        {
            get
            {
                if (_NetworkID == 0)
                {
                    _NetworkID = NetCms.Networks.NetworksManager.CurrentActiveNetwork.ID;
                }
                return _NetworkID;
            }
        }
        private int _NetworkID;

        private int _TitleOffset;
        public int TitleOffset {
            get
            {
                return _TitleOffset;
            }
            
        }

        public string NetworkSystemName
        {
            get
            {
                if (_NetworkSystemName == null)
                {
                    _NetworkSystemName = this.Parent!=null? this.Parent.NetworkSystemName : NetCms.Networks.NetworksManager.CurrentActiveNetwork.SystemName;
                }
                return _NetworkSystemName;
            }
        }
        private string _NetworkSystemName;

        private int _Stato;
        public int StateValue
        {
            get
            {
                if (Parent != null)
                    return (_Stato != 4 ) ? _Stato : Parent.StateValue;
                else
                    return _Stato;
            }
        }        

        public List<FrontendFolder> SubFolders
        {
            get
            {
                if (Folders == null)
                {
                    Folders = new List<FrontendFolder>();
                    List<StructureFolder> folders = FolderBusinessLogic.FindSubFolderOrdered(this.ID, "Order").ToList();
                    //var rows = NetFrontend.DAL.FileSystemDAL.ListFoldersRecordsBySqlConditions("Trash_Folder = 0 AND  Parent_Folder = " + this.ID, order: "Order_Folder");
                    foreach(StructureFolder fold in folders)
                        Folders.Add(NetCms.Front.FrontendPageCache.GetFolder(fold));
                }
                return Folders;
            }
        }
        protected List<FrontendFolder> Folders;
   
        
        public List<FrontendDocument> Documents
        {
            get
            {
                if (Docs == null)
                {
                    initDocs();
                }
                return Docs;
            }
        }
        protected List<FrontendDocument> Docs;
        protected virtual void initDocs()
        {
            Docs = new List<FrontendDocument>();

            List<Document> docs = DocumentBusinessLogic.FindDocumentRecordByFolderOrderedByLabel(ID);
            //DataRow[] rows = NetFrontend.DAL.FileSystemDAL.ListDocumentsRecordsBySqlConditions("Folder_Doc = " + ID + " AND Trash_Doc = 0", order: "Label_DocLang");
            //for (int i = 0; i < rows.Length; i++)
            if (docs != null && docs.Any())
            {
                foreach (Document document in docs)
                {
                    FrontendDocument doc = FrontendDocument.Fabricate(document, this);
                    Docs.Add(doc);
                }
            }
        }

        public bool NeedVisualization
        {
            get 
            {
                return  this.StateValue == 0 &&
                        (
                           (this.HasSubFolders && this.SubFolders.Count(x => x.NeedVisualization) > 0) ||
                           (this.HasDocuments && this.Documents.Count(x => x.Grant && x.Stato == 0) > 0) ||
                            this.Grant
                        );
            }
        }

        public bool Grant
        {
            get 
            {
                return CurrentUserHasGrants(this.ID.ToString(), this.Path);
            }
        }        

        public bool HasSubFolders
        {
            get { return SubFolders.Count > 0; }
        }
        public bool HasDocuments
        {
            get { return Documents.Count > 0; }
        }

        private string _FrontendUrl;
        public string FrontendUrl
        {
            get
            {
                if (_FrontendUrl == null)
                {
                    var network = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks[NetCms.Networks.NetworksManager.GlobalIndexer.NetworkKeys[this.NetworkID]];

                    if (LanguageManager.Utility.Config.EnableMultiLanguage)
                    {
                        // sintassi del url --> /en/network/folder/
                        string folderNameTranslate = TranslatedPathsCollection[LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode];
                        string twoLetterCode = (LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode == LanguageBusinessLogic.GetDefaultLanguage().LangTwoLetterCode) ? "" : "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode;
                        _FrontendUrl = twoLetterCode + network.Paths.AbsoluteFrontRoot + folderNameTranslate + "/";
                    }
                    else
                    {                       
                        _FrontendUrl = network.Paths.AbsoluteFrontRoot + this.Path + "/";
                    }
                }
                return _FrontendUrl;
            }
        }

        public Dictionary<string,string> TranslatedPathsCollection
        {
            get;set;    
        }
        

        private void InitData(StructureFolder folder)
        {
            _IsSystemFolder = folder.Sistema == 1; //DbData["System_Folder"].ToString() == "1";
            _Hidden = folder.Sistema == 2; //DbData["System_Folder"].ToString() == "2";

            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                IList<Language> languages = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages();
                foreach (LanguageManager.Model.Language l in languages)
                {
                    TranslatedPathsCollection.Add(l.LangTwoLetterCode, folder.TranslatePath(l.ID, l.Parent_Lang));
                }
            }

            //if (LanguageManager.Utility.Config.EnableMultiLanguage)
            //{
            //    if (LanguageBusinessLogic.CurrentLanguage.Default_Lang > 0)
            //        _Path = folder.Path; //DbData["Path_Folder"].ToString();
            //    else
            //        _Path = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + folder.Path; //DbData["Path_Folder"].ToString();
            //}
            //else
            _Path = folder.Path; //DbData["Path_Folder"].ToString();

            //_Path = folder.Path; //DbData["Path_Folder"].ToString();
            _Stato = folder.Stato; //int.Parse(DbData["Stato_Folder"].ToString());
            _Nome = folder.Nome; //DbData["Name_Folder"].ToString();
            _Label = folder.Label; //DbData["Label_FolderLang"].ToString();
            _FolderLang = folder.CurrentFolderLang.ID; //int.Parse(DbData["id_FolderLang"].ToString());
            _Depth = folder.Depth; //int.Parse(DbData["Depth_Folder"].ToString());
            _ID = folder.ID; //int.Parse(DbData["id_Folder"].ToString());
            _Tree = folder.Tree; //DbData["Tree_Folder"].ToString();
            _Descrizione = folder.Descrizione; //DbData["Desc_FolderLang"].ToString();
            _CssClass = folder.CssClass != null ? folder.CssClass : ""; //DbData["CssClass_Folder"].ToString();
            _ParentID = folder.Parent != null ? folder.Parent.ID : -1;
            _ShowMenu = folder.ShowMenu == 1;
            _Order = folder.Order;
            _TemplateStartPage = folder.Template; // template tpl associato alla pagina principale
            _ShowInTopMenu = (folder.ShowInTopMenu != 0);
            _CoverImage = folder.CoverImageSrc; // indirizzo img di copertina
            
            _TitleOffset = folder.TitleOffset;

            //int.TryParse(DbData["Parent_Folder"].ToString(), out _ParentID);
            if (folder.Rss.ToString().Length > 0)
            {
                switch (folder.Rss)
                {
                    case 0: _RssValue = RssStatus.Inheriths; break;
                    case 1: _RssValue = RssStatus.Enabled; break;
                    case 2: _RssValue = RssStatus.Disabled; break;
                }
            }
            else
                _RssValue = RssStatus.SystemError;
            _Type = folder.Tipo; //int.Parse(DbData["Tipo_Folder"].ToString());

            _MainpageID = folder.HomeType; //int.Parse(DbData["HomeType_Folder"].ToString());

            //if (MainpageID < 0) {
            //    this._Mainpage = MainpageTypes.List;
            //    _MainpageID = MainpageID * -1;
            //}
            //else if (MainpageID == 0)
            //    this._Mainpage = MainpageTypes.Simple;
            //else if (MainpageID > 0)
            //    this._Mainpage = MainpageTypes.Custom;

            if (folder.HomeType < -1)
                this._Mainpage = MainpageTypes.Custom;
            else
            {
                switch (folder.HomeType)
                {
                    case -1:
                        this._Mainpage = MainpageTypes.List;
                        break;
                    case 0:
                        this._Mainpage = MainpageTypes.Simple;
                        break;
                    case 1:
                        this._Mainpage = MainpageTypes.Custom;
                        break;
                    case 2:
                        this._Mainpage = MainpageTypes.Landing;
                        break;
                    case 3:
                        this._Mainpage = MainpageTypes.Section;
                        break;
                }
            }

            if (folder.Layout != -1) //DbData.Table.Columns.Contains("Layout_Folder"))
                //if (this.Type == 1) 
                //    _FrontLayout = new LayoutData(NetCms.Front.FrontLayouts.All);
                //else 
                    _FrontLayout = new LayoutData(folder.Layout.ToString(), (Parent != null ? this.Parent.FrontLayout : null));
            else
                _FrontLayout = new LayoutData(NetCms.Front.FrontLayouts.SystemError);


        }

        public FrontendFolder(string folderPath)
        {
            string sql = "";

            //Controllo se questa network contiene una cartella col path che arriva per paramentro
            //sql += "(Network_Folder = " + NetworkID + " AND Path_Folder LIKE '" + folderPath.Replace("'", "''") + "')";
            //In caso contrario verifico il path sia un path relativo ad una Cartella di Applicazione verticale
            //if(folderPath.Length>0)
            //    sql += " OR " + ParentsFolderSQL(folderPath);
            TranslatedPathsCollection = new Dictionary<string, string>();
            StructureFolder folder = FolderBusinessLogic.GetFolderByPathCheckingIfNetworkOrApplication(folderPath.Replace("'", "''"), NetworkID, ParentsFolderSQL(folderPath));
            //DataRow row = NetFrontend.DAL.FileSystemDAL.GetFolderRecordBySqlConditions(sql);
            if (folder != null)
            {
                InitData(folder);
            }

            //Inserire qui nuova inizializzazione per il multilingua.



            //else
            //    throw new NetCms.Exceptions.ServerError500Exception("Folder (" + folderPath + ") Inesistente");
        }
        public FrontendFolder(int folderID)
        {
            TranslatedPathsCollection = new Dictionary<string, string>();
            StructureFolder folder = FolderBusinessLogic.GetFolderRecordById(folderID);
            //DataRow row = NetFrontend.DAL.FileSystemDAL.GetFolderRecordByID(folderID);
            if (folder != null)
            {
                InitData(folder);               
            }
            //else
            //    throw new NetCms.Exceptions.ServerError500Exception("Folder #" + folderID + " Inesistente");
        }
        public FrontendFolder(StructureFolder row)
        {
            TranslatedPathsCollection = new Dictionary<string, string>();
            if (row!=null)
            {
                InitData(row);
            }
            //else
            //    throw new NetCms.Exceptions.ServerError500Exception("Folder Inesistente");
        }

        protected List<string> ParentsFolderSQL(string folderpath)
        {
            List<string> paths = new List<string>();

            string[] array = folderpath.Split('/');
            string pathtot = "";
           
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i].Length > 0)
                {
                   
                    pathtot += "/" + array[i].Replace("'", "''");

                    paths.Add(pathtot);
                }
            }

            return paths;
        }

        //protected string ParentsFolderSQL(string folderpath)
        //{
        //    string folders = "(Network_Folder = " + NetworkID + " AND HomeType_Folder>0 AND System_Folder = 5 AND(";

        //    string[] array = folderpath.Split('/');
        //    string pathtot = "";
        //    bool first = true;
        //    for (int i = 0; i < array.Length; i++)
        //    {
        //        if (array[i].Length > 0)
        //        {
        //            if (!first)
        //                folders += " OR ";
        //            else
        //                first = false;

        //            pathtot += "/" + array[i].Replace("'", "''");

        //            folders += " Path_Folder = '" + pathtot + "'";
        //        }
        //    }

        //    folders += "))";
        //    return folders;
        //}


        public FrontendFolder GetRootFolder()
        {
            var folder = this;
            while (folder.Parent != null) folder = folder.Parent;
            return folder;
        }

        /// <summary>
        /// Trova una cartella nella gerarchia delle cartelle a partire da questa
        /// </summary>
        /// <param name="searchTerms">Nome, Path o Id della cartella da cercare</param>
        /// <returns></returns>
        public FrontendFolder FindFolder(string searchTerms)
        {
            Func<FrontendFolder, bool> check = (folder) => { return folder.ID.ToString() == searchTerms || folder.Path.ToLower() == searchTerms.ToLower() || folder.Name.ToLower() == searchTerms.ToLower(); };

            FrontendFolder result = check(this)? this : null;

            if (result == null)
                result = this.SubFolders.FirstOrDefault(x=>check(x));

            if (result == null)
            {
                foreach(var sub in this.SubFolders)
                {
                    result = sub.FindFolder(searchTerms);
                    if(result !=null) break;
                }
            }

            return result;
        }

        /// <summary>
        /// Trova un documento nella gerarchia delle cartelle a partire da questa
        /// </summary>
        /// <param name="searchTerms">Nome o Id del documento da cercare</param>
        /// <returns></returns>
        public FrontendDocument FindDocument(string searchTerms)
        {
            Func<FrontendDocument, bool> check = (doc) => { return doc.ID.ToString() == searchTerms || doc.Name.ToLower() == searchTerms.ToLower(); };

            FrontendDocument result = this.Documents.FirstOrDefault(x => check(x));

            if (result == null)
            {
                foreach(var sub in this.SubFolders)
                {
                    result = sub.FindDocument(searchTerms);
                    if(result !=null) break;
                }
            }

            return result;
        }

        public static bool CurrentUserHasGrants(string folderID, string path)
        {
            if (IsFolderRestricted(folderID))
            {
                if (NetCms.Users.AccountManager.Logged)
                {
                    CriteriaFolder folder = new CriteriaFolder(int.Parse(folderID), path, NetCms.Front.FrontendNetwork.GetCurrentFrontendNetwork().SystemName, NetCms.Front.FrontendNetwork.GetCurrentFrontendNetwork().ID);
                    var grant = new NetCms.Networks.Grants.FolderGrants(folder, NetCms.Users.AccountManager.CurrentAccount.Profile)["frontend"].Allowed;
                    return grant;
                }
                else
                    return false;
            }
            else
                return true;

        }
        public static bool IsFolderRestricted(string folderID)
        {
            return FolderBusinessLogic.IsFolderRestricted(int.Parse(folderID)); //NetFrontend.DAL.FileSystemDAL.IsFolderRestricted(int.Parse(folderID));
        }

    }
}