﻿using System;
using log4net;
using System.Linq;
using System.Web;
using System.Collections.Generic;

public class FrontDebugLogger
{
    private static readonly ILog logCount = LogManager.GetLogger("Logger");

    private FrontDebugLogger()
    {
    }

    private DateTime LastSaveTime { get; set; }
    public DateTime FirstSaveTime { get; private set; }

    public void SaveTime()
    {
        if(FirstSaveTime == DateTime.MinValue)
            FirstSaveTime = DateTime.Now;
        LastSaveTime = DateTime.Now;
    }

    public void Log(string message)
    {
        TimeSpan time = DateTime.Now - LastSaveTime;
        string timeString = time.Minutes + ":" + time.Seconds + ":" + G2Core.Common.Utility.FormatNumber(time.Milliseconds.ToString(), 3);

        if (logCount.IsDebugEnabled)
            QueriesTimings.Add(message + " [" + Guid.NewGuid().ToString() + "]", time);
    }

    public static Dictionary<string, TimeSpan> QueriesTimings
    {
        get
        {
            if (!HttpContext.Current.Items.Contains("PageDalLoggerQueriesTimings"))
            {
                HttpContext.Current.Items["PageDalLoggerQueriesTimings"] = new Dictionary<string, TimeSpan>();
            }
            return (Dictionary<string, TimeSpan>)HttpContext.Current.Items["PageDalLoggerQueriesTimings"];
        }
    }

    public static void SaveQueriesReport()
    {
        if (NetCms.Configurations.Debug.FrontTimingsEnabled)
        {
            int i = 1;
            foreach (var entry in QueriesTimings)
            {
                string timeString = entry.Value.Minutes + ":" + entry.Value.Seconds + ":" + G2Core.Common.Utility.FormatNumber(entry.Value.Milliseconds.ToString(), 3);
                logCount.Debug(i + ") " + timeString + " - " + entry.Key);
                i++;
            }
        }
    }
    public static FrontDebugLogger PageLogger
    {
        get
        {
            if (!HttpContext.Current.Items.Contains("PageLogger"))
            {
                HttpContext.Current.Items["PageLogger"] = new FrontDebugLogger();
            }
            return (FrontDebugLogger)HttpContext.Current.Items["PageLogger"];
        }
    }

}