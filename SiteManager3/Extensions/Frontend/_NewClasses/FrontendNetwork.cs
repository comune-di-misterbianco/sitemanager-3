﻿using System;
using System.Web;
using System.Web.Caching;
using System.Collections.Generic;
using System.Data;
using System.Text;
using NetFrontend;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using LanguageManager.BusinessLogic;

namespace NetCms.Front
{
    public class FrontendNetwork : Networks.Network
    {
        public G2Core.Caching.PersistentCacheObject<FrontendFolder> CurrentFolderStore
        {
            get
            {
                if (_CurrentFolderStore == null)
                {
                    _CurrentFolderStore = new G2Core.Caching.PersistentCacheObject<FrontendFolder>("SM2_Front_NetworkCurrentFolder", G2Core.Caching.StoreTypes.Page);
                    _CurrentFolderStore.ObjectBuilder = () => 
                    {
                        try
                        {                            
                            return FrontendPageCache.GetFolder(GetCurrentFolderPathTranslated());

                        }
                        catch (Exception ex)
                        {
                            return null;
                        }
                    };
                }
                return _CurrentFolderStore;
            }
        }
        private G2Core.Caching.PersistentCacheObject<FrontendFolder> _CurrentFolderStore;

        public G2Core.Caching.PersistentCacheObject<FrontendFolder> CurrentRootStore
        {
            get
            {
                if (_CurrentRootStore == null)
                {
                    _CurrentRootStore = new G2Core.Caching.PersistentCacheObject<FrontendFolder>("SM2_Front_NetworkRootFolder", G2Core.Caching.StoreTypes.Page);
                    _CurrentRootStore.ObjectBuilder = () =>
                    {
                        try
                        {
                            return FrontendPageCache.GetFolder("");
                        }
                        catch
                        {
                            return null;
                        }
                    };
                }
                return _CurrentRootStore;
            }
        }
        private G2Core.Caching.PersistentCacheObject<FrontendFolder> _CurrentRootStore;

        public G2Core.Caching.PersistentCacheObject<FrontendDocument> CurrentDocumentStore
        {
            get
            {
                if (_CurrentDocumentStore == null)
                {
                    _CurrentDocumentStore = new G2Core.Caching.PersistentCacheObject<FrontendDocument>("SM2_Front_CurrentDocument", G2Core.Caching.StoreTypes.Page);
                    _CurrentDocumentStore.ObjectBuilder = () => 
                    {
                        if (this.CurrentFolder != null)
                        {
                            //DataRow row = NetFrontend.DAL.FileSystemDAL.GetDocumentRecordBySqlConditions("Folder_Doc = " + this.CurrentFolder.ID + " AND Name_Doc LIKE '" + NetCms.Configurations.Paths.PageName.Replace("'", "''") + "'");
                            if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang == 0)
                            {
                                string PageName = DocumentBusinessLogic.TranslateNameOnTheFly(LanguageBusinessLogic.CurrentLanguage.ID, GetCurrentFrontendNetwork().ID, NetCms.Configurations.Paths.PageName, this.CurrentFolder.Path);
                                Document doc = DocumentBusinessLogic.GetDocumentRecordByFolderAndName(this.CurrentFolder.ID, PageName.Replace("'", "''"));
                                return doc != null ? FrontendDocument.Fabricate(doc, CurrentFolder) : null;
                            }
                            else
                            {                                
                                Document doc = DocumentBusinessLogic.GetDocumentRecordByFolderAndName(this.CurrentFolder.ID, NetCms.Configurations.Paths.PageName.Replace("'", "''"));
                                return doc != null ? FrontendDocument.Fabricate(doc, CurrentFolder) : null;
                            }
                                
                        }
                        return null;
                    };
                }
                return _CurrentDocumentStore;
            }
        }
        private G2Core.Caching.PersistentCacheObject<FrontendDocument> _CurrentDocumentStore;

        public new FrontendFolder CurrentFolder
        {
            get
            {
                var currentFolder = CurrentFolderStore.Instance;
                return currentFolder;
            }
        }

        public new FrontendFolder RootFolder
        {
            get
            {
                return CurrentRootStore.Instance;
            }
        }

        public FrontendDocument CurrentDocument
        {
            get
            {
                return CurrentDocumentStore.Instance;
            }
        }

        public bool HasCurrentDocument
        {
            get
            {
                return CurrentDocument != null;
            }
        }

        public override NetCms.Faces Face
        {
            get { return NetCms.Faces.Front; }
        }

        public FrontendNetwork(int id) : base(id) { }
        public FrontendNetwork(string name) : base(name) { }
        

        public static string GetCurrentFolderPathTranslated()
        {
            string currentFolderPath = GetCurrentFolderPath();
            string pathTranslated = "";
            if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang == 0)
            {
                pathTranslated = FolderBusinessLogic.TranslatePathOnTheFly(LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.ID, GetCurrentFrontendNetwork().ID, currentFolderPath);
            }
            return pathTranslated.Length > 0 ? pathTranslated : currentFolderPath;
        }

        public static string GetCurrentFolderPath()
        {
            using (var CurrentFolderPath = new G2Core.Caching.PersistentCacheObject<string>("SM2_Front_CurrentFolderPath", G2Core.Caching.StoreTypes.Page))
            {
                CurrentFolderPath.ObjectBuilder = () =>
                {
                    string value = "";
                    NetUtility.RequestVariable path = new NetUtility.RequestVariable("path", NetUtility.RequestVariable.RequestType.QueryString);
                    if (path.IsValid())
                    {
                        value = path.StringValue;
                    }
                    else
                    {
                        NetUtility.RequestVariable fid = new NetUtility.RequestVariable("fid", NetUtility.RequestVariable.RequestType.QueryString);
                        if (fid.IsValidInteger && fid.IntValue > 0)
                        {
                            StructureFolder folder = FolderBusinessLogic.GetFolderRecordById(fid.IntValue);
                            //DataRow row = NetFrontend.DAL.FileSystemDAL.GetFolderRecordByID(fid.IntValue);
                            if (folder != null)
                                value = folder.Path + "/";
                        }
                        else
                        {
                            value = HttpContext.Current.Request.Url.LocalPath.ToLower().Replace(NetCms.Configurations.Paths.PageName.ToLower(), "");
                            string awr = NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;
                            if (awr.Length > 0)
                                value = value.Replace(awr.ToLower(), "");
                        }
                    }
                    //Rimuovo l'eventuale riferimento alla lingua
                    if (value != null)
                        value = LanguageBusinessLogic.GetUrlWithoutLanguage(value);

                    if (value != null && value.EndsWith("/"))
                        value = value.Substring(0, value.Length - 1);

                    return value.ToLower();
                };

                return CurrentFolderPath.Instance;
            }
        }

        

        public static string GetAbsoluteWebRoot()
        {
            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                if (LanguageBusinessLogic.CurrentLanguage.Default_Lang > 0)
                    return NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;
                else
                    //_Path = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + folder.Path; //DbData["Path_Folder"].ToString();
                    return "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;
            }
            else
                return NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot;


        }

        public static FrontendNetwork GetCurrentFrontendNetwork()
        {
            return NetCms.Networks.NetworksManager.CurrentActiveNetwork as FrontendNetwork;
        }

        #region Metodi non implementati su Frontend

        public override Networks.WebFS.NetworkFolder BuildRootFolder()
        {
            throw new Exception("Metodo non implementato su Frontend");
        }

        #endregion
    }
}
