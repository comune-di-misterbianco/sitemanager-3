﻿using System;
using System.Web;
using System.Web.Caching;
using System.Collections.Generic;
using System.Data;
using System.Text;
using NetFrontend;
using NetCms.Structure.WebFS;

namespace NetCms.Front
{
    public class FoldersDictionaryByID : Dictionary<int, FrontendFolder> { }
    public class FoldersDictionarybyPath : Dictionary<string, FrontendFolder> { }
    public class FrontendPageCache
    {
        private static G2Core.Caching.PersistentCacheObject<FoldersDictionaryByID> PageFoldersCacheByID
        {
            get
            {
                if (_PageFoldersCacheByID == null)
                {
                    _PageFoldersCacheByID = new G2Core.Caching.PersistentCacheObject<FoldersDictionaryByID>("SM2_Front_NetworkFoldersDictionaryByID", G2Core.Caching.StoreTypes.Page);
                }
                return _PageFoldersCacheByID;
            }
        }
        private static G2Core.Caching.PersistentCacheObject<FoldersDictionaryByID> _PageFoldersCacheByID;

        private static G2Core.Caching.PersistentCacheObject<FoldersDictionarybyPath> PageFoldersCacheByPath
        {
            get
            {
                if (_PageFoldersCacheByPath == null)
                {
                    _PageFoldersCacheByPath = new G2Core.Caching.PersistentCacheObject<FoldersDictionarybyPath>("SM2_Front_NetworkFoldersDictionaryByPath", G2Core.Caching.StoreTypes.Page);
                }
                return _PageFoldersCacheByPath;
            }
        }
        private static G2Core.Caching.PersistentCacheObject<FoldersDictionarybyPath> _PageFoldersCacheByPath;

        public static FrontendFolder GetFolder(int ID)
        {
            lock (PageFoldersCacheByID)
                lock (PageFoldersCacheByPath)
                {
                    var cacheIDs = PageFoldersCacheByID.Instance;
                    var cachePaths = PageFoldersCacheByPath.Instance;
                    if (cacheIDs.ContainsKey(ID))
                    {
                        return cacheIDs[ID];
                    }
                    else
                    {
                        var folder = new FrontendFolder(ID);
                        cacheIDs.Add(ID, folder);
                        cachePaths.Add(folder.Path, folder);
                        return folder;
                    }
                }
        }

        public static FrontendFolder GetFolder(StructureFolder row)
        {
            lock (PageFoldersCacheByID)
                lock (PageFoldersCacheByPath)
                {
                    int ID = row.ID;
                    var cacheIDs = PageFoldersCacheByID.Instance;
                    var cachePaths = PageFoldersCacheByPath.Instance;
                    if (cacheIDs.ContainsKey(ID))
                    {
                        return cacheIDs[ID];
                    }
                    else
                    {
                        var folder = new FrontendFolder(row);
                        cachePaths.Add(folder.Path, folder);
                        cacheIDs.Add(ID, folder);
                        return folder;
                    }
                }
        }

        public static FrontendFolder GetFolder(string path)
        {
            lock (PageFoldersCacheByID)
                lock (PageFoldersCacheByPath)
                {
                    var cacheIDs = PageFoldersCacheByID.Instance;
                    var cachePaths = PageFoldersCacheByPath.Instance;
                    if (cachePaths.ContainsKey(path))
                    {
                        return cachePaths[path];
                    }
                    else
                    {
                        var folder = new FrontendFolder(path);
                        cachePaths.Add(path, folder);
                        cacheIDs.Add(folder.ID, folder);
                        return folder;
                    }
                }
        }
    }
}
