using System;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Data;
using System.Web.UI;
using System.Collections.Generic;
using NetCms.Front;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using LanguageManager.BusinessLogic;
/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>


namespace NetCms.Front.Menu
{
    public class RootMenuNodeFiller:MenuNodeFiller
    {
        public new FolderMenuNode Node 
        {
            get
            {
                return base.Node as FolderMenuNode;
            }
        }

        //public string ChildsFoldersSqlConditions
        //{
        //    get
        //    {
        //        return _ChildsFoldersSqlConditions ?? (_ChildsFoldersSqlConditions = "(System_Folder = 0 OR System_Folder = 5) AND Stato_Folder = 0 AND Tree_Folder LIKE '" + this.Node.Tree + "%' AND Depth_Folder = " + (this.Node.Depth + 1));
        //    }
        //}
        //private string _ChildsFoldersSqlConditions;

        //public string ChildsDocsSqlConditions
        //{
        //    get
        //    {
        //        return _ChildsDocsSqlConditions ?? (_ChildsDocsSqlConditions = "Stato_Doc = 0 AND Folder_Doc = " + this.Node.ID);
        //    }
        //}
        //private string _ChildsDocsSqlConditions;

        public RootMenuNodeFiller(MenuNode nodeToFill)
            :base(nodeToFill)
        {
        }

        public override void FillNode(string path,int networkId)
        {
            FillNode(FolderBusinessLogic.GetFolderRecordByPath(LanguageBusinessLogic.GetUrlWithoutLanguage(path), networkId));
        }

        public override void FillNode(int ID)
        {
            FillNode(FolderBusinessLogic.GetFolderRecordById(ID));  // NetFrontend.DAL.FileSystemDAL.GetFolderRecordBySqlConditions("Path_Folder LIKE '" + ID.Replace("'", "''") + "'"));
        }
        public override void FillNode(object obj)
        {
            // obj != "" aggiunto per evitare eccezione quando viene chiamato da WfsMenuNode filler.FillNode("");
            if (obj != null && obj != "")
            {
                StructureFolder folder = obj as StructureFolder;
                FolderMenuNode node = Node;

                node.Depth = folder.Depth; //int.Parse(datarow["Depth_Folder"].ToString());
                node.Type = folder.Tipo; //int.Parse(datarow["Tipo_Folder"].ToString());
                node.ID = folder.ID; //int.Parse(datarow["id_Folder"].ToString());
                node.Label = folder.Label; //datarow["Label_FolderLang"].ToString();
                //if (datarow.Table.Columns.Contains("Order_Folder"))
                node.Order = folder.Order;//int.Parse(datarow["Order_Folder"].ToString());
                if (LanguageManager.Utility.Config.EnableMultiLanguage)
                {
                    if (LanguageBusinessLogic.CurrentLanguage.Default_Lang > 0)
                        node.Path = folder.Path; //datarow["Path_Folder"].ToString();
                    else
                        node.Path = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + folder.Path;
                }
                else
                    node.Path = folder.Path; //datarow["Path_Folder"].ToString();
                node.Restricted = folder.RestrictedInt; //int.Parse(datarow["Restricted_Folder"].ToString());
                node.SystemName = folder.Nome;//datarow["Name_Folder"].ToString();
                node.Tree = folder.Tree; //datarow["Tree_Folder"].ToString();
                node.Description = folder.Descrizione; //datarow["Desc_FolderLang"].ToString();
                node.ShowInTopMenu = folder.ShowInTopMenu != 0;


                switch (folder.Stato)
                {
                    case 0:
                        node.Stato = NetCms.Front.Menu.MenuNode.MenuNodeState.Menu;
                        break;
                    case 1:
                        node.Stato = NetCms.Front.Menu.MenuNode.MenuNodeState.Reachable;
                        break;
                    case 2:
                        node.Stato = NetCms.Front.Menu.MenuNode.MenuNodeState.NonReachable;
                        break;
                    case 4:
                        node.Stato = NetCms.Front.Menu.MenuNode.MenuNodeState.SubMenu;
                        break;
                }
                node.Link = this.CurrentAbsoluteFrontRoot.Instance + node.Path;
            }
        }

        protected override MenuNode[] BuildChildNodes()
        {
            IList<StructureFolder> frows = FolderBusinessLogic.ListChildsFoldersRecordsForRoot(this.Node.Tree, (this.Node.Depth + 1), (Node.NodeType == MenuNode.MenuNodeType.Contextual)); //NetFrontend.DAL.FileSystemDAL.ListFoldersRecordsBySqlConditions(ChildsFoldersSqlConditions);
            IList<Document> drows = DocumentBusinessLogic.ListChildsDocumentsRecords(this.Node.ID); //NetFrontend.DAL.FileSystemDAL.ListDocumentsRecordsBySqlConditions(ChildsDocsSqlConditions);

            List<MenuNode> nodes = new List<MenuNode>();

            foreach (StructureFolder row in frows)
            {
                var node = new FolderMenuNode();
                node.FillNode(row);
                nodes.Add(node);
            }
            foreach (Document doc in drows.Where(x => x.Application == 1 || x.Application == 2 || x.Application == 9))
            {
                var node = new DocMenuNode();
                node.FillNode(doc);
                nodes.Add(node);
            }
            return nodes.ToArray();
        }

        protected override int CountChildNodes()
        {
            int foldersCount = FolderBusinessLogic.CountListChildsFoldersRecordsForRoot(this.Node.Tree, (this.Node.Depth + 1), (Node.NodeType == MenuNode.MenuNodeType.Contextual));//NetFrontend.DAL.FileSystemDAL.CountFoldersRecordsBySqlConditions(ChildsFoldersSqlConditions);
            int docsCount = DocumentBusinessLogic.CountListChildsDocumentsRecords(this.Node.ID); //NetFrontend.DAL.FileSystemDAL.CountDocumentsRecordsBySqlConditions(ChildsDocsSqlConditions);

            return foldersCount >= 0 && docsCount >= 0 ? foldersCount + docsCount : -1;
        }
    }
}