using System;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Data;
using System.Web.UI;
using System.Collections.Generic;
using NetCms.Front;
using NetCms.Structure.WebFS;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using LanguageManager.BusinessLogic;
/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>


namespace NetCms.Front.Menu
{
    public class FolderMenuNodeFiller:MenuNodeFiller
    {
        public new FolderMenuNode Node
        {
            get
            {
                return base.Node as FolderMenuNode;
            }
        }
        
        //public string ChildsFoldersSqlConditions
        //{
        //    get
        //    {
        //        return _ChildsFoldersSqlConditions ?? (_ChildsFoldersSqlConditions = "System_Folder = 0 AND Stato_Folder = 0 AND Tree_Folder LIKE '" + Node.Tree + "%' AND Depth_Folder = " + (Node.Depth + 1));
        //    }
        //}
        //private string _ChildsFoldersSqlConditions;

        //public string ChildsDocsSqlConditions
        //{
        //    get
        //    {
        //        return _ChildsDocsSqlConditions ?? (_ChildsDocsSqlConditions = "Stato_Doc = 0 AND Folder_Doc = " + this.Node.ID);
        //    }
        //}
        //private string _ChildsDocsSqlConditions;

        public FolderMenuNodeFiller(MenuNode nodeToFill)
            :base(nodeToFill)
        {
        }

        public override void FillNode(string path, int networkId)
        {
            FillNode(FolderBusinessLogic.GetFolderRecordByPath(LanguageBusinessLogic.GetUrlWithoutLanguage(path), networkId));
        }

        public override void FillNode(int ID)
        {
            FillNode(FolderBusinessLogic.GetFolderRecordById(ID));
        }
        public override void FillNode(object obj)
        {
            StructureFolder folder = obj as StructureFolder;
            FolderMenuNode node = this.Node;

            node.Type = folder.Tipo;  //int.Parse(datarow["Tipo_Folder"].ToString());
            node.ID = folder.ID; //int.Parse(datarow["id_Folder"].ToString());            
            //if (datarow.Table.Columns.Contains("Order_Folder"))
            node.Order = folder.Order;//int.Parse(datarow["Order_Folder"].ToString());
            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                if (LanguageBusinessLogic.CurrentLanguage.Default_Lang > 0)
                {
                    node.Path = folder.Path;//datarow["Path_Folder"].ToString();
                    node.Label = folder.Label;//datarow["Label_FolderLang"].ToString();
                }
                else
                {
                    node.Path = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + folder.TranslatedPath;
                    node.Label = folder.CurrentFolderLang.Label;
                }
            }
            else
            {
                node.Path = folder.Path;//datarow["Path_Folder"].ToString();
                node.Label = folder.Label;//datarow["Label_FolderLang"].ToString();
            }
            node.CssClass = folder.CssClass;
            node.Restricted = folder.RestrictedInt;//int.Parse(datarow["Restricted_Folder"].ToString());
            node.SystemName = folder.Nome;//datarow["Name_Folder"].ToString();
            node.Depth = folder.Depth;//int.Parse(datarow["Depth_Folder"].ToString());
            node.Tree = folder.Tree;//datarow["Tree_Folder"].ToString();
            node.Description = folder.Descrizione;//datarow["Desc_FolderLang"].ToString();
            node.ShowInTopMenu = folder.ShowInTopMenu != 0;

            switch (folder.Stato)
            {
                case 0:
                    node.Stato = NetCms.Front.Menu.MenuNode.MenuNodeState.Menu;
                    break;
                case 1:
                    node.Stato = NetCms.Front.Menu.MenuNode.MenuNodeState.Reachable;
                    break;
                case 2:
                    node.Stato = NetCms.Front.Menu.MenuNode.MenuNodeState.NonReachable;
                    break;
                case 4:
                    node.Stato = NetCms.Front.Menu.MenuNode.MenuNodeState.SubMenu;
                    break;
            }
            node.Link = this.CurrentAbsoluteFrontRoot.Instance + node.Path + (NetCms.Configurations.Debug.GenericEnabled ? "/default.aspx" : "/");
        }

        protected override MenuNode[] BuildChildNodes()
        {           
            IList<StructureFolder> frows = FolderBusinessLogic.ListChildsFoldersRecords(Node.Tree, (Node.Depth + 1), (Node.NodeType == MenuNode.MenuNodeType.Contextual)); //NetFrontend.DAL.FileSystemDAL.ListFoldersRecordsBySqlConditions(ChildsFoldersSqlConditions);
            IList<Document> drows = DocumentBusinessLogic.ListChildsDocumentsRecords(this.Node.ID);//NetFrontend.DAL.FileSystemDAL.ListDocumentsRecordsBySqlConditions(ChildsDocsSqlConditions);
            
            List<MenuNode> nodes = new List<MenuNode>();

            foreach (StructureFolder row in frows)
            {
                var node = new FolderMenuNode();
                node.FillNode(row);
                nodes.Add(node);
            }
            foreach (Document doc in drows.Where(x=>x.Application == 1 || x.Application == 2 || x.Application == 9)) // solo homepage, webdocs, link
            {
                var node = new DocMenuNode();
                node.FillNode(doc);
                nodes.Add(node);
            }
            return nodes.ToArray();
        }

        protected override int CountChildNodes()
        {
            int foldersCount = FolderBusinessLogic.CountListChildsFoldersRecords(Node.Tree, (Node.Depth + 1), (Node.NodeType == MenuNode.MenuNodeType.Contextual)); //NetFrontend.DAL.FileSystemDAL.CountFoldersRecordsBySqlConditions(ChildsFoldersSqlConditions);
            int docsCount = DocumentBusinessLogic.CountListChildsDocumentsRecords(this.Node.ID); //NetFrontend.DAL.FileSystemDAL.CountDocumentsRecordsBySqlConditions(ChildsDocsSqlConditions);

            return foldersCount >= 0 && docsCount >= 0 ? foldersCount + docsCount : -1;
        }
    }
}