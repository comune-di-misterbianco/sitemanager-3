using System;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Data;
using System.Web.UI;
using System.Collections.Generic;
using NetCms.Front;
using NetCms.Structure.WebFS;
using System.Reflection;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using LanguageManager.BusinessLogic;
/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>


namespace NetCms.Front.Menu
{
    public class DocMenuNodeFiller:MenuNodeFiller
    {
        public DocMenuNodeFiller(MenuNode nodeToFill)
            :base(nodeToFill)
        {
        }

        public override void FillNode(string path, int networkId)
        {
            throw new NotImplementedException();
        }

        public override void FillNode(int ID)
        {
            FillNode(DocumentBusinessLogic.GetDocumentRecordById(ID));
        }
        public override void FillNode(object obj)
        {
            Document doc = obj as Document;
            DocMenuNode node = this.Node as DocMenuNode;

            node.ID = doc.ID; //int.Parse(datarow["id_Doc"].ToString());
            if (doc.Order.ToString().Length > 0) //datarow.Table.Columns.Contains("Order_Doc"))
                node.Order = doc.Order;

            StructureFolder currentFolder = (StructureFolder)NetCms.Structure.WebFS.FileSystemBusinessLogic.FolderBusinessLogic.GetById(doc.Folder.ID);

            node.Depth = currentFolder.Depth + 1;//int.Parse(datarow["Depth_Folder"].ToString())+1;
            
            node.Label = doc.Nome; //datarow["Label_DocLang"].ToString();
            node.Type = doc.Application;//int.Parse(datarow["Application_Doc"].ToString());

            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                if (LanguageBusinessLogic.CurrentLanguage.Default_Lang > 0)
                {
                    node.Path = doc.Folder.Path + "/" + doc.PhysicalName; //datarow["Path_Folder"].ToString() + "/" + datarow["Name_Doc"].ToString();
                    node.Label = doc.Nome;
                }
                else
                {
                    node.Path = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + doc.Folder.TranslatedPath + "/" + doc.TranslatedName;
                    node.Label = doc.CurrentDocLang.Label;
                }
            }
            else
            {
                node.Path = doc.Folder.Path + "/" + doc.PhysicalName; //datarow["Path_Folder"].ToString() + "/" + datarow["Name_Doc"].ToString();
                node.Label = doc.Nome;
            }
            node.CssClass = doc.CssClass;
            node.Restricted = currentFolder.RestrictedInt; //int.Parse(datarow["Restricted_Folder"].ToString());
            node.SystemName = doc.PhysicalName;//datarow["Name_Doc"].ToString();
            node.ContentID = doc.Content.ToString();//datarow["Content_DocLang"].ToString();
            node.Description = doc.Descrizione;//datarow["Desc_DocLang"].ToString();
            node.ShowInTopMenu = doc.ShowInTopMenu != 0; 

            switch (doc.Stato)
            {
                case 0:
                    node.Stato = NetCms.Front.Menu.MenuNode.MenuNodeState.Menu;
                    break;
                case 1:
                    node.Stato = NetCms.Front.Menu.MenuNode.MenuNodeState.Reachable;
                    break;
                case 2:
                    node.Stato = NetCms.Front.Menu.MenuNode.MenuNodeState.NonReachable;
                    break;
                case 4:
                    node.Stato = NetCms.Front.Menu.MenuNode.MenuNodeState.SubMenu;
                    break;
            }

            if (node.Type == 2)
                node.Link = CurrentAbsoluteFrontRoot.Instance + node.Path;
            if (node.Type == 9)
            {
                //DataRow link = NetFrontend.DAL.LinksDAL.GetDocumentRecordByID(node.ID);
                //if (link != null)
                PropertyInfo prop = doc.ContentRecord.GetType().GetProperty("Href");
                string linkValue = prop.GetValue(doc.ContentRecord, null).ToString();//link["Href_Link"].ToString();

                string tmpLinkValue = linkValue.Replace(NetCms.Structure.Utility.ImportContentFilter.WebRoot, "");

                if (tmpLinkValue.StartsWith("http://") || tmpLinkValue.StartsWith("https://"))
                    node.Link = tmpLinkValue;
                else
                {
                    if (string.IsNullOrEmpty(NetCms.Networks.NetworksManager.CurrentActiveNetwork.RootPath))
                        node.Link = (tmpLinkValue.StartsWith("/")) ? tmpLinkValue : "/" + tmpLinkValue;
                    else
                        node.Link = linkValue.Replace(NetCms.Structure.Utility.ImportContentFilter.WebRoot, NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot + (tmpLinkValue.StartsWith("/") ? "" : "/"));
                }
                
            }
        }

        protected override MenuNode[] BuildChildNodes()
        {
            return new MenuNode[0];
        }
        protected override int CountChildNodes()
        {
            return 0;
        }
    }
}