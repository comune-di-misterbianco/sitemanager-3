using System;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Data;
using System.Web.UI;
using System.Collections.Generic;
using NetCms.Front;
using G2Core.Caching;
/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>


namespace NetCms.Front.Menu
{
    public abstract class MenuNodeFiller
    {
        public PersistentCacheString CurrentAbsoluteFrontRoot
        {
            get
            {
                if (_CurrentAbsoluteFrontRoot == null)
                {
                    _CurrentAbsoluteFrontRoot = new PersistentCacheString("SM2_CurrentAbsoluteFrontRoot" , StoreTypes.Page);
                    _CurrentAbsoluteFrontRoot.InitializingMethod = () => { return NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot; };
                }
                return _CurrentAbsoluteFrontRoot;
            }
        }
        private PersistentCacheString _CurrentAbsoluteFrontRoot;

        public int ChildsCount
        {
            get
            {
                return (_ChildsCount ?? (_ChildsCount = new int?( CountChildNodes()))).Value;
            }
        }
        private int? _ChildsCount;
        
        public MenuNode Node { get; private set; }

        public MenuNodeFiller(MenuNode nodeToFill)
        {
            Node = nodeToFill;
        }

        public abstract void FillNode(string path, int networkId);
        public abstract void FillNode(int ID);
        public abstract void FillNode(object obj);

        protected abstract MenuNode[] BuildChildNodes();

        public MenuNode[] GetChildNodes()
        {
            NodesCache cache;
            IEnumerable<MenuNode> nodes = null;

            if (this.Node.Path != null)
            {
                cache = Node.Cache.PublicCache;                
                //nodes = cache.Values.Where(x => x.Path.Contains(this.Node.Path + "/") && x.ID != this.Node.ID);
                nodes = cache.Values.Where(x => x.Path.Contains(this.Node.Path) && x.ID != this.Node.ID);

                if (nodes.Count() == ChildsCount)
                return nodes.ToArray();
            else
                return BuildChildNodes();
            }
            return null;
        }

        protected abstract int CountChildNodes();
    }
}