using System;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Collections.Generic;



namespace NetCms.Front.Menu
{
    public abstract class MenuNode
    {
        public int ID { get; set; }
        public int Type { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public string SystemName { get; set; }
        public string NetworkSystemName { get; set; }
        public string Link { get; set; }
        public int Order { get; set; }
        public string CssClass { get; set; }

        public bool ShowInTopMenu { get; set; }

        public virtual bool AddToCache { get { return true; } }
        
        public MenuNodeType NodeType { get; set; }

        public MenuNodeState Stato { get; set; }
        
        public enum MenuNodeState
        {
            Menu,
            Reachable,
            NonReachable,
            SubMenu
        }
        
        public bool IsRootNode
        {
            get
            {
                return this.Depth == 1;
            }
        }
        public bool HasChildNodes
        {
            get
            {
                return _ChildNodes.Count > 0;
            }
        }      

        public string PageName
        {
            get
            {
                return NetCms.Configurations.Paths.PageName;
            }
        }

        private List<MenuNode> _ChildNodes;
        public virtual List<MenuNode> ChildNodes
        {
            get 
            {
                if (_ChildNodes == null)
                {
                    _ChildNodes = new List<MenuNode>();
                    MenuNode[] nodes =  GetFiller().GetChildNodes();
                    if (nodes != null)
                    {
                        RearrangeNodes(nodes);
                        foreach (MenuNode node in nodes)
                        {
                            ChildNodes.Add(node);
                            //node.ParentNode = this;
                        }
                    }
                }
                return _ChildNodes; 
            }
        }

        public virtual int Depth { get; set; }
        
        public NodesCacheManager Cache { get; private set; }

        public MenuNode()
        {
            Cache = new NodesCacheManager();
        }

        public void FillNode(string path,int networkId)
        {
            GetFiller().FillNode(path,networkId);

            if (this.Path != null && this.AddToCache && !Cache.PublicCache.ContainsKey(this.Path))
                AddNodeToCache();
        }

        public void FillNode(object obj)
        {
            GetFiller().FillNode(obj);
            if (this.AddToCache && !Cache.PublicCache.ContainsKey(this.Path))
                AddNodeToCache();
        }
        //public void FillNode(int id)
        //{
        //    GetFiller().FillNode(id);
        //    if (this.AddToCache && !Cache.PublicCache.ContainsKey(this.Path))
        //        AddNodeToCache();
        //}        
        
        public virtual void AddNodeToCache()
        {
            if (this.Path == null)
                throw new Exception(string.Format("Path null sull'istanza del nodo di men�: {0} {1}", this.GetType().FullName, this.ID));

            if (Cache == null)
                throw new Exception(string.Format("Cache null sull'istanza del nodo di men�: {0} {1}", this.GetType().FullName, this.ID));

            if (Cache.PublicCache == null)
                throw new Exception(string.Format("Cache.PublicCache null sull'istanza del nodo di men�: {0} {1}", this.GetType().FullName, this.ID));

            if (!string.IsNullOrEmpty(this.Path) && !Cache.PublicCache.ContainsKey(this.Path.ToLower())) // agginto controllo duplicati - in modo sporadico dava eccezione key gi� presente
                Cache.PublicCache.Add(this.Path.ToLower(), this);

        }

        public void RearrangeNodes(MenuNode[] nodes)
        {
            if (nodes != null)
            {
                int maxRange = nodes.Length;
                bool lastPass = true;
                while (maxRange > 0 && lastPass)
                {
                    lastPass = false;
                    for (int i = 0; i < maxRange - 1; i++)
                    {
                        if (nodes[i].Order < nodes[i + 1].Order)
                        {
                            #region Switch
                            MenuNode tmpNode = nodes[i];
                            nodes[i] = nodes[i + 1];
                            nodes[i + 1] = tmpNode;
                            lastPass = true;
                            #endregion
                        }
                    }
                    maxRange--;
                }
            }
        }

        public abstract MenuNodeFiller GetFiller();

        public abstract bool CheckSelected(ModelMenu menu);

        public abstract bool CheckAddToMenu();
        public abstract bool CheckAddToMenu(bool contextual = false);

        public abstract bool CheckAddDocs(ModelMenu menu);

        protected MenuNode GetParentNode() 
        {
            if (string.IsNullOrEmpty(this.Path)) return null;
            if (this.Path.Length <= 1) return null;
            if (this.Path.ToCharArray().Count(x => x == '/') <= 0) return null;

            string parentPath = this.Path.EndsWith("/") ? this.Path.Substring(0, this.Path.LastIndexOf('/') - 1) : this.Path;
            parentPath = parentPath.Substring(0, this.Path.LastIndexOf('/'));

            return Cache.PublicCache.ContainsKey(parentPath) ? Cache.PublicCache[parentPath] : BuildParentNode(parentPath);
        }

        protected abstract MenuNode BuildParentNode(string path);
        public void BuildParentsTree()
        {
            MenuNode parent = this;
            while (parent != null) parent = parent.GetParentNode();
        }

        public enum MenuNodeType
        {
            Normal,
            Responsive,
            Mobile,
            Megamenu,
            Contextual,
        }
    }
}