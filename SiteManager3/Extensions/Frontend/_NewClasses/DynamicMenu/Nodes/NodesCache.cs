using System.Web.UI.HtmlControls;
using System.Linq;
using System.Collections.Generic;
using NetCms.Front.Menu;
using G2Core.Caching;


/// <summary>
/// Summary description for Configs
/// </summary>

namespace NetCms.Front
{
    public class NodesCache : Dictionary<string, Menu.MenuNode> { }
    public class NodesCacheManager : Dictionary<string, Menu.MenuNode>
    {
        protected PersistentCacheObject<NodesCache> PublicCacheStore
        {
            get
            {
                if (_PublicCacheStore == null)
                {
                    _PublicCacheStore = new PersistentCacheObject<NodesCache>("SM2_" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.SystemName + "MenuCache", StoreTypes.Cache);
                }
                return _PublicCacheStore;
            }
        }
        private PersistentCacheObject<NodesCache> _PublicCacheStore;
        
        //protected PersistentCacheObject<NodesCache> RestrictedCacheStore
        //{
        //    get
        //    {
        //        if (_RestrictedCacheStore == null)
        //        {
        //            _RestrictedCacheStore = new PersistentCacheObject<NodesCache>("SM2_" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.SystemName + "MenuNodesRestrictedCache", StoreTypes.Session);
        //        }
        //        return _RestrictedCacheStore;
        //    }
        //}
        //private PersistentCacheObject<NodesCache> _RestrictedCacheStore;

        //public NodesCache RestrictedCache
        //{
        //    get { return RestrictedCacheStore.Instance; }
        //}

        public NodesCache PublicCache
        {
            get { return PublicCacheStore.Instance; }
        }

        [G2Core.Caching.Invalidation.InvalidationMethod("FrontendMenu")]
        public static void Invalidate(string networkSystemName)
        {
            if(NetCms.Networks.NetworksManager.CurrentActiveNetwork!=null)
                PersistentCacheObject<NodesCache>.Invalidate("SM2_" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.SystemName + "MenuCache", StoreTypes.Cache);
        }
    }
}