using System.Linq;
using System.Data;
using System.Collections.Generic;
using LanguageManager.BusinessLogic;

/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>
namespace NetCms.Front.Menu
{
    /// <summary>
    /// Menu Node Control Builder
    /// </summary>
    public class MenuNodeControlsBuilder
    {
        public string CurrentPath
        {
            get
            {
                return (GetCurrentContextFolderPath() + "/" + NetCms.Configurations.Paths.PageName).ToLower();
            }
        }

        public ModelMenu Menu { get; private set; }

       
        public MenuNodeControlsBuilder(ModelMenu menu)
        {
            Menu = menu;           
        }

        public string GetHtml(MenuNode node, bool isRootNode, string extraCssClass = null)
        {
            string childsHtml = GetChildsHtml(CurrentPath, node, isRootNode);
            string cssClass = !isRootNode && node.CheckSelected(Menu) ? " TabSelected" : "";

            if (!string.IsNullOrEmpty(extraCssClass))
                cssClass += " " + extraCssClass;

            string li = string.Format(BuildHtml(node, isRootNode), childsHtml, cssClass);

            return li;
        }
         
        protected string LabelHtml(MenuNode Node)
        {
            NetUtility.HtmlCleaner ht = new NetUtility.HtmlCleaner();

            string nodeDescr = "";
            if(!string.IsNullOrEmpty(Node.Description))
                nodeDescr = Node.Description;

            string linkTitle = (nodeDescr != null && nodeDescr.Length > 3) ? "title=\"" + ht.TruncateText(nodeDescr, 100)+"\""  : "";
            
            string printDescriptions = Menu.PrintDescriptions ? "<br /><em class=\"nodeDesc\">" + nodeDescr + "</em>" : "";
            
            string selected = Node.CheckSelected(Menu) ? " class=\"selected\"" : "";            
            
            string cssClass = Node.Type == 9 && (Node.Link.ToLower().StartsWith("http://")) ? " class=\"external\"" : "";
                        
            string label = string.Format(@"<a {4} {5} href=""{0}""><span><span {2}>{1}</span></span></a>{3}", Node.Link, Node.Label, selected, printDescriptions, cssClass, linkTitle);
            
            return label;
        }

        protected string GetChildsHtml(string currentPath, MenuNode Node, bool isRootNode)
        {
            string childs = "";
            if (Node != null)
            {
                bool isInRangeDepth = Menu.StartingLevel <= Node.Depth && (Node.Depth <= Menu.EndingLevel || Menu.EndingLevel == 0);


                int k = 0;

                if (isRootNode || (isInRangeDepth && (Menu.Explood || currentPath.StartsWith(LanguageBusinessLogic.GetUrlWithoutLanguage(Node.Path) + "/"))))
                    if (Node.ChildNodes != null)
                    {
                        int nodeChildsCount = Node.ChildNodes.Count;
                        
                        if (nodeChildsCount > 0)
                        {
                            IList<MenuNode> menuNode = Node.ChildNodes.Cast<MenuNode>().Where(x => x.CheckAddToMenu() || Node.CheckAddDocs(Menu)).ToList();

                            int lastNode = nodeChildsCount - 1;
                            foreach (MenuNode childNode in menuNode)
                            {

                                string extraCssClass = "";

                                if (k == 0)
                                    extraCssClass = "first";
                                if (k == lastNode)
                                    extraCssClass = "last";

                                childs += GetHtml(childNode, false, extraCssClass);
                                k++;
                            }
                        }
                    }
                if (!string.IsNullOrEmpty(childs))
                    childs = "<" + Menu.ListTag + " > " + childs + "</" + Menu.ListTag + ">";
            }
            return childs;
        }       

        protected virtual string BuildHtml(MenuNode node, bool isRootNode)
        {
            if (isRootNode) return "{0}";
            else
            {
                int relativeDepth = node.Depth - (Menu.StartingLevel + 1);

                string nodeCssClass = (node.CssClass != null) ? node.CssClass : "";

                string cssClass = "menu-item depth" + (relativeDepth) + "{1} " + nodeCssClass;
                string innerHtml = !isRootNode ? LabelHtml(node) : "";

                string li = string.Format(@"<li class=""{0}"">{1}{2}</li>"
                                          , cssClass
                                          , innerHtml
                                          , "{0}");
                return li;
            }
        }

        public string GetCurrentContextFolderPath()
        {
            return FrontendNetwork.GetCurrentFolderPath();
        }

        #region MegamenuHtml

       

        protected string GetMegaChildsHtml(string currentPath, MenuNode Node, bool isRootNode)
        {
            string childs = "";
            if (Node != null)
            {
                bool isInRangeDepth = Menu.StartingLevel <= Node.Depth && (Node.Depth <= Menu.EndingLevel || Menu.EndingLevel == 0);

                int k = 0;

                if (isRootNode || (isInRangeDepth && (Menu.Explood || currentPath.StartsWith(LanguageBusinessLogic.GetUrlWithoutLanguage(Node.Path) + "/"))))
                    if (Node.ChildNodes != null)
                    {
                        int nodeChildsCount = Node.ChildNodes.Count;

                        if (nodeChildsCount > 0)
                        {
                            IList<MenuNode> menuNode = Node.ChildNodes.Cast<MenuNode>().Where(x => x.CheckAddToMenu() || Node.CheckAddDocs(Menu)).ToList();

                            int lastNode = nodeChildsCount - 1;
                            foreach (MenuNode childNode in menuNode)
                            {
                                childs += GetMegaMenuHtml(childNode, false, "");
                                k++;
                            }
                        }
                    }
                if (!string.IsNullOrEmpty(childs))
                {
                    string SubChildCssClass = string.Empty;
                    bool addSubNavDiv = false;

                    if (Node.Depth == 2)
                    {
                        SubChildCssClass = "Megamenu-subnavGroup";
                        addSubNavDiv = true;
                    }

                    childs =  ((addSubNavDiv) ? "<div class=\"Megamenu-subnav u-jsDisplayNone\">" : "") 
                           + "<" + Menu.ListTag + string.Format(@" class=""{0}"" ", SubChildCssClass +"" + Node.CssClass) + " > " + childs + "</" + Menu.ListTag + ">"
                           + ((addSubNavDiv) ? "</div>" : "");
                    

                }
            }
            return childs;
        }

        public string GetMegaMenuHtml(MenuNode node, bool isRootNode, string extraCssClass = null)
        {
            string childsHtml = GetMegaChildsHtml(CurrentPath, node, isRootNode);

            string cssClass = !isRootNode && node.CheckSelected(Menu) ? " active" : "";

            if (!string.IsNullOrEmpty(extraCssClass))
                cssClass += " " + extraCssClass;

            string li = string.Format(BuildFogliaHtml(node, isRootNode,true), childsHtml, cssClass);

            return li;
        }

        protected string FogliaLabelHtml(MenuNode Node)
        {
            NetUtility.HtmlCleaner ht = new NetUtility.HtmlCleaner();

            string linkTitle = (Node.Description != null && Node.Description.Length > 3) ? "title=\"" + ht.TruncateText(Node.Description, 100) + "\"" : "";

            string printDescriptions = Menu.PrintDescriptions ? "<br /><em class=\"nodeDesc\">" + Node.Description + "</em>" : "";
            
            string cssClass = Node.Type == 9 && (Node.Link.ToLower().StartsWith("http://") || Node.Link.ToLower().StartsWith("https://")) ? " class=\"external\"" : "";            

            string link = (Node.Link != null) ? Node.Link : Node.Path;

            //if (Node.Type != 9)
            //{
            //    if (!link.ToLower().EndsWith(".aspx"))                
            //        link += (link.ToLower().EndsWith("/"))? "default.aspx" : "/default.aspx";                
            //}

            string label = string.Format(@"<a {3} {4} href=""{0}"">{1}</a>{2}", link, Node.Label, printDescriptions, cssClass, linkTitle);

            return label;
        }

        protected virtual string BuildFogliaHtml(MenuNode node, bool isRootNode, bool isMegaMenu, bool showParent = false)
        {            
            if (isRootNode) return "{0}";
            else
            {
                int relativeDepth = node.Depth - (Menu.StartingLevel + 1);

                string nodeCssClass = (node.CssClass != null) ? node.CssClass : "";

                string cssClassByDept = "";

                if (isMegaMenu && relativeDepth == 0)
                    cssClassByDept = "Megamenu-item";

                

                //string cssClass = "nav-item " +  cssClassByDept + " depth" + (relativeDepth) + "{1} " + nodeCssClass;
                string cssClass = cssClassByDept + " depth" + (relativeDepth) + "{1} " + nodeCssClass;


                string innerHtml = !isRootNode ? FogliaLabelHtml(node) : "";

                string li = string.Format(@"<li class=""{0}"">{1}{2}</li>"
                                          , cssClass
                                          , innerHtml
                                          , "{0}");
                return li;
            }
        }


        #endregion


        #region MobileMenu

        public string GetRootNodeHtml(MenuNode node)
        {
            string src = string.Empty;

            src = FogliaLabelHtml(node);            

            return src;
        }

        public string GetMobileMenuHtml(MenuNode node, bool isRootNode, string extraCssClass = null, bool contextualmenu = false, bool usedascontextual = false, bool usedinmodule = false)
        {
            string li = string.Empty;
            string childsHtml = "";
       
            childsHtml = GetMobileChildsHtml(CurrentPath, node, isRootNode, contextualmenu, usedascontextual, usedinmodule);

            string cssClass = !isRootNode && node.CheckSelected(Menu) ? " active" : "";

            if (!string.IsNullOrEmpty(extraCssClass))
                cssClass += " " + extraCssClass;
           
            li = string.Format(BuildFogliaHtml(node, isRootNode,false), childsHtml, cssClass);

            bool showParent = false;
            showParent = (usedascontextual && Menu.StartingLevel == node.Depth);
            if (showParent)
            {
                li = "<" + Menu.ListTag + @" class=""Linklist Linklist--padded Treeview Treeview--default js-Treeview sidebar-menu"">" 
                     + "<li class=\"Treeview-parent hasFocus\" aria-expanded=\"true\">"
                     + GetRootNodeHtml(node)
                     + li
                     + "</li>"
                     + "</" 
                     + Menu.ListTag + ">";
            }

            return li;
        }

        protected string GetMobileChildsHtml(string currentPath, MenuNode Node, bool isRootNode, bool contextualmenu = false, bool usedascontextual = false, bool usedinmodule = false)
        {
            string childs = "";
            if (Node != null)
            {
                bool isInRangeDepth = Menu.StartingLevel <= Node.Depth && (Node.Depth <= Menu.EndingLevel || Menu.EndingLevel == 0);

                int k = 0;

                if (isRootNode || (isInRangeDepth && (Menu.Explood || currentPath.StartsWith(LanguageBusinessLogic.GetUrlWithoutLanguage(Node.Path) + "/"))))
                    if (Node.ChildNodes != null)
                    {
                        int nodeChildsCount = Node.ChildNodes.Count;

                        if (nodeChildsCount > 0)
                        {
                            IList<MenuNode> menuNode = Node.ChildNodes.Cast<MenuNode>().Where(x => x.CheckAddToMenu(contextualmenu) || Node.CheckAddDocs(Menu)).ToList();

                            int lastNode = nodeChildsCount - 1;

                            IEnumerable<MenuNode> filteredNodes = menuNode;
                            
                            if (!usedinmodule)
                            {
                                filteredNodes = menuNode.Where(x => x.ShowInTopMenu);
                            }

                            //foreach (MenuNode childNode in menuNode.Where(x => x.ShowInTopMenu))
                            foreach (MenuNode childNode in filteredNodes)
                            {
                                string extraCssClass = "";

                                //if (k == 0)
                                //    extraCssClass = "first";
                                //if (k == lastNode)
                                //    extraCssClass = "last";
                             
                                childs += GetMobileMenuHtml(childNode, false, extraCssClass, contextualmenu, usedascontextual, usedinmodule);
                                k++;
                            }
                        }
                    }
                if (!string.IsNullOrEmpty(childs))
                {
                    string SubChildCssClass = string.Empty;
                    //bool addSubNavDiv = false;

                    //if (Node.Depth == 2)
                    //{
                    //    SubChildCssClass = "Megamenu-subnavGroup";
                    //    addSubNavDiv = true;
                    //}

                    //childs += (addSubNavDiv) ? "<div class=\"Megamenu-subnav u-jsDisplayNone\">" : "";
                    //childs = "<" + Menu.ListTag + string.Format(@" class=""{0}"" ", SubChildCssClass + "" + Node.CssClass) + " > " + childs + "</" + Menu.ListTag + ">";
                    //childs += (addSubNavDiv) ? "</div>" : "";


                    childs = "<" + Menu.ListTag + string.Format(@" class=""{0}"" ", SubChildCssClass + "" + Node.CssClass) + " > " + childs + "</" + Menu.ListTag + ">";
                           


                }
            }
            return childs;
        }

        #endregion

        #region ResponsiveHtml

        protected string ResponsiveLabelHtml(MenuNode Node)
        {
            NetUtility.HtmlCleaner ht = new NetUtility.HtmlCleaner();
            string linkTitle = (Node.Description != null && Node.Description.Length > 3) ? "title=\"" + ht.TruncateText(Node.Description, 100) + "\"" : "";
            string printDescriptions = Menu.PrintDescriptions ? "<br /><em class=\"nodeDesc\">" + Node.Description + "</em>" : "";
            string cssClass = Node.Type == 9 && Node.Link.ToLower().StartsWith("http://") ? " class=\"external\"" : "";
            string label = string.Format(@"<a {3} {4} href=""{0}""><span>{1}</span></a>{2}", Node.Link, Node.Label, printDescriptions, cssClass, linkTitle);
            return label;
        }

        public string GetResponsiveHtml(MenuNode node, bool isRootNode, string extraCssClass = null)
        {
            string childsHtml = GetResonsiveChildsHtml(CurrentPath, node, isRootNode);
            string cssClass = !isRootNode && node.CheckSelected(Menu) ? " active" : "";

            if (!string.IsNullOrEmpty(extraCssClass))
                cssClass += " " + extraCssClass;

            string li = string.Format(BuildResponsiveHtml(node, isRootNode), childsHtml, cssClass);

            return li;
        }

        protected string GetResonsiveChildsHtml(string currentPath, MenuNode Node, bool isRootNode)
        {
            string childs = "";
            if (Node != null)
            {
                bool isInRangeDepth = Menu.StartingLevel <= Node.Depth && (Node.Depth <= Menu.EndingLevel || Menu.EndingLevel == 0);
                    
                int k = 0;

                if (isRootNode || (isInRangeDepth && (Menu.Explood || currentPath.StartsWith(LanguageBusinessLogic.GetUrlWithoutLanguage(Node.Path) + "/"))))
                    if (Node.ChildNodes != null)
                    {
                        int nodeChildsCount = Node.ChildNodes.Count;

                        if (nodeChildsCount > 0)
                        {
                            IList<MenuNode> menuNode = Node.ChildNodes.Cast<MenuNode>().Where(x => x.CheckAddToMenu() || Node.CheckAddDocs(Menu)).ToList();

                            int lastNode = nodeChildsCount - 1;
                            foreach (MenuNode childNode in menuNode)
                            {

                                string extraCssClass = "";

                                if (k == 0)
                                    extraCssClass = "first";
                                if (k == lastNode)
                                    extraCssClass = "last";

                                childs += GetResponsiveHtml(childNode, false, extraCssClass);
                                k++;
                            }
                        }
                    }
                if (!string.IsNullOrEmpty(childs))
                    childs = "<" + Menu.ListTag + string.Format(@" class=""{0}"" ", Node.CssClass) + " > " + childs + "</" + Menu.ListTag + ">";
            }
            return childs;
        }

        protected virtual string BuildResponsiveHtml(MenuNode node, bool isRootNode)
        {
            if (isRootNode) return "{0}";
            else
            {
                int relativeDepth = node.Depth - (Menu.StartingLevel + 1);

                string nodeCssClass = (node.CssClass != null) ? node.CssClass : "";

                string cssClass = "menu-item depth" + (relativeDepth) + "{1} " + nodeCssClass;
                string innerHtml = !isRootNode ? ResponsiveLabelHtml(node) : "";

                string li = string.Format(@"<li class=""{0}"">{1}{2}</li>"
                                          , cssClass
                                          , innerHtml
                                          , "{0}");
                return li;
            }
        }
        #endregion



        #region WebDocInsideHTML

        protected string WebDocInsideLabelHtml(MenuNode Node)
        {
            NetUtility.HtmlCleaner ht = new NetUtility.HtmlCleaner();

            string linkTitle = "<h4 class=\"list-group-item-heading\">"+ Node.Label + "</h4>";

            string printDescriptions = Menu.PrintDescriptions ? "<p class=\"list-group-item-text nodeDesc\">" + Node.Description + "</p>" : "<p class=\"list-group-item-text nodeDesc\"></p>";

            string cssClass = Node.Type == 9 && Node.Link.ToLower().StartsWith("http://") ? " class=\"list-group-item external  %AdditionalCSS% \"" : " class=\"list-group-item %AdditionalCSS% \"";
          
            string label = string.Format(@"<a {3} href=""{0}"">{1} {2}</a>", Node.Link, linkTitle, printDescriptions, cssClass);
            
            return label;       
        }

        public string GetWebDocInsideHtml(MenuNode node, bool isRootNode, string extraCssClass = null)
        {
            string childsHtml = GetWebDocInsideChildsHtml(CurrentPath, node, isRootNode);
            string cssClass = !isRootNode && node.CheckSelected(Menu) ? " active" : "";

            if (!string.IsNullOrEmpty(extraCssClass))
                cssClass += " " + extraCssClass;

            string li = string.Format(BuildWebDocInsideHtml(node, isRootNode), childsHtml, cssClass);

            return li;
        }

        protected string GetWebDocInsideChildsHtml(string currentPath, MenuNode Node, bool isRootNode)
        {
            string childs = "";
            if (Node != null)
            {
                bool isInRangeDepth = Menu.StartingLevel <= Node.Depth && (Node.Depth <= Menu.EndingLevel || Menu.EndingLevel == 0);

                int k = 0;

                if (isRootNode || (isInRangeDepth && (Menu.Explood || currentPath.StartsWith(LanguageBusinessLogic.GetUrlWithoutLanguage(Node.Path) + "/"))))
                    if (Node.ChildNodes != null)
                    {
                        int nodeChildsCount = Node.ChildNodes.Count;

                        if (nodeChildsCount > 0)
                        {
                            IList<MenuNode> menuNode = Node.ChildNodes.Cast<MenuNode>().Where(x => x.CheckAddToMenu() || Node.CheckAddDocs(Menu)).ToList();

                            int lastNode = nodeChildsCount - 1;
                            foreach (MenuNode childNode in menuNode)
                            {

                                string extraCssClass = "";

                                if (k == 0)
                                    extraCssClass = "first";
                                if (k == lastNode)
                                    extraCssClass = "last";

                                childs += GetWebDocInsideHtml(childNode, false, extraCssClass);
                                k++;
                            }
                        }
                    }
                if (!string.IsNullOrEmpty(childs))
                    childs = "<" + Menu.ListTag + string.Format(@" class=""{0}"" ", "list-group " + Node.CssClass) + " > " + childs + "</" + Menu.ListTag + ">";
            }
            return childs;
        }

        protected virtual string BuildWebDocInsideHtml(MenuNode node, bool isRootNode)
        {
            if (isRootNode) return "{0}";
            else
            {
                int relativeDepth = node.Depth - (Menu.StartingLevel + 1);

                string nodeCssClass = (node.CssClass != null) ? node.CssClass : "";

                string cssClass = "depth" + (relativeDepth) + "{1} " + nodeCssClass;

                string innerHtml = !isRootNode ? WebDocInsideLabelHtml(node) : "";


                return innerHtml.Replace("%AdditionalCSS%", cssClass) + "{0}";





                //string li = string.Format(@"<li class=""{0}"">{1}{2}</li>"
                //                          , cssClass
                //                          , innerHtml
                //                          , "{0}");
                //return li;
            }
        }
        #endregion

    }
}