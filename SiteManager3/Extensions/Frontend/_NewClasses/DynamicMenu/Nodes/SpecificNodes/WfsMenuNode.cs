using System;
using System.Web.UI.HtmlControls;
using System.Data;
using NetFrontend;
/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>

namespace NetCms.Front.Menu
{
    public abstract class WfsMenuNode:MenuNode
    {

        public int Restricted { get; set; }

        public WfsMenuNode()
            :base()
        {
        }

        protected override MenuNode BuildParentNode(string path)
        {
            FolderMenuNode node = new FolderMenuNode();
            RootMenuNodeFiller filler = new RootMenuNodeFiller(node);
            
            filler.FillNode("");
            return node;
        }
    }

}