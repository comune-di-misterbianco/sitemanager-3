using System;
using System.Web.UI.HtmlControls;
using System.Data;
using NetFrontend;
using LanguageManager.BusinessLogic;
/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>


namespace NetCms.Front.Menu
{
    public class FolderMenuNode : WfsMenuNode
    {
    //{
    //    public FrontendFolder Folder
    //    {
    //        get
    //        {
    //            return _Folder ?? (_Folder = FrontendNetwork.GetCurrentFrontendNetwork().RootFolder.FindFolder(this.ID.ToString()));
    //        }
    //    }
    //    private FrontendFolder _Folder;

        public string Tree { get; set; }

        public FolderMenuNode()
        {            
        }

        public override bool CheckAddToMenu()
        {
            return this.Stato == MenuNodeState.Menu && (
                                                    NetCms.Configurations.Generics.EnableFrontMenuGrants ?
                                                    FrontendFolder.CurrentUserHasGrants(this.ID.ToString(), LanguageBusinessLogic.GetUrlWithoutLanguage(this.Path)) :
                                                    !FrontendFolder.IsFolderRestricted(this.ID.ToString())
                                                );
        }


        public override bool CheckAddToMenu(bool contextualmenu = false)
        {            
            if (!contextualmenu)
                return this.Stato == MenuNodeState.Menu && (
                                                            NetCms.Configurations.Generics.EnableFrontMenuGrants ?
                                                            FrontendFolder.CurrentUserHasGrants(this.ID.ToString(), LanguageBusinessLogic.GetUrlWithoutLanguage(this.Path)) :
                                                            !FrontendFolder.IsFolderRestricted(this.ID.ToString())
                                                        );
            else
                return (this.Stato == MenuNodeState.Menu || this.Stato == MenuNodeState.SubMenu) && (
                                                            NetCms.Configurations.Generics.EnableFrontMenuGrants ? 
                                                            FrontendFolder.CurrentUserHasGrants(this.ID.ToString(), LanguageBusinessLogic.GetUrlWithoutLanguage(this.Path)) :
                                                            !FrontendFolder.IsFolderRestricted(this.ID.ToString())
                                                        );
        }
        public override bool CheckAddDocs(ModelMenu menu)
        {
            int start = menu.StartingLevel;
            int end = menu.EndingLevel;
            bool explood = menu.Explood;

            bool result = (this.Type == 2 || this.Type == 1 || this.Type == 9) && menu.ShowPages;

            if (!this.IsRootNode)
            {
                result = result && (explood || (this.ChildNodes.Count > 0 || (menu.Path + "/").StartsWith(LanguageBusinessLogic.GetUrlWithoutLanguage(this.Path).Replace("'", "''") + "/")));
                result = result && (this.Depth + 1 < end || end == 0 || explood);
            }
            return result;
        }
        public override bool CheckSelected(ModelMenu menu)
        {
            string CurentPath = (menu.Path.Replace("''", "'") + PageName).ToLower();
            string langParsedPath = LanguageBusinessLogic.GetUrlWithoutLanguage(this.Path);
            string TestPath = (LanguageBusinessLogic.GetUrlWithoutLanguage(this.Path) + "/default.aspx").ToLower();

            bool selected = TestPath == CurentPath || CurentPath.StartsWith(langParsedPath + "/");
            //|| (menu.FirstLevelAllwaysVisible && menu.PathArray.Length > this.Depth + 1 && menu.PathArray[this.Depth - 1].Replace("''", "'").ToLower() == this.SystemName.ToLower());

            return selected;
        }

        public override MenuNodeFiller GetFiller()
        {
            return new FolderMenuNodeFiller(this);
        }
    }
}