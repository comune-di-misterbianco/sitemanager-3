using System;
using System.Web;
using System.Data;
using NetFrontend;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using NetCms.Structure.WebFS;
using System.Reflection;
using LanguageManager.BusinessLogic;

namespace NetCms.Front.Menu
{
    public class DocMenuNode:WfsMenuNode
    {
    //    public FrontendDocument Document
    //    {
    //        get
    //        {
    //            return _Document ?? (_Document = FrontendNetwork.GetCurrentFrontendNetwork().RootFolder.FindDocument(this.ID.ToString()));
    //        }
    //    }
    //    private FrontendDocument _Document;
        
        public string ContentID {get; set;}
        public override bool AddToCache { get { return false; } }
        


        public DocMenuNode()
        {
        }
        
        public override MenuNodeFiller GetFiller()
        {
            return new DocMenuNodeFiller(this);
        }

        public override bool CheckSelected(ModelMenu menu)
        {
            bool selected = false;
            string CurentPath = (menu.Path.Replace("''", "'") + PageName).ToLower();
            string langParsedPath = LanguageBusinessLogic.GetUrlWithoutLanguage(this.Path);
            string TestPath = langParsedPath.ToLower();
            
            selected = TestPath == CurentPath || CurentPath.StartsWith(langParsedPath) 
            /*||(menu.FirstLevelAllwaysVisible && menu.PathArray.Length > this.Depth + 1 && menu.PathArray[this.Depth + 1].Replace("''", "'").ToLower() == this.SystemName.ToLower())*/
            ;

            if (!selected && this.Type == 9 && FrontendNetwork.GetCurrentFrontendNetwork().HasCurrentDocument)
            {
                string[] paths = { langParsedPath };
                string[] paths1 = LanguageBusinessLogic.GetUrlWithoutLanguage(HttpContext.Current.Request.Url.ToString()).Split(paths, StringSplitOptions.None);

                Document link = DocumentBusinessLogic.GetDocumentRecordById(this.ID);
                //DataRow link = NetFrontend.DAL.LinksDAL.GetDocumentRecordByID(this.ID);
                if (link != null)
                {
                    PropertyInfo prop = link.ContentRecord.GetType().GetProperty("Href");
                    
                    string[] paths2 = prop.GetValue(link.ContentRecord, null).ToString().Split(paths, StringSplitOptions.None);
                    if (paths1.Length > 1 && paths2.Length > 1)
                    {
                        string p1 = paths1[1];
                        string p2 = paths2[1];
                        selected = p1 == p2 || (p1 == "/default.aspx" && p2 == string.Empty) || (p2 == "/default.aspx" && p1 == string.Empty);
                    }
                }
            }

            return selected;
        }
        public override bool CheckAddToMenu()
        {
            var addToMenu = this.Type.ToString() == "2" || this.Type.ToString() == "9";

            addToMenu &= (this.Stato == MenuNodeState.Menu);



            addToMenu &= NetCms.Configurations.Generics.EnableFrontMenuGrants ? 
                         FrontendDocument.CurrentUserHasGrants(this.ID.ToString()) :
                         !FrontendDocument.IsDocumentRestricted(this.ID.ToString());

            return addToMenu;
        }

        public override bool CheckAddToMenu(bool contextualmenu = false)
        {
            var addToMenu = this.Type.ToString() == "2" || this.Type.ToString() == "9";

            if (contextualmenu) 
                addToMenu &= (this.Stato == MenuNodeState.Menu || this.Stato == MenuNodeState.SubMenu);
            else
              addToMenu &= (this.Stato == MenuNodeState.Menu);

            addToMenu &= NetCms.Configurations.Generics.EnableFrontMenuGrants ?
                         FrontendDocument.CurrentUserHasGrants(this.ID.ToString()) :
                         !FrontendDocument.IsDocumentRestricted(this.ID.ToString());

            return addToMenu;
        }

        public override bool CheckAddDocs(ModelMenu menu)
        {
            return false;
        }
    }
}