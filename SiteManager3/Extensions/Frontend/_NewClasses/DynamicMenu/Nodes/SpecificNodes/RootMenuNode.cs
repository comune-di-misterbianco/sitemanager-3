using System;
using System.Web.UI.HtmlControls;
using System.Data;
using NetFrontend;
using LanguageManager.BusinessLogic;
/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>


namespace NetCms.Front.Menu
{
    public class RootMenuNode : FolderMenuNode
    {
        public RootMenuNode()
        {
        }        

        public override bool CheckAddToMenu()
        {
            var addToMenu = (Restricted < 1);
            addToMenu &= (this.Stato == MenuNodeState.Menu || this.Stato == MenuNodeState.Reachable);

            return addToMenu;
        }
        public override bool CheckAddDocs(ModelMenu menu)
        {
            bool result = (this.Type == 2 || this.Type == 1 || this.Type == 9) && menu.ShowPages;

            if (!this.IsRootNode)
            {
                result = result && (menu.Explood || (this.ChildNodes.Count > 0 || LanguageBusinessLogic.GetUrlWithoutLanguage(this.Path).Replace("'", "''") + "/" == menu.Path));
                result = result && (this.Depth + 1 < menu.EndingLevel || menu.EndingLevel == 0 || menu.Explood);
            }
            return result;
        }
        public override bool CheckSelected(ModelMenu menu)
        {
            string CurentPath = (menu.Path.Replace("''", "'") + PageName).ToLower();
            string langParsedPath = LanguageBusinessLogic.GetUrlWithoutLanguage(this.Path);
            string TestPath = (LanguageBusinessLogic.GetUrlWithoutLanguage(this.Path) + "/default.aspx").ToLower();

            return TestPath == CurentPath || CurentPath.Contains(langParsedPath + "/");/* ||
            (menu.FirstLevelAllwaysVisible && menu.PathArray.Length > this.Depth + 1 && menu.PathArray[this.Depth + 1].Replace("''", "'").ToLower() == this.SystemName.ToLower());*/
        }

        public override MenuNodeFiller GetFiller()
        {
            return new RootMenuNodeFiller(this);
        }
    }
}