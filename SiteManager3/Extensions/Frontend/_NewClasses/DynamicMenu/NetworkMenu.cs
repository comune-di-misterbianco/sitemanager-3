using System.Linq;
using NetCms.Front.Menu;
using System.Web.UI;
using LanguageManager.BusinessLogic;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;
using System;

/// <summary>
/// Summary description for Configs
/// </summary>

namespace NetCms.Front
{
    /// <summary>
    /// NetworkMenu
    /// </summary>
    public class NetworkMenu : ModelMenu
    {
        /// <summary>
        /// Node Cache Manager
        /// </summary>
        protected NodesCacheManager Cache
        {
            get
            {
                if (_Cache == null)
                {
                    _Cache = new NodesCacheManager();
                }
                return _Cache;
            }
        }
        private NodesCacheManager _Cache;

        public NetworkMenu()
            : base()
        {
            
        }

        public NetworkMenu(bool needToClearCache = false)
            : base()
        {
            if (needToClearCache)
                this.Cache.PublicCache.Clear();
        }

        public string GetCurrentFolderPathByBranchLevel(string currentPath, int startingLevel)
        {
            if (string.IsNullOrEmpty(currentPath)) return null;
            int slashCount = currentPath.ToCharArray().Count(x => x == '/');
            if (slashCount < startingLevel - 1) return null;

            string parentPath = currentPath.EndsWith("/") ? currentPath.Substring(0, currentPath.LastIndexOf('/') - 1) : currentPath;

            for (int i = startingLevel - 1; i < slashCount; i++)
                parentPath = parentPath.Substring(0, parentPath.LastIndexOf('/'));

            return parentPath;
        }

        public MenuNode FindCurrentBranch(int startingLevel, MenuNode.MenuNodeType nodetype)
        {
            MenuNode node = null;


            string currentPath = "";
            if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang == 0)
            {
                currentPath = (Front.FrontendNetwork.GetCurrentFolderPathTranslated()).ToLower();
            }
            else
            {
                currentPath = (Front.FrontendNetwork.GetCurrentFolderPath()).ToLower();
            }

            if (currentPath == "/")
                currentPath = "";

            if (currentPath.Length > 1)
                currentPath = GetCurrentFolderPathByBranchLevel(currentPath, startingLevel);

            //Se � abilitato il multilanguage, aggiungo informazione sulla lingua corrente in modo da prendere dalla cache il valore corretto del men�
            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                if (LanguageBusinessLogic.CurrentLanguage.Default_Lang == 0 && currentPath.Length > 0)
                    currentPath = "/" + LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode + currentPath;
            }
           

            if (nodetype != MenuNode.MenuNodeType.Contextual)
            {
                if (Cache.PublicCache.ContainsKey(currentPath))
                    node = Cache.PublicCache[currentPath];

                if (node == null)
                {
                    if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang == 0)
                    {
                        string PageName = DocumentBusinessLogic.TranslateNameOnTheFly(LanguageBusinessLogic.CurrentLanguage.ID, FrontendNetwork.GetCurrentFrontendNetwork().ID, NetCms.Configurations.Paths.PageName.ToLower(), currentPath);
                        string currentPathWithPagename = currentPath + "/" + PageName.ToLower();
                        if (Cache.PublicCache.ContainsKey(currentPathWithPagename))
                            node = Cache.PublicCache[currentPathWithPagename];
                    }
                    else {                        
                        string currentPathWithPagename = currentPath + "/" + NetCms.Configurations.Paths.PageName.ToLower();
                        if (Cache.PublicCache.ContainsKey(currentPathWithPagename))
                            node = Cache.PublicCache[currentPathWithPagename];
                    }
                    
                }
            }
            if (node == null)
            {
                MenuNode rootNode = new RootMenuNode();

                rootNode.NodeType = nodetype;

                rootNode.FillNode(currentPath, FrontendNetwork.GetCurrentFrontendNetwork().ID);
                rootNode.BuildParentsTree();
                return rootNode;
            }

            return node;
        }

        protected override Control BuildMenu()
        {
            ShowPages = true;

            MenuNodeControlsBuilder builder = new MenuNodeControlsBuilder(this);
            MenuNode rootNode = FindCurrentBranch(StartingLevel, MenuNode.MenuNodeType.Normal);

            string html = builder.GetHtml(rootNode, true);

            return new LiteralControl(html);
        }

        #region ResponsiveHtml
        protected override Control BuildResponsiveMenu()
        {
            ShowPages = true;

            MenuNodeControlsBuilder builder = new MenuNodeControlsBuilder(this);
            MenuNode rootNode = FindCurrentBranch(StartingLevel, MenuNode.MenuNodeType.Responsive);
            rootNode.CssClass = "nav navbar-nav";

            string html = builder.GetResponsiveHtml(rootNode, true);

            return new LiteralControl(html);
        }
        #endregion

        #region MegamenuHtml

        protected override Control BuildMegamenu()
        {
            ShowPages = true;

            MenuNodeControlsBuilder builder = new MenuNodeControlsBuilder(this);
            MenuNode rootNode = FindCurrentBranch(StartingLevel, MenuNode.MenuNodeType.Megamenu);
            rootNode.CssClass = "Megamenu-list";

            string html = builder.GetMegaMenuHtml(rootNode, true);

            return new LiteralControl(html);
        }


        #endregion

        #region MobileMenu
        
        protected override Control BuildMobileMenu(string cssClassForRootElement = "")
        {
            ShowPages = true;

            MenuNodeControlsBuilder builder = new MenuNodeControlsBuilder(this);
            MenuNode rootNode = FindCurrentBranch(StartingLevel, MenuNode.MenuNodeType.Mobile);
            if (!this.UsedAsContextual)
            rootNode.CssClass = cssClassForRootElement; // "Linklist Linklist--padded Treeview Treeview--default js-Treeview";

            string html = builder.GetMobileMenuHtml(rootNode, true, (UsedAsContextual ? cssClassForRootElement : ""), false, this.UsedAsContextual, this.UsedInModule);

            return new LiteralControl(html);
        }

        #endregion

        protected override Control BuildContextMenu(string cssClass)
        {
            ShowPages = true;

            MenuNodeControlsBuilder builder = new MenuNodeControlsBuilder(this);
            MenuNode rootNode = FindCurrentBranch(StartingLevel, MenuNode.MenuNodeType.Contextual);
            rootNode.CssClass = cssClass;

            string html = builder.GetMobileMenuHtml(rootNode, true, "", true, true, false);

            return new LiteralControl(html);
        }

        //public enum MenuType
        //{
        //    Normal,
        //    Responsive,
        //    Mobile,
        //    Megamenu,
        //    Contextual,
        //}

        #region WebDocInsideHtml

        protected override Control BuildInsideWebDocMenu()
        {
            ShowPages = true;

            MenuNodeControlsBuilder builder = new MenuNodeControlsBuilder(this);
            MenuNode rootNode = FindCurrentBranch(StartingLevel, MenuNode.MenuNodeType.Normal);
            rootNode.CssClass = "";

            string html = builder.GetWebDocInsideHtml(rootNode, true);

            return new LiteralControl(html);
        }
        #endregion

        protected override Control BuildMenuWithTpl(string cssClassForRootElement = "")
        {
            string src = string.Empty;
            // TODO: Da migliorare il modo in cui si richiede il tpl e si ricava il path
            string tpl_path = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteRoot + TplPath + "/commons/topmenu.tpl");

            MenuNodeControlsBuilder builder = new MenuNodeControlsBuilder(this);
            MenuNode rootNode = FindCurrentBranch(StartingLevel, MenuNode.MenuNodeType.Mobile);
            if (!this.UsedAsContextual)
                rootNode.CssClass = cssClassForRootElement;

            // voci di menu di primo livello
            var items = (from t in rootNode.ChildNodes
                         where t != null && t.ShowInTopMenu
                         select new { label = t.Label,
                                     url = t.Link,
                                     description = t.Description,
                                     cssclass =  t.CssClass,
                                     havechild = (t.ChildNodes != null && t.ChildNodes.Any()), 
                                     isactive = t.CheckSelected(this),
                                     childs = (from child in t.ChildNodes.OrderByDescending(x=>x.Order)  //voci di secondo livello
                                               where child != null && child.ShowInTopMenu && child.Stato == 0
                                               select new {
                                                   label = child.Label,
                                                   url = child.Link,
                                                   description = child.Description,
                                                   havechild = (child.ChildNodes != null && child.ChildNodes.Any()),
                                                   cssclass = child.CssClass,
                                                   isactive = child.CheckSelected(this),
                                                   childs = (from subitem in child.ChildNodes
                                                             where subitem != null && subitem.ShowInTopMenu && subitem.Stato == 0
                                                             select new
                                                             {
                                                                 label = subitem.Label,
                                                                 url = subitem.Link,
                                                                 description = subitem.Description,
                                                                 cssclass = subitem.CssClass,
                                                                 isactive = child.CheckSelected(this)
                                                             }
                                                             )
                                               }
                                               
                                               )
                        }
                        );

            object menu = new { items = items, usemegamenu = this.PrintAsMegamenu };

            var template = Scriban.Template.Parse(System.IO.File.ReadAllText(tpl_path), tpl_path);

            try
            {

                if (template.HasErrors)
                {
                    foreach (var error in template.Messages)
                        src += error.Message + System.Environment.NewLine;
                }
                else
                {
                    var context = new Scriban.TemplateContext();
                    context.PushCulture(System.Globalization.CultureInfo.CurrentCulture);

                    var scriptObj = new Scriban.Runtime.ScriptObject();
                    scriptObj.Add("menu", menu);
                    context.PushGlobal(scriptObj);

                    src = template.Render(context);
                }
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Topmenu: template parsing error", "", "", "", ex);
            }

            LiteralControl ctrl = new LiteralControl(src);

            return ctrl;
        }
    }
}