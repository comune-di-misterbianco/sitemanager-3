using System.Web.UI.HtmlControls;
using System.Web.UI;

/// <summary>
/// Summary description for Configs
/// </summary>

namespace NetCms.Front
{
    /// <summary>
    /// Menu Model
    /// </summary>
    public abstract class ModelMenu
    {      
        #region Path Variables
		
        private string _Path;
        public string Path
        {
            get
            {
                if (_Path == null)
                    initPathVariables();
                return _Path;
            }
        }

        private string[] _PathArray;
        public string[] PathArray
        {
            get
            {
                if (_PathArray == null)
                    initPathVariables();
                return _PathArray;
            }
        }

        private int _PageDepth = -1;
        public int PageDepth
        {
            get
            {
                if (_PageDepth == -1)
                    initPathVariables();
                return _PageDepth;
            }
        }


        private void initPathVariables()
        {
            _Path = Front.FrontendNetwork.GetCurrentFolderPath().ToLower().Replace("'","''");
            if (_Path.Length > 0)
                _Path += "/";
            _PathArray = _Path.Split('/');
            _PageDepth = PathArray.Length + 1;
        }

	    #endregion

        public string OwnerControlID
        {
            get
            {
                if (string.IsNullOrEmpty(_OwnerControlID))
                    _OwnerControlID = this.GetHashCode().ToString();
                return _OwnerControlID;
            }
        }
        protected string _OwnerControlID;

        private int _MaxDepth;
        public int MaxDepth
        {
            get
            {
                if (_MaxDepth == 0)
                    _MaxDepth = 0;//Da implementare
                return _MaxDepth;
            }
        }

        public enum ListTypes
        {
            OL,
            UL,
            Div
        }
        
        public bool ShowPages { get; set; }
        public bool Explood { get; set; }           
        public bool UsedAsContextual { get; set; }

        public bool UsedInModule { get; set; }

        public bool PrintAsMegamenu { get; set; }

        public int EndingLevel { get; set; }
        public int StartingLevel  { get; set; }
        public bool PrintDescriptions  { get; set; }	
        public bool FirstLevelAllwaysVisible  { get; set; }

        protected int LastDepth = 0;
        
        public ListTypes ListType { get; set; }

        public string TplPath { get; set; }

        public string ListTag
        {
            get
            {
                switch (ListType)
                {
                    case ListTypes.OL:
                        return "ol";
                    case ListTypes.UL:
                        return "ul";
                    case ListTypes.Div:
                        return "div";
                    default:   
                    return "ul";
                }
                   
            }
        }

        protected ModelMenu()
        {
            ListType = ListTypes.UL;
        }

        public Control GetMenu(string ownerControlID)
        {
            return GetMenu(ownerControlID,1,MaxDepth,false);
        }
        public Control GetMenu(string ownerControlID, int startingLevel)
        {
            return GetMenu(ownerControlID, startingLevel, MaxDepth, false);
        }
        public Control GetMenu(string ownerControlID, int startingLevel, int endingLevel)
        {
            return GetMenu(ownerControlID, startingLevel, endingLevel, false);
        }
        public Control GetMenu(string ownerControlID, int startingLevel, int endingLevel, bool explood)
        {
            _OwnerControlID = ownerControlID;

            this.StartingLevel = startingLevel;
            this.EndingLevel = endingLevel;
            this.Explood = explood;

            return BuildMenu();
        }
               
        protected abstract Control BuildMenu();

        #region ResponsiveHtml

        public Control GetResponsiveMenu(string ownerControlID, int startingLevel, int endingLevel, bool explood = false)
        {
            _OwnerControlID = ownerControlID;

            this.StartingLevel = startingLevel;
            this.EndingLevel = endingLevel;
            this.Explood = explood;

            return BuildResponsiveMenu();
        }

        public Control GetMegamenu(string ownerControlID, int startingLevel, int endingLevel, bool explood = false)
        {
            _OwnerControlID = ownerControlID;

            this.StartingLevel = startingLevel;
            this.EndingLevel = endingLevel;
            this.Explood = explood;

            return BuildMegamenu();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ownerControlID"></param>
        /// <param name="startingLevel"></param>
        /// <param name="endingLevel"></param>
        /// <param name="explood"></param>
        /// <param name="useascontextual"></param>
        /// <returns></returns>
        public Control GetMobileMenu(string ownerControlID, int startingLevel, int endingLevel, bool explood = false, bool usedascontextual = false, bool usedinmodule = false, string cssClassForRootElement = "")
        {
            _OwnerControlID = ownerControlID;

            this.StartingLevel = startingLevel;
            this.EndingLevel = endingLevel;
            this.Explood = explood;
            this.UsedAsContextual = usedascontextual;
            this.UsedInModule = usedinmodule;

            return BuildMobileMenu(cssClassForRootElement);
        }

        public Control GetContextMenu(string ownerControlID, int startingLevel, int endingLevel, bool explood = false, string cssClass = "")
        {
            _OwnerControlID = ownerControlID;

            this.StartingLevel = startingLevel;
            this.EndingLevel = endingLevel;
            this.Explood = explood;

            return BuildContextMenu(cssClass);
        }

        protected abstract Control BuildResponsiveMenu();
        protected abstract Control BuildMegamenu();
        protected abstract Control BuildMobileMenu(string cssClassForRootElement);

        protected abstract Control BuildContextMenu(string cssClass);
        
        
        protected abstract Control BuildMenuWithTpl(string cssClass);

        #endregion


        #region WebDocInsideMenu 

        public Control GetInsideWebDocMenu(string ownerControlID, int startingLevel, int endingLevel, bool explood = false)
        {
            _OwnerControlID = ownerControlID;

            this.StartingLevel = startingLevel;
            this.EndingLevel = endingLevel;
            this.Explood = explood;

            return BuildInsideWebDocMenu();
        }

        protected abstract Control BuildInsideWebDocMenu();

        #endregion



        public Control GetMenuWithTemplate(string ownerControlID, int startingLevel, int endingLevel, bool explood = false, bool usedascontextual = false, bool usedinmodule = false, bool printasMegamenu = false, string cssClassForRootElement = "")
        {
            _OwnerControlID = ownerControlID;

            this.StartingLevel = startingLevel;
            this.EndingLevel = endingLevel;
            this.Explood = explood;
            this.UsedAsContextual = usedascontextual;
            this.UsedInModule = usedinmodule;
            this.PrintAsMegamenu = printasMegamenu;

            return BuildMenuWithTpl(cssClassForRootElement);
        }

    }
}