using System;
using System.Collections;

namespace NetCms.Pages
{
    public class PagesSwitcherCollection : IEnumerable
    {
        private G2Collection coll;

        public int Count { get { return coll.Count; } }
        public PagesSwitcherCollection()
        {
            coll = new G2Collection();
        }

        public void Add(PagesSwitcher pagesSwitcher)
        {
            coll.Add(pagesSwitcher.GetHashCode().ToString(), pagesSwitcher);
        }

        public PagesSwitcher this[int i]
        {
            get
            {
                return (PagesSwitcher)coll[coll.Keys[i]];
            }
        }
        public PagesSwitcher this[string key]
        {
            get
            {
                return (PagesSwitcher)coll[key];
            }
        }
        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private PagesSwitcherCollection Collection;
            public CollectionEnumerator(PagesSwitcherCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}