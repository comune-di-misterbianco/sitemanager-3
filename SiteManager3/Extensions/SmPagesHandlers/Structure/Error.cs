﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages
{
    public class ErrorPage : PageV2
    {
        public Exception ExceptionData
        {
            get
            {
                return _ExceptionData;
            }
        }
        private Exception _ExceptionData;

        public ErrorPage(System.Web.UI.Page page, StateBag viewState, Exception ex)
            : base(page, viewState)
        {
            _ExceptionData = ex;
            ApplicationContext.SetCurrentComponent(ApplicationContext.BackofficeGenericComponentName);
        }

        public override Control Content 
        { 
             get 
             {
                 return new NetCms.GUI.SmPageError(ExceptionData);
             } 
        }
    }
}
