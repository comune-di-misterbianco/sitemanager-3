﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace NetCms.Pages
{
    public class Login : PageV2
    {
        public Login(System.Web.UI.Page page, StateBag viewState)
            : base(page, viewState)
        {
            ApplicationContext.SetCurrentComponent(ApplicationContext.BackofficeGenericComponentName);
        }
        
        public override Control Content
        {
	        get
            {
                return new NetCms.GUI.LoginPage();
            }
        }
    }
}
