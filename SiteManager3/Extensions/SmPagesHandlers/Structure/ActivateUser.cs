﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages
{
    public class ActivateUser : PageV2
    {
        public ActivateUser(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) { }

        public override Control Content
        {
            get 
            {
                return new NetCms.GUI.ActivateUserPage();
            }
        }
    }
}
