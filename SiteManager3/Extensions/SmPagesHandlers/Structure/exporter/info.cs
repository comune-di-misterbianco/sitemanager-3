﻿using System;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Applications.Importer
{
    public class Info : PageV1
    {
        public Info(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) { }

        public override PageV1.PageTypes Type
        {
            get { return PageTypes.Void; }
        }

        public override Control Content
        {
            get
            {
                return null;
            }
        }

        protected override void Build()
        {
            string userName = NetCms.Users.AccountManager.CurrentAccount.UserName;
            G2Core.Common.RequestVariable filePath = new G2Core.Common.RequestVariable("path", G2Core.Common.RequestVariable.RequestType.QueryString);
            if (filePath.IsValidString && (File.Exists(filePath.StringValue) || Directory.Exists(filePath.StringValue)))
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Verbose, "L'utente con username " + userName + " sta visualizzando la cartella di esportazione " + filePath.StringValue + ".", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);

                StreamReader streamFile = new StreamReader(filePath.StringValue);
                this.CurrentPage.Response.Write(streamFile.ReadToEnd());
                this.CurrentPage.Response.ContentType = "text/xml";
            }
            else
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Warning, "L'utente con username " + userName + " sta visualizzando un path errato nell'importer.", NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), userName);
                this.CurrentPage.Response.Write("File Path non valido");
            }
        }
    }
}
