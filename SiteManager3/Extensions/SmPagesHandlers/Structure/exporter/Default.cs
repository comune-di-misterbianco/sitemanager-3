﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Applications.Importer
{
    public class Default : PageV2
    {
        public Default(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) { }

        public override Control Content 
        { 
             get 
             {
                 return new NetCms.Structure.Applications.Importer.ImporterGUI();
             } 
        }
    }
}
