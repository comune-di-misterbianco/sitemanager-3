﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace NetCms.Pages.Applications.Search
{
    public class Search : PageV2
    {
        public Search(System.Web.UI.Page page, StateBag viewState)
            : base(page, viewState)
        {
            ApplicationContext.SetCurrentComponent("Search");
        }

        public override Control Content
        {
            get
            {
                return new NetCms.Structure.Search.CmsSearch();
            }
        }
    }
}