﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Applications.Search
{
    public class ContentIndex : PageV2
    {
        public ContentIndex(System.Web.UI.Page page, StateBag viewState)
            : base(page, viewState)
        {
            ApplicationContext.SetCurrentComponent("Search");
        }

        public override Control Content
        {
            get
            {
                return new NetCms.Structure.Applications.Configs.ContentIndex();
            }
        }
    }
}
