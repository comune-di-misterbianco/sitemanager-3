﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Applications.Configs
{
    public class LoginManager : PageV2
    {
        public LoginManager(System.Web.UI.Page page, StateBag viewState)
            : base(page, viewState)
        {
            ApplicationContext.SetCurrentComponent("Configs");
        }

        public override Control Content
        {
            get
            {
                return new NetCms.Structure.Applications.Configs.Pages.LoginManager();
            }
        }
    }
}
