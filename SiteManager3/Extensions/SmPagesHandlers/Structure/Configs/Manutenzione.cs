﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace NetCms.Pages.Applications.Configs
{
    public class Manutenzione : PageV2
    {
        public Manutenzione(System.Web.UI.Page page, StateBag viewState)
            : base(page, viewState)
        {
            ApplicationContext.SetCurrentComponent("Configs");
        }

        public override Control Content
        {
            get
            {
                return new NetCms.Structure.Applications.Configs.Manutenzione();
            }
        }
    }
}
