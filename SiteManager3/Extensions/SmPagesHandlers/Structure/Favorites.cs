﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages
{
    public class Favorites : PageV2
    {
        public Favorites(System.Web.UI.Page page, StateBag viewState)
            : base(page, viewState)
        {
            ApplicationContext.SetCurrentComponent(ApplicationContext.BackofficeGenericComponentName);
        }

        public override Control Content
        {
            get 
            {
                return new NetCms.GUI.Startpage.ModBookmarksPage();
            }
        }
    }
}
