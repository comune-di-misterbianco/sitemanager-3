﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Applications.Installer
{
    public class Networks : PageV2
    {
        public Networks(System.Web.UI.Page page, StateBag viewState) : base(page, viewState)
        {
            ApplicationContext.SetCurrentComponent("Installer");        
        }

        public override Control Content 
        { 
             get 
             {
                 return new NetCms.Structure.Applications.InstallerNetworks();
             } 
        }
    }
}
