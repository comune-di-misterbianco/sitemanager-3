﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Networks.Cms.Grants
{
    public class UserGrantsReview : PageV2
    {
        public UserGrantsReview(System.Web.UI.Page page, StateBag viewState)
            : base(page, viewState)
        {
            ApplicationContext.SetCurrentComponent("Permessi Applicazioni");
        }

        public override Control Content
        {
            get
            {
                return new NetCms.Vertical.Grants.VericalGrantsReview();
            }
        }
    }
}
