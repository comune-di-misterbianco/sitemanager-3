﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Networks.Cms
{
    public class FolderDel : PageV2
    {
        public FolderDel(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) { }
        
        public override Control Content
        {
            get
            {
                return new NetCms.Structure.WebFS.DeleteFolderPage();
            }
        }
    }
}
