﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Networks.Cms
{
    public class AjaxFoldersPopup : PageV2
    {
        public AjaxFoldersPopup(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) 
        {
           
        }

        public override Control Content
        {
            get
            {
                return null;
            }
        }

        protected override void Build()
        {
            if (NetCms.Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork)
            {
                NetCms.Structure.WebFS.StructureFolder folder = (NetCms.Structure.WebFS.StructureFolder)NetCms.Networks.NetworksManager.CurrentActiveNetwork.RootFolder;

                string s = this.CurrentPage.Request.QueryString["root"];
                G2Core.Common.RequestVariable chk = new G2Core.Common.RequestVariable("chk",G2Core.Common.RequestVariable.RequestType.QueryString);
                s = s.Replace("FATN_", "");
                int id;
   
                if (int.TryParse(s, out id))
                {
                    folder = (NetCms.Structure.WebFS.StructureFolder)folder.FindFolderByID(id);
                    this.CurrentPage.Response.Write(folder.Serialized.GetJsonChildsCode(chk.IntValue, "#", false, false, "retrieveDocuments({0},{1},{2},{3},{4})"));
                }
                else
                    this.CurrentPage.Response.Write("[" + folder.Serialized.GetJsonNodeCode(chk.IntValue, "#", false, false, "retrieveDocuments({0},{1},{2},{3},{4})") + "]");
            }
        }
    }
}
