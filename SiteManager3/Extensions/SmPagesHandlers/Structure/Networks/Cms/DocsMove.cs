﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Networks.Cms
{
    public class DocsMove : PageV1
    {
        public DocsMove(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) { }

        public override PageV1.PageTypes Type
        {
            get { return PageTypes.Default; }
        }

        public override Control Content
        {
            get
            {
                return null;// NetCms.Structure.Applications.WebDocs.WebDocs.MoveView(this.PageData);
            }
        }
    }
}
