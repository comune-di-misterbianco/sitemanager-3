﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Networks.Cms
{
    public class Trash : PageV2//PageV1
    {
        public Trash(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) { }

        //public override PageV1.PageTypes Type
        //{
        //    get { return PageTypes.Default; }
        //}

        public override Control Content
        {
            get
            {
                return new NetCms.Structure.WebFS.TrashMainPage(); //.Trash(PageData.CurrentReference).GetTrashView();
            }
        }
    }
}
