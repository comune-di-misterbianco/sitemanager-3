﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Networks.Cms.Popup
{
    public class NavigatePopup : PageV1
    {
        public NavigatePopup(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) { }

        public override PageV1.PageTypes Type
        {
            get { return PageTypes.Popup; }
        }
        public NetCms.GUI.Scripts.ScriptsIndexer Scripts
        {
            get
            {
                if (_Scripts == null)
                    _Scripts = new NetCms.GUI.Scripts.ScriptsIndexer(this.CurrentPage);
                return _Scripts;
            }
        }
        private NetCms.GUI.Scripts.ScriptsIndexer _Scripts;
        public override Control Content
        {
            get
            {
                NetUtility.RequestVariable targetctrid = new NetUtility.RequestVariable("targetctrid", NetUtility.RequestVariable.RequestType.QueryString);
                if (targetctrid.IsValid())
                {
                    CmsPopup window = new CmsPopup(this.PageData, targetctrid.StringValue);
                    return window.GetView();
                }
                return null;
            }
        }
    }
}
