﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.Structure.Popup;


namespace NetCms.Pages.Networks.Cms.Popup
{
    public class FileManagerPopup : PageV2
    {
        public FileManagerPopup(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) 
        {
            AddScriptsToHeader();
            AddCssToHeader();
        }

        public override Control Content
        {
            get
            {
                NetUtility.RequestVariable targetctrid = new NetUtility.RequestVariable("targetctrid", NetUtility.RequestVariable.RequestType.QueryString);
                if (targetctrid.IsValid())
                {
                    FileManagerControl navigator = new FileManagerControl(targetctrid.StringValue);
                    navigator.ShowListView = Grid;                    
                    return navigator;
                }
                return null;
            }
        }

        public bool Modal
        {
            get
            {
                NetUtility.RequestVariable modalMode = new NetUtility.RequestVariable("modal", NetUtility.RequestVariable.RequestType.QueryString);
                if (modalMode.IsValid() && modalMode.StringValue.Length > 0)
                    return bool.Parse(modalMode.StringValue);
                return false;
            }
            set 
            {
                _Modal = value;
            }
        }
        private bool _Modal = false;

        public bool Grid
        {
            get
            {
                NetUtility.RequestVariable modalMode = new NetUtility.RequestVariable("gridmode", NetUtility.RequestVariable.RequestType.QueryString);
                if (modalMode.IsValid() && modalMode.StringValue.Length > 0)
                    return bool.Parse(modalMode.StringValue);
                return false;
            }
            set
            {
                _Grid = value;
            }
        }
        private bool _Grid = false;


        public NetCms.GUI.Scripts.ScriptsIndexer Scripts
        {
            get
            {
                if (_Scripts == null)
                    _Scripts = new NetCms.GUI.Scripts.ScriptsIndexer(this.CurrentPage);
                return _Scripts;
            }
        }
        private NetCms.GUI.Scripts.ScriptsIndexer _Scripts;

        private void AddScriptsToHeader()
        {
            string idCurrentFolder = NetCms.Networks.NetworksManager.CurrentActiveNetwork.CurrentFolder.ID.ToString();
            NetUtility.RequestVariable SelFolderID = new NetUtility.RequestVariable("StructureFolderSideTree", NetUtility.RequestVariable.RequestType.Form_QueryString);
            if (SelFolderID.IsValid(NetUtility.RequestVariable.VariableType.Integer) && SelFolderID.IntValue > 0)
            {
                idCurrentFolder = SelFolderID.StringValue;
            }


            string value = "";

            if (!Modal)
            {
                value += NetCms.Configurations.Scripts.JQuery.HtmlHeadCode + System.Environment.NewLine;
                value += NetCms.Configurations.Scripts.JQueryMigrate.HtmlHeadCode + System.Environment.NewLine;
                value += NetCms.Configurations.Scripts.Popper.HtmlHeadCode + System.Environment.NewLine;
                value += NetCms.Configurations.Scripts.JQueryMask.HtmlHeadCode + System.Environment.NewLine;
                value += @"<script type=""text/javascript"" src=""" + NetCms.Configurations.Scripts.Bootstrap.Path + @"""></script><!--boostrap.js-->" + System.Environment.NewLine;
                value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Cookie) + "\"></script><!--Jquery_Cookie.js-->\n";
                value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Treeview) + "\"></script><!--Jquery_Treeview.js-->\n";
                value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Treeview_Async) + "\"></script><!--Jquery_Treeview_Async.js-->\n";
                value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Treeview_Edit) + "\"></script><!--Jquery_Treeview_Edit.js-->\n";
                value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Tools) + "\"></script><!--Jquery_Tools.js-->\n";
                value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.JScripts) + "\" ></script><!--Scripts.js-->\n";
            }
            else
            {
                value += NetCms.Configurations.Scripts.JQuery.HtmlHeadCode + System.Environment.NewLine;
                value += NetCms.Configurations.Scripts.JQueryMigrate.HtmlHeadCode + System.Environment.NewLine;
                value += NetCms.Configurations.Scripts.Popper.HtmlHeadCode + System.Environment.NewLine;
                value += NetCms.Configurations.Scripts.JQueryMask.HtmlHeadCode + System.Environment.NewLine;
                value += @"<script type=""text/javascript"" src=""" + NetCms.Configurations.Scripts.Bootstrap.Path + @"""></script><!--boostrap.js-->" + System.Environment.NewLine;
                value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Treeview) + "\"></script><!--Jquery_Treeview.js-->\n";
                value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Treeview_Async) + "\"></script><!--Jquery_Treeview_Async.js-->\n";
                value += "   <script type=\"text/javascript\" src=\"" + Scripts.GetScriptURL(NetCms.GUI.Scripts.ScriptsIndexer.Scripts.Jquery_Treeview_Edit) + "\"></script><!--Jquery_Treeview_Edit.js-->\n";

            }

            this.CurrentPage.Page.Header.Controls.Add(new LiteralControl(value));

            if (Grid)
            {
               this.CurrentPage.Page.Header.Controls.Add(new LiteralControl(Configurations.Scripts.JQueryPagination.HtmlHeadCode));
               this.CurrentPage.Page.Header.Controls.Add(new LiteralControl(Configurations.Scripts.Filepicker.HtmlHeadCode));                
            }
            else
                this.CurrentPage.Page.Header.Controls.Add(new LiteralControl(Configurations.Scripts.Flexigrid.HtmlHeadCode));


            string treeviewInit = @"<script type=""text/javascript"">
                            $(document).ready(function(){
		                        $(""#FolderPickerAjaxTree"").treeview({
			                        url: """ + NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteAdminRoot + @"/cms/ajaxfolderpopup.aspx?folder=" + idCurrentFolder + @"""
		                        });
	                        });

	                        </script>
                                ";
            this.CurrentPage.Page.Header.Controls.Add(new LiteralControl(treeviewInit));

        }

        private void AddCssToHeader()
        {
            string value = "";
            value += " <link rel=\"stylesheet\" type=\"text/css\" href=\"" + NetCms.Configurations.Scripts.Bootstrap.Css.FirstOrDefault() + "\" /><!--bootstrap.css-->" + System.Environment.NewLine;
            value += "<link media=\"all\" rel=\"stylesheet\" href=\"/scripts/shared/font-awesome-5/css/all.min.css\" />" + System.Environment.NewLine;
            value += " <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "filemanager.css\" /><!--filemanager.css-->";

            if (!Modal)
            {              
                value += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "side.css\" /><!--side.css-->";
                value += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "jquery-treeview.css\" /><!--tree.csx-->";
                value += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "template.css\" /><!--template.csx-->";                
            }
            else
            {
                value += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "side.css\" /><!--side.css-->";
                value += "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" + Configurations.Paths.BackOfficeSkinPath + "jquery-treeview.css\" /><!--tree.csx-->";
            }

            this.CurrentPage.Header.Controls.Add(new LiteralControl(value));
        }
    }
}
