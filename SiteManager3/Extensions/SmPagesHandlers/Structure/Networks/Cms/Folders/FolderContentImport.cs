﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace NetCms.Pages.Networks.Cms.Folders 
{
    public class FolderContentImport : PageV2
    {
        public FolderContentImport(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) { }

        public override Control Content
        {
            get
            {
                return new NetCms.Structure.WebFS.ImportContentPage();
            }
        }
    }
}
