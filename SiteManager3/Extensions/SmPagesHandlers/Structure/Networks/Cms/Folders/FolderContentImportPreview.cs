﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace NetCms.Pages.Networks.Cms.Folders 
{
    public class FolderContentImportPreview : PageV2
    {
        public FolderContentImportPreview(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) { }

        public override Control Content
        {
            get
            {
                return new NetCms.Structure.WebFS.ExporterPage();
            }
        }
    }
}
