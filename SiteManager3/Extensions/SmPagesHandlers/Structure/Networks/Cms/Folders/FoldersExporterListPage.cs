﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Networks.Cms.Folders
{
    public class FoldersExporterListPage : PageV2
    {
        public FoldersExporterListPage(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) { }

        public override Control Content
        {
            get
            {
                return new NetCms.Structure.WebFS.ExporterListPage();
            }
        }
    }
}
