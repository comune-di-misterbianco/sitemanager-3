﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Networks.Cms.Grants
{
    public class GrantsView : PageV2
    {
        public GrantsView(System.Web.UI.Page page, StateBag viewState)
            : base(page, viewState)
        {
            ApplicationContext.SetCurrentComponent("Permessi Applicazioni");
        }

        public override Control Content
        {
            get
            {
                return new NetCms.Structure.Grants.GrantsReview();
            }
        }
    }
}
