﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Networks.Cms.Grants
{
    public class Wizard : PageV2
    {
        public Wizard(System.Web.UI.Page page, StateBag viewState)
            : base(page, viewState)
        {
            ApplicationContext.SetCurrentComponent("Permessi Applicazioni");
        }
        /*
        public override PageV1.PageTypes Type
        {
            get { return PageTypes.Default; }
        }
        */
        public override Control Content
        {
            get
            {
                return new NetCms.Structure.WebFS.FolderGrantsPage();
            }
        }
    }
}
