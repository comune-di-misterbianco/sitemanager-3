﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Networks.Docs
{
    public class DocsMetaMod : PageV2
    {
        public DocsMetaMod(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) { }

        //public override PageV1.PageTypes Type
        //{
        //    get { return PageTypes.Default; }
        //}

        public override Control Content
        {
            get
            {
                return new NetCms.Structure.WebFS.ModMetaDocumentPage();
            }
        }
    }
}
