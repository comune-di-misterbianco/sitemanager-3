﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Networks.Cms.Docs
{
    public class DocsSendNotify : PageV2
    {
        public DocsSendNotify(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) 
        {
        }
        protected override void Build()
        {
            NetCms.Structure.WebFS.Reviews.ReviewNotifier notifier = new NetCms.Structure.WebFS.Reviews.ReviewNotifier();
            notifier.SendNotifies();
        }
        public override Control Content
        {
            get
            {
                return null;
            }
        }
    }
}
