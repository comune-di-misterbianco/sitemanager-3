﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Networks.Cms.Docs
{
    public class DocsOverview : PageV2
    {
        public DocsOverview(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) { }

        public override Control Content
        {
            get
            {
                return new NetCms.Structure.WebFS.Reviews.ReviewsManagerPage();
                //return NetCms.Structure.WebFS.DocumentOverview.FabricatePage();
            }
        }
    }
}
