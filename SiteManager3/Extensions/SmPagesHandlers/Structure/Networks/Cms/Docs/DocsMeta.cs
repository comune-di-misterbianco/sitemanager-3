﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Networks.Docs
{
    public class DocsMeta : PageV2
    {
        public DocsMeta(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) { }

        //public override PageV1.PageTypes Type
        //{
        //    get { return PageTypes.Default; }
        //}

        public override Control Content
        {
            get
            {
                return new NetCms.Structure.WebFS.MetaDocumentPage();
            }
        }
    }
}
