﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Networks.Cms.Docs
{
    public class DocsDetails : PageV2
    {
        public DocsDetails(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) { }

        public override Control Content
        {
            get
            {
                return NetCms.Structure.WebFS.DocumentDetailsPage.FabricatePage();
            }
        }
    }
}
