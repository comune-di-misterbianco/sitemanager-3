﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages.Networks.Cms
{
    public class AjaxFolder : PageV1
    {
        public AjaxFolder(System.Web.UI.Page page, StateBag viewState) : base(page, viewState) { }

        public override PageV1.PageTypes Type
        {
            get { return PageTypes.Void; }
        }

        public override Control Content
        {
            get
            {
                return null;
            }
        }

        protected override void Build()
        {
            if (NetCms.Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork)
            {
                NetCms.Structure.WebFS.StructureFolder folder = (NetCms.Structure.WebFS.StructureFolder)NetCms.Networks.NetworksManager.CurrentActiveNetwork.RootFolder;

                string s = this.CurrentPage.Request.QueryString["root"];
                string chk = new G2Core.Common.RequestVariable("chk",G2Core.Common.RequestVariable.RequestType.QueryString).StringValue;
                s = s.Replace("FATN_", "");
                int id;
                if (int.TryParse(s, out id))
                {
                    folder = (NetCms.Structure.WebFS.StructureFolder)folder.FindFolderByID(id);
                    this.CurrentPage.Response.Write(chk == "true" ? folder.Serialized.JsonFormChildsCode : folder.Serialized.JsonChildsCode );
                }
                else
                    this.CurrentPage.Response.Write("[" +( chk == "true" ?folder.Serialized.JsonFormNodeCode : folder.Serialized.JsonNodeCode) + "]");
            }
        }
    }
}
