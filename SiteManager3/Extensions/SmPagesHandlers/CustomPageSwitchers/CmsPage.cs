﻿using System;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages
{
    public class CmsPage : PageV2
    {
        public string ClassName
        {
            get { return _ClassName; }
            private set { _ClassName = value; }
        }
        private string _ClassName;

        public NetCms.Structure.Applications.Application Application
        {
            get { return _Application; }
            private set { _Application = value; }
        }
        private NetCms.Structure.Applications.Application _Application;
        
			

        public CmsPage(NetCms.Structure.Applications.Application application,string className, System.Web.UI.Page page, StateBag viewState) 
            : base(page, viewState) 
        {
            Application = application;
            ClassName = className; 
        }

        
        public override Control Content 
        { 
             get 
             {
                 Type pageType = Application.Assembly.GetType(ClassName);

                 return (WebControl)System.Activator.CreateInstance(pageType);
             } 
        }
    }
}
