﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages
{
    public class BasicPagesSwitcher : PagesSwitcher
    {
        public BasicPagesSwitcher()
        {
        }

        public override Page Fabricate(string url, System.Web.UI.Page page, StateBag viewState)
        {
            switch (url)
            {
                case "/default.aspx": return new Homepage(page, viewState);
                case "/error-no-grants.aspx": return new ErrorPage(page, viewState,null);
                case "/internal-error.aspx": return new ErrorPage(page, viewState, null);                
            
                case "/preferiti.aspx": return new Favorites(page, viewState);
                case "/login.aspx": return new Login(page, viewState);
             
                case "/applications/installer/default.aspx": return new NetCms.Pages.Applications.Installer.Default(page, viewState);
                case "/applications/installer/networks.aspx": return new NetCms.Pages.Applications.Installer.Networks(page, viewState);

                case "/applications/configs/default.aspx": return new NetCms.Pages.Applications.Configs.Default(page, viewState);
                case "/applications/configs/generic.aspx": return new NetCms.Pages.Applications.Configs.Generic(page, viewState);
                case "/applications/configs/setconfig.aspx": return new NetCms.Pages.Applications.Configs.SetConfig(page, viewState);

                case "/applications/configs/manutenzione.aspx": return new NetCms.Pages.Applications.Configs.Manutenzione(page, viewState);

                case "/applications/configs/loginmanager.aspx": return new NetCms.Pages.Applications.Configs.LoginManager(page, viewState);
                case "/applications/configs/loginconfig.aspx": return new NetCms.Pages.Applications.Configs.LoginConfig(page, viewState);


                case "/applications/configs/languages.aspx": return new NetCms.Pages.Applications.Configs.Language(page, viewState);
                case "/applications/configs/editlanguage.aspx": return new NetCms.Pages.Applications.Configs.EditLanguage(page, viewState);
                case "/applications/configs/index-config.aspx": return new NetCms.Pages.Applications.Configs.IndexConfig(page, viewState);
                case "/applications/configs/seo-meta.aspx": return new NetCms.Pages.Applications.Configs.SeoMeta(page, viewState);
                case "/applications/configs/social.aspx": return new NetCms.Pages.Applications.Configs.Social(page, viewState);
                       
                case "/applications/configs/cms-search-engine.aspx": return new NetCms.Pages.Applications.Search.ContentIndex(page, viewState);
                case "/networks/search/search.aspx": return new NetCms.Pages.Applications.Search.Search(page, viewState);
                case "/networks/search/search_solr.aspx": return new NetCms.Pages.Applications.Search.SolrSearch(page, viewState);


                case "/applications/grants/default.aspx": return new NetCms.Pages.Networks.Cms.Grants.UserGrants(page, viewState);
                case "/applications/grants/review.aspx": return new NetCms.Pages.Networks.Cms.Grants.UserGrantsReview(page, viewState);
                case "/networks/cms/grants/grants.aspx": return new NetCms.Pages.Networks.Cms.Grants.Grants(page, viewState);
                case "/networks/cms/grants/grants_view.aspx": return new NetCms.Pages.Networks.Cms.Grants.GrantsView(page, viewState);
                case "/networks/cms/grants/wizard.aspx": return new NetCms.Pages.Networks.Cms.Grants.Wizard(page, viewState);
          
                //sembra non utilizzato, verificare se rimuoverlo
                //case "/networks/cms/grants/vappwizard.aspx": return new NetCms.Pages.Networks.Cms.Grants.VerticalWizard(page, viewState);
                //end
                                    
                case "/networks/cms/default.aspx": return new NetCms.Pages.Networks.Cms.Default(page, viewState);
                case "/networks/cms/search.aspx": return new NetCms.Pages.Networks.Cms.Search(page, viewState);
               
                case "/networks/cms/ajaxfolder.aspx": return new NetCms.Pages.Networks.Cms.AjaxFolder(page, viewState);
                case "/networks/cms/ajaxfolderpopup.aspx": return new NetCms.Pages.Networks.Cms.AjaxFoldersPopup(page, viewState);
                
                //momentaneamente disattivato
                case "/networks/cms/docs_move.aspx": return new NetCms.Pages.Networks.Cms.DocsMove(page, viewState);
                //end

                //case "/networks/cms/empty.aspx": return new NetCms.Pages.Networks.Cms.Empty(page, viewState);
                case "/networks/cms/folder_del.aspx": return new NetCms.Pages.Networks.Cms.FolderDel(page, viewState);
                case "/networks/cms/folder_mod.aspx": return new NetCms.Pages.Networks.Cms.FolderMod(page, viewState);
                case "/networks/cms/folder_move.aspx": return new NetCms.Pages.Networks.Cms.FolderMove(page, viewState);
                case "/networks/cms/folder_new.aspx": return new NetCms.Pages.Networks.Cms.FolderNew(page, viewState);
                case "/networks/cms/folders_meta.aspx": return new NetCms.Pages.Networks.Cms.FoldersMeta(page, viewState);
                case "/networks/cms/folders_meta_add.aspx": return new NetCms.Pages.Networks.Cms.FoldersMetaAdd(page, viewState);
                case "/networks/cms/folders_meta_mod.aspx": return new NetCms.Pages.Networks.Cms.FoldersMetaMod(page, viewState);
                case "/networks/cms/order.aspx": return new NetCms.Pages.Networks.Cms.Order(page, viewState);
                //case "/networks/cms/overviews.aspx": return new NetCms.Pages.Networks.Cms.Overview(page, viewState);
                case "/networks/cms/trash.aspx": return new NetCms.Pages.Networks.Cms.Trash(page, viewState);
              
                case "/networks/docs/docs_meta.aspx": return new NetCms.Pages.Networks.Docs.DocsMeta(page, viewState);
                case "/networks/docs/docs_meta_add.aspx": return new NetCms.Pages.Networks.Docs.DocsMetaAdd(page, viewState);
                case "/networks/docs/docs_meta_mod.aspx": return new NetCms.Pages.Networks.Docs.DocsMetaMod(page, viewState);
                case "/networks/docs/docs_mod.aspx": return new NetCms.Pages.Networks.Docs.DocsMod(page, viewState);

                //Questa entry serve all'applicazione Links per far funzionare le revisioni dei links sulle cartelle di tipo Homepage
                case "/networks/cms/applications/link/modify.aspx": return new NetCms.Pages.Networks.Cms.Docs.DocsOverview(page, viewState);

                case "/networks/docs/overview.aspx": return new NetCms.Pages.Networks.Cms.Docs.DocsOverview(page, viewState);
                case "/networks/docs/details.aspx": return new NetCms.Pages.Networks.Cms.Docs.DocsDetails(page, viewState);
                case "/networks/docs/review.aspx": return new NetCms.Pages.Networks.Cms.Docs.DocsReview(page, viewState);
                case "/networks/docs/notify.aspx": return new NetCms.Pages.Networks.Cms.Docs.DocsSendNotify(page, viewState);
                
                // Export
                case "/networks/folders/export.aspx": return new NetCms.Pages.Networks.Cms.Folders.FoldersExporter(page, viewState);
                case "/networks/folders/exporter.aspx": return new NetCms.Pages.Networks.Cms.Folders.FoldersExporterListPage(page, viewState);

                // Content Import
                case "/networks/folders/import_content.aspx": return new NetCms.Pages.Networks.Cms.Folders.FolderContentImport(page, viewState);
                case "/networks/folders/import_content_preview.aspx": return new NetCms.Pages.Networks.Cms.Folders.FolderContentImportPreview(page, viewState);
                case "/networks/folders/import_content_status.aspx": return new NetCms.Pages.Networks.Cms.Folders.FolderContentImportStatus(page, viewState);

   
                case "/networks/cms/popup/navigate.aspx": return new NetCms.Pages.Networks.Cms.Popup.FileManagerPopup(page, viewState);
   
            }

            return null;
        }
    }
}
