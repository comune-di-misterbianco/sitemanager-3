﻿using System;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages
{
    public class VerticalPage : PageV2
    {
        public string ClassName
        {
            get { return _ClassName; }
            private set { _ClassName = value; }
        }
        private string _ClassName;

        public NetCms.Vertical.VerticalApplication Application
        {
            get { return _Application; }
            private set { _Application = value; }
        }
        private NetCms.Vertical.VerticalApplication _Application;



        public VerticalPage(NetCms.Vertical.VerticalApplication application, string className, System.Web.UI.Page page, StateBag viewState) 
            : base(page, viewState) 
        {
            Application = application;
            ClassName = className; 
        }

        
        public override Control Content 
        { 
             get 
             {                 
                 NetCms.Vertical.VerticalApplicationPageAdapter adapter = new NetCms.Vertical.VerticalApplicationPageAdapter(Application, ClassName, this.ViewState, this.CurrentPage.IsPostBack);
                 return adapter.DisableContent ? null : adapter;
             } 
        }
    }
}
