﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Reflection;
using NetCms.Structure.Applications;
using NetCms.Vertical;
using System.Linq;
using NetCms.Vertical.BusinessLogic;

namespace NetCms.Pages
{
    public class CmsApplicationsPagesSwitcher : PagesSwitcher
    {
			
        public CmsApplicationsPagesSwitcher()
        {
        }

        public override Page Fabricate(string url, System.Web.UI.Page page, StateBag viewState)
        {
            Page buildedPage = FabricateCMSApplicationPage(url,page,viewState);
            if(buildedPage == null)
                buildedPage = FabricateVerticalApplicationPage(url, page, viewState);
            return buildedPage;
        }

        public Page FabricateCMSApplicationPage(string url, System.Web.UI.Page page, StateBag viewState)
        {
            string baseNetworkapplicationsAddress = "/networks/cms/applications/";
            if (url.Contains(baseNetworkapplicationsAddress))
            {
                url = url.Replace(baseNetworkapplicationsAddress, "");

                string[] splittedUrl = SplitAtFirstSlash(url);
                //DataTable PagesTable = Connection.SqlQuery(@"SELECT * FROM applicazioni INNER JOIN applicazioni_pagine ON (applicazioni.id_Applicazione = applicazioni_pagine.Applicazione_PaginaApplicazione) WHERE SystemName_Applicazione = '" + splittedUrl[0] + "' AND Pagename_PaginaApplicazione = '" + splittedUrl[1] + "'");
                //if (PagesTable.Rows.Count == 1)
                //{
                //    string appID = PagesTable.Rows[0]["id_Applicazione"].ToString();
                //    if (ApplicationsPool.ActiveAssemblies.Contains(appID))
                //    {
                //        Application application = ApplicationsPool.ActiveAssemblies[appID];

                //        string className = PagesTable.Rows[0]["ClassName_PaginaApplicazione"].ToString();
                //        ApplicationContext.SetCurrentComponent(application.Label);
                //        return new CmsPage(application, className, page, viewState);
                //    }
                //}

                Application dbApp = ApplicationBusinessLogic.GetApplicationBySystemName(splittedUrl[0]);

                if (dbApp != null)
                {
                    NetCms.Structure.Applications.ApplicationPage dbPage = dbApp.ApplicationPages.FirstOrDefault(x => x.PageName == splittedUrl[1]);

                    if (dbPage != null)
                    {
                        if (ApplicationsPool.ActiveAssemblies.Contains(dbApp.ID.ToString()))
                        {
                            Application application = ApplicationsPool.ActiveAssemblies[dbApp.ID.ToString()];

                            string className = dbPage.ClassName;
                            ApplicationContext.SetCurrentComponent(application.Label);
                            return new CmsPage(application, className, page, viewState);
                        }
                    }
                }
            }
            return null;
        }
        public Page FabricateVerticalApplicationPage(string url, System.Web.UI.Page page, StateBag viewState)
        {
            if (url.Contains("/applications/"))
            {
                url = url.Replace("/applications/", "");
                // Da considerare per funzionamento vertical appliation da backend
                // Fede pensaci Tu!!!
                if (url.StartsWith("/networks"))
                    url = url.Replace("/networks", "");

                string[] splittedUrl = SplitAtFirstSlash(url);
                ExternalAppPage externalPage = VerticalAppBusinessLogic.GetExternalAppPageByPageNameAndKey(splittedUrl[0], splittedUrl[1]);
                //DataTable PagesTable = Connection.SqlQuery(@"SELECT * FROM externalappz INNER JOIN externalappz_pages ON (externalappz.id_ExternalApp = externalappz_pages.ExternalApp_ExternalAppPage) WHERE Key_ExternalApp = '" + splittedUrl[0] + "' AND (PageName_ExternalAppPage = '" + splittedUrl[1] + "' OR PageName_ExternalAppPage = '/" + splittedUrl[1] + "')");
                //if (PagesTable.Rows.Count > 0 )
                if (externalPage != null)
                {
                    string appID = externalPage.Application.ID.ToString();//PagesTable.Rows[0]["id_ExternalApp"].ToString();
                    if (VerticalApplicationsPool.ActiveAssemblies.Contains(appID))
                    {
                        VerticalApplication application = VerticalApplicationsPool.ActiveAssemblies[appID];
                        ApplicationContext.SetCurrentComponent(application.Label);

                        string className = externalPage.ClassName;//PagesTable.Rows[0]["ClassName_ExternalAppPage"].ToString();

                        var vpage = new VerticalPage(application, className, page, viewState);
                        if (application.NetworkDependant && NetCms.Networks.NetworksManager.CurrentActiveNetwork != null)
                            HttpContext.Current.Items["VerticalAppCurrentAbsoluteWebRoot"] = Configurations.Paths.AbsoluteSiteManagerRoot + "/" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.SystemName + "/applications/" + application.SystemName;
                        else
                            HttpContext.Current.Items["VerticalAppCurrentAbsoluteWebRoot"] = Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/" + application.SystemName;
                        return vpage;
                    }
                }
            }
            return null;
        }
        private string[] SplitAtFirstSlash(string path)
        {
            string[] value = new string[2];
            int IndexOfSlash = path.IndexOf('/');
            value[0] = path.Substring(
                                        0,
                                        IndexOfSlash
                                     );
            value[1] = path.Substring(
                                        IndexOfSlash + 1,
                                        (path.Length - IndexOfSlash) - 1
                                     );
            return value;
        }
    }
}
