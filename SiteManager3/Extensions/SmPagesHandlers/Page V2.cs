﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages
{
    public abstract class PageV2:Page
    {
        public PageV2(System.Web.UI.Page page, StateBag viewState):base(page,viewState)
        {
        }

        protected override void Build()
        {
            this.CurrentPage.Form.Controls.Add(this.Content);
        }
    }
}
