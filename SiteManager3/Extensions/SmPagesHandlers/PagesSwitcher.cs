﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages
{
    public abstract class PagesSwitcher
    {
        public PagesSwitcher()
        {
            
        }

        public abstract Page Fabricate(string url, System.Web.UI.Page page, StateBag viewState);
    }
}
