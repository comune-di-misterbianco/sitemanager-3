﻿using System;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages
{
    public abstract class Page
    {
        public string AssociatedUrl
        {
            get { return _AssociatedUrl; }
            private set { _AssociatedUrl = value; }
        }
        private string _AssociatedUrl;

        public System.Web.UI.Page CurrentPage
        {
            get { return _CurrentPage; }
            private set { _CurrentPage = value; }
        }
        private System.Web.UI.Page _CurrentPage;

        public NetCms.GUI.Css.ImagesIndexer ImagesIndexer
        {
            get { return _ImagesIndexer; }
            private set { HttpContext.Current.Items["PageImagesIndexer"] = _ImagesIndexer = value; }
        }
        private NetCms.GUI.Css.ImagesIndexer _ImagesIndexer;

        public NetCms.GUI.Icons.IconsIndexer IconsIndexer
        {
            get { return _IconsIndexer; }
            private set { HttpContext.Current.Items["PageIconsIndexer"] = _IconsIndexer = value; }
        }
        private NetCms.GUI.Icons.IconsIndexer _IconsIndexer;


        public StateBag ViewState
        {
            get
            {
                return _ViewState;
            }
        }
        private StateBag _ViewState;

        public Page(System.Web.UI.Page page, StateBag viewState)
        {
            this._ViewState = viewState;
            this.ImagesIndexer = new NetCms.GUI.Css.ImagesIndexer(page);
            this.IconsIndexer = new NetCms.GUI.Icons.IconsIndexer(page);
            CurrentPage = page;
            
        }

        public abstract Control Content { get; }

        public void BuildPage() 
        {
            if (!NetCms.Configurations.Debug.RawExceptionsEnabled)
            {
                try
                {
                    this.Build();
                }
                catch (ThreadAbortException ex )
                { }
                catch (Exception ex)
                {
                    if (!(this is Login))
                    {
                        NetCms.Pages.Page errorpage = new NetCms.Pages.ErrorPage(this.CurrentPage, this.ViewState, ex);
                        errorpage.Build();
                    }
                    Diagnostics.Diagnostics.TraceException(ex, "", "");
                }
            }
            else
            {
                this.Build();

                int i = 0;
                foreach (var log in NetCms.Diagnostics.Diagnostics.ExceptionsForThisPage)
                    NetCms.GUI.PopupBox.AddMessage((++i) + ") " + log.LogDateTime + ": " + log.LogMessage, NetCms.GUI.PostBackMessagesType.Normal);

                if (i > 0) NetCms.GUI.PopupBox.AddMessage("Questa pagina ha genarato " + i + " errori.", NetCms.GUI.PostBackMessagesType.Normal);
            }
            
        }
        protected abstract void Build();
    }
}
