﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages
{
    public abstract class PagesHandler
    {
        private static List<Type> _SwitchersTypes;
        public static List<Type> SwitchersTypes
        {
            get
            {
                if (_SwitchersTypes == null)
                {
                    _SwitchersTypes = new List<Type>();
                    _SwitchersTypes.Add(typeof(CmsApplicationsPagesSwitcher));
                    _SwitchersTypes.Add(typeof(BasicPagesSwitcher));
                }
                return _SwitchersTypes;
            }
        }

        private static List<PagesSwitcher> _SwitchersPool;
        public static List<PagesSwitcher> SwitchersPool
        {
            get
            {
                if (_SwitchersPool == null)
                {
                    _SwitchersPool = new List<PagesSwitcher>();
                    foreach (var type in SwitchersTypes)
                    {
                        var costructor = type.GetConstructor(new Type[]{ });
                        var instance = costructor.Invoke(new object[] { });
                        _SwitchersPool.Add(instance as PagesSwitcher);
                    }
                }
                return _SwitchersPool;
            }
        }

        public abstract PagesSwitcher[] Switchers { get; }

        public PageData PageData
        {
            get { return _PageData; }
            private set { _PageData = value; }
        }
        private PageData _PageData;

        public PagesHandler()
        {
            if (HttpContext.Current.Request["history"] != null && HttpContext.Current.Request["history"].ToString() == "back")
            {
                string back = "";
                NetCms.Users.User user = NetCms.Users.AccountManager.CurrentAccount;
                if (user != null)
                {
                    back = user.History.GetBackURL();     
                }
                else
                    back =  NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/default.aspx"; 
               NetCms.Diagnostics.Diagnostics.Redirect(back);


            }
        }

        public Page Fabricate(System.Web.UI.Page page, StateBag viewState)
        {
            Page BackEndPage = null;
            if (!NetCms.Configurations.Debug.GenericEnabled)
            {
                try
                {
                    BackEndPage = FabricatePage(page, viewState);
                }
                catch (Exception ex)
                {
                    BackEndPage = new NetCms.Pages.ErrorPage(page, viewState, ex);
                    /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                    ExLog.SaveLog(PageData.Account.ID, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Warning, ex);*/

                    // La tracciatura dell'eccezione causava un loop, per tale motivo è stata commentata
                    //Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, ex.Message, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName, "");
                }
            }
            else
                BackEndPage = FabricatePage(page, viewState);

            return BackEndPage;
        }
        protected Page FabricatePage(System.Web.UI.Page page, StateBag viewState)
        {
            PageData = new PageData(page, viewState);
            bool logged = PageData.BuildAccountAndCheckLogin();

            Page BackEndPage = null;
            if (logged)
            {
                if (NetCms.Users.AccountManager.CurrentAccount.Stato != NetCms.Users.User.Stati.Abilitato)
                    return new Pages.ActivateUser(page, viewState);

                if (NetCms.Users.AccountManager.CurrentAccount.NeedPasswordRefresh)// && Configurations.Generics.DebugMode == NetCms.Configurations.Generics.DebugModeStates.Off)
                    return new Pages.RefreshPassword(page, viewState);

                string url = page.Request.Url.PathAndQuery.ToLower().Split('?')[0];
                int cutAt = Configurations.Paths.AbsoluteSiteManagerRoot.Length;
                int newLength = url.Length - (Configurations.Paths.AbsoluteSiteManagerRoot.Length);
                url = url.Substring(cutAt, newLength);

                if (NetCms.Networks.NetworksManager.NetworkStatus == NetCms.Networks.NetworksStates.InsideNetwork)
                    if (url.StartsWith("/" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.SystemName.ToLower()))
                        url = url.Replace("/" + NetCms.Networks.NetworksManager.CurrentActiveNetwork.SystemName.ToLower(), "/networks");

                int i = 0;

                if (SwitchersPool != null)
                    while (BackEndPage == null && i < SwitchersPool.Count)
                        BackEndPage = SwitchersPool[i++].Fabricate(url, page, viewState);

                //BackEndPage = CmsSwitcher.Fabricate(url, page, viewState);
                //if (BackEndPage == null)
                //    BackEndPage = BasicSwitcher.Fabricate(url, page, viewState);

                //if (Switchers != null)
                //    while (BackEndPage == null && i < Switchers.Length)
                //        BackEndPage = Switchers[i++].Fabricate(url, page, viewState);

                if (BackEndPage == null)
                    throw new Exception("404. Pagina non trovata.");
            }
            else return new Pages.Login(page, viewState);
         
            return BackEndPage;
        }
    }
}
