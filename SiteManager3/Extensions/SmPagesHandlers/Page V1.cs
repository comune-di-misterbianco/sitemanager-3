﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetCms.Pages
{
    public abstract class PageV1:Page
    {
        public PageData PageData
        {
            get { return (PageData)HttpContext.Current.Items["SM_PageData"]; }
        }

        public PageBuilder PageBuilder
        {
            get
            {
                if (_PageBuilder == null)
                    _PageBuilder = new PageBuilder(PageData);
                return _PageBuilder;
            }
        }
        private PageBuilder _PageBuilder;

        public enum PageTypes { Home, Common, Lite, Default, Clean, Void, Debug, Popup, ThickBox }
        public abstract PageTypes Type
        {
            get;
        }

        public PageV1(System.Web.UI.Page page, StateBag viewState):base(page,viewState)
        {
            if ((Type == PageTypes.Home) || (Type == PageTypes.Common) || (Type == PageTypes.Default))
                NetCms.Users.AccountManager.CurrentAccount.History.UpdateHistoryData();

            
        }

        protected override void Build()
        {
            PageBuilder.Corpo = Content;

            switch (Type)
            {
                case PageTypes.Popup: PageBuilder.BuildPagePopup(CurrentPage.Form); break;               
                case PageTypes.ThickBox: PageBuilder.BuildPageThickBox(CurrentPage.Form); break;
                case PageTypes.Home: PageBuilder.BuildHomePage(CurrentPage.Form); break;
                //case PageTypes.Void: Builder.(CurrentPage.Form); break;
                case PageTypes.Clean: PageBuilder.BuildPageClean(CurrentPage.Form); break;
                case PageTypes.Common: PageBuilder.BuildCommonPage(CurrentPage.Form); break;
                case PageTypes.Debug: PageBuilder.BuildPageDebug(CurrentPage.Form); break;
                case PageTypes.Lite: PageBuilder.BuildPageLite(CurrentPage.Form); break;
                case PageTypes.Default: PageBuilder.BuildPage(CurrentPage.Form); break;
            }
        }
    }
}
