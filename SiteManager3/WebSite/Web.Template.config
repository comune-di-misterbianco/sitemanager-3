﻿<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <!-- Core application-->
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net" />
    <section name="pingsession" type="System.Configuration.NameValueFileSectionHandler,System, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    <section name="quartz" type="System.Configuration.NameValueSectionHandler, System, Version=1.0.5000.0,Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    <section name="filemanager" type="System.Configuration.NameValueFileSectionHandler,System, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    <section name="ckeditor" type="System.Configuration.NameValueFileSectionHandler,System, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    <section name="languagemanager" type="System.Configuration.NameValueFileSectionHandler,System, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    <section name="resizer" type="ImageResizer.ResizerSection" requirePermission="false" />
    <section name="AuthenticationUtilities" type="System.Configuration.NameValueFileSectionHandler,System, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    <section name="NetServiceDebug" type="System.Configuration.NameValueFileSectionHandler,System, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    <section name="ReCaptcha" type="System.Configuration.NameValueFileSectionHandler,System, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />    
    <!-- Cms application -->
    <section name="news" type="System.Configuration.NameValueFileSectionHandler,System, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    <section name="bandi" type="System.Configuration.NameValueFileSectionHandler,System, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />    
    <section name="faq" type="System.Configuration.NameValueFileSectionHandler,System, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    <section name="rassegna" type="System.Configuration.NameValueFileSectionHandler,System, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    <section name="imagegallery" type="System.Configuration.NameValueFileSectionHandler,System, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />    
  </configSections>
  <!-- Core application-->
  <log4net debug="false">
    <appender name="rollingFile" type="log4net.Appender.RollingFileAppender,log4net">      
      <File value="{#**AbsoluteLogsFolderPath**#}\cms3"/>
      <AppendToFile value="true"/>
      <ImmediateFlush value="true"/>
      <LockingModel type="log4net.Appender.FileAppender+MinimalLock"/>
      <RollingStyle value="Composite"/>
      <DatePattern value="-yyyy-MM-dd'.log'"/>                         
      <maxSizeRollBackups value="-1"/>
      <maximumFileSize value="200MB" />
      <CountDirection value="1" />
      <staticLogFileName value="true" />
      <layout type="NetCms.Diagnostics.Log4Net.LogEntry">
        <locationInfo value="true" />
      </layout>
    </appender>          
    <root>
      <level value="WARN" />
      <appender-ref ref="rollingFile" />
    </root>      
    <!-- This overcomes the proxy narrow log of hibernate -->
    <logger name="NHibernate.Engine.StatefulPersistenceContext.ProxyWarnLog">
      <level value="ERROR" />
    </logger>
  </log4net>
  <pingsession>
    <add key="timeoutping" value="10000" />
    <!--valore in millisecondi-->
    <add key="basedOnSessionTimeout" value="true" />
    <!--Dipende dal timeout della sessione-->
  </pingsession>
  <quartz>
    <add key="quartz.scheduler.instanceName" value="QuartzTestLog4Net" />
    <add key="quartz.threadPool.type" value="Quartz.Simpl.SimpleThreadPool, Quartz" />
    <add key="quartz.threadPool.threadCount" value="10" />
    <add key="quartz.threadPool.threadPriority" value="2" />
    <add key="quartz.jobStore.misfireThreshold" value="60000" />
    <add key="quartz.jobStore.type" value="Quartz.Simpl.RAMJobStore, Quartz" />
  </quartz>
  <filemanager>
    <add key="width" value="550" />
    <add key="height" value="400" />
    <add key="title" value="false" />
    <add key="rpp" value="15" />
    <add key="imgextensions" value="jpg,png,gif" />
    <add key="flashextensions" value="flv,swf" />
    <add key="videoextensions" value="avi,mp4,3gp" />
  </filemanager>
  <ckeditor>
    <add key="ToolbarConfigurations" value="default,basic,forum,normal,full" />
    <add key="width" value="900" />
    <add key="height" value="660" />
  </ckeditor>
  <languagemanager>
    <!-- defaultLanguage se valorizzata a 'db' forza la lettura del linguaggio di default dal db. Se invece viene inserito un'altro valore (qualunque), il linguaggio di default viene letto dal Browser.-->
    <add key="defaultLanguage" value="db" />
    <!-- language number indica dopo quante lingue 'abilitate' verrà effettuato lo switch grafico del WebControl.
	  Ovvero: nel caso di '2', se si avranno PIU' di due lingue il controllo verrà visualizzato in stile dropdownList con icona della bandiera e culture code.    
	  Altrimenti verranno visualizzate solo le bandiere delle lingue abilitate.-->
    <add key="languageNumber" value="2" />
  </languagemanager>
  <resizer>
    <diagnostics enableFor="Localhost" />
    <plugins>
      <add name="DiskCache" />
      <add name="Presets" />
    </plugins>
    <diskCache dir="repository/imagecache" autoClean="false" hashModifiedDate="true" enabled="true" subfolders="32" cacheAccessTimeout="15000" />
    <presets onlyAllowPresets="false">
      <preset name="thumb" settings="width=160;height=120;mode=crop" />
      <preset name="preview" settings="width=640;height=480;mode=crop" />
      <preset name="moduloimgwide" settings="width=600;height=200;mode=crop" />
      <preset name="tplthumb" settings="width=80;height=60;mode=crop" />
      <preset name="netimagepickerthumb" settings="width=220;height=100;mode=crop" />
      <preset name="newsmodulothumb" settings="width=80;height=60;mode=crop" />
      <preset name="newsthumb" settings="width=80;height=60;mode=crop" />
      <preset name="newsdetail" settings="width=280;height=140;mode=crop" />
    </presets>
  </resizer>
  <AuthenticationUtilities>
    <add key="EncryptionKey" value="" />
    <add key="EncryptionKey32" value="" />
  </AuthenticationUtilities>
  <NetServiceDebug>
    <add key="enableDebugLogin" value="false" />
    <add key="enableGenericDebug" value="false" />
    <add key="enableRawExceptions" value="false" />
    <add key="enableSqlExceptions" value="false" />
    <add key="frontTimingsEnabled" value="false" />
    <add key="applicationsInstallationEnabled" value="true" />
  </NetServiceDebug>
  <!-- End Core application-->
  <!-- Cms application -->
  <news>
    <add key="ArchivioStorico" value="false" />
    <!-- true/false -->
    <add key="DataPubblicazione" value="false" />
    <!-- true/false -->
    <add key="DataScadenza" value="false" />
    <!-- true/false -->
  </news>
  <bandi>
    <add key="AppRepository" value="repository" />
    <add key="TmpFolderUploadPath" value="repository/bandi/tmp" />
    <add key="UploaderHandler" value="charts/uploader.ashx" />
  </bandi>
  <faq>
    <add key="GroupToNotify" value="369" />
    <!-- Inserire l'id del gruppo a cui inviare le notifiche di nuove domande -->
  </faq>
  <rassegna>
    <add key="AppRepository" value="repository" />
    <add key="TmpFolderUploadPath" value="repository/rassegna/tmp" />
    <add key="UploaderHandler" value="charts/uploader.ashx" />
  </rassegna>
  <imagegallery>
    <add key="AppRepository" value="repository" />
    <add key="TmpFolderUploadPath" value="imagegallery/tmp" />
    <add key="UploaderHandler" value="charts/uploader.ashx" />
    <add key="UseAshxHandler" value="true" />
    <!-- Indica al sistema se usare l'handler ashx per eseguire il resize (si applica in modalità app pool classica) -->
    <add key="ThumbPreset" value="thumb" />
    <add key="PreviewPreset" value="preview" />
    <add key="ModuloImgGalleryPreset" value="moduloimgwide" />
  </imagegallery>
  <!-- End Cms application -->
  <location path="." inheritInChildApplications="false">
    <appSettings>
		<add key="NhConfigFileValid" value="true" />
		<!-- Impostazioni orari di disponibilità del servizio di creazione dei portali-->
		<add key="NetworksCreationAvailabilityStartTime" value="11" />
		<add key="NetworksCreationAvailabilityEndTime" value="10" />
		<!-- Trace Settings-->
		<add key="logWriter" value="log4net" />
		<add key="log4netVerbosityLevels" value="3" />
		<add key="LogFolderPath" value="{#**AbsoluteLogsFolderPath**#}"/>
		<!-- Email Settings-->
		<add key="SmtpHost" value="{#**SmtpHost**#}"/>
		<add key="SmtpPort" value="{#**SmtpPort**#}"/>
		<add key="SmtpUser" value="{#**SmtpUser**#}"/>
		<add key="SmtpPassword" value="{#**SmtpPw**#}"/>
		<add key="EnableSsl" value="{#**SmtpSsl**#}"/>
		<add key="EmailToReport" value="{#**PortalEmail**#}"/>
		<!--per liste di email usare ; come separatore -->
		<add key="EmailFrom" value="{#**PortalEmail**#}"/>
		<add key="DisableServerCertificateValidation" value="false"/>
		<!-- CMS Settings-->
		<add key="SiteManagerRoot" value="cms" />
		<add key="RootPath" value="" />
		<add key="NetworkVersion" value="2" />
		<add key="UseDefaultNetwork" value="false" />
		<add key="PasswordCripted" value="false" />
		<add key="enableMultiLanguage" value="false" />
		<add key="SiteManagerBackAccountSessionKey" value="SiteManagerBackAccountSessionKey" />
		<add key="BackAccountTokenID" value="SiteManagerCommonAccountToken" />
		<add key="FrontAccountTokenID" value="FrontCommonAccountToken" />
		<add key="SystemFolders" value="sso,cookie-policy,login-forte,setup,contact,user,services,ricerca,registrazione,charts,app_code,ajax,app_images,css,img,scripts,bin" />
		<add key="SystemFiles" value="maintenance.aspx,errorPage.aspx,grants.aspx,rss.xml,login.aspx,error.aspx,error404.aspx,details.aspx,navigate.aspx" />
		<add key="ClearPagesFiles" value="ajaxfolder.aspx,ajaxfolderpopup.aspx,info.aspx,upload.aspx" />
		<add key="DataServiceTYPE" value="database" />
		<add key="LoginProtocol" value="http" />
		<add key="VfLabelsPath" value="{#**AbsoluteCMSFolderPath**#}\configs\labels_apps.xml"/>
		<add key="HttpsHandlerClass" value="NetCms.Handlers.HttpsHandler_Off" />
		<!-- http/https-->
		<add key="ChartImageHandler" value="storage=file;timeout=20;" />
		<add key="UploadFileSizeLimit" value="200000" />
		<!-- Indicare il limite di dimensione per i file in migliaia 1M = 1000 -->
		<!-- 
		Le chiavi sottostanti devono essere rimosse in esercizio
		Valori:   
		off = valore per consegna cliente 
		on = Attiva il combo box per le login
		advanced =  Attiva il combo box per le login e mostra i dati sulle query eseguite 
		-->
        <add key="EnableNetServiceDebug" value="advanced" />
        <add key="LabelsPath" value="{#**AbsoluteCMSFolderPath**#}\labels"/>
        <!-- Settare a uno se si vogliono gestire le richieste di registrazione -->
        <add key="ManageRegRequests" value="1" />
        <add key="UniqueMailAddress" value="0" />
        <add key="BackOfficeSkinPath" value="/cms/css/admin/" />
        <add key="BreadCrumStartLabel" value="Home" />
        <add key="NotifyEnabled" value="true" />
        <add key="ChiarimentiEnabled" value="true" />
        <add key="PortalFakeDomain" value="https://nomedomainente.xy" />
        <!-- indirizzo fittizio per forzare la sostituazione del dominio ufficiale negli indirizzi che puntano allo stesso portale -->
        <add key="ActivationViaSMS" value="0" />
		<!-- Login mode - Active directory mode-->
		<add key="LoginManager" value="true" />
		<add key="LoginBoxMode" value="off" />
		<!-- off/modulo/head-->
		<add key="LoginMode" value="standard" />
		<!-- standard/onlyad/mixed -->
		<!-- ThemEngine-->
		<add key="ThemeEngineStatus" value="on"/>
		<!-- on / off-->
		<add key="ThemesFolder" value="themes"/>
		<!-- Back Theme settings-->
		<add key="PageInspector:ServerCodeMappingSupport" value="Disabled" />
    </appSettings>
  </location>
  <connectionStrings>    
    <add name="ConnectionString" connectionString="server={#**DbHost**#};database={#**DbName**#};user id={#**DbUser**#};password={#**DbPw**#};charset=utf8;port={#**DbPort**#}" providerName="MySql.Data.MySqlClient"/>
  </connectionStrings>
  <location path="." inheritInChildApplications="false">
    <system.web>
      <pages maintainScrollPositionOnPostBack="true" validateRequest="false" />
	  <httpRuntime enableVersionHeader="false" maxRequestLength="20000000" executionTimeout="1800" targetFramework="4.7.2" requestValidationMode="2.0"/>
      <globalization requestEncoding="UTF-8" responseEncoding="UTF-8" culture="it-IT" />
	  <customErrors mode="Off"/>     
	  <compilation debug="true" defaultLanguage="c#" targetFramework="4.7.2">
        <assemblies>
	        <add assembly="System.Management, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"/>
	        <add assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
	        <add assembly="System.Web.DataVisualization.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
	        <add assembly="System.Net, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"/>
	        <add assembly="System.Net.Http, Version=4.2.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"/>
	        <add assembly="System.Net.Http.Formatting, Version=5.2.7.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
	        <add assembly="System.Net.Http.WebRequest, Version=2.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"/>
	        <add assembly="System.Web.Http, Version=5.2.7.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
	        <add assembly="System.Web.Http.WebHost, Version=5.2.7.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
	        <add assembly="Microsoft.Web.Infrastructure, Version=1.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
	        <add assembly="MySql.Data, Version=6.9.12.0, Culture=neutral, PublicKeyToken=C5687FC88969C44D"/>				
	        <add assembly="PresentationCore, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
	        <add assembly="PresentationFramework, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
	        <add assembly="WindowsBase, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
	        <add assembly="System.Numerics, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
	        <add assembly="System.Runtime.Remoting, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
	        <add assembly="System.Security, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"/>
	        <add assembly="netstandard, Version=2.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51"/>				
        </assemblies>
       </compilation>
	   <httpModules>
			<add name="ImageResizingModule" type="ImageResizer.InterceptModule"/>
		</httpModules>
		<authentication mode="Windows"/>
		<xhtmlConformance mode="Strict"/>      
    </system.web>
  </location> 
  <system.webServer>
    <directoryBrowse enabled="true" showFlags="Date, Time, Size, Extension, LongDate" />
    <validation validateIntegratedModeConfiguration="false" />
	  <modules runAllManagedModulesForAllRequests="false">
		  <remove name="AnonymousIdentification"/>
		  <remove name="FileAuthorization"/>
		  <remove name="UrlAuthorization"/>
		  <remove name="WindowsAuthentication"/>
		  <remove name="UrlRoutingModule-4.0"/>
		  <add name="UrlRoutingModule-4.0" type="System.Web.Routing.UrlRoutingModule" preCondition=""/>
		  <remove name="Session"/>
		  <add name="Session" type="System.Web.SessionState.SessionStateModule" preCondition=""/>
		  <add name="CMSDBConnectionModule" type="NetCms.Connections.CMSDBConnectionModule" preCondition="managedHandler"/>
		  <add name="CMSSessionModule" type="NetCms.DBSessionProvider.FluentSessionModule" preCondition="managedHandler"/>
		  <add name="ImageResizingModule" type="ImageResizer.InterceptModule"/>
	  </modules>
	  <handlers>
		  <add name="PagineASPX" path="*.aspx" verb="*" type="NetCms.Handlers.NetworkHandler" preCondition="integratedMode"/>
		  <add name="PLUploader" path="*/plupload_handler.ashx" verb="*" type="NetUpload.UploadHandler" preCondition="integratedMode"/>
		  <add name="PLUploader2" path="*/plupload2_handler.ashx" verb="*" type="ValidatedFields.Handler.PlUpload2Handler" preCondition="integratedMode"/>
		  <add name="PLUploader2Del" path="*/plupload2_deletehandler.ashx" verb="*" type="ValidatedFields.Handler.PlUpload2FileDeleteHandler" preCondition="integratedMode"/>
		  <add name="MappingPDF" path="*/export-table-data/*.pdf" verb="*" type="NetCms.Handlers.DatatableConvertHandler" preCondition="integratedMode"/>
		  <add name="MappingCSV" path="*/export-table-data/*.csv" verb="*" type="NetCms.Handlers.DatatableConvertHandler" preCondition="integratedMode"/>
		  <add name="ChartImg" path="*/ChartImg.axd" verb="GET,HEAD,POST" type="System.Web.UI.DataVisualization.Charting.ChartHttpHandler,System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" preCondition="integratedMode"/>
		  <!-- new added -->
		  <remove name="ExtensionlessUrlHandler-ISAPI-4.0_32bit"/>
		  <remove name="ExtensionlessUrlHandler-ISAPI-4.0_64bit"/>
		  <remove name="ExtensionlessUrlHandler-Integrated-4.0"/>
		  <remove name="OPTIONSVerbHandler"/>
		  <remove name="TRACEVerbHandler"/>
		  <add name="ExtensionlessUrlHandler-ISAPI-4.0_32bit" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework\v4.0.30319\aspnet_isapi.dll" preCondition="classicMode,runtimeVersionv4.0,bitness32" responseBufferLimit="0"/>
		  <add name="ExtensionlessUrlHandler-ISAPI-4.0_64bit" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework64\v4.0.30319\aspnet_isapi.dll" preCondition="classicMode,runtimeVersionv4.0,bitness64" responseBufferLimit="0"/>
		  <add name="ExtensionlessUrlHandler-Integrated-4.0" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0"/>
	  </handlers>
	  <staticContent>
		  <remove fileExtension=".ttf"/>
		  <remove fileExtension=".woff"/>
		  <remove fileExtension=".woff2"/>
		  <mimeMap fileExtension=".ttf" mimeType="application/font-ttf"/>
		  <mimeMap fileExtension=".woff" mimeType="application/font-woff"/>
		  <mimeMap fileExtension=".woff2" mimeType="application/font-woff2"/>
		  <clientCache cacheControlMode="UseMaxAge" cacheControlMaxAge="10.00:00:00"/>
	  </staticContent>
	  <security>
		  <requestFiltering>
			  <requestLimits maxAllowedContentLength="200000000"/>
			  <verbs>
				  <add verb="PUT" allowed="true"/>
				  <add verb="OPTIONS" allowed="true"/>
				  <add verb="HEAD" allowed="true"/>
			  </verbs>
		  </requestFiltering>
	  </security>
	  <httpProtocol>
		  <customHeaders>
			  <!-- Allow Web API to be called from a different domain. -->
			  <add name="Access-Control-Allow-Origin" value="*"/>
			  <add name="Access-Control-Allow-Headers" value="Origin, X-Requested-With, Content-Type, Accept, Authorization"/>
			  <add name="Access-Control-Allow-Methods" value="POST,GET,PUT,DELETE,OPTIONS"/>
			  <add name="Access-Control-Allow-Credentials" value="true"/>
			  <remove name="X-Powered-By"/>
			  <!--
          # Security related. May help against some types of drive-by-downloads
          # Read more: https://www.owasp.org/index.php/List_of_useful_HTTP_headers
        -->
			  <add name="X-Content-Type-Options" value="nosniff"/>
			  <!--
          # Security related. May help against some types of cross-site scripting attacks
          # Read more: https://www.owasp.org/index.php/List_of_useful_HTTP_headers
        -->
			  <add name="X-XSS-Protection" value="1; mode=block"/>
			  <add name="X-Frame-Options" value="SAMEORIGIN"/>
			  <!--DENY-->
		  </customHeaders>
	  </httpProtocol>
    <httpErrors errorMode="Custom" existingResponse="Auto">
	    <remove statusCode="404"/>
	    <error statusCode="404" path="/themes/agid2019/models/errorPage.aspx?e=404" responseMode="ExecuteURL"/>		
	    <remove statusCode="403"/>
	    <error statusCode="403" path="/themes/agid2019/models/errorPage.aspx?e=403" responseMode="ExecuteURL"/>
	    <remove statusCode="503"/>
    </httpErrors>
  </system.webServer>
	<runtime>
		<assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
			<dependentAssembly>
				<assemblyIdentity name="DocumentFormat.OpenXml" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
				<bindingRedirect oldVersion="0.0.0.0-2.5.5631.0" newVersion="2.5.5631.0"/>
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral"/>
				<bindingRedirect oldVersion="0.0.0.0-12.0.0.0" newVersion="12.0.0.0"/>
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Text.Encoding.CodePages" publicKeyToken="b03f5f7f11d50a3a" culture="neutral"/>
				<bindingRedirect oldVersion="0.0.0.0-4.1.1.0" newVersion="4.1.1.0"/>
			</dependentAssembly>		
			<dependentAssembly>
				<assemblyIdentity name="Quartz" publicKeyToken="f6b8c98a402cc8a4" culture="neutral"/>
				<bindingRedirect oldVersion="0.0.0.0-2.3.3.0" newVersion="2.3.3.0"/>
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Google.Protobuf" publicKeyToken="a7d26565bac4d604" culture="neutral"/>
				<bindingRedirect oldVersion="0.0.0.0-3.6.1.0" newVersion="3.6.1.0"/>
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Diagnostics.DiagnosticSource" publicKeyToken="cc7b13ffcd2ddd51" culture="neutral"/>
				<bindingRedirect oldVersion="0.0.0.0-4.0.5.0" newVersion="4.0.5.0"/>
			</dependentAssembly>			
			<dependentAssembly>
				<assemblyIdentity name="System.Runtime.CompilerServices.Unsafe" publicKeyToken="b03f5f7f11d50a3a" culture="neutral"/>
				<bindingRedirect oldVersion="0.0.0.0-4.0.6.0" newVersion="4.0.6.0"/>
			</dependentAssembly>			
			<dependentAssembly>
				<assemblyIdentity name="System.ComponentModel.Annotations" publicKeyToken="b03f5f7f11d50a3a" culture="neutral"/>
				<bindingRedirect oldVersion="0.0.0.0-4.2.1.0" newVersion="4.2.1.0"/>
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Microsoft.Extensions.Logging.Abstractions" publicKeyToken="adb9793829ddae60" culture="neutral"/>
				<bindingRedirect oldVersion="0.0.0.0-2.2.0.0" newVersion="2.2.0.0"/>
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Threading.Tasks.Extensions" publicKeyToken="cc7b13ffcd2ddd51" culture="neutral"/>
				<bindingRedirect oldVersion="0.0.0.0-4.2.0.1" newVersion="4.2.0.1"/>				
			</dependentAssembly>			
		</assemblyBinding>
	</runtime>
</configuration>