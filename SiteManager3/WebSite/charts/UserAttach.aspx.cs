﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.Users;
using NetCms.Users.CustomAnagrafe;
using System.IO;

public partial class charts_UserAttach : System.Web.UI.Page
{
    private AllegatoUtente _RequestedAllegato;
    private AllegatoUtente RequestedAllegato
    {
        get
        {
            if (_RequestedAllegato == null)
            {
                NetUtility.RequestVariable req = new NetUtility.RequestVariable("id", NetUtility.RequestVariable.RequestType.QueryString);
                if (req.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                    _RequestedAllegato = CustomAnagrafeBusinessLogic.GetAllegatoUtenteById(req.IntValue);
            }
            if (_RequestedAllegato == null)
                throw new NetCms.Exceptions.ServerError500Exception("L'allegato selezionato non esiste.");

            return _RequestedAllegato;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Clear();
        Response.ContentType = RequestedAllegato.ContentType;
        Response.AddHeader("content-length", RequestedAllegato.File.Length.ToString());
        Response.AddHeader("content-disposition", "attachment;filename=" + RequestedAllegato.FileName);

        //MemoryStream stream = new MemoryStream(RequestedAllegato.File);
        //stream.WriteTo(Response.OutputStream);
        Response.BinaryWrite(RequestedAllegato.File);
        Response.Flush();
        Response.Close();
        Response.End();
    }

    //public bool ByteArrayToFile(string _FileName, byte[] _ByteArray)
    //{
    //    try
    //    {
    //        // Open file for reading
    //        System.IO.FileStream _FileStream = new System.IO.FileStream(_FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);

    //        // Writes a block of bytes to this stream using data from a byte array.
    //        _FileStream.Write(_ByteArray, 0, _ByteArray.Length);

    //        // close file stream
    //        _FileStream.Close();

    //        return true;
    //    }
    //    catch (Exception _Exception)
    //    {
    //        // Error
    //        Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
    //    }

    //    // error occured, return false
    //    return false;
    //}
}