﻿<%@ WebHandler Language="C#" Class="Upload" %>

using System;
using System.Web;
using System.IO;
using System.Collections.Generic;
using System.Web.SessionState;

public class Upload : IHttpHandler
{
   public void ProcessRequest (HttpContext context) 
   {
      context.Response.ContentType = "text/plain";
      context.Response.Expires = -1;      
      try
      {
          
          HttpPostedFile postedFile = context.Request.Files["Filedata"];
  
          string savepath = "";
          string tempPath = "";

          tempPath = context.Request["folder"];
       
          //If you prefer to use web.config for folder path, uncomment below line:
          //tempPath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];

          savepath = context.Server.MapPath(tempPath);

          string filename = postedFile.FileName;
          
          if (!Directory.Exists(savepath))
          {
              Directory.CreateDirectory(savepath);
          }

          postedFile.SaveAs(savepath + @"\" + filename);          
          context.Response.Write(tempPath + "/" + filename);
          context.Response.StatusCode = 200;
      }
      catch (Exception ex)
      {
          context.Response.Write("Error: " + ex.Message);
          //string[] logmsg = new string[2];
          //logmsg[0] = "[Data/time: " + DateTime.Now.ToString() + "][Exception: " + ex.Message + "]";
          //Logging.Log.log(Logging.Log.Level.Error, ex.Message, "Uploader.ashx", ex);
          //Sedute.Common.Log.addrows(logmsg);
      }
   }

   public bool IsReusable 
   {
      get 
      {
          return false;
      }
   }
}