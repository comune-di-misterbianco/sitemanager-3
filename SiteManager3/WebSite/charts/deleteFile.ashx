﻿<%@ WebHandler Language="C#" Class="FileDelete" %>

using System;
using System.Web;
using System.IO;
using System.Collections.Generic;
using System.Web.SessionState;

public class FileDelete : IHttpHandler
{
   public void ProcessRequest (HttpContext context) 
   {
      context.Response.ContentType = "text/plain";
      context.Response.Expires = -1;      
      try
      {          
          string filePath = context.Request.Form["FileNameTOBeDeleted"];              
          
          string filePathFull = context.Server.MapPath(filePath);

          if (File.Exists(filePathFull))
          {
              File.Delete(filePathFull);
              context.Response.Write("filedeleted");              
          }
          else         
              context.Response.Write("filenotfound");
          context.Response.StatusCode = 200;      
      }
      catch (Exception ex)
      {
          context.Response.Write("Error: " + ex.Message);       
      }
   }

   public bool IsReusable 
   {
      get 
      {
          return false;
      }
   }
}