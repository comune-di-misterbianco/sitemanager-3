﻿<%@ WebHandler Language="C#" Class="FileDeletePlUpload" %>

using System;
using System.Web;
using System.IO;
using System.Collections.Generic;
using System.Web.SessionState;
using Newtonsoft.Json;
public class FileDeletePlUpload : IHttpHandler
{
    public void ProcessRequest (HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        context.Response.Expires = -1;
        try
        {


            var jsonString = "";

            context.Request.InputStream.Position = 0;
            using (var inputStream = new StreamReader(context.Request.InputStream))
            {
                jsonString = inputStream.ReadToEnd();
            }
            //string filePath = context.Request.Form["FileNameTOBeDeleted"];
            //var path = (JToken)jsonString["FileNameTOBeDeleted"];
            dynamic deserializedValue = JsonConvert.DeserializeObject(jsonString);
            string path = deserializedValue["FileNameTOBeDeleted"];
            string filePathFull = context.Server.MapPath(path);

            if (File.Exists(filePathFull))
            {
                File.Delete(filePathFull);
                context.Response.Write("filedeleted");
            }
            else
                context.Response.Write("filenotfound");
            context.Response.StatusCode = 200;
        }
        catch (Exception ex)
        {
            context.Response.Write("Error: " + ex.Message);
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}