﻿// JScript File
var Homepage_SelectStarted = false;
var Homepage_CurentAction = "";

function Homepage_Start(mid,action)
{
    var oCtr = document.getElementById(mid);
    var oSrc = document.getElementById("HomepageHFSource");

    if(oCtr && oSrc)
    {
        Homepage_SelectStarted = true;
        oSrc.value = mid;
        Homepage_CurentAction = action;
    }
}
function Homepage_ModuloOver(mid)
{    
    Homepage_BlockOver(mid,"Modulo");
}
function Homepage_ModuloOut(mid)
{   
    Homepage_BlockOut(mid,"Modulo");
   
}

function Homepage_ContainerOver(mid,full)
{

    Homepage_BlockOver(mid,"Container",full=='true');
}
function Homepage_ContainerOut(mid,Append,full)
{   
    Homepage_BlockOut(mid,"Container",full=='true');
}

function Homepage_BlockOver(mid,obj,full)
{
    if(Homepage_SelectStarted)
    {
        if((Homepage_CurentAction=="move" && obj == "Container"  && !full) || 
           (Homepage_CurentAction=="switch" && obj == "Modulo") ||   
           (Homepage_CurentAction=="movecontainer" && obj == "Container")||
           (Homepage_CurentAction=="switchcontainer" && obj == "Container"))
        {
            var oCtr = document.getElementById(mid);
            oCtr.className += " Homepage" + obj + "_Hover";
        }
    }
}
function Homepage_BlockOut(mid,obj,full)
{   
    if(Homepage_SelectStarted)
    {
        if((Homepage_CurentAction=="move" && obj == "Container" && !full) || 
           (Homepage_CurentAction=="switch" && obj == "Modulo") ||   
           (Homepage_CurentAction=="movecontainer" && obj == "Container")||
           (Homepage_CurentAction=="switchcontainer" && obj == "Container"))
        {
            var oCtr = document.getElementById(mid);
            oCtr.className = oCtr.className.replace(" Homepage" + obj + "_Hover","");
        }
    }
}

function Homepage_BlockSelected(mid,obj,full)
{
    var notfull = (!full || full!='true');
    var MoveM = (obj == "Container" && Homepage_CurentAction == "move") && notfull;
    var SwitchC = (obj == "Container" && Homepage_CurentAction == "switchcontainer");
    var SwitchM = (obj == "Modulo" && Homepage_CurentAction == "switch") && notfull;

    if(Homepage_SelectStarted && (MoveM || SwitchC || SwitchM))
    {
        var oTrg = document.getElementById("HomepageHFTarget");
        var oAct = document.getElementById("HomepageHFAction");
        if(oTrg && oAct)
        {   
            oTrg.value = mid;
            oAct.value = Homepage_CurentAction;
            SubmitPage();
        }
        else
        {
            alert("Si è verificato un errore non previsto, non quindi sarà possibile portare a termine l'operazione.")
        }   
        
    }
}

function SubmitPage()
{
    oFrms = document.getElementsByTagName("FORM");
    if(oFrms && oFrms.length >0)
        oFrms[0].submit();
}

$(document).ready(function () {

    getAndFillContainerSorting();

    $('#sortRows').on("click", function () {
        //toggle visibiliy handle icons
        $('.handle_container').toggleClass('handle_container_active');

        // fill hidden field
        getAndFillContainerSorting();

        $("#abortSortRows").removeAttr('disabled');
        $("#saveSortRows").removeAttr('disabled');

        //enable sortable
        //$(".HomepageContainer_Colcx").sortable(
        //    {
        //        handle: '.handle_container',
        //        placeholder: "ui-state-highlight",
        //        //forceHelperSize: true,
        //        //forcePlaceholderSize: true,
        //        axis: "y",
        //        helper: 'clone',
        //        cursor: 'move',
        //        start: function (e, ui) {
        //            ui.placeholder.height(ui.item.children().height());
        //        }
        //    }
        //);
        //$(".HomepageContainer_Colcx").disableSelection();

        sortable('.HomepageContainer_Colcx', {
            placeholderClass: 'ui-state-highlight',
            forcePlaceholderSize: true,
            handle: '.handle_container'
        });
        sortable('.HomepageContainer_Colcx')[0].addEventListener('sortupdate', function (e) {

            
            getAndFillContainerSorting();
            /*
            This event is triggered when the user stopped sorting and the DOM position has changed.
        
            e.detail.item - {HTMLElement} dragged element
        
            Origin Container Data
            e.detail.origin.index - {Integer} Index of the element within Sortable Items Only
            e.detail.origin.elementIndex - {Integer} Index of the element in all elements in the Sortable Container
            e.detail.origin.container - {HTMLElement} Sortable Container that element was moved out of (or copied from)
            e.detail.origin.itemsBeforeUpdate - {Array} Sortable Items before the move
            e.detail.origin.items - {Array} Sortable Items after the move
        
            Destination Container Data
            e.detail.destination.index - {Integer} Index of the element within Sortable Items Only
            e.detail.destination.elementIndex - {Integer} Index of the element in all elements in the Sortable Container
            e.detail.destination.container - {HTMLElement} Sortable Container that element was moved out of (or copied from)
            e.detail.destination.itemsBeforeUpdate - {Array} Sortable Items before the move
            e.detail.destination.items - {Array} Sortable Items after the move
            */
        });

    });
}); 

function getAndFillContainerSorting() {
    var items = [];
    $('.HomepageContainer_Colcx .HomepageContainer').each(function (index) {
        items.push(
            $(this).attr("id")+'|'+(index+1)
        );
        //items.push({
        //    'id': $(this).attr("id"), 'order': index + 1
        //});
    });
    //console.log(items);
    $('#containerOrder').val(items);

}