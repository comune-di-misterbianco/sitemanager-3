﻿function share(e) {

    $('#shareModal').modal();
    $('#itemid').val(""); //hiddenfield
    $('#Ruolo_list').tagit('removeAll');
    $('#Nodo_list').tagit('removeAll');

    // Singolo item
    if (e !== 0){   
        var obj = this;
        var id = obj.ID();
        
        if (id > 0) {
            var parameter = { idDocument: id };
            $('#itemid').val(id);
            $.ajax({
                url: "/api/CDShareStatus/GetShareNode",
                contentType: "application/json;charset=utf-8",
                type: "GET",
                data: parameter,
                success: function (nodes) {
                    var array = JSON.parse(nodes); // conterrà i risultati della chiamata
                    var table = document.getElementById("sharesTable").getElementsByTagName('tbody')[0];
                    table.innerHTML = "";
                    
                    if (array.length > 0) {
                        for (var a in array) {
                            var nodo;
                            var ruolo;
                            if (array[a].NomeUfficio === null || array[a].NomeUfficio === "")
                                nodo = "Tutti";
                            else
                                nodo = array[a].NomeUfficio;
                            if (array[a].NomeRuolo === null || array[a].NomeRuolo === "")
                                ruolo = "Tutti";
                            else
                                ruolo = array[a].NomeRuolo;

                            var row = table.insertRow(0);
                            var nodocell = row.insertCell(0);
                            var ruolocell = row.insertCell(1);
                            var eliminacell = row.insertCell(2);
                            nodocell.innerHTML = '<span class="tag label label-default"> ' + nodo + ' </span>';
                            ruolocell.innerHTML = '<span class="tag label label-info"> ' + ruolo + ' </span>';
                            if (array[a].Direct)
                                eliminacell.innerHTML = '<a href="#"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true" id="deleteShare_' + array[a].Id + '" onclick="deleteItemShare(this)"></span><a>';
                            else
                                eliminacell.innerHTML = '<span class="tag label label-default"> Ereditato </span>';
                        }
                    }
                    else {
                        $('#sharesTable > tbody:last-child').append('<tr><td colspan="3"><p class="no-share-found">Nessuna condivisione attiva per l\'elemento selezionato.</p></td></tr>');
                    }
                },
                error: function (error) {
                    console.log('error on getSharedStatus' + error.status);
                }
            });
        }
    }
    else  // MultiSelezione di documenti
    {
        //Inserire Gestione multi items
        var table = document.getElementById("sharesTable").getElementsByTagName('tbody')[0];
        table.innerHTML = "";
        
        var items = "";
        var checkedBoxes = document.querySelectorAll('input[name=oid]:checked');
        for (var i = 0; i < checkedBoxes.length; i++) {

            items += checkedBoxes[i].textContent;
            if (i !== checkedBoxes.length - 1)
                items += ",";
        }
        if (items === "") {
            printMessage("Errore! Seleziona almeno una cartella o un documento.", "alert alert-danger");           
        }
        else {
            $('#itemid').val(items);
        }
    }
}

function deleteItemShare(e)
{
	//chiamare API di cancellazione
    var i = e.parentNode.parentNode.parentNode.rowIndex;	
    var parameter = { idDocument: e.id};

    $.ajax({
        url: "/api/CDShareStatus/DeleteShare",
        contentType: "application/json;charset=utf-8",
        type: "POST",
        data: JSON.stringify(parameter),
        success: function (response) {
            if (response) {
                var table = document.getElementById("sharesTable").getElementsByTagName('tbody')[0];
                table.deleteRow(i - 1);
                if (table.children.length === 0) { // se zero elementi rimangono aggiungo il testo nessun cond...
                    $('#sharesTable > tbody:last-child').append('<tr><td colspan="3"><p class="no-share-found">Nessuna condivisione attiva per l\'elemento selezionato.</p></td></tr>');
                }
                
                printMessage("Condivisione rimossa con successo.", "alert alert-success");
            }
            else {
                printMessage("Errore: non è stato possibile aggiungere la condivisione. Si invita a correggere i dati inseriti e riprovare", "alert alert-danger");
            }
        },
        error: function (error) {
            printMessage(error, "alert alert-danger");
        }
    });
}

function submitShare()
{
	//$('#shareButton').click();
	var share = $('#itemid').val();
	var ruoli = $('#Ruolo_tags').val();
	var nodi = $('#Nodo_tags').val();

	var items = { shares: share, roles: ruoli, nodes: nodi };
	$.ajax({
		url: "/api/CDShareStatus/CreateShare",
		contentType: "application/json;charset=utf-8",
		type: "POST",
		data: JSON.stringify(items),
		success: function(response)
		{
            if (response.length > 0) {
                
                var error_msgs = '';
                var confirm_msgs = '';

                $.each(response, function (index, value) {
                   
                    if (value.Status)
                        confirm_msgs += value.Message + "\n\r";
                    else
                        error_msgs += value.Message + "\n\r";
                });


                printMessage(confirm_msgs, "alert alert-success",true);
                printMessage(error_msgs, "alert alert-danger", true);

                var array = share.split(',');

                if (array.length === 1) {
                    var parameter = { idDocument: share };

                    $.ajax({
                        url: "/api/CDShareStatus/GetShareNode",
                        contentType: "application/json;charset=utf-8",
                        type: "GET",
                        data: parameter,
                        success: function (nodes) {
                            var array = JSON.parse(nodes); // conterrà i risultati della chiamata
                            var table = document.getElementById("sharesTable").getElementsByTagName('tbody')[0];
                            table.innerHTML = "";
                            for (var a in array) {
                                var nodo;
                                var ruolo;
                                if (array[a].NomeUfficio === null || array[a].NomeUfficio === "")
                                    nodo = "Tutti";
                                else
                                    nodo = array[a].NomeUfficio;
                                if (array[a].NomeRuolo === null || array[a].NomeRuolo === "")
                                    ruolo = "Tutti";
                                else
                                    ruolo = array[a].NomeRuolo;

                                var row = table.insertRow(0);
                                var nodocell = row.insertCell(0);
                                var ruolocell = row.insertCell(1);
                                var eliminacell = row.insertCell(2);

                                nodocell.innerHTML = '<span class="tag label label-default"> ' + nodo + ' </span>';
                                ruolocell.innerHTML = '<span class="tag label label-info"> ' + ruolo + ' </span>';
                                eliminacell.innerHTML = '<a href="#"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true" id="deleteShare_' + array[a].Id + '" onclick="deleteItemShare(this)"></span><a>';
                            }
                        },
                        error: function (error) {
                            printMessage(error, "alert alert-danger");
                        }
                    });
                }
                else {
                    printMessage("Condivisione effettuata con successo.", "alert alert-success");		            
                }
            }
            else {
                printMessage("Error!", "alert alert-danger");
            }
        },
		error: function (error) {			
                printMessage(error, "alert alert-danger");				
		}
	});	
}

function printMessage(message, cssclass, noclean) {
    noclean = noclean || false;
    var div = document.createElement("div");
    div.className = cssclass;

    var messageControl = document.createTextNode(message);
    div.appendChild(messageControl);

    if (noclean !== true)
        $('#message-share-area').empty();

    $('#message-share-area').append(div);

    $('#message-share-area').show();
    setTimeout(function () {
        $('#message-share-area').hide();
    }, 10000);
}

// Enumerators
var directions = {
    ascending: 1,
    descending: 2
},
    defaultDirection = directions.ascending,
    defaultSortColumn = 'Nome';

function ShareDocsViewModel() {
    var self = this;

    self.historyFolder = [];

    self.uploader = {};

    // parametri per l'ordinamento
    self.sortColumn = ko.observable(defaultSortColumn);
    self.sortDirection = ko.observable(defaultDirection);

    // current item id
    self.folderId = ko.observable(''); 

    // observable array for folder and doc
    self.allItems = ko.observableArray([]);
    self.Items = ko.observableArray([]);

    // observable array briciole di pane
    self.breadcrumb = ko.observableArray();

    //********************************************************
    //      da riumuovere (eseguire prima check dipendenze
    //********************************************************
    self.foldersList = ko.observableArray([]);  // sarà sostituito con items
    
    self.isRootFolder = ko.computed(function () {
        return !self.folderId() || self.folderId() === -1;
    });        

    self.addFolder = function () {
        // validazione form lato javascript    
        // ???
        var foldername = $('#Nome').val();
        var currentFolder = self.folderId(); //$('#currentFolderID').val();
		if(currentFolder === undefined)
			currentFolder = -1;
        var parameter = { nomeFolder: foldername, folderID: currentFolder };
        // post dei dati a web api per add folder        
        $.ajax({
            url: "/api/CDItems/AddFolder",
            contentType: "application/json;charset=utf-8",
            dataType: 'json',
            type: "POST",
            async : false,
            data: JSON.stringify(parameter),
            success: function (newFolder) {
				
				if(newFolder.ErrorMessage !== "" && newFolder.ID === 0)
				{
					$('#Nome').val("");
					var mess = newFolder.ErrorMessage;
					printMessage(newFolder.ErrorMessage, "alert alert-danger");
					
					$('#folderModal').modal('hide');
					
				}
				else 
				{
					var newItem = new ItemViewModel(self, newFolder.IsFolder, newFolder.ID, newFolder.Nome, newFolder.DataInserimento, newFolder.Parent, newFolder.Path)
					// aggiorna la tabella 
					self.foldersList.push(newItem);
					self.allItems.push(newItem);
					self.foldersList.valueHasMutated();
					// chiudo la modale
					printMessage("Cartella aggiunta con successo.", "alert alert-success");
					$('#Nome').val("");
					$('#folderModal').modal('hide');
					self.reloadItems();
				}
             
            },
            error: function (error) {
                console.log('error on addFolder' + error.status);
                printMessage("Errore: la cartella non è stata inserita.", "alert alert-danger");
            }
        });
    };

    self.deleteItem = function (item) {        
        // mostra una modale di conferma per l'eliminazione dell'item selezionato                
        $('#confirmDelFolder').modal({ backdrop: 'static', keyboard: false }).one('click', '#delete', function () { Delete(item) });
    };      

    self.init = function () {
        var rootFolder = new ItemViewModel(self, true);
        rootFolder.Nome('Home');
        self.foldersList.removeAll();       
        self.folderId('');

        self.breadcrumb.push(rootFolder);

        self.uploader = initFineUploader();

        loadData();        
    };

    self.reloadItems = function () {

        self.foldersList.removeAll();

        //console.log('cartella corrente' + self.folderId());

        var currentFolder = (self.folderId() === "") ? -1 : self.folderId();

        loadData(currentFolder);

        self.foldersList.sort(sortItems);
    };

    self.changeFolder = function (selectedFolder) {       

        self.foldersList.removeAll();

        self.folderId(selectedFolder);

        // cerco la folder da inserire o rimuovere alla breadcrumb
        var i, folder;
        for (i = 0; i < self.allItems().length; i++) {
            if (self.allItems()[i].ID() === selectedFolder) {
                folder = self.allItems()[i];
                break;
            }
        }
        setBreadCrumb(folder || self.breadcrumb()[0]);

        $('#currentFolderID').val(selectedFolder);

        loadData(selectedFolder);      
    };

    self.openParentFolder = function () {

        var parentId = '',
            currentId = self.folderId(),
            currentFolder = '';

        // cerco la folder nell'array self.allItems()
        for (var l = 0; l < self.allItems().length; l++) {
            if (self.allItems()[l].ID() === currentId) {
                currentFolder = self.allItems()[l];
                parentId = self.allItems()[l].folderId;
                break;
            }
        }
        var indexItemToRemove = self.allItems().indexOf(currentFolder);
        self.allItems().splice(indexItemToRemove, 1);

        self.changeFolder(parentId);
    };
  
    /*************************************************************/
    /*              Sorts folder by given column
    /*************************************************************/
    self.sortItems = function (column) {
        // Saves or cancels all edits

        var columnBefore = self.sortColumn(),
            wasDescending = self.sortDirection() === directions.descending;
        if (columnBefore === column) {
            self.sortDirection(wasDescending ? directions.ascending : directions.descending);
        } else {
            self.sortDirection(directions.ascending);
        }
        self.sortColumn(column);

        self.reloadItems();

    };

    /*************************************************************/
    //            variabili per la gestione dell'ordine
    /*************************************************************/

    self.isSortedAsc = function (column) {
        return column === self.sortColumn() && self.sortDirection() === directions.ascending;
    }

    self.isSortedDesc = function (column) {
        return column === self.sortColumn() && self.sortDirection() === directions.descending;
    }

    //********************************
    // private functions
    //********************************

    function Delete(item) {
  
        var currentFolder = findFolder(item);  
        var parentFolderID = currentFolder.Parent();

        if (currentFolder !== null)
        {
            $.ajax({
                url: "/api/CDItems/DeleteItem",
                contentType: "application/json;charset=utf-8",
                type: "POST",
                dataType: 'json',
                async: false,
                data: JSON.stringify({ itemID: item }),
                success: function (esito) {
                    if (esito) {                    
                        self.folderId(parentFolderID);
                        self.changeFolder(parentFolderID);
                        if (currentFolder.isFolder())
                            printMessage("Cartella eliminata con successo.", "alert alert-success");
                        else
                            printMessage("Documento eliminato con successo.", "alert alert-success");

                        $('#confirmDelFolder').modal('hide');
                        
                    }
                },
                error: function (data) {
                    alert('error occured on item ' + currentFolder.Nome() + ' deleting ');
                    //stampare messaggio di errore
                    printMessage("Errore: la cartella non è stata eliminata.", "alert alert-error");
                }
            });
        }
    }

    // funzione di ricerca folder per id dall'array self.allItems
    function findFolder(id)
    {
        var i, folder;

        for (i = 0; i < self.allItems().length; i++) {
            if (self.allItems()[i].ID() === id) {
                folder = self.allItems()[i];
                break;
            }
        }

        return folder;
    }

    function setBreadCrumb(folder) {
        var remove;

        if (!folder.ID()) {
            remove = true;
        }
        else {
            // cerco la cartella da rimuovere
            for (i = 0; i < self.breadcrumb().length; i++) {
                if (self.breadcrumb()[i].ID() === folder.ID()) {
                    remove = i + 1;
                    break;
                }
            }
        }

        if (remove) {
            for (i = remove; i < self.breadcrumb().length; i++) {
                self.breadcrumb.pop();
            }
        } else {
            // Add folder to path 
            self.breadcrumb.push(folder);
        }

    }

    function loadData(idFolder) {
        $.ajax({
            url: "/api/CDItems/GetItems",
            contentType: "application/json;charset=utf-8",
            type: "GET",
            dataType: 'json',
            data: { folderID: idFolder },
            success: function (data) {
                $.each(data, function (index) {                
                    self.foldersList.push(new ItemViewModel(self, data[index].IsFolder, data[index].ID, data[index].Nome, data[index].DataInserimento, data[index].Parent, data[index].Path, data[index].Size, data[index].Ext));                    
                    self.allItems.push(new ItemViewModel(self, data[index].IsFolder, data[index].ID, data[index].Nome, data[index].DataInserimento, data[index].Parent, data[index].Path, data[index].Size, data[index].Ext));
                });
                self.foldersList.sort(sortItems);

            },
            error: function (data) {
                alert("error occured");
            }
        });
    }

    function initFineUploader() {
        $('#fine-uploader').fineUploader({            
            autoUpload: true,
            button: $('#upload-btn'),
            request: {
                endpoint: '/api/CDItems/UploadFile/',
            },            
            thumbnails: {
                placeholders: {
                    waitingPath: '/js/fine-uploader/placeholders/waiting-generic.png',
                    notAvailablePath: '/js/fine-uploader/placeholders/not_available-generic.png'
                }
            },
            validation: {
                allowedExtensions: ['jpeg', 'jpg', 'txt', 'pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx'],
                itemLimit: 10,
                sizeLimit: 200000000 // 200MB in bytes
            },         
        }).on('progress', function (event, id, filename, uploadedBytes, totalBytes) {           
            if (uploadedBytes < totalBytes) {
                progress = '"' + fileName + '" uploading...  ' + Math.round(uploadedBytes /
                  totalBytes * 100) + '%';
                $('#qq-progress-bar').html(progress);
            }
            else {
                $('#qq-progress-bar').html('saving');
            }
        }).on('complete', function (event, id, filename, json, xhr) {
            if (json.success)
            {              
                var date = json.Data;
                var path = json.Path;               
                var size = json.Size;
                var ext = json.Ext;

                var item = new ItemViewModel(self, false, id, filename, date, self.folderId, path, size, ext);
                self.foldersList.push(item);
                self.allItems.push(item);
            }
            else {
                console.log("Errore durante il trasferimento dei file." + filename);                
            }
        }).on('upload', function (event, id, filename) {
            var folderId = $('#currentFolderID').val();
            $(this).fineUploader('setParams', { fid: folderId }, id);
        }).on('allComplete', function (event, succeeded, failed) {
            if (succeeded.length > 0){
                printMessage("Documenti trasferiti con successo.", "alert alert-success");
            }
            console.log(succeeded);
            console.log(failed);
        }).on('error', function (event, id, filename, errorReason, xhr) {
            printMessage("Errore durante il trasferimento del file." + errorReason, "alert alert-danger");
            console.log("Errore durante il trasferimento dei file." + errorReason);
        })
    }
    
    function printMessage(message, cssclass) {

        var div = document.createElement("div");      
        div.className = cssclass;

        var messageControl = document.createTextNode(message);
        div.appendChild(messageControl);        

        $('#messagearea').empty();
        $('#messagearea').append(div);
     
        $('#messagearea').show();
        setTimeout(function ()
        {
            $('#messagearea').hide();
        }, 10000);
    }

    function sortItems(left,right)
    {
        var column = self.sortColumn(),
            isAscending = self.sortDirection() === directions.ascending,
            leftValue = column === 'Nome' || left.isFolder ? left.Nome().toLowerCase() : left.DataInserimento().toLowerCase(),
            rightValue = column === 'Nome' || right.isFolder ? right.Nome().toLowerCase() : right.DataInserimento().toLowerCase();
        
        if (left.isFolder !== right.isFolder){
            if (left.isFolder) {
                return isAscending ? -1 : 1;
            } else {
                return isAscending ? 1 : -1;
            }
        }

        return leftValue === rightValue
           ? 0
           : (leftValue < rightValue
               ? (isAscending ? -1 : 1)
               : (isAscending ? 1 : -1));
    }
}

function ItemViewModel(parentObj, isFolder, id, nome, datainsert, parent, path, size, ext) {

    var self = this;

    self.ID = ko.observable();
    self.Nome = ko.observable().extend({ required: true });

    self.Path = ko.observable(path);
    self.Extention = 'doc-unk ' + ext; //'doc ' +
    self.DataInserimento = ko.observable(datainsert);
    self.Parent = ko.observable(parent);
    self.Size = ko.observable(size);
    self.isFolder = ko.observable(isFolder);
    self.parentObj = parentObj;

    self.folderId = parent;

    // private property
    self.oldNome = null; // mi servirà durante il processo di rename

    self.ID(id).Nome(nome);

    self.navigateFolder = function () {
        if (self.isFolder) {
            self.parentObj.changeFolder(self.ID());
        }
    };

    self.deleteItem = function () {        
            self.parentObj.deleteItem(self.ID());                
    };

    self.showItem = function (item) {
        var url = item.Path();
        var height = $('#viewDoc').height() - 200 + 'px';

        if (url !== undefined) {
            $('#viewDoc').on('shown.bs.modal', function () {
                $('.modal-title').text(item.Nome());
                $('iframe').attr('src', url);
                $('iframe').attr('height', height);
            });
            $('#viewDoc').on('hidden.bs.modal', function () {
                $('iframe').attr('src', '');
                $('.modal-title').text('');
            });
            $('#viewDoc').modal({ show: true });//{ backdrop: 'static', keyboard: false }
        }
        return false;
    };

   /* self.toggleRowBtn = function (item, event) {

        console.log(event);
        console.log(event.currentTarget);
        if (event.currentTarget.parentElement !== undefined) {
            console.log(event.currentTarget.parentElement);
            console.log(event.currentTarget.parentNode);
        }             
    }*/
}

$(document).ready(function () {

    var elencoItem = new ShareDocsViewModel();
    elencoItem.init();

    ko.applyBindings(elencoItem);
});


