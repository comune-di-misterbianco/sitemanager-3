﻿$(document).ready(function () {

        $('#dialogRichiesta').dialog({
            autoOpen: false,
            show: {
                effect: 'blind',
                duration: 500
            },
            hide: {
                effect: 'blind',
                duration: 500
            },
            position: {
                my: "left top",
                at: "left bottom",
                of: "#visualizza_button"
            }
        });

        $('#visualizza_button').click(function () {
            $('#dialogRichiesta').dialog('open');
            return false;
        });
});