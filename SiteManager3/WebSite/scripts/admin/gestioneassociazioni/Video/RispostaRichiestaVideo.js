﻿function start() {

    $('.ui-effects-wrapper').hide();
    $('#dialogVideo').dialog({
        autoOpen: false,
        closeOnEscape: false,
        resizable: false,
        draggable: false,
        modal: true,
        show: {
            effect: 'blind',
            duration: 500
        },
        hide: {
            effect: 'blind',
            duration: 500
        },
        title: 'Richiesta Video',
        buttons: {

            Conferma: function () {
                $('#submitEvento').click();
            },
            Annulla: function () {
                $(this).dialog('close');
                if ($('#RichiestaVideoForm').find('.not-valid').length > 0) {
                    $('#RichiestaVideoForm').find('.not-valid>.validation-results').hide();
                }
            }
        }
    });

    if ($('#RichiestaVideoForm').find('.not-valid').length > 0) {
        $('#dialogVideo').dialog('open');
    }

    $('#add_button').click(function () {
        $('#dialogVideo').dialog('open');
        $('#dialogVideo').parent().appendTo($('form:first'));
        $('.ui-effects-wrapper').hide();
        return false;
    });
}