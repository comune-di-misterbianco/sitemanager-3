﻿//$(document).ready(function () {

    var map;
    var pos;
    var lat;
    var lon;
    var denominazione;
    var ID;

    var contentString = '<div id="content">' +
      '<div id="siteNotice">' +
      '</div>' +
      '<h1 id="firstHeading" class="firstHeading">Sei Qui</h1>' +
      '<div id="bodyContent">' +
      denominazione +
      '</div>' +
      '</div>';


    function initialize() {

        geocoder = new google.maps.Geocoder();

        var mapOptions = {
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        pos = new google.maps.LatLng(lat, lon);

        var infowindow = new google.maps.InfoWindow({
            //map: map,
            //position: pos,
            content: denominazione
        });

        var marker = new google.maps.Marker({
            position: pos,
            map: map,
            title: "HERE",
            draggable: true
        });

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });

        google.maps.event.addListener(marker, 'dragend', function (event) {
            //alert('final position is ' + event.latLng.lat() + ' / ' + event.latLng.lng());

            $('#ID').val(ID);
            $('#Latitudine').val(event.latLng.lat());
            $('#Longitudine').val(event.latLng.lng());

            var latlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());

            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    } else {
                        alert('Indirizzo non trovato');
                    }
                } else {
                    alert('Geocoder non funziona: ' + status);
                }
            });
        });


        map.setCenter(pos);

        // Try HTML5 geolocation
//        if (navigator.geolocation) {
//            navigator.geolocation.getCurrentPosition(function (position) {
//                pos = new google.maps.LatLng(position.coords.latitude,
//                                       position.coords.longitude);

//                var infowindow = new google.maps.InfoWindow({
//                    //map: map,
//                    //position: pos,
//                    content: contentString
//                });

//                var marker = new google.maps.Marker({
//                    position: pos,
//                    map: map,
//                    title: "HERE",
//                    draggable: true
//                });

//                //            infowindow.open(map, marker);
//                google.maps.event.addListener(marker, 'click', function () {
//                    infowindow.open(map, marker);
//                });

//                map.setCenter(pos);
//            }, function () {
//                handleNoGeolocation(true);
//            });
//        } else {
//            // Browser doesn't support Geolocation
//            handleNoGeolocation(false);
//        }


    }

//    function handleNoGeolocation(errorFlag) {
//        if (errorFlag) {
//            var content = 'Error: The Geolocation service failed.';
//        } else {
//            var content = 'Error: Your browser doesn\'t support geolocation.';
//        }

//        var options = {
//            map: map,
//            position: new google.maps.LatLng(60, 12),
//            content: content
//        };

//        var infowindow = new google.maps.InfoWindow(options);
//        map.setCenter(options.position);
//    }

    function start(latitudine, longitudine, nome,id) {
        lat = latitudine;
        lon = longitudine;
        denominazione = nome;
        ID = id;

        $('#ID').val(ID);
        $('#Latitudine').val(lat);
        $('#Longitudine').val(lon);

        google.maps.event.addDomListener(window, 'load', initialize);
    }


    //    if (navigator.geolocation) {
    //        navigator.geolocation.getCurrentPosition(success);
    //    } else {
    //        alert("Geo Location is not supported on your current browser!");
    //    }
    //    function success(position) {
    //        var latitude = position.coords.latitude;
    //        var longitude = position.coords.longitude;
    //        var city = position.coords.locality;
    //        var myLatlng = new google.maps.LatLng(latitude, longitude);
    //        var myOptions = {
    //            center: myLatlng,
    //            zoom: 12,
    //            mapTypeId: google.maps.MapTypeId.ROADMAP
    //        };
    //        var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
    //        var marker = new google.maps.Marker({
    //            position: myLatlng,
    //            title: "lat: " + latitude + " long: " + longitude
    //        });

    //        marker.setMap(map);
    //        var infowindow = new google.maps.InfoWindow({ content: "<b>User Address</b><br/> Latitude:" + latitude + "<br /> Longitude:" + longitude + "" });
    //        infowindow.open(map, marker);
    //    }
//});	