﻿
    var map;
    var pos;
    var lat;
    var lon;
    var ID;
    var associazioniJSON;
    var path;

    

    function getContent(denominazione, indirizzo, idAssociazione) {
        var contentString = '<div class="content">' +
          '<div id="siteNotice">' +
          '</div>' +
          '<h1 id="firstHeading" class="firstHeading">'+denominazione+'</h1>' +
          '<div id="bodyContent">' +
          '<h3 id="thirdHeading" class="thirdHeading">' + indirizzo + '</h3>' +
          '<a href=' + path + idAssociazione + '>Vai alla pagina dell\'Associazione</a>' +
          '</div>' +
          '</div>';
        return contentString;   
    }

    function initialize() {
        var mapOptions = {
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        // HTML5 geolocalizazione
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                pos = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);

                map.setCenter(pos);

                var infowindow = new google.maps.InfoWindow();
                var marker, i;

                for (i = 0; i < associazioniJSON.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(associazioniJSON[i]["Latitudine"], associazioniJSON[i]["Longitudine"]),
                        map: map
                    });

                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            //infowindow.setContent( associazioniJSON[i]["Denominazione"]);
                            infowindow.setContent(getContent(associazioniJSON[i]["Denominazione"], associazioniJSON[i]["Indirizzo"], associazioniJSON[i]["IDAnagraficaAssociazione"]));
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                }
            }, function () {
                handleNoGeolocation(true);
            });
        } else {
            // Browser doesn't support Geolocation
            handleNoGeolocation(false);
        }
    }

    function handleNoGeolocation(errorFlag) {
        if (errorFlag) {
            var content = 'Error: The Geolocation service failed.';
        } else {
            var content = 'Error: Your browser doesn\'t support geolocation.';
        }

        var options = {
            map: map,
            position: new google.maps.LatLng(60, 12),
            content: content
        };

        var infowindow = new google.maps.InfoWindow(options);
        map.setCenter(options.position);
    }

    function start(associazioni,path) {
        associazioniJSON = associazioni;
        this.path = path;
        google.maps.event.addDomListener(window, 'load', initialize);
    }
