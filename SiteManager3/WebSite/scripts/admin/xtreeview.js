﻿// JScript File
    function xTreeViewCheck(id)
	{		
		li = document.getElementById(id).parentNode
		this_input = li.getElementsByTagName("INPUT")[0]
		stato= this_input.checked
		
		ul = li.getElementsByTagName("UL")[0]
		if(ul!=null)
		{
		    liColl = ul.getElementsByTagName("INPUT")
    		if(liColl!=null)    		
		        for(i=0;i<liColl.length;i++)
		        {
			        liColl[i].checked=stato
			        if(stato)
			        liColl[i].disabled=true
			        else
			        liColl[i].disabled=false
		        }	
		}		
	}
	
	function xTreeViewSelectNode(value,id,label,ControlID)
	{
	    //alert(value+id+label+ControlID)
		if(document.getElementById(ControlID+"Value")!=null)
		{
		    //alert(ControlID+"Value - "+document.getElementById(ControlID+"Value").value)
		    document.getElementById(ControlID+"Value").value=value;
		}
		if(document.getElementById(ControlID+"Label")!=null)
		{
		    //alert(ControlID+"Label - "+document.getElementById(ControlID+"Label").value)
		    document.getElementById(ControlID+"Label").value=label;
		}
		if(document.getElementById(ControlID)!=null)
		{
		    //alert(ControlID+"ID - "+document.getElementById(ControlID+"ID").value)
		    document.getElementById(ControlID).value=value;
		}
		if(document.getElementById(id)!=null)
		{
		    oldid=0
		   
		    if(document.getElementById(ControlID+"ID")!=null)
		    {
		        oldid = document.getElementById(ControlID+"ID").value
		        document.getElementById(ControlID+"ID").value=id;
		    }
		    if(document.getElementById(id+'_link')!=null )
		    {
		    document.getElementById(id+'_link').className='xNodeSelected';
		    }
		    if( document.getElementById(oldid+'_link')!=null)
		    {
		    document.getElementById(oldid+'_link').className='';
		    }
		}		
	}
	
	function xTreeViewOpenColapse(id,plus,minus)
	{
		if(document.getElementById(id)!=null)
		{
		    ul = document.getElementById(id).getElementsByTagName("UL")[0]
		    //alert(ul.style.display)
		    display = ul.style.display;
		    if(display == 'none')
		        ul.style.display = 'block';
		    else
		        ul.style.display = 'none';
		    img = document.getElementById(id+'_OpenColapse')
		    
		    Inizio = img.src.length - plus.length
		    Lunghezza = plus.length
		    src = img.src.substr(Inizio,Lunghezza)
		    
		    if(src == plus)
		        img.src = minus;
            else
            {
		        Inizio = img.src.length - minus.length
		        Lunghezza = minus.length
		        src = img.src.substr(Inizio,Lunghezza)
    		    
		        if(src == minus)
		            img.src = plus;
		    }
		    xTreeViewNodesState(id)
		}
	}
    
    function xTreeViewNodesState(value)
	{
		if(document.getElementById("node0")!=null)
		{
		    ulcoll = document.getElementById("node0").getElementsByTagName("ul")
		    srcs=""
		    for(i=0;i<ulcoll.length;i++)
		    {
			    if(ulcoll[i].style.display=="block")
			    srcs+=ulcoll[i].parentNode.id+",";
		    }	
		    SetCookie("xTreeView",srcs)
		    
		}		
	}
	
	function xTreeViewInitNodeState(minus)
	{
		srcs = GetCookie("xTreeView");
		if(srcs)
		{
		arraynode = srcs.split(',');
		
	    for(i=0;i<arraynode.length;i++)
	    {
	        li = document.getElementById(arraynode[i])        
	        
	        if(li!=null)
		    {
		        ul = li.getElementsByTagName("UL")[0]
		        ul.style.display='block';
		        img = li.getElementsByTagName("IMG")[0]
		        img.src=minus
		    }
	    }
	    }	/**/
	}
function SetCookie (name,value,expires,path,domain,secure) 
{
  document.cookie = name + "=" + escape (value) +
    ((expires) ? "; expires=" + expires.toGMTString() : "") +
    ((path) ? "; path=" + path : "") +
    ((domain) ? "; domain=" + domain : "") +
    ((secure) ? "; secure" : "");
}

function DeleteCookie (name,path,domain) 
{
  if (GetCookie(name)) {
    document.cookie = name + "=" +
      ((path) ? "; path=" + path : "") +
      ((domain) ? "; domain=" + domain : "") +
      "; expires=Thu, 01-Jan-70 00:00:01 GMT";
  }
}

function GetCookie (name) 
{
  var arg = name + "=";
  var alen = arg.length;
  var clen = document.cookie.length;
  var i = 0;
  while (i < clen) {
    var j = i + alen;
    if (document.cookie.substring(i, j) == arg)
      return getCookieVal (j);
	i = document.cookie.indexOf(" ", i) + 1;
    if (i == 0) break; 
  }
  return null;
}

function getCookieVal (offset)
{
	var str = document.cookie.indexOf(";", offset);
	if(str==-1) str = document.cookie.length;
	return unescape(document.cookie.substring(offset, str));
}
