
$(document).ready( function() {   

    $("#dialog").dialog({ autoOpen: false });
    $("#insertDialog").dialog({ autoOpen: false });
	
    
	     let calendarEl = document.getElementById('calendar');
            let calendar = new FullCalendar.Calendar(calendarEl, {
                initialView: 'dayGridMonth',
				headerToolbar: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
      },
            buttonText: {
                today: 'Oggi',
                month: 'Mese',
                week: 'Settimana',
                day: 'Giorno',
				list : 'Lista'
            },
            
                events: eventi,
                eventClick: (info) => {
					
                    $.ajax({
                        type: 'GET',
                        url: urlReservationById+ info.event.id,                       
                        success: currentEvent =>{
                            $("#dialog").dialog({
                                autoOpen: true,
                                modal: true,
                                width: '800px',
                                buttons: {
                                     'Modifica': () => onclick=location.href = urlModPage+'?reservation='+ info.event.id,
									 'Elimina': () => ($.ajax({
										type: 'POST',
										url: urlDeleteReservation,
										data: {
											ListIds: [parseInt(info.event.id)],
											GenericId: info.event.extendedProps.agendaRef
										},
										
										success: result =>{$("#dialog").dialog('close'); info.event.remove();},
										error: error => alert(error)
									 }))
                                },
                            });
                            $('#code').text(info.event.extendedProps.bookcode);
							$('#analysisname').text(info.event.extendedProps.analysisname);
                            $('#date').text($.datepicker.formatDate('dd/mm/yy', info.event.start));
							var h = info.event.start.getHours();
							var m = info.event.start.getMinutes();
							var s = info.event.start.getSeconds();

							if (h < 10) h = '0' + h;
							if (m < 10) m = '0' + m;
							if (s < 10) s = '0' + s;

							var dateString = h + ':' + m + ':' + s;
							$('#hour').text(dateString);
                            info.event.extendedProps.firstname ?   $('#customer').text(info.event.extendedProps.firstname + ' ' +  info.event.extendedProps.lastname ) : $('#customer').text('Non disponibile');
                            //info.event.extendedProps.email ?  $('#email').text(info.event.extendedProps.email) : $('#email').text('Non disponibile');
                            info.event.extendedProps.phoneNumber ?  $('#phone').text(info.event.extendedProps.phoneNumber) : $('#phone').text('Non disponibile');
                        },
                        error: error => alert(error)
                    })
                },
                
            });
			calendar.setOption('locale', 'it');
            calendar.render();
   
	
})

