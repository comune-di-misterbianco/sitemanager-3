﻿var clockID = 0;
var ClockControlID;
var DisableSeconds = false;

function UpdateClock() 
{
   if(clockID) 
   {
      clearTimeout(clockID);
      clockID = 0;
   }

   var tDate = new Date();

   var Hours = tDate.getHours() < 10 ?"0"+tDate.getHours():tDate.getHours();
   var Minutes = tDate.getMinutes()< 10 ?"0"+tDate.getMinutes():tDate.getMinutes();
   var Seconds = "";
   if(!DisableSeconds)
        Seconds = ":" + (tDate.getSeconds()< 10 ?"0"+tDate.getSeconds():tDate.getSeconds());

   document.getElementById(ClockControlID).innerHTML = "" + Hours + ":" + Minutes + Seconds;
   
   clockID = setTimeout("UpdateClock()", DisableSeconds?10000:1000);
}
function StartClock(clockControlID,disableSeconds) 
{
    if(disableSeconds)
        DisableSeconds = disableSeconds;
        
    ClockControlID = clockControlID;
    clockID = setTimeout("UpdateClock()", 500);
}

function KillClock() 
{
   if(clockID) 
   {
      clearTimeout(clockID);
      clockID  = 0;
   }
}