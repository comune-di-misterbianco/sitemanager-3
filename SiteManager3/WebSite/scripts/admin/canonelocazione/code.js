﻿function start() {

    setDefaultValue();
    setComponenteNucleoFamiliareForm();
    toggleRemoveButton();

//    $("#ComponenteNucleoFamiliareForm").hide();
//    $('#add_button').click(function () {
//        $("#ComponenteNucleoFamiliareForm").toggle('blind', 500);
//        return false;
//    }); 
   

    $('#ComponenteNucleoFamiliareForm').click(function () {
        setComponenteNucleoFamiliareForm();
    });

    function toggleRemoveButton() {
        var rowCount = $('#TabNucleoFamiliare tr').length;
        if (rowCount == 0) {
            $("#remove_member_button").hide();
        } else {
            $("#remove_member_button").show();
        }
    }

    function setComponenteNucleoFamiliareForm() {
        if ($('#NoReddito').is(':checked')) {
            HideRedditi();
        }
        else {
            ShowRedditi();
        }
    }

    function HideRedditi() {

        var LavoroDipendenteParent = $('#LavoroDipendente').parent();
        if (LavoroDipendenteParent.is(':visible')) {
            LavoroDipendenteParent.hide();
        }

        var LavoroAutonomoParent = $('#LavoroAutonomo').parent();
        if (LavoroAutonomoParent.is(':visible')) {
            LavoroAutonomoParent.hide();
        }

        var PensioneParent = $('#Pensione').parent();
        if (PensioneParent.is(':visible')) {
            PensioneParent.hide();
        }

        var AltriRedditiParent = $('#AltriRedditi').parent();
        if (AltriRedditiParent.is(':visible')) {
            AltriRedditiParent.hide();
        }

        var LavoroSaltuarioParent = $('#LavoroSaltuario').parent();
        if (LavoroSaltuarioParent.is(':visible')) {
            LavoroSaltuarioParent.hide();
        }

    }

    function ShowRedditi() {

        var LavoroDipendenteParent = $('#LavoroDipendente').parent();
        if (!LavoroDipendenteParent.is(':visible')) {
            LavoroDipendenteParent.show();
        }

        var LavoroAutonomoParent = $('#LavoroAutonomo').parent();
        if (!LavoroAutonomoParent.is(':visible')) {
            LavoroAutonomoParent.show();
        }

        var PensioneParent = $('#Pensione').parent();
        if (!PensioneParent.is(':visible')) {
            PensioneParent.show();
        }

        var AltriRedditiParent = $('#AltriRedditi').parent();
        if (!AltriRedditiParent.is(':visible')) {
            AltriRedditiParent.show();
        }

        var LavoroSaltuarioParent = $('#LavoroSaltuario').parent();
        if (!LavoroSaltuarioParent.is(':visible')) {
            LavoroSaltuarioParent.show();
        }
    }

    function setDefaultValue() {

        var selComune = $('#ComponenteNucleoFamiliareForm').find("select#ComuneNascita").val() || "";


        if ($('#ComponenteNucleoFamiliareForm').find('.not-valid').length > 0 || selComune == "") {
            //alert("Campo non valido");          
        }
        else {
            $('#Nome').val("");
            $('#Cognome').val("");
            $('#DataNascita').val("");
            $('#CF').val("");
            $('#Cognome').val("");
            $('#LavoroDipendente').val("0,00");
            $('#LavoroAutonomo').val("0,00");
            $('#Pensione').val("0,00");
            $('#AltriRedditi').val("0,00");
            $('#LavoroSaltuario').val("0,00");
            $('input:checkbox').removeAttr('checked');
            $("#RelazioneParentela option[value='']").attr('selected', true) 
        }
    }
};