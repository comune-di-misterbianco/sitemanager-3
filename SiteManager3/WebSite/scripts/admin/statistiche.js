
/*

var data = {


	labels: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto","Settembre","Ottobre","Novembre","Dicembre"], 
    datasets: [ 
        { 
            label: "Referti Totali", 
            fillColor: "rgba(99,240,220,0.2)", 
            strokeColor: "rgba(99,240,220,1)", 
            pointColor: "rgba(99,240,220,1)", 
            pointStrokeColor: "#fff", 
            pointHighlightFill: "#fff", 
            pointHighlightStroke: "rgba(220,220,220,1)", 
            data: [8, 11, 18, 22, 24, 26, 34, 39] 
        }
    ] 
};*/
var options = {
 
    // {boolean} Indica se visualizzare le linee della griglia
    scaleShowGridLines : true,
     
    // {string} Colore delle linee della griglia
    scaleGridLineColor : "rgba(0,0,0,.05)",
     
    // {number} Spessore delle linee della griglia
    scaleGridLineWidth : 1,
     
    // {boolean} Indica se le linee tra i punti sono curve (di Bezier)
    bezierCurve : true,
     
    // {number} Indica la tensione delle curve tra i punti
    bezierCurveTension : 0.4,
     
    // {boolean} Indica se mostrare un pallino per ogni punto
    pointDot : true,
     
    // {number} Definisce l’arrotondamento di ciascun punto
    pointDotRadius : 4,
     
    // {number} Definisce lo spessore di ciascun punto 
    pointDotStrokeWidth : 5,
     
    // {number} Aumenta lo spazio sensibile al click attorno a ciascun punto 
    pointHitDetectionRadius : 20,
     
    // {boolean} Indica se visualizzare il tratto per i dataset 
    datasetStroke : true,
     
    // {number} Indica lo spessore del tratto per i dataset 
    datasetStrokeWidth : 2,
     
    // {boolean} Indica se ciascun dataset deve essere riempito con un colore 
    datasetFill : false,
     
    // {string} Definisce il template per il markup della legenda del grafico
    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.len>h; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
};

var ctx = document.getElementById("myLineChart").getContext("2d");
var myLineChart = new Chart(ctx).Line(data,options);
//document.getElementById("myLineChart").onclick = function(e)
//{
//	var point = myLineChart.getPointsAtEvent(e);
//	var yvalue = point[0].value;
//};