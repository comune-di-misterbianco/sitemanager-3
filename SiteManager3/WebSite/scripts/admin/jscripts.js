﻿// JScript File
function OpenFolder_Grants(id,name)
    {
        oTable = document.getElementById(id);
        oLines = document.getElementById(name+"_Lines");
        
        if(oTable!=null)
        {
            ArrayRows = oTable.getElementsByTagName("tr");
            
            var ParentDepth = name.split('A').length;
           
            for(i=0;i<ArrayRows.length;i++)
            {
                if(ArrayRows[i].id.match(name) && ArrayRows[i].id != name)
                {
                    if(ArrayRows[i].className.match("GrantsReview_Hide"))
                    {
                        var Depth = ArrayRows[i].id.split('A').length;
                        if(Depth == ParentDepth+1)
                        {
                            ArrayRows[i].className = ArrayRows[i].className.replace("GrantsReview_Hide","GrantsReview_Show")
                            if(oLines)
                                oLines.className = oLines.className.replace("Plus","Minus")
                        }
                    } 
                    else
                    {
                        ArrayRows[i].className = ArrayRows[i].className.replace("GrantsReview_Show","GrantsReview_Hide ")
                        if(oLines)
                           oLines.className = oLines.className.replace("Minus","Plus")
                    }
                }                    
            }
                        /**/
        }
    }
    
function setToolbarCaption(val)
{
    if (val)
        document.getElementById("Toolbar_JSLabels").innerHTML = val;
    else
        document.getElementById("Toolbar_JSLabels").innerHTML = "";
}

function AllowsShowApp()
{
    if(document.getElementById("AllowFormApp")!=null)
    {
        selectedIndex = document.getElementById("AllowFormApp").options.selectedIndex
        selectedValue = document.getElementById("AllowFormApp").options[selectedIndex].value

        if(document.getElementById("AllowFormShowedApp")!=null)
        {
            oldShowed = document.getElementById(document.getElementById("AllowFormShowedApp").value+"_AllowSet")

            if(oldShowed!=null)
            {
                oldShowed.style.display = "none";
            }
            newShowed = document.getElementById(selectedValue+"_AllowSet")

            if(newShowed!=null)
            {
                newShowed.style.display = "block";
                document.getElementById("AllowFormShowedApp").value = selectedValue
            }
        }
            //document.getElementById("AllowFormApp") = document.getElementById("AllowFormShowedApp");
    }
}

    function setpage(val,id)
    {
        if(id==null)
        id = "page";
    	if (document.getElementById(id)!=null) {
    		document.getElementById(id).value = val;
           document.getElementById("PageForm").submit();
       }
    }
    
    
    function SetSelectedPage(val,id)
    {
    	if (document.getElementById(id)!=null) {
    		document.getElementById(id).value = val;
           document.getElementById("PageForm").submit();
       }
    }
    
    function setdel(val)
    {
       if (confirm("ATTENZIONE!!Sei sicuro di voler procedere con l'eliminazione?"))
    	if (document.getElementById("del")!=null) {
    		document.getElementById("del").value = val;
           document.getElementById("PageForm").submit();
       }
    }
    
    
    function setEnable(val)
    {
    	if (document.getElementById("enable")!=null) {
    		document.getElementById("enable").value = val;
           document.getElementById("PageForm").submit();
       }
    }

    function addGroupToUser()
    {
        if(document.getElementById("xTreeViewSelectedNodeValue")!=null
        && document.getElementById("xTreeViewSelectedNodeLabel")!=null
        && document.getElementById("ListGruppi")!=null)
        {
            oList = document.getElementById("ListGruppi")
            id = document.getElementById("xTreeViewSelectedNodeValue").value
            label = document.getElementById("xTreeViewSelectedNodeLabel").value
            exist = false
            for(i=0;i<oList.options.length;i++)
            {            
                if(oList.options[i].value == id)
                    exist = true
            }
            if(!exist)
            {                   
                oOption = new Option(label,id);
                oList.options[oList.options.length] = oOption;
            }
        }        
    }
    
    function addGroupToUser()
    {
        if(document.getElementById("xTreeViewSelectedNodeValue")!=null
        && document.getElementById("xTreeViewSelectedNodeLabel")!=null
        && document.getElementById("ListGruppi")!=null)
        {
            oList = document.getElementById("ListGruppi")
            id = document.getElementById("xTreeViewSelectedNodeValue").value
            label = document.getElementById("xTreeViewSelectedNodeLabel").value
            exist = false
            for(i=0;i<oList.options.length;i++)
            {            
                if(oList.options[i].value == id)
                    exist = true
            }
            if(!exist)
            {                   
                oOption = new Option(label,id);
                oList.options[oList.options.length] = oOption;
            }
        }
        refreshListGruppiID();        
    }
    
    function removeGroupToUser()
    {
        if(document.getElementById("ListGruppi")!=null)
        {
            oList = document.getElementById("ListGruppi")
            exist = false
            for(i=0;i<oList.options.length;i++)
            {            
                if(oList.options[i].selected == true)
                    oList.options[i] = null;
            }            
        }
        refreshListGruppiID();        
    }
    
    function refreshListGruppiID()
    {
        oList = document.getElementById("ListGruppi")
        oIDList = document.getElementById("ListGruppiID")
        oIDList.value="";
        for(i=0;i<oList.options.length;i++)
        {            
            if(i != 0)
                oIDList.value+=",";
            oIDList.value+=oList.options[i].value;
        }
    }
    
    function setUserToAdd(id)
    {
        
        oInput = document.getElementById("setUserToAdd")
        
        oForm = document.getElementById("PageForm")

        if(oInput!=null && oForm!=null)
        {
            oInput.value = id;
            oForm.submit();
        }/**/
    }
    
    function setFormValue(Value,TargetControlID,SubmitPage,SourceControlID)
    {    
        SubmitSourceID = SourceControlID ? SourceControlID : TargetControlID;
        
        oInput = document.getElementById(TargetControlID)  
        
        if(oInput!=null)
        {
            oInput.value = Value;
            if(SubmitPage == 1)
                Submit(SubmitSourceID);
        }
    }
    
    function Submit(ControlID)
    {    
        if(ControlID)
            setSubmitSource(ControlID);
        
        oForms = document.getElementsByTagName("FORM");
        
        if(oForms)
            oForms[0].submit();
            
    }
    
    function setFormValueConfirm(Value,TargetControlID,Submit,text,SourceControlID)
    {    
       if(!text)
            text = "ATTENZIONE!!Sei sicuro di voler procedere con l'eliminazione?"
       if (confirm(text))
            setFormValue(Value,TargetControlID,Submit,SourceControlID);
    }
    
    function DeleteDoc(Value,ControlID,Doc)
    {    
       if (confirm("ATTENZIONE!!\nSei sicuro di voler procedere \ncon l'eliminazione del documento: "+Doc+"?"))
       {
            oInput = document.getElementById(ControlID)
            
            oForm = document.getElementById("PageForm")
            
            if(oInput!=null && oForm!=null)
            {
                oInput.value = Value;
                oForm.submit();                
            }
        }
    }
    
    function setShowApplication(ControlID)
    {        
        
        oList = document.getElementById(ControlID);
        oSelected = document.getElementById("SelectedApplication");
        
        oOld = document.getElementById("AppCriteria0")
        
        if(oOld!=null)
            oOld.style.display = "none";
        
        oOld = document.getElementById("AppCriteria"+oSelected.value)
        
        if(oOld!=null)
            oOld.style.display = "none";
           
        if(oList!=null)
        {
            value = oList.options[oList.options.selectedIndex].value
            if(value=="")
                value=0
            TargetID = "AppCriteria"+value;           
            oTarget = document.getElementById(TargetID);            
            oTarget.style.display = "block";            
            oSelected.value = value;
        }
    }


    function previewImage(value)
    {                
        oFrame = document.getElementById("preview_frame");
        oBox = document.getElementById("preview_block");
        oDocs = document.getElementById("docs_block");
        
        if(oBox!=null&&oFrame!=null&&oDocs!=null)
        {
            oFrame.src="imagepreview.aspx?path="+value;
            oBox.style.display = "block";
            oDocs.style.height = "296px";            
        }
    }
    
    function selectFile(value,desc)
    {                
        oSel = document.getElementById("Selezionato");
      
        
        if(oSel!=null)
            oSel.value=value;
        else 
        {
            oSel = document.getElementById("src");
            if(oSel!=null)
                oSel.value=value;
        }
        if(desc)
        {
            oDesc = document.getElementById("descrizione");
            if(oDesc!=null)
                oDesc.value=desc;
            
            oAlt = document.getElementById("Alternate");
            if(oAlt!=null)
                oAlt.value=desc;
            else
            {
                oAlt = document.getElementById("alt");
                if(oAlt!=null)
                    oAlt.value=desc;
            }
        }
    }
//    function LoadImage()
//    {         
//        ftb = window.opener.FTB_API[document.getElementById('TargetFreeTextBox').value];       
//        img = ftb.GetNearest('img');
//        
//        
//	    if (img!=null) 
//	    {
//		    newhref = img.src
//	        newserver = ExtractServerName(newhref)
//            if(ExtractServerName(document.URL) == newserver)
//            {
//               newhref = newhref.replace("http://","");
//               newhref = newhref.replace(newserver,"");
//		    }
//		    
//            oSelected = document.getElementById("Selezionato");
//            oSelected.value = newhref;
//            oAlt = document.getElementById("Alternate");
//            oAlt.value = img.alt;
//        }
//    }

    function LoadImage() {
        ftb = window.opener.FTB_API[document.getElementById('TargetFreeTextBox').value];
        img = ftb.GetNearest('img');

        if (img != null) {
            newhref = img.src
            newserver = ExtractServerName(newhref)
            if (ExtractServerName(document.URL) == newserver) {
                newhref = newhref.replace("http://", "");
                newhref = newhref.replace(newserver, "");
            }

            oSelected = document.getElementById("Selezionato");
            oSelected.value = newhref;
            oAlt = document.getElementById("Alternate");
            oAlt.value = img.alt;
            oAlign = document.getElementById("Align");
            oCaption = document.getElementById("ImgCaption");

            if (img.className != null) {
                var imgclass = img.className;
                imgclass = imgclass.replace(' imgcaption', '');

                if (imgclass == 'Nessuno') {
                    oAlign.options[0].selected = true;
                }
                if (imgclass == 'Sinistra') {
                    oAlign.options[1].selected = true;
                }
//                if (imgclass == 'Centrato') {
//                    oAlign.options[2].selected = true;
//                }
                if (imgclass == 'Destra') {
                    oAlign.options[2].selected = true;
                }
            }

            if (img.className.search('imgcaption') != -1) {
                oCaption.checked = true;
            }
        }
    }
    
    
//    function LoadLink()
//    {      
//        ftb = window.opener.FTB_API[document.getElementById('TargetFreeTextBox').value];       
//        a = ftb.GetNearest('a'); 
//	    if (a!=null) 
//	    {
//	        newhref = a.href
//	        newserver = ExtractServerName(newhref)
//            if(ExtractServerName(document.URL) == newserver)
//            {
//               newhref = newhref.replace("http://","");
//               newhref = newhref.replace(newserver,"");
//		    }
//            oSelected = document.getElementById("Selezionato");
//            oSelected.value = newhref;
//            oDesc = document.getElementById("descrizione");        
//            oDesc.value = a.title;
//	    }
//    }

    function LoadLink() {
        ftb = window.opener.FTB_API[document.getElementById('TargetFreeTextBox').value];
        a = ftb.GetNearest('a');
        if (a != null) {
            newhref = a.href
            newserver = ExtractServerName(newhref)
            if (ExtractServerName(document.URL) == newserver) {
                newhref = newhref.replace("http://", "");
                newhref = newhref.replace(newserver, "");
            }
            oSelected = document.getElementById("Selezionato");
            oSelected.value = newhref;
            oDesc = document.getElementById("descrizione");
            oDesc.value = a.title;

            oEsterno = document.getElementById("Esterno");

            if (a.rel == "esterno") {
                oEsterno.checked = true;
            }
        }
    }
    
    function ExtractServerName(url)
    {
        var server = "" ;
        var protocol_less_url = url.replace("http://","");
        
        if(protocol_less_url.length != url.length)
        {
            var urlArray = protocol_less_url.split('/');
            server = urlArray[0];
        }
        return server;
    }
    
    function SendFile()
    {                
        oSelected = document.getElementById("Selezionato");
        
        if(oSelected!=null)
        {
            if(oSelected.value.length>0)
            {            
                window.returnValue = oSelected.value;
                window.close();
            }
            else
                alert("ERRORE! File non selezionato!")
        }
    }
//    function SendImage()
//    {                
//        oSelected = document.getElementById("Selezionato");    
//        oAlt = document.getElementById("Alternate");
//        
//        if(oAlt!=null&&oSelected!=null)
//        {
//            if(oAlt.value.length>0&&oSelected.value.length>0)
//            {
//                ftb = document.getElementById('TargetFreeTextBox').value;
//                img="<img src=\""+oSelected.value+"\" alt=\""+oAlt.value+"\"  />"                
//                window.opener.FTB_API[ftb].InsertHtml(img);
//                window.close();
//            }
//            else
//                alert("ERRORE! File non selezionato o testo alternativo non inserito!")
//        }
    //    }

    function SendImage() 
    {
        oSelected = document.getElementById("Selezionato");
        oAlt = document.getElementById("Alternate");
        oAlign = document.getElementById("Align");

        oCaption = document.getElementById("ImgCaption");


        if (oAlt != null && oSelected != null) 
        {
            if (oAlt.value.length > 0 && oSelected.value.length > 0) 
            {
                ftb = document.getElementById('TargetFreeTextBox').value;
                if (oAlign.value == "Nessuno") 
                {
                    if (oCaption.checked)
                        img = "<img src=\"" + oSelected.value + "\" class=\"imgcaption\" alt=\"" + oAlt.value + "\"  />";
                    else
                        img = "<img src=\"" + oSelected.value + "\" alt=\"" + oAlt.value + "\"  />";                    
                }
                else 
                {
                    if (oCaption.checked)
                        img = "<img src=\"" + oSelected.value + "\" class=\"" + oAlign.value + " imgcaption\" alt=\"" + oAlt.value + "\"  />";                                           
                    else                        
                        img = "<img src=\"" + oSelected.value + "\" class=\"" + oAlign.value + "\" alt=\"" + oAlt.value + "\"  />";                        
                }
                window.opener.FTB_API[ftb].InsertHtml(img);
                window.close();
            }
            else
                alert("ERRORE! File non selezionato o testo alternativo non inserito!")
        }
    }


    function SendImageTinyMCE(ImageDialog)
    {                
        oSelected = document.getElementById("src");    
        oAlt = document.getElementById("alt");
        if(oAlt!=null&&oSelected!=null)
            ImageDialog.insert(oSelected.value,oAlt.value)
    }
    
 
//function SendLink() 
//{ 
//    ftb_id= document.getElementById('TargetFreeTextBox').value;
//    ftb = window.opener.FTB_API[ftb_id];
//	link = ftb.GetNearest('a');
//	href = document.getElementById('Selezionato');
//	if (href.value == '')
//	{
//           alert("ERRORE! Collegamento ipertestuale non valido!")
//		return false;
//	}
//	if (!link) 
//	{
//		ftb.ExecuteCommand('createlink',null,href.value);
//		link = ftb.GetNearest('a');
//            
//            window.close();
//	}
//	updateLink(link)
//}
//function updateLink(link)
// {
//	if (link) {
//		href = document.getElementById('Selezionato');
//		title = document.getElementById('descrizione');
//		link.href = href.value;
//		link.title = title.value;
//		
//        window.close();
//	}
//}

    function SendLink() {
        ftb_id = document.getElementById('TargetFreeTextBox').value;
        ftb = window.opener.FTB_API[ftb_id];
        link = ftb.GetNearest('a');

        href = document.getElementById('Selezionato');
        if (href.value == '') {
            alert("ERRORE! Collegamento ipertestuale non valido!")
            return false;
        }
        if (!link) {

            //		ftb.ExecuteCommand('createlink',null,href.value); //<----- si ferma qui
            //		link = ftb.GetNearest('a');
            //      window.close();
            var tempUrl = 'http://tempuri.org/tempuri.html';
            ftb.ExecuteCommand('createlink', null, tempUrl);
            var links = ftb.designEditor.document.getElementsByTagName('a');
            for (var i = 0; i < links.length; i++) {
                if (links[i].href == tempUrl) {
                    link = links[i];
                    break;
                }
            }
        }
        updateLink(link)
    }
    function updateLink(link) {
        if (link) {

            href = document.getElementById('Selezionato');
            title = document.getElementById('descrizione');

            link.href = href.value;
            link.title = title.value;

            rel = document.getElementById('Esterno');
            if (rel.checked) {
                link.rel = "esterno";
            }
            window.close();
        }
    }

function Preview(path,id,docpath)
{
     var sFeatures;
    sFeatures = "height:1024px,width:768px,menubar=no,location=no,resizable=no,scrollbars=yes,status=no,modal=yes";
				
	//window.open(path+"preview.aspx?id="+id+"&path="+docpath, "Anteprima", sFeatures);
    window.open(path + "preview.aspx?id=" + id, "Anteprima", sFeatures);
}

function setSearchFormShowStatus()
{
    oSearchForm = document.getElementById("SearchForm");
    oSearchForm.style.display = oSearchForm.style.display == "block"?"none":"block";
}

function ColapseSide(id)
{
    oBlock = document.getElementById(id+"_Content");
    if(oBlock)
        oBlock.style.display = oBlock.style.display == "none"?"block":"none";
    
    oLink = document.getElementById(id+"_Link");
    if(oLink)
    {
        if(oLink.className.match("Collapsed"))
        {
            oLink.className = oLink.className.replace("Collapsed","Opened");
        }
        else
        {
            oLink.className = oLink.className.replace("Opened","Collapsed");
        }
    }
}

function ResumeDoc(Value,ControlID,Doc)
    {    
       if (confirm("ATTENZIONE!!\nSei sicuro di voler procedere \ncon l'il ripristino del documento: "+Doc+"?"))
       {
            oInput = document.getElementById(ControlID)
            
            oForm = document.getElementById("PageForm")
            
            if(oInput!=null && oForm!=null)
            {
                setSubmitSource(ControlID);
                oInput.value = Value;
                oForm.submit();                
            }
        }
    }
    
    function ResumeFolder(Value,ControlID,Doc)
    {    
       if (confirm("ATTENZIONE!!\nSei sicuro di voler procedere \ncon l'il ripristino della cartella: "+Doc+"?"))
       {
            oInput = document.getElementById(ControlID)
            
            oForm = document.getElementById("PageForm")
            
            if(oInput!=null && oForm!=null)
            {
                setSubmitSource(ControlID);
                oInput.value = Value;
                oForm.submit();                
            }
        }
    }
    
    
    function RefuseAction(Value,ControlID)
    {
        setFormValue(Value,ControlID,false);
        oInput = document.getElementById("notify");
        oInput.style.display = "block";
    }
    
    
    function GrantsTable_All(id,column)
    {
        oTable = document.getElementById(id);
        if(oTable!=null)
        {
            ArrayRadio = oTable.getElementsByTagName("INPUT");
           
            for(i=0;i<ArrayRadio.length;i++)
                if(ArrayRadio[i].value.substring(0,1) == column)
                    ArrayRadio[i].checked = true;
        }
    }
    
    
    function HideShowSide()
    {
        oSide = document.getElementById("side");
        oCorpo = document.getElementById("corpo");
        
        if(oSide.className.match("sideHide"))
        {
            oSide.className = "";
            oCorpo.style.margin = "0px 0px 0px 215px"; 
            
            oSide.onmouseover = null;
            oSide.onmouseout = null;        
              
            oGlobale = document.getElementsByTagName("BODY")[0];
            oGlobale.className = "";              
        }
        else
        {
            oCorpo.style.margin = "0px 0px 0px 20px"; 
            oSide.className = "sideHide";             
            
            oGlobale = document.getElementsByTagName("BODY")[0];
            oGlobale.className = "bodyNoBG";    
              
        }
    }
    
    /* funzioni per la gestione della popup dei template*/

    function SetTemplate(tpl) {
        ftb = window.opener.document.getElementById('TargetFreeTextBox').value;
        tpl2 = tpl.replace(/\#/g, "\"");
        window.opener.FTB_API[ftb].SetHtml(tpl2);
        window.close();
    }

    /* Funzioni per la gestione del codice sorgente con editarea*/
    function HtmlCode(id_textarea) {
        var oHtmlCode = document.getElementById(id_textarea);

        oHtmlCode = editAreaLoader.getValue(id_textarea)

        ftb = document.getElementById('TargetFreeTextBox').value;

        if (oHtmlCode != null) {
            var filteredSrc = restoreObjFlash(oHtmlCode);
            window.opener.FTB_API[ftb].SetHtml(filteredSrc);
            window.close();
        }
    }
    function SetHtmlCode(id_textarea) {
        ftb = document.getElementById('TargetFreeTextBox').value;
        var source = window.opener.FTB_API[ftb].GetHtml();

        editAreaLoader.setValue(id_textarea, replaceObjFlash(source));
    }






    function replaceObjFlash(source) {
        var newSource = source;
        var objFlash = "<object class=\"##classname##\" data=\"##data##\" alt=\"##alt##\" width=\"##width##\" height=\"##height##\"";
        objFlash += " type=\"application/x-shockwave-flash\" >";
        aImgs = source.match('<img[^>]*>', 'g');

        if (aImgs != null) {
            for (i = 0; i < aImgs.length; i++) {
                strImg = aImgs[i];
                if (strImg.match('objFlash') != null) {
                    flashSrc = strImg.match(/title="(.*?)"/);
                    if (flashSrc != null)
                        flashSrc = flashSrc[1];
                    flashAlt = strImg.match(/alt="(.*?)"/);
                    if (flashAlt != null)
                        flashAlt = flashAlt[1];
                    flashClassName = strImg.match(/class="(.*?)"/);
                    if (flashClassName != null)
                        flashClassName = flashClassName[1];

                    flashWidth = strImg.match(/width="(.*?)"/);
                    if (flashWidth != null)
                        flashWidth = flashWidth[1];

                    flashHeight = strImg.match(/height="(.*?)"/);
                    if (flashHeight != null)
                        flashHeight = flashHeight[1];

                    textReplace = objFlash.replace("##classname##", flashClassName);
                    textReplace = textReplace.replace("##data##", flashSrc);
                    textReplace = textReplace.replace("##alt##", flashAlt);
                    textReplace = textReplace.replace("##width##", flashWidth);
                    textReplace = textReplace.replace("##height##", flashHeight);

                    var textParam = "<param name=\"movie\" value=\"##data##\" />";
                    textParam = textParam.replace("##data##", flashSrc);
                    textReplace = textReplace + textParam + "</object>";

                    newSource = newSource.replace(strImg, textReplace);
                }
            }
        }
        return newSource;
    }

    function restoreObjFlash(source) {
        var oRootDist = document.getElementById("RootDist").value;
        var newSource = source.replace("</object>", "", "g");
        var objImgFlash = "<img class=\"##classname##\" src=\"" + oRootDist + "css/default/editor/flash.png\" title=\"##src##\" alt=\"##alternate##\" width=\"##width##\" height=\"##height##\"/>";

        aFlash = source.match('<object[^>]*>', 'g');

        if (aFlash != null) {
            for (i = 0; i < aFlash.length; i++) {
                strFlash = aFlash[i];
                if (strFlash.match('objFlash') != null) {
                    flashTitle = strFlash.match(/data="(.*?)"/);
                    if (flashTitle != null)
                        flashTitle = flashTitle[1];
                    flashAlt = strFlash.match(/alt="(.*?)"/);

                    if (flashAlt != null)
                        flashAlt = flashAlt[1];
                    flashClassName = strFlash.match(/class="(.*?)"/);
                    if (flashClassName != null)
                        flashClassName = flashClassName[1];

                    flashWidth = strFlash.match(/width="(.*?)"/);
                    if (flashWidth != null)
                        flashWidth = flashWidth[1];

                    flashHeight = strFlash.match(/height="(.*?)"/);
                    if (flashHeight != null)
                        flashHeight = flashHeight[1];

                    textReplace = objImgFlash.replace("##classname##", flashClassName);
                    textReplace = textReplace.replace("##src##", flashTitle);
                    textReplace = textReplace.replace("##alternate##", flashAlt);
                    textReplace = textReplace.replace("##width##", flashWidth);
                    textReplace = textReplace.replace("##height##", flashHeight);

                    newSource = newSource.replace(strFlash, textReplace);
                }
            }

            var flashParam = newSource.match('<param[^>]*>', 'g');
            if (flashParam != null && flashParam.length > 0) {
                for (j = 0; j < flashParam.length; j++) {
                    newSource = newSource.replace(flashParam[j], "");
                }
            }
        }
        return newSource;
    }

    function updatePreview() {
        var objFlash = "<object data=\"##data##\" width=\"##width##\" height=\"##height##\"";
        objFlash += " type=\"application/x-shockwave-flash\" >";

        src = document.getElementById('Selezionato');
        if (src.value == '')
            return;
        width = document.getElementById('flash_width');
        height = document.getElementById('flash_height');

        objFlash = objFlash.replace('##data##', src.value);
        objFlash = objFlash.replace('##width##', width.value);
        objFlash = objFlash.replace('##height##', height.value);

        var oPreview = document.getElementById('preview');

        var textParam = "<param name=\"movie\" value=\"##data##\" />";
        textParam = textParam.replace("##data##", src.value);
        objFlash = objFlash + textParam + "</object>";

        // alert(objFlash);

        oPreview.innerHTML = objFlash;
    }

    function loadflash() {
        ftb = window.opener.FTB_API[document.getElementById('TargetFreeTextBox').value];
        flash = ftb.GetNearest('img');
        //alert(flash);
        //if (flash!=null && flash.className != null && flash.className == 'objFlash' || flash.className.match("objFlash") != null)                
        if (flash != null) {
            if (flash.className != null && flash.className == 'objFlash' || flash.className.match("objFlash") != null) {
                newhref = flash.title
                newserver = ExtractServerName(newhref)
                if (ExtractServerName(document.URL) == newserver) {
                    newhref = newhref.replace("http://", "");
                    newhref = newhref.replace(newserver, "");
                }

                oSelected = document.getElementById("Selezionato");
                oSelected.value = newhref;
                oAlt = document.getElementById("Alternate");
                oAlt.value = flash.alt;
                oAlign = document.getElementById("Align");
                width = document.getElementById('flash_width');
                width.value = flash.width;
                height = document.getElementById('flash_height');
                height.value = flash.height;

                if (flash.className != null) {
                    var imgclass = flash.className;
                    imgclass = imgclass.replace(' objFlash', '');

                    if (imgclass == 'Nessuno') {
                        oAlign.options[0].selected = true;
                    }
                    if (imgclass == 'Sinistra') {
                        oAlign.options[1].selected = true;
                    }                   
                    if (imgclass == 'Destra') {
                        oAlign.options[2].selected = true;
                    }
                }
                updatePreview();
            }
        }
    }

    function updateFlash() {
        oRootDist = document.getElementById("RootDist").value;
        oFlashImg = document.getElementById("FlashImg").value;
        oSelected = document.getElementById("Selezionato");
        oAlt = document.getElementById("Alternate");
        oAlign = document.getElementById("Align");
        width = document.getElementById('flash_width').value;
        height = document.getElementById('flash_height').value;

        if (oAlt != null && oSelected != null) {
            if (oAlt.value.length > 0 && oSelected.value.length > 0) {
                ftb = document.getElementById('TargetFreeTextBox').value;
                if (oAlign.value == "Nessuno") 
                {
                    img = "<img width=\"" + width + "\" height=\"" + height + "\" src=\"" + oFlashImg + "\" alt=\"" + oAlt.value + "\" class=\"objFlash\" title=\"" + oSelected.value + "\" />";
                }
                else 
                {
                    img = "<img width=\"" + width + "\" height=\"" + height + "\" src=\"" + oFlashImg + "\" class=\"" + oAlign.value + " objFlash\" alt=\"" + oAlt.value + "\" title=\"" + oSelected.value + "\"  />";
                }

                window.opener.FTB_API[ftb].InsertHtml(img);
                window.close();
            }
            else
                alert("ERRORE! File non selezionato o testo alternativo non inserito!")
        }
    }

    //Logged User Settings
 $(document).ready(function () {

    // utente loggato
    var userCtrl = $('#userCtrl');
    var userNavBox = $('#userNav');

    userCtrl.mouseup(function (logged) {
        userNavBox.toggle();
        userCtrl.toggleClass('active');
    });
    userNavBox.mouseup(function () {
        return false;
    });

    $(this).mouseup(function (logged) {
        if (!($(logged.target).parent('#userCtrl').length > 0)) {
            userCtrl.removeClass('active');
            userNavBox.hide();
        }
    });


    $('[data-toggle="popover"]').popover({
        // trigger: 'focus'
    });

     $(".toggle-password").click(function () {

         $(this).toggleClass("fa-eye fa-eye-slash");
         var input = $($(this).attr("toggle"));
         if (input.attr("type") === "password") {
             input.attr("type", "text");
         } else {
             input.attr("type", "password");
         }
     });

});


