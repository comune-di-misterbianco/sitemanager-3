﻿//Funzioni JQuery eseguite al boot delle pagine web.
$(document).ready(function() {

    //$(".overlayOnDemand").overlay();
    /*
    $(".overlayOnDemand").overlay({

        finish: { top: 'center' },
        expose: '#586C8F'

    });
    */

    $(".open_alert_button").click(function(){
        $("#alert").overlay().load();
    });

   
    $("a[rel]").click(function() {
        var wrapID = $(this).attr("rel");
        $("#" + wrapID).overlay().load();
    });

    //Hide (Collapse) the toggle containers on load
    $(".collapsed-block").hide();

    //Switch the "Open" and "Close" state per click
    $(".toggle-button").click(function() {
        $(this).next().slideToggle("slow");
        var id = $(this).parent(".SideBlock").attr("id");
        if ($(this).hasClass("collapsed")) {
            $(this).removeClass("collapsed");
            $.cookie(id + 'Box_StartClosed', false);
        }
        else {
            $(this).addClass("collapsed");
            $.cookie(id + 'Box_StartClosed', true);
        }
    });

});