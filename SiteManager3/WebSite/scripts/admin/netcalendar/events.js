﻿function start(EventoFormId, EnableDeleteEvent) {
   
   $('.ui-effects-wrapper').hide();
   $('#dialogEvento').dialog({
        autoOpen: false,
        closeOnEscape: false,
        resizable: false,
        draggable: false,
        modal: true,
        show: {
            effect: 'blind',
            duration: 500
        },
        hide: {
            effect: 'blind',
            duration: 500
        },
        title: 'Nuovo Evento',
        buttons: {

            Conferma: function () {
                $('#submitEvento').click();
            },
            Annulla: function () {
                $(this).dialog('close');
                if ($('#' + EventoFormId).find('.not-valid').length > 0) {
                    $('#' + EventoFormId).find('.not-valid>.validation-results').hide();
                }
            },
            Elimina: function () {
                $('#deleteEvento').click();
            }
        }
   });

   if ($('#' + EventoFormId).find('.not-valid').length > 0) {
       $('#dialogEvento').dialog('open');
   }

   if (EnableDeleteEvent != "True") {
       $(".ui-dialog-buttonpane button:contains('Elimina')").attr("disabled", true).addClass("ui-state-disabled");
   }

    $('#add_button').click(function () {
        $('#Title').val("");
        $('#Start').val("");
        $('#End').val("");
        $('#BackgroundColor').val("");
        $('#EventID').val(""); 

        $('#dialogEvento').dialog('open');
        $('#dialogEvento').parent().appendTo($('form:first'));
        $('.ui-effects-wrapper').hide();
        return false;
    });
 }