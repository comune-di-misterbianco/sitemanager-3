﻿var ID;
var Title;
var AllDay;
var Start;
var End;
var Url;
var ClassName;
var Editable;
var StartEditable;
var DurationEditable;
var Color;
var BackgroundColor;
var BorderColor;
var TextColor;

function renderCalendar(eventi, enableEventClick, id, title, allDay, start, end, url, classname, editable, startEditable, durationEditable,
                color, backgroundColor, borderColor, textColor) {
        var currentLangCode = 'it';
        var date = new Date();
        var now = date.toISOString();
        //var EnableEventClick = Boolean(enableEventClick);
        var EnableEventClick = enableEventClick == "True" ? true : false;

        ID = id;
        Title = title;
        AllDay = allDay;
        Start = start;
        End = end;
        Url = url;
        ClassName = classname;
        Editable = editable;
        StartEditable = startEditable;
        DurationEditable = durationEditable;
        Color = color;
        BackgroundColor = backgroundColor;
        BorderColor = borderColor;
        TextColor = textColor;

       $("#container-calendar").resizable();
       $('#calendar').fullCalendar({
           header: {
               left: 'prev,next today',
               center: 'title',
               right: 'month,agendaWeek,agendaDay'
           },
           defaultDate: now,
           lang: currentLangCode,
           buttonIcons: false, // show the prev/next text
           weekNumbers: false,
           editable: false,
           eventLimit: false, // allow "more" link when too many events
           events: eventi,
           eventClick: function (calEvent, jsEvent, view) {
               if (EnableEventClick)
               { eventClick(calEvent, jsEvent, view); }
           }
       });
   }

   function eventClick(calEvent, jsEvent, view) {
	   
       $('#' + ID).val(calEvent.id);
       $('#' + Title).val(calEvent.title);
       $('#' + AllDay).val(calEvent.allDay);
       //$('#' + Start).val(isoDate(calEvent.start));
	   $('#' + Start).val(formatDate(calEvent.start));
       //$('#' + End).val(isoDate(calEvent.end));
	   $('#' + End).val(formatDate(calEvent.end));
       $('#' + ClassName).val(calEvent.classname);
       $('#' + Editable).val(calEvent.editable);
       $('#' + StartEditable).val(calEvent.startEditable);
       $('#' + DurationEditable).val(calEvent.durationEditable);
       $('#' + Color).val(calEvent.color);
       $('#' + BackgroundColor).val(calEvent.backgroundColor);
       $('#' + BorderColor).val(calEvent.borderColor);
       $('#' + TextColor).val(calEvent.textColor);
       $('#EventID').val(calEvent.id);

       $('#dialogEvento').dialog('open');
       $('#dialogEvento').parent().appendTo($('form:first'));
       $('#div_erroreEvento').hide();
       $('.ui-effects-wrapper').hide();
   }


   function GetEventsToJSON() {
       var events = $('#calendar').fullCalendar('clientEvents');
       for (index = 0; index < events.length; ++index) {
           var item = events[index];
           item.source = undefined;
       }
       var myJson = JSON.stringify(events);
       return myJson;
   }

   function isoDate(msSinceEpoch) {
       var d = new Date(msSinceEpoch);
       return d.getUTCDate() + '/' + (d.getUTCMonth() + 1) + '/' + d.getUTCFullYear() + " "
                + d.getUTCHours() + ':' + d.getUTCMinutes();
   }
   function formatDate(msSinceEpoch){
        var d = new Date(msSinceEpoch);
		if(d.getMinutes() == '0')
		{
			return d.getDate() + '/' + (d.getMonth() +1) + '/' + d.getFullYear() + " " 
				+ d.getHours() + ':' + d.getMinutes() + '0';
        } 
		else		
			return d.getDate() + '/' + (d.getMonth() +1) + '/' + d.getFullYear() + " " 
				+ d.getHours() + ':' + d.getMinutes() ;
   }