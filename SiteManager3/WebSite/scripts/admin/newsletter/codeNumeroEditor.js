﻿$(document).ready(function () {


    $('#myIframe').load(function () {
        var $iframe = $('#myIframe').contents();

        var dialogOpts = {
            autoOpen: false,
            width: 640,
            height: 480
        };

        $("#myDialog").dialog(dialogOpts);

        $("#anteprima").click(function () {

            var isVisible = $("#content_configuration").css('display') !== 'none';
            if (isVisible) {
                var contenuto = $iframe.find(".templateContent").html();
                $("#myDialog").html(contenuto);
                $("#myDialog .content_buttons").remove();
                $("#myDialog .eliminabile").remove();
                $("#myDialog .place").remove();
                $("#myDialog > ul").css({
                    'border': '0px '
                });
                $("#myDialog > ul > li").css({
                    'border': '0px '
                });
                if (!$("#myDialog").dialog("isOpen")) {
                    $("#myDialog").dialog("open");
                } else {
                    $("#myDialog").dialog("close");
                }
            } else {
                alert("Prima di visualizzare l'anteprima termina la modifica dell'elemento");
            }
        });

    });

    

});