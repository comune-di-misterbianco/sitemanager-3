function start(path_imgs, path, numeroID,canaleID) {

    var dropParameters = [];
    // Id di partenza, sar� sequenziale per tutti i widget. Non parte da 0 perch� in caso di caricamento da versioni precedenti non si dar� un ID basso a quelli presenti non DB.
	var contentId=1000; 
	console.log("path imgs: " + path_imgs);
	console.log("path WEBApi: " + path);
	console.log("numeroID: " + numeroID);
	console.log("canaleID: " + canaleID);
	
    //Contenuto incluso in ogni widget, permette la gestione degli stessi
	var content_buttons= "<div align=\"left\" class=\"content_buttons\">"+
								"<a title=\"Drag this content\" class=\"ui-icon ui-icon-arrow-4-diag\">Drag</a>"+
								"<a href=\"link/to/pencil\" title=\"Modify this content\" class=\"eventi_content ui-icon ui-icon-pencil\">Modify</a>"+
								"<a href=\"link/to/plus\" title=\"Copy this content\" class=\"eventi_content ui-icon ui-icon-plus\">Copy</a>"+
								"<a href=\"link/to/trash\" title=\"Delete this content\" class=\"eventi_content ui-icon ui-icon-trash\">Delete</a>"+
						"</div>";
	var modify = false;
	var content_toModify_html ="";
	var content_toModify_class ="";
	var content_toModify_id="";
	var maxWidth = 640; // Max width per l'immagine
	var maxHeight = 480;    // Max height  per l'immagine
	
	/*
	 * Contenuti da nascondere all'avvio e da mostrare in base ai tipi di rochieste 
	 */
	$("#content_configuration").hide();
	$("#content_configuration_text").hide();
	$("#content_configuration_image").hide();
	$("#content_configuration_caption").hide();
	$("#content_configuration_button").hide();
	$(".button_email").hide();
	$(".eliminabile").hide();
	/*
	 * Fine Contenuti da nascondere
	 */

  
		$("#myTabs").tabs();
		
		var sortOpts = {
			revert: true,
			handle: ".ui-icon-arrow-4-diag",
			placeholder: "sortable_placeholder",
			connectWith: ".sortable_panel tbody",
			dropOnEmpty: true,
			start:activateCallBack,
			stop: deactivateCallBack,
			receive: receiveCallBack
		};

        $(".sortable_panel>tbody").bind('click mousedown', function (event) {
            fillEmptyList();
            //console.log(".bind \'click mousedown\'");
            event.preventDefault();
            event.stopPropagation();
        });


        $(".sortable_panel tbody").sortable(sortOpts).disableSelection();		
		var dragOpts = {
			connectToSortable: ".sortable_panel>tbody",
			helper: function () 
             { return $
                (this).clone().appendTo('body').css('zIndex', 5).show(); 
             },
			start: activateCallBack,
			stop:deactivateCallBack
		};
		
		$(".drag").draggable(dragOpts);

		function activateCallBack() {
		    //console.log("activateCallBack");
			$(".eliminabile").show();
			$(".sortable_panel").addClass("activated");
            fillEmptyList();
           		
		}		
        
        //Se un elemento sortable diventa vuoto verr� riempito con un contenuto di default
        function fillEmptyList(){
            var empty=0;
            $("tbody.sortable_panel").each(function () {
                var childs = $(this).children().length;
                var id = $(this).parent().attr("id");
                //console.log("id: " + id + ". length: " + childs);

                if (childs == 0) {
                    empty++;
                    $(this).append("<tr class=\"content_panel place activated\"> <td style=\"min-heigth:200px;\">" +
									"<div heigth=200px> Trascina un nuovo contenuto qui" +
                    			    "</div> </td></tr>");
                } else { 
                    $(this).find(".activated tr").remove();
                }
            });
        }


        function deactivateCallBack() {
            $("tr.place").remove();
            fillEmptyList();
			$(".eliminabile").hide();
			$(".sortable_panel").removeClass("activated");
		}
		
		function receiveCallBack(e,ui) {			
			/*
			 * parametri che definiscono:
			 * L'oggetto draggato(che si vuole creare)
			 * L'oggetto droppato(dove si vuole creare)
			 */
		    fillEmptyList();
			var draggableId = ui.sender.attr("id");
			var $droppableId = $(this).attr("id");
			
			//console.log("draggable: "+draggableId+", droppable: "+$droppableId);
			
			dropParameters[0]=draggableId; //contiene l'oggetto che si vuole creare
			dropParameters[1]=$droppableId; //contiene la posizione. Non usato nuovo con id=tmp
			
            //console.log("draggableId: "+draggableId);
			//if(draggableId.indexOf("_panel") ==-1)
            if (typeof draggableId !== "undefined")
			{
				$(".sortable_panel>tbody>td").replaceWith("<tr id=\"tmp\"></tr>");
				discriminateContent(draggableId);
			}
			
		}

		/*
            Prende in ingresso il tipo di elemento che si vuole creare e lo crea nella posizione in cui l'utente ha effettuato il drop
		*/
		function discriminateContent(content) {
		    //console.log("content: " + content);
		    $('tbody').off('mouseover', 'tr', tbody_mouseover);
		    $('tbody').off('dblclick', 'tr', tbody_dblclick);
		    testo_default="<h1>inserisci il titolo qui</h1><p>inserisci il testo qui</p>";
		    editor.setData(testo_default);			
		    var html_content = $("#tmp").html();
		  			
		    if(content=="inputText")
		    {				
		        $("#tmp").attr("class","content_panel input_text temporanea");
		        $("#tmp").attr("id","tmp");				
		        $("#tmp").html("<td colspan=\"2\" class=\"text_from_editor\">"+
		            testo_default+	
		        "</td>");						
		        $("#myTabs").toggle();
		        $("#content_configuration").toggle();
		        console.log("creato un input text");
		        $("#content_configuration_text").show();
		    }
		    else if(content=="inputImage")
		    {
		        $("#tmp").attr("class","content_panel input_image temporanea");
		        $("#tmp").attr("id","tmp");
		        $("#tmp").html("<td align=\"center\" colspan=\"2\" class=\"img_caption\"><img id=\"blah\" src=\"" + path_imgs + "imgs/image.png\" alt=\"your image\"/></td>");
		        $("#myTabs").toggle();
		        $("#content_configuration").toggle();
		            console.log("creato un input image");
		        $("#content_configuration_image").show();				
		        }
			
		    else if(content=="inputCaption")
		    {
		        $("#tmp").attr("class","content_panel input_caption temporanea");
		        $("#tmp").attr("id","tmp");				
		        $("#tmp").html("<td class=\"text_from_editor\">"+
		            testo_default+
		            "</td>"+
		        "<td align=\"center\" class=\"img_caption\"><img id=\"blah\" src=\""+path_imgs+"imgs/image.png\" alt=\"your image\"/></td>"	
		        );					
		        $("#myTabs").toggle();
		        $("#content_configuration").toggle();
		        console.log("creato un input image");
		        $("#content_configuration_caption").show();				
		    }
		    else if(content=="inputButton")
		    {
		        $("#tmp").attr("class","content_panel input_button temporanea");
		        $("#tmp").attr("id","tmp");				
		        $("#button_text").val("Testo del Bottone");
		        $("#button_indirizzo_web").val("http://");			
		        $("#tmp").html("<td colspan=\"2\" class=\"button_from_editor\">"+
		            "<a class=\"button\" id=\"content_input_button\""+
		                "title=\"Testo del Bottone\""+
		                "href=\"indirizzo_web\""+
		                "target=\"_blank\""+								
		                ">Testo del Bottone</a>"+
		        "</td>");
		        $("#myTabs").toggle();
		        $("#content_configuration").toggle();
		        console.log("creato un input button");
		        $("#content_configuration_button").show();
		    }
		    else
		    {
		        console.log("contenuto non impostato");
		        $('tbody').on('mouseover', 'tr', tbody_mouseover);
		        $('tbody').on('dblclick', 'tr', tbody_dblclick);				
		    }
		    html_content = $("#tmp").html();	
		}
			
		function readURL(input) {
		    if (input.files && input.files[0]) {
		    var reader = new FileReader();
		    reader.onload = function (e) {
		        $img = $("#tmp>td.img_caption>#blah");
		        $img.attr('src', e.target.result);
		        $img.attr('original_width', $img.width());
		        $img.attr('original_height', $img.height());
		        $img.attr('percent', '100');
		        $("#imgSlider").val(100);
		        $("#imgSlider").slider('option', 'value', '100');
		        $("#tip").remove();
		        $("#imgSlider").slider('refresh');
		        resizeMaxDimension($img, $img.width(), $img.height());

		    };		            
		    reader.readAsDataURL(input.files[0]);
		    }
		}
		    
		$("#imgInp").delegate("","change", function() {
		    readURL(this);
		});

		
            
		function readURL_caption(input) {
		    if (input.files && input.files[0]) {
		    var reader = new FileReader();
		            
		    reader.onload = function (e) {				                
		        $("#tmp>td.img_caption>#blah").attr('src', e.target.result);
		    };
		            
		    reader.readAsDataURL(input.files[0]);
		    }
		}

		var sliderOpts = {
		    min: 10,
		    max: 200,
		    value: 100,
		    start: function () {
		        $("#tip").fadeOut(function () {
		            $(this).remove();
		        });
		    },
		    change: sliderChange
		};

		function sliderChange(e, ui)
		{
		    $("<div />", {
		        "class": "ui-widget-header ui-corner-all",
		        id: "tip",
		        text: ui.value + "%",
		        css: {
		            left: e.pageX - 35
		        }
		    }).appendTo("#imgSlider");
		    modifyDimension(ui.value);
        }
		$("#imgSlider").slider(sliderOpts);


		function resizeMaxDimension($img, width, height) {

		    var ratio = 0;  // rapporto
		    
            if (width > maxWidth) {
		        ratio = maxWidth / width;   // get ratio for scaling image
		        $img.css("width", maxWidth); // Set new width
		        $img.css("height", height * ratio);  // Scale height based on ratio
		        height = height * ratio;    // Reset height to match scaled image
		        width = width * ratio;    // Reset width to match scaled image
		    }

		    if (height > maxHeight) {
		        ratio = maxHeight / height; // get ratio for scaling image
		        $img.css("height", maxHeight);   // Set new height
		        $img.css("width", width * ratio);    // Scale width based on ratio
		        width = width * ratio;    // Reset width to match scaled image
		    }               
		}

		function modifyDimension(percent) {
		    $img = $("#tmp>td.img_caption>#blah");
		    var width = $img.attr('original_width');
		    var height = $img.attr('original_height');

		    $img.attr('percent', percent);

		    var newwidth = width * percent / 100;
		    var newheight = height * percent / 100;
		    if (newwidth < maxWidth && newheight < maxHeight) {
		        $img.css("width", newwidth);
		        $img.css("height", newheight);
		    }
		    else {
		        resizeMaxDimension($img, newwidth, newheight)
		    }
                    
        }
            
		$("#imgInp_caption").delegate("","change", function() {
		    readURL_caption(this);
		});
					
		var dialogOpts = {
		    autoOpen: false,
		    width: 640,
		    height: 480	
		};
		
		$("#myDialog").dialog(dialogOpts);

		$("#anteprima").click(function () {

		    var isVisible = $("#content_configuration").is(':hidden');
		    if (isVisible) {
		        var contenuto = $("#left_panel").html();
		        $("#myDialog").html(contenuto);
		        $("#myDialog .content_buttons").remove();
		        $("#myDialog .eliminabile").remove();
		        $("#myDialog .place").remove();
		        $("#myDialog > ul").css({
		            'border': '0px '
		        });
		        $("#myDialog > ul > li").css({
		            'border': '0px '
		        });
		        if (!$("#myDialog").dialog("isOpen")) {
		            $("#myDialog").dialog("open");
		        } else {
		            $("#myDialog").dialog("close");
		        }
		    } else {
		        alert("Prima di visualizzare l'anteprima termina la modifica dell'elemento");
		    }
		});

		//Preparo l'oggetto che tramite JSON verr� spedito al WebServer.
		// Vengono normalizzati gli ID in maniera tale che siano sequenziali.
		//tolti gli elementi vuoti(.place) e creo un array.
		$('#salva_html').on('click', {exit:false}, salva_HTML);

		function salva_HTML(event) {

		    var exit = true;
		    if (typeof event !== "undefined") {
		        exit = event.data.exit;
		        console.log("exit: " + exit);
		    }

		    $('#salva_html').off('click', '', salva_HTML);
		    var isVisible = $("#content_configuration").is(':hidden');
		    var toReturn = false;
		    if (isVisible) {

		        $.blockUI({
		            overlayCSS: { backgroundColor: '#00f' },
                    message: "Attendi il salvataggio" 
                }); 
		        //Normalizzo gli ID dei widget
		        var NormalizedID = 0;
		        $("tbody.sortable_panel>tr").each(function () {
		            var newNormalizedID = "content_" + NormalizedID;
		            if ($(this).attr("class") != "place") {
		                $(this).attr("id", newNormalizedID);
		                $(this).find(".content_buttons").attr("content_parent_id", newNormalizedID);
		                NormalizedID++;
		            }
		        });

		        var elements = [];
		        var sequence_number = 0;
		        $("tbody.sortable_panel").each(function () {
		            var sortable_panel_id = $(this).parent().attr("id");
		            var child = $(this).children().length;
		            if (child > 0) {
		                if ($(this).children("tr.place").length == 0) {
		                    var sortable_panel_html = $(this).html();
		                    elements[sequence_number] = [sortable_panel_id, sortable_panel_html];
		                    sequence_number++;
		                }
		            }
		        });
		        var elementJSON = [];
		        for (key in elements) {
		            var htmlcodeEscape = escape(elements[key][1]);
		            var obj = {
		                Sequenza: key,
		                html: htmlcodeEscape,
		                RegionId: elements[key][0]
		            };
		            //console.log("key :" + key + ".elements[key][0] :" + elements[key][0] + "\n\n");
		            elementJSON.push(obj);
		        }
		        sendData(elementJSON,exit);
		        toReturn = true;
		    } else {
		        toReturn= false;
		        alert("Prima di salvare termina la modifica dell'elemento");
		    }
		    $('#salva_html').on('click', {exit:false}, salva_HTML);
		    return toReturn;
		}		
		
		$("#salva").click(function() {			
		    $("#myTabs").toggle();
		    $("#content_configuration").toggle();
		    discriminateSalva();
		    $('tbody').on('mouseover', 'tr', tbody_mouseover);
		    $('tbody').on('dblclick', 'tr', tbody_dblclick);
		});

		$("#annulla").click(function() {
			
		    if(modify){			
		        modify=false;
		        console.log("annulla su modifica");								
		        $("#tmp").attr('class',content_toModify_class);
		        $("#tmp").html(content_toModify_html);
		        $("#tmp>td").removeClass("temporanea");
		        $("#tmp>td>.content_buttons").removeClass("content_buttons_inline");				
		        $("#tmp").attr('id',content_toModify_id);							
		        content_toModify="";					
		    }
		    else
		    {
		        $("#tmp").remove();
		    }
		    $("#myTabs").toggle();
		    $("#content_configuration").toggle();
		    $("#content_configuration_image").hide();
		    $("#content_configuration_text").hide();
		    $("#content_configuration_caption").hide();
		    $("#content_configuration_buttons").hide();
		    $('tbody').on('mouseover', 'tr', tbody_mouseover);
		    $('tbody').on('dblclick', 'tr', tbody_dblclick);
		    var img_width = 0;
		    var img_height = 0;
		});
		
		/*
		* Discrimina i vari tipi di content per il salvataggio
		*/
		function discriminateSalva(){
			if(dropParameters[0]=="inputText")
			{
				console.log("salva un input text");				
				$("#tmp").attr("class","content_panel input_text");				
				$("#tmp").html("<td colspan=\"2\" class=\"text_from_editor\">"+
					editor.getData()+
				content_buttons+
				"</td>");				
				$("#tmp>td>.content_buttons").attr("content_parent_id","content_"+contentId);				
				$("#tmp").removeClass("temporanea");
				$("#tmp").attr("id","content_"+contentId);				
				$('#content_configuration_textarea').val(" ");
				$("#content_configuration_text").hide();
				dropParameters=[];
				contentId++;					 
			}
			else if(dropParameters[0]=="inputImage")
			{
				console.log("salva un input image");
				$("#tmp").attr("class","content_panel input_image");				
				$("#tmp>td").append(content_buttons);								
				$("#tmp>td>.content_buttons").attr("content_parent_id","content_"+contentId);				
				$("#tmp").removeClass("temporanea");
				$("#tmp").attr("id","content_"+contentId);				
				$("#content_configuration_image").hide();
				dropParameters=[];
				contentId++;
				var img_width = 0;
				var img_height = 0;			
				
			}
			else if(dropParameters[0]=="inputCaption")
			{
				console.log("salva un input caption");
				$("#tmp").attr("class","content_panel input_caption");
				$("#tmp>td.text_from_editor").html(editor_caption.getData());				
				$("#tmp>td.text_from_editor").append(content_buttons);
				$("#tmp>td>.content_buttons").attr("content_parent_id","content_"+contentId);	
				$("#tmp").removeClass("temporanea");
				$("#tmp").attr("id","content_"+contentId);			
				$("#content_configuration_caption").hide();
				dropParameters=[];
				contentId++;			
				
			}
			else if(dropParameters[0]=="inputButton")
			{
				console.log("salva un input button");
				$("#tmp").attr("class","content_panel input_button");				
				
				var testo=$("#button_text").val();
				var tmp= $("#button_linkTo").val();
					if(tmp=="Email"){
						$("#tmp").addClass("email");
						var indirizzo_email= $("#indirizzo_email").val();
						var oggetto_email= $("#oggetto_email").val();
						var messaggio_email= $("#messaggio_email").val();
												
						console.log("indirizzo_email: "+indirizzo_email+
							". oggetto_email: "+oggetto_email+
							". messaggio_email: "+messaggio_email
						);
						
						$("#tmp>td").html(
								"<a class=\"button\" id=\"content_input_button\""+
									"title=\""+testo+"\""+
									"indirizzo_email=\""+indirizzo_email+"\""+
									"oggetto_email=\""+oggetto_email+"\""+
									"messaggio_email=\""+messaggio_email+"\""+
									"href=\"mailto:"+indirizzo_email+
									"?subject="+oggetto_email+
									"&body="+messaggio_email+
									"\""+
									"target=\"_blank\""+								
									">"+testo+"</a>"+
							content_buttons);
					}
					else if(tmp=="Indirizzo Web")
					{
						$("#tmp").addClass("indirizzo_web");						
						var indirizzo_web= $("#button_indirizzo_web").val();
						console.log("indirizzo_web: "+indirizzo_web);
						$("#tmp>td").html(
							"<a class=\"button\" id=\"content_input_button\""+
							"title=\""+testo+"\""+
							"href=\""+indirizzo_web+
							"\""+
							"target=\"_blank\""+								
							">"+testo+"</a>"+
						content_buttons);
							
					}
				$("#tmp>td>.content_buttons").attr("content_parent_id","content_"+contentId);
				$("#tmp").removeClass("temporanea");
				$("#tmp").attr("id","content_"+contentId);
				$("#content_configuration_button").hide();
				dropParameters=[];
				contentId++;							
			}
			else 
			{
				console.log("Salva per il contenuto non impostato");
			}
		}
		/*
		 * End discrominaSalva
		 */
		
		/*
		 * Gestione dei bottoni: drag, copia, modifica, cacella
		 * 
		 */
		$('.sortable_panel').on('click','.content_buttons',content_buttons_click);
		
		function content_buttons_click (event) {
			var $this = $(this);
			var $parent = $this.parent();
			var $target = $( event.target );
			var content_parent_id="#"+$this.attr("content_parent_id");
			
			if( $target.is( "a.button" )){
			}
			else if ( $target.is( "a.ui-icon-trash" ) ) {
				
				//console.log("TRASH: "+"content_parent_id: "+content_parent_id);
                var count=0;
                var empty=1;
                 
                //controllo se � presente un solo elemento        
                var this_childs=  $(content_parent_id).parent().children().length;
                $("tbody.sortable_panel").each(function(){             
                    count++;
                    var childs= $(this).has( ".content_panel" ).length;
                    var id=$(this).parent().attr("id");
                    //console.log("id: "+id+". length: "+childs);
                    if(childs==0){
                        empty++;
                    }                     
                });
                  //console.log("count: "+count+". empty: "+empty+". this_childs: "+this_childs);
                if(count == empty && this_childs==1){
                    alert("Devi inserire almeno un contenuto");
                }else{
                    $(content_parent_id).remove();	
                }
                fillEmptyList();				
			}
			else if( $target.is( "a.ui-icon-plus" ) ) {
			    console.log("PLUS: " + "content_parent_id: " + content_parent_id);
			    $(content_parent_id + ">td>.content_buttons").removeClass("content_buttons_inline");								
				var clone=$(content_parent_id).clone();
				contentId++;
				var content = "content_"+contentId;
				clone.attr('id',content);									
				$(content_parent_id).after(clone);
				var content_buttons="#"+content+">td>.content_buttons";
				$(content_buttons).attr("content_parent_id",content);
				$(content_buttons).removeClass("content_buttons_inline");					
			}
			else if( $target.is( "a.ui-icon-pencil" ) ) {	
                modify_content(content_parent_id);			    
			}			
			event.preventDefault();
			event.stopPropagation();			
		}
		/*
		 * Fine Gestione dei bottoni.
		 * 
		 */
		
		function modify_content(content_parent_id){

		    //console.log("PLUS: " + "content_parent_id: " + content_parent_id);
		    $('tbody').off('mouseover', 'tr', tbody_mouseover);
		    $('tbody').off('dblclick', 'tr', tbody_dblclick);
			modify=true;
			
			var this_id=content_parent_id;				
			var this_class=($(content_parent_id).attr('class')).split(" ");
					
			content_toModify_html=$(this_id).html();
			content_toModify_class=$(this_id).attr("class");
			content_toModify_id=content_parent_id.replace("#","");
			
//			console.log("content_toModify_html: "+content_toModify_html+
//					"\n content_toModify_class: "+content_toModify_class+
//					"\n content_toModify_id: "+content_toModify_id);			
			var content_type=jQuery.inArray("input_text",this_class);
			
//			console.log("<br>CLICK. $this: "+this_id+". classe:"+this_class);
			
			if(jQuery.inArray("input_text",this_class) >0)
			{
					
				$("#myTabs").toggle();
				$("#content_configuration").toggle();
				console.log("modifica un input text");				
				$(this_id+">.text_from_editor>.content_buttons").remove();
				html_text=$(this_id+">.text_from_editor").html();				
				console.log("html_text: "+html_text);			
				editor.setData(html_text);
				dropParameters[0]="inputText";					
				$(this_id).attr("class","content_panel input_text temporanea");
				$(this_id).attr("id","tmp");				
				$("#content_configuration_text").show();				
			}			
			else if(jQuery.inArray("input_image",this_class) >0)
			{
				$("#myTabs").toggle();
				$("#content_configuration").toggle();
				console.log("modifica un input image");				
				dropParameters[0]="inputImage";				
				$(this_id).attr("class","content_panel input_image temporanea");
				$(this_id+">.img_caption>.content_buttons").remove();
				$(this_id).attr("id", "tmp");

				$img = $("#tmp>td.img_caption>#blah");
				var percent = $img.attr('percent');

				$("#imgSlider").val(percent);
				$("#imgSlider").slider('option', 'value', percent);
				$("#tip").remove();
				$("#imgSlider").slider('refresh');
				
                					
				$("#content_configuration_image").show();
				
			}			
			else if(jQuery.inArray("input_caption",this_class) >0)
			{
					
				$("#myTabs").toggle();
				$("#content_configuration").toggle();
				console.log("modifica un input caption");
				dropParameters[0]="inputCaption";				
				$(this_id+">.text_from_editor>.content_buttons").remove();
				html_text=$(this_id+">.text_from_editor").html();			
				console.log("html_text: "+html_text);
				editor_caption.setData(html_text);				
				var img_src=$(this_id+">.img_caption>#blah").attr("src");
				$("#imgInp_caption").attr("src",img_src);								
				$(this_id).attr("class","content_panel input_caption temporanea");
				$(this_id).attr("id","tmp");			
				$("#content_configuration_caption").show();				
			}			
			else if(jQuery.inArray("input_button",this_class) >0)
			{
					
				$("#myTabs").toggle();
				$("#content_configuration").toggle();
				console.log("modifica un input button");
					
				if(jQuery.inArray("email",this_class) >0)
				{
					console.log("modifica email");						
					var button_text=$(this_id+">.button_from_editor>#content_input_button").attr("title");
					var indirizzo_email=$(this_id+">.button_from_editor>#content_input_button").attr("indirizzo_email");
					var oggetto_email=$(this_id+">.button_from_editor>#content_input_button").attr("oggetto_email");
					var messaggio_email=$(this_id+">.button_from_editor>#content_input_button").attr("messaggio_email");				
					$("#button_text").val(button_text);
					$("#button_linkTo").val("Email");
					$("#indirizzo_email").val(indirizzo_email);
					$("#oggetto_email").val(oggetto_email);
					$("#messaggio_email").val(messaggio_email);					
					$(".button_email").show();
					$(".button_indirizzo_web").hide();
						
				}				
				else if(jQuery.inArray("indirizzo_web",this_class) >0)
				{
					console.log("modifica indirizzo web");				
					var button_text=$(this_id+">.button_from_editor>#content_input_button").attr("title");
					var button_indirizzo_web=$(this_id+">.button_from_editor>#content_input_button").attr("href");
					$("#button_text").val(button_text);
					$("#button_linkTo").val("Indirizzo Web");
					$("#button_indirizzo_web").val(button_indirizzo_web);					
					$(".button_email").hide();
					$(".button_indirizzo_web").show();
				}							
				dropParameters[0]="inputButton";	
				$(this_id).attr("class","content_panel input_button temporanea");
				$(this_id+">td>.content_buttons").remove();
				$(this_id).attr("id","tmp");
				$("#content_configuration_button").show();				 
			} 
		}

		$("#button_linkTo").change(button_linkTo_manage);
		$('tbody').on('mouseover', 'tr', tbody_mouseover);
		$('tbody').on('mouseleave', 'tr', tbody_mouseleave);
		$('tbody').on('dblclick', 'tr', tbody_dblclick);
						
		function button_linkTo_manage(){
			var tmp= $("#button_linkTo").val();
			if(tmp=="Email"){
				$(".button_email").show();
				$(".button_indirizzo_web").hide();
			}
			else if(tmp=="Indirizzo Web")
			{
				$(".button_email").hide();
				$(".button_indirizzo_web").show();				
			}
		}
		
		function tbody_mouseover() {
			var id = $(this).attr("id");
			$( "#"+id+">td")
		        .addClass( "temporanea" );		        
		   	$( "#"+id+">td>.content_buttons" )
		        .addClass( "content_buttons_inline" );
		}
		
		function tbody_mouseleave() {
			var id = $(this).attr("id");		
			$( "#"+id+">td")
		        .removeClass( "temporanea" );		        
		   	$( "#"+id+">td>.content_buttons" )
		        .removeClass( "content_buttons_inline" );
		 }
				
		function tbody_dblclick(){
						
			var $this = $(this);
			var $parent = $this.parent();
			var $target = $( event.target );
			var content_parent_id="#"+$this.attr("id");
			//console.log("DBCLICK: "+"$this: "+$this+" $parent: "+$parent+" $target: "+$target+" content_parent_id: "+content_parent_id);
			modify_content(content_parent_id);
			event.preventDefault();
			event.stopPropagation();
		}

		$('#exit').click(function () {
		    $.blockUI({ message: $('#question'), css: { width: '275px'} });
		});

		$('#save_exit').click(function () {
		    salva_HTML();
		});

		$('#close_exit').click(function () {
		    window.location.href = "http://localhost:8639/website/cms/internet/applications/newsletter/admin/multichannel.aspx?idc=" + canaleID;
		});

		$('#annulla_exit').click(function () {
		    $.unblockUI();
		});

		function sendData(elementJSON,exit) {
		    $.ajax({
		        type: "POST",
		        url: path + "/api/NewsLetter/savenumero",
		        data: JSON.stringify({
		            RifNumero: numeroID,
		            Elements: elementJSON
		        }),
		        contentType: "application/json",
		        complete: function (e, xhr, settings) {
		            if (e.status === 200) {
		                console.log("200");
		                $.unblockUI({
		                    onUnblock: function () {
		                        var r = confirm("Salvataggio eseguito con successo");
		                        if (exit) {
		                            window.location.href = "http://localhost:8639/website/cms/internet/applications/newsletter/admin/multichannel.aspx?idc=" + canaleID;
		                        }
		                    }
		                });
		            } else {
		                console.log("ERROR! CODE: "+e.status);
		                $.unblockUI({
		                    onUnblock: function () {
		                        var r = confirm('Errore nel salvataggio');
		                    }
		                });
		            }
		        },
		        dataType: "json",
		        failure: function (e, xhr, settings) {
		            console.log("ERROR! CODE: " + e.status);
		        }
		    });
        }

        /*
        * start CKEditor
        * ****************************************************************************
        */      
		var editor,html='inserisci';
		var config ={};
	
	 	//editor = CKEDITOR.replace( 'content_configuration_textarea',config,html );
	 	editor = CKEDITOR.replace( 'content_configuration_textarea', {
		    customConfig: 'config.js' 
		});
	 	editor_caption = CKEDITOR.replace( 'content_configuration_caption_textarea', {
		    toolbar: 'Basic',
		    uiColor: '#9AB8F3'
		});
		/*
		 * end CKEditor
		 * ****************************************************************************
		 */				 	
};