﻿function start() {

    setSecondoTitoloForm();
    setTerzoTitoloForm();
    toggleCorsiRemoveButton();
    toggleServiziInfRemoveButton();
    toggleServiziSupRemoveButton();
    setDefaultValue();

    $('#CheckSecondoTitolo').change(function () {
        setSecondoTitoloForm();
    });

    $('#CheckSecondoTitolo3').change(function () {
        setTerzoTitoloForm();
    });
    
    function toggleCorsiRemoveButton() {
        var rowCount = $('#TabCorsi tr').length;
        if (rowCount == 0) {
            $("#remove_member_button").hide();
        } else {
            $("#remove_member_button").show();
        }
    }

    function toggleServiziInfRemoveButton() {
        var rowCount = $('#TabServiziInf tr').length;
        if (rowCount == 0) {
            $("#remove_servizi_inf_button").hide();
        } else {
            $("#remove_servizi_inf_button").show();
        }
    }

    function toggleServiziSupRemoveButton() {
        var rowCount = $('#TabServiziSup tr').length;
        if (rowCount == 0) {
            $("#remove_servizi_sup_button").hide();
        } else {
            $("#remove_servizi_sup_button").show();
        }
    }

    function setSecondoTitoloForm() {

        if ($('#CheckSecondoTitolo').is(':checked')) {
            ShowSecondoTitolo();
        }
        else {
            HideSecondoTitolo();
        }
    }

    function HideSecondoTitolo() {
        var TitoloDiStudioForm2 = $('#TitoloDiStudioForm2');
        if (TitoloDiStudioForm2.is(':visible')) {
            TitoloDiStudioForm2.hide();
        }

        var TipoTitoloDiStudio2 = $('#TipoTitoloDiStudio2').parent();
        if (TipoTitoloDiStudio2.is(':visible')) {
            TipoTitoloDiStudio2.hide();
        }

        var Voto2 = $('#Voto2').parent();
        if (Voto2.is(':visible')) {
            Voto2.hide();
        }

        var Max2 = $('#Max2').parent();
        if (Max2.is(':visible')) {
            Max2.hide();
        }

        var Lode2 = $('#Lode2').parent();
        if (Lode2.is(':visible')) {
            Lode2.hide();
        }           
    }

    function ShowSecondoTitolo() {
        var TitoloDiStudioForm2 = $('#TitoloDiStudioForm2');
        if (!TitoloDiStudioForm2.is(':visible')) {
            TitoloDiStudioForm2.show();
        }

        var TipoTitoloDiStudio2 = $('#TipoTitoloDiStudio2').parent();
        if (!TipoTitoloDiStudio2.is(':visible')) {
            TipoTitoloDiStudio2.show();
        }

        var Voto2 = $('#Voto2').parent();
        if (!Voto2.is(':visible')) {
            Voto2.show();
        }

        var Max2 = $('#Max2').parent();
        if (!Max2.is(':visible')) {
            Max2.show();
        }

        var Lode2 = $('#Lode2').parent();
        if (!Lode2.is(':visible')) {
            Lode2.show();
        }
    }

    function setTerzoTitoloForm() {

        if ($('#CheckSecondoTitolo3').is(':checked')) {
            ShowTerzoTitolo();
        }
        else {
            HideTerzoTitolo();
        }
    }

    function HideTerzoTitolo() {
        var TitoloDiStudioForm2 = $('#TitoloDiStudioForm3');
        if (TitoloDiStudioForm2.is(':visible')) {
            TitoloDiStudioForm2.hide();
        }

        var TipoTitoloDiStudio2 = $('#TipoTitoloDiStudio3').parent();
        if (TipoTitoloDiStudio2.is(':visible')) {
            TipoTitoloDiStudio2.hide();
        }

        var Voto2 = $('#Voto3').parent();
        if (Voto2.is(':visible')) {
            Voto2.hide();
        }

        var Max2 = $('#Max3').parent();
        if (Max2.is(':visible')) {
            Max2.hide();
        }

        var Lode2 = $('#Lode3').parent();
        if (Lode2.is(':visible')) {
            Lode2.hide();
        }
    }

    function ShowTerzoTitolo() {
        var TitoloDiStudioForm2 = $('#TitoloDiStudioForm3');
        if (!TitoloDiStudioForm2.is(':visible')) {
            TitoloDiStudioForm2.show();
        }

        var TipoTitoloDiStudio2 = $('#TipoTitoloDiStudio3').parent();
        if (!TipoTitoloDiStudio2.is(':visible')) {
            TipoTitoloDiStudio2.show();
        }

        var Voto2 = $('#Voto3').parent();
        if (!Voto2.is(':visible')) {
            Voto2.show();
        }

        var Max2 = $('#Max3').parent();
        if (!Max2.is(':visible')) {
            Max2.show();
        }

        var Lode2 = $('#Lode3').parent();
        if (!Lode2.is(':visible')) {
            Lode2.show();
        }

    }
            
    function setDefaultValue()
    {
        if (!($('#CorsoForm').find('.not-valid').length > 0)) {
            $('#CorsoForm').find('#DataInizio').val("");
            $('#CorsoForm').find('#DataFine').val("");
        }
                
        if (!($('#ServiziPrestatiInfForm').find('.not-valid').length > 0)) {
            $('#ServiziPrestatiInfForm').find('#DataInizio').val("");
            $('#ServiziPrestatiInfForm').find('#DataFine').val("");
        }

        if (!($('#ServiziPrestatiSupForm').find('.not-valid').length > 0)) {
            $('#ServiziPrestatiSupForm').find('#DataInizio_sup').val("");
            $('#ServiziPrestatiSupForm').find('#DataFine_sup').val("");
        }
    }
}