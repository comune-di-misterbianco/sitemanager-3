$(document).ready(function(){
    $('#loading').hide();
});
/*
$("#btnexport").click(function() {
	$("#loading").show();
});*/


$(function () {
	
	window.addEventListener('submit', logSubmit);
	
	function logSubmit(event) {
		
		event.preventDefault();
		
	    var loadingText = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Esportazione in corso ...';		
	
		var btn = document.getElementById("btnexport");
		btn.disabled = 'true';
		btn.insertAdjacentHTML('afterend',loadingText);		
		
		__doPostBack("btnexport");	
	}
})