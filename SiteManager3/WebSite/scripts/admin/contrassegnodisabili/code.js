﻿function start() {

    setPageCittadino();

    $('#RichiestaStartForm').click(function () {
        setPageCittadino();
    });
        
    function setPageCittadino() {
        if ($('#SceltaTipoCittadino_0').is(':checked')) {
            activateIntestatario(); 
            }
        else if ($('#SceltaTipoCittadino_1').is(':checked')) {
            activateTutor();
        }


        $("#SceltaTipoContrassegno_1").attr("disabled", true);

        if ($('#SceltaTipoContrassegno_0').is(':checked')) {

            $("#SceltaTipoRichiesta_0").attr("disabled", false);
            $("#SceltaTipoRichiesta_1").attr("disabled", false);
            $("#SceltaTipoRichiesta_2").attr("disabled", false);
            $("#SceltaTipoRichiesta_3").attr("disabled", true);
            $("#SceltaTipoRichiesta_4").attr("disabled", true);
        }
        else if ($('#SceltaTipoContrassegno_1').is(':checked')) {

            $("#SceltaTipoRichiesta_0").attr("disabled", true);
            $("#SceltaTipoRichiesta_1").attr("disabled", true);
            $("#SceltaTipoRichiesta_2").attr("disabled", true);
            $("#SceltaTipoRichiesta_3").attr("disabled", false);
            $("#SceltaTipoRichiesta_4").attr("disabled", false);
        }
        

        if ($('#SceltaTipoRichiesta_0').is(':checked')) {
            activateRichiestaRilascio(); 
        }
        else {
            deactivateRichiestaRilascio(); 
        }

        if ($('#SceltaTipoRichiesta_1').is(':checked')) {
            activateRichiestaRinnovo(); 
        }

        if ($('#SceltaTipoRichiesta_2').is(':checked')) {
            activateRichiestaDuplicato(); 
        }
        else {
            deactivateRichiestaDuplicato(); 
        }

        if ($('#SceltaTipoRichiesta_3').is(':checked')) { }

        if ($('#SceltaTipoRichiesta_4').is(':checked')) { }
    }

    function activateIntestatario() {

        if ($('#TipoTutorRadioButton').is(':visible')) {
            $('#TipoTutorRadioButton').hide();
        }
    }

    function activateTutor() {
        if (!$('#TipoTutorRadioButton').is(':visible')) {
            $('#TipoTutorRadioButton').show("clip",1000);
        }
    }

    function activateRichiestaRilascio() {
        if (!$('#TipoRichiestaRilascioRadioButton').is(':visible')) {
            $('#TipoRichiestaRilascioRadioButton').show("clip",1000);
        }
    }

    function deactivateRichiestaRilascio() {
        if ($('#TipoRichiestaRilascioRadioButton').is(':visible')) {
            $('#TipoRichiestaRilascioRadioButton').hide("clip",1000);
        }
    }

    function activateRichiestaDuplicato() {
        if (!$('#TipoRichiestaDuplicatoRAdioButton').is(':visible')) {
            $('#TipoRichiestaDuplicatoRAdioButton').show("clip",1000);
        }
    }

    function deactivateRichiestaDuplicato() {
        if ($('#TipoRichiestaDuplicatoRAdioButton').is(':visible')) {
            $('#TipoRichiestaDuplicatoRAdioButton').hide("clip",1000);
        }
    }
};