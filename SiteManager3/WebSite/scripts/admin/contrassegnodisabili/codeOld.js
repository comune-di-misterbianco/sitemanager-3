﻿function start() {


    setPageCittadino();
    //    $('#contenitoreForm').accordion();

    $('#RichiestaStartForm').click(function () {
        setPageCittadino();
    });

    function setPageCittadino() {
        if ($('#SceltaTipoCittadino_0').is(':checked')) { activateIntestatario(); }
        else if ($('#SceltaTipoCittadino_1').is(':checked')) { activateTutor(); }

        if ($('#SceltaTipoRichiesta_0').is(':checked')) { activateRichiestaRilascio(); }
        else { deactivateRichiestaRilascio(); }

        if ($('#SceltaTipoRichiesta_1').is(':checked')) { activateRichiestaRinnovo(); }

        if ($('#SceltaTipoRichiesta_2').is(':checked')) { activateRichiestaDuplicato(); }
        else { deactivateRichiestaDuplicato(); }

        if ($('#SceltaTipoRichiesta_3').is(':checked')) { deactivateAll(); }

        if ($('#SceltaTipoRichiesta_4').is(':checked')) { deactivateAll(); }
    }

    function activateIntestatario() {
        $('#RichiestaIntestatarioForm').hide();
        $('#Tutor').hide();
        $('#Intestatario').show();
        $('#CartaIdentitaIntestatario').show();
        $('#CartaIdentitaTutor').hide();
        $('#TipoTutorRadioButton').hide();
    }

    function activateTutor() {
        $('#RichiestaIntestatarioForm').show();
        $('#Intestatario').hide();
        $('#Tutor').show();
        $('#CartaIdentitaIntestatario').hide();
        $('#CartaIdentitaTutor').show();
        $('#TipoTutorRadioButton').show();
    }

    function activateRichiestaRilascio() {
        $('#TipoRichiestaRilascioRadioButton').show();

        CertificatoASP(true);
        CertificatoMedico(true);
        FotoTessera(true);
        Patente(true);
        LibrettoCircolazione(true);
        Denuncia(false);
        ContrassegnoScaduto(false);
    }

    function activateRichiestaRinnovo() {

        CertificatoASP(true);
        CertificatoMedico(true);
        FotoTessera(true);
        Patente(true);
        LibrettoCircolazione(true);
        Denuncia(false);
        ContrassegnoScaduto(true);
    }

    function deactivateRichiestaRilascio() {
        $('#TipoRichiestaRilascioRadioButton').hide();
    }

    function activateRichiestaDuplicato() {
        $('#TipoRichiestaDuplicatoRAdioButton').show();

        CertificatoASP(false);
        CertificatoMedico(false);
        FotoTessera(true);
        Patente(false);
        LibrettoCircolazione(false);
        Denuncia(true);
        ContrassegnoScaduto(false);
    }
    function deactivateAll() {

        CertificatoASP(false);
        CertificatoMedico(false);
        FotoTessera(false);
        Patente(false);
        LibrettoCircolazione(false);
        Denuncia(false);
        ContrassegnoScaduto(false);
    }


    function deactivateRichiestaDuplicato() {
        $('#TipoRichiestaDuplicatoRAdioButton').hide();
    }

    function FotoTessera(active) {
        if (active) { $('#FotoTessera').show(); }
        else { $('#FotoTessera').hide(); }
    }

    function CertificatoASP(active) {
        if (active) { $('#CertificatoASP').show(); }
        else { $('#CertificatoASP').hide(); }
    }

    function Denuncia(active) {
        if (active) { $('#Denuncia').show(); }
        else { $('#Denuncia').hide(); }
    }

    function Patente(active) {
        if (active) { $('#Patente').show(); }
        else { $('#Patente').hide(); }
    }

    function CertificatoMedico(active) {
        if (active) { $('#CertificatoMedico').show(); }
        else { $('#CertificatoMedico').hide(); }
    }

    function ContrassegnoScaduto(active) {
        if (active) { $('#ContrassegnoScaduto').show(); }
        else { $('#ContrassegnoScaduto').hide(); }
    }

    function LibrettoCircolazione(active) {
        if (active) { $('#LibrettoCircolazione').show(); }
        else { $('#LibrettoCircolazione').hide(); }
    }
};