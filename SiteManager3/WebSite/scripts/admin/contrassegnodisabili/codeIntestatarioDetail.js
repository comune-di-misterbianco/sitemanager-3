﻿function start() {

    $('#dialogModifica').dialog({
        autoOpen: false,
        closeOnEscape: false,
        resizable: false,
        draggable: false,
        modal: true,
        show: {
            effect: 'blind',
            //duration: 1000
        },
        hide: {
            effect: 'explode',
            duration: 1000
        },
        title: 'Richiesta modifica',
        buttons: {
            Conferma: function () {
                $('#div_erroreModifica').hide();
                if ($('#notaDiModifica').val().length > 0) {
                    if ($('#notaDiModifica').val().length > 255) {
                        $('#div_erroreModifica').text('Il campo Nota non può contenere più di 255 caratteri.');
                        $('#div_erroreModifica').show();
                    }
                    else {
                        $('#submitNota').click();
                    }
                }
                else {
                    $('#div_erroreModifica').text('Inserire un valore nel campo Nota');
                    $('#div_erroreModifica').show();
                }
            },
            Annulla: function () {
                $(this).dialog('close');
            }
        }
    });

    $('#respingi_button').click(function () {
        $('#dialogModifica').dialog('open');
        $('#dialogModifica').parent().appendTo($('form:first'));
       // $('#notaDiModifica').val('');
        $('#div_erroreModifica').hide();
        return false;
    });

    $('#dialogValida').dialog({
        autoOpen: false,
        closeOnEscape: false,
        resizable: false,
        draggable: false,
        modal: true,
        show: {
            effect: 'blind',
            //duration: 1000
        },
        hide: {
            effect: 'explode',
            duration: 1000
        },
        title: 'Valida',
        buttons: {
            Conferma: function () {
                $('#Valida').click();
            },
            Annulla: function () {
                $(this).dialog('close');
                if ($('#ValidaContrassegnoForm').find('.not-valid').length > 0) {
                    $('#ValidaContrassegnoForm').find('.not-valid>.validation-results').hide();
                }
            }
        }
    });

    if ($('#ValidaContrassegnoForm').find('.not-valid').length > 0) {
        $('#dialogValida').dialog('open');
    }

    $('#valida_button').click(function () {
        $('#dialogValida').dialog('open');
        $('#dialogValida').parent().appendTo($('form:first'));
        return false;
    });


    $('#dialogRifiuta').dialog({
        autoOpen: false,
        closeOnEscape: false,
        resizable: false,
        draggable: false,
        modal: true,
        show: {
            effect: 'blind',
            //duration: 1000
        },
        hide: {
            effect: 'explode',
            duration: 1000
        },
        title: 'RIFIUTA',
        buttons: {
            Conferma: function () {
                $('#annullaRichiesta').click();
            },
            Annulla: function () {
                $(this).dialog('close');
            }
        }
    });

    $('#rifiuta_button_handler').click(function () {
       $('#dialogRifiuta').dialog('open');
       $('#dialogRifiuta').parent().appendTo($('form:first'));
        return false;
    });
    

}

