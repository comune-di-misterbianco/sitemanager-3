﻿$(function () {

    $('#options-dialog').dialog({
        autoOpen: false,
        hide: {
            effect: 'explode',
            duration: 200
        },
        closeOnEscape: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        draggable: false,
        modal: true,
        title: 'Dashboard options',
        buttons: {
            Save: function () {

                var identifier = $('.optionsform').attr('id').split('-')[1];
                var frontendDashboard = $('#EnableFrontendDashboard').is(':checked');
                var paramerers =
                {
                    ElaborationID: identifier,

                    EnableFrontendDashboard: frontendDashboard

                };

                $.ajax({
                    url: $("#api-path").val(),
                    type: "POST",
                    accept: "application/json",
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify(paramerers),
                    success: function (data) {

                        if (data) {
                            $("#msgs-container").append("<p class='success'>Succcessful stored elaborations</p>");
                        }
                        else {
                            $("#msgs-container").empty();
                            $('#msgs-container').append("<p class='error'>Store error</p>");
                        }


                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $("#msgs-container").empty();
                        $('#msgs-container').append("<p class='error'>" + textStatus + ": " + errorThrown + "</p>");
                    }
                });
                

            },
            Close: function () {
                $(this).dialog('close');
            }
        }
    });


    $('#options_button').click(function () {
        $("#msgs-container").empty();
        $('#options-dialog').dialog('open');
        $('#options-dialog').parent().appendTo($('form:first'));
        return false;
    });

});