﻿$(function () {

    $('#dialogDomande').dialog({
        autoOpen: false,
        hide: {
            effect: 'explode',
            duration: 500
        },
        closeOnEscape: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        draggable: false,
        modal: true,
        title: 'Inserimento nuova domanda',
        buttons: {
            Conferma: function () {

                var ok = new Boolean(true);

                var testo = $('#Testo').val();
                var descrizione = $('#Descrizione').val();
                var tipoDomanda = $('#TipoDomanda').val()
                var help = $('#EnableInLineHelp').prop("checked");
                var risp = $('#IsMandatary').prop("checked");

                if (testo == '') {
                    ok = false;
                    $('#errors-container').append("<p class='error'>Il testo della domanda è obbligatorio</p>");
                }

                if (ok && descrizione == '') {
                    ok = false;
                    $('#errors-container').append("<p class='error'>La descrizione della domanda è obbligatoria</p>");
                }

                if (ok && tipoDomanda == '') {
                    ok = false;
                    $('#errors-container').append("<p class='error'>La tipologia di domanda è obbligatoria</p>");
                }

                if (ok) {

                    var question =
                    {
                        Testo: testo,
                        Descrizione: descrizione,
                        TipoDomanda: tipoDomanda,
                        EnableInLineHelp: help,
                        IsMandatary: risp
                    };

                    $.ajax({
                        url: $("#api-path").val(),
                        type: "POST",
                        accept: "application/json",
                        contentType: "application/json",   
                        dataType: "json",                   
                        data: JSON.stringify(question),
                        success: function (data) {

                            if (data.length != 0) {
                                id= data["ID"];
                                $('#domande').append($('<option>', {
                                    value: id,
                                    text: data['Text']
                                }));
                                $('#domande').val(id);
                                $('#domande').parent().append("<p class='success'>Domanda inserita con successo</p>");
                                $('#dialogDomande').dialog('close');
                            }
                            else {
                                $("#errors-container").empty();
                                $('#errors-container').append("<p class='error'>Errore nell'inserimento della domanda</p>");
                            }


                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $("#errors-container").empty();
                            $('#errors-container').append("<p class='error'>" + textStatus + ": " + errorThrown + "</p>");
                        }
                    });
                }

            },
            Annulla: function () {
                $(this).dialog('close');
            }
        }
    });


    $('#add_domanda_button').click(function () {
        $("#errors-container").empty();
        $(':input', $("#domandeorm")).each(function () {
            var type = this.type;
            var tag = this.tagName.toLowerCase();
            if (type == 'text' || type == 'password' || tag == 'textarea')
                this.value = "";
            else if (type == 'checkbox' || type == 'radio')
                this.checked = false;
            else if (tag == 'select')
                this.selectedIndex = -1;
        });
        $('#dialogDomande').dialog('open');
        $('#dialogDomande').parent().appendTo($('form:first'));
        return false;
    });

});