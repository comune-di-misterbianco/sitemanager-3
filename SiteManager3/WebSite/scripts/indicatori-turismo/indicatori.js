﻿$(document).ready(function () {

    var NotValidItem = false;
    var descriptionArray = new Array();
    var currentCorrelation = '';

    function AddToFormula(addedValue) {
        descriptionArray.push(addedValue);

        $('#Value').val(JSON.stringify(descriptionArray));
    }

    $(function () {


        function set(id, message, identifier) {
            $('#selection-box').val(message);
            $('#add-btn').attr('name', identifier.toString());
        }

        $('#selection-box').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: endPoint + (indicatorType == 'IC' ? "GetDomande/" : "GetIndicatori/"),
                    contentType: 'json',
                    data: {
                        value: request.term
                    },
                    success: function (data) {


                        if (data.length == 0)
                            NotValidItem = true;
                        else
                            NotValidItem = false;


                        response($.map(data, function (item) {
                            return {
                                id: item.ID,
                                label: item.Text,
                                value: item.Text
                            }
                        }));
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(textStatus);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {

                var id_tag = '" + selection-box + @"' +
										set(id_tag, ui.item ?
										'Selected: ' + ui.item.label :
										'Nothing selected, input was ' + this.value, ui.item.id);
                $('#add-btn').removeAttr('disabled');
            }
        });
    });

    //    $('#Formula').on('input', function () {
    //        currentCorrelation = $('#Formula').val();
    //    });

    $('#add-btn').click(function (event) {

        var text = $('#selection-box').val();
        var errors = false;
        var id = $(this).attr('name');

        if (text == '') {
            errors = true;
            $('.error').remove();
            var errorText = indicatorType == 'IC' ? 'La selezione della domanda è obbligatoria' : 'La seleziona dell\'indicatore è obbligatoria';
            var errorCode = '<div class=error><li><span>' + errorText + '</span></li></div>';
            $('.input-container').append(errorCode);
        }


        if (!errors && NotValidItem) {
            errors = true;
            $('.error').remove();
            var errorText = indicatorType == 'IC' ? 'La domanda immessa non è valida' : 'L\' indicatore immesso non è valido';
            var errorCode = '<div class=error><li><span>' + errorText + '</span></li></div>';
            $('.input-container').append(errorCode);
        }

        if (!errors && currentID != '' && id == currentID) {
            errors = true;
            $('.error').remove();
            var errorText = 'Impossibile correlare un indicatore con se stesso.';
            var errorCode = '<div class=error><li><span>' + errorText + '</span></li></div>';
            $('.input-container').append(errorCode);
        }

        if (!errors) {

            $('.error').remove();

            currentCorrelation += "[" + text + "]";
            $('#Formula').val(currentCorrelation);
            var index = "ID" + id;
            AddToFormula(index);

        }

        $('#selection-box').val('');
        $('#add-btn').prop('disabled', true);
        return false;
    });

    $('#operators').on('click', '.operator-btn', function () {
        if ($(this).val().trim() == 'C') {
            currentCorrelation = '';
            descriptionArray = new Array();
            $('#Value').val(currentCorrelation);
        }
        else {
            currentCorrelation += $(this).val().trim();
            AddToFormula($(this).val().trim());
        }


        $('#Formula').val(currentCorrelation);
        return false;
    });


    $('#add-const-btn').click(function (event) {

        var text = $('#const-box').val().trim();

        if (!$.isNumeric(text)) {
            $('.error').remove();
            var errorText = 'Non è stato inserito un numero valido.';
            var errorCode = '<div class=error><li><span>' + errorText + '</span></li></div>';
            $('.const-container').append(errorCode);
        }
        else {
            $('.error').remove();

            currentCorrelation += text;
            $('#Formula').val(currentCorrelation);
            AddToFormula(text);

        }

        return false;

    });
});