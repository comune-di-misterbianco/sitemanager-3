﻿function checkAllTags() {

    var status = $('#checkAllTop').prop('checked') || $('#checkAllBottom').prop('checked');

    $('.tag-div').find(':checkbox:not(:disabled)').each(function () {

            $(this).attr('checked', status);

    });
}