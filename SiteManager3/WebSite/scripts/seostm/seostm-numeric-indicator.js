﻿//*********     DINAMIC FORM COMPONENTS *********//
$(function () {

    var rangesCounter = $('#ranges-fieldset').find('fieldset').length;
    var currentFreeIndex = rangesCounter + 1;
    var Ranges = new Array();
    var freeIndex = 0;

    function CreateRange(idNumber) {
        //        div.find('#templatedel').attr('id', 'checkdel_' + currentFreeIndex);
        //        div.find('#del').attr('id', 'del_' + currentFreeIndex);
        //        div.find('#template').attr('id', 'range_' + currentFreeIndex);

        //        div.find('#template-fielset').attr('id', 'fielset_' + currentFreeIndex);
        //        div.find('#template-legend').text('Range n° ' + currentFreeIndex);
        //        div.find('#template-legend').attr('id', 'legend_' + currentFreeIndex);
        //        div.find('#template-Minimo').attr('id', 'div-minimo_' + currentFreeIndex);
        //        div.find('#template-Massimo').attr('id', 'div-massimo_' + currentFreeIndex);
        //        div.find('#template-Peso').attr('id', 'div-peso_' + currentFreeIndex);
        //        div.find('#ValoreMinimo').attr('id', 'min_' + currentFreeIndex);
        //        div.find('#ValoreMassimo').attr('id', 'max_' + currentFreeIndex);
        //        div.find('#Peso').attr('id', 'peso_' + currentFreeIndex);

        var div = $('<div/>', { id: 'range_' + idNumber, class: 'range' });

        //        var dellabel = $('<label>').text('Elimina');

        //        var delinput = $('<input type="checkbox">').attr({ id: 'del_' + idNumber, class: 'range-check' });

        //        div.append(dellabel);

        //        div.append(delinput);

        var fieldset = $('<fieldset/>', { id: 'fieldset_' + idNumber, class: 'singlerange-f' });

        fieldset.append('<legend> Range n° ' + idNumber + '</legend>');

        var deleteButton = $('<input type="submit">').attr({ id: 'del_' + idNumber, class: 'range-delete-button', value: ' X ' });

        deleteButton.click(function () {
            var index = $(this).prop('id').split('_')[1];
            $('#range_' + index).remove();
            rangesCounter--;
            if (freeIndex == 0)
                freeIndex = index;
            if (rangesCounter == 0)
                freeIndex = 0;
            return false;
        });

        fieldset.append(deleteButton);

        var minlabel = $('<label>').text('Valore minimo/iniziale del range *');

        var mininput = $('<input type="text">').attr({ id: 'min_' + idNumber, class: 'range-input' });

        fieldset.append(minlabel);

        fieldset.append(mininput);

        var maxlabel = $('<label>').text('Valore massimo/finale del range *');

        var maxinput = $('<input type="text">').attr({ id: 'max_' + idNumber, class: 'range-input' });

        fieldset.append(maxlabel);

        fieldset.append(maxinput);

        var pesolabel = $('<label>').text('Peso da associare al range *');

        var pesoinput = $('<input type="text">').attr({ id: 'peso_' + idNumber, class: 'range-input' });

        fieldset.append(pesolabel);

        fieldset.append(pesoinput);

        div.append(fieldset);

        return div;
    }

    function ShowError(msg) {
        return $('<div/>', { text: msg, class: 'validation-errors' });
    }

    function ValidateRange(range, id) {
        var validation = true;

        if (range.ValoreMinimo === undefined || range.ValoreMinimo == null || range.ValoreMinimo < 0 || range.ValoreMinimo.length == 0) {
            $('#fieldset_' + id).append(ShowError('Il campo valore minimo non è valido per questo range'));
            validation = false;
        }

        if (validation && (range.ValoreMassimo === undefined || range.ValoreMassimo == null || range.ValoreMassimo < 0 || range.ValoreMassimo.length == 0)) {
            $('#fieldset_' + id).append(ShowError('Il campo valore massimo non è valido per questo range'));
            validation = false;
        }

        if (validation && (range.Peso === undefined || range.Peso == null || range.Peso <= 0 || range.Peso.length == 0)) {
            $('#fieldset_' + id).append(ShowError('Il campo peso non è valido per questo range'));
            validation = false;
        }

        if (validation && range.ValoreMinimo > range.ValoreMassimo) {
            $('#fieldset_' + id).append(ShowError('Il campo valore minimo risulta maggiore del massimo, verificare'));
            validation = false;
        }

        return validation;
    }


    $('#add-button').click(function () {
        rangesCounter++;
        if (freeIndex != 0) {
            currentFreeIndex = freeIndex;
            freeIndex = 0;
        }
        else
            currentFreeIndex = rangesCounter;

        var div = CreateRange(currentFreeIndex);

        $('#ranges-fieldset').append(div);
        div.show();

        return false;
    });


    //    $('#remove-button').click(function () {

    //        $('#ranges-fieldset').find('input:checkbox:checked').each(function () {
    //            var index = $(this).prop('id').split('_')[1];
    //            $('#range_' + index).remove();
    //            rangesCounter--;
    //            if (freeIndex == 0)
    //                freeIndex = index;
    //            if (rangesCounter == 0)
    //                freeIndex = 0;

    //        });
    //        return false;
    //    });

    $('.range-delete-button').click(function () {

        var index = $(this).prop('id').split('_')[1];
        $('#range_' + index).remove();
        rangesCounter--;
        if (freeIndex == 0)
            freeIndex = index;
        if (rangesCounter == 0)
            freeIndex = 0;
        return false;

    });

    $('#new-insert-btn').click(function () {
        var ok = new Boolean();
        ok = true;

        for (var i = 1; i <= rangesCounter; i++) {
            var range = {
                ValoreMinimo: $('#min_' + i).val(),
                ValoreMassimo: $('#max_' + i).val(),
                Peso: $('#peso_' + i).val()
            };
            if (ValidateRange(range, i))
                Ranges.push(range);
            else {
                ok = false;
                break;
            }
        }

        if (ok) {
            $('#result').val(JSON.stringify(Ranges));
            return true;
        }
        else
            return false;
    });

});



     