$(document).ready(function () {
    initContainers();

	$("div.panel_button").click(function(){
		$("div#panel").animate({
			height: "148px"
		})
		.animate({
			height: "138px"
		}, "fast");
		$("div.panel_button").toggle();
	
	});	
	
   $("div#hide_button").click(function(){
		$("div#panel").animate({
			height: "0px"
		}, "fast");
		
	
   });

});

function initContainers() {
	var p0 = document.createElement("div");
    p0.id = "panel-global";
	
    var p1 = document.createElement("div");
    p1.id = "panel";

    var panel = $('<div id="panel_contents"></div>');
    var panel_contents = $('<div id="panel_contents"></div>');
	
	var panel = $("#panel-global");
    var panel = $("#panel");
    var panel_contents = $("#panel_contents");
    var loginFieldset = $("#LoginForm");
    var loginFieldsetParent = loginFieldset.parent();

	var trovato = loginFieldset.find('.Logged');	
	if (trovato.length == 0)
	{
		$('<div id="panel-global"><div id="panel"><div id="panel_contents"></div></div></div>').appendTo(loginFieldsetParent);
		var panel_contents = $("#panel_contents");
		loginFieldset.remove();
		
		loginFieldset.appendTo(panel_contents);
		$('<div class="panel_button" style="display: visible;"><img src="/images/expand.png"  alt="expand"/> <a href="#">Login</a> </div>').appendTo(loginFieldsetParent);
		$('<div class="panel_button" id="hide_button" style="display: none;"><img src="/images/collapse.png" alt="collapse" /> <a href="#">Chiudi Login</a> </div>').appendTo(loginFieldsetParent);
	}
}