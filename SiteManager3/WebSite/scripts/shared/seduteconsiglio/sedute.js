﻿var baseAddress = "http://localhost:1459/WebSite/api/";

function checkDiretta() {
    $.ajax({
        url: baseAddress + 'Sedute/GetDiretta/live',
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            //var statoDiretta = 'normal';
            var idDiretta = 0;
            if (data > -1) {
                //statoDiretta = 'live';
                idDiretta = data;
                var htmlDiretta = '<p>Diretta in corso</p><a href="diretta.aspx?sedutaID=' + idDiretta + '"><span>Segui il video live!</span></a>';
                $('#box_diretta').empty();
                $('#box_diretta').append(htmlDiretta);
            }
            else {
                var htmlNoDiretta = '<p>Nessuna diretta in corso!</p>';
                $('#box_diretta').empty();                
                $('#box_diretta').append(htmlNoDiretta);
            }
        },
        error: function (data) {
            //alert(""Error: "" + data);
        }
    });
}    
$(document).ready(function() {
    $(".accordionCtrl").accordion({
        active: false,
        autoHeight: false,
        collapsible: true
    });
    //setTimeout(checkDiretta, 5000);
});
var holdTheInterval = setInterval(checkDiretta, 5000); 