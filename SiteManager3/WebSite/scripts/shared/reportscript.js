$(function() {
	
	$('#dialogReport').dialog({
		
		autoOpen: false,
		hide: {
			 effect: 'explode',
			 duration: 500
		},
		closeOnEscape : false,
		height: 300,
		width  : 600,
		resizable: true,
		draggable: false,
		modal    : true,
		title    : 'Report',
		buttons : {
			
			Salva : function () {
			
				var ok = new Boolean(true);
				var regex = /^[_àèéìòù0-9a-zA-Z\'\,\.\- ]*$/;
			
				if(ok)
				{
					$('[id*=submitReport]').click();
				}
			},
			Genera: function() {
				
				$('[id*=generaPdf]').click();		
			},
			Annulla: function() {
				
				$('#div_errore_nv').hide();
				$('#div_errore_regex_rg').hide();
				$(this).dialog('close');
			}			
		}
	});
	$('input[id^=reportModify]').click(function() {
			
			$('#dialogReport').dialog('open');
			$('#dialogReport').parent().appendTo($('form:first'));
			return false;
		
	});	
	
	
});

