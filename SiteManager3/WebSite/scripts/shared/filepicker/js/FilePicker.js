(function ($) {
    var dataSource;

    var methods = {
        init: function (options) {
            var defaults = {
                url: "",
                dataType: 'json',
                height: 200,    //default height
                width: 'auto',   //auto width
                page: 1, //current page
                total: 1, //total pages
                rp: 15, //results per page
                highlightClass: 'ui-state-highlight',
                hoverClass: 'ui-state-active'
            };
            $.extend(defaults, options);

            var container = document.createElement('div');
            container.className = ("pickerContainer");
            $(this).append(container);

            var paginator = document.createElement('div');
            paginator.style.textAlign = "center";
            paginator.className = ("pagination_fp mt-3");
            $(this).append(paginator);

            if (defaults.url != "") {
                getDataSource(defaults.url);                
                loadFilePicker();                
                buildPagination(defaults.url);
            }

        },
        update: function (url) {
            getDataSource(url);            
            loadFilePicker();            
            buildPagination(url);
        },
        close: function () {
            window.parent.$('#filebrowser').modal('toggle');
        }
    };

    $.fn.filePicker = function (methodOrOptions) {
        if (methods[methodOrOptions]) {
            return methods[methodOrOptions].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
            // Default to "init"
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + methodOrOptions + ' does not exist on jQuery.tooltip');
        }
    };

    function loadFilePicker() {

        $(".pickerContainer").empty();
       // console.log(dataSource);
        if (typeof dataSource != 'undefined') {
            var currentPage = dataSource.page;
            var totalPages = dataSource.total;
            var files = dataSource.rows;

            createImageContainer($(".pickerContainer"), files);
        }
       
    }

    function getDataSource(url) {
        if (url) {
            $.ajax({
                url: url,
                dataType: 'json',
                async: false,
                type: 'GET',
                success: function (jsonData, status) {
                    dataSource = jsonData;
                }
            });
        }
    }

    function createImageContainer(element, files) {
        var picker = document.createElement('div');
        picker.className = ("picker");
        element.append(picker);

        if (files != null && files.length > 0) {

            var imagesList = document.createElement('ul');
            imagesList.className = ("thumbnails image_picker_selector");
            $('.picker').append(imagesList);

            files.forEach(function addLI(value) {
                if (value.id.indexOf("F_") == -1) {
                    var item = '<li><div class="thumbnail"><img class="image_picker_image" alt="" data-id="' + value.id + '" src="' + value.cell[4] + '?w=148&h=100&mode=scale" /><div class="info"><p><span>' + value.cell[0] + '</span>' + value.cell[2] + ' </p></div></div></li>';
                    $('.image_picker_selector').append(item);
                }
            });
        }
        else {
            var message = document.createElement('div');
            message.className = ("info_message");
            $(".picker").append(message);

            var txtMsg = "<p>La cartella selezionata non contiene immagini.</p>";
            $(".info_message").append(txtMsg);
        }
       
        $(".thumbnail").click(function () {

            if (this.className != ("thumbnail selected")) {
                $(".thumbnails li div").removeClass("selected");
                $(this).toggleClass("selected");                
                $('#select-button').removeAttr("disabled");
            }
            else {
                $(this).toggleClass("selected");
                $('#select-button').attr('disabled', 'true');
            }
        });

        $("#select-button").click(function () {
            var item = $(".image_picker_selector").find('.selected');
            var idDoc = item.children('img').attr('data-id');

            var srcImage = item.children('img').attr('src');
            srcImage = srcImage.substring(0, srcImage.indexOf('?'));

            var srcDoc = srcImage + '?preset=netimagepickerthumb';

            var oOpener = window.parent;
            
            var imgThumb = oOpener.$('#selectedimage');           
            imgThumb.attr('src', srcDoc); 
            oOpener.$('.netimagepicker input[type=hidden]').val(idDoc);
            oOpener.$('#removeimage').toggle();
            oOpener.$('#filebrowser').modal('toggle');
            
        });
    }

    function buildPagination(jsonUrl) {

        $(".pagination_fp").empty();
        
        if (typeof dataSource != 'undefined') {
            var currentPage = dataSource.page;
            var totalElement = dataSource.total;          

            if (totalElement > 1) {

                $(".pagination_fp").pagination(totalElement, {
                    items_per_page: 15,
                    current_page: currentPage - 1,
                    callback: function (page, event) {
                        var selPage = page + 1;
                        if (selPage > 1 || (dataSource.page + selPage > 0)) {

                            var arrayUrl = jsonUrl.split('/');

                            var newUrl = "";
                            // set in array path new page
                            var pagePos = (arrayUrl.length - 1) - 4;


                            arrayUrl[pagePos] = selPage;

                            for (i = 1; i < arrayUrl.length; i++) {
                                newUrl += "/";
                                newUrl += arrayUrl[i];                         
                            }

                            getDataSource(newUrl);
                            loadFilePicker();
                         

                        }
                        return false;
                    }
                });
            }
        }
    }   
})(jQuery);


function resetImageForm(absoluteWebRoot, rifdocid) {
     
    $('#selectedimage').attr('src', absoluteWebRoot + "/repository/imagepicker/noimages.jpg?preset=netimagepickerthumb");
    $('#selectedimage').attr('data-id', '');
 
    $('#'+rifdocid).val('');
    $('#Description').val('');

    //$("#imageAlign").val($("#imageAlign option:first").val());
    $("#Align :selected").removeAttr("selected");

    $("#ShowInModulo").attr('checked', false);
    $("#ShowInTeaser").attr('checked', false);
    $("#ShowInDetail").attr('checked', false);

    $("#removeimage").attr('style', 'display:none !important;');
}