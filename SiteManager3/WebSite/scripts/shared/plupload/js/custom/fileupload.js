﻿$(document).ready(function () {
    var files_selected = new Array();
    var uploader = new plupload.Uploader({
        runtimes: 'html5,flash,silverlight,html4',
        browse_button: 'pickfiles', 
        container: document.getElementById('DropArea'), // DOM element of plupload
        url: "/charts/plupload_handler.ashx",
        chunk_size: '100kb',
        filters: {
            max_file_size: '10mb',
            mime_types: [
            { title: "Image files", extensions: "jpg,jpeg,gif,png" },
            { title: "Document files", extensions: "doc,docx,xls,xlsx,pdf,p7f" },
            { title: "Zip files", extensions: "zip,rar" }
            ]
        },
        dragdrop: true,
        drop_element: 'DropArea',
        //autostart: true,
        multi_selection : true,
        flash_swf_url: '/scripts/shared/plupload/js/plupload.flash.swf', // Flash settings
        silverlight_xap_url: '/scripts/shared/plupload/js/plupload.silverlight.xap', // Silverlight settings

        init: {
            PostInit: function () {
              
                $('#plupload').empty();
               
                plupload.addI18n({
                    'File extension error.': 'Il file selezionato non è supportato.',
                    'File size error.': 'La dimensione del file supera i limiti consentiti.'
                });

                //$('#uploadfiles').click(function () {
                //    uploader.start();
                //    return false;
                //});

            },

            FilesAdded: function (up, files) {
                plupload.each(files, function (file)
                {
                    var results;
                    var params = {
                        "folderId": $('#currentFolder').val(),
                        "filename": file.name
                    };

                    $.ajax({
                        url: '/api/UploadManager/checkfile/',
                        type: 'POST',
                        data: JSON.stringify(params),
                        contentType: "application/json",
                        dataType: 'json',
                        async: false,
                        success: function(response){                       
                            results = response;
                        }
                    });

                    if (!results.exist) {
                        if ($.inArray(file.name, files_selected) == -1) {
                            var item = '<li class="file-selected" id="' + file.id + '">'
                                     + '<div class="file">'
                                     + '    <div class="file-container">'
                                     + '        <div class="status hide"></div>'
                                     + '        <div class="remove hide"><a href="#"><span>X</span></a></div>'
                                     + '        <div class="document"><img class="icon" src="/repository/plupload/document.png" /></div>'
                                     + '        <div class="filename"><div>' + file.name + ' [' + plupload.formatSize(file.size) +']</div></div>'                                     
                                     + '        <div class="progressbar hide"><div></div></div>'
                                     + '    </div>'
                                     + '</div>'
                                     + '</li>';

                            if ($('#files-selected-container').hasClass('hide'))
                                $('#files-selected-container').removeClass('hide');

                            $('.files-selected-list').append(item);
                            files_selected.push(file.name);
                            //up.start();
                            setTimeout(up.start(), 100);
                        }
                        else {
                            // add msg error // documento già selezionato
                            up.removeFile(file);
                        }
                    }
                    else
                    {
                        var errorMsg = '<li>' + results.message + '</li>';
                        if ($('#console').hasClass('hide'))
                            $('#console').removeClass('hide');
                        $('#console ul').append(errorMsg);
                    }
                });
                if (files_selected.length > 0) {
                    //$('#uploadfiles').prop('disabled', false);                    
                    if ($('#save').hasClass('btnHide')) {
                        $('#save').removeClass('btnHide').fadeIn(600);
                        $('#save').addClass('btnGreen');
                    }
                    $('#fileUploaded').val(files_selected);
                    
                }
            },
            UploadProgress: function (up, file) {

                if ($('#' + file.id + ' .progressbar').hasClass('hide'))
                $('#' + file.id + ' .progressbar').removeClass('hide').fadeIn(600);

                var percentLoadFile = file.percent;

                if (percentLoadFile < 100 && percentLoadFile >= 1) 
                    $('#' + file.id + ' .progressbar div').css('width', percentLoadFile + '%');                
                else                
                    $('#' + file.id + ' .progressbar').fadeOut(600);                
            },
            FileUploaded: function (up, file, response) {
                try{                    
                    if (response.status == '200') {                        
                        $('#' + file.id + ' .status').addClass('done');
                        $('#' + file.id + ' .status').css('display', 'block').append('<span>OK</span>');
                    }
                    else {                        
                        $('#' + file.id + ' .status').addClass('failed');
                        $('#' + file.id + ' .status').css('display', 'block').append('<span>Error!</span>');
                    }
                }
                catch (error) {
                    if ($('#console').hasClass('hide'))
                        $('#console').removeClass('hide');
                    $('#console ul').append("<li>"+errorMsg+"</li>");
                    
                }
            },
            Error: function (up, err) {                
                var errorMsg = '<li>Attenzione: ' + err.message + ' (cod.err ' + err.code + ') </li>';
                if ($('#console').hasClass('hide'))
                    $('#console').removeClass('hide');
                $('#console ul').append(errorMsg);
            }
        }
    });   

    uploader.init();   

    var dropArea = $('#DropArea');
    dropArea.on('dragenter', function (event) {               
        dropArea.addClass('dropzone');
        event.stopPropagation();
        event.preventDefault();
        return false;
    });
    dropArea.on('dragleave', function (event) {        
        dropArea.removeClass('dropzone');
        event.stopPropagation();
        event.preventDefault();
        return false;
    });
    dropArea.on('drop', function (event) {
        dropArea.removeClass('dropzone');
        event.stopPropagation();
        event.preventDefault();
        return false;
    });
});