/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here.
    // For complete reference see:
    // http://docs.ckeditor.com/#!/api/CKEDITOR.config    



    //La configurazione di default deve sempre esserci
    config.toolbar_default = [
	    { name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
	    { name: 'editing', groups: ['find', 'selection', 'spellchecker'], items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'] },
	    { name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
        { name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'SpecialChar'] },
        { name: 'tools', items: ['Maximize'] },
        { name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', 'autoFormat', 'CommentSelectedRange', 'UncommentSelectedRange', '-', 'Preview'] },
        { name: 'others', items: ['oembed', '-'] },
        '/',
	    { name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
	    { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
        { name: 'styles', items: ['Format'] },
	    { name: 'about', items: ['About'] }
    ];

    config.toolbar_basic = [
	    { name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
	    { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
	    '/',
	    { name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
	    { name: 'insert', items: ['Image'] },
	    { name: 'about', items: ['About'] }
    ];

    config.toolbar_forum = [
	    { name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
	    { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] }
    ];

    config.toolbar_normal = [
	    { name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
	    { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
	    '/',
	    { name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
	    { name: 'insert', items: ['Image'] },
	    { name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', 'autoFormat', 'CommentSelectedRange', 'UncommentSelectedRange', '-', 'Preview'] },
        { name: 'about', items: ['About'] }
    ];

    config.toolbar_full = [
	    { name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
	    { name: 'editing', groups: ['find', 'selection', 'spellchecker'], items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt', '-', 'Templates' ] },
	    { name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
        { name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'SpecialChar', 'ckeditorfa'] }, //,'btgrid', 'Glyphicons'
        { name: 'tools', items: ['Maximize'] },
        { name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', 'autoFormat', 'CommentSelectedRange', 'UncommentSelectedRange', '-', 'Preview'] },
        { name: 'others', items: ['oembed', '-'] },
        //{ name: 'youtube', items: ['Youtube'] },
        '/',
	    { name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
	    { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
        { name: 'styles', items: ['Format'] },	//		,'AddLayout'	
	    { name: 'about', items: ['About'] }
    ];

    // Remove some buttons provided by the standard plugins, which are
    // not needed in the Standard(s) toolbar.
    config.removeButtons = 'Underline,Subscript,Superscript';

    // Set the most common block elements.
    config.format_tags = 'p;h1;h2;h3;h4;h5;h6;pre';

    // Simplify the dialog windows.
    //config.removeDialogTabs = 'image:advanced;link:advanced';
    config.extraPlugins = 'codemirror,youtube,oembed,lineutils,colordialog,basewidget,templates,ckeditorfa'; //btgrid,layoutmanager,glyphicons,fontawesome
	config.allowedContent = true;
	config.removeFormatAttributes = '';
	config.contentsCss = '/scripts/shared/ckeditor/contents.css';

   // config.htmlEncodeOutput = true;

	config.layoutmanager_loadbootstrap = false;
    config.layoutmanager_buttonboxWidth = 58;
    config.layoutmanager_columnStart = 'xs'; //(bootstrap grid device breakpoints: xs, sm, md, lg)
	
	config.codemirror = {
		// Set this to the theme you wish to use (codemirror themes)
		theme: 'default',
		// Whether or not you want to show line numbers
		lineNumbers: true,
		// Whether or not you want to use line wrapping
		lineWrapping: true,
		// Whether or not you want to highlight matching braces
		matchBrackets: true,
		// Whether or not you want tags to automatically close themselves
		autoCloseTags: true,
		// Whether or not you want Brackets to automatically close themselves
		autoCloseBrackets: true,
		// Whether or not to enable search tools, CTRL+F (Find), CTRL+SHIFT+F (Replace), CTRL+SHIFT+R (Replace All), CTRL+G (Find Next), CTRL+SHIFT+G (Find Previous)
		enableSearchTools: true,
		// Whether or not you wish to enable code folding (requires 'lineNumbers' to be set to 'true')
		enableCodeFolding: true,
		// Whether or not to enable code formatting
		enableCodeFormatting: true,
		// Whether or not to automatically format code should be done when the editor is loaded
		autoFormatOnStart: true,
		// Whether or not to automatically format code should be done every time the source view is opened
		autoFormatOnModeChange: true,
		// Whether or not to automatically format code which has just been uncommented
		autoFormatOnUncomment: true,
		// Define the language specific mode 'htmlmixed' for html including (css, xml, javascript), 'application/x-httpd-php' for php mode including html, or 'text/javascript' for using java script only
		mode: 'htmlmixed',
		// Whether or not to show the search Code button on the toolbar
		showSearchButton: true,
		// Whether or not to show Trailing Spaces
		showTrailingSpace: true,
		// Whether or not to highlight all matches of current word/selection
		highlightMatches: true,
		// Whether or not to show the format button on the toolbar
		showFormatButton: true,
		// Whether or not to show the comment button on the toolbar
		showCommentButton: true,
		// Whether or not to show the uncomment button on the toolbar
		showUncommentButton: true,
		// Whether or not to show the showAutoCompleteButton button on the toolbar
		showAutoCompleteButton: true,
		// Whether or not to highlight the currently active line
		styleActiveLine: true
	};
};
CKEDITOR.dtd.$removeEmpty['span'] = false;
CKEDITOR.dtd.a.div = 1;