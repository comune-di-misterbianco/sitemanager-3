
var iframeCookies = ["youtube", "recaptcha", "googlemaps"]; // "facebook",

var profiling_cookies_config = {
    "initial_tracking": false,
    "cannot_be_modified": false,
    "checked_by_default": false,
    "description": {
        "en": "<p>This website uses <strong>third-party</strong> services that might set profiling cookies. To learn how third-party providers are processing personal data,"
            + "please refer to their specific privacy policy. These cookies can only be set by <strong>your explicit consent</strong>. Refusing your consent, some parts of this website may be missing. </p>",
        "it": "<p>Questo sito utilizza servizi di <strong>terze parti</strong> che potrebbero usare cookie di profilazione. Per conoscere i trattamenti di dati personali effettuati dalle terze parti consulta "
            + "le rispettive privacy policy. Questi cookie possono essere installati <strong>solamente dietro tuo consenso</strong>. Il blocco di questi cookie potrebbe impedire la visualizzazione di particolari "
            + "contenuti del sito.</p>"
    },
    "title": {
        "en": "Profiling Cookies",
        "it": "Cookie di profilazione"
    },
    "scripts": {
        "googlemaps": {
            "title": {
                "en": "Google Mapx",
                "it": "Mappe di Google",
            }
        },
        "recaptcha": {
            "title": {
                "en": "Recaptcha",
                "it": "Servizio Recaptcha"
            }
        },
        "youtube": {
            "title": {
                "en": "YouTube",
                "it": "YouTube"
            }
        },
    }

};

var analytics_cookies_config = {
    "initial_tracking": false,
    "cannot_be_modified": false,
    "checked_by_default": false,
    "description": {
        "en": "<p>This website uses <strong>analytics</strong> services that might set profiling cookies. </p>",
        "it": "<p>Questo sito utilizza servizi <strong>analytics</strong> per la raccolta di informazione ai fini statici.</p>"
    },
    "title": {
        "en": "Analytics Services",
        "it": "Servizi analytics"
    },
    "scripts": {
        analytics: {
            id: 'UA-xxxxxxxx-1',
            title: { de: 'Google Analytics', en: 'Google Analytics' },
            description: {
                it: `
                    <p>I cookie di Google Analytics vengono utilizzati per migliorare la qualit� del nostro sito Web e dei suoi contenuti e per garantire la funzionalit� dei servizi integrati dei nostri partner.</p>                    
                    `,
                en: `
                    <p>Google Analytics cookies are used to improve the quality of our website and its content and to ensure the functionality of integrated services of our partners.</p>                 
                `
            }
        },

    }
};


let placeholder = '<div class="iframe-placeholder-cc m-auto p-4 border" style="background-color: #eee; width:98%;">' +
    '<p>In base ai settaggi dei cookie non puoi consultare la sezione: � necessario abilitare la scrittura dei cookie terze parti associate al servizio <b>{item-name}</b> ' +
    'dalla sezione <b>Impostazioni Cookie</b> presente nel pi�-pagina del portale.</p></div>';

function check_iframe(cc) {
    document.querySelectorAll('iframe').forEach(function (iframe) {
        var src = iframe.dataset.ccSrc;
        var name = iframe.dataset.ccName;

        placeholder = placeholder.replace('{item-name}', name);

        let iframeSibiling = iframe.previousElementSibling;

        if (src) {
            if (cc.isAccepted(name)) {
                iframe.setAttribute('src', src);
                iframe.removeAttribute('hidden');

                if (iframeSibiling != null && iframeSibiling.className.includes('iframe-placeholder-cc')) {
                    iframeSibiling.setAttribute('hidden', false);
                }

            } else {

                if (iframeSibiling == null) {
                    iframe.insertAdjacentHTML('beforebegin', placeholder);
                }
                else if (iframeSibiling.className.includes('iframe-placeholder-cc')) {
                    iframeSibiling.hidden = false;
                }

                iframe.setAttribute('src', '');
                iframe.setAttribute('hidden', true);
            }
        }
    });

    // handle recaptcha
    if (cc.isAccepted('recaptcha')) {
        // re-enable scripts
        var scripts = document.querySelectorAll('script[data-cc-src*="https://www.google.com/recaptcha/api.js"]');
        if (scripts.length > 0) {
            scripts.forEach(function (script) {
                var scriptSrc = script.dataset.ccSrc;
                var sibling = script.nextElementSibling;
                script.parentNode.removeChild(script);
                var newScript = document.createElement('script');
                newScript.setAttribute('src', scriptSrc);
                newScript.setAttribute('async', '');
                newScript.setAttribute('defer', '');
                sibling.before(newScript);
            });
            document.querySelectorAll('div.iframe-placeholder.recaptcha').forEach(function (placeholder) {
                placeholder.setAttribute('hidden', true);
            });
        }
    }

    // handle analytics
    if (cc.isAccepted('analytics')) {
        cc.load('analytics', 'UA-xxxxxxxx-1');
    }

}

var cookies_settings = [];

var technical_cookies = {
    "initial_tracking": false,
    "cannot_be_modified": true,
    "checked_by_default": true,
    "description": {
        "en": "These cookies ensure a better website navigation and user experience. They do not collect your personal data or your browsing data for marketing and profiling purposes, and therefore can be used without the need to acquire your consent.",
        "it": "Questi cookie consentono la corretta navigazione del sito e la rendono ottimale per ogni utente. Essi non raccolgono i tuoi dati e le tue informazioni di navigazione per scopi di marketing e profilazione, e pertanto possono essere utilizzati senza bisogno di acquisire il tuo consenso."
    },
    "title": {
        "en": "Technical Cookies",
        "it": "Cookie tecnici"
    },
    "scripts": {
        "techcookies": {
            "description": {
                "en": "<p>Technical and functional cookies are placed to provide services or save settings in order to improve the user experience.</p>",
                "it": "<p>I cookie tecnici e di funzionalit\u00e0 vengono utilizzati per fornire servizi o memorizzare impostazioni al fine di garantire una corretta navigazione e migliorare l'esperienza d'uso dell'utente.</p>"
            },
            "title": {
                "en": " Technical and functional Cookies",
                "it": "Cookie tecnici e funzionali"
            }
        }
    }
};

if (Object.keys(technical_cookies).length > 0) {
    cookies_settings.push(technical_cookies);
}

if (Object.keys(analytics_cookies_config).length > 0) {
    cookies_settings.push(analytics_cookies_config);
}

if (Object.keys(profiling_cookies_config).length > 0) {
    cookies_settings.push(profiling_cookies_config);
}

let options = {
    message: {
        "en": "<h2>WE USE COOKIES </h2><p>This website uses <strong>technical cookies</strong> and sometimes also <strong>third-party cookies</strong> that might collect your personal data for profiling purposes. Profiling cookies can be set <strong>only with your explicit consent</strong>by clicking the <strong>Accept all button</strong> or by editing your preferences. By closing the banner by clicking <b>Only tcnical cookies</b> button only technical cookies will be used. </p><p>For more information and to change your preferences, please visit this page <a href=\"/utilit/privacy/\" target=\"_blank\">Privacy policy</a>.</p>",
        "it": "<h2>Usiamo i cookies</h2><p>Questo sito utilizza <strong>cookie tecnici </strong>e talvolta anche <strong>cookie di terze parti</strong> che potrebbero raccogliere i tuoi dati a fini di profilazione; questi ultimi possono essere installati <strong>solo con il tuo consenso esplicito</strong>, tramite il pulsante <strong>Accetta tutto</strong>, oppure modificando le tue preferenze. Selezionando il tasto <b>Solo cookie tecnici</b> verranno registrati solo i cookie tecnici.</p><p>Per maggiori informazioni e per modificare le tue preferenze, puoi consultare la nostra <a href=\"/utilit/privacy/\" target=\"_blank\">Privacy policy</a>.</p>"
    },
    accept_all_if_settings_closed: true,
    show_decline_button: true,
    scripts_selection: "collapse", // false|true|'collapse'
    debug_log: false,
    cookie_prefix: "cconsent_",
    consent_tracking: false,
    expiration: 1, // in days
    //exclude_google_pagespeed: false,
    exclude_ua_regex: /(Speed Insights|Chrome-Lighthouse|PSTS[\d\.]+)/,
    style: {
        layout: "bottombar",
        size: 3,
        color_text: "#4d4d4d",
        color_highlight: "#455972",
        color_background: "#eeeeee",
        highlight_accept: false,
        show_disabled_checkbox: true,
        noscroll: false,
        fade: true,
        blur: true,
        css_replace: '',
        css_add: ''
    },
    labels: {
        "decline": {
            "en": "Technical cookies only",
            "it": "Solo cookie tecnici"
        },
        "group_close": {
            "en": "Close more information",
            "it": "Chiudi altre informazioni"
        },
        "details_close": {
            "en": "Close details",
            "it": "Chiudi dettagli"
        },
        "group_open": {
            "en": "Show more information",
            "it": "Mostra altre informazioni"
        },
        "details_open": {
            "en": "Show details",
            "it": "Mostra dettagli"
        },
        "settings_close": {
            "en": "Close settings",
            "it": "Chiudi impostazioni"
        },
        "close": {
            "en": "Close and accept only technical cookies",
            "it": "Chiudi e accetta solo i cookie tecnici"
        },
        "accept_all": {
            "en": "Allow all",
            "it": "Accetta tutto"
        },
        "accept": {
            "en": "Save and continue",
            "it": "Salva e continua"
        },
        "settings_open": {
            "en": "Customize",
            "it": "Personalizza"
        }
    },
    exclude: [
        () => {
            return (
                document.cookie !== undefined);
        }
    ],
    settings: cookies_settings
};

let cc = null;
function addButtonEvents() {
    let intervalId = setInterval(function () {
        if (cc.isOpen()) {
            const myButtons = document.querySelectorAll('.chefcookie__button--accept, .chefcookie__button--decline');
            myButtons.forEach(button => {
                cc.registerEventListener(button, "click", e => {
                    check_iframe(cc);
                });
            })

            clearInterval(intervalId);
            intervalId = null;
        }
    }, 500);
}
document.addEventListener('DOMContentLoaded', function () {
    cc = new chefcookie(options);
    cc.init();
    check_iframe(cc);

    let cookies = document.cookie.split(';');
    if (!cookies.some(a => a.includes(cc.config.cookie_prefix))) {
        cc.open();
        addButtonEvents();
    }

    const openButtons = document.querySelectorAll('a[data-cc-open="true"]');

    openButtons.forEach(button => {
        button.addEventListener('click', function () {
            addButtonEvents();
        })
    })
});




