﻿function setFormValueByDDL(SourceControlID,TargetControlID,SubmitPage)
{        
    var oInput = document.getElementById(TargetControlID)  
    var oSource = document.getElementById(SourceControlID)  
    
    if(oInput!=null && oSource!=null)
    {
        oInput.value = oSource.options[oSource.selectedIndex].value;
        if(SubmitPage == 1)
            Submit(SourceControlID);
    }
}
function Submit(ControlID)
{    
    if(ControlID && setSubmitSource)
        setSubmitSource(ControlID);
    
    var oForms = document.getElementsByTagName("FORM");
    
    if(oForms)
        oForms[0].submit();

}

function GetBrowserName() {
    var nVer = navigator.appVersion;
    var nAgt = navigator.userAgent;
    var browserName = navigator.appName;
    var fullVersion = '' + parseFloat(navigator.appVersion);
    var majorVersion = parseInt(navigator.appVersion, 10);
    var nameOffset, verOffset, ix;

    //In Internet Explorer la versione reale si trova dopo "MSIE" nell'userAgent
    if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
        browserName = "Microsoft Internet Explorer";
        fullVersion = nAgt.substring(verOffset + 5);
    }
    // In Opera, la versione reale si trova dopo "Opera"
    else if ((verOffset = nAgt.indexOf("Opera")) != -1) {
        browserName = "Opera";
        fullVersion = nAgt.substring(verOffset + 6);
    }
    // In Chrome, la versione reale si trova dopo "Chrome"
    else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
        browserName = "Chrome";
        fullVersion = nAgt.substring(verOffset + 7);
    }
    // In Safari, la versione reale si trova "Safari"
    else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
        browserName = "Safari";
        fullVersion = nAgt.substring(verOffset + 7);
    }
    // In Firefox, la versione reale si trova dopo "Firefox"
    else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
        browserName = "Firefox";
        fullVersion = nAgt.substring(verOffset + 8);
    }
    // Nella maggior parte dei browser, "name/version" è alla fine dell'userAgent
    else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
        browserName = nAgt.substring(nameOffset, verOffset);
        fullVersion = nAgt.substring(verOffset + 1);
        if (browserName.toLowerCase() == browserName.toUpperCase()) {
            browserName = navigator.appName;
        }
    }
    // "taglia" la string fullVersion se è presente uno spazio
    if ((ix = fullVersion.indexOf(";")) != -1) fullVersion = fullVersion.substring(0, ix);
    if ((ix = fullVersion.indexOf(" ")) != -1) fullVersion = fullVersion.substring(0, ix);

    majorVersion = parseInt('' + fullVersion, 10);
    if (isNaN(majorVersion)) {
        fullVersion = '' + parseFloat(navigator.appVersion);
        majorVersion = parseInt(navigator.appVersion, 10);
    }

    //Scrive sullo schermo il nome del browser, la versione completa, la versione principali, il nome del navigatore e l'userAgent del navigatore. Potete cancellare queste righe, servono solo di test
//    document.write('Browser name  = ' + browserName + '<br>');
//    document.write('Full version  = ' + fullVersion + '<br>');
//    document.write('Major version = ' + majorVersion + '<br>');
//    document.write('navigator.appName = ' + navigator.appName + '<br>');
//    document.write('navigator.userAgent = ' + navigator.userAgent + '<br>');

    return browserName;
}

function setFormValue(Value,TargetControlID,SubmitPage,SourceControlID)
{    
    SubmitSourceID = SourceControlID ? SourceControlID : TargetControlID;
    
    oInput = document.getElementById(TargetControlID)  
    
    if(oInput!=null)
    {
        oInput.value = Value;
        if(SubmitPage == 1)
            Submit(SubmitSourceID);
    }
}
    
    
function setFormValueConfirm(Value,TargetControlID,Submit,text,SourceControlID)
{    
   if(!text)
        text = "ATTENZIONE!!Sei sicuro di voler procedere con l'eliminazione?"
   if (confirm(text))
        setFormValue(Value,TargetControlID,Submit,SourceControlID);
}

function SetColCXHeigth()
{      
   var oColSX = document.getElementById("ColSX");
   var oColCX = document.getElementById("ColCX");
   var oColDX = document.getElementById("ColDX");
   
   var MaxHeight = 0;
   
   if(oColSX != null && oColSX.offsetHeight>MaxHeight) MaxHeight = oColSX.offsetHeight;
   if(oColCX != null && oColCX.offsetHeight>MaxHeight) MaxHeight = oColCX.offsetHeight;
   if(oColDX != null && oColDX.offsetHeight>MaxHeight) MaxHeight = oColDX.offsetHeight; 
         
   //Per metterlo sul corpo
   
    var oCorpo = document.getElementById("ColCX");
    var offset = GetBrowserName() == "Chrome" ? 250 : 50;
    oCorpo.style.height = MaxHeight + offset + "px";   
} 



function SetColCXHeigthApp()
{      
   var oColSX_IS = document.getElementById("ColSX_IS");
   var oColCX_Content_IS = document.getElementById("ColCX_Content_IS");

   
   var MaxHeight = 0;
   
   if(oColSX_IS != null && oColSX_IS.offsetHeight>MaxHeight) MaxHeight = oColSX_IS.offsetHeight;
   if(oColCX_Content_IS != null && oColCX_Content_IS.offsetHeight>MaxHeight) MaxHeight = oColCX_Content_IS.offsetHeight;
         
   //Per metterlo sul corpo
   
    var oCorpoApp = document.getElementById("ColCX_Content_IS");
    var offset = GetBrowserName() == "Chrome" ? 250 : 10;
    oCorpoApp.style.height = MaxHeight + offset + "px";   
} 



function SelectSheet(NewSheetID,SelectedSheetControlID,SheetsControlID)
{
    oSheetsContainer = document.getElementById(SheetsControlID+'_SheetsContainer');
    if(oSheetsContainer)
    {
      Sheets = oSheetsContainer.childNodes
      
      var i;
      for (i in Sheets)
      {
           oOldSheetControl = document.getElementById(Sheets[i].id);
           oOldSheetControlLabel = document.getElementById(Sheets[i].id+'_Label');

           if(oOldSheetControl)
           {
              oOldSheetControl.style.display = 'none';
              if(oOldSheetControlLabel && oOldSheetControlLabel.className )
                oOldSheetControlLabel.className  = oOldSheetControlLabel.className.replace(' SheetLabelSelected','');
           }
      } 

      oSelectedSheetControl = document.getElementById(SelectedSheetControlID);
      var oSheetControl = document.getElementById(NewSheetID);
      if(oSheetControl)
      {
          var oSheetControlLabel = document.getElementById(NewSheetID+'_Label');
          oSheetControl.style.display = 'block';
          if(oSheetControlLabel)
            oSheetControlLabel.className = oSheetControlLabel.className  + ' SheetLabelSelected';
          if(oSelectedSheetControl)
            oSelectedSheetControl.value = NewSheetID;
      }
    }
}
function setSubmitSource(ControlID)
{

}

function linkesterno() {
if (!document.getElementsByTagName) return;
var anchors = document.getElementsByTagName("a");
for (var i=0; i<anchors.length; i++) {
 var anchor = anchors[i];
 if (anchor.getAttribute("href") && anchor.getAttribute("rel") == "esterno") {
 anchor.target = "_blank";
 if (anchor.title) anchor.title += " (Sito esterno)";
 if (!anchor.title) anchor.title = "Sito esterno";
 }
}
}
window.onload = linkesterno;

jQuery(document).ready(function(){
$("input").each( function(){
    $(this).keydown(function(e){
        if (e.keyCode == 13) {
            $('form').submit();
            return false;
        }
    });
})
});

function showDivWaithing()
{
    document.getElementById('divWaithingContainer').style.width = document.body.clientWidth;
    document.getElementById('divWaithingContainer').style.height = document.body.clientHeight;
    document.getElementById('inprogress').style.visibility='visible';
}


function CheckTable_All(nometable,idcolumn,value)
{
    oTable = document.getElementById(nometable);
    if(oTable!=null)
    {
        Controls = oTable.getElementsByTagName("INPUT");        
        for(i=0;i<Controls.length;i++)
            if(Controls[i].id.substring(0,idcolumn.length) == idcolumn)
                Controls[i].checked = value;
    }
}

function SelectIndexAllDropDown(nometable,idcolumn,value)
{
    oTable = document.getElementById(nometable);
    if(oTable!=null)
    {
        Controls = oTable.getElementsByTagName("Select");        
        for(i=0;i<Controls.length;i++)
            if(Controls[i].id.substring(0,idcolumn.length) == idcolumn)
                Controls[i].selectedIndex = value;
    }
}

/**********************************************************/
/* show search form on cstampa,rstampa e news */
/**********************************************************/
jQuery(document).ready(function(){
 $('.searchFieldset legend').click(function(){
   var height = $('#ColCX').height();   
   if ($('.searchForm').css('display') == 'block')
	  $('#ColCX').height(height - 200);
   else
	  $('#ColCX').height(height + 200);
   $(this).siblings().slideToggle("slow");   
	});
 });
 

 $(document).ready(function () {                                                        
/*var src = $('#" + div.ID + @"').html();
setTimeout(function () {
$('#" + div.ID + @"').colorbox({
  open: true, 
  width: '500px',
  height: '600px',
  escKey: false,
  scrolling: true,
  html:src
});                                                                                                               
}, 6000);*/
});