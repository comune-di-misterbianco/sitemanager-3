// Login Form
$(document).ready(function () {

    var button = $('#loginButton');
    var box = $('#loginBox');
    // var form = $('#loginForm');
    button.removeAttr('href');
    button.mouseup(function (login) {
        box.toggle();
        button.toggleClass('active');
    });

    box.mouseup(function () {
        return false;
    });
    $(this).mouseup(function (login) {
        if (!($(login.target).parent('#loginButton').length > 0)) {
            button.removeClass('active');
            box.hide();
        }
    });

    var found = box.find('#loginTestLoginControl1');
    var found2 = box.find('#Ask_LoginControl1');
        
    if (found.length > 0 || found2.length > 0) {
        box.toggle();
        button.toggleClass('active');
    }

    // utente loggato
    var userCtrl = $('#userCtrl');
    var userNavBox = $('#userNav');

    userCtrl.mouseup(function (logged) {
        userNavBox.toggle();
        userCtrl.toggleClass('active');
    });
    userNavBox.mouseup(function () {
        return false;
    });

    $(this).mouseup(function (logged) {
        if (!($(logged.target).parent('#userCtrl').length > 0)) {
            userCtrl.removeClass('active');
            userNavBox.hide();
        }
    });
});
