﻿$(document).ready(function () {
    $('.Megamenu-item').each(function (index, elem){
            
            var link_orig = $(elem).find('a').first();
            var link_copy = $('<a/>');
            if (window.location.pathname.indexOf(link_orig.attr('href')) === 0) {
                $(elem).addClass('is-active');
            }

        if ($(elem).find('a').length === 1)
            return;

            link_copy.attr('href', link_orig.attr('href'));
            link_copy.html(link_orig.html());
            var div = $(elem).find('div').first();
            var li = $('<li/>');
            li.append('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
            li.append(link_copy);
            var ul = $('<ul/>', { class: 'Megamenu-subnavGroup Megamenu-pageLink' });
            ul.append(li);
            div.append(ul);

            $(link_orig).click(function (e) {
                e.preventDefault();
            });

        }
    );

    $(".modulo_menu .Treeview li").each(function () {
        if ($(this).hasClass('active')) {

            var item = $(this); // li

            item.parent().find('.active > a').css('font-weight', 'bold');
            item.attr('aria-expanded', 'true');
            item.css('display', 'block');

            if (item.children().length > 0) {
                var ul = item.children('ul');
                ul.attr('aria-hidden', 'false');
                //ul.find('li.active > a').css('font-weight', 'bold');
            }
        }
    });
});