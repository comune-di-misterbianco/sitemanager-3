﻿$(document).ready(function () {

    $('a[rel="external"]').attr('target', '_blank');

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    if ($('.sidebar-menu').length) {
        if (window.attachEvent) {
            window.attachEvent('onresize', function () {
                //console.log('attachEvent - resize');
            });
        }
        else if (window.addEventListener) {
            window.addEventListener('resize', function () {
                //console.log('addEventListener - resize');
                if (window.matchMedia('(max-width: 974px)').matches) {
                    $('.sidebar-menu .Treeview-parent').attr("aria-expanded", false);
                    $('.sidebar-menu .Treeview-parent').find('ul').css('display', 'none');
                }
                else {
                    $('.sidebar-menu .Treeview-parent').attr("aria-expanded", true);
                    $('.sidebar-menu .Treeview-parent').find('ul').css('display', 'block');
                }
            }, true);
        }     
    }

    /* Toolbar Side Button*/
    $('#ToolbarSxButton').click(function () {
        var $this = $(this);
        if (!$this.hasClass('sxToolbarButton')) {
            $this.addClass('sxToolbarButton');
            $this.find('i').removeClass('glyphicon-chevron-left').addClass('glyphicon-chevron-right');
            $('#SideContentVapp').removeClass('visible-xs').addClass('hidden-xs');
            $('#SideContentVapp').removeClass('visible-sm').addClass('hidden-sm');
        }
        else {
            $this.removeClass('sxToolbarButton');
            $this.find('i').removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-left');
            $('#SideContentVapp').addClass('visible-xs').removeClass('hidden-xs');
            $('#SideContentVapp').addClass('visible-sm').removeClass('hidden-sm');
        }
    });

    /* content share*/
    $("#share-options").hide();
    $(".Share").on("click", ".Share-revealText, .Share-hideText", function (a) {
        a.preventDefault();
        $(".Share-revealText, #share-options").toggle()
    });

    $('#loginButton').click(function () {
        $('#LoginModal').modal('show');
    });

    //var found = $('#LoginModal').find("div.NotifyArea");
    //var n = $("#LoginModal div.NotifyArea").has("ul");
    //if (n > 0) {
    //    $('#LoginModal').modal('show');
    //}

    var recovery = $("#LoginModal").find("#Ask_LoginControl1");
    if ($("#LoginModal div.NotifyArea").has("ul").length || recovery.length > 0) {
        $('#LoginModal').modal('show');
    } else {
        $('#LoginModal').modal('hide');
    }
});


$(function () {
    $('.table').footable();
});

$(function () {

    /* iterate through nested list */

    $('.AT-navmenu ul').each(function () {
        var listItem = $(this).closest('li');
        var itemLink = listItem.find('> a');
        var title = 'Espandi il menu ' + $.trim(itemLink.text());
        var trigger = $('<span></span>').attr({
            tabindex: 0,
            'aria-label': title,
            'title': title,
        });

        $(listItem).find('span')
            .add(trigger)
            .on('click', function (ev) {
                var node = $(this).is('a') ? $(this) : $(this).closest('a');

                /* Clear all open and not nested element  */
                if (!$(this).closest('li.open').length) {
                    $('.AT-navmenu ul').slideUp(300);
                    $('.AT-navmenu li').removeClass('open');
                }

                if (node.siblings('ul').is(':visible')) {
                    node.siblings('ul').slideUp(300);
                    node.parent().removeClass('open');
                } else {
                    node.siblings('ul').slideDown(300);
                    node.parent().addClass('open');
                }
                return false;
            })
            .on('keydown', function (e) {
                if (e.which === 13 || e.which === 32) {
                    $(this).click();
                    return false;
                }
            });

        itemLink.append(trigger);
    });

    /* Get current element and set their ancestors as active */

    $('.AT-navmenu a[href=\'' + window.location.pathname + '\'], ' +
        '.AT-navmenu a[href=\'' + window.location.pathname.slice(0, -1) + '\']')
        .addClass('current');

    $('.AT-navmenu .current').parents().filter(function (index) {
        return (this.nodeName === 'LI');
    }).addClass('active open');


});