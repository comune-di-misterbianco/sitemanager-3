/* FOOTABLE*/

$(function () {
		$('.table').footable();
	});
	
$(function () {

    /* iterate through nested list */

    $('.AT-navmenu ul').each(function () {
        var listItem = $(this).closest('li');
        var itemLink = listItem.find('> a');
        var title = 'Espandi il menu ' + $.trim(itemLink.text());
        var trigger = $('<span></span>').attr({
            tabindex: 0,
            'aria-label': title,
            'title': title,
        });

        $(listItem).find('span')
          .add(trigger)
          .on('click', function (ev) {
              var node = $(this).is('a') ? $(this) : $(this).closest('a');

              /* Clear all open and not nested element  */
              if (!$(this).closest('li.open').length) {
                  $('.AT-navmenu ul').slideUp(300);
                  $('.AT-navmenu li').removeClass('open');
              }

              if (node.siblings('ul').is(':visible')) {
                  node.siblings('ul').slideUp(300);
                  node.parent().removeClass('open');
              } else {
                  node.siblings('ul').slideDown(300);
                  node.parent().addClass('open');
              }
              return false;
          })
          .on('keydown', function (e) {
              if (e.which === 13 || e.which === 32) {
                  $(this).click();
                  return false;
              }
          });

        itemLink.append(trigger);
    });

    /* Get current element and set their ancestors as active */

    $('.AT-navmenu a[href=\'' + window.location.pathname + '\'], ' +
        '.AT-navmenu a[href=\'' + window.location.pathname.slice(0, -1) + '\']')
      .addClass('current');

    $('.AT-navmenu .current').parents().filter(function (index) {
        return (this.nodeName === 'LI');
    }).addClass('active open');

  
});

/* Fix issues with <base> tag 
$('[href^=*]').on('click', function (e) {
        document.location.hash = $(this).attr('href');
        e.preventDefault();
    });

$(document).ready(function () {
    $("#AtMenuAccordion > li").click(function () {
        if (false == $("ul", this).is(':visible')) {
            $('#AtMenuAccordion ul').slideUp(300);
        }
        $("ul", this).slideToggle(300);
    });
    //$('#accordion ul:eq(0)').show();
});*/

$(document).ready(function () {

    

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});


$(document).ready(function () {
    $('#loginButton').click(function () {
        $('#LoginModal').modal('show');
    });

    //var found = $('#LoginModal').find("div.NotifyArea");
    //var n = $("#LoginModal div.NotifyArea").has("ul");
    //if (n > 0) {
    //    $('#LoginModal').modal('show');
    //}

    var recovery = $("#LoginModal").find("#Ask_LoginControl1");
    if ($("#LoginModal div.NotifyArea").has("ul").length || recovery.length > 0) {
        $('#LoginModal').modal('show');
    } else {
        $('#LoginModal').modal('hide');
    }

});


$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});


$(document).ready(function () {
    $('[data-toggle="popover"]').popover();
});

$(document).ready(function () {

	/*$(document).on('click', 'span.clickableSX', function(e){
                                var $this = $(this);
                                if(!$this.hasClass('coll')) {
                                        $this.addClass('coll');
                                    	$this.find('i').removeClass('glyphicon-chevron-left').addClass('glyphicon-chevron-right');
										document.getElementById('ColSX').style.cssText = 'right: 100%;';
										
                                }
                                else
                                {
                                        $this.removeClass('coll');
                                        $this.find('i').removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-left');
										document.getElementById('ColSX').style.cssText = 'right: 0%; left:0%; ';
										

                                }
                                });*/


    /* SideButton ColSX */
	$('#colSxButton').click(function() {
								
								var $this = $(this);
                                if(!$this.hasClass('coll')) {
                                        $this.addClass('coll');
                                    	$this.find('i').removeClass('glyphicon-chevron-left').addClass('glyphicon-chevron-right');
										//document.getElementById('ColSX').style.cssText = 'right: 200%;';
                                    	$('#ColSX').removeClass('visible-xs').addClass('hidden-xs');
                                    	$('#ColSX').removeClass('visible-sm').addClass('hidden-sm');
										//$('#ColSX').removeClass('hidden-xs');
										
                                }
                                else
                                {
                                        $this.removeClass('coll');
                                        $this.find('i').removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-left');
										//document.getElementById('ColSX').style.cssText = 'right: 0%; left:0%; ';
                                        $('#ColSX').addClass('visible-xs').removeClass('hidden-xs');
                                        $('#ColSX').addClass('visible-sm').removeClass('hidden-sm');
										//$('#ColSX').removeClass('hidden-xs');
										
										

                                }
		
		
	});


	$(window).scroll(function () {
	    if (!$("#colSxButton").hasClass('coll')) {

	        if (($(document).scrollTop() > $("#ColSX").height())) {

	            $('#colSxButton').addClass('hidden-xs');
	            $('#colSxButton').removeClass('visible-xs');
	            $('#colSxButton').addClass('hidden-sm');
	            $('#colSxButton').removeClass('visible-sm');
	            /*$('#colSxButton').css({'display': 'none'});*/
	        }
	        else
	        {
	            $('#colSxButton').addClass('visible-xs');
	            $('#colSxButton').removeClass('hidden-xs');
	            $('#colSxButton').addClass('visible-sm');
	            $('#colSxButton').removeClass('hidden-sm');
	        }
	    }
	});


    /* Toolbar Side Button*/
	$('#ToolbarSxButton').click(function () {
	    var $this = $(this);
	    if (!$this.hasClass('sxToolbarButton'))
	    {
	        $this.addClass('sxToolbarButton');
	        $this.find('i').removeClass('glyphicon-chevron-left').addClass('glyphicon-chevron-right');
	        $('#SideContentVapp').removeClass('visible-xs').addClass('hidden-xs');
	        $('#SideContentVapp').removeClass('visible-sm').addClass('hidden-sm');
	    }
	    else {
	        $this.removeClass('sxToolbarButton');
	        $this.find('i').removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-left');
	        $('#SideContentVapp').addClass('visible-xs').removeClass('hidden-xs');
	        $('#SideContentVapp').addClass('visible-sm').removeClass('hidden-sm');
        }
	});


	$(window).scroll(function () {
	    if (!$("#ToolbarSxButton").hasClass('sxToolbarButton')) {
	        if (($(document).scrollTop() > $("#SideContentVapp").height())) {
	            $('#ToolbarSxButton').addClass('hidden-xs');
	            $('#ToolbarSxButton').removeClass('visible-xs');
	            $('#ToolbarSxButton').addClass('hidden-sm');
	            $('#ToolbarSxButton').removeClass('visible-sm');
	        }
	        else {
	            $('#ToolbarSxButton').addClass('visible-xs');
	            $('#ToolbarSxButton').removeClass('hidden-xs');
	            $('#ToolbarSxButton').addClass('visible-sm');
	            $('#ToolbarSxButton').removeClass('hidden-sm');
	        }
	    }
	});




    /* Language List */
	var languageList = $('.languageList');
	var a = $('span.arrowSwitch');

	languageList.mouseup(function(){
		
		return false;
	
	});
	
	$(this).mouseup(function(e){
		
		if (!a.is(e.target) // if the target of the click isn't the container...
			&& a.has(e.target).length === 0) // ... nor a descendant of the container
		{
			languageList.hide();
		}
		else
		{
			languageList.toggle();
		}
	});
});



//$(document).ready(function () {
//if($(".mainpage").length) {
//    $("#ColSX").hide();
//    $("#ColCX").removeClass("col-md-10");
//    $("#ColCX").addClass("col-md-12");
//}
//});