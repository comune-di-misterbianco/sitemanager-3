﻿<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="Accordion Accordion--default fr-accordion js-fr-accordion" id="accordion-1">
           
                <h2 class="Accordion-header js-fr-accordion__header fr-accordion__header" id="accordion-header-{{document.id}}">
                    <span class="Accordion-link">{{document.domanda}}</span>
                </h2>
                <div id="accordion-panel-{{document.id}}" class="Accordion-panel fr-accordion__panel js-fr-accordion__panel">
                    <p class="u-color-grey-90 u-text-p u-padding-r-all">{{document.risposta | html.strip}}</p>
                </div>
                
            </div>
        </div>
    </div>

</div>