﻿<section class="u-padding-r-all">
    <div class="u-layout-medium u-layoutCenter">

        <div class="Grid">
            <h2 id="carousel-heading" class="Grid-cell u-text-h2 u-color-white u-layout-centerLeft"><span class="u-text-r-l Icon Icon-file"></span>{{ data.modulo.label }}</h2>

            <!-- <next / prev buttons> -->
            <div class="Grid-cell u-layout-centerRight">
                <button type="button" class="owl-prev u-padding-bottom-xl u-padding-right-xxl u-text-r-xl u-color-teal-50" aria-controls="carousel-1">
                    <span class="u-hiddenVisually">Vai alla slide precedente</span>
                    <span class="u-alignMiddle Icon Icon-arrow-left" role="presentation"></span>
                </button>
                <button type="button" class="owl-next u-padding-bottom-xl u-padding-left u-text-r-xl u-color-teal-50" aria-controls="carousel-1">
                    <span class="u-hiddenVisually">Vai alla slide successiva</span>
                    <span class="u-alignMiddle Icon Icon-arrow-right" role="presentation"></span>
                </button>
                <p class="u-hiddenVisually">È possibile navigare le slide utilizzando i tasti freccia</p>
            </div>
            <!-- </next / prev buttons> -->
        </div>

        <div class="owl-carousel owl-theme" role="region" id="carousel-1">
            {{if data.items.size > 0 }}
            {{for item in data.items }}
            <div class="Carousel-item">
                <div class="u-color-grey-30">
                    {{if item.img != false }}
                    <figure>
                        <a href="#img-1" aria-labelledby="desc-1" class="u-block u-padding-all-xxs">
                            <img src="{{item.img.imgsrc}}" class="u-sizeFull" alt="{{item.img.imgalt}}" />
                        </a>
                        <figcaption class="u-padding-r-top">
                            {{if item.img.imgname != false}}<p class="u-color-teal-50 u-text-r-xxs u-textWeight-700 u-padding-bottom-s">{{item.img.imgname}}</p>{{end }}
                            <div class="Grid">
                                <span class="Grid-cell u-sizeFit Icon-camera u-color-white u-floatLeft u-text-r-l" aria-hidden="true"></span>
                                <h3 id="desc-1" class="Grid-cell u-sizeFill u-padding-left-s u-lineHeight-l u-color-white u-text-r-xs u-textWeight-700">
                                    {{item.img.imgalt}}
                                </h3>
                            </div>
                        </figcaption>
                    </figure>
                    {{end }}
                    {{if item.titolo != false}}
                    <div class="u-text-r-l u-padding-r-all u-layout-prose">
                        <h3 class="u-text-h4 u-margin-r-bottom">
                            <a class="u-textClean u-color-black u-textWeight-400 u-text-r-m" href="{{item.itemurl}}">
                                {{item.titolo}}
                            </a>
                        </h3>
                    </div>
                    {{end}}
                </div>

            </div>
            {{end}}
            {{end}}
        </div>
        {{ if data.modulo.itemshowmore != false}}
        <p class="u-padding-r-top u-text-r-xl">
            <a href="{{data.modulo.url}}" class="u-layout-centerLeft u-padding-r-top u-text-h4 u-textWeight-700 u-color-teal-50">{{data.modulo.showmorelabel}}</a>
        </p>
        {{end }}
    </div>
</section>

