﻿<section class="Grid">
    <div class="Grid-cell u-sizeFull u-md-size1of2 u-lg-size1of2 u-text-r-s u-padding-r-all">        
        <div class="u-text-r-l u-layout-prose">
            {{if data.modulo.titolo != "false"}}
            <h2 class="u-text-h2 u-margin-r-bottom">
                {{if data.modulo.url != "false" }}
                <a class="u-text-h2 u-textClean u-color-black" href="{{data.modulo.url}}">
                    {{data.modulo.titolo}}
                </a>
                {{else}}
                {{data.modulo.titolo}}
                {{end}}
            </h2>
            {{end}}
            <p class="u-textSecondary u-lineHeight-l">
                {{data.modulo.testo}}
            </p>
        </div>
    </div>
    {{if data.modulo.img.imgsrc != "false" }}
    <div class="Grid-cell u-sizeFull u-md-size1of2 u-lg-size1of2 u-text-r-s u-padding-r-all">
        <figure>
            <img src="{{data.modulo.img.imgsrc}}" class="u-sizeFull" alt="{{data.modulo.img.imgalt}}" />
        </figure>
    </div>
    {{end}}
</section>
