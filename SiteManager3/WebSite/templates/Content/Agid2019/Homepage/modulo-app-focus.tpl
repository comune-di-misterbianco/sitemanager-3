﻿<div class=" u-background-compl-10 u-layout-centerContent u-padding-r-top">

    <section class="u-layout-wide u-layout-r-withGutter u-text-r-s u-padding-r-top u-padding-r-bottom">

        <h2 id="news" class="u-layout-centerLeft u-text-r-s">
            <a class="u-color-50 u-textClean u-text-h3 " href="{{data.modulo.url}}"><span class="u-text-r-l Icon Icon-file"></span> {{ data.modulo.label }}</a>
        </h2>

        <div class="Grid Grid--withGutterM">
            {{if data.items.size > 0 }}
            {{for item in data.items  limit:3}}
            <div class="Grid-cell u-md-size1of3 u-lg-size1of3 u-flex u-margin-r-bottom u-flexJustifyCenter">
                <div class="u-nbfc u-borderShadow-xxs u-borderRadius-m u-color-grey-30 u-background-white">
                    {{if data.modulo.showimages && item.img != false }}
                    <img src="{{item.img.imgsrc}}" class="u-sizeFull" alt="{{item.img.imgalt}}" />
                    {{end }}

                    <div class="u-text-r-l u-padding-r-all u-layout-prose">
                        <p class="u-text-h6 u-margin-bottom-l"><a class="u-color-50 u-textClean" href="{{item.itemcategoryurl}}">{{item.categoria | string.capitalizewords}}</a></p>
                        <h3 class="u-text-h4 u-margin-r-bottom"><a class="u-text-r-m u-color-black u-textWeight-400 u-textClean" href="{{item.itemurl}}">{{item.titolo}}</a></h3>
                        <p class="u-text-p u-textSecondary">{{item.descrizione}}</p>
                    </div>
                </div>

            </div>
            {{end}}
            {{end}}
        </div>
        {{if data.modulo.itemshowmore != false }}
        <p class="u-textCenter u-text-md-right u-text-lg-right u-margin-r-top">
            <a href="{{data.modulo.url}}" class="u-color-50 u-textClean u-text-h4">
                {{data.modulo.showmorelabel}} <span class="Icon Icon-chevron-right" />
            </a>
        </p>
        {{end}}
    </section>

</div>