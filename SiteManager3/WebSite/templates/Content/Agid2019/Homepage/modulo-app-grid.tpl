﻿<div class="u-layout-centerContent u-background-grey-20 u-padding-r-bottom">
    <section class="u-layout-wide">
        <h2 class="u-padding-r-bottom u-padding-r-top u-text-r-l"><span class="u-text-r-l Icon Icon-file"></span> {{ data.modulo.label }}</h2>

        <div class="Grid Grid--withGutter">
            {{if data.items.size > 0 }}
            {{for item in data.items }}
            {{if for.first}}
            <div class="Grid-cell u-nbfc u-md-size3of4 u-lg-size3of4">
                <div class="Grid u-layout-centerContent">
                    {{if data.modulo.showimages && item.img != false }}
                    <img src="{{item.img.imgsrc}}" class="u-sizeFill" alt="{{item.img.imgalt}}" />
                    {{end }}
                    <div class="Grid-cell u-md-sizeFill u-lg-sizeFill u-padding-r-left">
                        <div class="u-color-grey-30 u-border-top-xxs u-padding-right-xxl u-padding-r-all">
                            <p class="u-padding-r-bottom">
                                <span class="Dot u-background-70"></span>                                
                                <a class="u-textClean u-textWeight-700 u-text-r-xs u-color-50" href="{{item.itemcategoryurl}}">{{item.categoria | string.capitalizewords}}</a>
                                <span class="u-text-r-xxs u-textSecondary u-textWeight-400 u-lineHeight-xl u-cf">{{item.date}}</span>
                            </p>
                            <h3 class="u-padding-r-top u-padding-r-bottom">
                                <a class="u-text-h4 u-textClean u-color-black" href="{{item.itemurl}}">
                                    {{item.titolo}}
                                </a>
                            </h3>
                            <p class="u-lineHeight-l u-text-r-xs u-textSecondary u-padding-r-right">
                                {{item.descrizione}}
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            {{else}}
            <div class="Grid-cell u-md-size1of4 u-lg-size1of4 u-padding-r-left">
                <div class="u-color-grey-30 u-border-top-xxs u-padding-right-xxl u-padding-r-all">
                    <p class="u-padding-r-bottom">
                        <span class="Dot u-background-70"></span>
                        <a class="u-textClean u-textWeight-700 u-text-r-xs u-color-50" href="{{item.itemcategoryurl}}">{{item.categoria}}</a>
                        <span class="u-text-r-xxs u-textSecondary u-textWeight-400 u-lineHeight-xl u-cf">{{item.date}}</span>
                    </p>
                    <h3 class="u-padding-r-top u-padding-r-bottom">
                        <a class="u-text-h4 u-textClean u-color-black" href="{{item.itemurl}}">
                            {{item.titolo}}
                        </a>
                    </h3>
                    <p class="u-lineHeight-l u-text-r-xs u-textSecondary u-padding-r-right">
                        {{item.descrizione}}
                    </p>
                </div>

            </div>
            {{end}}

            {{end }}
            {{end }}
        </div>
        {{if data.modulo.itemshowmore != false }}
        <p class="u-textCenter u-text-md-right u-text-lg-right u-padding-r-top">
            <a href="{{data.modulo.url}}" class="u-color-50 u-textClean u-text-h4">
                {{data.modulo.showmorelabel}} <span class="Icon Icon-chevron-right"></span>
            </a>
        </p>
        {{end }}
    </section>
</div>

