﻿<div class="modulo-list">
    <div class="modulo-heading">
        <h2>{{ data.modulo.label }}</h2>
    </div>
    {{if data.items.size > 0 }}

    <div class="modulo-content">
        <ul class="media-list">
            {{for item in data.items }}
            <li class="media">
                <div class="media-body post-holder">
                    <span class="date"><small>{{item.date}}</small></span>
                    <h4><a href="{{item.itemurl}}" title="Consulta {{item.titolo}}"> {{item.titolo}}</a></h4>
                    {{if data.modulo.showimages  && item.img != false }}
                    <div class="modulo-img {{if item.img.imgalign != "false"; item.img.imgalign; end}}">
                        <img src="{{item.img.imgsrc}}" class="u-sizeFill" alt="{{item.img.imgalt}}" />
                    </div>
                    {{end }}
                    {{ if data.modulo.itemshowdesc != "false"}}
                    <p class="descrizione"> {{item.descrizione}}</p>
                    {{end }}
                </div>
</li>
            {{end }}
        </ul>
    </div>

    {{end }}
    {{ if data.modulo.itemshowmore != false}}
    <div class="modulo-footer">
        <a href="{{data.modulo.url}}" title="Accedi a tutti i contenuti della sezione {{ data.modulo.label }}">Tutti i contenuti</a>
    </div>
    {{end }}
</div>