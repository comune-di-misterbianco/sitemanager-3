﻿<section class="Grid Grid--withGutter u-padding-all-m">
    <div class="Grid-cell">
        <h3 class="SectionSubTitle">{{ data.modulo.label }}</h3>

        {{if data.items.size > 0 }}

        <div class="modulo-content">
           
                {{for item in data.items }}
                <div class="item u-border-bottom-xxs u-color-grey-30">
                    <div class="media-body post-holder">
                        <p class="u-color-black u-text-r-xxs"><span class="u-padding-right-xxs Icon Icon-calendar"></span>{{item.date}}</p>
                        <h4 class="item-title u-color-black"><a href="{{item.itemurl}}" title="Consulta {{item.titolo}}"> {{item.titolo}}</a></h4>
                        {{ if data.modulo.itemshowdesc != "false"}}
                        <p class="u-color-black descrizione"> {{item.descrizione}}</p>
                        {{end }}
                    </div>
                </div>
                {{end }}
         
        </div>
        {{else}}
        <p class="u-textCenter">{{labels.msgmodulobandinorecord}}</p>
        {{end }}

        {{ if data.modulo.itemshowmore != false}}
        <div class="SectionRow-linkall u-background-80 u-borderRadius-m u-margin-top-m">
            <p class="u-text-r-xxs u-padding-all-xs u-textCenter"><a class="u-color-white" href="{{data.modulo.url}}" title="Accedi a tutti i contenuti della sezione {{ data.modulo.label }}">Tutti i contenuti</a></p>
        </div>
        {{end }}

    </div>
</section>
