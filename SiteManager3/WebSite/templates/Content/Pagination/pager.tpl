﻿<nav aria-label="Page navigation">

    <ul class="pagination">

        {{ if pager.current > 1}}
        <li>
            <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current-1}}" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>
        {{end }}

        {{ for i in 1..pager.pages }}

        {{
                            cssclass = ""
                            if  i == pager.current
                                cssclass = "active"
                            end
        }}

        <li class="{{cssclass}}">
            <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{i}}">
                {{i}}
            </a>
        </li>
        {{end }}
    </ul>
</nav>