﻿<!-- template box -->
<div class="{{if data.modulo.cssclass != "false"; data.modulo.cssclass; end }}">
    <div class="single">
        {{if data.modulo.img.imgsrc != "false" }}
        <div class="img-box">
            <a href="{{data.modulo.urltitle}}" title="">
                <figure>
                    <img src="{{data.modulo.img.imgsrc}}" alt="{{data.modulo.img.imgalt}}" />
                </figure>
            </a>
        </div>
        {{end}}
        <div class="conten">
            <div class="icon"><span class="u-text-r-l Icon Icon-settings"></span></div>
            {{if data.modulo.titolo != "false"}}
            <h3><a href="{{data.modulo.url}}" title="{{data.modulo.urltitle}}"> {{data.modulo.titolo }}</a></h3>
            {{end}}

            <div class="text"><p>{{data.modulo.testo}}</p></div>

            <div class="button">
                <a class="button" href="{{data.modulo.url}}" title="{{data.modulo.urltitle}}">Approfondisci</a>
            </div>
        </div>
    </div>
</div>