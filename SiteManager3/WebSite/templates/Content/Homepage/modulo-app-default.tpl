﻿<div class="u-layout-centerContent">
    <section class="u-layout-wide u-padding-top-xxl u-padding-bottom-xxl">
        <h2 class="u-text-r-l u-padding-r-bottom"><span class="u-text-r-l Icon Icon-file"></span> {{ data.modulo.label }}</h2>

        {{if data.items.size > 0 }}
        {{
           itemscount = data.items.size
           k = 1
           i = 1
        }}
        {{for item in data.items }}
        {{
             if i == 5
                i = 1
             end
        }}
        {{if i == 1}}
        <div class="Grid Grid--withGutter">
            {{end}}
            <div class="Grid-cell u-md-size1of4 u-lg-size1of4">
                <div class="u-color-grey-30 u-border-top-xxs u-padding-right-xxl u-padding-r-all">
                    <p class="u-padding-r-bottom">
                        <a class="u-textClean u-textWeight-700 u-text-r-xs u-color-50" href="{{item.itemcategoryurl}}">{{item.categoria | string.capitalizewords}}</a>
                        <span class="u-text-r-xxs u-textSecondary u-textWeight-400 u-lineHeight-xl u-cf">{{item.date}}</span>
                    </p>
                    <h3 class="u-padding-r-top u-padding-r-bottom">
                        <a class="u-text-h4 u-textClean u-color-black" href="{{item.itemurl}}">
                            {{item.titolo}}
                        </a>
                    </h3>
                    <p class="u-lineHeight-l u-text-r-xs u-textSecondary u-padding-r-right">
                        {{item.descrizione}}
                    </p>
                </div>
            </div>
            {{if i == 4 || k == itemscount}}
        </div>
        {{end}}
        {{
            i = i + 1
            k = k + 1
        }}
        {{end}}
        {{end }}

        {{if data.modulo.itemshowmore != false }}
        <p class="u-textCenter u-text-md-right u-text-lg-right u-padding-r-top">
            <a href="{{data.modulo.url}}" class="u-color-50 u-textClean u-text-h4">
                {{data.modulo.showmorelabel}} <span class="Icon Icon-chevron-right" />
            </a>
        </p>
        {{end}}
    </section>
</div>

