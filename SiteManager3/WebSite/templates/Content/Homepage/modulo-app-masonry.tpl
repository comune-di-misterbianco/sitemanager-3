﻿
<div class="u-layout-centerContent u-cf">

    <h2 class="u-text-h2 u-color-50 u-layout-medium"><span class="u-text-r-l Icon Icon-file"></span> {{ data.modulo.label }}</h2>

    <section class="js-Masonry-container u-layout-medium" data-columns>
        {{if data.items.size > 0 }}
        {{for item in data.items }}
        <div class="Masonry-item js-Masonry-item">
            <div class="u-nbfc u-borderShadow-xxs u-borderRadius-m u-color-grey-30 u-background-white">
                {{if data.modulo.showimages && item.img != false}}
                <img src="{{item.img.imgsrc}}" class="u-sizeFull" alt="{{item.img.imgalt}}" />
                {{end }}

                <div class="u-text-r-l u-padding-r-all u-layout-prose">
                    <p class="u-text-h6 u-margin-bottom-l"><a class="u-color-50 u-textClean" href="{{item.itemcategoryurl}}">{{item.categoria | string.capitalizewords}}</a></p>
                    <h3 class="u-text-h4 u-margin-r-bottom"><a class="u-text-r-m u-color-black u-textWeight-400 u-textClean" href="{{item.itemurl}}">{{item.titolo}}</a></h3>
                    <p class="u-text-p u-textSecondary">{{item.descrizione}}</p>
                </div>
            </div>
        </div>
        {{end }}
        {{end }}


    </section>
    
</div>

{{if data.modulo.itemshowmore != false }}
    <div class="u-layout-centerContent u-cf">
        <div class="u-layout-wide u-layout-centerContent ui-helper-clearfix ">
            <p class="u-textCenter u-text-md-right u-text-lg-right u-padding-r-top">
                <a href="{{data.modulo.url}}" class="u-color-50 u-textClean u-text-h4">{{data.modulo.showmorelabel}}<span class="Icon Icon-chevron-right"></span></a>
            </p>
        </div>
    </div>
{{end }}
