﻿<div class="{{if data.modulo.cssclass != "false"; data.modulo.cssclass; end }}">
    {{if data.modulo.titolo != "false"}}
    <h3>
        {{if data.modulo.url != "false" }}
        <a href="{{data.modulo.url}}">{{data.modulo.titolo}}</a>
        {{else}}
        {{data.modulo.titolo }}
        {{end}}
    </h3>
    {{end}}
    {{if data.modulo.showimages && data.modulo.img.imgsrc != "false" }}

    <div class="modulo-img {{if data.modulo.img.imgalign != "false"; data.modulo.img.imgalign; end}}">
        <img class="img-responsive" src="{{data.modulo.img.imgsrc}}" alt="{{data.modulo.img.imgalt}}" />
    </div>
    {{end}}
    <p>{{data.modulo.testo}}</p>
</div>

