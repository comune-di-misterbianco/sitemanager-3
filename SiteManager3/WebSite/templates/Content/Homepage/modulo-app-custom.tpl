﻿<section class="Hero-customize">
<div class="Hero">
        <div class="Hero-content">
    {{if data.items.size > 0}}
    {{for item in data.items limit:1 }}
            <p class="u-padding-r-bottom u-padding-r-top u-text-r-xs">
                <span class="Dot u-background-60"></span> <a class="u-textClean u-color-60 u-text-h4 category" href="{{item.itemcategoryurl}}">{{item.categoria}}</a>
            </p>
            <h2 class="u-text-h2">
                <a class="u-color-95 u-textClean" href="{{item.itemurl}}">
                    {{item.titolo}}
                </a>
            </h2>
            <p class="u-padding-r-bottom u-padding-r-top u-text-p u-margin-r-bottom">
                {{item.descrizione}}
            </p>      
    {{if data.modulo.showimages && item.img != false}}
    <div class="Grid-cell u-sizeFull u-md-size1of2 u-lg-size1of2 u-text-r-s u-padding-r-all">
        <img src="{{item.img.imgsrc}}" class="u-sizeFull" alt="..." />
    </div>
    {{end}}
    {{end}}
    {{end }}
	</div>
</div>
</section>