﻿<div class="container">
    <article id="content">    
        <div class="news-body col-md-9">
            <header>
                <h1 class="title">{{document.titolo}}</h1>               
				<div class="row">
                    <div class="date col-md-10">
                        <i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;{{commonlabels.publishedon}}: {{document.datapub}} - {{ if document.datarev != "false"}} {{commonlabels.lastrevision}}: {{document.datarev}} {{end}}
                    </div>
                    <div class="print pull-right col-md-2 hidden-xs hidden-sm">
                        <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                        <a href="javascript:this.print();" title="{{commonlabels.printtitle}}">{{commonlabels.print}}</a>
                    </div>
                </div>                        
            </header>
			<div class="u-text-r-s u-padding-r-top">		
			
				<div class="info_evento">										
					<p>
					<i class="fa fa-clock-o" aria-hidden="true"></i>
					{{ if document.al == "null" }}
					{{labels.dataevento}}: {{ document.dal }}
					{{else}}
					{{labels.periodo}}: dal {{ document.dal }} al {{ document.al }}
					{{end}}
					</p>
					<p><i class="fa fa-sticky-note" aria-hidden="true"></i>
					{{labels.categoria}}: <a href="default.aspx?cat={{document.idcategoria}}">{{document.categoria}}</a></p>
				</div>

				<div class="luogo_evento">
					<address>					
					<i class="fa fa-map-marker" aria-hidden="true"></i>
					<strong> {{document.luogo.nome}} </strong>
					{{document.luogo.indirizzo}}, {{document.luogo.citta}}
					</address>
				</div>


				{{if document.locandina != "null" }}
				<div class="locandina mb-3">
					<img src="{{document.locandina.src}}" class="u-sizeFull" alt="{{document.locandina.descrizione}}" />
				</div>
				{{end}}

			

				<div class="news-text">
					{{document.testo}}
				</div>

				{{if document.tags | object.size > 0}}
				<div class="doc-tags">
					<i class="fa fa-tags" aria-hidden="true"></i>
					<span>{{commonlabels.tagstitle}}:</span>
					{{ for tag in document.tags }}
					<a class="doc-tag" rel="tag" href="/tags/argomenti.aspx?tag={{tag.key}}">{{tag.label}}</a>
					{{end}}
				</div>
				{{end}}
			</div>
        </div>

        <div class="news-column col-md-3">
            <div class="share text-right">
                         {{sharecontent}}
            </div>     

            <div class="column-content">

            </div>
        </div>
    </article>
</div>