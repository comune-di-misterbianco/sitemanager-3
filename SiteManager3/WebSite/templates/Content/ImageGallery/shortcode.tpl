﻿<div class="u-layout-centerContent u-padding-bottom-xxl u-padding-top-xxl">
    <section class="u-layout-medium">       
        <div class="Grid Grid--withGutter u-padding-r-top u-text-r-xxl" id="GalleryTable">		
           {{if items | object.size > 0}}
		   {{ 		   
		   itemlimit = 9
		   if section.itemlimit != -1 
				itemlimit = section.itemlimit
		   end		   
		   }}
            {{for item in items  limit:itemlimit}}
            <div class="Grid-cell usizefull u-md-size1of3 u-lg-size1of3 u-text-r-m u-margin-r-bottom u-layout-matchHeight" >
                <section class="u-nbfc u-borderShadow-xxs u-borderRadius-m u-color-grey-30 u-background-white">
                    {{if item.url != false }}
                    <figure class="u-background-grey-60 u-padding-all-s">

                        <a href="{{item.url}}/{{item.titolo}}" class="u-borderFocus u-block u-padding-all-xxs img-group" data-toggle="lightbox" data-gallery="#GalleryTable">
                            <img src="{{item.url}}/{{item.content}}" class="u-sizeFull" alt="{{item.titolo}}" />
                        </a>

                        <figcaption class="u-padding-r-top">
                            <span class="Icon Icon-camera u-color-white u-floatRight u-text-r-l" aria-hidden="true"></span>
                            {{if item.descrizione != false}}<p class="u-color-teal-50 u-text-r-xxs u-textWeight-700 u-padding-bottom-xs">{{item.descrizione}}</p>{{end }}
                            <p class="u-color-white u-text-r-xxs"></p>
                        </figcaption>

                    </figure>
                    {{end }}
                  
                </section>

            </div>
            {{end}}
            {{end}}
        </div>
        
    </section>

</div>
 <script>
    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox({
            loadingMessage: '<div class="ekko-lightbox-loader"></div>',
            leftArrow: '<span class="Icon Icon-chevron-left"></span>',
            rightArrow: '<span class="Icon Icon-chevron-right"></span>',
        });
    });
</script>