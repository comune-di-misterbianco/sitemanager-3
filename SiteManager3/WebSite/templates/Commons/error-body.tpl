﻿<div class="col-md-12">
	<div class="error-template m-5 text-center">
		<h1>{{data.statuscode}}</h1>
		<h2>{{data.errortitle}}</h2>
		<div class="error-details"> {{data.errormessage}} </div>
		<br>
		<br>
		<div class="error-actions"> 
			<a href ="javascript:history.back();" title="{{data.backlinktitle}}">{{data.backlinklabel}}</a>
			<a href="{{data.homepagelink}}" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span> Torna alla Home </a>
		<!--<a href="#" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-envelope"></span> Contatta il Supporto Tecnico </a> -->
		</div>
	</div>
</div>