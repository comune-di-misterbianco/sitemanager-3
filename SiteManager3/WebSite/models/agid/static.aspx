 <%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeFile="static.aspx.cs" Inherits="_Model" %>
<!DOCTYPE html>
<!--[if IE 8]><html class="no-js ie89 ie8" lang="it"><![endif]-->
<!--[if IE 9]><html class="no-js ie89 ie9" lang="it"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="js" <%=HTML5XmlLangInfo%> prefix="og:http://ogp.me/ns#">
<!--<![endif]-->
<head runat="server">
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Untitled Page</title>

<!-- <link rel="preload" href="/build/IWT.min.js" as="script"> -->
<!--
    In alternativa a WebFontLoader � possibile caricare il font direttamente da Google
      <link href='//fonts.googleapis.com/css?family=Titillium+Web:400,400italic,700,' rel='stylesheet' type='text/css' />
    o dal repository locale (src/fonts)
  -->
    <script type="text/javascript">
        WebFontConfig = {
            google: {
            families: ['Titillium+Web:300,400,600,700,400italic:latin']
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>
    <link media="all" rel="stylesheet" href="/scripts/shared/font-awesome-4.7.0/css/font-awesome.min.css" />
    <link media="all" rel="stylesheet" href="/build/build.css" />

</head>
<body id="CMSBody" runat="server">

<div id="cookie-bar" class="CookieBar js-CookieBar u-background-95 u-padding-r-all" aria-hidden="true">
    <p class="u-color-white u-text-r-xs u-lineHeight-m u-padding-r-bottom">Questo sito utilizza cookie tecnici, analytics e di terze parti.
         <br>Si invita a consultare l'informativa alla privacy sull'utilizzo dei cookie.<br>
    </p>
    <p>
        <button class="Button Button--default u-text-r-xxs js-cookieBarAccept u-inlineBlock u-margin-r-all">Accetto</button>
        <a href="#" class="u-text-r-xs u-color-teal-50">Privacy policy</a>
    </p>
</div>

    <form id="PageForm" runat="server">
	
	<ul class="Skiplinks element-invisible u-hiddenPrint">
		<li><a href="#BodyCorpo" class="element-invisible element-focusable">Vai al contenuto</a></li>
		<li><a href="#topmenu" class="element-invisible element-focusable" aria-controls="menu" aria-label="accedi al menu" title="accedi al menu">Vai al menu principale del sito</a></li>
	</ul>
	
        <header id="Intestazione" runat="server" class="page-header">
		
        </header>
		
        <main id="BodyCorpo" class="container-fluid" runat="server">

            <div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="LoginModal" runat="server">
			
            </div>
			
            <div class="row">
                <nav id="Where" aria-label="Ti trovi in:" class="container" runat="server"> </nav>
				<!-- bottone menu -->
                <button id="colSxButton"  type="button" class=" visible-xs btn btn-default coll" runat="server"><span id="prova" class="clickableSX coll"><i class="glyphicon glyphicon-chevron-right" ></i></span></button>                
            </div>
            <div class="row">			    
                 <div class="container">
                       <section id="sectioncontent" runat="server">
                  
                            <section id="ColSX"  runat="server">
                      
                            </section>
                            <section id="ColCX" runat="server">
                        
                            </section>
                            <section id="ColDX" runat="server">
                            </section>
                   
                        </section>
                 </div>
            </div>
            <div class="row">
                <section id="PiePagina" class="container-fluid" runat="server">
                </section>
            </div>
        </main>
        <footer id="Footer" class="panel-footer" runat="server">
		<!--[if IE 8]>
		  <script src="/build/vendor/respond.min.js"></script>
		  <script src="/build/vendor/rem.min.js"></script>
		  <script src="/build/vendor/selectivizr.js"></script>
		  <script src="/build/vendor/slice.js"></script>
		  <![endif]--> 

		<!--[if lte IE 9]>
		  <script src="/build/vendor/polyfill.min.js"></script>
		  <![endif]--> 

		
		    <script>__PUBLIC_PATH__ = '/build/'</script> 
		    <script src="/build/IWT.min.js"></script>
            <script src="/scripts/front/megamenu_utils.js"></script>  
        </footer>
    </form>
 
</body>
</html>