using System;
using System.Web.UI;
using NetFrontend;


public partial class _Model: System.Web.UI.Page
{
    public string HTML5XmlLangInfo = "";

    protected void Page_Load(object sender, EventArgs e)
    {    
        // utilizzo il costruttore esteso per passare la sessione a null in quanto in alcuni casi/errori non � possibile rileverla causando un eccezione
        PageData PageData = new PageData(this.ViewState, this.Request, null, this.Application, this.Server, this.Response, this.IsPostBack);
        PageData.Body = this.CMSBody;
        
        LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetLanguageFromUrl(Page.Request.RawUrl);
        HTML5XmlLangInfo = PageData.HTML5XmlLangInfo;
        ErrorPageBodyHandler handler = new ErrorPageBodyHandler(PageData, Page.Header);

        Intestazione.Controls.Add(handler.Header);
        ColCX.Controls.Add(handler.ColCx);
        Footer.Controls.Add(handler.Footer);

        this.Title = handler.Status;     
        Response.Status = handler.Status; 
        Response.StatusCode = handler.StatusCode;
        
    }
}
    