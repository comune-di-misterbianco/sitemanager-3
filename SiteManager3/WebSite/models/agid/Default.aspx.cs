using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetFrontend;

public partial class _Model: System.Web.UI.Page
{
    public string HTML5XmlLangInfo = "";
   
    public int ColDxClass = 3;
    public int ColSxClass = 3;
    public int ColCxOffset = 0;
    //public bool ContainerFluid = false;
	
    protected void Page_Load(object sender, EventArgs e)
    {

        PageData PageData = new PageData(this.ViewState, this);
        PageData.Body = this.CMSBody;
        
        LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetLanguageFromUrl(this.Request.Url.ToString());
        HTML5XmlLangInfo = PageData.HTML5XmlLangInfo;


        bool LanguageChanged = this.Request.UrlReferrer != null ? LanguageManager.BusinessLogic.LanguageBusinessLogic.ChangeLanguageHappened(LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage, this.Request.UrlReferrer.ToString()) : false;
        
        ResponsiveCMSBodyHandler handler = new ResponsiveCMSBodyHandler(PageData, Page.Header, LanguageChanged);
       
        Intestazione.Controls.Add(handler.Header);

        if (handler.LoginHeadModal && !NetCms.Users.AccountManager.Logged)
            LoginModal.Controls.Add(handler.ModalLoginBox());
                                             
        int col_cx_size = 12;
        Where.Controls.Add(handler.Where);

        if (PageData.Homepage.ShowColDX || PageData.Homepage.ShowColSX)
        {
            col_cx_size = col_cx_size - (PageData.Homepage.ShowColSX ? ColSxClass : 0) - (PageData.Homepage.ShowColDX ? ColDxClass : 0);
            sectioncontent.Attributes["class"] = "container";
        }


        if (PageData.Homepage.ShowColSX)
        {
            //ColSX.Attributes.Add("class", "hidden-xs col-md-" + ColSxClass);
            ColSX.Attributes.Add("class", "col-md-" + ColSxClass);
            ColSX.Controls.Add(PageData.Homepage.ColSX);

        }
        else
        {
            //colSxButton.Attributes.Add("style", "display:none;!important");
            colSxButton.Attributes["class"] += " hidden-xs";
            ColSX.Attributes.Add("class", "hidden-xs ");
        }

        //if (colcx == 12 && ColCxOffset > 0)
        //{
        //    // setta l'offset della colonna centrale nel caso sia l'unica presente nel corpo della pagina
        //    colcx = colcx - (ColCxOffset * 2);
        //    ColCX.Attributes.Add("class", "col-md-" + colcx + " col-md-offset-" + ColCxOffset);
        //}
        //else                    
        if (col_cx_size < 12)
            ColCX.Attributes.Add("class", "col-md-" + col_cx_size);
        
        ColCX.Controls.Add(handler.BuildCxControl());

        if (PageData.Homepage.ShowColDX)
        {            
            ColDX.Attributes.Add("class", "col-md-" + ColDxClass);
            ColDX.Controls.Add(PageData.Homepage.ColDX);            
        }
      
        if (PageData.Homepage.ShowFooter)
            PiePagina.Controls.Add(PageData.Homepage.Footer);
        Footer.Controls.Add(handler.Footer);
       
        //Intestazione.Controls.Add(handler.LoginControlBox);
        
    }
}
    