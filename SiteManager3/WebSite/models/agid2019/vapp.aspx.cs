using System;
using System.Web.UI;
using NetFrontend;

public partial class _Model : System.Web.UI.Page
{
    public string HTML5XmlLangInfo = "";

    public int ColDxClass = 2;
    public int ColSxClass = 2;
    public int ColCxOffset = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        PageData PageData = new PageData(this.ViewState, this);
        PageData.Body = this.CMSBody;

        LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetLanguageFromUrl(this.Request.Url.ToString());
        HTML5XmlLangInfo = PageData.HTML5XmlLangInfo;

        bool LanguageChanged = this.Request.UrlReferrer != null ? LanguageManager.BusinessLogic.LanguageBusinessLogic.ChangeLanguageHappened(LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage, this.Request.UrlReferrer.ToString()) : false;
        
        VerticalBodyHandler handler = new VerticalBodyHandler(PageData, Page.Header, LanguageChanged);

        Intestazione.Controls.Add(handler.Header);

        if (handler.LoginHeadModal && !NetCms.Users.AccountManager.Logged)
            LoginModal.Controls.Add(handler.ModalLoginBox());

        int colcx = 12;
        Where.Controls.Add(handler.Where);
        if (PageData.Homepage.ShowColDX)
        {
            colcx = colcx - ColDxClass;
            ColDX.Attributes.Add("class", "col-md-" + ColDxClass);
            ColDX.Controls.Add(PageData.Homepage.ColDX);

        }
        if (PageData.Homepage.ShowColSX)
        {
            colcx = colcx - ColSxClass;
            ColSX.Attributes.Add("class", "col-md-" + ColSxClass);
            ColSX.Controls.Add(PageData.Homepage.ColSX);
        }

        if (colcx == 12 && ColCxOffset > 0)
        {
            // setta l'offset della colonna centrale nel caso sia l'unica presente nel corpo della pagina
            colcx = colcx - (ColCxOffset * 2);
            ColCX.Attributes.Add("class", "col-md-" + colcx + " col-md-offset-" + ColCxOffset);
        }
        else
        {
   
            ColCX.Attributes.Add("class", "col-md-" + colcx);
        }

        ColCX.Controls.Add(handler.BuildControl());
        if (PageData.Homepage.ShowFooter)
            PiePagina.Controls.Add(PageData.Homepage.Footer);
        Footer.Controls.Add(handler.Footer);
    }
}