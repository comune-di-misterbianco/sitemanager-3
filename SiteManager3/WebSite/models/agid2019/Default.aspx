<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeFile="Default.aspx.cs" Inherits="_Model" %>
<!DOCTYPE html>
<!--[if IE 8]><html class="no-js ie89 ie8" lang="it"><![endif]-->
<!--[if IE 9]><html class="no-js ie89 ie9" lang="it"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="js" <%=HTML5XmlLangInfo%>  prefix="og:http://ogp.me/ns#">
<!--<![endif]-->
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Untitled Page</title>

    <link media="all" rel="stylesheet" href="/scripts/shared/font-awesome-4.7.0/css/font-awesome.min.css" />

    <!-- Bootstrap core CSS -->
    <link href="/bootstrap-italia/dist/css/bootstrap-italia.min.css" rel="stylesheet">

    <!-- Favicons -->
    <link rel="icon" href="/bootstrap-italia/dist/favicon.ico">
    <link rel="icon" href="/bootstrap-italia/dist/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/bootstrap-italia/dist/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="mask-icon" href="/bootstrap-italia/dist/assets/img/favicons/safari-pinned-tab.svg" color="#0066CC">
    <link rel="apple-touch-icon" href="/bootstrap-italia/dist/assets/img/favicons/apple-touch-icon.png">

    <link rel="manifest" href="/bootstrap-italia/dist/assets/img/favicons/manifest.webmanifest">
    <meta name="msapplication-config" content="/bootstrap-italia/dist/assets/img/favicons/browserconfig.xml">
</head>

<body id="CMSBody" runat="server">



    <form id="PageForm" runat="server">
	<!-- skip link -->	
    <div class="skiplinks">
      <a class="sr-only sr-only-focusable" href="#BodyCorpo">Vai al contenuto principale</a>
      <a class="sr-only sr-only-focusable" href="#topmenu">Vai al menu principale</a>
    </div>
	<!-- skip link -->

    <header id="Intestazione" runat="server" class="it-header-wrapper">
		
    </header>
		
    <main id="BodyCorpo" class="container-fluid my-1" runat="server">

        <div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="LoginModal" runat="server">
			
        </div>
			
        <div class="row">
            <nav id="Where" aria-label="Ti trovi in:" class="container" runat="server"> </nav>			
        </div>
        
        <div class="row">			    
                
            <section id="sectioncontent" runat="server">                                           
                <section id="ColSX"  runat="server"></section>

                <section id="ColCX" runat="server"></section>

                <section id="ColDX" runat="server"></section>
            </section>    
                
        </div>

        <div class="row">
                <section id="PiePagina" class="container-fluid" runat="server">
                </section>
        </div>
     </main>

        <footer id="Footer" class="it-footer" runat="server">
				    
            
        </footer>

        <!-- cookie-bar -->
        <div class="cookiebar">
          <p>
            Questo sito utilizza cookie tecnici, analytics e di terze parti.
            <br />Proseguendo nella navigazione accetti l�utilizzo dei cookie.
          </p>
          <div class="cookiebar-buttons">
            <a href="#" class="cookiebar-btn">Preferenze<span class="sr-only">cookies</span></a>
            <button data-accept="cookiebar" class="cookiebar-btn cookiebar-confirm">
              Accetto<span class="sr-only"> i cookies</span>
            </button>
          </div>
        </div>

        <a href="#" aria-hidden="true" data-attribute="back-to-top" class="back-to-top">
          <svg class="icon icon-light">
            <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-arrow-up"></use>
          </svg>
        </a>
        <!-- end cookie-bar -->

        <script>window.__PUBLIC_PATH__ = "/bootstrap-italia/dist/fonts"</script>
        <script src="/bootstrap-italia/dist/js/bootstrap-italia.bundle.min.js"></script>        
        <script src="/scripts/front/megamenu_utils.js"></script>  
        <script src="/scripts/front/lightbox/ekko-lightbox.js"></script>

    </form>
 
</body>
</html>
