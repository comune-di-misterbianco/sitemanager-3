using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetFrontend;
using NetFrontend.FrontPiecies;
using System.Globalization;

public partial class _Model: System.Web.UI.Page
{
    public string XmlLangInfo = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        PageData PageData = new PageData(this.ViewState, this);
        XmlLangInfo = PageData.XmlLangInfo;
        VerticalNextGenBodyHandler handler = new VerticalNextGenBodyHandler(PageData, Page.Header);
     
        Intestazione.Controls.Add(handler.Intestazione);

        if (PageData.Homepage.ShowColDX)
        {
            ColDX.Controls.Add(handler.ColDX);
        }
        if (PageData.Homepage.ShowColSX)
        {
            ColSX.Controls.Add(handler.ColSX);
        }

        ColCX.Controls.Add(handler.BuildControl());

        this.Where.Controls.Add(handler.Where);
        this.Footer.Controls.Add(new Footer(PageData).GetControl());

    }
}
   


           
    