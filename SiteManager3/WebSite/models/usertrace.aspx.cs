﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class models_usertrace : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpRequest req = HttpContext.Current.Request;

        Users.BusinessLogic.UserTraceBusinessLogic.TraceData(req);


    }

    //private void TraceData(System.Web.HttpRequest req)
    //{
    //    Users.Position pos = Users.BusinessLogic.UserTraceBusinessLogic.GetGeoLocationFromCookie(req);

    //    Users.Models.TraceData traceData = new Users.Models.TraceData();
    //    traceData.PublicIp = (req.Headers["X-Forwarded-For"] != null) ? req.Headers["X-Forwarded-For"] : req.ServerVariables["REMOTE_ADDR"];
    //    traceData.OS = req.UserAgent;
    //    traceData.Browser = req.UserAgent;
    //    traceData.Url = req.Url.AbsoluteUri;
    //    traceData.Referer = (req.UrlReferrer != null) ? req.UrlReferrer.ToString() : "";
    //    traceData.SessionID = System.Web.HttpContext.Current.Session.SessionID;
    //    traceData.Cookies = req.ServerVariables["HTTP_COOKIE"];
    //    traceData.DateOra = DateTime.Now;
    //    traceData.AcceptLanguage = req.ServerVariables["HTTP_ACCEPT_LANGUAGE"];
        
    //    traceData.Longitudine = pos.lon;
    //    traceData.Latitudine = pos.lat;
    //    traceData.PositionStatus = pos.PositionStatus;

    //    Users.BusinessLogic.UserTraceBusinessLogic.Save(traceData);
    //}

    //private Position GetGeoLocationFromCookie(System.Web.HttpRequest req)
    //{
    //    Position pos = new Position();

    //    if (req.Cookies["posdata"] != null)
    //    {
    //        var posData = req.Cookies["posdata"].Value;
    //        // sanityze
    //        if (posData != null && posData.Length > 1)
    //        {
    //            string[] coord = posData.Split(new char[] { '|' });
    //            pos.lat = coord[0];
    //            pos.lon = coord[1];
    //            pos.PositionStatus = Users.Models.PositionStatusEnum.ok;                
    //        }
    //        else
    //        {
    //            pos.lat = null;
    //            pos.lon = null;
    //            pos.PositionStatus = (Users.Models.PositionStatusEnum)Enum.Parse(typeof(Users.Models.PositionStatusEnum), posData);
    //        }

    //        req.Cookies.Remove("posdata");
    //    }
    //    return pos;
    //}


    //struct Position
    //{
    //    public string lat;
    //    public string lon;
    //    public Users.Models.PositionStatusEnum PositionStatus;
    //}

}