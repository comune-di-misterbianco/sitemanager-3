using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetFrontend;
using NetFrontend.FrontPiecies;
using System.Globalization;

public partial class _Model: System.Web.UI.Page
{
    public string HTML5XmlLangInfo = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        #region MyRegion
        //this.Title = NetCms.Configurations.PortalData.Nome + " - Si � verificato un errore";
        //this.Header.Controls.Add(new LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + NetCms.Configurations.Paths.AbsoluteRoot + "/css/error.css\" />"));

        //int statusCode = 500;

        //NetUtility.RequestVariable errorCode = new NetUtility.RequestVariable("e", NetUtility.RequestVariable.RequestType.QueryString);

        //if (errorCode != null && errorCode.IsValidInteger)
        //    statusCode = errorCode.IntValue;

        //string strMsg = "<h1>Pagina di errore impostata nel web.config (system.web -> customErrors )</h1>"
        //          + "<div>"
        //          + "<p>"
        //          + "la sezione system.webServer -> httpErrors non � stata configurata (eredita i settaggi di default di iis) <br />"
        //          + "in configurations -> ExceptionsHandler l'errore viene registrato ma il server.transfer � stato disattivato <br />"
        //          + "StatusCode corrente: " + statusCode  + "<br />"
        //          + "</p>"
        //          + "</div>";

        //Response.Status = "404 Not Found"; 
        //Response.StatusCode = statusCode;

        //int currentStatuCode = System.Web.HttpContext.Current.Response.StatusCode;

        //LiteralControl msg = new LiteralControl(strMsg);

        //this.ColCX.Controls.Add(msg); 
        #endregion

        PageData PageData = new PageData(this.ViewState, this.Request, null, this.Application, this.Server, this.Response, this.IsPostBack);
        PageData.Body = this.CMSBody;

        LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetLanguageFromUrl(Page.Request.RawUrl);
        HTML5XmlLangInfo = PageData.HTML5XmlLangInfo;

        ErrorPageBodyHandler handler = new ErrorPageBodyHandler(PageData, Page.Header);

        Intestazione.Controls.Add(handler.Header);
        ColCX.Controls.Add(handler.ColCx);
        Footer.Controls.Add(handler.Footer);

        this.Title = handler.Status;
        Response.Status = handler.Status; //"404 Not Found"; 
        Response.StatusCode = handler.StatusCode;

    }    
}
    