﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.Routing;
using System.Web.Http;
using System.Threading;

/// <summary>
/// Summary description for Global
/// </summary>
public class Global : System.Web.HttpApplication 
{
    public Global()
	{
    }
    void Application_BeginRequest(object sender, EventArgs e)
    {
        NetCms.ApplicationContext.SetCurrentComponent("System");

        if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
        {
           HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept");            
            HttpContext.Current.Response.End();
        }

        if (NetCms.Configurations.Generics.UseSecureConnection &&
            NetCms.Configurations.Paths.CurrentRequestIsSecure &&
            NetCms.Configurations.Paths.Extension == "aspx")
        {
            Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl);
        }
        
        NetCms.Utility.Maintenance.Check();
        
        if (LanguageManager.Utility.Config.EnableMultiLanguage)
        {
            if (LanguageManager.BusinessLogic.LanguageBusinessLogic.UrlHasLanguageInfo(HttpContext.Current.Request.Url.AbsolutePath) && !HttpContext.Current.Request.Url.AbsolutePath.EndsWith(".aspx"))
                Response.Redirect(HttpContext.Current.Request.Url.AbsolutePath + (!HttpContext.Current.Request.Url.AbsolutePath.EndsWith("/")? "/" : "")  + "default.aspx");
        }

        /* Fix for the Flash Player Cookie bug in Non-IE browsers.\
         * Since Flash Player always sends the IE cookies even in FireFox
         * we have to bypass the cookies by sending the values as part of the POST or GET
         * and overwrite the cookies with the passed in values.
         * 
         * The theory is that at this point (BeginRequest) the cookies have not been read by
         * the Session and Authentication logic and if we update the cookies here we'll get our
         * Session and Authentication restored correctly
         */
        try
        {
            string session_param_name = "ASPSESSID";
            string session_cookie_name = "ASP.NET_SessionId";

            if (HttpContext.Current.Request.Form[session_param_name] != null)
            {
                UpdateCookie(session_cookie_name, HttpContext.Current.Request.Form[session_param_name]);
            }
            else if (HttpContext.Current.Request.QueryString[session_param_name] != null)
            {
                UpdateCookie(session_cookie_name, HttpContext.Current.Request.QueryString[session_param_name]);
            }
        }
        catch { }

        try
        {
            string auth_param_name = "AUTHID";
            string auth_cookie_name = FormsAuthentication.FormsCookieName;

            if (HttpContext.Current.Request.Form[auth_param_name] != null)
            {
                UpdateCookie(auth_cookie_name, HttpContext.Current.Request.Form[auth_param_name]);
            }
            else if (HttpContext.Current.Request.QueryString[auth_param_name] != null)
            {
                UpdateCookie(auth_cookie_name, HttpContext.Current.Request.QueryString[auth_param_name]);
            }

        }
        catch { }

        try
        {
            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                var lang = (sender as HttpApplication).Context.Request.Headers["Accept-Language"];
                if (!string.IsNullOrEmpty(lang))
                {
                    var culture = new System.Globalization.CultureInfo(lang);
                    Thread.CurrentThread.CurrentCulture = culture;
                    Thread.CurrentThread.CurrentUICulture = culture;
                }
            }
            else
            {

            }
        }
        catch(Exception ex) { }

        NetCms.Utility.Maintenance.Handle();
    }

    void UpdateCookie(string cookie_name, string cookie_value)
    {
        HttpCookie cookie = HttpContext.Current.Request.Cookies.Get(cookie_name);
        if (cookie == null)
        {
            cookie = new HttpCookie(cookie_name);
            HttpContext.Current.Request.Cookies.Add(cookie);
        }
        cookie.Value = cookie_value;
        HttpContext.Current.Request.Cookies.Set(cookie);
    }

    void Application_Start(object sender, EventArgs e)
    {        
        Application["CmsStatus"] = NetCms.Configurations.CmsStatusEnum.nostatus;

        #region Routes

        RouteTable.Routes.MapHttpRoute(
            name: "FileManager",
            routeTemplate: "api/{controller}/{action}/{folderId}/{userId}/{page}/{rp}/{name}/{order}/{tipoFiltro}",
            defaults: new { folderId = System.Web.Http.RouteParameter.Optional, userId = System.Web.Http.RouteParameter.Optional, page = System.Web.Http.RouteParameter.Optional, rp = System.Web.Http.RouteParameter.Optional, name = System.Web.Http.RouteParameter.Optional, order = System.Web.Http.RouteParameter.Optional, tipoFiltro = System.Web.Http.RouteParameter.Optional },
            constraints: new { controller = "FileManager" }
        );

        RouteTable.Routes.MapHttpRoute(
            name: "UploadManager",
            routeTemplate: "api/{controller}/{action}/{id}",
            defaults: new { id = System.Web.Http.RouteParameter.Optional, action = System.Web.Http.RouteParameter.Optional },
            constraints: new { controller = "UploadManager" }
        );        

        RouteTable.Routes.MapHttpRoute(
            name: "CD",
            routeTemplate: "api/{controller}/{action}/{folderID}/{nomeFolder}",
            defaults: new {
                            folderID = System.Web.Http.RouteParameter.Optional,
                            action = System.Web.Http.RouteParameter.Optional,
                            nomeFolder = System.Web.Http.RouteParameter.Optional
                      },
            constraints: new { controller = "CDDocument|CDFolder|CDShareStatus|CDItems"}
        );

        //RouteTable.Routes.MapHttpRoute(
        //    name: "NewsLetter",
        //    routeTemplate: "api/{controller}/{action}/{id}",
        //    defaults: new { id = System.Web.Http.RouteParameter.Optional},
        //    constraints: new { controller = "NewsLetter" }
        //);

        RouteTable.Routes.MapHttpRoute(
            name: "PingSession",
            routeTemplate: "api/{controller}/{action}/{id}",
            defaults: new { id = System.Web.Http.RouteParameter.Optional },
            constraints: new { controller = "PingSession" }
        );
       
       RouteTable.Routes.MapHttpRoute(
            name: "Authentication",
            routeTemplate: "api/{controller}/{action}/{id}",
            defaults: new { id = System.Web.Http.RouteParameter.Optional, action = System.Web.Http.RouteParameter.Optional },
            constraints: new { controller = "Authentication" }
       );

        #region AUTH CONTROLLER
        RouteTable.Routes.MapHttpRoute(
            name: "auth",
            routeTemplate: "api/{controller}/{action}/{id}",
            defaults: new { id = System.Web.Http.RouteParameter.Optional },
            constraints: new { controller = "Auth" }
        );
        #endregion

        RouteTable.Routes.MapHttpRoute(
            name: "front",
            routeTemplate: "api/{controller}/{action}/{id}",
            defaults: new { id = System.Web.Http.RouteParameter.Optional },
            constraints: new { controller = "Front" }
        );

        RouteTable.Routes.MapHttpRoute(
          name: "document",
          routeTemplate: "api/{controller}/{action}/{id}",
          defaults: new { id = System.Web.Http.RouteParameter.Optional },
          constraints: new { controller = "Document" }
          );

       
        RouteTable.Routes.MapHttpRoute(
            name: "DefaultApi",
            routeTemplate: "api/{controller}/{net}/{id}",
            defaults: new { id = System.Web.Http.RouteParameter.Optional, net = System.Web.Http.RouteParameter.Optional }
        );

        #endregion

        //Metodo di avvio di log4net
        log4net.Config.XmlConfigurator.Configure();
             
        // Qui vanno aggiunti tutti gli assembly che in fase di startup vogliamo aggiungere alla session factory
        NetCms.DBSessionProvider.FluentSessionProvider.FluentAssemblies.Add(System.Reflection.Assembly.GetAssembly(typeof(NetCms.Grants.ObjectProfile)));
        NetCms.DBSessionProvider.FluentSessionProvider.FluentAssemblies.Add(System.Reflection.Assembly.GetAssembly(typeof(NetCms.Networks.BasicNetwork)));
        NetCms.DBSessionProvider.FluentSessionProvider.FluentAssemblies.Add(System.Reflection.Assembly.GetAssembly(typeof(NetCms.Structure.WebFS.Document)));
        NetCms.DBSessionProvider.FluentSessionProvider.FluentAssemblies.Add(System.Reflection.Assembly.GetAssembly(typeof(NetCms.Users.User)));
        NetCms.DBSessionProvider.FluentSessionProvider.FluentAssemblies.Add(System.Reflection.Assembly.GetAssembly(typeof(LanguageManager.Model.Language))); 
        NetCms.DBSessionProvider.FluentSessionProvider.FluentAssemblies.Add(System.Reflection.Assembly.GetAssembly(typeof(LoginManager.Models.LoginSetting)));       

        NetCms.Assemblies.AssembliesLoader.LoadAssemblies();
        
        LabelsManager.LabelsCache.LoadLabels();             

        //Avvio dello scheduler
        GenericJobs.Model.JobsScheduler.Start();

        //Caricamento configurazione del cms
        CmsConfigs.Model.GlobalConfig.Init();

        //NetCms.Spid.SpidConfig.Instance.;

        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Info, "Applicazione Avviata", "", "", "");
    }
    void Application_End(object sender, EventArgs e)
    {
        GenericJobs.Model.JobsScheduler.Shutdown();        
        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Info, "Applicazione Arrestata", "", "", "");
    }   

    void Application_Error(object sender, EventArgs e)
    {
        string errorPage = "{0}errorPage.aspx?e={1}";
        string urlErrorPage = "";
        Exception ex = Server.GetLastError();

        NetCms.Users.User account = null;        
        NetCms.Exceptions.ExceptionsHandler handler = new NetCms.Exceptions.ExceptionsHandler(ex);

        if (account != null)
        {
            handler.UserID = account.ID.ToString();
            handler.UserName = account.UserName;
        }
        handler.HandleException();

        var httpException = ex as HttpException;
        if (httpException != null)
        {
            int statusCode = httpException.GetHttpCode();

            Response.TrySkipIisCustomErrors = true;

            var currentPath = this.Request.Path;

            if ((NetCms.Configurations.CmsStatusEnum)Application["CmsStatus"] != NetCms.Configurations.CmsStatusEnum.dberror)
            {
                if (!currentPath.StartsWith("/cms"))
                {
                    Server.ClearError();

                    if (ex.Message.Contains("Valore potenzialmente pericoloso"))
                    {
                        GenericErrorHandler(this.Context, statusCode);
                    }
                    else
                    {
                        CmsConfigs.Model.Config CurrentConfig = CmsConfigs.Model.GlobalConfig.GetConfigByNetworkID(0);

                        if (CurrentConfig != null)
                            urlErrorPage = string.Format(errorPage, CurrentConfig.CurrentTheme.ModelFolder, statusCode);
                        else
                            urlErrorPage = string.Format(errorPage, "/models/", statusCode);

                        Server.Transfer(urlErrorPage, false);
                    }
                }
                else
                {
                    Server.ClearError();
                    Server.Transfer("/cms/error.htm", false);
                }
            }
            else
            {
                Server.ClearError();
                Server.Transfer("/models/error_nodb.htm", false);
            }
        }
        else if (ex != null && ex.InnerException != null && ex.InnerException.Message == "Unable to connect to any of the specified MySQL hosts.")
        {
            Server.ClearError();
            Server.Transfer("/models/error_nodb.htm", false);
        }
        else if (ex != null)
        {
            Server.ClearError();
            Server.Transfer("/models/error.htm", false);
        }
    }


    void Session_Start(object sender, EventArgs e)
    {
        if (Request.IsSecureConnection == true)
			Response.Cookies ["ASP.NET_SessionId"].Secure = true;
    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

        try
        {
            G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
            cache.RemoveCurrentUserCachedObjects();
        }
        catch { }
        if (NetCms.Networks.NetworksManager.CurrentFace == NetCms.Faces.Admin)

            Response.Redirect(NetCms.Configurations.Paths.AbsoluteSiteManagerRoot);
        else try
            { Response.Redirect(NetCms.Configurations.Paths.AbsoluteRoot + "/internet/default.aspx"); }
            catch { };
    }

    private static void GenericErrorHandler(HttpContext ctx, int StatusCode)
    {
        string errorTpl = "/models/errorTpl.htm";
        string path = ctx.Server.MapPath(errorTpl);

        byte[] buffer;
        try
        {
            System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            System.IO.FileInfo info = new System.IO.FileInfo(path);
            buffer = new byte[info.Length];
            fs.Read(buffer, 0, Convert.ToInt32(info.Length));
            fs.Close();

            ctx.Response.Clear();
            ctx.Response.StatusCode = StatusCode;
            ctx.Response.ContentType = "text/html";
            ctx.Response.AddHeader("Content-Length", buffer.Length.ToString());
            ctx.Response.OutputStream.Write(buffer, 0, buffer.Length);
            ctx.Response.OutputStream.Flush();
            ctx.Response.Flush();
        }
        catch
        {
            return;
        }

    }

}