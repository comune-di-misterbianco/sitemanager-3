﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

/// <summary>
/// Summary description for CMSDBConnectionModule
/// </summary>

namespace NetCms.Connections
{
    public class CMSDBConnectionModule : IHttpModule
    {       

        public void Init(HttpApplication context)
        {
            bool status = false;

            NetCms.Configurations.CmsStatusEnum currentStaus = (NetCms.Configurations.CmsStatusEnum)context.Application["CmsStatus"];

            // verifica permessi di scrittura su disco 

            // verifica connessione db
            if (currentStaus == NetCms.Configurations.CmsStatusEnum.nostatus)  
               status = CheckDBConnection();

            if (status)
               context.Application["CmsStatus"] = NetCms.Configurations.CmsStatusEnum.ok;
            
            context.Error += new EventHandler(OnError);
        }
     
        private void OnError(object sender, EventArgs e)
        {
            HttpApplication application = (HttpApplication)sender;

            Exception exception = application.Server.GetLastError();
            while (exception != null && exception as MySql.Data.MySqlClient.MySqlException == null)
            {
                exception = exception.InnerException;
            }

            if (exception as MySql.Data.MySqlClient.MySqlException != null)
            {                
                application.Context.Application["CmsStatus"] = NetCms.Configurations.CmsStatusEnum.dberror;
                application.Context.Server.ClearError();
                application.Context.Response.TrySkipIisCustomErrors = true;
                application.Context.Response.StatusCode = 500;
                //application.Context.Response.Status = exception.Message;
                application.Server.Transfer("/models/error_nodb.htm");                
            }

        }

        private bool CheckDBConnection()
        {
            bool status = false;

            using (ConnectionsManager ConnManager = new NetCms.Connections.ConnectionsManager())
            {                
                Connection conn = ConnManager.CommonConnection;
                conn.CheckConnection();             
                status = true;
            }

            return status;
        }


        public void Dispose()
        {            
        }

        
    }
}