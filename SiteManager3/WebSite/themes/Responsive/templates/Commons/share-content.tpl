﻿<div class="pull-right">
	<div class="Share">
		<div class="Share-reveal">
		  <a href="#share-options" class="Share-revealText">
			<span class="Share-revealIcon Icon Icon-share"></span>
			{{commonlabels.labelsharebutton}}
		 </a>
	   </div>	
		
		<ul id="share-options" >
			<li>
				<a href="#" title="{{commonlabels.titleshareclosepanel}}" aria-hidden="true">
					<span class="Share-hideText Icon-close"></span>
					<span class="u-hiddenVisually">{{commonlabels.labelclosebutton}}</span>
				</a>
			</li>
			<li>
			<a href="javascript:void(0)"
				onclick="window.open('https://www.facebook.com/sharer.php?u={{document.docurl}}','sharer','toolbar=0,status=0,width=548,height=325');"
				title="{{commonlabels.titlesharefb}}">
				<span class="Icon Icon-facebook"></span>
				<span class="u-hiddenVisually">Facebook</span>
			</a>
			</li>
			<li>
			<a href="http://www.twitter.com/share?text={{document.descforquerystring}}&url={{document.docurl}}"
				title="{{commonlabels.titlesharetw}}" target="_blank">
				<span class="Icon Icon-twitter"></span>
				<span class="u-hiddenVisually">Twitter</span>
			</a>
			</li>
			<li>
			<a href="whatsapp://send?text={{document.docurl}}" title="{{commonlabels.titlesharewa}}">
				<span class="Icon Icon-whatsapp"></span>
				<span class="u-hiddenVisually">Whatsapp</span>
			</a>
			</li>
		</ul>
	</div>
</div>