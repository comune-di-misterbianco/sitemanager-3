﻿<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>    
			<div class="">                
                {{if sezione.currentcategory != "-1"}}
                <p>
                    <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;
                   {{labels.selectedcategory}} <strong>{{sezione.currentcategory}}</strong> 
                    <a href="default.aspx"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;{{labels.removefilter}}</a>
                </p>
                {{end}}
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            {{if documents.size > 0 }}
			<div class="table-responsive">
			<table class="table">
				<thead> 
					<tr>
						<th>{{labels.data}}</th> 
						<th>{{labels.titolo}}</th> 
						<th>{{labels.descrizione}}</th> 
						<th>{{labels.categoria}}</th> 
					</tr>
				</thead>			
				<tbody> 
					{{for doc in documents }}
					<tr class="active">
						<td scope="row">{{doc.data}}</td> 
						<td>
							<a class="u-textClean" href="?cs={{doc.id}}">
								{{doc.titolo}}
							</a>
						</td> 
						<td>{{doc.descrizione}}</td> 
						<td><a class="u-textClean" href="default.aspx?cat={{doc.idcategoria}}">{{doc.categoria}}</a></td> 
					</tr>
					{{end}}
				</tbody>
				
			</table>
			</div>
            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <li class=""><a class="disabled" href="#">Pagina {{ pager.current }} di {{ pager.pages }}</a></li>
                    {{ if pager.current > 1}}
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}=1" aria-label="{{commonlabels.firstpage}}">
                            <span class="fa fa-angle-double-left"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.firstpage}}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current-1}}" aria-label="{{commonlabels.prevpage}}">
                            <span class="fa fa-angle-left" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.prevpage}}</span>
                        </a>
                    </li>
                    {{end }}

                    {{ for i in pager.lowrange..pager.toprange}}
                    <li {{ if  i == pager.current }} class="active" {{end}}>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{i}}">{{i}}</a>
                    </li>
                    {{end }}

                    {{ if pager.current < pager.pages}}
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current+1}}" aria-label="{{commonlabels.nextpage}}">
                            <span class="fa fa-angle-right" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.nextpage}}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.pages}}" aria-label="{{commonlabels.lastpage}}">
                            <span class="fa fa-angle-double-right" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.lastpage}}</span>
                        </a>
                    </li>
                    {{end }}
                </ul>
            </nav>            

            {{else}}
			  <p>{{labels.norecord}}</p>
            {{end}}
        </div>
    </div>
 </div>

