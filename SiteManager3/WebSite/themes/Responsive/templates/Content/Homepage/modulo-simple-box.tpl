﻿<div class="u-nbfc u-color-grey-30 u-xs-padding-all-none u-borderRadius-m u-background-white">
  {{if data.modulo.img.imgsrc != "false" }}
  <img src="{{data.modulo.img.imgsrc}}" class="img-responsive" alt="{{data.modulo.img.imgalt}}" />
   {{end}}
  <div class="u-padding-r-all">
    <div class="u-padding-r-all u-xs-padding-all-none">
      {{if data.modulo.titolo != "false"}}
	  <h3 class="u-padding-bottom-xs">
              {{if data.modulo.url != "false"}}
		<a class="u-textClean u-textWeight-600 u-text-r-l u-color-50" title="{{data.modulo.urltitle}}" href="{{data.modulo.url}}">{{data.modulo.titolo}}</a>
              {{else}}		
		<span class="u-textClean u-textWeight-600 u-text-r-l u-color-50">{{data.modulo.titolo}}</span>	
		{{end}}
      </h3>      
	  {{end}}
      <div class="u-text-p u-textSecondary u-margin-r-bottom u-padding-r-bottom">
        {{data.modulo.testo}}
      </div>
      
    </div>
  </div>
</div>
