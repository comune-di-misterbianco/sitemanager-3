﻿<div class="container">
    <div class="u-layout-wide u-layoutCenter u-layout-withGutter u-padding-r-top u-padding-bottom-xxl">
        <h1 class="u-text-h2">
            {{sezione.titolo}}
        </h1>
   
        <div class="Grid Grid--withGutter">
             <div class="Grid-cell {{if sezione.menu == "false" }} u-md-size12of12 u-lg-size12of12 {{ else }} u-md-size8of12 u-lg-size8of12   {{ end }}">

                <article class="pagina">
                    <div class="Grid Grid--withGutter">
                        
						<div class="date Grid-cell u-size1of2">
                            <p><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;{{commonlabels.labelpublishedon}}: {{sezione.data}}</p>
                        </div>
                       
						<div class="share Grid-cell u-size1of2">
                         {{sharecontent}}
						</div>
						 <div class="print Grid-cell u-size1of1 text-left">
                            <p>
                                <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                                <a href="javascript:this.print();" title="{{commonlabels.titlestampalink}}">{{commonlabels.labelstampa}}</a>
                            </p>
                        </div>
                    </div>
                    <p class="description">{{sezione.descrizione}}</p>
                    <div class="page-content">
                        {{sezione.testo | regex.replace "(<h1.*?>.*?</h1>)" "" "$"}}						
                    </div>
                    {{if sezione.tags | object.size > 0}}
                    <div class="doc-tags">
                        <i class="fa fa-tags" aria-hidden="true"></i>
                        <span>{{commonlabels.tagstitle}}:</span>
                        {{ for tag in sezione.tags }}
                        <a class="doc-tag" rel="tag" href="/tags/argomenti.aspx?tag={{tag.key}}">{{tag.label}}</a>
                        {{end}}
                    </div>
                    {{end}}					 
                </article>
            </div>
			{{if sezione.menu != false }}
            <div class="Grid-cell u-sizeFull u-md-size4of12 u-lg-size4of12">                
                <div class="u-sizeFull u-md-size11of12 u-lg-size11of12" id="subnav">
                    {{sezione.menu}}
                </div>
                               
            </div>
			{{end}}
        </div>
    </div>
</div>