﻿<div class="container">
    <article>
        <div class="row docContent">
		{{
			 size = 12
			 if sezione.sidebar.sidebarsx == true
				size = size-3
			 end
			 if sezione.sidebar.sidebardx == true
			 	size = size-3
			end 
			}}
            <div class="col-md-{{size}}">
                <div class="page-header">
                    <h1>{{sezione.titolo}}</h1>
                    <p class="description">{{sezione.descrizione}}</p>
                </div>
            </div>
        </div>
    </article>
</div>