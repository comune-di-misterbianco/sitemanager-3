﻿<div class="container">
    <article>
        <div class="row docContent">
			<div class="container">

			{{
			 size = 12
			 if sezione.sidebar.sidebarsx == true
				size = size-3
			 if sezione.sidebar.sidebardx == true
			 	size = size-3
			}}

				<div class="col-md-{{size}}">
					<div class="page-header">
						<h1>{{sezione.titolo}}</h1>
						<p class="description">{{sezione.descrizione}}</p>
					</div>
					<div class="page-content">
						{{sezione.testo}}
					</div>
					{{if document.tags | object.size > 0}}
					<div class="doc-tags">
						<i class="fa fa-tags" aria-hidden="true"></i>
						<span>{{commonlabels.tagstitle}}:</span>
						{{ for tag in document.tags }}
						<a class="doc-tag" rel="tag" href="/tags/argomenti.aspx?tag={{tag.key}}">{{tag.label}}</a>
						{{end}}
					</div>
					{{end}}
				</div>
			</div>
        </div>
    </article>
</div>