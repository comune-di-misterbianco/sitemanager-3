﻿<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>
        </div>
    </div>

    <div class="row ContentMenuTree">
        <div class="col-md-12">
            <div class="page-content">
                {{sezione.items}}
            </div>
        </div>
    </div>
</div>