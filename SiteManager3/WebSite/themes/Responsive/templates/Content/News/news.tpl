﻿<section class="Grid">
    {{if documents.size > 0 }}    
	{{for doc in documents limit:1 }}
    <div class="Grid-cell u-sizeFull u-md-size1of2 u-lg-size1of2 u-text-r-s u-padding-r-all">
        <div class="Grid Grid--fit u-margin-r-bottom">
            <p class="Grid-cell">
                <span class="Dot u-background-50"></span>
                <strong><a class="u-textClean u-text-r-xs category" href="{{doc.folderpath}}/default.aspx">{{doc.categoria}}</a></strong>				
            </p>
            <p class="Grid-cell u-textSecondary">
                <i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
                <time datetime="{{ doc.data }}">{{ doc.data }}</time>
            </p>
        </div>
        <div class="u-text-r-l u-layout-prose">
            <h2 class="u-text-h2 u-margin-r-bottom">
                <a class="u-text-h2 u-textClean u-color-black" href="?news={{doc.id}}">
                    {{doc.titolo}}
                </a>
            </h2>
            <p class="u-textSecondary u-lineHeight-l">
                {{doc.descrizione}}
            </p>
        </div>
    </div>
    {{if doc.imgevidenza != false}}
		{{if doc.imgevidenza.src | string.size > 0 }}
		<div class="Grid-cell u-sizeFull u-md-size1of2 u-lg-size1of2 u-text-r-s u-padding-r-all">
			<img src="{{doc.imgevidenza.src}}" class="u-sizeFull" alt="{{doc.imgevidenza.description}}" />
		</div>
		{{end}}
	{{end}}

    {{end}}
    {{end }}
</section>

