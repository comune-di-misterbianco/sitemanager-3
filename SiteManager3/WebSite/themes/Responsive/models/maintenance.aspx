<%@ Page Language="C#" AutoEventWireup="true" EnableSessionState="true" Async="true" CodeFile="maintenance.aspx.cs" Inherits="_Model" %>
<!DOCTYPE html>

<html>
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Errore generico</title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    
    
    <link rel="stylesheet" type="text/css" href="/scripts/bootstrap-4/css/bootstrap.min.css" />
    <link rel="shortcut icon" href="/imghome/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="/imghome/favicon.ico" type="image/x-icon" />
    <style>

    </style>
</head>
<body>
    <div id="globale">
        <div id="Intestazione" runat="server">
        </div>

        <div id="corpo" runat="server" >       
        
        </div>
        <div id="Footer" class="fixed-bottom" runat="server">        
             
        </div>
    </div>
    <script type="text/javascript" src="/scripts/bootstrap-4/js/bootstrap.js"></script>
</body>
</html>