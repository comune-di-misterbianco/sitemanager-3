<%@ Page Language="C#" AutoEventWireup="true" CodeFile="vapp.aspx.cs" Inherits="_Model" %>

<!DOCTYPE html> 

<html>
<head runat="server">
<meta charset="UTF-8">
<title>Untitled Page</title>
</head>
<body>
    <form id="PageForm" runat="server">
	    <section id="globale" runat="server">
            <header id="Intestazione" runat="server">
                <!--<h1 id="h1" runat="server"></h1> -->
            </header>
            <section id="Corpo" runat="server">
                <section id="Where" runat="server">
                </section>
                <section id="ColCX" runat="server">
                </section>
                <section id="ColDX" runat="server">
                </section>
                <section id="ColSX" runat="server">
                </section>
            </section>
            <footer id="Footer" runat="server">
                <!--<p>� 2010 Net.Service S.R.L. - SDK Applicazioni Verticali</p>-->
            </footer>
        </section>
    </form>
</body>
</html>