using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetFrontend;
using NetFrontend.FrontPiecies;
using System.Globalization;
using RefertiOnLine.BodyHandlers;

public partial class _Model : System.Web.UI.Page
{
    public string HTML5XmlLangInfo = "";

    public int ColDxClass = 2;
    public int ColSxClass = 2;
    public int ColCxOffset = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        PageData PageData = new PageData(this.ViewState, this);
        PageData.Body = this.CMSBody;
        HTML5XmlLangInfo = PageData.HTML5XmlLangInfo;

        VerticalNextGenBodyHandlerCustom handler = new VerticalNextGenBodyHandlerCustom(PageData, Page.Header);

        Intestazione.Controls.Add(handler.Intestazione);
     
        int colcx = 12;
        this.Where.Controls.Add(handler.Where);

        if (PageData.Homepage.ShowColDX)
        {
            colcx = colcx - ColDxClass;
            ColDX.Attributes.Add("class", "col-md-" + ColDxClass);
            ColDX.Controls.Add(handler.ColDX);
        }
        if (PageData.Homepage.ShowColSX)
        {
            colcx = colcx - ColSxClass;
            ColSX.Attributes.Add("class", "col-md-" + ColSxClass);
            ColSX.Controls.Add(handler.ColSX);
        }

        if (colcx == 12 && ColCxOffset > 0)
        {
            colcx = colcx - (ColCxOffset * 2);
            ColCX.Attributes.Add("class", "col-md-" + colcx + " col-md-offset-" + ColCxOffset);
        }
        else
        {
            ColCX.Attributes.Add("class", "col-md-" + colcx);
        }

        ColCX.Controls.Add(handler.BuildControl());

        if (PageData.Homepage.ShowFooter)
            PiePagina.Controls.Add(PageData.Homepage.Footer);

        this.Footer.Controls.Add(new Footer(PageData).GetControl());

    }
}




