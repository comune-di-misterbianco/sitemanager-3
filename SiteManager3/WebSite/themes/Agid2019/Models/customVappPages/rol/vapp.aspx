<%@ Page Language="C#" AutoEventWireup="true" CodeFile="vapp.aspx.cs" Inherits="_Model" ClientTarget="uplevel" %>

<!DOCTYPE html> 

<html>
<head runat="server">
<meta charset="UTF-8" />
<title>Untitled Page</title>
<link rel="icon" 
      type="image/png" 
      href="favicon.png" />
</head>
<body id="CMSBody" runat="server" class="login-page">
    <form id="PageForm" runat="server">
	
	        <ul class="Skiplinks sr-only sr-only-focusable">
				<li><a href="#BodyCorpo" class="element-invisible element-focusable">Vai al contenuto</a></li>
				<li><a href="#topmenu" class="element-invisible element-focusable" aria-controls="menu" aria-label="accedi al menu" title="accedi al menu">Vai al menu principale del sito</a></li>
			</ul>
            
			<header id="Intestazione" runat="server" class="page-header"></header>
            		
			<main id="BodyCorpo" class="container-fluid" runat="server">
				<div class="row">
					<section id="Where" class="container-fluid" runat="server"></section>
				</div>
				<div class="row">
					<div class="container-fluid">
						<section id="ColSX" runat="server"></section>
						<section id="ColCX" runat="server"></section>
						<section id="ColDX" runat="server"></section>
					</div>
				</div>
				<div class="row">
					<section id="PiePagina" class="container-fluid" runat="server"></section>				
				</div>
			</main>
        			
            <footer id="Footer" runat="server"></footer>
       
    </form>    
</body>
</html>