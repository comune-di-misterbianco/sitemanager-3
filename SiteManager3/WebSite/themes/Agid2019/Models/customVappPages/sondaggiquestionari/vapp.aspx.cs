﻿//using NetFrontend;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;

//using System;
//using System.Data;
//using System.Configuration;
//using System.Collections;
//using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
//using NetFrontend;
//using NetFrontend.FrontPiecies;
//using System.Globalization;

//public partial class _Model : System.Web.UI.Page
//{
//    public string XmlLangInfo = "";
//    protected void Page_Load(object sender, EventArgs e)
//    {
//        NetCms.Vertical.ApplicationPage page = NetCms.Vertical.PagesHandler.DynamicUrlHandler.GetCurrentPage(Page, ViewState);

//        //SondaggiQuestionariUI.Controls.SQLoginControl login = new SondaggiQuestionariUI.Controls.SQLoginControl(System.Web.Configuration.WebConfigurationManager.AppSettings["usersDatabase"]);
//        h1.Attributes["class"] = "breadcrumbs";
//        h1.Controls.Add(new LiteralControl(page.PageTitle));
//        cx.Controls.Add(page.Control);
//        WebControl div = new WebControl(HtmlTextWriterTag.Div);
//        WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
//        WebControl link = new WebControl(HtmlTextWriterTag.Link);
//        link.Attributes.Add("default.aspx", "clicca qui");
//        legend.Controls.Add(new LiteralControl("<strong>Login</strong>"));
//        div.Controls.Add(legend);
//        div.Controls.Add(new LiteralControl("Se sei Amministratore <a title=\"home\" href= \"default.aspx \"> clicca qui </a>  per effettuare il Login"));
//        //if (!page.DisableLoginControl)
//        //    sx.Controls.Add(login);
//        //else
//        //    sx.Controls.Add(div);

//        sx.Controls.Add(page.InformationBox);
//        if (page.Toolbar.Enabled)
//            sx.Controls.Add(page.Toolbar);
//    }
////}



//using System;
//using System.Data;
//using System.Configuration;
//using System.Collections;
//using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
//using NetFrontend;
//using NetFrontend.FrontPiecies;
//using System.Globalization;

//public partial class _Model: System.Web.UI.Page
//{
//    public string XmlLangInfo = "";
//    protected void Page_Load(object sender, EventArgs e)
//    {
//        PageData PageData = new PageData(this.ViewState, this);
//        XmlLangInfo = PageData.XmlLangInfo;
//        VerticalNextGenBodyHandler handler = new VerticalNextGenBodyHandler(PageData, Page.Header);

//        Intestazione.Controls.Add(handler.Intestazione);

//        if (PageData.Homepage.ShowColDX)
//        {
//            ColDX.Controls.Add(handler.ColDX);
//        }
//        if (PageData.Homepage.ShowColSX)
//        {
//            ColSX.Controls.Add(handler.ColSX);
//        }

//        ColCX.Controls.Add(handler.BuildControl());

//        this.Where.Controls.Add(handler.Where);
//        this.Footer.Controls.Add(new Footer(PageData).GetControl());

//    }
//}



using System;
using System.Web.UI;
using NetFrontend;

public partial class _Model : System.Web.UI.Page
{
    public string HTML5XmlLangInfo = "";

    public int ColDxClass = 2;
    public int ColSxClass = 2;
    public int ColCxOffset = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        PageData PageData = new PageData(this.ViewState, this);
        PageData.Body = this.CMSBody;
        HTML5XmlLangInfo = PageData.HTML5XmlLangInfo;

        VerticalBodyHandler handler = new VerticalBodyHandler(PageData, Page.Header);

        Intestazione.Controls.Add(handler.Header);

        if (handler.LoginHeadModal && !NetCms.Users.AccountManager.Logged)
            LoginModal.Controls.Add(handler.ModalLoginBox());

        int colcx = 12;
        Where.Controls.Add(handler.Where);
        if (PageData.Homepage.ShowColDX)
        {
            colcx = colcx - ColDxClass;
            ColDX.Attributes.Add("class", "col-md-" + ColDxClass);
            ColDX.Controls.Add(PageData.Homepage.ColDX);

        }
        if (PageData.Homepage.ShowColSX)
        {
            colcx = colcx - ColSxClass;
            ColSX.Attributes.Add("class", "col-md-" + ColSxClass);
            ColSX.Controls.Add(PageData.Homepage.ColSX);
        }

        if (colcx == 12 && ColCxOffset > 0)
        {
            // setta l'offset della colonna centrale nel caso sia l'unica presente nel corpo della pagina
            colcx = colcx - (ColCxOffset * 2);
            ColCX.Attributes.Add("class", "col-md-" + colcx + " col-md-offset-" + ColCxOffset);
        }
        else
        {

            ColCX.Attributes.Add("class", "col-md-" + colcx);
        }

        ColCX.Controls.Add(handler.BuildControl());
        if (PageData.Homepage.ShowFooter)
            PiePagina.Controls.Add(PageData.Homepage.Footer);
        Footer.Controls.Add(handler.Footer);
    }
}