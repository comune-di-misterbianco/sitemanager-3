﻿<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>
        </div>
    </div>
    
	

	
	{{if sezione.childfolder | object.size > 0}}
		<div class="it-grid-list-wrapper">
        <div class="grid-row">
            {{for folder in sezione.childfolder}}
            <div class="col-6 col-lg-4">
                <div class="it-grid-item-wrapper">                  				 
                    <a href="{{folder.url}}">
                     {{if folder.image | string.size > 0 }}
                      <div class="img-responsive-wrapper">
                        <div class="img-responsive">
                          <div class="img-wrapper"><img src="{{folder.image.url}}?preset=moduloimgwide" alt="{{folder.image.Desc}}"></div>
                        </div>
                      </div>
                      {{end }}
                      <span class="it-griditem-text-wrapper">
                        <span class="it-griditem-text">{{folder.titolo}}</span>                    
                        <svg class="icon">
                          <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-code-circle"></use>
                        </svg>
                      </span>
                    </a>                                    
                </div>
            </div>
            {{  end }}	
		</div>
        </div>
    {{end}}
	
	

	{{if documents.size > 0}}
    <div class="it-grid-list-wrapper it-image-label-grid" id="Gallery-Table">
        <div class="grid-row">

        {{ for doc in documents }}
        <div class="col-6 col-lg-4">
            
                  <div class="it-grid-item-wrapper">
                    <a href="{{sezione.url}}{{doc.filename}}" data-toggle="lightbox" data-gallery="#Gallery-Table" data-footer="{{doc.titolo | string.replace "_" " "}}" >
                      <div class="img-responsive-wrapper">
                        <div class="img-responsive">
                          <div class="img-wrapper"><img src="{{sezione.url}}{{doc.content}}" alt="{{doc.titolo | string.replace "_" " "}}" title="{{doc.titolo | string.replace "_" " "}}"></div>
                        </div>
                      </div>
                      <span class="it-griditem-text-wrapper">
                        <span class="it-griditem-text">{{doc.titolo}}</span>
                        <svg class="icon">
                          <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-code-circle"></use>
                        </svg>
                      </span>
                    </a>
                  </div>                           
        </div>
        {{end }}
        
        </div>
                
        <script>
            $(document).on('click', '.it-grid-item-wrapper[data-toggle="lightbox"]', function (event) {
                event.preventDefault();
                $(this).ekkoLightbox({
                    loadingMessage: '<div class="ekko-lightbox-loader">Caricamento ...</div>',
                    leftArrow: '<span class="Icon Icon-chevron-left"></span>',
                    rightArrow: '<span class="Icon Icon-chevron-right"></span>',
                });
            });
        </script>
        </div>
       <!-- paginazion -->
		 <nav class="pagination-wrapper pagination-total" aria-label="Page navigation">			    
					<ul class="pagination">
						<li class="page-item"><a class="page-link text" href="#">Pagina {{ pager.current }} di {{ pager.pages }}</a><li>
						{{ if pager.current > 1}}
						<li class="page-item">
							<a class="page-link" href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}=1" aria-label="{{commonlabels.firstpage}}">
								<span class="fa fa-angle-double-left"></span>
								<span aria-hidden="true" class="sr-only">{{commonlabels.firstpage}}</span>
							</a>
						</li>
						<li class="page-item">
							<a class="page-link" href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current-1}}" aria-label="{{commonlabels.prevpage}}">
								<span class="fa fa-angle-left" role="presentation"></span>
								<span aria-hidden="true" class="sr-only">{{commonlabels.prevpage}}</span>
							</a>
						</li>
						{{end }}

						{{ for i in pager.lowrange..pager.toprange}}
						<li class="page-item {{ if  i == pager.current }} active {{end}}">
							<a class="page-link" {{if  i == pager.current }}aria-current="page"{{end}} href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{i}}">{{i}}</a>
						</li>
						{{end }}

						{{ if pager.current < pager.pages}}
						<li class="page-item">
							<a class="page-link" href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current+1}}" aria-label="{{commonlabels.nextpage}}">
								<span class="fa fa-angle-right" role="presentation"></span>
								<span aria-hidden="true" class="sr-only">{{commonlabels.nextpage}}</span>
							</a>
						</li>
						<li class="page-item">
							<a class="page-link" href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.pages}}" aria-label="{{commonlabels.lastpage}}">
								<span class="fa fa-angle-double-right" role="presentation"></span>
								<span aria-hidden="true" class="sr-only">{{commonlabels.lastpage}}</span>
							</a>
						</li>
						{{end }}
					</ul>
              </nav>  
		<!-- fine paginazion -->       
        
        {{end }}
    
	   {{if (sezione.childfolder | object.size == 0 && documents.size == 0 )}}
		<p class="u-text-p u-textNormal u-color-grey-60">{{commonlabels.norecord}}</p>
		{{end }}
</div>