﻿{{
maxitem = 5
itemcount = items | object.size
if items | object.size > 0
}}
<ul class="u-text-r-xs">
	{{ for doc in items limit:maxitem }}					     
		<li  class="u-text-r-xxs u-border-top-xxs u-padding-r-all {{if for.odd }}u-background-5{{end }}" > 							 
			<i class="fa fa-file-text-o fa-lg" aria-hidden="true"></i>
			<a class="" href="{{doc.url}}" title="{{ doc.descrizione}}">
				{{doc.titolo}}
			</a>
			{{if doc.descrizione != doc.titolo}}
			<p class="u-text-r-xxl u-margin-r-left  u-text-p u-textSecondary">{{doc.descrizione}}</p>
			{{end }}		 
        </li>
	{{end }}
	
	{{ if itemcount > maxitem }}
	<li class="u-textCenter u-text-md-right u-text-lg-right ">
		<a class="u-color-50 u-textClean u-text-h5" href="" title="Consulta gli altri documenti" >
			Tutti i documenti 
			<span class="Icon Icon-chevron-right"></span>
		</a>
	</li>
	{{ end }}	
<ul>
{{else}}
<p class="u-textLeft u-text-md-right u-text-h5">
	Consulta il contenuto della sezione 
	<a href="{{section.link}}" title="{{section.descrizione}}">{{section.titolo}}</a>
</p>
{{end }}