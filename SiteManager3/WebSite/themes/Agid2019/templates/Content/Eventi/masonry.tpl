﻿<div class="container">
   <!-- page int --> 
   <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1 class="text-center py-4"><span class="fa fa-calendar mx-3"></span>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>        
			 
                {{if sezione.currentcategory != "-1"}}
				<div class="">                
                <p>
                    <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;
                    {{labels.selectedcategory}} <strong>{{sezione.currentcategory}}</strong> 
						<a href="default.aspx" class="btn btn-sm btn-outline-danger text-danger"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;{{labels.removefilter}}</a>
                </p>
				</div>
                {{end}}
				 {{if sezione.currentplace != "-1"}}
				<div class="filter">                
                <p>
                    <i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;
                    {{labels.selectedplace}} <strong>{{sezione.currentplace}}</strong> 
                    <a href="default.aspx" class="btn btn-sm btn-outline-danger text-danger"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;{{labels.removefilter}}</a>
                </p>
				</div>
                {{end}}
				
				
				<p class="text-lg-right pb-4">
				{{if sezione.showallitem == false}}
					<a class="btn btn-sm btn-outline-primary" href="default.aspx?showall=true">{{labels.showallevent}}</a>
				{{else}}
					<a class="btn btn-sm btn-outline-primary" href="default.aspx">{{labels.showscheduledevent}}</a>
				{{end}}
				</p>

        </div>
    </div>

	<!-- end page int -->
	{{if documents.size > 0 }}
    <div class="it-grid-list-wrapper it-image-label-grid it-masonry">
    <div class="card-columns">						  
	
		{{for doc in documents }}
				<div class="col">
					<div class="it-grid-item-wrapper">
						<div class="card-wrapper card-space ">
							{{					
							showimg = false			
							if doc.locandina != null 
								if doc.locandina.show != false
									showimg = true
								end
							end							
							}}		
							<div class="card {{if showimg != false }}card-img{{end }} rounded shadow no-after">
								{{if showimg != false  }}
									{{if doc.locandina.src | string.size > 0 }}
									<a href="{{sezione.url}}default.aspx?evento={{doc.id}}">
									<div class="img-responsive-wrapper">
									<div class="img-responsive">
										<div class="img-wrapper">												
											<img src="{{doc.locandina.src}}?w=500&h=300&mode=max" class="img-fluid" alt="{{doc.locandina.Description}}" />
										</div>
									</div>
									</div>
									</a>
									<div class="event-category"><i class="fa fa-tag mr-1 text-primary" aria-hidden="true"></i> 
										<a href="default.aspx?cat={{doc.idcategoria}}&showall=true"><span class="d-none">{{labels.categoria}}: </span> {{doc.categoria}}</a>
									</div>
									{{end }}
								{{end }}
								<div class="card-body">	
									{{if showimg == false  }}										
											<div class="event-category-noimg p-2 text-right mb-4">
													<i class="fa fa-tag mr-1 text-primary" aria-hidden="true"></i>
													<a class="category" href="default.aspx?cat={{doc.idcategoria}}&showall=true"><span class="d-none">{{labels.categoria}}:</span> {{doc.categoria}}</a>													
											</div>										
									{{end}}
									<h4 class="card-title">
										<a class="u-text-r-m u-color-black u-textWeight-400 u-textClean" href="{{sezione.url}}default.aspx?evento={{doc.id}}">{{doc.titolo}}</a>
									</h4>
									<p class="card-text">
										<i class="fa fa-calendar-o" aria-hidden="true"></i> {{doc.periodo}} 										
										<br />
										{{if doc.altredate != "" }} 							
											<br />
											<i class="fa fa-calendar-o" aria-hidden="true"></i>
											{{labels.altredate}}: 
											{{ for repaeatdata in doc.altredate | string.split '-' }}											
											<span class="badge badge-info">{{ repaeatdata }} </span>								
											{{ end }}
										{{end}}	
									</p>
									<hr />
									<p class="card-text m-0">	
										<i class="fa fa-map-marker mr-1 text-primary" aria-hidden="true"></i>
										{{labels.place}}: <a class="place" href="default.aspx?place={{doc.idluogo}}&showall=true"> {{doc.luogo}}</a>									
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
		{{end }}
		</div>	
	</div>

		<!-- fine corpo -->

		<!-- paginazion -->
	<nav class="pagination-wrapper pagination-total" aria-label="Page navigation">			    
		<ul class="pagination">
			<li class="page-item disabled"><a class="page-link text" href="#">{{commonlabels.page}} {{ pager.current }} {{commonlabels.of}} {{ pager.pages }}</a><li>
			{{ if pager.current > 1}}
			<li class="page-item">
				<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" 1 }}" aria-label="{{commonlabels.firstpage}}">
					<span class="fa fa-angle-double-left"></span>
					<span aria-hidden="true" class="sr-only">{{commonlabels.firstpage}}</span>
				</a>
			</li>
			<li class="page-item">
				<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" pager.current-1 }}" aria-label="{{commonlabels.prevpage}}">
					<span class="fa fa-angle-left" role="presentation"></span>
					<span aria-hidden="true" class="sr-only">{{commonlabels.prevpage}}</span>
				</a>
			</li>
			{{end }}

			{{ for i in pager.lowrange..pager.toprange}}
			<li class="page-item {{ if  i == pager.current }} active {{end}}">
				<a class="page-link" {{if  i == pager.current }}aria-current="page"{{end}} href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" i }}">{{i}}</a>							
			</li>
			{{end }}

			{{ if pager.current < pager.pages}}
			<li class="page-item">
				<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" pager.current+1 }}" aria-label="{{commonlabels.nextpage}}">
					<span class="fa fa-angle-right" role="presentation"></span>
					<span aria-hidden="true" class="sr-only">{{commonlabels.nextpage}}</span>
				</a>
			</li>
			<li class="page-item">
				<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" pager.pages}}" aria-label="{{commonlabels.lastpage}}">
					<span class="fa fa-angle-double-right" role="presentation"></span>
					<span aria-hidden="true" class="sr-only">{{commonlabels.lastpage}}</span>
				</a>
			</li>
			{{end }}
		</ul>
	</nav>  
	<!-- fine paginazion -->	

	{{else}}
		
		<p>{{commonlabels.norecord}}</p>
      
	{{end}}

</div>