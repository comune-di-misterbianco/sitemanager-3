﻿<div class="container">
    <div class="row">
		<div class="col-lg-10">
        	<h1 class="h3 pr-lg-4 mt-4 mt-md-0">{{document.titolo}}</h1>     		
			<p class="description d-print-none d-none">{{document.descrizione}}</p>	

			<div class="avatar-wrapper avatar-extra-text mb-4 d-print-none">
				<div class="extra-text d-flex flex-lg-column ml-0">					
					<h4 class="text-info">
						<svg class="icon icon-info"><use href="/bootstrap-italia/dist/svg/sprite.svg#it-calendar"></use></svg>
					{{ if document.al == "null" }}
					{{labels.dataevento}}: {{ document.dal }}
					{{else}}
					{{labels.periodo}}: dal {{ document.dal }} al {{ document.al }}
					{{end}}
					</h4>
				</div>			
			</div>	
			
			
				{{if document.altredate != "" }} 	
				<p>						
					<i class="fa fa-calendar-o" aria-hidden="true"></i>
					{{labels.altredate}}: 
					{{ for repaeatdata in document.altredate | string.split '-' }}											
					<span class="badge badge-info">{{ repaeatdata }} </span>								
					{{ end }}
				</p>
				{{end}}	
			
        </div>
		<div class="col-lg-2 text-lg-right">
			<div class="avatar-wrapper avatar-extra-text mb-4 small">
				<div class="extra-text d-flex flex-lg-column ml-0">
					<strong class="d-block mr-2 mr-lg-0">{{commonlabels.labelpublishedon}}:</strong>			
					<time datetime="{{document.datapub}}" class="text-capitalize">{{document.datapub}}</time>
				</div>

			</div>
		</div>
		<div class="col-12">
				<div class="btn-toolbar justify-content-end justify-content-lg-between border-top border-bottom border-light position-relative py-2 my-4 d-print-none" role="toolbar" aria-label="Strumenti">
					<a href="javascript:this.print();" title="{{commonlabels.printtitle}}">
						<svg class="icon icon-sm mx-auto mx-lg-0"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-print"></use></svg>
						<span>{{commonlabels.print}}</span>
					</a>
					{{sharecontent}}
				</div>		
		</div>		
	</div><!-- /row -->
</div>  

{{if document.locandina != "null" }}		     
<div class="container-fluid mt-2 mb-4 p-0">				
	<div class="imgevidenza">
		<figure>
			<img src="{{document.locandina.src}}" class="img-fluid" alt="{{document.locandina.descrizione}}" />
			<figcaption>{{document.locandina.descrizione}}</figcaption>
		</figure>
	</div>	
</div>
{{end}}
<div class="container">
	<div class="row">
		<div class="col-md-12 col-lg-12">
		
		{{if sezione.menu == null }}
		<article class="article-body">	
			<div class="mt-2">
					<address class="bg-white">
					<p class="p-2">					
						<i class="fa fa-map-marker" aria-hidden="true"></i> {{labels.place}}: <a class="place" href="default.aspx?place={{document.idluogo}}&showall=true"> <strong>{{document.luogo.nome}} </strong></a>{{document.luogo.indirizzo}}, {{document.luogo.citta}}							
					</p>
					</address>
			</div>		
			<div class="page-content">
				{{
					testo = document.testo | regex.replace "(<h1.*?>.*?</h1>)" "" "$"
					test = testo | regex.replace "(style=.*?)" "" ""
					testo
				}}
			</div>					
		</article>
		{{else }}
			<aside class="col-lg-4">
				<div class="menu-wrapper">                
					<div class="link-list-wrapper menu-link-list">
						{{sezione.menu}}
					</div>                              
				</div>
			</aside>
			<section class="col-lg-8">
				<article class="pagina">
					<div class="mt-2">
						<address>		
							<h4 class="h5 pt-2">{{labels.place}}</h4>							
							<i class="fa fa-map-marker" aria-hidden="true"></i><strong> {{document.luogo.nome}} </strong>{{document.luogo.indirizzo}}, {{document.luogo.citta}}							
							{{if document.altredate != "" }} 							
											<br />
											<i class="fa fa-calendar-o" aria-hidden="true"></i>
											{{labels.altredate}}: 
											{{ for repaeatdata in document.altredate | string.split '-' }}											
											<span class="badge badge-info">{{ repaeatdata }} </span>								
											{{ end }}
							{{end}}		
						</address>
					</div>				
				<div class="page-content">{{document.testo}}</div>
				</article>
			</section>
		{{end}}
		</div><!-- /col -->
	</div><!-- /row -->
	<div class="border-top border-light">
		<h4 class="h5 pt-2">{{commonlabels.moreinfo}}</h4>
		<!-- revisione -->
		<div class="avatar-wrapper avatar-extra-text my-4">
			<div class="extra-text d-flex flex-lg-column ml-0">
				<strong class="d-block mr-2">{{commonlabels.lastrevision}}:</strong>
				<time datetime="{{document.datarev}}" class="text-capitalize">{{document.datarev}}</time>
			</div>
		</div>

		<!-- argomenti/TAGS -->
		{{if document.tags | object.size > 0}}
		<div class="mt-2 mb-4 d-print-none">
			<h6><small>{{commonlabels.tagstitle}}</small></h6>
			{{ for tag in document.tags }}
				<a class="text-decoration-none" rel="tag" href="/tags/argomenti.aspx?tag={{tag.key}}">
					<div class="chip chip-simple chip-primary">
						<span class="chip-label">{{tag.label | string.capitalizewords}}</span>
					</div>
				</a>
			{{end}}
		</div>
		{{end}}

	</div>
</div><!-- /container -->	
	