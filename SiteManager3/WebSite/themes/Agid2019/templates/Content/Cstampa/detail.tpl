﻿<div class="container my-4">
    <div class="row">
		<div class="col-lg-8 px-lg-4 py-lg-2">
        <h1>{{document.titolo}}</h1>     		
		    			
           <p class="description">{{document.descrizione}}</p>				
			<article class="article-body">
					<div class="row mt-5 mb-4">
						<div class="col">
							<small>{{commonlabels.lastrevision}}:</small>
							<p class="font-weight-semibold categoria">							
							{{ if document.datarev != "false"}} {{document.datarev}}{{end}}
							</p>
						</div>
						<div class="col">
							<small>Tempo di lettura:</small>
							<p class="font-weight-semibold reading-time"></p>
						</div>
						<div class="col">
							<h6>{{labels.categoria}}</h6>
							<div class="chip chip-simple chip-primary">
								<span class="chip-label">{{ document.categoria}}</span>
							</div>
						</div>
					</div>
					
					{{if document.img.showindetail == true }}
					<div class="imgevidenza">
						<img src="{{document.img.src}}" class="img-fluid" alt="{{document.img.descrizione}}" />
					</div>
					{{end}}
					
				<div class="page-content">
					{{
						testo = document.testo | regex.replace "(<h1.*?>.*?</h1>)" "" "$"
						test = testo | regex.replace "(style=.*?)" "" ""
						testo
					}}
				</div>				
						
            </article>

        </div>
		<div class="col-lg-3 offset-lg-1">
			{{sharecontent}}

			<!-- Azioni -->
			<div class="dropdown d-inline">
			  <button class="btn btn-dropdown dropdown-toggle" type="button" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<svg class="icon">
				  <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-more-items"></use>
				</svg>
				<small>Azioni</small>
			  </button>
			  <div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
				<div class="link-list-wrapper">
				  <ul class="link-list">
					<li>
					  <a class="list-item" href="#">
						<svg class="icon">
						  <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-download"></use>
						</svg>
						<span>Scarica</span>
					  </a>
					</li>
					<li>
					  <a class="list-item" href="javascript:this.print();" title="{{commonlabels.printtitle}}">
						<svg class="icon">
						  <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-print"></use>
						</svg>
						<span>{{commonlabels.print}}</span>
					  </a>
					</li>					
					<li>
					  <a class="list-item" href="mailto:?subject={{sezione.titolo}}&body={{sezione.descrizione}}">
						<svg class="icon">
						  <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-mail"></use>
						</svg>
						<span>Invia</span>
					  </a>
					</li>
				  </ul>
				</div>
			  </div>
			</div>
			<!-- Fine azioni -->

			<!-- argomenti/TAGS -->
			{{if document.tags | object.size > 0}}
			<div class="mt-4 mb-4">
				<h6><small>{{commonlabels.tagstitle}}</small></h6>
				{{ for tag in document.tags }}
					<a class="text-decoration-none" rel="tag" href="/tags/argomenti.aspx?tag={{tag.key}}">
						<div class="chip chip-simple chip-primary">
							<span class="chip-label">{{tag.label | string.capitalizewords}}</span>
						</div>
					</a>
				{{end}}
			</div>
			{{end}}
			<!-- END argomenti/TAGS -->

		</div>
			
        
        </div>
    </div>