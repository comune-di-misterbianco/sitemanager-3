﻿<div class="container">
	<div class="row">
		<div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class=" searchFieldset">
			<p class="text-right">
				<a href="#archivio-storico" class="btn btn-secondary" title="Ricerca per anno e mese">
					<span class="text-white">Archivio storico</span>
				</a>
				<button class="btn btn-primary " data-toggle="collapse" type="button" data-target="#comunicatiForm" >
					 {{labels.cercastampa}}
				</button>
			</p>
				<div class="searchForm collapse" id="comunicatiForm" style="">
					<div id="SearchForm" class="Axf" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'SearchForm_submit')">
						<div class="col-md-12">
							<div class="page-header title-vfm mb-4">
								<h4>{{labels.searchformtitle}}</h4>
							</div>
							<div class="form-group validatedfield textbox-vf ">								
								<input name="Nome" type="text" id="Nome" class="form-control" />
								<label for="Nome">{{labels.titolo}}</label>
							</div>
							<div class="form-group validatedfield col textbox-vf ">								
									<input name="Descrizione" type="text" id="Descrizione" class="form-control" />
									<label for="Descrizione">{{labels.descrizione}}</label>
							</div>		
							<div class="form-row">
								<div id="DC_DataDal" class="it-datepicker-wrapper col">							
									<div class="form-group validatedfield textbox-vf">							
										<input name="DataDal" type="text" id="DataDal" class="form-control it-date-datepicker" placeholder="{{labels.dateformatplaceholder}}" />
										<label for="DataDal" class="control-label">{{labels.data}} {{labels.successiveal}}</label>
									</div>
								</div>
								<div id="DC_DataAl" class="it-datepicker-wrapper col">	
									<div class="form-group validatedfield textbox-vf">
										<input name="DataAl" type="text" id="DataAl" class="form-control it-date-datepicker" placeholder="{{labels.dateformatplaceholder}}" />
										<label for="DataAl">{{labels.data}} {{labels.antecedential}}</label>							
									</div>
								</div>
							</div>
							<script type="text/javascript">				
								   $(document).ready(function() {									 
									  $('.it-date-datepicker').datepicker({
										  inputFormat: ["dd/MM/yyyy"],
										  outputFormat: 'dd/MM/yyyy',
										});
								   });
							</script>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group text-center vf back-vf">
										<input type="submit" name="SearchForm_back" value="{{labels.azzeraricerca}}" id="SearchForm_back" class="btn btn-outline-secondary">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group text-center vf submit-vf">
										<input type="submit" name="SearchForm_submit" value="{{labels.labelsearchbutton}}" id="SearchForm_submit" class="btn btn-primary text-white">
									</div>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
   </div>
   <!-- page int --> 
   <div class="row">
        <div class="col-md-12">            
            <div class="">                
                {{if sezione.currentcategory != "-1"}}
                <p>
                    <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;
                   {{labels.selectedcategory}} <strong>{{sezione.currentcategory}}</strong> 
                    <a href="default.aspx"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;{{labels.removefilter}}</a>
                </p>
                {{end}}
            </div>

        </div>
    </div>

	<!-- end page int -->

	<div class="it-grid-list-wrapper it-image-label-grid it-masonry">
    <div class="card-columns">
	{{if documents.size > 0 }}
		{{for doc in documents }}
		<div class="col-12">
			<div class="it-grid-item-wrapper">
				<div class="card-wrapper card-space ">
				
					<div class="card {{if doc.coverimage != false }}card-img{{end }} rounded shadow">
						{{if doc.coverimage != null && doc.coverimage != false }}
							  {{if doc.coverimage.src | string.size > 0 }}
							  <div class="img-responsive-wrapper">
								<div class="img-responsive">
									<div class="img-wrapper"><img src="{{doc.coverimage.src}}" class="u-sizeFull" alt="{{doc.coverimage.description}}" /></div>
								</div>
							  </div>
							  {{end }}
						{{end }}
						<div class="card-body">								
							<div class="category-top">
								<a class="category" href="default.aspx?cat={{doc.idcategoria}}">{{doc.categoria}}</a>
									<span class="data">{{doc.data}}</span>
							</div>
							<h5 class="card-title">
								<a class="u-text-r-m u-color-black u-textWeight-400 u-textClean" href="?cs={{doc.id}}">{{doc.titolo}}</a>
							</h5>
							<p class="card-text">{{doc.descrizione}}</p>
						</div>
					</div>
				</div>
			</div>
		</div>				
		{{end }}
	</div>
	</div>		
		 <!-- paginazion -->
		 <nav class="pagination-wrapper pagination-total" aria-label="Page navigation">			    
					<ul class="pagination">
						<li class="page-item disabled"><a class="page-link text" href="#">Pagina {{ pager.current }} di {{ pager.pages }}</a><li>
						{{ if pager.current > 1}}
						<li class="page-item">
							<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" 1 }}" aria-label="{{commonlabels.firstpage}}">
								<span class="fa fa-angle-double-left"></span>
								<span aria-hidden="true" class="sr-only">{{commonlabels.firstpage}}</span>
							</a>
						</li>
						<li class="page-item">
							<a class="page-link" href="{{sezione.url}}{{sezione.pagename}}?{{pager.pageandquerystring | string.replace "{0}" pager.current-1 }}" aria-label="{{commonlabels.prevpage}}">
								<span class="fa fa-angle-left" role="presentation"></span>
								<span aria-hidden="true" class="sr-only">{{commonlabels.prevpage}}</span>
							</a>
						</li>
						{{end }}

						{{ for i in pager.lowrange..pager.toprange}}
						<li class="page-item {{ if  i == pager.current }} active {{end}}">
							<a class="page-link" {{if  i == pager.current }}aria-current="page"{{end}} href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" i }}">{{i}}</a>
						</li>
						{{end }}

						{{ if pager.current < pager.pages}}
						<li class="page-item">
							<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" pager.current+1 }}" aria-label="{{commonlabels.nextpage}}">
								<span class="fa fa-angle-right" role="presentation"></span>
								<span aria-hidden="true" class="sr-only">{{commonlabels.nextpage}}</span>
							</a>
						</li>
						<li class="page-item">
							<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" pager.pages}}" aria-label="{{commonlabels.lastpage}}">
								<span class="fa fa-angle-double-right" role="presentation"></span>
								<span aria-hidden="true" class="sr-only">{{commonlabels.lastpage}}</span>
							</a>
						</li>
						{{end }}
					</ul>
         </nav>  
		 <!-- fine paginazion -->

		<!-- archivio storico -->
		{{ sezione.archivio.mesi.size}}
		<div class="row">
			<div class="col-md-12">
				<div class="page-header"><h4>{{labels.archiviostorico}}</h4></div>
				<div class="archivioStorico row" id="archivio-storico">
					{{
					anni = sezione.archivio.anni | object.size
					if anni > 0
					}}
					<div class="col">						
						<ul class="list-inline">
							{{for item in sezione.archivio.anni }}
							<li class="list-inline-item"><a class="btn btn-default {{if sezione.archivio.selectedyear == item  }} selected {{end}}" href="default.aspx?anno={{item}}">{{item}}</a></li>
							{{end}}
						</ul>						
					</div>
					{{end}}
					{{
					mesi = sezione.archivio.mesi | object.size
					if mesi > 0
					}}
					<div class="col">						
						<ul class="list-inline">
							{{for item in sezione.archivio.mesi }}
							<li class="list-inline-item"><a class="btn btn-default {{if sezione.archivio.selectedmonth == item.month  }} selected {{end}}" href="default.aspx?anno={{sezione.archivio.selectedyear}}&mese={{item.month}}">{{item.monthname}}</a></li>
							{{end}}
						</ul>						
					</div>
					{{end}}
					{{
					days = sezione.archivio.giorni | object.size
					if days > 0 
					}}
					<div class="col">						
						<ul class="list-inline">
							{{for item in sezione.archivio.giorni }}
							<li class="list-inline-item"><a class="btn btn-default" href="default.aspx?anno={{sezione.archivio.selectedyear}}&mese={{item.month}}&giorno={{item.day}}">{{item.day}}</a></li>
							{{end}}
						</ul>						
					</div>
					{{end}}
				</div>
			</div>
		</div>
		<!-- fine archivio storico -->
	{{else}}		
		<p>{{commonlabels.norecord}}</p>      
	{{end}}

</div>