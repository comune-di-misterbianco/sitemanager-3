﻿{{if data.items.size > 0 }}
<div class="py-4">
 <div class="row">
		<h3 class="mb-1 block-title text-primary"> {{ data.modulo.label }}</h3>
</div>
<div class="it-carousel-wrapper it-carousel-landscape-abstract-three-cols">
  <div class="it-carousel-all owl-carousel it-img-card">
  
    {{for item in data.items }}

	<div class="it-single-slide-wrapper">
      <div class="card-wrapper">
        <div class="card">
		   {{if data.modulo.showimages && item.img != null && item.img != false }}
				<div class="img-responsive-wrapper">
					<div class="img-responsive">
						<div class="img-wrapper"><img class="img-fluid" src="{{item.img.imgsrc}}" alt="{{item.img.imgalt}}" title="{{item.img.imgalt}}"></div>
					</div>
				</div>
		  {{end}}
          <div class="card-body">
           
		   <div class="category-top">		   
			  
              <a class="category" href="{{item.itemcategoryurl}}">{{item.categoria | string.capitalizewords}}</a>
              <span class="data">			
			  {{item.date}}
			  </span>
            </div>
            {{if item.titolo != false}}
		    <h5 class="card-title big-heading">{{item.titolo}}</h5>
            {{end}}
		   
		   <p class="card-text">{{item.descrizione}}</p>
           
		   <a class="read-more" href="{{item.itemurl}}">
              <span class="text">{{commonlabels.readmore}}</span>
              <svg class="icon">
                <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-arrow-right"></use>
              </svg>
            </a>

          </div>
        </div>
      </div>
    </div>   

	{{end}}

  </div>
  {{if data.modulo.itemshowmore != false }}
		<a class="read-more" href="{{data.modulo.url}}">
                <span class="text">{{data.modulo.showmorelabel}}</span>
                <svg class="icon"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-arrow-right"></use></svg>
        </a>
    {{end}}
</div>
</div>
{{end}}