﻿<div class="it-hero-wrapper it-dark it-overlay it-bottom-overlapping-content">
   {{if data.modulo.img.imgsrc != "false" }}
   <div class="img-responsive-wrapper">
      <div class="img-responsive">
         <div class="img-wrapper"><img src="{{data.modulo.img.imgsrc}}" alt="{{data.modulo.img.imgalt}}" title="{{data.modulo.img.imgalt}}"></div><!-- inserire img hero full size "wide" o costruire il path manualmente inserendo il preset alla fine-->
      </div>
   </div>
   {{end}}
   <div class="container">
      <div class="row">
         <div class="col-12">
            <div class="it-hero-text-wrapper bg-dark">
               <!--<span class="it-category">Category</span>-->
               <h1 class="no_toc">{{data.modulo.titolo}}</h1>
               <p class="d-none d-lg-block">{{data.modulo.testo}}</p>
               <div class="it-btn-container"><a class="btn btn-sm btn-secondary" href="{{data.modulo.url}}">{{commonlabels.readmore}}</a></div>
            </div>
         </div>
      </div>
   </div>
</div>