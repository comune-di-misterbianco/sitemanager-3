﻿{{if data.items.size > 0 }}
<div class="it-carousel-wrapper col-md-12 col-lg-12">
  <div class="it-carousel-all owl-carousel it-img-card">
   
    {{for item in data.items  limit:9}}
	<div class="it-single-slide-wrapper">

      <div class="card-wrapper">
        <div class="card card-img no-after">
          {{if item.img != false }}
		  <div class="img-responsive-wrapper">
            <div class="img-responsive">
              <div class="img-wrapper">
				<a href="{{item.itemurl}}" {{if item.img.imgname != false}} data-footer="{{item.img.imgname | string.replace "_" " "}}" {{end }} data-toggle="lightbox" data-gallery="#GalleryTable" data-type="image" >
					<img src="{{item.img.imgsrc}}" title="{{item.img.imgname}}" alt="{{item.img.imgalt}}">
				</a>
			  </div>
            </div>
          </div>
		  {{end}}        
        </div>
      </div>

    </div>   
	{{end}}

  </div>

  {{if data.modulo.itemshowmore != false }}
		<a class="read-more" href="{{data.modulo.url}}">
                <span class="text">{{data.modulo.showmorelabel}}</span>
                <svg class="icon"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-arrow-right"></use></svg>
        </a>
  {{end}}


  <script>
            $(document).on('click', '[data-toggle="lightbox"]', function (event) {
                event.preventDefault();
                $(this).ekkoLightbox({
                    loadingMessage: '<div class="ekko-lightbox-loader">Caricamento ...</div>',                               
                });
            });
     </script>
</div>
{{end}}