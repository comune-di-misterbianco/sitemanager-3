﻿<div class="row">
  <div class="col-12 col-lg-6">
    <!--start card-->
    <div class="card-wrapper">
      <div class="card">
        <div class="card-body">
		  {{if data.modulo.titolo != "false"}}
			<h5 class="card-title big-heading">{{data.modulo.titolo}}</h5>
		  {{end}}
          <p class="card-text">{{data.modulo.testo}}</p>          
          <a class="read-more" href="{{data.modulo.url}}">
            <span class="text">{{commonlabels.readmore}}</span>
            <svg class="icon">
              <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-arrow-right"></use>
            </svg>
          </a>
        </div>
		{{if data.modulo.img.imgsrc != "false" }}
		<div class="img-responsive-wrapper">
			<div class="img-responsive">
				<div class="img-wrapper">
					<img class="img-fluid" src="{{data.modulo.img.imgsrc}}" alt="{{data.modulo.img.imgalt}}" title="{{data.modulo.img.imgalt}}">
				</div>
			</div>
		</div>
		{{end}}
      </div>
    </div>
    <!--end card-->
  </div>
</div>