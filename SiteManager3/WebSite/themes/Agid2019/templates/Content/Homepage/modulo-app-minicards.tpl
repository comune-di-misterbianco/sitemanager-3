﻿<div class="row">
	<h3 class="mb-1 block-title text-primary"> 
	<a href="{{data.modulo.url}}">{{ data.modulo.label }}</a>
	</h3>
</div>
{{if data.items.size > 0 }}
<div class="row mx-lg-n3">                
	{{for item in data.items}}
	<div class="col-md-6 col-lg-4 px-lg-3 pb-lg-3">          
		<a href="{{item.itemurl}}" class="minicard rounded shadow ">
	        {{if data.modulo.showimages && item.img != null}}
			<div class="minicard-image">
                 <img src="{{item.img.imgsrc}}" class="img-fluid" alt="{{item.img.imgalt}}" title="{{item.img.imgname}}">    <!-- inserire img thumb o costruire il path manualmente inserendo il preset alla fine-->
			</div>
			 {{end}}
			<div class="minicard-text p-2">
				<h4>{{item.titolo}}</h4>
				<p>{{item.descrizione}}</p>
			</div>
		</a>
     </div>
	 {{end}}
</div>
{{if data.modulo.itemshowmore != false }}
	<p class"text-right">
		<a class="read-more" href="{{data.modulo.url}}">
				<span class="text">{{data.modulo.showmorelabel}}</span>
				<svg class="icon"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-arrow-right"></use></svg>
		</a>
	</p>
{{end}}
{{end}}