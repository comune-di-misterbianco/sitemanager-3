﻿<div class="container">
	<div class="my-5 text-center">
        <h2 class="u-text-h2 u-color-50 u-layout-medium label-container"><span class="fa fa-calendar mx-3"></span>{{data.modulo.label}}</h2>
        <p class="buttons-container">
			<span class="u-text-h3 u-color-50 u-layout-medium"><b>{{string.capitalizewords data.rangeeventsdates}}</b></span>
		</p>
	</div>
	<div class="event-calendar-container">
        <p class="calendar-container">
        {{for day in data.eventsidanddatedictionary}}
            {{$dateparsed = date.parse day.key | date.to_string '%d %b %Y'}}
            {{$datenowparsed = date.parse date.now | date.to_string '%d %b %Y'}}
            <a title="{{labels.Data}} {{$dateparsed}}" class="button-calendar-link{{if day.value | array.size > 0}} has-event{{if $dateparsed == $datenowparsed}} has-event-today{{end}}{{end}}" onclick="openEventCalendar(this);">{{ date.parse day.key | date.to_string '%d<br>%h' | string.upcase }}
            {{if day.value | array.size > 0}}
                <i class="fa fa-circle has-event-icon"></i>
                <input class="hidden-event-ref" type="hidden" value="{{for id in day.value}}{{id}};{{end}}">
            {{end}}
            </a> 
        {{end}}
        </p>
        <div class="container-hidden-event-calendar">
            {{for doc in data.items}}
            <div class="hidden-event-calendar col-lg-4" id="event-id-{{doc.id}}">
                <div class="event-calendar m-3 p-3">
                    <div class="card-img no-after">
                        {{if doc.img != null}}
                        <div class="img-responsive-wrapper">
                            <div class="img-responsive">         
                                <figure class="img-wrapper">
                                    <img src="{{doc.img.imgsrc}}" title="{{doc.img.imgalt}}" alt="{{doc.img.imgalt}}">
                                </figure>
                            </div>
                        </div>
                        {{end}}
						<div class="u-text-r-s u-padding-r-top">
							<div class="info_evento">								
								<p><i class="fa fa-clock-o mr-1" aria-hidden="true"></i>{{if doc.al != "null"}} {{labels.Dal}} {{end}}{{ doc.dal }}</p>
                                {{if doc.al != "null"}}
                                <p>{{labels.Al | string.downcase}} {{ doc.al }}</p>
                                {{end}}
								<p><i class="fa fa-sticky-note mr-1" aria-hidden="true"></i>{{labels.Categoria}}: <a href="{{doc.itemcategoryurl}}{{doc.categoria.id}}">{{doc.categoria.nome}}</a></p>
							</div>
							<div class="luogo_evento">
								<address>	
									<i class="fa fa-map-marker" aria-hidden="true"></i>
									<strong>{{doc.luogo.nome}}</strong>
								</address>
							</div>
						</div>
						<div class="news-text">
							<p>{{doc.descrizione}}</p>
						</div>
						<h5>{{doc.titolo}}</h5>
						<a class="read-more" href="{{doc.itemurl}}"><span class="text">{{commonlabels.readmore}}</span>
						    <i class="fa fa-arrow-right mx-1" aria-hidden="true"></i>			                       
						</a>
					</div>
				</div>
		    </div>
            {{end}}
        </div>
    </div>
    {{if data.modulo.itemshowmore != false }}
	<a class="read-more" href="{{data.modulo.url}}">
            <span class="text">{{data.modulo.showmorelabel}}</span>
            <svg class="icon"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-arrow-right"></use></svg>
    </a>
   {{end}}
</div>
