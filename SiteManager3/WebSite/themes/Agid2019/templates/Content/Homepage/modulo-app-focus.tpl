﻿<div class="py-4">
	<div class="row">
		<h3 class="mb-4 block-title text-primary"> {{ data.modulo.label }}</h3>
	</div>
	<div class="container">
	{{if data.items.size > 0}}
	{{
		itemcount = data.items.size		
	}}    	
		<div class="row mx-lg-n3">
		{{for item in data.items limit:3 }}
		<!-- inizio card -->
		<div class="{{if itemcount > 2}}col-md-6 col-lg-4 px-lg-3 pb-lg-3{{else}}col-md-12 col-lg-12 px-lg-3 pb-lg-3 {{end}}">
			<div class="card-wrapper card-space ">  		
				<div class="card {{if data.modulo.showimages && item.img != null }}card-img{{end}} rounded shadow">		
						{{if data.modulo.showimages && item.img != null }}
						<div class="img-responsive-wrapper">
						   <div class="img-responsive">
							   <div class="img-wrapper">
									<img class="img-fluid" src="{{item.img.imgsrc}}" alt="{{item.img.imgalt}}" title="{{item.img.imgalt}}">
							   </div>
						   </div>
						 </div>
						 {{end}}

						<div class="card-body">
						  <div class="category-top">
							   <!-- icon -->
							   <!-- categoria -->
							   <a class="category" href="{{item.itemcategoryurl}}" >{{item.categoria | string.capitalizewords}}</a>
							   <span class="data"> {{item.date}}</span> 
						  </div>
						  <h5 class="card-title"> {{item.titolo}}</h5>
						  <p class="card-text">{{item.descrizione}}</p>
						  <a class="read-more" href="{{item.itemurl}}">
							<span class="text">{{commonlabels.readmore}}</span>
							<svg class="icon">
							  <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-arrow-right"></use>
							</svg>
						  </a>
						</div>
				</div>
			</div>
		</div>
		{{end}} <!-- fine card -->
		</div>	
    {{end }}
	{{if data.modulo.itemshowmore != false }}
		<a class="read-more" href="{{data.modulo.url}}">
                <span class="text">{{data.modulo.showmorelabel}}</span>
                <svg class="icon"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-arrow-right"></use></svg>
        </a>
    {{end}}
	</div>

</div>


