﻿{{if data.items.size > 0 }}
<div class="py-4">

<div class="it-carousel-wrapper it-carousel-landscape-abstract-three-cols splide">

  <div class="it-header-block">
		<h2 class="it-header-block-title">
			<a  href="{{data.modulo.url}}"> {{ data.modulo.label }}</a>
		</h2>
  </div>

 <!-- <div class="it-carousel-all owl-carousel it-img-card">-->
  <div class="splide__track pl-lg-3 pr-lg-3">
    <ul class="splide__list it-carousel-all">
  
    {{for item in data.items }}

	<li class="splide__slide">
      <div class="it-single-slide-wrapper">
      <div class="card-wrapper card-space">
        <div class="card card-bg">	
          <div class="card-body">
 		   
            {{if item.titolo != false}}
		    <h5 class="card-title"><a href="{{item.itemurl}}">{{item.titolo}}</a></h5>
            {{end}}
			  {{if data.modulo.showimages &&  item.img != null && item.img != false }}
				<div class="img-responsive-wrapper">
					<div class="img-responsive">
						<div class="img-wrapper">
							<a href="{{item.itemurl}}">
								<img class="img-fluid" src="{{item.img.imgsrc}}" alt="{{item.img.imgalt}}" title="{{item.img.imgalt}}">
							</a>
						</div>
					</div>
				</div>
		  {{end}}
		  
		
		   {{if data.modulo.itemshowdesc != false}}
			<p class="card-text">{{item.descrizione}}</p>
		  {{end}}
		   <a class="read-more" href="{{item.itemurl}}">
              <span class="text">{{commonlabels.readmore}}</span>
              <svg class="icon">
                <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-arrow-right"></use>
              </svg>
            </a>

          </div>
        </div>
      </div>
    </div>   
	</li>
	{{end}}
	</ul>
  </div>
  {{if data.modulo.itemshowmore != false }}
		<a class="read-more" href="{{data.modulo.url}}">
                <span class="text">{{data.modulo.showmorelabel}}</span>
                <svg class="icon"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-arrow-right"></use></svg>
        </a>
    {{end}}
</div>
</div>
{{end}}