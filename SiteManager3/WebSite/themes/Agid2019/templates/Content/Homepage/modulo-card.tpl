﻿<div class="card-wrapper card-space h-100">
	<div class="card card-bg">
		<div class="card-body">
			{{if data.modulo.titolo != "false" }}
			<h4 class="card-title">
				{{if data.modulo.url != "false"}}
				<a href="{{data.modulo.url}}" title="" class="">
					{{data.modulo.titolo}}
				</a>
				{{else}}
					{{data.modulo.titolo}}
				{{end}}
			</h4>
			{{end}}
			{{if data.modulo.img.imgsrc != "false" }}
 
			{{
			
			imgpar = "false"	
			contentealign = "false"			
			imgalign = data.modulo.img.imgalign
			
			if imgalign == "pull-left" 
				imgpar = imgalign
				contentealign = "pull-right"
			end
			if imgalign == "pull-right"
				imgpar = imgalign
				contentealign = "pull-left"
			end		
			if imgalign == "center" 
				imgpar = "text-center"
			end 
			
			}}
			
			<div {{if imgpar != "false"}}class="{{imgpar}}" {{end}}>
				<img src="{{data.modulo.img.imgsrc}}" class="img-fluid d-none d-md-block" alt="{{data.modulo.img.imgalt}}" title="{{data.modulo.img.imgalt}}" />
			</div>
			
			{{end}}
			<div class="card-text {{if contentealign != "false"}} {{contentealign}} {{end}} " >
			{{data.modulo.testo}}
			</div>

			{{ if data.modulo.url != 'false' }}
			<a class="read-more" href="{{data.modulo.url}}"> <span class="text">{{commonlabels.readmore}}</span>
			<svg class="icon">
				<use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-arrow-right"></use>
			</svg>
			</a> 
			{{end}}	   
		  
		</div>
	</div>
</div>