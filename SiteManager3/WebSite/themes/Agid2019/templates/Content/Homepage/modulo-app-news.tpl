﻿<div class="py-4">
{{if data.items.size > 0}}
<div class="card-wrapper">  
    {{for item in data.items limit:1 }}
	<div class="card {{if data.modulo.showimages && item.img != false }}card-img{{end}} no-after">		

			{{if data.modulo.showimages && item.img != false }}
			<div class="img-responsive-wrapper">
			   <div class="img-responsive">
				   <div class="img-wrapper"><img class="img-fluid" src="{{item.img.imgsrc}}" alt="{{item.img.imgalt}}" title="{{item.img.imgalt}}"></div>
			   </div>
			 </div>
			 {{end}}

			<div class="card-body">
			  <div class="category-top">
				   <!-- icon -->
				   <!-- categoria -->
				   <a class="category" href="{{item.itemcategoryurl}}" >{{item.categoria | string.capitalizewords}}</a>
				   <span class="data"> {{item.date}}</span> 
			  </div>
              <h5 class="card-title"> {{item.titolo}}</h5>
              <p class="card-text">{{item.descrizione}}</p>
              <a class="read-more" href="{{item.itemurl}}">
                <span class="text">{{commonlabels.readmore}}</span>
                <svg class="icon">
                  <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-arrow-right"></use>
                </svg>
              </a>
            </div>

	</div>
	{{end}}
</div>
{{end }}
</div>


