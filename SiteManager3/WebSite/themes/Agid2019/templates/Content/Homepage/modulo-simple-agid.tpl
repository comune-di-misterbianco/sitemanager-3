﻿<section id="head-section">
    <div class="container">
      <div class="row">
        {{if data.modulo.img.imgsrc != "false" }}
		<div class="col-lg-6 offset-lg-1 order-lg-2">
          <img src="{{data.modulo.img.imgsrc}}" title="{{data.modulo.img.imgalt}}" alt="{{data.modulo.img.imgalt}}" class="img-responsive">
        </div>
		{{end}}
        <div class="col-lg-5 order-lg-1">
          <div class="card">
            <div class="card-body pb-5">              
			  {{if data.modulo.titolo != "false"}}
			  <h1 class="h4 card-title">
			    {{if data.modulo.url != "false" }}
                <a class="pb-3" href="{{data.modulo.url}}">
                   <span class="text">{{data.modulo.titolo}}</span>
                </a>
				{{else}}
					{{data.modulo.titolo}}
				{{end}}
			  </h1>
			  {{end}}
              <p class="card-text"> {{data.modulo.testo}}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>