﻿<div class="row">
	<h3 class="mb-1 block-title text-primary"> {{ data.modulo.label }}</h3>
</div>
<div class="it-grid-list-wrapper it-image-label-grid it-masonry">
  <div class="card-columns">
  	{{if data.items.size > 0 }}
		{{for item in data.items }}
		<div class="col-12">
		  <div class="it-grid-item-wrapper">
			<div class="card-wrapper card-space ">  		
				<div class="card {{if data.modulo.showimages && item.img != null }}card-img{{end}} rounded shadow">		
					{{if data.modulo.showimages && item.img != null }}
					  <div class="img-responsive-wrapper">
						<div class="img-responsive">
						  <div class="img-wrapper"><img src="{{item.img.imgsrc}}" alt="{{item.img.imgalt}}" title="{{item.img.imgalt}}"></div>
						</div>
					  </div>
					{{end}}
			 
					<div class="card-body">
						<div class="category-top">
							<!-- icon -->
							<!-- categoria -->
							<a class="category" href="{{item.itemcategoryurl}}" >{{item.categoria | string.capitalizewords}}</a>
							<span class="data"> {{item.date}}</span> 
						</div>
						<h5 class="card-title"> {{item.titolo}}</h5>
						<p class="card-text">{{item.descrizione}}</p>
						<a class="read-more" href="{{item.itemurl}}">
						<span class="text">{{commonlabels.readmore}}</span>
						<svg class="icon">
							<use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-arrow-right"></use>
						</svg>
						</a>
					</div>
				</div>
			</div>
		  </div>
		</div>
		{{end }}
    {{end }}
  </div>
</div>
{{if data.modulo.itemshowmore != false }}
	<a class="read-more" href="{{data.modulo.url}}">
            <span class="text">{{data.modulo.showmorelabel}}</span>
            <svg class="icon"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-arrow-right"></use></svg>
    </a>
{{end}}