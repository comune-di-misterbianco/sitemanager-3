﻿
<div class="py-1">
	<div class="row">
		<h3 class="mb-4 block-title text-primary"> {{ data.modulo.label }}</h3>
	</div>

    {{if data.items.size > 0 }}
    
        <ul class="it-list">
            {{for item in data.items }}
            <li class="media">
                <div class="media-body post-holder">
                    <svg class="icon icon-primary icon-sm"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-calendar"></use></svg>
                    <span class="date"><small>{{item.date}}</small></span><br />
                    <a href="{{item.itemurl}}" title="Consulta {{item.titolo}}"> {{item.titolo}}</a>
                    {{if data.modulo.showimages  && item.img != null && item.img != false }}
                    					
                    {{##if item.img.imgalign != "false"; item.img.imgalign; end}}
					<div class="img-responsive-wrapper">
						   <div class="img-responsive">
							   <div class="img-wrapper"><img class="img-fluid" src="{{item.img.imgsrc}}" alt="{{item.img.imgalt}}" title="{{item.img.imgalt}}"></div>
						   </div>
					</div>
					{{end }}
                    {{ if data.modulo.itemshowdesc != "false"}}
                    <p class="descrizione"> {{item.descrizione}}</p>
                    {{end }}
                </div>
			</li>
            {{end }}
        </ul>

	{{else }}
	<p></p>
    {{end }}

	{{if data.modulo.itemshowmore != false }}
		<a class="read-more" href="{{data.modulo.url}}">
                <span class="text">{{data.modulo.showmorelabel}}</span>
                <svg class="icon"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-arrow-right"></use></svg>
        </a>
    {{end}}


</div>