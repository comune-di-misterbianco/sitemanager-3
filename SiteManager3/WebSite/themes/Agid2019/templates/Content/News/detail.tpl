﻿<div class="container">
    <div class="row">
		<div class="col-lg-10">
        	<h1 class="h3 pr-lg-4 mt-4 mt-md-0">{{document.titolo}}</h1>     		
			<p class="description d-print-none d-none">{{document.descrizione}}</p>	
        </div>
		<div class="col-lg-2">
			<div class="avatar-wrapper avatar-extra-text mb-4">
				<div class="extra-text d-flex flex-lg-column ml-0 mr-4">
					<strong class="d-block mr-2 mr-lg-0">{{commonlabels.labelpublishedon}}:</strong>			
					<time datetime="{{document.datapub}}" class="text-capitalize">{{document.datapub}}</time>
				</div>

			</div>
			<div class="avatar-wrapper avatar-extra-text mb-4 d-print-none">
				<div class="extra-text d-flex flex-lg-column ml-0">
					<strong class="d-block mr-2 mr-lg-0">{{commonlabels.timetoread}}:</strong>
					<p class="text-capitalize reading-time"></p>
				</div>			
			</div>	
		</div>
		<div class="col-12">
				<div class="btn-toolbar justify-content-end justify-content-lg-between border-top border-bottom border-light position-relative py-2 my-4 d-print-none" role="toolbar" aria-label="Strumenti">
					<a href="javascript:this.print();" title="{{commonlabels.printtitle}}">
						<svg class="icon icon-sm mx-auto mx-lg-0"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-print"></use></svg>
						<span>{{commonlabels.print}}</span>
					</a>
					<a href="mailto:?subject={{sezione.titolo}}&body={{sezione.descrizione}}">
						<svg class="icon icon-sm mx-auto mx-lg-0"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-mail"></use></svg>
						<span>{{commonlabels.send}}</span>
					  </a>
					{{sharecontent}}
				</div>		
		</div>		
	</div><!-- /row -->
</div>  

{{if document.img.showindetail == true }}     
<div class="container-fluid mt-2 mb-4 p-0">				
	<div class="imgevidenza">
		<figure>
			<img src="{{document.img.src}}" class="img-fluid" alt="{{document.img.descrizione}}" />
			<figcaption>{{document.img.descrizione}}</figcaption>
		</figure>
	</div>	
</div>
{{end}}
<div class="container">
	<div class="row">
		<div class="col-md-12 col-lg-12">
		{{if sezione.menu == false }}
		<article class="article-body">			
			<div class="page-content">
				{{
					testo = document.testo | regex.replace "(<h1.*?>.*?</h1>)" "" "$"
					test = testo | regex.replace "(style=.*?)" "" ""
					testo
				}}
			</div>					
		</article>
		{{else }}
			<aside class="col-lg-4">
				<div class="menu-wrapper">                
					<div class="link-list-wrapper menu-link-list">
						{{sezione.menu}}
					</div>                              
				</div>
			</aside>
			<section class="col-lg-8">
				<article class="pagina">		
				<div class="page-content">{{document.testo}}</div>
				</article>
			</section>
		{{end}}
		</div><!-- /col -->
	</div><!-- /row -->
	<div class="border-top border-light">
		<h4 class="h5 pt-2">Ulteriori informazioni</h4>
		<!-- revisione -->
		<div class="avatar-wrapper avatar-extra-text my-4">
			<div class="extra-text d-flex flex-lg-column ml-0">
				<strong class="d-block mr-2">{{commonlabels.lastrevision}}:</strong>
				<time datetime="{{document.datarev}}" class="text-capitalize">{{document.datarev}}</time>
			</div>
		</div>

		<!-- argomenti/TAGS -->
		{{if document.tags | object.size > 0}}
		<div class="mt-2 mb-4 d-print-none">
			<h6><small>{{commonlabels.tagstitle}}</small></h6>
			{{ for tag in document.tags }}
				<a class="text-decoration-none" rel="tag" href="/tags/argomenti.aspx?tag={{tag.key}}">
					<div class="chip chip-simple chip-primary">
						<span class="chip-label">{{tag.label | string.capitalizewords}}</span>
					</div>
				</a>
			{{end}}
		</div>
		{{end}}

		<!-- END argomenti/TAGS -->					  
	</div>
	
	<!-- Allegati -->
	{{if document.allegati  | object.size > 0}}			
	<div class="col-12">
		<h4>{{commonlabels.attacheddocumentstitle}}</h4>
	</div>
	<div class="row mx-lg-n3"> 
		{{ for allegato in document.allegati }}
		<div class="col-12 px-lg-3 pb-lg-3">				    
			<div class="card card-teaser shadow p-4 mt-3 rounded border">
				<svg class="icon"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-clip"></use></svg>		
				<div class="card-body">
				  <h5 class="card-title">
						<a class="stretched-link" href="{{allegato.url}}">			    
							{{allegato.label}}	    	
						</a>
						<small class="d-block">(File {{allegato.extension}}  {{allegato.size}} kB)</small>
				  </h5>
				</div>
			</div>
		</div>	
		{{end}}
	</div>
	{{end}}
	<!-- End Allegati -->
	
</div><!-- /container -->