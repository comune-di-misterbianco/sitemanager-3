﻿<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>           
        </div>
    </div>

    <!-- search form -->
	<div class="row">
		<div class="col-md-12">
			<div class=" searchFieldset">
				<p class="text-right">
					<button class="btn btn-primary " data-toggle="collapse" type="button" data-target="#newsform" >
						{{labels.ricercanews}}
					</button>
				</p>
			<div class="searchForm collapse" id="newsform" style="">
					<div id="SearchForm" class="Axf" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'SearchForm_submit')">
						<div class="col-md-12">
							<div class="page-header title-vfm mb-4">
								<h4>Cerca</h4>
							</div>
							<div class="form-row">
								<div class="form-group validatedfield col textbox-vf ">								
									<input name="Label" type="text" id="Label" class="form-control" />
									<label for="Label">{{labels.titolo}}</label>
								</div>
								<div class="form-group validatedfield col textbox-vf ">								
									<input name="Descrizione" type="text" id="Descrizione" class="form-control" />
									<label for="Descrizione">{{labels.descrizione}}</label>
								</div>
							</div>
							<div class="form-row">														
								<div class="form-group validatedfield col">
										<select class="form-control" name="Search_DateOptions" id="Search_DateOptions">
											<option selected="selected" value="">{{labels.sceglimodalita}}</option>
											<option value="0">{{labels.successivaal}}</option>
											<option value="1">{{labels.antecedential}}</option>
											<option value="2">{{labels.dataesatta}}</option>
										</select>
								</div>
								<div id="DataNews" class="it-datepicker-wrapper col-7">	
									<div class="form-group validatedfield textbox-vf">																	
										<input name="Data" type="text" id="Data" class="form-control it-date-datepicker"  placeholder="{{labels.dateformatplaceholder}}" />
										<label for="Data" class="control-label">{{labels.data}}</label>									
									</div>
								</div>							
							</div>
						<script type="text/javascript">						
								   $(document).ready(function() {									 
									  $('.it-date-datepicker').datepicker({
										  inputFormat: ["dd/MM/yyyy"],
										  outputFormat: 'dd/MM/yyyy',
										});
								   });
						</script>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group text-center vf back-vf">
									<input type="submit" name="SearchForm_back" value="{{labels.azzeraricerca}}" id="SearchForm_back" class="btn btn-outline-secondary" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group text-center vf submit-vf">
									<input type="submit" name="SearchForm_submit" value="{{labels.labelsearchbutton}}" id="SearchForm_submit" class="btn btn-primary text-white" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
    </div>
	<!-- end search form -->

    <div class="row">
        <div class="col-md-12">

            {{if documents.size > 0 }}
			<div class="table-responsive">
			<table class="table">
				<thead> 
					<tr>
						<th>Data</th> 
						<th>Titolo</th> 
						<th>Descrizione</th> 
					{{ if sezione.showchild }}	<th>Categoria</th> {{end}}
					</tr>
				</thead>			
				<tbody> 
					{{for doc in documents }}
					<tr class="active">
						<td scope="row">{{doc.data}}</td> 
						<td>
							<a class="u-textClean" href="?news={{doc.id}}">
								{{doc.titolo}}
							</a>
						</td> 
						<td>{{doc.descrizione}}</td> 
						{{ if sezione.showchild }}<td><a class="u-textClean" href="{{doc.folderpath}}/default.aspx">{{doc.categoria}}</a></td> {{end}}
					</tr>
					{{end}}
				</tbody>
				
			</table>
			</div>
        <!-- paginazion -->
		<nav class="pagination-wrapper pagination-total" aria-label="Page navigation">			    
		<ul class="pagination">
			<li class="page-item disabled"><a class="page-link text" href="#">{{commonlabels.page}} {{ pager.current }} {{commonlabels.pageof}} {{ pager.pages }}</a><li>
			{{ if pager.current > 1}}
			<li class="page-item">
				<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" 1 }}" aria-label="{{commonlabels.firstpage}}">
					<span class="fa fa-angle-double-left"></span>
					<span aria-hidden="true" class="sr-only">{{commonlabels.firstpage}}</span>
				</a>
			</li>
			<li class="page-item">
				<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" pager.current-1 }}" aria-label="{{commonlabels.prevpage}}">
					<span class="fa fa-angle-left" role="presentation"></span>
					<span aria-hidden="true" class="sr-only">{{commonlabels.prevpage}}</span>
				</a>
			</li>
			{{end }}

			{{ for i in pager.lowrange..pager.toprange}}
			<li class="page-item {{ if  i == pager.current }} active {{end}}">
				<a class="page-link" {{if  i == pager.current }}aria-current="page"{{end}} href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" i }}">{{i}}</a>							
			</li>
			{{end }}

			{{ if pager.current < pager.pages}}
			<li class="page-item">
				<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" pager.current+1 }}" aria-label="{{commonlabels.nextpage}}">
					<span class="fa fa-angle-right" role="presentation"></span>
					<span aria-hidden="true" class="sr-only">{{commonlabels.nextpage}}</span>
				</a>
			</li>
			<li class="page-item">
				<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" pager.pages}}" aria-label="{{commonlabels.lastpage}}">
					<span class="fa fa-angle-double-right" role="presentation"></span>
					<span aria-hidden="true" class="sr-only">{{commonlabels.lastpage}}</span>
				</a>
			</li>
			{{end }}
		</ul>
	</nav>  
	<!-- fine paginazion -->	     

            {{else}}
			  <p class="no-records">{{commonlabels.norecord}}</p>
            {{end}}
        </div>
    </div>
 </div>

