﻿<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>
        </div>
    </div>
	<!-- search form -->
	<div class="row">
		<div class="col-md-12">
			<div class=" searchFieldset">
				<p class="text-right">
					<button class="btn btn-primary " data-toggle="collapse" type="button" data-target="#newsform" >
						{{labels.ricercanews}}
					</button>
				</p>
			<div class="searchForm collapse" id="newsform" style="">
					<div id="SearchForm" class="Axf" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'SearchForm_submit')">
						<div class="col-md-12">
							<div class="page-header title-vfm mb-4">
								<h4>Cerca</h4>
							</div>
							<div class="form-row">
								<div class="form-group validatedfield col textbox-vf ">								
									<input name="Label" type="text" id="Label" class="form-control" />
									<label for="Label">{{labels.titolo}}</label>
								</div>
								<div class="form-group validatedfield col textbox-vf ">								
									<input name="Descrizione" type="text" id="Descrizione" class="form-control" />
									<label for="Descrizione">{{labels.descrizione}}</label>
								</div>
							</div>
							<div class="form-row">														
								<div class="form-group validatedfield col">
										<select class="form-control" name="Search_DateOptions" id="Search_DateOptions">
											<option selected="selected" value="">{{labels.sceglimodalita}}</option>
											<option value="0">{{labels.successivaal}}</option>
											<option value="1">{{labels.antecedential}}</option>
											<option value="2">{{labels.dataesatta}}</option>
										</select>
								</div>
								<div id="DataNews" class="it-datepicker-wrapper col-7">	
									<div class="form-group validatedfield textbox-vf">																	
										<input name="Data" type="text" id="Data" class="form-control it-date-datepicker" placeholder="{{labels.dateformatplaceholder}}" />
										<label for="Data" class="control-label">{{labels.data}}</label>									
									</div>
								</div>							
							</div>
						<script type="text/javascript">						
								   $(document).ready(function() {									 
									  $('.it-date-datepicker').datepicker({
										  inputFormat: ["dd/MM/yyyy"],
										  outputFormat: 'dd/MM/yyyy',
										});
								   });
						</script>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group text-center vf back-vf">
									<input type="submit" name="SearchForm_back" value="{{labels.azzeraricerca}}" id="SearchForm_back" class="btn btn-outline-secondary" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group text-center vf submit-vf">
									<input type="submit" name="SearchForm_submit" value="{{labels.labelsearchbutton}}" id="SearchForm_submit" class="btn btn-primary text-white" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
    </div>
	<!-- end search form -->
    <div class="row">       
           
           {{if documents.size > 0}}

                {{ for doc in documents }}
				<div class="col-md-12 col-lg-12 mb-3">
                <div class="card-wrapper">
                    <div class="card card-bg">
                         {{if doc.imgevidenza != null && doc.imgevidenza != false }}
                            {{if doc.imgevidenza.src | string.size > 0 }}
						    <div class="img-responsive-wrapper">
								<div class="img-responsive">
									<img src="{{doc.imgevidenza.src}}" class="img-fluid" alt="{{doc.imgevidenza.description}}" />
								</div>
							</div>
                            {{end }}
                         {{end }}
                         <div class="card-body">
                                <a href="{{doc.folderpath}}/default.aspx"><span class="chip chip-simple chip-primary"><span class="chip-label">{{doc.categoria}}</span></span></a>
                                <h5 class="card-title mb-3">
                                    <a href="?news={{doc.id}}" title="{{ doc.titolo | string.truncate 50}}">{{doc.titolo}}</a>
                                </h5>
                                <p>
									<svg class="icon icon-sm"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-calendar"></use></svg>                         
                                    <span class="data">{{ doc.data }}</span>
                                </p>
                                <p class="card-text">{{doc.descrizione}}</p>
                         </div>
                    </div>
                </div>
				</div>
                {{end }}           
                       
        <!-- paginazion -->
		<nav class="pagination-wrapper pagination-total" aria-label="Page navigation">			    
		<ul class="pagination">
			<li class="page-item disabled"><a class="page-link text" href="#">{{commonlabels.page}} {{ pager.current }} {{commonlabels.pageof}} {{ pager.pages }}</a><li>
			{{ if pager.current > 1}}
			<li class="page-item">
				<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" 1 }}" aria-label="{{commonlabels.firstpage}}">
					<span class="fa fa-angle-double-left"></span>
					<span aria-hidden="true" class="sr-only">{{commonlabels.firstpage}}</span>
				</a>
			</li>
			<li class="page-item">
				<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" pager.current-1 }}" aria-label="{{commonlabels.prevpage}}">
					<span class="fa fa-angle-left" role="presentation"></span>
					<span aria-hidden="true" class="sr-only">{{commonlabels.prevpage}}</span>
				</a>
			</li>
			{{end }}

			{{ for i in pager.lowrange..pager.toprange}}
			<li class="page-item {{ if  i == pager.current }} active {{end}}">
				<a class="page-link" {{if  i == pager.current }}aria-current="page"{{end}} href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" i }}">{{i}}</a>							
			</li>
			{{end }}

			{{ if pager.current < pager.pages}}
			<li class="page-item">
				<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" pager.current+1 }}" aria-label="{{commonlabels.nextpage}}">
					<span class="fa fa-angle-right" role="presentation"></span>
					<span aria-hidden="true" class="sr-only">{{commonlabels.nextpage}}</span>
				</a>
			</li>
			<li class="page-item">
				<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" pager.pages}}" aria-label="{{commonlabels.lastpage}}">
					<span class="fa fa-angle-double-right" role="presentation"></span>
					<span aria-hidden="true" class="sr-only">{{commonlabels.lastpage}}</span>
				</a>
			</li>
			{{end }}
		</ul>
	</nav>  
	<!-- fine paginazion -->	
            
           {{else}}
                <p class="no-records">{{commonlabels.norecord}}</p>
           {{end}}            
    </div>
 </div>
 