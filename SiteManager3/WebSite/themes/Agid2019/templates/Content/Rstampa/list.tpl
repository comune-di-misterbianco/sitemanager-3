﻿<div class="container">
   <!-- page int --> 
   <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>
        </div>
    </div>

    <div class="row">
        
	{{if documents.size > 0 }}
					{{for doc in documents }}
					<div class="col-12 col-sm-6 col-lg-4 py-2">
						
						
								<div class="card h-100 rounded shadow">
								
									
									<div class="card-body">								
										<div class="head-tags">											
											<span class="data">{{doc.data}}</span>											
											<span class="etichetta">
												<svg class="icon">
													<use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-file"></use>
												</svg>
												{{doc.testata}}
											</span>
										</div>										
										<h5 class="card-title">
											<a class="u-text-r-m u-color-black u-textWeight-400 u-textClean" href="{{doc.folderpath}}/default.aspx?rs={{doc.id}}">{{doc.titolo}}</a>
										</h5>
										<p class="card-text">{{doc.descrizione}}</p>
									</div>
								</div>
						
					</div>
					{{end }}

	</div>

		<!-- fine corpo -->

		<!-- paginazion -->
		 <nav class="pagination-wrapper pagination-total" aria-label="Page navigation">			    
					<ul class="pagination">
			<li class="page-item disabled"><a class="page-link text" href="#">Pagina {{ pager.current }} di {{ pager.pages }}</a><li>
						{{ if pager.current > 1}}
						<li class="page-item">
				<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" 1 }}" aria-label="{{commonlabels.firstpage}}">
								<span class="fa fa-angle-double-left"></span>
								<span aria-hidden="true" class="sr-only">{{commonlabels.firstpage}}</span>
							</a>
						</li>
						<li class="page-item">
				<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" pager.current-1 }}" aria-label="{{commonlabels.prevpage}}">
								<span class="fa fa-angle-left" role="presentation"></span>
								<span aria-hidden="true" class="sr-only">{{commonlabels.prevpage}}</span>
							</a>
						</li>
						{{end }}

						{{ for i in pager.lowrange..pager.toprange}}
						<li class="page-item {{ if  i == pager.current }} active {{end}}">
				<a class="page-link" {{if  i == pager.current }}aria-current="page"{{end}} href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" i }}">{{i}}</a>							
						</li>
						{{end }}

						{{ if pager.current < pager.pages}}
						<li class="page-item">
				<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" pager.current+1 }}" aria-label="{{commonlabels.nextpage}}">
								<span class="fa fa-angle-right" role="presentation"></span>
								<span aria-hidden="true" class="sr-only">{{commonlabels.nextpage}}</span>
							</a>
						</li>
						<li class="page-item">
				<a class="page-link" href="{{sezione.url}}{{pager.pageandquerystring | string.replace "{0}" pager.pages}}" aria-label="{{commonlabels.lastpage}}">
								<span class="fa fa-angle-double-right" role="presentation"></span>
								<span aria-hidden="true" class="sr-only">{{commonlabels.lastpage}}</span>
							</a>
						</li>
						{{end }}
					</ul>
              </nav>  
	<!-- fine paginazion -->
      <!-- archivio storico -->
		{{ sezione.archivio.mesi.size}}
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header"><h4>{{labels.archiviostorico}}</h4></div>
				<div class="archivioStorico row">
					{{
					anni = sezione.archivio.anni | object.size
					if anni > 0
					}}
					<div class="col">						
                            <ul class="list-inline">
                                {{for item in sezione.archivio.anni }}
							<li class="list-inline-item"><a class="btn btn-default {{if sezione.archivio.selectedyear == item  }} selected {{end}}" href="default.aspx?anno={{item}}">{{item}}</a></li>
                            {{end}}
						</ul>						
                        </div>
					{{end}}
					{{
					mesi = sezione.archivio.mesi | object.size
					if mesi > 0
					}}
					<div class="col">						
                            <ul class="list-inline">
                                {{for item in sezione.archivio.mesi }}
							<li class="list-inline-item"><a class="btn btn-default {{if sezione.archivio.selectedmonth == item.month  }} selected {{end}}" href="default.aspx?anno={{sezione.archivio.selectedyear}}&mese={{item.month}}">{{item.monthname}}</a></li>
                            {{end}}
						</ul>						
                        </div>
					{{end}}
					{{
					days = sezione.archivio.giorni | object.size
					if days > 0 
					}}
					<div class="col">						
                            <ul class="list-inline">
                                {{for item in sezione.archivio.giorni }}
							<li class="list-inline-item"><a class="btn btn-default" href="default.aspx?anno={{sezione.archivio.selectedyear}}&mese={{item.month}}&giorno={{item.day}}">{{item.day}}</a></li>
                            {{end}}
						</ul>						
                        </div>
					{{end}}
                    </div>
                </div>
            </div>
		<!-- fine archivio storico -->
      
            {{else}}
            <p>{{labels.norecord}}</p>
	{{end}}
        </div>
    </div>
 </div>