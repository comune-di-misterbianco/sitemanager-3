﻿<div class="container">
    <article id="content">     
        <div class="news-body col-md-9">
            <header>
                <h1 class="title">{{document.titolo}}</h1>
                <div class="description"><p>{{document.descrizione}}</p></div>
                <div class="date"><p><strong>{{labels.data}}</strong>: {{document.data}}</p></div>
                <div class="testata"><p><strong>{{labels.testatagiornalistica}}</strong>: {{ document.testata}}</p></div>
            </header>
            {{if document.linkarticolo != false }}
            <div class="linkarticolo">
                <p>
					<a class="btn btn-default" rel="esterno" href="{{document.linkarticolo}}"><i class="fa fa-external-link" aria-hidden="true"></i>
					{{labels.consultaarticololink}}
					</a>
				</p>
            </div>
            {{end}}            
            {{if document.allegati != empty}}
            <div class="allegati">
                <ul>
                    {{for allegato in document.allegati}}
					<li class="allegato {{allegato.ext}}">
						<i class="fa fa-file-o" aria-hidden="true"></i>&nbsp;<a rel="esterno" href="{{allegato.filepath}}" title="{{labels.filedownloadtitle}} {{allegato.filename}}">{{allegato.filename}}</a>
					</li>
                    {{end}}
                </ul>
            </div>
            {{end}}
        </div>
        <div class="news-column col-md-3">
            <div class="share text-right">
                         {{sharecontent}}
            </div>
            <div class="column-content">

            </div>
        </div>
    </article>
</div>