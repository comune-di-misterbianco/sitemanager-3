﻿<div class="container my-4">
    <div class="row">
		<div class="col-lg-8 px-lg-4 py-lg-2">
        <h1>{{sezione.titolo}}</h1>   
		{{if sezione.menu == false }}           
			<article class="pagina">
				<p class="description">{{sezione.descrizione}}</p>				
				<div class="row mt-5 mb-4">
					    <div class="col-6">
							<small>{{commonlabels.labelpublishedon}}:</small>
							<p class="font-weight-semibold">{{sezione.data}}</p>
						</div>
						<div class="col-6">
							<small>{{commonlabels.lastrevision}}:</small>
							<p>{{sezione.datarev}}</p>
						</div>						
				</div>										
				<div class="page-content">{{sezione.testo | regex.replace "(<h1.*?>.*?</h1>)" "" "$"}}</div>										
            </article>
			{{else }}
				<aside class="col-lg-4">
					<div class="menu-wrapper">                
						<div class="link-list-wrapper menu-link-list">
							{{sezione.menu}}
						</div>                              
					</div>
				</aside>
				<section class="col-lg-8">
					<article class="pagina">
					<div class="row mt-5 mb-4">
						<div class="col-6">
							<small>{{commonlabels.lastrevision}}:</small>
							<p>{{sezione.datarev}}</p>
						</div>
						<div class="col-6">
							<small>Tempo di lettura:</small>
							<p class="font-weight-semibold reading-time"></p>
						</div>
					</div>
						
					<p class="description">{{sezione.descrizione}}</p>				
					<div class="page-content">{{sezione.testo | regex.replace "(<h1.*?>.*?</h1>)" "" "$"}}</div>
				</section>
			{{end}}
        </div>
		<div class="col-lg-3 offset-lg-1">
			{{sharecontent}}

			<!-- Azioni -->
			<div class="dropdown d-inline">
			  <button class="btn btn-dropdown dropdown-toggle" type="button" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<svg class="icon">
				  <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-more-items"></use>
				</svg>
				<small>Azioni disponibili</small>
			  </button>
			  <div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
				<div class="link-list-wrapper">
				  <ul class="link-list">
					<!--<li>
					  <a class="list-item" href="#">
						<svg class="icon">
						  <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-download"></use>
						</svg>
						<span>Scarica</span>
					  </a>
					</li>-->
					<li>
					  <a class="list-item" href="javascript:this.print();" title="{{commonlabels.printtitle}}">
						<svg class="icon">
						  <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-print"></use>
						</svg>
						<span>{{commonlabels.print}}</span>
					  </a>
					</li>
					<!--<li>
					  <a class="list-item" href="#">
						<svg class="icon">
						  <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-hearing"></use>
						</svg>
						<span>Ascolta</span>
					  </a>
					</li>-->
					<li>
					  <a class="list-item" href="mailto:?subject={{sezione.titolo}}&body={{sezione.descrizione}}">
						<svg class="icon">
						  <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-mail"></use>
						</svg>
						<span>Invia</span>
					  </a>
					</li>
				  </ul>
				</div>
			  </div>
			</div>
			<!-- Fine azioni -->

			<!-- argomenti/TAGS -->
			{{if sezione.tags | object.size > 0}}
			<div class="mt-4 mb-4">
				<h6><small>{{commonlabels.tagstitle}}</small></h6>
				{{ for tag in sezione.tags }}
					<a class="text-decoration-none" rel="tag" href="/tags/argomenti.aspx?tag={{tag.key}}">
						<div class="chip chip-simple chip-primary">
							<span class="chip-label">{{tag.label | string.capitalizewords}}</span>
						</div>
					</a>
				{{end}}
			</div>
			{{end}}
			<!-- END argomenti/TAGS -->

		</div>
			
        
        </div>
    </div>