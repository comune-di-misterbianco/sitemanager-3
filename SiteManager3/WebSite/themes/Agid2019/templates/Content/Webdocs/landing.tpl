﻿<div class="container">

	<div class="row">
		<div class="col">
			<h1 class="u-text-h2">{{sezione.titolo}}</h1>
			<p class="">{{sezione.descrizione}}</p>
		</div>
    </div>

	  <div class="row">
	  
            {{if sezione.folders | object.size > 0}}
            {{for folder in sezione.folders}}
            <div class="col-12 col-sm-6 col-lg-4">
                <div class="card-wrapper card-space {{folder.cssclass}}">                   
				    <div class="card card-bg rounded shadow">
						<div class="card-body">
						<h5 class="card-title big-heading">
							<a href="{{folder.url}}">{{folder.titolo}}</a>
						</h5>
						<p class="card-text">
							{{folder.descrizione}}
						</p>					                    				

						{{if folder.childfolder.size > 0 || folder.documents.size > 0}}
						<div class="link-list-wrapper">
						<ul class="link-list">
							{{ for subfolder in folder.childfolder}}                       
							<li>
								<a class="list-item icon-left" href="{{subfolder.url}}" title="{{subfolder.descrizione}}">
									<svg class="icon icon-primary"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-chevron-right"></use></svg>
									<span>{{subfolder.titolo}}</span>
								</a>
							</li>
							{{  end }}
						
							{{ if folder.riffapp != 11 }}
								{{ for doc in folder.documents}}
									<li>
									<a class="list-item icon-left" href="{{doc.url}}" title="{{doc.descrizione}}">
									   <svg class="icon icon-primary"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-chevron-right"></use></svg>
									   <span>{{doc.titolo}}</span>
									</a>
									</li>
								{{  end }}
							{{  end }}

						</ul>
						</div>
						{{  end }}
						</div>
					</div>
				</div>
			</div>
            {{  end }}
            {{end}}



            {{if sezione.pages | object.size > 0}}           
            {{for page in sezione.pages}}
            <div class="col-12 col-sm-6 col-lg-4">
                 <div class="card-wrapper card-space">
				    <div class="card card-bg rounded shadow">
						<div class="card-body">
						<h5 class="card-title big-heading">
							<a href="{{page.url}}">{{page.titolo}}</a>
						</h5>
						<p class="card-text">
	                        {{page.descrizione}}
						</p>
						</div>
				 	</div>
            	</div>
            </div>
            {{  end }}
            {{end}}
       </div>
    
</div>