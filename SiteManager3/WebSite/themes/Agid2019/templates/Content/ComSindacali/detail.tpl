﻿	<div class="container my-4">
    <div class="row">
		<div class="col-lg-8 px-lg-4 py-lg-2">
        <h1>{{document.oggetto}}</h1>     		
		    
			{{if sezione.menu == false }}
           <p class="description">{{document.descrizione}}</p>				
			<article class="article-body">
					<div class="row mt-5 mb-4">
						<div class="col-6">
							<small>{{commonlabels.lastrevision}}:</small>
							<p>{{document.datarev}}</p>
						</div>
						<div class="col-6">
							<small>Tempo di lettura:</small>
							<p class="font-weight-semibold reading-time"></p>
						</div>
					</div>
									
				
				<div class="page-content">
					<div class="it-list-wrapper">
						<ul class="it-list">
							<li>{{labels.data}}: {{document.datapub}}</li>
							<li>{{labels.autore}}: {{document.autore}}</li>							
						</ul>
					</div>				
				</div>				
						
            </article>



			{{else }}
			
				<aside class="col-lg-4">
					<div class="menu-wrapper">                
						<div class="link-list-wrapper menu-link-list">
							{{sezione.menu}}
						</div>                              
					</div>
				</aside>
				<section class="col-lg-8">
					<article class="pagina">
					<div class="row mt-5 mb-4">
						<div class="col-6">
							<small>{{commonlabels.lastrevision}}:</small>
							<p>{{sezione.data}}</p>
						</div>
						<div class="col-6">
							<small>Tempo di lettura:</small>
							<p class="font-weight-semibold reading-time"></p>
						</div>
					</div>					
					{{if document.img.showindetail == true }}
					<div class="imgevidenza">
						<img src="{{document.img.src}}" class="img-fluid" alt="{{document.img.descrizione}}" />
					</div>
					{{end}}						
					<p class="description">{{document.descrizione}}</p>				
					<div class="page-content">{{document.testo}}</div>
					</article>
				</section>
			{{end}}


        </div>
		<div class="col-lg-3 offset-lg-1">
			{{sharecontent}}

			<!-- Azioni -->
			<div class="dropdown d-inline">
			  <button 
				class="btn btn-dropdown dropdown-toggle" 
				type="button" 
				id="viewActions" 
				data-toggle="dropdown" 
				aria-haspopup="true" 
				aria-expanded="false"
			   >
				<svg class="icon">
				  <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-more-items"></use>
				</svg>
				<small>Azioni</small>
			  </button>
			  <div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
				<div class="link-list-wrapper">
				  <ul class="link-list">
					<!--<li>
					  <a class="list-item" href="#">
						<svg class="icon">
						  <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-download"></use>
						</svg>
						<span>Scarica</span>
					  </a>
					</li>-->
					<li>
					  <a class="list-item" href="javascript:this.print();" title="{{commonlabels.printtitle}}">
						<svg class="icon">
						  <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-print"></use>
						</svg>
						<span>{{commonlabels.print}}</span>
					  </a>
					</li>					
					<li>
					  <a class="list-item" href="mailto:?subject={{sezione.titolo}}&body={{sezione.descrizione}}">
						<svg class="icon">
						  <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-mail"></use>
						</svg>
						<span>Invia</span>
					  </a>
					</li>
				  </ul>
				</div>
			  </div>
			</div>
			<!-- Fine azioni -->

			<!-- argomenti/TAGS -->
			{{if document.tags | object.size > 0}}
			<div class="mt-4 mb-4">
				<h6><small>{{commonlabels.tagstitle}}</small></h6>
				{{ for tag in document.tags }}
					<a class="text-decoration-none" rel="tag" href="/tags/argomenti.aspx?tag={{tag.key}}">
						<div class="chip chip-simple chip-primary">
							<span class="chip-label">{{tag.label | string.capitalizewords}}</span>
						</div>
					</a>
				{{end}}
			</div>
			{{end}}
			<!-- END argomenti/TAGS -->

		</div>
			
			<!-- Allegati -->
			{{if document.allegati  | object.size > 0}}		
			<div class="col-12">
				<h4>Documenti</h4>
			</div>
			<div class="row mx-lg-n3"> 
				{{ for allegato in document.allegati }}
				<div class="col-12 px-lg-3 pb-lg-3">				    
					<div class="card card-teaser shadow p-4 mt-3 rounded border">
						<svg class="icon"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-clip"></use></svg>		
						<div class="card-body">
						  <h5 class="card-title">
				  				<a class="stretched-link" href="{{allegato.url}}">			    
									{{allegato.label}}	    	
								</a>
								<small class="d-block">(File {{allegato.extension}}  {{allegato.size}} kB)</small>
						  </h5>
						</div>
					</div>
				</div>	
				{{end}}
			</div>
			{{end}}
			<!-- End Allegati -->	
        
        </div>
    </div>