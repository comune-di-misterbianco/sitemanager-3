﻿<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="search-fields">
                <button class="btn btn-primary collapsed" data-toggle="collapse" type="button" data-target="#BandiSearchForm" aria-expanded="true">
                    <span class="glyphicon glyphicon-search"></span>
                    {{labels.ricerca}}
                </button>
                <div class="searchForm collapse " id="BandiSearchForm">

                   

                        <div class="page-header title-vfm mb-4">
                            <h4>{{labels.cerca}}</h4>
                        </div>
						
						
						
						<div class="form-group">                            
										
							<textarea name="Search_Oggetto" rows="3" id="Search_Oggetto"></textarea>
							<label style="width:auto;" for="Search_Oggetto">{{labels.oggetto}}</label>				
						</div>						

						<div class="form-row">
							<div class="form-group col-md-12">                        
								<div class="bootstrap-select-wrapper validatedfield dropdown-vf mb-3">
								<label>{{labels.stato}}</label>
								{{if form.statooptions | object.size > 0}}
								<select name="Stato_Bando" id="Stato_Bando">									
									{{ for option in form.statooptions }}
									<option value="{{option.key}}">{{option.value}}</option>
									{{end}}
								</select>
								{{end}}
							</div>
							</div>
						</div>						

						<div class="form-row">
							
							<div class="form-group col-md-6 validatedfield dropdown-vf ">								
								<div class="bootstrap-select-wrapper">
								{{if form.ufficioptions | object.size > 0}}
								<select name="Search_Uffici" id="Search_Uffici" class="form-control ">									
									{{ for option in form.ufficioptions }}
									<option value="{{option.key}}">{{option.value}}</option>
									{{end}}
								</select>
								{{end}}
								<label for="Search_Uffici">{{labels.ufficio}}:</label>
								</div>
							</div>
							
							<div class="form-group col-md-6 validatedfield dropdown-vf ">
								<div class="bootstrap-select-wrapper">
								<label for="Search_Procedure" >{{labels.procedura}}:   </label>
								{{if form.procedureoptions | object.size > 0}}
								<select name="Search_Procedure" id="Search_Procedure" class="form-control ">									
									{{ for option in form.procedureoptions }}
									<option value="{{option.key}}">{{option.value}}</option>
									{{end}}
								</select>
								{{end}}
								</div>
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-12 validatedfield dropdown-vf ">
								<div class="bootstrap-select-wrapper">
									<label>{{labels.opera}}:</label>
									{{if form.opereoptions | object.size > 0}}								
									<select name="Search_Opere" id="Search_Opere" class="form-control ">									
										{{ for option in form.opereoptions }}
										<option value="{{option.key}}">{{option.value}}</option>
										{{end}}
									</select>
									{{end}}
								</div>
							</div>
						</div>
                   

                    <div class="form-row">
                        <div class="form-group col text-center vf submit-vf">
                            <button name="BandiSearchForm_submit" id="BandiSearchForm_submit" class="btn btn-primary" type="submit">{{labels.cerca}}</button>
                        </div>
                    </div>

                 </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            {{if documents.size > 0}}
			<div class="table-responsive">
            <table class="table">
                <caption class="u-hiddenVisually">{{labels.archiviobandi}}</caption>
                <thead>
                    <tr>
                        <th scope="col">{{labels.datascadenza}}</th>                        
                        <th scope="col">{{labels.numeroidentificativo}}</th>
                        <th scope="col">{{labels.ufficio}}</th>
                        <th scope="col">{{labels.opera}}</th>
                        <th scope="col">{{labels.procedura}}</th>
                        <th scope="col">{{labels.oggetto}}</th>
                        <th scope="col">{{labels.importo}}</th>
                        <th scope="col">{{labels.stato}}</th>
                    </tr>
                </thead>
                <tbody>
                    {{ for doc in documents }}
                    <tr>
                        <td>{{doc.datascadenza | date.to_string '%d/%m/%Y %H.%M'}}</td>
                        <td>{{doc.identificativo}}</td>
                        <td>{{doc.ufficio}}</td>
                        <td>{{doc.categoria}}</td>
                        <td>{{doc.procedura}}</td>
                        <td><a class="text-lowercase" title="" href="?bando={{doc.id}}">{{doc.titolo}}</a></td>
                        <td>{{doc.importo | math.format "C2"}}</td>
                        <td>
                            {{if doc.stato > 0 }}
                            {{
                               labelstato = ''
                               case doc.stato
                                when 1
                                   labelstato = labels.garaaggiudicata
                                   classstato = "badge Green"
                                when 2
                                   labelstato = labels.garasospesa
                                   classstato = "badge Red"
                                else
                                   labelstato = labels.contieneinformazioniaggiuntive
                                   classstato = "badge Orange"
                               end
                            }}
                            <span class="{{classstato}}">{{labelstato}}</span>
                            {{end}}
                        </td>
                    </tr>                 
                    {{ end }}
                </tbody>
            </table>

           <!-- paginazion -->
			 <nav class="pagination-wrapper pagination-total" aria-label="Page navigation">			    
                <ul class="pagination">
                    <li class="page-item"><a class="page-link text" href="#">Pagina {{ pager.current }} di {{ pager.pages }}</a><li>
                    {{ if pager.current > 1}}
                    <li class="page-item">
                        <a class="page-link" href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}=1" aria-label="{{commonlabels.firstpage}}">
                            <span class="fa fa-angle-double-left"></span>
                            <span aria-hidden="true" class="sr-only">{{commonlabels.firstpage}}</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current-1}}" aria-label="{{commonlabels.prevpage}}">
                            <span class="fa fa-angle-left" role="presentation"></span>
                            <span aria-hidden="true" class="sr-only">{{commonlabels.prevpage}}</span>
                        </a>
                    </li>
                    {{end }}

                    {{ for i in pager.lowrange..pager.toprange}}
                    <li class="page-item {{ if  i == pager.current }} active {{end}}">
                        <a class="page-link" {{if  i == pager.current }}aria-current="page"{{end}} href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{i}}">{{i}}</a>
                    </li>
                    {{end }}

                    {{ if pager.current < pager.pages}}
                    <li class="page-item">
                        <a class="page-link" href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current+1}}" aria-label="{{commonlabels.nextpage}}">
                            <span class="fa fa-angle-right" role="presentation"></span>
                            <span aria-hidden="true" class="sr-only">{{commonlabels.nextpage}}</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.pages}}" aria-label="{{commonlabels.lastpage}}">
                            <span class="fa fa-angle-double-right" role="presentation"></span>
                            <span aria-hidden="true" class="sr-only">{{commonlabels.lastpage}}</span>
                        </a>
                    </li>
                    {{end }}
                </ul>
            </nav>   
		<!-- fine paginazion -->	
            {{else}}
            <p>{{labels.norecordmessage}}</p>
            {{end}}
        </div>
    </div>
</div>