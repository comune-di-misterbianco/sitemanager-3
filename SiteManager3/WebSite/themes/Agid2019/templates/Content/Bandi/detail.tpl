﻿<div class="container">
	<div class="row">
		<div class="bandi-body mb-4">
			<header>
		        <h1 class="title">{{document.titolo}}</h1>
                <div class="description">{{document.descrizione}}</div>
			</header>
		</div>
	</div>
	<div class="row">
        <div class="bandi-body ">
            
                {{if options.statodatapublicazione != false }}
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;{{labels.datapubblicazione}}: {{document.datapubblicazione | date.to_string '%d/%m/%Y' }}</div>
                {{end}}

                {{if options.statodatascadenza != false }}
                {{if options.statotimescadenza == false }}
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;{{labels.datascadenza}}: {{document.datascadenza | date.to_string '%d/%m/%Y'}}</div>
                {{else}}
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;{{labels.datascadenza}}: {{document.datascadenza | date.to_string '%d/%m/%Y %H.%M'}}</div>
                {{end}}
                {{end}}

                {{if options.statodatainizio != false }}
                {{if options.statotimeinizio == false }}
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;{{labels.datainizio}}: {{document.datainizio | date.to_string '%d/%m/%Y'}}</div>
                {{else}}
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;{{labels.datainizio}}: {{document.datainizio | date.to_string '%d/%m/%Y %H.%M'}}</div>
                {{end}}
                {{end}}

                <div class="identificativo">
                    <i class="fa fa-archive" aria-hidden="true"></i>
                    {{labels.numeroidentificativo}}: {{document.identificativo}}
                </div>

                {{if document.importo == 0.00 }}
                <div class="importo">
                    <i class="fa fa-eur" aria-hidden="true"></i>
                    {{labels.importo}}:&nbsp;{{labels.noimporto}}
                </div>
                {{else}}
                <div class="importo">
                    <i class="fa fa-eur" aria-hidden="true"></i>
                    {{labels.importo}}:&nbsp;{{document.importo | math.format "C2"}}
                </div>
                {{end}}

                <div class="ufficio">
                    <i class="fa fa-building-o" aria-hidden="true"></i>
                    {{labels.ufficio}}:&nbsp;{{document.ufficio}}
                </div>

                <div class="categoria">
                    <i class="fa fa-folder-o" aria-hidden="true"></i>
                    {{labels.opera}}:&nbsp;{{ document.categoria}}
                </div>

                <div class="procedura">
                    <i class="fa fa-tag" aria-hidden="true"></i>
                    {{labels.procedura}}:&nbsp;{{document.procedura}}
                </div>
            

            
        </div>

		
		<div class="bando-text mt-4 ">
            {{document.testo}}
        </div>

        <div class="col-md-12 col-lg-12">

            <!--<div class="share text-right">
                         {{##sharecontent}}
            </div>-->

            {{if options.statochiarimenti != false}}
            <div class="chiarimenti text-center mt-4">
                <p>
                    <a class="btn btn-default" href="/chiarimenti/richiesta.aspx?doc={{document.docid}}">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        {{labels.labelchiarimenti}}
                    </a>
                </p>
            </div>
            {{end}}

            
            {{if document.allegati != empty}}
          
			<div class="it-list-wrapper"> 
				<h4 class="border-bottom"> {{labels.documenti}}</h4>               
                <ul class="it-list">
                    {{for allegato in document.allegati}}
                    <li class="allegato {{allegato.ext}}">
						<a rel="esterno" title="{{labels.filedownloadtitle}} {{allegato.filename}}" href="{{allegato.filepath}}">
						{{
						cssclass = "fa-file-o"
						fileext = allegato.ext
						case fileext
							when "pdf"
								cssclass = " fa-file-pdf-o"
							when "zip","rar","tar","gzip"
								cssclass = " fa-file-archive-o"
							when "doc","docx"
								cssclass = " fa-file-word-o"
							end
						}}
						<div class="it-rounded-icon"><svg class="icon"><use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-file"></use></svg></div>
                        <!-- <i class="fa {{##cssclass}}" aria-hidden="true"></i>&nbsp;-->
						<div class="it-right-zone">
							<span class="text">{{allegato.desc}}</span>
						</div>
						</a>
                    </li>
                    {{end}}
                </ul>
            </div>
            {{end}}

            {{if document.eventi != empty}}
			<div class="it-list-wrapper mt-4"> 
            <div class="it-list avanzamento ">
                <h4 class="border-bottom">{{labels.statodiavanzamento}}</h4>
                <ul class="it-list">
                    {{for evento in document.eventi}}
                    <li class="allegato {{evento.fileext}} ">
					<a rel="esterno" title="{{labels.filedownloadtitle}}  {{evento.filename}}" href="{{evento.filepath}}">
						
						<div class="it-right-zone">
							<p class="text">		
								{{ 
									cssclass = ""
									tipoevento = evento.idtipo
									case tipoevento 
										when 1
											cssclass = " fa-check-circle text-success"
										when 2
											cssclass = " fa-exclamation-circle text-danger"
										else
											cssclass = " fa-exclamation-circle text-warning"
									end
								
								}}
								<i class="fa {{cssclass}}" aria-hidden="true"></i>&nbsp;

								<span class="event-type">{{evento.nometipo}}</span>	<br />							
								<i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;
								<span class="event"> {{evento.data}}</span>	<br />							
							
							

							
								{{
									cssclass = "fa-file-o"
									fileext = evento.fileext
									case fileext
										when "pdf"
											cssclass = " fa-file-pdf-o"
										when "zip","rar","tar","gzip"
											cssclass = " fa-file-archive-o"
										when "doc","docx"
											cssclass = " fa-file-word-o"
									end
								}}
		 								
								<span>{{evento.testo}}</span>							
						</p>
						</div>
						</a>
                    </li>
                    {{end}}
                </ul>
            </div>
			</div>
            {{end}}
            

        </div>
    
	</div>
</div>