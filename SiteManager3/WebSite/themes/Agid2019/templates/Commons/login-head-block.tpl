﻿{{if data.pageposition == "body" }}
<a class="btn btn-primary btn-icon btn-full" href="{{data.autenticationpage}}" title="{{data.lang.accesstitle}}">
    <span class="rounded-icon">
        <svg class="icon icon-primary">
            <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-user"></use>
        </svg>
    </span>
    <span class="d-none d-lg-block">{{data.lang.loginlabellink}}</span>
</a>
{{end}}


<a class="btn btn-primary btn-icon btn-full" href="#" title="Accedi all'area personale">
    <span class="rounded-icon">
        <svg class="icon icon-primary">
            <use xlink:href="/design-comuni-prototipi/assets/bootstrap-italia/dist/svg/sprite.svg#it-user"></use>
        </svg>
    </span>
    <span class="d-none d-lg-block">Accedi all'area personale</span>
</a>