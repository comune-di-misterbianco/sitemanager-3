﻿<div class="container">
   <!-- page int --> 
   <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>           
        </div>
    </div>
    <div class="it-grid-list-wrapper it-image-label-grid it-masonry">
    <div class="card-columns">
	{{if items.size > 0 }}
					{{for doc in items }}
					<div class="col">
						<div class="it-grid-item-wrapper">
							<div class="card-wrapper card-space ">
							
								<div class="card rounded shadow">									
									<div class="card-body">								
										<div class="category-top">	
											<span class="tipo"><a href="{{doc.folderurl}}">{{doc.folderlabel}}</a></span>
											<span class="data">{{doc.data}}</span>
										</div>
										<h5 class="card-title">
											<a class="u-text-r-m u-color-black u-textWeight-400 u-textClean"
											href="{{doc.url}}">{{doc.titolo}}</a>
										</h5>
										<p class="card-text">{{doc.descrizione}}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					{{end }}
	</div>
	</div>
	<!-- fine corpo -->
	<!-- paginazion -->
	<nav class="pagination-wrapper pagination-total" aria-label="Page navigation">			    
		<ul class="pagination">
			<li class="page-item disabled"><a class="page-link text" href="#">Pagina {{ pager.current }} di {{ pager.pages }}</a><li>
			{{ if pager.current > 1}}
			<li class="page-item">
				<a class="page-link" href="{{sezione.url}}/{{pager.pageandquerystring | string.replace "{0}" 1 }}" aria-label="{{commonlabels.firstpage}}">
					<span class="fa fa-angle-double-left"></span>
					<span aria-hidden="true" class="sr-only">{{commonlabels.firstpage}}</span>
				</a>
			</li>
			<li class="page-item">
				<a class="page-link" href="{{sezione.url}}/{{pager.pageandquerystring | string.replace "{0}" pager.current-1 }}" aria-label="{{commonlabels.prevpage}}">
					<span class="fa fa-angle-left" role="presentation"></span>
					<span aria-hidden="true" class="sr-only">{{commonlabels.prevpage}}</span>
				</a>
			</li>
			{{end }}

			{{ for i in pager.lowrange..pager.toprange}}
			<li class="page-item {{ if  i == pager.current }} active {{end}}">
				<a class="page-link" {{if  i == pager.current }}aria-current="page"{{end}} href="{{sezione.url}}/{{pager.pageandquerystring | string.replace "{0}" i }}">{{i}}</a>							
			</li>
			{{end }}

			{{ if pager.current < pager.pages}}
			<li class="page-item">
				<a class="page-link" href="{{sezione.url}}/{{pager.pageandquerystring | string.replace "{0}" pager.current+1 }}" aria-label="{{commonlabels.nextpage}}">
					<span class="fa fa-angle-right" role="presentation"></span>
					<span aria-hidden="true" class="sr-only">{{commonlabels.nextpage}}</span>
				</a>
			</li>
			<li class="page-item">
				<a class="page-link" href="{{sezione.url}}/{{pager.pageandquerystring | string.replace "{0}" pager.pages}}" aria-label="{{commonlabels.lastpage}}">
					<span class="fa fa-angle-double-right" role="presentation"></span>
					<span aria-hidden="true" class="sr-only">{{commonlabels.lastpage}}</span>
				</a>
			</li>
			{{end }}
		</ul>
	</nav>  
	<!-- fine paginazion -->		
	{{else}}		
	<p>{{commonlabels.norecord}}</p>      
	{{end}}
</div>