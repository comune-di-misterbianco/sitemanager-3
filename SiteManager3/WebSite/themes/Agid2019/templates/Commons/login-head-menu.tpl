﻿<div class="it-user-wrapper nav-item dropdown">
    <a aria-expanded="false" class="btn btn-primary btn-icon btn-full" data-toggle="dropdown" href="#">
        <span class="rounded-icon">
            <img src="https://lorempixel.com/20/20/people" class="border rounded-circle icon-white" alt="{{data.nomeutente}}">
        </span>
        <span class="d-none d-lg-block">{{data.nomeutente}}</span>
        <svg class="icon icon-white d-none d-lg-block">
            <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-expand"></use>
        </svg>
    </a>
    <div class="dropdown-menu">
        <div class="row">
            <div class="col-12">
                <div class="link-list-wrapper">
                    <ul class="link-list">
                        <li>
                            <a class="list-item" href="{{data.servizipageurl}}"><span>I miei servizi</span></a>
                        </li>
                        <li>
                            <a class="list-item" href="{{data.pratichepageurl}}"><span>Le mie pratiche</span></a>
                        </li>
                        <li>
                            <a class="list-item" href="{{data.notifichepageurl}}"><span>Notifiche</span></a>
                        </li>
                        <li>
                            <span class="divider"></span>
                        </li>
                        <li>
                            <a class="list-item" href="{{data.profilopageurl}}"><span>Profilo utente</span></a>
                        </li>
                        <li>
                            <a class="list-item" href="{{data.editprofilopageurl}}"><span>Modifica dati profilo</span></a>
                        </li>
                        <li>
                            <a class="list-item" href="{{data.editpasswordpageurl}}"><span>Modifica password</span></a>
                        </li>
                        <li>
                            <a class="list-item left-icon" href="{{data.logoutlink}}">
                                <svg class="icon icon-primary icon-sm left">
                                <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-external-link"></use>
                                </svg>
                                <span class="font-weight-bold">Esci</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>