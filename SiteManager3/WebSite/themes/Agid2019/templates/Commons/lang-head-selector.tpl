﻿<div class="nav-item dropdown">
	<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">
		<span class="text-uppercase">{{currentlang.label | string.slice 0 3}}</span>                  
		<svg class="icon d-none d-lg-block">
		<use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-expand"></use>
		</svg>                  
	</a>
	<div class="dropdown-menu">
		<div class="row">
			<div class="col-12">
				<div class="link-list-wrapper">                       
					<ul>
					{{for lang in langs }}
						<li><a href="{{lang.url}}" class="list-item text-uppercase"><span lang="{{lang.twolettercode}}">{{lang.label | string.slice 0 3 }}</span></a></li>
					{{end}}    
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>