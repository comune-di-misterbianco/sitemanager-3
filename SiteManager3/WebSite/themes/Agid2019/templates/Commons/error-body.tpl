﻿<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="error-template text-center mt-5">
				<h1>{{data.statuscode}}</h1>
				<h2>{{data.errortitle}}</h2>
				<p>{{data.errormessage}} 
					<a href ="javascript:history.back();" title="{{data.backlinktitle}}">{{data.backlinklabel}}</a>
				</p>
				<a class="btn btn-primary mb-5" href="{{data.homepagelink}}" title="{{data.backlinktitle}}">{{data.backlinklabel}}</a>
			</div>
		</div>
	</div>
</div>