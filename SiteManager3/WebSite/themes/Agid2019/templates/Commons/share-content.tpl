﻿<div class="dropdown d-inline show">
  <button class="btn btn-dropdown dropdown-toggle" type="button" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <svg class="icon">
      <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-share"></use>
    </svg>
    <small>{{commonlabels.labelsharebutton}}</small>
  </button>
  <div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
    <div class="link-list-wrapper">
      <ul class="link-list">
        <li>
          <a class="list-item" href="javascript:void(0)"
				onclick="window.open('https://www.facebook.com/sharer.php?u={{document.docurl}}','sharer','toolbar=0,status=0,width=548,height=325');"
				title="{{commonlabels.titlesharefb}}">
            <svg class="icon">
              <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-facebook"></use>
            </svg>
            <span class="d-none d-md-inline-flex">Facebook</span>
          </a>
        </li>
        <li>
          <a class="list-item" title="{{commonlabels.titlesharetw}}" target="_blank" href="http://www.twitter.com/share?text={{document.descforquerystring}}&url={{document.docurl}}">
            <svg class="icon">
              <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-twitter"></use>
            </svg>
            <span class="d-none d-md-inline-flex">Twitter</span>
          </a>
        </li>
        <li>
          <a class="list-item" href="https://www.linkedin.com/shareArticle?url={{document.docurl}}&title={{document.title}}&summary={{document.descforquerystring}}&source=&mini=true">
            <svg class="icon">
              <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-linkedin"></use>
            </svg>
            <span class="d-none d-md-inline-flex">Linkedin</span>
          </a>
        </li>
        <li>
          <a class="list-item" title="{{commonlabels.titlesharewa}}" href="whatsapp://send?text={{document.docurl}}">
            <svg class="icon">
              <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-whatsapp"></use>
            </svg>
            <span class="d-none d-md-inline-flex">Whatsapp</span>
          </a>
        </li>
      </ul>
    </div>
  </div>
</div>