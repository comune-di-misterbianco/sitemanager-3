﻿$(document).ready(function () {
    if ($(".article-body, .page-content").length) {
        var txt = $(".article-body, .page-content")[0].textContent,

            wordCount = txt.replace(/[^\w ]/g, "").split(/\s+/).length;

        var readingTimeInMinutes = Math.floor(wordCount / 228) + 1;
        var readingTimeAsString = readingTimeInMinutes + " min";

        $('.reading-time').html(readingTimeAsString);
    }

    /* Toolbar Side Button*/
    $('.btn-toggle').click(function () {
        var $this = $(this);
        
        var menu = $this.data("target");
        $(menu).toggleClass('d-block');
    });
	
    $('.owl-nav button').click(function (){
	event.preventDefault()
    });
    $(".toggle-password").click(function () {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") === "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
});
