﻿$(document).ready(function () {
    $('.megamenu').each(function (index, elem) {

        var link_orig = $(elem).find('a').first();

        var link_copy = $('<a/>');

        if (window.location.pathname.indexOf(link_orig.attr('href')) === 0) {
            $(elem).addClass('is-active');
        }

        if ($(elem).find('a').length === 1)
            return;

        link_copy.attr('href', link_orig.attr('href'));
        link_copy.html(link_orig.html());

        link_copy.find('svg').first().remove();

        var div = $(elem).find('div').first();        

        var li = $('<li/>', { class: 'list-inline-item' });
        li.append('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
        li.append(link_copy);
      
        var ul = $('<ul/>', { class: 'list-inline' });
        ul.append(li);

        var row = $('<div/>', { class: 'row topmenu-items' });
        row.append(ul);
        div.append(row);

        $(link_orig).click(function (e) {
            e.preventDefault();
        });

    }
    );
});