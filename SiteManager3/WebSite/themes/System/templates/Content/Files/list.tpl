﻿<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="">{{sezione.descrizione}}</p>
            </div>
        </div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
            <div class="page-content">
                 {{if documents | object.size > 0}}
					<ul class="u-text-r-xs">
					 {{ for doc in documents }}		
					 


						{{

						doccssclass = ""
						if (doc.itemtype == "file")
							doccssclass = "fa fa-file-text-o"
						else if (doc.itemtype == "link")
							doccssclass = "fa fa-link"
						else
							doccssclass = "fa fa-external-link"
						end
						
						}}	


						 <li  class="u-text-r-xxs u-border-top-xxs u-padding-r-all {{if for.odd }}u-background-5{{end }}" > 							 
								<i class="{{doccssclass}} fa-lg" aria-hidden="true"></i>
								<a href="{{doc.url}}" {{if (doc.itemtype == "extlink")}} target="_blank"{{end}} title="{{ doc.descrizione}}">
									{{doc.titolo}}
								</a>
								{{if doc.descrizione != doc.titolo}}
								<p class="u-text-r-xxl u-margin-r-left  u-text-p u-textSecondary">{{doc.descrizione}}</p>
								{{end }}		 
                         </li>
					 {{end }}
					<ul>
				  {{end }}
            </div>
        </div>
    </div>
	
	{{if sezione.menu != false }}
				
		 <div class="Grid Grid--withGutter">
            {{if sezione.folders | object.size > 0}}
            {{for folder in sezione.folders}}
            <div class="Grid-cell u-sm-size1of2 u-md-size1of2 u-lg-size1of3 u-padding-r-all">
                <div class="u-border-top-l u-color-70 u-text-r-l u-padding-r-bottom">
                    <h2 class="u-text-h3 u-padding-r-top">
                        <a href="{{folder.url}}">{{folder.titolo}}</a>
                    </h2>
                    <p class="Prose u-padding-r-top u-padding-r-bottom">
                        {{folder.descrizione}}
                    </p>
                    {{if folder.childfolder.size > 0 || folder.documents.size > 0}}
                    <ul class="Linklist Prose u-text-r-xs">
                        {{ for subfolder in folder.childfolder}}                        
							<li><a href="{{subfolder.url}}" title="{{subfolder.descrizione}}">{{subfolder.titolo}}</a></li>
                        {{  end }}
						
                        {{
						
						maxfiles = folder.documents.size
						
						for doc in folder.documents limit:5
						}}						
						{{
							cssclass = "fa-file-o"
						    							
							if (doc.url | string.contains "http" )							 							
								cssclass = "fa-external-link"
							else
								name = doc.titolo | string.split '.'
								fileext = name | array.last
							
								case fileext
									when "pdf"
										cssclass = " fa-file-pdf-o"
									when "zip","rar","tar","gzip"
										cssclass = " fa-file-archive-o"
									when "doc","docx"
										cssclass = " fa-file-word-o"
								end							
							end 
							}}
						
                        <li>
							<a class="u-text-r-xxs u-padding-r-left"  {{if (cssclass == "fa-external-link")}} target="_blank"{{end}} href="{{doc.url}}" title="{{doc.descrizione}}">
								<i class="fa {{cssclass}}" aria-hidden="true"></i>&nbsp;
								{{doc.titolo}}
							</a>
						</li>
                        {{ end }}
						{{ if maxfiles > 10 }}
						<li class="u-textCenter u-text-md-right u-text-lg-right ">
							<a class="u-color-50 u-textClean u-text-h5" href="{{folder.url}}" title="{{labels.alldocstitle}}" >
								{{labels.alldocs}}
								<span class="Icon Icon-chevron-right"></span>
							</a>
						</li>
						{{ end }}
                    </ul>
                    {{  end }}
                </div>
            </div>
            {{  end }}
            {{end}}



            {{if sezione.pages | object.size > 0}}           
            {{for page in sezione.pages}}
            <div class="Grid-cell u-sm-size1of2 u-md-size1of2 u-lg-size1of3 u-padding-r-all">
                <div class="u-border-top-l u-color-70 u-text-r-l u-padding-r-bottom">
                    <h2 class="u-text-h3 u-padding-r-top">
                        <a href="{{page.url}}">{{page.titolo}}</a>
                    </h2>
                    <p class="Prose u-padding-r-top u-padding-r-bottom">
                        {{page.descrizione}}
                    </p>
                </div>
            </div>
            {{  end }}
            {{end}}
        </div>
		
    {{end}}
</div>