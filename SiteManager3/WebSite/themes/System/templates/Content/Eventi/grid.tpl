﻿<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            {{if documents.size > 0 }}
            {{
           itemscount = documents.size
           k = 1
           i = 1
            }}
            {{for doc in documents }}
            {{
             if i == 5
                i = 1
             end
            }}
            {{if i == 1}}
            <div class="Grid Grid--withGutter">
            {{end}}
                <div class="Grid-cell u-md-size1of4 u-lg-size1of4">
                    <div class="u-color-grey-30 u-border-top-xxs u-padding-right-xxl u-padding-r-all">
                        <p class="u-padding-r-bottom">
                            <a class="u-textClean u-textWeight-700 u-text-r-xs u-color-50" href="{{doc.folderpath}}/default.aspx">{{doc.categoria}}</a>
                            <span class="u-text-r-xxs u-textSecondary u-textWeight-400 u-lineHeight-xl u-cf">{{doc.data}}</span>
                        </p>
                        <h3 class="u-padding-r-top u-padding-r-bottom">
                            <a class="u-text-h4 u-textClean u-color-black" href="?evento={{doc.id}}">
                                {{doc.titolo}}
                            </a>
                        </h3>
                        <p class="u-lineHeight-l u-text-r-xs u-textSecondary u-padding-r-right">
                            {{doc.descrizione}}
                        </p>
                    </div>
                </div>
            {{if i == 4 || k == itemscount}}
            </div>
            {{end}}
            {{
            i = i + 1
            k = k + 1
            }}
            {{end}}
         





           <!-- paginazion -->
		 <nav aria-label="Page navigation">
                <ul class="pagination">
                    <li class=""><a class="disabled" href="#">Pagina {{ pager.current }} di {{ pager.pages }}</a></li>
                    {{ if pager.current > 1}}
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}=1" aria-label="{{commonlabels.firstpage}}">
                            <span class="fa fa-angle-double-left"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.firstpage}}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current-1}}" aria-label="{{commonlabels.prevpage}}">
                            <span class="fa fa-angle-left" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.prevpage}}</span>
                        </a>
                    </li>
                    {{end }}

                    {{ for i in pager.lowrange..pager.toprange}}
                    <li {{ if  i == pager.current }} class="active" {{end}}>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{i}}">{{i}}</a>
                    </li>
                    {{end }}

                    {{ if pager.current < pager.pages}}
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current+1}}" aria-label="{{commonlabels.nextpage}}">
                            <span class="fa fa-angle-right" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.nextpage}}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.pages}}" aria-label="{{commonlabels.lastpage}}">
                            <span class="fa fa-angle-double-right" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.lastpage}}</span>
                        </a>
                    </li>
                    {{end }}
                </ul>
            </nav>
		<!-- fine paginazion -->	     

            {{else}}
            <p>{{commonlabels.norecord}}</p>
            {{end}}
        </div>
    </div>
 </div>

