﻿<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            {{if documents.size > 0}}

            {{ for doc in documents }}
            <!-- lista news -->
            <article class="item news">
                <p class="inner-date">
                    <i class="glyphicon glyphicon-calendar data_news" aria-hidden="true"></i>
                    <time datetime="{{ doc.data }}">{{ doc.data }}</time>
                </p>
                <a href="?evento={{doc.id}}" title="{{ doc.titolo | string.truncate 50}}">
                    <h4>{{ doc.titolo }}</h4>

                    {{if doc.imgevidenza != false }}
                    <figure class="image">
                        <img class="img-thumbnail {{doc.imgevidenza.cssclass}}" src="{{doc.imgevidenza.src}}" alt="{{doc.imgevidenza.description}}">
                    </figure>
                    {{ end }}
                </a>
                <div class="inner-news-body">
                    <p>{{ doc.descrizione }}</p>
                </div>
            </article>            

            {{ end }}

            <nav aria-label="Page navigation">

                <ul class="pagination">

                    {{ if pager.current > 1}}
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current-1}}" aria-label="{{commonlabels.prevpage}}">
                            <span class="Icon-chevron-left" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.prevpage}}</span>
                        </a>
                    </li>
                    {{end }}

                    {{ for i in 1..pager.pages }}

                    {{
                            cssclass = ""
                            if  i == pager.current
                                cssclass = "active"
                            end
                    }}

                    <li class="{{cssclass}}">
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{i}}">
                            {{i}}
                        </a>
                    </li>
                    {{end }}

                    {{ if pager.current < pager.pages}}
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current-1}}" aria-label="{{commonlabels.nextpage}}">
                            <span class="Icon-chevron-right" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.nextpage}}</span>
                        </a>
                    </li>
                    {{end }}
                </ul>
            </nav>
            {{else}}
            <p>{{commonlabels.norecord}}</p>
            {{end}}
        </div>
    </div>
 </div>