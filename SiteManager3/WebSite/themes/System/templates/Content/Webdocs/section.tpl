﻿<div class="container">
    <div class="u-layout-wide u-layoutCenter u-layout-withGutter u-padding-r-top u-padding-bottom-xxl">
        <h1 class="u-text-h2">
            {{sezione.titolo}}
        </h1>
       
        <div class="Grid Grid--withGutter">
			<div class="Grid-cell  {{if sezione.menu == "false" }} u-md-size12of12 u-lg-size12of12 {{ else }} u-md-size9of12 u-lg-size9of12 {{end}}">
                <article class="pagina">
                    <div class="Grid Grid--withGutter">
                        <div class="date Grid-cell u-size1of2">
                            <p><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;{{commonlabels.lastrevision}}: {{sezione.data}}</p>							
                        </div>
	  
						{{if sezione.menu == "false" }}
						<div class="share Grid-cell u-size1of2">
							{{sharecontent}}
						</div>
						{{end}}

                        <div class="print Grid-cell u-size1of1 text-left">
                            <p>
                                <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                                <a href="javascript:this.print();" title="{{commonlabels.printtitle}}">{{commonlabels.print}}</a>
                            </p>
                        </div>
	  
                    </div>
                
	 
                    <p class="description">{{sezione.descrizione}}</p>
                    <div class="page-content">
                        {{sezione.testo}}
                    </div>                   
                    {{if sezione.tags | object.size > 0}}
                    <div class="doc-tags">
                        <i class="fa fa-tags" aria-hidden="true"></i>
                        <span>{{commonlabels.tagstitle}}:</span>
                        {{ for tag in sezione.tags }}
                        <a class="doc-tag" rel="tag" href="/tags/argomenti.aspx?tag={{tag.key}}">{{tag.label}}</a>
                        {{end}}
                    </div>                    
                    {{end}}					
                </article>
            </div>
                          
			   
			{{if sezione.menu != false }}
				<div class="Grid-cell u-sizeFull u-md-size3of12 u-lg-size3of12">		
	 
					<div class="share u-text-r-xxl u-padding-r-bottom ">
								{{sharecontent}}
					</div>
					          
					<div class="u-sizeFull u-md-size12of12 u-lg-size12of12 menu-sezione" id="subnav">
						{{sezione.menu}}
					</div>
				</div>
            {{end}}
				               
            
        </div>
    </div>
</div>