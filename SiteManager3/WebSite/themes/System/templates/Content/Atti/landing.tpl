﻿<div class="container">
    <section class="u-layout-wide u-layout-withGutter u-layoutCenter u-padding-r-top">
        <h1 class="u-text-h2">{{sezione.titolo}}</h1>
        <p class="">{{sezione.descrizione}}</p>
        <div class="Grid Grid--withGutter">
            {{if sezione.folders | object.size > 0}}
            {{for folder in sezione.folders}}
            <div class="Grid-cell u-sm-size1of2 u-md-size1of2 u-lg-size1of3 u-padding-r-all">
                <div class="u-border-top-l u-color-70 u-text-r-l u-padding-r-bottom">
                    <h2 class="u-text-h3 u-padding-r-top">
                        <a href="{{folder.url}}">{{folder.titolo}}</a>
                    </h2>
                    <p class="Prose u-padding-r-top u-padding-r-bottom">
                        {{folder.descrizione}}
                    </p>
                    {{if folder.childfolder.size > 0 }} {{##|| folder.documents.size > 0}}
                    <ul class="Linklist Prose u-text-r-xs">
                        {{ for subfolder in folder.childfolder}}
                        {{subfolder}}
                        <li><a href="{{subfolder.url}}" title="{{subfolder.descrizione}}">{{subfolder.titolo}}</a></li>
                        {{  end }}                        
                    </ul>
                    {{  end }}
                </div>
            </div>
            {{  end }}
            {{end}}



            {{if sezione.pages | object.size > 0}}
            {{for page in sezione.pages}}
            <div class="Grid-cell u-sm-size1of2 u-md-size1of2 u-lg-size1of3 u-padding-r-all">
                <div class="u-border-top-l u-color-70 u-text-r-l u-padding-r-bottom">
                    <h2 class="u-text-h3 u-padding-r-top">
                        <a href="{{page.url}}">{{page.titolo}}</a>
                    </h2>
                    <p class="Prose u-padding-r-top u-padding-r-bottom">
                        {{page.descrizione}}
                    </p>
                </div>
            </div>
            {{  end }}
            {{end}}
        </div>
    </section>
</div>