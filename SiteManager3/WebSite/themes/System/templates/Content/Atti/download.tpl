﻿<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
         <p>{{sezione.message}}</p>
        </div>
    </div>
</div>