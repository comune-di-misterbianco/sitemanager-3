﻿<div class="container">
    <article id="content" class="bando">      
        <div class="news-body col-md-9">
            <header>
                <h1 class="title">{{document.titolo}}</h1>

                <div class="description">{{document.descrizione}}</div>

                {{if options.statodatapublicazione != false }}
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;{{labels.datapubblicazione}}: {{document.datapubblicazione | date.to_string '%d/%m/%Y' }}</div>
                {{end}}

                {{if options.statodatascadenza != false }}
                {{if options.statotimescadenza == false }}
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;{{labels.datascadenza}}: {{document.datascadenza | date.to_string '%d/%m/%Y'}}</div>
                {{else}}
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;{{labels.datascadenza}}: {{document.datascadenza | date.to_string '%d/%m/%Y %H.%M'}}</div>
                {{end}}
                {{end}}

                {{if options.statodatainizio != false }}
                {{if options.statotimeinizio == false }}
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;{{labels.datainizio}}: {{document.datainizio | date.to_string '%d/%m/%Y'}}</div>
                {{else}}
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;{{labels.datainizio}}: {{document.datainizio | date.to_string '%d/%m/%Y %H.%M'}}</div>
                {{end}}
                {{end}}

                <div class="identificativo">
                    <i class="fa fa-archive" aria-hidden="true"></i>
                    {{labels.numeroidentificativo}}: {{document.identificativo}}
                </div>

                {{if document.importo == 0.00 }}
                <div class="importo">
                    <i class="fa fa-eur" aria-hidden="true"></i>
                    {{labels.importo}}:&nbsp;{{labels.noimporto}}
                </div>
                {{else}}
                <div class="importo">
                    <i class="fa fa-eur" aria-hidden="true"></i>
                    {{labels.importo}}:&nbsp;{{document.importo | math.format "C2"}}
                </div>
                {{end}}

                <div class="ufficio">
                    <i class="fa fa-building-o" aria-hidden="true"></i>
                    {{labels.ufficio}}:&nbsp;{{document.ufficio}}
                </div>

                <div class="categoria">
                    <i class="fa fa-folder-o" aria-hidden="true"></i>
                    {{labels.opera}}:&nbsp;{{ document.categoria}}
                </div>

                <div class="procedura">
                    <i class="fa fa-tag" aria-hidden="true"></i>
                    {{labels.procedura}}:&nbsp;{{document.procedura}}
                </div>
            </header>

            <div class="bando-text u-margin-bottom-l u-margin-top-l">
                {{document.testo}}
            </div>
        </div>
        <div class="bando-column col-md-3">

            <!--<div class="share text-right">
                         {{##sharecontent}}
            </div>-->

            {{if options.statochiarimenti != false}}
            <div class="chiarimenti">
                <p>
                    <a class="btn btn-default" href="/chiarimenti/richiesta.aspx?doc={{document.docid}}">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        {{labels.labelchiarimenti}}
                    </a>
                </p>
            </div>
            {{end}}

            <div class="column-content u-margin-bottom-l">
                {{if document.allegati != empty}}
                <div class="allegati">
                    <h4 class="u-border-bottom-xxs"> {{labels.documenti}}</h4>
                    <ul class="">
                        {{for allegato in document.allegati}}
                        <li class="allegato {{allegato.ext}}">
						    {{
							cssclass = "fa-file-o"
							fileext = allegato.ext
							case fileext
								when "pdf"
									cssclass = " fa-file-pdf-o"
								when "zip","rar","tar","gzip"
									cssclass = " fa-file-archive-o"
								when "doc","docx"
									cssclass = " fa-file-word-o"
								end
							}}

                            <i class="fa {{cssclass}}" aria-hidden="true"></i>&nbsp;<a rel="esterno" title="{{labels.filedownloadtitle}} {{allegato.filename}}" href="{{allegato.filepath}}">{{allegato.desc}}</a>
                        </li>
                        {{end}}
                    </ul>
                </div>
                {{end}}

                {{if document.eventi != empty}}
                <div class="avanzamento">
                    <h4 class="u-border-bottom-xxs">{{labels.statodiavanzamento}}</h4>
                    <ul class="">
                        {{for evento in document.eventi}}
                        <li class="allegato {{evento.fileext}} ">
                            <p class="dataEvento">
		
							{{ 
									cssclass = ""
									tipoevento = evento.idtipo
									case tipoevento 
										when 1
										    cssclass = " fa-check-circle text-success"
										when 2
											cssclass = " fa-exclamation-circle text-danger"
										else
											cssclass = " fa-exclamation-circle text-warning"
									end
								
								}}
								<i class="fa {{cssclass}}" aria-hidden="true"></i>&nbsp;

								<span class="event-type">{{evento.nometipo}}</span>	<br />							
								<i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;
								<span class="event"> {{evento.data}}</span>
							</p>
                            <p class="linkEvento">
								{{
									cssclass = "fa-file-o"
									fileext = evento.fileext
									case fileext
										when "pdf"
											cssclass = " fa-file-pdf-o"
										when "zip","rar","tar","gzip"
											cssclass = " fa-file-archive-o"
										when "doc","docx"
											cssclass = " fa-file-word-o"
									end
								}}
		  
								<i class="fa {{cssclass}}" aria-hidden="true"></i>&nbsp;
								<a rel="esterno" title="{{labels.filedownloadtitle}}  {{evento.filename}}" href="{{evento.filepath}}">{{evento.testo}}</a>
							</p>
                        </li>
                        {{end}}
                    </ul>
                </div>
                {{end}}
            </div>

        </div>
    </article>
</div>