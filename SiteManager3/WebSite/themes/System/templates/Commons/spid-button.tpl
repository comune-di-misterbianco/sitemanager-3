﻿{{ 
    spidauthurl = "/spid/initsso.aspx"
    if data.serviceindex == "2"
      spidauthurl = "/bonusspesa/initsso.aspx"
    end
}} 

<link type="text/css" rel="stylesheet" href="/resources/static/spid-access-button/css/spid-sp-access-button.min.css" />
 <div class="text-center mb-5">
<!-- AGID - SPID IDP BUTTON MEDIUM "ENTRA CON SPID" * begin * -->
    <a href="#" 
       class="italia-it-button italia-it-button-size-m button-spid" 
        spid-idp-button="#spid-idp-button-medium-get" 
        aria-haspopup="true" 
        aria-expanded="false">
        <span class="italia-it-button-icon"><img src="/resources/static/spid-access-button/img/spid-ico-circle-bb.svg" 
        onerror="this.src='/resources/static/spid-access-button/img/spid-ico-circle-bb.png'; this.onerror=null;" alt="" /></span>
        <span class="italia-it-button-text">Entra con SPID</span>
    </a>
    <div id="spid-idp-button-medium-get" 
         class="spid-idp-button spid-idp-button-tip spid-idp-button-relative">
        
         <ul id="spid-idp-list-medium-root-get" class="spid-idp-button-menu" aria-labelledby="spid-idp">
                                                      <li class="spid-idp-button-link">
                                                          <a href="{{spidauthurl}}?ProviderID=namirialid&ServiceId={{data.serviceindex}}">
                                                            <span class="spid-sr-only">Namirial ID</span>
                                                            <img src="/resources/static/spid-access-button/img/spid-idp-namirialid.svg" onerror="this.src='/resources/static/spid-access-button/img/spid-idp-namirialid.png'; this.onerror=null;" alt="Namirial ID">
                                                          </a>
                                                      </li>
                                                      <li class="spid-idp-button-link">
                                                          <a href="{{spidauthurl}}?ProviderID=spiditalia&ServiceId={{data.serviceindex}}">
                                                            <span class="spid-sr-only">SPIDItalia Register.it</span>
                                                            <img src="/resources/static/spid-access-button/img/spid-idp-spiditalia.svg" onerror="this.src='/resources/static/spid-access-button/img/spid-idp-spiditalia.png'; this.onerror=null;" alt="SPIDItalia Register.it">
                                                          </a>
                                                      </li>
                                                      <li class="spid-idp-button-link">
                                                          <a href="{{spidauthurl}}?ProviderID=arubaid&ServiceId={{data.serviceindex}}">
                                                            <span class="spid-sr-only">Aruba ID</span>
                                                            <img src="/resources/static/spid-access-button/img/spid-idp-arubaid.svg" onerror="this.src='/resources/static/spid-access-button/img/spid-idp-arubaid.png'; this.onerror=null;" alt="Aruba ID">
                                                          </a>
                                                      </li>
                                                      <li class="spid-idp-button-link">
                                                          <a href="{{spidauthurl}}?ProviderID=infocert&ServiceId={{data.serviceindex}}">
                                                            <span class="spid-sr-only">Infocert ID</span>
                                                            <img src="/resources/static/spid-access-button/img/spid-idp-infocertid.svg" onerror="this.src='/resources/static/spid-access-button/img/spid-idp-infocertid.png'; this.onerror=null;" alt="Infocert ID">
                                                          </a>
                                                      </li>
                                                      <li class="spid-idp-button-link">
                                                          <a href="{{spidauthurl}}?ProviderID=poste&ServiceId={{data.serviceindex}}">
                                                            <span class="spid-sr-only">Poste ID</span>
                                                            <img src="/resources/static/spid-access-button/img/spid-idp-posteid.svg" onerror="this.src='/resources/static/spid-access-button/img/spid-idp-posteid.png'; this.onerror=null;" alt="Poste ID">
                                                          </a>
                                                      </li>
                                                      <li class="spid-idp-button-link">
                                                          <a href="{{spidauthurl}}?ProviderID=sielte&ServiceId={{data.serviceindex}}">
                                                            <span class="spid-sr-only">Sielte ID</span>
                                                            <img src="/resources/static/spid-access-button/img/spid-idp-sielteid.svg" onerror="this.src='/resources/static/spid-access-button/img/spid-idp-sielteid.png'; this.onerror=null;" alt="Sielte ID">
                                                          </a>
                                                      </li>
                                                      <li class="spid-idp-button-link">
                                                          <a href="{{spidauthurl}}?ProviderID=tim&ServiceId={{data.serviceindex}}">
                                                            <span class="spid-sr-only">Tim ID</span>
                                                            <img src="/resources/static/spid-access-button/img/spid-idp-timid.svg" onerror="this.src='/resources/static/spid-access-button/img/spid-idp-timid.png'; this.onerror=null;" alt="Tim ID">
                                                          </a>
                                                      </li>
                                                      <li class="spid-idp-button-link">
                                                          <a href="{{spidauthurl}}?ProviderID=intesa&ServiceId={{data.serviceindex}}">
                                                            <span class="spid-sr-only">Intesa ID</span>
                                                            <img src="/resources/static/spid-access-button/img/spid-idp-intesaid.svg" onerror="this.src='/resources/static/spid-access-button/img/spid-idp-intesaid.png'; this.onerror=null;" alt="Intesa ID">
                                                          </a>
                                                      </li>
                                                      <li class="spid-idp-button-link">
                                                          <a href="{{spidauthurl}}?ProviderID=lepida&ServiceId={{data.serviceindex}}">
                                                            <span class="spid-sr-only">Lepida ID</span>
                                                            <img src="/resources/static/spid-access-button/img/spid-idp-lepidaid.svg" onerror="this.src='/resources/static/spid-access-button/img/spid-idp-lepidaid.png'; this.onerror=null;" alt="Lepida S.P.A.">
                                                          </a>
                                                      </li>
                                                      <!--
                                                      <li class="spid-idp-button-link">
                                                            <a href="{{spidauthurl}}?ProviderID=spid_validator">
                                                                <span class="spid-sr-only">Spid Validator</span>
                                                                <img src="/resources/static/spid-access-button/img/spid-idp-spid_validator.png" onerror="this.src='/resources/static/spid-access-button/img/spid-idp-spid_validator.png'; this.onerror=null;" alt="Spid Validator">
                                                             </a>
                                                        </li>
                                                        -->
                                                      <li class="spid-idp-support-link">
                                                          <a href="http://www.spid.gov.it">Maggiori info</a>
                                                      </li>
                                                      <li class="spid-idp-support-link">
                                                          <a href="http://www.spid.gov.it/richiedi-spid">Non hai ?</a>
                                                      </li>
                                                  </ul>
    </div>
    <!-- AGID - SPID IDP BUTTON MEDIUM "ENTRA CON SPID" * end * -->
    <!-- 
    <p class="spid-idp-support-link">
        <a href="https://www.spid.gov.it">Maggiori informazioni</a>
    </p>
    <p class="spid-idp-support-link">
        <a href="https://www.spid.gov.it/richiedi-spid">Non hai ?</a>
    </p>
    <p class="spid-idp-support-link">
        <a href="https://www.spid.gov.it/serve-aiuto">Serve aiuto?</a>
    </p> 
    -->
   </div>
     <script>
       $(document).ready(function(){
            var rootList = $("#spid-idp-list-medium-root-get");
            var idpList = rootList.children(".spid-idp-button-link");
            var lnkList = rootList.children(".spid-idp-support-link");
            while (idpList.length) {
                rootList.append(idpList.splice(Math.floor(Math.random() * idpList.length), 1)[0]);
            }
            rootList.append(lnkList);
        });
        $(document).ready(function(){
			    var rootList = $("#spid-idp-list-medium-root-post");
			    var idpList = rootList.children(".spid-idp-button-link");
			    var lnkList = rootList.children(".spid-idp-support-link");
			    while (idpList.length) {
			        rootList.append(idpList.splice(Math.floor(Math.random() * idpList.length), 1)[0]);
			    }
			    rootList.append(lnkList);
			});
     </script>
      <script type="text/javascript" src="/resources/static/spid-access-button/js/spid-sp-access-button.min.js"></script>