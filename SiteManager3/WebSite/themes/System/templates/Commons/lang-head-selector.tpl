﻿<div class="Header-languages ">   
	<a href="#languages" data-menu-trigger="languages" class="Header-language u-border-none u-zindex-max u-inlineBlock">
				<span class="u-hiddenVisually">lingua attiva:</span>
				<span class="">{{currentlang.label}}</span>
				<span class="Icon Icon-expand u-padding-left-xs"></span>
	</a>	

	<div id="languages" data-menu class="Dropdown-menu Header-language-other u-jsVisibilityHidden u-nojsDisplayNone">
	<span class="Icon-drop-down Dropdown-arrow u-color-white"></span>
	<ul>
		{{for lang in langs }}
		<li><a href="{{lang.url}}" class="u-padding-r-all"><span lang="{{lang.twolettercode}}">{{lang.label}}</span></a></li>
		{{end}}    
	</ul>
	</div>
</div>