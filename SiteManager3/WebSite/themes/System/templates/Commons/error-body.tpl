﻿<div class="ErrorPage">
	<h1 class="ErrorPage-title">{{data.statuscode}}</h1>
	<h2 class="ErrorPage-subtitle">{{data.errortitle}}</h2>
	<p>{{data.errormessage}} 
		<a href ="javascript:history.back();" title="{{data.backlinktitle}}">{{data.backlinklabel}}</a>
	</p>
	<a href="{{data.homepagelink}}" title="{{data.backlinktitle}}">{{data.backlinklabel}}</a>
</div>