﻿<div class="it-hero-wrapper it-hero-small-size">
   <div class="img-responsive-wrapper">
      <div class="img-responsive">
         <div class="img-wrapper"><img src="{{coverimg.coverimgpath}}" title="{{coverimg.coverimgalt}}" alt="{{coverimg.coverimgalt}}"></div>
      </div>
   </div>
</div>