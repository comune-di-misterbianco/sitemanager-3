﻿<div class="card-wrapper card-space h-100">
  <div class="card card-bg no-after">
    <div class="card-body">
      <div class="card-text">
        <p class="border-bottom pb-2">
            <svg class="icon icon-primary">
                <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-chevron-right"></use>
            </svg> 
            {{ data.modulo.label }}
        </p>
        <div class="link-list-wrapper">	
        {{if data.items.size > 0}}
          <ul class="link-list">
            {{for item in data.items}}
			{{
				
			}}
            <li><a class="list-item" href="{{data.modulo.tagspageurl}}{{item | string.downcase | string.replace " " "-"}}">{{item | string.replace "-" " "}}</a></li>            
            {{end}}
          </ul>
        {{end}}
        </div>
      </div>
    </div>
  </div>
</div>