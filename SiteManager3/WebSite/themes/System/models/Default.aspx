<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeFile="Default.aspx.cs" Inherits="_Model" %>

<!DOCTYPE html>

<html <%=HTML5XmlLangInfo%>>

<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Untitled Page</title>
       
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  
</head>
<body id="CMSBody" runat="server">


    <form id="PageForm" runat="server">
        <header id="Intestazione" runat="server" class="page-header">
        </header>
        <main id="BodyCorpo" class="container-fluid" runat="server">

            <div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="LoginModal" runat="server">
            </div>
            <div class="row">
                <section id="Where" class="container-fluid" runat="server">
                </section>
                 <button id="colSxButton"  type="button" class=" visible-xs visible-sm btn btn-default coll" runat="server">
                     <div id="prova" class="clickableSX coll">
                         <span aria-hidden="true" title="Accedi al menu di navigazione per mobile" class="glyphicon glyphicon-chevron-right" ></span>
                         <span class="sr-only">Accedi al menu di navigazione per mobile</span>
                     </div>
                 </button>                
                </div>
            <div class="row">
                <div class="container-fluid">
                    <section id="ColSX"  runat="server">
                      
                    </section>
                    <section id="ColCX" runat="server">
                        
                    </section>
                    <section id="ColDX" runat="server">
                    </section>
                </div>
            </div>
            <div class="row">
                <section id="PiePagina" class="container-fluid" runat="server">
                </section>
            </div>
        </main>
        <footer id="Footer" class="panel-footer" runat="server">
        </footer>
    </form>
 
</body>
</html>
