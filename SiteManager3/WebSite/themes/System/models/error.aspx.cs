using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetFrontend;
using NetFrontend.FrontPiecies;
using System.Globalization;

public partial class _Model : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        

        if (Context.Items["StatusCode"] != null)
        {
            int statusCode = int.Parse(Context.Items["StatusCode"].ToString());            
            Response.StatusCode = statusCode;
            if (statusCode == 404)
                Response.StatusDescription = "Not found";
        }

        this.Form.Controls.Add(new LiteralControl("Error! " + Response.StatusDescription));
    }
}
