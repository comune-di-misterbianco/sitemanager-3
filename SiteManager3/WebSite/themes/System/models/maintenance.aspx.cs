using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetFrontend;
using NetFrontend.FrontPiecies;
using System.Globalization;

public partial class _Model: System.Web.UI.Page
{
    public string HTML5XmlLangInfo = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        //LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetLanguageFromUrl(Page.Request.RawUrl);

        string code = @"<div class=""logo text-center"">
                <p>
                    <span class=""titolosito d-none"">" + NetCms.Configurations.PortalData.Nome + @"</span>
                    <img src=""/imghome/logo.png"" alt="""" />
                </p>
            </div>";

        WebControl intInnerContent = new WebControl(HtmlTextWriterTag.Div);
        intInnerContent.CssClass = "container mt-5";
        intInnerContent.Controls.Add(new LiteralControl(code));

        code = @"<div class=""row"">
                    <div class=""col-md-6"">
                        <h1>Temporarily down for maintenance</h1>
                        <h2>We�ll be back soon!</h2>
                        <p>Sorry for the inconvenience but we�re performing some maintenance at the moment. we�ll be back online shortly!</p>
                    </div>
                    <div class=""col-md-6"">
                        <img class=""img-fluid"" src=""http://placeimg.com/640/360/tech"" alt=""Placeholder image""/>
                    </div>
                </div>
                ";

        WebControl bodyInnerContent = new WebControl(HtmlTextWriterTag.Div);
        bodyInnerContent.CssClass = "container mt-5";
        bodyInnerContent.Controls.Add(new LiteralControl(code));

        code = "<p class=\"text-center\">� Copyright " + DateTime.Now.Year + " " + NetCms.Configurations.PortalData.Nome+ " all rights reserved.</p>";
        WebControl footerInnerContent = new WebControl(HtmlTextWriterTag.Div);
        footerInnerContent.CssClass = "container";
        footerInnerContent.Controls.Add(new LiteralControl(code));

        Intestazione.Controls.Add(intInnerContent);
        corpo.Controls.Add(bodyInnerContent);
        Footer.Controls.Add(footerInnerContent);

        Page.Title = "Portal in maintenance mode";
        System.Web.HttpContext.Current.Response.TrySkipIisCustomErrors = true;
        System.Web.HttpContext.Current.Response.Status = "503 Service Unavailable"; 
        System.Web.HttpContext.Current.Response.StatusCode = 503; 
    }    
}
    