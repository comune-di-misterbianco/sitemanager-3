﻿function openEventCalendar(div) {
    $(".hidden-event-calendar").hide();

    $('.button-calendar-link').removeClass('button-calendar-active');
    $(div).addClass("button-calendar-active");

    var hiddenEvent = ($(div).find('.hidden-event-ref').val());

    if (hiddenEvent != null) {
        var arr = hiddenEvent.split(';');
        for (var i = 0; i < arr.length; i++) {
            $("#event-id-" + arr[i]).fadeIn(250);
        };
    };
};

$(document).ready(function () {
    if (document.querySelector('.button-calendar-link.has-event-today') !== null) {

        $('.button-calendar-link.has-event-today').addClass("button-calendar-active");

        var hiddenEvent = ($('.button-calendar-link.has-event-today').find('.hidden-event-ref').val());
        var arr = hiddenEvent.split(';');
        for (var i = 0; i < arr.length; i++) {
            $("#event-id-" + arr[i]).show();
        };
    }
});