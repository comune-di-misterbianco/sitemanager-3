﻿{{ 
menuisnull = menu.items | object.size > 0
if (menuisnull != false)	
}}	
	<nav class="navbar navbar-expand-lg has-megamenu">
              <button class="custom-navbar-toggler" type="button" aria-controls="nav10" aria-expanded="false" aria-label="Toggle navigation" data-target="#nav10">                
                <svg class="icon">
                  <use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-burger"></use>
                </svg>                
              </button>
              <div class="navbar-collapsable" id="nav10">
                <div class="overlay"></div>
               
                <div class="menu-wrapper">
				 <div class="close-div">
                   <button class="btn close-menu" type="button">
					<svg class="icon icon-primary">
						<use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-close-circle"></use>
					</svg>		
					<span class="sr-only">Close</span>
				   </button>
                </div>
					<ul class="navbar-nav">
						{{ if menu.usemegamenu == false }}
						
							{{for item in menu.items }}
							<li class="nav-item {{if item.isactive }} active {{end}}">
								<a class="nav-link {{if item.isactive }} active {{end}} {{item.cssclass}}" href="{{item.url}}">
									<span>{{item.label}}</span>								
								</a>
							</li>
							{{end}}
						{{else}}
							{{for item in menu.items }}
								<li class="nav-item {{if item.isactive }} active {{end}} {{if item.havechild == true }} dropdown megamenu {{end}}">
					   
									{{if item.havechild == true }}
									<a class="nav-link dropdown-toggle {{if item.isactive }} active {{end}} {{item.cssclass}}" {{if item.havechild == true}} href="{{item.url}}" data-toggle="dropdown" aria-expanded="false" {{else}} href="{{item.url}}" {{end}}>
										<span>{{item.label}}</span>
										<svg class="icon icon-xs">
											<use xlink:href="/bootstrap-italia/dist/svg/sprite.svg#it-expand"></use>
										</svg>
									</a>
									{{else}}
									<a class="nav-link {{if item.isactive }} active {{end}} {{item.cssclass}}" href="{{item.url}}" ><span>{{item.label}}</span></a>
									{{end}}
						
						
									{{ 
										itemsize = item.childs | object.size

										

										if itemsize > 0 
										dpmenucssclass = ""
											if itemsize <=5
												colsize =   12 / itemsize
												dpmenucssclass = "class=\"col-12 col-lg-"+colsize+"\""
											end 
									}}
									<div class="dropdown-menu">
										<div class="row">
											  {{for submenu in item.childs}}
											<div {{dpmenucssclass}}>											
												<div class="link-list-wrapper">
													  <ul class="link-list">
															<li>

																{{ 
																itemsize = item.childs | object.size
																
																if (itemsize < 5)
															    submenusize = submenu.childs | object.size	
																	if (submenusize > 0 )
																	}}
																	<h3 class=""><a class="list-item" href="{{submenu.url}}"><span>{{submenu.label}}</span></a></h3>
																	<ul class="link-list">
																		 {{for submenuitem in submenu.childs}}
																		 <li>
																			 <a class="list-item" href="{{submenuitem.url}}"><span>{{submenuitem.label}}</span></a>
																		 </li>
																		 {{ end }}
																	</ul>
																	{{else}}
																		<a class="list-item" href="{{submenu.url}}"><span>{{submenu.label}}</span></a>
																	{{ end }}
																{{else}}
																	<a class="list-item" href="{{submenu.url}}"><span>{{submenu.label}}</span></a>
																{{ end }}

															</li>
													 
													  </ul>
												</div>												
											</div>
											{{end}}

										</div>
									</div>
									{{end}}

								</li>
							{{end}}						
						{{end}}
					</ul>      
				</div>
              </div>
     </nav>	
{{end}}