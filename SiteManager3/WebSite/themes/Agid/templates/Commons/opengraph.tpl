﻿<!-- open graph -->
<meta property="og:title" content="{{og.title}}" />
<meta property="og:type" content="{{og.type}}" />
<meta property="og:url" content="{{og.url}}" />
{{if og.usehttps }}
<meta property="og:image:secure_url" content="{{og.imageurl}}" />
{{else}}
<meta property="og:image" itemprop="image" content="{{og.imageurl}}" />
{{end}}
<meta property="og:description" content="{{og.description}}" />
<meta property="og:site_name" content="{{og.websitename}}" />
<!-- end open graph -->


