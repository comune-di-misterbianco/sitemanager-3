﻿<div class="Grid">
	<div class="Grid-cell Grid-cell--center u-size10of12 u-sm-size8of12 u-md-size5of12 u-lg-size4of12">
		<div class="ErrorPage u-textCenter u-text-xxs u-text-md-xs u-text-lg-s">
			<h1 class="ErrorPage-title">{{data.statuscode}}</h1>
			<h2 class="ErrorPage-subtitle">{{data.errortitle}}</h2>
			<p class="Prose u-margin-r-all">{{data.errormessage}} 
				<a href ="javascript:history.back();" title="{{data.backlinktitle}}">{{data.backlinklabel}}</a>
			</p>
			<a class="Button Button--default u-margin-r-all" href="{{data.homepagelink}}" title="{{data.backlinktitle}}">{{data.backlinklabel}}</a>
		</div>
	</div>
</div>