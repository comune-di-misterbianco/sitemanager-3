﻿<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="u-layout-centerContent u-cf">
            {{if documents.size > 0}}
            <!-- masonry layout -->

            <section class="js-Masonry-container u-layout-medium u-margin-top-s" data-columns>

                {{ for doc in documents }}
                <div class="Masonry-item js-Masonry-item">
                    <div class="u-nbfc u-borderShadow-xxs u-borderRadius-m u-color-grey-30 u-background-white">


                        {{if doc.imgevidenza != false }}
                            {{if doc.imgevidenza.src | string.size > 0 }}
                            <img src="{{doc.imgevidenza.src}}" class="u-sizeFull" alt="{{doc.imgevidenza.description}}" />
                            {{end }}
                        {{end }}
                            <div class="u-text-r-l u-padding-r-all u-layout-prose">
                                <p class="u-text-h6 u-margin-bottom-l"><a class="u-color-50 u-textClean" href="{{doc.folderpath}}/default.aspx">{{doc.categoria}}</a></p>
                                <h3 class="u-text-h4 u-margin-r-bottom">
                                    <a class="u-text-r-m u-color-black u-textWeight-400 u-textClean" href="?evento={{doc.id}}" title="{{ doc.titolo | string.truncate 50}}">
                                        {{doc.titolo}}
                                    </a>
                                </h3>
                                <p class="u-text-p u-textNormal u-color-grey-60">
                                    <i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
                                    <time datetime="{{ doc.data }}">{{ doc.data }}</time>
                                </p>
                                <p class="u-text-p u-textSecondary">{{doc.descrizione}}</p>
                            </div>
                    </div>
                </div>
                {{end }}

            </section>
            </div>
           
            <div class="u-layout-centerContent u-cf">
                <!-- paginazion -->
		 <nav aria-label="Page navigation">
                <ul class="pagination">
                    <li class=""><a class="disabled" href="#">Pagina {{ pager.current }} di {{ pager.pages }}</a></li>
                    {{ if pager.current > 1}}
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}=1" aria-label="{{commonlabels.firstpage}}">
                            <span class="fa fa-angle-double-left"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.firstpage}}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current-1}}" aria-label="{{commonlabels.prevpage}}">
                            <span class="fa fa-angle-left" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.prevpage}}</span>
                        </a>
                    </li>
                    {{end }}

                    {{ for i in pager.lowrange..pager.toprange}}
                    <li {{ if  i == pager.current }} class="active" {{end}}>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{i}}">{{i}}</a>
                    </li>
                    {{end }}

                    {{ if pager.current < pager.pages}}
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current+1}}" aria-label="{{commonlabels.nextpage}}">
                            <span class="fa fa-angle-right" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.nextpage}}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.pages}}" aria-label="{{commonlabels.lastpage}}">
                            <span class="fa fa-angle-double-right" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.lastpage}}</span>
                        </a>
                    </li>
                    {{end }}
                </ul>
            </nav>
		<!-- fine paginazion -->	
            </div>
            {{else}}
                <p class="u-text-p u-textNormal u-color-grey-60">{{commonlabels.norecord}}</p>
            {{end}}            
        </div>
    </div>
 </div>