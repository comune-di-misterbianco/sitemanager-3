﻿<section class="Grid">
    {{if data.items.size > 0}}
    {{for item in data.items limit:1 }}
    <div class="Grid-cell u-sizeFull u-md-size1of2 u-lg-size1of2 u-text-r-s u-padding-r-all">
        <div class="Grid Grid--fit u-margin-r-bottom">
            <p class="Grid-cell">
                <span class="Dot u-background-50"></span>
                <strong><a class="u-textClean u-text-r-xs category" href="{{item.itemcategoryurl}}">{{item.categoria | string.capitalizewords}}</a></strong>
            </p>
            <p class="Grid-cell u-textSecondary">
                {{item.date}}
            </p>
        </div>
        <div class="u-text-r-l u-layout-prose">
            <h2 class="u-text-h2 u-margin-r-bottom">
                <a class="u-text-h2 u-textClean u-color-black" href="{{item.itemurl}}">
                    {{item.titolo}}
                </a>
            </h2>
            <p class="u-textSecondary u-lineHeight-l">
                {{item.descrizione}}
            </p>
        </div>
    </div>
    {{if data.modulo.showimages && item.img != false }}
    <div class="Grid-cell u-sizeFull u-md-size1of2 u-lg-size1of2 u-text-r-s u-padding-r-all">
        <img src="{{item.img.imgsrc}}" class="u-sizeFull" alt="..." />
    </div>
    {{end}}
    {{end}}
    {{end }}
</section>

