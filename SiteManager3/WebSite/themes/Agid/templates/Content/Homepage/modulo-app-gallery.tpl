﻿<div class="u-layout-centerContent u-background-grey-20 u-padding-bottom-xxl u-padding-top-xxl">
    <section class="u-layout-medium">
        <div class="u-layout-centerLeft">
            <h2 class="u-text-r-l"><span class="u-text-r-l Icon Icon-file"></span> {{ data.modulo.label }}</h2>
        </div>
        <div class="Grid Grid--withGutter u-padding-r-top u-text-r-xxl">
            {{if data.items.size > 0 }}
            {{for item in data.items  limit:9}}
            <div class="Grid-cell usizefull u-md-size1of3 u-lg-size1of3 u-text-r-m u-margin-r-bottom u-layout-matchHeight">
                <section class="u-nbfc u-borderShadow-xxs u-borderRadius-m u-color-grey-30 u-background-white">
                    {{if item.img != false }}
                    <figure class="u-background-grey-60 u-padding-all-s">

                        <a href="{{item.itemurl}}" {{if item.img.imgname != false}} data-title="{{item.img.imgname | string.replace "_" " "}}"{{end }} data-toggle="lightbox" data-gallery="#GalleryTable" class="u-borderFocus u-block u-padding-all-xxs">
                            <img src="{{item.img.imgsrc}}" class="u-sizeFull" alt="{{item.img.imgalt}}" />
                        </a>

                        <figcaption class="u-padding-r-top">
                            <span class="Icon Icon-camera u-color-white u-floatRight u-text-r-l" aria-hidden="true"></span>
                            {{if item.img.imgname != false}}
								<p class="u-color-teal-50 u-text-r-xxs u-textWeight-700 u-padding-bottom-xs">{{item.img.imgname}}</p>
							{{end }}
                            <p class="u-color-white u-text-r-xxs">{{item.date}}</p>
                        </figcaption>

                    </figure>
                    {{end }}
                    {{if item.titolo != false}}
                    <div class="u-text-r-l u-padding-r-all u-layout-prose">
                        <h3 class="u-text-h4 u-margin-r-bottom">
                            <a class="u-textClean u-color-black u-textWeight-400 u-text-r-m" href="{{item.itemurl}}">
                                {{item.titolo}}
                            </a>
                        </h3>
                    </div>
                    {{end}}
                </section>

            </div>
            {{end}}
            {{end}}
        </div>
        {{if data.modulo.itemshowmore != false }}
        <p class="u-textCenter u-text-md-right u-text-lg-right u-padding-r-top">
            <a href="{{data.modulo.url}}" class="u-color-50 u-textClean u-text-h4">
                {{data.modulo.showmorelabel}} <span class="Icon Icon-chevron-right" />
            </a>
        </p>
        {{end}}
    </section>
	<script>
            $(document).on('click', '[data-toggle="lightbox"]', function (event) {
                event.preventDefault();
                $(this).ekkoLightbox({
                    loadingMessage: '<div class="ekko-lightbox-loader">Caricamento ...</div>',
                    leftArrow: '<span class="Icon Icon-chevron-left"></span>',
                    rightArrow: '<span class="Icon Icon-chevron-right"></span>',
                });
            });
     </script>
</div>