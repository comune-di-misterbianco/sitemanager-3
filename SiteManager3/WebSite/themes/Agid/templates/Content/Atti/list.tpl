﻿<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="search-fields">
                <button class="btn btn-primary collapsed" data-toggle="collapse" type="button" data-target="#BandiSearchForm" aria-expanded="true">
                    <span class="glyphicon glyphicon-search"></span>
                    {{labels.NuovaRicerca}}
                </button>
                <div class="searchForm collapse" id="BandiSearchForm">
                    <div class="col-md-12">

                        <div class="page-header title-vfm">
                            <h4>{{labels.ricercalegendalbo}}</h4>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group validatedfield textbox-vf ">
                                <label for="Numero" class="control-label">Numero</label>
                                <input name="Numero" id="Numero" class="form-control " type="text">
                            </div>
                            <div class="form-group validatedfield textbox-vf ">
                                <label for="Oggetto" class="control-label">Oggetto</label>
                                <textarea name="Oggetto" rows="2" cols="80" id="Oggetto" class="form-control "></textarea>
                            </div>
                            <div class="form-group validatedfield dropdown-vf ">
                                <label for="IdUfficio" class="control-label">{{labels.soggettoemittente}}:  </label>
                                {{if form.soggemittentioptions | object.size > 0}}
                                <select name="IdUfficio" id="IdUfficio" class="form-control ">
                                    {{ for option in form.soggemittentioptions }}
                                    <option value="{{option.key}}">{{option.value}}</option>
                                    {{end}}
                                </select>
                                {{end}}
                            </div>
                            <div class="form-group validatedfield textbox-vf ">
                                <label for="Data" class="control-label">Data atto</label>
                                <div id="DC_Data" class="input-group date">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    <input name="Data" id="Data" class="form-control" type="text">
                                </div>
                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        $("#DC_Data").datetimepicker({
                                            locale: 'it',
                                            format: 'DD/MM/YYYY',
                                            inline: false,
                                            sideBySide: false,
                                            minDate: '01/01/2008',
                                            maxDate: '01/01/2028',
                                            calendarWeeks: false,
                                            showClear: false,
                                            showTodayButton: false,
                                            showClose: false,
                                            toolbarPlacement: 'default'
                                        })
                                    });
                                </script>
                            </div>
                        </div>                     

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group text-center vf back-vf">
                                    <input name="reset" value="{{labels.azzeraricerca}}" id="reset" class="btn btn-primary" type="submit">
                                </div>
                            </div><div class="col-md-6">
                                <div class="form-group text-center vf submit-vf">
                                    <input name="SearchForm_submit" value="{{labels.cercabutton}}" id="SearchForm_submit" class="btn btn-primary" type="submit">
                                </div>
                            </div>
                        </div>

                    </div>
              </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            {{if documents.size > 0}}
            <table class="Table Table--withBorder js-TableResponsive tablesaw tablesaw-stack" data-tablesaw-mode="stack">
                <caption class="u-hiddenVisually">{{labels.archiviostorico}}</caption>
                <thead>
                    <tr>
                        <th scope="col">{{labels.dataatto}}</th>
                        <th scope="col">{{labels.numero}}</th>
                        <th scope="col">{{labels.oggetto}}</th>
                        <th scope="col">{{labels.soggettoemittente}}</th>
                        <th scope="col">
                            {{if options.showallegaticolumn }}
                            {{labels.allegatocolumn}}
                            {{ end }}

                            {{if options.showestrattocolumn }}
                            {{labels.estrattocolumn}}
                            {{ end }}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {{ for atto in documents }}
                    <tr>
                        <td>{{atto.data | date.to_string '%d/%m/%Y'}}</td>
                        <td>{{atto.numero}}</td>
                        <td>{{atto.oggetto}}</td>
                        <td>{{atto.soggettoemittente}}</td>
                        <td>
                            {{if options.showallegaticolumn }}
                                {{ if atto.allegati.size > 0 }}
                                    {{for allegato in atto.allegati}}
                                     <i class="fa fa-file-{{allegato.ext}}"></i>
									 	{{if options.useencrypturl == true}}
										<a href="?GKID={{allegato.url}}">{{allegato.label}}</a>&nbsp;
										{{else}}
										 <a href="{{allegato.url}}">{{allegato.label}}</a>&nbsp;
										 {{end}}
                                    {{ end }}
                                {{ end }}
                            {{ end }}

                            {{if options.showestrattocolumn }}
                                {{ if atto.estratti.size > 0 }}
                                    {{for estratto in atto.estratti}}
										<i class="fa fa-file-{{estratto.ext}}"></i>										
										{{if options.useencrypturl == true}}
										 <a href="?GKID={{estratto.url}}">{{estratto.label}}</a>&nbsp;
										 {{else}}
										 <a href="{{estratto.url}}">{{estratto.label}}</a>&nbsp;
										 {{end}}
                                    {{ end }}
                                {{ end }}
                            {{ end }}
                        </td>
                    </tr>
                    {{ end }}
                </tbody>
            </table>

            <nav aria-label="Page navigation">

                <ul class="pagination">

                    {{ if pager.current > 1}}
                    <li>
                        <a href="{{pager.pageandquerystring | string.replace "{0}" 1 }}">
                            <span class="fa fa-angle-double-left"></span>
                            <span aria-hidden="true" class="u-hidden">Prima pagina</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{pager.pageandquerystring | string.replace "{0}" pager.current-1 }}" aria-label="Previous">
                            <span class="Icon-chevron-left" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">Pagina precedente</span>
                        </a>
                    </li>
                    {{end }}				

                    {{ for i in pager.lowrange..pager.toprange}}
                    <li {{ if  i == pager.current }} class="active" {{end}}>                        
						<a href="{{pager.pageandquerystring | string.replace "{0}" i }}">{{i}}</a>
                    </li>
                    {{end }}


                    {{ if pager.current < pager.pages}}
                    <li>
                        <a href="{{pager.pageandquerystring | string.replace "{0}" pager.current+1 }}" aria-label="Pagina successiva">
                            <span class="fa fa-angle-right" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">Pagina successiva</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{pager.pageandquerystring | string.replace "{0}" pager.pages }}" aria-label="Ultima pagina">
                            <span class="fa fa-angle-double-right" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">Ultima pagina</span>
                        </a>
                    </li>
                    {{end }}
                </ul>
            </nav>
			
            {{else}}
            <p>{{labels.norecord}}</p>
            {{end}}



			{{if sezione.archivio != false }}
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header"><h4>Archivio storico</h4></div>
                    <div class="sezione.archivio">
                        <div class="col-md-4">
                            
                            <ul class="list-inline">      
								{{for item in sezione.archivio.anni }}
                                <li class="u-padding-all-xs"><a class="btn btn-default {{if sezione.archivio.selectedyear == item  }} selected {{end}}" href="default.aspx?anno={{item}}">{{item}}</a></li>                              
								{{end}}
                            </ul>
                            
                        </div>
                        <div class="col-md-4">
                           
                            <ul class="list-inline">
                                {{for item in sezione.archivio.mesi }}
                                <li class="u-padding-all-xs"><a class="btn btn-default {{if sezione.archivio.selectedmonth == item.month  }} selected {{end}}" href="default.aspx?anno={{sezione.archivio.selectedyear}}&mese={{item.month}}">{{item.monthname}}</a></li>
                                {{end}}
                            </ul>
                        
                        </div>
                        <div class="col-md-4">
                        
                            <ul class="list-inline">
                                {{for item in sezione.archivio.giorni }}
                                <li class="u-padding-all-xs"><a class="btn btn-default" href="default.aspx?anno={{archiviostorico.selectedyear}}&mese={{item.month}}&giorno={{item.day}}">{{item.day}}</a></li>
                                {{end}}
                            </ul>
                          
                        </div>
                    </div>
                </div>
            </div>
			 {{end}}
        </div>
    </div>
</div>