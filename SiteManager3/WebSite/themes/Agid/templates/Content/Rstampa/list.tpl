﻿<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            {{if documents.size > 0}}

            {{ for doc in documents }}

            <article class="item rstampa">
                <p class="inner-date">
                    <i class="glyphicon glyphicon-calendar data_news" aria-hidden="true"></i>
                    <time datetime="{{ doc.data }}">{{ doc.data }}</time>
                </p>
                <a href="?rs={{doc.id}}" title="{{ doc.titolo | string.truncate 50}}">
                    <h4>{{ doc.titolo }}</h4>
                </a>
                <div class="inner-news-body">
                    <p>{{labels.testatagiornalistica}}: {{ doc.testata}}</p>
                    <p>{{ doc.descrizione }}</p>
                </div>
                
            </article>
            {{ end }}

            <nav aria-label="Page navigation">

                <ul class="pagination">

                    {{ if pager.current > 1}}
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current-1}}" aria-label="{{commonlabels.prevpage}}">
                            <span class="Icon-chevron-left" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.prevpage}}</span>
                        </a>
                    </li>
                    {{end }}

                    {{ for i in 1..pager.pages }}

                    {{
                            cssclass = ""
                            if  i == pager.current
                                cssclass = "active"
                            end
                    }}

                    <li class="{{cssclass}}">
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{i}}">
                            {{i}}
                        </a>
                    </li>
                    {{end }}

                    {{ if pager.current < pager.pages}}
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current-1}}" aria-label="{{commonlabels.nextpage}}">
                            <span class="Icon-chevron-right" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.nextpage}}</span>
                        </a>
                    </li>
                    {{end }}
                </ul>
            </nav>
           
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header"><h4>{{labels.archiviostorico}}</h4></div>
                    <div class="archivioStorico">
                        <div class="col-md-4">
                            {{if sezione.archivio != false }}                           
                            <ul class="list-inline">
                                {{for item in sezione.archivio.anni }}
                                <li><a class="btn btn-default {{if sezione.archivio.selectedyear == item  }} selected {{end}}" href="default.aspx?anno={{item}}">{{item}}</a></li>
                                {{end}}
                            </ul>
                            {{end}}
                        </div>
                        <div class="col-md-4">
                            {{if sezione.archivio != false }}
                            <ul class="list-inline">
                                {{for item in sezione.archivio.mesi }}
                                <li><a class="btn btn-default {{if sezione.archivio.selectedmonth == item.month  }} selected {{end}}" href="default.aspx?anno={{sezione.archivio.selectedyear}}&mese={{item.month}}">{{item.monthname}}</a></li>
                                {{end}}
                            </ul>
                            {{end}}
                        </div>
                        <div class="col-md-4">
                            {{if sezione.archivio != false }}
                            <ul class="list-inline">
                                {{for item in sezione.archivio.giorni }}
                                <li><a class="btn btn-default" href="default.aspx?anno={{sezione.archivio.selectedyear}}&mese={{item.month}}&giorno={{item.day}}">{{item.day}}</a></li>
                                {{end}}
                            </ul>
                            {{end}}
                        </div>
                    </div>
                </div>
            </div>
            
            {{else}}
            <p>{{labels.norecord}}</p>
            {{end}}
        </div>
    </div>
 </div>