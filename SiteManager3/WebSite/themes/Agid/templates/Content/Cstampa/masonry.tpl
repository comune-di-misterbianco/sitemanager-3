﻿<div class="container">
   <!-- page int --> 
   <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>
            <div class="">                
                {{if sezione.currentcategory != "-1"}}
                <p>
                    <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;
                   {{labels.selectedcategory}} <strong>{{sezione.currentcategory}}</strong> 
                    <a href="default.aspx"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;{{labels.removefilter}}</a>
                </p>
                {{end}}
            </div>

        </div>
    </div>

	<!-- end page int -->

	<div class="row">
		<div class="col-md-12">
			<div class=" searchFieldset">
			<p class="text-right">
				<button class="btn btn-primary " data-toggle="collapse" type="button" data-target="#comunicatiForm" >
					 Fai una ricerca
				</button>
			</p>
				<div class="searchForm collapse" id="comunicatiForm" style="">
					<div id="SearchForm" class="Axf" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'SearchForm_submit')">
						<div class="col-md-12">
							<div class="page-header title-vfm">
								<h4>Cerca</h4>
							</div>
							<div class="form-group validatedfield textbox-vf ">								
								<input name="Nome" type="text" id="Nome" class="form-control">
								<label for="Nome">Titolo</label>
							</div>
						<div id="DC_DataDal" class="it-datepicker-wrapper">							
							<div class="form-group validatedfield textbox-vf">							
								<input name="DataDal" type="text" id="DataDal" class="form-control it-date-datepicker">
								<label for="DataDal" class="control-label">Data Successiva al </label>
							</div>
						</div>
						<div id="DC_DataAl" class="it-datepicker-wrapper">	
							<div class="form-group validatedfield textbox-vf ">
								<input name="DataAl" type="text" id="DataAl" class="form-control it-date-datepicker">
								<label for="DataAl">Data Antecedente al </label>							
							</div>
						</div>
						<script type="text/javascript">				
								   $(document).ready(function() {									 
									  $('.it-date-datepicker').datepicker({
										  inputFormat: ["dd/MM/yyyy"],
										  outputFormat: 'dd/MM/yyyy',
										});
								   });
						</script>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group text-center vf back-vf">
									<input type="submit" name="SearchForm_back" value="Reset" id="SearchForm_back" class="btn btn-default">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group text-center vf submit-vf">
									<input type="submit" name="SearchForm_submit" value="Cerca" id="SearchForm_submit" class="btn btn-info">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	{{if documents.size > 0 }}

		<!-- corpo -->
		<div class="row">

			<div class="u-layout-centerContent u-cf">
   
				<section class="js-Masonry-container u-layout-medium" data-columns>

					{{for doc in documents }}
					<div class="Masonry-item js-Masonry-item">
						<div class="u-nbfc u-borderShadow-xxs u-borderRadius-m u-color-grey-30 u-background-white">
							{{if doc.coverimage != false }}
							  {{if doc.coverimage.src | string.size > 0 }}
								<img src="{{doc.coverimage.src}}" class="u-sizeFull" alt="{{doc.coverimage.description}}" />
							  {{end }}
							{{end }}
							<div class="u-text-r-l u-padding-r-all u-layout-prose">
								<p class="u-text-h6 u-margin-bottom-l"><a class="u-color-50 u-textClean" href="default.aspx?cat={{doc.idcategoria}}">{{doc.categoria}}</a></p>
								<h3 class="u-text-h4 u-margin-r-bottom"><a class="u-text-r-m u-color-black u-textWeight-400 u-textClean" href="?cs={{doc.id}}">{{doc.titolo}}</a></h3>
								<p class="u-text-p u-textSecondary">{{doc.descrizione}}</p>
							</div>
						</div>
					</div>
					{{end }}
				


				</section>
    
			</div>
	
		</div>

		<!-- fine corpo -->

		<!-- paginazion -->
		<nav aria-label="Page navigation">
			<ul class="pagination">
				<li class=""><a class="disabled" href="#">Pagina {{ pager.current }} di {{ pager.pages }}</a></li>
				{{ if pager.current > 1}}
				<li>
					<a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}=1" aria-label="{{commonlabels.firstpage}}">
						<span class="fa fa-angle-double-left"></span>
						<span aria-hidden="true" class="u-hidden">{{commonlabels.firstpage}}</span>
					</a>
				</li>
				<li>
					<a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current-1}}" aria-label="{{commonlabels.prevpage}}">
						<span class="fa fa-angle-left" role="presentation"></span>
						<span aria-hidden="true" class="u-hidden">{{commonlabels.prevpage}}</span>
					</a>
				</li>
				{{end }}

				{{ for i in pager.lowrange..pager.toprange}}
				<li {{ if  i == pager.current }} class="active" {{end}}>
					<a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{i}}">{{i}}</a>
				</li>
				{{end }}

				{{ if pager.current < pager.pages}}
				<li>
					<a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current+1}}" aria-label="{{commonlabels.nextpage}}">
						<span class="fa fa-angle-right" role="presentation"></span>
						<span aria-hidden="true" class="u-hidden">{{commonlabels.nextpage}}</span>
					</a>
				</li>
				<li>
					<a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.pages}}" aria-label="{{commonlabels.lastpage}}">
						<span class="fa fa-angle-double-right" role="presentation"></span>
						<span aria-hidden="true" class="u-hidden">{{commonlabels.lastpage}}</span>
					</a>
				</li>
				{{end }}
			</ul>
		</nav>
		<!-- fine paginazion -->

		<!-- archivio storico -->

		 <div class="row">
					<div class="col-md-12">
						<div class="page-header"><h4>{{labels.archiviostorico}}</h4></div>
						<div class="archivioStorico">
							<div class="col-md-4">
								{{if sezione.archivio != false }}
								<ul class="list-inline">
									{{for item in sezione.archivio.anni }}
									<li><a class="btn btn-default {{if sezione.archivio.selectedyear == item  }} selected {{end}}" href="default.aspx?anno={{item}}">{{item}}</a></li>
									{{end}}
								</ul>
								{{end}}
							</div>
							<div class="col-md-4">
								{{if sezione.archivio != false }}
								<ul class="list-inline">
									{{for item in sezione.archivio.mesi }}
									<li><a class="btn btn-default {{if sezione.archivio.selectedmonth == item.month  }} selected {{end}}" href="default.aspx?anno={{sezione.archivio.selectedyear}}&mese={{item.month}}">{{item.monthname}}</a></li>
									{{end}}
								</ul>
								{{end}}
							</div>
							<div class="col-md-4">
								{{if sezione.archivio != false }}
								<ul class="list-inline">
									{{for item in sezione.archivio.giorni }}
									<li><a class="btn btn-default" href="default.aspx?anno={{sezione.archivio.selectedyear}}&mese={{item.month}}&giorno={{item.day}}">{{item.day}}</a></li>
									{{end}}
								</ul>
								{{end}}
							</div>
						</div>
					</div>
		 </div>
		 <!-- fine archivio storico -->

	{{else}}
		
		<p>{{labels.norecord}}</p>
      
	{{end}}

</div>