﻿<div class="container">
	<div class="u-layout-wide u-layoutCenter u-layout-withGutter u-padding-r-top u-padding-bottom-xxl">
		
		<h1 class="u-text-h2">{{document.titolo}}</h1>

		<div class="Grid Grid--withGutter">

				<div class="Grid-cell u-md-size12of12 u-lg-size12of12">
                 
				<article class="pagina">		
                    <div class="Grid Grid--withGutter">
						<div class="date Grid-cell u-size1of2">
							<p>         
							<i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;
							{{commonlabels.publishedon}} : {{document.datapub}} 
							{{ if document.datarev != "false"}} - {{commonlabels.lastrevision}}: {{document.datarev}}{{end}}
							</p>
						</div>

						<div class="share Grid-cell u-size1of2">
							{{sharecontent}}
						</div>

						<div class="print Grid-cell u-size1of1 text-left">
							<p>
							<i class="fa fa-print" aria-hidden="true"></i>&nbsp;
							<a href="javascript:this.print();" title="{{commonlabels.printtitle}}">{{commonlabels.print}}</a>
							</p>
						</div>

						<div class="categoria">{{labels.categoria}}: {{ document.categoria}}</div>
					</div>
					
					<p class="description">{{document.descrizione}}</p>
			        {{if document.img.showindetail == true }}
					<div class="imgevidenza">
						<img src="{{document.img.src}}" class="img-fluid" alt="{{document.img.descrizione}}" />
					</div>
					{{end}}
					<div class="u-text-r-s u-padding-r-top">
						<div class="news-text u-margin-bottom-l">
							{{document.testo}}
						</div>

						{{if document.tags | object.size > 0}}
						<div class="doc-tags">
							<i class="fa fa-tags" aria-hidden="true"></i>
							<span>{{commonlabels.tagstitle}}:</span>
							{{ for tag in document.tags }}								
								<a class="doc-tag" rel="tag" href="/tags/argomenti.aspx?tag={{tag.key}}">{{tag.label}}</a>
							{{end}}
						</div>
						{{end}}
					</div>
				</article>
				
				</div>
		
		</div>
   </div>
</div>