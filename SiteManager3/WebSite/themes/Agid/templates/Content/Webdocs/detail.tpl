﻿<div class="container">
    <div class="u-layout-wide u-layoutCenter u-layout-withGutter u-padding-r-top u-padding-bottom-xxl">
        <h1 class="u-text-h2">{{document.titolo}}</h1>            
        <div class="Grid Grid--withGutter">
            
                <div class="Grid-cell u-md-size12of12 u-lg-size12of12">
                 
                    <article class="pagina">
                        
						<div class="Grid Grid--withGutter">
							<div class="date Grid-cell u-size1of2">
								<p><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp; {{commonlabels.lastrevision}}: {{sezione.data}}</p>
							</div>

							<div class="share Grid-cell u-size1of2">
								{{sharecontent}}
							</div>

							<div class="print Grid-cell u-size1of1 text-left">
								<p>
									<i class="fa fa-print" aria-hidden="true"></i>&nbsp;
									<a href="javascript:this.print();" title="{{commonlabels.printtitle}}">{{commonlabels.print}}</a>
								</p>
							</div>
						</div>
						<p class="description">{{document.descrizione}}</p>
						<div class="page-content">
							{{document.testo}}
						</div>

						{{if document.tags | object.size > 0}}
						<div class="doc-tags">
							<i class="fa fa-tags" aria-hidden="true"></i>
							<span>Tags:</span>
							{{ for tag in document.tags }}
								<a class="doc-tag" rel="tag" href="/tags/argomenti.aspx?tag={{tag.key}}">{{tag.label}}</a>
							{{end}}
						</div>
						{{end}}
                    </article>
                </div>            
			<a href="#" title="torna all'inizio del contenuto" class="u-hiddenVisually">torna all'inizio del contenuto</a>
        </div>
        </div>
    </div>