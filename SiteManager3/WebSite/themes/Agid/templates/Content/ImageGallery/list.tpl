﻿<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{sezione.titolo}}</h1>
                <p class="description">{{sezione.descrizione}}</p>
            </div>
        </div>
    </div>
    
	

	
	{{if sezione.childfolder | object.size > 0}}
		<div class="Grid Grid--withGutter">
            {{for folder in sezione.childfolder}}
            <div class="Grid-cell u-sm-size1of2 u-md-size1of2 u-lg-size1of3 u-padding-r-all">
                <div class="u-border-top-l u-color-70 u-text-r-l u-padding-r-bottom">                  				 
				
					{{if folder.image | string.size > 0 }}
                        <img src="{{folder.image.url}}?preset=moduloimgwide" class="u-sizeFull" alt="{{folder.image.Desc}}" />
                     {{end }}

					<h2 class="u-text-h3 u-padding-r-top">
                        <a href="{{folder.url}}">{{folder.titolo}}</a>
                    </h2>

					<p class="Prose u-padding-r-top u-padding-r-bottom">
                        {{folder.descrizione}}
                    </p>					                    				
                    
                </div>
            </div>
            {{  end }}	
		</div>
    {{end}}
	
	

	{{if documents.size > 0}}
    <div class="Grid Grid--withGutter u-padding-r-top u-text-r-xxl" id="Gallery-Table">
       

        {{ for doc in documents }}
        <div class="Grid-cell usizefull u-md-size1of3 u-lg-size1of3 u-text-r-m u-margin-r-bottom u-layout-matchHeight">
            <section class="u-nbfc u-borderShadow-xxs u-borderRadius-m u-color-grey-30 u-background-white">
                <figure class="u-background-grey-60 u-padding-all-s figure figure-imagegallery thumbnail">
                    <a href="{{sezione.url}}{{doc.filename}}" class="u-borderFocus u-block u-padding-all-xxs img-group" data-title="{{doc.titolo | string.replace "_" " "}}" data-toggle="lightbox" data-gallery="#Gallery-Table">
                        <img src="{{sezione.url}}{{doc.content}}" class="u-sizeFull figure-img img-fluid">
                    </a>
                    <figcaption class="u-padding-r-top">
                        <span class="Icon Icon-camera u-color-white u-floatRight u-text-r-l" aria-hidden="true"></span>
                        <p class="u-color-teal-50 u-text-r-xxs u-textWeight-700 u-padding-bottom-xs"> {{doc.titolo}}</p>
                        <p class="u-color-white u-text-r-xxs">{{ doc.data }}</p>
                    </figcaption>
                </figure>
                <div class="u-text-r-l u-padding-r-all u-layout-prose">
                    <h3 class="u-text-h4 u-margin-r-bottom">
                        <a class="u-textClean u-color-black u-textWeight-400 u-text-r-m" href="">
                            {{doc.descrizione}}
                        </a>
                    </h3>
                </div>
            </section>
        </div>
        {{end }}
        <script>
            $(document).on('click', '[data-toggle="lightbox"]', function (event) {
                event.preventDefault();
                $(this).ekkoLightbox({
                    loadingMessage: '<div class="ekko-lightbox-loader">Caricamento ...</div>',
                    leftArrow: '<span class="Icon Icon-chevron-left"></span>',
                    rightArrow: '<span class="Icon Icon-chevron-right"></span>',
                });
            });
        </script>
        </div>
        <div class="u-layout-centerContent u-cf">
            <nav aria-label="Page navigation">

                <ul class="pagination">
                  
                    {{ if pager.current > 1}}
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current-1}}" aria-label="{{commonlabels.prevpage}}">
                            <span class="Icon-chevron-left" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.prevpage}}</span>
                        </a>
                    </li>
                    {{end }}

                    {{ for i in 1..pager.pages }}

                    {{
                            cssclass = ""
                            if  i == pager.current
                                cssclass = "active"
                            end
                    }}

                    <li class="{{cssclass}}">
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{i}}">
                            {{i}}
                        </a>
                    </li>
                    {{end }}

                    {{ if pager.current < pager.pages}}
                    <li>
                        <a href="{{sezione.url}}{{sezione.pagename}}?{{pager.pagekey}}={{pager.current-1}}" aria-label="{{commonlabels.nextpage}}">
                            <span class="Icon-chevron-right" role="presentation"></span>
                            <span aria-hidden="true" class="u-hidden">{{commonlabels.nextpage}}</span>
                        </a>
                    </li>
                    {{end }}
                </ul>
            </nav>
        </div>        
        
        {{end }}
    
	   {{if (sezione.childfolder | object.size == 0 && documents.size == 0 )}}
		<p class="u-text-p u-textNormal u-color-grey-60">{{commonlabels.norecord}}</p>
		{{end }}
</div>