﻿/* eslint-disable indent */
document.addEventListener('DOMContentLoaded', function () {
SPID.init({
    lang: 'it',
    selector: '#spid-button',  // opzionale
    method: 'POST',               // opzionale
    url: '/Login/{{idp}}',
    fieldName: 'idp',
    extraFields: {                // opzionale
        foo: 'bar',
        baz: 'baz'
    },
    mapping: {                    // opzionale
        'https://loginspid.aruba.it': 4,
        'https://posteid.poste.it': 5,
        'https://idp.namirialtsp.com/idp': 7,
    },
    supported: [
        'https://identity.sieltecloud.it',
        'https://loginspid.aruba.it',
        'https://identity.infocert.it',
        'https://login.id.tim.it/affwebservices/public/saml2sso',
        'https://idp.namirialtsp.com/idp',
        'https://spid.register.it',
        'https://id.lepida.it/idp/shibboleth'
    ],
    protocol: "SAML",
    size: "medium",
    colorScheme: "positive",
    cornerStyle: "rounded"
});

    SPID.init({
selector: '#my-spid-button2',  // opzionale
        url: '/Login/{{idp}}',
        supported: [],
        extraProviders: [
            {
"entityID": "https://testidp.net-serv.it/",
                "entityName": "Test IdP"
            }
        ]
    });
});
/* eslint-enable indent */