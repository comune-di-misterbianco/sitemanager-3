﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page 
{
    public class LocalPagesHandler : NetCms.Pages.PagesHandler
    {
        public override NetCms.Pages.PagesSwitcher[] Switchers { get { return null; } }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Inserita una forzatura al linguaggio di default al fine di evitare un bug che al cambiamento della lingua del frontend la faceva cambiare anche nel backend causando la traduzione dei nomi di cartelle e documenti.
        LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetDefaultLanguage();
        NetCms.Pages.PagesHandler handler = new LocalPagesHandler();
        NetCms.Pages.Page localPage = handler.Fabricate(this,this.ViewState);
        localPage.BuildPage();
    }
}
