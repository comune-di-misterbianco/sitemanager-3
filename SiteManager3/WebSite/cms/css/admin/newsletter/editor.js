﻿$(document).ready(function () {
    $(".bodyContainer .region").sortable({
        connectWith: ".region",
        opacity: .35,
        placeholder: "ui-state-highlight",
        handle: ".drag",
        start: function (e, ui) {
            ui.placeholder.height(ui.item.height());
        }
    });

    $(".bodyContainer").delegate(".remove", "click", function (e) {
        e.preventDefault();
        $(this).parent().remove();
    });

    // save function
    $(".editorContainer").delegate("#save", "click", function (e) {
        e.preventDefault();
        //alert("Btn save clicked");
        save();

    });

    // preview function
    $(".editorContainer").delegate("#preview", "click", function (e) {
        e.preventDefault();
        preview();
    });

    $(".widgetContainer .region-element").draggable({
        connectToSortable: ".region",
        helper: "clone",
        handle: ".drag",
        drag: function (e, t) {
            t.helper.width(400)
        },
        stop: function (e, t) {
            $(".bodyContainer .region").sortable({
                opacity: .35,
                connectWith: ".region"
            })
        }
    });

});


function preview() {

    var views = [];
    var t = $(".bodyContainer");

    var regions = t.find(".region");

    $.each(regions, function (index, value) {

        var box = [];
        var view = $(this).find('.view');

        $.each(view, function (i, val) {
            var srcView = cleanView(val.innerHTML);
            box.push(srcView);
        });

        views.push({
            view: box,
            region: value.id
        });
    });

    console.log(views);

//    var oJson = JSON.stringify(views);
//    
//    console.log(oJson);   
}

function save() {
    var views = [];
    var t = $(".bodyContainer");

    var regions = t.find(".region");

    $.each(regions, function (index, value) {

        var box = [];
        var view = $(this).find('.view');

        $.each(view, function (i, val) {
            var srcView = cleanView(val.innerHTML);
            box.push(srcView);
        });

        views.push({
            view: box,
            region: value.id
        });
    });
    var idNumero = 12;
    
    //var oJson = JSON.stringify(views);
    //var dati = "idNumero:'" + idNumero + "', body:'" + oJson + "'";

    var model = JSON.stringify({
        'idNumero': idNumero,
        'body': views
    });
    
    
    var baseAddress = "http://localhost:14847/WebSite/api/";
        
     //    invoke webapi to update regionelement of currentNewsletter
    $.ajax({
        url: baseAddress + 'Newsletter/Numero/SaveNumero/',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: model,
        success: function (data) {
            alert(data);
        }
    });
    //    console.log(oJson);   
}

function cleanView(src) 
{
    formatSrc = $.htmlClean(src, {
        format: true,
        allowedAttributes: [            
            ["style"]            
        ],
        removeAttrs: [
            ["contenteditable"]
        ]
    });
    return formatSrc;
}