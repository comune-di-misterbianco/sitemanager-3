'use strict';

(function($){

  $(function() {

    $('#chart-container').orgchart({
      'data' : $('#ul-data'),
	  'direction': 'l2r'
    });

  });

})(jQuery);