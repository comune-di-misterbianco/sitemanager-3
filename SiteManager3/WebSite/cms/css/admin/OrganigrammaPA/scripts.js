//'use strict';
//alert($.fn.jquery);
//jQuery.noConflict(true);
//(function($){

//    $(function() {

       

//    });

//})(jQuery);
//alert($.fn.jquery);
//$('#chart-container').orgchart({
//    'data': $('#ul-data')
//});
$.noConflict();
jQuery(document).ready(function ($) {
    $('#chart-container').orgchart({
        'data': $('#ul-data'),        
        'direction': 'l2r',
        'nodeContent': 'title',
        'nodeID': 'id',

        //'verticalDepth': 3,
        'pan': true,
        'zoom': true,
        //'exportButton': true,
        //'exportFilename': 'MyOrgChart'
        'createNode': function ($node,data) {
            var secondMenuIcon = $('<i>', {
                'class': 'fa fa-gear second-menu-icon',
                click: function () {
                    $(this).siblings('.second-menu').toggle();
                }
            });
            var people = "";
            if ($("#dialogImage_" + data.id).prop('outerHTML') !== undefined) {
                people = $("#dialogImage_" + data.id).prop('outerHTML');
                //$node.find('.content').append(people);
            }

            var strum = ""
            if ($("#link_strum_" + data.id).prop('outerHTML') !== undefined) {
                strum = $("#link_strum_" + data.id).prop('outerHTML');
                //$node.find('.content').append(strum);
            }

            var div = "";
            if ($("#div_link_" + data.id).prop('outerHTML') !== undefined) {
                div = $("#div_link_" + data.id).prop('outerHTML');
                //$node.find('.content').append(div);
            }

            var secondMenu = '<div class="second-menu">'+people +  strum + div+'</div>';//<img class="avatar" src="cms/css/admin/OrganigrammaPA/images/user.png">
            //$node.append(secondMenuIcon);//.append(secondMenu);
            $node.append(secondMenuIcon).append(secondMenu);//find('.content').
        }
    });
    //serve a nascondere l'ul e gli li che contengono la struttutra dell'albero
    $('#ul-data').children().hide();
    //disabilita l'immagine a griglia che compare nella costruzione del componente
    $(".orgchart").css('background-image', 'none');      
    //serve per non far visualizzare le freccine che permettono di espndere/ridurre la visualizzazione dell'albero
    $('.orgchart').addClass('noncollapsable');
    //serve a mettere nella proprieta title di ogni oggetto della classe .title il testo che contiene il nodo
    $('.title').each(function () {
        $(this).prop('title', $(this).text());
    });
    $('li .nodo_found').each(function (index, value) {
        $('.orgchart').find('.node[id="' + value.id + '"]').css("background-color", "blue");
    });
    //var id_to_found_in_table_component = $('li .nodo_found').attr('id');
    //$('.orgchart').find('.node[id="' + id_to_found_in_table_component + '"]').css("background-color", "blue");


});
