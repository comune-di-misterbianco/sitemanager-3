﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Riepilogo.aspx.cs" Inherits="Riepilogo" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title>Sitemanager Cms 3 - Setup</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="wrapper"> 
    <form id="form1" runat="server">
    <div id="header">
        <div class="logo"><span>Sitemanager Cms 3 - Setup</span></div>		
    </div>
    <div id="content">
         <h1><i class="glyphicon glyphicon-cog"></i> Setup CMS 3</h1>
        <div class="flowbar">
            <ul>
                <li class="item select">                   
                    <a href="default.aspx">                        
                        <span class="step-label">Scelta cartella cms</span> 
                    </a>
                </li>
                <li class="item select">                
                    <a href="Step2.aspx">                        
                        <span class="step-label">Parametri di configurazione</span> 
                    </a>    
                </li>
                <li class="item select">
                    <a href="#">                        
                        <span class="step-label">Riepilogo</span>
                    </a> 
                </li>
            </ul>
        </div>
        <div class="form-contend" >            
            <div ID="EsitoInstall" runat="server"></div>
            <div ID="divRiepilogo" runat="server" class="panel panel-default">
            <div class="info green"><i class="glyphicon glyphicon-info-sign"></i> Cms app folder: <%=(Setup.Model.SetupConfig.AbsoluteCMSParentFolderPath)%></div><br>
            <h3>Riepilogo dati immessi</h3>
                        
            <h4>Dati generali</h4>
            <ul>                        
                <li><span>Nome portale: </span> <%= Setup.Model.SetupConfig.PortalName%></li>
                <li><span>Email portale: </span> <%= Setup.Model.SetupConfig.PortalEmail%></li>
                <li><span>Percorso assoluto cartella log: </span><%= Setup.Model.SetupConfig.AbsoluteLogsFolderPath %></li>
                <li><span>Percorso relativo applicazione IIS: </span><%= Setup.Model.SetupConfig.PortalRootPath%></li>
            </ul>                            
            <h4>Dati database</h4>
            <ul>
                <li><span>Indirizzo database server e porta tcp: </span> <%= Setup.Model.DBConfig.DbHost %> &nbsp; <%= Setup.Model.DBConfig.DbPort %> </li>
                <li><span>Nome database: </span><%= Setup.Model.DBConfig.DbName %></li>
                <li><span>Nome utente: </span><%=Setup.Model.DBConfig.DbUser %></li>
                <li><span>Password: </span><%= Setup.Model.DBConfig.DbPw  %></li>
            </ul>
            <h4>Dati server di posta</h4>
            <ul>
                <li><span>Indirizzo smtp e porta tcp: </span><%= Setup.Model.SetupConfig.SmtpHost %> &nbsp; <%= Setup.Model.SetupConfig.SmtpPort %></li>
                <li><span>Nome utente: </span><%=Setup.Model.SetupConfig.SmtpUser %></li>
                <li><span>Password: </span> <%=Setup.Model.SetupConfig.SmtpPw %></li>
                <li><span>Usa autenticazione SSL: </span><%= Setup.Model.SetupConfig.SmtpSsl %></li>
            </ul>  
            <br>                                  
        </div>
        <p class="buttons">
                <asp:Button ID="Button1" CssClass="btn btn-primary btn-lg" runat="server" onclick="Button1_Click" Text="Installa" />    
            </p>          
    </div>
    <div id="footer">
        <div class="foter-content"><p>Copyright (c) 2014 Net.Service S.r.l.</p></div>
    </div>
    </form>
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>
