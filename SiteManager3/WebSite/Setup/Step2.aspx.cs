﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data;
using Setup.BusinessLogic;
using Setup.Model;

public partial class Step2 : System.Web.UI.Page
{
    //private SetupConfig _Cfg;
    //public SetupConfig Cfg
    //{
    //    get
    //    {
    //        if (_Cfg == null)
    //        {
    //            if (HttpContext.Current.Session["SetupCfg"] != null)
    //            {
    //                _Cfg = (SetupConfig)HttpContext.Current.Session["SetupCfg"];
    //            }
    //        }
    //        return _Cfg;
    //    }     
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) 
            FillForm();
    }

    //private void FillCfg() 
    //{
    //    Cfg.PortalName = PortalName.Text;
    //    Cfg.PortalEmail = PortalEmail.Text;
    //    Cfg.PortalRootPath = PortalRootPath.Text;
    //    Cfg.AbsoluteLogsFolderPath = AbsoluteLogsFolderPath.Text;

    //    Cfg.DBConfig.DbHost = DbHost.Text;
    //    Cfg.DBConfig.DbName = DbName.Text;
    //    Cfg.DBConfig.DbPort = DbPort.Text;
    //    Cfg.DBConfig.DbUser = DbUser.Text;
    //    Cfg.DBConfig.DbPw = DbPw.Text;

    //    Cfg.SmtpHost = SmtpHost.Text;
    //    Cfg.SmtpPort = SmtpPort.Text;
    //    Cfg.SmtpUser = SmtpUser.Text;
    //    Cfg.SmtpPw = SmtpPw.Text;
    //    Cfg.SmtpSsl = SmtpSsl.SelectedValue == "Si" ? "true" : "false";

    //    HttpContext.Current.Session["SetupCfg"] = Cfg;
    //}
    private void FillCfg()
    {
        SetupConfig.PortalName = PortalName.Text;
        SetupConfig.PortalEmail = PortalEmail.Text;
        SetupConfig.PortalRootPath = PortalRootPath.Text;
        SetupConfig.AbsoluteLogsFolderPath = AbsoluteLogsFolderPath.Text;

        DBConfig.DbHost = DbHost.Text;
        DBConfig.DbName = DbName.Text;
        DBConfig.DbPort = DbPort.Text;
        DBConfig.DbUser = DbUser.Text;
        DBConfig.DbPw = DbPw.Text;

        SetupConfig.SmtpHost = SmtpHost.Text;
        SetupConfig.SmtpPort = SmtpPort.Text;
        SetupConfig.SmtpUser = SmtpUser.Text;
        SetupConfig.SmtpPw = SmtpPw.Text;
        SetupConfig.SmtpSsl = SmtpSsl.SelectedValue == "Si" ? "true" : "false";


    }

    //private void FillForm() 
    //{
    //    if (Cfg != null)
    //    {
    //        if (!string.IsNullOrEmpty(Cfg.PortalName))
    //            PortalName.Text = Cfg.PortalName;

    //        if (!string.IsNullOrEmpty(Cfg.PortalEmail))
    //            PortalEmail.Text = Cfg.PortalEmail;

    //        if (!string.IsNullOrEmpty(Cfg.PortalRootPath))
    //            PortalRootPath.Text = Cfg.PortalRootPath;

    //        if (!string.IsNullOrEmpty(Cfg.AbsoluteLogsFolderPath))
    //            AbsoluteLogsFolderPath.Text = Cfg.AbsoluteLogsFolderPath;

    //        if (!string.IsNullOrEmpty(Cfg.DBConfig.DbHost))
    //            DbHost.Text = Cfg.DBConfig.DbHost;

    //        if (!string.IsNullOrEmpty(Cfg.DBConfig.DbName))
    //            DbName.Text = Cfg.DBConfig.DbName;

    //        if (!string.IsNullOrEmpty(Cfg.DBConfig.DbPort))
    //            DbPort.Text = Cfg.DBConfig.DbPort;

    //        if (!string.IsNullOrEmpty(Cfg.DBConfig.DbUser))
    //            DbUser.Text = Cfg.DBConfig.DbUser;

    //        if (!string.IsNullOrEmpty(Cfg.DBConfig.DbPw))
    //            DbPw.Text = Cfg.DBConfig.DbPw;

    //        if (!string.IsNullOrEmpty(Cfg.SmtpHost))
    //            SmtpHost.Text = Cfg.SmtpHost;

    //        if (!string.IsNullOrEmpty(Cfg.SmtpPort))
    //            SmtpPort.Text = Cfg.SmtpPort;

    //        if (!string.IsNullOrEmpty(Cfg.SmtpUser))
    //            SmtpUser.Text = Cfg.SmtpUser;

    //        if (!string.IsNullOrEmpty(Cfg.SmtpPw))
    //            SmtpPw.Text = Cfg.SmtpPw;

    //        if (!string.IsNullOrEmpty(Cfg.SmtpSsl))
    //        {
    //            if (Cfg.SmtpSsl == "true")
    //                SmtpSsl.SelectedValue = "Si";
    //            else
    //                SmtpSsl.SelectedValue = "No";
    //        }
    //    }
    //}

    private void FillForm()
    {

        if (!string.IsNullOrEmpty(SetupConfig.PortalName))
            PortalName.Text = SetupConfig.PortalName;

        if (!string.IsNullOrEmpty(SetupConfig.PortalEmail))
            PortalEmail.Text = SetupConfig.PortalEmail;

        if (!string.IsNullOrEmpty(SetupConfig.PortalRootPath))
            PortalRootPath.Text = SetupConfig.PortalRootPath;

        if (!string.IsNullOrEmpty(SetupConfig.AbsoluteLogsFolderPath))
            AbsoluteLogsFolderPath.Text = SetupConfig.AbsoluteLogsFolderPath;

        if (!string.IsNullOrEmpty(DBConfig.DbHost))
            DbHost.Text = DBConfig.DbHost;

        if (!string.IsNullOrEmpty(DBConfig.DbName))
            DbName.Text = DBConfig.DbName;

        if (!string.IsNullOrEmpty(DBConfig.DbPort))
            DbPort.Text = DBConfig.DbPort;

        if (!string.IsNullOrEmpty(DBConfig.DbUser))
            DbUser.Text = DBConfig.DbUser;

        if (!string.IsNullOrEmpty(DBConfig.DbPw))
            DbPw.Text = DBConfig.DbPw;

        if (!string.IsNullOrEmpty(SetupConfig.SmtpHost))
            SmtpHost.Text = SetupConfig.SmtpHost;

        if (!string.IsNullOrEmpty(SetupConfig.SmtpPort))
            SmtpPort.Text = SetupConfig.SmtpPort;

        if (!string.IsNullOrEmpty(SetupConfig.SmtpUser))
            SmtpUser.Text = SetupConfig.SmtpUser;

        if (!string.IsNullOrEmpty(SetupConfig.SmtpPw))
            SmtpPw.Text = SetupConfig.SmtpPw;

        if (!string.IsNullOrEmpty(SetupConfig.SmtpSsl))
        {
            if (SetupConfig.SmtpSsl == "true")
                SmtpSsl.SelectedValue = "Si";
            else
                SmtpSsl.SelectedValue = "No";
        }
    }

    protected void GoToStep3_Click(object sender, EventArgs e)
    {        
        FillCfg();        
        HttpContext.Current.Response.Redirect("Riepilogo.aspx");
    }

    //protected void checkDbConn_Click(object sender, EventArgs e)
    //{
    //    if (Cfg != null)
    //    {
    //        FillCfg();
    //        FillForm();
    //        //Cfg.DBConfig.DbHost = DbHost.Text;
    //        //Cfg.DBConfig.DbName = DbName.Text;
    //        //Cfg.DBConfig.DbPort = DbPort.Text;
    //        //Cfg.DBConfig.DbUser = DbUser.Text;
    //        //Cfg.DBConfig.DbPw = DbPw.Text;

    //        bool dbStatus = Cfg.CheckDbConn();

    //        if (dbStatus)
    //        {
    //            checkDbConn.CssClass = "btn btn-success";
    //            ConnStatus.InnerHtml = "<div class=\"alert alert-success\">Connessione al database eseguita con successo.</div>";
    //            GoToStep3.Enabled = true;
    //        }
    //        else
    //        {
    //            checkDbConn.CssClass = "btn btn-danger";
    //            ConnStatus.InnerHtml = "<div class=\"alert alert-danger\">Connessione al database fallita.</div> " + Cfg.DBConfig.GetStringConn();

    //        }
    //    }

    //}


    protected void checkDbConn_Click(object sender, EventArgs e)
    {
        FillCfg();
        FillForm();

        bool dbStatus = DbUtils.CheckDbConn();

        if (dbStatus)
        {
            checkDbConn.CssClass = "btn btn-success";
            ConnStatus.InnerHtml = "<div class=\"alert alert-success\">Connessione al database eseguita con successo.</div>";
            GoToStep3.Enabled = true;
        }
        else
        {
            checkDbConn.CssClass = "btn btn-danger";
            ConnStatus.InnerHtml = "<div class=\"alert alert-danger\">Connessione al database fallita.</div> ";// + DBConfig.GetStringConn();

        }
    }
}