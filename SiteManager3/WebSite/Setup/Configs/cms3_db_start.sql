/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `anagrafica`
--

DROP TABLE IF EXISTS `anagrafica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anagrafica` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anagrafica`
--

LOCK TABLES `anagrafica` WRITE;
/*!40000 ALTER TABLE `anagrafica` DISABLE KEYS */;
INSERT INTO `anagrafica` VALUES (1,'areatecnica@net-serv.it');
/*!40000 ALTER TABLE `anagrafica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anagrafica_attachments`
--

DROP TABLE IF EXISTS `anagrafica_attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anagrafica_attachments` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `File` mediumblob,
  `FileName` varchar(255) DEFAULT NULL,
  `ContentType` varchar(255) DEFAULT NULL,
  `AnagraficaID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `AnagraficaID` (`AnagraficaID`),
  CONSTRAINT `attachments_anagrafica` FOREIGN KEY (`AnagraficaID`) REFERENCES `anagrafica` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anagrafica_attachments`
--

LOCK TABLES `anagrafica_attachments` WRITE;
/*!40000 ALTER TABLE `anagrafica_attachments` DISABLE KEYS */;
/*!40000 ALTER TABLE `anagrafica_attachments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anagrafica_backoffice`
--

DROP TABLE IF EXISTS `anagrafica_backoffice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anagrafica_backoffice` (
  `id_anagrafica_Backoffice` int(11) NOT NULL,
  `Nome` varchar(255) NOT NULL,
  `Cognome` varchar(255) NOT NULL,
  `Nome_utente` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_anagrafica_Backoffice`),
  KEY `id_anagrafica_Backoffice` (`id_anagrafica_Backoffice`),
  KEY `id_anagrafica_Backoffice_2` (`id_anagrafica_Backoffice`),
  CONSTRAINT `FK66AD8F73C7566CA8` FOREIGN KEY (`id_anagrafica_Backoffice`) REFERENCES `anagrafica` (`ID`),
  CONSTRAINT `FKD0F7EE5FA68CD5D6` FOREIGN KEY (`id_anagrafica_Backoffice`) REFERENCES `anagrafica` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anagrafica_backoffice`
--

LOCK TABLES `anagrafica_backoffice` WRITE;
/*!40000 ALTER TABLE `anagrafica_backoffice` DISABLE KEYS */;
INSERT INTO `anagrafica_backoffice` VALUES (1,'Sistema','Amministratore',NULL);
/*!40000 ALTER TABLE `anagrafica_backoffice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anagrafica_frontend`
--

DROP TABLE IF EXISTS `anagrafica_frontend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anagrafica_frontend` (
  `id_anagrafica_Frontend` int(11) NOT NULL,
  `Nome` varchar(255) NOT NULL,
  `Cognome` varchar(255) NOT NULL,
  `Comune_di_Residenza` int(11) DEFAULT NULL,
  `Indirizzo` varchar(255) DEFAULT NULL,
  `CAP` varchar(5) DEFAULT NULL,
  `Telefono` varchar(255) DEFAULT NULL,
  `Cellulare` varchar(255) DEFAULT NULL,
  `Codice_Fiscale` varchar(16) DEFAULT NULL,
  `Azienda` varchar(255) DEFAULT NULL,
  `Data_di_Nascita` datetime DEFAULT NULL,
  `Comune_di_Nascita` int(11) DEFAULT NULL,
  `Fax` varchar(255) DEFAULT NULL,
  `Settore_Azienda` varchar(255) DEFAULT NULL,
  `Mansione_Azienda` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_anagrafica_Frontend`),
  KEY `id_anagrafica_Frontend` (`id_anagrafica_Frontend`),
  KEY `Comune_di_Residenza` (`Comune_di_Residenza`),
  KEY `Comune_di_Nascita` (`Comune_di_Nascita`),
  KEY `id_anagrafica_Frontend_2` (`id_anagrafica_Frontend`),
  CONSTRAINT `FK2A9085967935983F` FOREIGN KEY (`id_anagrafica_Frontend`) REFERENCES `anagrafica` (`ID`),
  CONSTRAINT `FKEE951DECE909429` FOREIGN KEY (`id_anagrafica_Frontend`) REFERENCES `anagrafica` (`ID`),
  CONSTRAINT `Frontend_Comune_di_Nascita` FOREIGN KEY (`Comune_di_Nascita`) REFERENCES `comuni` (`IDcomune`),
  CONSTRAINT `Frontend_Comune_di_Residenza` FOREIGN KEY (`Comune_di_Residenza`) REFERENCES `comuni` (`IDcomune`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anagrafica_frontend`
--

LOCK TABLES `anagrafica_frontend` WRITE;
/*!40000 ALTER TABLE `anagrafica_frontend` DISABLE KEYS */;
/*!40000 ALTER TABLE `anagrafica_frontend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applicazioni`
--

DROP TABLE IF EXISTS `applicazioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicazioni` (
  `id_Applicazione` int(11) NOT NULL,
  `SystemName_Applicazione` varchar(255) DEFAULT NULL,
  `LabelPlurale_Applicazione` varchar(255) DEFAULT NULL,
  `LabelSingolare_Applicazione` varchar(255) DEFAULT NULL,
  `EnableReviews_Applicazione` int(11) DEFAULT '0',
  `Enabled_Applicazione` int(11) DEFAULT '0',
  `EnableMove_Applicazione` int(11) DEFAULT '0',
  `EnableNewsLetter_Applicazione` int(11) DEFAULT '0',
  `EnableModule_Applicazione` int(11) DEFAULT NULL,
  `Namespace_Applicazione` mediumtext,
  PRIMARY KEY (`id_Applicazione`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicazioni`
--

LOCK TABLES `applicazioni` WRITE;
/*!40000 ALTER TABLE `applicazioni` DISABLE KEYS */;
INSERT INTO `applicazioni` VALUES (1,'homepage','Homepage','Homepage',0,1,1,1,0,NULL),(2,'webdocs','Pagine Web','Pagina Web',1,1,1,1,0,NULL),(3,'news','News','News',1,1,1,1,1,NULL),(4,'files','Allegati','Allegato',1,1,0,0,0,NULL),(9,'links','Collegamenti Ipertestuali','Collegamento Ipertestuale',1,1,1,0,1,NULL);
/*!40000 ALTER TABLE `applicazioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applicazioni_pagine`
--

DROP TABLE IF EXISTS `applicazioni_pagine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicazioni_pagine` (
  `id_PaginaApplicazione` int(11) NOT NULL AUTO_INCREMENT,
  `Pagename_PaginaApplicazione` varchar(255) DEFAULT NULL,
  `ClassName_PaginaApplicazione` mediumtext,
  `Applicazione_PaginaApplicazione` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_PaginaApplicazione`),
  KEY `Applicazione_PaginaApplicazione` (`Applicazione_PaginaApplicazione`),
  KEY `Applicazione_PaginaApplicazion_2` (`Applicazione_PaginaApplicazione`),
  CONSTRAINT `FK5EDBE445DA11F65C` FOREIGN KEY (`Applicazione_PaginaApplicazione`) REFERENCES `applicazioni` (`id_Applicazione`),
  CONSTRAINT `FKEB28084B31D2E4C` FOREIGN KEY (`Applicazione_PaginaApplicazione`) REFERENCES `applicazioni` (`id_Applicazione`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicazioni_pagine`
--

LOCK TABLES `applicazioni_pagine` WRITE;
/*!40000 ALTER TABLE `applicazioni_pagine` DISABLE KEYS */;
INSERT INTO `applicazioni_pagine` VALUES (123,'links_new.aspx','NetCms.Structure.Applications.Links.Pages.LinksAdd',9),(129,'news_new.aspx','NetCms.Structure.Applications.News.Pages.NewsAdd',3),(130,'modify.aspx','NetCms.Structure.Applications.News.Pages.NewsModify',3),(159,'files_new.aspx','NetCms.Structure.Applications.Files.Pages.FilesAdd',4),(160,'transfer.aspx','NetCms.Structure.Applications.Files.Pages.FilesPopupAdd',4),(161,'default.aspx','NetCms.Structure.Applications.Homepage.Pages.HomepageManager',1),(162,'gest_contenitore.aspx','NetCms.Structure.Applications.Homepage.Pages.EditContainer',1),(163,'gest_modulo.aspx','NetCms.Structure.Applications.Homepage.Pages.EditModule',1),(164,'ins_modulo.aspx','NetCms.Structure.Applications.Homepage.Pages.AddModule',1),(165,'modify.aspx','NetCms.Structure.Applications.Homepage.Pages.HomepageManager',1),(166,'prop_homepage.aspx','NetCms.Structure.Applications.Homepage.Pages.EditHomepage',1),(169,'webdocs_new.aspx','NetCms.Structure.Applications.WebDocs.Pages.WebDocsAdd',2),(170,'webdocs_move.aspx','NetCms.Structure.Applications.WebDocs.DocMove',2),(171,'files_new.aspx','NetCms.Structure.Applications.Files.Pages.FilesAdd',4),(172,'transfer.aspx','NetCms.Structure.Applications.Files.Pages.FilesPopupAdd',4),(173,'multi_upload.aspx','NetCms.Structure.Applications.Files.Pages.MultiUpload',4),(174,'default.aspx','NetCms.Structure.Applications.Homepage.Pages.HomepageManager',1),(175,'gest_contenitore.aspx','NetCms.Structure.Applications.Homepage.Pages.EditContainer',1),(176,'gest_modulo.aspx','NetCms.Structure.Applications.Homepage.Pages.EditModule',1),(177,'ins_modulo.aspx','NetCms.Structure.Applications.Homepage.Pages.AddModule',1),(178,'modify.aspx','NetCms.Structure.Applications.Homepage.Pages.HomepageManager',1),(179,'prop_homepage.aspx','NetCms.Structure.Applications.Homepage.Pages.EditHomepage',1),(180,'lang_modulo.aspx','NetCms.Structure.Applications.Homepage.Pages.TranslateModule',1);
/*!40000 ALTER TABLE `applicazioni_pagine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `changelog`
--

DROP TABLE IF EXISTS `changelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `changelog` (
  `id_Log` int(11) NOT NULL AUTO_INCREMENT,
  `Title_Log` text,
  `Desc_Log` longtext,
  `RequestText_Log` text,
  `RequestFrom_Log` varchar(255) DEFAULT NULL,
  `Date_Log` date DEFAULT NULL,
  PRIMARY KEY (`id_Log`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `changelog`
--

LOCK TABLES `changelog` WRITE;
/*!40000 ALTER TABLE `changelog` DISABLE KEYS */;
/*!40000 ALTER TABLE `changelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chiarimenti`
--

DROP TABLE IF EXISTS `chiarimenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chiarimenti` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Oggetto` varchar(255) DEFAULT NULL,
  `Testo` varchar(255) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `NetworkID` int(11) DEFAULT NULL,
  `Stato` varchar(255) NOT NULL,
  `RifDocument` int(11) DEFAULT NULL,
  `RifUtente` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RifDocument` (`RifDocument`),
  KEY `RifUtente` (`RifUtente`),
  KEY `RifDocument_2` (`RifDocument`),
  KEY `RifUtente_2` (`RifUtente`),
  CONSTRAINT `FK728E5F5611ACF482` FOREIGN KEY (`RifDocument`) REFERENCES `docs` (`id_Doc`),
  CONSTRAINT `FK728E5F56B11BBFA4` FOREIGN KEY (`RifUtente`) REFERENCES `users` (`id_User`),
  CONSTRAINT `FK77979DB2F549BB2` FOREIGN KEY (`RifUtente`) REFERENCES `users` (`id_User`),
  CONSTRAINT `FK77979DB66F3B934` FOREIGN KEY (`RifDocument`) REFERENCES `docs` (`id_Doc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chiarimenti`
--

LOCK TABLES `chiarimenti` WRITE;
/*!40000 ALTER TABLE `chiarimenti` DISABLE KEYS */;
/*!40000 ALTER TABLE `chiarimenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chiarimenti_competenze`
--

DROP TABLE IF EXISTS `chiarimenti_competenze`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chiarimenti_competenze` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(255) DEFAULT NULL,
  `RifApplicazione` int(11) DEFAULT NULL,
  `NetworkID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chiarimenti_competenze`
--

LOCK TABLES `chiarimenti_competenze` WRITE;
/*!40000 ALTER TABLE `chiarimenti_competenze` DISABLE KEYS */;
/*!40000 ALTER TABLE `chiarimenti_competenze` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chiarimenti_competenzegruppi`
--

DROP TABLE IF EXISTS `chiarimenti_competenzegruppi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chiarimenti_competenzegruppi` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RifGruppo` int(11) DEFAULT NULL,
  `RifCompentenzaGroup` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RifCompentenzaGroup` (`RifCompentenzaGroup`),
  KEY `RifCompentenzaGroup_2` (`RifCompentenzaGroup`),
  CONSTRAINT `FK1EC0E10233CEB5ED` FOREIGN KEY (`RifCompentenzaGroup`) REFERENCES `chiarimenti_competenze` (`ID`),
  CONSTRAINT `FK6D33BF8153A26F63` FOREIGN KEY (`RifCompentenzaGroup`) REFERENCES `chiarimenti_competenze` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chiarimenti_competenzegruppi`
--

LOCK TABLES `chiarimenti_competenzegruppi` WRITE;
/*!40000 ALTER TABLE `chiarimenti_competenzegruppi` DISABLE KEYS */;
/*!40000 ALTER TABLE `chiarimenti_competenzegruppi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chiarimenti_risposte`
--

DROP TABLE IF EXISTS `chiarimenti_risposte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chiarimenti_risposte` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Testo` varchar(255) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `RifUtente` int(11) DEFAULT NULL,
  `RifChiarimento` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RifUtente` (`RifUtente`),
  KEY `RifChiarimento` (`RifChiarimento`),
  KEY `RifUtente_2` (`RifUtente`),
  KEY `RifChiarimento_2` (`RifChiarimento`),
  CONSTRAINT `FK594C2B08B11BBFA4` FOREIGN KEY (`RifUtente`) REFERENCES `users` (`id_User`),
  CONSTRAINT `FK594C2B08E9E89F83` FOREIGN KEY (`RifChiarimento`) REFERENCES `chiarimenti` (`ID`),
  CONSTRAINT `FKEF6ED7612F549BB2` FOREIGN KEY (`RifUtente`) REFERENCES `users` (`id_User`),
  CONSTRAINT `FKEF6ED761BD25FE6C` FOREIGN KEY (`RifChiarimento`) REFERENCES `chiarimenti` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chiarimenti_risposte`
--

LOCK TABLES `chiarimenti_risposte` WRITE;
/*!40000 ALTER TABLE `chiarimenti_risposte` DISABLE KEYS */;
/*!40000 ALTER TABLE `chiarimenti_risposte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientclaims`
--

DROP TABLE IF EXISTS `clientclaims`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientclaims` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ClaimType` varchar(255) DEFAULT NULL,
  `ClaimValue` varchar(255) DEFAULT NULL,
  `ClientRef` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `ClientRef` (`ClientRef`),
  CONSTRAINT `clientref` FOREIGN KEY (`ClientRef`) REFERENCES `clients` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientclaims`
--

LOCK TABLES `clientclaims` WRITE;
/*!40000 ALTER TABLE `clientclaims` DISABLE KEYS */;
/*!40000 ALTER TABLE `clientclaims` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ClientId` varchar(255) DEFAULT NULL,
  `ClientSecret` varchar(255) DEFAULT NULL,
  `Disabled` tinyint(1) DEFAULT '0',
  `DisableBy` varchar(255) DEFAULT NULL,
  `DisableTimestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comuni`
--

DROP TABLE IF EXISTS `comuni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comuni` (
  `IDcomune` int(11) NOT NULL AUTO_INCREMENT,
  `codice_comune` varchar(6) NOT NULL DEFAULT '0',
  `codice_catastale` varchar(20) DEFAULT NULL,
  `comune` varchar(255) NOT NULL,
  `IDProvincia` int(11) DEFAULT NULL,
  `attivo` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`IDcomune`),
  UNIQUE KEY `codice_comune` (`codice_comune`),
  KEY `IDProvincia` (`IDProvincia`),
  CONSTRAINT `comune_provincia` FOREIGN KEY (`IDProvincia`) REFERENCES `province` (`IDProvincia`)
) ENGINE=InnoDB AUTO_INCREMENT=8573 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=53 PACK_KEYS=0 ROW_FORMAT=COMPACT COMMENT='InnoDB free: 9216 kB';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comuni`
--

LOCK TABLES `comuni` WRITE;
/*!40000 ALTER TABLE `comuni` DISABLE KEYS */;
INSERT INTO `comuni` VALUES (244,'001001','A074','Agliè',2,1),(245,'001002','A109','Airasca',2,1),(246,'001003','A117','Ala di Stura',2,1),(247,'001004','A157','Albiano d\'Ivrea',2,1),(248,'001005','A199','Alice Superiore',2,1),(249,'001006','A218','Almese',2,1),(250,'001007','A221','Alpette',2,1),(251,'001008','A222','Alpignano',2,1),(252,'001009','A275','Andezeno',2,1),(253,'001010','A282','Andrate',2,1),(254,'001011','A295','Angrogna',2,1),(255,'001012','A405','Arignano',2,1),(256,'001013','A518','Avigliana',2,1),(257,'001014','A525','Azeglio',2,1),(258,'001015','A584','Bairo',2,1),(259,'001016','A587','Balangero',2,1),(260,'001017','A590','Baldissero Canavese',2,1),(261,'001018','A591','Baldissero Torinese',2,1),(262,'001019','A599','Balme',2,1),(263,'001020','A607','Banchette',2,1),(264,'001021','A625','Barbania',2,1),(265,'001022','A651','Bardonecchia',2,1),(266,'001023','A673','Barone Canavese',2,1),(267,'001024','A734','Beinasco',2,1),(268,'001025','A853','Bibiana',2,1),(269,'001026','A910','Bobbio Pellice',2,1),(270,'001027','A941','Bollengo',2,1),(271,'001028','A990','Borgaro Torinese',2,1),(272,'001029','B003','Borgiallo',2,1),(273,'001030','B015','Borgofranco d\'Ivrea',2,1),(274,'001031','B021','Borgomasino',2,1),(275,'001032','B024','Borgone Susa',2,1),(276,'001033','B075','Bosconero',2,1),(277,'001034','B121','Brandizzo',2,1),(278,'001035','B171','Bricherasio',2,1),(279,'001036','B205','Brosso',2,1),(280,'001037','B209','Brozolo',2,1),(281,'001038','B216','Bruino',2,1),(282,'001039','B225','Brusasco',2,1),(283,'001040','B232','Bruzolo',2,1),(284,'001041','B278','Buriasco',2,1),(285,'001042','B279','Burolo',2,1),(286,'001043','B284','Busano',2,1),(287,'001044','B297','Bussoleno',2,1),(288,'001045','B305','Buttigliera Alta',2,1),(289,'001046','B350','Cafasse',2,1),(290,'001047','B435','Caluso',2,1),(291,'001048','B462','Cambiano',2,1),(292,'001049','B512','Campiglione-Fenile',2,1),(293,'001050','B588','Candia Canavese',2,1),(294,'001051','B592','Candiolo',2,1),(295,'001052','B605','Canischio',2,1),(296,'001053','B628','Cantalupa',2,1),(297,'001054','B637','Cantoira',2,1),(298,'001055','B705','Caprie',2,1),(299,'001056','B733','Caravino',2,1),(300,'001057','B762','Carema',2,1),(301,'001058','B777','Carignano',2,1),(302,'001059','B791','Carmagnola',2,1),(303,'001060','B867','Casalborgone',2,1),(304,'001061','B953','Cascinette d\'Ivrea',2,1),(305,'001062','B955','Caselette',2,1),(306,'001063','B960','Caselle Torinese',2,1),(307,'001064','C045','Castagneto Po',2,1),(308,'001065','C048','Castagnole Piemonte',2,1),(309,'001066','C133','Castellamonte',2,1),(310,'001067','C241','Castelnuovo Nigra',2,1),(311,'001068','C307','Castiglione Torinese',2,1),(312,'001069','C369','Cavagnolo',2,1),(313,'001070','C404','Cavour',2,1),(314,'001071','C487','Cercenasco',2,1),(315,'001072','C497','Ceres',2,1),(316,'001073','C505','Ceresole Reale',2,1),(317,'001074','C564','Cesana Torinese',2,1),(318,'001075','C604','Chialamberto',2,1),(319,'001076','C610','Chianocco',2,1),(320,'001077','C624','Chiaverano',2,1),(321,'001078','C627','Chieri',2,1),(322,'001079','C629','Chiesanuova',2,1),(323,'001080','C639','Chiomonte',2,1),(324,'001081','C655','Chiusa di San Michele',2,1),(325,'001082','C665','Chivasso',2,1),(326,'001083','C679','Ciconio',2,1),(327,'001084','C711','Cintano',2,1),(328,'001085','C715','Cinzano',2,1),(329,'001086','C722','Ciriè',2,1),(330,'001087','C793','Claviere',2,1),(331,'001088','C801','Coassolo Torinese',2,1),(332,'001089','C803','Coazze',2,1),(333,'001090','C860','Collegno',2,1),(334,'001091','C867','Colleretto Castelnuovo',2,1),(335,'001092','C868','Colleretto Giacosa',2,1),(336,'001093','C955','Condove',2,1),(337,'001094','D008','Corio',2,1),(338,'001095','D092','Cossano Canavese',2,1),(339,'001096','D197','Cuceglio',2,1),(340,'001097','D202','Cumiana',2,1),(341,'001098','D208','Cuorgnè',2,1),(342,'001099','D373','Druento',2,1),(343,'001100','D433','Exilles',2,1),(344,'001101','D520','Favria',2,1),(345,'001102','D524','Feletto',2,1),(346,'001103','D532','Fenestrelle',2,1),(347,'001104','D562','Fiano',2,1),(348,'001105','D608','Fiorano Canavese',2,1),(349,'001106','D646','Foglizzo',2,1),(350,'001107','D725','Forno Canavese',2,1),(351,'001108','D781','Frassinetto',2,1),(352,'001109','D805','Front',2,1),(353,'001110','D812','Frossasco',2,1),(354,'001111','D931','Garzigliana',2,1),(355,'001112','D933','Gassino Torinese',2,1),(356,'001113','D983','Germagnano',2,1),(357,'001114','E009','Giaglione',2,1),(358,'001115','E020','Giaveno',2,1),(359,'001116','E067','Givoletto',2,1),(360,'001117','E154','Gravere',2,1),(361,'001118','E199','Groscavallo',2,1),(362,'001119','E203','Grosso',2,1),(363,'001120','E216','Grugliasco',2,1),(364,'001121','E301','Ingria',2,1),(365,'001122','E311','Inverso Pinasca',2,1),(366,'001123','E345','Isolabella',2,1),(367,'001124','E368','Issiglio',2,1),(368,'001125','E379','Ivrea',2,1),(369,'001126','E394','La Cassa',2,1),(370,'001127','E423','La Loggia',2,1),(371,'001128','E445','Lanzo Torinese',2,1),(372,'001129','E484','Lauriano',2,1),(373,'001130','E518','Leinì',2,1),(374,'001131','E520','Lemie',2,1),(375,'001132','E551','Lessolo',2,1),(376,'001133','E566','Levone',2,1),(377,'001134','E635','Locana',2,1),(378,'001135','E660','Lombardore',2,1),(379,'001136','E661','Lombriasco',2,1),(380,'001137','E683','Loranzè',2,1),(381,'001138','E727','Lugnacco',2,1),(382,'001139','E758','Luserna San Giovanni',2,1),(383,'001140','E759','Lusernetta',2,1),(384,'001141','E763','Lusigliè',2,1),(385,'001142','E782','Macello',2,1),(386,'001143','E817','Maglione',2,1),(387,'001144','E941','Marentino',2,1),(388,'001145','F041','Massello',2,1),(389,'001146','F053','Mathi',2,1),(390,'001147','F058','Mattie',2,1),(391,'001148','F067','Mazzè',2,1),(392,'001149','F074','Meana di Susa',2,1),(393,'001150','F140','Mercenasco',2,1),(394,'001151','F164','Meugliano',2,1),(395,'001152','F182','Mezzenile',2,1),(396,'001153','F315','Mombello di Torino',2,1),(397,'001154','F318','Mompantero',2,1),(398,'001155','F327','Monastero di Lanzo',2,1),(399,'001156','F335','Moncalieri',2,1),(400,'001157','D553','Moncenisio',2,1),(401,'001158','F407','Montaldo Torinese',2,1),(402,'001159','F411','Montalenghe',2,1),(403,'001160','F420','Montalto Dora',2,1),(404,'001161','F422','Montanaro',2,1),(405,'001162','F651','Monteu da Po',2,1),(406,'001163','F733','Moriondo Torinese',2,1),(407,'001164','F889','Nichelino',2,1),(408,'001165','F906','Noasca',2,1),(409,'001166','F925','Nole',2,1),(410,'001167','F927','Nomaglio',2,1),(411,'001168','F931','None',2,1),(412,'001169','F948','Novalesa',2,1),(413,'001170','G010','Oglianico',2,1),(414,'001171','G087','Orbassano',2,1),(415,'001172','G109','Orio Canavese',2,1),(416,'001173','G151','Osasco',2,1),(417,'001174','G152','Osasio',2,1),(418,'001175','G196','Oulx',2,1),(419,'001176','G202','Ozegna',2,1),(420,'001177','G262','Palazzo Canavese',2,1),(421,'001178','G303','Pancalieri',2,1),(422,'001179','G330','Parella',2,1),(423,'001180','G387','Pavarolo',2,1),(424,'001181','G392','Pavone Canavese',2,1),(425,'001182','G396','Pecco',2,1),(426,'001183','G398','Pecetto Torinese',2,1),(427,'001184','G463','Perosa Argentina',2,1),(428,'001185','G462','Perosa Canavese',2,1),(429,'001186','G465','Perrero',2,1),(430,'001187','G477','Pertusio',2,1),(431,'001188','G505','Pessinetto',2,1),(432,'001189','G559','Pianezza',2,1),(433,'001190','G672','Pinasca',2,1),(434,'001191','G674','Pinerolo',2,1),(435,'001192','G678','Pino Torinese',2,1),(436,'001193','G684','Piobesi Torinese',2,1),(437,'001194','G691','Piossasco',2,1),(438,'001195','G705','Piscina',2,1),(439,'001196','G719','Piverone',2,1),(440,'001197','G777','Poirino',2,1),(441,'001198','G805','Pomaretto',2,1),(442,'001199','G826','Pont-Canavese',2,1),(443,'001200','G900','Porte',2,1),(444,'001201','G973','Pragelato',2,1),(445,'001202','G978','Prali',2,1),(446,'001203','G979','Pralormo',2,1),(447,'001204','G982','Pramollo',2,1),(448,'001205','G986','Prarostino',2,1),(449,'001206','G988','Prascorsano',2,1),(450,'001207','G997','Pratiglione',2,1),(451,'001208','H100','Quagliuzzo',2,1),(452,'001209','H120','Quassolo',2,1),(453,'001210','H127','Quincinetto',2,1),(454,'001211','H207','Reano',2,1),(455,'001212','H270','Ribordone',2,1),(456,'001213','H333','Rivalba',2,1),(457,'001214','H335','Rivalta di Torino',2,1),(458,'001215','H337','Riva presso Chieri',2,1),(459,'001216','H338','Rivara',2,1),(460,'001217','H340','Rivarolo Canavese',2,1),(461,'001218','H344','Rivarossa',2,1),(462,'001219','H355','Rivoli',2,1),(463,'001220','H367','Robassomero',2,1),(464,'001221','H386','Rocca Canavese',2,1),(465,'001222','H498','Roletto',2,1),(466,'001223','H511','Romano Canavese',2,1),(467,'001224','H539','Ronco Canavese',2,1),(468,'001225','H547','Rondissone',2,1),(469,'001226','H554','Rorà',2,1),(470,'001227','H555','Roure',2,1),(471,'001228','H583','Rosta',2,1),(472,'001229','H627','Rubiana',2,1),(473,'001230','H631','Rueglio',2,1),(474,'001231','H691','Salassa',2,1),(475,'001232','H684','Salbertrand',2,1),(476,'001233','H702','Salerano Canavese',2,1),(477,'001234','H734','Salza di Pinerolo',2,1),(478,'001235','H753','Samone',2,1),(479,'001236','H775','San Benigno Canavese',2,1),(480,'001237','H789','San Carlo Canavese',2,1),(481,'001238','H804','San Colombano Belmonte',2,1),(482,'001239','H820','San Didero',2,1),(483,'001240','H847','San Francesco al Campo',2,1),(484,'001241','H855','Sangano',2,1),(485,'001242','H862','San Germano Chisone',2,1),(486,'001243','H873','San Gillio',2,1),(487,'001244','H890','San Giorgio Canavese',2,1),(488,'001245','H900','San Giorio di Susa',2,1),(489,'001246','H936','San Giusto Canavese',2,1),(490,'001247','H997','San Martino Canavese',2,1),(491,'001248','I024','San Maurizio Canavese',2,1),(492,'001249','I030','San Mauro Torinese',2,1),(493,'001250','I090','San Pietro Val Lemina',2,1),(494,'001251','I126','San Ponso',2,1),(495,'001252','I137','San Raffaele Cimena',2,1),(496,'001253','I152','San Sebastiano da Po',2,1),(497,'001254','I154','San Secondo di Pinerolo',2,1),(498,'001255','I258','Sant\'Ambrogio di Torino',2,1),(499,'001256','I296','Sant\'Antonino di Susa',2,1),(500,'001257','I327','Santena',2,1),(501,'001258','I465','Sauze di Cesana',2,1),(502,'001259','I466','Sauze d\'Oulx',2,1),(503,'001260','I490','Scalenghe',2,1),(504,'001261','I511','Scarmagno',2,1),(505,'001262','I539','Sciolze',2,1),(506,'001263','I692','Sestriere',2,1),(507,'001264','I701','Settimo Rottaro',2,1),(508,'001265','I703','Settimo Torinese',2,1),(509,'001266','I702','Settimo Vittone',2,1),(510,'001267','I886','Sparone',2,1),(511,'001268','I969','Strambinello',2,1),(512,'001269','I970','Strambino',2,1),(513,'001270','L013','Susa',2,1),(514,'001271','L066','Tavagnasco',2,1),(515,'001272','L219','Torino',2,1),(516,'001273','L238','Torrazza Piemonte',2,1),(517,'001274','L247','Torre Canavese',2,1),(518,'001275','L277','Torre Pellice',2,1),(519,'001276','L327','Trana',2,1),(520,'001277','L338','Trausella',2,1),(521,'001278','L345','Traversella',2,1),(522,'001279','L340','Traves',2,1),(523,'001280','L445','Trofarello',2,1),(524,'001281','L515','Usseaux',2,1),(525,'001282','L516','Usseglio',2,1),(526,'001283','L538','Vaie',2,1),(527,'001284','L555','Val della Torre',2,1),(528,'001285','L578','Valgioie',2,1),(529,'001286','L629','Vallo Torinese',2,1),(530,'001287','L644','Valperga',2,1),(531,'001288','B510','Valprato Soana',2,1),(532,'001289','L685','Varisella',2,1),(533,'001290','L698','Vauda Canavese',2,1),(534,'001291','L726','Venaus',2,1),(535,'001292','L727','Venaria Reale',2,1),(536,'001293','L779','Verolengo',2,1),(537,'001294','L787','Verrua Savoia',2,1),(538,'001295','L811','Vestignè',2,1),(539,'001296','L830','Vialfrè',2,1),(540,'001297','L548','Vico Canavese',2,1),(541,'001298','L857','Vidracco',2,1),(542,'001299','L898','Vigone',2,1),(543,'001300','L948','Villafranca Piemonte',2,1),(544,'001301','L982','Villanova Canavese',2,1),(545,'001302','M002','Villarbasse',2,1),(546,'001303','L999','Villar Dora',2,1),(547,'001304','M004','Villareggia',2,1),(548,'001305','M007','Villar Focchiardo',2,1),(549,'001306','M013','Villar Pellice',2,1),(550,'001307','M014','Villar Perosa',2,1),(551,'001308','M027','Villastellone',2,1),(552,'001309','M060','Vinovo',2,1),(553,'001310','M069','Virle Piemonte',2,1),(554,'001311','M071','Vische',2,1),(555,'001312','M080','Vistrorio',2,1),(556,'001313','M094','Viù',2,1),(557,'001314','M122','Volpiano',2,1),(558,'001315','M133','Volvera',2,1),(559,'002002','A119','Alagna Valsesia',3,1),(560,'002003','A130','Albano Vercellese',3,1),(561,'002004','A198','Alice Castello',3,1),(562,'002006','A358','Arborio',3,1),(563,'002007','A466','Asigliano Vercellese',3,1),(564,'002008','A600','Balmuccia',3,1),(565,'002009','A601','Balocco',3,1),(566,'002011','A847','Bianzè',3,1),(567,'002014','A914','Boccioleto',3,1),(568,'002015','B009','Borgo d\'Ale',3,1),(569,'002016','B041','Borgosesia',3,1),(570,'002017','B046','Borgo Vercelli',3,1),(571,'002019','B136','Breia',3,1),(572,'002021','B280','Buronzo',3,1),(573,'002025','B505','Campertogno',3,1),(574,'002029','B752','Carcoforo',3,1),(575,'002030','B767','Caresana',3,1),(576,'002031','B768','Caresanablot',3,1),(577,'002032','B782','Carisio',3,1),(578,'002033','B928','Casanova Elvo',3,1),(579,'002035','B952','San Giacomo Vercellese',3,1),(580,'002038','C450','Cellio',3,1),(581,'002041','C548','Cervatto',3,1),(582,'002042','C680','Cigliano',3,1),(583,'002043','C757','Civiasco',3,1),(584,'002045','C884','Collobiano',3,1),(585,'002047','D113','Costanzana',3,1),(586,'002048','D132','Cravagliana',3,1),(587,'002049','D154','Crescentino',3,1),(588,'002052','D187','Crova',3,1),(589,'002054','D281','Desana',3,1),(590,'002057','D641','Fobello',3,1),(591,'002058','D676','Fontanetto Po',3,1),(592,'002059','D712','Formigliana',3,1),(593,'002061','D938','Gattinara',3,1),(594,'002062','E007','Ghislarengo',3,1),(595,'002065','E163','Greggio',3,1),(596,'002066','E237','Guardabosone',3,1),(597,'002067','E433','Lamporo',3,1),(598,'002068','E528','Lenta',3,1),(599,'002070','E583','Lignana',3,1),(600,'002071','E626','Livorno Ferraris',3,1),(601,'002072','E711','Lozzolo',3,1),(602,'002078','F297','Mollia',3,1),(603,'002079','F342','Moncrivello',3,1),(604,'002082','F774','Motta de\' Conti',3,1),(605,'002088','G016','Olcenengo',3,1),(606,'002089','G018','Oldenico',3,1),(607,'002090','G266','Palazzolo Vercellese',3,1),(608,'002091','G471','Pertengo',3,1),(609,'002093','G528','Pezzana',3,1),(610,'002096','G666','Pila',3,1),(611,'002097','G685','Piode',3,1),(612,'002102','G940','Postua',3,1),(613,'002104','G985','Prarolo',3,1),(614,'002107','H108','Quarona',3,1),(615,'002108','H132','Quinto Vercellese',3,1),(616,'002110','H188','Rassa',3,1),(617,'002111','H291','Rima San Giuseppe',3,1),(618,'002112','H292','Rimasco',3,1),(619,'002113','H293','Rimella',3,1),(620,'002114','H329','Riva Valdobbia',3,1),(621,'002115','H346','Rive',3,1),(622,'002116','H365','Roasio',3,1),(623,'002118','H549','Ronsecco',3,1),(624,'002121','H577','Rossa',3,1),(625,'002122','H364','Rovasenda',3,1),(626,'002123','H648','Sabbia',3,1),(627,'002126','H690','Salasco',3,1),(628,'002127','H707','Sali Vercellese',3,1),(629,'002128','H725','Saluggia',3,1),(630,'002131','H861','San Germano Vercellese',3,1),(631,'002133','I337','Santhià',3,1),(632,'002134','I544','Scopa',3,1),(633,'002135','I545','Scopello',3,1),(634,'002137','I663','Serravalle Sesia',3,1),(635,'002142','I984','Stroppiana',3,1),(636,'002147','L420','Tricerro',3,1),(637,'002148','L429','Trino',3,1),(638,'002150','L451','Tronzano Vercellese',3,1),(639,'002152','L566','Valduggia',3,1),(640,'002156','L669','Varallo',3,1),(641,'002158','L750','Vercelli',3,1),(642,'002163','M003','Villarboit',3,1),(643,'002164','M028','Villata',3,1),(644,'002166','M106','Vocca',3,1),(645,'003001','A088','Agrate Conturbia',4,1),(646,'003002','A264','Ameno',4,1),(647,'003006','A414','Armeno',4,1),(648,'003008','A429','Arona',4,1),(649,'003012','A653','Barengo',4,1),(650,'003016','A752','Bellinzago Novarese',4,1),(651,'003018','A844','Biandrate',4,1),(652,'003019','A911','Boca',4,1),(653,'003021','A929','Bogogno',4,1),(654,'003022','A953','Bolzano Novarese',4,1),(655,'003023','B016','Borgolavezzaro',4,1),(656,'003024','B019','Borgomanero',4,1),(657,'003025','B043','Borgo Ticino',4,1),(658,'003026','B176','Briga Novarese',4,1),(659,'003027','B183','Briona',4,1),(660,'003030','B431','Caltignaga',4,1),(661,'003032','B473','Cameri',4,1),(662,'003036','B823','Carpignano Sesia',4,1),(663,'003037','B864','Casalbeltrame',4,1),(664,'003039','B883','Casaleggio Novara',4,1),(665,'003040','B897','Casalino',4,1),(666,'003041','B920','Casalvolone',4,1),(667,'003042','C149','Castellazzo Novarese',4,1),(668,'003043','C166','Castelletto sopra Ticino',4,1),(669,'003044','C364','Cavaglietto',4,1),(670,'003045','C365','Cavaglio d\'Agogna',4,1),(671,'003047','C378','Cavallirio',4,1),(672,'003049','C483','Cerano',4,1),(673,'003051','C829','Colazza',4,1),(674,'003052','C926','Comignago',4,1),(675,'003055','D162','Cressa',4,1),(676,'003058','D216','Cureggio',4,1),(677,'003060','D309','Divignano',4,1),(678,'003062','D347','Dormelletto',4,1),(679,'003065','D492','Fara Novarese',4,1),(680,'003066','D675','Fontaneto d\'Agogna',4,1),(681,'003068','D872','Galliate',4,1),(682,'003069','D911','Garbagna Novarese',4,1),(683,'003070','D921','Gargallo',4,1),(684,'003071','D937','Gattico',4,1),(685,'003073','E001','Ghemme',4,1),(686,'003076','E120','Gozzano',4,1),(687,'003077','E143','Granozzo con Monticello',4,1),(688,'003079','E177','Grignasco',4,1),(689,'003082','E314','Invorio',4,1),(690,'003083','E436','Landiona',4,1),(691,'003084','E544','Lesa',4,1),(692,'003088','E803','Maggiora',4,1),(693,'003090','E880','Mandello Vitta',4,1),(694,'003091','E907','Marano Ticino',4,1),(695,'003093','F047','Massino Visconti',4,1),(696,'003095','F093','Meina',4,1),(697,'003097','F188','Mezzomerico',4,1),(698,'003098','F191','Miasino',4,1),(699,'003100','F317','Momo',4,1),(700,'003103','F859','Nebbiuno',4,1),(701,'003104','F886','Nibbiola',4,1),(702,'003106','F952','Novara',4,1),(703,'003108','G019','Oleggio',4,1),(704,'003109','G020','Oleggio Castello',4,1),(705,'003112','G134','Orta San Giulio',4,1),(706,'003114','G349','Paruzzaro',4,1),(707,'003115','G421','Pella',4,1),(708,'003116','G520','Pettenasco',4,1),(709,'003119','G703','Pisano',4,1),(710,'003120','G775','Pogno',4,1),(711,'003121','G809','Pombia',4,1),(712,'003122','H001','Prato Sesia',4,1),(713,'003129','H213','Recetto',4,1),(714,'003130','H502','Romagnano Sesia',4,1),(715,'003131','H518','Romentino',4,1),(716,'003133','I025','San Maurizio d\'Opaglio',4,1),(717,'003134','I052','San Nazzaro Sesia',4,1),(718,'003135','I116','San Pietro Mosezzo',4,1),(719,'003138','I736','Sillavengo',4,1),(720,'003139','I767','Sizzano',4,1),(721,'003140','I857','Soriso',4,1),(722,'003141','I880','Sozzago',4,1),(723,'003143','L007','Suno',4,1),(724,'003144','L104','Terdobbiate',4,1),(725,'003146','L223','Tornaco',4,1),(726,'003149','L356','Trecate',4,1),(727,'003153','L668','Vaprio d\'Agogna',4,1),(728,'003154','L670','Varallo Pombia',4,1),(729,'003157','L798','Veruno',4,1),(730,'003158','L808','Vespolate',4,1),(731,'003159','L847','Vicolungo',4,1),(732,'003164','M062','Vinzaglio',4,1),(733,'004001','A016','Acceglio',5,1),(734,'004002','A113','Aisone',5,1),(735,'004003','A124','Alba',5,1),(736,'004004','A139','Albaretto della Torre',5,1),(737,'004005','A238','Alto',5,1),(738,'004006','A394','Argentera',5,1),(739,'004007','A396','Arguello',5,1),(740,'004008','A555','Bagnasco',5,1),(741,'004009','A571','Bagnolo Piemonte',5,1),(742,'004010','A589','Baldissero d\'Alba',5,1),(743,'004011','A629','Barbaresco',5,1),(744,'004012','A660','Barge',5,1),(745,'004013','A671','Barolo',5,1),(746,'004014','A709','Bastia Mondovì',5,1),(747,'004015','A716','Battifollo',5,1),(748,'004016','A735','Beinette',5,1),(749,'004017','A750','Bellino',5,1),(750,'004018','A774','Belvedere Langhe',5,1),(751,'004019','A779','Bene Vagienna',5,1),(752,'004020','A782','Benevello',5,1),(753,'004021','A798','Bergolo',5,1),(754,'004022','A805','Bernezzo',5,1),(755,'004023','A979','Bonvicino',5,1),(756,'004024','B018','Borgomale',5,1),(757,'004025','B033','Borgo San Dalmazzo',5,1),(758,'004026','B079','Bosia',5,1),(759,'004027','B084','Bossolasco',5,1),(760,'004028','B101','Boves',5,1),(761,'004029','B111','Bra',5,1),(762,'004030','B167','Briaglia',5,1),(763,'004031','B175','Briga Alta',5,1),(764,'004032','B200','Brondello',5,1),(765,'004033','B204','Brossasco',5,1),(766,'004034','B285','Busca',5,1),(767,'004035','B467','Camerana',5,1),(768,'004036','B489','Camo',5,1),(769,'004037','B573','Canale',5,1),(770,'004038','B621','Canosio',5,1),(771,'004039','B692','Caprauna',5,1),(772,'004040','B719','Caraglio',5,1),(773,'004041','B720','Caramagna Piemonte',5,1),(774,'004042','B755','Cardè',5,1),(775,'004043','B841','Carrù',5,1),(776,'004044','B845','Cartignano',5,1),(777,'004045','B894','Casalgrasso',5,1),(778,'004046','C046','Castagnito',5,1),(779,'004047','C081','Casteldelfino',5,1),(780,'004048','C140','Castellar',5,1),(781,'004049','C165','Castelletto Stura',5,1),(782,'004050','C167','Castelletto Uzzone',5,1),(783,'004051','C173','Castellinaldo',5,1),(784,'004052','C176','Castellino Tanaro',5,1),(785,'004053','C205','Castelmagno',5,1),(786,'004054','C214','Castelnuovo di Ceva',5,1),(787,'004055','C314','Castiglione Falletto',5,1),(788,'004056','C317','Castiglione Tinella',5,1),(789,'004057','C323','Castino',5,1),(790,'004058','C375','Cavallerleone',5,1),(791,'004059','C376','Cavallermaggiore',5,1),(792,'004060','C441','Celle di Macra',5,1),(793,'004061','C466','Centallo',5,1),(794,'004062','C504','Ceresole Alba',5,1),(795,'004063','C530','Cerretto Langhe',5,1),(796,'004064','C547','Cervasca',5,1),(797,'004065','C550','Cervere',5,1),(798,'004066','C589','Ceva',5,1),(799,'004067','C599','Cherasco',5,1),(800,'004068','C653','Chiusa di Pesio',5,1),(801,'004069','C681','Cigliè',5,1),(802,'004070','C738','Cissone',5,1),(803,'004071','C792','Clavesana',5,1),(804,'004072','D022','Corneliano d\'Alba',5,1),(805,'004073','D062','Cortemilia',5,1),(806,'004074','D093','Cossano Belbo',5,1),(807,'004075','D120','Costigliole Saluzzo',5,1),(808,'004076','D133','Cravanzana',5,1),(809,'004077','D172','Crissolo',5,1),(810,'004078','D205','Cuneo',5,1),(811,'004079','D271','Demonte',5,1),(812,'004080','D291','Diano d\'Alba',5,1),(813,'004081','D314','Dogliani',5,1),(814,'004082','D372','Dronero',5,1),(815,'004083','D401','Elva',5,1),(816,'004084','D410','Entracque',5,1),(817,'004085','D412','Envie',5,1),(818,'004086','D499','Farigliano',5,1),(819,'004087','D511','Faule',5,1),(820,'004088','D523','Feisoglio',5,1),(821,'004089','D742','Fossano',5,1),(822,'004090','D751','Frabosa Soprana',5,1),(823,'004091','D752','Frabosa Sottana',5,1),(824,'004092','D782','Frassino',5,1),(825,'004093','D856','Gaiola',5,1),(826,'004094','D894','Gambasca',5,1),(827,'004095','D920','Garessio',5,1),(828,'004096','D967','Genola',5,1),(829,'004097','E111','Gorzegno',5,1),(830,'004098','E115','Gottasecca',5,1),(831,'004099','E118','Govone',5,1),(832,'004100','E182','Grinzane Cavour',5,1),(833,'004101','E251','Guarene',5,1),(834,'004102','E282','Igliano',5,1),(835,'004103','E327','Isasca',5,1),(836,'004104','E406','Lagnasco',5,1),(837,'004105','E430','La Morra',5,1),(838,'004106','E540','Lequio Berria',5,1),(839,'004107','E539','Lequio Tanaro',5,1),(840,'004108','E546','Lesegno',5,1),(841,'004109','E564','Levice',5,1),(842,'004110','E597','Limone Piemonte',5,1),(843,'004111','E615','Lisio',5,1),(844,'004112','E789','Macra',5,1),(845,'004113','E809','Magliano Alfieri',5,1),(846,'004114','E808','Magliano Alpi',5,1),(847,'004115','E887','Mango',5,1),(848,'004116','E894','Manta',5,1),(849,'004117','E939','Marene',5,1),(850,'004118','E945','Margarita',5,1),(851,'004119','E963','Marmora',5,1),(852,'004120','E973','Marsaglia',5,1),(853,'004121','E988','Martiniana Po',5,1),(854,'004122','F114','Melle',5,1),(855,'004123','F279','Moiola',5,1),(856,'004124','F309','Mombarcaro',5,1),(857,'004125','F312','Mombasiglio',5,1),(858,'004126','F326','Monastero di Vasco',5,1),(859,'004127','F329','Monasterolo Casotto',5,1),(860,'004128','F330','Monasterolo di Savigliano',5,1),(861,'004129','F338','Monchiero',5,1),(862,'004130','F351','Mondovì',5,1),(863,'004131','F355','Monesiglio',5,1),(864,'004132','F358','Monforte d\'Alba',5,1),(865,'004133','F385','Montà',5,1),(866,'004134','F405','Montaldo di Mondovì',5,1),(867,'004135','F408','Montaldo Roero',5,1),(868,'004136','F424','Montanera',5,1),(869,'004137','F550','Montelupo Albese',5,1),(870,'004138','F558','Montemale di Cuneo',5,1),(871,'004139','F608','Monterosso Grana',5,1),(872,'004140','F654','Monteu Roero',5,1),(873,'004141','F666','Montezemolo',5,1),(874,'004142','F669','Monticello d\'Alba',5,1),(875,'004143','F723','Moretta',5,1),(876,'004144','F743','Morozzo',5,1),(877,'004145','F809','Murazzano',5,1),(878,'004146','F811','Murello',5,1),(879,'004147','F846','Narzole',5,1),(880,'004148','F863','Neive',5,1),(881,'004149','F883','Neviglie',5,1),(882,'004150','F894','Niella Belbo',5,1),(883,'004151','F895','Niella Tanaro',5,1),(884,'004152','F961','Novello',5,1),(885,'004153','F972','Nucetto',5,1),(886,'004154','G066','Oncino',5,1),(887,'004155','G114','Ormea',5,1),(888,'004156','G183','Ostana',5,1),(889,'004157','G228','Paesana',5,1),(890,'004158','G240','Pagno',5,1),(891,'004159','G302','Pamparato',5,1),(892,'004160','G339','Paroldo',5,1),(893,'004161','G457','Perletto',5,1),(894,'004162','G458','Perlo',5,1),(895,'004163','G526','Peveragno',5,1),(896,'004164','G532','Pezzolo Valle Uzzone',5,1),(897,'004165','G561','Pianfei',5,1),(898,'004166','G575','Piasco',5,1),(899,'004167','G625','Pietraporzio',5,1),(900,'004168','G683','Piobesi d\'Alba',5,1),(901,'004169','G697','Piozzo',5,1),(902,'004170','G742','Pocapaglia',5,1),(903,'004171','G800','Polonghera',5,1),(904,'004172','G837','Pontechianale',5,1),(905,'004173','G970','Pradleves',5,1),(906,'004174','H011','Prazzo',5,1),(907,'004175','H059','Priero',5,1),(908,'004176','H068','Priocca',5,1),(909,'004177','H069','Priola',5,1),(910,'004178','H085','Prunetto',5,1),(911,'004179','H150','Racconigi',5,1),(912,'004180','H247','Revello',5,1),(913,'004181','H285','Rifreddo',5,1),(914,'004182','H326','Rittana',5,1),(915,'004183','H362','Roaschia',5,1),(916,'004184','H363','Roascio',5,1),(917,'004185','H377','Robilante',5,1),(918,'004186','H378','Roburent',5,1),(919,'004187','H385','Roccabruna',5,1),(920,'004188','H391','Rocca Cigliè',5,1),(921,'004189','H395','Rocca de\' Baldi',5,1),(922,'004190','H407','Roccaforte Mondovì',5,1),(923,'004191','H447','Roccasparvera',5,1),(924,'004192','H453','Roccavione',5,1),(925,'004193','H462','Rocchetta Belbo',5,1),(926,'004194','H472','Roddi',5,1),(927,'004195','H473','Roddino',5,1),(928,'004196','H474','Rodello',5,1),(929,'004197','H578','Rossana',5,1),(930,'004198','H633','Ruffia',5,1),(931,'004199','H695','Sale delle Langhe',5,1),(932,'004200','H704','Sale San Giovanni',5,1),(933,'004201','H710','Saliceto',5,1),(934,'004202','H716','Salmour',5,1),(935,'004203','H727','Saluzzo',5,1),(936,'004204','H746','Sambuco',5,1),(937,'004205','H755','Sampeyre',5,1),(938,'004206','H770','San Benedetto Belbo',5,1),(939,'004207','H812','San Damiano Macra',5,1),(940,'004208','H851','Sanfrè',5,1),(941,'004209','H852','Sanfront',5,1),(942,'004210','I037','San Michele Mondovì',5,1),(943,'004211','I210','Sant\'Albano Stura',5,1),(944,'004212','I316','Santa Vittoria d\'Alba',5,1),(945,'004213','I367','Santo Stefano Belbo',5,1),(946,'004214','I372','Santo Stefano Roero',5,1),(947,'004215','I470','Savigliano',5,1),(948,'004216','I484','Scagnello',5,1),(949,'004217','I512','Scarnafigi',5,1),(950,'004218','I646','Serralunga d\'Alba',5,1),(951,'004219','I659','Serravalle Langhe',5,1),(952,'004220','I750','Sinio',5,1),(953,'004221','I817','Somano',5,1),(954,'004222','I822','Sommariva del Bosco',5,1),(955,'004223','I823','Sommariva Perno',5,1),(956,'004224','I985','Stroppo',5,1),(957,'004225','L048','Tarantasca',5,1),(958,'004226','L252','Torre Bormida',5,1),(959,'004227','L241','Torre Mondovì',5,1),(960,'004228','L278','Torre San Giorgio',5,1),(961,'004229','L281','Torresina',5,1),(962,'004230','L367','Treiso',5,1),(963,'004231','L410','Trezzo Tinella',5,1),(964,'004232','L427','Trinità',5,1),(965,'004233','L558','Valdieri',5,1),(966,'004234','L580','Valgrana',5,1),(967,'004235','L631','Valloriate',5,1),(968,'004236','L636','Valmala',5,1),(969,'004237','L729','Venasca',5,1),(970,'004238','L758','Verduno',5,1),(971,'004239','L771','Vernante',5,1),(972,'004240','L804','Verzuolo',5,1),(973,'004241','L817','Vezza d\'Alba',5,1),(974,'004242','L841','Vicoforte',5,1),(975,'004243','L888','Vignolo',5,1),(976,'004244','L942','Villafalletto',5,1),(977,'004245','L974','Villanova Mondovì',5,1),(978,'004246','L990','Villanova Solaro',5,1),(979,'004247','M015','Villar San Costanzo',5,1),(980,'004248','M055','Vinadio',5,1),(981,'004249','M063','Viola',5,1),(982,'004250','M136','Vottignasco',5,1),(983,'005001','A072','Agliano Terme',6,1),(984,'005002','A173','Albugnano',6,1),(985,'005003','A312','Antignano',6,1),(986,'005004','A352','Aramengo',6,1),(987,'005005','A479','Asti',6,1),(988,'005006','A527','Azzano d\'Asti',6,1),(989,'005007','A588','Baldichieri d\'Asti',6,1),(990,'005008','A770','Belveglio',6,1),(991,'005009','A812','Berzano di San Pietro',6,1),(992,'005010','B221','Bruno',6,1),(993,'005011','B236','Bubbio',6,1),(994,'005012','B306','Buttigliera d\'Asti',6,1),(995,'005013','B376','Calamandrana',6,1),(996,'005014','B418','Calliano',6,1),(997,'005015','B425','Calosso',6,1),(998,'005016','B469','Camerano Casasco',6,1),(999,'005017','B594','Canelli',6,1),(1000,'005018','B633','Cantarana',6,1),(1001,'005019','B707','Capriglio',6,1),(1002,'005020','B991','Casorzo',6,1),(1003,'005021','C022','Cassinasco',6,1),(1004,'005022','C049','Castagnole delle Lanze',6,1),(1005,'005023','C047','Castagnole Monferrato',6,1),(1006,'005024','C064','Castel Boglione',6,1),(1007,'005025','C127','Castell\'Alfero',6,1),(1008,'005026','C154','Castellero',6,1),(1009,'005027','C161','Castelletto Molina',6,1),(1010,'005028','A300','Castello di Annone',6,1),(1011,'005029','C226','Castelnuovo Belbo',6,1),(1012,'005030','C230','Castelnuovo Calcea',6,1),(1013,'005031','C232','Castelnuovo Don Bosco',6,1),(1014,'005032','C253','Castel Rocchero',6,1),(1015,'005033','C438','Cellarengo',6,1),(1016,'005034','C440','Celle Enomondo',6,1),(1017,'005035','C528','Cerreto d\'Asti',6,1),(1018,'005036','C533','Cerro Tanaro',6,1),(1019,'005037','C583','Cessole',6,1),(1020,'005038','C658','Chiusano d\'Asti',6,1),(1021,'005039','C701','Cinaglio',6,1),(1022,'005040','C739','Cisterna d\'Asti',6,1),(1023,'005041','C804','Coazzolo',6,1),(1024,'005042','C807','Cocconato',6,1),(1025,'005044','D046','Corsione',6,1),(1026,'005045','D050','Cortandone',6,1),(1027,'005046','D051','Cortanze',6,1),(1028,'005047','D052','Cortazzone',6,1),(1029,'005048','D072','Cortiglione',6,1),(1030,'005049','D101','Cossombrato',6,1),(1031,'005050','D119','Costigliole d\'Asti',6,1),(1032,'005051','D207','Cunico',6,1),(1033,'005052','D388','Dusino San Michele',6,1),(1034,'005053','D554','Ferrere',6,1),(1035,'005054','D678','Fontanile',6,1),(1036,'005055','D802','Frinco',6,1),(1037,'005056','E134','Grana',6,1),(1038,'005057','E159','Grazzano Badoglio',6,1),(1039,'005058','E295','Incisa Scapaccino',6,1),(1040,'005059','E338','Isola d\'Asti',6,1),(1041,'005060','E633','Loazzolo',6,1),(1042,'005061','E917','Maranzana',6,1),(1043,'005062','E944','Maretto',6,1),(1044,'005063','F254','Moasca',6,1),(1045,'005064','F308','Mombaldone',6,1),(1046,'005065','F311','Mombaruzzo',6,1),(1047,'005066','F316','Mombercelli',6,1),(1048,'005067','F323','Monale',6,1),(1049,'005068','F325','Monastero Bormida',6,1),(1050,'005069','F336','Moncalvo',6,1),(1051,'005070','F343','Moncucco Torinese',6,1),(1052,'005071','F361','Mongardino',6,1),(1053,'005072','F386','Montabone',6,1),(1054,'005073','F390','Montafia',6,1),(1055,'005074','F409','Montaldo Scarampi',6,1),(1056,'005075','F468','Montechiaro d\'Asti',6,1),(1057,'005076','F527','Montegrosso d\'Asti',6,1),(1058,'005077','F556','Montemagno',6,1),(1059,'005079','F709','Moransengo',6,1),(1060,'005080','F902','Nizza Monferrato',6,1),(1061,'005081','G048','Olmo Gentile',6,1),(1062,'005082','G358','Passerano Marmorito',6,1),(1063,'005083','G430','Penango',6,1),(1064,'005084','G593','Piea',6,1),(1065,'005085','G676','Pino d\'Asti',6,1),(1066,'005086','G692','Piovà Massaia',6,1),(1067,'005087','G894','Portacomaro',6,1),(1068,'005088','H102','Quaranti',6,1),(1069,'005089','H219','Refrancore',6,1),(1070,'005090','H250','Revigliasco d\'Asti',6,1),(1071,'005091','H366','Roatto',6,1),(1072,'005092','H376','Robella',6,1),(1073,'005093','H392','Rocca d\'Arazzo',6,1),(1074,'005094','H451','Roccaverano',6,1),(1075,'005095','H466','Rocchetta Palafea',6,1),(1076,'005096','H468','Rocchetta Tanaro',6,1),(1077,'005097','H811','San Damiano d\'Asti',6,1),(1078,'005098','H899','San Giorgio Scarampi',6,1),(1079,'005099','H987','San Martino Alfieri',6,1),(1080,'005100','I017','San Marzano Oliveto',6,1),(1081,'005101','I076','San Paolo Solbrito',6,1),(1082,'005103','I555','Scurzolengo',6,1),(1083,'005104','I637','Serole',6,1),(1084,'005105','I678','Sessame',6,1),(1085,'005106','I698','Settime',6,1),(1086,'005107','I781','Soglio',6,1),(1087,'005108','L168','Tigliole',6,1),(1088,'005109','L203','Tonco',6,1),(1089,'005110','L204','Tonengo',6,1),(1090,'005111','L531','Vaglio Serra',6,1),(1091,'005112','L574','Valfenera',6,1),(1092,'005113','L807','Vesime',6,1),(1093,'005114','L829','Viale',6,1),(1094,'005115','L834','Viarigi',6,1),(1095,'005116','L879','Vigliano d\'Asti',6,1),(1096,'005117','L945','Villafranca d\'Asti',6,1),(1097,'005118','L984','Villanova d\'Asti',6,1),(1098,'005119','M019','Villa San Secondo',6,1),(1099,'005120','M058','Vinchio',6,1),(1100,'005121','M302','Montiglio Monferrato',6,1),(1101,'006001','A052','Acqui Terme',7,1),(1102,'006002','A146','Albera Ligure',7,1),(1103,'006003','A182','Alessandria',7,1),(1104,'006004','A189','Alfiano Natta',7,1),(1105,'006005','A197','Alice Bel Colle',7,1),(1106,'006006','A211','Alluvioni Cambiò',7,1),(1107,'006007','A227','Altavilla Monferrato',7,1),(1108,'006008','A245','Alzano Scrivia',7,1),(1109,'006009','A436','Arquata Scrivia',7,1),(1110,'006010','A523','Avolasca',7,1),(1111,'006011','A605','Balzola',7,1),(1112,'006012','A689','Basaluzzo',7,1),(1113,'006013','A708','Bassignana',7,1),(1114,'006014','A738','Belforte Monferrato',7,1),(1115,'006015','A793','Bergamasco',7,1),(1116,'006016','A813','Berzano di Tortona',7,1),(1117,'006017','A889','Bistagno',7,1),(1118,'006018','A998','Borghetto di Borbera',7,1),(1119,'006019','B029','Borgoratto Alessandrino',7,1),(1120,'006020','B037','Borgo San Martino',7,1),(1121,'006021','B071','Bosco Marengo',7,1),(1122,'006022','B080','Bosio',7,1),(1123,'006023','B109','Bozzole',7,1),(1124,'006024','B179','Brignano-Frascata',7,1),(1125,'006025','B311','Cabella Ligure',7,1),(1126,'006026','B453','Camagna Monferrato',7,1),(1127,'006027','B482','Camino',7,1),(1128,'006028','B629','Cantalupo Ligure',7,1),(1129,'006029','B701','Capriata d\'Orba',7,1),(1130,'006030','B736','Carbonara Scrivia',7,1),(1131,'006031','B765','Carentino',7,1),(1132,'006032','B769','Carezzano',7,1),(1133,'006033','B818','Carpeneto',7,1),(1134,'006034','B836','Carrega Ligure',7,1),(1135,'006035','B840','Carrosio',7,1),(1136,'006036','B847','Cartosio',7,1),(1137,'006037','B870','Casal Cermelli',7,1),(1138,'006038','B882','Casaleggio Boiro',7,1),(1139,'006039','B885','Casale Monferrato',7,1),(1140,'006040','B902','Casalnoceto',7,1),(1141,'006041','B941','Casasco',7,1),(1142,'006042','C005','Cassano Spinola',7,1),(1143,'006043','C027','Cassine',7,1),(1144,'006044','C030','Cassinelle',7,1),(1145,'006045','C137','Castellania',7,1),(1146,'006046','C142','Castellar Guidobono',7,1),(1147,'006047','C148','Castellazzo Bormida',7,1),(1148,'006048','C156','Castelletto d\'Erro',7,1),(1149,'006049','C158','Castelletto d\'Orba',7,1),(1150,'006050','C160','Castelletto Merli',7,1),(1151,'006051','C162','Castelletto Monferrato',7,1),(1152,'006052','C229','Castelnuovo Bormida',7,1),(1153,'006053','C243','Castelnuovo Scrivia',7,1),(1154,'006054','C274','Castelspina',7,1),(1155,'006055','C387','Cavatore',7,1),(1156,'006056','C432','Cella Monte',7,1),(1157,'006057','C503','Cereseto',7,1),(1158,'006058','C507','Cerreto Grue',7,1),(1159,'006059','C531','Cerrina Monferrato',7,1),(1160,'006060','C962','Coniolo',7,1),(1161,'006061','C977','Conzano',7,1),(1162,'006062','D102','Costa Vescovato',7,1),(1163,'006063','D149','Cremolino',7,1),(1164,'006064','D194','Cuccaro Monferrato',7,1),(1165,'006065','D272','Denice',7,1),(1166,'006066','D277','Dernice',7,1),(1167,'006067','D447','Fabbrica Curone',7,1),(1168,'006068','D528','Felizzano',7,1),(1169,'006069','D559','Fraconalto',7,1),(1170,'006070','D759','Francavilla Bisio',7,1),(1171,'006071','D770','Frascaro',7,1),(1172,'006072','D777','Frassinello Monferrato',7,1),(1173,'006073','D780','Frassineto Po',7,1),(1174,'006074','D797','Fresonara',7,1),(1175,'006075','D813','Frugarolo',7,1),(1176,'006076','D814','Fubine',7,1),(1177,'006077','D835','Gabiano',7,1),(1178,'006078','D890','Gamalero',7,1),(1179,'006079','D910','Garbagna',7,1),(1180,'006080','D941','Gavazzana',7,1),(1181,'006081','D944','Gavi',7,1),(1182,'006082','E015','Giarole',7,1),(1183,'006083','E164','Gremiasco',7,1),(1184,'006084','E188','Grognardo',7,1),(1185,'006085','E191','Grondona',7,1),(1186,'006086','E255','Guazzora',7,1),(1187,'006087','E360','Isola Sant\'Antonio',7,1),(1188,'006088','E543','Lerma',7,1),(1189,'006089','E712','Lu',7,1),(1190,'006090','E870','Malvicino',7,1),(1191,'006091','F015','Masio',7,1),(1192,'006092','F096','Melazzo',7,1),(1193,'006093','F131','Merana',7,1),(1194,'006094','F232','Mirabello Monferrato',7,1),(1195,'006095','F281','Molare',7,1),(1196,'006096','F293','Molino dei Torti',7,1),(1197,'006097','F313','Mombello Monferrato',7,1),(1198,'006098','F320','Momperone',7,1),(1199,'006099','F337','Moncestino',7,1),(1200,'006100','F365','Mongiardino Ligure',7,1),(1201,'006101','F374','Monleale',7,1),(1202,'006102','F387','Montacuto',7,1),(1203,'006103','F403','Montaldeo',7,1),(1204,'006104','F404','Montaldo Bormida',7,1),(1205,'006105','F455','Montecastello',7,1),(1206,'006106','F469','Montechiaro d\'Acqui',7,1),(1207,'006107','F518','Montegioco',7,1),(1208,'006108','F562','Montemarzino',7,1),(1209,'006109','F707','Morano sul Po',7,1),(1210,'006110','F713','Morbello',7,1),(1211,'006111','F737','Mornese',7,1),(1212,'006112','F751','Morsasco',7,1),(1213,'006113','F814','Murisengo',7,1),(1214,'006114','F965','Novi Ligure',7,1),(1215,'006115','F995','Occimiano',7,1),(1216,'006116','F997','Odalengo Grande',7,1),(1217,'006117','F998','Odalengo Piccolo',7,1),(1218,'006118','G042','Olivola',7,1),(1219,'006119','G124','Orsara Bormida',7,1),(1220,'006120','G193','Ottiglio',7,1),(1221,'006121','G197','Ovada',7,1),(1222,'006122','G199','Oviglio',7,1),(1223,'006123','G204','Ozzano Monferrato',7,1),(1224,'006124','G215','Paderna',7,1),(1225,'006125','G334','Pareto',7,1),(1226,'006126','G338','Parodi Ligure',7,1),(1227,'006127','G367','Pasturana',7,1),(1228,'006128','G397','Pecetto di Valenza',7,1),(1229,'006129','G619','Pietra Marazzi',7,1),(1230,'006130','G695','Piovera',7,1),(1231,'006131','G807','Pomaro Monferrato',7,1),(1232,'006132','G839','Pontecurone',7,1),(1233,'006133','G858','Pontestura',7,1),(1234,'006134','G861','Ponti',7,1),(1235,'006135','G872','Ponzano Monferrato',7,1),(1236,'006136','G877','Ponzone',7,1),(1237,'006137','G960','Pozzol Groppo',7,1),(1238,'006138','G961','Pozzolo Formigaro',7,1),(1239,'006139','G987','Prasco',7,1),(1240,'006140','H021','Predosa',7,1),(1241,'006141','H104','Quargnento',7,1),(1242,'006142','H121','Quattordio',7,1),(1243,'006143','H272','Ricaldone',7,1),(1244,'006144','H334','Rivalta Bormida',7,1),(1245,'006145','H343','Rivarone',7,1),(1246,'006146','H406','Roccaforte Ligure',7,1),(1247,'006147','H414','Rocca Grimalda',7,1),(1248,'006148','H465','Rocchetta Ligure',7,1),(1249,'006149','H569','Rosignano Monferrato',7,1),(1250,'006150','H677','Sala Monferrato',7,1),(1251,'006151','H694','Sale',7,1),(1252,'006152','H810','San Cristoforo',7,1),(1253,'006153','H878','San Giorgio Monferrato',7,1),(1254,'006154','I144','San Salvatore Monferrato',7,1),(1255,'006155','I150','San Sebastiano Curone',7,1),(1256,'006156','I190','Sant\'Agata Fossili',7,1),(1257,'006157','I429','Sardigliano',7,1),(1258,'006158','I432','Sarezzano',7,1),(1259,'006159','I645','Serralunga di Crea',7,1),(1260,'006160','I657','Serravalle Scrivia',7,1),(1261,'006161','I711','Sezzadio',7,1),(1262,'006162','I738','Silvano d\'Orba',7,1),(1263,'006163','I798','Solero',7,1),(1264,'006164','I808','Solonghello',7,1),(1265,'006165','I901','Spigno Monferrato',7,1),(1266,'006166','I911','Spineto Scrivia',7,1),(1267,'006167','I941','Stazzano',7,1),(1268,'006168','I977','Strevi',7,1),(1269,'006169','L027','Tagliolo Monferrato',7,1),(1270,'006170','L059','Tassarolo',7,1),(1271,'006171','L139','Terruggia',7,1),(1272,'006172','L143','Terzo',7,1),(1273,'006173','L165','Ticineto',7,1),(1274,'006174','L304','Tortona',7,1),(1275,'006175','L403','Treville',7,1),(1276,'006176','L432','Trisobbio',7,1),(1277,'006177','L570','Valenza',7,1),(1278,'006178','L633','Valmacca',7,1),(1279,'006179','L881','Vignale Monferrato',7,1),(1280,'006180','L887','Vignole Borbera',7,1),(1281,'006181','L904','Viguzzolo',7,1),(1282,'006182','L931','Villadeati',7,1),(1283,'006183','L963','Villalvernia',7,1),(1284,'006184','L970','Villamiroglio',7,1),(1285,'006185','L972','Villanova Monferrato',7,1),(1286,'006186','M009','Villaromagnano',7,1),(1287,'006187','M077','Visone',7,1),(1288,'006188','M120','Volpedo',7,1),(1289,'006189','M121','Volpeglino',7,1),(1290,'006190','M123','Voltaggio',7,1),(1291,'007001','A205','Allein',8,1),(1292,'007002','A305','Antey-Saint-Andrè',8,1),(1293,'007003','A326','Aosta',8,1),(1294,'007004','A424','Arnad',8,1),(1295,'007005','A452','Arvier',8,1),(1296,'007006','A521','Avise',8,1),(1297,'007007','A094','Ayas',8,1),(1298,'007008','A108','Aymavilles',8,1),(1299,'007009','A643','Bard',8,1),(1300,'007010','A877','Bionaz',8,1),(1301,'007011','B192','Brissogne',8,1),(1302,'007012','B230','Brusson',8,1),(1303,'007013','C593','Challand-Saint-Anselme',8,1),(1304,'007014','C594','Challand-Saint-Victor',8,1),(1305,'007015','C595','Chambave',8,1),(1306,'007016','B491','Chamois',8,1),(1307,'007017','C596','Champdepraz',8,1),(1308,'007018','B540','Champorcher',8,1),(1309,'007019','C598','Charvensod',8,1),(1310,'007020','C294','Chatillon',8,1),(1311,'007021','C821','Cogne',8,1),(1312,'007022','D012','Courmayeur',8,1),(1313,'007023','D338','Donnas',8,1),(1314,'007024','D356','Doues',8,1),(1315,'007025','D402','Emarèse',8,1),(1316,'007026','D444','Etroubles',8,1),(1317,'007027','D537','Fénis',8,1),(1318,'007028','D666','Fontainemore',8,1),(1319,'007029','D839','Gaby',8,1),(1320,'007030','E029','Gignod',8,1),(1321,'007031','E165','Gressan',8,1),(1322,'007032','E167','Gressoney-La-Trinitè',8,1),(1323,'007033','E168','Gressoney-Saint-Jean',8,1),(1324,'007034','E273','Hone',8,1),(1325,'007035','E306','Introd',8,1),(1326,'007036','E369','Issime',8,1),(1327,'007037','E371','Issogne',8,1),(1328,'007038','E391','Jovencan',8,1),(1329,'007039','A308','La Magdeleine',8,1),(1330,'007040','E458','La Salle',8,1),(1331,'007041','E470','La Thuile',8,1),(1332,'007042','E587','Lillianes',8,1),(1333,'007043','F367','Montjovet',8,1),(1334,'007044','F726','Morgex',8,1),(1335,'007045','F987','Nus',8,1),(1336,'007046','G045','Ollomont',8,1),(1337,'007047','G012','Oyace',8,1),(1338,'007048','G459','Perloz',8,1),(1339,'007049','G794','Pollein',8,1),(1340,'007050','G545','Pontboset',8,1),(1341,'007051','G860','Pontey',8,1),(1342,'007052','G854','Pont-Saint-Martin',8,1),(1343,'007053','H042','Prè-Saint-Didier',8,1),(1344,'007054','H110','Quart',8,1),(1345,'007055','H262','Rhemes-Notre-Dame',8,1),(1346,'007056','H263','Rhemes-Saint-Georges',8,1),(1347,'007057','H497','Roisan',8,1),(1348,'007058','H669','Saint-Christophe',8,1),(1349,'007059','H670','Saint-Denis',8,1),(1350,'007060','H671','Saint-Marcel',8,1),(1351,'007061','H672','Saint-Nicolas',8,1),(1352,'007062','H673','Saint-Oyen',8,1),(1353,'007063','H674','Saint-Pierre',8,1),(1354,'007064','H675','Saint-Rhémy-en-Bosses',8,1),(1355,'007065','H676','Saint-Vincent',8,1),(1356,'007066','I442','Sarre',8,1),(1357,'007067','L217','Torgnon',8,1),(1358,'007068','L582','Valgrisenche',8,1),(1359,'007069','L643','Valpelline',8,1),(1360,'007070','L647','Valsavarenche',8,1),(1361,'007071','L654','Valtournenche',8,1),(1362,'007072','L783','Verrayes',8,1),(1363,'007073','C282','Verrès',8,1),(1364,'007074','L981','Villeneuve',8,1),(1365,'008001','A111','Airole',9,1),(1366,'008002','A338','Apricale',9,1),(1367,'008003','A344','Aquila d\'Arroscia',9,1),(1368,'008004','A418','Armo',9,1),(1369,'008005','A499','Aurigo',9,1),(1370,'008006','A536','Badalucco',9,1),(1371,'008007','A581','Bajardo',9,1),(1372,'008008','A984','Bordighera',9,1),(1373,'008009','A993','Borghetto d\'Arroscia',9,1),(1374,'008010','B020','Borgomaro',9,1),(1375,'008011','B559','Camporosso',9,1),(1376,'008012','B734','Caravonica',9,1),(1377,'008013','B814','Carpasio',9,1),(1378,'008014','C143','Castellaro',9,1),(1379,'008015','C110','Castel Vittorio',9,1),(1380,'008016','C511','Ceriana',9,1),(1381,'008017','C559','Cervo',9,1),(1382,'008018','C578','Cesio',9,1),(1383,'008019','C657','Chiusanico',9,1),(1384,'008020','C660','Chiusavecchia',9,1),(1385,'008021','C718','Cipressa',9,1),(1386,'008022','C755','Civezza',9,1),(1387,'008023','D087','Cosio d\'Arroscia',9,1),(1388,'008024','D114','Costarainera',9,1),(1389,'008025','D293','Diano Arentino',9,1),(1390,'008026','D296','Diano Castello',9,1),(1391,'008027','D297','Diano Marina',9,1),(1392,'008028','D298','Diano San Pietro',9,1),(1393,'008029','D318','Dolceacqua',9,1),(1394,'008030','D319','Dolcedo',9,1),(1395,'008031','E290','Imperia',9,1),(1396,'008032','E346','Isolabona',9,1),(1397,'008033','E719','Lucinasco',9,1),(1398,'008034','F123','Mendatica',9,1),(1399,'008035','F290','Molini di Triora',9,1),(1400,'008036','F406','Montalto Ligure',9,1),(1401,'008037','F528','Montegrosso Pian Latte',9,1),(1402,'008038','G041','Olivetta San Michele',9,1),(1403,'008039','G164','Ospedaletti',9,1),(1404,'008040','G454','Perinaldo',9,1),(1405,'008041','G607','Pietrabruna',9,1),(1406,'008042','G632','Pieve di Teco',9,1),(1407,'008043','G660','Pigna',9,1),(1408,'008044','G814','Pompeiana',9,1),(1409,'008045','G840','Pontedassio',9,1),(1410,'008046','G890','Pornassio',9,1),(1411,'008047','H027','Prelà',9,1),(1412,'008048','H180','Ranzo',9,1),(1413,'008049','H257','Rezzo',9,1),(1414,'008050','H328','Riva Ligure',9,1),(1415,'008051','H460','Rocchetta Nervina',9,1),(1416,'008052','H763','San Bartolomeo al Mare',9,1),(1417,'008053','H780','San Biagio della Cima',9,1),(1418,'008054','H957','San Lorenzo al Mare',9,1),(1419,'008055','I138','Sanremo',9,1),(1420,'008056','I365','Santo Stefano al Mare',9,1),(1421,'008057','I556','Seborga',9,1),(1422,'008058','I796','Soldano',9,1),(1423,'008059','L024','Taggia',9,1),(1424,'008060','L146','Terzorio',9,1),(1425,'008061','L430','Triora',9,1),(1426,'008062','L596','Vallebona',9,1),(1427,'008063','L599','Vallecrosia',9,1),(1428,'008064','L693','Vasia',9,1),(1429,'008065','L741','Ventimiglia',9,1),(1430,'008066','L809','Vessalico',9,1),(1431,'008067','L943','Villa Faraldi',9,1),(1432,'009001','A122','Alassio',10,1),(1433,'009002','A145','Albenga',10,1),(1434,'009003','A165','Albissola Marina',10,1),(1435,'009004','A166','Albisola Superiore',10,1),(1436,'009005','A226','Altare',10,1),(1437,'009006','A278','Andora',10,1),(1438,'009007','A422','Arnasco',10,1),(1439,'009008','A593','Balestrino',10,1),(1440,'009009','A647','Bardineto',10,1),(1441,'009010','A796','Bergeggi',10,1),(1442,'009011','A931','Boissano',10,1),(1443,'009012','A999','Borghetto Santo Spirito',10,1),(1444,'009013','B005','Borgio Verezzi',10,1),(1445,'009014','B048','Bormida',10,1),(1446,'009015','B369','Cairo Montenotte',10,1),(1447,'009016','B409','Calice Ligure',10,1),(1448,'009017','B416','Calizzano',10,1),(1449,'009018','B748','Carcare',10,1),(1450,'009019','B927','Casanova Lerrone',10,1),(1451,'009020','C063','Castelbianco',10,1),(1452,'009021','C276','Castelvecchio di Rocca Barbena',10,1),(1453,'009022','C443','Celle Ligure',10,1),(1454,'009023','C463','Cengio',10,1),(1455,'009024','C510','Ceriale',10,1),(1456,'009025','C729','Cisano sul Neva',10,1),(1457,'009026','D095','Cosseria',10,1),(1458,'009027','D264','Dego',10,1),(1459,'009028','D424','Erli',10,1),(1460,'009029','D600','Finale Ligure',10,1),(1461,'009030','D927','Garlenda',10,1),(1462,'009031','E064','Giustenice',10,1),(1463,'009032','E066','Giusvalla',10,1),(1464,'009033','E414','Laigueglia',10,1),(1465,'009034','E632','Loano',10,1),(1466,'009035','E816','Magliolo',10,1),(1467,'009036','E860','Mallare',10,1),(1468,'009037','F046','Massimino',10,1),(1469,'009038','F213','Millesimo',10,1),(1470,'009039','F226','Mioglia',10,1),(1471,'009040','F813','Murialdo',10,1),(1472,'009041','F847','Nasino',10,1),(1473,'009042','F926','Noli',10,1),(1474,'009043','G076','Onzo',10,1),(1475,'009044','D522','Orco Feglino',10,1),(1476,'009045','G144','Ortovero',10,1),(1477,'009046','G155','Osiglia',10,1),(1478,'009047','G281','Pallare',10,1),(1479,'009048','G542','Piana Crixia',10,1),(1480,'009049','G605','Pietra Ligure',10,1),(1481,'009050','G741','Plodio',10,1),(1482,'009051','G866','Pontinvrea',10,1),(1483,'009052','H126','Quiliano',10,1),(1484,'009053','H266','Rialto',10,1),(1485,'009054','H452','Roccavignale',10,1),(1486,'009055','I453','Sassello',10,1),(1487,'009056','I480','Savona',10,1),(1488,'009057','I926','Spotorno',10,1),(1489,'009058','I946','Stella',10,1),(1490,'009059','I947','Stellanello',10,1),(1491,'009060','L152','Testico',10,1),(1492,'009061','L190','Toirano',10,1),(1493,'009062','L315','Tovo San Giacomo',10,1),(1494,'009063','L499','Urbe',10,1),(1495,'009064','L528','Vado Ligure',10,1),(1496,'009065','L675','Varazze',10,1),(1497,'009066','L730','Vendone',10,1),(1498,'009067','L823','Vezzi Portio',10,1),(1499,'009068','L975','Villanova d\'Albenga',10,1),(1500,'009069','M197','Zuccarello',10,1),(1501,'010001','A388','Arenzano',11,1),(1502,'010002','A506','Avegno',11,1),(1503,'010003','A658','Bargagli',11,1),(1504,'010004','A922','Bogliasco',11,1),(1505,'010005','B067','Borzonasca',11,1),(1506,'010006','B282','Busalla',11,1),(1507,'010007','B490','Camogli',11,1),(1508,'010008','B538','Campo Ligure',11,1),(1509,'010009','B551','Campomorone',11,1),(1510,'010010','B726','Carasco',11,1),(1511,'010011','B939','Casarza Ligure',11,1),(1512,'010012','B956','Casella',11,1),(1513,'010013','C302','Castiglione Chiavarese',11,1),(1514,'010014','C481','Ceranesi',11,1),(1515,'010015','C621','Chiavari',11,1),(1516,'010016','C673','Cicagna',11,1),(1517,'010017','C823','Cogoleto',11,1),(1518,'010018','C826','Cogorno',11,1),(1519,'010019','C995','Coreglia Ligure',11,1),(1520,'010020','D175','Crocefieschi',11,1),(1521,'010021','D255','Davagna',11,1),(1522,'010022','D509','Fascia',11,1),(1523,'010023','D512','Favale di Malvaro',11,1),(1524,'010024','D677','Fontanigorda',11,1),(1525,'010025','D969','Genova',11,1),(1526,'010026','E109','Gorreto',11,1),(1527,'010027','E341','Isola del Cantone',11,1),(1528,'010028','E488','Lavagna',11,1),(1529,'010029','E519','Leivi',11,1),(1530,'010030','E695','Lorsica',11,1),(1531,'010031','E737','Lumarzo',11,1),(1532,'010032','F020','Masone',11,1),(1533,'010033','F098','Mele',11,1),(1534,'010034','F173','Mezzanego',11,1),(1535,'010035','F202','Mignanego',11,1),(1536,'010036','F256','Moconesi',11,1),(1537,'010037','F354','Moneglia',11,1),(1538,'010038','F445','Montebruno',11,1),(1539,'010039','F682','Montoggio',11,1),(1540,'010040','F858','Ne',11,1),(1541,'010041','F862','Neirone',11,1),(1542,'010042','G093','Orero',11,1),(1543,'010043','G646','Pieve Ligure',11,1),(1544,'010044','G913','Portofino',11,1),(1545,'010045','H073','Propata',11,1),(1546,'010046','H183','Rapallo',11,1),(1547,'010047','H212','Recco',11,1),(1548,'010048','H258','Rezzoaglio',11,1),(1549,'010049','H536','Ronco Scrivia',11,1),(1550,'010050','H546','Rondanina',11,1),(1551,'010051','H581','Rossiglione',11,1),(1552,'010052','H599','Rovegno',11,1),(1553,'010053','H802','San Colombano Certenoli',11,1),(1554,'010054','I225','Santa Margherita Ligure',11,1),(1555,'010055','I346','Sant\'Olcese',11,1),(1556,'010056','I368','Santo Stefano d\'Aveto',11,1),(1557,'010057','I475','Savignone',11,1),(1558,'010058','I640','Serra Riccò',11,1),(1559,'010059','I693','Sestri Levante',11,1),(1560,'010060','I852','Sori',11,1),(1561,'010061','L167','Tiglieto',11,1),(1562,'010062','L298','Torriglia',11,1),(1563,'010063','L416','Tribogna',11,1),(1564,'010064','L507','Uscio',11,1),(1565,'010065','L546','Valbrevenna',11,1),(1566,'010066','M105','Vobbia',11,1),(1567,'010067','M182','Zoagli',11,1),(1568,'011001','A261','Ameglia',12,1),(1569,'011002','A373','Arcola',12,1),(1570,'011003','A836','Beverino',12,1),(1571,'011004','A932','Bolano',12,1),(1572,'011005','A961','Bonassola',12,1),(1573,'011006','A992','Borghetto di Vara',12,1),(1574,'011007','B214','Brugnato',12,1),(1575,'011008','B410','Calice al Cornoviglio',12,1),(1576,'011009','B838','Carro',12,1),(1577,'011010','B839','Carrodano',12,1),(1578,'011011','C240','Castelnuovo Magra',12,1),(1579,'011012','D265','Deiva Marina',12,1),(1580,'011013','D655','Follo',12,1),(1581,'011014','D758','Framura',12,1),(1582,'011015','E463','La Spezia',12,1),(1583,'011016','E542','Lerici',12,1),(1584,'011017','E560','Levanto',12,1),(1585,'011018','E842','Maissana',12,1),(1586,'011019','F609','Monterosso al Mare',12,1),(1587,'011020','G143','Ortonovo',12,1),(1588,'011021','G664','Pignone',12,1),(1589,'011022','G925','Portovenere',12,1),(1590,'011023','H275','Riccò del Golfo di Spezia',12,1),(1591,'011024','H304','Riomaggiore',12,1),(1592,'011025','H461','Rocchetta di Vara',12,1),(1593,'011026','I363','Santo Stefano di Magra',12,1),(1594,'011027','I449','Sarzana',12,1),(1595,'011028','E070','Sesta Godano',12,1),(1596,'011029','L681','Varese Ligure',12,1),(1597,'011030','L774','Vernazza',12,1),(1598,'011031','L819','Vezzano Ligure',12,1),(1599,'011032','M177','Zignago',12,1),(1600,'012001','A085','Agra',13,1),(1601,'012002','A167','Albizzate',13,1),(1602,'012003','A290','Angera',13,1),(1603,'012004','A371','Arcisate',13,1),(1604,'012005','A441','Arsago Seprio',13,1),(1605,'012006','A531','Azzate',13,1),(1606,'012007','A532','Azzio',13,1),(1607,'012008','A619','Barasso',13,1),(1608,'012009','A645','Bardello',13,1),(1609,'012010','A728','Bedero Valcuvia',13,1),(1610,'012011','A819','Besano',13,1),(1611,'012012','A825','Besnate',13,1),(1612,'012013','A826','Besozzo',13,1),(1613,'012014','A845','Biandronno',13,1),(1614,'012015','A891','Bisuschio',13,1),(1615,'012016','A918','Bodio Lomnago',13,1),(1616,'012017','B126','Brebbia',13,1),(1617,'012018','B131','Bregano',13,1),(1618,'012019','B150','Brenta',13,1),(1619,'012020','B166','Brezzo di Bedero',13,1),(1620,'012021','B182','Brinzio',13,1),(1621,'012022','B191','Brissago-Valtravaglia',13,1),(1622,'012023','B219','Brunello',13,1),(1623,'012024','B228','Brusimpiano',13,1),(1624,'012025','B258','Buguggiate',13,1),(1625,'012026','B300','Busto Arsizio',13,1),(1626,'012027','B326','Cadegliano-Viconago',13,1),(1627,'012028','B347','Cadrezzate',13,1),(1628,'012029','B368','Cairate',13,1),(1629,'012030','B634','Cantello',13,1),(1630,'012031','B732','Caravate',13,1),(1631,'012032','B754','Cardano al Campo',13,1),(1632,'012033','B796','Carnago',13,1),(1633,'012034','B805','Caronno Pertusella',13,1),(1634,'012035','B807','Caronno Varesino',13,1),(1635,'012036','B875','Casale Litta',13,1),(1636,'012037','B921','Casalzuigno',13,1),(1637,'012038','B949','Casciago',13,1),(1638,'012039','B987','Casorate Sempione',13,1),(1639,'012040','C004','Cassano Magnago',13,1),(1640,'012041','B999','Cassano Valcuvia',13,1),(1641,'012042','C139','Castellanza',13,1),(1642,'012043','B312','Castello Cabiaglio',13,1),(1643,'012044','C273','Castelseprio',13,1),(1644,'012045','C181','Castelveccana',13,1),(1645,'012046','C300','Castiglione Olona',13,1),(1646,'012047','C343','Castronno',13,1),(1647,'012048','C382','Cavaria con Premezzo',13,1),(1648,'012049','C409','Cazzago Brabbia',13,1),(1649,'012050','C732','Cislago',13,1),(1650,'012051','C751','Cittiglio',13,1),(1651,'012052','C796','Clivio',13,1),(1652,'012053','C810','Cocquio-Trevisago',13,1),(1653,'012054','C911','Comabbio',13,1),(1654,'012055','C922','Comerio',13,1),(1655,'012056','D144','Cremenaga',13,1),(1656,'012057','D185','Crosio della Valle',13,1),(1657,'012058','D192','Cuasso al Monte',13,1),(1658,'012059','D199','Cugliate-Fabiasco',13,1),(1659,'012060','D204','Cunardo',13,1),(1660,'012061','D217','Curiglia con Monteviasco',13,1),(1661,'012062','D238','Cuveglio',13,1),(1662,'012063','D239','Cuvio',13,1),(1663,'012064','D256','Daverio',13,1),(1664,'012065','D384','Dumenza',13,1),(1665,'012066','D385','Duno',13,1),(1666,'012067','D467','Fagnano Olona',13,1),(1667,'012068','D543','Ferno',13,1),(1668,'012069','D551','Ferrera di Varese',13,1),(1669,'012070','D869','Gallarate',13,1),(1670,'012071','D871','Galliate Lombardo',13,1),(1671,'012072','D946','Gavirate',13,1),(1672,'012073','D951','Gazzada Schianno',13,1),(1673,'012074','D963','Gemonio',13,1),(1674,'012075','D981','Gerenzano',13,1),(1675,'012076','D987','Germignaga',13,1),(1676,'012077','E079','Golasecca',13,1),(1677,'012078','E101','Gorla Maggiore',13,1),(1678,'012079','E102','Gorla Minore',13,1),(1679,'012080','E104','Gornate-Olona',13,1),(1680,'012081','E144','Grantola',13,1),(1681,'012082','E292','Inarzo',13,1),(1682,'012083','E299','Induno Olona',13,1),(1683,'012084','E367','Ispra',13,1),(1684,'012085','E386','Jerago con Orago',13,1),(1685,'012086','E494','Lavena Ponte Tresa',13,1),(1686,'012087','E496','Laveno-Mombello',13,1),(1687,'012088','E510','Leggiuno',13,1),(1688,'012089','E665','Lonate Ceppino',13,1),(1689,'012090','E666','Lonate Pozzolo',13,1),(1690,'012091','E707','Lozza',13,1),(1691,'012092','E734','Luino',13,1),(1692,'012093','E769','Luvinate',13,1),(1693,'012094','E775','Maccagno',13,1),(1694,'012095','E856','Malgesso',13,1),(1695,'012096','E863','Malnate',13,1),(1696,'012097','E929','Marchirolo',13,1),(1697,'012098','E965','Marnate',13,1),(1698,'012099','F002','Marzio',13,1),(1699,'012100','F007','Masciago Primo',13,1),(1700,'012101','F134','Mercallo',13,1),(1701,'012102','F154','Mesenzana',13,1),(1702,'012103','F526','Montegrino Valtravaglia',13,1),(1703,'012104','F703','Monvalle',13,1),(1704,'012105','F711','Morazzone',13,1),(1705,'012106','F736','Mornago',13,1),(1706,'012107','G008','Oggiona con Santo Stefano',13,1),(1707,'012108','G028','Olgiate Olona',13,1),(1708,'012109','G103','Origgio',13,1),(1709,'012110','G105','Orino',13,1),(1710,'012111','E529','Osmate',13,1),(1711,'012113','G906','Porto Ceresio',13,1),(1712,'012114','G907','Porto Valtravaglia',13,1),(1713,'012115','H173','Rancio Valcuvia',13,1),(1714,'012116','H174','Ranco',13,1),(1715,'012117','H723','Saltrio',13,1),(1716,'012118','H736','Samarate',13,1),(1717,'012119','I441','Saronno',13,1),(1718,'012120','I688','Sesto Calende',13,1),(1719,'012121','I793','Solbiate Arno',13,1),(1720,'012122','I794','Solbiate Olona',13,1),(1721,'012123','I819','Somma Lombardo',13,1),(1722,'012124','L003','Sumirago',13,1),(1723,'012125','L032','Taino',13,1),(1724,'012126','L115','Ternate',13,1),(1725,'012127','L319','Tradate',13,1),(1726,'012128','L342','Travedona-Monate',13,1),(1727,'012129','A705','Tronzano Lago Maggiore',13,1),(1728,'012130','L480','Uboldo',13,1),(1729,'012131','L577','Valganna',13,1),(1730,'012132','L671','Varano Borghi',13,1),(1731,'012133','L682','Varese',13,1),(1732,'012134','L703','Vedano Olona',13,1),(1733,'012135','L705','Veddasca',13,1),(1734,'012136','L733','Venegono Inferiore',13,1),(1735,'012137','L734','Venegono Superiore',13,1),(1736,'012138','L765','Vergiate',13,1),(1737,'012139','L876','Viggiù',13,1),(1738,'012140','M101','Vizzola Ticino',13,1),(1739,'012141','H872','Sangiano',13,1),(1740,'013003','A143','Albavilla',14,1),(1741,'013004','A153','Albese con Cassano',14,1),(1742,'013005','A164','Albiolo',14,1),(1743,'013006','A224','Alserio',14,1),(1744,'013007','A249','Alzate Brianza',14,1),(1745,'013009','A319','Anzano del Parco',14,1),(1746,'013010','A333','Appiano Gentile',14,1),(1747,'013011','A391','Argegno',14,1),(1748,'013012','A430','Arosio',14,1),(1749,'013013','A476','Asso',14,1),(1750,'013015','A670','Barni',14,1),(1751,'013019','A744','Bellagio',14,1),(1752,'013021','A778','Bene Lario',14,1),(1753,'013022','A791','Beregazzo con Figliaro',14,1),(1754,'013023','A870','Binago',14,1),(1755,'013024','A898','Bizzarone',14,1),(1756,'013025','A904','Blessagno',14,1),(1757,'013026','A905','Blevio',14,1),(1758,'013028','B134','Bregnano',14,1),(1759,'013029','B144','Brenna',14,1),(1760,'013030','B172','Brienno',14,1),(1761,'013032','B218','Brunate',14,1),(1762,'013034','B262','Bulgarograsso',14,1),(1763,'013035','B313','Cabiate',14,1),(1764,'013036','B346','Cadorago',14,1),(1765,'013037','B355','Caglio',14,1),(1766,'013038','B359','Cagno',14,1),(1767,'013040','B513','Campione d\'Italia',14,1),(1768,'013041','B639','Cantù',14,1),(1769,'013042','B641','Canzo',14,1),(1770,'013043','B653','Capiago Intimiano',14,1),(1771,'013044','B730','Carate Urio',14,1),(1772,'013045','B742','Carbonate',14,1),(1773,'013046','B778','Carimate',14,1),(1774,'013047','B785','Carlazzo',14,1),(1775,'013048','B851','Carugo',14,1),(1776,'013050','B942','Casasco d\'Intelvi',14,1),(1777,'013052','B974','Caslino d\'Erba',14,1),(1778,'013053','B977','Casnate con Bernate',14,1),(1779,'013055','C020','Cassina Rizzardi',14,1),(1780,'013058','C206','Castelmarte',14,1),(1781,'013059','C220','Castelnuovo Bozzente',14,1),(1782,'013060','C299','Castiglione d\'Intelvi',14,1),(1783,'013061','C374','Cavallasca',14,1),(1784,'013062','C381','Cavargna',14,1),(1785,'013063','C482','Cerano d\'Intelvi',14,1),(1786,'013064','C516','Cermenate',14,1),(1787,'013065','C520','Cernobbio',14,1),(1788,'013068','C724','Cirimido',14,1),(1789,'013070','C754','Civenna',14,1),(1790,'013071','C787','Claino con Osteno',14,1),(1791,'013074','C902','Colonno',14,1),(1792,'013075','C933','Como',14,1),(1793,'013076','C965','Consiglio di Rumo',14,1),(1794,'013077','D041','Corrido',14,1),(1795,'013083','D147','Cremia',14,1),(1796,'013084','D196','Cucciago',14,1),(1797,'013085','D232','Cusino',14,1),(1798,'013087','D310','Dizzasco',14,1),(1799,'013089','D329','Domaso',14,1),(1800,'013090','D341','Dongo',14,1),(1801,'013092','D355','Dosso del Liro',14,1),(1802,'013093','D369','Drezzo',14,1),(1803,'013095','D416','Erba',14,1),(1804,'013097','D445','Eupilio',14,1),(1805,'013098','D462','Faggeto Lario',14,1),(1806,'013099','D482','Faloppio',14,1),(1807,'013100','D531','Fenegrò',14,1),(1808,'013101','D579','Figino Serenza',14,1),(1809,'013102','D605','Fino Mornasco',14,1),(1810,'013106','D930','Garzeno',14,1),(1811,'013107','D974','Gera Lario',14,1),(1812,'013108','D986','Germasino',14,1),(1813,'013109','E051','Gironico',14,1),(1814,'013110','E139','Grandate',14,1),(1815,'013111','E141','Grandola ed Uniti',14,1),(1816,'013112','E151','Gravedona',14,1),(1817,'013113','E172','Griante',14,1),(1818,'013114','E235','Guanzate',14,1),(1819,'013118','E309','Inverigo',14,1),(1820,'013119','E405','Laglio',14,1),(1821,'013120','E416','Laino',14,1),(1822,'013121','E428','Lambrugo',14,1),(1823,'013122','E444','Lanzo d\'Intelvi',14,1),(1824,'013123','E462','Lasnigo',14,1),(1825,'013125','E525','Lenno',14,1),(1826,'013126','E569','Lezzeno',14,1),(1827,'013128','E593','Limido Comasco',14,1),(1828,'013129','E607','Lipomo',14,1),(1829,'013130','E623','Livo',14,1),(1830,'013131','E638','Locate Varesino',14,1),(1831,'013133','E659','Lomazzo',14,1),(1832,'013134','E679','Longone al Segrino',14,1),(1833,'013135','E735','Luisago',14,1),(1834,'013136','E749','Lurago d\'Erba',14,1),(1835,'013137','E750','Lurago Marinone',14,1),(1836,'013138','E753','Lurate Caccivio',14,1),(1837,'013139','E830','Magreglio',14,1),(1838,'013143','E951','Mariano Comense',14,1),(1839,'013144','F017','Maslianico',14,1),(1840,'013145','F120','Menaggio',14,1),(1841,'013147','F151','Merone',14,1),(1842,'013148','F181','Mezzegra',14,1),(1843,'013152','F305','Moltrasio',14,1),(1844,'013153','F372','Monguzzo',14,1),(1845,'013154','F427','Montano Lucino',14,1),(1846,'013155','F564','Montemezzo',14,1),(1847,'013157','F688','Montorfano',14,1),(1848,'013159','F788','Mozzate',14,1),(1849,'013160','F828','Musso',14,1),(1850,'013161','F877','Nesso',14,1),(1851,'013163','F958','Novedrate',14,1),(1852,'013165','G025','Olgiate Comasco',14,1),(1853,'013169','G056','Oltrona di San Mamette',14,1),(1854,'013170','G126','Orsenigo',14,1),(1855,'013172','G182','Ossuccio',14,1),(1856,'013175','G329','Parè',14,1),(1857,'013178','G415','Peglio',14,1),(1858,'013179','G427','Pellio Intelvi',14,1),(1859,'013183','G556','Pianello del Lario',14,1),(1860,'013184','G665','Pigra',14,1),(1861,'013185','G737','Plesio',14,1),(1862,'013186','G773','Pognana Lario',14,1),(1863,'013187','G821','Ponna',14,1),(1864,'013188','G847','Ponte Lambro',14,1),(1865,'013189','G889','Porlezza',14,1),(1866,'013192','H074','Proserpio',14,1),(1867,'013193','H094','Pusiano',14,1),(1868,'013194','H171','Ramponio Verna',14,1),(1869,'013195','H255','Rezzago',14,1),(1870,'013197','H478','Rodero',14,1),(1871,'013199','H521','Ronago',14,1),(1872,'013201','H601','Rovellasca',14,1),(1873,'013202','H602','Rovello Porro',14,1),(1874,'013203','H679','Sala Comacina',14,1),(1875,'013204','H760','San Bartolomeo Val Cavargna',14,1),(1876,'013205','H830','San Fedele Intelvi',14,1),(1877,'013206','H840','San Fermo della Battaglia',14,1),(1878,'013207','I051','San Nazzaro Val Cavargna',14,1),(1879,'013211','I529','Schignano',14,1),(1880,'013212','I611','Senna Comasco',14,1),(1881,'013215','I792','Solbiate',14,1),(1882,'013216','I856','Sorico',14,1),(1883,'013217','I860','Sormano',14,1),(1884,'013218','I943','Stazzona',14,1),(1885,'013222','L071','Tavernerio',14,1),(1886,'013223','L228','Torno',14,1),(1887,'013225','L371','Tremezzo',14,1),(1888,'013226','L413','Trezzone',14,1),(1889,'013227','L470','Turate',14,1),(1890,'013228','L487','Uggiate-Trevano',14,1),(1891,'013229','L547','Valbrona',14,1),(1892,'013232','L640','Valmorea',14,1),(1893,'013233','H259','Val Rezzo',14,1),(1894,'013234','C936','Valsolda',14,1),(1895,'013236','L715','Veleso',14,1),(1896,'013238','L737','Veniano',14,1),(1897,'013239','L748','Vercana',14,1),(1898,'013242','L792','Vertemate con Minoprio',14,1),(1899,'013245','L956','Villa Guardia',14,1),(1900,'013246','M156','Zelbio',14,1),(1901,'013248','I162','San Siro',14,1),(1902,'014001','A135','Albaredo per San Marco',15,1),(1903,'014002','A172','Albosaggia',15,1),(1904,'014003','A273','Andalo Valtellino',15,1),(1905,'014004','A337','Aprica',15,1),(1906,'014005','A382','Ardenno',15,1),(1907,'014006','A777','Bema',15,1),(1908,'014007','A787','Berbenno di Valtellina',15,1),(1909,'014008','A848','Bianzone',15,1),(1910,'014009','B049','Bormio',15,1),(1911,'014010','B255','Buglio in Monte',15,1),(1912,'014011','B366','Caiolo',15,1),(1913,'014012','B530','Campodolcino',15,1),(1914,'014013','B993','Caspoggio',15,1),(1915,'014014','C186','Castello dell\'Acqua',15,1),(1916,'014015','C325','Castione Andevenno',15,1),(1917,'014016','C418','Cedrasco',15,1),(1918,'014017','C493','Cercino',15,1),(1919,'014018','C623','Chiavenna',15,1),(1920,'014019','C628','Chiesa in Valmalenco',15,1),(1921,'014020','C651','Chiuro',15,1),(1922,'014021','C709','Cino',15,1),(1923,'014022','C785','Civo',15,1),(1924,'014023','C903','Colorina',15,1),(1925,'014024','D088','Cosio Valtellino',15,1),(1926,'014025','D258','Dazio',15,1),(1927,'014026','D266','Delebio',15,1),(1928,'014027','D377','Dubino',15,1),(1929,'014028','D456','Faedo Valtellino',15,1),(1930,'014029','D694','Forcola',15,1),(1931,'014030','D830','Fusine',15,1),(1932,'014031','D990','Gerola Alta',15,1),(1933,'014032','E090','Gordona',15,1),(1934,'014033','E200','Grosio',15,1),(1935,'014034','E201','Grosotto',15,1),(1936,'014035','E342','Madesimo',15,1),(1937,'014036','E443','Lanzada',15,1),(1938,'014037','E621','Livigno',15,1),(1939,'014038','E705','Lovero',15,1),(1940,'014039','E896','Mantello',15,1),(1941,'014040','F070','Mazzo di Valtellina',15,1),(1942,'014041','F115','Mello',15,1),(1943,'014042','F121','Menarola',15,1),(1944,'014043','F153','Mese',15,1),(1945,'014044','F393','Montagna in Valtellina',15,1),(1946,'014045','F712','Morbegno',15,1),(1947,'014046','F956','Novate Mezzola',15,1),(1948,'014047','G410','Pedesina',15,1),(1949,'014048','G572','Piantedo',15,1),(1950,'014049','G576','Piateda',15,1),(1951,'014050','G718','Piuro',15,1),(1952,'014051','G431','Poggiridenti',15,1),(1953,'014052','G829','Ponte in Valtellina',15,1),(1954,'014053','G937','Postalesio',15,1),(1955,'014054','G993','Prata Camportaccio',15,1),(1956,'014055','H192','Rasura',15,1),(1957,'014056','H493','Rogolo',15,1),(1958,'014057','H752','Samolaco',15,1),(1959,'014058','H868','San Giacomo Filippo',15,1),(1960,'014059','I636','Sernio',15,1),(1961,'014060','I828','Sondalo',15,1),(1962,'014061','I829','Sondrio',15,1),(1963,'014062','I928','Spriana',15,1),(1964,'014063','L035','Talamona',15,1),(1965,'014064','L056','Tartano',15,1),(1966,'014065','L084','Teglio',15,1),(1967,'014066','L175','Tirano',15,1),(1968,'014067','L244','Torre di Santa Maria',15,1),(1969,'014068','L316','Tovo di Sant\'Agata',15,1),(1970,'014069','L330','Traona',15,1),(1971,'014070','L392','Tresivio',15,1),(1972,'014071','L557','Valdidentro',15,1),(1973,'014072','L563','Valdisotto',15,1),(1974,'014073','L576','Valfurva',15,1),(1975,'014074','L638','Val Masino',15,1),(1976,'014075','L749','Verceia',15,1),(1977,'014076','L799','Vervio',15,1),(1978,'014077','L907','Villa di Chiavenna',15,1),(1979,'014078','L908','Villa di Tirano',15,1),(1980,'015002','A010','Abbiategrasso',16,1),(1981,'015003','A087','Agrate Brianza',16,1),(1982,'015004','A096','Aicurzio',16,1),(1983,'015005','A127','Albairate',16,1),(1984,'015006','A159','Albiate',16,1),(1985,'015007','A375','Arconate',16,1),(1986,'015008','A376','Arcore',16,1),(1987,'015009','A389','Arese',16,1),(1988,'015010','A413','Arluno',16,1),(1989,'015011','A473','Assago',16,1),(1990,'015012','A652','Bareggio',16,1),(1991,'015013','A668','Barlassina',16,1),(1992,'015014','A697','Basiano',16,1),(1993,'015015','A699','Basiglio',16,1),(1994,'015016','A751','Bellinzago Lombardo',16,1),(1995,'015017','A759','Bellusco',16,1),(1996,'015018','A802','Bernareggio',16,1),(1997,'015019','A804','Bernate Ticino',16,1),(1998,'015021','A818','Besana in Brianza',16,1),(1999,'015022','A820','Besate',16,1),(2000,'015023','A849','Biassono',16,1),(2001,'015024','A872','Binasco',16,1),(2002,'015026','A920','Boffalora sopra Ticino',16,1),(2003,'015027','A940','Bollate',16,1),(2004,'015030','B105','Bovisio-Masciago',16,1),(2005,'015032','B162','Bresso',16,1),(2006,'015033','B187','Briosco',16,1),(2007,'015034','B212','Brugherio',16,1),(2008,'015035','B235','Bubbiano',16,1),(2009,'015036','B240','Buccinasco',16,1),(2010,'015037','B272','Burago di Molgora',16,1),(2011,'015038','B286','Buscate',16,1),(2012,'015039','B289','Busnago',16,1),(2013,'015040','B292','Bussero',16,1),(2014,'015041','B301','Busto Garolfo',16,1),(2015,'015042','B448','Calvignasco',16,1),(2016,'015044','B461','Cambiago',16,1),(2017,'015045','B501','Camparada',16,1),(2018,'015046','B593','Canegrate',16,1),(2019,'015047','B671','Caponago',16,1),(2020,'015048','B729','Carate Brianza',16,1),(2021,'015049','B798','Carnate',16,1),(2022,'015050','B820','Carpiano',16,1),(2023,'015051','B850','Carugate',16,1),(2024,'015055','B938','Casarile',16,1),(2025,'015058','B989','Casorezzo',16,1),(2026,'015059','C003','Cassano d\'Adda',16,1),(2027,'015060','C014','Cassina de\' Pecchi',16,1),(2028,'015061','C033','Cassinetta di Lugagnano',16,1),(2029,'015062','C052','Castano Primo',16,1),(2030,'015068','C395','Cavenago di Brianza',16,1),(2031,'015069','C512','Ceriano Laghetto',16,1),(2032,'015070','C523','Cernusco sul Naviglio',16,1),(2033,'015071','C536','Cerro al Lambro',16,1),(2034,'015072','C537','Cerro Maggiore',16,1),(2035,'015074','C565','Cesano Boscone',16,1),(2036,'015075','C566','Cesano Maderno',16,1),(2037,'015076','C569','Cesate',16,1),(2038,'015077','C707','Cinisello Balsamo',16,1),(2039,'015078','C733','Cisliano',16,1),(2040,'015080','C820','Cogliate',16,1),(2041,'015081','C895','Cologno Monzese',16,1),(2042,'015082','C908','Colturano',16,1),(2043,'015084','C952','Concorezzo',16,1),(2044,'015085','C986','Corbetta',16,1),(2045,'015086','D013','Cormano',16,1),(2046,'015087','D018','Cornaredo',16,1),(2047,'015088','D019','Cornate d\'Adda',16,1),(2048,'015092','D038','Correzzana',16,1),(2049,'015093','D045','Corsico',16,1),(2050,'015096','D198','Cuggiono',16,1),(2051,'015097','D229','Cusago',16,1),(2052,'015098','D231','Cusano Milanino',16,1),(2053,'015099','D244','Dairago',16,1),(2054,'015100','D286','Desio',16,1),(2055,'015101','D367','Dresano',16,1),(2056,'015103','D845','Gaggiano',16,1),(2057,'015105','D912','Garbagnate Milanese',16,1),(2058,'015106','D995','Gessate',16,1),(2059,'015107','E063','Giussano',16,1),(2060,'015108','E094','Gorgonzola',16,1),(2061,'015110','E170','Grezzago',16,1),(2062,'015112','E258','Gudo Visconti',16,1),(2063,'015113','E313','Inveruno',16,1),(2064,'015114','E317','Inzago',16,1),(2065,'015115','E395','Lacchiarella',16,1),(2066,'015116','E415','Lainate',16,1),(2067,'015117','E504','Lazzate',16,1),(2068,'015118','E514','Legnano',16,1),(2069,'015119','E530','Lentate sul Seveso',16,1),(2070,'015120','E550','Lesmo',16,1),(2071,'015121','E591','Limbiate',16,1),(2072,'015122','E610','Liscate',16,1),(2073,'015123','E617','Lissone',16,1),(2074,'015125','E639','Locate di Triulzi',16,1),(2075,'015129','E786','Macherio',16,1),(2076,'015130','E801','Magenta',16,1),(2077,'015131','E819','Magnago',16,1),(2078,'015134','E921','Marcallo con Casone',16,1),(2079,'015136','F003','Masate',16,1),(2080,'015138','F078','Meda',16,1),(2081,'015139','F084','Mediglia',16,1),(2082,'015140','F100','Melegnano',16,1),(2083,'015142','F119','Melzo',16,1),(2084,'015144','F155','Mesero',16,1),(2085,'015145','F165','Mezzago',16,1),(2086,'015146','F205','Milano',16,1),(2087,'015147','F247','Misinto',16,1),(2088,'015149','F704','Monza',16,1),(2089,'015150','D033','Morimondo',16,1),(2090,'015151','F783','Motta Visconti',16,1),(2091,'015152','F797','Muggiò',16,1),(2092,'015154','F874','Nerviano',16,1),(2093,'015155','F939','Nosate',16,1),(2094,'015156','F944','Nova Milanese',16,1),(2095,'015157','F955','Novate Milanese',16,1),(2096,'015158','F968','Noviglio',16,1),(2097,'015159','G078','Opera',16,1),(2098,'015161','G116','Ornago',16,1),(2099,'015164','G181','Ossona',16,1),(2100,'015165','G206','Ozzero',16,1),(2101,'015166','G220','Paderno Dugnano',16,1),(2102,'015167','G316','Pantigliate',16,1),(2103,'015168','G324','Parabiago',16,1),(2104,'015169','G385','Paullo',16,1),(2105,'015170','C013','Pero',16,1),(2106,'015171','G488','Peschiera Borromeo',16,1),(2107,'015172','G502','Pessano con Bornago',16,1),(2108,'015173','G634','Pieve Emanuele',16,1),(2109,'015175','G686','Pioltello',16,1),(2110,'015176','G772','Pogliano Milanese',16,1),(2111,'015177','G955','Pozzo d\'Adda',16,1),(2112,'015178','G965','Pozzuolo Martesana',16,1),(2113,'015179','H026','Pregnana Milanese',16,1),(2114,'015180','H233','Renate',16,1),(2115,'015181','H240','Rescaldina',16,1),(2116,'015182','H264','Rho',16,1),(2117,'015183','H371','Robecchetto con Induno',16,1),(2118,'015184','H373','Robecco sul Naviglio',16,1),(2119,'015185','H470','Rodano',16,1),(2120,'015186','H529','Roncello',16,1),(2121,'015187','H537','Ronco Briantino',16,1),(2122,'015188','H560','Rosate',16,1),(2123,'015189','H623','Rozzano',16,1),(2124,'015191','H803','San Colombano al Lambro',16,1),(2125,'015192','H827','San Donato Milanese',16,1),(2126,'015194','H884','San Giorgio su Legnano',16,1),(2127,'015195','H930','San Giuliano Milanese',16,1),(2128,'015200','I361','Santo Stefano Ticino',16,1),(2129,'015201','I409','San Vittore Olona',16,1),(2130,'015202','I415','San Zenone al Lambro',16,1),(2131,'015204','I566','Sedriano',16,1),(2132,'015205','I577','Segrate',16,1),(2133,'015206','I602','Senago',16,1),(2134,'015208','I625','Seregno',16,1),(2135,'015209','I690','Sesto San Giovanni',16,1),(2136,'015210','I696','Settala',16,1),(2137,'015211','I700','Settimo Milanese',16,1),(2138,'015212','I709','Seveso',16,1),(2139,'015213','I786','Solaro',16,1),(2140,'015216','I878','Sovico',16,1),(2141,'015217','I998','Sulbiate',16,1),(2142,'015219','L408','Trezzano Rosa',16,1),(2143,'015220','L409','Trezzano sul Naviglio',16,1),(2144,'015221','L411','Trezzo sull\'Adda',16,1),(2145,'015222','L415','Tribiano',16,1),(2146,'015223','L434','Triuggio',16,1),(2147,'015224','L454','Truccazzano',16,1),(2148,'015226','L471','Turbigo',16,1),(2149,'015227','L511','Usmate Velate',16,1),(2150,'015229','L665','Vanzago',16,1),(2151,'015230','L667','Vaprio d\'Adda',16,1),(2152,'015231','L677','Varedo',16,1),(2153,'015232','L704','Vedano al Lambro',16,1),(2154,'015233','L709','Veduggio con Colzano',16,1),(2155,'015234','L744','Verano Brianza',16,1),(2156,'015235','L768','Vermezzo',16,1),(2157,'015236','L773','Vernate',16,1),(2158,'015237','L883','Vignate',16,1),(2159,'015239','M017','Villasanta',16,1),(2160,'015241','M052','Vimercate',16,1),(2161,'015242','M053','Vimodrone',16,1),(2162,'015243','M091','Vittuone',16,1),(2163,'015244','M102','Vizzolo Predabissi',16,1),(2164,'015246','M160','Zelo Surrigone',16,1),(2165,'015247','M176','Zibido San Giacomo',16,1),(2166,'015248','L928','Villa Cortese',16,1),(2167,'015249','L664','Vanzaghello',16,1),(2168,'015250','A618','Baranzate',16,1),(2169,'016001','A057','Adrara San Martino',17,1),(2170,'016002','A058','Adrara San Rocco',17,1),(2171,'016003','A129','Albano Sant\'Alessandro',17,1),(2172,'016004','A163','Albino',17,1),(2173,'016005','A214','Almè',17,1),(2174,'016006','A216','Almenno San Bartolomeo',17,1),(2175,'016007','A217','Almenno San Salvatore',17,1),(2176,'016008','A246','Alzano Lombardo',17,1),(2177,'016009','A259','Ambivere',17,1),(2178,'016010','A304','Antegnate',17,1),(2179,'016011','A365','Arcene',17,1),(2180,'016012','A383','Ardesio',17,1),(2181,'016013','A440','Arzago d\'Adda',17,1),(2182,'016014','A511','Averara',17,1),(2183,'016015','A517','Aviatico',17,1),(2184,'016016','A528','Azzano San Paolo',17,1),(2185,'016017','A533','Azzone',17,1),(2186,'016018','A557','Bagnatica',17,1),(2187,'016019','A631','Barbata',17,1),(2188,'016020','A664','Bariano',17,1),(2189,'016021','A684','Barzana',17,1),(2190,'016022','A732','Bedulita',17,1),(2191,'016023','A786','Berbenno',17,1),(2192,'016024','A794','Bergamo',17,1),(2193,'016025','A815','Berzo San Fermo',17,1),(2194,'016026','A846','Bianzano',17,1),(2195,'016027','A903','Blello',17,1),(2196,'016028','A937','Bolgare',17,1),(2197,'016029','A950','Boltiere',17,1),(2198,'016030','A963','Bonate Sopra',17,1),(2199,'016031','A962','Bonate Sotto',17,1),(2200,'016032','B010','Borgo di Terzo',17,1),(2201,'016033','B083','Bossico',17,1),(2202,'016034','B088','Bottanuco',17,1),(2203,'016035','B112','Bracca',17,1),(2204,'016036','B123','Branzi',17,1),(2205,'016037','B137','Brembate',17,1),(2206,'016038','B138','Brembate di Sopra',17,1),(2207,'016039','B140','Brembilla',17,1),(2208,'016040','B178','Brignano Gera d\'Adda',17,1),(2209,'016041','B217','Brumano',17,1),(2210,'016042','B223','Brusaporto',17,1),(2211,'016043','B393','Calcinate',17,1),(2212,'016044','B395','Calcio',17,1),(2213,'016046','B434','Calusco d\'Adda',17,1),(2214,'016047','B442','Calvenzano',17,1),(2215,'016048','B471','Camerata Cornello',17,1),(2216,'016049','B618','Canonica d\'Adda',17,1),(2217,'016050','B661','Capizzone',17,1),(2218,'016051','B703','Capriate San Gervasio',17,1),(2219,'016052','B710','Caprino Bergamasco',17,1),(2220,'016053','B731','Caravaggio',17,1),(2221,'016055','B801','Carobbio degli Angeli',17,1),(2222,'016056','B803','Carona',17,1),(2223,'016057','B854','Carvico',17,1),(2224,'016058','B947','Casazza',17,1),(2225,'016059','B971','Casirate d\'Adda',17,1),(2226,'016060','B978','Casnigo',17,1),(2227,'016061','C007','Cassiglio',17,1),(2228,'016062','C079','Castelli Calepio',17,1),(2229,'016063','C255','Castel Rozzone',17,1),(2230,'016064','C324','Castione della Presolana',17,1),(2231,'016065','C337','Castro',17,1),(2232,'016066','C396','Cavernago',17,1),(2233,'016067','C410','Cazzano Sant\'Andrea',17,1),(2234,'016068','C456','Cenate Sopra',17,1),(2235,'016069','C457','Cenate Sotto',17,1),(2236,'016070','C459','Cene',17,1),(2237,'016071','C506','Cerete',17,1),(2238,'016072','C635','Chignolo d\'Isola',17,1),(2239,'016073','C649','Chiuduno',17,1),(2240,'016074','C728','Cisano Bergamasco',17,1),(2241,'016075','C730','Ciserano',17,1),(2242,'016076','C759','Cividate al Piano',17,1),(2243,'016077','C800','Clusone',17,1),(2244,'016078','C835','Colere',17,1),(2245,'016079','C894','Cologno al Serio',17,1),(2246,'016080','C910','Colzate',17,1),(2247,'016081','C937','Comun Nuovo',17,1),(2248,'016082','D015','Corna Imagna',17,1),(2249,'016083','D066','Cortenuova',17,1),(2250,'016084','D110','Costa di Mezzate',17,1),(2251,'016085','D103','Costa Valle Imagna',17,1),(2252,'016086','D117','Costa Volpino',17,1),(2253,'016087','D126','Covo',17,1),(2254,'016088','D139','Credaro',17,1),(2255,'016089','D221','Curno',17,1),(2256,'016090','D233','Cusio',17,1),(2257,'016091','D245','Dalmine',17,1),(2258,'016092','D352','Dossena',17,1),(2259,'016093','D406','Endine Gaiano',17,1),(2260,'016094','D411','Entratico',17,1),(2261,'016096','D490','Fara Gera d\'Adda',17,1),(2262,'016097','D491','Fara Olivana con Sola',17,1),(2263,'016098','D588','Filago',17,1),(2264,'016099','D604','Fino del Monte',17,1),(2265,'016100','D606','Fiorano al Serio',17,1),(2266,'016101','D672','Fontanella',17,1),(2267,'016102','D684','Fonteno',17,1),(2268,'016103','D688','Foppolo',17,1),(2269,'016104','D697','Foresto Sparso',17,1),(2270,'016105','D727','Fornovo San Giovanni',17,1),(2271,'016106','D817','Fuipiano Valle Imagna',17,1),(2272,'016107','D903','Gandellino',17,1),(2273,'016108','D905','Gandino',17,1),(2274,'016109','D906','Gandosso',17,1),(2275,'016110','D943','Gaverina Terme',17,1),(2276,'016111','D952','Gazzaniga',17,1),(2277,'016112','D991','Gerosa',17,1),(2278,'016113','E006','Ghisalba',17,1),(2279,'016114','E100','Gorlago',17,1),(2280,'016115','E103','Gorle',17,1),(2281,'016116','E106','Gorno',17,1),(2282,'016117','E148','Grassobbio',17,1),(2283,'016118','E189','Gromo',17,1),(2284,'016119','E192','Grone',17,1),(2285,'016120','E219','Grumello del Monte',17,1),(2286,'016121','E353','Isola di Fondra',17,1),(2287,'016122','E370','Isso',17,1),(2288,'016123','E422','Lallio',17,1),(2289,'016124','E509','Leffe',17,1),(2290,'016125','E524','Lenna',17,1),(2291,'016126','E562','Levate',17,1),(2292,'016127','E640','Locatello',17,1),(2293,'016128','E704','Lovere',17,1),(2294,'016129','E751','Lurano',17,1),(2295,'016130','E770','Luzzana',17,1),(2296,'016131','E794','Madone',17,1),(2297,'016132','E901','Mapello',17,1),(2298,'016133','E987','Martinengo',17,1),(2299,'016134','F186','Mezzoldo',17,1),(2300,'016135','F243','Misano di Gera d\'Adda',17,1),(2301,'016136','F276','Moio de\' Calvi',17,1),(2302,'016137','F328','Monasterolo del Castello',17,1),(2303,'016139','F547','Montello',17,1),(2304,'016140','F720','Morengo',17,1),(2305,'016141','F738','Mornico al Serio',17,1),(2306,'016142','F786','Mozzanica',17,1),(2307,'016143','F791','Mozzo',17,1),(2308,'016144','F864','Nembro',17,1),(2309,'016145','G049','Olmo al Brembo',17,1),(2310,'016146','G050','Oltre il Colle',17,1),(2311,'016147','G054','Oltressenda Alta',17,1),(2312,'016148','G068','Oneta',17,1),(2313,'016149','G075','Onore',17,1),(2314,'016150','G108','Orio al Serio',17,1),(2315,'016151','G118','Ornica',17,1),(2316,'016152','G159','Osio Sopra',17,1),(2317,'016153','G160','Osio Sotto',17,1),(2318,'016154','G233','Pagazzano',17,1),(2319,'016155','G249','Paladina',17,1),(2320,'016156','G259','Palazzago',17,1),(2321,'016157','G295','Palosco',17,1),(2322,'016158','G346','Parre',17,1),(2323,'016159','G350','Parzanica',17,1),(2324,'016160','G412','Pedrengo',17,1),(2325,'016161','G418','Peia',17,1),(2326,'016162','G564','Pianico',17,1),(2327,'016163','G574','Piario',17,1),(2328,'016164','G579','Piazza Brembana',17,1),(2329,'016165','G583','Piazzatorre',17,1),(2330,'016166','G588','Piazzolo',17,1),(2331,'016167','G774','Pognano',17,1),(2332,'016168','F941','Ponte Nossa',17,1),(2333,'016169','G853','Ponteranica',17,1),(2334,'016170','G856','Ponte San Pietro',17,1),(2335,'016171','G864','Pontida',17,1),(2336,'016172','G867','Pontirolo Nuovo',17,1),(2337,'016173','G968','Pradalunga',17,1),(2338,'016174','H020','Predore',17,1),(2339,'016175','H036','Premolo',17,1),(2340,'016176','H046','Presezzo',17,1),(2341,'016177','H091','Pumenengo',17,1),(2342,'016178','H176','Ranica',17,1),(2343,'016179','H177','Ranzanico',17,1),(2344,'016180','H331','Riva di Solto',17,1),(2345,'016182','H492','Rogno',17,1),(2346,'016183','H509','Romano di Lombardia',17,1),(2347,'016184','H535','Roncobello',17,1),(2348,'016185','H544','Roncola',17,1),(2349,'016186','H584','Rota d\'Imagna',17,1),(2350,'016187','H615','Rovetta',17,1),(2351,'016188','H910','San Giovanni Bianco',17,1),(2352,'016189','B310','San Paolo d\'Argon',17,1),(2353,'016190','I079','San Pellegrino Terme',17,1),(2354,'016191','I168','Santa Brigida',17,1),(2355,'016192','I349','Sant\'Omobono Terme',17,1),(2356,'016193','I437','Sarnico',17,1),(2357,'016194','I506','Scanzorosciate',17,1),(2358,'016195','I530','Schilpario',17,1),(2359,'016196','I567','Sedrina',17,1),(2360,'016197','I597','Selvino',17,1),(2361,'016198','I628','Seriate',17,1),(2362,'016199','I629','Serina',17,1),(2363,'016200','I812','Solto Collina',17,1),(2364,'016201','I830','Songavazzo',17,1),(2365,'016202','I858','Sorisole',17,1),(2366,'016203','I869','Sotto il Monte Giovanni XXIII',17,1),(2367,'016204','I873','Sovere',17,1),(2368,'016205','I916','Spinone al Lago',17,1),(2369,'016206','I919','Spirano',17,1),(2370,'016207','I951','Stezzano',17,1),(2371,'016208','I986','Strozza',17,1),(2372,'016209','I997','Suisio',17,1),(2373,'016210','L037','Taleggio',17,1),(2374,'016211','L073','Tavernola Bergamasca',17,1),(2375,'016212','L087','Telgate',17,1),(2376,'016213','L118','Terno d\'Isola',17,1),(2377,'016214','L251','Torre Boldone',17,1),(2378,'016216','L265','Torre de\' Roveri',17,1),(2379,'016217','L276','Torre Pallavicina',17,1),(2380,'016218','L388','Trescore Balneario',17,1),(2381,'016219','L400','Treviglio',17,1),(2382,'016220','L404','Treviolo',17,1),(2383,'016221','C789','Ubiale Clanezzo',17,1),(2384,'016222','L502','Urgnano',17,1),(2385,'016223','L544','Valbondione',17,1),(2386,'016224','L545','Valbrembo',17,1),(2387,'016225','L579','Valgoglio',17,1),(2388,'016226','L623','Valleve',17,1),(2389,'016227','L642','Valnegra',17,1),(2390,'016228','L649','Valsecca',17,1),(2391,'016229','L655','Valtorta',17,1),(2392,'016230','L707','Vedeseta',17,1),(2393,'016232','L752','Verdellino',17,1),(2394,'016233','L753','Verdello',17,1),(2395,'016234','L795','Vertova',17,1),(2396,'016235','L827','Viadanica',17,1),(2397,'016236','L865','Vigano San Martino',17,1),(2398,'016237','L894','Vigolo',17,1),(2399,'016238','L929','Villa d\'Adda',17,1),(2400,'016239','A215','Villa d\'Almè',17,1),(2401,'016240','L936','Villa di Serio',17,1),(2402,'016241','L938','Villa d\'Ogna',17,1),(2403,'016242','M045','Villongo',17,1),(2404,'016243','M050','Vilminore di Scalve',17,1),(2405,'016244','M144','Zandobbio',17,1),(2406,'016245','M147','Zanica',17,1),(2407,'016246','M184','Zogno',17,1),(2408,'016247','D111','Costa Serina',17,1),(2409,'016248','A193','Algua',17,1),(2410,'016249','D016','Cornalba',17,1),(2411,'016250','F085','Medolago',17,1),(2412,'016251','I813','Solza',17,1),(2413,'017001','A034','Acquafredda',18,1),(2414,'017002','A060','Adro',18,1),(2415,'017003','A082','Agnosine',18,1),(2416,'017004','A188','Alfianello',18,1),(2417,'017005','A288','Anfo',18,1),(2418,'017006','A293','Angolo Terme',18,1),(2419,'017007','A451','Artogne',18,1),(2420,'017008','A529','Azzano Mella',18,1),(2421,'017009','A569','Bagnolo Mella',18,1),(2422,'017010','A578','Bagolino',18,1),(2423,'017011','A630','Barbariga',18,1),(2424,'017012','A661','Barghe',18,1),(2425,'017013','A702','Bassano Bresciano',18,1),(2426,'017014','A729','Bedizzole',18,1),(2427,'017015','A799','Berlingo',18,1),(2428,'017016','A816','Berzo Demo',18,1),(2429,'017017','A817','Berzo Inferiore',18,1),(2430,'017018','A861','Bienno',18,1),(2431,'017019','A878','Bione',18,1),(2432,'017020','B035','Borgo San Giacomo',18,1),(2433,'017021','B040','Borgosatollo',18,1),(2434,'017022','B054','Borno',18,1),(2435,'017023','B091','Botticino',18,1),(2436,'017024','B100','Bovegno',18,1),(2437,'017025','B102','Bovezzo',18,1),(2438,'017026','B120','Brandico',18,1),(2439,'017027','B124','Braone',18,1),(2440,'017028','B149','Breno',18,1),(2441,'017029','B157','Brescia',18,1),(2442,'017030','B184','Brione',18,1),(2443,'017031','B365','Caino',18,1),(2444,'017032','B394','Calcinato',18,1),(2445,'017033','B436','Calvagese della Riviera',18,1),(2446,'017034','B450','Calvisano',18,1),(2447,'017035','B664','Capo di Ponte',18,1),(2448,'017036','B676','Capovalle',18,1),(2449,'017037','B698','Capriano del Colle',18,1),(2450,'017038','B711','Capriolo',18,1),(2451,'017039','B817','Carpenedolo',18,1),(2452,'017040','C055','Castegnato',18,1),(2453,'017041','C072','Castelcovati',18,1),(2454,'017042','C208','Castel Mella',18,1),(2455,'017043','C293','Castenedolo',18,1),(2456,'017044','C330','Casto',18,1),(2457,'017045','C332','Castrezzato',18,1),(2458,'017046','C408','Cazzago San Martino',18,1),(2459,'017047','C417','Cedegolo',18,1),(2460,'017048','C439','Cellatica',18,1),(2461,'017049','C549','Cerveno',18,1),(2462,'017050','C585','Ceto',18,1),(2463,'017051','C591','Cevo',18,1),(2464,'017052','C618','Chiari',18,1),(2465,'017053','C685','Cigole',18,1),(2466,'017054','C691','Cimbergo',18,1),(2467,'017055','C760','Cividate Camuno',18,1),(2468,'017056','C806','Coccaglio',18,1),(2469,'017057','C850','Collebeato',18,1),(2470,'017058','C883','Collio',18,1),(2471,'017059','C893','Cologne',18,1),(2472,'017060','C925','Comezzano-Cizzago',18,1),(2473,'017061','C948','Concesio',18,1),(2474,'017062','D058','Corte Franca',18,1),(2475,'017063','D064','Corteno Golgi',18,1),(2476,'017064','D082','Corzano',18,1),(2477,'017065','D251','Darfo Boario Terme',18,1),(2478,'017066','D270','Dello',18,1),(2479,'017067','D284','Desenzano del Garda',18,1),(2480,'017068','D391','Edolo',18,1),(2481,'017069','D421','Erbusco',18,1),(2482,'017070','D434','Esine',18,1),(2483,'017071','D576','Fiesse',18,1),(2484,'017072','D634','Flero',18,1),(2485,'017073','D891','Gambara',18,1),(2486,'017074','D917','Gardone Riviera',18,1),(2487,'017075','D918','Gardone Val Trompia',18,1),(2488,'017076','D924','Gargnano',18,1),(2489,'017077','D940','Gavardo',18,1),(2490,'017078','D999','Ghedi',18,1),(2491,'017079','E010','Gianico',18,1),(2492,'017080','E116','Gottolengo',18,1),(2493,'017081','E271','Gussago',18,1),(2494,'017082','E280','Idro',18,1),(2495,'017083','E297','Incudine',18,1),(2496,'017084','E325','Irma',18,1),(2497,'017085','E333','Iseo',18,1),(2498,'017086','E364','Isorella',18,1),(2499,'017087','E497','Lavenone',18,1),(2500,'017088','E526','Leno',18,1),(2501,'017089','E596','Limone sul Garda',18,1),(2502,'017090','E652','Lodrino',18,1),(2503,'017091','E654','Lograto',18,1),(2504,'017092','E667','Lonato del Garda',18,1),(2505,'017093','E673','Longhena',18,1),(2506,'017094','E698','Losine',18,1),(2507,'017095','E706','Lozio',18,1),(2508,'017096','E738','Lumezzane',18,1),(2509,'017097','E787','Maclodio',18,1),(2510,'017098','E800','Magasa',18,1),(2511,'017099','E841','Mairano',18,1),(2512,'017100','E851','Malegno',18,1),(2513,'017101','E865','Malonno',18,1),(2514,'017102','E883','Manerba del Garda',18,1),(2515,'017103','E884','Manerbio',18,1),(2516,'017104','E928','Marcheno',18,1),(2517,'017105','E961','Marmentino',18,1),(2518,'017106','E967','Marone',18,1),(2519,'017107','F063','Mazzano',18,1),(2520,'017108','F216','Milzano',18,1),(2521,'017109','F373','Moniga del Garda',18,1),(2522,'017110','F375','Monno',18,1),(2523,'017111','F532','Monte Isola',18,1),(2524,'017112','F672','Monticelli Brusati',18,1),(2525,'017113','F471','Montichiari',18,1),(2526,'017114','F680','Montirone',18,1),(2527,'017115','F806','Mura',18,1),(2528,'017116','F820','Muscoline',18,1),(2529,'017117','F851','Nave',18,1),(2530,'017118','F884','Niardo',18,1),(2531,'017119','F989','Nuvolento',18,1),(2532,'017120','F990','Nuvolera',18,1),(2533,'017121','G001','Odolo',18,1),(2534,'017122','G006','Offlaga',18,1),(2535,'017123','G061','Ome',18,1),(2536,'017124','G074','Ono San Pietro',18,1),(2537,'017125','G149','Orzinuovi',18,1),(2538,'017126','G150','Orzivecchi',18,1),(2539,'017127','G170','Ospitaletto',18,1),(2540,'017128','G179','Ossimo',18,1),(2541,'017129','G213','Padenghe sul Garda',18,1),(2542,'017130','G217','Paderno Franciacorta',18,1),(2543,'017131','G247','Paisco Loveno',18,1),(2544,'017132','G248','Paitone',18,1),(2545,'017133','G264','Palazzolo sull\'Oglio',18,1),(2546,'017134','G327','Paratico',18,1),(2547,'017135','G354','Paspardo',18,1),(2548,'017136','G361','Passirano',18,1),(2549,'017137','G391','Pavone del Mella',18,1),(2550,'017138','G407','San Paolo',18,1),(2551,'017139','G474','Pertica Alta',18,1),(2552,'017140','G475','Pertica Bassa',18,1),(2553,'017141','G529','Pezzaze',18,1),(2554,'017142','G546','Pian Camuno',18,1),(2555,'017143','G710','Pisogne',18,1),(2556,'017144','G779','Polaveno',18,1),(2557,'017145','G801','Polpenazze del Garda',18,1),(2558,'017146','G815','Pompiano',18,1),(2559,'017147','G818','Poncarale',18,1),(2560,'017148','G844','Ponte di Legno',18,1),(2561,'017149','G859','Pontevico',18,1),(2562,'017150','G869','Pontoglio',18,1),(2563,'017151','G959','Pozzolengo',18,1),(2564,'017152','G977','Pralboino',18,1),(2565,'017153','H043','Preseglie',18,1),(2566,'017154','H050','Prestine',18,1),(2567,'017155','H055','Prevalle',18,1),(2568,'017156','H078','Provaglio d\'Iseo',18,1),(2569,'017157','H077','Provaglio Val Sabbia',18,1),(2570,'017158','H086','Puegnago sul Garda',18,1),(2571,'017159','H140','Quinzano d\'Oglio',18,1),(2572,'017160','H230','Remedello',18,1),(2573,'017161','H256','Rezzato',18,1),(2574,'017162','H410','Roccafranca',18,1),(2575,'017163','H477','Rodengo Saiano',18,1),(2576,'017164','H484','Roè Volciano',18,1),(2577,'017165','H525','Roncadelle',18,1),(2578,'017166','H598','Rovato',18,1),(2579,'017167','H630','Rudiano',18,1),(2580,'017168','H650','Sabbio Chiese',18,1),(2581,'017169','H699','Sale Marasino',18,1),(2582,'017170','H717','Salò',18,1),(2583,'017171','H838','San Felice del Benaco',18,1),(2584,'017172','H865','San Gervasio Bresciano',18,1),(2585,'017173','I412','San Zeno Naviglio',18,1),(2586,'017174','I433','Sarezzo',18,1),(2587,'017175','I476','Saviore dell\'Adamello',18,1),(2588,'017176','I588','Sellero',18,1),(2589,'017177','I607','Seniga',18,1),(2590,'017178','I631','Serle',18,1),(2591,'017179','I633','Sirmione',18,1),(2592,'017180','I782','Soiano del Lago',18,1),(2593,'017181','I831','Sonico',18,1),(2594,'017182','L002','Sulzano',18,1),(2595,'017183','C698','Tavernole sul Mella',18,1),(2596,'017184','L094','Temù',18,1),(2597,'017185','L169','Tignale',18,1),(2598,'017186','L210','Torbole Casaglia',18,1),(2599,'017187','L312','Toscolano-Maderno',18,1),(2600,'017188','L339','Travagliato',18,1),(2601,'017189','L372','Tremosine',18,1),(2602,'017190','L380','Trenzano',18,1),(2603,'017191','L406','Treviso Bresciano',18,1),(2604,'017192','L494','Urago d\'Oglio',18,1),(2605,'017193','L626','Vallio Terme',18,1),(2606,'017194','L468','Valvestino',18,1),(2607,'017195','L777','Verolanuova',18,1),(2608,'017196','L778','Verolavecchia',18,1),(2609,'017197','L812','Vestone',18,1),(2610,'017198','L816','Vezza d\'Oglio',18,1),(2611,'017199','L919','Villa Carcina',18,1),(2612,'017200','L923','Villachiara',18,1),(2613,'017201','L995','Villanuova sul Clisi',18,1),(2614,'017202','M065','Vione',18,1),(2615,'017203','M070','Visano',18,1),(2616,'017204','M104','Vobarno',18,1),(2617,'017205','M188','Zone',18,1),(2618,'017206','G549','Piancogno',18,1),(2619,'018001','A118','Alagna',19,1),(2620,'018002','A134','Albaredo Arnaboldi',19,1),(2621,'018003','A171','Albonese',19,1),(2622,'018004','A175','Albuzzano',19,1),(2623,'018005','A387','Arena Po',19,1),(2624,'018006','A538','Badia Pavese',19,1),(2625,'018007','A550','Bagnaria',19,1),(2626,'018008','A634','Barbianello',19,1),(2627,'018009','A690','Bascapè',19,1),(2628,'018010','A711','Bastida de\' Dossi',19,1),(2629,'018011','A712','Bastida Pancarana',19,1),(2630,'018012','A718','Battuda',19,1),(2631,'018013','A741','Belgioioso',19,1),(2632,'018014','A792','Bereguardo',19,1),(2633,'018015','A989','Borgarello',19,1),(2634,'018016','B028','Borgo Priolo',19,1),(2635,'018017','B030','Borgoratto Mormorolo',19,1),(2636,'018018','B038','Borgo San Siro',19,1),(2637,'018019','B051','Bornasco',19,1),(2638,'018020','B082','Bosnasco',19,1),(2639,'018021','B117','Brallo di Pregola',19,1),(2640,'018022','B142','Breme',19,1),(2641,'018023','B159','Bressana Bottarone',19,1),(2642,'018024','B201','Broni',19,1),(2643,'018025','B447','Calvignano',19,1),(2644,'018026','B567','Campospinoso',19,1),(2645,'018027','B587','Candia Lomellina',19,1),(2646,'018028','B599','Canevino',19,1),(2647,'018029','B613','Canneto Pavese',19,1),(2648,'018030','B741','Carbonara al Ticino',19,1),(2649,'018031','B929','Casanova Lonati',19,1),(2650,'018032','B945','Casatisma',19,1),(2651,'018033','B954','Casei Gerola',19,1),(2652,'018034','B988','Casorate Primo',19,1),(2653,'018035','C038','Cassolnovo',19,1),(2654,'018036','C050','Castana',19,1),(2655,'018037','C053','Casteggio',19,1),(2656,'018038','C157','Castelletto di Branduzzo',19,1),(2657,'018039','C184','Castello d\'Agogna',19,1),(2658,'018040','C213','Castelnovetto',19,1),(2659,'018041','C360','Cava Manara',19,1),(2660,'018042','C414','Cecima',19,1),(2661,'018043','C484','Ceranova',19,1),(2662,'018044','C508','Ceretto Lomellina',19,1),(2663,'018045','C509','Cergnago',19,1),(2664,'018046','C541','Certosa di Pavia',19,1),(2665,'018047','C551','Cervesina',19,1),(2666,'018048','C637','Chignolo Po',19,1),(2667,'018049','C684','Cigognola',19,1),(2668,'018050','C686','Cilavegna',19,1),(2669,'018051','C813','Codevilla',19,1),(2670,'018052','C958','Confienza',19,1),(2671,'018053','C979','Copiano',19,1),(2672,'018054','C982','Corana',19,1),(2673,'018055','D017','Cornale',19,1),(2674,'018056','D067','Corteolona',19,1),(2675,'018057','D081','Corvino San Quirico',19,1),(2676,'018058','D109','Costa de\' Nobili',19,1),(2677,'018059','D127','Cozzo',19,1),(2678,'018060','B824','Cura Carpignano',19,1),(2679,'018061','D348','Dorno',19,1),(2680,'018062','D552','Ferrera Erbognone',19,1),(2681,'018063','D594','Filighera',19,1),(2682,'018064','D732','Fortunago',19,1),(2683,'018065','D771','Frascarolo',19,1),(2684,'018066','D873','Galliavola',19,1),(2685,'018067','D892','Gambarana',19,1),(2686,'018068','D901','Gambolò',19,1),(2687,'018069','D925','Garlasco',19,1),(2688,'018070','D973','Genzone',19,1),(2689,'018071','D980','Gerenzago',19,1),(2690,'018072','E062','Giussago',19,1),(2691,'018073','E072','Godiasco',19,1),(2692,'018074','E081','Golferenzo',19,1),(2693,'018075','E152','Gravellona Lomellina',19,1),(2694,'018076','E195','Gropello Cairoli',19,1),(2695,'018077','E310','Inverno e Monteleone',19,1),(2696,'018078','E437','Landriano',19,1),(2697,'018079','E439','Langosco',19,1),(2698,'018080','E454','Lardirago',19,1),(2699,'018081','E600','Linarolo',19,1),(2700,'018082','E608','Lirio',19,1),(2701,'018083','E662','Lomello',19,1),(2702,'018084','B387','Lungavilla',19,1),(2703,'018085','E804','Magherno',19,1),(2704,'018086','E934','Marcignago',19,1),(2705,'018087','E999','Marzano',19,1),(2706,'018088','F080','Mede',19,1),(2707,'018089','F122','Menconico',19,1),(2708,'018090','F170','Mezzana Bigli',19,1),(2709,'018091','F171','Mezzana Rabattone',19,1),(2710,'018092','F175','Mezzanino',19,1),(2711,'018093','F238','Miradolo Terme',19,1),(2712,'018094','F417','Montalto Pavese',19,1),(2713,'018095','F440','Montebello della Battaglia',19,1),(2714,'018096','F449','Montecalvo Versiggia',19,1),(2715,'018097','F638','Montescano',19,1),(2716,'018098','F644','Montesegale',19,1),(2717,'018099','F670','Monticelli Pavese',19,1),(2718,'018100','F701','Montù Beccaria',19,1),(2719,'018101','F739','Mornico Losana',19,1),(2720,'018102','F754','Mortara',19,1),(2721,'018103','F891','Nicorvo',19,1),(2722,'018104','G021','Olevano di Lomellina',19,1),(2723,'018105','G032','Oliva Gessi',19,1),(2724,'018106','G194','Ottobiano',19,1),(2725,'018107','G275','Palestro',19,1),(2726,'018108','G304','Pancarana',19,1),(2727,'018109','G342','Parona',19,1),(2728,'018110','G388','Pavia',19,1),(2729,'018111','G612','Pietra de\' Giorgi',19,1),(2730,'018112','G635','Pieve Albignola',19,1),(2731,'018113','G639','Pieve del Cairo',19,1),(2732,'018114','G650','Pieve Porto Morone',19,1),(2733,'018115','G671','Pinarolo Po',19,1),(2734,'018116','G720','Pizzale',19,1),(2735,'018117','G851','Ponte Nizza',19,1),(2736,'018118','G895','Portalbera',19,1),(2737,'018119','H204','Rea',19,1),(2738,'018120','H216','Redavalle',19,1),(2739,'018121','H246','Retorbido',19,1),(2740,'018122','H336','Rivanazzano',19,1),(2741,'018123','H369','Robbio',19,1),(2742,'018124','H375','Robecco Pavese',19,1),(2743,'018125','H396','Rocca de\' Giorgi',19,1),(2744,'018126','H450','Rocca Susella',19,1),(2745,'018127','H491','Rognano',19,1),(2746,'018128','H505','Romagnese',19,1),(2747,'018129','H527','Roncaro',19,1),(2748,'018130','H559','Rosasco',19,1),(2749,'018131','H614','Rovescala',19,1),(2750,'018132','H637','Ruino',19,1),(2751,'018133','H799','San Cipriano Po',19,1),(2752,'018134','H814','San Damiano al Colle',19,1),(2753,'018135','H859','San Genesio ed Uniti',19,1),(2754,'018136','H885','San Giorgio di Lomellina',19,1),(2755,'018137','I014','San Martino Siccomario',19,1),(2756,'018138','I048','Sannazzaro de\' Burgondi',19,1),(2757,'018139','I175','Santa Cristina e Bissone',19,1),(2758,'018140','I203','Santa Giuletta',19,1),(2759,'018141','I213','Sant\'Alessio con Vialone',19,1),(2760,'018142','I230','Santa Margherita di Staffora',19,1),(2761,'018143','I237','Santa Maria della Versa',19,1),(2762,'018144','I276','Sant\'Angelo Lomellina',19,1),(2763,'018145','I416','San Zenone al Po',19,1),(2764,'018146','I447','Sartirana Lomellina',19,1),(2765,'018147','I487','Scaldasole',19,1),(2766,'018148','I599','Semiana',19,1),(2767,'018149','I739','Silvano Pietra',19,1),(2768,'018150','E265','Siziano',19,1),(2769,'018151','I825','Sommo',19,1),(2770,'018152','I894','Spessa',19,1),(2771,'018153','I968','Stradella',19,1),(2772,'018154','B014','Suardi',19,1),(2773,'018155','L237','Torrazza Coste',19,1),(2774,'018156','L250','Torre Beretti e Castellaro',19,1),(2775,'018157','L256','Torre d\'Arese',19,1),(2776,'018158','L262','Torre de\' Negri',19,1),(2777,'018159','L269','Torre d\'Isola',19,1),(2778,'018160','L285','Torrevecchia Pia',19,1),(2779,'018161','L292','Torricella Verzate',19,1),(2780,'018162','I236','Travacò Siccomario',19,1),(2781,'018163','L440','Trivolzio',19,1),(2782,'018164','L449','Tromello',19,1),(2783,'018165','L453','Trovo',19,1),(2784,'018166','L562','Val di Nizza',19,1),(2785,'018167','L568','Valeggio',19,1),(2786,'018168','L593','Valle Lomellina',19,1),(2787,'018169','L617','Valle Salimbene',19,1),(2788,'018170','L659','Valverde',19,1),(2789,'018171','L690','Varzi',19,1),(2790,'018172','L716','Velezzo Lomellina',19,1),(2791,'018173','L720','Vellezzo Bellini',19,1),(2792,'018174','L784','Verretto',19,1),(2793,'018175','L788','Verrua Po',19,1),(2794,'018176','L854','Vidigulfo',19,1),(2795,'018177','L872','Vigevano',19,1),(2796,'018178','L917','Villa Biscossi',19,1),(2797,'018179','L983','Villanova d\'Ardenghi',19,1),(2798,'018180','L994','Villanterio',19,1),(2799,'018181','M079','Vistarino',19,1),(2800,'018182','M109','Voghera',19,1),(2801,'018183','M119','Volpara',19,1),(2802,'018184','M150','Zavattarello',19,1),(2803,'018185','M152','Zeccone',19,1),(2804,'018186','M161','Zeme',19,1),(2805,'018187','M162','Zenevredo',19,1),(2806,'018188','M166','Zerbo',19,1),(2807,'018189','M167','Zerbolò',19,1),(2808,'018190','M180','Zinasco',19,1),(2809,'019001','A039','Acquanegra Cremonese',20,1),(2810,'019002','A076','Agnadello',20,1),(2811,'019003','A299','Annicco',20,1),(2812,'019004','A526','Azzanello',20,1),(2813,'019005','A570','Bagnolo Cremasco',20,1),(2814,'019006','A972','Bonemerse',20,1),(2815,'019007','A986','Bordolano',20,1),(2816,'019008','B320','Ca\' d\'Andrea',20,1),(2817,'019009','B439','Calvatone',20,1),(2818,'019010','B484','Camisano',20,1),(2819,'019011','B498','Campagnola Cremasca',20,1),(2820,'019012','B650','Capergnanica',20,1),(2821,'019013','B679','Cappella Cantone',20,1),(2822,'019014','B680','Cappella de\' Picenardi',20,1),(2823,'019015','B686','Capralba',20,1),(2824,'019016','B869','Casalbuttano ed Uniti',20,1),(2825,'019017','B881','Casale Cremasco-Vidolasco',20,1),(2826,'019018','B889','Casaletto Ceredano',20,1),(2827,'019019','B890','Casaletto di Sopra',20,1),(2828,'019020','B891','Casaletto Vaprio',20,1),(2829,'019021','B898','Casalmaggiore',20,1),(2830,'019022','B900','Casalmorano',20,1),(2831,'019023','C089','Casteldidone',20,1),(2832,'019024','C115','Castel Gabbiano',20,1),(2833,'019025','C153','Castelleone',20,1),(2834,'019026','B129','Castelverde',20,1),(2835,'019027','C290','Castelvisconti',20,1),(2836,'019028','C435','Cella Dati',20,1),(2837,'019029','C634','Chieve',20,1),(2838,'019030','C678','Cicognolo',20,1),(2839,'019031','C703','Cingia de\' Botti',20,1),(2840,'019032','D056','Corte de\' Cortesi con Cignone',20,1),(2841,'019033','D057','Corte de\' Frati',20,1),(2842,'019034','D141','Credera Rubbiano',20,1),(2843,'019035','D142','Crema',20,1),(2844,'019036','D150','Cremona',20,1),(2845,'019037','D151','Cremosano',20,1),(2846,'019038','D186','Crotta d\'Adda',20,1),(2847,'019039','D203','Cumignano sul Naviglio',20,1),(2848,'019040','D278','Derovere',20,1),(2849,'019041','D358','Dovera',20,1),(2850,'019042','D370','Drizzona',20,1),(2851,'019043','D574','Fiesco',20,1),(2852,'019044','D710','Formigara',20,1),(2853,'019045','D834','Gabbioneta-Binanuova',20,1),(2854,'019046','D841','Gadesco-Pieve Delmona',20,1),(2855,'019047','D966','Genivolta',20,1),(2856,'019048','D993','Gerre de\' Caprioli',20,1),(2857,'019049','E082','Gombito',20,1),(2858,'019050','E193','Grontardo',20,1),(2859,'019051','E217','Grumello Cremonese ed Uniti',20,1),(2860,'019052','E272','Gussola',20,1),(2861,'019053','E356','Isola Dovarese',20,1),(2862,'019054','E380','Izano',20,1),(2863,'019055','E793','Madignano',20,1),(2864,'019056','E843','Malagnino',20,1),(2865,'019057','E983','Martignana di Po',20,1),(2866,'019058','F434','Monte Cremasco',20,1),(2867,'019059','F681','Montodine',20,1),(2868,'019060','F761','Moscazzano',20,1),(2869,'019061','F771','Motta Baluffi',20,1),(2870,'019062','G004','Offanengo',20,1),(2871,'019063','G047','Olmeneta',20,1),(2872,'019064','G185','Ostiano',20,1),(2873,'019065','G222','Paderno Ponchielli',20,1),(2874,'019066','G260','Palazzo Pignano',20,1),(2875,'019067','G306','Pandino',20,1),(2876,'019068','G469','Persico Dosimo',20,1),(2877,'019069','G483','Pescarolo ed Uniti',20,1),(2878,'019070','G504','Pessina Cremonese',20,1),(2879,'019071','G536','Piadena',20,1),(2880,'019072','G558','Pianengo',20,1),(2881,'019073','G603','Pieranica',20,1),(2882,'019074','G647','Pieve d\'Olmi',20,1),(2883,'019075','G651','Pieve San Giacomo',20,1),(2884,'019076','G721','Pizzighettone',20,1),(2885,'019077','B914','Pozzaglio ed Uniti',20,1),(2886,'019078','H130','Quintano',20,1),(2887,'019079','H276','Ricengo',20,1),(2888,'019080','H314','Ripalta Arpina',20,1),(2889,'019081','H315','Ripalta Cremasca',20,1),(2890,'019082','H316','Ripalta Guerina',20,1),(2891,'019083','H341','Rivarolo del Re ed Uniti',20,1),(2892,'019084','H357','Rivolta d\'Adda',20,1),(2893,'019085','H372','Robecco d\'Oglio',20,1),(2894,'019086','H508','Romanengo',20,1),(2895,'019087','H731','Salvirola',20,1),(2896,'019088','H767','San Bassano',20,1),(2897,'019089','H815','San Daniele Po',20,1),(2898,'019090','H918','San Giovanni in Croce',20,1),(2899,'019091','I007','San Martino del Lago',20,1),(2900,'019092','I497','Scandolara Ravara',20,1),(2901,'019093','I498','Scandolara Ripa d\'Oglio',20,1),(2902,'019094','I627','Sergnano',20,1),(2903,'019095','I683','Sesto ed Uniti',20,1),(2904,'019096','I790','Solarolo Rainerio',20,1),(2905,'019097','I827','Soncino',20,1),(2906,'019098','I849','Soresina',20,1),(2907,'019099','I865','Sospiro',20,1),(2908,'019100','I906','Spinadesco',20,1),(2909,'019101','I909','Spineda',20,1),(2910,'019102','I914','Spino d\'Adda',20,1),(2911,'019103','I935','Stagno Lombardo',20,1),(2912,'019104','L164','Ticengo',20,1),(2913,'019105','L221','Torlino Vimercati',20,1),(2914,'019106','L225','Tornata',20,1),(2915,'019107','L258','Torre de\' Picenardi',20,1),(2916,'019108','L296','Torricella del Pizzo',20,1),(2917,'019109','L389','Trescore Cremasco',20,1),(2918,'019110','L426','Trigolo',20,1),(2919,'019111','L535','Vaiano Cremasco',20,1),(2920,'019112','L539','Vailate',20,1),(2921,'019113','L806','Vescovato',20,1),(2922,'019114','M116','Volongo',20,1),(2923,'019115','M127','Voltido',20,1),(2924,'020001','A038','Acquanegra sul Chiese',21,1),(2925,'020002','A470','Asola',21,1),(2926,'020003','A575','Bagnolo San Vito',21,1),(2927,'020004','A866','Bigarello',21,1),(2928,'020005','B011','Borgoforte',21,1),(2929,'020006','B013','Borgofranco sul Po',21,1),(2930,'020007','B110','Bozzolo',21,1),(2931,'020008','B612','Canneto sull\'Oglio',21,1),(2932,'020009','B739','Carbonara di Po',21,1),(2933,'020010','B901','Casalmoro',21,1),(2934,'020011','B907','Casaloldo',21,1),(2935,'020012','B911','Casalromano',21,1),(2936,'020013','C059','Castelbelforte',21,1),(2937,'020014','C076','Castel d\'Ario',21,1),(2938,'020015','C118','Castel Goffredo',21,1),(2939,'020016','C195','Castellucchio',21,1),(2940,'020017','C312','Castiglione delle Stiviere',21,1),(2941,'020018','C406','Cavriana',21,1),(2942,'020019','C502','Ceresara',21,1),(2943,'020020','C930','Commessaggio',21,1),(2944,'020021','D227','Curtatone',21,1),(2945,'020022','D351','Dosolo',21,1),(2946,'020023','D529','Felonica',21,1),(2947,'020024','D949','Gazoldo degli Ippoliti',21,1),(2948,'020025','D959','Gazzuolo',21,1),(2949,'020026','E078','Goito',21,1),(2950,'020027','E089','Gonzaga',21,1),(2951,'020028','E261','Guidizzolo',21,1),(2952,'020029','E818','Magnacavallo',21,1),(2953,'020030','E897','Mantova',21,1),(2954,'020031','E922','Marcaria',21,1),(2955,'020032','E949','Mariana Mantovana',21,1),(2956,'020033','E962','Marmirolo',21,1),(2957,'020034','F086','Medole',21,1),(2958,'020035','F267','Moglia',21,1),(2959,'020036','F705','Monzambano',21,1),(2960,'020037','B012','Motteggiana',21,1),(2961,'020038','G186','Ostiglia',21,1),(2962,'020039','G417','Pegognaga',21,1),(2963,'020040','G633','Pieve di Coriano',21,1),(2964,'020041','G717','Piubega',21,1),(2965,'020042','G753','Poggio Rusco',21,1),(2966,'020043','G816','Pomponesco',21,1),(2967,'020044','G862','Ponti sul Mincio',21,1),(2968,'020045','G917','Porto Mantovano',21,1),(2969,'020046','H129','Quingentole',21,1),(2970,'020047','H143','Quistello',21,1),(2971,'020048','H218','Redondesco',21,1),(2972,'020049','H248','Revere',21,1),(2973,'020050','H342','Rivarolo Mantovano',21,1),(2974,'020051','H481','Rodigo',21,1),(2975,'020052','H541','Roncoferraro',21,1),(2976,'020053','H604','Roverbella',21,1),(2977,'020054','H652','Sabbioneta',21,1),(2978,'020055','H771','San Benedetto Po',21,1),(2979,'020056','H870','San Giacomo delle Segnate',21,1),(2980,'020057','H883','San Giorgio di Mantova',21,1),(2981,'020058','H912','San Giovanni del Dosso',21,1),(2982,'020059','I005','San Martino dall\'Argine',21,1),(2983,'020060','I532','Schivenoglia',21,1),(2984,'020061','I632','Sermide',21,1),(2985,'020062','I662','Serravalle a Po',21,1),(2986,'020063','I801','Solferino',21,1),(2987,'020064','L015','Sustinente',21,1),(2988,'020065','L020','Suzzara',21,1),(2989,'020066','L826','Viadana',21,1),(2990,'020067','F804','Villa Poma',21,1),(2991,'020068','M044','Villimpenta',21,1),(2992,'020069','H123','Virgilio',21,1),(2993,'020070','M125','Volta Mantovana',21,1),(2994,'021001','A179','Aldino/Aldein',22,1),(2995,'021002','A286','Andriano/Andrian',22,1),(2996,'021003','A306','Anterivo/Altrei',22,1),(2997,'021005','A507','Avelengo/Hafling',22,1),(2998,'021006','A537','Badia/Abtei',22,1),(2999,'021007','A635','Barbiano/Barbian',22,1),(3000,'021008','A952','Bolzano/Bozen',22,1),(3001,'021009','B116','Braies/Prags',22,1),(3002,'021010','B145','Brennero/Brenner',22,1),(3003,'021011','B160','Bressanone/Brixen',22,1),(3004,'021012','B203','Bronzolo/Branzoll',22,1),(3005,'021013','B220','Brunico/Bruneck',22,1),(3006,'021014','B364','Caines/Kuens',22,1),(3007,'021016','B529','Campo di Trens/Freienfeld',22,1),(3008,'021017','B570','Campo Tures/Sand in Taufers',22,1),(3009,'021019','C254','Castelrotto/Kastelruth',22,1),(3010,'021020','A022','Cermes/Tscherms',22,1),(3011,'021021','C625','Chienes/Kiens',22,1),(3012,'021022','C652','Chiusa/Klausen',22,1),(3013,'021023','B799','Cornedo all\'Isarco/Karneid',22,1),(3014,'021026','D079','Corvara in Badia/Corvara',22,1),(3015,'021027','D222','Curon Venosta/Graun im Vinschgau',22,1),(3016,'021028','D311','Dobbiaco/Toblach',22,1),(3017,'021029','D392','Egna/Neumarkt',22,1),(3018,'021030','D484','Falzes/Pfalzen',22,1),(3019,'021032','D731','Fortezza/Franzensfeste',22,1),(3020,'021033','D821','Funes/Villnoess',22,1),(3021,'021034','D860','Gais/Gais',22,1),(3022,'021035','D923','Gargazzone/Gargazon',22,1),(3023,'021036','E069','Glorenza/Glurns',22,1),(3024,'021037','E398','Laces/Latsch',22,1),(3025,'021038','E412','Lagundo/Algund',22,1),(3026,'021039','E420','Laion/Lajen',22,1),(3027,'021040','E421','Laives/Leifers',22,1),(3028,'021041','E434','Lana/Lana',22,1),(3029,'021042','E457','Lasa/Laas',22,1),(3030,'021043','E481','Lauregno/Laurein',22,1),(3031,'021044','E764','Luson/Luesen',22,1),(3032,'021046','E862','Malles Venosta/Mals',22,1),(3033,'021047','E938','Marebbe/Enneberg',22,1),(3034,'021048','E959','Marlengo/Marling',22,1),(3035,'021049','E981','Martello/Martell',22,1),(3036,'021050','F118','Meltina/Moelten',22,1),(3037,'021051','F132','Merano/Meran',22,1),(3038,'021053','F392','Montagna/Montan',22,1),(3039,'021055','F836','Nalles/Nals',22,1),(3040,'021056','F849','Naturno/Naturns',22,1),(3041,'021057','F856','Naz-Sciaves/Natz-Schabs',22,1),(3042,'021058','F949','Nova Levante/Welschnofen',22,1),(3043,'021059','F950','Nova Ponente/Deutschnofen',22,1),(3044,'021060','G083','Ora/Auer',22,1),(3045,'021061','G140','Ortisei/St. Ulrich',22,1),(3046,'021062','G328','Parcines/Partschins',22,1),(3047,'021063','G443','Perca/Percha',22,1),(3048,'021064','G299','Plaus/Plaus',22,1),(3049,'021065','G830','Ponte Gardena/Waidbruck',22,1),(3050,'021066','G936','Postal/Burgstall',22,1),(3051,'021068','H019','Predoi/Prettau',22,1),(3052,'021069','H081','Proves/Proveis',22,1),(3053,'021070','H152','Racines/Ratschings',22,1),(3054,'021071','H189','Rasun Anterselva/Rasen-Antholz',22,1),(3055,'021072','H236','Renon/Ritten',22,1),(3056,'021073','H284','Rifiano/Riffian',22,1),(3057,'021074','H299','Rio di Pusteria/Muehlbach',22,1),(3058,'021075','H475','Rodengo/Rodeneck',22,1),(3059,'021076','H719','Salorno/Salurn',22,1),(3060,'021077','H786','San Candido/Innichen',22,1),(3061,'021079','H858','San Genesio Atesino/Jenesien',22,1),(3062,'021084','I065','San Pancrazio/St. Pankraz',22,1),(3063,'021086','I431','Sarentino/Sarntal',22,1),(3064,'021087','I519','Scena/Schenna',22,1),(3065,'021088','I593','Selva dei Molini/Muehlwald',22,1),(3066,'021091','I604','Senales/Schnals',22,1),(3067,'021092','I687','Sesto/Sexten',22,1),(3068,'021093','I729','Silandro/Schlanders',22,1),(3069,'021094','I771','Sluderno/Schluderns',22,1),(3070,'021095','I948','Stelvio/Stilfs',22,1),(3071,'021096','L106','Terento/Terenten',22,1),(3072,'021097','L108','Terlano/Terlan',22,1),(3073,'021099','L149','Tesimo/Tisens',22,1),(3074,'021100','L176','Tires/Tiers',22,1),(3075,'021101','L178','Tirolo/Tirol',22,1),(3076,'021103','L455','Tubre/Taufers im Muenstertal',22,1),(3077,'021104','L490','Ultimo/Ulten',22,1),(3078,'021105','L527','Vadena/Pfatten',22,1),(3079,'021106','L552','Valdaora/Olang',22,1),(3080,'021107','L564','Val di Vizze/Pfitsch',22,1),(3081,'021108','L595','Valle Aurina/Ahrntal',22,1),(3082,'021109','L601','Valle di Casies/Gsies',22,1),(3083,'021110','L660','Vandoies/Vintl',22,1),(3084,'021111','L687','Varna/Vahrn',22,1),(3085,'021112','L745','Verano/Voeran',22,1),(3086,'021113','L915','Villabassa/Niederdorf',22,1),(3087,'021114','L971','Villandro/Villanders',22,1),(3088,'021115','M067','Vipiteno/Sterzing',22,1),(3089,'021116','L724','Velturno/Feldthurns',22,1),(3090,'021117','E491','La Valle/Wengen',22,1),(3091,'022001','A116','Ala',23,1),(3092,'022002','A158','Albiano',23,1),(3093,'022003','A178','Aldeno',23,1),(3094,'022004','A260','Amblar',23,1),(3095,'022005','A274','Andalo',23,1),(3096,'022006','A372','Arco',23,1),(3097,'022007','A520','Avio',23,1),(3098,'022009','A694','Baselga di Pinè',23,1),(3099,'022011','A730','Bedollo',23,1),(3100,'022012','A808','Bersone',23,1),(3101,'022013','A821','Besenello',23,1),(3102,'022014','A839','Bezzecca',23,1),(3103,'022015','A863','Bieno',23,1),(3104,'022016','A901','Bleggio Inferiore',23,1),(3105,'022017','A902','Bleggio Superiore',23,1),(3106,'022018','A916','Bocenago',23,1),(3107,'022019','A933','Bolbeno',23,1),(3108,'022020','A967','Bondo',23,1),(3109,'022021','A968','Bondone',23,1),(3110,'022022','B006','Borgo Valsugana',23,1),(3111,'022023','B078','Bosentino',23,1),(3112,'022024','B135','Breguzzo',23,1),(3113,'022025','B153','Brentonico',23,1),(3114,'022026','B158','Bresimo',23,1),(3115,'022027','B165','Brez',23,1),(3116,'022028','B185','Brione',23,1),(3117,'022029','B335','Caderzone Terme',23,1),(3118,'022030','B360','Cagnò',23,1),(3119,'022031','B386','Calavino',23,1),(3120,'022032','B389','Calceranica al Lago',23,1),(3121,'022033','B400','Caldes',23,1),(3122,'022034','B404','Caldonazzo',23,1),(3123,'022035','B419','Calliano',23,1),(3124,'022036','B514','Campitello di Fassa',23,1),(3125,'022037','B525','Campodenno',23,1),(3126,'022038','B577','Canal San Bovo',23,1),(3127,'022039','B579','Canazei',23,1),(3128,'022040','B697','Capriana',23,1),(3129,'022041','B723','Carano',23,1),(3130,'022042','B783','Carisolo',23,1),(3131,'022043','B856','Carzano',23,1),(3132,'022045','C183','Castel Condino',23,1),(3133,'022046','C103','Castelfondo',23,1),(3134,'022047','C189','Castello-Molina di Fiemme',23,1),(3135,'022048','C194','Castello Tesino',23,1),(3136,'022049','C216','Castelnuovo',23,1),(3137,'022050','C372','Cavalese',23,1),(3138,'022051','C380','Cavareno',23,1),(3139,'022052','C392','Cavedago',23,1),(3140,'022053','C393','Cavedine',23,1),(3141,'022054','C400','Cavizzana',23,1),(3142,'022055','C452','Cembra',23,1),(3143,'022056','C467','Centa San Nicolò',23,1),(3144,'022057','C694','Cimego',23,1),(3145,'022058','C700','Cimone',23,1),(3146,'022059','C712','Cinte Tesino',23,1),(3147,'022060','C727','Cis',23,1),(3148,'022061','C756','Civezzano',23,1),(3149,'022062','C794','Cles',23,1),(3150,'022063','C797','Cloz',23,1),(3151,'022064','C931','Commezzadura',23,1),(3152,'022065','C944','Concei',23,1),(3153,'022066','C953','Condino',23,1),(3154,'022067','C994','Coredo',23,1),(3155,'022068','D188','Croviana',23,1),(3156,'022069','D206','Cunevo',23,1),(3157,'022070','D243','Daiano',23,1),(3158,'022071','D246','Dambel',23,1),(3159,'022072','D248','Daone',23,1),(3160,'022073','D250','Darè',23,1),(3161,'022074','D273','Denno',23,1),(3162,'022075','D302','Dimaro',23,1),(3163,'022076','D336','Don',23,1),(3164,'022077','D349','Dorsino',23,1),(3165,'022078','D365','Drena',23,1),(3166,'022079','D371','Dro',23,1),(3167,'022080','D457','Faedo',23,1),(3168,'022081','D468','Fai della Paganella',23,1),(3169,'022082','D516','Faver',23,1),(3170,'022083','D565','Fiavè',23,1),(3171,'022084','D572','Fiera di Primiero',23,1),(3172,'022085','D573','Fierozzo',23,1),(3173,'022086','D631','Flavon',23,1),(3174,'022087','D651','Folgaria',23,1),(3175,'022088','D663','Fondo',23,1),(3176,'022089','D714','Fornace',23,1),(3177,'022090','D775','Frassilongo',23,1),(3178,'022091','D928','Garniga Terme',23,1),(3179,'022092','E048','Giovo',23,1),(3180,'022093','E065','Giustino',23,1),(3181,'022094','E150','Grauno',23,1),(3182,'022095','E178','Grigno',23,1),(3183,'022096','E222','Grumes',23,1),(3184,'022097','E288','Imer',23,1),(3185,'022098','E334','Isera',23,1),(3186,'022099','E378','Ivano-Fracena',23,1),(3187,'022100','E452','Lardaro',23,1),(3188,'022101','E461','Lasino',23,1),(3189,'022102','E492','Lavarone',23,1),(3190,'022103','E500','Lavis',23,1),(3191,'022104','E565','Levico Terme',23,1),(3192,'022105','E614','Lisignago',23,1),(3193,'022106','E624','Livo',23,1),(3194,'022107','E658','Lomaso',23,1),(3195,'022108','E664','Lona-Lases',23,1),(3196,'022109','E757','Luserna',23,1),(3197,'022110','E850','Malè',23,1),(3198,'022111','E866','Malosco',23,1),(3199,'022112','F045','Massimeno',23,1),(3200,'022113','F068','Mazzin',23,1),(3201,'022114','F168','Mezzana',23,1),(3202,'022115','F176','Mezzano',23,1),(3203,'022116','F183','Mezzocorona',23,1),(3204,'022117','F187','Mezzolombardo',23,1),(3205,'022118','F263','Moena',23,1),(3206,'022119','F286','Molina di Ledro',23,1),(3207,'022120','F307','Molveno',23,1),(3208,'022121','F341','Monclassico',23,1),(3209,'022122','F396','Montagne',23,1),(3210,'022123','F728','Mori',23,1),(3211,'022124','F835','Nago-Torbole',23,1),(3212,'022125','F837','Nanno',23,1),(3213,'022126','F853','Nave San Rocco',23,1),(3214,'022127','F920','Nogaredo',23,1),(3215,'022128','F929','Nomi',23,1),(3216,'022129','F947','Novaledo',23,1),(3217,'022130','G168','Ospedaletto',23,1),(3218,'022131','G173','Ossana',23,1),(3219,'022132','G214','Padergnone',23,1),(3220,'022133','G296','Palù del Fersina',23,1),(3221,'022134','G305','Panchià',23,1),(3222,'022135','M303','Ronzo-Chienis',23,1),(3223,'022136','G419','Peio',23,1),(3224,'022137','G428','Pellizzano',23,1),(3225,'022138','G429','Pelugo',23,1),(3226,'022139','G452','Pergine Valsugana',23,1),(3227,'022140','G641','Pieve di Bono',23,1),(3228,'022141','G644','Pieve di Ledro',23,1),(3229,'022142','G656','Pieve Tesino',23,1),(3230,'022143','G681','Pinzolo',23,1),(3231,'022144','G808','Pomarolo',23,1),(3232,'022145','G950','Pozza di Fassa',23,1),(3233,'022146','G989','Praso',23,1),(3234,'022147','H018','Predazzo',23,1),(3235,'022148','H039','Preore',23,1),(3236,'022149','H057','Prezzo',23,1),(3237,'022150','H146','Rabbi',23,1),(3238,'022151','H162','Ragoli',23,1),(3239,'022152','H254','Revò',23,1),(3240,'022153','H330','Riva del Garda',23,1),(3241,'022154','H506','Romallo',23,1),(3242,'022155','H517','Romeno',23,1),(3243,'022156','H528','Roncegno Terme',23,1),(3244,'022157','H532','Ronchi Valsugana',23,1),(3245,'022158','H545','Roncone',23,1),(3246,'022159','H552','Ronzone',23,1),(3247,'022160','H607','Roverè della Luna',23,1),(3248,'022161','H612','Rovereto',23,1),(3249,'022162','H634','Ruffrè-Mendola',23,1),(3250,'022163','H639','Rumo',23,1),(3251,'022164','H666','Sagron Mis',23,1),(3252,'022165','H754','Samone',23,1),(3253,'022166','H966','San Lorenzo in Banale',23,1),(3254,'022167','I042','San Michele all\'Adige',23,1),(3255,'022168','I354','Sant\'Orsola Terme',23,1),(3256,'022169','I411','Sanzeno',23,1),(3257,'022170','I439','Sarnonico',23,1),(3258,'022171','I554','Scurelle',23,1),(3259,'022172','I576','Segonzano',23,1),(3260,'022173','I714','Sfruz',23,1),(3261,'022174','I760','Siror',23,1),(3262,'022175','I772','Smarano',23,1),(3263,'022176','I839','Soraga',23,1),(3264,'022177','I871','Sover',23,1),(3265,'022178','I889','Spera',23,1),(3266,'022179','I899','Spiazzo',23,1),(3267,'022180','I924','Spormaggiore',23,1),(3268,'022181','I925','Sporminore',23,1),(3269,'022182','I949','Stenico',23,1),(3270,'022183','I964','Storo',23,1),(3271,'022184','I975','Strembo',23,1),(3272,'022185','I979','Strigno',23,1),(3273,'022186','L033','Taio',23,1),(3274,'022187','L060','Tassullo',23,1),(3275,'022188','L089','Telve',23,1),(3276,'022189','L090','Telve di Sopra',23,1),(3277,'022190','L096','Tenna',23,1),(3278,'022191','L097','Tenno',23,1),(3279,'022192','L107','Terlago',23,1),(3280,'022193','L121','Terragnolo',23,1),(3281,'022194','L137','Terres',23,1),(3282,'022195','L145','Terzolas',23,1),(3283,'022196','L147','Tesero',23,1),(3284,'022197','L162','Tiarno di Sopra',23,1),(3285,'022198','L163','Tiarno di Sotto',23,1),(3286,'022199','L174','Tione di Trento',23,1),(3287,'022200','L200','Ton',23,1),(3288,'022201','L201','Tonadico',23,1),(3289,'022202','L211','Torcegno',23,1),(3290,'022203','L322','Trambileno',23,1),(3291,'022204','L329','Transacqua',23,1),(3292,'022205','L378','Trento',23,1),(3293,'022206','L385','Tres',23,1),(3294,'022207','L457','Tuenno',23,1),(3295,'022208','L550','Valda',23,1),(3296,'022209','L575','Valfloriana',23,1),(3297,'022210','L588','Vallarsa',23,1),(3298,'022211','L678','Varena',23,1),(3299,'022212','L697','Vattaro',23,1),(3300,'022213','L769','Vermiglio',23,1),(3301,'022214','L800','Vervò',23,1),(3302,'022215','L821','Vezzano',23,1),(3303,'022216','L886','Vignola-Falesina',23,1),(3304,'022217','L893','Vigo di Fassa',23,1),(3305,'022219','L896','Vigolo Vattaro',23,1),(3306,'022220','L903','Vigo Rendena',23,1),(3307,'022221','L910','Villa Agnedo',23,1),(3308,'022222','L957','Villa Lagarina',23,1),(3309,'022223','M006','Villa Rendena',23,1),(3310,'022224','M113','Volano',23,1),(3311,'022225','M142','Zambana',23,1),(3312,'022226','M173','Ziano di Fiemme',23,1),(3313,'022227','M198','Zuclo',23,1),(3314,'023001','A061','Affi',24,1),(3315,'023002','A137','Albaredo d\'Adige',24,1),(3316,'023003','A292','Angiari',24,1),(3317,'023004','A374','Arcole',24,1),(3318,'023005','A540','Badia Calavena',24,1),(3319,'023006','A650','Bardolino',24,1),(3320,'023007','A737','Belfiore',24,1),(3321,'023008','A837','Bevilacqua',24,1),(3322,'023009','A964','Bonavigo',24,1),(3323,'023010','B070','Boschi Sant\'Anna',24,1),(3324,'023011','B073','Bosco Chiesanuova',24,1),(3325,'023012','B107','Bovolone',24,1),(3326,'023013','B152','Brentino Belluno',24,1),(3327,'023014','B154','Brenzone',24,1),(3328,'023015','B296','Bussolengo',24,1),(3329,'023016','B304','Buttapietra',24,1),(3330,'023017','B402','Caldiero',24,1),(3331,'023018','B709','Caprino Veronese',24,1),(3332,'023019','B886','Casaleone',24,1),(3333,'023020','C041','Castagnaro',24,1),(3334,'023021','C078','Castel d\'Azzano',24,1),(3335,'023022','C225','Castelnuovo del Garda',24,1),(3336,'023023','C370','Cavaion Veronese',24,1),(3337,'023024','C412','Cazzano di Tramigna',24,1),(3338,'023025','C498','Cerea',24,1),(3339,'023026','C538','Cerro Veronese',24,1),(3340,'023027','C890','Cologna Veneta',24,1),(3341,'023028','C897','Colognola ai Colli',24,1),(3342,'023029','C943','Concamarise',24,1),(3343,'023030','D118','Costermano',24,1),(3344,'023031','D317','Dolcè',24,1),(3345,'023032','D419','Erbè',24,1),(3346,'023033','D420','Erbezzo',24,1),(3347,'023034','D549','Ferrara di Monte Baldo',24,1),(3348,'023035','D818','Fumane',24,1),(3349,'023036','D915','Garda',24,1),(3350,'023037','D957','Gazzo Veronese',24,1),(3351,'023038','E171','Grezzana',24,1),(3352,'023039','E284','Illasi',24,1),(3353,'023040','E349','Isola della Scala',24,1),(3354,'023041','E358','Isola Rizza',24,1),(3355,'023042','E489','Lavagno',24,1),(3356,'023043','E502','Lazise',24,1),(3357,'023044','E512','Legnago',24,1),(3358,'023045','E848','Malcesine',24,1),(3359,'023046','E911','Marano di Valpolicella',24,1),(3360,'023047','F172','Mezzane di Sotto',24,1),(3361,'023048','F218','Minerbe',24,1),(3362,'023049','F461','Montecchia di Crosara',24,1),(3363,'023050','F508','Monteforte d\'Alpone',24,1),(3364,'023051','F789','Mozzecane',24,1),(3365,'023052','F861','Negrar',24,1),(3366,'023053','F918','Nogara',24,1),(3367,'023054','F921','Nogarole Rocca',24,1),(3368,'023055','G080','Oppeano',24,1),(3369,'023056','G297','Palù',24,1),(3370,'023057','G365','Pastrengo',24,1),(3371,'023058','G481','Pescantina',24,1),(3372,'023059','G489','Peschiera del Garda',24,1),(3373,'023060','G945','Povegliano Veronese',24,1),(3374,'023061','H048','Pressana',24,1),(3375,'023062','H356','Rivoli Veronese',24,1),(3376,'023063','H522','Roncà',24,1),(3377,'023064','H540','Ronco all\'Adige',24,1),(3378,'023065','H606','Roverchiara',24,1),(3379,'023066','H610','Roveredo di Guà',24,1),(3380,'023067','H608','Roverè Veronese',24,1),(3381,'023068','H714','Salizzole',24,1),(3382,'023069','H783','San Bonifacio',24,1),(3383,'023070','H916','San Giovanni Ilarione',24,1),(3384,'023071','H924','San Giovanni Lupatoto',24,1),(3385,'023072','H944','Sanguinetto',24,1),(3386,'023073','I003','San Martino Buon Albergo',24,1),(3387,'023074','H712','San Mauro di Saline',24,1),(3388,'023075','I105','San Pietro di Morubio',24,1),(3389,'023076','I109','San Pietro in Cariano',24,1),(3390,'023077','I259','Sant\'Ambrogio di Valpolicella',24,1),(3391,'023078','I292','Sant\'Anna d\'Alfaedo',24,1),(3392,'023079','I414','San Zeno di Montagna',24,1),(3393,'023080','I594','Selva di Progno',24,1),(3394,'023081','I775','Soave',24,1),(3395,'023082','I821','Sommacampagna',24,1),(3396,'023083','I826','Sona',24,1),(3397,'023084','I850','Sorgà',24,1),(3398,'023085','L136','Terrazzo',24,1),(3399,'023086','L287','Torri del Benaco',24,1),(3400,'023087','L364','Tregnago',24,1),(3401,'023088','L396','Trevenzuolo',24,1),(3402,'023089','L567','Valeggio sul Mincio',24,1),(3403,'023090','L722','Velo Veronese',24,1),(3404,'023091','L781','Verona',24,1),(3405,'023092','D193','Veronella',24,1),(3406,'023093','L810','Vestenanova',24,1),(3407,'023094','L869','Vigasio',24,1),(3408,'023095','L912','Villa Bartolomea',24,1),(3409,'023096','L949','Villafranca di Verona',24,1),(3410,'023097','M172','Zevio',24,1),(3411,'023098','M178','Zimella',24,1),(3412,'024001','A093','Agugliaro',25,1),(3413,'024002','A154','Albettone',25,1),(3414,'024003','A220','Alonte',25,1),(3415,'024004','A231','Altavilla Vicentina',25,1),(3416,'024005','A236','Altissimo',25,1),(3417,'024006','A377','Arcugnano',25,1),(3418,'024007','A444','Arsiero',25,1),(3419,'024008','A459','Arzignano',25,1),(3420,'024009','A465','Asiago',25,1),(3421,'024010','A467','Asigliano Veneto',25,1),(3422,'024011','A627','Barbarano Vicentino',25,1),(3423,'024012','A703','Bassano del Grappa',25,1),(3424,'024013','A954','Bolzano Vicentino',25,1),(3425,'024014','B132','Breganze',25,1),(3426,'024015','B143','Brendola',25,1),(3427,'024016','B161','Bressanvido',25,1),(3428,'024017','B196','Brogliano',25,1),(3429,'024018','B403','Caldogno',25,1),(3430,'024019','B433','Caltrano',25,1),(3431,'024020','B441','Calvene',25,1),(3432,'024021','B485','Camisano Vicentino',25,1),(3433,'024022','B511','Campiglia dei Berici',25,1),(3434,'024023','B547','Campolongo sul Brenta',25,1),(3435,'024024','B835','Carrè',25,1),(3436,'024025','B844','Cartigliano',25,1),(3437,'024026','C037','Cassola',25,1),(3438,'024027','C056','Castegnero',25,1),(3439,'024028','C119','Castelgomberto',25,1),(3440,'024029','C605','Chiampo',25,1),(3441,'024030','C650','Chiuppano',25,1),(3442,'024031','C734','Cismon del Grappa',25,1),(3443,'024032','C824','Cogollo del Cengio',25,1),(3444,'024033','C949','Conco',25,1),(3445,'024034','D020','Cornedo Vicentino',25,1),(3446,'024035','D107','Costabissara',25,1),(3447,'024036','D136','Creazzo',25,1),(3448,'024037','D156','Crespadoro',25,1),(3449,'024038','D379','Dueville',25,1),(3450,'024039','D407','Enego',25,1),(3451,'024040','D496','Fara Vicentino',25,1),(3452,'024041','D750','Foza',25,1),(3453,'024042','D882','Gallio',25,1),(3454,'024043','D897','Gambellara',25,1),(3455,'024044','D902','Gambugliano',25,1),(3456,'024045','E138','Grancona',25,1),(3457,'024046','E184','Grisignano di Zocco',25,1),(3458,'024047','E226','Grumolo delle Abbadesse',25,1),(3459,'024048','E354','Isola Vicentina',25,1),(3460,'024049','E403','Laghi',25,1),(3461,'024050','E465','Lastebasse',25,1),(3462,'024051','E671','Longare',25,1),(3463,'024052','E682','Lonigo',25,1),(3464,'024053','E731','Lugo di Vicenza',25,1),(3465,'024054','E762','Lusiana',25,1),(3466,'024055','E864','Malo',25,1),(3467,'024056','E912','Marano Vicentino',25,1),(3468,'024057','E970','Marostica',25,1),(3469,'024058','F019','Mason Vicentino',25,1),(3470,'024059','F306','Molvena',25,1),(3471,'024060','F442','Montebello Vicentino',25,1),(3472,'024061','F464','Montecchio Maggiore',25,1),(3473,'024062','F465','Montecchio Precalcino',25,1),(3474,'024063','F486','Monte di Malo',25,1),(3475,'024064','F514','Montegalda',25,1),(3476,'024065','F515','Montegaldella',25,1),(3477,'024066','F662','Monteviale',25,1),(3478,'024067','F675','Monticello Conte Otto',25,1),(3479,'024068','F696','Montorso Vicentino',25,1),(3480,'024069','F768','Mossano',25,1),(3481,'024070','F829','Mussolente',25,1),(3482,'024071','F838','Nanto',25,1),(3483,'024072','F922','Nogarole Vicentino',25,1),(3484,'024073','F957','Nove',25,1),(3485,'024074','F964','Noventa Vicentina',25,1),(3486,'024075','G095','Orgiano',25,1),(3487,'024076','G406','Pedemonte',25,1),(3488,'024077','G560','Pianezze',25,1),(3489,'024078','G694','Piovene Rocchette',25,1),(3490,'024079','G776','Pojana Maggiore',25,1),(3491,'024080','G931','Posina',25,1),(3492,'024081','G943','Pove del Grappa',25,1),(3493,'024082','G957','Pozzoleone',25,1),(3494,'024083','H134','Quinto Vicentino',25,1),(3495,'024084','H214','Recoaro Terme',25,1),(3496,'024085','H361','Roana',25,1),(3497,'024086','H512','Romano d\'Ezzelino',25,1),(3498,'024087','H556','Rosà',25,1),(3499,'024088','H580','Rossano Veneto',25,1),(3500,'024089','H594','Rotzo',25,1),(3501,'024090','F810','Salcedo',25,1),(3502,'024091','H829','Sandrigo',25,1),(3503,'024092','H863','San Germano dei Berici',25,1),(3504,'024093','I047','San Nazario',25,1),(3505,'024094','I117','San Pietro Mussolino',25,1),(3506,'024095','I353','Santorso',25,1),(3507,'024096','I401','San Vito di Leguzzano',25,1),(3508,'024097','I425','Sarcedo',25,1),(3509,'024098','I430','Sarego',25,1),(3510,'024099','I527','Schiavon',25,1),(3511,'024100','I531','Schio',25,1),(3512,'024101','I783','Solagna',25,1),(3513,'024102','I867','Sossano',25,1),(3514,'024103','I879','Sovizzo',25,1),(3515,'024104','L156','Tezze sul Brenta',25,1),(3516,'024105','L157','Thiene',25,1),(3517,'024106','D717','Tonezza del Cimone',25,1),(3518,'024107','L248','Torrebelvicino',25,1),(3519,'024108','L297','Torri di Quartesolo',25,1),(3520,'024110','L433','Trissino',25,1),(3521,'024111','L551','Valdagno',25,1),(3522,'024112','L554','Valdastico',25,1),(3523,'024113','L624','Valli del Pasubio',25,1),(3524,'024114','L650','Valstagna',25,1),(3525,'024115','L723','Velo d\'Astico',25,1),(3526,'024116','L840','Vicenza',25,1),(3527,'024117','L952','Villaga',25,1),(3528,'024118','M032','Villaverla',25,1),(3529,'024119','M145','Zanè',25,1),(3530,'024120','M170','Zermeghedo',25,1),(3531,'024121','M194','Zovencedo',25,1),(3532,'024122','M199','Zugliano',25,1),(3533,'025001','A083','Agordo',26,1),(3534,'025002','A121','Alano di Piave',26,1),(3535,'025003','A206','Alleghe',26,1),(3536,'025004','A443','Arsiè',26,1),(3537,'025005','A501','Auronzo di Cadore',26,1),(3538,'025006','A757','Belluno',26,1),(3539,'025007','A982','Borca di Cadore',26,1),(3540,'025008','B375','Calalzo di Cadore',26,1),(3541,'025009','C146','Castellavazzo',26,1),(3542,'025010','C458','Cencenighe Agordino',26,1),(3543,'025011','C577','Cesiomaggiore',26,1),(3544,'025012','C630','Chies d\'Alpago',26,1),(3545,'025013','C672','Cibiana di Cadore',26,1),(3546,'025014','C872','Colle Santa Lucia',26,1),(3547,'025015','C920','Comelico Superiore',26,1),(3548,'025016','A266','Cortina d\'Ampezzo',26,1),(3549,'025017','D247','Danta di Cadore',26,1),(3550,'025018','D330','Domegge di Cadore',26,1),(3551,'025019','D470','Falcade',26,1),(3552,'025020','D506','Farra d\'Alpago',26,1),(3553,'025021','D530','Feltre',26,1),(3554,'025022','D686','Fonzaso',26,1),(3555,'025023','B574','Canale d\'Agordo',26,1),(3556,'025024','D726','Forno di Zoldo',26,1),(3557,'025025','E113','Gosaldo',26,1),(3558,'025026','E429','Lamon',26,1),(3559,'025027','E490','La Valle Agordina',26,1),(3560,'025028','C562','Lentiai',26,1),(3561,'025029','E588','Limana',26,1),(3562,'025030','E622','Livinallongo del Col di Lana',26,1),(3563,'025031','E672','Longarone',26,1),(3564,'025032','E687','Lorenzago di Cadore',26,1),(3565,'025033','E708','Lozzo di Cadore',26,1),(3566,'025034','F094','Mel',26,1),(3567,'025035','G169','Ospitale di Cadore',26,1),(3568,'025036','G404','Pedavena',26,1),(3569,'025037','G442','Perarolo di Cadore',26,1),(3570,'025038','G638','Pieve d\'Alpago',26,1),(3571,'025039','G642','Pieve di Cadore',26,1),(3572,'025040','B662','Ponte nelle Alpi',26,1),(3573,'025041','H092','Puos d\'Alpago',26,1),(3574,'025042','H124','Quero',26,1),(3575,'025043','H327','Rivamonte Agordino',26,1),(3576,'025044','H379','Rocca Pietore',26,1),(3577,'025045','H938','San Gregorio nelle Alpi',26,1),(3578,'025046','I063','San Nicolò di Comelico',26,1),(3579,'025047','I088','San Pietro di Cadore',26,1),(3580,'025048','I206','Santa Giustina',26,1),(3581,'025049','I347','San Tomaso Agordino',26,1),(3582,'025050','C919','Santo Stefano di Cadore',26,1),(3583,'025051','I392','San Vito di Cadore',26,1),(3584,'025052','I421','Sappada',26,1),(3585,'025053','I563','Sedico',26,1),(3586,'025054','I592','Selva di Cadore',26,1),(3587,'025055','I626','Seren del Grappa',26,1),(3588,'025056','I866','Sospirolo',26,1),(3589,'025057','I876','Soverzene',26,1),(3590,'025058','I673','Sovramonte',26,1),(3591,'025059','L030','Taibon Agordino',26,1),(3592,'025060','L040','Tambre',26,1),(3593,'025061','L422','Trichiana',26,1),(3594,'025062','L584','Vallada Agordina',26,1),(3595,'025063','L590','Valle di Cadore',26,1),(3596,'025064','L692','Vas',26,1),(3597,'025065','L890','Vigo di Cadore',26,1),(3598,'025066','M108','Vodo Cadore',26,1),(3599,'025067','M124','Voltago Agordino',26,1),(3600,'025068','I345','Zoldo Alto',26,1),(3601,'025069','M189','Zoppè di Cadore',26,1),(3602,'026001','A237','Altivole',27,1),(3603,'026002','A360','Arcade',27,1),(3604,'026003','A471','Asolo',27,1),(3605,'026004','B061','Borso del Grappa',27,1),(3606,'026005','B128','Breda di Piave',27,1),(3607,'026006','B349','Caerano di San Marco',27,1),(3608,'026007','B678','Cappella Maggiore',27,1),(3609,'026008','B744','Carbonera',27,1),(3610,'026009','B879','Casale sul Sile',27,1),(3611,'026010','B965','Casier',27,1),(3612,'026011','C073','Castelcucco',27,1),(3613,'026012','C111','Castelfranco Veneto',27,1),(3614,'026013','C190','Castello di Godego',27,1),(3615,'026014','C384','Cavaso del Tomba',27,1),(3616,'026015','C580','Cessalto',27,1),(3617,'026016','C614','Chiarano',27,1),(3618,'026017','C689','Cimadolmo',27,1),(3619,'026018','C735','Cison di Valmarino',27,1),(3620,'026019','C815','Codognè',27,1),(3621,'026020','C848','Colle Umberto',27,1),(3622,'026021','C957','Conegliano',27,1),(3623,'026022','C992','Cordignano',27,1),(3624,'026023','D030','Cornuda',27,1),(3625,'026024','D157','Crespano del Grappa',27,1),(3626,'026025','C670','Crocetta del Montello',27,1),(3627,'026026','D505','Farra di Soligo',27,1),(3628,'026027','D654','Follina',27,1),(3629,'026028','D674','Fontanelle',27,1),(3630,'026029','D680','Fonte',27,1),(3631,'026030','D794','Fregona',27,1),(3632,'026031','D854','Gaiarine',27,1),(3633,'026032','E021','Giavera del Montello',27,1),(3634,'026033','E071','Godega di Sant\'Urbano',27,1),(3635,'026034','E092','Gorgo al Monticano',27,1),(3636,'026035','E373','Istrana',27,1),(3637,'026036','E692','Loria',27,1),(3638,'026037','E893','Mansuè',27,1),(3639,'026038','E940','Mareno di Piave',27,1),(3640,'026039','F009','Maser',27,1),(3641,'026040','F012','Maserada sul Piave',27,1),(3642,'026041','F088','Meduna di Livenza',27,1),(3643,'026042','F190','Miane',27,1),(3644,'026043','F269','Mogliano Veneto',27,1),(3645,'026044','F332','Monastier di Treviso',27,1),(3646,'026045','F360','Monfumo',27,1),(3647,'026046','F443','Montebelluna',27,1),(3648,'026047','F725','Morgano',27,1),(3649,'026048','F729','Moriago della Battaglia',27,1),(3650,'026049','F770','Motta di Livenza',27,1),(3651,'026050','F872','Nervesa della Battaglia',27,1),(3652,'026051','F999','Oderzo',27,1),(3653,'026052','G115','Ormelle',27,1),(3654,'026053','G123','Orsago',27,1),(3655,'026054','G221','Paderno del Grappa',27,1),(3656,'026055','G229','Paese',27,1),(3657,'026056','G408','Pederobba',27,1),(3658,'026057','G645','Pieve di Soligo',27,1),(3659,'026058','G846','Ponte di Piave',27,1),(3660,'026059','G875','Ponzano Veneto',27,1),(3661,'026060','G909','Portobuffolè',27,1),(3662,'026061','G933','Possagno',27,1),(3663,'026062','G944','Povegliano',27,1),(3664,'026063','H022','Preganziol',27,1),(3665,'026064','H131','Quinto di Treviso',27,1),(3666,'026065','H220','Refrontolo',27,1),(3667,'026066','H238','Resana',27,1),(3668,'026067','H253','Revine Lago',27,1),(3669,'026068','H280','Riese Pio X',27,1),(3670,'026069','H523','Roncade',27,1),(3671,'026070','H706','Salgareda',27,1),(3672,'026071','H781','San Biagio di Callalta',27,1),(3673,'026072','H843','San Fior',27,1),(3674,'026073','I103','San Pietro di Feletto',27,1),(3675,'026074','I124','San Polo di Piave',27,1),(3676,'026075','I221','Santa Lucia di Piave',27,1),(3677,'026076','I382','San Vendemiano',27,1),(3678,'026077','I417','San Zenone degli Ezzelini',27,1),(3679,'026078','I435','Sarmede',27,1),(3680,'026079','I578','Segusino',27,1),(3681,'026080','I635','Sernaglia della Battaglia',27,1),(3682,'026081','F116','Silea',27,1),(3683,'026082','I927','Spresiano',27,1),(3684,'026083','L014','Susegana',27,1),(3685,'026084','L058','Tarzo',27,1),(3686,'026085','L402','Trevignano',27,1),(3687,'026086','L407','Treviso',27,1),(3688,'026087','L565','Valdobbiadene',27,1),(3689,'026088','L700','Vazzola',27,1),(3690,'026089','L706','Vedelago',27,1),(3691,'026090','L856','Vidor',27,1),(3692,'026091','M048','Villorba',27,1),(3693,'026092','M089','Vittorio Veneto',27,1),(3694,'026093','M118','Volpago del Montello',27,1),(3695,'026094','M163','Zenson di Piave',27,1),(3696,'026095','M171','Zero Branco',27,1),(3697,'027001','A302','Annone Veneto',28,1),(3698,'027002','B493','Campagna Lupia',28,1),(3699,'027003','B546','Campolongo Maggiore',28,1),(3700,'027004','B554','Camponogara',28,1),(3701,'027005','B642','Caorle',28,1),(3702,'027006','C383','Cavarzere',28,1),(3703,'027007','C422','Ceggia',28,1),(3704,'027008','C638','Chioggia',28,1),(3705,'027009','C714','Cinto Caomaggiore',28,1),(3706,'027010','C938','Cona',28,1),(3707,'027011','C950','Concordia Sagittaria',28,1),(3708,'027012','D325','Dolo',28,1),(3709,'027013','D415','Eraclea',28,1),(3710,'027014','D578','Fiesso d\'Artico',28,1),(3711,'027015','D740','Fossalta di Piave',28,1),(3712,'027016','D741','Fossalta di Portogruaro',28,1),(3713,'027017','D748','Fossò',28,1),(3714,'027018','E215','Gruaro',28,1),(3715,'027019','C388','Jesolo',28,1),(3716,'027020','E936','Marcon',28,1),(3717,'027021','E980','Martellago',28,1),(3718,'027022','F130','Meolo',28,1),(3719,'027023','F229','Mira',28,1),(3720,'027024','F241','Mirano',28,1),(3721,'027025','F826','Musile di Piave',28,1),(3722,'027026','F904','Noale',28,1),(3723,'027027','F963','Noventa di Piave',28,1),(3724,'027028','G565','Pianiga',28,1),(3725,'027029','G914','Portogruaro',28,1),(3726,'027030','G981','Pramaggiore',28,1),(3727,'027031','H117','Quarto d\'Altino',28,1),(3728,'027032','H735','Salzano',28,1),(3729,'027033','H823','San Donà di Piave',28,1),(3730,'027034','I040','San Michele al Tagliamento',28,1),(3731,'027035','I242','Santa Maria di Sala',28,1),(3732,'027036','I373','Santo Stino di Livenza',28,1),(3733,'027037','I551','Scorzè',28,1),(3734,'027038','I908','Spinea',28,1),(3735,'027039','I965','Stra',28,1),(3736,'027040','L085','Teglio Veneto',28,1),(3737,'027041','L267','Torre di Mosto',28,1),(3738,'027042','L736','Venezia',28,1),(3739,'027043','L899','Vigonovo',28,1),(3740,'027044','M308','Cavallino-Treporti',28,1),(3741,'028001','A001','Abano Terme',29,1),(3742,'028002','A075','Agna',29,1),(3743,'028003','A161','Albignasego',29,1),(3744,'028004','A296','Anguillara Veneta',29,1),(3745,'028005','A434','Arquà Petrarca',29,1),(3746,'028006','A438','Arre',29,1),(3747,'028007','A458','Arzergrande',29,1),(3748,'028008','A568','Bagnoli di Sopra',29,1),(3749,'028009','A613','Baone',29,1),(3750,'028010','A637','Barbona',29,1),(3751,'028011','A714','Battaglia Terme',29,1),(3752,'028012','A906','Boara Pisani',29,1),(3753,'028013','B031','Borgoricco',29,1),(3754,'028014','B106','Bovolenta',29,1),(3755,'028015','B213','Brugine',29,1),(3756,'028016','B345','Cadoneghe',29,1),(3757,'028017','B524','Campodarsego',29,1),(3758,'028018','B531','Campodoro',29,1),(3759,'028019','B563','Camposampiero',29,1),(3760,'028020','B564','Campo San Martino',29,1),(3761,'028021','B589','Candiana',29,1),(3762,'028022','B749','Carceri',29,1),(3763,'028023','B795','Carmignano di Brenta',29,1),(3764,'028026','B848','Cartura',29,1),(3765,'028027','B877','Casale di Scodosia',29,1),(3766,'028028','B912','Casalserugo',29,1),(3767,'028029','C057','Castelbaldo',29,1),(3768,'028030','C544','Cervarese Santa Croce',29,1),(3769,'028031','C713','Cinto Euganeo',29,1),(3770,'028032','C743','Cittadella',29,1),(3771,'028033','C812','Codevigo',29,1),(3772,'028034','C964','Conselve',29,1),(3773,'028035','D040','Correzzola',29,1),(3774,'028036','D226','Curtarolo',29,1),(3775,'028037','D442','Este',29,1),(3776,'028038','D679','Fontaniva',29,1),(3777,'028039','D879','Galliera Veneta',29,1),(3778,'028040','D889','Galzignano Terme',29,1),(3779,'028041','D956','Gazzo',29,1),(3780,'028042','E145','Grantorto',29,1),(3781,'028043','E146','Granze',29,1),(3782,'028044','E515','Legnaro',29,1),(3783,'028045','E592','Limena',29,1),(3784,'028046','E684','Loreggia',29,1),(3785,'028047','E709','Lozzo Atestino',29,1),(3786,'028048','F011','Maserà di Padova',29,1),(3787,'028049','F013','Masi',29,1),(3788,'028050','F033','Massanzago',29,1),(3789,'028051','F091','Megliadino San Fidenzio',29,1),(3790,'028052','F092','Megliadino San Vitale',29,1),(3791,'028053','F148','Merlara',29,1),(3792,'028054','F161','Mestrino',29,1),(3793,'028055','F382','Monselice',29,1),(3794,'028056','F394','Montagnana',29,1),(3795,'028057','F529','Montegrotto Terme',29,1),(3796,'028058','F962','Noventa Padovana',29,1),(3797,'028059','G167','Ospedaletto Euganeo',29,1),(3798,'028060','G224','Padova',29,1),(3799,'028061','G461','Pernumia',29,1),(3800,'028062','G534','Piacenza d\'Adige',29,1),(3801,'028063','G587','Piazzola sul Brenta',29,1),(3802,'028064','G688','Piombino Dese',29,1),(3803,'028065','G693','Piove di Sacco',29,1),(3804,'028066','G802','Polverara',29,1),(3805,'028067','G823','Ponso',29,1),(3806,'028068','G850','Pontelongo',29,1),(3807,'028069','G855','Ponte San Nicolò',29,1),(3808,'028070','G963','Pozzonovo',29,1),(3809,'028071','H622','Rovolon',29,1),(3810,'028072','H625','Rubano',29,1),(3811,'028073','H655','Saccolongo',29,1),(3812,'028074','H705','Saletto',29,1),(3813,'028075','H893','San Giorgio delle Pertiche',29,1),(3814,'028076','H897','San Giorgio in Bosco',29,1),(3815,'028077','I008','San Martino di Lupari',29,1),(3816,'028078','I107','San Pietro in Gu',29,1),(3817,'028079','I120','San Pietro Viminario',29,1),(3818,'028080','I207','Santa Giustina in Colle',29,1),(3819,'028081','I226','Santa Margherita d\'Adige',29,1),(3820,'028082','I275','Sant\'Angelo di Piove di Sacco',29,1),(3821,'028083','I319','Sant\'Elena',29,1),(3822,'028084','I375','Sant\'Urbano',29,1),(3823,'028085','I418','Saonara',29,1),(3824,'028086','I595','Selvazzano Dentro',29,1),(3825,'028087','I799','Solesino',29,1),(3826,'028088','I938','Stanghella',29,1),(3827,'028089','L100','Teolo',29,1),(3828,'028090','L132','Terrassa Padovana',29,1),(3829,'028091','L199','Tombolo',29,1),(3830,'028092','L270','Torreglia',29,1),(3831,'028093','L349','Trebaseleghe',29,1),(3832,'028094','L414','Tribano',29,1),(3833,'028095','L497','Urbana',29,1),(3834,'028096','L710','Veggiano',29,1),(3835,'028097','L805','Vescovana',29,1),(3836,'028098','L878','Vighizzolo d\'Este',29,1),(3837,'028099','L892','Vigodarzere',29,1),(3838,'028100','L900','Vigonza',29,1),(3839,'028101','L934','Villa del Conte',29,1),(3840,'028102','L937','Villa Estense',29,1),(3841,'028103','L947','Villafranca Padovana',29,1),(3842,'028104','L979','Villanova di Camposampiero',29,1),(3843,'028105','M103','Vo\'',29,1),(3844,'028106','M300','Due Carrare',29,1),(3845,'029001','A059','Adria',30,1),(3846,'029002','A400','Ariano nel Polesine',30,1),(3847,'029003','A435','Arquà Polesine',30,1),(3848,'029004','A539','Badia Polesine',30,1),(3849,'029005','A574','Bagnolo di Po',30,1),(3850,'029006','A795','Bergantino',30,1),(3851,'029007','B069','Bosaro',30,1),(3852,'029008','B432','Calto',30,1),(3853,'029009','B578','Canaro',30,1),(3854,'029010','B582','Canda',30,1),(3855,'029011','C122','Castelguglielmo',30,1),(3856,'029012','C207','Castelmassa',30,1),(3857,'029013','C215','Castelnovo Bariano',30,1),(3858,'029014','C461','Ceneselli',30,1),(3859,'029015','C500','Ceregnano',30,1),(3860,'029017','C987','Corbola',30,1),(3861,'029018','D105','Costa di Rovigo',30,1),(3862,'029019','D161','Crespino',30,1),(3863,'029021','D568','Ficarolo',30,1),(3864,'029022','D577','Fiesso Umbertiano',30,1),(3865,'029023','D776','Frassinelle Polesine',30,1),(3866,'029024','D788','Fratta Polesine',30,1),(3867,'029025','D855','Gaiba',30,1),(3868,'029026','D942','Gavello',30,1),(3869,'029027','E008','Giacciano con Baruchella',30,1),(3870,'029028','E240','Guarda Veneta',30,1),(3871,'029029','E522','Lendinara',30,1),(3872,'029030','E689','Loreo',30,1),(3873,'029031','E761','Lusia',30,1),(3874,'029032','F095','Melara',30,1),(3875,'029033','F994','Occhiobello',30,1),(3876,'029034','G323','Papozze',30,1),(3877,'029035','G525','Pettorazza Grimani',30,1),(3878,'029036','G673','Pincara',30,1),(3879,'029037','G782','Polesella',30,1),(3880,'029038','G836','Pontecchio Polesine',30,1),(3881,'029039','G923','Porto Tolle',30,1),(3882,'029040','H573','Rosolina',30,1),(3883,'029041','H620','Rovigo',30,1),(3884,'029042','H689','Salara',30,1),(3885,'029043','H768','San Bellino',30,1),(3886,'029044','H996','San Martino di Venezze',30,1),(3887,'029045','I953','Stienta',30,1),(3888,'029046','L026','Taglio di Po',30,1),(3889,'029047','L359','Trecenta',30,1),(3890,'029048','L939','Villadose',30,1),(3891,'029049','L967','Villamarzana',30,1),(3892,'029050','L985','Villanova del Ghebbo',30,1),(3893,'029051','L988','Villanova Marchesana',30,1),(3894,'029052','G926','Porto Viro',30,1),(3895,'030001','A103','Aiello del Friuli',31,1),(3896,'030002','A254','Amaro',31,1),(3897,'030003','A267','Ampezzo',31,1),(3898,'030004','A346','Aquileia',31,1),(3899,'030005','A447','Arta Terme',31,1),(3900,'030006','A448','Artegna',31,1),(3901,'030007','A491','Attimis',31,1),(3902,'030008','A553','Bagnaria Arsa',31,1),(3903,'030009','A700','Basiliano',31,1),(3904,'030010','A810','Bertiolo',31,1),(3905,'030011','A855','Bicinicco',31,1),(3906,'030012','A983','Bordano',31,1),(3907,'030013','B259','Buja',31,1),(3908,'030014','B309','Buttrio',31,1),(3909,'030015','B483','Camino al Tagliamento',31,1),(3910,'030016','B536','Campoformido',31,1),(3911,'030018','B788','Carlino',31,1),(3912,'030019','B994','Cassacco',31,1),(3913,'030020','C327','Castions di Strada',31,1),(3914,'030021','C389','Cavazzo Carnico',31,1),(3915,'030022','C494','Cercivento',31,1),(3916,'030023','C556','Cervignano del Friuli',31,1),(3917,'030024','C641','Chiopris-Viscone',31,1),(3918,'030025','C656','Chiusaforte',31,1),(3919,'030026','C758','Cividale del Friuli',31,1),(3920,'030027','C817','Codroipo',31,1),(3921,'030028','C885','Colloredo di Monte Albano',31,1),(3922,'030029','C918','Comeglians',31,1),(3923,'030030','D027','Corno di Rosazzo',31,1),(3924,'030031','D085','Coseano',31,1),(3925,'030032','D300','Dignano',31,1),(3926,'030033','D316','Dogna',31,1),(3927,'030034','D366','Drenchia',31,1),(3928,'030035','D408','Enemonzo',31,1),(3929,'030036','D455','Faedis',31,1),(3930,'030037','D461','Fagagna',31,1),(3931,'030038','D627','Fiumicello',31,1),(3932,'030039','D630','Flaibano',31,1),(3933,'030040','D718','Forni Avoltri',31,1),(3934,'030041','D719','Forni di Sopra',31,1),(3935,'030042','D720','Forni di Sotto',31,1),(3936,'030043','D962','Gemona del Friuli',31,1),(3937,'030044','E083','Gonars',31,1),(3938,'030045','E179','Grimacco',31,1),(3939,'030046','E473','Latisana',31,1),(3940,'030047','E476','Lauco',31,1),(3941,'030048','E553','Lestizza',31,1),(3942,'030049','E584','Lignano Sabbiadoro',31,1),(3943,'030050','E586','Ligosullo',31,1),(3944,'030051','E760','Lusevera',31,1),(3945,'030052','E820','Magnano in Riviera',31,1),(3946,'030053','E833','Majano',31,1),(3947,'030054','E847','Malborghetto Valbruna',31,1),(3948,'030055','E899','Manzano',31,1),(3949,'030056','E910','Marano Lagunare',31,1),(3950,'030057','E982','Martignacco',31,1),(3951,'030058','F144','Mereto di Tomba',31,1),(3952,'030059','F266','Moggio Udinese',31,1),(3953,'030060','F275','Moimacco',31,1),(3954,'030061','F574','Montenars',31,1),(3955,'030062','F756','Mortegliano',31,1),(3956,'030063','F760','Moruzzo',31,1),(3957,'030064','F832','Muzzana del Turgnano',31,1),(3958,'030065','F898','Nimis',31,1),(3959,'030066','G163','Osoppo',31,1),(3960,'030067','G198','Ovaro',31,1),(3961,'030068','G238','Pagnacco',31,1),(3962,'030069','G268','Palazzolo dello Stella',31,1),(3963,'030070','G284','Palmanova',31,1),(3964,'030071','G300','Paluzza',31,1),(3965,'030072','G352','Pasian di Prato',31,1),(3966,'030073','G381','Paularo',31,1),(3967,'030074','G389','Pavia di Udine',31,1),(3968,'030075','G743','Pocenia',31,1),(3969,'030076','G831','Pontebba',31,1),(3970,'030077','G891','Porpetto',31,1),(3971,'030078','G949','Povoletto',31,1),(3972,'030079','G966','Pozzuolo del Friuli',31,1),(3973,'030080','G969','Pradamano',31,1),(3974,'030081','H002','Prato Carnico',31,1),(3975,'030082','H014','Precenicco',31,1),(3976,'030083','H029','Premariacco',31,1),(3977,'030084','H038','Preone',31,1),(3978,'030085','H040','Prepotto',31,1),(3979,'030086','H089','Pulfero',31,1),(3980,'030087','H161','Ragogna',31,1),(3981,'030088','H196','Ravascletto',31,1),(3982,'030089','H200','Raveo',31,1),(3983,'030090','H206','Reana del Rojale',31,1),(3984,'030091','H229','Remanzacco',31,1),(3985,'030092','H242','Resia',31,1),(3986,'030093','H244','Resiutta',31,1),(3987,'030094','H289','Rigolato',31,1),(3988,'030095','H347','Rive d\'Arcano',31,1),(3989,'030096','H352','Rivignano',31,1),(3990,'030097','H533','Ronchis',31,1),(3991,'030098','H629','Ruda',31,1),(3992,'030099','H816','San Daniele del Friuli',31,1),(3993,'030100','H895','San Giorgio di Nogaro',31,1),(3994,'030101','H906','San Giovanni al Natisone',31,1),(3995,'030102','H951','San Leonardo',31,1),(3996,'030103','I092','San Pietro al Natisone',31,1),(3997,'030104','I248','Santa Maria la Longa',31,1),(3998,'030105','I404','San Vito al Torre',31,1),(3999,'030106','I405','San Vito di Fagagna',31,1),(4000,'030107','I464','Sauris',31,1),(4001,'030108','I478','Savogna',31,1),(4002,'030109','I562','Sedegliano',31,1),(4003,'030110','I777','Socchieve',31,1),(4004,'030111','I974','Stregna',31,1),(4005,'030112','L018','Sutrio',31,1),(4006,'030113','G736','Taipana',31,1),(4007,'030114','L039','Talmassons',31,1),(4008,'030116','L050','Tarcento',31,1),(4009,'030117','L057','Tarvisio',31,1),(4010,'030118','L065','Tavagnacco',31,1),(4011,'030119','L101','Teor',31,1),(4012,'030120','L144','Terzo d\'Aquileia',31,1),(4013,'030121','L195','Tolmezzo',31,1),(4014,'030122','L246','Torreano',31,1),(4015,'030123','L309','Torviscosa',31,1),(4016,'030124','L335','Trasaghis',31,1),(4017,'030125','L381','Treppo Carnico',31,1),(4018,'030126','L382','Treppo Grande',31,1),(4019,'030127','L421','Tricesimo',31,1),(4020,'030128','L438','Trivignano Udinese',31,1),(4021,'030129','L483','Udine',31,1),(4022,'030130','L686','Varmo',31,1),(4023,'030131','L743','Venzone',31,1),(4024,'030132','L801','Verzegnis',31,1),(4025,'030133','L909','Villa Santina',31,1),(4026,'030134','M034','Villa Vicentina',31,1),(4027,'030135','M073','Visco',31,1),(4028,'030136','M200','Zuglio',31,1),(4029,'030137','D700','Forgaria nel Friuli',31,1),(4030,'030138','M311','Campolongo Tapogliano',31,1),(4031,'031001','B712','Capriva del Friuli',32,1),(4032,'031002','D014','Cormons',32,1),(4033,'031003','D312','Doberdò del Lago',32,1),(4034,'031004','D321','Dolegna del Collio',32,1),(4035,'031005','D504','Farra d\'Isonzo',32,1),(4036,'031006','D645','Fogliano Redipuglia',32,1),(4037,'031007','E098','Gorizia',32,1),(4038,'031008','E124','Gradisca d\'Isonzo',32,1),(4039,'031009','E125','Grado',32,1),(4040,'031010','E952','Mariano del Friuli',32,1),(4041,'031011','F081','Medea',32,1),(4042,'031012','F356','Monfalcone',32,1),(4043,'031013','F710','Moraro',32,1),(4044,'031014','F767','Mossa',32,1),(4045,'031015','H514','Romans d\'Isonzo',32,1),(4046,'031016','H531','Ronchi dei Legionari',32,1),(4047,'031017','H665','Sagrado',32,1),(4048,'031018','H787','San Canzian d\'Isonzo',32,1),(4049,'031019','H845','San Floriano del Collio',32,1),(4050,'031020','H964','San Lorenzo Isontino',32,1),(4051,'031021','I082','San Pier d\'Isonzo',32,1),(4052,'031022','I479','Savogna d\'Isonzo',32,1),(4053,'031023','I939','Staranzano',32,1),(4054,'031024','L474','Turriaco',32,1),(4055,'031025','M043','Villesse',32,1),(4056,'032001','D383','Duino-Aurisina',33,1),(4057,'032002','F378','Monrupino',33,1),(4058,'032003','F795','Muggia',33,1),(4059,'032004','D324','San Dorligo della Valle - Dolina',33,1),(4060,'032005','I715','Sgonico',33,1),(4061,'032006','L424','Trieste',33,1),(4062,'033001','A067','Agazzano',34,1),(4063,'033002','A223','Alseno',34,1),(4064,'033003','A823','Besenzone',34,1),(4065,'033004','A831','Bettola',34,1),(4066,'033005','A909','Bobbio',34,1),(4067,'033006','B025','Borgonovo Val Tidone',34,1),(4068,'033007','B332','Cadeo',34,1),(4069,'033008','B405','Calendasco',34,1),(4070,'033009','B479','Caminata',34,1),(4071,'033010','B643','Caorso',34,1),(4072,'033011','B812','Carpaneto Piacentino',34,1),(4073,'033012','C145','Castell\'Arquato',34,1),(4074,'033013','C261','Castel San Giovanni',34,1),(4075,'033014','C288','Castelvetro Piacentino',34,1),(4076,'033015','C513','Cerignale',34,1),(4077,'033016','C838','Coli',34,1),(4078,'033017','D054','Corte Brugnatella',34,1),(4079,'033018','D061','Cortemaggiore',34,1),(4080,'033019','D502','Farini',34,1),(4081,'033020','D555','Ferriere',34,1),(4082,'033021','D611','Fiorenzuola d\'Arda',34,1),(4083,'033022','D958','Gazzola',34,1),(4084,'033023','E114','Gossolengo',34,1),(4085,'033024','E132','Gragnano Trebbiense',34,1),(4086,'033025','E196','Gropparello',34,1),(4087,'033026','E726','Lugagnano Val d\'Arda',34,1),(4088,'033027','F671','Monticelli d\'Ongina',34,1),(4089,'033028','F724','Morfasso',34,1),(4090,'033029','F885','Nibbiano',34,1),(4091,'033030','G195','Ottone',34,1),(4092,'033031','G399','Pecorara',34,1),(4093,'033032','G535','Piacenza',34,1),(4094,'033033','G557','Pianello Val Tidone',34,1),(4095,'033034','G696','Piozzano',34,1),(4096,'033035','G747','Podenzano',34,1),(4097,'033036','G842','Ponte dell\'Olio',34,1),(4098,'033037','G852','Pontenure',34,1),(4099,'033038','H350','Rivergaro',34,1),(4100,'033039','H593','Rottofreno',34,1),(4101,'033040','H887','San Giorgio Piacentino',34,1),(4102,'033041','G788','San Pietro in Cerro',34,1),(4103,'033042','I434','Sarmato',34,1),(4104,'033043','L348','Travo',34,1),(4105,'033044','L772','Vernasca',34,1),(4106,'033045','L897','Vigolzone',34,1),(4107,'033046','L980','Villanova sull\'Arda',34,1),(4108,'033047','M165','Zerba',34,1),(4109,'033048','L848','Ziano Piacentino',34,1),(4110,'034001','A138','Albareto',35,1),(4111,'034002','A646','Bardi',35,1),(4112,'034003','A731','Bedonia',35,1),(4113,'034004','A788','Berceto',35,1),(4114,'034005','A987','Bore',35,1),(4115,'034006','B042','Borgo Val di Taro',35,1),(4116,'034007','B293','Busseto',35,1),(4117,'034008','B408','Calestano',35,1),(4118,'034009','C852','Collecchio',35,1),(4119,'034010','C904','Colorno',35,1),(4120,'034011','C934','Compiano',35,1),(4121,'034012','D026','Corniglio',35,1),(4122,'034013','D526','Felino',35,1),(4123,'034014','B034','Fidenza',35,1),(4124,'034015','D673','Fontanellato',35,1),(4125,'034016','D685','Fontevivo',35,1),(4126,'034017','D728','Fornovo di Taro',35,1),(4127,'034018','E438','Langhirano',35,1),(4128,'034019','E547','Lesignano de\' Bagni',35,1),(4129,'034020','F082','Medesano',35,1),(4130,'034021','F174','Mezzani',35,1),(4131,'034022','F340','Monchio delle Corti',35,1),(4132,'034023','F473','Montechiarugolo',35,1),(4133,'034024','F882','Neviano degli Arduini',35,1),(4134,'034025','F914','Noceto',35,1),(4135,'034026','G255','Palanzano',35,1),(4136,'034027','G337','Parma',35,1),(4137,'034028','G424','Pellegrino Parmense',35,1),(4138,'034029','G783','Polesine Parmense',35,1),(4139,'034030','H384','Roccabianca',35,1),(4140,'034031','H682','Sala Baganza',35,1),(4141,'034032','H720','Salsomaggiore Terme',35,1),(4142,'034033','I153','San Secondo Parmense',35,1),(4143,'034034','I763','Sissa',35,1),(4144,'034035','I803','Solignano',35,1),(4145,'034036','I840','Soragna',35,1),(4146,'034037','I845','Sorbolo',35,1),(4147,'034038','E548','Terenzo',35,1),(4148,'034039','L183','Tizzano Val Parma',35,1),(4149,'034040','L229','Tornolo',35,1),(4150,'034041','L299','Torrile',35,1),(4151,'034042','L346','Traversetolo',35,1),(4152,'034043','L354','Trecasali',35,1),(4153,'034044','L641','Valmozzola',35,1),(4154,'034045','L672','Varano de\' Melegari',35,1),(4155,'034046','L689','Varsi',35,1),(4156,'034048','M174','Zibello',35,1),(4157,'035001','A162','Albinea',36,1),(4158,'035002','A573','Bagnolo in Piano',36,1),(4159,'035003','A586','Baiso',36,1),(4160,'035004','A850','Bibbiano',36,1),(4161,'035005','A988','Boretto',36,1),(4162,'035006','B156','Brescello',36,1),(4163,'035007','B283','Busana',36,1),(4164,'035008','B328','Cadelbosco di Sopra',36,1),(4165,'035009','B499','Campagnola Emilia',36,1),(4166,'035010','B502','Campegine',36,1),(4167,'035011','B825','Carpineti',36,1),(4168,'035012','B893','Casalgrande',36,1),(4169,'035013','B967','Casina',36,1),(4170,'035014','C141','Castellarano',36,1),(4171,'035015','C218','Castelnovo di Sotto',36,1),(4172,'035016','C219','Castelnovo ne\' Monti',36,1),(4173,'035017','C405','Cavriago',36,1),(4174,'035018','C669','Canossa',36,1),(4175,'035019','C840','Collagna',36,1),(4176,'035020','D037','Correggio',36,1),(4177,'035021','D450','Fabbrico',36,1),(4178,'035022','D934','Gattatico',36,1),(4179,'035023','E232','Gualtieri',36,1),(4180,'035024','E253','Guastalla',36,1),(4181,'035025','E585','Ligonchio',36,1),(4182,'035026','E772','Luzzara',36,1),(4183,'035027','F463','Montecchio Emilia',36,1),(4184,'035028','F960','Novellara',36,1),(4185,'035029','G947','Poviglio',36,1),(4186,'035030','H122','Quattro Castella',36,1),(4187,'035031','G654','Ramiseto',36,1),(4188,'035032','H225','Reggiolo',36,1),(4189,'035033','H223','Reggio nell\'Emilia',36,1),(4190,'035034','H298','Rio Saliceto',36,1),(4191,'035035','H500','Rolo',36,1),(4192,'035036','H628','Rubiera',36,1),(4193,'035037','I011','San Martino in Rio',36,1),(4194,'035038','I123','San Polo d\'Enza',36,1),(4195,'035039','I342','Sant\'Ilario d\'Enza',36,1),(4196,'035040','I496','Scandiano',36,1),(4197,'035041','L184','Toano',36,1),(4198,'035042','L815','Vetto',36,1),(4199,'035043','L820','Vezzano sul Crostolo',36,1),(4200,'035044','L831','Viano',36,1),(4201,'035045','L969','Villa Minozzo',36,1),(4202,'036001','A713','Bastiglia',37,1),(4203,'036002','A959','Bomporto',37,1),(4204,'036003','B539','Campogalliano',37,1),(4205,'036004','B566','Camposanto',37,1),(4206,'036005','B819','Carpi',37,1),(4207,'036006','C107','Castelfranco Emilia',37,1),(4208,'036007','C242','Castelnuovo Rangone',37,1),(4209,'036008','C287','Castelvetro di Modena',37,1),(4210,'036009','C398','Cavezzo',37,1),(4211,'036010','C951','Concordia sulla Secchia',37,1),(4212,'036011','D486','Fanano',37,1),(4213,'036012','D599','Finale Emilia',37,1),(4214,'036013','D607','Fiorano Modenese',37,1),(4215,'036014','D617','Fiumalbo',37,1),(4216,'036015','D711','Formigine',37,1),(4217,'036016','D783','Frassinoro',37,1),(4218,'036017','E264','Guiglia',37,1),(4219,'036018','E426','Lama Mocogno',37,1),(4220,'036019','E904','Maranello',37,1),(4221,'036020','E905','Marano sul Panaro',37,1),(4222,'036021','F087','Medolla',37,1),(4223,'036022','F240','Mirandola',37,1),(4224,'036023','F257','Modena',37,1),(4225,'036024','F484','Montecreto',37,1),(4226,'036025','F503','Montefiorino',37,1),(4227,'036026','F642','Montese',37,1),(4228,'036027','F930','Nonantola',37,1),(4229,'036028','F966','Novi di Modena',37,1),(4230,'036029','G250','Palagano',37,1),(4231,'036030','G393','Pavullo nel Frignano',37,1),(4232,'036031','G649','Pievepelago',37,1),(4233,'036032','G789','Polinago',37,1),(4234,'036033','H061','Prignano sulla Secchia',37,1),(4235,'036034','H195','Ravarino',37,1),(4236,'036035','H303','Riolunato',37,1),(4237,'036036','H794','San Cesario sul Panaro',37,1),(4238,'036037','H835','San Felice sul Panaro',37,1),(4239,'036038','I128','San Possidonio',37,1),(4240,'036039','I133','San Prospero',37,1),(4241,'036040','I462','Sassuolo',37,1),(4242,'036041','I473','Savignano sul Panaro',37,1),(4243,'036042','F357','Serramazzoni',37,1),(4244,'036043','I689','Sestola',37,1),(4245,'036044','I802','Soliera',37,1),(4246,'036045','I903','Spilamberto',37,1),(4247,'036046','L885','Vignola',37,1),(4248,'036047','M183','Zocca',37,1),(4249,'037001','A324','Anzola dell\'Emilia',38,1),(4250,'037002','A392','Argelato',38,1),(4251,'037003','A665','Baricella',38,1),(4252,'037004','A726','Bazzano',38,1),(4253,'037005','A785','Bentivoglio',38,1),(4254,'037006','A944','Bologna',38,1),(4255,'037007','B044','Borgo Tossignano',38,1),(4256,'037008','B249','Budrio',38,1),(4257,'037009','B399','Calderara di Reno',38,1),(4258,'037010','B572','Camugnano',38,1),(4259,'037011','B880','Casalecchio di Reno',38,1),(4260,'037012','B892','Casalfiumanese',38,1),(4261,'037013','C075','Castel d\'Aiano',38,1),(4262,'037014','C086','Castel del Rio',38,1),(4263,'037015','B969','Castel di Casio',38,1),(4264,'037016','C121','Castel Guelfo di Bologna',38,1),(4265,'037017','C185','Castello d\'Argile',38,1),(4266,'037018','C191','Castello di Serravalle',38,1),(4267,'037019','C204','Castel Maggiore',38,1),(4268,'037020','C265','Castel San Pietro Terme',38,1),(4269,'037021','C292','Castenaso',38,1),(4270,'037022','C296','Castiglione dei Pepoli',38,1),(4271,'037023','D158','Crespellano',38,1),(4272,'037024','D166','Crevalcore',38,1),(4273,'037025','D360','Dozza',38,1),(4274,'037026','D668','Fontanelice',38,1),(4275,'037027','D847','Gaggio Montano',38,1),(4276,'037028','D878','Galliera',38,1),(4277,'037029','E135','Granaglione',38,1),(4278,'037030','E136','Granarolo dell\'Emilia',38,1),(4279,'037031','E187','Grizzana Morandi',38,1),(4280,'037032','E289','Imola',38,1),(4281,'037033','A771','Lizzano in Belvedere',38,1),(4282,'037034','E655','Loiano',38,1),(4283,'037035','E844','Malalbergo',38,1),(4284,'037036','B689','Marzabotto',38,1),(4285,'037037','F083','Medicina',38,1),(4286,'037038','F219','Minerbio',38,1),(4287,'037039','F288','Molinella',38,1),(4288,'037040','F363','Monghidoro',38,1),(4289,'037041','F597','Monterenzio',38,1),(4290,'037042','F627','Monte San Pietro',38,1),(4291,'037043','F659','Monteveglio',38,1),(4292,'037044','F706','Monzuno',38,1),(4293,'037045','F718','Mordano',38,1),(4294,'037046','G205','Ozzano dell\'Emilia',38,1),(4295,'037047','G570','Pianoro',38,1),(4296,'037048','G643','Pieve di Cento',38,1),(4297,'037049','A558','Porretta Terme',38,1),(4298,'037050','H678','Sala Bolognese',38,1),(4299,'037051','G566','San Benedetto Val di Sambro',38,1),(4300,'037052','H896','San Giorgio di Piano',38,1),(4301,'037053','G467','San Giovanni in Persiceto',38,1),(4302,'037054','H945','San Lazzaro di Savena',38,1),(4303,'037055','I110','San Pietro in Casale',38,1),(4304,'037056','I191','Sant\'Agata Bolognese',38,1),(4305,'037057','G972','Sasso Marconi',38,1),(4306,'037058','I474','Savigno',38,1),(4307,'037059','L762','Vergato',38,1),(4308,'037060','M185','Zola Predosa',38,1),(4309,'038001','A393','Argenta',39,1),(4310,'038002','A806','Berra',39,1),(4311,'038003','A965','Bondeno',39,1),(4312,'038004','C469','Cento',39,1),(4313,'038005','C814','Codigoro',39,1),(4314,'038006','C912','Comacchio',39,1),(4315,'038007','C980','Copparo',39,1),(4316,'038008','D548','Ferrara',39,1),(4317,'038009','D713','Formignana',39,1),(4318,'038010','E320','Jolanda di Savoia',39,1),(4319,'038011','E410','Lagosanto',39,1),(4320,'038012','F016','Masi Torello',39,1),(4321,'038013','F026','Massa Fiscaglia',39,1),(4322,'038014','F156','Mesola',39,1),(4323,'038015','F198','Migliarino',39,1),(4324,'038016','F235','Mirabello',39,1),(4325,'038017','G184','Ostellato',39,1),(4326,'038018','G768','Poggio Renatico',39,1),(4327,'038019','G916','Portomaggiore',39,1),(4328,'038020','H360','Ro',39,1),(4329,'038021','I209','Sant\'Agostino',39,1),(4330,'038022','L868','Vigarano Mainarda',39,1),(4331,'038023','M110','Voghiera',39,1),(4332,'038024','L390','Tresigallo',39,1),(4333,'038025','E107','Goro',39,1),(4334,'038026','F199','Migliaro',39,1),(4335,'039001','A191','Alfonsine',40,1),(4336,'039002','A547','Bagnacavallo',40,1),(4337,'039003','A551','Bagnara di Romagna',40,1),(4338,'039004','B188','Brisighella',40,1),(4339,'039005','B982','Casola Valsenio',40,1),(4340,'039006','C065','Castel Bolognese',40,1),(4341,'039007','C553','Cervia',40,1),(4342,'039008','C963','Conselice',40,1),(4343,'039009','D121','Cotignola',40,1),(4344,'039010','D458','Faenza',40,1),(4345,'039011','D829','Fusignano',40,1),(4346,'039012','E730','Lugo',40,1),(4347,'039013','F029','Massa Lombarda',40,1),(4348,'039014','H199','Ravenna',40,1),(4349,'039015','H302','Riolo Terme',40,1),(4350,'039016','H642','Russi',40,1),(4351,'039017','I196','Sant\'Agata sul Santerno',40,1),(4352,'039018','I787','Solarolo',40,1),(4353,'040001','A565','Bagno di Romagna',41,1),(4354,'040003','A809','Bertinoro',41,1),(4355,'040004','B001','Borghi',41,1),(4356,'040007','C573','Cesena',41,1),(4357,'040008','C574','Cesenatico',41,1),(4358,'040009','C777','Civitella di Romagna',41,1),(4359,'040011','D357','Dovadola',41,1),(4360,'040012','D704','Forlì',41,1),(4361,'040013','D705','Forlimpopoli',41,1),(4362,'040014','D867','Galeata',41,1),(4363,'040015','D899','Gambettola',41,1),(4364,'040016','D935','Gatteo',41,1),(4365,'040018','E675','Longiano',41,1),(4366,'040019','F097','Meldola',41,1),(4367,'040020','F139','Mercato Saraceno',41,1),(4368,'040022','F259','Modigliana',41,1),(4369,'040028','F668','Montiano',41,1),(4370,'040031','G904','Portico e San Benedetto',41,1),(4371,'040032','H017','Predappio',41,1),(4372,'040033','H034','Premilcuore',41,1),(4373,'040036','H437','Rocca San Casciano',41,1),(4374,'040037','H542','Roncofreddo',41,1),(4375,'040041','I027','San Mauro Pascoli',41,1),(4376,'040043','I310','Santa Sofia',41,1),(4377,'040044','I444','Sarsina',41,1),(4378,'040045','I472','Savignano sul Rubicone',41,1),(4379,'040046','I779','Sogliano al Rubicone',41,1),(4380,'040049','L361','Tredozio',41,1),(4381,'040050','L764','Verghereto',41,1),(4382,'041001','A035','Acqualagna',42,1),(4383,'041002','A327','Apecchio',42,1),(4384,'041003','A493','Auditore',42,1),(4385,'041004','A639','Barchi',42,1),(4386,'041005','A740','Belforte all\'Isauro',42,1),(4387,'041006','B026','Borgo Pace',42,1),(4388,'041007','B352','Cagli',42,1),(4389,'041008','B636','Cantiano',42,1),(4390,'041009','B816','Carpegna',42,1),(4391,'041010','B846','Cartoceto',42,1),(4392,'041011','C080','Casteldelci',42,1),(4393,'041012','C830','Colbordolo',42,1),(4394,'041013','D488','Fano',42,1),(4395,'041014','D541','Fermignano',42,1),(4396,'041015','D749','Fossombrone',42,1),(4397,'041016','D791','Fratte Rosa',42,1),(4398,'041017','D807','Frontino',42,1),(4399,'041018','D808','Frontone',42,1),(4400,'041019','D836','Gabicce Mare',42,1),(4401,'041020','E122','Gradara',42,1),(4402,'041021','E351','Isola del Piano',42,1),(4403,'041022','E743','Lunano',42,1),(4404,'041023','E785','Macerata Feltria',42,1),(4405,'041024','E838','Maiolo',42,1),(4406,'041025','F135','Mercatello sul Metauro',42,1),(4407,'041026','F136','Mercatino Conca',42,1),(4408,'041027','F310','Mombaroccio',42,1),(4409,'041028','F347','Mondavio',42,1),(4410,'041029','F348','Mondolfo',42,1),(4411,'041030','F450','Montecalvo in Foglia',42,1),(4412,'041031','F467','Monte Cerignone',42,1),(4413,'041032','F474','Monteciccardo',42,1),(4414,'041033','F478','Montecopiolo',42,1),(4415,'041034','F497','Montefelcino',42,1),(4416,'041035','F524','Monte Grimano Terme',42,1),(4417,'041036','F533','Montelabbate',42,1),(4418,'041037','F555','Montemaggiore al Metauro',42,1),(4419,'041038','F589','Monte Porzio',42,1),(4420,'041039','F137','Novafeltria',42,1),(4421,'041040','G089','Orciano di Pesaro',42,1),(4422,'041041','G416','Peglio',42,1),(4423,'041042','G433','Pennabilli',42,1),(4424,'041043','G453','Pergola',42,1),(4425,'041044','G479','Pesaro',42,1),(4426,'041045','G514','Petriano',42,1),(4427,'041046','G537','Piagge',42,1),(4428,'041047','G551','Piandimeleto',42,1),(4429,'041048','G627','Pietrarubbia',42,1),(4430,'041049','G682','Piobbico',42,1),(4431,'041050','H721','Saltara',42,1),(4432,'041051','H809','San Costanzo',42,1),(4433,'041052','H886','San Giorgio di Pesaro',42,1),(4434,'041053','H949','San Leo',42,1),(4435,'041054','H958','San Lorenzo in Campo',42,1),(4436,'041055','I201','Sant\'Agata Feltria',42,1),(4437,'041056','I285','Sant\'Angelo in Lizzola',42,1),(4438,'041057','I287','Sant\'Angelo in Vado',42,1),(4439,'041058','I344','Sant\'Ippolito',42,1),(4440,'041059','I459','Sassocorvaro',42,1),(4441,'041060','I460','Sassofeltrio',42,1),(4442,'041061','I654','Serra Sant\'Abbondio',42,1),(4443,'041062','I670','Serrungarina',42,1),(4444,'041063','L034','Talamello',42,1),(4445,'041064','L078','Tavoleto',42,1),(4446,'041065','L081','Tavullia',42,1),(4447,'041066','L498','Urbania',42,1),(4448,'041067','L500','Urbino',42,1),(4449,'042001','A092','Agugliano',43,1),(4450,'042002','A271','Ancona',43,1),(4451,'042003','A366','Arcevia',43,1),(4452,'042004','A626','Barbara',43,1),(4453,'042005','A769','Belvedere Ostrense',43,1),(4454,'042006','B468','Camerano',43,1),(4455,'042007','B470','Camerata Picena',43,1),(4456,'042008','C060','Castelbellino',43,1),(4457,'042009','C071','Castel Colonna',43,1),(4458,'042010','C100','Castelfidardo',43,1),(4459,'042011','C152','Castelleone di Suasa',43,1),(4460,'042012','C248','Castelplanio',43,1),(4461,'042013','C524','Cerreto d\'Esi',43,1),(4462,'042014','C615','Chiaravalle',43,1),(4463,'042015','D007','Corinaldo',43,1),(4464,'042016','D211','Cupramontana',43,1),(4465,'042017','D451','Fabriano',43,1),(4466,'042018','D472','Falconara Marittima',43,1),(4467,'042019','D597','Filottrano',43,1),(4468,'042020','D965','Genga',43,1),(4469,'042021','E388','Jesi',43,1),(4470,'042022','E690','Loreto',43,1),(4471,'042023','E837','Maiolati Spontini',43,1),(4472,'042024','F145','Mergo',43,1),(4473,'042025','F381','Monsano',43,1),(4474,'042026','F453','Montecarotto',43,1),(4475,'042027','F560','Montemarciano',43,1),(4476,'042028','F593','Monterado',43,1),(4477,'042029','F600','Monte Roberto',43,1),(4478,'042030','F634','Monte San Vito',43,1),(4479,'042031','F745','Morro d\'Alba',43,1),(4480,'042032','F978','Numana',43,1),(4481,'042033','G003','Offagna',43,1),(4482,'042034','G157','Osimo',43,1),(4483,'042035','F401','Ostra',43,1),(4484,'042036','F581','Ostra Vetere',43,1),(4485,'042037','G771','Poggio San Marcello',43,1),(4486,'042038','G803','Polverigi',43,1),(4487,'042039','H322','Ripe',43,1),(4488,'042040','H575','Rosora',43,1),(4489,'042041','H979','San Marcello',43,1),(4490,'042042','I071','San Paolo di Jesi',43,1),(4491,'042043','I251','Santa Maria Nuova',43,1),(4492,'042044','I461','Sassoferrato',43,1),(4493,'042045','I608','Senigallia',43,1),(4494,'042046','I643','Serra de\' Conti',43,1),(4495,'042047','I653','Serra San Quirico',43,1),(4496,'042048','I758','Sirolo',43,1),(4497,'042049','I932','Staffolo',43,1),(4498,'043001','A031','Acquacanina',44,1),(4499,'043002','A329','Apiro',44,1),(4500,'043003','A334','Appignano',44,1),(4501,'043004','A739','Belforte del Chienti',44,1),(4502,'043005','A947','Bolognola',44,1),(4503,'043006','B398','Caldarola',44,1),(4504,'043007','B474','Camerino',44,1),(4505,'043008','B562','Camporotondo di Fiastrone',44,1),(4506,'043009','C251','Castelraimondo',44,1),(4507,'043010','C267','Castelsantangelo sul Nera',44,1),(4508,'043011','C582','Cessapalombo',44,1),(4509,'043012','C704','Cingoli',44,1),(4510,'043013','C770','Civitanova Marche',44,1),(4511,'043014','C886','Colmurano',44,1),(4512,'043015','D042','Corridonia',44,1),(4513,'043016','D429','Esanatoglia',44,1),(4514,'043017','D564','Fiastra',44,1),(4515,'043018','D609','Fiordimonte',44,1),(4516,'043019','D628','Fiuminata',44,1),(4517,'043020','D853','Gagliole',44,1),(4518,'043021','E228','Gualdo',44,1),(4519,'043022','E694','Loro Piceno',44,1),(4520,'043023','E783','Macerata',44,1),(4521,'043024','F051','Matelica',44,1),(4522,'043025','F268','Mogliano',44,1),(4523,'043026','F454','Montecassiano',44,1),(4524,'043027','F460','Monte Cavallo',44,1),(4525,'043028','F482','Montecosaro',44,1),(4526,'043029','F496','Montefano',44,1),(4527,'043030','F552','Montelupone',44,1),(4528,'043031','F621','Monte San Giusto',44,1),(4529,'043032','F622','Monte San Martino',44,1),(4530,'043033','F749','Morrovalle',44,1),(4531,'043034','F793','Muccia',44,1),(4532,'043035','G436','Penna San Giovanni',44,1),(4533,'043036','G515','Petriolo',44,1),(4534,'043037','G637','Pievebovigliana',44,1),(4535,'043038','G657','Pieve Torina',44,1),(4536,'043039','G690','Pioraco',44,1),(4537,'043040','D566','Poggio San Vicino',44,1),(4538,'043041','F567','Pollenza',44,1),(4539,'043042','G919','Porto Recanati',44,1),(4540,'043043','F632','Potenza Picena',44,1),(4541,'043044','H211','Recanati',44,1),(4542,'043045','H323','Ripe San Ginesio',44,1),(4543,'043046','H876','San Ginesio',44,1),(4544,'043047','I156','San Severino Marche',44,1),(4545,'043048','I286','Sant\'Angelo in Pontano',44,1),(4546,'043049','I436','Sarnano',44,1),(4547,'043050','I569','Sefro',44,1),(4548,'043051','I651','Serrapetrona',44,1),(4549,'043052','I661','Serravalle di Chienti',44,1),(4550,'043053','L191','Tolentino',44,1),(4551,'043054','L366','Treia',44,1),(4552,'043055','L501','Urbisaglia',44,1),(4553,'043056','L517','Ussita',44,1),(4554,'043057','M078','Visso',44,1),(4555,'044001','A044','Acquasanta Terme',45,1),(4556,'044002','A047','Acquaviva Picena',45,1),(4557,'044003','A233','Altidona',45,1),(4558,'044004','A252','Amandola',45,1),(4559,'044005','A335','Appignano del Tronto',45,1),(4560,'044006','A437','Arquata del Tronto',45,1),(4561,'044007','A462','Ascoli Piceno',45,1),(4562,'044008','A760','Belmonte Piceno',45,1),(4563,'044009','B534','Campofilone',45,1),(4564,'044010','B727','Carassai',45,1),(4565,'044011','C093','Castel di Lama',45,1),(4566,'044012','C321','Castignano',45,1),(4567,'044013','C331','Castorano',45,1),(4568,'044014','C877','Colli del Tronto',45,1),(4569,'044015','C935','Comunanza',45,1),(4570,'044016','D096','Cossignano',45,1),(4571,'044017','D210','Cupra Marittima',45,1),(4572,'044018','D477','Falerone',45,1),(4573,'044019','D542','Fermo',45,1),(4574,'044020','D652','Folignano',45,1),(4575,'044021','D691','Force',45,1),(4576,'044022','D760','Francavilla d\'Ete',45,1),(4577,'044023','E207','Grottammare',45,1),(4578,'044024','E208','Grottazzolina',45,1),(4579,'044025','E447','Lapedona',45,1),(4580,'044026','E807','Magliano di Tenna',45,1),(4581,'044027','E868','Maltignano',45,1),(4582,'044028','F021','Massa Fermana',45,1),(4583,'044029','F044','Massignano',45,1),(4584,'044030','F379','Monsampietro Morico',45,1),(4585,'044031','F380','Monsampolo del Tronto',45,1),(4586,'044032','F415','Montalto delle Marche',45,1),(4587,'044033','F428','Montappone',45,1),(4588,'044034','F487','Montedinove',45,1),(4589,'044035','F493','Montefalcone Appennino',45,1),(4590,'044036','F501','Montefiore dell\'Aso',45,1),(4591,'044037','F509','Montefortino',45,1),(4592,'044038','F516','Montegallo',45,1),(4593,'044039','F517','Monte Giberto',45,1),(4594,'044040','F520','Montegiorgio',45,1),(4595,'044041','F522','Montegranaro',45,1),(4596,'044042','F536','Monteleone di Fermo',45,1),(4597,'044043','F549','Montelparo',45,1),(4598,'044044','F570','Montemonaco',45,1),(4599,'044045','F591','Monteprandone',45,1),(4600,'044046','F599','Monte Rinaldo',45,1),(4601,'044047','F614','Monterubbiano',45,1),(4602,'044048','F626','Monte San Pietrangeli',45,1),(4603,'044049','F653','Monte Urano',45,1),(4604,'044050','F664','Monte Vidon Combatte',45,1),(4605,'044051','F665','Monte Vidon Corrado',45,1),(4606,'044052','F697','Montottone',45,1),(4607,'044053','F722','Moresco',45,1),(4608,'044054','G005','Offida',45,1),(4609,'044055','G137','Ortezzano',45,1),(4610,'044056','G289','Palmiano',45,1),(4611,'044057','G403','Pedaso',45,1),(4612,'044058','G516','Petritoli',45,1),(4613,'044059','G873','Ponzano di Fermo',45,1),(4614,'044060','G920','Porto San Giorgio',45,1),(4615,'044061','G921','Porto Sant\'Elpidio',45,1),(4616,'044062','H182','Rapagnano',45,1),(4617,'044063','H321','Ripatransone',45,1),(4618,'044064','H390','Roccafluvione',45,1),(4619,'044065','H588','Rotella',45,1),(4620,'044066','H769','San Benedetto del Tronto',45,1),(4621,'044067','I315','Santa Vittoria in Matenano',45,1),(4622,'044068','I324','Sant\'Elpidio a Mare',45,1),(4623,'044069','C070','Servigliano',45,1),(4624,'044070','I774','Smerillo',45,1),(4625,'044071','I912','Spinetoli',45,1),(4626,'044072','L279','Torre San Patrizio',45,1),(4627,'044073','L728','Venarotta',45,1),(4628,'045001','A496','Aulla',46,1),(4629,'045002','A576','Bagnone',46,1),(4630,'045003','B832','Carrara',46,1),(4631,'045004','B979','Casola in Lunigiana',46,1),(4632,'045005','C914','Comano',46,1),(4633,'045006','D590','Filattiera',46,1),(4634,'045007','D629','Fivizzano',46,1),(4635,'045008','D735','Fosdinovo',46,1),(4636,'045009','E574','Licciana Nardi',46,1),(4637,'045010','F023','Massa',46,1),(4638,'045011','F679','Montignoso',46,1),(4639,'045012','F802','Mulazzo',46,1),(4640,'045013','G746','Podenzana',46,1),(4641,'045014','G870','Pontremoli',46,1),(4642,'045015','L386','Tresana',46,1),(4643,'045016','L946','Villafranca in Lunigiana',46,1),(4644,'045017','M169','Zeri',46,1),(4645,'046001','A241','Altopascio',47,1),(4646,'046002','A560','Bagni di Lucca',47,1),(4647,'046003','A657','Barga',47,1),(4648,'046004','B007','Borgo a Mozzano',47,1),(4649,'046005','B455','Camaiore',47,1),(4650,'046006','B557','Camporgiano',47,1),(4651,'046007','B648','Capannori',47,1),(4652,'046008','B760','Careggine',47,1),(4653,'046009','C236','Castelnuovo di Garfagnana',47,1),(4654,'046010','C303','Castiglione di Garfagnana',47,1),(4655,'046011','C996','Coreglia Antelminelli',47,1),(4656,'046012','D449','Fabbriche di Vallico',47,1),(4657,'046013','D730','Forte dei Marmi',47,1),(4658,'046014','D734','Fosciandora',47,1),(4659,'046015','D874','Gallicano',47,1),(4660,'046016','E059','Giuncugnano',47,1),(4661,'046017','E715','Lucca',47,1),(4662,'046018','F035','Massarosa',47,1),(4663,'046019','F225','Minucciano',47,1),(4664,'046020','F283','Molazzana',47,1),(4665,'046021','F452','Montecarlo',47,1),(4666,'046022','G480','Pescaglia',47,1),(4667,'046023','G582','Piazza al Serchio',47,1),(4668,'046024','G628','Pietrasanta',47,1),(4669,'046025','G648','Pieve Fosciana',47,1),(4670,'046026','G882','Porcari',47,1),(4671,'046027','I142','San Romano in Garfagnana',47,1),(4672,'046028','I622','Seravezza',47,1),(4673,'046029','I737','Sillano',47,1),(4674,'046030','I942','Stazzema',47,1),(4675,'046031','L533','Vagli Sotto',47,1),(4676,'046032','L763','Vergemoli',47,1),(4677,'046033','L833','Viareggio',47,1),(4678,'046034','L913','Villa Basilica',47,1),(4679,'046035','L926','Villa Collemandina',47,1),(4680,'047001','A012','Abetone',48,1),(4681,'047002','A071','Agliana',48,1),(4682,'047003','B251','Buggiano',48,1),(4683,'047004','D235','Cutigliano',48,1),(4684,'047005','E432','Lamporecchio',48,1),(4685,'047006','E451','Larciano',48,1),(4686,'047007','E960','Marliana',48,1),(4687,'047008','F025','Massa e Cozzile',48,1),(4688,'047009','F384','Monsummano Terme',48,1),(4689,'047010','F410','Montale',48,1),(4690,'047011','A561','Montecatini-Terme',48,1),(4691,'047012','G491','Pescia',48,1),(4692,'047013','G636','Pieve a Nievole',48,1),(4693,'047014','G713','Pistoia',48,1),(4694,'047015','G715','Piteglio',48,1),(4695,'047016','G833','Ponte Buggianese',48,1),(4696,'047017','H109','Quarrata',48,1),(4697,'047018','H744','Sambuca Pistoiese',48,1),(4698,'047019','H980','San Marcello Pistoiese',48,1),(4699,'047020','I660','Serravalle Pistoiese',48,1),(4700,'047021','L522','Uzzano',48,1),(4701,'047022','C631','Chiesina Uzzanese',48,1),(4702,'048001','A564','Bagno a Ripoli',49,1),(4703,'048002','A632','Barberino di Mugello',49,1),(4704,'048003','A633','Barberino Val d\'Elsa',49,1),(4705,'048004','B036','Borgo San Lorenzo',49,1),(4706,'048005','B406','Calenzano',49,1),(4707,'048006','B507','Campi Bisenzio',49,1),(4708,'048008','B684','Capraia e Limite',49,1),(4709,'048010','C101','Castelfiorentino',49,1),(4710,'048011','C529','Cerreto Guidi',49,1),(4711,'048012','C540','Certaldo',49,1),(4712,'048013','D299','Dicomano',49,1),(4713,'048014','D403','Empoli',49,1),(4714,'048015','D575','Fiesole',49,1),(4715,'048016','D583','Figline Valdarno',49,1),(4716,'048017','D612','Firenze',49,1),(4717,'048018','D613','Firenzuola',49,1),(4718,'048019','D815','Fucecchio',49,1),(4719,'048020','D895','Gambassi Terme',49,1),(4720,'048021','E169','Greve in Chianti',49,1),(4721,'048022','E291','Impruneta',49,1),(4722,'048023','E296','Incisa in Val d\'Arno',49,1),(4723,'048024','E466','Lastra a Signa',49,1),(4724,'048025','E668','Londa',49,1),(4725,'048026','E971','Marradi',49,1),(4726,'048027','F398','Montaione',49,1),(4727,'048028','F551','Montelupo Fiorentino',49,1),(4728,'048030','F648','Montespertoli',49,1),(4729,'048031','G270','Palazzuolo sul Senio',49,1),(4730,'048032','G420','Pelago',49,1),(4731,'048033','G825','Pontassieve',49,1),(4732,'048035','H222','Reggello',49,1),(4733,'048036','H286','Rignano sull\'Arno',49,1),(4734,'048037','H635','Rufina',49,1),(4735,'048038','H791','San Casciano in Val di Pesa',49,1),(4736,'048039','H937','San Godenzo',49,1),(4737,'048040','I085','San Piero a Sieve',49,1),(4738,'048041','B962','Scandicci',49,1),(4739,'048042','I514','Scarperia',49,1),(4740,'048043','I684','Sesto Fiorentino',49,1),(4741,'048044','I728','Signa',49,1),(4742,'048045','L067','Tavarnelle Val di Pesa',49,1),(4743,'048046','L529','Vaglia',49,1),(4744,'048049','L838','Vicchio',49,1),(4745,'048050','M059','Vinci',49,1),(4746,'049001','A852','Bibbona',50,1),(4747,'049002','B509','Campiglia Marittima',50,1),(4748,'049003','B553','Campo nell\'Elba',50,1),(4749,'049004','B669','Capoliveri',50,1),(4750,'049005','B685','Capraia Isola',50,1),(4751,'049006','C044','Castagneto Carducci',50,1),(4752,'049007','C415','Cecina',50,1),(4753,'049008','C869','Collesalvetti',50,1),(4754,'049009','E625','Livorno',50,1),(4755,'049010','E930','Marciana',50,1),(4756,'049011','E931','Marciana Marina',50,1),(4757,'049012','G687','Piombino',50,1),(4758,'049013','E680','Porto Azzurro',50,1),(4759,'049014','G912','Portoferraio',50,1),(4760,'049015','H305','Rio Marina',50,1),(4761,'049016','H297','Rio nell\'Elba',50,1),(4762,'049017','H570','Rosignano Marittimo',50,1),(4763,'049018','I390','San Vincenzo',50,1),(4764,'049019','I454','Sassetta',50,1),(4765,'049020','L019','Suvereto',50,1),(4766,'050001','A864','Bientina',51,1),(4767,'050002','B303','Buti',51,1),(4768,'050003','B390','Calci',51,1),(4769,'050004','B392','Calcinaia',51,1),(4770,'050005','B647','Capannoli',51,1),(4771,'050006','B878','Casale Marittimo',51,1),(4772,'050007','A559','Casciana Terme',51,1),(4773,'050008','B950','Cascina',51,1),(4774,'050009','C113','Castelfranco di Sotto',51,1),(4775,'050010','C174','Castellina Marittima',51,1),(4776,'050011','C244','Castelnuovo di Val di Cecina',51,1),(4777,'050012','C609','Chianni',51,1),(4778,'050013','D160','Crespina',51,1),(4779,'050014','D510','Fauglia',51,1),(4780,'050015','E250','Guardistallo',51,1),(4781,'050016','E413','Lajatico',51,1),(4782,'050017','E455','Lari',51,1),(4783,'050018','E688','Lorenzana',51,1),(4784,'050019','F458','Montecatini Val di Cecina',51,1),(4785,'050020','F640','Montescudaio',51,1),(4786,'050021','F661','Monteverdi Marittimo',51,1),(4787,'050022','F686','Montopoli in Val d\'Arno',51,1),(4788,'050023','G090','Orciano Pisano',51,1),(4789,'050024','G254','Palaia',51,1),(4790,'050025','G395','Peccioli',51,1),(4791,'050026','G702','Pisa',51,1),(4792,'050027','G804','Pomarance',51,1),(4793,'050028','G822','Ponsacco',51,1),(4794,'050029','G843','Pontedera',51,1),(4795,'050030','H319','Riparbella',51,1),(4796,'050031','A562','San Giuliano Terme',51,1),(4797,'050032','I046','San Miniato',51,1),(4798,'050033','I177','Santa Croce sull\'Arno',51,1),(4799,'050034','I217','Santa Luce',51,1),(4800,'050035','I232','Santa Maria a Monte',51,1),(4801,'050036','L138','Terricciola',51,1),(4802,'050037','L702','Vecchiano',51,1),(4803,'050038','L850','Vicopisano',51,1),(4804,'050039','M126','Volterra',51,1),(4805,'051001','A291','Anghiari',52,1),(4806,'051002','A390','Arezzo',52,1),(4807,'051003','A541','Badia Tedalda',52,1),(4808,'051004','A851','Bibbiena',52,1),(4809,'051005','B243','Bucine',52,1),(4810,'051006','B670','Capolona',52,1),(4811,'051007','B693','Caprese Michelangelo',52,1),(4812,'051008','C102','Castel Focognano',52,1),(4813,'051009','C112','Castelfranco di Sopra',52,1),(4814,'051010','C263','Castel San Niccolò',52,1),(4815,'051011','C318','Castiglion Fibocchi',52,1),(4816,'051012','C319','Castiglion Fiorentino',52,1),(4817,'051013','C407','Cavriglia',52,1),(4818,'051014','C648','Chitignano',52,1),(4819,'051015','C663','Chiusi della Verna',52,1),(4820,'051016','C774','Civitella in Val di Chiana',52,1),(4821,'051017','D077','Cortona',52,1),(4822,'051018','D649','Foiano della Chiana',52,1),(4823,'051019','E468','Laterina',52,1),(4824,'051020','E693','Loro Ciuffenna',52,1),(4825,'051021','E718','Lucignano',52,1),(4826,'051022','E933','Marciano della Chiana',52,1),(4827,'051023','F565','Montemignaio',52,1),(4828,'051024','F594','Monterchi',52,1),(4829,'051025','F628','Monte San Savino',52,1),(4830,'051026','F656','Montevarchi',52,1),(4831,'051027','G139','Ortignano Raggiolo',52,1),(4832,'051028','G451','Pergine Valdarno',52,1),(4833,'051029','G552','Pian di Sco',52,1),(4834,'051030','G653','Pieve Santo Stefano',52,1),(4835,'051031','G879','Poppi',52,1),(4836,'051032','H008','Pratovecchio',52,1),(4837,'051033','H901','San Giovanni Valdarno',52,1),(4838,'051034','I155','Sansepolcro',52,1),(4839,'051035','I681','Sestino',52,1),(4840,'051036','I952','Stia',52,1),(4841,'051037','I991','Subbiano',52,1),(4842,'051038','L038','Talla',52,1),(4843,'051039','L123','Terranuova Bracciolini',52,1),(4844,'052001','A006','Abbadia San Salvatore',53,1),(4845,'052002','A461','Asciano',53,1),(4846,'052003','B269','Buonconvento',53,1),(4847,'052004','B984','Casole d\'Elsa',53,1),(4848,'052005','C172','Castellina in Chianti',53,1),(4849,'052006','C227','Castelnuovo Berardenga',53,1),(4850,'052007','C313','Castiglione d\'Orcia',53,1),(4851,'052008','C587','Cetona',53,1),(4852,'052009','C608','Chianciano Terme',53,1),(4853,'052010','C661','Chiusdino',53,1),(4854,'052011','C662','Chiusi',53,1),(4855,'052012','C847','Colle di Val d\'Elsa',53,1),(4856,'052013','D858','Gaiole in Chianti',53,1),(4857,'052014','F402','Montalcino',53,1),(4858,'052015','F592','Montepulciano',53,1),(4859,'052016','F598','Monteriggioni',53,1),(4860,'052017','F605','Monteroni d\'Arbia',53,1),(4861,'052018','F676','Monticiano',53,1),(4862,'052019','F815','Murlo',53,1),(4863,'052020','G547','Piancastagnaio',53,1),(4864,'052021','G602','Pienza',53,1),(4865,'052022','G752','Poggibonsi',53,1),(4866,'052023','H153','Radda in Chianti',53,1),(4867,'052024','H156','Radicofani',53,1),(4868,'052025','H157','Radicondoli',53,1),(4869,'052026','H185','Rapolano Terme',53,1),(4870,'052027','H790','San Casciano dei Bagni',53,1),(4871,'052028','H875','San Gimignano',53,1),(4872,'052029','H911','San Giovanni d\'Asso',53,1),(4873,'052030','I135','San Quirico d\'Orcia',53,1),(4874,'052031','I445','Sarteano',53,1),(4875,'052032','I726','Siena',53,1),(4876,'052033','A468','Sinalunga',53,1),(4877,'052034','I877','Sovicille',53,1),(4878,'052035','L303','Torrita di Siena',53,1),(4879,'052036','L384','Trequanda',53,1),(4880,'053001','A369','Arcidosso',54,1),(4881,'053002','B497','Campagnatico',54,1),(4882,'053003','B646','Capalbio',54,1),(4883,'053004','C085','Castel del Piano',54,1),(4884,'053005','C147','Castell\'Azzara',54,1),(4885,'053006','C310','Castiglione della Pescaia',54,1),(4886,'053007','C705','Cinigiano',54,1),(4887,'053008','C782','Civitella Paganico',54,1),(4888,'053009','D656','Follonica',54,1),(4889,'053010','D948','Gavorrano',54,1),(4890,'053011','E202','Grosseto',54,1),(4891,'053012','E348','Isola del Giglio',54,1),(4892,'053013','E810','Magliano in Toscana',54,1),(4893,'053014','E875','Manciano',54,1),(4894,'053015','F032','Massa Marittima',54,1),(4895,'053016','F437','Monte Argentario',54,1),(4896,'053017','F677','Montieri',54,1),(4897,'053018','G088','Orbetello',54,1),(4898,'053019','G716','Pitigliano',54,1),(4899,'053020','H417','Roccalbegna',54,1),(4900,'053021','H449','Roccastrada',54,1),(4901,'053022','I187','Santa Fiora',54,1),(4902,'053023','I504','Scansano',54,1),(4903,'053024','I510','Scarlino',54,1),(4904,'053025','I571','Seggiano',54,1),(4905,'053026','I841','Sorano',54,1),(4906,'053027','F612','Monterotondo Marittimo',54,1),(4907,'053028','I601','Semproniano',54,1),(4908,'054001','A475','Assisi',55,1),(4909,'054002','A710','Bastia Umbra',55,1),(4910,'054003','A832','Bettona',55,1),(4911,'054004','A835','Bevagna',55,1),(4912,'054005','B504','Campello sul Clitunno',55,1),(4913,'054006','B609','Cannara',55,1),(4914,'054007','B948','Cascia',55,1),(4915,'054008','C252','Castel Ritaldi',55,1),(4916,'054009','C309','Castiglione del Lago',55,1),(4917,'054010','C527','Cerreto di Spoleto',55,1),(4918,'054011','C742','Citerna',55,1),(4919,'054012','C744','Città della Pieve',55,1),(4920,'054013','C745','Città di Castello',55,1),(4921,'054014','C845','Collazzone',55,1),(4922,'054015','C990','Corciano',55,1),(4923,'054016','D108','Costacciaro',55,1),(4924,'054017','D279','Deruta',55,1),(4925,'054018','D653','Foligno',55,1),(4926,'054019','D745','Fossato di Vico',55,1),(4927,'054020','D787','Fratta Todina',55,1),(4928,'054021','E012','Giano dell\'Umbria',55,1),(4929,'054022','E229','Gualdo Cattaneo',55,1),(4930,'054023','E230','Gualdo Tadino',55,1),(4931,'054024','E256','Gubbio',55,1),(4932,'054025','E613','Lisciano Niccone',55,1),(4933,'054026','E805','Magione',55,1),(4934,'054027','E975','Marsciano',55,1),(4935,'054028','F024','Massa Martana',55,1),(4936,'054029','F456','Monte Castello di Vibio',55,1),(4937,'054030','F492','Montefalco',55,1),(4938,'054031','F540','Monteleone di Spoleto',55,1),(4939,'054032','F629','Monte Santa Maria Tiberina',55,1),(4940,'054033','F685','Montone',55,1),(4941,'054034','F911','Nocera Umbra',55,1),(4942,'054035','F935','Norcia',55,1),(4943,'054036','G212','Paciano',55,1),(4944,'054037','G308','Panicale',55,1),(4945,'054038','G359','Passignano sul Trasimeno',55,1),(4946,'054039','G478','Perugia',55,1),(4947,'054040','G601','Piegaro',55,1),(4948,'054041','G618','Pietralunga',55,1),(4949,'054042','G758','Poggiodomo',55,1),(4950,'054043','H015','Preci',55,1),(4951,'054044','H935','San Giustino',55,1),(4952,'054045','I263','Sant\'Anatolia di Narco',55,1),(4953,'054046','I522','Scheggia e Pascelupo',55,1),(4954,'054047','I523','Scheggino',55,1),(4955,'054048','I585','Sellano',55,1),(4956,'054049','I727','Sigillo',55,1),(4957,'054050','I888','Spello',55,1),(4958,'054051','I921','Spoleto',55,1),(4959,'054052','L188','Todi',55,1),(4960,'054053','L216','Torgiano',55,1),(4961,'054054','L397','Trevi',55,1),(4962,'054055','L466','Tuoro sul Trasimeno',55,1),(4963,'054056','D786','Umbertide',55,1),(4964,'054057','L573','Valfabbrica',55,1),(4965,'054058','L627','Vallo di Nera',55,1),(4966,'054059','L653','Valtopina',55,1),(4967,'055001','A045','Acquasparta',56,1),(4968,'055002','A207','Allerona',56,1),(4969,'055003','A242','Alviano',56,1),(4970,'055004','A262','Amelia',56,1),(4971,'055005','A439','Arrone',56,1),(4972,'055006','A490','Attigliano',56,1),(4973,'055007','A691','Baschi',56,1),(4974,'055008','B446','Calvi dell\'Umbria',56,1),(4975,'055009','C117','Castel Giorgio',56,1),(4976,'055010','C289','Castel Viscardo',56,1),(4977,'055011','D454','Fabro',56,1),(4978,'055012','D538','Ferentillo',56,1),(4979,'055013','D570','Ficulle',56,1),(4980,'055014','E045','Giove',56,1),(4981,'055015','E241','Guardea',56,1),(4982,'055016','E729','Lugnano in Teverina',56,1),(4983,'055017','F457','Montecastrilli',56,1),(4984,'055018','F462','Montecchio',56,1),(4985,'055019','F510','Montefranco',56,1),(4986,'055020','F513','Montegabbione',56,1),(4987,'055021','F543','Monteleone d\'Orvieto',56,1),(4988,'055022','F844','Narni',56,1),(4989,'055023','G148','Orvieto',56,1),(4990,'055024','G189','Otricoli',56,1),(4991,'055025','G344','Parrano',56,1),(4992,'055026','G432','Penna in Teverina',56,1),(4993,'055027','G790','Polino',56,1),(4994,'055028','G881','Porano',56,1),(4995,'055029','H857','San Gemini',56,1),(4996,'055030','I381','San Venanzo',56,1),(4997,'055031','I981','Stroncone',56,1),(4998,'055032','L117','Terni',56,1),(4999,'055033','M258','Avigliano Umbro',56,1),(5000,'056001','A040','Acquapendente',57,1),(5001,'056002','A412','Arlena di Castro',57,1),(5002,'056003','A577','Bagnoregio',57,1),(5003,'056004','A628','Barbarano Romano',57,1),(5004,'056005','A704','Bassano Romano',57,1),(5005,'056006','A706','Bassano in Teverina',57,1),(5006,'056007','A857','Blera',57,1),(5007,'056008','A949','Bolsena',57,1),(5008,'056009','A955','Bomarzo',57,1),(5009,'056010','B388','Calcata',57,1),(5010,'056011','B597','Canepina',57,1),(5011,'056012','B604','Canino',57,1),(5012,'056013','B663','Capodimonte',57,1),(5013,'056014','B688','Capranica',57,1),(5014,'056015','B691','Caprarola',57,1),(5015,'056016','B735','Carbognano',57,1),(5016,'056017','C269','Castel Sant\'Elia',57,1),(5017,'056018','C315','Castiglione in Teverina',57,1),(5018,'056019','C446','Celleno',57,1),(5019,'056020','C447','Cellere',57,1),(5020,'056021','C765','Civita Castellana',57,1),(5021,'056022','C780','Civitella d\'Agliano',57,1),(5022,'056023','C988','Corchiano',57,1),(5023,'056024','D452','Fabrica di Roma',57,1),(5024,'056025','D475','Faleria',57,1),(5025,'056026','D503','Farnese',57,1),(5026,'056027','D870','Gallese',57,1),(5027,'056028','E126','Gradoli',57,1),(5028,'056029','E128','Graffignano',57,1),(5029,'056030','E210','Grotte di Castro',57,1),(5030,'056031','E330','Ischia di Castro',57,1),(5031,'056032','E467','Latera',57,1),(5032,'056033','E713','Lubriano',57,1),(5033,'056034','E978','Marta',57,1),(5034,'056035','F419','Montalto di Castro',57,1),(5035,'056036','F499','Montefiascone',57,1),(5036,'056037','F603','Monte Romano',57,1),(5037,'056038','F606','Monterosi',57,1),(5038,'056039','F868','Nepi',57,1),(5039,'056040','G065','Onano',57,1),(5040,'056041','G111','Oriolo Romano',57,1),(5041,'056042','G135','Orte',57,1),(5042,'056043','G571','Piansano',57,1),(5043,'056044','H071','Proceno',57,1),(5044,'056045','H534','Ronciglione',57,1),(5045,'056046','H913','Villa San Giovanni in Tuscia',57,1),(5046,'056047','H969','San Lorenzo Nuovo',57,1),(5047,'056048','I855','Soriano nel Cimino',57,1),(5048,'056049','L017','Sutri',57,1),(5049,'056050','D024','Tarquinia',57,1),(5050,'056051','L150','Tessennano',57,1),(5051,'056052','L310','Tuscania',57,1),(5052,'056053','L569','Valentano',57,1),(5053,'056054','L612','Vallerano',57,1),(5054,'056055','A701','Vasanello',57,1),(5055,'056056','L713','Vejano',57,1),(5056,'056057','L814','Vetralla',57,1),(5057,'056058','L882','Vignanello',57,1),(5058,'056059','M082','Viterbo',57,1),(5059,'056060','M086','Vitorchiano',57,1),(5060,'057001','A019','Accumoli',58,1),(5061,'057002','A258','Amatrice',58,1),(5062,'057003','A315','Antrodoco',58,1),(5063,'057004','A464','Ascrea',58,1),(5064,'057005','A765','Belmonte in Sabina',58,1),(5065,'057006','A981','Borbona',58,1),(5066,'057007','B008','Borgorose',58,1),(5067,'057008','A996','Borgo Velino',58,1),(5068,'057009','B627','Cantalice',58,1),(5069,'057010','B631','Cantalupo in Sabina',58,1),(5070,'057011','B934','Casaprota',58,1),(5071,'057012','A472','Casperia',58,1),(5072,'057013','C098','Castel di Tora',58,1),(5073,'057014','C224','Castelnuovo di Farfa',58,1),(5074,'057015','C268','Castel Sant\'Angelo',58,1),(5075,'057016','C746','Cittaducale',58,1),(5076,'057017','C749','Cittareale',58,1),(5077,'057018','C841','Collalto Sabino',58,1),(5078,'057019','C857','Colle di Tora',58,1),(5079,'057020','C859','Collegiove',58,1),(5080,'057021','C876','Collevecchio',58,1),(5081,'057022','C880','Colli sul Velino',58,1),(5082,'057023','C946','Concerviano',58,1),(5083,'057024','C959','Configni',58,1),(5084,'057025','C969','Contigliano',58,1),(5085,'057026','D124','Cottanello',58,1),(5086,'057027','D493','Fara in Sabina',58,1),(5087,'057028','D560','Fiamignano',58,1),(5088,'057029','D689','Forano',58,1),(5089,'057030','D785','Frasso Sabino',58,1),(5090,'057031','E160','Greccio',58,1),(5091,'057032','E393','Labro',58,1),(5092,'057033','E535','Leonessa',58,1),(5093,'057034','E681','Longone Sabino',58,1),(5094,'057035','E812','Magliano Sabina',58,1),(5095,'057036','E927','Marcetelli',58,1),(5096,'057037','F193','Micigliano',58,1),(5097,'057038','F319','Mompeo',58,1),(5098,'057039','F430','Montasola',58,1),(5099,'057040','F446','Montebuono',58,1),(5100,'057041','F541','Monteleone Sabino',58,1),(5101,'057042','F579','Montenero Sabino',58,1),(5102,'057043','F619','Monte San Giovanni in Sabina',58,1),(5103,'057044','F687','Montopoli di Sabina',58,1),(5104,'057045','F746','Morro Reatino',58,1),(5105,'057046','F876','Nespolo',58,1),(5106,'057047','B595','Orvinio',58,1),(5107,'057048','G232','Paganico Sabino',58,1),(5108,'057049','G498','Pescorocchiano',58,1),(5109,'057050','G513','Petrella Salto',58,1),(5110,'057051','G756','Poggio Bustone',58,1),(5111,'057052','G757','Poggio Catino',58,1),(5112,'057053','G763','Poggio Mirteto',58,1),(5113,'057054','G764','Poggio Moiano',58,1),(5114,'057055','G765','Poggio Nativo',58,1),(5115,'057056','G770','Poggio San Lorenzo',58,1),(5116,'057057','G934','Posta',58,1),(5117,'057058','G951','Pozzaglia Sabina',58,1),(5118,'057059','H282','Rieti',58,1),(5119,'057060','H354','Rivodutri',58,1),(5120,'057061','H427','Roccantica',58,1),(5121,'057062','H446','Rocca Sinibalda',58,1),(5122,'057063','H713','Salisano',58,1),(5123,'057064','I499','Scandriglia',58,1),(5124,'057065','I581','Selci',58,1),(5125,'057066','I959','Stimigliano',58,1),(5126,'057067','L046','Tarano',58,1),(5127,'057068','L189','Toffia',58,1),(5128,'057069','L293','Torricella in Sabina',58,1),(5129,'057070','L286','Torri in Sabina',58,1),(5130,'057071','G507','Turania',58,1),(5131,'057072','L525','Vacone',58,1),(5132,'057073','L676','Varco Sabino',58,1),(5133,'058001','A062','Affile',59,1),(5134,'058002','A084','Agosta',59,1),(5135,'058003','A132','Albano Laziale',59,1),(5136,'058004','A210','Allumiere',59,1),(5137,'058005','A297','Anguillara Sabazia',59,1),(5138,'058006','A309','Anticoli Corrado',59,1),(5139,'058007','A323','Anzio',59,1),(5140,'058008','A370','Arcinazzo Romano',59,1),(5141,'058009','A401','Ariccia',59,1),(5142,'058010','A446','Arsoli',59,1),(5143,'058011','A449','Artena',59,1),(5144,'058012','A749','Bellegra',59,1),(5145,'058013','B114','Bracciano',59,1),(5146,'058014','B472','Camerata Nuova',59,1),(5147,'058015','B496','Campagnano di Roma',59,1),(5148,'058016','B576','Canale Monterano',59,1),(5149,'058017','B635','Canterano',59,1),(5150,'058018','B649','Capena',59,1),(5151,'058019','B687','Capranica Prenestina',59,1),(5152,'058020','B828','Carpineto Romano',59,1),(5153,'058021','B932','Casape',59,1),(5154,'058022','C116','Castel Gandolfo',59,1),(5155,'058023','C203','Castel Madama',59,1),(5156,'058024','C237','Castelnuovo di Porto',59,1),(5157,'058025','C266','Castel San Pietro Romano',59,1),(5158,'058026','C390','Cave',59,1),(5159,'058027','C518','Cerreto Laziale',59,1),(5160,'058028','C543','Cervara di Roma',59,1),(5161,'058029','C552','Cerveteri',59,1),(5162,'058030','C677','Ciciliano',59,1),(5163,'058031','C702','Cineto Romano',59,1),(5164,'058032','C773','Civitavecchia',59,1),(5165,'058033','C784','Civitella San Paolo',59,1),(5166,'058034','C858','Colleferro',59,1),(5167,'058035','C900','Colonna',59,1),(5168,'058036','D561','Fiano Romano',59,1),(5169,'058037','D586','Filacciano',59,1),(5170,'058038','D707','Formello',59,1),(5171,'058039','D773','Frascati',59,1),(5172,'058040','D875','Gallicano nel Lazio',59,1),(5173,'058041','D945','Gavignano',59,1),(5174,'058042','D964','Genazzano',59,1),(5175,'058043','D972','Genzano di Roma',59,1),(5176,'058044','D978','Gerano',59,1),(5177,'058045','E091','Gorga',59,1),(5178,'058046','E204','Grottaferrata',59,1),(5179,'058047','E263','Guidonia Montecelio',59,1),(5180,'058048','E382','Jenne',59,1),(5181,'058049','E392','Labico',59,1),(5182,'058050','C767','Lanuvio',59,1),(5183,'058051','E576','Licenza',59,1),(5184,'058052','E813','Magliano Romano',59,1),(5185,'058053','B632','Mandela',59,1),(5186,'058054','E900','Manziana',59,1),(5187,'058055','E908','Marano Equo',59,1),(5188,'058056','E924','Marcellina',59,1),(5189,'058057','E958','Marino',59,1),(5190,'058058','F064','Mazzano Romano',59,1),(5191,'058059','F127','Mentana',59,1),(5192,'058060','F477','Monte Compatri',59,1),(5193,'058061','F504','Monteflavio',59,1),(5194,'058062','F534','Montelanico',59,1),(5195,'058063','F545','Montelibretti',59,1),(5196,'058064','F590','Monte Porzio Catone',59,1),(5197,'058065','F611','Monterotondo',59,1),(5198,'058066','F692','Montorio Romano',59,1),(5199,'058067','F730','Moricone',59,1),(5200,'058068','F734','Morlupo',59,1),(5201,'058069','F857','Nazzano',59,1),(5202,'058070','F865','Nemi',59,1),(5203,'058071','F871','Nerola',59,1),(5204,'058072','F880','Nettuno',59,1),(5205,'058073','G022','Olevano Romano',59,1),(5206,'058074','G274','Palestrina',59,1),(5207,'058075','G293','Palombara Sabina',59,1),(5208,'058076','G444','Percile',59,1),(5209,'058077','G704','Pisoniano',59,1),(5210,'058078','G784','Poli',59,1),(5211,'058079','G811','Pomezia',59,1),(5212,'058080','G874','Ponzano Romano',59,1),(5213,'058081','H267','Riano',59,1),(5214,'058082','H288','Rignano Flaminio',59,1),(5215,'058083','H300','Riofreddo',59,1),(5216,'058084','H387','Rocca Canterano',59,1),(5217,'058085','H401','Rocca di Cave',59,1),(5218,'058086','H404','Rocca di Papa',59,1),(5219,'058087','H411','Roccagiovine',59,1),(5220,'058088','H432','Rocca Priora',59,1),(5221,'058089','H441','Rocca Santo Stefano',59,1),(5222,'058090','H494','Roiate',59,1),(5223,'058091','H501','Roma',59,1),(5224,'058092','H618','Roviano',59,1),(5225,'058093','H658','Sacrofano',59,1),(5226,'058094','H745','Sambuci',59,1),(5227,'058095','H942','San Gregorio da Sassola',59,1),(5228,'058096','I125','San Polo dei Cavalieri',59,1),(5229,'058097','I255','Santa Marinella',59,1),(5230,'058098','I284','Sant\'Angelo Romano',59,1),(5231,'058099','I352','Sant\'Oreste',59,1),(5232,'058100','I400','San Vito Romano',59,1),(5233,'058101','I424','Saracinesco',59,1),(5234,'058102','I573','Segni',59,1),(5235,'058103','I992','Subiaco',59,1),(5236,'058104','L182','Tivoli',59,1),(5237,'058105','L192','Tolfa',59,1),(5238,'058106','L302','Torrita Tiberina',59,1),(5239,'058107','L401','Trevignano Romano',59,1),(5240,'058108','L611','Vallepietra',59,1),(5241,'058109','L625','Vallinfreda',59,1),(5242,'058110','L639','Valmontone',59,1),(5243,'058111','L719','Velletri',59,1),(5244,'058112','L851','Vicovaro',59,1),(5245,'058113','M095','Vivaro Romano',59,1),(5246,'058114','M141','Zagarolo',59,1),(5247,'058115','M207','Lariano',59,1),(5248,'058116','M212','Ladispoli',59,1),(5249,'058117','M213','Ardea',59,1),(5250,'058118','M272','Ciampino',59,1),(5251,'058119','M295','San Cesareo',59,1),(5252,'058120','M297','Fiumicino',59,1),(5253,'058122','M309','Fonte Nuova',59,1),(5254,'059001','A341','Aprilia',60,1),(5255,'059002','A707','Bassiano',60,1),(5256,'059003','B527','Campodimele',60,1),(5257,'059004','C104','Castelforte',60,1),(5258,'059005','C740','Cisterna di Latina',60,1),(5259,'059006','D003','Cori',60,1),(5260,'059007','D662','Fondi',60,1),(5261,'059008','D708','Formia',60,1),(5262,'059009','D843','Gaeta',60,1),(5263,'059010','E375','Itri',60,1),(5264,'059011','E472','Latina',60,1),(5265,'059012','E527','Lenola',60,1),(5266,'059013','E798','Maenza',60,1),(5267,'059014','F224','Minturno',60,1),(5268,'059015','F616','Monte San Biagio',60,1),(5269,'059016','F937','Norma',60,1),(5270,'059017','G865','Pontinia',60,1),(5271,'059018','G871','Ponza',60,1),(5272,'059019','G698','Priverno',60,1),(5273,'059020','H076','Prossedi',60,1),(5274,'059021','H413','Roccagorga',60,1),(5275,'059022','H421','Rocca Massima',60,1),(5276,'059023','H444','Roccasecca dei Volsci',60,1),(5277,'059024','H647','Sabaudia',60,1),(5278,'059025','H836','San Felice Circeo',60,1),(5279,'059026','I339','Santi Cosma e Damiano',60,1),(5280,'059027','I634','Sermoneta',60,1),(5281,'059028','I712','Sezze',60,1),(5282,'059029','I832','Sonnino',60,1),(5283,'059030','I892','Sperlonga',60,1),(5284,'059031','I902','Spigno Saturnia',60,1),(5285,'059032','L120','Terracina',60,1),(5286,'059033','L742','Ventotene',60,1),(5287,'060001','A032','Acquafondata',61,1),(5288,'060002','A054','Acuto',61,1),(5289,'060003','A123','Alatri',61,1),(5290,'060004','A244','Alvito',61,1),(5291,'060005','A256','Amaseno',61,1),(5292,'060006','A269','Anagni',61,1),(5293,'060007','A348','Aquino',61,1),(5294,'060008','A363','Arce',61,1),(5295,'060009','A421','Arnara',61,1),(5296,'060010','A433','Arpino',61,1),(5297,'060011','A486','Atina',61,1),(5298,'060012','A502','Ausonia',61,1),(5299,'060013','A763','Belmonte Castello',61,1),(5300,'060014','A720','Boville Ernica',61,1),(5301,'060015','B195','Broccostella',61,1),(5302,'060016','B543','Campoli Appennino',61,1),(5303,'060017','B862','Casalattico',61,1),(5304,'060018','B919','Casalvieri',61,1),(5305,'060019','C034','Cassino',61,1),(5306,'060020','C177','Castelliri',61,1),(5307,'060021','C223','Castelnuovo Parano',61,1),(5308,'060022','C340','Castrocielo',61,1),(5309,'060023','C338','Castro dei Volsci',61,1),(5310,'060024','C413','Ceccano',61,1),(5311,'060025','C479','Ceprano',61,1),(5312,'060026','C545','Cervaro',61,1),(5313,'060027','C836','Colfelice',61,1),(5314,'060028','C864','Collepardo',61,1),(5315,'060029','C870','Colle San Magno',61,1),(5316,'060030','C998','Coreno Ausonio',61,1),(5317,'060031','D440','Esperia',61,1),(5318,'060032','D483','Falvaterra',61,1),(5319,'060033','D539','Ferentino',61,1),(5320,'060034','D591','Filettino',61,1),(5321,'060035','A310','Fiuggi',61,1),(5322,'060036','D667','Fontana Liri',61,1),(5323,'060037','D682','Fontechiari',61,1),(5324,'060038','D810','Frosinone',61,1),(5325,'060039','D819','Fumone',61,1),(5326,'060040','D881','Gallinaro',61,1),(5327,'060041','E057','Giuliano di Roma',61,1),(5328,'060042','E236','Guarcino',61,1),(5329,'060043','E340','Isola del Liri',61,1),(5330,'060044','F620','Monte San Giovanni Campano',61,1),(5331,'060045','F740','Morolo',61,1),(5332,'060046','G276','Paliano',61,1),(5333,'060047','G362','Pastena',61,1),(5334,'060048','G374','Patrica',61,1),(5335,'060049','G500','Pescosolido',61,1),(5336,'060050','G591','Picinisco',61,1),(5337,'060051','G592','Pico',61,1),(5338,'060052','G598','Piedimonte San Germano',61,1),(5339,'060053','G659','Piglio',61,1),(5340,'060054','G662','Pignataro Interamna',61,1),(5341,'060055','G749','Pofi',61,1),(5342,'060056','G838','Pontecorvo',61,1),(5343,'060057','G935','Posta Fibreno',61,1),(5344,'060058','H324','Ripi',61,1),(5345,'060059','H393','Rocca d\'Arce',61,1),(5346,'060060','H443','Roccasecca',61,1),(5347,'060061','H779','San Biagio Saracinisco',61,1),(5348,'060062','H824','San Donato Val di Comino',61,1),(5349,'060063','H880','San Giorgio a Liri',61,1),(5350,'060064','H917','San Giovanni Incarico',61,1),(5351,'060065','I256','Sant\'Ambrogio sul Garigliano',61,1),(5352,'060066','I265','Sant\'Andrea del Garigliano',61,1),(5353,'060067','I302','Sant\'Apollinare',61,1),(5354,'060068','I321','Sant\'Elia Fiumerapido',61,1),(5355,'060069','I351','Santopadre',61,1),(5356,'060070','I408','San Vittore del Lazio',61,1),(5357,'060071','I669','Serrone',61,1),(5358,'060072','I697','Settefrati',61,1),(5359,'060073','I716','Sgurgola',61,1),(5360,'060074','I838','Sora',61,1),(5361,'060075','I973','Strangolagalli',61,1),(5362,'060076','L009','Supino',61,1),(5363,'060077','L105','Terelle',61,1),(5364,'060078','L243','Torre Cajetani',61,1),(5365,'060079','L290','Torrice',61,1),(5366,'060080','L398','Trevi nel Lazio',61,1),(5367,'060081','L437','Trivigliano',61,1),(5368,'060082','L598','Vallecorsa',61,1),(5369,'060083','L605','Vallemaio',61,1),(5370,'060084','L614','Vallerotonda',61,1),(5371,'060085','L780','Veroli',61,1),(5372,'060086','L836','Vicalvi',61,1),(5373,'060087','L843','Vico nel Lazio',61,1),(5374,'060088','A081','Villa Latina',61,1),(5375,'060089','L905','Villa Santa Lucia',61,1),(5376,'060090','I364','Villa Santo Stefano',61,1),(5377,'060091','M083','Viticuso',61,1),(5378,'061001','A106','Ailano',62,1),(5379,'061002','A200','Alife',62,1),(5380,'061003','A243','Alvignano',62,1),(5381,'061004','A403','Arienzo',62,1),(5382,'061005','A512','Aversa',62,1),(5383,'061006','A579','Baia e Latina',62,1),(5384,'061007','A755','Bellona',62,1),(5385,'061008','B361','Caianello',62,1),(5386,'061009','B362','Caiazzo',62,1),(5387,'061010','B445','Calvi Risorta',62,1),(5388,'061011','B477','Camigliano',62,1),(5389,'061012','B581','Cancello ed Arnone',62,1),(5390,'061013','B667','Capodrise',62,1),(5391,'061014','B704','Capriati a Volturno',62,1),(5392,'061015','B715','Capua',62,1),(5393,'061016','B779','Carinaro',62,1),(5394,'061017','B781','Carinola',62,1),(5395,'061018','B860','Casagiove',62,1),(5396,'061019','B872','Casal di Principe',62,1),(5397,'061020','B916','Casaluce',62,1),(5398,'061021','B935','Casapulla',62,1),(5399,'061022','B963','Caserta',62,1),(5400,'061023','B494','Castel Campagnano',62,1),(5401,'061024','C097','Castel di Sasso',62,1),(5402,'061025','C178','Castello del Matese',62,1),(5403,'061026','C211','Castel Morrone',62,1),(5404,'061027','C291','Castel Volturno',62,1),(5405,'061028','C558','Cervino',62,1),(5406,'061029','C561','Cesa',62,1),(5407,'061030','C716','Ciorlano',62,1),(5408,'061031','C939','Conca della Campania',62,1),(5409,'061032','D228','Curti',62,1),(5410,'061033','D361','Dragoni',62,1),(5411,'061034','D683','Fontegreca',62,1),(5412,'061035','D709','Formicola',62,1),(5413,'061036','D769','Francolise',62,1),(5414,'061037','D799','Frignano',62,1),(5415,'061038','D884','Gallo Matese',62,1),(5416,'061039','D886','Galluccio',62,1),(5417,'061040','E011','Giano Vetusto',62,1),(5418,'061041','E039','Gioia Sannitica',62,1),(5419,'061042','E158','Grazzanise',62,1),(5420,'061043','E173','Gricignano di Aversa',62,1),(5421,'061044','E554','Letino',62,1),(5422,'061045','E570','Liberi',62,1),(5423,'061046','E754','Lusciano',62,1),(5424,'061047','E784','Macerata Campania',62,1),(5425,'061048','E791','Maddaloni',62,1),(5426,'061049','E932','Marcianise',62,1),(5427,'061050','E998','Marzano Appio',62,1),(5428,'061051','F203','Mignano Monte Lungo',62,1),(5429,'061052','F352','Mondragone',62,1),(5430,'061053','G130','Orta di Atella',62,1),(5431,'061054','G333','Parete',62,1),(5432,'061055','G364','Pastorano',62,1),(5433,'061056','G541','Piana di Monte Verna',62,1),(5434,'061057','G596','Piedimonte Matese',62,1),(5435,'061058','G620','Pietramelara',62,1),(5436,'061059','G630','Pietravairano',62,1),(5437,'061060','G661','Pignataro Maggiore',62,1),(5438,'061061','G849','Pontelatone',62,1),(5439,'061062','G903','Portico di Caserta',62,1),(5440,'061063','G991','Prata Sannita',62,1),(5441,'061064','G995','Pratella',62,1),(5442,'061065','H045','Presenzano',62,1),(5443,'061066','H202','Raviscanina',62,1),(5444,'061067','H210','Recale',62,1),(5445,'061068','H268','Riardo',62,1),(5446,'061069','H398','Rocca d\'Evandro',62,1),(5447,'061070','H423','Roccamonfina',62,1),(5448,'061071','H436','Roccaromana',62,1),(5449,'061072','H459','Rocchetta e Croce',62,1),(5450,'061073','H165','Ruviano',62,1),(5451,'061074','H798','San Cipriano d\'Aversa',62,1),(5452,'061075','H834','San Felice a Cancello',62,1),(5453,'061076','H939','San Gregorio Matese',62,1),(5454,'061077','H978','San Marcellino',62,1),(5455,'061078','I056','San Nicola la Strada',62,1),(5456,'061079','I113','San Pietro Infine',62,1),(5457,'061080','I130','San Potito Sannitico',62,1),(5458,'061081','I131','San Prisco',62,1),(5459,'061082','I233','Santa Maria a Vico',62,1),(5460,'061083','I234','Santa Maria Capua Vetere',62,1),(5461,'061084','I247','Santa Maria la Fossa',62,1),(5462,'061085','I261','San Tammaro',62,1),(5463,'061086','I273','Sant\'Angelo d\'Alife',62,1),(5464,'061087','I306','Sant\'Arpino',62,1),(5465,'061088','I676','Sessa Aurunca',62,1),(5466,'061089','I885','Sparanise',62,1),(5467,'061090','I993','Succivo',62,1),(5468,'061091','L083','Teano',62,1),(5469,'061092','L155','Teverola',62,1),(5470,'061093','L205','Tora e Piccilli',62,1),(5471,'061094','L379','Trentola-Ducenta',62,1),(5472,'061095','L540','Vairano Patenora',62,1),(5473,'061096','L594','Valle Agricola',62,1),(5474,'061097','L591','Valle di Maddaloni',62,1),(5475,'061098','D801','Villa di Briano',62,1),(5476,'061099','L844','Villa Literno',62,1),(5477,'061100','M092','Vitulazio',62,1),(5478,'061101','D471','Falciano del Massico',62,1),(5479,'061102','M262','Cellole',62,1),(5480,'061103','M260','Casapesenna',62,1),(5481,'061104','F043','San Marco Evangelista',62,1),(5482,'062001','A110','Airola',63,1),(5483,'062002','A265','Amorosi',63,1),(5484,'062003','A328','Apice',63,1),(5485,'062004','A330','Apollosa',63,1),(5486,'062005','A431','Arpaia',63,1),(5487,'062006','A432','Arpaise',63,1),(5488,'062007','A696','Baselice',63,1),(5489,'062008','A783','Benevento',63,1),(5490,'062009','A970','Bonea',63,1),(5491,'062010','B239','Bucciano',63,1),(5492,'062011','B267','Buonalbergo',63,1),(5493,'062012','B444','Calvi',63,1),(5494,'062013','B541','Campolattaro',63,1),(5495,'062014','B542','Campoli del Monte Taburno',63,1),(5496,'062015','B873','Casalduni',63,1),(5497,'062016','C106','Castelfranco in Miscano',63,1),(5498,'062017','C245','Castelpagano',63,1),(5499,'062018','C250','Castelpoto',63,1),(5500,'062019','C280','Castelvenere',63,1),(5501,'062020','C284','Castelvetere in Val Fortore',63,1),(5502,'062021','C359','Cautano',63,1),(5503,'062022','C476','Ceppaloni',63,1),(5504,'062023','C525','Cerreto Sannita',63,1),(5505,'062024','C719','Circello',63,1),(5506,'062025','C846','Colle Sannita',63,1),(5507,'062026','D230','Cusano Mutri',63,1),(5508,'062027','D380','Dugenta',63,1),(5509,'062028','D386','Durazzano',63,1),(5510,'062029','D469','Faicchio',63,1),(5511,'062030','D644','Foglianise',63,1),(5512,'062031','D650','Foiano di Val Fortore',63,1),(5513,'062032','D693','Forchia',63,1),(5514,'062033','D755','Fragneto l\'Abate',63,1),(5515,'062034','D756','Fragneto Monforte',63,1),(5516,'062035','D784','Frasso Telesino',63,1),(5517,'062036','E034','Ginestra degli Schiavoni',63,1),(5518,'062037','E249','Guardia Sanframondi',63,1),(5519,'062038','E589','Limatola',63,1),(5520,'062039','F113','Melizzano',63,1),(5521,'062040','F274','Moiano',63,1),(5522,'062041','F287','Molinara',63,1),(5523,'062042','F494','Montefalcone di Val Fortore',63,1),(5524,'062043','F636','Montesarchio',63,1),(5525,'062044','F717','Morcone',63,1),(5526,'062045','G227','Paduli',63,1),(5527,'062046','G243','Pago Veiano',63,1),(5528,'062047','G311','Pannarano',63,1),(5529,'062048','G318','Paolisi',63,1),(5530,'062049','G386','Paupisi',63,1),(5531,'062050','G494','Pesco Sannita',63,1),(5532,'062051','G626','Pietraroja',63,1),(5533,'062052','G631','Pietrelcina',63,1),(5534,'062053','G827','Ponte',63,1),(5535,'062054','G848','Pontelandolfo',63,1),(5536,'062055','H087','Puglianello',63,1),(5537,'062056','H227','Reino',63,1),(5538,'062057','H764','San Bartolomeo in Galdo',63,1),(5539,'062058','H894','San Giorgio del Sannio',63,1),(5540,'062059','H898','San Giorgio La Molara',63,1),(5541,'062060','H953','San Leucio del Sannio',63,1),(5542,'062061','H955','San Lorenzello',63,1),(5543,'062062','H967','San Lorenzo Maggiore',63,1),(5544,'062063','H973','San Lupo',63,1),(5545,'062064','H984','San Marco dei Cavoti',63,1),(5546,'062065','I002','San Martino Sannita',63,1),(5547,'062066','I049','San Nazzaro',63,1),(5548,'062067','I062','San Nicola Manfredi',63,1),(5549,'062068','I145','San Salvatore Telesino',63,1),(5550,'062069','I179','Santa Croce del Sannio',63,1),(5551,'062070','I197','Sant\'Agata de\' Goti',63,1),(5552,'062071','I277','Sant\'Angelo a Cupolo',63,1),(5553,'062072','I455','Sassinoro',63,1),(5554,'062073','I809','Solopaca',63,1),(5555,'062074','L086','Telese Terme',63,1),(5556,'062075','L185','Tocco Caudio',63,1),(5557,'062076','L254','Torrecuso',63,1),(5558,'062077','M093','Vitulano',63,1),(5559,'062078','F557','Sant\'Arcangelo Trimonte',63,1),(5560,'063001','A024','Acerra',64,1),(5561,'063002','A064','Afragola',64,1),(5562,'063003','A068','Agerola',64,1),(5563,'063004','A268','Anacapri',64,1),(5564,'063005','A455','Arzano',64,1),(5565,'063006','A535','Bacoli',64,1),(5566,'063007','A617','Barano d\'Ischia',64,1),(5567,'063008','B076','Boscoreale',64,1),(5568,'063009','B077','Boscotrecase',64,1),(5569,'063010','B227','Brusciano',64,1),(5570,'063011','B371','Caivano',64,1),(5571,'063012','B452','Calvizzano',64,1),(5572,'063013','B565','Camposano',64,1),(5573,'063014','B696','Capri',64,1),(5574,'063015','B740','Carbonara di Nola',64,1),(5575,'063016','B759','Cardito',64,1),(5576,'063017','B905','Casalnuovo di Napoli',64,1),(5577,'063018','B922','Casamarciano',64,1),(5578,'063019','B924','Casamicciola Terme',64,1),(5579,'063020','B925','Casandrino',64,1),(5580,'063021','B946','Casavatore',64,1),(5581,'063022','B980','Casola di Napoli',64,1),(5582,'063023','B990','Casoria',64,1),(5583,'063024','C129','Castellammare di Stabia',64,1),(5584,'063025','C188','Castello di Cisterna',64,1),(5585,'063026','C495','Cercola',64,1),(5586,'063027','C675','Cicciano',64,1),(5587,'063028','C697','Cimitile',64,1),(5588,'063029','C929','Comiziano',64,1),(5589,'063030','D170','Crispano',64,1),(5590,'063031','D702','Forio',64,1),(5591,'063032','D789','Frattamaggiore',64,1),(5592,'063033','D790','Frattaminore',64,1),(5593,'063034','E054','Giugliano in Campania',64,1),(5594,'063035','E131','Gragnano',64,1),(5595,'063036','E224','Grumo Nevano',64,1),(5596,'063037','E329','Ischia',64,1),(5597,'063038','E396','Lacco Ameno',64,1),(5598,'063039','E557','Lettere',64,1),(5599,'063040','E620','Liveri',64,1),(5600,'063041','E906','Marano di Napoli',64,1),(5601,'063042','E954','Mariglianella',64,1),(5602,'063043','E955','Marigliano',64,1),(5603,'063044','F030','Massa Lubrense',64,1),(5604,'063045','F111','Melito di Napoli',64,1),(5605,'063046','F162','Meta',64,1),(5606,'063047','F488','Monte di Procida',64,1),(5607,'063048','F799','Mugnano di Napoli',64,1),(5608,'063049','F839','Napoli',64,1),(5609,'063050','F924','Nola',64,1),(5610,'063051','G190','Ottaviano',64,1),(5611,'063052','G283','Palma Campania',64,1),(5612,'063053','G568','Piano di Sorrento',64,1),(5613,'063054','G670','Pimonte',64,1),(5614,'063055','G762','Poggiomarino',64,1),(5615,'063056','G795','Pollena Trocchia',64,1),(5616,'063057','G812','Pomigliano d\'Arco',64,1),(5617,'063058','G813','Pompei',64,1),(5618,'063059','G902','Portici',64,1),(5619,'063060','G964','Pozzuoli',64,1),(5620,'063061','H072','Procida',64,1),(5621,'063062','H101','Qualiano',64,1),(5622,'063063','H114','Quarto',64,1),(5623,'063064','H243','Ercolano',64,1),(5624,'063065','H433','Roccarainola',64,1),(5625,'063066','H860','San Gennaro Vesuviano',64,1),(5626,'063067','H892','San Giorgio a Cremano',64,1),(5627,'063068','H931','San Giuseppe Vesuviano',64,1),(5628,'063069','I073','San Paolo Bel Sito',64,1),(5629,'063070','I151','San Sebastiano al Vesuvio',64,1),(5630,'063071','I208','Sant\'Agnello',64,1),(5631,'063072','I262','Sant\'Anastasia',64,1),(5632,'063073','I293','Sant\'Antimo',64,1),(5633,'063074','I300','Sant\'Antonio Abate',64,1),(5634,'063075','I391','San Vitaliano',64,1),(5635,'063076','I469','Saviano',64,1),(5636,'063077','I540','Scisciano',64,1),(5637,'063078','I652','Serrara Fontana',64,1),(5638,'063079','I820','Somma Vesuviana',64,1),(5639,'063080','I862','Sorrento',64,1),(5640,'063081','I978','Striano',64,1),(5641,'063082','L142','Terzigno',64,1),(5642,'063083','L245','Torre Annunziata',64,1),(5643,'063084','L259','Torre del Greco',64,1),(5644,'063085','L460','Tufino',64,1),(5645,'063086','L845','Vico Equense',64,1),(5646,'063087','G309','Villaricca',64,1),(5647,'063088','M072','Visciano',64,1),(5648,'063089','M115','Volla',64,1),(5649,'063090','M273','Santa Maria la Carità',64,1),(5650,'063091','M280','Trecase',64,1),(5651,'063092','M289','Massa di Somma',64,1),(5652,'064001','A101','Aiello del Sabato',65,1),(5653,'064002','A228','Altavilla Irpina',65,1),(5654,'064003','A284','Andretta',65,1),(5655,'064004','A347','Aquilonia',65,1),(5656,'064005','A399','Ariano Irpino',65,1),(5657,'064006','A489','Atripalda',65,1),(5658,'064007','A508','Avella',65,1),(5659,'064008','A509','Avellino',65,1),(5660,'064009','A566','Bagnoli Irpino',65,1),(5661,'064010','A580','Baiano',65,1),(5662,'064011','A881','Bisaccia',65,1),(5663,'064012','A975','Bonito',65,1),(5664,'064013','B367','Cairano',65,1),(5665,'064014','B374','Calabritto',65,1),(5666,'064015','B415','Calitri',65,1),(5667,'064016','B590','Candida',65,1),(5668,'064017','B674','Caposele',65,1),(5669,'064018','B706','Capriglia Irpina',65,1),(5670,'064019','B776','Carife',65,1),(5671,'064020','B866','Casalbore',65,1),(5672,'064021','B997','Cassano Irpino',65,1),(5673,'064022','C058','Castel Baronia',65,1),(5674,'064023','C105','Castelfranci',65,1),(5675,'064024','C283','Castelvetere sul Calore',65,1),(5676,'064025','C557','Cervinara',65,1),(5677,'064026','C576','Cesinali',65,1),(5678,'064027','C606','Chianche',65,1),(5679,'064028','C659','Chiusano di San Domenico',65,1),(5680,'064029','C971','Contrada',65,1),(5681,'064030','C976','Conza della Campania',65,1),(5682,'064031','D331','Domicella',65,1),(5683,'064032','D638','Flumeri',65,1),(5684,'064033','D671','Fontanarosa',65,1),(5685,'064034','D701','Forino',65,1),(5686,'064035','D798','Frigento',65,1),(5687,'064036','D998','Gesualdo',65,1),(5688,'064037','E161','Greci',65,1),(5689,'064038','E206','Grottaminarda',65,1),(5690,'064039','E214','Grottolella',65,1),(5691,'064040','E245','Guardia Lombardi',65,1),(5692,'064041','E397','Lacedonia',65,1),(5693,'064042','E448','Lapio',65,1),(5694,'064043','E487','Lauro',65,1),(5695,'064044','E605','Lioni',65,1),(5696,'064045','E746','Luogosano',65,1),(5697,'064046','E891','Manocalzati',65,1),(5698,'064047','E997','Marzano di Nola',65,1),(5699,'064048','F110','Melito Irpino',65,1),(5700,'064049','F141','Mercogliano',65,1),(5701,'064050','F230','Mirabella Eclano',65,1),(5702,'064051','F397','Montaguto',65,1),(5703,'064052','F448','Montecalvo Irpino',65,1),(5704,'064053','F491','Montefalcione',65,1),(5705,'064054','F506','Monteforte Irpino',65,1),(5706,'064055','F511','Montefredane',65,1),(5707,'064056','F512','Montefusco',65,1),(5708,'064057','F546','Montella',65,1),(5709,'064058','F559','Montemarano',65,1),(5710,'064059','F566','Montemiletto',65,1),(5711,'064060','F660','Monteverde',65,1),(5712,'064061','F693','Montoro Inferiore',65,1),(5713,'064062','F694','Montoro Superiore',65,1),(5714,'064063','F744','Morra De Sanctis',65,1),(5715,'064064','F762','Moschiano',65,1),(5716,'064065','F798','Mugnano del Cardinale',65,1),(5717,'064066','F988','Nusco',65,1),(5718,'064067','G165','Ospedaletto d\'Alpinolo',65,1),(5719,'064068','G242','Pago del Vallo di Lauro',65,1),(5720,'064069','G340','Parolise',65,1),(5721,'064070','G370','Paternopoli',65,1),(5722,'064071','G519','Petruro Irpino',65,1),(5723,'064072','G611','Pietradefusi',65,1),(5724,'064073','G629','Pietrastornina',65,1),(5725,'064074','G990','Prata di Principato Ultra',65,1),(5726,'064075','H006','Pratola Serra',65,1),(5727,'064076','H097','Quadrelle',65,1),(5728,'064077','H128','Quindici',65,1),(5729,'064078','H382','Roccabascerana',65,1),(5730,'064079','H438','Rocca San Felice',65,1),(5731,'064080','H592','Rotondi',65,1),(5732,'064081','H733','Salza Irpina',65,1),(5733,'064082','H975','San Mango sul Calore',65,1),(5734,'064083','I016','San Martino Valle Caudina',65,1),(5735,'064084','I034','San Michele di Serino',65,1),(5736,'064085','I061','San Nicola Baronia',65,1),(5737,'064086','I129','San Potito Ultra',65,1),(5738,'064087','I163','San Sossio Baronia',65,1),(5739,'064088','I219','Santa Lucia di Serino',65,1),(5740,'064089','I264','Sant\'Andrea di Conza',65,1),(5741,'064090','I279','Sant\'Angelo all\'Esca',65,1),(5742,'064091','I280','Sant\'Angelo a Scala',65,1),(5743,'064092','I281','Sant\'Angelo dei Lombardi',65,1),(5744,'064093','I301','Santa Paolina',65,1),(5745,'064095','I357','Santo Stefano del Sole',65,1),(5746,'064096','I471','Savignano Irpino',65,1),(5747,'064097','I493','Scampitella',65,1),(5748,'064098','I606','Senerchia',65,1),(5749,'064099','I630','Serino',65,1),(5750,'064100','I756','Sirignano',65,1),(5751,'064101','I805','Solofra',65,1),(5752,'064102','I843','Sorbo Serpico',65,1),(5753,'064103','I893','Sperone',65,1),(5754,'064104','I990','Sturno',65,1),(5755,'064105','L004','Summonte',65,1),(5756,'064106','L061','Taurano',65,1),(5757,'064107','L062','Taurasi',65,1),(5758,'064108','L102','Teora',65,1),(5759,'064109','L214','Torella dei Lombardi',65,1),(5760,'064110','L272','Torre Le Nocelle',65,1),(5761,'064111','L301','Torrioni',65,1),(5762,'064112','L399','Trevico',65,1),(5763,'064113','L461','Tufo',65,1),(5764,'064114','L589','Vallata',65,1),(5765,'064115','L616','Vallesaccarda',65,1),(5766,'064116','L739','Venticano',65,1),(5767,'064117','L965','Villamaina',65,1),(5768,'064118','L973','Villanova del Battista',65,1),(5769,'064119','M130','Volturara Irpina',65,1),(5770,'064120','M203','Zungoli',65,1),(5771,'065001','A023','Acerno',66,1),(5772,'065002','A091','Agropoli',66,1),(5773,'065003','A128','Albanella',66,1),(5774,'065004','A186','Alfano',66,1),(5775,'065005','A230','Altavilla Silentina',66,1),(5776,'065006','A251','Amalfi',66,1),(5777,'065007','A294','Angri',66,1),(5778,'065008','A343','Aquara',66,1),(5779,'065009','A460','Ascea',66,1),(5780,'065010','A484','Atena Lucana',66,1),(5781,'065011','A487','Atrani',66,1),(5782,'065012','A495','Auletta',66,1),(5783,'065013','A674','Baronissi',66,1),(5784,'065014','A717','Battipaglia',66,1),(5785,'065015','A756','Bellosguardo',66,1),(5786,'065016','B115','Bracigliano',66,1),(5787,'065017','B242','Buccino',66,1),(5788,'065018','B266','Buonabitacolo',66,1),(5789,'065019','B351','Caggiano',66,1),(5790,'065020','B437','Calvanico',66,1),(5791,'065021','B476','Camerota',66,1),(5792,'065022','B492','Campagna',66,1),(5793,'065023','B555','Campora',66,1),(5794,'065024','B608','Cannalonga',66,1),(5795,'065025','B644','Capaccio',66,1),(5796,'065026','B868','Casalbuono',66,1),(5797,'065027','B888','Casaletto Spartano',66,1),(5798,'065028','B895','Casal Velino',66,1),(5799,'065029','B959','Caselle in Pittari',66,1),(5800,'065030','C069','Castelcivita',66,1),(5801,'065031','C125','Castellabate',66,1),(5802,'065032','C231','Castelnuovo Cilento',66,1),(5803,'065033','C235','Castelnuovo di Conza',66,1),(5804,'065034','C259','Castel San Giorgio',66,1),(5805,'065035','C262','Castel San Lorenzo',66,1),(5806,'065036','C306','Castiglione del Genovesi',66,1),(5807,'065037','C361','Cava de\' Tirreni',66,1),(5808,'065038','C444','Celle di Bulgheria',66,1),(5809,'065039','C470','Centola',66,1),(5810,'065040','C485','Ceraso',66,1),(5811,'065041','C584','Cetara',66,1),(5812,'065042','C676','Cicerale',66,1),(5813,'065043','C879','Colliano',66,1),(5814,'065044','C940','Conca dei Marini',66,1),(5815,'065045','C973','Controne',66,1),(5816,'065046','C974','Contursi Terme',66,1),(5817,'065047','C984','Corbara',66,1),(5818,'065048','D011','Corleto Monforte',66,1),(5819,'065049','D195','Cuccaro Vetere',66,1),(5820,'065050','D390','Eboli',66,1),(5821,'065051','D527','Felitto',66,1),(5822,'065052','D615','Fisciano',66,1),(5823,'065053','D826','Furore',66,1),(5824,'065054','D832','Futani',66,1),(5825,'065055','E026','Giffoni Sei Casali',66,1),(5826,'065056','E027','Giffoni Valle Piana',66,1),(5827,'065057','E037','Gioi',66,1),(5828,'065058','E060','Giungano',66,1),(5829,'065059','E365','Ispani',66,1),(5830,'065060','E480','Laureana Cilento',66,1),(5831,'065061','E485','Laurino',66,1),(5832,'065062','E486','Laurito',66,1),(5833,'065063','E498','Laviano',66,1),(5834,'065064','E767','Lustra',66,1),(5835,'065065','E814','Magliano Vetere',66,1),(5836,'065066','E839','Maiori',66,1),(5837,'065067','F138','Mercato San Severino',66,1),(5838,'065068','F223','Minori',66,1),(5839,'065069','F278','Moio della Civitella',66,1),(5840,'065070','F426','Montano Antilia',66,1),(5841,'065071','F479','Montecorice',66,1),(5842,'065072','F480','Montecorvino Pugliano',66,1),(5843,'065073','F481','Montecorvino Rovella',66,1),(5844,'065074','F507','Monteforte Cilento',66,1),(5845,'065075','F618','Monte San Giacomo',66,1),(5846,'065076','F625','Montesano sulla Marcellana',66,1),(5847,'065077','F731','Morigerati',66,1),(5848,'065078','F912','Nocera Inferiore',66,1),(5849,'065079','F913','Nocera Superiore',66,1),(5850,'065080','F967','Novi Velia',66,1),(5851,'065081','G011','Ogliastro Cilento',66,1),(5852,'065082','G023','Olevano sul Tusciano',66,1),(5853,'065083','G039','Oliveto Citra',66,1),(5854,'065084','G063','Omignano',66,1),(5855,'065085','G121','Orria',66,1),(5856,'065086','G192','Ottati',66,1),(5857,'065087','G226','Padula',66,1),(5858,'065088','G230','Pagani',66,1),(5859,'065089','G292','Palomonte',66,1),(5860,'065090','G426','Pellezzano',66,1),(5861,'065091','G447','Perdifumo',66,1),(5862,'065092','G455','Perito',66,1),(5863,'065093','G476','Pertosa',66,1),(5864,'065094','G509','Petina',66,1),(5865,'065095','G538','Piaggine',66,1),(5866,'065096','G707','Pisciotta',66,1),(5867,'065097','G793','Polla',66,1),(5868,'065098','G796','Pollica',66,1),(5869,'065099','G834','Pontecagnano Faiano',66,1),(5870,'065100','G932','Positano',66,1),(5871,'065101','G939','Postiglione',66,1),(5872,'065102','G976','Praiano',66,1),(5873,'065103','H062','Prignano Cilento',66,1),(5874,'065104','H198','Ravello',66,1),(5875,'065105','H277','Ricigliano',66,1),(5876,'065106','H394','Roccadaspide',66,1),(5877,'065107','H412','Roccagloriosa',66,1),(5878,'065108','H431','Roccapiemonte',66,1),(5879,'065109','H485','Rofrano',66,1),(5880,'065110','H503','Romagnano al Monte',66,1),(5881,'065111','H564','Roscigno',66,1),(5882,'065112','H644','Rutino',66,1),(5883,'065113','H654','Sacco',66,1),(5884,'065114','H683','Sala Consilina',66,1),(5885,'065115','H686','Salento',66,1),(5886,'065116','H703','Salerno',66,1),(5887,'065117','H732','Salvitelle',66,1),(5888,'065118','H800','San Cipriano Picentino',66,1),(5889,'065119','H907','San Giovanni a Piro',66,1),(5890,'065120','H943','San Gregorio Magno',66,1),(5891,'065121','H977','San Mango Piemonte',66,1),(5892,'065122','I019','San Marzano sul Sarno',66,1),(5893,'065123','I031','San Mauro Cilento',66,1),(5894,'065124','I032','San Mauro la Bruca',66,1),(5895,'065125','I089','San Pietro al Tanagro',66,1),(5896,'065126','I143','San Rufo',66,1),(5897,'065127','I253','Santa Marina',66,1),(5898,'065128','I278','Sant\'Angelo a Fasanella',66,1),(5899,'065129','I307','Sant\'Arsenio',66,1),(5900,'065130','I317','Sant\'Egidio del Monte Albino',66,1),(5901,'065131','I260','Santomenna',66,1),(5902,'065132','I377','San Valentino Torio',66,1),(5903,'065133','I410','Sanza',66,1),(5904,'065134','I422','Sapri',66,1),(5905,'065135','I438','Sarno',66,1),(5906,'065136','I451','Sassano',66,1),(5907,'065137','I483','Scafati',66,1),(5908,'065138','I486','Scala',66,1),(5909,'065139','I648','Serramezzana',66,1),(5910,'065140','I666','Serre',66,1),(5911,'065141','I677','Sessa Cilento',66,1),(5912,'065142','I720','Siano',66,1),(5913,'065143','M253','Sicignano degli Alburni',66,1),(5914,'065144','G887','Stella Cilento',66,1),(5915,'065145','I960','Stio',66,1),(5916,'065146','D292','Teggiano',66,1),(5917,'065147','L212','Torchiara',66,1),(5918,'065148','L233','Torraca',66,1),(5919,'065149','L274','Torre Orsaia',66,1),(5920,'065150','L306','Tortorella',66,1),(5921,'065151','L323','Tramonti',66,1),(5922,'065152','L377','Trentinara',66,1),(5923,'065153','G540','Valle dell\'Angelo',66,1),(5924,'065154','L628','Vallo della Lucania',66,1),(5925,'065155','L656','Valva',66,1),(5926,'065156','L835','Vibonati',66,1),(5927,'065157','L860','Vietri sul Mare',66,1),(5928,'065158','M294','Bellizzi',66,1),(5929,'066001','A018','Acciano',67,1),(5930,'066002','A100','Aielli',67,1),(5931,'066003','A187','Alfedena',67,1),(5932,'066004','A318','Anversa degli Abruzzi',67,1),(5933,'066005','A481','Ateleta',67,1),(5934,'066006','A515','Avezzano',67,1),(5935,'066007','A603','Balsorano',67,1),(5936,'066008','A656','Barete',67,1),(5937,'066009','A667','Barisciano',67,1),(5938,'066010','A678','Barrea',67,1),(5939,'066011','A884','Bisegna',67,1),(5940,'066012','B256','Bugnara',67,1),(5941,'066013','B358','Cagnano Amiterno',67,1),(5942,'066014','B382','Calascio',67,1),(5943,'066015','B526','Campo di Giove',67,1),(5944,'066016','B569','Campotosto',67,1),(5945,'066017','B606','Canistro',67,1),(5946,'066018','B624','Cansano',67,1),(5947,'066019','B651','Capestrano',67,1),(5948,'066020','B656','Capistrello',67,1),(5949,'066021','B658','Capitignano',67,1),(5950,'066022','B672','Caporciano',67,1),(5951,'066023','B677','Cappadocia',67,1),(5952,'066024','B725','Carapelle Calvisio',67,1),(5953,'066025','B842','Carsoli',67,1),(5954,'066026','C083','Castel del Monte',67,1),(5955,'066027','C090','Castel di Ieri',67,1),(5956,'066028','C096','Castel di Sangro',67,1),(5957,'066029','C126','Castellafiume',67,1),(5958,'066030','C278','Castelvecchio Calvisio',67,1),(5959,'066031','C279','Castelvecchio Subequo',67,1),(5960,'066032','C426','Celano',67,1),(5961,'066033','C492','Cerchio',67,1),(5962,'066034','C766','Civita d\'Antino',67,1),(5963,'066035','C778','Civitella Alfedena',67,1),(5964,'066036','C783','Civitella Roveto',67,1),(5965,'066037','C811','Cocullo',67,1),(5966,'066038','C844','Collarmele',67,1),(5967,'066039','C862','Collelongo',67,1),(5968,'066040','C866','Collepietro',67,1),(5969,'066041','C999','Corfinio',67,1),(5970,'066042','D465','Fagnano Alto',67,1),(5971,'066043','D681','Fontecchio',67,1),(5972,'066044','D736','Fossa',67,1),(5973,'066045','D850','Gagliano Aterno',67,1),(5974,'066046','E040','Gioia dei Marsi',67,1),(5975,'066047','E096','Goriano Sicoli',67,1),(5976,'066048','E307','Introdacqua',67,1),(5977,'066049','A345','L\'Aquila',67,1),(5978,'066050','E505','Lecce nei Marsi',67,1),(5979,'066051','E723','Luco dei Marsi',67,1),(5980,'066052','E724','Lucoli',67,1),(5981,'066053','E811','Magliano de\' Marsi',67,1),(5982,'066054','F022','Massa d\'Albe',67,1),(5983,'066055','M255','Molina Aterno',67,1),(5984,'066056','F595','Montereale',67,1),(5985,'066057','F732','Morino',67,1),(5986,'066058','F852','Navelli',67,1),(5987,'066059','F996','Ocre',67,1),(5988,'066060','G002','Ofena',67,1),(5989,'066061','G079','Opi',67,1),(5990,'066062','G102','Oricola',67,1),(5991,'066063','G142','Ortona dei Marsi',67,1),(5992,'066064','G145','Ortucchio',67,1),(5993,'066065','G200','Ovindoli',67,1),(5994,'066066','G210','Pacentro',67,1),(5995,'066067','G449','Pereto',67,1),(5996,'066068','G484','Pescasseroli',67,1),(5997,'066069','G492','Pescina',67,1),(5998,'066070','G493','Pescocostanzo',67,1),(5999,'066071','G524','Pettorano sul Gizio',67,1),(6000,'066072','G726','Pizzoli',67,1),(6001,'066073','G766','Poggio Picenze',67,1),(6002,'066074','G992','Prata d\'Ansidonia',67,1),(6003,'066075','H007','Pratola Peligna',67,1),(6004,'066076','H056','Prezza',67,1),(6005,'066077','H166','Raiano',67,1),(6006,'066078','H353','Rivisondoli',67,1),(6007,'066079','H389','Roccacasale',67,1),(6008,'066080','H399','Rocca di Botte',67,1),(6009,'066081','H400','Rocca di Cambio',67,1),(6010,'066082','H402','Rocca di Mezzo',67,1),(6011,'066083','H429','Rocca Pia',67,1),(6012,'066084','H434','Roccaraso',67,1),(6013,'066085','H772','San Benedetto dei Marsi',67,1),(6014,'066086','H773','San Benedetto in Perillis',67,1),(6015,'066087','H819','San Demetrio ne\' Vestini',67,1),(6016,'066088','I121','San Pio delle Camere',67,1),(6017,'066089','I326','Sante Marie',67,1),(6018,'066090','I336','Sant\'Eusanio Forconese',67,1),(6019,'066091','I360','Santo Stefano di Sessanio',67,1),(6020,'066092','I389','San Vincenzo Valle Roveto',67,1),(6021,'066093','I501','Scanno',67,1),(6022,'066094','I543','Scontrone',67,1),(6023,'066095','I546','Scoppito',67,1),(6024,'066096','I553','Scurcola Marsicana',67,1),(6025,'066097','I558','Secinaro',67,1),(6026,'066098','I804','Sulmona',67,1),(6027,'066099','L025','Tagliacozzo',67,1),(6028,'066100','L173','Tione degli Abruzzi',67,1),(6029,'066101','L227','Tornimparte',67,1),(6030,'066102','L334','Trasacco',67,1),(6031,'066103','L958','Villalago',67,1),(6032,'066104','M021','Villa Santa Lucia degli Abruzzi',67,1),(6033,'066105','M023','Villa Sant\'Angelo',67,1),(6034,'066106','M031','Villavallelonga',67,1),(6035,'066107','M041','Villetta Barrea',67,1),(6036,'066108','M090','Vittorito',67,1),(6037,'067001','A125','Alba Adriatica',68,1),(6038,'067002','A270','Ancarano',68,1),(6039,'067003','A445','Arsita',68,1),(6040,'067004','A488','Atri',68,1),(6041,'067005','A692','Basciano',68,1),(6042,'067006','A746','Bellante',68,1),(6043,'067007','A885','Bisenti',68,1),(6044,'067008','B515','Campli',68,1),(6045,'067009','B640','Canzano',68,1),(6046,'067010','C040','Castel Castagna',68,1),(6047,'067011','C128','Castellalto',68,1),(6048,'067012','C169','Castelli',68,1),(6049,'067013','C316','Castiglione Messer Raimondo',68,1),(6050,'067014','C322','Castilenti',68,1),(6051,'067015','C449','Cellino Attanasio',68,1),(6052,'067016','C517','Cermignano',68,1),(6053,'067017','C781','Civitella del Tronto',68,1),(6054,'067018','C311','Colledara',68,1),(6055,'067019','C901','Colonnella',68,1),(6056,'067020','C972','Controguerra',68,1),(6057,'067021','D043','Corropoli',68,1),(6058,'067022','D076','Cortino',68,1),(6059,'067023','D179','Crognaleto',68,1),(6060,'067024','D489','Fano Adriano',68,1),(6061,'067025','E058','Giulianova',68,1),(6062,'067026','E343','Isola del Gran Sasso d\'Italia',68,1),(6063,'067027','F500','Montefino',68,1),(6064,'067028','F690','Montorio al Vomano',68,1),(6065,'067029','F747','Morro d\'Oro',68,1),(6066,'067030','F764','Mosciano Sant\'Angelo',68,1),(6067,'067031','F870','Nereto',68,1),(6068,'067032','F942','Notaresco',68,1),(6069,'067033','G437','Penna Sant\'Andrea',68,1),(6070,'067034','G608','Pietracamela',68,1),(6071,'067035','F831','Pineto',68,1),(6072,'067036','H440','Rocca Santa Maria',68,1),(6073,'067037','F585','Roseto degli Abruzzi',68,1),(6074,'067038','I318','Sant\'Egidio alla Vibrata',68,1),(6075,'067039','I348','Sant\'Omero',68,1),(6076,'067040','I741','Silvi',68,1),(6077,'067041','L103','Teramo',68,1),(6078,'067042','L207','Torano Nuovo',68,1),(6079,'067043','L295','Torricella Sicura',68,1),(6080,'067044','L307','Tortoreto',68,1),(6081,'067045','L314','Tossicia',68,1),(6082,'067046','L597','Valle Castellana',68,1),(6083,'067047','E989','Martinsicuro',68,1),(6084,'068001','A008','Abbateggio',69,1),(6085,'068002','A120','Alanno',69,1),(6086,'068003','A945','Bolognano',69,1),(6087,'068004','B193','Brittoli',69,1),(6088,'068005','B294','Bussi sul Tirino',69,1),(6089,'068006','B681','Cappelle sul Tavo',69,1),(6090,'068007','B722','Caramanico Terme',69,1),(6091,'068008','B827','Carpineto della Nora',69,1),(6092,'068009','C308','Castiglione a Casauria',69,1),(6093,'068010','C354','Catignano',69,1),(6094,'068011','C474','Cepagatti',69,1),(6095,'068012','C750','Città Sant\'Angelo',69,1),(6096,'068013','C771','Civitaquana',69,1),(6097,'068014','C779','Civitella Casanova',69,1),(6098,'068015','C853','Collecorvino',69,1),(6099,'068016','D078','Corvara',69,1),(6100,'068017','D201','Cugnoli',69,1),(6101,'068018','D394','Elice',69,1),(6102,'068019','D501','Farindola',69,1),(6103,'068020','E558','Lettomanoppello',69,1),(6104,'068021','E691','Loreto Aprutino',69,1),(6105,'068022','E892','Manoppello',69,1),(6106,'068023','F441','Montebello di Bertona',69,1),(6107,'068024','F646','Montesilvano',69,1),(6108,'068025','F765','Moscufo',69,1),(6109,'068026','F908','Nocciano',69,1),(6110,'068027','G438','Penne',69,1),(6111,'068028','G482','Pescara',69,1),(6112,'068029','G499','Pescosansonesco',69,1),(6113,'068030','G555','Pianella',69,1),(6114,'068031','G589','Picciano',69,1),(6115,'068032','G621','Pietranico',69,1),(6116,'068033','G878','Popoli',69,1),(6117,'068034','H425','Roccamorice',69,1),(6118,'068035','H562','Rosciano',69,1),(6119,'068036','H715','Salle',69,1),(6120,'068037','I332','Sant\'Eufemia a Maiella',69,1),(6121,'068039','I482','Scafa',69,1),(6122,'068040','I649','Serramonacesca',69,1),(6123,'068041','I922','Spoltore',69,1),(6124,'068042','L186','Tocco da Casauria',69,1),(6125,'068043','L263','Torre de\' Passeri',69,1),(6126,'068044','L475','Turrivalignani',69,1),(6127,'068045','L846','Vicoli',69,1),(6128,'068046','L922','Villa Celiera',69,1),(6129,'069001','A235','Altino',70,1),(6130,'069002','A367','Archi',70,1),(6131,'069003','A398','Ari',70,1),(6132,'069004','A402','Arielli',70,1),(6133,'069005','A485','Atessa',70,1),(6134,'069006','A956','Bomba',70,1),(6135,'069007','B057','Borrello',70,1),(6136,'069008','B238','Bucchianico',70,1),(6137,'069009','B268','Montebello sul Sangro',70,1),(6138,'069010','B620','Canosa Sannita',70,1),(6139,'069011','B826','Carpineto Sinello',70,1),(6140,'069012','B853','Carunchio',70,1),(6141,'069013','B859','Casacanditella',70,1),(6142,'069014','B861','Casalanguida',70,1),(6143,'069015','B865','Casalbordino',70,1),(6144,'069016','B896','Casalincontrada',70,1),(6145,'069017','B985','Casoli',70,1),(6146,'069018','C114','Castel Frentano',70,1),(6147,'069019','C123','Castelguidone',70,1),(6148,'069020','C298','Castiglione Messer Marino',70,1),(6149,'069021','C428','Celenza sul Trigno',70,1),(6150,'069022','C632','Chieti',70,1),(6151,'069023','C768','Civitaluparella',70,1),(6152,'069024','C776','Civitella Messer Raimondo',70,1),(6153,'069025','C855','Colledimacine',70,1),(6154,'069026','C856','Colledimezzo',70,1),(6155,'069027','D137','Crecchio',70,1),(6156,'069028','D209','Cupello',70,1),(6157,'069029','D315','Dogliola',70,1),(6158,'069030','D494','Fara Filiorum Petri',70,1),(6159,'069031','D495','Fara San Martino',70,1),(6160,'069032','D592','Filetto',70,1),(6161,'069033','D738','Fossacesia',70,1),(6162,'069034','D757','Fraine',70,1),(6163,'069035','D763','Francavilla al Mare',70,1),(6164,'069036','D796','Fresagrandinaria',70,1),(6165,'069037','D803','Frisa',70,1),(6166,'069038','D823','Furci',70,1),(6167,'069039','D898','Gamberale',70,1),(6168,'069040','D996','Gessopalena',70,1),(6169,'069041','E052','Gissi',70,1),(6170,'069042','E056','Giuliano Teatino',70,1),(6171,'069043','E243','Guardiagrele',70,1),(6172,'069044','E266','Guilmi',70,1),(6173,'069045','E424','Lama dei Peligni',70,1),(6174,'069046','E435','Lanciano',70,1),(6175,'069047','E531','Lentella',70,1),(6176,'069048','E559','Lettopalena',70,1),(6177,'069049','E611','Liscia',70,1),(6178,'069050','F196','Miglianico',70,1),(6179,'069051','F433','Montazzoli',70,1),(6180,'069052','F498','Monteferrante',70,1),(6181,'069053','F535','Montelapiano',70,1),(6182,'069054','F578','Montenerodomo',70,1),(6183,'069055','F582','Monteodorisio',70,1),(6184,'069056','F785','Mozzagrogna',70,1),(6185,'069057','G128','Orsogna',70,1),(6186,'069058','G141','Ortona',70,1),(6187,'069059','G237','Paglieta',70,1),(6188,'069060','G271','Palena',70,1),(6189,'069061','G290','Palmoli',70,1),(6190,'069062','G294','Palombaro',70,1),(6191,'069063','G434','Pennadomo',70,1),(6192,'069064','G435','Pennapiedimonte',70,1),(6193,'069065','G441','Perano',70,1),(6194,'069066','G724','Pizzoferrato',70,1),(6195,'069067','G760','Poggiofiorito',70,1),(6196,'069068','G799','Pollutri',70,1),(6197,'069069','H052','Pretoro',70,1),(6198,'069070','H098','Quadri',70,1),(6199,'069071','H184','Rapino',70,1),(6200,'069072','H320','Ripa Teatina',70,1),(6201,'069073','H424','Roccamontepiano',70,1),(6202,'069074','H439','Rocca San Giovanni',70,1),(6203,'069075','H442','Roccascalegna',70,1),(6204,'069076','H448','Roccaspinalveti',70,1),(6205,'069077','H495','Roio del Sangro',70,1),(6206,'069078','H566','Rosello',70,1),(6207,'069079','H784','San Buono',70,1),(6208,'069080','H923','San Giovanni Lipioni',70,1),(6209,'069081','D690','San Giovanni Teatino',70,1),(6210,'069082','H991','San Martino sulla Marrucina',70,1),(6211,'069083','I148','San Salvo',70,1),(6212,'069084','I244','Santa Maria Imbaro',70,1),(6213,'069085','I335','Sant\'Eusanio del Sangro',70,1),(6214,'069086','I394','San Vito Chietino',70,1),(6215,'069087','I520','Scerni',70,1),(6216,'069088','I526','Schiavi di Abruzzo',70,1),(6217,'069089','L047','Taranta Peligna',70,1),(6218,'069090','L194','Tollo',70,1),(6219,'069091','L218','Torino di Sangro',70,1),(6220,'069092','L224','Tornareccio',70,1),(6221,'069093','L253','Torrebruna',70,1),(6222,'069094','L284','Torrevecchia Teatina',70,1),(6223,'069095','L291','Torricella Peligna',70,1),(6224,'069096','L363','Treglio',70,1),(6225,'069097','L459','Tufillo',70,1),(6226,'069098','L526','Vacri',70,1),(6227,'069099','E372','Vasto',70,1),(6228,'069100','L961','Villalfonsina',70,1),(6229,'069101','L964','Villamagna',70,1),(6230,'069102','M022','Villa Santa Maria',70,1),(6231,'069103','G613','Pietraferrazzana',70,1),(6232,'069104','D480','Fallo',70,1),(6233,'070001','A050','Acquaviva Collecroce',71,1),(6234,'070002','A616','Baranello',71,1),(6235,'070003','A930','Bojano',71,1),(6236,'070004','A971','Bonefro',71,1),(6237,'070005','B295','Busso',71,1),(6238,'070006','B519','Campobasso',71,1),(6239,'070007','B522','Campochiaro',71,1),(6240,'070008','B528','Campodipietra',71,1),(6241,'070009','B544','Campolieto',71,1),(6242,'070010','B550','Campomarino',71,1),(6243,'070011','B858','Casacalenda',71,1),(6244,'070012','B871','Casalciprano',71,1),(6245,'070013','C066','Castelbottaccio',71,1),(6246,'070014','C175','Castellino del Biferno',71,1),(6247,'070015','C197','Castelmauro',71,1),(6248,'070016','C346','Castropignano',71,1),(6249,'070017','C486','Cercemaggiore',71,1),(6250,'070018','C488','Cercepiccola',71,1),(6251,'070019','C764','Civitacampomarano',71,1),(6252,'070020','C854','Colle d\'Anchise',71,1),(6253,'070021','C875','Colletorto',71,1),(6254,'070022','C772','Duronia',71,1),(6255,'070023','D550','Ferrazzano',71,1),(6256,'070024','D737','Fossalto',71,1),(6257,'070025','D896','Gambatesa',71,1),(6258,'070026','E030','Gildone',71,1),(6259,'070027','E244','Guardialfiera',71,1),(6260,'070028','E248','Guardiaregia',71,1),(6261,'070029','E259','Guglionesi',71,1),(6262,'070030','E381','Jelsi',71,1),(6263,'070031','E456','Larino',71,1),(6264,'070032','E599','Limosano',71,1),(6265,'070033','E722','Lucito',71,1),(6266,'070034','E748','Lupara',71,1),(6267,'070035','E780','Macchia Valfortore',71,1),(6268,'070036','E799','Mafalda',71,1),(6269,'070037','F055','Matrice',71,1),(6270,'070038','F233','Mirabello Sannitico',71,1),(6271,'070039','F294','Molise',71,1),(6272,'070040','F322','Monacilioni',71,1),(6273,'070041','F391','Montagano',71,1),(6274,'070042','F475','Montecilfone',71,1),(6275,'070043','F495','Montefalcone nel Sannio',71,1),(6276,'070044','F548','Montelongo',71,1),(6277,'070045','F569','Montemitro',71,1),(6278,'070046','F576','Montenero di Bisaccia',71,1),(6279,'070047','F689','Montorio nei Frentani',71,1),(6280,'070048','F748','Morrone del Sannio',71,1),(6281,'070049','G086','Oratino',71,1),(6282,'070050','G257','Palata',71,1),(6283,'070051','G506','Petacciato',71,1),(6284,'070052','G512','Petrella Tifernina',71,1),(6285,'070053','G609','Pietracatella',71,1),(6286,'070054','G610','Pietracupa',71,1),(6287,'070055','G910','Portocannone',71,1),(6288,'070056','H083','Provvidenti',71,1),(6289,'070057','H273','Riccia',71,1),(6290,'070058','H311','Ripabottoni',71,1),(6291,'070059','H313','Ripalimosani',71,1),(6292,'070060','H454','Roccavivara',71,1),(6293,'070061','H589','Rotello',71,1),(6294,'070062','H693','Salcito',71,1),(6295,'070063','H782','San Biase',71,1),(6296,'070064','H833','San Felice del Molise',71,1),(6297,'070065','H867','San Giacomo degli Schiavoni',71,1),(6298,'070066','H920','San Giovanni in Galdo',71,1),(6299,'070067','H928','San Giuliano del Sannio',71,1),(6300,'070068','H929','San Giuliano di Puglia',71,1),(6301,'070069','H990','San Martino in Pensilis',71,1),(6302,'070070','I023','San Massimo',71,1),(6303,'070071','I122','San Polo Matese',71,1),(6304,'070072','I181','Santa Croce di Magliano',71,1),(6305,'070073','I289','Sant\'Angelo Limosano',71,1),(6306,'070074','I320','Sant\'Elia a Pianisi',71,1),(6307,'070075','I618','Sepino',71,1),(6308,'070076','I910','Spinete',71,1),(6309,'070077','L069','Tavenna',71,1),(6310,'070078','L113','Termoli',71,1),(6311,'070079','L215','Torella del Sannio',71,1),(6312,'070080','L230','Toro',71,1),(6313,'070081','L435','Trivento',71,1),(6314,'070082','L458','Tufara',71,1),(6315,'070083','L505','Ururi',71,1),(6316,'070084','M057','Vinchiaturo',71,1),(6317,'071001','A015','Accadia',72,1),(6318,'071002','A150','Alberona',72,1),(6319,'071003','A320','Anzano di Puglia',72,1),(6320,'071004','A339','Apricena',72,1),(6321,'071005','A463','Ascoli Satriano',72,1),(6322,'071006','A854','Biccari',72,1),(6323,'071007','B104','Bovino',72,1),(6324,'071008','B357','Cagnano Varano',72,1),(6325,'071009','B584','Candela',72,1),(6326,'071010','B724','Carapelle',72,1),(6327,'071011','B784','Carlantino',72,1),(6328,'071012','B829','Carpino',72,1),(6329,'071013','B904','Casalnuovo Monterotaro',72,1),(6330,'071014','B917','Casalvecchio di Puglia',72,1),(6331,'071015','C198','Castelluccio dei Sauri',72,1),(6332,'071016','C202','Castelluccio Valmaggiore',72,1),(6333,'071017','C222','Castelnuovo della Daunia',72,1),(6334,'071018','C429','Celenza Valfortore',72,1),(6335,'071019','C442','Celle di San Vito',72,1),(6336,'071020','C514','Cerignola',72,1),(6337,'071021','C633','Chieuti',72,1),(6338,'071022','D269','Deliceto',72,1),(6339,'071023','D459','Faeto',72,1),(6340,'071024','D643','Foggia',72,1),(6341,'071025','E332','Ischitella',72,1),(6342,'071026','E363','Isole Tremiti',72,1),(6343,'071027','E549','Lesina',72,1),(6344,'071028','E716','Lucera',72,1),(6345,'071029','E885','Manfredonia',72,1),(6346,'071030','E946','Margherita di Savoia',72,1),(6347,'071031','F059','Mattinata',72,1),(6348,'071032','F538','Monteleone di Puglia',72,1),(6349,'071033','F631','Monte Sant\'Angelo',72,1),(6350,'071034','F777','Motta Montecorvino',72,1),(6351,'071035','G125','Orsara di Puglia',72,1),(6352,'071036','G131','Orta Nova',72,1),(6353,'071037','G312','Panni',72,1),(6354,'071038','G487','Peschici',72,1),(6355,'071039','G604','Pietramontecorvino',72,1),(6356,'071040','G761','Poggio Imperiale',72,1),(6357,'071041','H287','Rignano Garganico',72,1),(6358,'071042','H467','Rocchetta Sant\'Antonio',72,1),(6359,'071043','H480','Rodi Garganico',72,1),(6360,'071044','H568','Roseto Valfortore',72,1),(6361,'071045','H839','San Ferdinando di Puglia',72,1),(6362,'071046','H926','San Giovanni Rotondo',72,1),(6363,'071047','H985','San Marco in Lamis',72,1),(6364,'071048','H986','San Marco la Catola',72,1),(6365,'071049','I054','San Nicandro Garganico',72,1),(6366,'071050','I072','San Paolo di Civitate',72,1),(6367,'071051','I158','San Severo',72,1),(6368,'071052','I193','Sant\'Agata di Puglia',72,1),(6369,'071053','I641','Serracapriola',72,1),(6370,'071054','I962','Stornara',72,1),(6371,'071055','I963','Stornarella',72,1),(6372,'071056','L273','Torremaggiore',72,1),(6373,'071057','B915','Trinitapoli',72,1),(6374,'071058','L447','Troia',72,1),(6375,'071059','L842','Vico del Gargano',72,1),(6376,'071060','L858','Vieste',72,1),(6377,'071061','M131','Volturara Appula',72,1),(6378,'071062','M132','Volturino',72,1),(6379,'071063','M266','Ordona',72,1),(6380,'071064','M267','Zapponeta',72,1),(6381,'072001','A048','Acquaviva delle Fonti',73,1),(6382,'072002','A055','Adelfia',73,1),(6383,'072003','A149','Alberobello',73,1),(6384,'072004','A225','Altamura',73,1),(6385,'072005','A285','Andria',73,1),(6386,'072006','A662','Bari',73,1),(6387,'072007','A669','Barletta',73,1),(6388,'072008','A874','Binetto',73,1),(6389,'072009','A883','Bisceglie',73,1),(6390,'072010','A892','Bitetto',73,1),(6391,'072011','A893','Bitonto',73,1),(6392,'072012','A894','Bitritto',73,1),(6393,'072013','B619','Canosa di Puglia',73,1),(6394,'072014','B716','Capurso',73,1),(6395,'072015','B923','Casamassima',73,1),(6396,'072016','B998','Cassano delle Murge',73,1),(6397,'072017','C134','Castellana Grotte',73,1),(6398,'072018','C436','Cellamare',73,1),(6399,'072019','C975','Conversano',73,1),(6400,'072020','C983','Corato',73,1),(6401,'072021','E038','Gioia del Colle',73,1),(6402,'072022','E047','Giovinazzo',73,1),(6403,'072023','E155','Gravina in Puglia',73,1),(6404,'072024','E223','Grumo Appula',73,1),(6405,'072025','E645','Locorotondo',73,1),(6406,'072026','F220','Minervino Murge',73,1),(6407,'072027','F262','Modugno',73,1),(6408,'072028','F280','Mola di Bari',73,1),(6409,'072029','F284','Molfetta',73,1),(6410,'072030','F376','Monopoli',73,1),(6411,'072031','F915','Noci',73,1),(6412,'072032','F923','Noicattaro',73,1),(6413,'072033','G291','Palo del Colle',73,1),(6414,'072034','G769','Poggiorsini',73,1),(6415,'072035','G787','Polignano a Mare',73,1),(6416,'072036','H096','Putignano',73,1),(6417,'072037','H643','Rutigliano',73,1),(6418,'072038','H645','Ruvo di Puglia',73,1),(6419,'072039','H749','Sammichele di Bari',73,1),(6420,'072040','I053','Sannicandro di Bari',73,1),(6421,'072041','I330','Santeramo in Colle',73,1),(6422,'072042','I907','Spinazzola',73,1),(6423,'072043','L109','Terlizzi',73,1),(6424,'072044','L220','Toritto',73,1),(6425,'072045','L328','Trani',73,1),(6426,'072046','L425','Triggiano',73,1),(6427,'072047','L472','Turi',73,1),(6428,'072048','L571','Valenzano',73,1),(6429,'073001','A514','Avetrana',74,1),(6430,'073002','B808','Carosino',74,1),(6431,'073003','C136','Castellaneta',74,1),(6432,'073004','D171','Crispiano',74,1),(6433,'073005','D463','Faggiano',74,1),(6434,'073006','D754','Fragagnano',74,1),(6435,'073007','E036','Ginosa',74,1),(6436,'073008','E205','Grottaglie',74,1),(6437,'073009','E469','Laterza',74,1),(6438,'073010','E537','Leporano',74,1),(6439,'073011','E630','Lizzano',74,1),(6440,'073012','E882','Manduria',74,1),(6441,'073013','E986','Martina Franca',74,1),(6442,'073014','E995','Maruggio',74,1),(6443,'073015','F027','Massafra',74,1),(6444,'073016','F531','Monteiasi',74,1),(6445,'073017','F563','Montemesola',74,1),(6446,'073018','F587','Monteparano',74,1),(6447,'073019','F784','Mottola',74,1),(6448,'073020','G251','Palagianello',74,1),(6449,'073021','G252','Palagiano',74,1),(6450,'073022','H090','Pulsano',74,1),(6451,'073023','H409','Roccaforzata',74,1),(6452,'073024','H882','San Giorgio Ionico',74,1),(6453,'073025','I018','San Marzano di San Giuseppe',74,1),(6454,'073026','I467','Sava',74,1),(6455,'073027','L049','Taranto',74,1),(6456,'073028','L294','Torricella',74,1),(6457,'073029','M298','Statte',74,1),(6458,'074001','B180','Brindisi',75,1),(6459,'074002','B809','Carovigno',75,1),(6460,'074003','C424','Ceglie Messapica',75,1),(6461,'074004','C448','Cellino San Marco',75,1),(6462,'074005','C741','Cisternino',75,1),(6463,'074006','D422','Erchie',75,1),(6464,'074007','D508','Fasano',75,1),(6465,'074008','D761','Francavilla Fontana',75,1),(6466,'074009','E471','Latiano',75,1),(6467,'074010','F152','Mesagne',75,1),(6468,'074011','G098','Oria',75,1),(6469,'074012','G187','Ostuni',75,1),(6470,'074013','H822','San Donaci',75,1),(6471,'074014','I045','San Michele Salentino',75,1),(6472,'074015','I066','San Pancrazio Salentino',75,1),(6473,'074016','I119','San Pietro Vernotico',75,1),(6474,'074017','I396','San Vito dei Normanni',75,1),(6475,'074018','L213','Torchiarolo',75,1),(6476,'074019','L280','Torre Santa Susanna',75,1),(6477,'074020','L920','Villa Castelli',75,1),(6478,'075001','A042','Acquarica del Capo',76,1),(6479,'075002','A184','Alessano',76,1),(6480,'075003','A185','Alezio',76,1),(6481,'075004','A208','Alliste',76,1),(6482,'075005','A281','Andrano',76,1),(6483,'075006','A350','Aradeo',76,1),(6484,'075007','A425','Arnesano',76,1),(6485,'075008','A572','Bagnolo del Salento',76,1),(6486,'075009','B086','Botrugno',76,1),(6487,'075010','B413','Calimera',76,1),(6488,'075011','B506','Campi Salentina',76,1),(6489,'075012','B616','Cannole',76,1),(6490,'075013','B690','Caprarica di Lecce',76,1),(6491,'075014','B792','Carmiano',76,1),(6492,'075015','B822','Carpignano Salentino',76,1),(6493,'075016','B936','Casarano',76,1),(6494,'075017','C334','Castri di Lecce',76,1),(6495,'075018','C335','Castrignano de\' Greci',76,1),(6496,'075019','C336','Castrignano del Capo',76,1),(6497,'075020','C377','Cavallino',76,1),(6498,'075021','C865','Collepasso',76,1),(6499,'075022','C978','Copertino',76,1),(6500,'075023','D006','Corigliano d\'Otranto',76,1),(6501,'075024','D044','Corsano',76,1),(6502,'075025','D223','Cursi',76,1),(6503,'075026','D237','Cutrofiano',76,1),(6504,'075027','D305','Diso',76,1),(6505,'075028','D851','Gagliano del Capo',76,1),(6506,'075029','D862','Galatina',76,1),(6507,'075030','D863','Galatone',76,1),(6508,'075031','D883','Gallipoli',76,1),(6509,'075032','E053','Giuggianello',76,1),(6510,'075033','E061','Giurdignano',76,1),(6511,'075034','E227','Guagnano',76,1),(6512,'075035','E506','Lecce',76,1),(6513,'075036','E538','Lequile',76,1),(6514,'075037','E563','Leverano',76,1),(6515,'075038','E629','Lizzanello',76,1),(6516,'075039','E815','Maglie',76,1),(6517,'075040','E979','Martano',76,1),(6518,'075041','E984','Martignano',76,1),(6519,'075042','F054','Matino',76,1),(6520,'075043','F101','Melendugno',76,1),(6521,'075044','F109','Melissano',76,1),(6522,'075045','F117','Melpignano',76,1),(6523,'075046','F194','Miggiano',76,1),(6524,'075047','F221','Minervino di Lecce',76,1),(6525,'075048','F604','Monteroni di Lecce',76,1),(6526,'075049','F623','Montesano Salentino',76,1),(6527,'075050','F716','Morciano di Leuca',76,1),(6528,'075051','F816','Muro Leccese',76,1),(6529,'075052','F842','Nardò',76,1),(6530,'075053','F881','Neviano',76,1),(6531,'075054','F916','Nociglia',76,1),(6532,'075055','F970','Novoli',76,1),(6533,'075056','G136','Ortelle',76,1),(6534,'075057','G188','Otranto',76,1),(6535,'075058','G285','Palmariggi',76,1),(6536,'075059','G325','Parabita',76,1),(6537,'075060','G378','Patù',76,1),(6538,'075061','G751','Poggiardo',76,1),(6539,'075062','H047','Presicce',76,1),(6540,'075063','H147','Racale',76,1),(6541,'075064','H632','Ruffano',76,1),(6542,'075065','H708','Salice Salentino',76,1),(6543,'075066','H729','Salve',76,1),(6544,'075067','H757','Sanarica',76,1),(6545,'075068','H793','San Cesario di Lecce',76,1),(6546,'075069','H826','San Donato di Lecce',76,1),(6547,'075070','I059','Sannicola',76,1),(6548,'075071','I115','San Pietro in Lama',76,1),(6549,'075072','I172','Santa Cesarea Terme',76,1),(6550,'075073','I549','Scorrano',76,1),(6551,'075074','I559','Seclì',76,1),(6552,'075075','I780','Sogliano Cavour',76,1),(6553,'075076','I800','Soleto',76,1),(6554,'075077','I887','Specchia',76,1),(6555,'075078','I923','Spongano',76,1),(6556,'075079','I930','Squinzano',76,1),(6557,'075080','I950','Sternatia',76,1),(6558,'075081','L008','Supersano',76,1),(6559,'075082','L010','Surano',76,1),(6560,'075083','L011','Surbo',76,1),(6561,'075084','L064','Taurisano',76,1),(6562,'075085','L074','Taviano',76,1),(6563,'075086','L166','Tiggiano',76,1),(6564,'075087','L383','Trepuzzi',76,1),(6565,'075088','L419','Tricase',76,1),(6566,'075089','L462','Tuglie',76,1),(6567,'075090','L484','Ugento',76,1),(6568,'075091','L485','Uggiano la Chiesa',76,1),(6569,'075092','L711','Veglie',76,1),(6570,'075093','L776','Vernole',76,1),(6571,'075094','M187','Zollino',76,1),(6572,'075095','M264','San Cassiano',76,1),(6573,'075096','M261','Castro',76,1),(6574,'075097','M263','Porto Cesareo',76,1),(6575,'076001','A013','Abriola',77,1),(6576,'076002','A020','Acerenza',77,1),(6577,'076003','A131','Albano di Lucania',77,1),(6578,'076004','A321','Anzi',77,1),(6579,'076005','A415','Armento',77,1),(6580,'076006','A482','Atella',77,1),(6581,'076007','A519','Avigliano',77,1),(6582,'076008','A604','Balvano',77,1),(6583,'076009','A612','Banzi',77,1),(6584,'076010','A615','Baragiano',77,1),(6585,'076011','A666','Barile',77,1),(6586,'076012','A743','Bella',77,1),(6587,'076013','B173','Brienza',77,1),(6588,'076014','B181','Brindisi Montagna',77,1),(6589,'076015','B440','Calvello',77,1),(6590,'076016','B443','Calvera',77,1),(6591,'076017','B549','Campomaggiore',77,1),(6592,'076018','B580','Cancellara',77,1),(6593,'076019','B743','Carbone',77,1),(6594,'076020','B906','San Paolo Albanese',77,1),(6595,'076021','C120','Castelgrande',77,1),(6596,'076022','C199','Castelluccio Inferiore',77,1),(6597,'076023','C201','Castelluccio Superiore',77,1),(6598,'076024','C209','Castelmezzano',77,1),(6599,'076025','C271','Castelsaraceno',77,1),(6600,'076026','C345','Castronuovo di Sant\'Andrea',77,1),(6601,'076027','C539','Cersosimo',77,1),(6602,'076028','C619','Chiaromonte',77,1),(6603,'076029','D010','Corleto Perticara',77,1),(6604,'076030','D414','Episcopia',77,1),(6605,'076031','D497','Fardella',77,1),(6606,'076032','D593','Filiano',77,1),(6607,'076033','D696','Forenza',77,1),(6608,'076034','D766','Francavilla in Sinni',77,1),(6609,'076035','D876','Gallicchio',77,1),(6610,'076036','D971','Genzano di Lucania',77,1),(6611,'076037','E221','Grumento Nova',77,1),(6612,'076038','E246','Guardia Perticara',77,1),(6613,'076039','E409','Lagonegro',77,1),(6614,'076040','E474','Latronico',77,1),(6615,'076041','E482','Laurenzana',77,1),(6616,'076042','E483','Lauria',77,1),(6617,'076043','E493','Lavello',77,1),(6618,'076044','E919','Maratea',77,1),(6619,'076045','E976','Marsico Nuovo',77,1),(6620,'076046','E977','Marsicovetere',77,1),(6621,'076047','F006','Maschito',77,1),(6622,'076048','F104','Melfi',77,1),(6623,'076049','F249','Missanello',77,1),(6624,'076050','F295','Moliterno',77,1),(6625,'076051','F568','Montemilone',77,1),(6626,'076052','F573','Montemurro',77,1),(6627,'076053','F817','Muro Lucano',77,1),(6628,'076054','F866','Nemoli',77,1),(6629,'076055','F917','Noepoli',77,1),(6630,'076056','G081','Oppido Lucano',77,1),(6631,'076057','G261','Palazzo San Gervasio',77,1),(6632,'076058','G496','Pescopagano',77,1),(6633,'076059','G590','Picerno',77,1),(6634,'076060','G616','Pietragalla',77,1),(6635,'076061','G623','Pietrapertosa',77,1),(6636,'076062','G663','Pignola',77,1),(6637,'076063','G942','Potenza',77,1),(6638,'076064','H186','Rapolla',77,1),(6639,'076065','H187','Rapone',77,1),(6640,'076066','H307','Rionero in Vulture',77,1),(6641,'076067','H312','Ripacandida',77,1),(6642,'076068','H348','Rivello',77,1),(6643,'076069','H426','Roccanova',77,1),(6644,'076070','H590','Rotonda',77,1),(6645,'076071','H641','Ruoti',77,1),(6646,'076072','H646','Ruvo del Monte',77,1),(6647,'076073','H795','San Chirico Nuovo',77,1),(6648,'076074','H796','San Chirico Raparo',77,1),(6649,'076075','H808','San Costantino Albanese',77,1),(6650,'076076','H831','San Fele',77,1),(6651,'076077','H994','San Martino d\'Agri',77,1),(6652,'076078','I157','San Severino Lucano',77,1),(6653,'076079','I288','Sant\'Angelo Le Fratte',77,1),(6654,'076080','I305','Sant\'Arcangelo',77,1),(6655,'076081','I426','Sarconi',77,1),(6656,'076082','I457','Sasso di Castalda',77,1),(6657,'076083','G614','Satriano di Lucania',77,1),(6658,'076084','H730','Savoia di Lucania',77,1),(6659,'076085','I610','Senise',77,1),(6660,'076086','I917','Spinoso',77,1),(6661,'076087','L082','Teana',77,1),(6662,'076088','L126','Terranova di Pollino',77,1),(6663,'076089','L181','Tito',77,1),(6664,'076090','L197','Tolve',77,1),(6665,'076091','L326','Tramutola',77,1),(6666,'076092','L357','Trecchina',77,1),(6667,'076093','L439','Trivigno',77,1),(6668,'076094','L532','Vaglio Basilicata',77,1),(6669,'076095','L738','Venosa',77,1),(6670,'076096','L859','Vietri di Potenza',77,1),(6671,'076097','L873','Viggianello',77,1),(6672,'076098','L874','Viggiano',77,1),(6673,'076099','E033','Ginestra',77,1),(6674,'076100','M269','Paterno',77,1),(6675,'077001','A017','Accettura',78,1),(6676,'077002','A196','Aliano',78,1),(6677,'077003','A801','Bernalda',78,1),(6678,'077004','B391','Calciano',78,1),(6679,'077005','C723','Cirigliano',78,1),(6680,'077006','C888','Colobraro',78,1),(6681,'077007','D128','Craco',78,1),(6682,'077008','D547','Ferrandina',78,1),(6683,'077009','D909','Garaguso',78,1),(6684,'077010','E093','Gorgoglione',78,1),(6685,'077011','E147','Grassano',78,1),(6686,'077012','E213','Grottole',78,1),(6687,'077013','E326','Irsina',78,1),(6688,'077014','F052','Matera',78,1),(6689,'077015','F201','Miglionico',78,1),(6690,'077016','F399','Montalbano Jonico',78,1),(6691,'077017','F637','Montescaglioso',78,1),(6692,'077018','A942','Nova Siri',78,1),(6693,'077019','G037','Oliveto Lucano',78,1),(6694,'077020','G712','Pisticci',78,1),(6695,'077021','G786','Policoro',78,1),(6696,'077022','G806','Pomarico',78,1),(6697,'077023','H591','Rotondella',78,1),(6698,'077024','H687','Salandra',78,1),(6699,'077025','H888','San Giorgio Lucano',78,1),(6700,'077026','I029','San Mauro Forte',78,1),(6701,'077027','I954','Stigliano',78,1),(6702,'077028','L418','Tricarico',78,1),(6703,'077029','L477','Tursi',78,1),(6704,'077030','D513','Valsinni',78,1),(6705,'077031','M256','Scanzano Jonico',78,1),(6706,'078001','A033','Acquaformosa',79,1),(6707,'078002','A041','Acquappesa',79,1),(6708,'078003','A053','Acri',79,1),(6709,'078004','A102','Aiello Calabro',79,1),(6710,'078005','A105','Aieta',79,1),(6711,'078006','A160','Albidona',79,1),(6712,'078007','A183','Alessandria del Carretto',79,1),(6713,'078008','A234','Altilia',79,1),(6714,'078009','A240','Altomonte',79,1),(6715,'078010','A253','Amantea',79,1),(6716,'078011','A263','Amendolara',79,1),(6717,'078012','A340','Aprigliano',79,1),(6718,'078013','A762','Belmonte Calabro',79,1),(6719,'078014','A768','Belsito',79,1),(6720,'078015','A773','Belvedere Marittimo',79,1),(6721,'078016','A842','Bianchi',79,1),(6722,'078017','A887','Bisignano',79,1),(6723,'078018','A912','Bocchigliero',79,1),(6724,'078019','A973','Bonifati',79,1),(6725,'078020','B270','Buonvicino',79,1),(6726,'078021','B424','Calopezzati',79,1),(6727,'078022','B426','Caloveto',79,1),(6728,'078023','B500','Campana',79,1),(6729,'078024','B607','Canna',79,1),(6730,'078025','B774','Cariati',79,1),(6731,'078026','B802','Carolei',79,1),(6732,'078027','B813','Carpanzano',79,1),(6733,'078028','B983','Casole Bruzio',79,1),(6734,'078029','C002','Cassano all\'Ionio',79,1),(6735,'078030','C301','Castiglione Cosentino',79,1),(6736,'078031','C108','Castrolibero',79,1),(6737,'078032','C348','Castroregio',79,1),(6738,'078033','C349','Castrovillari',79,1),(6739,'078034','C430','Celico',79,1),(6740,'078035','C437','Cellara',79,1),(6741,'078036','C489','Cerchiara di Calabria',79,1),(6742,'078037','C515','Cerisano',79,1),(6743,'078038','C554','Cervicati',79,1),(6744,'078039','C560','Cerzeto',79,1),(6745,'078040','C588','Cetraro',79,1),(6746,'078041','C763','Civita',79,1),(6747,'078042','C795','Cleto',79,1),(6748,'078043','C905','Colosimi',79,1),(6749,'078044','D005','Corigliano Calabro',79,1),(6750,'078045','D086','Cosenza',79,1),(6751,'078046','D180','Cropalati',79,1),(6752,'078047','D184','Crosia',79,1),(6753,'078048','D289','Diamante',79,1),(6754,'078049','D304','Dipignano',79,1),(6755,'078050','D328','Domanico',79,1),(6756,'078051','D464','Fagnano Castello',79,1),(6757,'078052','D473','Falconara Albanese',79,1),(6758,'078053','D582','Figline Vegliaturo',79,1),(6759,'078054','D614','Firmo',79,1),(6760,'078055','D624','Fiumefreddo Bruzio',79,1),(6761,'078056','D764','Francavilla Marittima',79,1),(6762,'078057','D774','Frascineto',79,1),(6763,'078058','D828','Fuscaldo',79,1),(6764,'078059','E180','Grimaldi',79,1),(6765,'078060','E185','Grisolia',79,1),(6766,'078061','E242','Guardia Piemontese',79,1),(6767,'078062','E407','Lago',79,1),(6768,'078063','E417','Laino Borgo',79,1),(6769,'078064','E419','Laino Castello',79,1),(6770,'078065','E450','Lappano',79,1),(6771,'078066','E475','Lattarico',79,1),(6772,'078067','E677','Longobardi',79,1),(6773,'078068','E678','Longobucco',79,1),(6774,'078069','E745','Lungro',79,1),(6775,'078070','E773','Luzzi',79,1),(6776,'078071','E835','Maierà',79,1),(6777,'078072','E859','Malito',79,1),(6778,'078073','E872','Malvito',79,1),(6779,'078074','E878','Mandatoriccio',79,1),(6780,'078075','E888','Mangone',79,1),(6781,'078076','E914','Marano Marchesato',79,1),(6782,'078077','E915','Marano Principato',79,1),(6783,'078078','F001','Marzi',79,1),(6784,'078079','F125','Mendicino',79,1),(6785,'078080','F370','Mongrassano',79,1),(6786,'078081','F416','Montalto Uffugo',79,1),(6787,'078082','F519','Montegiordano',79,1),(6788,'078083','F708','Morano Calabro',79,1),(6789,'078084','F735','Mormanno',79,1),(6790,'078085','F775','Mottafollone',79,1),(6791,'078086','F907','Nocara',79,1),(6792,'078087','G110','Oriolo',79,1),(6793,'078088','G129','Orsomarso',79,1),(6794,'078089','G298','Paludi',79,1),(6795,'078090','G307','Panettieri',79,1),(6796,'078091','G317','Paola',79,1),(6797,'078092','G320','Papasidero',79,1),(6798,'078093','G331','Parenti',79,1),(6799,'078094','G372','Paterno Calabro',79,1),(6800,'078095','G400','Pedace',79,1),(6801,'078096','G411','Pedivigliano',79,1),(6802,'078097','G553','Piane Crati',79,1),(6803,'078098','G615','Pietrafitta',79,1),(6804,'078099','G622','Pietrapaola',79,1),(6805,'078100','G733','Plataci',79,1),(6806,'078101','G975','Praia a Mare',79,1),(6807,'078102','H235','Rende',79,1),(6808,'078103','H416','Rocca Imperiale',79,1),(6809,'078104','H488','Roggiano Gravina',79,1),(6810,'078105','H490','Rogliano',79,1),(6811,'078106','H565','Rose',79,1),(6812,'078107','H572','Roseto Capo Spulico',79,1),(6813,'078108','H579','Rossano',79,1),(6814,'078109','H585','Rota Greca',79,1),(6815,'078110','H621','Rovito',79,1),(6816,'078111','H765','San Basile',79,1),(6817,'078112','H774','San Benedetto Ullano',79,1),(6818,'078113','H806','San Cosmo Albanese',79,1),(6819,'078114','H818','San Demetrio Corone',79,1),(6820,'078115','H825','San Donato di Ninea',79,1),(6821,'078116','H841','San Fili',79,1),(6822,'078117','H877','Sangineto',79,1),(6823,'078118','H881','San Giorgio Albanese',79,1),(6824,'078119','H919','San Giovanni in Fiore',79,1),(6825,'078120','H961','San Lorenzo Bellizzi',79,1),(6826,'078121','H962','San Lorenzo del Vallo',79,1),(6827,'078122','H971','San Lucido',79,1),(6828,'078123','H981','San Marco Argentano',79,1),(6829,'078124','H992','San Martino di Finita',79,1),(6830,'078125','I060','San Nicola Arcella',79,1),(6831,'078126','I108','San Pietro in Amantea',79,1),(6832,'078127','I114','San Pietro in Guarano',79,1),(6833,'078128','I165','San Sosti',79,1),(6834,'078129','I171','Santa Caterina Albanese',79,1),(6835,'078130','I183','Santa Domenica Talao',79,1),(6836,'078131','I192','Sant\'Agata di Esaro',79,1),(6837,'078132','C717','Santa Maria del Cedro',79,1),(6838,'078133','I309','Santa Sofia d\'Epiro',79,1),(6839,'078134','I359','Santo Stefano di Rogliano',79,1),(6840,'078135','I388','San Vincenzo La Costa',79,1),(6841,'078136','I423','Saracena',79,1),(6842,'078137','I485','Scala Coeli',79,1),(6843,'078138','I489','Scalea',79,1),(6844,'078139','D290','Scigliano',79,1),(6845,'078140','I642','Serra d\'Aiello',79,1),(6846,'078141','I650','Serra Pedace',79,1),(6847,'078142','I895','Spezzano Albanese',79,1),(6848,'078143','I896','Spezzano della Sila',79,1),(6849,'078144','I898','Spezzano Piccolo',79,1),(6850,'078145','L055','Tarsia',79,1),(6851,'078146','L124','Terranova da Sibari',79,1),(6852,'078147','L134','Terravecchia',79,1),(6853,'078148','L206','Torano Castello',79,1),(6854,'078149','L305','Tortora',79,1),(6855,'078150','L353','Trebisacce',79,1),(6856,'078151','L375','Trenta',79,1),(6857,'078152','L524','Vaccarizzo Albanese',79,1),(6858,'078153','L747','Verbicaro',79,1),(6859,'078154','B903','Villapiana',79,1),(6860,'078155','M202','Zumpano',79,1),(6861,'079002','A155','Albi',80,1),(6862,'079003','A255','Amaroni',80,1),(6863,'079004','A257','Amato',80,1),(6864,'079005','A272','Andali',80,1),(6865,'079007','A397','Argusto',80,1),(6866,'079008','A542','Badolato',80,1),(6867,'079009','A736','Belcastro',80,1),(6868,'079011','B002','Borgia',80,1),(6869,'079012','B085','Botricello',80,1),(6870,'079017','B717','Caraffa di Catanzaro',80,1),(6871,'079018','B758','Cardinale',80,1),(6872,'079020','B790','Carlopoli',80,1),(6873,'079023','C352','Catanzaro',80,1),(6874,'079024','C453','Cenadi',80,1),(6875,'079025','C472','Centrache',80,1),(6876,'079027','C542','Cerva',80,1),(6877,'079029','C616','Chiaravalle Centrale',80,1),(6878,'079030','C674','Cicala',80,1),(6879,'079033','C960','Conflenti',80,1),(6880,'079034','D049','Cortale',80,1),(6881,'079036','D181','Cropani',80,1),(6882,'079039','D218','Curinga',80,1),(6883,'079042','D257','Davoli',80,1),(6884,'079043','D261','Decollatura',80,1),(6885,'079047','D476','Falerna',80,1),(6886,'079048','D544','Feroleto Antico',80,1),(6887,'079052','D744','Fossato Serralta',80,1),(6888,'079055','D852','Gagliato',80,1),(6889,'079056','D932','Gasperina',80,1),(6890,'079058','E031','Gimigliano',80,1),(6891,'079059','E050','Girifalco',80,1),(6892,'079060','E068','Gizzeria',80,1),(6893,'079061','E239','Guardavalle',80,1),(6894,'079063','E328','Isca sullo Ionio',80,1),(6895,'079065','E274','Jacurso',80,1),(6896,'079068','E806','Magisano',80,1),(6897,'079069','E834','Maida',80,1),(6898,'079071','E923','Marcedusa',80,1),(6899,'079072','E925','Marcellinara',80,1),(6900,'079073','E990','Martirano',80,1),(6901,'079074','E991','Martirano Lombardo',80,1),(6902,'079077','F200','Miglierina',80,1),(6903,'079080','F432','Montauro',80,1),(6904,'079081','F586','Montepaone',80,1),(6905,'079083','F780','Motta Santa Lucia',80,1),(6906,'079087','F910','Nocera Terinese',80,1),(6907,'079088','G034','Olivadi',80,1),(6908,'079089','G272','Palermiti',80,1),(6909,'079092','G439','Pentone',80,1),(6910,'079094','G517','Petrizzi',80,1),(6911,'079095','G518','Petronà',80,1),(6912,'079096','D546','Pianopoli',80,1),(6913,'079099','G734','Platania',80,1),(6914,'079108','H846','San Floro',80,1),(6915,'079110','H976','San Mango d\'Aquino',80,1),(6916,'079114','I093','San Pietro a Maida',80,1),(6917,'079115','I095','San Pietro Apostolo',80,1),(6918,'079116','I164','San Sostene',80,1),(6919,'079117','I170','Santa Caterina dello Ionio',80,1),(6920,'079118','I266','Sant\'Andrea Apostolo dello Ionio',80,1),(6921,'079122','I393','San Vito sullo Ionio',80,1),(6922,'079123','I463','Satriano',80,1),(6923,'079126','I589','Sellia',80,1),(6924,'079127','I590','Sellia Marina',80,1),(6925,'079129','I655','Serrastretta',80,1),(6926,'079130','I671','Sersale',80,1),(6927,'079131','I704','Settingiano',80,1),(6928,'079133','I745','Simeri Crichi',80,1),(6929,'079134','I844','Sorbo San Basile',80,1),(6930,'079137','I872','Soverato',80,1),(6931,'079138','I874','Soveria Mannelli',80,1),(6932,'079139','I875','Soveria Simeri',80,1),(6933,'079142','I929','Squillace',80,1),(6934,'079143','I937','Stalettì',80,1),(6935,'079146','L070','Taverna',80,1),(6936,'079147','L177','Tiriolo',80,1),(6937,'079148','L240','Torre di Ruggiero',80,1),(6938,'079151','I322','Vallefiorita',80,1),(6939,'079157','M140','Zagarise',80,1),(6940,'079160','M208','Lamezia Terme',80,1),(6941,'080001','A065','Africo',81,1),(6942,'080002','A077','Agnana Calabra',81,1),(6943,'080003','A303','Anoia',81,1),(6944,'080004','A314','Antonimina',81,1),(6945,'080005','A385','Ardore',81,1),(6946,'080006','A544','Bagaladi',81,1),(6947,'080007','A552','Bagnara Calabra',81,1),(6948,'080008','A780','Benestare',81,1),(6949,'080009','A843','Bianco',81,1),(6950,'080010','A897','Bivongi',81,1),(6951,'080011','B097','Bova',81,1),(6952,'080012','B098','Bovalino',81,1),(6953,'080013','B099','Bova Marina',81,1),(6954,'080014','B118','Brancaleone',81,1),(6955,'080015','B234','Bruzzano Zeffirio',81,1),(6956,'080016','B379','Calanna',81,1),(6957,'080017','B481','Camini',81,1),(6958,'080018','B516','Campo Calabro',81,1),(6959,'080019','B591','Candidoni',81,1),(6960,'080020','B617','Canolo',81,1),(6961,'080021','B718','Caraffa del Bianco',81,1),(6962,'080022','B756','Cardeto',81,1),(6963,'080023','B766','Careri',81,1),(6964,'080024','B966','Casignana',81,1),(6965,'080025','C285','Caulonia',81,1),(6966,'080026','C695','Ciminà',81,1),(6967,'080027','C710','Cinquefrondi',81,1),(6968,'080028','C747','Cittanova',81,1),(6969,'080029','C954','Condofuri',81,1),(6970,'080030','D089','Cosoleto',81,1),(6971,'080031','D268','Delianuova',81,1),(6972,'080032','D545','Feroleto della Chiesa',81,1),(6973,'080033','D557','Ferruzzano',81,1),(6974,'080034','D619','Fiumara',81,1),(6975,'080035','D864','Galatro',81,1),(6976,'080036','D975','Gerace',81,1),(6977,'080037','E025','Giffone',81,1),(6978,'080038','E041','Gioia Tauro',81,1),(6979,'080039','E044','Gioiosa Ionica',81,1),(6980,'080040','E212','Grotteria',81,1),(6981,'080041','E402','Laganadi',81,1),(6982,'080042','E479','Laureana di Borrello',81,1),(6983,'080043','D976','Locri',81,1),(6984,'080044','E873','Mammola',81,1),(6985,'080045','E956','Marina di Gioiosa Ionica',81,1),(6986,'080046','E968','Maropati',81,1),(6987,'080047','E993','Martone',81,1),(6988,'080048','F105','Melicuccà',81,1),(6989,'080049','F106','Melicucco',81,1),(6990,'080050','F112','Melito di Porto Salvo',81,1),(6991,'080051','F301','Molochio',81,1),(6992,'080052','F324','Monasterace',81,1),(6993,'080053','D746','Montebello Ionico',81,1),(6994,'080054','F779','Motta San Giovanni',81,1),(6995,'080055','G082','Oppido Mamertina',81,1),(6996,'080056','G277','Palizzi',81,1),(6997,'080057','G288','Palmi',81,1),(6998,'080058','G394','Pazzano',81,1),(6999,'080059','G729','Placanica',81,1),(7000,'080060','G735','Platì',81,1),(7001,'080061','G791','Polistena',81,1),(7002,'080062','G905','Portigliola',81,1),(7003,'080063','H224','Reggio di Calabria',81,1),(7004,'080064','H265','Riace',81,1),(7005,'080065','H359','Rizziconi',81,1),(7006,'080066','H408','Roccaforte del Greco',81,1),(7007,'080067','H456','Roccella Ionica',81,1),(7008,'080068','H489','Roghudi',81,1),(7009,'080069','H558','Rosarno',81,1),(7010,'080070','H013','Samo',81,1),(7011,'080071','H889','San Giorgio Morgeto',81,1),(7012,'080072','H903','San Giovanni di Gerace',81,1),(7013,'080073','H959','San Lorenzo',81,1),(7014,'080074','H970','San Luca',81,1),(7015,'080075','I102','San Pietro di Caridà',81,1),(7016,'080076','I132','San Procopio',81,1),(7017,'080077','I139','San Roberto',81,1),(7018,'080078','I176','Santa Cristina d\'Aspromonte',81,1),(7019,'080079','I198','Sant\'Agata del Bianco',81,1),(7020,'080080','I214','Sant\'Alessio in Aspromonte',81,1),(7021,'080081','I333','Sant\'Eufemia d\'Aspromonte',81,1),(7022,'080082','I341','Sant\'Ilario dello Ionio',81,1),(7023,'080083','I371','Santo Stefano in Aspromonte',81,1),(7024,'080084','I536','Scido',81,1),(7025,'080085','I537','Scilla',81,1),(7026,'080086','I600','Seminara',81,1),(7027,'080087','I656','Serrata',81,1),(7028,'080088','I725','Siderno',81,1),(7029,'080089','I753','Sinopoli',81,1),(7030,'080090','I936','Staiti',81,1),(7031,'080091','I955','Stignano',81,1),(7032,'080092','I956','Stilo',81,1),(7033,'080093','L063','Taurianova',81,1),(7034,'080094','L127','Terranova Sappo Minulio',81,1),(7035,'080095','L673','Varapodio',81,1),(7036,'080096','M018','Villa San Giovanni',81,1),(7037,'080097','M277','San Ferdinando',81,1),(7038,'081001','A176','Alcamo',82,1),(7039,'081002','B288','Buseto Palizzolo',82,1),(7040,'081003','B385','Calatafimi-Segesta',82,1),(7041,'081004','B521','Campobello di Mazara',82,1),(7042,'081005','C130','Castellammare del Golfo',82,1),(7043,'081006','C286','Castelvetrano',82,1),(7044,'081007','D234','Custonaci',82,1),(7045,'081008','D423','Erice',82,1),(7046,'081009','D518','Favignana',82,1),(7047,'081010','E023','Gibellina',82,1),(7048,'081011','E974','Marsala',82,1),(7049,'081012','F061','Mazara del Vallo',82,1),(7050,'081013','G208','Paceco',82,1),(7051,'081014','G315','Pantelleria',82,1),(7052,'081015','G347','Partanna',82,1),(7053,'081016','G767','Poggioreale',82,1),(7054,'081017','H688','Salaparuta',82,1),(7055,'081018','H700','Salemi',82,1),(7056,'081019','I291','Santa Ninfa',82,1),(7057,'081020','I407','San Vito Lo Capo',82,1),(7058,'081021','L331','Trapani',82,1),(7059,'081022','G319','Valderice',82,1),(7060,'081023','M081','Vita',82,1),(7061,'081024','M281','Petrosino',82,1),(7062,'082001','A195','Alia',83,1),(7063,'082002','A202','Alimena',83,1),(7064,'082003','A203','Aliminusa',83,1),(7065,'082004','A229','Altavilla Milicia',83,1),(7066,'082005','A239','Altofonte',83,1),(7067,'082006','A546','Bagheria',83,1),(7068,'082007','A592','Balestrate',83,1),(7069,'082008','A719','Baucina',83,1),(7070,'082009','A764','Belmonte Mezzagno',83,1),(7071,'082010','A882','Bisacquino',83,1),(7072,'082011','A946','Bolognetta',83,1),(7073,'082012','A958','Bompietro',83,1),(7074,'082013','A991','Borgetto',83,1),(7075,'082014','B315','Caccamo',83,1),(7076,'082015','B430','Caltavuturo',83,1),(7077,'082016','B533','Campofelice di Fitalia',83,1),(7078,'082017','B532','Campofelice di Roccella',83,1),(7079,'082018','B535','Campofiorito',83,1),(7080,'082019','B556','Camporeale',83,1),(7081,'082020','B645','Capaci',83,1),(7082,'082021','B780','Carini',83,1),(7083,'082022','C067','Castelbuono',83,1),(7084,'082023','C074','Casteldaccia',83,1),(7085,'082024','C135','Castellana Sicula',83,1),(7086,'082025','C344','Castronovo di Sicilia',83,1),(7087,'082026','C420','Cefalà Diana',83,1),(7088,'082027','C421','Cefalù',83,1),(7089,'082028','C496','Cerda',83,1),(7090,'082029','C654','Chiusa Sclafani',83,1),(7091,'082030','C696','Ciminna',83,1),(7092,'082031','C708','Cinisi',83,1),(7093,'082032','C871','Collesano',83,1),(7094,'082033','C968','Contessa Entellina',83,1),(7095,'082034','D009','Corleone',83,1),(7096,'082035','D567','Ficarazzi',83,1),(7097,'082036','D907','Gangi',83,1),(7098,'082037','D977','Geraci Siculo',83,1),(7099,'082038','E013','Giardinello',83,1),(7100,'082039','E055','Giuliana',83,1),(7101,'082040','E074','Godrano',83,1),(7102,'082041','E149','Gratteri',83,1),(7103,'082042','E337','Isnello',83,1),(7104,'082043','E350','Isola delle Femmine',83,1),(7105,'082044','E459','Lascari',83,1),(7106,'082045','E541','Lercara Friddi',83,1),(7107,'082046','E957','Marineo',83,1),(7108,'082047','F184','Mezzojuso',83,1),(7109,'082048','F246','Misilmeri',83,1),(7110,'082049','F377','Monreale',83,1),(7111,'082050','F544','Montelepre',83,1),(7112,'082051','F553','Montemaggiore Belsito',83,1),(7113,'082052','G263','Palazzo Adriano',83,1),(7114,'082053','G273','Palermo',83,1),(7115,'082054','G348','Partinico',83,1),(7116,'082055','G510','Petralia Soprana',83,1),(7117,'082056','G511','Petralia Sottana',83,1),(7118,'082057','G543','Piana degli Albanesi',83,1),(7119,'082058','G792','Polizzi Generosa',83,1),(7120,'082059','G797','Pollina',83,1),(7121,'082060','H070','Prizzi',83,1),(7122,'082061','H422','Roccamena',83,1),(7123,'082062','H428','Roccapalumba',83,1),(7124,'082063','H797','San Cipirello',83,1),(7125,'082064','H933','San Giuseppe Jato',83,1),(7126,'082065','I028','San Mauro Castelverde',83,1),(7127,'082066','I174','Santa Cristina Gela',83,1),(7128,'082067','I188','Santa Flavia',83,1),(7129,'082068','I534','Sciara',83,1),(7130,'082069','I541','Sclafani Bagni',83,1),(7131,'082070','L112','Termini Imerese',83,1),(7132,'082071','L131','Terrasini',83,1),(7133,'082072','L282','Torretta',83,1),(7134,'082073','L317','Trabia',83,1),(7135,'082074','L332','Trappeto',83,1),(7136,'082075','L519','Ustica',83,1),(7137,'082076','L603','Valledolmo',83,1),(7138,'082077','L740','Ventimiglia di Sicilia',83,1),(7139,'082078','L837','Vicari',83,1),(7140,'082079','L916','Villabate',83,1),(7141,'082080','L951','Villafrati',83,1),(7142,'082081','I538','Scillato',83,1),(7143,'082082','M268','Blufi',83,1),(7144,'083001','A177','Alcara li Fusi',84,1),(7145,'083002','A194','Alì',84,1),(7146,'083003','A201','Alì Terme',84,1),(7147,'083004','A313','Antillo',84,1),(7148,'083005','A638','Barcellona Pozzo di Gotto',84,1),(7149,'083006','A698','Basicò',84,1),(7150,'083007','B198','Brolo',84,1),(7151,'083008','B660','Capizzi',84,1),(7152,'083009','B666','Capo d\'Orlando',84,1),(7153,'083010','B695','Capri Leone',84,1),(7154,'083011','B804','Caronia',84,1),(7155,'083012','B918','Casalvecchio Siculo',84,1),(7156,'083013','C094','Castel di Lucio',84,1),(7157,'083014','C051','Castell\'Umberto',84,1),(7158,'083015','C210','Castelmola',84,1),(7159,'083016','C347','Castroreale',84,1),(7160,'083017','C568','Cesarò',84,1),(7161,'083018','C956','Condrò',84,1),(7162,'083019','D474','Falcone',84,1),(7163,'083020','D569','Ficarra',84,1),(7164,'083021','D622','Fiumedinisi',84,1),(7165,'083022','D635','Floresta',84,1),(7166,'083023','D661','Fondachelli-Fantina',84,1),(7167,'083024','D733','Forza d\'Agrò',84,1),(7168,'083025','D765','Francavilla di Sicilia',84,1),(7169,'083026','D793','Frazzanò',84,1),(7170,'083027','D824','Furci Siculo',84,1),(7171,'083028','D825','Furnari',84,1),(7172,'083029','D844','Gaggi',84,1),(7173,'083030','D861','Galati Mamertino',84,1),(7174,'083031','D885','Gallodoro',84,1),(7175,'083032','E014','Giardini-Naxos',84,1),(7176,'083033','E043','Gioiosa Marea',84,1),(7177,'083034','E142','Graniti',84,1),(7178,'083035','E233','Gualtieri Sicaminò',84,1),(7179,'083036','E374','Itala',84,1),(7180,'083037','E523','Leni',84,1),(7181,'083038','E555','Letojanni',84,1),(7182,'083039','E571','Librizzi',84,1),(7183,'083040','E594','Limina',84,1),(7184,'083041','E606','Lipari',84,1),(7185,'083042','E674','Longi',84,1),(7186,'083043','E855','Malfa',84,1),(7187,'083044','E869','Malvagna',84,1),(7188,'083045','E876','Mandanici',84,1),(7189,'083046','F066','Mazzarrà Sant\'Andrea',84,1),(7190,'083047','F147','Merì',84,1),(7191,'083048','F158','Messina',84,1),(7192,'083049','F206','Milazzo',84,1),(7193,'083050','F210','Militello Rosmarino',84,1),(7194,'083051','F242','Mirto',84,1),(7195,'083052','F251','Mistretta',84,1),(7196,'083053','F277','Moio Alcantara',84,1),(7197,'083054','F359','Monforte San Giorgio',84,1),(7198,'083055','F368','Mongiuffi Melia',84,1),(7199,'083056','F395','Montagnareale',84,1),(7200,'083057','F400','Montalbano Elicona',84,1),(7201,'083058','F772','Motta Camastra',84,1),(7202,'083059','F773','Motta d\'Affermo',84,1),(7203,'083060','F848','Naso',84,1),(7204,'083061','F901','Nizza di Sicilia',84,1),(7205,'083062','F951','Novara di Sicilia',84,1),(7206,'083063','G036','Oliveri',84,1),(7207,'083064','G209','Pace del Mela',84,1),(7208,'083065','G234','Pagliara',84,1),(7209,'083066','G377','Patti',84,1),(7210,'083067','G522','Pettineo',84,1),(7211,'083068','G699','Piraino',84,1),(7212,'083069','H151','Raccuja',84,1),(7213,'083070','H228','Reitano',84,1),(7214,'083071','H405','Roccafiorita',84,1),(7215,'083072','H418','Roccalumera',84,1),(7216,'083073','H380','Roccavaldina',84,1),(7217,'083074','H455','Roccella Valdemone',84,1),(7218,'083075','H479','Rodì Milici',84,1),(7219,'083076','H519','Rometta',84,1),(7220,'083077','H842','San Filippo del Mela',84,1),(7221,'083078','H850','San Fratello',84,1),(7222,'083079','H982','San Marco d\'Alunzio',84,1),(7223,'083080','I084','San Pier Niceto',84,1),(7224,'083081','I086','San Piero Patti',84,1),(7225,'083082','I147','San Salvatore di Fitalia',84,1),(7226,'083083','I184','Santa Domenica Vittoria',84,1),(7227,'083084','I199','Sant\'Agata di Militello',84,1),(7228,'083085','I215','Sant\'Alessio Siculo',84,1),(7229,'083086','I220','Santa Lucia del Mela',84,1),(7230,'083087','I254','Santa Marina Salina',84,1),(7231,'083088','I283','Sant\'Angelo di Brolo',84,1),(7232,'083089','I311','Santa Teresa di Riva',84,1),(7233,'083090','I328','San Teodoro',84,1),(7234,'083091','I370','Santo Stefano di Camastra',84,1),(7235,'083092','I420','Saponara',84,1),(7236,'083093','I477','Savoca',84,1),(7237,'083094','I492','Scaletta Zanclea',84,1),(7238,'083095','I747','Sinagra',84,1),(7239,'083096','I881','Spadafora',84,1),(7240,'083097','L042','Taormina',84,1),(7241,'083098','L271','Torregrotta',84,1),(7242,'083099','L308','Tortorici',84,1),(7243,'083100','L431','Tripi',84,1),(7244,'083101','L478','Tusa',84,1),(7245,'083102','L482','Ucria',84,1),(7246,'083103','L561','Valdina',84,1),(7247,'083104','L735','Venetico',84,1),(7248,'083105','L950','Villafranca Tirrena',84,1),(7249,'083106','M210','Terme Vigliatore',84,1),(7250,'083107','M211','Acquedolci',84,1),(7251,'083108','M286','Torrenova',84,1),(7252,'084001','A089','Agrigento',85,1),(7253,'084002','A181','Alessandria della Rocca',85,1),(7254,'084003','A351','Aragona',85,1),(7255,'084004','A896','Bivona',85,1),(7256,'084005','B275','Burgio',85,1),(7257,'084006','B377','Calamonaci',85,1),(7258,'084007','B427','Caltabellotta',85,1),(7259,'084008','B460','Camastra',85,1),(7260,'084009','B486','Cammarata',85,1),(7261,'084010','B520','Campobello di Licata',85,1),(7262,'084011','B602','Canicattì',85,1),(7263,'084012','C275','Casteltermini',85,1),(7264,'084013','C341','Castrofilippo',85,1),(7265,'084014','C356','Cattolica Eraclea',85,1),(7266,'084015','C668','Cianciana',85,1),(7267,'084016','C928','Comitini',85,1),(7268,'084017','D514','Favara',85,1),(7269,'084018','E209','Grotte',85,1),(7270,'084019','E390','Joppolo Giancaxio',85,1),(7271,'084020','E431','Lampedusa e Linosa',85,1),(7272,'084021','E573','Licata',85,1),(7273,'084022','E714','Lucca Sicula',85,1),(7274,'084023','F126','Menfi',85,1),(7275,'084024','F414','Montallegro',85,1),(7276,'084025','F655','Montevago',85,1),(7277,'084026','F845','Naro',85,1),(7278,'084027','G282','Palma di Montechiaro',85,1),(7279,'084028','F299','Porto Empedocle',85,1),(7280,'084029','H148','Racalmuto',85,1),(7281,'084030','H159','Raffadali',85,1),(7282,'084031','H194','Ravanusa',85,1),(7283,'084032','H205','Realmonte',85,1),(7284,'084033','H269','Ribera',85,1),(7285,'084034','H743','Sambuca di Sicilia',85,1),(7286,'084035','H778','San Biagio Platani',85,1),(7287,'084036','H914','San Giovanni Gemini',85,1),(7288,'084037','I185','Santa Elisabetta',85,1),(7289,'084038','I224','Santa Margherita di Belice',85,1),(7290,'084039','I290','Sant\'Angelo Muxaro',85,1),(7291,'084040','I356','Santo Stefano Quisquina',85,1),(7292,'084041','I533','Sciacca',85,1),(7293,'084042','I723','Siculiana',85,1),(7294,'084043','L944','Villafranca Sicula',85,1),(7295,'085001','A049','Acquaviva Platani',86,1),(7296,'085002','A957','Bompensiere',86,1),(7297,'085003','B302','Butera',86,1),(7298,'085004','B429','Caltanissetta',86,1),(7299,'085005','B537','Campofranco',86,1),(7300,'085006','D267','Delia',86,1),(7301,'085007','D960','Gela',86,1),(7302,'085008','E953','Marianopoli',86,1),(7303,'085009','F065','Mazzarino',86,1),(7304,'085010','E618','Milena',86,1),(7305,'085011','F489','Montedoro',86,1),(7306,'085012','F830','Mussomeli',86,1),(7307,'085013','F899','Niscemi',86,1),(7308,'085014','H245','Resuttano',86,1),(7309,'085015','H281','Riesi',86,1),(7310,'085016','H792','San Cataldo',86,1),(7311,'085017','I169','Santa Caterina Villarmosa',86,1),(7312,'085018','I644','Serradifalco',86,1),(7313,'085019','I824','Sommatino',86,1),(7314,'085020','L016','Sutera',86,1),(7315,'085021','L609','Vallelunga Pratameno',86,1),(7316,'085022','L959','Villalba',86,1),(7317,'086001','A070','Agira',87,1),(7318,'086002','A098','Aidone',87,1),(7319,'086003','A478','Assoro',87,1),(7320,'086004','A676','Barrafranca',87,1),(7321,'086005','B381','Calascibetta',87,1),(7322,'086006','C353','Catenanuova',87,1),(7323,'086007','C471','Centuripe',87,1),(7324,'086008','C480','Cerami',87,1),(7325,'086009','C342','Enna',87,1),(7326,'086010','D849','Gagliano Castelferrato',87,1),(7327,'086011','E536','Leonforte',87,1),(7328,'086012','F892','Nicosia',87,1),(7329,'086013','F900','Nissoria',87,1),(7330,'086014','G580','Piazza Armerina',87,1),(7331,'086015','G624','Pietraperzia',87,1),(7332,'086016','H221','Regalbuto',87,1),(7333,'086017','I891','Sperlinga',87,1),(7334,'086018','L448','Troina',87,1),(7335,'086019','L583','Valguarnera Caropepe',87,1),(7336,'086020','M011','Villarosa',87,1),(7337,'087001','A025','Aci Bonaccorsi',88,1),(7338,'087002','A026','Aci Castello',88,1),(7339,'087003','A027','Aci Catena',88,1),(7340,'087004','A028','Acireale',88,1),(7341,'087005','A029','Aci Sant\'Antonio',88,1),(7342,'087006','A056','Adrano',88,1),(7343,'087007','A766','Belpasso',88,1),(7344,'087008','A841','Biancavilla',88,1),(7345,'087009','B202','Bronte',88,1),(7346,'087010','B384','Calatabiano',88,1),(7347,'087011','B428','Caltagirone',88,1),(7348,'087012','B561','Camporotondo Etneo',88,1),(7349,'087013','C091','Castel di Iudica',88,1),(7350,'087014','C297','Castiglione di Sicilia',88,1),(7351,'087015','C351','Catania',88,1),(7352,'087016','D623','Fiumefreddo di Sicilia',88,1),(7353,'087017','E017','Giarre',88,1),(7354,'087018','E133','Grammichele',88,1),(7355,'087019','E156','Gravina di Catania',88,1),(7356,'087020','E578','Licodia Eubea',88,1),(7357,'087021','E602','Linguaglossa',88,1),(7358,'087022','E854','Maletto',88,1),(7359,'087023','F004','Mascali',88,1),(7360,'087024','F005','Mascalucia',88,1),(7361,'087025','F209','Militello in Val di Catania',88,1),(7362,'087026','F214','Milo',88,1),(7363,'087027','F217','Mineo',88,1),(7364,'087028','F231','Mirabella Imbaccari',88,1),(7365,'087029','F250','Misterbianco',88,1),(7366,'087030','F781','Motta Sant\'Anastasia',88,1),(7367,'087031','F890','Nicolosi',88,1),(7368,'087032','G253','Palagonia',88,1),(7369,'087033','G371','Paternò',88,1),(7370,'087034','G402','Pedara',88,1),(7371,'087035','G597','Piedimonte Etneo',88,1),(7372,'087036','H154','Raddusa',88,1),(7373,'087037','H168','Ramacca',88,1),(7374,'087038','H175','Randazzo',88,1),(7375,'087039','H325','Riposto',88,1),(7376,'087040','H805','San Cono',88,1),(7377,'087041','H922','San Giovanni la Punta',88,1),(7378,'087042','H940','San Gregorio di Catania',88,1),(7379,'087043','I035','San Michele di Ganzaria',88,1),(7380,'087044','I098','San Pietro Clarenza',88,1),(7381,'087045','I202','Sant\'Agata li Battiati',88,1),(7382,'087046','I216','Sant\'Alfio',88,1),(7383,'087047','I240','Santa Maria di Licodia',88,1),(7384,'087048','I314','Santa Venerina',88,1),(7385,'087049','I548','Scordia',88,1),(7386,'087050','L355','Trecastagni',88,1),(7387,'087051','L369','Tremestieri Etneo',88,1),(7388,'087052','L658','Valverde',88,1),(7389,'087053','L828','Viagrande',88,1),(7390,'087054','M100','Vizzini',88,1),(7391,'087055','M139','Zafferana Etnea',88,1),(7392,'087056','M271','Mazzarrone',88,1),(7393,'087057','M283','Maniace',88,1),(7394,'087058','M287','Ragalna',88,1),(7395,'088001','A014','Acate',89,1),(7396,'088002','C612','Chiaramonte Gulfi',89,1),(7397,'088003','C927','Comiso',89,1),(7398,'088004','E016','Giarratana',89,1),(7399,'088005','E366','Ispica',89,1),(7400,'088006','F258','Modica',89,1),(7401,'088007','F610','Monterosso Almo',89,1),(7402,'088008','G953','Pozzallo',89,1),(7403,'088009','H163','Ragusa',89,1),(7404,'088010','I178','Santa Croce Camerina',89,1),(7405,'088011','I535','Scicli',89,1),(7406,'088012','M088','Vittoria',89,1),(7407,'089001','A494','Augusta',90,1),(7408,'089002','A522','Avola',90,1),(7409,'089003','B237','Buccheri',90,1),(7410,'089004','B287','Buscemi',90,1),(7411,'089005','B603','Canicattini Bagni',90,1),(7412,'089006','B787','Carlentini',90,1),(7413,'089007','C006','Cassaro',90,1),(7414,'089008','D540','Ferla',90,1),(7415,'089009','D636','Floridia',90,1),(7416,'089010','D768','Francofonte',90,1),(7417,'089011','E532','Lentini',90,1),(7418,'089012','F107','Melilli',90,1),(7419,'089013','F943','Noto',90,1),(7420,'089014','G211','Pachino',90,1),(7421,'089015','G267','Palazzolo Acreide',90,1),(7422,'089016','H574','Rosolini',90,1),(7423,'089017','I754','Siracusa',90,1),(7424,'089018','I785','Solarino',90,1),(7425,'089019','I864','Sortino',90,1),(7426,'089020','M257','Portopalo di Capo Passero',90,1),(7427,'089021','M279','Priolo Gargallo',90,1),(7428,'090003','A192','Alghero',91,1),(7429,'090004','A287','Anela',91,1),(7430,'090005','A379','Ardara',91,1),(7431,'090007','A606','Banari',91,1),(7432,'090008','A781','Benetutti',91,1),(7433,'090010','A827','Bessude',91,1),(7434,'090011','A976','Bonnanaro',91,1),(7435,'090012','A977','Bono',91,1),(7436,'090013','A978','Bonorva',91,1),(7437,'090015','B064','Borutta',91,1),(7438,'090016','B094','Bottidda',91,1),(7439,'090018','B264','Bultei',91,1),(7440,'090019','B265','Bulzi',91,1),(7441,'090020','B276','Burgos',91,1),(7442,'090022','B772','Cargeghe',91,1),(7443,'090023','C272','Castelsardo',91,1),(7444,'090024','C600','Cheremule',91,1),(7445,'090025','C613','Chiaramonti',91,1),(7446,'090026','C818','Codrongianos',91,1),(7447,'090027','D100','Cossoine',91,1),(7448,'090028','D441','Esporlatu',91,1),(7449,'090029','D637','Florinas',91,1),(7450,'090030','E019','Giave',91,1),(7451,'090031','E285','Illorai',91,1),(7452,'090032','E376','Ittireddu',91,1),(7453,'090033','E377','Ittiri',91,1),(7454,'090034','E401','Laerru',91,1),(7455,'090038','E902','Mara',91,1),(7456,'090039','E992','Martis',91,1),(7457,'090040','F542','Monteleone Rocca Doria',91,1),(7458,'090042','F721','Mores',91,1),(7459,'090043','F818','Muros',91,1),(7460,'090044','F975','Nughedu San Nicolò',91,1),(7461,'090045','F976','Nule',91,1),(7462,'090046','F977','Nulvi',91,1),(7463,'090048','G046','Olmedo',91,1),(7464,'090050','G156','Osilo',91,1),(7465,'090051','G178','Ossi',91,1),(7466,'090052','G203','Ozieri',91,1),(7467,'090053','G225','Padria',91,1),(7468,'090055','G376','Pattada',91,1),(7469,'090056','G450','Perfugas',91,1),(7470,'090057','G740','Ploaghe',91,1),(7471,'090058','G924','Porto Torres',91,1),(7472,'090059','G962','Pozzomaggiore',91,1),(7473,'090060','H095','Putifigari',91,1),(7474,'090061','H507','Romana',91,1),(7475,'090064','I452','Sassari',91,1),(7476,'090065','I565','Sedini',91,1),(7477,'090066','I598','Semestene',91,1),(7478,'090067','I614','Sennori',91,1),(7479,'090068','I732','Siligo',91,1),(7480,'090069','I863','Sorso',91,1),(7481,'090071','L158','Thiesi',91,1),(7482,'090072','L180','Tissi',91,1),(7483,'090073','L235','Torralba',91,1),(7484,'090075','L464','Tula',91,1),(7485,'090076','L503','Uri',91,1),(7486,'090077','L509','Usini',91,1),(7487,'090078','L989','Villanova Monteleone',91,1),(7488,'090079','L604','Valledoria',91,1),(7489,'090082','M259','Viddalba',91,1),(7490,'090086','M282','Tergu',91,1),(7491,'090087','M284','Santa Maria Coghinas',91,1),(7492,'090088','M292','Erula',91,1),(7493,'090089','M290','Stintino',91,1),(7494,'091001','A407','Aritzo',92,1),(7495,'091003','A492','Atzara',92,1),(7496,'091004','A503','Austis',92,1),(7497,'091007','A776','Belvì',92,1),(7498,'091008','A880','Birori',92,1),(7499,'091009','A895','Bitti',92,1),(7500,'091010','A948','Bolotana',92,1),(7501,'091011','B056','Borore',92,1),(7502,'091012','B062','Bortigali',92,1),(7503,'091016','D287','Desulo',92,1),(7504,'091017','D345','Dorgali',92,1),(7505,'091018','D376','Dualchi',92,1),(7506,'091024','D665','Fonni',92,1),(7507,'091025','D842','Gadoni',92,1),(7508,'091027','D888','Galtellì',92,1),(7509,'091028','D947','Gavoi',92,1),(7510,'091033','E323','Irgoli',92,1),(7511,'091038','E517','Lei',92,1),(7512,'091040','E646','Loculi',92,1),(7513,'091041','E647','Lodè',92,1),(7514,'091043','E736','Lula',92,1),(7515,'091044','E788','Macomer',92,1),(7516,'091046','E874','Mamoiada',92,1),(7517,'091047','F073','Meana Sardo',92,1),(7518,'091050','F933','Noragugume',92,1),(7519,'091051','F979','Nuoro',92,1),(7520,'091055','G031','Oliena',92,1),(7521,'091056','G044','Ollolai',92,1),(7522,'091057','G058','Olzai',92,1),(7523,'091058','G064','Onanì',92,1),(7524,'091059','G070','Onifai',92,1),(7525,'091060','G071','Oniferi',92,1),(7526,'091061','G084','Orani',92,1),(7527,'091062','G097','Orgosolo',92,1),(7528,'091063','G119','Orosei',92,1),(7529,'091064','G120','Orotelli',92,1),(7530,'091066','G146','Ortueri',92,1),(7531,'091067','G147','Orune',92,1),(7532,'091068','G154','Osidda',92,1),(7533,'091070','G191','Ottana',92,1),(7534,'091071','G201','Ovodda',92,1),(7535,'091073','G929','Posada',92,1),(7536,'091077','I448','Sarule',92,1),(7537,'091083','I730','Silanus',92,1),(7538,'091084','I748','Sindia',92,1),(7539,'091085','I751','Siniscola',92,1),(7540,'091086','I851','Sorgono',92,1),(7541,'091090','L153','Teti',92,1),(7542,'091091','L160','Tiana',92,1),(7543,'091093','L202','Tonara',92,1),(7544,'091094','L231','Torpè',92,1),(7545,'091104','E649','Lodine',92,1),(7546,'092002','A419','Armungia',93,1),(7547,'092003','A474','Assemini',93,1),(7548,'092004','A597','Ballao',93,1),(7549,'092005','A677','Barrali',93,1),(7550,'092008','B274','Burcei',93,1),(7551,'092009','B354','Cagliari',93,1),(7552,'092011','B675','Capoterra',93,1),(7553,'092015','D259','Decimomannu',93,1),(7554,'092016','D260','Decimoputzu',93,1),(7555,'092017','D323','Dolianova',93,1),(7556,'092018','D333','Domus de Maria',93,1),(7557,'092020','D344','Donori',93,1),(7558,'092024','D994','Gesico',93,1),(7559,'092027','E084','Goni',93,1),(7560,'092030','E234','Guamaggiore',93,1),(7561,'092031','E252','Guasila',93,1),(7562,'092036','E877','Mandas',93,1),(7563,'092037','E903','Maracalagonis',93,1),(7564,'092038','F333','Monastir',93,1),(7565,'092039','F808','Muravera',93,1),(7566,'092042','F983','Nuraminis',93,1),(7567,'092044','G133','Ortacesus',93,1),(7568,'092048','G669','Pimentel',93,1),(7569,'092050','H088','Pula',93,1),(7570,'092051','H118','Quartu Sant\'Elena',93,1),(7571,'092053','H739','Samatzai',93,1),(7572,'092054','H766','San Basilio',93,1),(7573,'092058','G383','San Nicolò Gerrei',93,1),(7574,'092059','I166','San Sperate',93,1),(7575,'092061','I271','Sant\'Andrea Frius',93,1),(7576,'092064','I402','San Vito',93,1),(7577,'092066','I443','Sarroch',93,1),(7578,'092068','I580','Selargius',93,1),(7579,'092069','I582','Selegas',93,1),(7580,'092070','I615','Senorbì',93,1),(7581,'092071','I624','Serdiana',93,1),(7582,'092074','I695','Sestu',93,1),(7583,'092075','I699','Settimo San Pietro',93,1),(7584,'092078','I734','Siliqua',93,1),(7585,'092079','I735','Silius',93,1),(7586,'092080','I752','Sinnai',93,1),(7587,'092081','I765','Siurgus Donigala',93,1),(7588,'092082','I797','Soleminis',93,1),(7589,'092083','I995','Suelli',93,1),(7590,'092084','L154','Teulada',93,1),(7591,'092088','L512','Ussana',93,1),(7592,'092090','L521','Uta',93,1),(7593,'092091','L613','Vallermosa',93,1),(7594,'092097','L998','Villaputzu',93,1),(7595,'092098','M016','Villasalto',93,1),(7596,'092099','I118','Villa San Pietro',93,1),(7597,'092100','B738','Villasimius',93,1),(7598,'092101','M025','Villasor',93,1),(7599,'092102','M026','Villaspeciosa',93,1),(7600,'092105','H119','Quartucciu',93,1),(7601,'092106','M288','Castiadas',93,1),(7602,'092108','D399','Elmas',93,1),(7603,'092109','F383','Monserrato',93,1),(7604,'092110','D430','Escalaplano',93,1),(7605,'092111','D431','Escolca',93,1),(7606,'092112','D443','Esterzili',93,1),(7607,'092113','D982','Gergei',93,1),(7608,'092114','E336','Isili',93,1),(7609,'092115','F981','Nuragus',93,1),(7610,'092116','F982','Nurallao',93,1),(7611,'092117','F986','Nurri',93,1),(7612,'092118','G122','Orroli',93,1),(7613,'092119','H659','Sadali',93,1),(7614,'092120','I668','Serri',93,1),(7615,'092121','I707','Seulo',93,1),(7616,'092122','L992','Villanova Tulo',93,1),(7617,'093001','A283','Andreis',94,1),(7618,'093002','A354','Arba',94,1),(7619,'093003','A456','Arzene',94,1),(7620,'093004','A516','Aviano',94,1),(7621,'093005','A530','Azzano Decimo',94,1),(7622,'093006','A640','Barcis',94,1),(7623,'093007','B215','Brugnera',94,1),(7624,'093008','B247','Budoia',94,1),(7625,'093009','B598','Caneva',94,1),(7626,'093010','B940','Casarsa della Delizia',94,1),(7627,'093011','C217','Castelnovo del Friuli',94,1),(7628,'093012','C385','Cavasso Nuovo',94,1),(7629,'093013','C640','Chions',94,1),(7630,'093014','C699','Cimolais',94,1),(7631,'093015','C790','Claut',94,1),(7632,'093016','C791','Clauzetto',94,1),(7633,'093017','C991','Cordenons',94,1),(7634,'093018','C993','Cordovado',94,1),(7635,'093019','D426','Erto e Casso',94,1),(7636,'093020','D487','Fanna',94,1),(7637,'093021','D621','Fiume Veneto',94,1),(7638,'093022','D670','Fontanafredda',94,1),(7639,'093024','D804','Frisanco',94,1),(7640,'093025','E889','Maniago',94,1),(7641,'093026','F089','Meduno',94,1),(7642,'093027','F596','Montereale Valcellina',94,1),(7643,'093028','F750','Morsano al Tagliamento',94,1),(7644,'093029','G353','Pasiano di Pordenone',94,1),(7645,'093030','G680','Pinzano al Tagliamento',94,1),(7646,'093031','G780','Polcenigo',94,1),(7647,'093032','G886','Porcia',94,1),(7648,'093033','G888','Pordenone',94,1),(7649,'093034','G994','Prata di Pordenone',94,1),(7650,'093035','H010','Pravisdomini',94,1),(7651,'093036','H609','Roveredo in Piano',94,1),(7652,'093037','H657','Sacile',94,1),(7653,'093038','H891','San Giorgio della Richinvelda',94,1),(7654,'093039','H999','San Martino al Tagliamento',94,1),(7655,'093040','I136','San Quirino',94,1),(7656,'093041','I403','San Vito al Tagliamento',94,1),(7657,'093042','I621','Sequals',94,1),(7658,'093043','I686','Sesto al Reghena',94,1),(7659,'093044','I904','Spilimbergo',94,1),(7660,'093045','L324','Tramonti di Sopra',94,1),(7661,'093046','L325','Tramonti di Sotto',94,1),(7662,'093047','L347','Travesio',94,1),(7663,'093048','L657','Valvasone',94,1),(7664,'093049','M085','Vito d\'Asio',94,1),(7665,'093050','M096','Vivaro',94,1),(7666,'093051','M190','Zoppola',94,1),(7667,'093052','M265','Vajont',94,1),(7668,'094001','A051','Acquaviva d\'Isernia',95,1),(7669,'094002','A080','Agnone',95,1),(7670,'094003','A567','Bagnoli del Trigno',95,1),(7671,'094004','A761','Belmonte del Sannio',95,1),(7672,'094005','B630','Cantalupo nel Sannio',95,1),(7673,'094006','B682','Capracotta',95,1),(7674,'094007','B810','Carovilli',95,1),(7675,'094008','B830','Carpinone',95,1),(7676,'094009','C082','Castel del Giudice',95,1),(7677,'094010','C246','Castelpetroso',95,1),(7678,'094011','C247','Castelpizzuto',95,1),(7679,'094012','C270','Castel San Vincenzo',95,1),(7680,'094013','C200','Castelverrino',95,1),(7681,'094014','C534','Cerro al Volturno',95,1),(7682,'094015','C620','Chiauci',95,1),(7683,'094016','C769','Civitanova del Sannio',95,1),(7684,'094017','C878','Colli a Volturno',95,1),(7685,'094018','C941','Conca Casale',95,1),(7686,'094019','D595','Filignano',95,1),(7687,'094020','D703','Forlì del Sannio',95,1),(7688,'094021','D715','Fornelli',95,1),(7689,'094022','D811','Frosolone',95,1),(7690,'094023','E335','Isernia',95,1),(7691,'094024','E669','Longano',95,1),(7692,'094025','E778','Macchia d\'Isernia',95,1),(7693,'094026','E779','Macchiagodena',95,1),(7694,'094027','F239','Miranda',95,1),(7695,'094028','F429','Montaquila',95,1),(7696,'094029','F580','Montenero Val Cocchiara',95,1),(7697,'094030','F601','Monteroduni',95,1),(7698,'094031','G486','Pesche',95,1),(7699,'094032','G495','Pescolanciano',95,1),(7700,'094033','G497','Pescopennataro',95,1),(7701,'094034','G523','Pettoranello del Molise',95,1),(7702,'094035','G606','Pietrabbondante',95,1),(7703,'094036','G727','Pizzone',95,1),(7704,'094037','B317','Poggio Sannita',95,1),(7705,'094038','G954','Pozzilli',95,1),(7706,'094039','H308','Rionero Sannitico',95,1),(7707,'094040','H420','Roccamandolfi',95,1),(7708,'094041','H445','Roccasicura',95,1),(7709,'094042','H458','Rocchetta a Volturno',95,1),(7710,'094043','I096','San Pietro Avellana',95,1),(7711,'094044','I189','Sant\'Agapito',95,1),(7712,'094045','I238','Santa Maria del Molise',95,1),(7713,'094046','I282','Sant\'Angelo del Pesco',95,1),(7714,'094047','B466','Sant\'Elena Sannita',95,1),(7715,'094048','I507','Scapoli',95,1),(7716,'094049','I679','Sessano del Molise',95,1),(7717,'094050','I682','Sesto Campano',95,1),(7718,'094051','L696','Vastogirardi',95,1),(7719,'094052','L725','Venafro',95,1),(7720,'095001','A007','Abbasanta',96,1),(7721,'095002','A097','Aidomaggiore',96,1),(7722,'095003','A126','Albagiara',96,1),(7723,'095004','A180','Ales',96,1),(7724,'095005','A204','Allai',96,1),(7725,'095006','A357','Arborea',96,1),(7726,'095007','A380','Ardauli',96,1),(7727,'095008','A477','Assolo',96,1),(7728,'095009','A480','Asuni',96,1),(7729,'095010','A614','Baradili',96,1),(7730,'095011','A621','Baratili San Pietro',96,1),(7731,'095012','A655','Baressa',96,1),(7732,'095013','A721','Bauladu',96,1),(7733,'095014','A856','Bidonì',96,1),(7734,'095015','A960','Bonarcado',96,1),(7735,'095016','B055','Boroneddu',96,1),(7736,'095017','B281','Busachi',96,1),(7737,'095018','B314','Cabras',96,1),(7738,'095019','D200','Cuglieri',96,1),(7739,'095020','D695','Fordongianus',96,1),(7740,'095021','E004','Ghilarza',96,1),(7741,'095022','E087','Gonnoscodina',96,1),(7742,'095023','D585','Gonnosnò',96,1),(7743,'095024','E088','Gonnostramatza',96,1),(7744,'095025','E972','Marrubiu',96,1),(7745,'095026','F050','Masullas',96,1),(7746,'095027','F208','Milis',96,1),(7747,'095028','F270','Mogorella',96,1),(7748,'095029','F272','Mogoro',96,1),(7749,'095030','F727','Morgongiori',96,1),(7750,'095031','F840','Narbolia',96,1),(7751,'095032','F867','Neoneli',96,1),(7752,'095033','F934','Norbello',96,1),(7753,'095034','F974','Nughedu Santa Vittoria',96,1),(7754,'095035','F980','Nurachi',96,1),(7755,'095036','F985','Nureci',96,1),(7756,'095037','G043','Ollastra',96,1),(7757,'095038','G113','Oristano',96,1),(7758,'095039','G286','Palmas Arborea',96,1),(7759,'095040','G379','Pau',96,1),(7760,'095041','G384','Paulilatino',96,1),(7761,'095042','G817','Pompu',96,1),(7762,'095043','H301','Riola Sardo',96,1),(7763,'095044','F271','Ruinas',96,1),(7764,'095045','H756','Samugheo',96,1),(7765,'095046','A368','San Nicolò d\'Arcidano',96,1),(7766,'095047','I205','Santa Giusta',96,1),(7767,'095048','I298','Villa Sant\'Antonio',96,1),(7768,'095049','I374','Santu Lussurgiu',96,1),(7769,'095050','I384','San Vero Milis',96,1),(7770,'095051','I503','Scano di Montiferro',96,1),(7771,'095052','I564','Sedilo',96,1),(7772,'095053','I605','Seneghe',96,1),(7773,'095054','I609','Senis',96,1),(7774,'095055','I613','Sennariolo',96,1),(7775,'095056','I717','Siamaggiore',96,1),(7776,'095057','I718','Siamanna',96,1),(7777,'095058','I742','Simala',96,1),(7778,'095059','I743','Simaxis',96,1),(7779,'095060','I749','Sini',96,1),(7780,'095061','I757','Siris',96,1),(7781,'095062','I791','Solarussa',96,1),(7782,'095063','I861','Sorradile',96,1),(7783,'095064','L023','Tadasuni',96,1),(7784,'095065','L122','Terralba',96,1),(7785,'095066','L321','Tramatza',96,1),(7786,'095067','L393','Tresnuraghes',96,1),(7787,'095068','L488','Ulà Tirso',96,1),(7788,'095069','L496','Uras',96,1),(7789,'095070','L508','Usellus',96,1),(7790,'095071','L991','Villanova Truschedu',96,1),(7791,'095072','M030','Villaurbana',96,1),(7792,'095073','A609','Villa Verde',96,1),(7793,'095074','M153','Zeddiani',96,1),(7794,'095075','M168','Zerfaliu',96,1),(7795,'095076','I721','Siapiccia',96,1),(7796,'095077','D214','Curcuris',96,1),(7797,'095078','I778','Soddì',96,1),(7798,'095079','B068','Bosa',96,1),(7799,'095080','D640','Flussio',96,1),(7800,'095081','D968','Genoni',96,1),(7801,'095082','E400','Laconi',96,1),(7802,'095083','E825','Magomadas',96,1),(7803,'095084','F261','Modolo',96,1),(7804,'095085','F698','Montresta',96,1),(7805,'095086','H661','Sagama',96,1),(7806,'095087','L006','Suni',96,1),(7807,'095088','L172','Tinnura',96,1),(7808,'096001','A107','Ailoche',97,1),(7809,'096002','A280','Andorno Micca',97,1),(7810,'096003','A784','Benna',97,1),(7811,'096004','A859','Biella',97,1),(7812,'096005','A876','Bioglio',97,1),(7813,'096006','B058','Borriana',97,1),(7814,'096007','B229','Brusnengo',97,1),(7815,'096008','B417','Callabiana',97,1),(7816,'096009','B457','Camandona',97,1),(7817,'096010','B465','Camburzano',97,1),(7818,'096011','B508','Campiglia Cervo',97,1),(7819,'096012','B586','Candelo',97,1),(7820,'096013','B708','Caprile',97,1),(7821,'096014','B933','Casapinta',97,1),(7822,'096015','C155','Castelletto Cervo',97,1),(7823,'096016','C363','Cavaglià',97,1),(7824,'096017','C526','Cerreto Castello',97,1),(7825,'096018','C532','Cerrione',97,1),(7826,'096019','C819','Coggiola',97,1),(7827,'096020','D094','Cossato',97,1),(7828,'096021','D165','Crevacuore',97,1),(7829,'096022','D182','Crosa',97,1),(7830,'096023','D219','Curino',97,1),(7831,'096024','D339','Donato',97,1),(7832,'096025','D350','Dorzano',97,1),(7833,'096026','D848','Gaglianico',97,1),(7834,'096027','E024','Gifflenga',97,1),(7835,'096028','E130','Graglia',97,1),(7836,'096029','E552','Lessona',97,1),(7837,'096030','E821','Magnano',97,1),(7838,'096031','F037','Massazza',97,1),(7839,'096032','F042','Masserano',97,1),(7840,'096033','F167','Mezzana Mortigliengo',97,1),(7841,'096034','F189','Miagliano',97,1),(7842,'096035','F369','Mongrando',97,1),(7843,'096037','F776','Mottalciata',97,1),(7844,'096038','F833','Muzzano',97,1),(7845,'096039','F878','Netro',97,1),(7846,'096040','F992','Occhieppo Inferiore',97,1),(7847,'096041','F993','Occhieppo Superiore',97,1),(7848,'096042','G521','Pettinengo',97,1),(7849,'096043','G577','Piatto',97,1),(7850,'096044','G594','Piedicavallo',97,1),(7851,'096046','G798','Pollone',97,1),(7852,'096047','G820','Ponderano',97,1),(7853,'096048','G927','Portula',97,1),(7854,'096049','G980','Pralungo',97,1),(7855,'096050','G974','Pray',97,1),(7856,'096051','H103','Quaregna',97,1),(7857,'096052','H145','Quittengo',97,1),(7858,'096053','H538','Ronco Biellese',97,1),(7859,'096054','H553','Roppolo',97,1),(7860,'096055','H561','Rosazza',97,1),(7861,'096056','H662','Sagliano Micca',97,1),(7862,'096057','H681','Sala Biellese',97,1),(7863,'096058','H726','Salussola',97,1),(7864,'096059','H821','Sandigliano',97,1),(7865,'096060','I074','San Paolo Cervo',97,1),(7866,'096061','I596','Selve Marcone',97,1),(7867,'096062','I835','Soprana',97,1),(7868,'096063','I847','Sordevolo',97,1),(7869,'096064','I868','Sostegno',97,1),(7870,'096065','I980','Strona',97,1),(7871,'096066','L075','Tavigliano',97,1),(7872,'096067','L116','Ternengo',97,1),(7873,'096068','L193','Tollegno',97,1),(7874,'096069','L239','Torrazzo',97,1),(7875,'096070','L436','Trivero',97,1),(7876,'096071','L556','Valdengo',97,1),(7877,'096072','L586','Vallanzengo',97,1),(7878,'096073','L606','Valle Mosso',97,1),(7879,'096074','L620','Valle San Nicolao',97,1),(7880,'096075','L712','Veglio',97,1),(7881,'096076','L785','Verrone',97,1),(7882,'096077','L880','Vigliano Biellese',97,1),(7883,'096078','L933','Villa del Bosco',97,1),(7884,'096079','L978','Villanova Biellese',97,1),(7885,'096080','M098','Viverone',97,1),(7886,'096081','M179','Zimone',97,1),(7887,'096082','M196','Zubiena',97,1),(7888,'096083','M201','Zumaglia',97,1),(7889,'096084','M304','Mosso',97,1),(7890,'097001','A005','Abbadia Lariana',98,1),(7891,'097002','A112','Airuno',98,1),(7892,'097003','A301','Annone di Brianza',98,1),(7893,'097004','A594','Ballabio',98,1),(7894,'097005','A683','Barzago',98,1),(7895,'097006','A686','Barzanò',98,1),(7896,'097007','A687','Barzio',98,1),(7897,'097008','A745','Bellano',98,1),(7898,'097009','B081','Bosisio Parini',98,1),(7899,'097010','B194','Brivio',98,1),(7900,'097011','B261','Bulciago',98,1),(7901,'097012','B396','Calco',98,1),(7902,'097013','B423','Calolziocorte',98,1),(7903,'097014','B763','Carenno',98,1),(7904,'097015','B937','Casargo',98,1),(7905,'097016','B943','Casatenovo',98,1),(7906,'097017','B996','Cassago Brianza',98,1),(7907,'097018','C024','Cassina Valsassina',98,1),(7908,'097019','C187','Castello di Brianza',98,1),(7909,'097020','C521','Cernusco Lombardone',98,1),(7910,'097021','C563','Cesana Brianza',98,1),(7911,'097022','C752','Civate',98,1),(7912,'097023','C839','Colico',98,1),(7913,'097024','C851','Colle Brianza',98,1),(7914,'097025','D065','Cortenova',98,1),(7915,'097026','D112','Costa Masnaga',98,1),(7916,'097027','D131','Crandola Valsassina',98,1),(7917,'097028','D143','Cremella',98,1),(7918,'097029','D145','Cremeno',98,1),(7919,'097030','D280','Dervio',98,1),(7920,'097031','D327','Dolzago',98,1),(7921,'097032','D346','Dorio',98,1),(7922,'097033','D398','Ello',98,1),(7923,'097034','D428','Erve',98,1),(7924,'097035','D436','Esino Lario',98,1),(7925,'097036','D865','Galbiate',98,1),(7926,'097037','D913','Garbagnate Monastero',98,1),(7927,'097038','D926','Garlate',98,1),(7928,'097039','E287','Imbersago',98,1),(7929,'097040','E305','Introbio',98,1),(7930,'097041','E308','Introzzo',98,1),(7931,'097042','E507','Lecco',98,1),(7932,'097043','E581','Lierna',98,1),(7933,'097044','E656','Lomagna',98,1),(7934,'097045','E858','Malgrate',98,1),(7935,'097046','E879','Mandello del Lario',98,1),(7936,'097047','E947','Margno',98,1),(7937,'097048','F133','Merate',98,1),(7938,'097049','F248','Missaglia',98,1),(7939,'097050','F265','Moggio',98,1),(7940,'097051','F304','Molteno',98,1),(7941,'097052','F561','Monte Marenzo',98,1),(7942,'097053','F657','Montevecchia',98,1),(7943,'097054','F674','Monticello Brianza',98,1),(7944,'097055','F758','Morterone',98,1),(7945,'097056','F887','Nibionno',98,1),(7946,'097057','G009','Oggiono',98,1),(7947,'097058','G026','Olgiate Molgora',98,1),(7948,'097059','G030','Olginate',98,1),(7949,'097060','G040','Oliveto Lario',98,1),(7950,'097061','G161','Osnago',98,1),(7951,'097062','G218','Paderno d\'Adda',98,1),(7952,'097063','G241','Pagnona',98,1),(7953,'097064','G336','Parlasco',98,1),(7954,'097065','G368','Pasturo',98,1),(7955,'097066','G448','Perego',98,1),(7956,'097067','G456','Perledo',98,1),(7957,'097068','G485','Pescate',98,1),(7958,'097069','H028','Premana',98,1),(7959,'097070','H063','Primaluna',98,1),(7960,'097071','G223','Robbiate',98,1),(7961,'097072','H486','Rogeno',98,1),(7962,'097073','H596','Rovagnate',98,1),(7963,'097074','I243','Santa Maria Hoè',98,1),(7964,'097075','I759','Sirone',98,1),(7965,'097076','I761','Sirtori',98,1),(7966,'097077','I994','Sueglio',98,1),(7967,'097078','I996','Suello',98,1),(7968,'097079','L022','Taceno',98,1),(7969,'097080','L257','Torre de\' Busi',98,1),(7970,'097081','L368','Tremenico',98,1),(7971,'097082','L581','Valgreghentino',98,1),(7972,'097083','L634','Valmadrera',98,1),(7973,'097084','L680','Varenna',98,1),(7974,'097085','L731','Vendrogno',98,1),(7975,'097086','L751','Vercurago',98,1),(7976,'097087','L755','Verderio Inferiore',98,1),(7977,'097088','L756','Verderio Superiore',98,1),(7978,'097089','L813','Vestreno',98,1),(7979,'097090','L866','Viganò',98,1),(7980,'098001','A004','Abbadia Cerreto',99,1),(7981,'098002','A811','Bertonico',99,1),(7982,'098003','A919','Boffalora d\'Adda',99,1),(7983,'098004','A995','Borghetto Lodigiano',99,1),(7984,'098005','B017','Borgo San Giovanni',99,1),(7985,'098006','B141','Brembio',99,1),(7986,'098007','B456','Camairago',99,1),(7987,'098008','B887','Casaletto Lodigiano',99,1),(7988,'098009','B899','Casalmaiocco',99,1),(7989,'098010','B910','Casalpusterlengo',99,1),(7990,'098011','B961','Caselle Landi',99,1),(7991,'098012','B958','Caselle Lurani',99,1),(7992,'098013','C228','Castelnuovo Bocca d\'Adda',99,1),(7993,'098014','C304','Castiglione d\'Adda',99,1),(7994,'098015','C329','Castiraga Vidardo',99,1),(7995,'098016','C362','Cavacurta',99,1),(7996,'098017','C394','Cavenago d\'Adda',99,1),(7997,'098018','C555','Cervignano d\'Adda',99,1),(7998,'098019','C816','Codogno',99,1),(7999,'098020','C917','Comazzo',99,1),(8000,'098021','D021','Cornegliano Laudense',99,1),(8001,'098022','D028','Corno Giovine',99,1),(8002,'098023','D029','Cornovecchio',99,1),(8003,'098024','D068','Corte Palasio',99,1),(8004,'098025','D159','Crespiatica',99,1),(8005,'098026','D660','Fombio',99,1),(8006,'098027','D868','Galgagnano',99,1),(8007,'098028','E127','Graffignana',99,1),(8008,'098029','E238','Guardamiglio',99,1),(8009,'098030','E627','Livraga',99,1),(8010,'098031','E648','Lodi',99,1),(8011,'098032','E651','Lodi Vecchio',99,1),(8012,'098033','E777','Maccastorna',99,1),(8013,'098034','E840','Mairago',99,1),(8014,'098035','E852','Maleo',99,1),(8015,'098036','E994','Marudo',99,1),(8016,'098037','F028','Massalengo',99,1),(8017,'098038','F102','Meleti',99,1),(8018,'098039','F149','Merlino',99,1),(8019,'098040','F423','Montanaso Lombardo',99,1),(8020,'098041','F801','Mulazzano',99,1),(8021,'098042','G107','Orio Litta',99,1),(8022,'098043','G166','Ospedaletto Lodigiano',99,1),(8023,'098044','G171','Ossago Lodigiano',99,1),(8024,'098045','G096','Pieve Fissiraga',99,1),(8025,'098046','H701','Salerano sul Lambro',99,1),(8026,'098047','H844','San Fiorano',99,1),(8027,'098048','I012','San Martino in Strada',99,1),(8028,'098049','I140','San Rocco al Porto',99,1),(8029,'098050','I274','Sant\'Angelo Lodigiano',99,1),(8030,'098051','I362','Santo Stefano Lodigiano',99,1),(8031,'098052','I561','Secugnago',99,1),(8032,'098053','I612','Senna Lodigiana',99,1),(8033,'098054','I815','Somaglia',99,1),(8034,'098055','I848','Sordio',99,1),(8035,'098056','F260','Tavazzano con Villavesco',99,1),(8036,'098057','L125','Terranova dei Passerini',99,1),(8037,'098058','L469','Turano Lodigiano',99,1),(8038,'098059','L572','Valera Fratta',99,1),(8039,'098060','L977','Villanova del Sillaro',99,1),(8040,'098061','M158','Zelo Buon Persico',99,1),(8041,'099001','A747','Bellaria-Igea Marina',100,1),(8042,'099002','C357','Cattolica',100,1),(8043,'099003','D004','Coriano',100,1),(8044,'099004','D961','Gemmano',100,1),(8045,'099005','F244','Misano Adriatico',100,1),(8046,'099006','F346','Mondaino',100,1),(8047,'099007','F476','Monte Colombo',100,1),(8048,'099008','F502','Montefiore Conca',100,1),(8049,'099009','F523','Montegridolfo',100,1),(8050,'099010','F641','Montescudo',100,1),(8051,'099011','F715','Morciano di Romagna',100,1),(8052,'099012','G755','Poggio Berni',100,1),(8053,'099013','H274','Riccione',100,1),(8054,'099014','H294','Rimini',100,1),(8055,'099015','H724','Saludecio',100,1),(8056,'099016','H801','San Clemente',100,1),(8057,'099017','H921','San Giovanni in Marignano',100,1),(8058,'099018','I304','Santarcangelo di Romagna',100,1),(8059,'099019','I550','Torriana',100,1),(8060,'099020','L797','Verucchio',100,1),(8061,'100001','B626','Cantagallo',101,1),(8062,'100002','B794','Carmignano',101,1),(8063,'100003','F572','Montemurlo',101,1),(8064,'100004','G754','Poggio a Caiano',101,1),(8065,'100005','G999','Prato',101,1),(8066,'100006','L537','Vaiano',101,1),(8067,'100007','L775','Vernio',101,1),(8068,'101001','A772','Belvedere di Spinello',102,1),(8069,'101002','B319','Caccuri',102,1),(8070,'101003','B771','Carfizzi',102,1),(8071,'101004','B857','Casabona',102,1),(8072,'101005','B968','Castelsilano',102,1),(8073,'101006','C501','Cerenzia',102,1),(8074,'101007','C725','Cirò',102,1),(8075,'101008','C726','Cirò Marina',102,1),(8076,'101009','D123','Cotronei',102,1),(8077,'101010','D122','Crotone',102,1),(8078,'101011','D189','Crucoli',102,1),(8079,'101012','D236','Cutro',102,1),(8080,'101013','E339','Isola di Capo Rizzuto',102,1),(8081,'101014','F108','Melissa',102,1),(8082,'101015','F157','Mesoraca',102,1),(8083,'101016','G278','Pallagorio',102,1),(8084,'101017','G508','Petilia Policastro',102,1),(8085,'101018','H383','Roccabernarda',102,1),(8086,'101019','H403','Rocca di Neto',102,1),(8087,'101020','I026','San Mauro Marchesato',102,1),(8088,'101021','I057','San Nicola dell\'Alto',102,1),(8089,'101022','I308','Santa Severina',102,1),(8090,'101023','I468','Savelli',102,1),(8091,'101024','I494','Scandale',102,1),(8092,'101025','I982','Strongoli',102,1),(8093,'101026','L492','Umbriatico',102,1),(8094,'101027','L802','Verzino',102,1),(8095,'102001','A043','Acquaro',103,1),(8096,'102002','A386','Arena',103,1),(8097,'102003','B169','Briatico',103,1),(8098,'102004','B197','Brognaturo',103,1),(8099,'102005','B655','Capistrano',103,1),(8100,'102006','C581','Cessaniti',103,1),(8101,'102007','D253','Dasà',103,1),(8102,'102008','D303','Dinami',103,1),(8103,'102009','D364','Drapia',103,1),(8104,'102010','D453','Fabrizia',103,1),(8105,'102011','D587','Filadelfia',103,1),(8106,'102012','D589','Filandari',103,1),(8107,'102013','D596','Filogaso',103,1),(8108,'102014','D762','Francavilla Angitola',103,1),(8109,'102015','D767','Francica',103,1),(8110,'102016','D988','Gerocarne',103,1),(8111,'102017','E321','Ionadi',103,1),(8112,'102018','E389','Joppolo',103,1),(8113,'102019','E590','Limbadi',103,1),(8114,'102020','E836','Maierato',103,1),(8115,'102021','F207','Mileto',103,1),(8116,'102022','F364','Mongiana',103,1),(8117,'102023','F607','Monterosso Calabro',103,1),(8118,'102024','F843','Nardodipace',103,1),(8119,'102025','F893','Nicotera',103,1),(8120,'102026','G335','Parghelia',103,1),(8121,'102027','G722','Pizzo',103,1),(8122,'102028','G728','Pizzoni',103,1),(8123,'102029','G785','Polia',103,1),(8124,'102030','H271','Ricadi',103,1),(8125,'102031','H516','Rombiolo',103,1),(8126,'102032','H785','San Calogero',103,1),(8127,'102033','H807','San Costantino Calabro',103,1),(8128,'102034','H941','San Gregorio d\'Ippona',103,1),(8129,'102035','I058','San Nicola da Crissa',103,1),(8130,'102036','I350','Sant\'Onofrio',103,1),(8131,'102037','I639','Serra San Bruno',103,1),(8132,'102038','I744','Simbario',103,1),(8133,'102039','I853','Sorianello',103,1),(8134,'102040','I854','Soriano Calabro',103,1),(8135,'102041','I884','Spadola',103,1),(8136,'102042','I905','Spilinga',103,1),(8137,'102043','I945','Stefanaconi',103,1),(8138,'102044','L452','Tropea',103,1),(8139,'102045','L607','Vallelonga',103,1),(8140,'102046','L699','Vazzano',103,1),(8141,'102047','F537','Vibo Valentia',103,1),(8142,'102048','M138','Zaccanopoli',103,1),(8143,'102049','M143','Zambrone',103,1),(8144,'102050','M204','Zungri',103,1),(8145,'103001','A317','Antrona Schieranco',104,1),(8146,'103002','A325','Anzola d\'Ossola',104,1),(8147,'103003','A409','Arizzano',104,1),(8148,'103004','A427','Arola',104,1),(8149,'103005','A497','Aurano',104,1),(8150,'103006','A534','Baceno',104,1),(8151,'103007','A610','Bannio Anzino',104,1),(8152,'103008','A725','Baveno',104,1),(8153,'103009','A733','Bee',104,1),(8154,'103010','A742','Belgirate',104,1),(8155,'103011','A834','Beura-Cardezza',104,1),(8156,'103012','A925','Bognanco',104,1),(8157,'103013','B207','Brovello-Carpugnino',104,1),(8158,'103014','B380','Calasca-Castiglione',104,1),(8159,'103015','B463','Cambiasca',104,1),(8160,'103016','B610','Cannero Riviera',104,1),(8161,'103017','B615','Cannobio',104,1),(8162,'103018','B694','Caprezzo',104,1),(8163,'103019','B876','Casale Corte Cerro',104,1),(8164,'103020','C367','Cavaglio-Spoccia',104,1),(8165,'103021','C478','Ceppo Morelli',104,1),(8166,'103022','C567','Cesara',104,1),(8167,'103023','D099','Cossogno',104,1),(8168,'103024','D134','Craveggia',104,1),(8169,'103025','D168','Crevoladossola',104,1),(8170,'103026','D177','Crodo',104,1),(8171,'103027','D225','Cursolo-Orasso',104,1),(8172,'103028','D332','Domodossola',104,1),(8173,'103029','D374','Druogno',104,1),(8174,'103030','D481','Falmenta',104,1),(8175,'103031','D706','Formazza',104,1),(8176,'103032','D984','Germagno',104,1),(8177,'103033','E003','Ghiffa',104,1),(8178,'103034','E028','Gignese',104,1),(8179,'103035','E153','Gravellona Toce',104,1),(8180,'103036','E269','Gurro',104,1),(8181,'103037','E304','Intragna',104,1),(8182,'103038','E685','Loreglia',104,1),(8183,'103039','E790','Macugnaga',104,1),(8184,'103040','E795','Madonna del Sasso',104,1),(8185,'103041','E853','Malesco',104,1),(8186,'103042','F010','Masera',104,1),(8187,'103043','F048','Massiola',104,1),(8188,'103044','F146','Mergozzo',104,1),(8189,'103045','F192','Miazzina',104,1),(8190,'103046','F483','Montecrestese',104,1),(8191,'103047','F639','Montescheno',104,1),(8192,'103048','F932','Nonio',104,1),(8193,'103049','G007','Oggebbio',104,1),(8194,'103050','G062','Omegna',104,1),(8195,'103051','G117','Ornavasso',104,1),(8196,'103052','G280','Pallanzeno',104,1),(8197,'103053','G600','Piedimulera',104,1),(8198,'103054','G658','Pieve Vergonte',104,1),(8199,'103055','H030','Premeno',104,1),(8200,'103056','H033','Premia',104,1),(8201,'103057','H037','Premosello-Chiovenda',104,1),(8202,'103058','H106','Quarna Sopra',104,1),(8203,'103059','H107','Quarna Sotto',104,1),(8204,'103060','H203','Re',104,1),(8205,'103061','H777','San Bernardino Verbano',104,1),(8206,'103062','I249','Santa Maria Maggiore',104,1),(8207,'103063','I619','Seppiana',104,1),(8208,'103064','I976','Stresa',104,1),(8209,'103065','L187','Toceno',104,1),(8210,'103066','L333','Trarego Viggiona',104,1),(8211,'103067','L336','Trasquera',104,1),(8212,'103068','L450','Trontano',104,1),(8213,'103069','L651','Valstrona',104,1),(8214,'103070','L666','Vanzone con San Carlo',104,1),(8215,'103071','L691','Varzo',104,1),(8216,'103072','L746','Verbania',104,1),(8217,'103073','L864','Viganella',104,1),(8218,'103074','L889','Vignone',104,1),(8219,'103075','L906','Villadossola',104,1),(8220,'103076','M042','Villette',104,1),(8221,'103077','M111','Vogogna',104,1),(8222,'104001','A069','Aggius',105,1),(8223,'104002','H848','Aglientu',105,1),(8224,'104003','A115','Alà dei Sardi',105,1),(8225,'104004','A453','Arzachena',105,1),(8226,'104005','M214','Badesi',105,1),(8227,'104006','A789','Berchidda',105,1),(8228,'104007','B063','Bortigiadas',105,1),(8229,'104008','B246','Buddusò',105,1),(8230,'104009','B248','Budoni',105,1),(8231,'104010','B378','Calangianus',105,1),(8232,'104011','M274','Golfo Aranci',105,1),(8233,'104012','E425','La Maddalena',105,1),(8234,'104013','M275','Loiri Porto San Paolo',105,1),(8235,'104014','E747','Luogosanto',105,1),(8236,'104015','E752','Luras',105,1),(8237,'104016','F667','Monti',105,1),(8238,'104017','G015','Olbia',105,1),(8239,'104018','G153','Oschiri',105,1),(8240,'104019','M301','Padru',105,1),(8241,'104020','G258','Palau',105,1),(8242,'104021','M276','Sant\'Antonio di Gallura',105,1),(8243,'104022','I312','Santa Teresa Gallura',105,1),(8244,'104023','I329','San Teodoro',105,1),(8245,'104024','L088','Telti',105,1),(8246,'104025','L093','Tempio Pausania',105,1),(8247,'104026','L428','Trinità d\'Agultu e Vignola',105,1),(8248,'105001','A454','Arzana',106,1),(8249,'105002','A663','Bari Sardo',106,1),(8250,'105003','A722','Baunei',106,1),(8251,'105004','M285','Cardedu',106,1),(8252,'105005','D395','Elini',106,1),(8253,'105006','D859','Gairo',106,1),(8254,'105007','E049','Girasole',106,1),(8255,'105008','E283','Ilbono',106,1),(8256,'105009','E387','Jerzu',106,1),(8257,'105010','E441','Lanusei',106,1),(8258,'105011','E644','Loceri',106,1),(8259,'105012','E700','Lotzorai',106,1),(8260,'105013','G158','Osini',106,1),(8261,'105014','G445','Perdasdefogu',106,1),(8262,'105015','I706','Seui',106,1),(8263,'105016','L036','Talana',106,1),(8264,'105017','L140','Tertenia',106,1),(8265,'105018','A355','Tortolì',106,1),(8266,'105019','L423','Triei',106,1),(8267,'105020','L489','Ulassai',106,1),(8268,'105021','L506','Urzulei',106,1),(8269,'105022','L514','Ussassai',106,1),(8270,'105023','L953','Villagrande Strisaili',106,1),(8271,'106001','A359','Arbus',107,1),(8272,'106002','A681','Barumini',107,1),(8273,'106003','C882','Collinas',107,1),(8274,'106004','D827','Furtei',107,1),(8275,'106005','D970','Genuri',107,1),(8276,'106006','D997','Gesturi',107,1),(8277,'106007','E085','Gonnosfanadiga',107,1),(8278,'106008','E270','Guspini',107,1),(8279,'106009','E464','Las Plassas',107,1),(8280,'106010','E742','Lunamatrona',107,1),(8281,'106011','G207','Pabillonis',107,1),(8282,'106012','G382','Pauli Arbarei',107,1),(8283,'106013','H738','Samassi',107,1),(8284,'106014','H856','San Gavino Monreale',107,1),(8285,'106015','H974','Sanluri',107,1),(8286,'106016','I428','Sardara',107,1),(8287,'106017','I570','Segariu',107,1),(8288,'106018','I647','Serramanna',107,1),(8289,'106019','I667','Serrenti',107,1),(8290,'106020','I705','Setzu',107,1),(8291,'106021','I724','Siddi',107,1),(8292,'106022','L463','Tuili',107,1),(8293,'106023','L473','Turri',107,1),(8294,'106024','L513','Ussaramanna',107,1),(8295,'106025','L924','Villacidro',107,1),(8296,'106026','L966','Villamar',107,1),(8297,'106027','L986','Villanovaforru',107,1),(8298,'106028','L987','Villanovafranca',107,1),(8299,'107001','B250','Buggerru',108,1),(8300,'107002','B383','Calasetta',108,1),(8301,'107003','B745','Carbonia',108,1),(8302,'107004','B789','Carloforte',108,1),(8303,'107005','D334','Domusnovas',108,1),(8304,'107006','D639','Fluminimaggiore',108,1),(8305,'107007','E022','Giba',108,1),(8306,'107008','E086','Gonnesa',108,1),(8307,'107009','E281','Iglesias',108,1),(8308,'107010','M270','Masainas',108,1),(8309,'107011','F822','Musei',108,1),(8310,'107012','F841','Narcao',108,1),(8311,'107013','F991','Nuxis',108,1),(8312,'107014','G446','Perdaxius',108,1),(8313,'107015','M291','Piscinas',108,1),(8314,'107016','G922','Portoscuso',108,1),(8315,'107017','G287','San Giovanni Suergiu',108,1),(8316,'107018','I182','Santadi',108,1),(8317,'107019','M209','Sant\'Anna Arresi',108,1),(8318,'107020','I294','Sant\'Antioco',108,1),(8319,'107021','L337','Tratalias',108,1),(8320,'107022','L968','Villamassargia',108,1),(8321,'107023','M278','Villaperuccio',108,1),(8329,'999301','Z200','AFGHANISTAN',1,1),(8330,'999201','Z100','ALBANIA',1,1),(8331,'999401','Z301','ALGERIA',1,1),(8332,'999202','Z101','ANDORRA',1,1),(8333,'999402','Z302','ANGOLA',1,1),(8334,'999502','Z529','ANGUILLA (ISOLA)',1,1),(8335,'999503','Z532','ANTIGUA E BARBUDA',1,1),(8336,'999504','Z501','ANTILLE OLANDESI',1,1),(8337,'999302','Z203','ARABIA SAUDITA',1,1),(8338,'999602','Z600','ARGENTINA',1,1),(8339,'999358','Z252','ARMENIA',1,1),(8340,'999359','Z253','ARZEBAIGIAN',1,1),(8341,'999701','Z700','AUSTRALIA',1,1),(8342,'999203','Z102','AUSTRIA',1,1),(8343,'999505','Z502','BAHAMAS',1,1),(8344,'999304','Z204','BAHREIN',1,1),(8345,'999305','Z249','BANGLADESH',1,1),(8346,'999506','Z522','BARBADOS',1,1),(8347,'999206','Z103','BELGIO',1,1),(8348,'999507','Z512','BELIZE',1,1),(8349,'999406','Z314','BENIN',1,1),(8350,'999508','Z400','BERMUDA (ISOLE)',1,1),(8351,'999306','Z205','BHUTAN',1,1),(8352,'999256','Z139','BIELORUSSIA',1,1),(8353,'999604','Z601','BOLIVIA',1,1),(8354,'999407','Z364','BOPHUTHATSWANA',1,1),(8355,'999252','Z153','BOSNIA-ERZEGOVINA',1,1),(8356,'999408','Z358','BOTSWANA',1,1),(8357,'999605','Z602','BRASILE',1,1),(8358,'999309','Z207','BRUNEI',1,1),(8359,'999209','Z104','BULGARIA',1,1),(8360,'999409','Z354','BURKINA FASO',1,1),(8361,'999410','Z305','BURUNDI',1,1),(8362,'999310','Z208','CAMBOGIA',1,1),(8363,'999411','Z306','CAMERUN',1,1),(8364,'999509','Z401','CANADA',1,1),(8365,'999413','Z307','CAPO VERDE',1,1),(8366,'999511','Z530','CAYMAN (ISOLE)',1,1),(8367,'999210','Z105','CECOSLOVACCHIA',1,1),(8368,'999312','Z702','CHRISTMAS (ISOLA)',1,1),(8369,'999415','Z309','CIAD',1,1),(8370,'999606','Z603','CILE',1,1),(8371,'999314','Z210','CINA',1,1),(8372,'999315','Z211','CIPRO',1,1),(8373,'999416','Z367','CISKEI',1,1),(8374,'999246','Z106','CITTA\' DEL VATICANO',1,1),(8375,'999318','Z212','COCOS (ISOLE)',1,1),(8376,'999608','Z604','COLOMBIA',1,1),(8377,'999417','Z310','COMORE',1,1),(8378,'999418','Z311','CONGO',1,1),(8379,'999463','Z312','CONGO,REPUBBLICA DEMOCRATICA DEL(EX ZAIRE)',1,1),(8380,'999702','Z703','COOK (ISOLE)',1,1),(8381,'999319','Z214','COREA DEL NORD',1,1),(8382,'999320','Z213','COREA DEL SUD',1,1),(8383,'999404','Z313','COSTA D\'AVORIO',1,1),(8384,'999513','Z503','COSTA RICA',1,1),(8385,'999250','Z149','CROAZIA',1,1),(8386,'999514','Z504','CUBA',1,1),(8387,'999212','Z107','DANIMARCA',1,1),(8388,'999515','Z526','DOMINICA',1,1),(8389,'999609','Z605','ECUADOR',1,1),(8390,'999419','Z336','EGITTO',1,1),(8391,'999517','Z506','EL SALVADOR',1,1),(8392,'999322','Z215','EMIRATI ARABI UNITI',1,1),(8393,'999466','Z368','ERITREA',1,1),(8394,'999247','Z144','ESTONIA',1,1),(8395,'999420','Z315','ETIOPIA',1,1),(8396,'999213','Z108','FAER OER(ISOLE)',1,1),(8397,'999703','Z704','FIGI',1,1),(8398,'999323','Z216','FILIPPINE',1,1),(8399,'999214','Z109','FINLANDIA',1,1),(8400,'999215','Z110','FRANCIA',1,1),(8401,'999421','Z316','GABON',1,1),(8402,'999422','Z317','GAMBIA',1,1),(8403,'999360','Z254','GEORGIA',1,1),(8404,'999216','Z112','GERMANIA',1,1),(8405,'999217','Z111','GERMANIA REPUBBLICA DEMOCRATICA',1,1),(8406,'999423','Z318','GHANA',1,1),(8407,'999518','Z507','GIAMAICA',1,1),(8408,'999326','Z219','GIAPPONE',1,1),(8409,'999218','Z113','GIBILTERRA',1,1),(8410,'999424','Z361','GIBUTI',1,1),(8411,'999327','Z220','GIORDANIA',1,1),(8412,'999219','Z114','GRAN BRETAGNA',1,1),(8413,'999220','Z115','GRECIA',1,1),(8414,'999519','Z524','GRENADA',1,1),(8415,'999520','Z402','GROENLANDIA',1,1),(8416,'999521','Z508','GUADALUPA',1,1),(8417,'999613','Z607','GUAIANA FRANCESE',1,1),(8418,'999523','Z509','GUATEMALA',1,1),(8419,'999425','Z319','GUINEA',1,1),(8420,'999426','Z320','GUINEA BISSAU',1,1),(8421,'999427','Z321','GUINEA EQUATORIALE',1,1),(8422,'999612','Z606','GUYANA',1,1),(8423,'999524','Z510','HAITI',1,1),(8424,'999525','Z511','HONDURAS',1,1),(8425,'999329','Z221','HONG KONG',1,1),(8426,'999330','Z222','INDIA',1,1),(8427,'999331','Z223','INDONESIA',1,1),(8428,'999332','Z224','IRAN',1,1),(8429,'999333','Z225','IRAQ',1,1),(8430,'999706','Z707','IRIAN OCCIDENTALE',1,1),(8431,'999221','Z116','IRLANDA',1,1),(8432,'999223','Z117','ISLANDA',1,1),(8433,'999445','Z324','ISOLA DELLA RIUNIONE',1,1),(8434,'999704','Z706','ISOLA DI GUAM',1,1),(8435,'999722','Z721','ISOLA DI PASQUA',1,1),(8436,'999610','Z609','ISOLE FALKLAND',1,1),(8437,'999711','Z710','ISOLE MARIANNE',1,1),(8438,'999714','Z712','ISOLE MIDWAY',1,1),(8439,'999729','Z727','ISOLE TOKELAU',1,1),(8440,'999734','Z729','ISOLE WALLIS E FUTUNA',1,1),(8441,'999334','Z226','ISRAELE',1,1),(8442,'999356','Z255','KAZAKISTAN',1,1),(8443,'999428','Z322','KENIA',1,1),(8444,'999361','Z256','KIRGHIZISTAN',1,1),(8445,'999708','Z731','KIRIBATI',1,1),(8446,'999272','Z160','KOSOVO',1,1),(8447,'999335','Z227','KUWAIT',1,1),(8448,'999336','Z228','LAOS',1,1),(8449,'999429','Z359','LESOTHO',1,1),(8450,'999248','Z145','LETTONIA',1,1),(8451,'999337','Z229','LIBANO',1,1),(8452,'999430','Z325','LIBERIA',1,1),(8453,'999431','Z326','LIBIA',1,1),(8454,'999225','Z119','LIECHTENSTEIN',1,1),(8455,'999249','Z146','LITUANIA',1,1),(8456,'999226','Z120','LUSSEMBURGO',1,1),(8457,'999338','Z231','MACAO',1,1),(8458,'999253','Z148','MACEDONIA',1,1),(8459,'999432','Z327','MADAGASCAR',1,1),(8460,'999434','Z328','MALAWI',1,1),(8461,'999340','Z247','MALAYSIA',1,1),(8462,'999339','Z232','MALDIVE',1,1),(8463,'999435','Z329','MALI',1,1),(8464,'999227','Z121','MALTA',1,1),(8465,'999228','Z122','MAN (ISOLA)',1,1),(8466,'999709','Z708','MAQUARIE (ISOLE)',1,1),(8467,'999436','Z330','MAROCCO',1,1),(8468,'999712','Z711','MARSHALL',1,1),(8469,'999526','Z513','MARTINICA',1,1),(8470,'999437','Z331','MAURITANIA',1,1),(8471,'999438','Z332','MAURITIUS',1,1),(8472,'999439','Z360','MAYOTTE (ISOLA)',1,1),(8473,'999527','Z514','MESSICO',1,1),(8474,'999713','Z735','MICRONESIA STATI FEDERATI',1,1),(8475,'999254','Z140','MOLDOVA',1,1),(8476,'999341','Z233','MONGOLIA',1,1),(8477,'999270','Z159','MONTENEGRO',1,1),(8478,'999528','Z531','MONTSERRAT',1,1),(8479,'999440','Z333','MOZAMBICO',1,1),(8480,'999307','Z206','MYANMAR (EX BIRMANIA)',1,1),(8481,'999441','Z300','NAMIBIA',1,1),(8482,'999715','Z713','NAURU',1,1),(8483,'999342','Z234','NEPAL',1,1),(8484,'999529','Z515','NICARAGUA',1,1),(8485,'999442','Z334','NIGER',1,1),(8486,'999443','Z335','NIGERIA',1,1),(8487,'999716','Z714','NIUE (ISOLA)',1,1),(8488,'999717','Z715','NORFOLK (ISOLE)',1,1),(8489,'999230','Z124','NORMANNE (ISOLE)',1,1),(8490,'999231','Z125','NORVEGIA',1,1),(8491,'999718','Z716','NUOVA CALEDONIA (ISOLE)',1,1),(8492,'999719','Z719','NUOVA ZELANDA',1,1),(8493,'999343','Z235','OMAN',1,1),(8494,'999232','Z126','PAESI BASSI',1,1),(8495,'999344','Z236','PAKISTAN',1,1),(8496,'999720','Z734','PALAU',1,1),(8497,'999530','Z516','PANAMA',1,1),(8498,'999510','Z517','PANAMA ZONA DEL CANALE',1,1),(8499,'999721','Z730','PAPUASIA NUOVA GUINEA',1,1),(8500,'999614','Z610','PARAGUAY',1,1),(8501,'999615','Z611','PERU\'',1,1),(8502,'999723','Z722','PITCAIRN (ISOLA)',1,1),(8503,'999724','Z723','POLINESIA FRANCESE',1,1),(8504,'999233','Z127','POLONIA',1,1),(8505,'999234','Z128','PORTOGALLO',1,1),(8506,'999229','Z123','PRINCIPATO DI MONACO',1,1),(8507,'999531','Z518','PUERTO RICO',1,1),(8508,'999345','Z237','QATAR',1,1),(8509,'999257','Z156','REPUBBLICA CECA',1,1),(8510,'999414','Z308','REPUBBLICA CENTRAFRICANA',1,1),(8511,'999516','Z505','REPUBBLICA DOMINICANA',1,1),(8512,'999454','Z347','REPUBBLICA SUDAFRICANA',1,1),(8513,'999235','Z129','ROMANIA',1,1),(8514,'999446','Z338','RUANDA',1,1),(8515,'999245','Z154','RUSSIA',1,1),(8516,'999534','Z533','SAINT KITTS E NEVIS',1,1),(8517,'999535','Z403','SAINT PIERRE ET MIQUELON (ISOLE)',1,1),(8518,'999533','Z528','SAINT VINCENT E GRENADINE',1,1),(8519,'999725','Z724','SALOMONE,ISOLE',1,1),(8520,'999727','Z726','SAMOA',1,1),(8521,'999726','Z725','SAMOA AMERICANE (ISOLE)',1,1),(8522,'999236','Z130','SAN MARINO',1,1),(8523,'999532','Z527','SANTA LUCIA',1,1),(8524,'999447','Z340','SANT\'ELENA',1,1),(8525,'999448','Z341','SAO TOME E PRINCIPE',1,1),(8526,'999450','Z343','SENEGAL',1,1),(8527,'999271','Z158','SERBIA',1,1),(8528,'999224','Z157','SERBIA E MONTENEGRO',1,1),(8529,'999449','Z342','SEYCHELLES',1,1),(8530,'999451','Z344','SIERRA LEONE',1,1),(8531,'999346','Z248','SINGAPORE',1,1),(8532,'999348','Z240','SIRIA',1,1),(8533,'999255','Z155','SLOVACCHIA',1,1),(8534,'999251','Z150','SLOVENIA',1,1),(8535,'999453','Z345','SOMALIA',1,1),(8536,'999239','Z131','SPAGNA',1,1),(8537,'999311','Z209','SRI LANKA',1,1),(8538,'999536','Z404','STATI UNITI D\'AMERICA',1,1),(8539,'999455','Z348','SUDAN',1,1),(8540,'999616','Z608','SURINAME',1,1),(8541,'999240','Z132','SVEZIA',1,1),(8542,'999241','Z133','SVIZZERA',1,1),(8543,'999456','Z349','SWAZILAND',1,1),(8544,'999362','Z257','TAGIKISTAN',1,1),(8545,'999363','Z217','TAIWAN',1,1),(8546,'999457','Z357','TANZANIA',1,1),(8547,'999324','Z218','TERRITORIO DI GAZA',1,1),(8548,'999349','Z241','THAILANDIA',1,1),(8549,'999350','Z242','TIMOR (ISOLA)',1,1),(8550,'999458','Z351','TOGO',1,1),(8551,'999730','Z728','TONGA',1,1),(8552,'999459','Z365','TRANSKEI',1,1),(8553,'999617','Z612','TRINIDAD E TOBAGO',1,1),(8554,'999460','Z352','TUNISIA',1,1),(8555,'999351','Z243','TURCHIA',1,1),(8556,'999364','Z258','TURKMENISTAN',1,1),(8557,'999537','Z519','TURKS E CAICOS (ISOLE)',1,1),(8558,'999731','Z732','TUVALU',1,1),(8559,'999243','Z138','UCRAINA',1,1),(8560,'999461','Z353','UGANDA',1,1),(8561,'999244','Z134','UNGHERIA',1,1),(8562,'999618','Z613','URUGUAY',1,1),(8563,'999357','Z259','UZBEKISTAN',1,1),(8564,'999732','Z733','VANUATU',1,1),(8565,'999462','Z366','VENDA',1,1),(8566,'999619','Z614','VENEZUELA',1,1),(8567,'999538','Z520','VERGINI AMERICANE (ISOLE)',1,1),(8568,'999539','Z525','VERGINI BRITANNICHE (ISOLE)',1,1),(8569,'999353','Z251','VIETNAM',1,1),(8570,'999354','Z246','YEMEN',1,1),(8571,'999464','Z355','ZAMBIA',1,1),(8572,'999465','Z337','ZIMBABWE',1,1);
/*!40000 ALTER TABLE `comuni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config_index_settings`
--

DROP TABLE IF EXISTS `config_index_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_index_settings` (
  `IndexConfigKey` varchar(255) NOT NULL,
  `Mode` varchar(255) DEFAULT NULL,
  `SolrHostAddress` varchar(255) DEFAULT NULL,
  `SolrHostTcpPort` int(11) DEFAULT NULL,
  `LuceneSettingsFolderPath` varchar(255) DEFAULT NULL,
  `ZoomSettingsFolderPath` varchar(255) DEFAULT NULL,
  `SolrCore` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IndexConfigKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_index_settings`
--

LOCK TABLES `config_index_settings` WRITE;
/*!40000 ALTER TABLE `config_index_settings` DISABLE KEYS */;
INSERT INTO `config_index_settings` VALUES ('IndexConfig','zoom',NULL,0,NULL,'/ricerca',NULL);
/*!40000 ALTER TABLE `config_index_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config_login_settings`
--

DROP TABLE IF EXISTS `config_login_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_login_settings` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LoginControlNamespace` varchar(255) DEFAULT NULL,
  `LoginControlAssemblyName` varchar(255) DEFAULT NULL,
  `RifNetwork` int(11) DEFAULT NULL,
  `ShowDebugControl` tinyint(1) DEFAULT NULL,
  `ShowPasswordRecoveryLink` tinyint(1) DEFAULT NULL,
  `ShowRegistrationLink` tinyint(1) DEFAULT NULL,
  `RedirectUrl` varchar(255) DEFAULT NULL,
  `RegistrationtUrl` varchar(255) DEFAULT NULL,
  `AutenticationUrl` varchar(255) DEFAULT NULL,
  `PasswordRecoveryUrl` varchar(255) DEFAULT NULL,
  `PagePosition` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_login_settings`
--

LOCK TABLES `config_login_settings` WRITE;
/*!40000 ALTER TABLE `config_login_settings` DISABLE KEYS */;
INSERT INTO `config_login_settings` VALUES (1,'NetCms.Users.LoginControlStandard','Structure, Version=1.0.0.346, Culture=neutral, PublicKeyToken=null',0,0,0,0,'','','','','Intestazione');
/*!40000 ALTER TABLE `config_login_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config_settings`
--

DROP TABLE IF EXISTS `config_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_settings` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RifNetwork` int(11) DEFAULT NULL,
  `PortalName` varchar(255) DEFAULT NULL,
  `Layout` varchar(255) DEFAULT NULL,
  `TopMenuState` varchar(255) DEFAULT NULL,
  `TopMenuStyle` varchar(255) DEFAULT NULL,
  `Theme` varchar(255) DEFAULT NULL,
  `SupportedAttachmentsExtensions` varchar(255) DEFAULT NULL,
  `CmsAppAttachToContentStatus` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_settings`
--

LOCK TABLES `config_settings` WRITE;
/*!40000 ALTER TABLE `config_settings` DISABLE KEYS */;
INSERT INTO `config_settings` VALUES (1,0,'Cms3 Start','0','On','Megamenu','agid2019',NULL,NULL);
/*!40000 ALTER TABLE `config_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `console_scheduler_jobs`
--

DROP TABLE IF EXISTS `console_scheduler_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `console_scheduler_jobs` (
  `IdJob` int(11) NOT NULL,
  PRIMARY KEY (`IdJob`),
  KEY `IdJob` (`IdJob`),
  CONSTRAINT `FK42994A0614F63EB1` FOREIGN KEY (`IdJob`) REFERENCES `scheduler_jobs` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `console_scheduler_jobs`
--

LOCK TABLES `console_scheduler_jobs` WRITE;
/*!40000 ALTER TABLE `console_scheduler_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `console_scheduler_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id_Contact` int(11) NOT NULL AUTO_INCREMENT,
  `Name_Contact` varchar(255) DEFAULT NULL,
  `Surname_Contact` varchar(255) DEFAULT NULL,
  `Text_Contact` varchar(2000) DEFAULT NULL,
  `Subject_Contact` varchar(1000) DEFAULT NULL,
  `Email_Contact` varchar(255) DEFAULT NULL,
  `Tel_Contact` varchar(255) DEFAULT NULL,
  `Fax_Contact` varchar(255) DEFAULT NULL,
  `Group_Contact` int(11) DEFAULT NULL,
  `Stato_Contact` int(11) DEFAULT '0',
  `UserThatAnswer_Contact` int(11) DEFAULT NULL,
  `Answer_Contact` longtext,
  `Date_Contact` datetime DEFAULT NULL,
  `Office_Contact` int(11) DEFAULT NULL,
  `AnswerDate_Contact` datetime DEFAULT NULL,
  PRIMARY KEY (`id_Contact`),
  KEY `Group_Contact` (`Group_Contact`),
  CONSTRAINT `contacts_fk` FOREIGN KEY (`Group_Contact`) REFERENCES `groups` (`id_Group`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatti_suggerimenti`
--

DROP TABLE IF EXISTS `contatti_suggerimenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatti_suggerimenti` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(255) DEFAULT NULL,
  `Cognome` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Telefono` varchar(255) DEFAULT NULL,
  `Testo` varchar(255) DEFAULT NULL,
  `DataOra` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatti_suggerimenti`
--

LOCK TABLES `contatti_suggerimenti` WRITE;
/*!40000 ALTER TABLE `contatti_suggerimenti` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatti_suggerimenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `criteri`
--

DROP TABLE IF EXISTS `criteri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `criteri` (
  `id_Criterio` int(11) NOT NULL AUTO_INCREMENT,
  `Nome_Criterio` varchar(255) DEFAULT NULL,
  `Key_Criterio` varchar(255) DEFAULT NULL,
  `Tipo_Criterio` int(11) DEFAULT NULL,
  `ForDoc_Criterio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Criterio`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `criteri`
--

LOCK TABLES `criteri` WRITE;
/*!40000 ALTER TABLE `criteri` DISABLE KEYS */;
INSERT INTO `criteri` VALUES (1,'Visualizzazione','show',0,1),(2,'Creazione','create',0,1),(3,'Modifica','modify',0,1),(4,'Eliminazione','delete',0,1),(5,'Gestione Permessi','grants',0,1),(6,'Creazione Cartella','newfolder',0,0),(7,'Revisore','redactor',0,1),(8,'Pubblicatore','publisher',0,1),(10,'Opzioni Avanzate','advanced',0,1),(11,'Visibilità Cartelle','foldersvisibility',0,0),(12,'Visualizzazione dei contenuti da Frontend','frontend',0,0),(16,'Creazione Cartella Allegati','newfolder_files',4,0),(18,'Creazione Cartella Collegamenti Ipertestuali','newfolder_links',9,0),(19,'Creazione Cartella News','newfolder_news',3,0),(23,'Creazione Cartella Homepage','newfolder_homepage',1,0),(24,'Creazione Cartella Pagine Web','newfolder_webdocs',2,0);
/*!40000 ALTER TABLE `criteri` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_anagrafe`
--

DROP TABLE IF EXISTS `custom_anagrafe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_anagrafe` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(255) DEFAULT NULL,
  `Installata` tinyint(1) DEFAULT NULL,
  `TestoEmailAttivazione` text,
  `DefaultGroup_id` int(11) DEFAULT NULL,
  `RenderType` varchar(255) DEFAULT NULL,
  `TestoEmailRegistrazioneNegata` text,
  `VisibileInFrontend` tinyint(1) DEFAULT NULL,
  `EmailAsUserName` tinyint(1) DEFAULT NULL,
  `ValidaConCaptcha` tinyint(1) DEFAULT NULL,
  `AggiungiInformativaPrivacy` tinyint(1) DEFAULT NULL,
  `TestoInformativaPrivacy` text,
  `LabelInformativaPrivacy` varchar(255) DEFAULT NULL,
  `DomandaAccettazioneInformativaPrivacy` varchar(255) DEFAULT NULL,
  `RichiediConfermaEmail` tinyint(1) DEFAULT NULL,
  `NumeroRigheDaVisualizzareAreaInformativa` int(11) DEFAULT NULL,
  `NumeroCaratteriDaVisualizzareAreaInformativa` int(11) DEFAULT NULL,
  `RegRequestNotifyGroup_id` int(11) DEFAULT NULL,
  `UseSpid` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `DefaultGroup_id` (`DefaultGroup_id`),
  KEY `RegRequestNotifyGroup_id` (`RegRequestNotifyGroup_id`),
  KEY `DefaultGroup_id_2` (`DefaultGroup_id`),
  KEY `RegRequestNotifyGroup_id_2` (`RegRequestNotifyGroup_id`),
  CONSTRAINT `FK2FE9449933693D5D` FOREIGN KEY (`RegRequestNotifyGroup_id`) REFERENCES `groups` (`id_Group`),
  CONSTRAINT `FK2FE944994A841B88` FOREIGN KEY (`DefaultGroup_id`) REFERENCES `groups` (`id_Group`),
  CONSTRAINT `FKB20C49D072587BF9` FOREIGN KEY (`RegRequestNotifyGroup_id`) REFERENCES `groups` (`id_Group`),
  CONSTRAINT `FKB20C49D0BA7350B` FOREIGN KEY (`DefaultGroup_id`) REFERENCES `groups` (`id_Group`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_anagrafe`
--

LOCK TABLES `custom_anagrafe` WRITE;
/*!40000 ALTER TABLE `custom_anagrafe` DISABLE KEYS */;
INSERT INTO `custom_anagrafe` VALUES (1,'Backoffice',1,'<p>Riepilogo dati registrazione:</p>\r\n\r\n<ul>\r\n	<li>username: {USERNAME}</li>\r\n	<li>password: {PASSWORD}</li>\r\n	<li>codice di attivazione: {KEY}</li>\r\n</ul>\r\n\r\n<p>Per attivare il suo account vada su {ACTIVATE_URL}</p>\r\n\r\n<p>Saluti</p>\r\n\r\n<p>{NOME_PORTALE}</p>\r\n\r\n<p>Email inviata dall&#39;indirizzo: {IP}</p>',1,'Normale','<p>Gentile utente, la sua richiesti di registrazione a {NOME_PORTALE} ? stata negata a causa del seguente motivo:<br />\r\n<br />\r\n{MOTIVAZIONE}<br />\r\n&nbsp;</p>\r\n\r\n<p>Saluti</p>\r\n\r\n<p>{NOME_PORTALE}</p>\r\n\r\n<p>Email inviata dall&#39;indirizzo: {IP}</p>',0,0,0,0,'','Informativa e consenso','Accetti le condizioni di utilizzo?',0,0,0,NULL,0),(2,'Frontend',1,'Riepilogo dati registrazione:<br /><ul><li>username: {USERNAME}</li><li>password: {PASSWORD}</li><li>codice di attivazione: {KEY}</li></ul><p>Per attivare il suo account vada su {ACTIVATE_URL}</p><p>Saluti</p><p>{NOME_PORTALE}</p>Email inviata dall\'\'indirizzo: {IP}',1,'FieldSet','Gentile utente, la sua richiesti di registrazione a {NOME_PORTALE} ? stata negata a causa del seguente motivo:<br /><br />{MOTIVAZIONE}<br /><br /><p>Saluti</p><p>{NOME_PORTALE}</p>Email inviata dall\'\'indirizzo: {IP}<br />',1,0,0,0,NULL,NULL,NULL,0,0,0,NULL,0);
/*!40000 ALTER TABLE `custom_anagrafe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_anagrafe_applicazioni`
--

DROP TABLE IF EXISTS `custom_anagrafe_applicazioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_anagrafe_applicazioni` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IdApplicazione` int(11) NOT NULL,
  `IDCustomAnagrafe` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDCustomAnagrafe` (`IDCustomAnagrafe`),
  KEY `IDCustomAnagrafe_2` (`IDCustomAnagrafe`),
  CONSTRAINT `FK67BF369BBDA449EE` FOREIGN KEY (`IDCustomAnagrafe`) REFERENCES `custom_anagrafe` (`ID`),
  CONSTRAINT `FKACA28D02311DD2AD` FOREIGN KEY (`IDCustomAnagrafe`) REFERENCES `custom_anagrafe` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_anagrafe_applicazioni`
--

LOCK TABLES `custom_anagrafe_applicazioni` WRITE;
/*!40000 ALTER TABLE `custom_anagrafe_applicazioni` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_anagrafe_applicazioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field`
--

DROP TABLE IF EXISTS `custom_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_field` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Required` tinyint(1) DEFAULT NULL,
  `IDCustomAnagrafe` int(11) DEFAULT NULL,
  `ShowInDetails` tinyint(1) DEFAULT NULL,
  `ShowInSearch` tinyint(1) DEFAULT NULL,
  `Cancellato` tinyint(1) DEFAULT '0',
  `IDFrontCustomAnagrafe` int(11) DEFAULT NULL,
  `IsUsername` tinyint(1) DEFAULT NULL,
  `InfoDescription` varchar(255) DEFAULT NULL,
  `IDCampoCustomNuovo` int(11) DEFAULT NULL,
  `PositionInGroup` int(11) DEFAULT '0',
  `IDCustomGroup` int(11) DEFAULT NULL,
  `CriterioRicerca` varchar(255) DEFAULT NULL,
  `ColumnCssClass` varchar(255) DEFAULT NULL,
  `RowOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IDCampoCustomNuovo` (`IDCampoCustomNuovo`),
  KEY `IDCustomAnagrafe` (`IDCustomAnagrafe`),
  KEY `IDFrontCustomAnagrafe` (`IDFrontCustomAnagrafe`),
  KEY `IDCustomGroup` (`IDCustomGroup`),
  KEY `IDCampoCustomNuovo_2` (`IDCampoCustomNuovo`),
  KEY `IDCustomAnagrafe_2` (`IDCustomAnagrafe`),
  KEY `IDCustomGroup_2` (`IDCustomGroup`),
  KEY `IDFrontCustomAnagrafe_2` (`IDFrontCustomAnagrafe`),
  CONSTRAINT `FK26A24126BDA449EE` FOREIGN KEY (`IDCustomAnagrafe`) REFERENCES `custom_anagrafe` (`ID`),
  CONSTRAINT `FK26A24126C64989C7` FOREIGN KEY (`IDCampoCustomNuovo`) REFERENCES `custom_field` (`ID`),
  CONSTRAINT `FK26A24126F3BDF37` FOREIGN KEY (`IDFrontCustomAnagrafe`) REFERENCES `custom_anagrafe` (`ID`),
  CONSTRAINT `FK26A24126F98FC3FE` FOREIGN KEY (`IDCustomGroup`) REFERENCES `custom_group` (`ID`),
  CONSTRAINT `FKC0E77D75311DD2AD` FOREIGN KEY (`IDCustomAnagrafe`) REFERENCES `custom_anagrafe` (`ID`),
  CONSTRAINT `FKC0E77D753AA00A15` FOREIGN KEY (`IDCampoCustomNuovo`) REFERENCES `custom_field` (`ID`),
  CONSTRAINT `FKC0E77D75CA922318` FOREIGN KEY (`IDCustomGroup`) REFERENCES `custom_group` (`ID`),
  CONSTRAINT `FKC0E77D75E1517CA9` FOREIGN KEY (`IDFrontCustomAnagrafe`) REFERENCES `custom_anagrafe` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field`
--

LOCK TABLES `custom_field` WRITE;
/*!40000 ALTER TABLE `custom_field` DISABLE KEYS */;
INSERT INTO `custom_field` VALUES (1,'Nome',1,1,1,1,0,1,0,'',NULL,0,NULL,'Equals',NULL,0),(2,'Cognome',1,1,1,1,0,1,0,'',NULL,0,NULL,'Equals',NULL,0),(3,'Nome',1,2,1,1,0,2,0,'',NULL,1,1,'Equals',NULL,0),(4,'Cognome',1,2,1,1,0,2,1,'',NULL,2,1,'Equals',NULL,0),(5,'Comune di Residenza',0,2,1,0,0,NULL,0,'',NULL,4,1,'Equals',NULL,0),(6,'Indirizzo',0,2,1,0,0,NULL,0,'',NULL,3,1,'Equals',NULL,0),(7,'CAP',0,2,1,0,0,NULL,0,'',NULL,5,1,'Equals',NULL,0),(8,'Telefono',0,2,1,1,0,NULL,0,'',NULL,7,1,'Equals',NULL,0),(9,'Cellulare',0,2,1,0,0,NULL,0,'',NULL,8,1,'Equals',NULL,0),(10,'Codice Fiscale',0,2,1,0,0,NULL,0,'',NULL,6,1,'Equals',NULL,0),(11,'Azienda',0,2,1,0,0,NULL,0,'',NULL,1,2,'Equals',NULL,0),(12,'Data di Nascita',0,2,1,0,0,NULL,0,'',NULL,9,1,'Equals',NULL,0),(13,'Comune di Nascita',0,2,1,0,0,NULL,0,'',NULL,10,1,'Equals',NULL,0),(14,'Fax',0,2,1,0,0,NULL,0,'',NULL,4,2,'Equals',NULL,0),(15,'Settore Azienda',0,2,1,0,0,NULL,0,'',NULL,2,2,'Equals',NULL,0),(16,'Mansione Azienda',0,2,1,0,0,NULL,0,'',NULL,3,2,'Equals',NULL,0),(17,'Nome utente',1,1,1,1,0,NULL,1,'',NULL,0,NULL,'Equals',NULL,0);
/*!40000 ALTER TABLE `custom_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field_checkbox`
--

DROP TABLE IF EXISTS `custom_field_checkbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_field_checkbox` (
  `IDcustom_field_checkbox` int(11) NOT NULL,
  `Value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDcustom_field_checkbox`),
  KEY `IDcustom_field_checkbox` (`IDcustom_field_checkbox`),
  KEY `IDcustom_field_checkbox_2` (`IDcustom_field_checkbox`),
  CONSTRAINT `FK28D922A323E852F` FOREIGN KEY (`IDcustom_field_checkbox`) REFERENCES `custom_field` (`ID`),
  CONSTRAINT `FKC9D27A5A11F3DF03` FOREIGN KEY (`IDcustom_field_checkbox`) REFERENCES `custom_field` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field_checkbox`
--

LOCK TABLES `custom_field_checkbox` WRITE;
/*!40000 ALTER TABLE `custom_field_checkbox` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_field_checkbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field_checkboxlist`
--

DROP TABLE IF EXISTS `custom_field_checkboxlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_field_checkboxlist` (
  `IDcustom_field_checkboxlist` int(11) NOT NULL,
  `AtLeastOneRequired` tinyint(1) DEFAULT NULL,
  `RepeatHorizontal` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`IDcustom_field_checkboxlist`),
  KEY `IDcustom_field_checkboxlist` (`IDcustom_field_checkboxlist`),
  KEY `IDcustom_field_checkboxlist_2` (`IDcustom_field_checkboxlist`),
  CONSTRAINT `FK3E4CBC521DAD1B4D` FOREIGN KEY (`IDcustom_field_checkboxlist`) REFERENCES `custom_field` (`ID`),
  CONSTRAINT `FK3F762D0467E8EC9C` FOREIGN KEY (`IDcustom_field_checkboxlist`) REFERENCES `custom_field` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field_checkboxlist`
--

LOCK TABLES `custom_field_checkboxlist` WRITE;
/*!40000 ALTER TABLE `custom_field_checkboxlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_field_checkboxlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field_checkboxlist_values`
--

DROP TABLE IF EXISTS `custom_field_checkboxlist_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_field_checkboxlist_values` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Value` varchar(255) DEFAULT NULL,
  `Label` varchar(255) DEFAULT NULL,
  `IDCheckboxListField` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDCheckboxListField` (`IDCheckboxListField`),
  KEY `IDCheckboxListField_2` (`IDCheckboxListField`),
  CONSTRAINT `FK3B72E9614BCB45F0` FOREIGN KEY (`IDCheckboxListField`) REFERENCES `custom_field_checkboxlist` (`IDcustom_field_checkboxlist`),
  CONSTRAINT `FK3B72E9615E17B39F` FOREIGN KEY (`IDCheckboxListField`) REFERENCES `custom_field` (`ID`),
  CONSTRAINT `FKE6DCF4039225FCD` FOREIGN KEY (`IDCheckboxListField`) REFERENCES `custom_field_checkboxlist` (`IDcustom_field_checkboxlist`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field_checkboxlist_values`
--

LOCK TABLES `custom_field_checkboxlist_values` WRITE;
/*!40000 ALTER TABLE `custom_field_checkboxlist_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_field_checkboxlist_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field_comune`
--

DROP TABLE IF EXISTS `custom_field_comune`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_field_comune` (
  `IDcustom_field_comune` int(11) NOT NULL,
  PRIMARY KEY (`IDcustom_field_comune`),
  KEY `IDcustom_field_comune` (`IDcustom_field_comune`),
  KEY `IDcustom_field_comune_2` (`IDcustom_field_comune`),
  CONSTRAINT `FK52C826C6ECE0027B` FOREIGN KEY (`IDcustom_field_comune`) REFERENCES `custom_field` (`ID`),
  CONSTRAINT `FKEC590A935733674` FOREIGN KEY (`IDcustom_field_comune`) REFERENCES `custom_field` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field_comune`
--

LOCK TABLES `custom_field_comune` WRITE;
/*!40000 ALTER TABLE `custom_field_comune` DISABLE KEYS */;
INSERT INTO `custom_field_comune` VALUES (5),(13);
/*!40000 ALTER TABLE `custom_field_comune` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field_date`
--

DROP TABLE IF EXISTS `custom_field_date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_field_date` (
  `IDcustom_field_date` int(11) NOT NULL,
  `MaxDate` datetime DEFAULT NULL,
  `MinDate` datetime DEFAULT NULL,
  `Formato` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDcustom_field_date`),
  KEY `IDcustom_field_date` (`IDcustom_field_date`),
  KEY `IDcustom_field_date_2` (`IDcustom_field_date`),
  CONSTRAINT `FK918F635E1F54EF6A` FOREIGN KEY (`IDcustom_field_date`) REFERENCES `custom_field` (`ID`),
  CONSTRAINT `FK9FEA40716D324B34` FOREIGN KEY (`IDcustom_field_date`) REFERENCES `custom_field` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field_date`
--

LOCK TABLES `custom_field_date` WRITE;
/*!40000 ALTER TABLE `custom_field_date` DISABLE KEYS */;
INSERT INTO `custom_field_date` VALUES (12,'0001-01-01 00:00:00','0001-01-01 00:00:00','SoloData');
/*!40000 ALTER TABLE `custom_field_date` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field_dependency_item`
--

DROP TABLE IF EXISTS `custom_field_dependency_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_field_dependency_item` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DependencyDDValue` varchar(255) DEFAULT NULL,
  `isRequiredForValue` tinyint(1) DEFAULT NULL,
  `IDCustomField` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDCustomField` (`IDCustomField`),
  KEY `IDCustomField_2` (`IDCustomField`),
  CONSTRAINT `FK5DACFF84FB2C6BBC` FOREIGN KEY (`IDCustomField`) REFERENCES `custom_field` (`ID`),
  CONSTRAINT `FK9C69C674584DEE70` FOREIGN KEY (`IDCustomField`) REFERENCES `custom_field` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field_dependency_item`
--

LOCK TABLES `custom_field_dependency_item` WRITE;
/*!40000 ALTER TABLE `custom_field_dependency_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_field_dependency_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field_dropdown`
--

DROP TABLE IF EXISTS `custom_field_dropdown`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_field_dropdown` (
  `IDcustom_field_dropdown` int(11) NOT NULL,
  PRIMARY KEY (`IDcustom_field_dropdown`),
  KEY `IDcustom_field_dropdown` (`IDcustom_field_dropdown`),
  KEY `IDcustom_field_dropdown_2` (`IDcustom_field_dropdown`),
  CONSTRAINT `FK7BB4C638CC75C969` FOREIGN KEY (`IDcustom_field_dropdown`) REFERENCES `custom_field` (`ID`),
  CONSTRAINT `FKF43ACB4A4178F0F` FOREIGN KEY (`IDcustom_field_dropdown`) REFERENCES `custom_field` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field_dropdown`
--

LOCK TABLES `custom_field_dropdown` WRITE;
/*!40000 ALTER TABLE `custom_field_dropdown` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_field_dropdown` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field_dropdown_values`
--

DROP TABLE IF EXISTS `custom_field_dropdown_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_field_dropdown_values` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Value` varchar(255) DEFAULT NULL,
  `IDDropdownField` int(11) DEFAULT NULL,
  `Label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDDropdownField` (`IDDropdownField`),
  KEY `IDDropdownField_2` (`IDDropdownField`),
  CONSTRAINT `FKA1A26F222220FD1` FOREIGN KEY (`IDDropdownField`) REFERENCES `custom_field_dropdown` (`IDcustom_field_dropdown`),
  CONSTRAINT `FKA1A26F2762AB649` FOREIGN KEY (`IDDropdownField`) REFERENCES `custom_field` (`ID`),
  CONSTRAINT `FKAD701409F80941CD` FOREIGN KEY (`IDDropdownField`) REFERENCES `custom_field_dropdown` (`IDcustom_field_dropdown`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field_dropdown_values`
--

LOCK TABLES `custom_field_dropdown_values` WRITE;
/*!40000 ALTER TABLE `custom_field_dropdown_values` DISABLE KEYS */;
INSERT INTO `custom_field_dropdown_values` VALUES (26,'1',NULL,'Prova 1');
/*!40000 ALTER TABLE `custom_field_dropdown_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field_file`
--

DROP TABLE IF EXISTS `custom_field_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_field_file` (
  `IDcustom_field_file` int(11) NOT NULL,
  `FileType` varchar(255) DEFAULT NULL,
  `FileTypeDesc` varchar(255) DEFAULT NULL,
  `UploadButtonText` varchar(255) DEFAULT NULL,
  `UploadFileHandler` varchar(255) DEFAULT NULL,
  `UploadFolder` varchar(255) DEFAULT NULL,
  `MultiUpload` tinyint(1) DEFAULT NULL,
  `AutoUpload` tinyint(1) DEFAULT NULL,
  `SwfObjectFilePath` varchar(255) DEFAULT NULL,
  `UploadifyScriptsFolderPath` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDcustom_field_file`),
  KEY `IDcustom_field_file` (`IDcustom_field_file`),
  KEY `IDcustom_field_file_2` (`IDcustom_field_file`),
  CONSTRAINT `FK7B0EF067EE94F63A` FOREIGN KEY (`IDcustom_field_file`) REFERENCES `custom_field` (`ID`),
  CONSTRAINT `FK951108366FA6EF62` FOREIGN KEY (`IDcustom_field_file`) REFERENCES `custom_field` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field_file`
--

LOCK TABLES `custom_field_file` WRITE;
/*!40000 ALTER TABLE `custom_field_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_field_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field_foreignkeymapped`
--

DROP TABLE IF EXISTS `custom_field_foreignkeymapped`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_field_foreignkeymapped` (
  `IDcustom_field_foreignKeyMapped` int(11) NOT NULL,
  `PropertyName` varchar(255) DEFAULT NULL,
  `MappedEntityName` varchar(255) DEFAULT NULL,
  `CustomId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDcustom_field_foreignKeyMapped`),
  KEY `IDcustom_field_foreignKeyMapped` (`IDcustom_field_foreignKeyMapped`),
  KEY `IDcustom_field_foreignKeyMappe_2` (`IDcustom_field_foreignKeyMapped`),
  CONSTRAINT `FK3570FFC5E8F57FE5` FOREIGN KEY (`IDcustom_field_foreignKeyMapped`) REFERENCES `custom_field` (`ID`),
  CONSTRAINT `FK7047281552D7D310` FOREIGN KEY (`IDcustom_field_foreignKeyMapped`) REFERENCES `custom_field` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field_foreignkeymapped`
--

LOCK TABLES `custom_field_foreignkeymapped` WRITE;
/*!40000 ALTER TABLE `custom_field_foreignkeymapped` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_field_foreignkeymapped` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field_manytomanymapped`
--

DROP TABLE IF EXISTS `custom_field_manytomanymapped`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_field_manytomanymapped` (
  `IDcustom_field_manyToManyMapped` int(11) NOT NULL,
  `ControlloAssociato` varchar(255) DEFAULT NULL,
  `NomeCampoIdentificativo` varchar(255) DEFAULT NULL,
  `NomeCampoEtichetta` varchar(255) DEFAULT NULL,
  `MappedEntityName` varchar(255) DEFAULT NULL,
  `MappedTableName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDcustom_field_manyToManyMapped`),
  KEY `IDcustom_field_manyToManyMapped` (`IDcustom_field_manyToManyMapped`),
  KEY `IDcustom_field_manyToManyMappe_2` (`IDcustom_field_manyToManyMapped`),
  CONSTRAINT `FKA427C4CFC754EC6A` FOREIGN KEY (`IDcustom_field_manyToManyMapped`) REFERENCES `custom_field` (`ID`),
  CONSTRAINT `FKA801856FE6FC4D3A` FOREIGN KEY (`IDcustom_field_manyToManyMapped`) REFERENCES `custom_field` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field_manytomanymapped`
--

LOCK TABLES `custom_field_manytomanymapped` WRITE;
/*!40000 ALTER TABLE `custom_field_manytomanymapped` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_field_manytomanymapped` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field_provincia`
--

DROP TABLE IF EXISTS `custom_field_provincia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_field_provincia` (
  `IDcustom_field_provincia` int(11) NOT NULL,
  PRIMARY KEY (`IDcustom_field_provincia`),
  KEY `IDcustom_field_provincia` (`IDcustom_field_provincia`),
  KEY `IDcustom_field_provincia_2` (`IDcustom_field_provincia`),
  CONSTRAINT `FK1D16995E2EFCAB8B` FOREIGN KEY (`IDcustom_field_provincia`) REFERENCES `custom_field` (`ID`),
  CONSTRAINT `FK91E150627B057637` FOREIGN KEY (`IDcustom_field_provincia`) REFERENCES `custom_field` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field_provincia`
--

LOCK TABLES `custom_field_provincia` WRITE;
/*!40000 ALTER TABLE `custom_field_provincia` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_field_provincia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field_text`
--

DROP TABLE IF EXISTS `custom_field_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_field_text` (
  `IDcustom_field_text` int(11) NOT NULL,
  `MaxLength` int(11) DEFAULT NULL,
  `InputType` varchar(255) DEFAULT NULL,
  `MinLength` int(11) DEFAULT NULL,
  `Columns` int(11) DEFAULT '0',
  `isMultiline` tinyint(1) DEFAULT '0',
  `Rows` int(11) DEFAULT '0',
  PRIMARY KEY (`IDcustom_field_text`),
  KEY `IDcustom_field_text` (`IDcustom_field_text`),
  KEY `IDcustom_field_text_2` (`IDcustom_field_text`),
  CONSTRAINT `FK1B51CAD8F3066D45` FOREIGN KEY (`IDcustom_field_text`) REFERENCES `custom_field` (`ID`),
  CONSTRAINT `FKEC8935BB66D8B199` FOREIGN KEY (`IDcustom_field_text`) REFERENCES `custom_field` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field_text`
--

LOCK TABLES `custom_field_text` WRITE;
/*!40000 ALTER TABLE `custom_field_text` DISABLE KEYS */;
INSERT INTO `custom_field_text` VALUES (1,255,'Text',0,0,0,0),(2,255,'Text',0,0,0,0),(3,255,'Text',0,0,0,0),(4,255,'Text',0,0,0,0),(6,255,'Text',0,0,0,0),(7,5,'Cap',0,0,0,0),(8,255,'Phone',0,0,0,0),(9,255,'Phone',0,0,0,0),(10,16,'CodiceFiscale',0,0,0,0),(11,255,'Text',0,0,0,0),(14,255,'Phone',0,0,0,0),(15,255,'Text',0,0,0,0),(16,255,'Text',0,0,0,0),(17,255,'Text',8,0,0,0);
/*!40000 ALTER TABLE `custom_field_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_group`
--

DROP TABLE IF EXISTS `custom_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(255) DEFAULT NULL,
  `Descrizione` varchar(255) DEFAULT NULL,
  `Posizione` int(11) DEFAULT NULL,
  `IDCustomAnagrafe` int(11) DEFAULT NULL,
  `DependsOnAnotherField` tinyint(1) DEFAULT '0',
  `DependencyCustomFieldName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDCustomAnagrafe` (`IDCustomAnagrafe`),
  KEY `IDCustomAnagrafe_2` (`IDCustomAnagrafe`),
  CONSTRAINT `FK484E13F7BDA449EE` FOREIGN KEY (`IDCustomAnagrafe`) REFERENCES `custom_anagrafe` (`ID`),
  CONSTRAINT `FK893A4D3F311DD2AD` FOREIGN KEY (`IDCustomAnagrafe`) REFERENCES `custom_anagrafe` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_group`
--

LOCK TABLES `custom_group` WRITE;
/*!40000 ALTER TABLE `custom_group` DISABLE KEYS */;
INSERT INTO `custom_group` VALUES (1,'Dati Anagrafici','Inserire qui i dati relativi alla propria anagrafica',1,2,0,NULL),(2,'Dati Aziendali','Inserire qui i dati relativi alla propria azienda',2,2,0,NULL);
/*!40000 ALTER TABLE `custom_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doc_profiles`
--

DROP TABLE IF EXISTS `doc_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_profiles` (
  `id_DocProfile` int(11) NOT NULL,
  `Document_ObjectProfile` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_DocProfile`),
  KEY `id_DocProfile` (`id_DocProfile`),
  KEY `Document_ObjectProfile` (`Document_ObjectProfile`),
  KEY `id_DocProfile_2` (`id_DocProfile`),
  CONSTRAINT `FKB77DB907F7638389` FOREIGN KEY (`id_DocProfile`) REFERENCES `object_profiles` (`id_ObjectProfile`),
  CONSTRAINT `FKBC7C433DBE70693B` FOREIGN KEY (`id_DocProfile`) REFERENCES `object_profiles` (`id_ObjectProfile`),
  CONSTRAINT `object_profiles_document` FOREIGN KEY (`Document_ObjectProfile`) REFERENCES `network_docs` (`id_NetworkDoc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_profiles`
--

LOCK TABLES `doc_profiles` WRITE;
/*!40000 ALTER TABLE `doc_profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doc_profiles_criteria`
--

DROP TABLE IF EXISTS `doc_profiles_criteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_profiles_criteria` (
  `id_DocProfileCriteria` int(11) NOT NULL,
  `DocProfile_ObjectProfileCriteria` int(11) DEFAULT NULL,
  `Criteria_ObjectProfileCriteria` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_DocProfileCriteria`),
  KEY `id_DocProfileCriteria` (`id_DocProfileCriteria`),
  KEY `DocProfile_ObjectProfileCriteria` (`DocProfile_ObjectProfileCriteria`),
  KEY `Criteria_ObjectProfileCriteria` (`Criteria_ObjectProfileCriteria`),
  KEY `id_DocProfileCriteria_2` (`id_DocProfileCriteria`),
  CONSTRAINT `FK6C464F564BD2C01E` FOREIGN KEY (`id_DocProfileCriteria`) REFERENCES `object_profiles_criteria` (`id_ObjectProfileCriteria`),
  CONSTRAINT `FKB5FE2D134EF88EEF` FOREIGN KEY (`id_DocProfileCriteria`) REFERENCES `object_profiles_criteria` (`id_ObjectProfileCriteria`),
  CONSTRAINT `doc_profiles_criteria_criterio` FOREIGN KEY (`Criteria_ObjectProfileCriteria`) REFERENCES `criteri` (`id_Criterio`),
  CONSTRAINT `object_profiles_criteria_docprofile` FOREIGN KEY (`DocProfile_ObjectProfileCriteria`) REFERENCES `doc_profiles` (`id_DocProfile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_profiles_criteria`
--

LOCK TABLES `doc_profiles_criteria` WRITE;
/*!40000 ALTER TABLE `doc_profiles_criteria` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_profiles_criteria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docs`
--

DROP TABLE IF EXISTS `docs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docs` (
  `id_Doc` int(11) NOT NULL AUTO_INCREMENT,
  `Name_Doc` varchar(255) DEFAULT NULL,
  `Stato_Doc` int(11) DEFAULT NULL,
  `Data_Doc` datetime DEFAULT NULL,
  `Modify_Doc` datetime DEFAULT NULL,
  `Autore_Doc` int(11) DEFAULT NULL,
  `Hide_Doc` int(11) DEFAULT NULL,
  `CssClass_Doc` varchar(255) DEFAULT NULL,
  `Application_Doc` int(11) DEFAULT NULL,
  `TitleOffset_Doc` int(11) DEFAULT NULL,
  `Trash_Doc` int(11) DEFAULT NULL,
  `Create_Doc` datetime DEFAULT NULL,
  `LangPickMode_Doc` int(11) DEFAULT NULL,
  `Order_Doc` int(11) DEFAULT NULL,
  `Layout_Doc` int(11) DEFAULT NULL,
  `DocRiferito_Doc` int(11) DEFAULT NULL,
  `ShowInTopMenu_Doc` int(11) DEFAULT '1',
  PRIMARY KEY (`id_Doc`),
  KEY `id_Doc` (`id_Doc`),
  KEY `Application_Doc` (`Application_Doc`),
  KEY `DocRiferito_Doc` (`DocRiferito_Doc`),
  KEY `id_Doc_2` (`id_Doc`),
  CONSTRAINT `FKA0C5D81730982D7B` FOREIGN KEY (`id_Doc`) REFERENCES `network_docs` (`id_NetworkDoc`),
  CONSTRAINT `FKF9C11B3F86DA4D56` FOREIGN KEY (`id_Doc`) REFERENCES `network_docs` (`id_NetworkDoc`),
  CONSTRAINT `docs_docriferito` FOREIGN KEY (`DocRiferito_Doc`) REFERENCES `docs` (`id_Doc`),
  CONSTRAINT `docs_fk` FOREIGN KEY (`Application_Doc`) REFERENCES `applicazioni` (`id_Applicazione`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docs`
--

LOCK TABLES `docs` WRITE;
/*!40000 ALTER TABLE `docs` DISABLE KEYS */;
INSERT INTO `docs` VALUES (1,'default.aspx',1,'2011-09-12 00:00:00','2011-09-12 00:00:00',1,0,NULL,1,0,0,'2011-09-12 00:00:00',0,0,0,NULL,1);
/*!40000 ALTER TABLE `docs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docs_annotates`
--

DROP TABLE IF EXISTS `docs_annotates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docs_annotates` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `offset` int(11) DEFAULT NULL,
  `score` double DEFAULT NULL,
  `surfaceForm` varchar(255) DEFAULT NULL,
  `types` varchar(255) DEFAULT NULL,
  `URI` varchar(255) DEFAULT NULL,
  `RifDocContentAnnotate` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RifDocContentAnnotate` (`RifDocContentAnnotate`),
  CONSTRAINT `DocContentAnnotate` FOREIGN KEY (`RifDocContentAnnotate`) REFERENCES `docs_contents_annotates` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docs_annotates`
--

LOCK TABLES `docs_annotates` WRITE;
/*!40000 ALTER TABLE `docs_annotates` DISABLE KEYS */;
/*!40000 ALTER TABLE `docs_annotates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docs_contents`
--

DROP TABLE IF EXISTS `docs_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docs_contents` (
  `id_DocContent` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_DocContent`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docs_contents`
--

LOCK TABLES `docs_contents` WRITE;
/*!40000 ALTER TABLE `docs_contents` DISABLE KEYS */;
INSERT INTO `docs_contents` VALUES (1);
/*!40000 ALTER TABLE `docs_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docs_contents_annotates`
--

DROP TABLE IF EXISTS `docs_contents_annotates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docs_contents_annotates` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RifContent` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docs_contents_annotates`
--

LOCK TABLES `docs_contents_annotates` WRITE;
/*!40000 ALTER TABLE `docs_contents_annotates` DISABLE KEYS */;
/*!40000 ALTER TABLE `docs_contents_annotates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docs_contents_attaches`
--

DROP TABLE IF EXISTS `docs_contents_attaches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docs_contents_attaches` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `DocContent_id` int(11) DEFAULT NULL,
  `Document_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `DocContent_id` (`DocContent_id`),
  KEY `Document_id` (`Document_id`),
  CONSTRAINT `FK9BE7C84C3468140B` FOREIGN KEY (`Document_id`) REFERENCES `docs` (`id_Doc`),
  CONSTRAINT `FK9BE7C84CD0A84A5B` FOREIGN KEY (`DocContent_id`) REFERENCES `docs_contents` (`id_DocContent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docs_contents_attaches`
--

LOCK TABLES `docs_contents_attaches` WRITE;
/*!40000 ALTER TABLE `docs_contents_attaches` DISABLE KEYS */;
/*!40000 ALTER TABLE `docs_contents_attaches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docs_histories_for_import`
--

DROP TABLE IF EXISTS `docs_histories_for_import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docs_histories_for_import` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NewID` int(11) DEFAULT NULL,
  `OldID` int(11) DEFAULT NULL,
  `Type` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docs_histories_for_import`
--

LOCK TABLES `docs_histories_for_import` WRITE;
/*!40000 ALTER TABLE `docs_histories_for_import` DISABLE KEYS */;
/*!40000 ALTER TABLE `docs_histories_for_import` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docs_lang`
--

DROP TABLE IF EXISTS `docs_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docs_lang` (
  `id_DocLang` int(11) NOT NULL AUTO_INCREMENT,
  `Lang_DocLang` int(11) DEFAULT NULL,
  `Label_DocLang` varchar(255) DEFAULT NULL,
  `Doc_DocLang` int(11) DEFAULT NULL,
  `Content_DocLang` int(11) DEFAULT NULL,
  `Desc_DocLang` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id_DocLang`),
  KEY `Lang_DocLang` (`Lang_DocLang`),
  KEY `Doc_DocLang` (`Doc_DocLang`),
  KEY `Content_DocLang` (`Content_DocLang`),
  CONSTRAINT `docs_lang_docs` FOREIGN KEY (`Doc_DocLang`) REFERENCES `docs` (`id_Doc`),
  CONSTRAINT `docs_lang_docs_contents` FOREIGN KEY (`Content_DocLang`) REFERENCES `docs_contents` (`id_DocContent`),
  CONSTRAINT `docs_lang_langs` FOREIGN KEY (`Lang_DocLang`) REFERENCES `lang` (`id_Lang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 6144 kB; (`Doc_DocLang`) REFER `smnet_lang/docs';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docs_lang`
--

LOCK TABLES `docs_lang` WRITE;
/*!40000 ALTER TABLE `docs_lang` DISABLE KEYS */;
INSERT INTO `docs_lang` VALUES (1,1,'Homepage',1,1,NULL);
/*!40000 ALTER TABLE `docs_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docs_meta`
--

DROP TABLE IF EXISTS `docs_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docs_meta` (
  `id_DocMeta` int(11) NOT NULL AUTO_INCREMENT,
  `Meta_DocMeta` int(11) DEFAULT NULL,
  `Content_DocMeta` longtext,
  `Stato_DocMeta` int(11) DEFAULT NULL,
  `Doc_DocMeta` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_DocMeta`),
  KEY `Meta_DocMeta` (`Meta_DocMeta`),
  KEY `Doc_DocMeta` (`Doc_DocMeta`),
  CONSTRAINT `docs_meta_docs` FOREIGN KEY (`Doc_DocMeta`) REFERENCES `docs` (`id_Doc`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `docs_meta_meta` FOREIGN KEY (`Meta_DocMeta`) REFERENCES `meta` (`id_Meta`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docs_meta`
--

LOCK TABLES `docs_meta` WRITE;
/*!40000 ALTER TABLE `docs_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `docs_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docs_tags`
--

DROP TABLE IF EXISTS `docs_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docs_tags` (
  `id_DocTag` int(11) NOT NULL AUTO_INCREMENT,
  `Tag_DocTag` int(11) DEFAULT NULL,
  `Doc_DocTag` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_DocTag`),
  KEY `Doc_DocTag` (`Doc_DocTag`),
  KEY `Tag_DocTag` (`Tag_DocTag`),
  CONSTRAINT `FKA7BF130191C6DA38` FOREIGN KEY (`Tag_DocTag`) REFERENCES `tags` (`id_Tag`),
  CONSTRAINT `FKA7BF1301C41F7907` FOREIGN KEY (`Doc_DocTag`) REFERENCES `docs` (`id_Doc`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docs_tags`
--

LOCK TABLES `docs_tags` WRITE;
/*!40000 ALTER TABLE `docs_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `docs_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `externalapp_grants`
--

DROP TABLE IF EXISTS `externalapp_grants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `externalapp_grants` (
  `id_ExternalAppGrant` int(11) NOT NULL,
  `ExternalAppProfile_ObjectProfileCriteria` int(11) DEFAULT NULL,
  `Criteria_ObjectProfileCriteria` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ExternalAppGrant`),
  KEY `id_ExternalAppGrant` (`id_ExternalAppGrant`),
  KEY `ExternalAppProfile_ObjectProfileCriteria` (`ExternalAppProfile_ObjectProfileCriteria`),
  KEY `Criteria_ObjectProfileCriteria` (`Criteria_ObjectProfileCriteria`),
  KEY `id_ExternalAppGrant_2` (`id_ExternalAppGrant`),
  CONSTRAINT `FK8325CEF58F5F93D5` FOREIGN KEY (`id_ExternalAppGrant`) REFERENCES `object_profiles_criteria` (`id_ObjectProfileCriteria`),
  CONSTRAINT `FKC5DD13263ECAAD49` FOREIGN KEY (`id_ExternalAppGrant`) REFERENCES `object_profiles_criteria` (`id_ObjectProfileCriteria`),
  CONSTRAINT `externalapp_profiles_criteria_criterio` FOREIGN KEY (`Criteria_ObjectProfileCriteria`) REFERENCES `externalappzcriteria` (`id_ExternalAppCriteria`),
  CONSTRAINT `object_profiles_criteria_externalappprofile` FOREIGN KEY (`ExternalAppProfile_ObjectProfileCriteria`) REFERENCES `externalapp_profiles` (`id_ExternalAppzProfile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `externalapp_grants`
--

LOCK TABLES `externalapp_grants` WRITE;
/*!40000 ALTER TABLE `externalapp_grants` DISABLE KEYS */;
INSERT INTO `externalapp_grants` VALUES (3836,705,20),(3837,705,475),(3844,761,801),(3845,761,802),(3846,762,803),(3847,762,804),(3848,763,805),(3849,763,806),(3850,763,807),(3851,763,808),(3852,763,809),(3853,763,810),(3854,763,811),(3855,763,812),(3856,763,813),(3857,763,814),(3858,763,815),(3859,764,816),(3860,764,817),(3867,765,816),(3868,765,817),(3869,766,805),(3870,766,806),(3871,766,807),(3872,766,808),(3873,766,809),(3874,766,810),(3875,766,811),(3876,766,812),(3877,766,813),(3878,766,814),(3879,766,815),(20442,702,4),(20443,702,470),(20491,767,1107),(20492,767,1108),(20495,769,1111),(20496,769,1112),(20497,769,1113),(20498,770,1114),(20499,770,1115),(20500,770,1116);
/*!40000 ALTER TABLE `externalapp_grants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `externalapp_profiles`
--

DROP TABLE IF EXISTS `externalapp_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `externalapp_profiles` (
  `id_ExternalAppzProfile` int(11) NOT NULL,
  `Network_ExternalAppzProfile` int(11) DEFAULT NULL,
  `Application_ObjectProfile` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ExternalAppzProfile`),
  KEY `id_ExternalAppzProfile` (`id_ExternalAppzProfile`),
  KEY `Application_ObjectProfile` (`Application_ObjectProfile`),
  KEY `id_ExternalAppzProfile_2` (`id_ExternalAppzProfile`),
  CONSTRAINT `FK31EEA89BA9CC912F` FOREIGN KEY (`id_ExternalAppzProfile`) REFERENCES `object_profiles` (`id_ObjectProfile`),
  CONSTRAINT `FK9ED605F5636703D6` FOREIGN KEY (`id_ExternalAppzProfile`) REFERENCES `object_profiles` (`id_ObjectProfile`),
  CONSTRAINT `object_profiles_externalapp` FOREIGN KEY (`Application_ObjectProfile`) REFERENCES `externalappz` (`id_ExternalApp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `externalapp_profiles`
--

LOCK TABLES `externalapp_profiles` WRITE;
/*!40000 ALTER TABLE `externalapp_profiles` DISABLE KEYS */;
INSERT INTO `externalapp_profiles` VALUES (680,0,4),(681,0,4),(682,0,4),(683,0,4),(684,0,4),(702,0,4),(705,0,20),(761,0,235),(762,0,3001),(763,0,5),(764,0,6),(765,0,6),(766,0,5),(767,0,2216),(769,0,2002),(770,0,240);
/*!40000 ALTER TABLE `externalapp_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `externalappz`
--

DROP TABLE IF EXISTS `externalappz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `externalappz` (
  `id_ExternalApp` int(11) NOT NULL AUTO_INCREMENT,
  `Nome_ExternalApp` varchar(255) DEFAULT NULL,
  `Key_ExternalApp` varchar(255) DEFAULT NULL,
  `HomepageGroup_ExternalApp` int(11) DEFAULT '0',
  `Description_ExternalApp` varchar(512) DEFAULT NULL,
  `NetworkDependant_ExternalApp` int(11) DEFAULT '0',
  `Enabled_ExternalApp` int(11) DEFAULT NULL,
  `AccessoMode_ExternalApp` int(11) DEFAULT '0',
  `Path_ExternalApp` varchar(1024) DEFAULT NULL,
  `FrontLayout_ExternalApp` int(11) DEFAULT '0',
  PRIMARY KEY (`id_ExternalApp`)
) ENGINE=InnoDB AUTO_INCREMENT=3002 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `externalappz`
--

LOCK TABLES `externalappz` WRITE;
/*!40000 ALTER TABLE `externalappz` DISABLE KEYS */;
INSERT INTO `externalappz` VALUES (4,'Permessi Applicazioni','grants',1,'Gestione dei permessi delle applicazioni',0,1,0,NULL,0),(5,'Utenti','users',1,'Gestione degli utenti',0,1,0,NULL,0),(6,'Gruppi','groups',1,'Gestione dei gruppi di utenti',0,1,0,NULL,0),(20,'Installer','installer',3,'Gestisce l\'istallazione delle applicazioni.',0,1,0,NULL,0),(235,'Gestione delle Reti','networksmanager',3,'Gestione delle Reti',0,1,0,NULL,0),(240,'Gestione Chiarimenti','clarifications',2,'Gestione Chiarimenti',1,1,2,'/chiarimenti',0),(2002,'Messaggistica','messaggistica',2,'Messaggistica',0,1,0,NULL,0),(2216,'Task Scheduler','genericjobsapp',2,'Area di gestione dei task del cms',0,1,0,NULL,0),(3001,'Generazione Anagrafiche Custom','customanagrafe',3,'Custom Anagrafe',0,1,0,NULL,0);
/*!40000 ALTER TABLE `externalappz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `externalappz_pages`
--

DROP TABLE IF EXISTS `externalappz_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `externalappz_pages` (
  `id_ExternalAppPage` int(11) NOT NULL AUTO_INCREMENT,
  `PageName_ExternalAppPage` varchar(1024) DEFAULT NULL,
  `ClassName_ExternalAppPage` text,
  `ExternalApp_ExternalAppPage` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ExternalAppPage`),
  KEY `ExternalApp_ExternalAppPage` (`ExternalApp_ExternalAppPage`),
  KEY `ExternalApp_ExternalAppPage_2` (`ExternalApp_ExternalAppPage`),
  CONSTRAINT `externalapp_pages` FOREIGN KEY (`ExternalApp_ExternalAppPage`) REFERENCES `externalappz` (`id_ExternalApp`),
  CONSTRAINT `externalappz_pages_fk` FOREIGN KEY (`ExternalApp_ExternalAppPage`) REFERENCES `externalappz` (`id_ExternalApp`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `externalappz_pages`
--

LOCK TABLES `externalappz_pages` WRITE;
/*!40000 ALTER TABLE `externalappz_pages` DISABLE KEYS */;
INSERT INTO `externalappz_pages` VALUES (9931,'default.aspx','NetCms.NetworksManager.NetworksManagerMainPage',235),(9932,'add.aspx','NetCms.NetworksManager.NetworksManagerAddPage',235),(9933,'details.aspx','NetCms.NetworksManager.NetworksManagerDetailPage',235),(9934,'delete.aspx','NetCms.NetworksManager.NetworksManagerDeletePage',235),(10019,'macro_add.aspx','NetCms.Users.GroupsManager.MacroGroupAddPage',6),(10020,'macro.aspx','NetCms.Users.GroupsManager.MacroGroupsPage',6),(10021,'macro_edit.aspx','NetCms.Users.GroupsManager.EditMacroGroupPage',6),(10022,'group_mod.aspx','NetCms.Users.GroupsManager.ModPage',6),(10023,'default.aspx','NetCms.Users.GroupsManager.MainPage',6),(10024,'group_details.aspx','NetCms.Users.GroupsManager.DetailPage',6),(10025,'group_users_list.aspx','NetCms.Users.GroupsManager.UsersPage',6),(10026,'group_new.aspx','NetCms.Users.GroupsManager.NewPage',6),(10027,'group_move.aspx','NetCms.Users.GroupsManager.MovePage',6),(11011,'/default.aspx','NetCms.Users.CustomAnagrafe.View.MainPage',3001),(11012,'customcheckboxlistvalue_gest.aspx','NetCms.Users.CustomAnagrafe.View.CustomCheckboxListValueMod',3001),(11013,'/anagrafica_gest.aspx','NetCms.Users.CustomAnagrafe.View.CustomAnagrafeCCK',3001),(11014,'anagrafica_frontimport.aspx','NetCms.Users.CustomAnagrafe.View.CustomAnagrafeFrontImport',3001),(11015,'groups_export.aspx','NetCms.Users.CustomAnagrafe.View.GroupsExport',3001),(11016,'anagrafica_backimport.aspx','NetCms.Users.CustomAnagrafe.View.CustomAnagrafeBackImport',3001),(11017,'anagrafica_mod.aspx','NetCms.Users.CustomAnagrafe.View.CustomAnagrafeMod',3001),(11018,'/customgroup_mod.aspx','NetCms.Users.CustomAnagrafe.View.CustomGroupMod',3001),(11019,'anagrafica_new.aspx','NetCms.Users.CustomAnagrafe.View.CustomAnagrafeAdd',3001),(11020,'/customgroup_gest.aspx','NetCms.Users.CustomAnagrafe.View.CustomGroupGest',3001),(11021,'anagrafica_dati_export.aspx','NetCms.Users.CustomAnagrafe.View.CustomAnagrafeDatiExport',3001),(11022,'/campocustomdropdown_gest.aspx','NetCms.Users.CustomAnagrafe.View.CustomDropdownFieldMod',3001),(11023,'/customgroup_fields_gest.aspx','NetCms.Users.CustomAnagrafe.View.CustomGroupFieldsGest',3001),(11024,'/campifrontend_gest.aspx','NetCms.Users.CustomAnagrafe.View.CustomAnagrafeFrontFields',3001),(11025,'/applicazioni.aspx','NetCms.Users.CustomAnagrafe.View.CustomAnagrafeApplicationsPage',3001),(11026,'campocustom_mod.aspx','NetCms.Users.CustomAnagrafe.View.CustomFieldMod',3001),(11027,'/campocustomcheckboxlist_gest.aspx','NetCms.Users.CustomAnagrafe.View.CustomCheckboxListFieldMod',3001),(11028,'customdropdownvalue_gest.aspx','NetCms.Users.CustomAnagrafe.View.CustomDropdownValueMod',3001),(11029,'anagrafica_structure_export.aspx','NetCms.Users.CustomAnagrafe.View.CustomAnagrafeStructureExport',3001),(11217,'user_mod.aspx','NetCms.Users.UsersManager.UsersModifyAnagrafePage',5),(11218,'account_pwd_change.aspx','NetCms.Users.UsersManager.UserChangePasswordPage',5),(11219,'user_new.aspx','NetCms.Users.UsersManager.UsersNewPage',5),(11220,'nogroups.aspx','NetCms.Users.UsersManager.UsersListNoGroupsPage',5),(11221,'new_password.aspx','NetCms.Users.UsersManager.UsersNewPassword',5),(11222,'networks.aspx','NetCms.Users.UsersManager.UsersNetworkPage',5),(11223,'user_groups.aspx','NetCms.Users.UsersManager.UsersGroupsPage',5),(11224,'scadenzamod.aspx','NetCms.Users.UsersManager.UsersModifyScadenzaServizioPage',5),(11225,'servicesmoredata.aspx','NetCms.Users.UsersManager.UsersActivateServicesWithMoreDataPage',5),(11226,'user_details.aspx','NetCms.Users.UsersManager.UsersDetailsPage',5),(11227,'account_details.aspx','NetCms.Users.UsersManager.UserProfileDetailsPage',5),(11228,'services.aspx','NetCms.Users.UsersManager.UsersServicesPage',5),(11229,'default.aspx','NetCms.Users.UsersManager.MainPage',5),(11230,'account_edit.aspx','NetCms.Users.UsersManager.UserProfileEditPage',5),(11231,'reg_requests.aspx','NetCms.Users.UsersManager.UsersRegistrationRequestsPage',5),(11237,'/provider_details.aspx','Messaggistica.View.ProviderDetail',2002),(11238,'/spedizioni_list.aspx','Messaggistica.View.SpedizioniList',2002),(11239,'/provider_mod.aspx','Messaggistica.View.ProviderMod',2002),(11240,'/provider_new.aspx','Messaggistica.View.ProviderAdd',2002),(11241,'/spedizioni_new.aspx','Messaggistica.View.SpedizioniNew',2002),(11242,'/details.aspx','Messaggistica.View.SpedizioniDetail',2002),(11243,'/provider_list.aspx','Messaggistica.View.ProviderList',2002),(11244,'/default.aspx','Messaggistica.View.MainPage',2002),(11257,'/admin/competenze/list.aspx','Chiarimenti.Views.Admin.CompetenzeList',240),(11258,'/admin/competenze/add.aspx','Chiarimenti.Views.Admin.CompetenzeAdd',240),(11259,'/admin/competenze/mod.aspx','Chiarimenti.Views.Admin.CompetenzeMod',240),(11260,'/admin/dettaglio.aspx','Chiarimenti.Views.Admin.DettaglioPageAdmin',240),(11261,'/admin/default.aspx','Chiarimenti.Views.Admin.ElencoPage',240),(11262,'/dettaglio.aspx','Chiarimenti.Views.Front.DettaglioPage',240),(11263,'/default.aspx','Chiarimenti.Views.Front.ElencoChiarimenti',240),(11264,'/richiesta.aspx','Chiarimenti.Views.Front.RichiestaPage',240),(11276,'/detail.aspx','GenericJobs.Views.JobDetail',2216),(11277,'/default.aspx','GenericJobs.Views.JobList',2216),(11278,'/jobs/addcustomjob.aspx','GenericJobs.Views.ConsoleJobAdd',2216);
/*!40000 ALTER TABLE `externalappz_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `externalappzcriteria`
--

DROP TABLE IF EXISTS `externalappzcriteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `externalappzcriteria` (
  `id_ExternalAppCriteria` int(11) NOT NULL AUTO_INCREMENT,
  `Nome_ExternalAppCriteria` varchar(255) DEFAULT NULL,
  `Key_ExternalAppCriteria` varchar(255) DEFAULT NULL,
  `App_ExternalAppCriteria` int(11) DEFAULT NULL,
  `Dynamic_ExternalAppCriteria` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ExternalAppCriteria`),
  KEY `App_ExternalAppCriteria` (`App_ExternalAppCriteria`),
  KEY `App_ExternalAppCriteria_2` (`App_ExternalAppCriteria`),
  CONSTRAINT `externalapp_criteria` FOREIGN KEY (`App_ExternalAppCriteria`) REFERENCES `externalappz` (`id_ExternalApp`),
  CONSTRAINT `externalappzcriteria_fk` FOREIGN KEY (`App_ExternalAppCriteria`) REFERENCES `externalappz` (`id_ExternalApp`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1117 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `externalappzcriteria`
--

LOCK TABLES `externalappzcriteria` WRITE;
/*!40000 ALTER TABLE `externalappzcriteria` DISABLE KEYS */;
INSERT INTO `externalappzcriteria` VALUES (4,'Consenti Utilizzo','enable',4,0),(20,'Consenti Utilizzo','enable',20,0),(470,'Permetti gestione dei permessi','grants',4,0),(475,'Permetti gestione dei permessi','grants',20,0),(801,'Consenti Utilizzo','enable',235,1),(802,'Permetti gestione dei permessi','grants',235,1),(803,'Consenti Utilizzo','enable',3001,0),(804,'Permetti gestione dei permessi','grants',3001,0),(805,'Consenti Utilizzo','enable',5,0),(806,'Permetti gestione dei permessi','grants',5,0),(807,'Abilita creazione di nuovi utenti.','users_create',5,0),(808,'Abilita la modifica dei dati relativi agli utenti.','users_edit',5,0),(809,'Abilita la visione di tutti i dati anagrafici dell\'utente.','users_fulldetails',5,0),(810,'Abilita la gestione dei gruppi associati all\'utente.','users_groups',5,0),(811,'Abilita la gestione dei portali al quale gli utenti ha diritto accedere.','users_networks',5,0),(812,'Abilita la modifica dello stato dell\'utente.','users_status',5,0),(813,'Abilita la gestione degli Utenti non associati ad alcun gruppo.','users_ungroupped',5,0),(814,'Abilita l\'inoltro di nuove credenziali temporanee di accesso agli utenti.','users_new_password',5,0),(815,'Abilita visualizzazione riepilogo permessi.','users_grants',5,0),(816,'Consenti Utilizzo','enable',6,0),(817,'Permetti gestione dei permessi','grants',6,0),(1101,'Abilita la gestione dei servizi associabili all\'utente.','users_services',5,0),(1107,'Consenti Utilizzo','enable',2216,0),(1108,'Permetti gestione dei permessi','grants',2216,0),(1111,'Consenti Utilizzo','enable',2002,0),(1112,'Permetti gestione dei permessi','grants',2002,0),(1113,'Amministrazione Net.Service','netservice',2002,0),(1114,'Consenti Utilizzo','enable',240,1),(1115,'Permetti gestione dei permessi','grants',240,1),(1116,'Consenti la ricezione delle notifiche','takenotify',240,1);
/*!40000 ALTER TABLE `externalappzcriteria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorites`
--

DROP TABLE IF EXISTS `favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorites` (
  `User_Favorite` int(11) DEFAULT NULL,
  `Folder_Favorite` int(11) DEFAULT NULL,
  KEY `Folder_Favorite` (`Folder_Favorite`),
  KEY `User_Favorite` (`User_Favorite`),
  KEY `Folder_Favorite_2` (`Folder_Favorite`),
  KEY `User_Favorite_2` (`User_Favorite`),
  CONSTRAINT `FK139C840371205BB4` FOREIGN KEY (`Folder_Favorite`) REFERENCES `network_folders` (`id_NetworkFolder`),
  CONSTRAINT `FK139C8403E2938B46` FOREIGN KEY (`User_Favorite`) REFERENCES `users` (`id_User`),
  CONSTRAINT `FKF92CF820170E62D3` FOREIGN KEY (`Folder_Favorite`) REFERENCES `network_folders` (`id_NetworkFolder`),
  CONSTRAINT `FKF92CF820788DB001` FOREIGN KEY (`User_Favorite`) REFERENCES `users` (`id_User`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorites`
--

LOCK TABLES `favorites` WRITE;
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files_docs`
--

DROP TABLE IF EXISTS `files_docs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files_docs` (
  `id_docs` int(11) NOT NULL,
  PRIMARY KEY (`id_docs`),
  KEY `id_docs` (`id_docs`),
  KEY `id_docs_2` (`id_docs`),
  CONSTRAINT `FK77128CED47E672DB` FOREIGN KEY (`id_docs`) REFERENCES `docs` (`id_Doc`),
  CONSTRAINT `FKA3177F8BB0D99845` FOREIGN KEY (`id_docs`) REFERENCES `docs` (`id_Doc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files_docs`
--

LOCK TABLES `files_docs` WRITE;
/*!40000 ALTER TABLE `files_docs` DISABLE KEYS */;
/*!40000 ALTER TABLE `files_docs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files_docs_contents`
--

DROP TABLE IF EXISTS `files_docs_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files_docs_contents` (
  `id_DocFiles` int(11) NOT NULL,
  `Link_DocFiles` mediumtext,
  `ContentType_DocFiles` varchar(255) DEFAULT NULL,
  `Extension_DocFiles` varchar(255) DEFAULT NULL,
  `Size_DocFiles` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_DocFiles`),
  KEY `id_DocFiles` (`id_DocFiles`),
  KEY `id_DocFiles_2` (`id_DocFiles`),
  CONSTRAINT `FKA4E9905FFFAC1356` FOREIGN KEY (`id_DocFiles`) REFERENCES `docs_contents` (`id_DocContent`),
  CONSTRAINT `FKDC0F82D0F2DD94AB` FOREIGN KEY (`id_DocFiles`) REFERENCES `docs_contents` (`id_DocContent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files_docs_contents`
--

LOCK TABLES `files_docs_contents` WRITE;
/*!40000 ALTER TABLE `files_docs_contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `files_docs_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files_folders`
--

DROP TABLE IF EXISTS `files_folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files_folders` (
  `id_folders` int(11) NOT NULL,
  PRIMARY KEY (`id_folders`),
  KEY `id_folders` (`id_folders`),
  KEY `id_folders_2` (`id_folders`),
  CONSTRAINT `FK551A246B7CE62EC5` FOREIGN KEY (`id_folders`) REFERENCES `folders` (`id_Folder`),
  CONSTRAINT `FKC5ED416EC73A8FD` FOREIGN KEY (`id_folders`) REFERENCES `folders` (`id_Folder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files_folders`
--

LOCK TABLES `files_folders` WRITE;
/*!40000 ALTER TABLE `files_folders` DISABLE KEYS */;
INSERT INTO `files_folders` VALUES (3);
/*!40000 ALTER TABLE `files_folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folder_profiles`
--

DROP TABLE IF EXISTS `folder_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folder_profiles` (
  `id_FolderProfile` int(11) NOT NULL,
  `Folder_ObjectProfile` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_FolderProfile`),
  KEY `id_FolderProfile` (`id_FolderProfile`),
  KEY `Folder_ObjectProfile` (`Folder_ObjectProfile`),
  KEY `id_FolderProfile_2` (`id_FolderProfile`),
  CONSTRAINT `FK998B56E92D2DA0AF` FOREIGN KEY (`id_FolderProfile`) REFERENCES `object_profiles` (`id_ObjectProfile`),
  CONSTRAINT `FKBFAC8879DA4EA3CE` FOREIGN KEY (`id_FolderProfile`) REFERENCES `object_profiles` (`id_ObjectProfile`),
  CONSTRAINT `object_profiles_folder` FOREIGN KEY (`Folder_ObjectProfile`) REFERENCES `folders` (`id_Folder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folder_profiles`
--

LOCK TABLES `folder_profiles` WRITE;
/*!40000 ALTER TABLE `folder_profiles` DISABLE KEYS */;
INSERT INTO `folder_profiles` VALUES (331,1),(332,1);
/*!40000 ALTER TABLE `folder_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folder_profiles_criteria`
--

DROP TABLE IF EXISTS `folder_profiles_criteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folder_profiles_criteria` (
  `id_FolderProfileCriteria` int(11) NOT NULL,
  `Application_FolderProfileCriteria` int(11) DEFAULT NULL,
  `FolderProfile_ObjectProfileCriteria` int(11) DEFAULT NULL,
  `Criteria_ObjectProfileCriteria` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_FolderProfileCriteria`),
  KEY `id_FolderProfileCriteria` (`id_FolderProfileCriteria`),
  KEY `FolderProfile_ObjectProfileCriteria` (`FolderProfile_ObjectProfileCriteria`),
  KEY `Criteria_ObjectProfileCriteria` (`Criteria_ObjectProfileCriteria`),
  KEY `id_FolderProfileCriteria_2` (`id_FolderProfileCriteria`),
  CONSTRAINT `FK84907E91247D1381` FOREIGN KEY (`id_FolderProfileCriteria`) REFERENCES `object_profiles_criteria` (`id_ObjectProfileCriteria`),
  CONSTRAINT `FKD8A17D328A2C0B1` FOREIGN KEY (`id_FolderProfileCriteria`) REFERENCES `object_profiles_criteria` (`id_ObjectProfileCriteria`),
  CONSTRAINT `folder_profiles_criteria_criterio` FOREIGN KEY (`Criteria_ObjectProfileCriteria`) REFERENCES `criteri` (`id_Criterio`),
  CONSTRAINT `object_profiles_criteria_folderprofile` FOREIGN KEY (`FolderProfile_ObjectProfileCriteria`) REFERENCES `folder_profiles` (`id_FolderProfile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folder_profiles_criteria`
--

LOCK TABLES `folder_profiles_criteria` WRITE;
/*!40000 ALTER TABLE `folder_profiles_criteria` DISABLE KEYS */;
INSERT INTO `folder_profiles_criteria` VALUES (20396,0,332,1),(20397,0,332,2),(20398,0,332,3),(20399,0,332,4),(20400,0,332,5),(20401,0,332,6),(20402,0,332,7),(20403,0,332,8),(20404,0,332,10),(20405,0,332,11),(20406,0,332,12),(20407,0,332,23),(20408,0,332,24),(20409,0,332,16),(20410,0,332,18),(20475,0,331,1),(20476,0,331,2),(20477,0,331,3),(20478,0,331,4),(20479,0,331,5),(20480,0,331,6),(20481,0,331,7),(20482,0,331,8),(20483,0,331,10),(20484,0,331,11),(20485,0,331,12),(20486,0,331,23),(20487,0,331,24),(20488,0,331,19),(20489,0,331,16),(20490,0,331,18);
/*!40000 ALTER TABLE `folder_profiles_criteria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folders`
--

DROP TABLE IF EXISTS `folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders` (
  `id_Folder` int(11) NOT NULL AUTO_INCREMENT,
  `Tree_Folder` varchar(255) DEFAULT NULL,
  `Tipo_Folder` int(11) DEFAULT NULL,
  `Stato_Folder` int(11) DEFAULT NULL,
  `Name_Folder` varchar(255) DEFAULT NULL,
  `Depth_Folder` int(11) DEFAULT NULL,
  `Trash_Folder` int(11) DEFAULT '0',
  `Restricted_Folder` int(11) DEFAULT '0',
  `HomeType_Folder` int(11) DEFAULT '0',
  `Disable_Folder` int(11) DEFAULT '0',
  `TitleOffset_Folder` int(11) DEFAULT NULL,
  `CssClass_Folder` varchar(255) DEFAULT NULL,
  `Order_Folder` int(11) DEFAULT '0',
  `Rss_Folder` int(11) DEFAULT '0',
  `Layout_Folder` int(11) DEFAULT '0',
  `ShowMenu_Folder` int(11) DEFAULT '0',
  `Template_Folder` varchar(255) DEFAULT NULL,
  `ShowInTopMenu_Folder` int(11) DEFAULT '1',
  `CoverImage_Folder` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_Folder`),
  KEY `Tipo_Folder` (`Tipo_Folder`),
  KEY `id_Folder` (`id_Folder`),
  KEY `id_Folder_2` (`id_Folder`),
  CONSTRAINT `FKB45CDF0CCCE1061A` FOREIGN KEY (`id_Folder`) REFERENCES `network_folders` (`id_NetworkFolder`),
  CONSTRAINT `FKC270B45E89661ED` FOREIGN KEY (`id_Folder`) REFERENCES `network_folders` (`id_NetworkFolder`),
  CONSTRAINT `folders_applicazioni` FOREIGN KEY (`Tipo_Folder`) REFERENCES `applicazioni` (`id_Applicazione`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folders`
--

LOCK TABLES `folders` WRITE;
/*!40000 ALTER TABLE `folders` DISABLE KEYS */;
INSERT INTO `folders` VALUES (1,'.000001.',1,0,'root',1,0,0,0,0,0,NULL,0,0,0,0,NULL,1,NULL),(3,'.000001.000003.',4,1,'ImgHome',2,0,0,0,0,0,NULL,0,0,0,0,NULL,1,NULL);
/*!40000 ALTER TABLE `folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folders_meta`
--

DROP TABLE IF EXISTS `folders_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders_meta` (
  `id_FolderMeta` int(11) NOT NULL AUTO_INCREMENT,
  `Folder_FolderMeta` int(11) DEFAULT NULL,
  `Meta_FolderMeta` int(11) DEFAULT NULL,
  `Stato_FolderMeta` int(11) DEFAULT NULL,
  `Content_FolderMeta` longtext,
  PRIMARY KEY (`id_FolderMeta`),
  KEY `Folder_FolderMeta` (`Folder_FolderMeta`),
  KEY `Meta_FolderMeta` (`Meta_FolderMeta`),
  CONSTRAINT `folders_meta_folders` FOREIGN KEY (`Folder_FolderMeta`) REFERENCES `folders` (`id_Folder`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `folders_meta_meta` FOREIGN KEY (`Meta_FolderMeta`) REFERENCES `meta` (`id_Meta`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folders_meta`
--

LOCK TABLES `folders_meta` WRITE;
/*!40000 ALTER TABLE `folders_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `folders_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folders_users_custom_view`
--

DROP TABLE IF EXISTS `folders_users_custom_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders_users_custom_view` (
  `id_FolderCustomView` int(11) NOT NULL AUTO_INCREMENT,
  `Folder_FolderCustomView` int(11) DEFAULT NULL,
  `User_FolderCustomView` int(11) DEFAULT NULL,
  `View_FolderCustomView` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_FolderCustomView`),
  KEY `Folder_FolderCustomView` (`Folder_FolderCustomView`),
  KEY `User_FolderCustomView` (`User_FolderCustomView`),
  CONSTRAINT `foldersuserscustomview_folder` FOREIGN KEY (`Folder_FolderCustomView`) REFERENCES `folders` (`id_Folder`),
  CONSTRAINT `foldersuserscustomview_user` FOREIGN KEY (`User_FolderCustomView`) REFERENCES `users` (`id_User`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folders_users_custom_view`
--

LOCK TABLES `folders_users_custom_view` WRITE;
/*!40000 ALTER TABLE `folders_users_custom_view` DISABLE KEYS */;
/*!40000 ALTER TABLE `folders_users_custom_view` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folderslang`
--

DROP TABLE IF EXISTS `folderslang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folderslang` (
  `id_FolderLang` int(11) NOT NULL AUTO_INCREMENT,
  `Lang_FolderLang` int(11) DEFAULT NULL,
  `Folder_FolderLang` int(11) DEFAULT NULL,
  `Label_FolderLang` text,
  `Desc_FolderLang` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id_FolderLang`),
  KEY `Folder_FolderLang` (`Folder_FolderLang`),
  KEY `Lang_FolderLang` (`Lang_FolderLang`),
  CONSTRAINT `folderslang_folders` FOREIGN KEY (`Folder_FolderLang`) REFERENCES `folders` (`id_Folder`),
  CONSTRAINT `folderslang_lang` FOREIGN KEY (`Lang_FolderLang`) REFERENCES `lang` (`id_Lang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folderslang`
--

LOCK TABLES `folderslang` WRITE;
/*!40000 ALTER TABLE `folderslang` DISABLE KEYS */;
INSERT INTO `folderslang` VALUES (1,1,1,'Radice sito Internet','Radice'),(3,1,3,'Immagini della Homepage','Immagini della homepage');
/*!40000 ALTER TABLE `folderslang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id_Group` int(11) NOT NULL AUTO_INCREMENT,
  `Name_Group` varchar(255) DEFAULT NULL,
  `Tree_Group` varchar(255) DEFAULT NULL,
  `Parent_Group` int(11) DEFAULT NULL,
  `Profile_Group` int(11) DEFAULT NULL,
  `Depth_Group` int(11) DEFAULT NULL,
  `SystemKey_Group` varchar(255) DEFAULT NULL,
  `MacroGroup_Group` int(11) DEFAULT '0',
  PRIMARY KEY (`id_Group`),
  KEY `Profile_Group` (`Profile_Group`),
  KEY `MacroGroup_Group` (`MacroGroup_Group`),
  KEY `Parent_Group` (`Parent_Group`),
  KEY `MacroGroup_Group_2` (`MacroGroup_Group`),
  CONSTRAINT `FK7B78D1127223222D` FOREIGN KEY (`MacroGroup_Group`) REFERENCES `groups_macrogroups` (`id_MacroGroup`),
  CONSTRAINT `FKBA94BDAEBBBAF7D` FOREIGN KEY (`MacroGroup_Group`) REFERENCES `groups_macrogroups` (`id_MacroGroup`),
  CONSTRAINT `groups_parent` FOREIGN KEY (`Parent_Group`) REFERENCES `groups` (`id_Group`),
  CONSTRAINT `groups_profile` FOREIGN KEY (`Profile_Group`) REFERENCES `groups_profile` (`id_Profile`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'Root','.000001.',NULL,1,0,'root',1);
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups_macrogroups`
--

DROP TABLE IF EXISTS `groups_macrogroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups_macrogroups` (
  `id_MacroGroup` int(11) NOT NULL AUTO_INCREMENT,
  `Label_MacroGroup` varchar(255) DEFAULT NULL,
  `Key_MacroGroup` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_MacroGroup`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups_macrogroups`
--

LOCK TABLES `groups_macrogroups` WRITE;
/*!40000 ALTER TABLE `groups_macrogroups` DISABLE KEYS */;
INSERT INTO `groups_macrogroups` VALUES (1,'Nessuno','empty'),(2,'Docenti','docenti'),(3,'Studenti','studenti'),(4,'Personale ATA','ata'),(5,'Genitori','genitori'),(6,'Direttivo','direttivo'),(7,'Istituti','istituti'),(8,'Associazioni','associazioni'),(9,'Universita','universita'),(10,'Comuni','comuni'),(11,'Province','province');
/*!40000 ALTER TABLE `groups_macrogroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups_profile`
--

DROP TABLE IF EXISTS `groups_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups_profile` (
  `id_Profile` int(11) NOT NULL,
  PRIMARY KEY (`id_Profile`),
  KEY `id_Profile` (`id_Profile`),
  KEY `id_Profile_2` (`id_Profile`),
  CONSTRAINT `FK80FB0C44516C31EB` FOREIGN KEY (`id_Profile`) REFERENCES `profiles` (`id_Profile`),
  CONSTRAINT `FKF9AFC76148DE61AF` FOREIGN KEY (`id_Profile`) REFERENCES `profiles` (`id_Profile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups_profile`
--

LOCK TABLES `groups_profile` WRITE;
/*!40000 ALTER TABLE `groups_profile` DISABLE KEYS */;
INSERT INTO `groups_profile` VALUES (1);
/*!40000 ALTER TABLE `groups_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gw_actors`
--

DROP TABLE IF EXISTS `gw_actors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gw_actors` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gw_actors`
--

LOCK TABLES `gw_actors` WRITE;
/*!40000 ALTER TABLE `gw_actors` DISABLE KEYS */;
/*!40000 ALTER TABLE `gw_actors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gw_roles`
--

DROP TABLE IF EXISTS `gw_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gw_roles` (
  `IDactor` int(11) NOT NULL,
  `IDstep` int(11) NOT NULL,
  PRIMARY KEY (`IDstep`,`IDactor`),
  KEY `IDstep` (`IDstep`),
  KEY `IDactor` (`IDactor`),
  KEY `IDstep_2` (`IDstep`),
  KEY `IDactor_2` (`IDactor`),
  CONSTRAINT `FK25F21652132CFA06` FOREIGN KEY (`IDactor`) REFERENCES `gw_actors` (`ID`),
  CONSTRAINT `FK25F216524559206E` FOREIGN KEY (`IDstep`) REFERENCES `gw_steps` (`ID`),
  CONSTRAINT `FK4763E1B01B50A445` FOREIGN KEY (`IDstep`) REFERENCES `gw_steps` (`ID`),
  CONSTRAINT `FK4763E1B0C862B57F` FOREIGN KEY (`IDactor`) REFERENCES `gw_actors` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gw_roles`
--

LOCK TABLES `gw_roles` WRITE;
/*!40000 ALTER TABLE `gw_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `gw_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gw_steps`
--

DROP TABLE IF EXISTS `gw_steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gw_steps` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `orderstep` int(11) DEFAULT NULL,
  `IDworkflow` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDworkflow` (`IDworkflow`),
  KEY `IDworkflow_2` (`IDworkflow`),
  CONSTRAINT `FK3CF2CDEC8BC1C268` FOREIGN KEY (`IDworkflow`) REFERENCES `gw_workflows` (`ID`),
  CONSTRAINT `FK6CCA3A181AFD911F` FOREIGN KEY (`IDworkflow`) REFERENCES `gw_workflows` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gw_steps`
--

LOCK TABLES `gw_steps` WRITE;
/*!40000 ALTER TABLE `gw_steps` DISABLE KEYS */;
/*!40000 ALTER TABLE `gw_steps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gw_workflow_steps`
--

DROP TABLE IF EXISTS `gw_workflow_steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gw_workflow_steps` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IsCurrent` tinyint(1) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `Direction` varchar(255) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `IDstep` int(11) DEFAULT NULL,
  `IDworkflowinstance` int(11) DEFAULT NULL,
  `IDpreviousworkflowstep` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDstep` (`IDstep`),
  KEY `IDworkflowinstance` (`IDworkflowinstance`),
  KEY `IDpreviousworkflowstep` (`IDpreviousworkflowstep`),
  KEY `IDstep_2` (`IDstep`),
  KEY `IDworkflowinstance_2` (`IDworkflowinstance`),
  KEY `IDpreviousworkflowstep_2` (`IDpreviousworkflowstep`),
  CONSTRAINT `FK7C8791F31B50A445` FOREIGN KEY (`IDstep`) REFERENCES `gw_steps` (`ID`),
  CONSTRAINT `FK7C8791F3320B6FEF` FOREIGN KEY (`IDworkflowinstance`) REFERENCES `gw_workflowsinstances` (`ID`),
  CONSTRAINT `FK7C8791F3F069AE48` FOREIGN KEY (`IDpreviousworkflowstep`) REFERENCES `gw_workflow_steps` (`ID`),
  CONSTRAINT `FKE39003404559206E` FOREIGN KEY (`IDstep`) REFERENCES `gw_steps` (`ID`),
  CONSTRAINT `FKE39003409FEA68CE` FOREIGN KEY (`IDworkflowinstance`) REFERENCES `gw_workflowsinstances` (`ID`),
  CONSTRAINT `FKE3900340F63E1FF9` FOREIGN KEY (`IDpreviousworkflowstep`) REFERENCES `gw_workflow_steps` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gw_workflow_steps`
--

LOCK TABLES `gw_workflow_steps` WRITE;
/*!40000 ALTER TABLE `gw_workflow_steps` DISABLE KEYS */;
/*!40000 ALTER TABLE `gw_workflow_steps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gw_workflows`
--

DROP TABLE IF EXISTS `gw_workflows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gw_workflows` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `workflowkey` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gw_workflows`
--

LOCK TABLES `gw_workflows` WRITE;
/*!40000 ALTER TABLE `gw_workflows` DISABLE KEYS */;
/*!40000 ALTER TABLE `gw_workflows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gw_workflowsinstances`
--

DROP TABLE IF EXISTS `gw_workflowsinstances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gw_workflowsinstances` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `Parent` int(11) DEFAULT NULL,
  `IDworkflow` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Parent` (`Parent`),
  KEY `IDworkflow` (`IDworkflow`),
  KEY `Parent_2` (`Parent`),
  KEY `IDworkflow_2` (`IDworkflow`),
  CONSTRAINT `FK835B05161AFD911F` FOREIGN KEY (`IDworkflow`) REFERENCES `gw_workflows` (`ID`),
  CONSTRAINT `FK835B0516F412A3C6` FOREIGN KEY (`Parent`) REFERENCES `gw_workflowsinstances` (`ID`),
  CONSTRAINT `FKD4ABEED58BC1C268` FOREIGN KEY (`IDworkflow`) REFERENCES `gw_workflows` (`ID`),
  CONSTRAINT `FKD4ABEED5EEA621BF` FOREIGN KEY (`Parent`) REFERENCES `gw_workflowsinstances` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gw_workflowsinstances`
--

LOCK TABLES `gw_workflowsinstances` WRITE;
/*!40000 ALTER TABLE `gw_workflowsinstances` DISABLE KEYS */;
/*!40000 ALTER TABLE `gw_workflowsinstances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_contenitori`
--

DROP TABLE IF EXISTS `homepages_contenitori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_contenitori` (
  `ID_contenitore` int(11) NOT NULL AUTO_INCREMENT,
  `nome_contenitore` varchar(255) DEFAULT NULL,
  `tree_contenitore` varchar(255) DEFAULT NULL,
  `stato_contenitore` int(11) DEFAULT NULL,
  `posizione_contenitore` int(11) DEFAULT NULL,
  `depth_contenitore` int(11) DEFAULT NULL,
  `cssclass_contenitore` varchar(255) DEFAULT NULL,
  `parent_contenitore` int(11) DEFAULT NULL,
  `homepage_contenitore` int(11) DEFAULT NULL,
  `eredita_contenitore` tinyint(1) DEFAULT NULL,
  `order_contenitore` int(11) DEFAULT '0',
  `wrappercssclass_contenitore` varchar(255) DEFAULT NULL,
  `mode_contenitore` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_contenitore`),
  KEY `homepage_contenitore` (`homepage_contenitore`),
  KEY `parent_contenitore` (`parent_contenitore`),
  CONSTRAINT `homepages_contenitori_homepages_docs_contents` FOREIGN KEY (`homepage_contenitore`) REFERENCES `homepages_docs_contents` (`id_Homepage`),
  CONSTRAINT `homepages_contenitori_parent` FOREIGN KEY (`parent_contenitore`) REFERENCES `homepages_contenitori` (`ID_contenitore`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_contenitori`
--

LOCK TABLES `homepages_contenitori` WRITE;
/*!40000 ALTER TABLE `homepages_contenitori` DISABLE KEYS */;
INSERT INTO `homepages_contenitori` VALUES (5,'radice','.000005.',1,0,0,NULL,NULL,1,0,0,NULL,NULL),(6,'Colsx','.000005.000006.',1,0,1,NULL,5,1,0,0,NULL,NULL),(7,'Colcx','.000005.000007.',1,0,1,NULL,5,1,0,0,NULL,NULL),(8,'Coldx','.000005.000008.',1,0,1,NULL,5,1,0,0,NULL,NULL),(9,'Footer','.000005.000009.',1,0,1,NULL,5,1,0,0,NULL,NULL);
/*!40000 ALTER TABLE `homepages_contenitori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_docs`
--

DROP TABLE IF EXISTS `homepages_docs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_docs` (
  `id_docs` int(11) NOT NULL,
  PRIMARY KEY (`id_docs`),
  KEY `id_docs` (`id_docs`),
  KEY `id_docs_2` (`id_docs`),
  CONSTRAINT `FK7F5E0BEB47E672DB` FOREIGN KEY (`id_docs`) REFERENCES `docs` (`id_Doc`),
  CONSTRAINT `FKE8BF386AB0D99845` FOREIGN KEY (`id_docs`) REFERENCES `docs` (`id_Doc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_docs`
--

LOCK TABLES `homepages_docs` WRITE;
/*!40000 ALTER TABLE `homepages_docs` DISABLE KEYS */;
INSERT INTO `homepages_docs` VALUES (1);
/*!40000 ALTER TABLE `homepages_docs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_docs_contents`
--

DROP TABLE IF EXISTS `homepages_docs_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_docs_contents` (
  `id_Homepage` int(11) NOT NULL,
  `Nome_homepage` varchar(255) DEFAULT NULL,
  `MetaDescription_homepage` mediumtext,
  `Keywords_homepage` mediumtext,
  `MsgManutenzione_homepage` mediumtext,
  `Stato_homepage` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Homepage`),
  KEY `id_Homepage` (`id_Homepage`),
  KEY `id_Homepage_2` (`id_Homepage`),
  CONSTRAINT `FK1B7A2CF2AFBF528E` FOREIGN KEY (`id_Homepage`) REFERENCES `docs_contents` (`id_DocContent`),
  CONSTRAINT `FKD1D7A4FEF3AAA00` FOREIGN KEY (`id_Homepage`) REFERENCES `docs_contents` (`id_DocContent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_docs_contents`
--

LOCK TABLES `homepages_docs_contents` WRITE;
/*!40000 ALTER TABLE `homepages_docs_contents` DISABLE KEYS */;
INSERT INTO `homepages_docs_contents` VALUES (1,'Homepage',NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `homepages_docs_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_folders`
--

DROP TABLE IF EXISTS `homepages_folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_folders` (
  `id_folders` int(11) NOT NULL,
  PRIMARY KEY (`id_folders`),
  KEY `id_folders` (`id_folders`),
  KEY `id_folders_2` (`id_folders`),
  CONSTRAINT `FK5A266176C73A8FD` FOREIGN KEY (`id_folders`) REFERENCES `folders` (`id_Folder`),
  CONSTRAINT `FKCCB69197CE62EC5` FOREIGN KEY (`id_folders`) REFERENCES `folders` (`id_Folder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_folders`
--

LOCK TABLES `homepages_folders` WRITE;
/*!40000 ALTER TABLE `homepages_folders` DISABLE KEYS */;
INSERT INTO `homepages_folders` VALUES (1);
/*!40000 ALTER TABLE `homepages_folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_moduli`
--

DROP TABLE IF EXISTS `homepages_moduli`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_moduli` (
  `id_Modulo` int(11) NOT NULL AUTO_INCREMENT,
  `Posizione_Modulo` int(11) DEFAULT NULL,
  `Tipo_Modulo` int(11) DEFAULT NULL,
  `Stato_Modulo` int(11) DEFAULT NULL,
  `Contenitore_Modulo` int(11) DEFAULT NULL,
  `CssClass_Modulo` varchar(255) DEFAULT NULL,
  `TemplateMode_Modulo` varchar(255) DEFAULT NULL,
  `ShowOnlyInUrl_Modulo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_Modulo`),
  KEY `Contenitore_Modulo` (`Contenitore_Modulo`),
  CONSTRAINT `homepages_moduli_homepages_contenitori` FOREIGN KEY (`Contenitore_Modulo`) REFERENCES `homepages_contenitori` (`ID_contenitore`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_moduli`
--

LOCK TABLES `homepages_moduli` WRITE;
/*!40000 ALTER TABLE `homepages_moduli` DISABLE KEYS */;
INSERT INTO `homepages_moduli` VALUES (6,1,2,1,6,NULL,NULL,NULL),(13,1,21,1,8,'',NULL,NULL);
/*!40000 ALTER TABLE `homepages_moduli` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_moduli_applicativi`
--

DROP TABLE IF EXISTS `homepages_moduli_applicativi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_moduli_applicativi` (
  `Modulo_ModuloApplicativo` int(11) NOT NULL,
  `Subfolders_ModuloApplicativo` int(11) DEFAULT NULL,
  `Records_ModuloApplicativo` int(11) DEFAULT NULL,
  `ShowDesc_ModuloApplicativo` int(11) DEFAULT NULL,
  `DescLength_ModuloApplicativo` int(11) DEFAULT NULL,
  `Order_ModuloApplicativo` int(11) DEFAULT NULL,
  `Folder_ModuloApplicativo` int(11) DEFAULT NULL,
  `ShowMoreLink_ModuloApplicativo` tinyint(1) NOT NULL DEFAULT '0',
  `HideImage_ModuloApplicativo` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Modulo_ModuloApplicativo`),
  KEY `Modulo_ModuloApplicativo` (`Modulo_ModuloApplicativo`),
  KEY `Modulo_ModuloApplicativo_2` (`Modulo_ModuloApplicativo`),
  CONSTRAINT `FKB8FA35F94D9585B7` FOREIGN KEY (`Modulo_ModuloApplicativo`) REFERENCES `homepages_moduli` (`id_Modulo`),
  CONSTRAINT `FKBBF871006DDAA099` FOREIGN KEY (`Modulo_ModuloApplicativo`) REFERENCES `homepages_moduli` (`id_Modulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_moduli_applicativi`
--

LOCK TABLES `homepages_moduli_applicativi` WRITE;
/*!40000 ALTER TABLE `homepages_moduli_applicativi` DISABLE KEYS */;
/*!40000 ALTER TABLE `homepages_moduli_applicativi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_moduli_applicativi_news`
--

DROP TABLE IF EXISTS `homepages_moduli_applicativi_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_moduli_applicativi_news` (
  `Modulo_ModuloApplicativoNews` int(11) NOT NULL,
  `ShowCustomNews_ModuloApplicativoNews` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Modulo_ModuloApplicativoNews`),
  KEY `Modulo_ModuloApplicativoNews` (`Modulo_ModuloApplicativoNews`),
  CONSTRAINT `FK7F2CA82DE0601E9E` FOREIGN KEY (`Modulo_ModuloApplicativoNews`) REFERENCES `homepages_moduli_applicativi` (`Modulo_ModuloApplicativo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_moduli_applicativi_news`
--

LOCK TABLES `homepages_moduli_applicativi_news` WRITE;
/*!40000 ALTER TABLE `homepages_moduli_applicativi_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `homepages_moduli_applicativi_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_moduli_circolari`
--

DROP TABLE IF EXISTS `homepages_moduli_circolari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_moduli_circolari` (
  `Modulo_ModuloCircolare` int(11) NOT NULL,
  `ModuloCollegato_ModuloCircolare` int(11) DEFAULT NULL,
  PRIMARY KEY (`Modulo_ModuloCircolare`),
  KEY `Modulo_ModuloCircolare` (`Modulo_ModuloCircolare`),
  KEY `ModuloCollegato_ModuloCircolare` (`ModuloCollegato_ModuloCircolare`),
  KEY `Modulo_ModuloCircolare_2` (`Modulo_ModuloCircolare`),
  KEY `ModuloCollegato_ModuloCircolar_2` (`ModuloCollegato_ModuloCircolare`),
  CONSTRAINT `FK3D0BF3F99A3C681F` FOREIGN KEY (`Modulo_ModuloCircolare`) REFERENCES `homepages_moduli` (`id_Modulo`),
  CONSTRAINT `FK3D0BF3F9AD7A020B` FOREIGN KEY (`ModuloCollegato_ModuloCircolare`) REFERENCES `homepages_moduli` (`id_Modulo`),
  CONSTRAINT `FK44E70ADFC41D00F7` FOREIGN KEY (`Modulo_ModuloCircolare`) REFERENCES `homepages_moduli` (`id_Modulo`),
  CONSTRAINT `FK44E70ADFD5E28ACF` FOREIGN KEY (`ModuloCollegato_ModuloCircolare`) REFERENCES `homepages_moduli` (`id_Modulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_moduli_circolari`
--

LOCK TABLES `homepages_moduli_circolari` WRITE;
/*!40000 ALTER TABLE `homepages_moduli_circolari` DISABLE KEYS */;
/*!40000 ALTER TABLE `homepages_moduli_circolari` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_moduli_login`
--

DROP TABLE IF EXISTS `homepages_moduli_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_moduli_login` (
  `Modulo_ModuloLogin` int(11) NOT NULL,
  `LinkRegistrazione_ModuloLogin` tinyint(1) DEFAULT NULL,
  `RecuperaPassword_ModuloLogin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Modulo_ModuloLogin`),
  KEY `Modulo_ModuloLogin` (`Modulo_ModuloLogin`),
  KEY `Modulo_ModuloLogin_2` (`Modulo_ModuloLogin`),
  CONSTRAINT `FK14330C59110FCA9C` FOREIGN KEY (`Modulo_ModuloLogin`) REFERENCES `homepages_moduli` (`id_Modulo`),
  CONSTRAINT `FK7A2387426E8A5A62` FOREIGN KEY (`Modulo_ModuloLogin`) REFERENCES `homepages_moduli` (`id_Modulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_moduli_login`
--

LOCK TABLES `homepages_moduli_login` WRITE;
/*!40000 ALTER TABLE `homepages_moduli_login` DISABLE KEYS */;
INSERT INTO `homepages_moduli_login` VALUES (13,1,0);
/*!40000 ALTER TABLE `homepages_moduli_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_moduli_login_advanced`
--

DROP TABLE IF EXISTS `homepages_moduli_login_advanced`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_moduli_login_advanced` (
  `Modulo_ModuloLoginAdv` int(11) NOT NULL,
  PRIMARY KEY (`Modulo_ModuloLoginAdv`),
  KEY `Modulo_ModuloLoginAdv` (`Modulo_ModuloLoginAdv`),
  CONSTRAINT `FKCAB5763BAE6D7481` FOREIGN KEY (`Modulo_ModuloLoginAdv`) REFERENCES `homepages_moduli` (`id_Modulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_moduli_login_advanced`
--

LOCK TABLES `homepages_moduli_login_advanced` WRITE;
/*!40000 ALTER TABLE `homepages_moduli_login_advanced` DISABLE KEYS */;
/*!40000 ALTER TABLE `homepages_moduli_login_advanced` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_moduli_menu`
--

DROP TABLE IF EXISTS `homepages_moduli_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_moduli_menu` (
  `Modulo_ModuloMenu` int(11) NOT NULL,
  `Starting_ModuloMenu` int(11) DEFAULT NULL,
  `Ending_ModuloMenu` int(11) DEFAULT NULL,
  `FirstLevel_ModuloMenu` int(11) DEFAULT NULL,
  `Type_ModuloMenu` int(11) DEFAULT NULL,
  PRIMARY KEY (`Modulo_ModuloMenu`),
  KEY `Modulo_ModuloMenu` (`Modulo_ModuloMenu`),
  KEY `Modulo_ModuloMenu_2` (`Modulo_ModuloMenu`),
  CONSTRAINT `FK148050BBD1B0BCC2` FOREIGN KEY (`Modulo_ModuloMenu`) REFERENCES `homepages_moduli` (`id_Modulo`),
  CONSTRAINT `FK3D7FD8C23D707036` FOREIGN KEY (`Modulo_ModuloMenu`) REFERENCES `homepages_moduli` (`id_Modulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_moduli_menu`
--

LOCK TABLES `homepages_moduli_menu` WRITE;
/*!40000 ALTER TABLE `homepages_moduli_menu` DISABLE KEYS */;
INSERT INTO `homepages_moduli_menu` VALUES (6,1,0,0,0);
/*!40000 ALTER TABLE `homepages_moduli_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_moduli_notifiche`
--

DROP TABLE IF EXISTS `homepages_moduli_notifiche`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_moduli_notifiche` (
  `Modulo_ModuloNotifiche` int(11) NOT NULL,
  `ShowTitle_ModuloNotifiche` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Modulo_ModuloNotifiche`),
  KEY `Modulo_ModuloNotifiche` (`Modulo_ModuloNotifiche`),
  KEY `Modulo_ModuloNotifiche_2` (`Modulo_ModuloNotifiche`),
  CONSTRAINT `FK854EFF352D984E42` FOREIGN KEY (`Modulo_ModuloNotifiche`) REFERENCES `homepages_moduli` (`id_Modulo`),
  CONSTRAINT `FKE9ECBBCAF8BDCEE6` FOREIGN KEY (`Modulo_ModuloNotifiche`) REFERENCES `homepages_moduli` (`id_Modulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_moduli_notifiche`
--

LOCK TABLES `homepages_moduli_notifiche` WRITE;
/*!40000 ALTER TABLE `homepages_moduli_notifiche` DISABLE KEYS */;
/*!40000 ALTER TABLE `homepages_moduli_notifiche` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_moduli_ricerca_semantica`
--

DROP TABLE IF EXISTS `homepages_moduli_ricerca_semantica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_moduli_ricerca_semantica` (
  `Modulo_RicercaSemantica` int(11) NOT NULL,
  `ShowTitle_RicercaSemantica` tinyint(1) DEFAULT NULL,
  `MaxDocs_RicercaSemantica` int(11) DEFAULT NULL,
  PRIMARY KEY (`Modulo_RicercaSemantica`),
  KEY `Modulo_RicercaSemantica` (`Modulo_RicercaSemantica`),
  CONSTRAINT `FKE0AADA2EE084BE33` FOREIGN KEY (`Modulo_RicercaSemantica`) REFERENCES `homepages_moduli` (`id_Modulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_moduli_ricerca_semantica`
--

LOCK TABLES `homepages_moduli_ricerca_semantica` WRITE;
/*!40000 ALTER TABLE `homepages_moduli_ricerca_semantica` DISABLE KEYS */;
/*!40000 ALTER TABLE `homepages_moduli_ricerca_semantica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_moduli_rss`
--

DROP TABLE IF EXISTS `homepages_moduli_rss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_moduli_rss` (
  `Modulo_ModuloRss` int(11) NOT NULL,
  `Descrizione_ModuloRss` int(11) DEFAULT NULL,
  `DescLen_ModuloRss` int(11) DEFAULT NULL,
  `Records_ModuloRss` int(11) DEFAULT NULL,
  `Link_ModuloRss` text,
  PRIMARY KEY (`Modulo_ModuloRss`),
  KEY `Modulo_ModuloRss` (`Modulo_ModuloRss`),
  KEY `Modulo_ModuloRss_2` (`Modulo_ModuloRss`),
  CONSTRAINT `FK3FEEE46BFA9DD363` FOREIGN KEY (`Modulo_ModuloRss`) REFERENCES `homepages_moduli` (`id_Modulo`),
  CONSTRAINT `FKD1371BB44DA2EFE3` FOREIGN KEY (`Modulo_ModuloRss`) REFERENCES `homepages_moduli` (`id_Modulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_moduli_rss`
--

LOCK TABLES `homepages_moduli_rss` WRITE;
/*!40000 ALTER TABLE `homepages_moduli_rss` DISABLE KEYS */;
/*!40000 ALTER TABLE `homepages_moduli_rss` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_moduli_servizi`
--

DROP TABLE IF EXISTS `homepages_moduli_servizi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_moduli_servizi` (
  `Modulo_ModuloServizi` int(11) NOT NULL,
  PRIMARY KEY (`Modulo_ModuloServizi`),
  KEY `Modulo_ModuloServizi` (`Modulo_ModuloServizi`),
  KEY `Modulo_ModuloServizi_2` (`Modulo_ModuloServizi`),
  CONSTRAINT `FK2CA15C35A919BCB9` FOREIGN KEY (`Modulo_ModuloServizi`) REFERENCES `homepages_moduli` (`id_Modulo`),
  CONSTRAINT `FKDC15B4D1E515352F` FOREIGN KEY (`Modulo_ModuloServizi`) REFERENCES `homepages_moduli` (`id_Modulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_moduli_servizi`
--

LOCK TABLES `homepages_moduli_servizi` WRITE;
/*!40000 ALTER TABLE `homepages_moduli_servizi` DISABLE KEYS */;
/*!40000 ALTER TABLE `homepages_moduli_servizi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_moduli_statici`
--

DROP TABLE IF EXISTS `homepages_moduli_statici`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_moduli_statici` (
  `rif_modulo_modulo_statico` int(11) NOT NULL,
  `nome_modulo_statico` varchar(255) DEFAULT NULL,
  `titolo_modulo_statico` varchar(255) DEFAULT NULL,
  `testo_modulo_statico` mediumtext,
  `link_modulo_statico` varchar(255) DEFAULT NULL,
  `title_modulo_statico` varchar(255) DEFAULT NULL,
  `img_modulo_statico` varchar(255) DEFAULT NULL,
  `imgAlt_modulo_statico` varchar(255) DEFAULT NULL,
  `imgAll_modulo_statico` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rif_modulo_modulo_statico`),
  KEY `rif_modulo_modulo_statico` (`rif_modulo_modulo_statico`),
  KEY `rif_modulo_modulo_statico_2` (`rif_modulo_modulo_statico`),
  CONSTRAINT `FK78308826BB0A7D3B` FOREIGN KEY (`rif_modulo_modulo_statico`) REFERENCES `homepages_moduli` (`id_Modulo`),
  CONSTRAINT `FKFFB8F3C9B6E75AA0` FOREIGN KEY (`rif_modulo_modulo_statico`) REFERENCES `homepages_moduli` (`id_Modulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_moduli_statici`
--

LOCK TABLES `homepages_moduli_statici` WRITE;
/*!40000 ALTER TABLE `homepages_moduli_statici` DISABLE KEYS */;
/*!40000 ALTER TABLE `homepages_moduli_statici` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_moduli_statici_lang`
--

DROP TABLE IF EXISTS `homepages_moduli_statici_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_moduli_statici_lang` (
  `id_ModuloStatico_lang` int(11) NOT NULL AUTO_INCREMENT,
  `nome_modulo_statico` varchar(255) DEFAULT NULL,
  `titolo_modulo_statico` varchar(255) DEFAULT NULL,
  `testo_modulo_statico` mediumtext,
  `link_modulo_statico` varchar(255) DEFAULT NULL,
  `title_modulo_statico` varchar(255) DEFAULT NULL,
  `img_modulo_statico` varchar(255) DEFAULT NULL,
  `imgAlt_modulo_statico` varchar(255) DEFAULT NULL,
  `imgAll_modulo_statico` varchar(255) DEFAULT NULL,
  `modulo_statico_lang` int(11) DEFAULT NULL,
  `Modulo_ModuloLang` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ModuloStatico_lang`),
  KEY `Modulo_ModuloLang` (`Modulo_ModuloLang`),
  CONSTRAINT `lang_modulo` FOREIGN KEY (`Modulo_ModuloLang`) REFERENCES `homepages_moduli_statici` (`rif_modulo_modulo_statico`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_moduli_statici_lang`
--

LOCK TABLES `homepages_moduli_statici_lang` WRITE;
/*!40000 ALTER TABLE `homepages_moduli_statici_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `homepages_moduli_statici_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homepages_moduli_tags`
--

DROP TABLE IF EXISTS `homepages_moduli_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepages_moduli_tags` (
  `Modulo_ModuloTags` int(11) NOT NULL,
  `RifNetwork_ModuloTags` int(11) DEFAULT NULL,
  `Records_ModuloTags` int(11) DEFAULT NULL,
  PRIMARY KEY (`Modulo_ModuloTags`),
  KEY `Modulo_ModuloTags` (`Modulo_ModuloTags`),
  CONSTRAINT `FK190DECF0A70CC2A8` FOREIGN KEY (`Modulo_ModuloTags`) REFERENCES `homepages_moduli` (`id_Modulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homepages_moduli_tags`
--

LOCK TABLES `homepages_moduli_tags` WRITE;
/*!40000 ALTER TABLE `homepages_moduli_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `homepages_moduli_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import_groups`
--

DROP TABLE IF EXISTS `import_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_groups` (
  `GroupNewID` int(11) NOT NULL,
  `GroupLegacyID` int(11) NOT NULL,
  PRIMARY KEY (`GroupNewID`,`GroupLegacyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_groups`
--

LOCK TABLES `import_groups` WRITE;
/*!40000 ALTER TABLE `import_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import_users`
--

DROP TABLE IF EXISTS `import_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_users` (
  `UserNewID` int(11) NOT NULL,
  `UserLegacyID` int(11) NOT NULL,
  `isFrontend` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`UserNewID`,`UserLegacyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_users`
--

LOCK TABLES `import_users` WRITE;
/*!40000 ALTER TABLE `import_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lang`
--

DROP TABLE IF EXISTS `lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lang` (
  `id_Lang` int(11) NOT NULL AUTO_INCREMENT,
  `Nome_Lang` varchar(256) DEFAULT NULL,
  `Default_Lang` int(11) DEFAULT NULL,
  `CultureCode_Lang` varchar(20) DEFAULT NULL,
  `Parent_Lang` int(11) DEFAULT NULL,
  `Enabled` tinyint(1) DEFAULT NULL,
  `LangTwoLetterCode` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`id_Lang`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lang`
--

LOCK TABLES `lang` WRITE;
/*!40000 ALTER TABLE `lang` DISABLE KEYS */;
INSERT INTO `lang` VALUES (1,'Italiano',1,'it-IT',0,1,'it'),(2,'Inglese',0,'en-US',1,0,'en'),(3,'Spagnolo',0,'es-ES',1,0,'es'),(4,'Tedesco',0,'de-DE',1,0,'de'),(5,'Francese',0,'fr-FR',1,0,'fr'),(6,'Messicano',0,'es-MX',3,0,'mx');
/*!40000 ALTER TABLE `lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `links_docs`
--

DROP TABLE IF EXISTS `links_docs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `links_docs` (
  `id_docs` int(11) NOT NULL,
  PRIMARY KEY (`id_docs`),
  KEY `id_docs` (`id_docs`),
  KEY `id_docs_2` (`id_docs`),
  CONSTRAINT `FK21FAF10747E672DB` FOREIGN KEY (`id_docs`) REFERENCES `docs` (`id_Doc`),
  CONSTRAINT `FKFE8A92C3B0D99845` FOREIGN KEY (`id_docs`) REFERENCES `docs` (`id_Doc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `links_docs`
--

LOCK TABLES `links_docs` WRITE;
/*!40000 ALTER TABLE `links_docs` DISABLE KEYS */;
/*!40000 ALTER TABLE `links_docs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `links_docs_contents`
--

DROP TABLE IF EXISTS `links_docs_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `links_docs_contents` (
  `id_Link` int(11) NOT NULL,
  `Href_Link` mediumtext,
  `Clicks_Link` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Link`),
  KEY `id_Link` (`id_Link`),
  KEY `id_Link_2` (`id_Link`),
  CONSTRAINT `FK7A4AD722CDB838FC` FOREIGN KEY (`id_Link`) REFERENCES `docs_contents` (`id_DocContent`),
  CONSTRAINT `FKB861DF4EB4962EC2` FOREIGN KEY (`id_Link`) REFERENCES `docs_contents` (`id_DocContent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `links_docs_contents`
--

LOCK TABLES `links_docs_contents` WRITE;
/*!40000 ALTER TABLE `links_docs_contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `links_docs_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `links_folders`
--

DROP TABLE IF EXISTS `links_folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `links_folders` (
  `id_folders` int(11) NOT NULL,
  PRIMARY KEY (`id_folders`),
  KEY `id_folders` (`id_folders`),
  KEY `id_folders_2` (`id_folders`),
  CONSTRAINT `FK3C310A2D7CE62EC5` FOREIGN KEY (`id_folders`) REFERENCES `folders` (`id_Folder`),
  CONSTRAINT `FKDA2125C2C73A8FD` FOREIGN KEY (`id_folders`) REFERENCES `folders` (`id_Folder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `links_folders`
--

LOCK TABLES `links_folders` WRITE;
/*!40000 ALTER TABLE `links_folders` DISABLE KEYS */;
/*!40000 ALTER TABLE `links_folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggistica_canali`
--

DROP TABLE IF EXISTS `messaggistica_canali`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggistica_canali` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggistica_canali`
--

LOCK TABLES `messaggistica_canali` WRITE;
/*!40000 ALTER TABLE `messaggistica_canali` DISABLE KEYS */;
INSERT INTO `messaggistica_canali` VALUES (1,'Sms'),(2,'Email');
/*!40000 ALTER TABLE `messaggistica_canali` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggistica_email_inviate`
--

DROP TABLE IF EXISTS `messaggistica_email_inviate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggistica_email_inviate` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IndirizzoDestinatario` varchar(255) DEFAULT NULL,
  `IDspedizione_email` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IDspedizione_email` (`IDspedizione_email`),
  KEY `IDspedizione_email_2` (`IDspedizione_email`),
  CONSTRAINT `FK196A3D69ADA2FE8` FOREIGN KEY (`IDspedizione_email`) REFERENCES `messaggistica_spedizioni_email` (`IDspedizioni_email`),
  CONSTRAINT `FK892BBD66919693A` FOREIGN KEY (`IDspedizione_email`) REFERENCES `messaggistica_spedizioni_email` (`IDspedizioni_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggistica_email_inviate`
--

LOCK TABLES `messaggistica_email_inviate` WRITE;
/*!40000 ALTER TABLE `messaggistica_email_inviate` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggistica_email_inviate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggistica_provider`
--

DROP TABLE IF EXISTS `messaggistica_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggistica_provider` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(255) DEFAULT NULL,
  `IDcanale` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IDcanale` (`IDcanale`),
  KEY `IDcanale_2` (`IDcanale`),
  CONSTRAINT `FK6BFBBD6D4E543921` FOREIGN KEY (`IDcanale`) REFERENCES `messaggistica_canali` (`Id`),
  CONSTRAINT `FKC8888E1871F6DECB` FOREIGN KEY (`IDcanale`) REFERENCES `messaggistica_canali` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggistica_provider`
--

LOCK TABLES `messaggistica_provider` WRITE;
/*!40000 ALTER TABLE `messaggistica_provider` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggistica_provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggistica_provider_email`
--

DROP TABLE IF EXISTS `messaggistica_provider_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggistica_provider_email` (
  `IDprovider_email` int(11) NOT NULL,
  `SmtpHost` varchar(255) DEFAULT NULL,
  `SmtpPassword` varchar(255) DEFAULT NULL,
  `SmtpPort` int(11) DEFAULT NULL,
  `SmtpUser` varchar(255) DEFAULT NULL,
  `EnableSsl` tinyint(1) DEFAULT NULL,
  `LabelMittente` varchar(255) DEFAULT NULL,
  `EmailFrom` varchar(255) DEFAULT NULL,
  `IsDefault` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`IDprovider_email`),
  KEY `IDprovider_email` (`IDprovider_email`),
  KEY `IDprovider_email_2` (`IDprovider_email`),
  CONSTRAINT `FK125BA96E8583A821` FOREIGN KEY (`IDprovider_email`) REFERENCES `messaggistica_provider` (`Id`),
  CONSTRAINT `FKD7822BD9F038BEC4` FOREIGN KEY (`IDprovider_email`) REFERENCES `messaggistica_provider` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggistica_provider_email`
--

LOCK TABLES `messaggistica_provider_email` WRITE;
/*!40000 ALTER TABLE `messaggistica_provider_email` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggistica_provider_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggistica_provider_network`
--

DROP TABLE IF EXISTS `messaggistica_provider_network`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggistica_provider_network` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IdNetwork` int(11) DEFAULT NULL,
  `IDprovider` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDprovider` (`IDprovider`),
  KEY `IDprovider_2` (`IDprovider`),
  CONSTRAINT `FK1CC7F05A80AA5BB1` FOREIGN KEY (`IDprovider`) REFERENCES `messaggistica_provider` (`Id`),
  CONSTRAINT `FK97A8ECE7D3692651` FOREIGN KEY (`IDprovider`) REFERENCES `messaggistica_provider` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggistica_provider_network`
--

LOCK TABLES `messaggistica_provider_network` WRITE;
/*!40000 ALTER TABLE `messaggistica_provider_network` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggistica_provider_network` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggistica_provider_sms`
--

DROP TABLE IF EXISTS `messaggistica_provider_sms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggistica_provider_sms` (
  `IDprovider_sms` int(11) NOT NULL,
  `IsDefault` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`IDprovider_sms`),
  KEY `IDprovider_sms` (`IDprovider_sms`),
  KEY `IDprovider_sms_2` (`IDprovider_sms`),
  CONSTRAINT `FK1EF4932ABC3EA663` FOREIGN KEY (`IDprovider_sms`) REFERENCES `messaggistica_provider` (`Id`),
  CONSTRAINT `FK8CA38115360B388F` FOREIGN KEY (`IDprovider_sms`) REFERENCES `messaggistica_provider` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggistica_provider_sms`
--

LOCK TABLES `messaggistica_provider_sms` WRITE;
/*!40000 ALTER TABLE `messaggistica_provider_sms` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggistica_provider_sms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggistica_provider_sms_aruba`
--

DROP TABLE IF EXISTS `messaggistica_provider_sms_aruba`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggistica_provider_sms_aruba` (
  `IDprovider_sms_aruba` int(11) NOT NULL,
  `UserName` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Mittente` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDprovider_sms_aruba`),
  KEY `IDprovider_sms_aruba` (`IDprovider_sms_aruba`),
  KEY `IDprovider_sms_aruba_2` (`IDprovider_sms_aruba`),
  CONSTRAINT `FK4D35C4825CD74A0E` FOREIGN KEY (`IDprovider_sms_aruba`) REFERENCES `messaggistica_provider_sms` (`IDprovider_sms`),
  CONSTRAINT `FK9A4B7447E06ED2A1` FOREIGN KEY (`IDprovider_sms_aruba`) REFERENCES `messaggistica_provider_sms` (`IDprovider_sms`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggistica_provider_sms_aruba`
--

LOCK TABLES `messaggistica_provider_sms_aruba` WRITE;
/*!40000 ALTER TABLE `messaggistica_provider_sms_aruba` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggistica_provider_sms_aruba` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggistica_provider_sms_clickatell`
--

DROP TABLE IF EXISTS `messaggistica_provider_sms_clickatell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggistica_provider_sms_clickatell` (
  `IDprovider_sms_clickatell` int(11) NOT NULL,
  `ApiId` int(11) DEFAULT NULL,
  `UserName` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Mittente` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDprovider_sms_clickatell`),
  KEY `IDprovider_sms_clickatell` (`IDprovider_sms_clickatell`),
  KEY `IDprovider_sms_clickatell_2` (`IDprovider_sms_clickatell`),
  CONSTRAINT `FK9AF75821FEFBD309` FOREIGN KEY (`IDprovider_sms_clickatell`) REFERENCES `messaggistica_provider_sms` (`IDprovider_sms`),
  CONSTRAINT `FKBE68BC352EF69F89` FOREIGN KEY (`IDprovider_sms_clickatell`) REFERENCES `messaggistica_provider_sms` (`IDprovider_sms`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggistica_provider_sms_clickatell`
--

LOCK TABLES `messaggistica_provider_sms_clickatell` WRITE;
/*!40000 ALTER TABLE `messaggistica_provider_sms_clickatell` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggistica_provider_sms_clickatell` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggistica_provider_sms_smshosting`
--

DROP TABLE IF EXISTS `messaggistica_provider_sms_smshosting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggistica_provider_sms_smshosting` (
  `IDprovider_sms_smshosting` int(11) NOT NULL,
  `AUTH_KEY` varchar(255) DEFAULT NULL,
  `AUTH_SECRET` varchar(255) DEFAULT NULL,
  `Mittente` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDprovider_sms_smshosting`),
  KEY `IDprovider_sms_smshosting` (`IDprovider_sms_smshosting`),
  KEY `IDprovider_sms_smshosting_2` (`IDprovider_sms_smshosting`),
  CONSTRAINT `FKE8447AC56AC41171` FOREIGN KEY (`IDprovider_sms_smshosting`) REFERENCES `messaggistica_provider_sms` (`IDprovider_sms`),
  CONSTRAINT `FKF7FCF6FA41817706` FOREIGN KEY (`IDprovider_sms_smshosting`) REFERENCES `messaggistica_provider_sms` (`IDprovider_sms`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggistica_provider_sms_smshosting`
--

LOCK TABLES `messaggistica_provider_sms_smshosting` WRITE;
/*!40000 ALTER TABLE `messaggistica_provider_sms_smshosting` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggistica_provider_sms_smshosting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggistica_sms_inviati`
--

DROP TABLE IF EXISTS `messaggistica_sms_inviati`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggistica_sms_inviati` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NumeroDestinatario` varchar(255) DEFAULT NULL,
  `Stato` varchar(255) DEFAULT NULL,
  `IDspedizione_sms` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IDspedizione_sms` (`IDspedizione_sms`),
  KEY `IDspedizione_sms_2` (`IDspedizione_sms`),
  CONSTRAINT `FK4EE21795DA9E858E` FOREIGN KEY (`IDspedizione_sms`) REFERENCES `messaggistica_spedizioni_sms` (`IDspedizioni_sms`),
  CONSTRAINT `FK9F7B561B8D847240` FOREIGN KEY (`IDspedizione_sms`) REFERENCES `messaggistica_spedizioni_sms` (`IDspedizioni_sms`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggistica_sms_inviati`
--

LOCK TABLES `messaggistica_sms_inviati` WRITE;
/*!40000 ALTER TABLE `messaggistica_sms_inviati` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggistica_sms_inviati` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggistica_spedizioni`
--

DROP TABLE IF EXISTS `messaggistica_spedizioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggistica_spedizioni` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `DataOra` datetime DEFAULT NULL,
  `IdOperatore` int(11) DEFAULT NULL,
  `Testo` text,
  `IDprovider` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IDprovider` (`IDprovider`),
  KEY `IDprovider_2` (`IDprovider`),
  CONSTRAINT `FK9F831AAFD3692651` FOREIGN KEY (`IDprovider`) REFERENCES `messaggistica_provider` (`Id`),
  CONSTRAINT `FKF22C620880AA5BB1` FOREIGN KEY (`IDprovider`) REFERENCES `messaggistica_provider` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggistica_spedizioni`
--

LOCK TABLES `messaggistica_spedizioni` WRITE;
/*!40000 ALTER TABLE `messaggistica_spedizioni` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggistica_spedizioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggistica_spedizioni_email`
--

DROP TABLE IF EXISTS `messaggistica_spedizioni_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggistica_spedizioni_email` (
  `IDspedizioni_email` int(11) NOT NULL,
  `Oggetto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDspedizioni_email`),
  KEY `IDspedizioni_email` (`IDspedizioni_email`),
  KEY `IDspedizioni_email_2` (`IDspedizioni_email`),
  CONSTRAINT `FK8A3F44982C1A5EE` FOREIGN KEY (`IDspedizioni_email`) REFERENCES `messaggistica_spedizioni` (`Id`),
  CONSTRAINT `FKA0E9DF6E26312` FOREIGN KEY (`IDspedizioni_email`) REFERENCES `messaggistica_spedizioni` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggistica_spedizioni_email`
--

LOCK TABLES `messaggistica_spedizioni_email` WRITE;
/*!40000 ALTER TABLE `messaggistica_spedizioni_email` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggistica_spedizioni_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggistica_spedizioni_sms`
--

DROP TABLE IF EXISTS `messaggistica_spedizioni_sms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggistica_spedizioni_sms` (
  `IDspedizioni_sms` int(11) NOT NULL,
  PRIMARY KEY (`IDspedizioni_sms`),
  KEY `IDspedizioni_sms` (`IDspedizioni_sms`),
  KEY `IDspedizioni_sms_2` (`IDspedizioni_sms`),
  CONSTRAINT `FK783BD2BD141A4255` FOREIGN KEY (`IDspedizioni_sms`) REFERENCES `messaggistica_spedizioni` (`Id`),
  CONSTRAINT `FKB36A5B09DAB08227` FOREIGN KEY (`IDspedizioni_sms`) REFERENCES `messaggistica_spedizioni` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggistica_spedizioni_sms`
--

LOCK TABLES `messaggistica_spedizioni_sms` WRITE;
/*!40000 ALTER TABLE `messaggistica_spedizioni_sms` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggistica_spedizioni_sms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggistica_spedizioni_sms_aruba`
--

DROP TABLE IF EXISTS `messaggistica_spedizioni_sms_aruba`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggistica_spedizioni_sms_aruba` (
  `IDspedizioni_sms_aruba` int(11) NOT NULL,
  PRIMARY KEY (`IDspedizioni_sms_aruba`),
  KEY `IDspedizioni_sms_aruba` (`IDspedizioni_sms_aruba`),
  KEY `IDspedizioni_sms_aruba_2` (`IDspedizioni_sms_aruba`),
  CONSTRAINT `FK2FD8EF8B18EBC104` FOREIGN KEY (`IDspedizioni_sms_aruba`) REFERENCES `messaggistica_spedizioni_sms` (`IDspedizioni_sms`),
  CONSTRAINT `FKC97B8D30B08F30F7` FOREIGN KEY (`IDspedizioni_sms_aruba`) REFERENCES `messaggistica_spedizioni_sms` (`IDspedizioni_sms`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggistica_spedizioni_sms_aruba`
--

LOCK TABLES `messaggistica_spedizioni_sms_aruba` WRITE;
/*!40000 ALTER TABLE `messaggistica_spedizioni_sms_aruba` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggistica_spedizioni_sms_aruba` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggistica_spedizioni_sms_clickatell`
--

DROP TABLE IF EXISTS `messaggistica_spedizioni_sms_clickatell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggistica_spedizioni_sms_clickatell` (
  `IDspedizioni_sms_clickatell` int(11) NOT NULL,
  PRIMARY KEY (`IDspedizioni_sms_clickatell`),
  KEY `IDspedizioni_sms_clickatell` (`IDspedizioni_sms_clickatell`),
  KEY `IDspedizioni_sms_clickatell_2` (`IDspedizioni_sms_clickatell`),
  CONSTRAINT `FK6CE09D23ACBBDF88` FOREIGN KEY (`IDspedizioni_sms_clickatell`) REFERENCES `messaggistica_spedizioni_sms` (`IDspedizioni_sms`),
  CONSTRAINT `FK75C06EC8907C2FBF` FOREIGN KEY (`IDspedizioni_sms_clickatell`) REFERENCES `messaggistica_spedizioni_sms` (`IDspedizioni_sms`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggistica_spedizioni_sms_clickatell`
--

LOCK TABLES `messaggistica_spedizioni_sms_clickatell` WRITE;
/*!40000 ALTER TABLE `messaggistica_spedizioni_sms_clickatell` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggistica_spedizioni_sms_clickatell` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messaggistica_spedizioni_sms_smshosting`
--

DROP TABLE IF EXISTS `messaggistica_spedizioni_sms_smshosting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messaggistica_spedizioni_sms_smshosting` (
  `IDspedizioni_sms_smshosting` int(11) NOT NULL,
  PRIMARY KEY (`IDspedizioni_sms_smshosting`),
  KEY `IDspedizioni_sms_smshosting` (`IDspedizioni_sms_smshosting`),
  KEY `IDspedizioni_sms_smshosting_2` (`IDspedizioni_sms_smshosting`),
  CONSTRAINT `FK3FF671B3BB15A03C` FOREIGN KEY (`IDspedizioni_sms_smshosting`) REFERENCES `messaggistica_spedizioni_sms` (`IDspedizioni_sms`),
  CONSTRAINT `FKEC79E0B9F3BA6567` FOREIGN KEY (`IDspedizioni_sms_smshosting`) REFERENCES `messaggistica_spedizioni_sms` (`IDspedizioni_sms`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messaggistica_spedizioni_sms_smshosting`
--

LOCK TABLES `messaggistica_spedizioni_sms_smshosting` WRITE;
/*!40000 ALTER TABLE `messaggistica_spedizioni_sms_smshosting` DISABLE KEYS */;
/*!40000 ALTER TABLE `messaggistica_spedizioni_sms_smshosting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta`
--

DROP TABLE IF EXISTS `meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta` (
  `id_Meta` int(11) NOT NULL AUTO_INCREMENT,
  `Name_Meta` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_Meta`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta`
--

LOCK TABLES `meta` WRITE;
/*!40000 ALTER TABLE `meta` DISABLE KEYS */;
INSERT INTO `meta` VALUES (1,'KEYWORDS'),(2,'AUTHOR'),(3,'OWNER'),(4,'SUBJECT'),(5,'RATING'),(6,'DESCRIPTION'),(7,'ABSTRACT'),(9,'REVISIT-AFTER'),(10,'LANGUAGE'),(11,'COPYRIGHT'),(12,'ROBOTS');
/*!40000 ALTER TABLE `meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `network_docs`
--

DROP TABLE IF EXISTS `network_docs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `network_docs` (
  `id_NetworkDoc` int(11) NOT NULL AUTO_INCREMENT,
  `Folder_Doc` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_NetworkDoc`),
  KEY `Folder_Doc` (`Folder_Doc`),
  CONSTRAINT `network_docs_network_folders` FOREIGN KEY (`Folder_Doc`) REFERENCES `network_folders` (`id_NetworkFolder`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `network_docs`
--

LOCK TABLES `network_docs` WRITE;
/*!40000 ALTER TABLE `network_docs` DISABLE KEYS */;
INSERT INTO `network_docs` VALUES (1,1);
/*!40000 ALTER TABLE `network_docs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `network_folders`
--

DROP TABLE IF EXISTS `network_folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `network_folders` (
  `id_NetworkFolder` int(11) NOT NULL AUTO_INCREMENT,
  `Path_Folder` varchar(2000) DEFAULT NULL,
  `System_Folder` int(11) DEFAULT NULL,
  `Parent_Folder` int(11) DEFAULT NULL,
  `Network_Folder` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_NetworkFolder`),
  KEY `Parent_Folder` (`Parent_Folder`),
  KEY `Network_Folder` (`Network_Folder`),
  CONSTRAINT `folders_networks` FOREIGN KEY (`Network_Folder`) REFERENCES `networks` (`id_Network`),
  CONSTRAINT `network_folders_parent` FOREIGN KEY (`Parent_Folder`) REFERENCES `network_folders` (`id_NetworkFolder`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `network_folders`
--

LOCK TABLES `network_folders` WRITE;
/*!40000 ALTER TABLE `network_folders` DISABLE KEYS */;
INSERT INTO `network_folders` VALUES (1,'',0,NULL,1),(3,'/imghome',1,1,1);
/*!40000 ALTER TABLE `network_folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `networks`
--

DROP TABLE IF EXISTS `networks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `networks` (
  `id_Network` int(11) NOT NULL AUTO_INCREMENT,
  `Nome_Network` varchar(255) NOT NULL,
  `Default_Network` int(11) DEFAULT '0',
  `WebRoot_Network` varchar(255) DEFAULT NULL,
  `Frontend_Network` int(11) DEFAULT '1',
  `DefaultLang_Network` int(11) DEFAULT NULL,
  `Enabled_Network` int(11) DEFAULT '0',
  `Label_Network` varchar(512) DEFAULT NULL,
  `Cms_Network` int(11) DEFAULT '0' COMMENT '0: Cms NON Creato. 1: Ha il cms Creato, 2: Cms Creato ma disabilitato',
  PRIMARY KEY (`id_Network`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `networks`
--

LOCK TABLES `networks` WRITE;
/*!40000 ALTER TABLE `networks` DISABLE KEYS */;
INSERT INTO `networks` VALUES (1,'internet',1,'',1,1,1,'Sito Web',1);
/*!40000 ALTER TABLE `networks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_docs`
--

DROP TABLE IF EXISTS `news_docs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_docs` (
  `id_docs` int(11) NOT NULL,
  PRIMARY KEY (`id_docs`),
  KEY `id_docs` (`id_docs`),
  KEY `id_docs_2` (`id_docs`),
  CONSTRAINT `FK1972EC0347E672DB` FOREIGN KEY (`id_docs`) REFERENCES `docs` (`id_Doc`),
  CONSTRAINT `FKCD380FABB0D99845` FOREIGN KEY (`id_docs`) REFERENCES `docs` (`id_Doc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_docs`
--

LOCK TABLES `news_docs` WRITE;
/*!40000 ALTER TABLE `news_docs` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_docs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_docs_contents`
--

DROP TABLE IF EXISTS `news_docs_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_docs_contents` (
  `id_News` int(11) NOT NULL,
  `Testo_News` mediumtext,
  `Stato_News` varchar(255) NOT NULL DEFAULT 'Normale',
  `DataPubblicazione_News` datetime DEFAULT NULL,
  `DataScadenza_News` datetime DEFAULT NULL,
  `HighlightImages_News` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_News`),
  KEY `id_News` (`id_News`),
  KEY `HighlightImages_News` (`HighlightImages_News`),
  KEY `id_News_2` (`id_News`),
  KEY `HighlightImages_News_2` (`HighlightImages_News`),
  CONSTRAINT `FK266C72447F98D241` FOREIGN KEY (`id_News`) REFERENCES `docs_contents` (`id_DocContent`),
  CONSTRAINT `FK266C7244EB543EA6` FOREIGN KEY (`HighlightImages_News`) REFERENCES `news_highlightimages` (`ID`),
  CONSTRAINT `FK503937DB1171D475` FOREIGN KEY (`HighlightImages_News`) REFERENCES `news_highlightimages` (`ID`),
  CONSTRAINT `FK503937DBE4DA0176` FOREIGN KEY (`id_News`) REFERENCES `docs_contents` (`id_DocContent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_docs_contents`
--

LOCK TABLES `news_docs_contents` WRITE;
/*!40000 ALTER TABLE `news_docs_contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_docs_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_folders`
--

DROP TABLE IF EXISTS `news_folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_folders` (
  `id_folders` int(11) NOT NULL,
  `ShowChild` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_folders`),
  KEY `id_folders` (`id_folders`),
  KEY `id_folders_2` (`id_folders`),
  CONSTRAINT `FK21BEFE057CE62EC5` FOREIGN KEY (`id_folders`) REFERENCES `folders` (`id_Folder`),
  CONSTRAINT `FK256B8B02C73A8FD` FOREIGN KEY (`id_folders`) REFERENCES `folders` (`id_Folder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_folders`
--

LOCK TABLES `news_folders` WRITE;
/*!40000 ALTER TABLE `news_folders` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_highlightimages`
--

DROP TABLE IF EXISTS `news_highlightimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_highlightimages` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RifDoc` int(11) DEFAULT NULL,
  `Align` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `ShowInDetail` tinyint(1) DEFAULT NULL,
  `ShowInModulo` tinyint(1) DEFAULT NULL,
  `ShowInTeaser` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_highlightimages`
--

LOCK TABLES `news_highlightimages` WRITE;
/*!40000 ALTER TABLE `news_highlightimages` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_highlightimages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifiche`
--

DROP TABLE IF EXISTS `notifiche`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifiche` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Titolo` varchar(255) DEFAULT NULL,
  `Testo` varchar(255) DEFAULT NULL,
  `Letta` tinyint(1) DEFAULT NULL,
  `Link` varchar(255) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Destinatario` int(11) DEFAULT NULL,
  `Responsabile` int(11) DEFAULT NULL,
  `Network` int(11) DEFAULT NULL,
  `Cancellata` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Destinatario` (`Destinatario`),
  KEY `Responsabile` (`Responsabile`),
  KEY `Network` (`Network`),
  KEY `Network_2` (`Network`),
  KEY `Destinatario_2` (`Destinatario`),
  KEY `Responsabile_2` (`Responsabile`),
  CONSTRAINT `FK61FE62892AFE157A` FOREIGN KEY (`Network`) REFERENCES `networks` (`id_Network`),
  CONSTRAINT `FK61FE628981A31375` FOREIGN KEY (`Destinatario`) REFERENCES `users` (`id_User`),
  CONSTRAINT `FK61FE6289B90FC031` FOREIGN KEY (`Responsabile`) REFERENCES `users` (`id_User`),
  CONSTRAINT `FK68F9C0B582B3EF88` FOREIGN KEY (`Responsabile`) REFERENCES `users` (`id_User`),
  CONSTRAINT `FK68F9C0B5A5139D0E` FOREIGN KEY (`Destinatario`) REFERENCES `users` (`id_User`),
  CONSTRAINT `FK68F9C0B5CFE117A6` FOREIGN KEY (`Network`) REFERENCES `networks` (`id_Network`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifiche`
--

LOCK TABLES `notifiche` WRITE;
/*!40000 ALTER TABLE `notifiche` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifiche` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifiche_backoffice`
--

DROP TABLE IF EXISTS `notifiche_backoffice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifiche_backoffice` (
  `id_Notifica` int(11) NOT NULL,
  `NotifyBackofficeType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_Notifica`),
  KEY `id_Notifica` (`id_Notifica`),
  KEY `id_Notifica_2` (`id_Notifica`),
  CONSTRAINT `FK74AFB7FF57E50EA8` FOREIGN KEY (`id_Notifica`) REFERENCES `notifiche` (`ID`),
  CONSTRAINT `FKA7C2FC0529A95BAF` FOREIGN KEY (`id_Notifica`) REFERENCES `notifiche` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifiche_backoffice`
--

LOCK TABLES `notifiche_backoffice` WRITE;
/*!40000 ALTER TABLE `notifiche_backoffice` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifiche_backoffice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifiche_frontend`
--

DROP TABLE IF EXISTS `notifiche_frontend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifiche_frontend` (
  `id_Notifica` int(11) NOT NULL,
  PRIMARY KEY (`id_Notifica`),
  KEY `id_Notifica` (`id_Notifica`),
  KEY `id_Notifica_2` (`id_Notifica`),
  CONSTRAINT `FK8713F63C57E50EA8` FOREIGN KEY (`id_Notifica`) REFERENCES `notifiche` (`ID`),
  CONSTRAINT `FKC7AD4ECC29A95BAF` FOREIGN KEY (`id_Notifica`) REFERENCES `notifiche` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifiche_frontend`
--

LOCK TABLES `notifiche_frontend` WRITE;
/*!40000 ALTER TABLE `notifiche_frontend` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifiche_frontend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifiche_system`
--

DROP TABLE IF EXISTS `notifiche_system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifiche_system` (
  `id_Notifica` int(11) NOT NULL,
  PRIMARY KEY (`id_Notifica`),
  KEY `id_Notifica` (`id_Notifica`),
  CONSTRAINT `FK6B5F224B29A95BAF` FOREIGN KEY (`id_Notifica`) REFERENCES `notifiche` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifiche_system`
--

LOCK TABLES `notifiche_system` WRITE;
/*!40000 ALTER TABLE `notifiche_system` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifiche_system` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `object_profiles`
--

DROP TABLE IF EXISTS `object_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_profiles` (
  `id_ObjectProfile` int(11) NOT NULL AUTO_INCREMENT,
  `Profile_ObjectProfile` int(11) DEFAULT NULL,
  `Creator_ObjectProfile` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ObjectProfile`),
  KEY `Profile_ObjectProfile` (`Profile_ObjectProfile`),
  KEY `Creator_ObjectProfile` (`Creator_ObjectProfile`),
  CONSTRAINT `object_profiles_creator` FOREIGN KEY (`Creator_ObjectProfile`) REFERENCES `users` (`id_User`),
  CONSTRAINT `object_profiles_profile` FOREIGN KEY (`Profile_ObjectProfile`) REFERENCES `profiles` (`id_Profile`)
) ENGINE=InnoDB AUTO_INCREMENT=771 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `object_profiles`
--

LOCK TABLES `object_profiles` WRITE;
/*!40000 ALTER TABLE `object_profiles` DISABLE KEYS */;
INSERT INTO `object_profiles` VALUES (322,320,1),(331,2,1),(332,1,1),(680,317,1),(681,318,1),(682,320,1),(683,696,1),(684,697,1),(701,2,1),(702,2,1),(705,2,1),(716,2,1),(761,2,1),(762,2,1),(763,2,1),(764,2,1),(765,328,1),(766,329,1),(767,2,1),(769,2,1),(770,2,1);
/*!40000 ALTER TABLE `object_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `object_profiles_criteria`
--

DROP TABLE IF EXISTS `object_profiles_criteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_profiles_criteria` (
  `id_ObjectProfileCriteria` int(11) NOT NULL AUTO_INCREMENT,
  `Value_ObjectProfileCriteria` int(11) DEFAULT NULL,
  `Updater_ObjectProfileCriteria` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ObjectProfileCriteria`),
  KEY `Updater_ObjectProfileCriteria` (`Updater_ObjectProfileCriteria`),
  CONSTRAINT `object_profiles_criteria_updater` FOREIGN KEY (`Updater_ObjectProfileCriteria`) REFERENCES `users` (`id_User`)
) ENGINE=InnoDB AUTO_INCREMENT=20501 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `object_profiles_criteria`
--

LOCK TABLES `object_profiles_criteria` WRITE;
/*!40000 ALTER TABLE `object_profiles_criteria` DISABLE KEYS */;
INSERT INTO `object_profiles_criteria` VALUES (3836,1,1),(3837,1,1),(3844,1,1),(3845,1,1),(3846,1,1),(3847,1,1),(3848,1,1),(3849,1,1),(3850,1,1),(3851,1,1),(3852,1,1),(3853,1,1),(3854,1,1),(3855,1,1),(3856,1,1),(3857,1,1),(3858,1,1),(3859,1,1),(3860,1,1),(3867,1,1),(3868,1,1),(3869,1,1),(3870,1,1),(3871,2,1),(3872,1,1),(3873,1,1),(3874,1,1),(3875,2,1),(3876,1,1),(3877,1,1),(3878,2,1),(3879,1,1),(20396,1,1),(20397,1,1),(20398,1,1),(20399,1,1),(20400,1,1),(20401,1,1),(20402,1,1),(20403,1,1),(20404,1,1),(20405,1,1),(20406,1,1),(20407,1,1),(20408,1,1),(20409,1,1),(20410,1,1),(20442,1,1),(20443,1,1),(20475,1,1),(20476,1,1),(20477,1,1),(20478,1,1),(20479,1,1),(20480,1,1),(20481,1,1),(20482,1,1),(20483,1,1),(20484,1,1),(20485,1,1),(20486,1,1),(20487,1,1),(20488,1,1),(20489,1,1),(20490,1,1),(20491,1,1),(20492,1,1),(20495,1,1),(20496,1,1),(20497,1,1),(20498,1,1),(20499,1,1),(20500,1,1);
/*!40000 ALTER TABLE `object_profiles_criteria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id_Profile` int(11) NOT NULL AUTO_INCREMENT,
  `Name_Profile` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_Profile`)
) ENGINE=InnoDB AUTO_INCREMENT=784 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` VALUES (1,'Root'),(2,'Administrator');
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `province`
--

DROP TABLE IF EXISTS `province`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `province` (
  `IDProvincia` int(11) NOT NULL AUTO_INCREMENT,
  `codice_provincia` varchar(3) NOT NULL DEFAULT '0',
  `provincia` varchar(255) DEFAULT NULL,
  `sigla_provincia` varchar(2) DEFAULT NULL,
  `ID_regione` int(11) DEFAULT NULL,
  `attivo` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`IDProvincia`),
  UNIQUE KEY `codice_provincia` (`codice_provincia`),
  KEY `ID_regione` (`ID_regione`),
  KEY `ID_regione_2` (`ID_regione`),
  CONSTRAINT `provincia_regione` FOREIGN KEY (`ID_regione`) REFERENCES `regioni` (`ID_regione`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 PACK_KEYS=0 ROW_FORMAT=COMPACT COMMENT='InnoDB free: 4096 kB';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `province`
--

LOCK TABLES `province` WRITE;
/*!40000 ALTER TABLE `province` DISABLE KEYS */;
INSERT INTO `province` VALUES (1,'000','ESTERO','EE',6,1),(2,'001','Torino','TO',13,1),(3,'002','Vercelli','VC',13,1),(4,'003','Novara','NO',13,1),(5,'004','Cuneo','CN',13,1),(6,'005','Asti','AT',13,1),(7,'006','Alessandria','AL',13,1),(8,'007','Valle d\'Aosta/Vallée d\'Aoste','AO',20,1),(9,'008','Imperia','IM',9,1),(10,'009','Savona','SV',9,1),(11,'010','Genova','GE',9,1),(12,'011','La Spezia','SP',9,1),(13,'012','Varese','VA',10,1),(14,'013','Como','CO',10,1),(15,'014','Sondrio','SO',10,1),(16,'015','Milano','MI',10,1),(17,'016','Bergamo','BG',10,1),(18,'017','Brescia','BS',10,1),(19,'018','Pavia','PV',10,1),(20,'019','Cremona','CR',10,1),(21,'020','Mantova','MN',10,1),(22,'021','Bolzano/Bozen','BZ',18,1),(23,'022','Trento','TN',18,1),(24,'023','Verona','VR',21,1),(25,'024','Vicenza','VI',21,1),(26,'025','Belluno','BL',21,1),(27,'026','Treviso','TV',21,1),(28,'027','Venezia','VE',21,1),(29,'028','Padova','PD',21,1),(30,'029','Rovigo','RO',21,1),(31,'030','Udine','UD',7,1),(32,'031','Gorizia','GO',7,1),(33,'032','Trieste','TS',7,1),(34,'033','Piacenza','PC',5,1),(35,'034','Parma','PR',5,1),(36,'035','Reggio nell\'Emilia','RE',5,1),(37,'036','Modena','MO',5,1),(38,'037','Bologna','BO',5,1),(39,'038','Ferrara','FE',5,1),(40,'039','Ravenna','RA',5,1),(41,'040','Forlì-Cesena','FC',5,1),(42,'041','Pesaro e Urbino','PU',11,1),(43,'042','Ancona','AN',11,1),(44,'043','Macerata','MC',11,1),(45,'044','Ascoli Piceno','AP',11,1),(46,'045','Massa-Carrara','MS',17,1),(47,'046','Lucca','LU',17,1),(48,'047','Pistoia','PT',17,1),(49,'048','Firenze','FI',17,1),(50,'049','Livorno','LI',17,1),(51,'050','Pisa','PI',17,1),(52,'051','Arezzo','AR',17,1),(53,'052','Siena','SI',17,1),(54,'053','Grosseto','GR',17,1),(55,'054','Perugia','PG',19,1),(56,'055','Terni','TR',19,1),(57,'056','Viterbo','VT',8,1),(58,'057','Rieti','RI',8,1),(59,'058','Roma','RM',8,1),(60,'059','Latina','LT',8,1),(61,'060','Frosinone','FR',8,1),(62,'061','Caserta','CE',4,1),(63,'062','Benevento','BN',4,1),(64,'063','Napoli','NA',4,1),(65,'064','Avellino','AV',4,1),(66,'065','Salerno','SA',4,1),(67,'066','L\'Aquila','AQ',1,1),(68,'067','Teramo','TE',1,1),(69,'068','Pescara','PE',1,1),(70,'069','Chieti','CH',1,1),(71,'070','Campobasso','CB',12,1),(72,'071','Foggia','FG',14,1),(73,'072','Bari','BA',14,1),(74,'073','Taranto','TA',14,1),(75,'074','Brindisi','BR',14,1),(76,'075','Lecce','LE',14,1),(77,'076','Potenza','PZ',2,1),(78,'077','Matera','MT',2,1),(79,'078','Cosenza','CS',3,1),(80,'079','Catanzaro','CZ',3,1),(81,'080','Reggio di Calabria','RC',3,1),(82,'081','Trapani','TP',16,1),(83,'082','Palermo','PA',16,1),(84,'083','Messina','ME',16,1),(85,'084','Agrigento','AG',16,1),(86,'085','Caltanissetta','CL',16,1),(87,'086','Enna','EN',16,1),(88,'087','Catania','CT',16,1),(89,'088','Ragusa','RG',16,1),(90,'089','Siracusa','SR',16,1),(91,'090','Sassari','SS',15,1),(92,'091','Nuoro','NU',15,1),(93,'092','Cagliari','CA',15,1),(94,'093','Pordenone','PN',7,1),(95,'094','Isernia','IS',12,1),(96,'095','Oristano','OR',15,1),(97,'096','Biella','BI',13,1),(98,'097','Lecco','LC',10,1),(99,'098','Lodi','LO',10,1),(100,'099','Rimini','RN',5,1),(101,'100','Prato','PO',17,1),(102,'101','Crotone','KR',3,1),(103,'102','Vibo Valentia','VV',3,1),(104,'103','Verbano-Cusio-Ossola','VB',13,1),(105,'104','Olbia-Tempio','OT',15,1),(106,'105','Ogliastra','OG',15,1),(107,'106','Medio Campidano','VS',15,1),(108,'107','Carbonia-Iglesias','CI',15,1);
/*!40000 ALTER TABLE `province` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regioni`
--

DROP TABLE IF EXISTS `regioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regioni` (
  `ID_regione` int(11) NOT NULL AUTO_INCREMENT,
  `regione` varchar(255) DEFAULT NULL,
  `attivo` tinyint(1) NOT NULL DEFAULT '1',
  `ID` int(11) DEFAULT NULL,
  `Nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_regione`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regioni`
--

LOCK TABLES `regioni` WRITE;
/*!40000 ALTER TABLE `regioni` DISABLE KEYS */;
INSERT INTO `regioni` VALUES (1,'ABRUZZO',1,NULL,NULL),(2,'BASILICATA',1,NULL,NULL),(3,'CALABRIA',1,NULL,NULL),(4,'CAMPANIA',1,NULL,NULL),(5,'EMILIA-ROMAGNA',1,NULL,NULL),(6,'ESTERO',1,NULL,NULL),(7,'FRIULI-VENEZIA GIULIA',1,NULL,NULL),(8,'LAZIO',1,NULL,NULL),(9,'LIGURIA',1,NULL,NULL),(10,'LOMBARDIA',1,NULL,NULL),(11,'MARCHE',1,NULL,NULL),(12,'MOLISE',1,NULL,NULL),(13,'PIEMONTE',1,NULL,NULL),(14,'PUGLIA',1,NULL,NULL),(15,'SARDEGNA',1,NULL,NULL),(16,'SICILIA',1,NULL,NULL),(17,'TOSCANA',1,NULL,NULL),(18,'TRENTINO-ALTO ADIGE',1,NULL,NULL),(19,'UMBRIA',1,NULL,NULL),(20,'VALLE D\'AOSTA',1,NULL,NULL),(21,'VENETO',1,NULL,NULL);
/*!40000 ALTER TABLE `regioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revisioni`
--

DROP TABLE IF EXISTS `revisioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revisioni` (
  `id_Revisione` int(11) NOT NULL AUTO_INCREMENT,
  `Published_Revisione` int(11) DEFAULT '0',
  `Approvata_Revisione` int(11) DEFAULT '-1',
  `Contenuto_Revisione` int(11) DEFAULT NULL,
  `Autore_Revisione` int(11) DEFAULT NULL,
  `Data_Revisione` datetime DEFAULT NULL,
  `Origine_Revisione` int(11) DEFAULT '0',
  `DocLang_Revisione` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Revisione`),
  KEY `DocLang_Revisione` (`DocLang_Revisione`),
  KEY `Contenuto_Revisione` (`Contenuto_Revisione`),
  CONSTRAINT `revisioni_docs_contents` FOREIGN KEY (`Contenuto_Revisione`) REFERENCES `docs_contents` (`id_DocContent`),
  CONSTRAINT `revisioni_docs_lang` FOREIGN KEY (`DocLang_Revisione`) REFERENCES `docs_lang` (`id_DocLang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revisioni`
--

LOCK TABLES `revisioni` WRITE;
/*!40000 ALTER TABLE `revisioni` DISABLE KEYS */;
INSERT INTO `revisioni` VALUES (1,1,1,NULL,1,'2011-09-12 00:00:00',0,1);
/*!40000 ALTER TABLE `revisioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revisioni_notifiche`
--

DROP TABLE IF EXISTS `revisioni_notifiche`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revisioni_notifiche` (
  `id_RevisioneNotifica` int(11) NOT NULL AUTO_INCREMENT,
  `Doc_RevisioneNotifica` int(11) DEFAULT NULL,
  `Destinatario_RevisioneNotifica` int(11) DEFAULT NULL,
  `Mittente_RevisioneNotifica` int(11) DEFAULT NULL,
  `Revisione_RevisioneNotifica` int(11) DEFAULT NULL,
  `Log_RevisioneNotifica` int(11) DEFAULT NULL,
  `Stato_RevisioneNotifica` int(11) DEFAULT '0',
  PRIMARY KEY (`id_RevisioneNotifica`),
  KEY `Doc_RevisioneNotifica` (`Doc_RevisioneNotifica`),
  KEY `Destinatario_RevisioneNotifica` (`Destinatario_RevisioneNotifica`),
  KEY `Mittente_RevisioneNotifica` (`Mittente_RevisioneNotifica`),
  KEY `Revisione_RevisioneNotifica` (`Revisione_RevisioneNotifica`),
  KEY `Log_RevisioneNotifica` (`Log_RevisioneNotifica`),
  CONSTRAINT `revisioni_notifiche_docs` FOREIGN KEY (`Doc_RevisioneNotifica`) REFERENCES `docs` (`id_Doc`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `revisioni_notifiche_revisioni` FOREIGN KEY (`Revisione_RevisioneNotifica`) REFERENCES `revisioni` (`id_Revisione`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `revisioni_notifiche_revisionilog` FOREIGN KEY (`Log_RevisioneNotifica`) REFERENCES `revisionilog` (`id_RevLog`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `revisioni_notifiche_users_destinatario` FOREIGN KEY (`Destinatario_RevisioneNotifica`) REFERENCES `users` (`id_User`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `revisioni_notifiche_users_mittente` FOREIGN KEY (`Mittente_RevisioneNotifica`) REFERENCES `users` (`id_User`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revisioni_notifiche`
--

LOCK TABLES `revisioni_notifiche` WRITE;
/*!40000 ALTER TABLE `revisioni_notifiche` DISABLE KEYS */;
/*!40000 ALTER TABLE `revisioni_notifiche` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revisionilog`
--

DROP TABLE IF EXISTS `revisionilog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revisionilog` (
  `id_RevLog` int(11) NOT NULL AUTO_INCREMENT,
  `Revisione_RevLog` int(11) DEFAULT NULL,
  `Mittente_RevLog` int(11) DEFAULT NULL,
  `Livello_RevLog` int(11) DEFAULT NULL,
  `Oggetto_RevLog` int(11) DEFAULT '0' COMMENT 'Valori: 0 = Richiede, 1 = Approvata, -1 = Annullata dal richiedente, 2 = Bocciata',
  `Testo_RevLog` varchar(2000) DEFAULT NULL,
  `Data_RevLog` datetime DEFAULT NULL,
  `Destinatario_RevLog` int(11) DEFAULT '0',
  PRIMARY KEY (`id_RevLog`),
  KEY `Revisione_RevLog` (`Revisione_RevLog`),
  CONSTRAINT `revisionilog_revisioni` FOREIGN KEY (`Revisione_RevLog`) REFERENCES `revisioni` (`id_Revisione`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revisionilog`
--

LOCK TABLES `revisionilog` WRITE;
/*!40000 ALTER TABLE `revisionilog` DISABLE KEYS */;
/*!40000 ALTER TABLE `revisionilog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scheduler_datamaps`
--

DROP TABLE IF EXISTS `scheduler_datamaps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scheduler_datamaps` (
  `IdJob` int(11) NOT NULL,
  `Data` text,
  `Chiave` varchar(255) NOT NULL,
  PRIMARY KEY (`IdJob`,`Chiave`),
  KEY `IdJob` (`IdJob`),
  KEY `IdJob_2` (`IdJob`),
  CONSTRAINT `FK9E582744717A3666` FOREIGN KEY (`IdJob`) REFERENCES `scheduler_jobs` (`ID`),
  CONSTRAINT `FKDE3E9EDF14F63EB1` FOREIGN KEY (`IdJob`) REFERENCES `scheduler_jobs` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scheduler_datamaps`
--

LOCK TABLES `scheduler_datamaps` WRITE;
/*!40000 ALTER TABLE `scheduler_datamaps` DISABLE KEYS */;
/*!40000 ALTER TABLE `scheduler_datamaps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scheduler_jobs`
--

DROP TABLE IF EXISTS `scheduler_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scheduler_jobs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(255) DEFAULT NULL,
  `Descrizione` varchar(255) DEFAULT NULL,
  `JobType` varchar(255) DEFAULT NULL,
  `Creation` datetime DEFAULT NULL,
  `Gruppo` varchar(255) DEFAULT NULL,
  `ScheduleAt` datetime DEFAULT NULL,
  `ExecuteNow` tinyint(1) DEFAULT NULL,
  `StatoCorrente` varchar(255) DEFAULT NULL,
  `StatoPrecedente` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scheduler_jobs`
--

LOCK TABLES `scheduler_jobs` WRITE;
/*!40000 ALTER TABLE `scheduler_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `scheduler_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scheduler_jobs_stati`
--

DROP TABLE IF EXISTS `scheduler_jobs_stati`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scheduler_jobs_stati` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Data` datetime NOT NULL,
  `Stato` varchar(255) DEFAULT NULL,
  `IdJob` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IdJob` (`IdJob`),
  KEY `IdJob_2` (`IdJob`),
  CONSTRAINT `FK13D6736A14F63EB1` FOREIGN KEY (`IdJob`) REFERENCES `scheduler_jobs` (`ID`),
  CONSTRAINT `FK741EF023717A3666` FOREIGN KEY (`IdJob`) REFERENCES `scheduler_jobs` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scheduler_jobs_stati`
--

LOCK TABLES `scheduler_jobs_stati` WRITE;
/*!40000 ALTER TABLE `scheduler_jobs_stati` DISABLE KEYS */;
/*!40000 ALTER TABLE `scheduler_jobs_stati` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scheduler_triggerinfo`
--

DROP TABLE IF EXISTS `scheduler_triggerinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scheduler_triggerinfo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Hours` int(11) DEFAULT NULL,
  `Minutes` int(11) DEFAULT NULL,
  `Seconds` int(11) DEFAULT NULL,
  `StartTime` datetime DEFAULT NULL,
  `EndTime` datetime DEFAULT NULL,
  `ExecuteNow` tinyint(1) DEFAULT NULL,
  `RepeatForEver` tinyint(1) DEFAULT NULL,
  `TypeOfTrigger` varchar(255) DEFAULT NULL,
  `RifJob` int(11) DEFAULT NULL,
  `Gruppo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RifJob` (`RifJob`),
  KEY `RifJob_2` (`RifJob`),
  CONSTRAINT `FK64871D51B15BE88` FOREIGN KEY (`RifJob`) REFERENCES `scheduler_jobs` (`ID`),
  CONSTRAINT `FKCB8278AC5349B189` FOREIGN KEY (`RifJob`) REFERENCES `scheduler_jobs` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scheduler_triggerinfo`
--

LOCK TABLES `scheduler_triggerinfo` WRITE;
/*!40000 ALTER TABLE `scheduler_triggerinfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `scheduler_triggerinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stati_cittadinanza`
--

DROP TABLE IF EXISTS `stati_cittadinanza`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stati_cittadinanza` (
  `IDStato` int(11) NOT NULL AUTO_INCREMENT,
  `stato` varchar(255) DEFAULT NULL,
  `membro_cee` tinyint(1) DEFAULT NULL,
  `cittadinanza` varchar(255) DEFAULT NULL,
  `cod_stato` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`IDStato`)
) ENGINE=InnoDB AUTO_INCREMENT=196 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stati_cittadinanza`
--

LOCK TABLES `stati_cittadinanza` WRITE;
/*!40000 ALTER TABLE `stati_cittadinanza` DISABLE KEYS */;
INSERT INTO `stati_cittadinanza` VALUES (1,'AFGHANISTAN',0,'AFGHANA','301'),(2,'ALBANIA',0,'ALBANESE','201'),(3,'ALGERIA',0,'ALGERINA','401'),(4,'ANDORRA',0,'ANDORRANA','202'),(5,'ANGOLA',0,'ANGOLANA','402'),(6,'ANTIGUA E BARBUDA',0,'ANTIGUA E BARBUDA','503'),(7,'APOLIDE',0,'APOLIDE','999'),(8,'ARABIA SAUDITA',0,'SAUDITA','302'),(9,'ARGENTINA',0,'ARGENTINA','602'),(10,'ARMENIA',0,'ARMENA','358'),(11,'AUSTRALIA',0,'AUSTRALIANA','701'),(12,'AUSTRIA',1,'AUSTRIACA','203'),(13,'AZERBAIGIAN',0,'AZERBAIGIAN','359'),(14,'BAHAMAS',0,'BAHAMAS','505'),(15,'BAHREIN',0,'BAHREIN','304'),(16,'BANGLADESH',0,'BANGLADESH','305'),(17,'BARBADOS',0,'BARBADOS','506'),(18,'BELGIO',1,'BELGA','206'),(19,'BELIZE',0,'BELIZE','507'),(20,'BENIN',0,'BENIN','406'),(21,'BHUTAN',0,'BHUTANESE','306'),(22,'BIELORUSSIA',0,'BIELORUSSA','256'),(23,'BOLIVIA',0,'BOLIVIANA','604'),(24,'BOSNIA-ERZEGOVINA',0,'BOSNIACA','252'),(25,'BOTSWANA',0,'BOTSWANA','408'),(26,'BRASILE',0,'BRASILIANA','605'),(27,'BRUNEI',0,'BRUNEI','309'),(28,'BULGARIA',1,'BULGARA','209'),(29,'BURKINA FASO',0,'BURKINA FASO','409'),(30,'BURUNDI',0,'BURUNDI','410'),(31,'CAMBOGIA',0,'CAMBOGIANA','310'),(32,'CAMERUN',0,'CAMERUNENSE','411'),(33,'CANADA',0,'CANADESE','509'),(34,'CAPO VERDE',0,'CAPOVERDIANA','413'),(35,'CECA, Repubblica',1,'CECA','257'),(36,'Centrafricana, Repubblica',0,'Centrafricana, Repubblica','414'),(37,'CIAD',0,'CIAD','415'),(38,'CILE',0,'CILENA','606'),(39,'Cinese, Repubblica Popolare',0,'CINESE','314'),(40,'CIPRO',1,'CIPRIOTA','315'),(41,'COLOMBIA',0,'COLOMBIANA','608'),(42,'COMORE',0,'COMORE','417'),(43,'CONGO',0,'CONGOLESE','418'),(44,'Congo, Rep.Democratica',0,'Congo, Rep.Democratica','463'),(45,'Corea, Repubblica',0,'SUDCOREANA','320'),(46,'Corea, Repubblica Popolare',0,'NORDCOREANA','319'),(47,'COSTA D\'AVORIO',0,'IVORIANA','404'),(48,'COSTA RICA',0,'COSTATICANA','513'),(49,'CROAZIA',0,'CROATA','250'),(50,'CUBA',0,'CUBANA','514'),(51,'DANIMARCA',1,'DANESE','212'),(52,'DOMINICA',0,'DOMINICA','515'),(53,'Dominicana, Repubblica',0,'Dominicana, Repubblica','516'),(54,'ECUADOR',0,'ECUADOREGNA','609'),(55,'EGITTO',0,'EGIZIANA','419'),(56,'EL SALVADOR',0,'SALVADOREGNA','517'),(57,'EMIRATI ARABI UNITI',0,'EMIRATI ARABI UNITI','322'),(58,'ERITREA',0,'ERITREA','466'),(59,'ESTONIA',1,'ESTONE','247'),(60,'ETIOPIA',0,'ETIOPE','420'),(61,'FIGI',0,'FIGI','703'),(62,'FILIPPINE',0,'FILIPPINA','323'),(63,'FINLANDIA',1,'FINLANDESE','214'),(64,'FRANCIA',1,'FRANCESE','215'),(65,'GABON',0,'GABON','421'),(66,'GAMBIA',0,'GAMBIA','422'),(67,'GEORGIA',0,'GEORGIANA','360'),(68,'GERMANIA',1,'TEDESCA','216'),(69,'GHANA',0,'GHANESE','423'),(70,'GIAMAICA',0,'GIAMAICANA','518'),(71,'GIAPPONE',0,'GIAPPONESE','326'),(72,'GIBUTI',0,'GIBUTI','424'),(73,'GIORDANIA',0,'GIORDANA','327'),(74,'GRECIA',1,'GRECA','220'),(75,'GRENADA',0,'GRENADA','519'),(76,'GUATEMALA',0,'GUATEMALTECA','523'),(77,'GUINEA',0,'GUINEA','425'),(78,'GUINEA BISSAU',0,'GUINEA BISSAU','426'),(79,'GUINEA EQUATORIALE',0,'GUINEA EQUATORIALE','427'),(80,'GUYANA',0,'GUYANA','612'),(81,'HAITI',0,'HAITIANA','524'),(82,'HONDURAS',0,'HONDUREGNA','525'),(83,'INDIA',0,'INDIANA','330'),(84,'INDONESIA',0,'INDONESIANA','331'),(85,'IRAN',0,'IRANIANA','332'),(86,'IRAQ',0,'IRACHENA','333'),(87,'IRLANDA',1,'IRLANDESE','221'),(88,'ISLANDA',0,'ISLANDESE','223'),(89,'ISRAELE',0,'ISRAELIANA','334'),(90,'ITALIA',1,'ITALIANA','ITA'),(91,'KAZAKHSTAN',0,'KAZAKA','356'),(92,'KENYA',0,'KENIANA','428'),(93,'KIRGHIZISTAN',0,'KIRGISA','361'),(94,'KIRIBATI',0,'KIRIBATI','708'),(95,'KUWAIT',0,'KUATIANA','335'),(96,'LAOS',0,'LAOTIANA','336'),(97,'LESOTHO',0,'LESOTHO','429'),(98,'LETTONIA',1,'LETTONE','248'),(99,'LIBANO',0,'LIBANESE','337'),(100,'LIBERIA',0,'LIBERIANA','430'),(101,'LIBIA',0,'LIBICA','431'),(102,'LIECHTENSTEIN',0,'LIECHTENSTEIN','225'),(103,'LITUANIA',1,'LITUANA','249'),(104,'LUSSEMBURGO',1,'LUSSEMBURGHESE','226'),(105,'MACEDONIA, ex REP. JUGOSLAVIA',0,'MACEDONE','253'),(106,'MADAGASCAR',0,'MADAGASCAR','432'),(107,'MALAWI',0,'MALAWI','434'),(108,'MALAYSIA',0,'MALESIANA','340'),(109,'MALDIVE',0,'MALDIVE','339'),(110,'MALI',0,'MALI','435'),(111,'MALTA',1,'MALTESE','227'),(112,'MAROCCO',0,'MAROCCHINA','436'),(113,'Marshall, Isole',0,'Marshall, Isole','712'),(114,'MAURITANIA',0,'MAURITANIA','437'),(115,'Mauritius',0,'Mauritius','438'),(116,'MESSICO',0,'MESSICANA','527'),(117,'Micronesia, Stati Federati',0,'Micronesia, Stati Federati','713'),(118,'Moldova',0,'Moldova','254'),(119,'MONACO',0,'MONEGASCA','229'),(120,'MONGOLIA',0,'MONGOLA','341'),(121,'MOZAMBICO',0,'MOZAMBICO','440'),(122,'MYANMAR',0,'BIRMANA','307'),(123,'NAMIBIA',0,'NAMIBIA','441'),(124,'NAURU',0,'NAURU','715'),(125,'NEPAL',0,'NEPALESE','342'),(126,'NICARAGUA',0,'NICARACUENSE','529'),(127,'NIGER',0,'NIGER','442'),(128,'NIGERIA',0,'NIGERIANA','443'),(129,'NORVEGIA',0,'NORVEGESE','231'),(130,'NUOVA ZELANDA',0,'NEOZELANDESE','719'),(131,'OMAN',0,'OMAN','343'),(132,'PAESI BASSI',1,'OLANDESE','232'),(133,'PAKISTAN',0,'PACHISTANA','344'),(134,'PALAU',0,'PALAU','720'),(135,'PANAMA',0,'PANAMENSE','530'),(136,'PAPUA NUOVA GUINEA',0,'PAPUA NUOVA GUINEA','721'),(137,'PARAGUAY',0,'PARAGUAIANA','614'),(138,'PERU\'',0,'PERUVIANA','615'),(139,'POLONIA',1,'POLACCA','233'),(140,'PORTOGALLO',1,'PORTOGHESE','234'),(141,'QATAR',0,'QATAR','345'),(142,'REGNO UNITO',1,'BRITANNICA','219'),(143,'ROMANIA',1,'ROMENA','235'),(144,'RUANDA',0,'RUANDA','446'),(145,'RUSSA, Federazione',0,'RUSSA','245'),(146,'Saint Kitts e Nevis',0,'Saint Kitts e Nevis','534'),(147,'SAINT LUCIA',0,'SAINT LUCIA','532'),(148,'Saint Vincent e Grenadin',0,'Saint Vincent e Grenadin','533'),(149,'Salomone, Isole',0,'Salomone, Isole','725'),(150,'SAMOA',0,'SAMOA','727'),(151,'SAN MARINO',0,'SANMARINESE','236'),(152,'SANTA SEDE',0,'SANTA SEDE','246'),(153,'Sao Tomè e Principe',0,'Sao Tomè e Principe','448'),(154,'SENEGAL',0,'SENEGALESE','450'),(155,'SERBIA E MONTENEGRO',0,'SERBA / MONTENEGRINA','224'),(156,'Seycelles',0,'Seycelles','449'),(157,'SIERRA LEONE',0,'SIERRA LEONE','451'),(158,'SINGAPORE',0,'SINGAPORE','346'),(159,'SIRIA',0,'SIRIANA','348'),(160,'SLOVACCHIA',1,'SLOVACCA','255'),(161,'SLOVENIA',1,'SLOVENA','251'),(162,'SOMALIA',0,'SOMALA','453'),(163,'SPAGNA',1,'SPAGNOLA','239'),(164,'SRI LANKA',0,'CINGALESE','311'),(165,'STATI UNITI D\'AMERICA',0,'STATUNITENSE','536'),(166,'SUD AFRICA',0,'SUDAFRICANA','454'),(167,'SUDAN',0,'SUDANESE','455'),(168,'SURINAME',0,'SURINAME','616'),(169,'SVEZIA',1,'SVEDESE','240'),(170,'SVIZZERA',0,'SVIZZERA','241'),(171,'SWAZILAND',0,'SWAZILAND','456'),(172,'TAGIKISTAN',0,'TAGIKA','362'),(173,'TAIWAN',0,'TAIWAN','363'),(174,'TANZANIA',0,'TANZANIANA','457'),(175,'Territori Autonomia Palestinese',0,'PALESTINESE','324'),(176,'THAILANDIA',0,'TAILANDESE','349'),(177,'Timor Orientale',0,'Timor Orientale','338'),(178,'TOGO',0,'TOGOLESE','458'),(179,'TONGA',0,'TONGA','730'),(180,'TRINIDAD E TOBAGO',0,'TRINIDAD E TOBAGO','617'),(181,'TUNISIA',0,'TUNISINA','460'),(182,'TURCHIA',0,'TURCA','351'),(183,'TURKMENISTAN',0,'TURKMENA','364'),(184,'TUVALU',0,'TUVALU','731'),(185,'UCRAINA',0,'UCRAINA','243'),(186,'UGANDA',0,'UGANDESE','461'),(187,'UNGHERIA',1,'UNGHERESE','244'),(188,'URUGUAY',0,'URUGUAIANA','618'),(189,'UZBEKISTAN',0,'UZBEKA','357'),(190,'VANUATU',0,'VANUATU','732'),(191,'VENEZUELA',0,'VENZUELANA','619'),(192,'VIETNAM',0,'VIETNAMITA','353'),(193,'YEMEN',0,'YEMENITA','354'),(194,'ZAMBIA',0,'ZAMBESE','464'),(195,'ZIMBABWE',0,'ZIMBABWE','465');
/*!40000 ALTER TABLE `stati_cittadinanza` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id_Tag` int(11) NOT NULL AUTO_INCREMENT,
  `Label_Tag` varchar(255) DEFAULT NULL,
  `Key_Tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_Tag`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uffici`
--

DROP TABLE IF EXISTS `uffici`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uffici` (
  `id_Ufficio` int(11) NOT NULL AUTO_INCREMENT,
  `Nome_Ufficio` varchar(255) DEFAULT NULL,
  `Email_Ufficio` varchar(255) DEFAULT NULL,
  `Gruppo_Ufficio` int(11) DEFAULT NULL,
  `Eliminato_Ufficio` int(11) DEFAULT '0',
  `Network_Ufficio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Ufficio`),
  KEY `Gruppo_Ufficio` (`Gruppo_Ufficio`),
  CONSTRAINT `uffici_fk` FOREIGN KEY (`Gruppo_Ufficio`) REFERENCES `groups` (`id_Group`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uffici`
--

LOCK TABLES `uffici` WRITE;
/*!40000 ALTER TABLE `uffici` DISABLE KEYS */;
/*!40000 ALTER TABLE `uffici` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uffici_competenze`
--

DROP TABLE IF EXISTS `uffici_competenze`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uffici_competenze` (
  `id_Competenza` int(11) NOT NULL AUTO_INCREMENT,
  `Ufficio_Competenza` int(11) DEFAULT NULL,
  `Applicazione_Competenza` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Competenza`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uffici_competenze`
--

LOCK TABLES `uffici_competenze` WRITE;
/*!40000 ALTER TABLE `uffici_competenze` DISABLE KEYS */;
/*!40000 ALTER TABLE `uffici_competenze` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uffici_membri`
--

DROP TABLE IF EXISTS `uffici_membri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uffici_membri` (
  `id_Member` int(11) NOT NULL AUTO_INCREMENT,
  `Profile_Member` int(11) DEFAULT NULL,
  `Office_Member` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Member`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uffici_membri`
--

LOCK TABLES `uffici_membri` WRITE;
/*!40000 ALTER TABLE `uffici_membri` DISABLE KEYS */;
/*!40000 ALTER TABLE `uffici_membri` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_externalappzservice`
--

DROP TABLE IF EXISTS `user_externalappzservice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_externalappzservice` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DataAttivazioneServizio` datetime DEFAULT NULL,
  `ScadenzaServizio` datetime DEFAULT NULL,
  `User` int(11) DEFAULT NULL,
  `VerticalApplication` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `User` (`User`),
  KEY `VerticalApplication` (`VerticalApplication`),
  KEY `User_2` (`User`),
  KEY `VerticalApplication_2` (`VerticalApplication`),
  CONSTRAINT `FKAB644C115471F0CF` FOREIGN KEY (`VerticalApplication`) REFERENCES `externalappz` (`id_ExternalApp`),
  CONSTRAINT `FKAB644C11869FC2AD` FOREIGN KEY (`User`) REFERENCES `users` (`id_User`),
  CONSTRAINT `FKF9FAD672A3484F9E` FOREIGN KEY (`User`) REFERENCES `users` (`id_User`),
  CONSTRAINT `FKF9FAD672D4659BC0` FOREIGN KEY (`VerticalApplication`) REFERENCES `externalappz` (`id_ExternalApp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_externalappzservice`
--

LOCK TABLES `user_externalappzservice` WRITE;
/*!40000 ALTER TABLE `user_externalappzservice` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_externalappzservice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_tokens`
--

DROP TABLE IF EXISTS `user_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_tokens` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) DEFAULT NULL,
  `LoginProvider` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Value` mediumtext,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_tokens`
--

LOCK TABLES `user_tokens` WRITE;
/*!40000 ALTER TABLE `user_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id_User` int(11) NOT NULL AUTO_INCREMENT,
  `PreviewType_User` int(11) DEFAULT '0',
  `Name_User` varchar(255) DEFAULT NULL,
  `Anagrafica_User` int(11) DEFAULT NULL,
  `Profile_User` int(11) DEFAULT NULL,
  `Password_User` varchar(255) DEFAULT NULL,
  `State_User` int(11) DEFAULT '2',
  `Deleted_User` int(11) DEFAULT '0',
  `FoldersGUI_User` int(11) DEFAULT '0',
  `Editor_User` int(11) DEFAULT '0',
  `AnagraficaType_User` varchar(1024) DEFAULT '0',
  `Key_User` varchar(255) DEFAULT NULL,
  `NewPassword_User` varchar(255) DEFAULT NULL,
  `CreationTime_User` datetime DEFAULT NULL,
  `LastAccess_User` datetime DEFAULT NULL,
  `LastUpdate_User` datetime DEFAULT NULL,
  `God_User` int(11) DEFAULT '0',
  `Debug_User` int(11) DEFAULT '0',
  `Type_User` int(11) DEFAULT '0',
  `Registrato` tinyint(1) DEFAULT '0',
  `CurrentNetworkPath` varchar(255) DEFAULT NULL,
  `OldID_User` int(11) DEFAULT '-1',
  PRIMARY KEY (`id_User`),
  KEY `Anagrafica_User` (`Anagrafica_User`),
  KEY `Profile_User` (`Profile_User`),
  KEY `Anagrafica_User_2` (`Anagrafica_User`),
  KEY `Anagrafica_User_3` (`Anagrafica_User`),
  CONSTRAINT `users_anagrafica` FOREIGN KEY (`Anagrafica_User`) REFERENCES `anagrafica` (`ID`),
  CONSTRAINT `users_profile` FOREIGN KEY (`Profile_User`) REFERENCES `users_profile` (`id_Profile`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Administrator',1,2,'a4f6f309f1c6906f15c9abfbdfc22bd7',1,0,1,0,'AnagraficaBackoffice',NULL,NULL,'2022-09-17 16:48:46','2021-03-01 13:16:50','2021-03-01 11:10:24',0,0,0,1,NULL,-1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_activities`
--

DROP TABLE IF EXISTS `users_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_activities` (
  `id_Activity` int(11) NOT NULL AUTO_INCREMENT,
  `Title_Activity` varchar(1024) DEFAULT NULL,
  `Text_Activity` longtext,
  `Type_Activity` int(11) DEFAULT NULL,
  `Status_Activity` int(11) DEFAULT NULL,
  `Subject_Activity` varchar(20) DEFAULT NULL,
  `Profile_Activity` int(11) DEFAULT NULL,
  `Network_Activity` int(11) DEFAULT NULL,
  `Date_Activity` datetime DEFAULT NULL,
  `DoneDate_Activity` datetime DEFAULT NULL,
  `SenderProfile_Activity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Activity`),
  KEY `Network_Activity` (`Network_Activity`),
  KEY `Type_Activity` (`Type_Activity`),
  CONSTRAINT `users_activities_fk` FOREIGN KEY (`Network_Activity`) REFERENCES `networks` (`id_Network`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_activities_fk1` FOREIGN KEY (`Type_Activity`) REFERENCES `users_activities_types` (`id_ActivityType`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_activities`
--

LOCK TABLES `users_activities` WRITE;
/*!40000 ALTER TABLE `users_activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_activities_types`
--

DROP TABLE IF EXISTS `users_activities_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_activities_types` (
  `id_ActivityType` int(11) NOT NULL AUTO_INCREMENT,
  `Label_ActivityType` varchar(50) DEFAULT NULL,
  `DetailUrl_ActivityType` text,
  PRIMARY KEY (`id_ActivityType`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_activities_types`
--

LOCK TABLES `users_activities_types` WRITE;
/*!40000 ALTER TABLE `users_activities_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_activities_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_networks`
--

DROP TABLE IF EXISTS `users_networks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_networks` (
  `id_UserNetwork` int(11) NOT NULL AUTO_INCREMENT,
  `User_UserNetwork` int(11) DEFAULT NULL,
  `Network_UserNetwork` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_UserNetwork`),
  KEY `User_UserNetwork` (`User_UserNetwork`),
  KEY `Network_UserNetwork` (`Network_UserNetwork`),
  CONSTRAINT `network_usersnetworks` FOREIGN KEY (`Network_UserNetwork`) REFERENCES `networks` (`id_Network`),
  CONSTRAINT `user_usersnetworks` FOREIGN KEY (`User_UserNetwork`) REFERENCES `users` (`id_User`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_networks`
--

LOCK TABLES `users_networks` WRITE;
/*!40000 ALTER TABLE `users_networks` DISABLE KEYS */;
INSERT INTO `users_networks` VALUES (1,1,1);
/*!40000 ALTER TABLE `users_networks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_profile`
--

DROP TABLE IF EXISTS `users_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_profile` (
  `id_Profile` int(11) NOT NULL,
  PRIMARY KEY (`id_Profile`),
  KEY `id_Profile` (`id_Profile`),
  KEY `id_Profile_2` (`id_Profile`),
  CONSTRAINT `FK79975924516C31EB` FOREIGN KEY (`id_Profile`) REFERENCES `profiles` (`id_Profile`),
  CONSTRAINT `FK95DD1FCC48DE61AF` FOREIGN KEY (`id_Profile`) REFERENCES `profiles` (`id_Profile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_profile`
--

LOCK TABLES `users_profile` WRITE;
/*!40000 ALTER TABLE `users_profile` DISABLE KEYS */;
INSERT INTO `users_profile` VALUES (2);
/*!40000 ALTER TABLE `users_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_tracing`
--

DROP TABLE IF EXISTS `users_tracing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_tracing` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PublicIp` varchar(255) DEFAULT NULL,
  `OS` varchar(255) DEFAULT NULL,
  `Browser` varchar(255) DEFAULT NULL,
  `Url` varchar(255) DEFAULT NULL,
  `Referer` varchar(255) DEFAULT NULL,
  `SessionID` varchar(255) DEFAULT NULL,
  `Cookies` varchar(255) DEFAULT NULL,
  `AcceptLanguage` varchar(255) DEFAULT NULL,
  `DateOra` datetime DEFAULT NULL,
  `HypotheticUserType` varchar(255) DEFAULT NULL,
  `PositionStatus` varchar(255) DEFAULT NULL,
  `Latitudine` varchar(255) DEFAULT NULL,
  `Longitudine` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_tracing`
--

LOCK TABLES `users_tracing` WRITE;
/*!40000 ALTER TABLE `users_tracing` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_tracing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersaggregator_campi_custom`
--

DROP TABLE IF EXISTS `usersaggregator_campi_custom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersaggregator_campi_custom` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ControlId` varchar(255) DEFAULT NULL,
  `Label` varchar(255) DEFAULT NULL,
  `Required` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersaggregator_campi_custom`
--

LOCK TABLES `usersaggregator_campi_custom` WRITE;
/*!40000 ALTER TABLE `usersaggregator_campi_custom` DISABLE KEYS */;
/*!40000 ALTER TABLE `usersaggregator_campi_custom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersaggregator_campi_custom_checkbox`
--

DROP TABLE IF EXISTS `usersaggregator_campi_custom_checkbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersaggregator_campi_custom_checkbox` (
  `IDcampi_custom_checkbox` int(11) NOT NULL,
  PRIMARY KEY (`IDcampi_custom_checkbox`),
  KEY `IDcampi_custom_checkbox` (`IDcampi_custom_checkbox`),
  KEY `IDcampi_custom_checkbox_2` (`IDcampi_custom_checkbox`),
  CONSTRAINT `FK45609C1510DC266E` FOREIGN KEY (`IDcampi_custom_checkbox`) REFERENCES `usersaggregator_campi_custom` (`Id`),
  CONSTRAINT `FK5DB8FA12BD229867` FOREIGN KEY (`IDcampi_custom_checkbox`) REFERENCES `usersaggregator_campi_custom` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersaggregator_campi_custom_checkbox`
--

LOCK TABLES `usersaggregator_campi_custom_checkbox` WRITE;
/*!40000 ALTER TABLE `usersaggregator_campi_custom_checkbox` DISABLE KEYS */;
/*!40000 ALTER TABLE `usersaggregator_campi_custom_checkbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersaggregator_campi_custom_dropdown`
--

DROP TABLE IF EXISTS `usersaggregator_campi_custom_dropdown`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersaggregator_campi_custom_dropdown` (
  `IDcampi_custom_dropdown` int(11) NOT NULL,
  PRIMARY KEY (`IDcampi_custom_dropdown`),
  KEY `IDcampi_custom_dropdown` (`IDcampi_custom_dropdown`),
  KEY `IDcampi_custom_dropdown_2` (`IDcampi_custom_dropdown`),
  CONSTRAINT `FKB02E14D2EA26DE57` FOREIGN KEY (`IDcampi_custom_dropdown`) REFERENCES `usersaggregator_campi_custom` (`Id`),
  CONSTRAINT `FKD2FCABDF8B896F6C` FOREIGN KEY (`IDcampi_custom_dropdown`) REFERENCES `usersaggregator_campi_custom` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersaggregator_campi_custom_dropdown`
--

LOCK TABLES `usersaggregator_campi_custom_dropdown` WRITE;
/*!40000 ALTER TABLE `usersaggregator_campi_custom_dropdown` DISABLE KEYS */;
/*!40000 ALTER TABLE `usersaggregator_campi_custom_dropdown` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersaggregator_campi_custom_dropdown_values`
--

DROP TABLE IF EXISTS `usersaggregator_campi_custom_dropdown_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersaggregator_campi_custom_dropdown_values` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Value` varchar(255) DEFAULT NULL,
  `IDcampi_custom_dropdown` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IDcampi_custom_dropdown` (`IDcampi_custom_dropdown`),
  KEY `IDcampi_custom_dropdown_2` (`IDcampi_custom_dropdown`),
  CONSTRAINT `FK189B4E2DFE9CC9E2` FOREIGN KEY (`IDcampi_custom_dropdown`) REFERENCES `usersaggregator_campi_custom_dropdown` (`IDcampi_custom_dropdown`),
  CONSTRAINT `FK8CB6EABEA28613F` FOREIGN KEY (`IDcampi_custom_dropdown`) REFERENCES `usersaggregator_campi_custom_dropdown` (`IDcampi_custom_dropdown`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersaggregator_campi_custom_dropdown_values`
--

LOCK TABLES `usersaggregator_campi_custom_dropdown_values` WRITE;
/*!40000 ALTER TABLE `usersaggregator_campi_custom_dropdown_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `usersaggregator_campi_custom_dropdown_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersaggregator_campi_custom_testo`
--

DROP TABLE IF EXISTS `usersaggregator_campi_custom_testo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersaggregator_campi_custom_testo` (
  `IDcampi_custom_testo` int(11) NOT NULL,
  `MaxLength` int(11) DEFAULT NULL,
  `InputType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDcampi_custom_testo`),
  KEY `IDcampi_custom_testo` (`IDcampi_custom_testo`),
  KEY `IDcampi_custom_testo_2` (`IDcampi_custom_testo`),
  CONSTRAINT `FK575B519FAF396990` FOREIGN KEY (`IDcampi_custom_testo`) REFERENCES `usersaggregator_campi_custom` (`Id`),
  CONSTRAINT `FKD67A59025720629` FOREIGN KEY (`IDcampi_custom_testo`) REFERENCES `usersaggregator_campi_custom` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersaggregator_campi_custom_testo`
--

LOCK TABLES `usersaggregator_campi_custom_testo` WRITE;
/*!40000 ALTER TABLE `usersaggregator_campi_custom_testo` DISABLE KEYS */;
/*!40000 ALTER TABLE `usersaggregator_campi_custom_testo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersaggregator_destinatari`
--

DROP TABLE IF EXISTS `usersaggregator_destinatari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersaggregator_destinatari` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdUtente` int(11) DEFAULT NULL,
  `TipoAnagrafe` varchar(255) DEFAULT NULL,
  `NomeCompleto` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Cellulare` varchar(255) DEFAULT NULL,
  `TipoUtente` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersaggregator_destinatari`
--

LOCK TABLES `usersaggregator_destinatari` WRITE;
/*!40000 ALTER TABLE `usersaggregator_destinatari` DISABLE KEYS */;
/*!40000 ALTER TABLE `usersaggregator_destinatari` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersaggregator_destinatari_liste`
--

DROP TABLE IF EXISTS `usersaggregator_destinatari_liste`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersaggregator_destinatari_liste` (
  `IDdestinatario` int(11) NOT NULL,
  `IDlista` int(11) NOT NULL,
  PRIMARY KEY (`IDlista`,`IDdestinatario`),
  KEY `IDlista` (`IDlista`),
  KEY `IDdestinatario` (`IDdestinatario`),
  KEY `IDlista_2` (`IDlista`),
  KEY `IDdestinatario_2` (`IDdestinatario`),
  CONSTRAINT `FK4DDC7056A0434092` FOREIGN KEY (`IDdestinatario`) REFERENCES `usersaggregator_destinatari` (`Id`),
  CONSTRAINT `FK4DDC7056C541D795` FOREIGN KEY (`IDlista`) REFERENCES `usersaggregator_liste` (`Id`),
  CONSTRAINT `FKDCA2D9207A4DE9A9` FOREIGN KEY (`IDdestinatario`) REFERENCES `usersaggregator_destinatari` (`Id`),
  CONSTRAINT `FKDCA2D9209C3BC178` FOREIGN KEY (`IDlista`) REFERENCES `usersaggregator_liste` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersaggregator_destinatari_liste`
--

LOCK TABLES `usersaggregator_destinatari_liste` WRITE;
/*!40000 ALTER TABLE `usersaggregator_destinatari_liste` DISABLE KEYS */;
/*!40000 ALTER TABLE `usersaggregator_destinatari_liste` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersaggregator_liste`
--

DROP TABLE IF EXISTS `usersaggregator_liste`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersaggregator_liste` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersaggregator_liste`
--

LOCK TABLES `usersaggregator_liste` WRITE;
/*!40000 ALTER TABLE `usersaggregator_liste` DISABLE KEYS */;
/*!40000 ALTER TABLE `usersaggregator_liste` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersaggregator_proprieta`
--

DROP TABLE IF EXISTS `usersaggregator_proprieta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersaggregator_proprieta` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Value` varchar(255) DEFAULT NULL,
  `IDutente_rubrica` int(11) DEFAULT NULL,
  `IDcampo_custom` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IDutente_rubrica` (`IDutente_rubrica`),
  KEY `IDcampo_custom` (`IDcampo_custom`),
  KEY `IDutente_rubrica_2` (`IDutente_rubrica`),
  KEY `IDcampo_custom_2` (`IDcampo_custom`),
  CONSTRAINT `FK5552EC7C5A829A0C` FOREIGN KEY (`IDcampo_custom`) REFERENCES `usersaggregator_campi_custom` (`Id`),
  CONSTRAINT `FK5552EC7CD79E8CD2` FOREIGN KEY (`IDutente_rubrica`) REFERENCES `usersaggregator_utenti_rubrica` (`Id`),
  CONSTRAINT `FK94943B485FFA371B` FOREIGN KEY (`IDutente_rubrica`) REFERENCES `usersaggregator_utenti_rubrica` (`Id`),
  CONSTRAINT `FK94943B487C3F1759` FOREIGN KEY (`IDcampo_custom`) REFERENCES `usersaggregator_campi_custom` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersaggregator_proprieta`
--

LOCK TABLES `usersaggregator_proprieta` WRITE;
/*!40000 ALTER TABLE `usersaggregator_proprieta` DISABLE KEYS */;
/*!40000 ALTER TABLE `usersaggregator_proprieta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersaggregator_utenti_rubrica`
--

DROP TABLE IF EXISTS `usersaggregator_utenti_rubrica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersaggregator_utenti_rubrica` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NomeCompleto` varchar(255) DEFAULT NULL,
  `Cellulare` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersaggregator_utenti_rubrica`
--

LOCK TABLES `usersaggregator_utenti_rubrica` WRITE;
/*!40000 ALTER TABLE `usersaggregator_utenti_rubrica` DISABLE KEYS */;
/*!40000 ALTER TABLE `usersaggregator_utenti_rubrica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersgroups`
--

DROP TABLE IF EXISTS `usersgroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersgroups` (
  `User_UserGroup` int(11) NOT NULL,
  `Group_UserGroup` int(11) NOT NULL,
  `Type_UserGroup` int(11) DEFAULT '0',
  `PublishRole_UserGroup` int(11) DEFAULT '0',
  PRIMARY KEY (`User_UserGroup`,`Group_UserGroup`),
  KEY `User_UserGroup` (`User_UserGroup`),
  KEY `Group_UserGroup` (`Group_UserGroup`),
  KEY `User_UserGroup_2` (`User_UserGroup`),
  KEY `Group_UserGroup_2` (`Group_UserGroup`),
  CONSTRAINT `FKE85A72138381E6E5` FOREIGN KEY (`User_UserGroup`) REFERENCES `users` (`id_User`),
  CONSTRAINT `FKE85A72138785D76A` FOREIGN KEY (`Group_UserGroup`) REFERENCES `groups` (`id_Group`),
  CONSTRAINT `FKECD3A75E374F9F2` FOREIGN KEY (`Group_UserGroup`) REFERENCES `groups` (`id_Group`),
  CONSTRAINT `FKECD3A75EA5C90EAC` FOREIGN KEY (`User_UserGroup`) REFERENCES `users` (`id_User`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersgroups`
--

LOCK TABLES `usersgroups` WRITE;
/*!40000 ALTER TABLE `usersgroups` DISABLE KEYS */;
INSERT INTO `usersgroups` VALUES (1,1,2,2);
/*!40000 ALTER TABLE `usersgroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webdocs_docs`
--

DROP TABLE IF EXISTS `webdocs_docs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webdocs_docs` (
  `id_docs` int(11) NOT NULL,
  PRIMARY KEY (`id_docs`),
  KEY `id_docs` (`id_docs`),
  KEY `id_docs_2` (`id_docs`),
  CONSTRAINT `FKCA0B57A0B0D99845` FOREIGN KEY (`id_docs`) REFERENCES `docs` (`id_Doc`),
  CONSTRAINT `FKF878CC1F47E672DB` FOREIGN KEY (`id_docs`) REFERENCES `docs` (`id_Doc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webdocs_docs`
--

LOCK TABLES `webdocs_docs` WRITE;
/*!40000 ALTER TABLE `webdocs_docs` DISABLE KEYS */;
/*!40000 ALTER TABLE `webdocs_docs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webdocs_docs_contents`
--

DROP TABLE IF EXISTS `webdocs_docs_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webdocs_docs_contents` (
  `id_WebDocs` int(11) NOT NULL,
  `Testo_WebDocs` mediumtext,
  PRIMARY KEY (`id_WebDocs`),
  KEY `id_WebDocs` (`id_WebDocs`),
  KEY `id_WebDocs_2` (`id_WebDocs`),
  CONSTRAINT `FK1122B451F1BCE57B` FOREIGN KEY (`id_WebDocs`) REFERENCES `docs_contents` (`id_DocContent`),
  CONSTRAINT `FK57C6C64A7AD7F5A5` FOREIGN KEY (`id_WebDocs`) REFERENCES `docs_contents` (`id_DocContent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webdocs_docs_contents`
--

LOCK TABLES `webdocs_docs_contents` WRITE;
/*!40000 ALTER TABLE `webdocs_docs_contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `webdocs_docs_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webdocs_folders`
--

DROP TABLE IF EXISTS `webdocs_folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webdocs_folders` (
  `id_folders` int(11) NOT NULL,
  PRIMARY KEY (`id_folders`),
  KEY `id_folders` (`id_folders`),
  KEY `id_folders_2` (`id_folders`),
  CONSTRAINT `FK15991A157CE62EC5` FOREIGN KEY (`id_folders`) REFERENCES `folders` (`id_Folder`),
  CONSTRAINT `FKE9A7A1B1C73A8FD` FOREIGN KEY (`id_folders`) REFERENCES `folders` (`id_Folder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webdocs_folders`
--

LOCK TABLES `webdocs_folders` WRITE;
/*!40000 ALTER TABLE `webdocs_folders` DISABLE KEYS */;
/*!40000 ALTER TABLE `webdocs_folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'cms3_db_start'
--
/*!50003 DROP PROCEDURE IF EXISTS `GrantsConverterProfilesCriteriaSP` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `GrantsConverterProfilesCriteriaSP`()
BEGIN

DECLARE done INTEGER DEFAULT 0;

DECLARE idFolderProfileCriteria INTEGER;
DECLARE FolderProfile INTEGER;
DECLARE Criteria INTEGER;
DECLARE Val INTEGER;
DECLARE Application INTEGER;
DECLARE Updater INTEGER;

DECLARE cursorFolderProfilesCriteria CURSOR FOR SELECT folderprofilescriteria.`id_FolderProfileCriteria`,folderprofilescriteria.`FolderProfile_FolderProfileCriteria`,folderprofilescriteria.`Criteria_FolderProfileCriteria`,folderprofilescriteria.`Value_FolderProfileCriteria`,folderprofilescriteria.`Application_FolderProfileCriteria`,folderprofilescriteria.`Updater_FolderProfileCriteria` 
                                    FROM `folderprofilescriteria`;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

OPEN cursorFolderProfilesCriteria;

REPEAT
	FETCH cursorFolderProfilesCriteria INTO idFolderProfileCriteria,FolderProfile,Criteria,Val,Application,Updater;
	IF NOT done THEN   
		INSERT INTO `object_profiles_criteria` (`id_ObjectProfileCriteria`,`Value_ObjectProfileCriteria`,`Updater_ObjectProfileCriteria`) VALUES (idFolderProfileCriteria,Val,Updater);
        INSERT INTO `folder_profiles_criteria` (`id_FolderProfileCriteria`,`Application_FolderProfileCriteria`,`FolderProfile_ObjectProfileCriteria`,`Criteria_ObjectProfileCriteria`) VALUE (idFolderProfileCriteria,Application,FolderProfile,Criteria);
	END IF;
UNTIL done END REPEAT;

CLOSE cursorFolderProfilesCriteria;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GrantsConverterSP` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `GrantsConverterSP`()
BEGIN

DECLARE done INTEGER DEFAULT 0;

DECLARE idFolderProfile INTEGER;
DECLARE Folder INTEGER;
DECLARE Profile INTEGER;
DECLARE Creator INTEGER;

DECLARE cursorFolderProfiles CURSOR FOR SELECT folderprofiles.`id_FolderProfile`,folderprofiles.`Folder_FolderProfile`,folderprofiles.`Profile_FolderProfile`,folderprofiles.`Creator_FolderProfile` 
                                    FROM `folderprofiles`;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

OPEN cursorFolderProfiles;

REPEAT
	FETCH cursorFolderProfiles INTO idFolderProfile,Folder,Profile,Creator;
	IF NOT done THEN   
		INSERT INTO `object_profiles` (`id_ObjectProfile`,`Profile_ObjectProfile`,`Creator_ObjectProfile`) VALUES (idFolderProfile,Profile,Creator);
        INSERT INTO `folder_profiles` (`id_FolderProfile`,`Folder_ObjectProfile`) VALUES (idFolderProfile,Folder);
	END IF;
UNTIL done END REPEAT;

CLOSE cursorFolderProfiles;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_cms_vertical_grant_value_by_group` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `sp_cms_vertical_grant_value_by_group`(
        IN `in_criterio` INTEGER,
        IN `in_group_tree` VARCHAR(255) CHARACTER SET utf8,
        IN `in_network` INTEGER,
        OUT `out_value` INTEGER
    )
    READS SQL DATA
    DETERMINISTIC
BEGIN

SELECT p.Value_ObjectProfileCriteria INTO out_value
FROM groups g
LEFT JOIN (
            SELECT * FROM (externalapp_profiles INNER JOIN `object_profiles` ON object_profiles.`id_ObjectProfile` = externalapp_profiles.`id_ExternalAppzProfile`)
            LEFT JOIN (externalapp_grants INNER JOIN object_profiles_criteria ON externalapp_grants.`id_ExternalAppGrant` = id_ObjectProfileCriteria) ON id_ExternalAppzProfile = ExternalAppProfile_ObjectProfileCriteria
            WHERE externalapp_grants.Criteria_ObjectProfileCriteria = in_criterio
            AND in_network = externalapp_profiles.`Network_ExternalAppzProfile`
          ) as p ON g.Profile_Group = p.`Profile_ObjectProfile`  
WHERE in_group_tree LIKE Tree_Group
AND p.Value_ObjectProfileCriteria > 0;

IF out_value = 0 OR ISNULL(out_value) THEN
 SET out_value = 0;
END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_cms_vertical_grant_value_by_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `sp_cms_vertical_grant_value_by_user`(
        IN `in_criterio` INTEGER,
        IN `in_user` INTEGER,
        IN `in_network` INTEGER,
        OUT `out_value` INTEGER
    )
    READS SQL DATA
    DETERMINISTIC
BEGIN
  
DECLARE done INTEGER DEFAULT 0;

DECLARE grantValueUsers INTEGER DEFAULT 0;
DECLARE grantValueGroups INTEGER DEFAULT 0;

DECLARE newValue INTEGER DEFAULT 0;

DECLARE tree VARCHAR(255);
DECLARE cursorUserGroups CURSOR FOR 	
										SELECT groups.Tree_Group 
                                        FROM usersgroups 
                                        INNER JOIN groups ON groups.id_Group = usersgroups.Group_UserGroup
								      	WHERE usersgroups.`User_UserGroup` = in_user;

DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN SET done = 1; END;

IF ISNULL(in_network) THEN
	SET in_network = 0;
END IF;

/* Verifico se l'utente ha il permesso direttamente impostato per l'applicazione. */
SELECT object_profiles_criteria.`Value_ObjectProfileCriteria` INTO grantValueUsers
FROM users u
LEFT JOIN (externalapp_profiles INNER JOIN `object_profiles` ON object_profiles.`id_ObjectProfile` = externalapp_profiles.`id_ExternalAppzProfile`) ON u.Profile_User = object_profiles.`Profile_ObjectProfile`
LEFT JOIN (externalapp_grants INNER JOIN object_profiles_criteria ON externalapp_grants.`id_ExternalAppGrant` = id_ObjectProfileCriteria) ON id_ExternalAppzProfile = ExternalAppProfile_ObjectProfileCriteria
WHERE externalapp_grants.Criteria_ObjectProfileCriteria = in_criterio
AND u.id_User = in_user
AND in_network = externalapp_profiles.`Network_ExternalAppzProfile`
AND object_profiles_criteria.`Value_ObjectProfileCriteria` > 0;

/* Se l'utente non ha il permesso impostato, procedo a scansionare i gruppi a cui è associato, alla ricerca di un consenti. */
IF grantValueUsers = 0 OR ISNULL(grantValueUsers) THEN

  SET done = 0;
	
  OPEN cursorUserGroups;

  ciclo:LOOP
    FETCH cursorUserGroups INTO tree;
    IF done THEN
    	CLOSE cursorUserGroups;
        LEAVE ciclo;
    END IF;
    
   	
    	CALL sp_cms_vertical_grant_value_by_group(in_criterio, tree, in_network, newValue);
        
    	IF newValue > 0 THEN
			SET grantValueGroups = newValue;
            /* Esco dal ciclo al primo consenti trovato, altrimenti continuo a cercare nei gruppi. */
            IF newValue = 1 THEN
            	SET done = 1;
            END IF;
  		END IF;
		
  END LOOP;

	SET out_value = grantValueGroups;
ELSE
	SET out_value = grantValueUsers;
END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_dhuh` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `sp_dhuh`(
        IN `in_criterio` INTEGER,
        IN `in_user` INTEGER,
        IN `in_doc` INTEGER,
        IN `in_network_id` INTEGER,
        IN `in_check_parent` BOOLEAN,
        OUT `out_value` INTEGER
    )
    READS SQL DATA
    DETERMINISTIC
BEGIN

DECLARE tree VARCHAR(255) CHARACTER SET utf8;

DECLARE grantValue INTEGER DEFAULT 0;

/* Controlla i permessi per il profilo utente */
SELECT object_profiles_criteria.Value_ObjectProfileCriteria INTO grantValue
FROM users u
LEFT JOIN (doc_profiles INNER JOIN object_profiles ON id_DocProfile = id_ObjectProfile) ON u.Profile_User = object_profiles.Profile_ObjectProfile
LEFT JOIN (doc_profiles_criteria INNER JOIN object_profiles_criteria ON id_DocProfileCriteria = id_ObjectProfileCriteria) ON doc_profiles.id_DocProfile = doc_profiles_criteria.DocProfile_ObjectProfileCriteria
WHERE doc_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio
AND u.id_User = in_user
AND in_doc = doc_profiles.Document_ObjectProfile
AND object_profiles_criteria.Value_ObjectProfileCriteria > 0;

/* Se non ho trovato una specifica controllo i permessi per tutti i gruppi dell'utente */
IF grantValue = 0 OR ISNULL(grantValue) THEN
	CALL sp_dhuh_groupscheck(in_criterio, in_user, in_doc, grantValue);  
END IF;

/* Se ancora non ho una specifica controllo i permessi relativi alla cartella parent; Il controllo va fatto solo quando richiamo la procedura su un documento stand-alone per la prima volta 
e non quando uso la procedura in un ciclo che controlla i documenti di una cartella in quanto la parent ? la cartella in questione che sto verificando ed ? stata gi? controllata. */
IF in_check_parent = true THEN
IF grantValue = 0 OR ISNULL(grantValue) THEN
    SELECT Tree_Folder INTO tree FROM network_docs INNER JOIN (Folders INNER JOIN network_folders ON id_Folder = id_NetworkFolder) ON id_NetworkFolder = Folder_Doc WHERE id_NetworkDoc = in_doc;
    CALL sp_folder_grant_value_by_user(in_criterio, in_user,in_network_id,tree,grantValue);
END IF;
END IF;
SET out_value = grantValue;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_dhuh_groupscheck` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `sp_dhuh_groupscheck`(
        IN `in_criterio` INTEGER,
        IN `in_user` INTEGER,
        IN `in_doc` INTEGER,
        OUT `out_value` INTEGER
    )
    READS SQL DATA
    DETERMINISTIC
BEGIN
  
DECLARE done INTEGER DEFAULT 0;
DECLARE newValue INTEGER DEFAULT 0;
DECLARE groupsCount INTEGER DEFAULT 0;
DECLARE treeParentFolder VARCHAR(255) CHARACTER SET utf8;
DECLARE newValueParents INTEGER DEFAULT 0;

DROP TEMPORARY TABLE IF EXISTS `gruppiutente`; 

CREATE TEMPORARY TABLE IF NOT EXISTS `gruppiutente` AS
SELECT `groups`.`id_Group`, `groups`.`Profile_Group`, `groups`.`Tree_Group`
FROM `usersgroups` INNER JOIN groups ON `usersgroups`.`Group_UserGroup` = `groups`.`id_Group`
WHERE `usersgroups`.`User_UserGroup` = in_user;



/*Cerco i prima i Nega,e se li trovo setto out_value a 2, dopo cerco i consenti e se ne trovo sovrascrivo out_value a 1 -- NUOVA MODIFICA*/
SELECT COUNT(Document_ObjectProfile) INTO newValue
              FROM `gruppiutente`
              LEFT JOIN ((doc_profiles INNER JOIN `network_docs` ON `doc_profiles`.`Document_ObjectProfile` = `network_docs`.`id_NetworkDoc` )
                  INNER JOIN object_profiles ON id_DocProfile = id_ObjectProfile) ON `gruppiutente`.`Profile_Group` = object_profiles.Profile_ObjectProfile
              LEFT JOIN (doc_profiles_criteria INNER JOIN object_profiles_criteria ON id_DocProfileCriteria = id_ObjectProfileCriteria) ON doc_profiles.id_DocProfile = doc_profiles_criteria.DocProfile_ObjectProfileCriteria         
              WHERE doc_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio 
              AND object_profiles_criteria.Value_ObjectProfileCriteria = 2
			  AND `doc_profiles`.`Document_ObjectProfile` = in_doc;	
              
    SELECT COUNT(`gruppiutente`.id_Group) INTO groupsCount FROM `gruppiutente`;
    
    	/* Se il numero di specifiche impostate a nega coincide con il numero di gruppi (dopo aver verificato che non ho specifiche impostate a consenti) ritorno nega. */
  	IF newValue = groupsCount THEN
       	SET out_value = 2;
    END IF;
    
     IF newValue > 0 AND ISNULL(out_value) THEN
        
        	/* Estrazione tree della parent folder */
        	
        	SELECT SUBSTRING(Tree_Folder,1,((Depth_Folder) * 7) + 1) INTO treeParentFolder  FROM folders LEFT JOIN ( docs INNER JOIN `network_docs` ON `docs`.`id_Doc` = `network_docs`.`id_NetworkDoc`) ON `folders`.`id_Folder` = `network_docs`.`Folder_Doc`
WHERE docs.id_Doc = in_doc;
            /* Estrazione dei gruppi utente che non hanno permessi nega sulla cartella corrente */
        	DROP TEMPORARY TABLE IF EXISTS `gruppiUtenteFiltrati`; 
			
            CREATE TEMPORARY TABLE IF NOT EXISTS `gruppiUtenteFiltrati` AS
            SELECT `gruppiutente`.`id_Group`, `gruppiutente`.`Profile_Group`, `gruppiutente`.`Tree_Group`
            FROM `gruppiutente`
            WHERE `gruppiutente`.`id_Group` NOT IN (
            
            SELECT `g1`.`id_Group`
            FROM (      SELECT `groups`.`id_Group`, `groups`.`Profile_Group`, `groups`.`Tree_Group`
			FROM `usersgroups` INNER JOIN groups ON `usersgroups`.`Group_UserGroup` = `groups`.`id_Group`
			WHERE `usersgroups`.`User_UserGroup` = in_user  ) AS `g1`
              LEFT JOIN ((doc_profiles INNER JOIN `network_docs` ON `doc_profiles`.`Document_ObjectProfile` = `network_docs`.`id_NetworkDoc` )
                  INNER JOIN object_profiles ON id_DocProfile = id_ObjectProfile) ON `g1`.`Profile_Group` = object_profiles.Profile_ObjectProfile
              LEFT JOIN (doc_profiles_criteria INNER JOIN object_profiles_criteria ON id_DocProfileCriteria = id_ObjectProfileCriteria) ON doc_profiles.id_DocProfile = doc_profiles_criteria.DocProfile_ObjectProfileCriteria         
              WHERE doc_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio 
              AND object_profiles_criteria.Value_ObjectProfileCriteria = 2
			  AND `doc_profiles`.`Document_ObjectProfile` = in_doc

            );
        
        	/* Verifica che i suddetti gruppi non abbiano consenti su una delle cartelle padre */
            
            SELECT COUNT(Folder_ObjectProfile) INTO newValueParents
            FROM `gruppiUtenteFiltrati`
            LEFT JOIN (((network_folders INNER JOIN folders ON `network_folders`.`id_NetworkFolder` = folders.`id_Folder`) INNER JOIN folder_profiles ON folder_profiles.Folder_ObjectProfile = network_folders.id_NetworkFolder) INNER JOIN object_profiles ON id_FolderProfile = id_ObjectProfile) ON `gruppiUtenteFiltrati`.`Profile_Group` = object_profiles.Profile_ObjectProfile
            LEFT JOIN (folder_profiles_criteria INNER JOIN object_profiles_criteria ON id_FolderProfileCriteria = id_ObjectProfileCriteria) ON folder_profiles.id_FolderProfile = folder_profiles_criteria.FolderProfile_ObjectProfileCriteria
            WHERE folder_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio AND object_profiles_criteria.Value_ObjectProfileCriteria = 1
            AND treeParentFolder LIKE CONCAT(Tree_Folder,"%");	
            
            /* Se non ho trovato consenti, allora sono nel caso in cui bisogna tornare NEGA */
            IF newValueParents = 0 THEN
            	SET out_value = 2;
        	END IF;
            
     END IF;
                     

/*FINE NUOVA MODIFICA*/
SELECT COUNT(Document_ObjectProfile) INTO newValue
              FROM `gruppiutente`
              LEFT JOIN ((doc_profiles INNER JOIN `network_docs` ON `doc_profiles`.`Document_ObjectProfile` = `network_docs`.`id_NetworkDoc` )
                  INNER JOIN object_profiles ON id_DocProfile = id_ObjectProfile) ON `gruppiutente`.`Profile_Group` = object_profiles.Profile_ObjectProfile
              LEFT JOIN (doc_profiles_criteria INNER JOIN object_profiles_criteria ON id_DocProfileCriteria = id_ObjectProfileCriteria) ON doc_profiles.id_DocProfile = doc_profiles_criteria.DocProfile_ObjectProfileCriteria         
              WHERE doc_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio 
              AND object_profiles_criteria.Value_ObjectProfileCriteria = 1
			  AND `doc_profiles`.`Document_ObjectProfile` = in_doc;	

IF newValue > 0 OR newValueParents > 0 THEN
	SET out_value = 1;
END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_doc_grant_value_by_group` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `sp_doc_grant_value_by_group`(
        IN `in_criterio` INTEGER,
        IN `in_group_tree` VARCHAR(255) CHARACTER SET utf8,
        IN `in_doc` INTEGER,
        IN `in_network_id` INTEGER,
        OUT `out_value` INTEGER
    )
    READS SQL DATA
    DETERMINISTIC
BEGIN

DECLARE treeFolder VARCHAR(255);
DECLARE newValue INTEGER DEFAULT 0;

/*Nuova Logica. Controlla solo i permessi del Gruppo sul documento */
 SELECT Value_ObjectProfileCriteria INTO newValue  /*p.Value_ObjectProfileCriteria as val, g.Depth_Group as weight, g.Tree_Group */
        FROM groups g
        LEFT JOIN (
                    SELECT Value_ObjectProfileCriteria,Profile_ObjectProfile FROM (doc_profiles INNER JOIN object_profiles ON id_DocProfile = id_ObjectProfile)
                    LEFT JOIN (doc_profiles_criteria INNER JOIN object_profiles_criteria ON id_DocProfileCriteria = id_ObjectProfileCriteria) ON doc_profiles.id_DocProfile = doc_profiles_criteria.DocProfile_ObjectProfileCriteria
                    WHERE doc_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio
                    AND doc_profiles.Document_ObjectProfile = in_doc
                  ) as p ON g.Profile_Group = p.Profile_ObjectProfile
       WHERE Tree_Group LIKE in_group_tree
       AND Value_ObjectProfileCriteria > 0;
       
IF newValue > 0 THEN
	SET out_value = newValue;  
END IF;      

/* Se non ho trovato permessi associati direttamente al documento, procedo a calcolare i permessi del gruppo alla cartella padre. */
IF out_value = 0 OR ISNULL(out_value) THEN
    SELECT Tree_Folder INTO treeFolder FROM (Docs INNER JOIN network_docs ON id_Doc = id_NetworkDoc) INNER JOIN (Folders INNER JOIN network_folders ON id_Folder = id_NetworkFolder) ON id_Folder = Folder_Doc WHERE id_Doc = in_doc; 
    CALL sp_folder_grant_value_by_group(in_criterio, in_group_tree,treeFolder,in_network_id, out_value);
END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_doc_grant_value_by_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `sp_doc_grant_value_by_user`(
        IN `in_criterio` INTEGER,
        IN `in_user` INTEGER,
        IN `in_doc` INTEGER,
        IN `in_network_id` INTEGER,
        OUT `out_value` INTEGER
    )
    READS SQL DATA
    DETERMINISTIC
BEGIN

CALL sp_dhuh(in_criterio, in_user, in_doc,in_network_id, true, out_value);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_fhgh` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `sp_fhgh`(
        IN `in_criterio` INTEGER,
        IN `in_group_tree` VARCHAR(255) CHARACTER SET utf8,
        IN `in_tree_folder` VARCHAR(255),
        IN `in_network_id` INTEGER,
        OUT `out_value` INTEGER
    )
    READS SQL DATA
    DETERMINISTIC
BEGIN
-- sp_folder_heritage_group_heritage
  
DECLARE newValue INTEGER DEFAULT 0;
DECLARE done INTEGER DEFAULT 0;
DECLARE folderid INTEGER;
DECLARE localFolders CURSOR FOR SELECT id_Folder FROM (Folders INNER JOIN network_folders ON id_Folder = id_NetworkFolder) WHERE Network_Folder = in_network_id AND in_tree_folder LIKE CONCAT(Tree_Folder,"%") ORDER BY Tree_Folder DESC;
DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN SET done = 1; END;

 OPEN localFolders;
 /* Per ogni cartella padre a partire da quella corrente e via via risalendo */
ciclo: LOOP
    FETCH localFolders INTO folderid;
    IF done THEN
    	CLOSE localFolders;
        LEAVE ciclo;
    END IF;
    
    	/* Se il gruppo ha permessi assegnati per la cartella corrente, restituisco il permesso trovato. Altrimenti salgo al livello superiore, ovvero la cartella padre. */
         SELECT p.Value_ObjectProfileCriteria INTO newValue
         FROM groups g
         LEFT JOIN (
                     SELECT Value_ObjectProfileCriteria,Profile_ObjectProfile FROM (folder_profiles INNER JOIN object_profiles ON id_FolderProfile = id_ObjectProfile)
                     LEFT JOIN (folder_profiles_criteria INNER JOIN object_profiles_criteria ON id_FolderProfileCriteria = id_ObjectProfileCriteria) ON folder_profiles.id_FolderProfile = folder_profiles_criteria.FolderProfile_ObjectProfileCriteria
                     WHERE folder_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio
                     AND folder_profiles.Folder_ObjectProfile = folderid
                   ) as p ON g.Profile_Group = p.Profile_ObjectProfile
         WHERE in_group_tree LIKE Tree_Group
         AND p.Value_ObjectProfileCriteria > 0;
 
  
		IF newValue > 0 THEN
		  SET out_value = newValue;
		  SET done = 1;
		END IF;

 END LOOP;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_fhuh` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `sp_fhuh`(
        IN `in_criterio` INTEGER,
        IN `in_user` INTEGER,
        IN `in_network_id` INTEGER,
        IN `in_tree_folder` VARCHAR(255),
        OUT `out_value` INTEGER
    )
    READS SQL DATA
    DETERMINISTIC
BEGIN
  
DECLARE newValue INTEGER DEFAULT 0;
DECLARE done INTEGER DEFAULT 0;
DECLARE folderid INTEGER;
DECLARE localFolders CURSOR FOR SELECT id_NetworkFolder FROM (network_folders INNER JOIN folders ON `network_folders`.`id_NetworkFolder` = folders.`id_Folder`) WHERE Network_Folder = in_network_id AND in_tree_folder LIKE CONCAT(Tree_Folder,"%") ORDER BY Path_Folder DESC;
DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN SET done = 1; END;

 OPEN localFolders;
 
ciclo: LOOP
    FETCH localFolders INTO folderid;
    IF done THEN
    	CLOSE localFolders;
        LEAVE ciclo;
    END IF;
    
		CALL sp_sfuh(in_criterio,in_user,folderid,newValue);
  
		IF newValue > 0 THEN
		  SET out_value = newValue;
		  SET done = 1;
		END IF;
END LOOP;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_folder_grant_value_by_group` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `sp_folder_grant_value_by_group`(
        IN `in_criterio` INTEGER,
        IN `in_group_tree` VARCHAR(255) CHARACTER SET utf8,
        IN `in_tree_folder` VARCHAR(255),
        IN `in_network_id` INTEGER,
        OUT `out_value` INTEGER
    )
    READS SQL DATA
    DETERMINISTIC
BEGIN

CALL sp_fhgh(in_criterio,in_group_tree,in_tree_folder,in_network_id,out_value);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_folder_grant_value_by_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `sp_folder_grant_value_by_user`(
        IN `in_criterio` INTEGER,
        IN `in_user` INTEGER,
        IN `in_network_id` INTEGER,
        IN `in_tree_folder` VARCHAR(255),
        OUT `out_value` INTEGER
    )
    READS SQL DATA
    DETERMINISTIC
BEGIN

CALL sp_fhuh(in_criterio,in_user,in_network_id,in_tree_folder, out_value);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_folder_need_to_show` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `sp_folder_need_to_show`(
        IN `in_criterio` INTEGER(11) UNSIGNED,
        IN `in_user` INTEGER(11) UNSIGNED,
        IN `in_tree_folder` VARCHAR(255),
        IN `in_network_id` INTEGER(11) UNSIGNED,
        OUT `out_value` INTEGER(11) UNSIGNED
    )
    READS SQL DATA
    DETERMINISTIC
BEGIN

DECLARE done INTEGER DEFAULT 0;

DECLARE lastWeight INTEGER DEFAULT -1;

DECLARE newValue INTEGER DEFAULT 0;
DECLARE newWeight INTEGER DEFAULT 0;

DECLARE newValuePerf INTEGER DEFAULT 0;

DECLARE tree VARCHAR(255) CHARACTER SET utf8;

DECLARE groupsCount INTEGER DEFAULT 0;

/* Controllo immediatamente se l'utente ha una specifica settata a "Consenti" per il criterio dato
in almeno una delle sottocartelle del path dato in input. Se si, posso affermare che quella cartella va visualizzata, altrimenti
procedo col calcolo dei permessi. */

SELECT COUNT(Folder_ObjectProfile) INTO newValuePerf
FROM users u
LEFT JOIN (((network_folders INNER JOIN folders ON `network_folders`.`id_NetworkFolder` = folders.`id_Folder`) INNER JOIN folder_profiles ON folder_profiles.Folder_ObjectProfile = network_folders.id_NetworkFolder) INNER JOIN object_profiles ON id_FolderProfile = id_ObjectProfile) ON u.Profile_User = object_profiles.Profile_ObjectProfile
LEFT JOIN (folder_profiles_criteria INNER JOIN object_profiles_criteria ON id_FolderProfileCriteria = id_ObjectProfileCriteria) ON folder_profiles.id_FolderProfile = folder_profiles_criteria.FolderProfile_ObjectProfileCriteria
WHERE u.id_User = in_user AND folder_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio AND object_profiles_criteria.Value_ObjectProfileCriteria = 1
AND Tree_Folder LIKE CONCAT(in_tree_folder,"%") AND Network_Folder = in_network_id;

IF newValuePerf > 0 THEN
	SET out_value = 1;
ELSE 
	/*Se non ho trovato alcuna specifica, controllo se almeno uno dei gruppi associali all'utente ha una specifica "consenti" in almeno una sottocartella. */	
    /* Creo una tabella temporanea con i gruppi dell'utente */
    DROP TEMPORARY TABLE IF EXISTS `gruppiutente`;
    CREATE TEMPORARY TABLE IF NOT EXISTS `gruppiutente` AS
    SELECT `groups`.`id_Group`, `groups`.`Profile_Group`, `groups`.`Tree_Group`
    FROM `usersgroups` INNER JOIN groups ON `usersgroups`.`Group_UserGroup` = `groups`.`id_Group`
    WHERE `usersgroups`.`User_UserGroup` = in_user;
    
    
	SELECT COUNT(Folder_ObjectProfile) INTO newValuePerf
    FROM `gruppiutente`
    LEFT JOIN (((network_folders INNER JOIN folders ON `network_folders`.`id_NetworkFolder` = folders.`id_Folder`) INNER JOIN folder_profiles ON folder_profiles.Folder_ObjectProfile = network_folders.id_NetworkFolder) INNER JOIN object_profiles ON id_FolderProfile = id_ObjectProfile) ON `gruppiutente`.`Profile_Group` = object_profiles.Profile_ObjectProfile
    LEFT JOIN (folder_profiles_criteria INNER JOIN object_profiles_criteria ON id_FolderProfileCriteria = id_ObjectProfileCriteria) ON folder_profiles.id_FolderProfile = folder_profiles_criteria.FolderProfile_ObjectProfileCriteria
    WHERE folder_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio AND object_profiles_criteria.Value_ObjectProfileCriteria = 1
	AND Tree_Folder LIKE CONCAT(in_tree_folder,"%") AND Network_Folder = in_network_id;	

	IF newValuePerf > 0 THEN
		SET out_value = 1;
	ELSE
    	/* Se non ho ancora trovato una specifica "consenti", verifico quanti dei gruppi associati all'utente hanno una specifica nega in quella cartella. */
        SELECT COUNT(Folder_ObjectProfile) INTO newValuePerf
    	FROM `gruppiutente`
    	LEFT JOIN (((network_folders INNER JOIN folders ON `network_folders`.`id_NetworkFolder` = folders.`id_Folder`) INNER JOIN folder_profiles ON folder_profiles.Folder_ObjectProfile = network_folders.id_NetworkFolder) INNER JOIN object_profiles ON id_FolderProfile = id_ObjectProfile) ON `gruppiutente`.`Profile_Group` = object_profiles.Profile_ObjectProfile
    	LEFT JOIN (folder_profiles_criteria INNER JOIN object_profiles_criteria ON id_FolderProfileCriteria = id_ObjectProfileCriteria) ON folder_profiles.id_FolderProfile = folder_profiles_criteria.FolderProfile_ObjectProfileCriteria
    	WHERE folder_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio AND object_profiles_criteria.Value_ObjectProfileCriteria = 2
		AND Tree_Folder LIKE in_tree_folder AND Network_Folder = in_network_id;
    

    	
        SELECT COUNT(`gruppiutente`.id_Group) INTO groupsCount FROM `gruppiutente`;
    
    	/* Se il numero di specifiche impostate a nega coincide con il numero di gruppi (dopo aver verificato che non ho specifiche impostate a consenti) ritorno nega. */
    	IF newValuePerf = groupsCount THEN
        	SET out_value = 2;
    	END IF;
        /* Questo però non è condizione necessaria e sufficiente a dire che non devo visualizzare, quindi procedo a controllare i documenti. */
           		
          /* Controllo se l'utente ha una specifica settata a "Consenti" per il criterio dato
          in almeno uno dei documenti delle sottocartelle del path dato in input. Se si, posso affermare che quella cartella va visualizzata, altrimenti
          procedo col calcolo dei permessi. */
          SELECT COUNT(Document_ObjectProfile) INTO newValuePerf
          FROM users u
          LEFT JOIN ((doc_profiles INNER JOIN `network_docs` ON `doc_profiles`.`Document_ObjectProfile` = `network_docs`.`id_NetworkDoc` INNER JOIN (network_folders INNER JOIN folders ON `network_folders`.`id_NetworkFolder` = folders.`id_Folder`) ON `network_docs`.`Folder_Doc` = `network_folders`.`id_NetworkFolder`) 
              INNER JOIN object_profiles ON id_DocProfile = id_ObjectProfile) ON u.Profile_User = object_profiles.Profile_ObjectProfile
          LEFT JOIN (doc_profiles_criteria INNER JOIN object_profiles_criteria ON id_DocProfileCriteria = id_ObjectProfileCriteria) ON doc_profiles.id_DocProfile = doc_profiles_criteria.DocProfile_ObjectProfileCriteria         
          WHERE u.id_User = in_user 
          AND doc_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio 
          AND object_profiles_criteria.Value_ObjectProfileCriteria = 1
          AND Tree_Folder LIKE CONCAT(in_tree_folder,"%") 
          AND Network_Folder = in_network_id;
           
          IF newValuePerf > 0 THEN
              SET out_value = 1;
          ELSE 
              /*Se non ho trovato alcuna specifica, controllo se almeno uno dei gruppi associali all'utente ha una specifica "consenti" in almeno un documento. */	
              SELECT COUNT(Document_ObjectProfile) INTO newValuePerf
              FROM `gruppiutente`
              LEFT JOIN ((doc_profiles INNER JOIN `network_docs` ON `doc_profiles`.`Document_ObjectProfile` = `network_docs`.`id_NetworkDoc` INNER JOIN (network_folders INNER JOIN folders ON `network_folders`.`id_NetworkFolder` = folders.`id_Folder`) ON `network_docs`.`Folder_Doc` = `network_folders`.`id_NetworkFolder`) 
                  INNER JOIN object_profiles ON id_DocProfile = id_ObjectProfile) ON `gruppiutente`.`Profile_Group` = object_profiles.Profile_ObjectProfile
              LEFT JOIN (doc_profiles_criteria INNER JOIN object_profiles_criteria ON id_DocProfileCriteria = id_ObjectProfileCriteria) ON doc_profiles.id_DocProfile = doc_profiles_criteria.DocProfile_ObjectProfileCriteria         
              WHERE doc_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio 
              AND object_profiles_criteria.Value_ObjectProfileCriteria = 1
              AND Tree_Folder LIKE CONCAT(in_tree_folder,"%") 
              AND Network_Folder = in_network_id;	
                    
              IF newValuePerf > 0 THEN
                  SET out_value = 1;
              END IF;                    
          END IF;    
    END IF; 	
END IF;

/* Se non ho ancora trovato alcuna specifica, procedo a controllare esclusivamente le cartelle padre. */
IF ISNULL(out_value) THEN
   CALL sp_fhuh(in_criterio,in_user,in_network_id,in_tree_folder,out_value);
END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_sdgh` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `sp_sdgh`(
        IN in_criterio INT,
        IN in_group_tree VARCHAR(255) CHARACTER SET utf8,
        IN in_doc INT,
        IN in_winning_value INT,
        OUT out_value INT,
        OUT out_weight INT
    )
    READS SQL DATA
    DETERMINISTIC
BEGIN

SET out_value = 0;

SET out_weight = 0;



 SELECT val, weight INTO out_value, out_weight FROM 

 (

        SELECT p.Value_ObjectProfileCriteria as val, g.Depth_Group as weight, g.Tree_Group as ordercolumn

        FROM groups g

        LEFT JOIN (

                    SELECT Value_ObjectProfileCriteria,Profile_ObjectProfile FROM (doc_profiles INNER JOIN object_profiles ON id_DocProfile = id_ObjectProfile)

                    LEFT JOIN (doc_profiles_criteria INNER JOIN object_profiles_criteria ON id_DocProfileCriteria = id_ObjectProfileCriteria) ON doc_profiles.id_DocProfile = doc_profiles_criteria.DocProfile_ObjectProfileCriteria

                    WHERE doc_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio

                    AND doc_profiles.Document_ObjectProfile = in_doc

                  ) as p ON g.Profile_Group = p.Profile_ObjectProfile

        WHERE in_group_tree LIKE CONCAT(Tree_Group,"%")

        AND p.Value_ObjectProfileCriteria > 0      

        ORDER BY ordercolumn DESC 

        LIMIT 0,1

    ) as tab

;  



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_sfgh` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `sp_sfgh`(
        IN in_criterio INT,
        IN in_group_tree VARCHAR(255) CHARACTER SET utf8,
        IN in_folder INT,
        OUT out_value INT,
        OUT out_weight INT
    )
    READS SQL DATA
    DETERMINISTIC
BEGIN




 SELECT val, weight INTO out_value, out_weight FROM 

  (

         SELECT p.Value_ObjectProfileCriteria as val, g.Depth_Group as weight, g.Tree_Group as ordercolumn

         FROM groups g

         LEFT JOIN (

                     SELECT Value_ObjectProfileCriteria,Profile_ObjectProfile FROM (folder_profiles INNER JOIN object_profiles ON id_FolderProfile = id_ObjectProfile)

                     LEFT JOIN (folder_profiles_criteria INNER JOIN object_profiles_criteria ON id_FolderProfileCriteria = id_ObjectProfileCriteria) ON folder_profiles.id_FolderProfile = folder_profiles_criteria.FolderProfile_ObjectProfileCriteria

                     WHERE folder_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio

                     AND folder_profiles.Folder_ObjectProfile = in_folder

                   ) as p ON g.Profile_Group = p.Profile_ObjectProfile

         WHERE in_group_tree LIKE CONCAT(Tree_Group,"%")

         AND p.Value_ObjectProfileCriteria > 0      

         ORDER BY ordercolumn DESC 

         LIMIT 0,1

     ) as tab

 ;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_sfuh` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `sp_sfuh`(
        IN `in_criterio` INTEGER,
        IN `in_user` INTEGER,
        IN `in_folder` INTEGER,
        OUT `out_value` INTEGER
    )
    READS SQL DATA
    DETERMINISTIC
BEGIN
  
/* Prima vedo se sono stati assegnati i permessi direttamente all'utente */
SELECT object_profiles_criteria.Value_ObjectProfileCriteria INTO out_value
FROM users u
LEFT JOIN (folder_profiles INNER JOIN object_profiles ON id_FolderProfile = id_ObjectProfile) ON u.Profile_User = object_profiles.Profile_ObjectProfile
LEFT JOIN (folder_profiles_criteria INNER JOIN object_profiles_criteria ON id_FolderProfileCriteria = id_ObjectProfileCriteria) ON folder_profiles.id_FolderProfile = folder_profiles_criteria.FolderProfile_ObjectProfileCriteria
WHERE folder_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio
AND u.id_User = in_user
AND in_folder = folder_profiles.Folder_ObjectProfile
AND object_profiles_criteria.Value_ObjectProfileCriteria > 0;

/* se out_value = 0 Allora non esistono permessi per questa combinazione di utente-criterio-network 
    Richiamo la sp che controlla i gruppi.
 */

IF out_value = 0 OR ISNULL(out_value) THEN
  CALL sp_sfuh_groupscheck(in_criterio, in_user,in_folder, out_value);
END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_sfuh_groupscheck` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `sp_sfuh_groupscheck`(
        IN `in_criterio` INTEGER,
        IN `in_user` INTEGER,
        IN `in_folder` INTEGER,
        OUT `out_value` INTEGER
    )
    READS SQL DATA
    DETERMINISTIC
BEGIN


DECLARE newValue INTEGER DEFAULT 0;
DECLARE newValueParents INTEGER DEFAULT 0;
DECLARE groupsCount INTEGER DEFAULT 0;
DECLARE treeParentFolder VARCHAR(255) CHARACTER SET utf8;

DROP TEMPORARY TABLE IF EXISTS `gruppiutente`; 

CREATE TEMPORARY TABLE IF NOT EXISTS `gruppiutente` AS
SELECT `groups`.`id_Group`, `groups`.`Profile_Group`, `groups`.`Tree_Group`
FROM `usersgroups` INNER JOIN groups ON `usersgroups`.`Group_UserGroup` = `groups`.`id_Group`
WHERE `usersgroups`.`User_UserGroup` = in_user;




SELECT COUNT(Folder_ObjectProfile) INTO newValue
FROM `gruppiutente`
LEFT JOIN (((network_folders INNER JOIN folders ON `network_folders`.`id_NetworkFolder` = folders.`id_Folder`) INNER JOIN folder_profiles ON folder_profiles.Folder_ObjectProfile = network_folders.id_NetworkFolder) INNER JOIN object_profiles ON id_FolderProfile = id_ObjectProfile) ON `gruppiutente`.`Profile_Group` = object_profiles.Profile_ObjectProfile
LEFT JOIN (folder_profiles_criteria INNER JOIN object_profiles_criteria ON id_FolderProfileCriteria = id_ObjectProfileCriteria) ON folder_profiles.id_FolderProfile = folder_profiles_criteria.FolderProfile_ObjectProfileCriteria
WHERE folder_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio AND object_profiles_criteria.Value_ObjectProfileCriteria = 1
AND `network_folders`.`id_NetworkFolder` = in_folder;

IF newValue > 0 THEN
SET out_value = 1;
ELSE
    	/* Se non ho ancora trovato una specifica "consenti", verifico quanti dei gruppi associati all'utente hanno una specifica nega in quella cartella. */
        SELECT COUNT(Folder_ObjectProfile) INTO newValue
    	FROM `gruppiutente`
    	LEFT JOIN (((network_folders INNER JOIN folders ON `network_folders`.`id_NetworkFolder` = folders.`id_Folder`) INNER JOIN folder_profiles ON folder_profiles.Folder_ObjectProfile = network_folders.id_NetworkFolder) INNER JOIN object_profiles ON id_FolderProfile = id_ObjectProfile) ON `gruppiutente`.`Profile_Group` = object_profiles.Profile_ObjectProfile
    	LEFT JOIN (folder_profiles_criteria INNER JOIN object_profiles_criteria ON id_FolderProfileCriteria = id_ObjectProfileCriteria) ON folder_profiles.id_FolderProfile = folder_profiles_criteria.FolderProfile_ObjectProfileCriteria
    	WHERE folder_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio AND object_profiles_criteria.Value_ObjectProfileCriteria = 2
		AND `network_folders`.`id_NetworkFolder` = in_folder;
    
    	
        SELECT COUNT(`gruppiutente`.id_Group) INTO groupsCount FROM `gruppiutente`;
    
    	/* Se il numero di specifiche impostate a nega coincide con il numero di gruppi (dopo aver verificato che non ho specifiche impostate a consenti) ritorno nega. */
    	IF newValue = groupsCount THEN
        	SET out_value = 2;
    	END IF;
        
        IF newValue > 0 AND ISNULL(out_value) THEN
        
        	/* Estrazione tree della parent folder */
        	SELECT SUBSTRING(Tree_Folder,1,((Depth_Folder -1) * 7) + 1) into treeParentFolder FROM folders WHERE folders.`id_Folder` = in_folder;
        	
            /* Estrazione dei gruppi utente che non hanno permessi nega sulla cartella corrente */
        	DROP TEMPORARY TABLE IF EXISTS `gruppiUtenteFiltrati`; 
			
            CREATE TEMPORARY TABLE IF NOT EXISTS `gruppiUtenteFiltrati` AS
            SELECT `gruppiutente`.`id_Group`, `gruppiutente`.`Profile_Group`, `gruppiutente`.`Tree_Group`
            FROM `gruppiutente`
            WHERE `gruppiutente`.`id_Group` NOT IN (
            
            SELECT `g1`.`id_Group`
            FROM (      SELECT `groups`.`id_Group`, `groups`.`Profile_Group`, `groups`.`Tree_Group`
			FROM `usersgroups` INNER JOIN groups ON `usersgroups`.`Group_UserGroup` = `groups`.`id_Group`
			WHERE `usersgroups`.`User_UserGroup` = in_user  ) AS `g1`
            LEFT JOIN (((network_folders INNER JOIN folders ON `network_folders`.`id_NetworkFolder` = folders.`id_Folder`) INNER JOIN folder_profiles ON folder_profiles.Folder_ObjectProfile = network_folders.id_NetworkFolder) INNER JOIN object_profiles ON id_FolderProfile = id_ObjectProfile) ON `g1`.`Profile_Group` = object_profiles.Profile_ObjectProfile
            LEFT JOIN (folder_profiles_criteria INNER JOIN object_profiles_criteria ON id_FolderProfileCriteria = id_ObjectProfileCriteria) ON folder_profiles.id_FolderProfile = folder_profiles_criteria.FolderProfile_ObjectProfileCriteria
            WHERE folder_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio AND object_profiles_criteria.Value_ObjectProfileCriteria = 2
            AND `network_folders`.`id_NetworkFolder` = in_folder

            );
        
        	/* Verifica che i suddetti gruppi non abbiano consenti su una delle cartelle padre */
            
            SELECT COUNT(Folder_ObjectProfile) INTO newValueParents
            FROM `gruppiUtenteFiltrati`
            LEFT JOIN (((network_folders INNER JOIN folders ON `network_folders`.`id_NetworkFolder` = folders.`id_Folder`) INNER JOIN folder_profiles ON folder_profiles.Folder_ObjectProfile = network_folders.id_NetworkFolder) INNER JOIN object_profiles ON id_FolderProfile = id_ObjectProfile) ON `gruppiUtenteFiltrati`.`Profile_Group` = object_profiles.Profile_ObjectProfile
            LEFT JOIN (folder_profiles_criteria INNER JOIN object_profiles_criteria ON id_FolderProfileCriteria = id_ObjectProfileCriteria) ON folder_profiles.id_FolderProfile = folder_profiles_criteria.FolderProfile_ObjectProfileCriteria
            WHERE folder_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio AND object_profiles_criteria.Value_ObjectProfileCriteria = 1
            AND treeParentFolder LIKE CONCAT(Tree_Folder,"%");	
            
            /* Se non ho trovato consenti, allora sono nel caso in cui bisogna tornare NEGA */
            IF newValueParents = 0 THEN
            	SET out_value = 2;
        	END IF;
        END IF;
END IF;  


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_sf_need_to_show` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `sp_sf_need_to_show`(
        IN in_criterio INTEGER(11),
        IN in_user INTEGER(11),
        IN in_winning_value INTEGER(11),
        IN in_id_folder INTEGER(11),
        IN in_path_folder VARCHAR(255) CHARACTER SET utf8,
        IN in_network_id INTEGER(11),
        OUT out_value INTEGER(11)
    )
    READS SQL DATA
    DETERMINISTIC
BEGIN



DECLARE newValuePerfDoc INTEGER DEFAULT 0;



DECLARE newValue INTEGER DEFAULT 0;

DECLARE done INTEGER DEFAULT 0;

DECLARE docid INTEGER;



DECLARE localDocs CURSOR FOR SELECT id_NetworkDoc FROM network_docs WHERE Folder_Doc = in_id_folder;

DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN SET done = 1; END;





CALL sp_sfuh(in_criterio,in_user,in_winning_value,in_id_folder,newValue);





IF newValue <> 1 OR ISNULL(newValue) THEN







SELECT COUNT(Document_ObjectProfile) INTO newValuePerfDoc

FROM users u

LEFT JOIN ((network_docs INNER JOIN doc_profiles ON network_docs.id_NetworkDoc = doc_profiles.Document_ObjectProfile) INNER JOIN object_profiles ON id_DocProfile = id_ObjectProfile) ON u.Profile_User = object_profiles.Profile_ObjectProfile

LEFT JOIN (doc_profiles_criteria INNER JOIN object_profiles_criteria ON id_DocProfileCriteria = id_ObjectProfileCriteria) ON doc_profiles.id_DocProfile = doc_profiles_criteria.DocProfile_ObjectProfileCriteria

WHERE doc_profiles_criteria.Criteria_ObjectProfileCriteria = in_criterio

AND u.id_User = in_user

AND Folder_Doc = in_id_folder

AND object_profiles_criteria.Value_ObjectProfileCriteria = 1;







IF newValuePerfDoc > 0 THEN

	SET out_value = 1;

ELSE



OPEN localDocs;

ciclo: LOOP

    FETCH localDocs INTO docid;

    IF done THEN

    	CLOSE localDocs;

        LEAVE ciclo;

    END IF;

    

    CALL sp_dhuh(in_criterio, in_user, docid,in_winning_value,in_network_id, false, newValue);



        IF newValue = 1 THEN

            SET out_value = newValue;

            SET done = 1;

        END IF;



END LOOP;

 

END IF;



ELSE

	SET out_value = newValue;	

END IF;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `VerticalGrantsConverterProfilesGrantsSP` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `VerticalGrantsConverterProfilesGrantsSP`()
BEGIN

DECLARE done INTEGER DEFAULT 0;

DECLARE idExternalAppGrants INTEGER;
DECLARE VappProfile INTEGER;
DECLARE Criteria INTEGER;
DECLARE Val INTEGER;
DECLARE Updater INTEGER;

DECLARE cursorVappProfilesCriteria CURSOR FOR SELECT `externalappzgrants`.`id_ExternalAppGrant`,`externalappzgrants`.`ProfileAssociation_ExternalAppGrant`,`externalappzgrants`.`Criteria_ExternalAppGrant`,`externalappzgrants`.`Value_ExternalAppGrant`,`externalappzgrants`.`Author_ExternalAppGrant`
                                    FROM `externalappzgrants`;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

OPEN cursorVappProfilesCriteria;

REPEAT
	FETCH cursorVappProfilesCriteria INTO idExternalAppGrants,VappProfile,Criteria,Val,Updater;
	IF NOT done THEN   
		INSERT INTO `object_profiles_criteria` (`id_ObjectProfileCriteria`,`Value_ObjectProfileCriteria`,`Updater_ObjectProfileCriteria`) VALUES (idExternalAppGrants,Val,Updater);
        INSERT INTO `externalapp_grants` (`id_ExternalAppGrant`,`ExternalAppProfile_ObjectProfileCriteria`,`Criteria_ObjectProfileCriteria`) VALUE (idExternalAppGrants,VappProfile,Criteria);
	END IF;
UNTIL done END REPEAT;

CLOSE cursorVappProfilesCriteria;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `VerticalGrantsConverterSP` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`cmsdbuser`@`%` PROCEDURE `VerticalGrantsConverterSP`()
BEGIN

DECLARE done INTEGER DEFAULT 0;

DECLARE idExternalAppProfile INTEGER;
DECLARE Application INTEGER;
DECLARE Profile INTEGER;
DECLARE Network INTEGER;
DECLARE Creator INTEGER;


DECLARE cursorVAPPProfiles CURSOR FOR SELECT `externalappz_profiles`.`id_ExternalAppzProfile`,`externalappz_profiles`.`Application_ExternalAppzProfile`,`externalappz_profiles`.`Profile_ExternalAppzProfile`,`externalappz_profiles`.`Network_ExternalAppzProfile`,`externalappz_profiles`.Creator_ExternalAppzProfile 
                                    FROM `externalappz_profiles`;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

OPEN cursorVAPPProfiles;

REPEAT
	FETCH cursorVAPPProfiles INTO idExternalAppProfile,Application,Profile,Network,Creator;
	IF NOT done THEN   
		INSERT INTO `object_profiles` (`id_ObjectProfile`,`Profile_ObjectProfile`,`Creator_ObjectProfile`) VALUES (idExternalAppProfile,Profile,Creator);
        INSERT INTO `externalapp_profiles` (`id_ExternalAppzProfile`,`Application_ObjectProfile`,`Network_ExternalAppzProfile`) VALUES (idExternalAppProfile,Application,Network);
	END IF;
UNTIL done END REPEAT;

CLOSE cursorVAPPProfiles;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

