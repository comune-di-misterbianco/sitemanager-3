﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Sitemanager Cms 3 - Setup</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="wrapper">     
    <div id="header">
        <div class="logo"><span>Sitemanager Cms 3 - Setup</span></div>
		<div class="info"></div>
    </div>
    <div id="content">
        <h1><i class="glyphicon glyphicon-cog"></i> Setup CMS 3</h1>
        <div class="flowbar">
            <ul>
                <li class="item select">                   
                    <a href="#">                        
                        <span class="step-label">Scelta cartella cms</span> 
                    </a>
                </li>
                <li class="item">                
                    <a href="#>                        
                        <span class="step-label">Parametri di configurazione</span> 
                    </a>    
                </li>
                <li class="item">
                    <a href="#">                        
                        <span class="step-label">Riepilogo</span>
                    </a> 
                </li>
            </ul>
        </div>
        <form id="form1" runat="server" role="form">
            <div class="form-contend" >
            <asp:Panel ID="Panel1" CssClass="panel panel-default" runat="server">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="CMSFolderPath" runat="server">Percorso di installazione del cms</label>            
                        <asp:TextBox ID="CMSFolderPath" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <h3>Verifica file Web.Config e Config.xml</h3>
                    <ul ID="CheckFiles" runat="server"></ul>            
                    <p class="buttons"><asp:Button CssClass="btn btn-primary" ID="GoStep2" runat="server" onclick="GoStep2_Click" Text="Avanti" Enabled="False" /></p>
                </div>
            </asp:Panel>            
            </div>
        </form>
    </div>    
    <div id="footer">
        <div class="foter-content"><p>Copyright (c) 2014 Net.Service S.r.l.</p></div>
    </div>
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>
