﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Setup.BusinessLogic;
using Setup.Model;

public partial class Riepilogo : System.Web.UI.Page
{
    //private SetupConfig _Cfg;
    //public SetupConfig Cfg 
    //{
    //    get 
    //    {
    //        if (_Cfg == null)
    //        {
    //            if (HttpContext.Current.Session["SetupCfg"] != null)
    //            {
    //                _Cfg = (SetupConfig)HttpContext.Current.Session["SetupCfg"];
    //            }
    //        }
    //        return _Cfg;
    //    }
        
    //}

    protected void Page_Load(object sender, EventArgs e)
    {      

    }

    private string _CurrentAddress;
    private string CurrentAddress
    {
        get
        {
            if (string.IsNullOrEmpty(_CurrentAddress))
            {
                string strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                _CurrentAddress = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
            }
            return _CurrentAddress;
        }
    }

    // start install

    //protected void Button1_Click(object sender, EventArgs e)
    //{
    //    // parsing file web.config
    //    FileParser configParser = new FileParser(Cfg);

    //    if (!Cfg.WebConfigParsed)
    //    EsitoInstall.InnerHtml += configParser.ParseWebConfigFile() + System.Environment.NewLine;

    //    if (!Cfg.ConfigXMLParsed)
    //    EsitoInstall.InnerHtml += configParser.ParseConfigFile() + System.Environment.NewLine;

    //    bool esito = false;

    //    if (!Cfg.DbImported)
    //    {
    //        DbUtils dbUtils = new DbUtils(Cfg);
    //        esito = dbUtils.Execute("Configs/cms3_db_start.sql");
    //    }

    //    if (esito)
    //    {
    //        Button1.Enabled = false;
    //        Cfg.DbImported = true;

    //        EsitoInstall.InnerHtml += (esito ? "<div class=\"alert alert-success\">Database importato correttamente</div>" : "<div class=\"alert alert-danger\">Errore durante l'importazione del database!</div>");     
    //    }

    //    if (Cfg.DbImported && Cfg.WebConfigParsed && Cfg.ConfigXMLParsed)
    //    { 
    //        // cms installato
    //        divRiepilogo.Attributes["style"] = "display: none;";
    //        Button1.Attributes["style"] = "display: none;";

    //        EsitoInstall.InnerHtml += "<div class=\"alert alert-success\">Cms installato correttamente</div>";
    //        EsitoInstall.InnerHtml += "<div class=\"alert alert-info\"><p><i class=\"glyphicon glyphicon-link\"></i>&nbsp;<a href=\"" + CurrentAddress + Cfg.PortalRootPath + "\">Accedi al frontend del sito</a></p>"
    //                               + "<p><i class=\"glyphicon glyphicon-link\"></i>&nbsp;<a href=\"" + CurrentAddress + Cfg.PortalRootPath + "cms/default.aspx\">Accedi al backoffice</div></p>";

    //    }


    //}

    protected void Button1_Click(object sender, EventArgs e)
    {
        // parsing file web.config
        //      FileParser configParser = new FileParser(Cfg);

        if (!SetupConfig.WebConfigParsed)
            EsitoInstall.InnerHtml += FileParser.ParseWebConfigFile() + System.Environment.NewLine;

        if (!SetupConfig.ConfigXMLParsed)
            EsitoInstall.InnerHtml += FileParser.ParseConfigFile() + System.Environment.NewLine;

        bool esito = false;

        if (!SetupConfig.DbImported)
        {
            //DbUtils dbUtils = new DbUtils(Cfg);
            esito = DbUtils.ImportDBFromFile("Configs/cms3_db_start.sql");
        }

        if (esito)
        {
            Button1.Enabled = false;
            SetupConfig.DbImported = true;

            EsitoInstall.InnerHtml += (esito ? "<div class=\"alert alert-success\">Database importato correttamente</div>" : "<div class=\"alert alert-danger\">Errore durante l'importazione del database!</div>");
        }

        if (SetupConfig.DbImported && SetupConfig.WebConfigParsed && SetupConfig.ConfigXMLParsed)
        {
            // cms installato
            divRiepilogo.Attributes["style"] = "display: none;";
            Button1.Attributes["style"] = "display: none;";

            EsitoInstall.InnerHtml += "<div class=\"alert alert-success\">Cms installato correttamente</div>";
            EsitoInstall.InnerHtml += "<div class=\"alert alert-info\"><p><i class=\"glyphicon glyphicon-link\"></i>&nbsp;<a href=\"" + CurrentAddress + SetupConfig.PortalRootPath + "\">Accedi al frontend del sito</a></p>"
                                   + "<p><i class=\"glyphicon glyphicon-link\"></i>&nbsp;<a href=\"" + CurrentAddress + SetupConfig.PortalRootPath + "cms/default.aspx\">Accedi al backoffice</div></p>";

        }


    }
}