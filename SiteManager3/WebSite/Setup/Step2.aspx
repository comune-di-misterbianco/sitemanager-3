﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Step2.aspx.cs" Inherits="Step2" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title>Sitemanager Cms 3 - Setup</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="wrapper"> 
    <div id="header">
        <div class="logo"><span>Sitemanager Cms 3 - Setup</span></div>		
    </div>
    <div id="content">
         <h1><i class="glyphicon glyphicon-cog"></i> Setup CMS 3</h1>
        <div class="flowbar">
            <ul>
                <li class="item select">                   
                    <a href="default.aspx">                        
                        <span class="step-label">Scelta cartella cms</span> 
                    </a>
                </li>
                <li class="item select">                
                    <a href="#">                        
                        <span class="step-label">Parametri di configurazione</span> 
                    </a>    
                </li>
                <li class="item">
                    <a href="#">                        
                        <span class="step-label">Riepilogo</span>
                    </a> 
                </li>
            </ul>
        </div>
        <form id="form1" runat="server" role="form"> 
        <div class="form-contend" >
            <asp:Panel ID="Panel1" CssClass="panel panel-default" runat="server">
            <div class="info green"><i class="glyphicon glyphicon-info-sign"></i> Cms app folder: <%=(Setup.Model.SetupConfig.AbsoluteCMSParentFolderPath)%></div><br>
                <h2>Dati generali sito web</h2>                                
                <div class="panel-body">
                    <div class="form-group">
                        <label for="PortalName">Nome portale</label>                
                        <asp:TextBox ID="PortalName" CssClass="form-control" runat="server" ></asp:TextBox>                
                    </div>
                
                    <div class="form-group">
                        <label for="PortalEmail">Email associata al portale</label>              
                        <asp:TextBox ID="PortalEmail" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <label for="AbsoluteLogsFolderPath">Inserire il percorso assoluto della cartella per i log</label>
                        <asp:TextBox ID="AbsoluteLogsFolderPath" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>

                    <div class="form-group">
                         <label for="PortalRootPath">Selezionare i percorso relativo della &quot;Applicazione IIS&quot; del cms</label>               
                         <asp:TextBox ID="PortalRootPath" runat="server" CssClass="form-control"></asp:TextBox>
                         <span class="blue help-block"><i class="glyphicon glyphicon-info-sign"></i> (lasciarlo vuoto per farlo partire col dominio - altrimenti inserire esempio /nomeapp)</span>
                    </div>                
                </div>
            </asp:Panel>
            <asp:Panel ID="Panel2" CssClass="panel panel-default" runat="server">
                <h2>Dati database Mysql</h2>
                <div class="panel-body">
                    <div class="form-group">
                         <label for="DbHost">Indirizzo database server</label>              
                         <asp:TextBox ID="DbHost" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                         <label for="form-group">Nome database</label>                    
                         <asp:TextBox ID="DbName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                         <label for="DbPort">Porta TCP</label>                  
                         <asp:TextBox ID="DbPort" runat="server" CssClass="form-control">3306</asp:TextBox>
                    </div>
                    <div class="form-group">
                         <label for="DbUser">Database User</label>                    
                         <asp:TextBox ID="DbUser" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">                                       
                         <label for="DbPw">Database Password</label> 
                         <asp:TextBox ID="DbPw" runat="server" CssClass="form-control"></asp:TextBox>
                         <asp:Button ID="checkDbConn" runat="server" onclick="checkDbConn_Click" Text="Verifica connessione" CssClass="btn btn-info" />
                         <div ID="ConnStatus" runat="server"></div>                
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="Panel3" CssClass="panel panel-default" runat="server">
                <h2>Dati server smtp</h2>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="SmtpHost">Indirizzo server smtp</label>                
                        <asp:TextBox ID="SmtpHost" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="SmtpPort">Porta TCP server smtp</label>     
                        <asp:TextBox ID="SmtpPort" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="SmtpUser">Nome utente smtp server</label>             
                        <asp:TextBox ID="SmtpUser" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="SmtpPw">Password utente smtp server</label>
                        <asp:TextBox ID="SmtpPw" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="SmtpSsl">Usa autenticazione SSL</label>                
                        <asp:RadioButtonList ID="SmtpSsl" runat="server" CssClass="radio">
                            <asp:ListItem>Si</asp:ListItem>
                            <asp:ListItem>No</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
            </asp:Panel>
            <p class="buttons">
                <asp:Button ID="GoToStep3" runat="server" Text="Avanti" 
                    CssClass="btn btn-primary" onclick="GoToStep3_Click" Enabled="False" />
            </p>
        </div>
        </form>
    </div>
    <div id="footer">
        <div class="foter-content"><p>Copyright (c) 2017 Net.Service S.r.l.</p></div>
    </div>
</div>    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>
