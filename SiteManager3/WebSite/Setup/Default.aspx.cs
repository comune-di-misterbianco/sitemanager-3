﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Setup.Model;
using Setup.BusinessLogic;

public partial class _Default : System.Web.UI.Page
{

    //private SetupConfig _Cfg;
    //public SetupConfig Cfg 
    //{
    //    get
    //    {
    //        if (_Cfg == null)
    //            _Cfg = new SetupConfig();
    //        return _Cfg;
    //    }
    //    set { _Cfg = value; }
    //}     

    protected void Page_Load(object sender, EventArgs e)
    {
        //string applicationFolder = HttpRuntime.AppDomainAppPath;
        //DirectoryInfo parentFolder = new DirectoryInfo(applicationFolder);       
        CMSFolderPath.Text = SetupConfig.AbsoluteCMSParentFolderPath;
        //Cfg.AbsoluteCMSFolderPath = CMSFolderPath.Text;

        //FileParser configParser = new FileParser(Cfg);

        string testoWebConfig = "";
        string testoConfigXml = "";
        bool StepStatus = false;

        // verifica esistenza web.config
        if (File.Exists(FileParser.webconfigFilePath))
        {
            testoWebConfig = "<li><i class=\"green glyphicon glyphicon-ok\"></i> File web.config</li>";
            StepStatus = true;
        }
        else
        {
            testoWebConfig = "<li class=\"error\"><i class=\"red glyphicon glyphicon-remove\"></i> il file web.config non è stato trovato. Verificare il seguente percorso: " + FileParser.webconfigFilePath + "</li>";
            StepStatus = false;
        }
                
        // verifica esistenza config.xml
        if (File.Exists(FileParser.configFilePath))
        {
            testoConfigXml = "<li><i class=\"green glyphicon glyphicon-ok\"></i> File config.xml</li>";
            StepStatus = true;
        }
        else
        {
            testoConfigXml = "<li class=\"error\"><i class=\"red glyphicon glyphicon-remove\"></i> Il file config.xml non è stato trovato. Verificare il seguente percorso: " + FileParser.configFilePath + "</li>";
            StepStatus = false;
        }

        //string testoWebConfig = File.Exists(configParser.webconfigFilePath) ? "<span>File web.config <span class=\"green\">Ok</span></span>" : "<span class=\"error\">il file web.config non è stato trovato. Verificare il seguente percorso: " + configParser.webconfigFilePath + "</span>";
        // string testoConfigXml = File.Exists(configParser.configFilePath) ? "<span>File config.xml <span class=\"green\">Ok</span></span>" :  "<span class=\"error\">Il file config.xml non è stato trovato. Verificare il seguente percorso: " + configParser.configFilePath + "</span>";

        CheckFiles.InnerHtml += testoWebConfig;
        CheckFiles.InnerHtml += testoConfigXml;

        if (StepStatus)
            GoStep2.Enabled = true;
                        
    }

    protected void GoStep2_Click(object sender, EventArgs e)
    {      
        //HttpContext.Current.Session["CMSFolderPath"] = CMSFolderPath.Text;

        //Cfg.AbsoluteCMSFolderPath = CMSFolderPath.Text;

        //HttpContext.Current.Session["SetupCfg"] = Cfg;
        HttpContext.Current.Response.Redirect("step2.aspx");
    }
}