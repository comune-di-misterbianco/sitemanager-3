﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Store;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Search;
using Lucene.Net.QueryParsers;
using Lucene.Net.Analysis;



namespace GenericSearch.LuceneWrapper
{
    public class SearchLucene : ISearch
    {
        public string IndexStorePath { get; set; }

        private Directory _indexDir;
        private Directory IndexDir
        {
            get 
            {
                if (_indexDir == null)
                {
                    System.IO.DirectoryInfo indexDir = new System.IO.DirectoryInfo(IndexStorePath);
                    _indexDir = FSDirectory.Open(indexDir);
                }
                return _indexDir;
            }
            
        }
        
        /// <summary>
        /// Implementa la ricerca nell'indice di lucene.net
        /// </summary>
        /// <param name="indexStorePath">Percorso assoluto della cartella che contiene l'indice di lucene</param>
        public SearchLucene(string indexStorePath)
        {
            IndexStorePath = indexStorePath;

        }

        /// <summary>
        /// Salva o aggiorna un record nell'indice
        /// </summary>
        /// <param name="searchFields">Campi da includere nell'indice</param>
        /// <param name="entityID">Campo "chiave" per l'aggiornamento del record nell'indice</param>
        public void SaveOrUpdateEntityToIndex(List<SearchField> searchFields, SearchTerm entityID = null)
        {
            //IndexWriter IndexWriter = new IndexWriter(IndexDir, new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_29), IndexWriter.MaxFieldLength.UNLIMITED);
            IndexWriter IndexWriter = new IndexWriter(IndexDir, new Analyzer(), IndexWriter.MaxFieldLength.UNLIMITED);            

            
            try
            {
                Document doc = new Document();

                foreach (SearchField searchField in searchFields)
                {
                    doc.Add(new Field(searchField.Key, searchField.Value, GetStoreMode(searchField.StoreMode), GetIndexType(searchField.IndexType), GetTermVector(searchField.TermVector)));                    
                }

                if (entityID != null)
                {
                    Term entityTermID = new Term(entityID.Key, entityID.Value);
                    IndexWriter.UpdateDocument(entityTermID, doc);
                }
                else
                {
                    IndexWriter.AddDocument(doc);
                }

                IndexWriter.Optimize();
                IndexWriter.Commit();                            
            }
            catch (Exception ex)
            {
                IndexWriter.Rollback();
            }
            finally
            {
                //IndexWriter.Close();
                IndexWriter.Dispose();
            }

        }

        /// <summary>
        /// Rimuove un record dall'indice
        /// </summary>
        /// <param name="entityID"></param>
        public void RemoveEntityFormIndex(SearchTerm entityID)
        {
            IndexWriter IndexWriter = new IndexWriter(IndexDir, new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30), false, IndexWriter.MaxFieldLength.UNLIMITED);
            try
            {
                Term entityTermID = new Term(entityID.Key, entityID.Value);

                IndexWriter.DeleteDocuments(entityTermID);

                IndexWriter.Optimize();
                IndexWriter.Commit();
            }
            catch (Exception ex)
            {
                IndexWriter.Rollback();
            }
            finally
            {
                //IndexWriter.Close();
                IndexWriter.Dispose();
            }
        }

        private Field.Store GetStoreMode(SearchField.Store storeMode) 
        {
            Field.Store fieldStoreMode = Field.Store.NO;

            switch (storeMode)
            {
                case SearchField.Store.Yes:
                    fieldStoreMode = Field.Store.YES;
                    break;
                case SearchField.Store.No:
                    fieldStoreMode = Field.Store.NO;
                    break;
                //case SearchField.Store.Compress:
                //    fieldStoreMode = Field.Store.COMPRESS;
                //    break;                
            }
            return fieldStoreMode;
        }

        private Field.TermVector GetTermVector(SearchField.TermVectorTypes termVector)
        {
            Field.TermVector termVectorValue = Field.TermVector.NO;
            switch (termVector)
            {
                case SearchField.TermVectorTypes.YES:
                    termVectorValue = Field.TermVector.YES;
                    break;
                case SearchField.TermVectorTypes.NO:
                    termVectorValue = Field.TermVector.NO;
                    break;
                case SearchField.TermVectorTypes.WITH_OFFSETS:
                    termVectorValue = Field.TermVector.WITH_OFFSETS;
                    break;
                case SearchField.TermVectorTypes.WITH_POSITIONS:
                    termVectorValue = Field.TermVector.WITH_POSITIONS;
                    break;
                case SearchField.TermVectorTypes.WITH_POSITIONS_OFFSETS:
                    termVectorValue = Field.TermVector.WITH_POSITIONS_OFFSETS;
                    break;
                default:
                    break;
            }
            return termVectorValue;
        }

        private Field.Index GetIndexType(SearchField.IndexTypes indexType) 
        {
            Field.Index fieldIndexType = Field.Index.NO;

            switch (indexType)
            {
                case SearchField.IndexTypes.NO:
                    fieldIndexType = Field.Index.NO;
                    break;
                case SearchField.IndexTypes.ANALYZED:
                    fieldIndexType = Field.Index.ANALYZED;
                    break;
                case SearchField.IndexTypes.ANALYZED_NO_NORMS:
                    fieldIndexType = Field.Index.ANALYZED_NO_NORMS;
                    break;
                case SearchField.IndexTypes.NOT_ANALYZED:
                    fieldIndexType = Field.Index.NOT_ANALYZED;
                    break;
                case SearchField.IndexTypes.NOT_ANALYZED_NO_NORMS:
                    fieldIndexType = Field.Index.NOT_ANALYZED_NO_NORMS;
                    break;              
            }
            return fieldIndexType;
        }

        /// <summary>
        /// Esegue la ricerca paginata nell'indice
        /// </summary>
        /// <param name="fieldsOnResults">Campi da includere nei risultati di ricerca</param>
        /// <param name="searchTerm">Campo (con il relativo valore) su cui eseguire la ricerca</param>
        /// <param name="pageNumber">Pagina corrente</param>
        /// <param name="pageSize">Numero di record per pagina </param>
        /// <returns>Una lista di risultati paginati</returns>
        public List<SearchResult> Search(ResultFieldToShow fieldsOnResults, SearchTerm searchTerm, int pageNumber, int pageSize)
        {
           System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(IndexStorePath);           

            Directory directory = FSDirectory.Open(dir);
         
            QueryParser parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_30, searchTerm.Key, new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30));
            Query query = parser.Parse(searchTerm.Value);

            IndexSearcher searcher = new IndexSearcher(directory, true);
            
            TopDocs docs = searcher.Search(query,null,1000);
            
            List<SearchResult> results = new List<SearchResult>();

            int currentPage = ((pageNumber - 1) * pageSize);
            int lastPage= (pageNumber * pageSize);
            
            for (int i = currentPage; i <= lastPage && i < docs.TotalHits; i++)
            {
                int docId = docs.ScoreDocs[i].Doc;
                Document doc = searcher.Doc(docId);

                int id = int.Parse(doc.Get(fieldsOnResults.ID.ToString()));
                float score = docs.ScoreDocs[i].Score;
                string title = doc.Get(fieldsOnResults.Title);

                string desc = "";
                
                foreach (string field in fieldsOnResults.Fields)
                    desc += doc.Get(field);

                results.Add(new GenericSearch.SearchResult(id,title,desc,score));                
            }
            
            searcher.Dispose();

            return results;
        }

        /// <summary>
        /// Esegue la ricerca paginata nell'indice
        /// </summary>
        /// <param name="fieldsOnResults">Campi da includere nei risultati di ricerca</param>
        /// <param name="searchTerm">Campo (con il relativo valore) su cui eseguire la ricerca</param>
        /// <param name="pageNumber">Pagina corrente</param>
        /// <param name="pageSize">Numero di record per pagina </param>
        /// <returns>Una lista di risultati paginati</returns>
        public List<CmsDocumentLucene> SearchContent(ResultFieldToShow fieldsOnResults, SearchTerm searchTerm, int pageNumber, int pageSize)
        {
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(IndexStorePath);

            Directory directory = FSDirectory.Open(dir);

            QueryParser parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_30, searchTerm.Key, new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30));
            Query query = parser.Parse(searchTerm.Value);

            IndexSearcher searcher = new IndexSearcher(directory, true);

            TopDocs docs = searcher.Search(query, null, 1000);

            List<CmsDocumentLucene> results = new List<CmsDocumentLucene>();

            int currentPage = ((pageNumber - 1) * pageSize);
            int lastPage = (pageNumber * pageSize);

            for (int i = currentPage; i <= lastPage && i < docs.TotalHits; i++)
            {
                int docId = docs.ScoreDocs[i].Doc;
                Document doc = searcher.Doc(docId);

                int id = int.Parse(doc.Get(fieldsOnResults.ID.ToString()));
                float score = docs.ScoreDocs[i].Score;
                string title = doc.Get(fieldsOnResults.Title);

                string desc = "";

                foreach (string field in fieldsOnResults.Fields)
                    desc += doc.Get(field);

                CmsDocumentLucene item = new CmsDocumentLucene(id, title, doc.Get("Description"), score);
              
                item.BackUrl = doc.Get("BackURL");
                item.FrontUrl = doc.Get("FrontURL");
                item.NetworkSystemName = doc.Get("NetworkSystemName");
                item.LastModify = doc.Get("Data");
                item.DocumentID = int.Parse(doc.Get("DocumentID"));
                item.FolderID = int.Parse(doc.Get("FolderID"));

                results.Add(item);
            }

            searcher.Dispose();

            return results;
        }

        /// <summary>
        /// Restituisce il numero di risultati trovati
        /// </summary>        
        /// <param name="searchTerm">Campo (con il relativo valore) su cui eseguire la ricerca</param>
        /// <returns>Numero di risultati</returns>
        public int ResultsCount(SearchTerm searchTerm)
        {
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(IndexStorePath);

            Directory directory = FSDirectory.Open(dir);

            QueryParser parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_30, searchTerm.Key, new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30));
            Query query = parser.Parse(searchTerm.Value);

            IndexSearcher searcher = new IndexSearcher(directory, true);

            TopDocs docs = searcher.Search(query, null, 1000);

            return docs.TotalHits;
        }
    }
}
