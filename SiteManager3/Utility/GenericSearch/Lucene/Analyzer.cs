﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;

namespace GenericSearch
{
    public class Analyzer : StandardAnalyzer
    {
        public Analyzer()
            : base(Lucene.Net.Util.Version.LUCENE_30)
        {
        
        }

        public override Lucene.Net.Analysis.TokenStream TokenStream(string fieldName, System.IO.TextReader reader)
        {
            ISet<string> stopWords = StopFilter.MakeStopSet(ItalianStopWords.getStopWords(),false);

            TokenStream result = new StandardTokenizer(Lucene.Net.Util.Version.LUCENE_30, reader);
            result = new StandardFilter(result);
            result = new LowerCaseFilter(result);
            result = new StopFilter(true, result, stopWords);

            return result;//base.TokenStream(fieldName, reader);
        }
    }
}
