﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Text;
//using System.Xml;

//namespace GenericSearch.Solr
//{
//    public class Configuration
//    {
        //public Configuration(string hostbase, int tcpport)
        //{
        //    HostBase = hostbase;
        //    HostTcpPort = tcpport;
        //}

        //public string HostBase
        //{
        //    get;
        //    protected set;
        //}

        //public int HostTcpPort
        //{
        //    get;
        //    protected set;
        //}

        ///// <summary>
        ///// Indirizzo web dell'istanza Solr
        ///// </summary>
        //public string SolrIstanceUrl
        //{
        //    get
        //    {
        //        return HostBase + ":" + HostTcpPort + "/solr";
        //    }
        //}

        ///// <summary>
        ///// Parte delle query string che indirizza alla cores list di Solr
        ///// </summary>
        //private string SolrCoresListUrl
        //{
        //    get
        //    {
        //        return SolrIstanceUrl + "/admin/cores";
        //    }
        //}

        ///// <summary>
        ///// Elenco dei cores di Solr
        ///// </summary>
        //public ICollection<string> SolrCores
        //{
        //    get
        //    {
        //        if (_Cores == null)
        //        {
        //            _Cores = new List<string>();

        //            try
        //            {
        //                HttpWebRequest request = WebRequest.Create(SolrCoresListUrl) as HttpWebRequest;
        //                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

        //                XmlDocument xmlDoc = new XmlDocument();
        //                xmlDoc.Load(response.GetResponseStream());

        //                XmlNode MainNode = xmlDoc.SelectSingleNode("response");

        //                XmlNode CoresNode = MainNode.SelectSingleNode("lst[@name='status']");

        //                if (!CoresNode.HasChildNodes)
        //                {
        //                    XmlNode DefaultCoreNode = MainNode.SelectSingleNode("str");
        //                    _Cores.Add(DefaultCoreNode.Attributes["defaultCoreName"].ToString());
        //                }
        //                else
        //                {
        //                    foreach (XmlNode CoreNode in CoresNode.ChildNodes)
        //                    {
        //                        _Cores.Add(CoreNode.Attributes["name"].Value);
        //                    }
        //                }

        //                response.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //            }

        //        }

        //        return _Cores;
        //    }
        //}
        //private  ICollection<string> _Cores;


        ///// <summary>
        ///// Core di default attualmente settato su Solr
        ///// </summary>
        //public string DefaultSolrCore
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(_DefaultSolrCore))
        //        {
        //            try
        //            {
        //                HttpWebRequest request = WebRequest.Create(SolrCoresListUrl) as HttpWebRequest;
        //                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

        //                XmlDocument xmlDoc = new XmlDocument();
        //                xmlDoc.Load(response.GetResponseStream());

        //                XmlNode MainNode = xmlDoc.SelectSingleNode("response");

        //                XmlNode DefaultCoreNode = MainNode.SelectSingleNode("str[@name='defaultCoreName']");

        //                _DefaultSolrCore = DefaultCoreNode.InnerText;

        //                response.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //            }
        //        }

        //        return _DefaultSolrCore;
        //    }
        //}
        //private string _DefaultSolrCore;


        //public static void Init(string hostbase, int tcpport, string core) 
        //{
        //    SolrNet.Startup.Init<GenericSearch.Solr.Model.CmsDocument>(hostbase + ":" + tcpport.ToString() +"/solr/" + core);            
        //}
//    }
//}
