﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SolrNet.Commands.Parameters;
using SolrNet;
using Microsoft.Practices.ServiceLocation;
using SolrNet.Impl.ResponseParsers;
using System.Collections.ObjectModel;

namespace GenericSearch.Solr.DAO
{
    public class CriteriaSolrDAO<T> : System.IDisposable
    {
        //public SolrConnection<T> Connection
        //{
        //    get;
        //    protected set;
        //}

        ///// <summary>
        ///// Deprecate
        ///// </summary>
        ///// <param name="Connection"></param>
        //public CriteriaSolrDAO(SolrConnection<T> Connection)
        //{
        //    this.Connection = Connection;
        //}

        public CriteriaSolrDAO()
        {

        }

        private ISolrOperations<T> Solr
        {
            get
            {
                ISolrOperations<T> solr = ServiceLocator.Current.GetInstance<ISolrOperations<T>>();
                return solr;
            }
        }

        public bool GetStatus()
        {
            bool status = true;
            try
            {
                Solr.Ping();             
            }
            catch(Exception ex)
            {
                status = false;
            }
            return status;
        }

        /// <summary>
        /// Effettua un get del singolo documento il cui id corrisponde all'input (in questa fase ovviamente si suppone che l'id sia l'URL)
        /// </summary>
        /// <param name="Url">Url in input</param>
        /// <returns></returns>
        public T GetById(string id)
        {
            try
            {
                QueryOptions LimitOptions = new QueryOptions();
                LimitOptions.Rows = 1;

                SolrQueryResults<T> results = null;
                results = Solr.Query(new SolrQuery("id:" + id), LimitOptions);

                if (results != null)
                    return results.FirstOrDefault();
            }
            catch (Exception ex)
            {


            }

            return default(T);


            //return Solr.Query(new SolrQuery("id:" + id), LimitOptions).FirstOrDefault();
        }
        /// <summary>
        /// Effettua il get di tutti i documenti indicizzati nell'istanza di SOLR
        /// non ordinati e non paginati
        /// </summary>
        /// <returns>IEnumerable di T</returns>
        public IEnumerable<T> FindAll()
        {
            IEnumerable<T> Documents = Enumerable.Empty<T>();

            //Documents = Connection.Solr.Query(SolrQuery.All);
            Documents = Solr.Query(SolrQuery.All);

            return Documents;
        }

        /// <summary>
        /// Effettua il get paginato di tutti i documenti indicizzati nell'istanza di SOLR
        /// non ordinati e non paginati
        /// </summary>
        /// <returns>IEnumerable di T</returns>
        public IEnumerable<T> FindAll(int pageNumber, int pageSize)
        {

            QueryOptions LimitOptions = new QueryOptions();
            LimitOptions.Rows = pageSize;
            LimitOptions.Start = pageNumber;

            IEnumerable<T> Documents = Enumerable.Empty<T>();

            Documents = Solr.Query(SolrQuery.All, LimitOptions);

            return Documents;
        }

        /// <summary>
        /// Effettua il get dei documenti indicizzati corrispondenti ai parametri impostati,
        /// se non vengono impostati parametri, restituisce tutti i documenti indicizzati
        /// </summary>
        /// <param name="Parameters">Dizionario dei parametri di ricerca</param>
        /// <param name="Operator">Enumerativo corrispondente all'operatore da usare per la query</param>
        /// <returns>IEnumerable di T</returns>
        public IEnumerable<T> FindByCriteria(IDictionary<string, string> Parameters, Operator Operator = Operator.AND)
        {
            if (Parameters.Count > 0)
            {
                IEnumerable<T> Documents = Enumerable.Empty<T>();

                ICollection<SolrQueryByField> Queries = new List<SolrQueryByField>();

                foreach (KeyValuePair<string, string> parameter in Parameters)
                {
                    SolrQueryByField FieldQuery = new SolrQueryByField(parameter.Key.ToString(), parameter.Value);
                    Queries.Add(FieldQuery);

                }

                Documents = Solr.Query(new SolrMultipleCriteriaQuery(Queries.ToArray(), Operator.ToString()));

                return Documents;
            }
            return null;
        }

        /// <summary>
        /// Effettua il get paginato dei documenti indicizzati corrispondenti ai parametri impostati,
        /// se non vengono impostati parametri, restituisce tutti i documenti indicizzati
        /// </summary>
        /// <param name="Parameters">Dizionario dei parametri di ricerca</param>
        /// <param name="Operator">Enumerativo corrispondente all'operatore da usare per la query</param>
        /// <param name="StartRecordNumber">Record iniziale</param>
        /// <param name="pageSize">Numero degli elementi da visualizzare</param>
        /// <returns>IEnumerable di T</returns>
        public IEnumerable<T> FindByCriteria(IDictionary<string, string> Parameters, int StartRecordNumber, int pageSize, IDictionary<string, bool> orders, Operator Operator = Operator.AND)
        {
            if (Parameters.Count > 0)
            {
                QueryOptions QueryOptions = new QueryOptions();
                QueryOptions.Rows = pageSize;
                QueryOptions.Start = StartRecordNumber;

                IEnumerable<T> Documents = Enumerable.Empty<T>();

                ICollection<SolrQueryByField> Queries = new List<SolrQueryByField>();

                foreach (KeyValuePair<string, string> parameter in Parameters)
                {
                    SolrQueryByField FieldQuery = new SolrQueryByField(parameter.Key.ToString(), parameter.Value);
                    Queries.Add(FieldQuery);
                }

                foreach (KeyValuePair<string, bool> order in orders)
                {
                    QueryOptions.AddOrder(new SortOrder(order.Key, ((order.Value) ? Order.ASC : Order.DESC)));

                }

                //SortOrder DefaultSolrOrder = new SortOrder(SolrQueryParameters.Fields.title.ToString(), Order.ASC);
                //QueryOptions.AddOrder(new SortOrder[] { DefaultSolrOrder });


                Documents = Solr.Query(new SolrMultipleCriteriaQuery(Queries.ToArray(), Operator.ToString()), QueryOptions);

                return Documents;
            }

            return FindAll(StartRecordNumber, pageSize);
        }

        /// <summary>
        /// Effettua il get paginato dei documenti indicizzati corrispondenti ai parametri impostati,
        /// se non vengono impostati parametri, restituisce tutti i documenti indicizzati
        /// </summary>
        /// <param name="Parameters">Dizionario dei parametri di ricerca</param>
        /// <param name="Operator">Enumerativo corrispondente all'operatore da usare per la query</param>
        /// <param name="StartRecordNumber">Record iniziale</param>
        /// <param name="pageSize">Numero degli elementi da visualizzare</param>
        /// <param name="CustomResult">Elenco di eventuali fields da ottenere come risultato custom</param>
        /// <returns>IEnumerable di T</returns>
        public IEnumerable<T> FindByCriteria(IDictionary<string, string> Parameters, int StartRecordNumber, int pageSize, IDictionary<string, bool> orders, Operator Operator = Operator.AND, ICollection<string> CustomResult = null)
        {
            if (Parameters.Count > 0)
            {
                QueryOptions QueryOptions = new QueryOptions();
                QueryOptions.Rows = pageSize;
                QueryOptions.Start = StartRecordNumber;


                IEnumerable<T> Documents = Enumerable.Empty<T>();

                ICollection<SolrQueryByField> Queries = new List<SolrQueryByField>();

                foreach (KeyValuePair<string, string> parameter in Parameters)
                {
                    SolrQueryByField FieldQuery = new SolrQueryByField(parameter.Key.ToString(), parameter.Value);
                    Queries.Add(FieldQuery);
                }

                //SortOrder DefaultSolrOrder = new SortOrder(SolrQueryParameters.Fields.title.ToString(), Order.ASC);
                //QueryOptions.AddOrder(new SortOrder[] { DefaultSolrOrder });

                foreach (KeyValuePair<string, bool> order in orders)
                {
                    QueryOptions.AddOrder(new SortOrder(order.Key, ((order.Value) ? Order.ASC : Order.DESC)));
                }

                if (CustomResult != null)
                {
                    foreach (string field in CustomResult)
                        QueryOptions.Fields.Add(field);
                }


                Documents = Solr.Query(new SolrMultipleCriteriaQuery(Queries.ToArray(), Operator.ToString()), QueryOptions);

                return Documents;
            }

            return FindAll(StartRecordNumber, pageSize);
        }

        /// <summary>
        /// Effettua il get paginato dei documenti indicizzati corrispondenti ai parametri impostati,
        /// se non vengono impostati parametri, restituisce tutti i documenti indicizzati
        /// </summary>
        /// <param name="Parameters">Dizionario dei parametri di ricerca</param>
        /// <param name="Operator">Enumerativo corrispondente all'operatore da usare per la query</param>
        /// <param name="StartRecordNumber">Record iniziale</param>
        /// <param name="pageSize">Numero degli elementi da visualizzare</param>
        /// <param name="OrderList">Elenco dei campi su cui effettuare l'ordinamento</param>
        /// <returns>IEnumerable di T</returns>
        public IEnumerable<T> FindByCriteria(IDictionary<string, string> Parameters, int StartRecordNumber, int pageSize, Operator Operator = Operator.AND, ICollection<SortOrder> OrderList = null)
        {
            if (Parameters.Count > 0)
            {
                QueryOptions QueryOptions = new QueryOptions();
                QueryOptions.Rows = pageSize;
                QueryOptions.Start = StartRecordNumber;


                IEnumerable<T> Documents = Enumerable.Empty<T>();

                ICollection<SolrQueryByField> Queries = new List<SolrQueryByField>();

                foreach (KeyValuePair<string, string> parameter in Parameters)
                {
                    SolrQueryByField FieldQuery = new SolrQueryByField(parameter.Key.ToString(), parameter.Value);
                    Queries.Add(FieldQuery);
                }

                QueryOptions.AddOrder(OrderList.ToArray());

                Documents = Solr.Query(new SolrMultipleCriteriaQuery(Queries.ToArray(), Operator.ToString()), QueryOptions);

                return Documents;
            }

            return FindAll(StartRecordNumber, pageSize);
        }

        /// <summary>
        /// Effettua il get paginato dei documenti indicizzati corrispondenti ai parametri impostati,
        /// se non vengono impostati parametri, restituisce tutti i documenti indicizzati
        /// </summary>
        /// <param name="Parameters">Dizionario dei parametri di ricerca</param>
        /// <param name="Operator">Enumerativo corrispondente all'operatore da usare per la query</param>
        /// <param name="StartRecordNumber">Record iniziale</param>
        /// <param name="pageSize">Numero degli elementi da visualizzare</param>
        /// <param name="OrderList">Elenco dei campi su cui effettuare l'ordinamento</param>
        /// <param name="CustomResult">Elenco di eventuali fields da ottenere come risultato custom</param>
        /// <returns>IEnumerable di T</returns>
        public IEnumerable<T> FindByCriteria(IDictionary<string, string> Parameters, int StartRecordNumber, int pageSize, ICollection<SortOrder> OrderList, Operator Operator = Operator.AND, ICollection<string> CustomResult = null)
        {
            if (Parameters.Count > 0)
            {
                QueryOptions QueryOptions = new QueryOptions();
                QueryOptions.Rows = pageSize;
                QueryOptions.Start = StartRecordNumber;


                IEnumerable<T> Documents = Enumerable.Empty<T>();

                ICollection<SolrQueryByField> Queries = new List<SolrQueryByField>();

                foreach (KeyValuePair<string, string> parameter in Parameters)
                {
                    SolrQueryByField FieldQuery = new SolrQueryByField(parameter.Key.ToString(), parameter.Value);
                    Queries.Add(FieldQuery);
                }



                QueryOptions.AddOrder(OrderList.ToArray());

                if (CustomResult != null)
                {
                    foreach (string field in CustomResult)
                        QueryOptions.Fields.Add(field);
                }


                Documents = Solr.Query(new SolrMultipleCriteriaQuery(Queries.ToArray(), Operator.ToString()), QueryOptions);

                return Documents;
            }

            return FindAll(StartRecordNumber, pageSize);
        }

        /// <summary>
        /// Effettua un count dei documenti indicizzati corrispondenti ai parametri di ricerca impostati,
        /// se la collezione dei parametri è vuuta, conteggia tutti i documenti indicizzati
        /// </summary>
        /// <param name="Parameters">Dizionario dei parametri di ricerca</param>
        /// <param name="Operator">Enumerativo corrispondente all'operatore da usare per la query</param>
        /// <returns>IEnumerable di T</returns>
        public Int64 FindByCriteriaCount(IDictionary<string, string> Parameters, Operator Operator = Operator.AND)
        {

            ICollection<SolrQueryByField> Queries = new List<SolrQueryByField>();

            QueryOptions LimitOptions = new QueryOptions();
            LimitOptions.Rows = 0;

            if (Parameters.Count > 0)
            {
                foreach (KeyValuePair<string, string> parameter in Parameters)
                {
                    SolrQueryByField FieldQuery = new SolrQueryByField(parameter.Key.ToString(), parameter.Value);
                    Queries.Add(FieldQuery);

                }

                return Solr.Query(new SolrMultipleCriteriaQuery(Queries.ToArray(), Operator.ToString()), LimitOptions).NumFound;
            }
            else
                return Solr.Query(SolrQuery.All, LimitOptions).NumFound;
        }

        /// <summary>
        /// Effettua un count degli elementi indicizzati corrispondenti ai parametri di ricerca impostati,
        /// se la collezione dei parametri è vuota, conteggia tutti gli elementi indicizzati
        /// </summary>
        /// <param name="Parameters">Dizionario dei parametri di ricerca</param>
        /// <param name="Operator">Enumerativo corrispondente all'operatore da usare per la query</param>
        /// <returns>Numero di istanze</returns>
        public Int64 FindByCriteriaCount(CompositeParametersCollection Parameters, Operator Operator = Operator.AND)
        {

            ICollection<ISolrQuery> Queries = new List<ISolrQuery>();

            QueryOptions LimitOptions = new QueryOptions();
            LimitOptions.Rows = 0;

            if (Parameters.Count > 0)
            {
                foreach (CompositeParameter parameter in Parameters)
                {
                    SolrQueryByField FieldQuery = new SolrQueryByField(parameter.Key, parameter.Value);

                    if (parameter.ParameterOperator == Operator.NOT)
                    {
                        SolrNotQuery Not = new SolrNotQuery(FieldQuery);
                        Queries.Add(Not);
                    }
                    else
                    {
                        Queries.Add(FieldQuery);
                    }

                }

                return Solr.Query(new SolrMultipleCriteriaQuery(Queries.ToArray(), Operator.ToString()), LimitOptions).NumFound;
            }
            else
                return Solr.Query(SolrQuery.All, LimitOptions).NumFound;
        }

        /// <summary>
        /// Effettua il get degli elementi indicizzati corrispondenti ai parametri impostati.
        /// Del singolo elemento indicizzato viene selezionata solo la proprietà richiesta
        /// </summary>
        /// <param name="Parameters">Dizionario dei parametri di ricerca</param>
        /// <param name="Operator">Enumerativo corrispondente all'operatore da usare per la query</param>
        /// <returns>IEnumerable di T<returns>
        public IEnumerable<T> FindByCriteria(IDictionary<string, string> Parameters, string SelectedField, Operator Operator = Operator.AND)
        {
            IEnumerable<T> Documents = Enumerable.Empty<T>();

            ICollection<SolrQueryByField> Queries = new List<SolrQueryByField>();

            foreach (KeyValuePair<string, string> parameter in Parameters)
            {
                SolrQueryByField FieldQuery = new SolrQueryByField(parameter.Key, parameter.Value);
                Queries.Add(FieldQuery);

            }

            QueryOptions QueryOptions = new QueryOptions();
            QueryOptions.Fields.Add(SelectedField);

            Documents = Solr.Query(new SolrMultipleCriteriaQuery(Queries.ToArray(), Operator.ToString()), QueryOptions);

            return Documents;

        }


        public void AddContent(T entity)
        {

            Solr.Add(entity);
            Solr.Commit();
        }

        public void DeleteContent(T entity)
        {
            Solr.Delete(entity);
            Solr.Commit();
        }

        public void Optimize()
        {
            Solr.Optimize();
            Solr.Commit();
        }

        public void Rollback()
        {
            Solr.Rollback();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Free other state (managed objects).
                }
                // Free your own state (unmanaged objects).
                // Set large fields to null.
                disposed = true;
            }
        }

        //public ClusterResults GetCluster(string searchTerm) //IDictionary<string, string> Parameters, int StartRecordNumber, int pageSize
        //{
        //    ClusterResults cluRes = null;

        //    QueryOptions QueryOptions = new QueryOptions();
        //    QueryOptions.Rows = 10;
        //    QueryOptions.Fields = new[] { "*,score" };
        //    //QueryOptions.Start = StartRecordNumber;

        //    ClusteringParameters cluParam = new ClusteringParameters();

        //    //cluParam.ProduceSummary = true;
        //    cluParam.Collection = true;
        //    cluParam.Results = true;

        //    //cluParam.Engine = "kmeans";                  
        //    //cluParam.Algorithm = "org.carrot2.clustering.kmeans.BisectingKMeansClusteringAlgorithm";

        //    cluParam.Engine = "lingo";
        //    cluParam.Algorithm = "org.carrot2.clustering.lingo.LingoClusteringAlgorithm";
        //    cluParam.LexicalResources = "clustering/carrot2";

        //    //cluParam.NumDescriptions = 5;
        //    //cluParam.SubClusters = false;

        //    //cluParam.Title = "name";
        //    //cluParam.Url = "id";
        //    //cluParam.Snippet = "document_content_text";

        //    QueryOptions.Clustering = cluParam;

        //    IEnumerable<T> Documents = Enumerable.Empty<T>();

        //    SolrQuery query = new SolrQuery(searchTerm);
        //    SolrQueryResults<T> results = Solr.Query(query, QueryOptions);

        //    cluRes = results.Clusters;

        //    return cluRes;
        //}


        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="StartRecordNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="qt">Query type</param>
        /// <param name="qf"></param>
        /// <param name="useStopWords"></param>
        /// <param name="lowercaseOperators"></param>
        /// <param name="filters"></param>
        /// <returns></returns>
        //public IEnumerable<T> SearchEdismax(string searchTerm, int StartRecordNumber, int pageSize, string qt, string qf, bool useStopWords, bool lowercaseOperators, IDictionary<string,string> filters = null)
        //{
        //    QueryOptions QueryOptions = new QueryOptions();
        //    QueryOptions.Rows = pageSize;
        //    QueryOptions.Start = StartRecordNumber;

        //    if (filters != null)
        //    {
        //        Collection<SolrNet.ISolrQuery> filterQueries = new Collection<ISolrQuery>();
        //        SolrQueryByField filterQuerie;
        //        foreach(KeyValuePair<string, string> filter in filters)
        //        {
        //            filterQuerie = new SolrQueryByField(filter.Key, filter.Value);
        //            filterQueries.Add(filterQuerie);
        //        }

        //        QueryOptions.FilterQueries = filterQueries; 
        //    }

        //    Dictionary<string, string> extraParams = new Dictionary<string, string>();
        //    extraParams.Add("qt", qt); //extraParams.Add("qt", "edismax");
        //    extraParams.Add("q.alt", "*:*");
        //    extraParams.Add("qf", qf);
        //    extraParams.Add("stopwords", useStopWords.ToString().ToLower());
        //    extraParams.Add("lowercaseOperators", lowercaseOperators.ToString().ToLower());

        //    QueryOptions.ExtraParams = extraParams;

        //    SolrQuery query = new SolrQuery(searchTerm);

        //    IEnumerable<T> Documents = Enumerable.Empty<T>();
        //    Documents = Solr.Query(query, QueryOptions);

        //    return Documents;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="qt"></param>
        /// <param name="qf"></param>
        /// <param name="useStopWords"></param>
        /// <param name="lowercaseOperators"></param>
        /// <param name="filters"></param>
        /// <returns></returns>
        //public Int64 SearchEdismaxCount(string searchTerm, string qt, string qf, bool useStopWords, bool lowercaseOperators, IDictionary<string, string> filters = null)
        //{
        //    QueryOptions QueryOptions = new QueryOptions();

        //    if (filters != null)
        //    {
        //        Collection<SolrNet.ISolrQuery> filterQueries = new Collection<ISolrQuery>();
        //        SolrQueryByField filterQuerie;
        //        foreach (KeyValuePair<string, string> filter in filters)
        //        {
        //            filterQuerie = new SolrQueryByField(filter.Key, filter.Value);
        //            filterQueries.Add(filterQuerie);
        //        }

        //        QueryOptions.FilterQueries = filterQueries;
        //    }


        //    Dictionary<string, string> extraParams = new Dictionary<string, string>();
        //    extraParams.Add("qt", qt); //extraParams.Add("qt", "edismax");
        //    extraParams.Add("q.alt", "*:*");
        //    extraParams.Add("qf", qf);
        //    extraParams.Add("stopwords", useStopWords.ToString().ToLower());
        //    extraParams.Add("lowercaseOperators", lowercaseOperators.ToString().ToLower());

        //    QueryOptions.ExtraParams = extraParams;

        //    SolrQuery query = new SolrQuery(searchTerm);

        //    return Solr.Query(query, QueryOptions).NumFound;           
        //}

    }
}
