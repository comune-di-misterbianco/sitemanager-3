﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericSearch.Solr.DAO
{
    public class CompositeParameter
    {
        public Operator ParameterOperator
        {
            get;
            set;
        }

        public KeyValuePair<string, string> Paramerer
        {
            get;
            set;
        }

        public string Key
        {
            get
            {
                return Paramerer.Key;
            }
        }

        public string Value
        {
            get
            {
                return Paramerer.Value;
            }
        }


        public CompositeParameter()
        {
            Paramerer = new KeyValuePair<string, string>();
        }
    }
}
