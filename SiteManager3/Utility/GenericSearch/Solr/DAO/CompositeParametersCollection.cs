﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericSearch.Solr.DAO
{
    public class CompositeParametersCollection
    {
        public ICollection<CompositeParameter> Parameters
        {
            get;
            private set;
        }

        public void Add(string key, object value, Operator QueryOperator = Operator.AND)
        {
            CompositeParameter Composite = new CompositeParameter();
            Composite.Paramerer = new KeyValuePair<string, string>(key, value.ToString());
            Composite.ParameterOperator = QueryOperator;

            this.Parameters.Add(Composite);
        }

        public int Count
        {
            get
            {
                return Parameters.Count;
            }
        }

        public CompositeParametersCollection()
        {
            this.Parameters = new HashSet<CompositeParameter>();
        }

        public IEnumerator GetEnumerator()
        {
            return Parameters.GetEnumerator();
        }
    }
}
