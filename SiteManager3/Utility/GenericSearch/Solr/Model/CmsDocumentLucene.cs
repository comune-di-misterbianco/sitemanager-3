﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericSearch
{
    public class CmsDocumentLucene : SearchResult
    {
        public CmsDocumentLucene(int id, string title, string description, float score):base(id, title, description, score)
        {

        }

        public string BackUrl { get; set; }

        public string FrontUrl { get; set; }

        public int FolderID { get; set; }

        public int DocumentID { get; set; }

        public string NetworkSystemName { get; set; }

        public string LastModify { get; set; }
    }
}
