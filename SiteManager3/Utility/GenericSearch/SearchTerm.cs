﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericSearch
{
    public class SearchTerm
    {
        public SearchTerm(string key, string value)
        {
            Key = key;
            Value = value;
        }

        public string Key { get; set; }
        public string Value { get; set; }
    }
}
