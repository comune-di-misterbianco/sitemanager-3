﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericSearch
{
    public class SearchResult
    {
        public SearchResult(int id, string title, string description, float score)
        {
            ID = id;
            Title = title;
            Description = description;
            Score = score;
        }
     
        public float Score { get; set; }
        public int ID { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
    }
}
