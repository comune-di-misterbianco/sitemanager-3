﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericSearch
{
    public class ResultFieldToShow
    {
        public ResultFieldToShow(string id, string title, List<string> fields) 
        {
            ID = id;
            Title = title;
            Fields = fields;
        }

        public string ID { get; set; }
        public string Title { get; set; }
        public List<string> Fields { get; set;}
    }
}
