﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericSearch
{
    public class SearchField
    {
        public SearchField(string key, string value, Store storeMode, IndexTypes indexType)
        {
            Key = key;
            Value = value;
            StoreMode = storeMode;
            IndexType = indexType;
        }

        public SearchField(string key, string value, Store storeMode, IndexTypes indexType, TermVectorTypes termVectorType)
        {
            Key = key;
            Value = value;
            StoreMode = storeMode;
            IndexType = indexType;
            TermVector = termVectorType;
        }

        public string Key
        {
            get;
            set;
        }

        public string Value
        {
            get;
            set;
        }
        public Store StoreMode
        {
            get;
            set;
        }

        public IndexTypes IndexType
        {
            get;
            set;
        }

        public TermVectorTypes TermVector
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// YES - Stores the content in the index as supplied to the Field’s constructor
        /// NO - Doesn’t store the value at all (you won’t be able to retrieve it) 
        /// Compress - Compresses the original value and stores it into the index 
        /// </summary>
        public enum Store
        {
            Yes,
            No,
            Compress
        }

        /// <summary>
        /// NO - The value is not indexed (it cannot be searched but only retrieved, provided it was stored)
        /// ANALYZED - The value is fully indexed, using the Analyzer (so the text is first tokenized, then normalized, and so on
        /// NOT_ANALYZED - The value is indexed, but without a analyzer, so it can be searched, but only as single term. 
        ///  ANALYZED_NO_NORMS ???
        ///  NOT_ANALYZED_NO_NORMS ???
        /// </summary>
        public enum IndexTypes
        {
            NO,
            ANALYZED,
            ANALYZED_NO_NORMS,
            NOT_ANALYZED,
            NOT_ANALYZED_NO_NORMS
        }

        public enum TermVectorTypes
        {
            YES,
            NO,
            WITH_OFFSETS,
            WITH_POSITIONS,
            WITH_POSITIONS_OFFSETS
        }
    }
}
