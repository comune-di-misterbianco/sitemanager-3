﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericSearch
{
    public interface ISearch
    {
        string IndexStorePath { get; set; }

        void SaveOrUpdateEntityToIndex(List<SearchField> searchFields, SearchTerm entityID = null);

        void RemoveEntityFormIndex(SearchTerm entityID);

        List<SearchResult> Search(ResultFieldToShow fieldsOnResults, SearchTerm searchTerm, int pageNumber, int pageSize);
    }
}
