﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCalendar.Mapping
{
    public class GenericEventMapping : ClassMap<GenericEvent>
    {
        public GenericEventMapping()
        {
            Table("generic_event");
            Id(x => x.ID);
            Map(x => x.Title);
            Map(x => x.AllDay);
            Map(x => x.Start);
            Map(x => x.End);
            Map(x => x.Url);
            Map(x => x.ClassName);
            Map(x => x.Editable);
            Map(x => x.StartEditable);
            Map(x => x.DurationEditable);
            Map(x => x.Color);
            Map(x => x.BackgroundColor);
            Map(x => x.BorderColor);
            Map(x => x.TextColor);
        }
    }
}