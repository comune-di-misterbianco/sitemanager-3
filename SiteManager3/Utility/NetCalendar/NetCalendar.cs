﻿using NetService.Utility.ValidatedFields;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace NetCalendar
{
    public class NetCalendar<T> : WebControl
    {
        public List<T> Eventi
        {
            set
            {
                _Eventi = value;
            }
        }

        private List<T> _Eventi;

        public enum Fields
        {
            ID,
            Title,
            AllDay,
            Start,
            End,
            Url,
            ClassName,
            Editable,
            StartEditable,
            DurationEditable,
            Color,
            BackgroundColor,
            BorderColor,
            TextColor
        }

        private Dictionary<string, string> _IdFormDictionary;
        public Dictionary<string, string> IdFormDictionary
        {
            get
            {
                if (_IdFormDictionary == null)
                {
                    _IdFormDictionary = new Dictionary<string, string>();
                    IdFormDictionary.Add(Fields.ID.ToString(), "ID");
                    IdFormDictionary.Add(Fields.Title.ToString(), "Title");
                    IdFormDictionary.Add(Fields.AllDay.ToString(), "AllDay");
                    IdFormDictionary.Add(Fields.Start.ToString(), "Start");
                    IdFormDictionary.Add(Fields.End.ToString(), "End");
                    IdFormDictionary.Add(Fields.Url.ToString(), "Url");
                    IdFormDictionary.Add(Fields.ClassName.ToString(), "ClassName");
                    IdFormDictionary.Add(Fields.Editable.ToString(), "Editable");
                    IdFormDictionary.Add(Fields.StartEditable.ToString(), "StartEditable");
                    IdFormDictionary.Add(Fields.DurationEditable.ToString(), "DurationEditable");
                    IdFormDictionary.Add(Fields.Color.ToString(), "Color");
                    IdFormDictionary.Add(Fields.BackgroundColor.ToString(), "BackgroundColor");
                    IdFormDictionary.Add(Fields.BorderColor.ToString(), "BorderColor");
                    IdFormDictionary.Add(Fields.TextColor.ToString(), "TextColor");
                }
                return _IdFormDictionary;
            }
            set { _IdFormDictionary = value; }

        }

        private bool _Editable = false;
        public bool Editable
        {
            get { return _Editable; }
            set { _Editable = value; }
        }

        private bool _EnableAddEvent = false;
        public bool EnableAddEvent
        {
            get { return _EnableAddEvent; }
            set { _EnableAddEvent = value; }
        }

        private bool _EnableDeleteEvent = false;
        public bool EnableDeleteEvent
        {
            get { return _EnableDeleteEvent; }
            set { _EnableDeleteEvent = value; }
        }

        private VfManager _EventoForm = null;
        public VfManager EventoForm
        {
            get
            {
                if (_EventoForm != null)
                    return _EventoForm;
                else
                    return null;
            }
            set
            {
                _EventoForm = value;
                _EventoForm.Fields.Add(EventID);
                _EventoForm.SubmitButton.Style.Add(HtmlTextWriterStyle.Display, "none");
                _EventoForm.SubmitButton.ID = "submitEvento";
            }
        }

        private DeleteEventoForm _DeleteEventoForm;
        public DeleteEventoForm DeleteEventoForm
        {
            get
            {
                if (_DeleteEventoForm == null && EliminaEvento_Click != null)
                {
                    _DeleteEventoForm = new DeleteEventoForm("DeleteEventoForm");
                    _DeleteEventoForm.SubmitButton.Style.Add(HtmlTextWriterStyle.Display, "none");
                    _DeleteEventoForm.SubmitButton.ID = "deleteEvento";
                    _DeleteEventoForm.ShowMandatoryInfo = false;
                    _DeleteEventoForm.SubmitButton.Click += new EventHandler(EliminaEvento_Click);
                }
                return _DeleteEventoForm;
            }
            set { _DeleteEventoForm = value; }
        }

        private EventHandler _EliminaEvento_Click;
        public EventHandler EliminaEvento_Click
        {
            get { return _EliminaEvento_Click; }
            set { _EliminaEvento_Click = value; }
        }

        public NetCalendar()
            : base(HtmlTextWriterTag.Div)
        { }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            // CSS include
            HtmlLink jquery_ui_min = new HtmlLink();
            jquery_ui_min.Href = NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/admin/netcalendar/lib/cupertino/jquery-ui.min.css";
            jquery_ui_min.Attributes.Add("rel", "stylesheet");
            jquery_ui_min.Attributes.Add("type", "text/css");

            HtmlLink fullcalendar = new HtmlLink();
            fullcalendar.Href = NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/admin/netcalendar/fullcalendar.css";
            fullcalendar.Attributes.Add("rel", "stylesheet");
            fullcalendar.Attributes.Add("type", "text/css");

            HtmlLink fullcalendar_print = new HtmlLink();
            fullcalendar_print.Href = NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/admin/netcalendar/fullcalendar.print.css";
            fullcalendar_print.Attributes.Add("rel", "stylesheet");
            fullcalendar_print.Attributes.Add("type", "text/css");
            fullcalendar_print.Attributes.Add("media", "print");

            HtmlLink gestioneassociazioni = new HtmlLink();
            gestioneassociazioni.Href = NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/admin/netcalendar/netcalendar.css";
            gestioneassociazioni.Attributes.Add("rel", "stylesheet");
            gestioneassociazioni.Attributes.Add("type", "text/css");

            this.Page.Header.Controls.Add(jquery_ui_min);
            this.Page.Header.Controls.Add(fullcalendar);
            this.Page.Header.Controls.Add(fullcalendar_print);
            this.Page.Header.Controls.Add(gestioneassociazioni);

            // Javascript include
            string moment_minScriptUrl = NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/admin/netcalendar/lib/moment.min.js";
            string fullcalendarScriptUrl = NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/admin/netcalendar/fullcalendar.js";
            string lang_allScriptUrl = NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/admin/netcalendar/lang-all.js";
            string codeScriptUrl = NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/admin/netcalendar/code.js";
            string eventsScriptUrl = NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/admin/netcalendar/events.js";

            Page.ClientScript.RegisterClientScriptInclude("Moment_min", moment_minScriptUrl);
            Page.ClientScript.RegisterClientScriptInclude("Fullcalendar", fullcalendarScriptUrl);
            Page.ClientScript.RegisterClientScriptInclude("Lang_all", lang_allScriptUrl);
            Page.ClientScript.RegisterClientScriptInclude("Code", codeScriptUrl);
            Page.ClientScript.RegisterClientScriptInclude("Event", eventsScriptUrl);

            this.Controls.Add(getWebControl());
        }

        private WebControl getWebControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);
            control.ID = "container-calendar";

            WebControl calendar = new WebControl(HtmlTextWriterTag.Div);
            calendar.ID = "calendar";
            control.Controls.Add(calendar);

            if (EnableAddEvent && EventoForm != null)
                control.Controls.Add(BuildButtons());

            WebControl divModaleFormEvento = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
            divModaleFormEvento.ID = "dialogEvento";
            divModaleFormEvento.Style.Add(HtmlTextWriterStyle.Display, "none");
            divModaleFormEvento.CssClass = "divModaleFormEvento";

            if (EventoForm != null)
                divModaleFormEvento.Controls.Add(EventoForm);
            else
                this.Editable = false;

            if (EnableDeleteEvent && DeleteEventoForm != null)
                divModaleFormEvento.Controls.Add(DeleteEventoForm);

            control.Controls.Add(divModaleFormEvento);
            control.Controls.Add(new LiteralControl(jsStart()));

            return control;
        }

        private string SerializeEventi()
        {
            if (_Eventi != null && _Eventi.Count > 0)
            {
                DataContractJsonSerializer js = new DataContractJsonSerializer(_Eventi.GetType());
                MemoryStream ms = new MemoryStream();
                js.WriteObject(ms, _Eventi);
                ms.Position = 0;
                StreamReader sr = new StreamReader(ms);
                return sr.ReadToEnd();
            }
            else
                return "''";
        }

        private WebControl BuildButtons()
        {
            WebControl divButtons = new WebControl(HtmlTextWriterTag.Div);
            divButtons.CssClass = "operazioni_valida_respingi";

            Button addButton = new Button();
            addButton.ID = "add_button";
            addButton.CssClass = "insert button_yellow";
            addButton.Text = "Nuovo Evento";
            divButtons.Controls.Add(addButton);

            return divButtons;
        }

        private string jsStart()
        {
            string toPass = SerializeEventi() + ",'" + Editable + "'";

            foreach (Fields field in (Fields[])Enum.GetValues(typeof(Fields)))
            {
                toPass += ",'";
                if (IdFormDictionary.ContainsKey(field.ToString()))
                    toPass += IdFormDictionary[field.ToString()];
                toPass += "'";
            }

            string eventoID = "";
            if (EventoForm != null)
                eventoID = EventoForm.ID;            

            return @"
            <script type=""text/javascript"">  
                $(document).ready(function() {	
		            renderCalendar(" + toPass + @");
                    start('" + eventoID + "','" + EnableDeleteEvent + @"');
		        });		
            </script>";
        }

        private VfTextBox EventID
        {
            get
            {
                if (_EventID == null)
                {
                    _EventID = new VfTextBox("EventID", "EventID", "EventID", InputTypes.Text);
                    _EventID.Attributes.Add("style", "display:none");
                    _EventID.BindField = false;
                }
                return _EventID;
            }
        }
        private VfTextBox _EventID;
    }

    public class DeleteEventoForm : VfManager
    {
        public DeleteEventoForm(string controlId)
            : base(controlId)
        { }
    }

    public static class CalendarUtility
    {
        public static int GetIdFromForm(VfManager manager)
        {
            string EventID = manager.Fields.Find(x => x.Key == "EventID").PostBackValue;

            if (EventID != "")
            {
                try
                { return int.Parse(EventID); }
                catch
                { return 0; }
            }
            else
                return 0;
        }

        public static VfManager CleanForm(VfManager manager)
        {
            manager.Fields.Remove(manager.Fields.Find(x => x.Key == "EventID"));
            return manager;
        }
    }
}
