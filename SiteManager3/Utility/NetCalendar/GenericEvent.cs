﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NetCalendar
{
    [DataContract]
    public abstract class GenericEvent
    {
        public GenericEvent() { }

        [DataMember(Name = "id", IsRequired = true)]
        public virtual int ID { get; set; }

        [DataMember(Name = "title", IsRequired = true)]
        public virtual string Title { get; set; }

        [DataMember(Name = "allDay", IsRequired = false)]
        public virtual bool AllDay { get; set; }

        [DataMember(Name = "start", IsRequired = true)]
        public virtual DateTime Start { get; set; }

        [DataMember(Name = "end", IsRequired = false)]
        public virtual DateTime End { get; set; }

        [DataMember(Name = "url", IsRequired = false)]
        public virtual string Url { get; set; }

        [DataMember(Name = "className", IsRequired = false)]
        public virtual string ClassName { get; set; }

        [DataMember(Name = "editable", IsRequired = false)]
        public virtual bool Editable { get; set; }

        [DataMember(Name = "startEditable", IsRequired = false)]
        public virtual bool StartEditable { get; set; }

        [DataMember(Name = "durationEditable", IsRequired = false)]
        public virtual bool DurationEditable { get; set; }

        [DataMember(Name = "color", IsRequired = false)]
        public virtual string Color { get; set; }

        [DataMember(Name = "backgroundColor", IsRequired = false)]
        public virtual string BackgroundColor { get; set; }

        [DataMember(Name = "borderColor", IsRequired = false)]
        public virtual string BorderColor { get; set; }

        [DataMember(Name = "textColor", IsRequired = false)]
        public virtual string TextColor { get; set; }

        public enum Columns
        {
            ID,
            Title,
            AllDay,
            Start,
            End,
            Url,
            ClassName,
            Editable,
            StartEditable,
            DurationEditable,
            Color,
            BackgroundColor,
            BorderColor,
            TextColor
        }
    }
}

