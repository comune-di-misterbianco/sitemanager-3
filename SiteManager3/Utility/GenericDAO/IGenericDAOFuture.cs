﻿using System.Collections;
using System.Collections.Generic;
using NetService.Utility.RecordsFinder;
using NHibernate;

namespace GenericDAO.DAO
{
    public interface IGenericDAOFuture<T>
    {
        IFutureValue<T> GetById(int id);
        IFutureValue<T> GetById(string id);

        IEnumerable<T> FindAll();
        IEnumerable<T> FindAll(int pageStart, int pageSize, string sortBy, bool descending);
        IEnumerable<T> FindAll(int pageStart, int pageSize);
        IEnumerable<T> FindAll(string sortBy, bool descending);

        IEnumerable<T> FindByCriteria(SearchParameter[] criteria);
        IEnumerable<T> FindByCriteria(string type, SearchParameter[] criteria);
        IEnumerable<T> FindByCriteria(int pageStart, int pageSize, string type, SearchParameter[] criteria);
        IEnumerable<T> FindByCriteria(int pageStart, int pageSize, string sortBy, bool descending, SearchParameter[] criteria);
        IEnumerable<T> FindByCriteria(int pageStart, int pageSize, IList<string> sortBy, bool descending, SearchParameter[] criteria);
        IEnumerable<T> FindByCriteria(int pageStart, int pageSize, string sortBy, bool descending, SearchParameter[] criteria, bool distinct);
        IEnumerable<T> FindByCriteria(int pageStart, int pageSize, IList<string> sortBy, bool descending, SearchParameter[] criteria, bool distinct);
        IEnumerable<T> FindByCriteria(int pageStart, int pageSize, SearchParameter[] criteria);
        IEnumerable<T> FindByCriteria(string sortBy, bool descending, SearchParameter[] criteria);
        IEnumerable<T> FindByCriteria(IList<string> sortBy, bool descending, SearchParameter[] criteria);
        IEnumerable<T> FindByCriteria(IDictionary<string, bool> sortByWithDescending, SearchParameter[] criteria);
        IEnumerable<T> FindByCriteria(IDictionary<string, bool> sortByWithDescending, SearchParameter[] criteria, bool distinct);
        IEnumerable<T> FindByCriteria(int pageStart, int pageSize, IDictionary<string, bool> sortByWithDescending, SearchParameter[] criteria);
        IEnumerable<T> FindByCriteria(string sortBy, bool descending, SearchParameter[] criteria, bool distinct);
        IEnumerable<T> FindByCriteria(SearchParameter[] criteria, bool distinct);

        IFutureValue<T> GetByCriteria(SearchParameter[] criteria);
        object GetPropertyValueByGroupParameter(SearchParameter[] criteria);

        IFutureValue<int> FindByCriteriaCount(SearchParameter[] criteria);
        IFutureValue<int> FindByCriteriaCount(SearchParameter[] criteria, bool distinct);
        IFutureValue<int> FindByCriteriaCount(string type, SearchParameter[] criteria);

        T SaveOrUpdate(T entity);
        void Delete(T entity);
    }
}
