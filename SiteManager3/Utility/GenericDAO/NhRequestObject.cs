﻿using System;
using System.Collections.Specialized;
using System.Web;
using GenericDAO.DAO;
using NHibernate;
using NetService.Utility.Common;

namespace GenericDAO.DAO.Utility
{
    public class NhRequestObject<T>
    {
        public string RequestKey { get; private set; }
        public RequestVariable.RequestType RequestType { get; private set; }
        public ISession Session { get; private set; }

        public T Instance
        {
            get
            {
                if (_ObjectInstance == null)
                {
                    _ObjectInstance = GetInstance();
                }
                return _ObjectInstance;
            }
        }
        private T _ObjectInstance;

        public bool Exists
        {
            get
            {
                return Instance != null;
            }
        }

        public NetService.Utility.Common.RequestVariable Request
        {
            get
            {
                if (_Request == null)
                {
                    _Request = new NetService.Utility.Common.RequestVariable(RequestKey, RequestType);
                }
                return _Request;
            }
        }
        private NetService.Utility.Common.RequestVariable _Request;

        public NhRequestObject(ISession session, string requestKey)
            : this(session, requestKey, RequestVariable.RequestType.QueryString)
        {

        }
        public NhRequestObject(ISession session, string requestKey, RequestVariable.RequestType requestType)
        {
            this.Session = session;
            this.RequestKey = requestKey;
            this.RequestType = requestType;
        }

        private T GetInstance()
        {
            IGenericDAO<T> dao = new CriteriaNhibernateDAO<T>(Session);

            if (Request.IsValidInteger)
            {
                return dao.GetById(Request.IntValue);
            }           

            return default(T);
        }
    }
}
