﻿using System.Collections;
using System.Collections.Generic;
using NetService.Utility.RecordsFinder;

namespace GenericDAO.DAO
{
    public interface IGenericDAO<T>
    {
        T GetById(int id);
        T GetById(string id);

        IList<T> FindAll();
        IList<T> FindAll(int pageStart, int pageSize, string sortBy, bool descending);
        IList<T> FindAll(int pageStart, int pageSize);
        IList<T> FindAll(string sortBy, bool descending);

        IList<T> FindByCriteria(SearchParameter[] criteria);
        IList<T> FindByCriteria(string type, SearchParameter[] criteria);
        IList<T> FindByCriteria(int pageStart, int pageSize, string type, SearchParameter[] criteria);
        IList<T> FindByCriteria(int pageStart, int pageSize, string sortBy, bool descending, SearchParameter[] criteria);
        IList<T> FindByCriteria(int pageStart, int pageSize, string sortByCollection, string sortByProperty, bool descending, SearchParameter[] criteria, bool distinct = false);
        IList<T> FindByCriteria(int pageStart, int pageSize, IList<string> sortBy, bool descending, SearchParameter[] criteria);
        IList<T> FindByCriteria(int pageStart, int pageSize, string sortBy, bool descending, SearchParameter[] criteria, bool distinct);
        IList<T> FindByCriteria(int pageStart, int pageSize, IList<string> sortBy, bool descending, SearchParameter[] criteria, bool distinct);
        IList<T> FindByCriteria(int pageStart, int pageSize, SearchParameter[] criteria);
        IList<T> FindByCriteria(string sortBy, bool descending, SearchParameter[] criteria);
        IList<T> FindByCriteria(IList<string> sortBy, bool descending, SearchParameter[] criteria);
        IList<T> FindByCriteria(IDictionary<string, bool> sortByWithDescending, SearchParameter[] criteria);
        IList<T> FindByCriteria(IDictionary<string, bool> sortByWithDescending, SearchParameter[] criteria, bool distinct);
        IList<T> FindByCriteria(int pageStart, int pageSize, IDictionary<string, bool> sortByWithDescending, SearchParameter[] criteria);
        IList<T> FindByCriteria(int pageStart, int pageSize, IDictionary<string, bool> sortByWithDescending, SearchParameter[] criteria, bool distinct);
        IList<T> FindByCriteria(string sortBy, bool descending, SearchParameter[] criteria, bool distinct);
        IList<T> FindByCriteria(SearchParameter[] criteria, bool distinct);

        T GetByCriteria(SearchParameter[] criteria);
        object GetPropertyValueByGroupParameter(SearchParameter[] criteria);

        int FindByCriteriaCount(SearchParameter[] criteria);
        int FindByCriteriaCount(SearchParameter[] criteria, bool distinct);
        int FindByCriteriaCount(string type, SearchParameter[] criteria);

        T SaveOrUpdate(T entity);
        void Delete(T entity);
    }
}
