﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;

namespace NetService.Utility.RecordsFinder
{
    public class FileSystemFinder : Finder
    {
        public DirectoryInfo Folder { get; private set; }
        public bool ShowFolders { get; set; }
        public bool ShowFiles { get; set; }
        public string FoldersSearchPattern { get; set; }
        public string FilesSearchPattern { get; set; } 

        public FileSystemFinder(string folderPath,List<SearchParameter> searchParameters = null)
            : base(searchParameters)
        {
            ShowFolders = true;
            ShowFiles = true;
            if (folderPath.Contains("\\"))
                Folder = new DirectoryInfo(folderPath);
            else
                Folder = new DirectoryInfo(HttpContext.Current.Server.MapPath(folderPath));
        }

        public override Array Find()
        {
            return GetElements().ToArray();
        }

        private List<FileSystemInfo> GetElements()
        {
            List<FileSystemInfo> elements = new List<FileSystemInfo>();

            if (Folder.Exists)
            {
                if (ShowFolders)
                {
                    if (string.IsNullOrEmpty(FoldersSearchPattern))
                        elements.AddRange(Folder.GetDirectories());
                    else
                        elements.AddRange(Folder.GetDirectories(FoldersSearchPattern));
                }
                if (ShowFiles)
                {
                    if (string.IsNullOrEmpty(FilesSearchPattern))
                        elements.AddRange(Folder.GetFiles());
                    else
                        elements.AddRange(Folder.GetFiles(FilesSearchPattern));
                }
            }

            return elements;
        }

        public override Array Find(int firstResult, int maxResults)
        {
            return GetElements().Skip(firstResult)
                                .Take(maxResults)
                                .ToArray();
        }

        public override int Count()
        {
            return GetElements().Count;
        }

    }
}
