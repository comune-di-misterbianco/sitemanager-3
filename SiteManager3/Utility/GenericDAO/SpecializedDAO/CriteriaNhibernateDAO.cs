﻿using System;
using System.Collections;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using System.Linq;
using NetService.Utility.RecordsFinder;
using System.Reflection;
using NHibernate.Transform;

namespace GenericDAO.DAO
{
    public class CriteriaNhibernateDAO<T> : IGenericDAO<T>
    {
        private ISession _Session;
        private Type persistentType = typeof(T);

        public CriteriaNhibernateDAO(ISession session)
        {
            _Session = session;
        }
               
        /// <summary>
        /// Metodo che restituisce un oggetto di tipo T dato l'id in formato int.
        /// </summary>
        /// <param name="id">Id dell'oggetto in formato int.</param>
        /// <returns>Oggetto di tipo T con id corrispondente.</returns>
        public T GetById(int id)
        {
            T entity = default(T);
            try
            {
                ISession session = _Session;
                entity = (T)session.Get(persistentType, id);
            }
            catch (Exception ex) 
            {                
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca di un entità di tipo " + typeof(T) + " con id: " + id.ToString() + ". " + ex.Message);
                return default(T);
            }
            return entity;
        }

        /// <summary>
        /// Metodo che restituisce un oggetto di tipo T dato l'id in formato string.
        /// </summary>
        /// <param name="id">Id dell'oggetto in formato string.</param>
        /// <returns>Oggetto di tipo T con id corrispondente.</returns>
        public T GetById(string id)
        {
            T entity = default(T);
            try
            {
                ISession session = _Session;
                entity = (T)session.Get(persistentType, id);
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca di un entità di tipo " + typeof(T) + " con id: " + id.ToString() + ". " + ex.Message);
                return default(T);
            }
            return entity;
        }


        /// <summary>
        /// Metodo che restituisce una lista di tutti oggetti di tipo T
        /// </summary>
        /// <returns>Lista di oggetti di tipo T.</returns>
        public IList<T> FindAll()
        {
            try
            {
                ICriteria query = BuildCriteria(null);
                return query.List<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Metodo per cancellare un oggetto di tipo T
        /// </summary>
        /// <param name="entity">Oggetto di tipo T.</param>
        public void Delete(T entity)
        {
            try
            {
                ISession session = _Session;
                session.Delete(entity);
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la cancellazione di un'entità di tipo " + entity.GetType().Name + ". " + ex.Message);
            }
        }

        /// <summary>
        /// Metodo per il salvataggio di un oggetto di tipo T
        /// </summary>
        /// <param name="entity">Oggetto di tipo T</param>
        /// <returns>Oggetto di tipo T salvato</returns>
        public T SaveOrUpdate(T entity)
        {
            try
            {
                ISession session = _Session;
                session.SaveOrUpdate(entity);
                return entity;
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante il salvataggio o l'update di un'entità di tipo " + entity.GetType().Name + ". " + ex.Message);
                return default(T);
            }
        }

        /// <summary>
        /// Metodo per effettuare una ricerca su oggetti di tipo T, con la possibilità di impostare eventuali ordinamenti sui criteri di ricerca, senza distinct.
        /// </summary>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <param name="enableCache">Parametro per abilitare la cache di secondo livello.</param>
        /// <returns>ICriteria generato</returns>
        public ICriteria BuildCriteria(SearchParameter[] criteria, bool enableCache = true)
        {
            ICriteria query = null;
            if (enableCache)
                query = _Session.CreateCriteria(persistentType).SetCacheable(true).SetCacheMode(CacheMode.Normal);
            else
                query = _Session.CreateCriteria(persistentType);
            //query.SetTimeout(60);

            if (criteria == null || criteria.Length == 0)
            {
                return query;
            }

            foreach (SearchParameter parameter in criteria.Where(x => x.AddToCriteria))
            {
                if (parameter.Value is SearchParameter[] && parameter.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                {
                    if (parameter.Behaviour != SearchParameter.BehaviourClause.Default)
                        ElapseJunctionParameter(query, parameter);
                    else
                        ElapseNonLeafParameter(query, parameter);
                        
                                        
                }
                else
                {
                    if (parameter.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                        ElapseLeafParameter(query, parameter);
                    else
                    {
                        if (parameter.ReturnPropertyValue)
                        {
                            ElapseGroupParameterProperty(query, parameter);
                        }
                        else
                            ElapseGroupParameter(query, parameter);
                    }
                }
            }

            return query;
        }

        /// <summary>
        /// Metodo da utilizzare per ottenere una lista di oggetti di tipo T nel caso in cui si voglia applicare il distinct.
        /// </summary>
        /// <param name="pageStart">Indice del record da cui partire. (Deve essere sempre numero pagina * record per pagina oppure -1 per non applicare paginazione).</param>
        /// <param name="pageSize">Numero di record per pagina oppure zero per non attiavare paginazione.</param>
        /// <param name="sortByWithDescending">Dizionario contenente eventuali ordinamenti da effettuare su proprietà dell'oggetto base. Può essere null o lista vuota.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <param name="enableCache">Parametro per abilitare il cache di secondo livello per le query. Nel caso di distinct potrebbe creare problemi di refresh (verificare)i</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> BuildCriteriaForDistinct(int pageStart, int pageSize, IDictionary<string, bool> sortByWithDescending, SearchParameter[] criteria, bool enableCache = true)
        {
            ICriteria query = null;

            if (enableCache)
                query = _Session.CreateCriteria(persistentType).SetCacheable(true).SetCacheMode(CacheMode.Normal);
            else
                query = _Session.CreateCriteria(persistentType);
            //query.SetTimeout(60);

            if (criteria == null || criteria.Length == 0)
            {
                if (sortByWithDescending != null && sortByWithDescending.Count > 0)
                {
                    foreach (KeyValuePair<string, bool> sort in sortByWithDescending)
                    {
                        query.AddOrder(new Order(sort.Key, !sort.Value));
                    }
                }

                if (pageStart >= 0 && pageSize > 0)
                    return query.SetFirstResult(pageStart).SetMaxResults(pageSize).List<T>();

                return query.List<T>();
            }
            
            DetachedCriteria subCriteria = BuildDetachedCriteriaForDistinct(criteria);

            if (sortByWithDescending != null && sortByWithDescending.Count > 0)
            {
                foreach (KeyValuePair<string, bool> sort in sortByWithDescending)
                {
                    subCriteria.AddOrder(new Order(sort.Key, !sort.Value));
                }
            }
            List<int> ids = new List<int>();

            if (pageStart >= 0 && pageSize > 0)
            {
                ids = subCriteria.GetExecutableCriteria(_Session).List<int>().Distinct().Skip(pageStart).Take(pageSize).ToList();
            }
            else
                ids = subCriteria.GetExecutableCriteria(_Session).List<int>().Distinct().ToList();
            
            List<T> result = new List<T>();

            query.Add(Expression.In(_Session.SessionFactory.GetClassMetadata(typeof(T)).IdentifierPropertyName, ids));

            IList<T> notOrdered = query.List<T>();

            foreach (int id in ids)
                result.Add(_Session.Get<T>(id));            

            return result;
        }


        /// <summary>
        /// Questo metodo crea un detached criteria che tiene conto di tutti i  seach parameters impostati e serve sia come supporto al metodo di build criteria, sia per qualsiasi metodo count che preveda i distinct.
        /// </summary>
        /// <param name="criteria">Criteri di ricerca impostati.</param>
        /// <returns>Un detached criteria da utilizzare per filtrare i risultati della query principale sugli oggetti che fanno parte del tipo T nel caso di distinct.</returns>
        private DetachedCriteria BuildDetachedCriteriaForDistinct(SearchParameter[] criteria)
        {

            DetachedCriteria subCriteria = DetachedCriteria.For(persistentType);

            foreach (SearchParameter parameter in criteria.Where(x => x.AddToCriteria))
            {
                if (parameter.Value is SearchParameter[] && parameter.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                {
                    if (parameter.Behaviour != SearchParameter.BehaviourClause.Default)
                        ElapseJunctionParameter(subCriteria, parameter);
                    else
                        ElapseNonLeafParameter(subCriteria, parameter);


                }
                else
                {
                    if (parameter.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                        ElapseLeafParameter(subCriteria, parameter);
                    else
                    {
                        if (parameter.ReturnPropertyValue)
                        {
                            ElapseGroupParameterProperty(subCriteria, parameter);
                        }
                        else
                            ElapseGroupParameter(subCriteria, parameter);
                    }
                }
            }
            subCriteria.SetProjection(NHibernate.Criterion.Projections.Id());

            return subCriteria;
        }

        /// <summary>
        /// Metodo per effettuare una ricerca su oggetti di tipo type, con la possibilità di impostare eventuali ordinamenti sui criteri di ricerca, senza distinct.
        /// </summary>
        /// <param name="type">Tipo degli oggetti su cui fare la ricerca.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <param name="enableCache">Parametro per abilitare la cache di secondo livello.</param>
        /// <returns>ICriteria generato</returns>
        private ICriteria BuildCriteria(string type, SearchParameter[] criteria, bool enableCache = true)
        {
            ICriteria query = null;

            if (enableCache)
                query = _Session.CreateCriteria(type).SetCacheable(true).SetCacheMode(CacheMode.Normal);
            else
                query = _Session.CreateCriteria(type);
            //query.SetTimeout(60);

            if (criteria == null || criteria.Length == 0)
            {
                return query;
            }

            foreach (SearchParameter parameter in criteria.Where(x => x.AddToCriteria))
            {
                if (parameter.Value is SearchParameter[] && parameter.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                {
                    if (parameter.Behaviour != SearchParameter.BehaviourClause.Default)
                        ElapseJunctionParameter(query, parameter);
                    else
                        ElapseNonLeafParameter(query, parameter);

                }
                else
                {
                    if (parameter.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                        ElapseLeafParameter(query, parameter);
                    else
                    {
                        if (parameter.ReturnPropertyValue)
                        {
                            ElapseGroupParameterProperty(query, parameter);
                        }
                        else
                            ElapseGroupParameter(query, parameter);
                    }
                }
            }

            return query;
        }

        /// <summary>
        /// Metodo che restituisce una lista di oggetti di tipo T con possibilità di paginazione e ordinamento su singolo campo
        /// </summary>
        /// <param name="pageStart">Indice del record da cui partire. (Deve essere sempre numero pagina * record per pagina).</param>
        /// <param name="pageSize">Numero di record per pagina oppure zero per non attiavare paginazione.</param>
        /// <param name="sortBy">Proprietà dell'oggetto base su cui fare l'ordinamento.</param>
        /// <param name="descending">true se ordinamento discendente, false se ascendente.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindAll(int pageStart, int pageSize, string sortBy, bool descending)
        {
            try
            {
                ICriteria query = BuildCriteria(null);
                return query.AddOrder(new Order(sortBy, !descending)).SetFirstResult(pageStart).SetMaxResults(pageSize).List<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Metodo che restituisce una lista di oggetti di tipo T con possibilità di paginazione
        /// </summary>
        /// <param name="pageStart">Indice del record da cui partire. (Deve essere sempre numero pagina * record per pagina).</param>
        /// <param name="pageSize">Numero di record per pagina oppure zero per non attiavare paginazione.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindAll(int pageStart, int pageSize)
        {
            try
            {
                ICriteria query = BuildCriteria(null);
                return query.SetFirstResult(pageStart).SetMaxResults(pageSize).List<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Metodo che restituisce una lista di oggetti di tipo T con possibilità di ordinamento su singolo campo
        /// </summary>
        /// <param name="sortBy">Proprietà dell'oggetto base su cui fare l'ordinamento.</param>
        /// <param name="descending">true se ordinamento discendente, false se ascendente.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindAll(string sortBy, bool descending) 
        {
            try
            {
                ICriteria query = BuildCriteria(null);
                return query.AddOrder(new Order(sortBy, !descending)).List<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Metodo per effettuare una ricerca su oggetti di tipo T, con la possibilità di impostare eventuali ordinamenti sui criteri di ricerca, senza distinct.
        /// </summary>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                return query.List<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
            
        }

        /// <summary>
        /// Metodo usato per le sessioni di tipo Map
        /// </summary>
        /// <param name="type">Tipo di istanza da usare.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(string type, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(type, criteria);
                return query.List<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }

        }

        /// <summary>
        /// Metodo usato per le sessioni di tipo Map
        /// </summary>
        /// <param name="pageStart">Indice del record da cui partire. (Deve essere sempre numero pagina * record per pagina).</param>
        /// <param name="pageSize">Numero di record per pagina oppure zero per non attiavare paginazione.</param>
        /// <param name="type">Tipo di istanza da usare.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(int pageStart, int pageSize, string type, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(type, criteria);
                return query.SetFirstResult(pageStart).SetMaxResults(pageSize).List<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Metodo per effettuare una ricerca su oggetti di tipo T, con la possibilità di impostare eventuali ordinamenti sui criteri di ricerca, con distinct.
        /// </summary>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(SearchParameter[] criteria, bool distinct)
        {
            try
            {
                if (!distinct)
                    return FindByCriteria(criteria);
                else
                {
                    
                    return BuildCriteriaForDistinct(-1, 0, null, criteria, !distinct);
                }
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }

        }

        /// <summary>
        /// Metodo che restituisce una lista di oggetti di tipo T con possibilità di paginazione e ordinamento su singolo campo, senza distinct
        /// </summary>
        /// <param name="pageStart">Indice del record da cui partire. (Deve essere sempre numero pagina * record per pagina).</param>
        /// <param name="pageSize">Numero di record per pagina oppure zero per non attiavare paginazione.</param>
        /// <param name="sortBy">Proprietà dell'oggetto base su cui fare l'ordinamento.</param>
        /// <param name="descending">true se ordinamento discendente, false se ascendente.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(int pageStart, int pageSize, string sortBy, bool descending, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                return query.AddOrder(new Order(sortBy,!descending)).SetFirstResult(pageStart).SetMaxResults(pageSize).List<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Questo FindByCriteria consente di ordinare usando una proprietà all'interno di una collezione specificata contenuta nell'oggetto, senza distinct
        /// </summary>
        /// <param name="pageStart">Indice del record da cui partire. (Deve essere sempre numero pagina * record per pagina).</param>
        /// <param name="pageSize">Numero di record per pagina oppure zero per non attiavare paginazione.</param>
        /// <param name="sortByCollection">Nome della collezione di oggetti appartenenti all'oggetto di tipo T che contiene la proprietà su cui fare l'ordinamento.</param>
        /// <param name="sortByProperty">Nome della proprietà appartenente alla collezione, su cui fare l'ordinamento.</param>
        /// <param name="descending">true se ordinamento discendente, false se ascendente.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <param name="distinct">True se si vuole ottenere risultati distinti. Di default è false. Aggiunto a seguito del supporto multilingua.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(int pageStart, int pageSize, string sortByCollection, string sortByProperty, bool descending, SearchParameter[] criteria, bool distinct = false)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                if (!distinct)
                    return query.CreateAlias(sortByCollection, "col", NHibernate.SqlCommand.JoinType.InnerJoin).AddOrder(new Order("col." + sortByProperty, !descending)).SetFirstResult(pageStart).SetMaxResults(pageSize).List<T>();
                IList<T> temp = query.CreateAlias(sortByCollection, "col", NHibernate.SqlCommand.JoinType.InnerJoin).AddOrder(new Order("col." + sortByProperty, !descending)).List<T>();
                return temp.Distinct().Skip(pageStart * pageSize).Take(pageSize).ToList<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Metodo che consente di ordinare oggetti di tipo T sulla base di proprietà specificate in una lista, con possibilità di ricerca senza distinct
        /// </summary>
        /// <param name="pageStart">Indice del record da cui partire. (Deve essere sempre numero pagina * record per pagina).</param>
        /// <param name="pageSize">Numero di record per pagina oppure zero per non attiavare paginazione.</param>
        /// <param name="sortBy">Lista di proprietà da utilizzare per fare l'ordinamento.</param>
        /// <param name="descending">true se ordinamento discendente, false se ascendente.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(int pageStart, int pageSize, IList<string> sortBy, bool descending, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);

                foreach (string sort in sortBy) 
                {
                    query.AddOrder(new Order(sort, !descending));
                }

                return query.SetFirstResult(pageStart).SetMaxResults(pageSize).List<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        
        /// <summary>
        /// Metodo che restituisce una lista di oggetti di tipo T con possibilità di paginazione e ordinamento su singolo campo, con distinct
        /// </summary>
        /// <param name="pageStart">Indice del record da cui partire. (Deve essere sempre numero pagina * record per pagina).</param>
        /// <param name="pageSize">Numero di record per pagina oppure zero per non attiavare paginazione.</param>
        /// <param name="sortBy">Proprietà dell'oggetto base su cui fare l'ordinamento.</param>
        /// <param name="descending">true se ordinamento discendente, false se ascendente.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <param name="distinct">Parametro per attivare o meno la distinct.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(int pageStart, int pageSize, string sortBy, bool descending, SearchParameter[] criteria, bool distinct)
        {
            try
            {
                if (!distinct)
                    return FindByCriteria(pageStart, pageSize, sortBy, descending, criteria);
                else
                {
                    Dictionary<string, bool> sortByWithDescending = new Dictionary<string, bool>();
                    
                    sortByWithDescending.Add(sortBy, descending);
                    
                    return BuildCriteriaForDistinct(pageStart, pageSize, sortByWithDescending, criteria, !distinct);
                }
                
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Metodo che consente di ordinare oggetti di tipo T sulla base di proprietà specificate in una lista, con possibilità di ricerca senza distinct
        /// </summary>
        /// <param name="pageStart">Indice del record da cui partire. (Deve essere sempre numero pagina * record per pagina).</param>
        /// <param name="pageSize">Numero di record per pagina oppure zero per non attiavare paginazione.</param>
        /// <param name="sortBy">Lista di proprietà da utilizzare per fare l'ordinamento.</param>
        /// <param name="descending">true se ordinamento discendente, false se ascendente.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <param name="distinct">Parametro per attivare o meno la distinct.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(int pageStart, int pageSize, IList<string> sortBy, bool descending, SearchParameter[] criteria, bool distinct)
        {
            try
            {
                if (!distinct)
                    return FindByCriteria(pageStart, pageSize, sortBy, descending, criteria);
                else
                {
                    Dictionary<string, bool> sortByWithDescending = new Dictionary<string, bool>();
                    foreach (string sort in sortBy)
                    {
                        sortByWithDescending.Add(sort, descending);
                    }
                    return BuildCriteriaForDistinct(pageStart,pageSize, sortByWithDescending, criteria, !distinct);
                }

            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        
        /// <summary>
        /// Metodo che restituisce una lista di oggetti di tipo T, paginati e con possibilità di ricerca, senza distinct
        /// </summary>
        /// <param name="pageStart">Indice del record da cui partire. (Deve essere sempre numero pagina * record per pagina).</param>
        /// <param name="pageSize">Numero di record per pagina oppure zero per non attiavare paginazione.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(int pageStart, int pageSize, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                return query.SetFirstResult(pageStart).SetMaxResults(pageSize).List<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Metodo che restituisce una lista di oggetti di tipo T con possibilità di ordinamento su singolo campo, senza distinct
        /// </summary>
        /// <param name="sortBy">Proprietà dell'oggetto base su cui fare l'ordinamento.</param>
        /// <param name="descending">true se ordinamento discendente, false se ascendente.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(string sortBy, bool descending, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                return query.AddOrder(new Order(sortBy, !descending)).List<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Metodo che consente di ordinare oggetti di tipo T sulla base di proprietà specificate in una lista, con possibilità di ricerca senza distinct
        /// </summary>
        /// <param name="sortBy">Lista di proprietà da utilizzare per fare l'ordinamento.</param>
        /// <param name="descending">true se ordinamento discendente, false se ascendente.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(IList<string> sortBy, bool descending, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                foreach (string sort in sortBy)
                {
                    query.AddOrder(new Order(sort, !descending));
                }
                return query.List<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }


        /// <summary>
        /// Metodo che consente di ordinare oggetti di tipo T sulla base di proprietà dell'oggetto T specificate in un dizionario, con possibilità di indicare l'ordinamento ASC O DESC per ciascuna. Senza distinct.
        /// </summary>
        /// <param name="sortByWithDescending">Dizionario di coppie chiave-valore in cui la chiave è il nome della proprietà e il valore un booleano true se ordinamento DESC o false se ASC.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(IDictionary<string, bool> sortByWithDescending, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                foreach (KeyValuePair<string, bool> sort in sortByWithDescending)
                {
                    query.AddOrder(new Order(sort.Key, !sort.Value));
                }
                return query.List<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Metodo che consente di ordinare oggetti di tipo T sulla base di proprietà dell'oggetto T specificate in un dizionario, con possibilità di indicare l'ordinamento ASC O DESC per ciascuna. Con distinct.
        /// </summary>
        /// <param name="sortByWithDescending">Dizionario di coppie chiave-valore in cui la chiave è il nome della proprietà e il valore un booleano true se ordinamento DESC o false se ASC.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// /// <param name="distinct">Parametro per attivare o meno la distinct.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(IDictionary<string, bool> sortByWithDescending, SearchParameter[] criteria, bool distinct)
        {
            try
            {
                if (!distinct)
                    return FindByCriteria(sortByWithDescending, criteria);
                else
                {                    
                    return BuildCriteriaForDistinct(-1, 0, sortByWithDescending, criteria, !distinct);
                }

            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Metodo che consente di paginare, filtrare e ordinare oggetti di tipo T sulla base di proprietà dell'oggetto T specificate in un dizionario, con possibilità di indicare l'ordinamento ASC O DESC per ciascuna. Senza distinct.
        /// </summary>
        /// <param name="pageStart">Indice del record da cui partire. (Deve essere sempre numero pagina * record per pagina).</param>
        /// <param name="pageSize">Numero di record per pagina oppure zero per non attiavare paginazione.</param>
        /// <param name="sortByWithDescending">Dizionario di coppie chiave-valore in cui la chiave è il nome della proprietà e il valore un booleano true se ordinamento DESC o false se ASC.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(int pageStart, int pageSize, IDictionary<string, bool> sortByWithDescending, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                foreach (KeyValuePair<string, bool> sort in sortByWithDescending)
                {
                    query.AddOrder(new Order(sort.Key, !sort.Value));
                }
                return query.SetFirstResult(pageStart).SetMaxResults(pageSize).List<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Metodo che consente di paginare, filtrare e ordinare oggetti di tipo T sulla base di proprietà dell'oggetto T specificate in un dizionario, con possibilità di indicare l'ordinamento ASC O DESC per ciascuna. Con distinct.
        /// </summary>
        /// <param name="pageStart">Indice del record da cui partire. (Deve essere sempre numero pagina * record per pagina).</param>
        /// <param name="pageSize">Numero di record per pagina oppure zero per non attiavare paginazione.</param>
        /// <param name="sortByWithDescending">Dizionario di coppie chiave-valore in cui la chiave è il nome della proprietà e il valore un booleano true se ordinamento DESC o false se ASC.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <param name="distinct">Parametro per attivare o meno la distinct.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(int pageStart, int pageSize, IDictionary<string, bool> sortByWithDescending, SearchParameter[] criteria, bool distinct)
        {
            try
            {
                if (!distinct)
                {
                    return FindByCriteria(pageStart, pageSize, sortByWithDescending, criteria);
                }
                else
                {                  
                    return BuildCriteriaForDistinct(pageStart,pageSize,sortByWithDescending,criteria);
                }
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }


        /// <summary>
        /// Metodo che restituisce una lista di oggetti di tipo T con possibilità di ordinamento su singolo campo, con distinct
        /// </summary>
        /// <param name="sortBy">Proprietà dell'oggetto base su cui fare l'ordinamento.</param>
        /// <param name="descending">true se ordinamento discendente, false se ascendente.</param>
        /// <param name="criteria">Criteri di ricerca su proprietà annidate dell'oggetto base, con la possibilità di specificare ordinamenti interni in aggiunta ai filtri.</param>
        /// <param name="distinct">Parametro per attivare o meno la distinct.</param>
        /// <returns>Lista di oggetti di tipo T</returns>
        public IList<T> FindByCriteria(string sortBy, bool descending, SearchParameter[] criteria, bool distinct)
        {
            try
            {
                if (!distinct)
                {
                    return FindByCriteria(sortBy, descending, criteria);
                }
                else
                {
                    Dictionary<string, bool> sortByWithDescending = new Dictionary<string, bool>();
                    sortByWithDescending.Add(sortBy, descending);
                    return BuildCriteriaForDistinct(-1, 0, sortByWithDescending, criteria, !distinct);
                }                
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Metodo che restituisce un singolo oggetto di tipo T sulla base dei filtri impostati.
        /// </summary>
        /// <param name="criteria">Criteri di ricerca su proprietà dell'oggetto.</param>
        /// <returns>Oggetto trovato</returns>
        public T GetByCriteria(SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                return query.UniqueResult<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return default(T);
            }
        }

        /// <summary>
        /// Metodo che restituisce il valore di una proprietà specificata per il raggruppamento di oggetti di tipo T
        /// </summary>
        /// <param name="criteria">Criteri di filtro e raggruppamento.</param>
        /// <returns>Valore della proprietà</returns>
        public object GetPropertyValueByGroupParameter(SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                return query.UniqueResult();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca per raggruppamento " + typeof(T) + ". " + ex.Message);
                return default(T);
            }
        }

        /// <summary>
        /// Metodo che restitusce il numero di oggetti di tipo T della query, senza applicare l'operatore distinct
        /// </summary>
        /// <param name="criteria">Criteri di ricerca.</param>
        /// <returns>Numero di record</returns>
        public int FindByCriteriaCount(SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                ICriteria countCriteria = CriteriaTransformer.TransformToRowCount(query);
                return countCriteria.UniqueResult<int>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return -1;
            }
        }

        /// <summary>
        /// Metodo che restitusce il numero di oggetti di tipo T della query, applicando l'operatore distinct
        /// </summary>
        /// <param name="criteria">Criteri di ricerca.</param>
        /// <param name="distinct">Parametro per decidere se attivare o meno il distinct.</param>
        /// <returns>Numero di record</returns>
        public int FindByCriteriaCount(SearchParameter[] criteria, bool distinct)
        {
            try
            {
                if (!distinct)
                {   
                    return FindByCriteriaCount(criteria);
                }
                else
                {
                    DetachedCriteria subQuery = BuildDetachedCriteriaForDistinct(criteria);

                    return subQuery.GetExecutableCriteria(_Session).List<int>().Distinct().Count();
                }
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return -1;
            }
        }

        /// <summary>
        /// Metodo che restitusce il numero di oggetti di tipo type della query, senza applicare l'operatore distinct
        /// </summary>
        /// <param name="type">Tipo di oggetto.</param>
        /// <param name="criteria">Criteri di ricerca.</param>
        /// <returns>Numero di record</returns>
        public int FindByCriteriaCount(string type, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(type,criteria);
                ICriteria countCriteria = CriteriaTransformer.TransformToRowCount(query);
                return countCriteria.UniqueResult<int>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return -1;
            }
        }

        /// <summary>
        /// ElapseJunctionParameter versione standard
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameter"></param>
        /// <param name="criterionOrAnd"></param>
        private void ElapseJunctionParameter(ICriteria query, SearchParameter parameter, Junction criterionOrAnd = null)
        {
            switch (parameter.Behaviour)
            {
                case SearchParameter.BehaviourClause.And:
                    Conjunction andParam = new Conjunction();
                    foreach (SearchParameter paramAnd in (SearchParameter[])parameter.Value)
                    {
                        if (paramAnd.Value is SearchParameter[])
                        {
                            if (paramAnd.Behaviour != SearchParameter.BehaviourClause.Default)
                            {
                                ElapseJunctionParameter(query, paramAnd, andParam);
                            }
                            else
                                ElapseNonLeafParameter(query, paramAnd, andParam);
                        }
                        else
                            ElapseLeafParameter(query, paramAnd, andParam);
                    }
                    if (criterionOrAnd != null)
                        criterionOrAnd.Add(andParam);
                    else
                        query.Add(andParam);
                    break;
                case SearchParameter.BehaviourClause.Or:
                    Disjunction orParam = new Disjunction();
                    foreach (SearchParameter paramOr in (SearchParameter[])parameter.Value)
                    {
                        if (paramOr.Value is SearchParameter[])
                        {
                            if (paramOr.Behaviour != SearchParameter.BehaviourClause.Default)
                            {
                                ElapseJunctionParameter(query, paramOr, orParam);
                            }
                            else
                                ElapseNonLeafParameter(query, paramOr, orParam);
                        }
                        else
                            ElapseLeafParameter(query, paramOr, orParam);
                    }
                    if (criterionOrAnd != null)
                        criterionOrAnd.Add(orParam);
                    else
                        query.Add(orParam);
                    break;
            }
        }

        /// <summary>
        /// ElapseJunctionParameter versione DetachedCriteria per raggruppamento
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameter"></param>
        /// <param name="criterionOrAnd"></param>
        private void ElapseJunctionParameter(DetachedCriteria query, SearchParameter parameter, Junction criterionOrAnd = null)
        {
            switch (parameter.Behaviour)
            {
                case SearchParameter.BehaviourClause.And:
                    Conjunction andParam = new Conjunction();
                    foreach (SearchParameter paramAnd in (SearchParameter[])parameter.Value)
                    {
                        if (paramAnd.Value is SearchParameter[])
                        {
                            if (paramAnd.Behaviour != SearchParameter.BehaviourClause.Default)
                            {
                                ElapseJunctionParameter(query, paramAnd, andParam);
                            }
                            else
                                ElapseNonLeafParameter(query, paramAnd, andParam);
                        }
                        else
                            ElapseLeafParameter(query, paramAnd, andParam);
                    }
                    if (criterionOrAnd != null)
                        criterionOrAnd.Add(andParam);
                    else
                        query.Add(andParam);
                    break;
                case SearchParameter.BehaviourClause.Or:
                    Disjunction orParam = new Disjunction();
                    foreach (SearchParameter paramOr in (SearchParameter[])parameter.Value)
                    {
                        if (paramOr.Value is SearchParameter[])
                        {
                            if (paramOr.Behaviour != SearchParameter.BehaviourClause.Default)
                            {
                                ElapseJunctionParameter(query, paramOr, orParam);
                            }
                            else
                                ElapseNonLeafParameter(query, paramOr, orParam);
                        }
                        else
                            ElapseLeafParameter(query, paramOr, orParam);
                    }
                    if (criterionOrAnd != null)
                        criterionOrAnd.Add(orParam);
                    else
                        query.Add(orParam);
                    break;
            }
        }

        /// <summary>
        /// ElapseNonLeafParameter versione standard
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameter"></param>
        /// <param name="criterionOrAnd"></param>
        private void ElapseNonLeafParameter(ICriteria query, SearchParameter parameter, Junction criterionOrAnd = null)
        {
            switch (parameter.RelationType)
            {
                case SearchParameter.RelationTypes.OneToOne:
                    {
                        ElapseOneToOne(query, parameter, criterionOrAnd);
                        break;
                    }
                case SearchParameter.RelationTypes.OneToMany:
                    {
                        ElapseOneToMany(query, parameter, criterionOrAnd);
                        break;
                    }
                case SearchParameter.RelationTypes.ManyToMany:
                    {
                        ElapseManyToMany(query, parameter, criterionOrAnd);
                        break;
                    }
            }
        }

        /// <summary>
        /// ElapseNonLeafParameter versione DetachedCriteria per raggruppamento
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameter"></param>
        /// <param name="criterionOrAnd"></param>
        private void ElapseNonLeafParameter(DetachedCriteria query, SearchParameter parameter, Junction criterionOrAnd = null)
        {
            switch (parameter.RelationType)
            {
                case SearchParameter.RelationTypes.OneToOne:
                    {
                        ElapseOneToOne(query, parameter, criterionOrAnd);
                        break;
                    }
                case SearchParameter.RelationTypes.OneToMany:
                    {
                        ElapseOneToMany(query, parameter, criterionOrAnd);
                        break;
                    }
                case SearchParameter.RelationTypes.ManyToMany:
                    {
                        ElapseManyToMany(query, parameter, criterionOrAnd);
                        break;
                    }
            }
        }

        /// <summary>
        /// ElapseOneToOne versione standard
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameter"></param>
        /// <param name="orAnd"></param>
        private void ElapseOneToOne(ICriteria query, SearchParameter parameter, Junction orAnd = null)
        {
            NHibernate.SqlCommand.JoinType joinType = GetJoinType(parameter);

            ICriteria oneToOneCriteria = query.GetCriteriaByAlias(parameter.Name.ToLower()) != null ? query.GetCriteriaByAlias(parameter.Name.ToLower()) : query.CreateCriteria(parameter.Name, parameter.Name.ToLower(), joinType);


            if (parameter.OrdersForJoinSp != null)
            {
                foreach (KeyValuePair<string, bool> order in parameter.OrdersForJoinSp)
                    oneToOneCriteria.AddOrder(new Order(order.Key, order.Value));
            }

            SearchParameter[] searchParameters = (SearchParameter[])parameter.Value;
            foreach (SearchParameter param in searchParameters.Where(x => x.AddToCriteria))
            {
                if (param.Value is SearchParameter[] && param.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                {
                    if (param.Behaviour != SearchParameter.BehaviourClause.Default)
                    {
                        ElapseJunctionParameter(oneToOneCriteria, param, orAnd);
                    }
                    else
                        ElapseNonLeafParameter(oneToOneCriteria, param, orAnd);
                }
                else
                {
                    if (param.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                        ElapseLeafParameter(oneToOneCriteria, param, orAnd);
                    else
                    {
                        if (param.ReturnPropertyValue)
                        {
                            ElapseGroupParameterProperty(oneToOneCriteria, param);
                        }
                        else
                            ElapseGroupParameter(oneToOneCriteria, param);
                    }
                }
            }
        }

        /// <summary>
        /// ElapseOneToOne versione DetachedCriteria per raggruppamento
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameter"></param>
        /// <param name="orAnd"></param>
        private void ElapseOneToOne(DetachedCriteria query, SearchParameter parameter, Junction orAnd = null)
        {
            NHibernate.SqlCommand.JoinType joinType = GetJoinType(parameter);

            DetachedCriteria oneToOneCriteria = query.GetCriteriaByAlias(parameter.Name.ToLower()) != null ? query.GetCriteriaByAlias(parameter.Name.ToLower()) : query.CreateCriteria(parameter.Name, parameter.Name.ToLower(), joinType);

            if (parameter.OrdersForJoinSp != null)
            {
                foreach (KeyValuePair<string, bool> order in parameter.OrdersForJoinSp)
                    oneToOneCriteria.AddOrder(new Order(order.Key, order.Value));
            }

            SearchParameter[] searchParameters = (SearchParameter[])parameter.Value;
            foreach (SearchParameter param in searchParameters.Where(x => x.AddToCriteria))
            {
                if (param.Value is SearchParameter[])
                {
                    if (param.Behaviour != SearchParameter.BehaviourClause.Default)
                    {
                        ElapseJunctionParameter(oneToOneCriteria, param, orAnd);
                    }
                    else
                        ElapseNonLeafParameter(oneToOneCriteria, param, orAnd);
                }
                else
                {
                    
                    ElapseLeafParameter(oneToOneCriteria, param, orAnd);
                    
                }
            }
        }


        /// <summary>
        /// ElapseOneToMany versione standard
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameter"></param>
        /// <param name="orAnd"></param>
        private void ElapseOneToMany(ICriteria query, SearchParameter parameter, Junction orAnd = null)
        {
            NHibernate.SqlCommand.JoinType joinType = GetJoinType(parameter);

            ICriteria oneToManyCriteria = query.GetCriteriaByAlias(parameter.Name.ToLower()) != null ? query.GetCriteriaByAlias(parameter.Name.ToLower()) : query.CreateCriteria(parameter.Name, parameter.Name.ToLower(), joinType);

            if (parameter.OrdersForJoinSp != null)
            {
                foreach (KeyValuePair<string, bool> order in parameter.OrdersForJoinSp)
                    oneToManyCriteria.AddOrder(new Order(order.Key, order.Value));
            }

            SearchParameter[] searchParameters = (SearchParameter[])parameter.Value;
            foreach (SearchParameter param in searchParameters.Where(x => x.AddToCriteria))
            {
                if (param.Value is SearchParameter[] && param.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                {
                    if (param.Behaviour != SearchParameter.BehaviourClause.Default)
                    {
                        ElapseJunctionParameter(oneToManyCriteria, param, orAnd);
                    }
                    else
                        ElapseNonLeafParameter(oneToManyCriteria, param, orAnd);                  
                }
                else
                {
                    if (param.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                        ElapseLeafParameter(oneToManyCriteria, param, orAnd);
                    else                    
                    {
                        if (param.ReturnPropertyValue)
                        {
                            ElapseGroupParameterProperty(oneToManyCriteria, param);
                        }
                        else
                        ElapseGroupParameter(oneToManyCriteria, param); 
                    }
                }
            }
        }

        /// <summary>
        /// ElapseOneToMany versione DetachedCriteria per raggruppamento
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameter"></param>
        /// <param name="orAnd"></param>
        private void ElapseOneToMany(DetachedCriteria query, SearchParameter parameter, Junction orAnd = null)
        {
            NHibernate.SqlCommand.JoinType joinType = GetJoinType(parameter);

            DetachedCriteria oneToManyCriteria = query.GetCriteriaByAlias(parameter.Name.ToLower()) != null ? query.GetCriteriaByAlias(parameter.Name.ToLower()) : query.CreateCriteria(parameter.Name, parameter.Name.ToLower(), joinType);

            if (parameter.OrdersForJoinSp != null)
            {
                foreach (KeyValuePair<string, bool> order in parameter.OrdersForJoinSp)
                    oneToManyCriteria.AddOrder(new Order(order.Key, order.Value));
            }

            SearchParameter[] searchParameters = (SearchParameter[])parameter.Value;
            foreach (SearchParameter param in searchParameters.Where(x => x.AddToCriteria))
            {
                if (param.Value is SearchParameter[])
                {
                    if (param.Behaviour != SearchParameter.BehaviourClause.Default)
                    {
                        ElapseJunctionParameter(oneToManyCriteria, param, orAnd);
                    }
                    else
                        ElapseNonLeafParameter(oneToManyCriteria, param, orAnd);
                }
                else
                {
                    ElapseLeafParameter(oneToManyCriteria, param, orAnd);
                    
                }
            }
        }


        /// <summary>
        /// ElapseManyToMany versione standard
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameter"></param>
        /// <param name="orAnd"></param>
        private void ElapseManyToMany(ICriteria query, SearchParameter parameter, Junction orAnd = null)
        {
            // temporaneamente l'implementazione è la stessa dell'one to many. DA VERIFICARE
            NHibernate.SqlCommand.JoinType joinType = GetJoinType(parameter);

            ICriteria manyToManyCriteria = query.GetCriteriaByAlias(parameter.Name.ToLower()) != null ? query.GetCriteriaByAlias(parameter.Name.ToLower()) : query.CreateCriteria(parameter.Name, parameter.Name.ToLower(), joinType);

            if (parameter.OrdersForJoinSp != null)
            {
                foreach (KeyValuePair<string, bool> order in parameter.OrdersForJoinSp)
                    manyToManyCriteria.AddOrder(new Order(order.Key, order.Value));
            }

            SearchParameter[] searchParameters = (SearchParameter[])parameter.Value;
            foreach (SearchParameter param in searchParameters.Where(x => x.AddToCriteria))
            {
                if (param.Value is SearchParameter[] && param.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                {
                    if (param.Behaviour != SearchParameter.BehaviourClause.Default)
                    {
                        ElapseJunctionParameter(manyToManyCriteria, param, orAnd);
                    }
                    else
                        ElapseNonLeafParameter(manyToManyCriteria, param, orAnd);
                 
                }
                else
                {
                    if (param.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                        ElapseLeafParameter(manyToManyCriteria, param, orAnd);
                    else
                    {
                        if (param.ReturnPropertyValue)
                        {
                            ElapseGroupParameterProperty(manyToManyCriteria, param);
                        }
                        else
                        ElapseGroupParameter(manyToManyCriteria, param);
                    }
                }
            }
        }

        /// <summary>
        /// ElapseManyToMany versione DetachedCriteria per raggruppamento
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameter"></param>
        /// <param name="orAnd"></param>
        private void ElapseManyToMany(DetachedCriteria query, SearchParameter parameter, Junction orAnd = null)
        {
            // temporaneamente l'implementazione è la stessa dell'one to many. DA VERIFICARE
            NHibernate.SqlCommand.JoinType joinType = GetJoinType(parameter);

            DetachedCriteria manyToManyCriteria = query.GetCriteriaByAlias(parameter.Name.ToLower()) != null ? query.GetCriteriaByAlias(parameter.Name.ToLower()) : query.CreateCriteria(parameter.Name, parameter.Name.ToLower(), joinType);

            if (parameter.OrdersForJoinSp != null)
            {
                foreach (KeyValuePair<string, bool> order in parameter.OrdersForJoinSp)
                    manyToManyCriteria.AddOrder(new Order(order.Key, order.Value));
            }

            SearchParameter[] searchParameters = (SearchParameter[])parameter.Value;
            foreach (SearchParameter param in searchParameters.Where(x => x.AddToCriteria))
            {
                if (param.Value is SearchParameter[])
                {
                    if (param.Behaviour != SearchParameter.BehaviourClause.Default)
                    {
                        ElapseJunctionParameter(manyToManyCriteria, param, orAnd);
                    }
                    else
                        ElapseNonLeafParameter(manyToManyCriteria, param, orAnd);

                }
                else
                {
                    ElapseLeafParameter(manyToManyCriteria, param, orAnd);
                    
                }
            }
        }

        private void ElapseLeafParameter(ICriteria query, SearchParameter parameter, Junction orAnd = null)
        {
            object value = parameter.Value.ToString() == SearchParameter.NullValue ? null : parameter.Value;

            string alias = "";
            if (orAnd != null && parameter.Parent != null && !string.IsNullOrEmpty(parameter.Parent.Name))
            {
                alias = parameter.Parent.Name.ToLower() + ".";
            }

            switch (parameter.ComparisonCriteria)
            {
                case Finder.ComparisonCriteria.Equals:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.Eq(alias + parameter.Name, value));
                    else
                        query.Add(Restrictions.Eq(alias + parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.EqualsIgnoreCase:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.Eq(alias + parameter.Name, value).IgnoreCase());
                    else
                        query.Add(Restrictions.Eq(alias + parameter.Name, value).IgnoreCase());
                    break;

                case Finder.ComparisonCriteria.GreaterEquals:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.Ge(alias + parameter.Name, value));
                    else
                        query.Add(Restrictions.Ge(alias + parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.GreaterThan:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.Gt(alias + parameter.Name, value));
                    else
                        query.Add(Restrictions.Gt(alias + parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.In:
                    {
                        ICollection values = null;
                        if (value is ICollection)
                        {
                            values = (ICollection)value;
                            if (orAnd != null)
                                orAnd.Add(Restrictions.In(alias + parameter.Name, values));
                            else
                                query.Add(Restrictions.In(alias + parameter.Name, values));
                        }
                        break;
                    }
                case Finder.ComparisonCriteria.NotIn:
                    {
                        ICollection values = null;
                        if (value is ICollection)
                        {
                            values = (ICollection)value;
                            if (orAnd != null)
                                orAnd.Add(Restrictions.Not(Restrictions.In(alias + parameter.Name, values)));
                            else
                                query.Add(Restrictions.Not(Restrictions.In(alias + parameter.Name, values)));
                        }
                        break;
                    }
                case Finder.ComparisonCriteria.LessEquals:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.Le(alias + parameter.Name, value));
                    else
                        query.Add(Restrictions.Le(alias + parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.LessThan:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.Lt(alias + parameter.Name, value));
                    else
                        query.Add(Restrictions.Lt(alias + parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.Like:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.InsensitiveLike(alias + parameter.Name, value.ToString(), MatchMode.Anywhere));
                    else
                        query.Add(Restrictions.InsensitiveLike(alias + parameter.Name, value.ToString(), MatchMode.Anywhere));
                    break;

                case Finder.ComparisonCriteria.LikeStart:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.InsensitiveLike(alias + parameter.Name, value.ToString(), MatchMode.Start));
                    else
                        query.Add(Restrictions.InsensitiveLike(alias + parameter.Name, value.ToString(), MatchMode.Start));
                    break;

                case Finder.ComparisonCriteria.LikeEnd:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.InsensitiveLike(alias + parameter.Name, value.ToString(), MatchMode.End));
                    else
                        query.Add(Restrictions.InsensitiveLike(alias + parameter.Name, value.ToString(), MatchMode.End));
                    break;

                case Finder.ComparisonCriteria.Not:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.Not(Restrictions.Eq(alias + parameter.Name, value)));
                    else
                        query.Add(Restrictions.Not(Restrictions.Eq(alias + parameter.Name, value)));
                    break;

                case Finder.ComparisonCriteria.NotNull:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.IsNotNull(alias + parameter.Name));
                    else
                        query.Add(Restrictions.IsNotNull(alias + parameter.Name));
                    break;

                case Finder.ComparisonCriteria.Null:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.IsNull(alias + parameter.Name));
                    else
                        query.Add(Restrictions.IsNull(alias + parameter.Name));
                    break;

                case Finder.ComparisonCriteria.IsEmpty:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.IsEmpty(alias + parameter.Name));
                    else
                        query.Add(Restrictions.IsEmpty(alias + parameter.Name));
                    break;

                case Finder.ComparisonCriteria.IsNotEmpty:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.IsNotEmpty(alias + parameter.Name));
                    else
                        query.Add(Restrictions.IsNotEmpty(alias + parameter.Name));
                    break;

            }
        }

        private void ElapseLeafParameter(DetachedCriteria query, SearchParameter parameter, Junction orAnd = null)
        {
            object value = parameter.Value.ToString() == SearchParameter.NullValue ? null : parameter.Value;

            string alias = "";
            if (orAnd != null && parameter.Parent != null && !string.IsNullOrEmpty(parameter.Parent.Name))
            {
                alias = parameter.Parent.Name.ToLower() + ".";
            }

            switch (parameter.ComparisonCriteria)
            {
                case Finder.ComparisonCriteria.Equals:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.Eq(alias + parameter.Name, value));
                    else
                        query.Add(Restrictions.Eq(alias + parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.EqualsIgnoreCase:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.Eq(alias + parameter.Name, value).IgnoreCase());
                    else
                        query.Add(Restrictions.Eq(alias + parameter.Name, value).IgnoreCase());
                    break;

                case Finder.ComparisonCriteria.GreaterEquals:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.Ge(alias + parameter.Name, value));
                    else
                        query.Add(Restrictions.Ge(alias + parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.GreaterThan:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.Gt(alias + parameter.Name, value));
                    else
                        query.Add(Restrictions.Gt(alias + parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.In:
                    {
                        ICollection values = null;
                        if (value is ICollection)
                        {
                            values = (ICollection)value;
                            if (orAnd != null)
                                orAnd.Add(Restrictions.In(alias + parameter.Name, values));
                            else
                                query.Add(Restrictions.In(alias + parameter.Name, values));
                        }
                        break;
                    }
                case Finder.ComparisonCriteria.NotIn:
                    {
                        ICollection values = null;
                        if (value is ICollection)
                        {
                            values = (ICollection)value;
                            if (orAnd != null)
                                orAnd.Add(Restrictions.Not(Restrictions.In(alias + parameter.Name, values)));
                            else
                                query.Add(Restrictions.Not(Restrictions.In(alias + parameter.Name, values)));
                        }
                        break;
                    }
                case Finder.ComparisonCriteria.LessEquals:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.Le(alias + parameter.Name, value));
                    else
                        query.Add(Restrictions.Le(alias + parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.LessThan:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.Lt(alias + parameter.Name, value));
                    else
                        query.Add(Restrictions.Lt(alias + parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.Like:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.InsensitiveLike(alias + parameter.Name, value.ToString(), MatchMode.Anywhere));
                    else
                        query.Add(Restrictions.InsensitiveLike(alias + parameter.Name, value.ToString(), MatchMode.Anywhere));
                    break;

                case Finder.ComparisonCriteria.LikeStart:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.InsensitiveLike(alias + parameter.Name, value.ToString(), MatchMode.Start));
                    else
                        query.Add(Restrictions.InsensitiveLike(alias + parameter.Name, value.ToString(), MatchMode.Start));
                    break;

                case Finder.ComparisonCriteria.LikeEnd:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.InsensitiveLike(alias + parameter.Name, value.ToString(), MatchMode.End));
                    else
                        query.Add(Restrictions.InsensitiveLike(alias + parameter.Name, value.ToString(), MatchMode.End));
                    break;

                case Finder.ComparisonCriteria.Not:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.Not(Restrictions.Eq(alias + parameter.Name, value)));
                    else
                        query.Add(Restrictions.Not(Restrictions.Eq(alias + parameter.Name, value)));
                    break;

                case Finder.ComparisonCriteria.NotNull:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.IsNotNull(alias + parameter.Name));
                    else
                        query.Add(Restrictions.IsNotNull(alias + parameter.Name));
                    break;

                case Finder.ComparisonCriteria.Null:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.IsNull(alias + parameter.Name));
                    else
                        query.Add(Restrictions.IsNull(alias + parameter.Name));
                    break;

                case Finder.ComparisonCriteria.IsEmpty:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.IsEmpty(alias + parameter.Name));
                    else
                        query.Add(Restrictions.IsEmpty(alias + parameter.Name));
                    break;

                case Finder.ComparisonCriteria.IsNotEmpty:
                    if (orAnd != null)
                        orAnd.Add(Restrictions.IsNotEmpty(alias + parameter.Name));
                    else
                        query.Add(Restrictions.IsNotEmpty(alias + parameter.Name));
                    break;

            }
        }

        private void ElapseGroupParameter(ICriteria query, SearchParameter parameter)
        {            
            Type criteriaType;
            if (parameter.Parent != null)
            {
                PropertyInfo prop = query.GetRootEntityTypeIfAvailable().GetProperty(parameter.Parent.Name);
                criteriaType = prop.PropertyType;
            }
            else
                criteriaType = query.GetRootEntityTypeIfAvailable();

            DetachedCriteria subCriteria = DetachedCriteria.For(criteriaType);

            ProjectionList projList = Projections.ProjectionList();

            switch (parameter.GroupingCriteria)
            {
                case Finder.GroupingCriteria.Max:
                    projList.Add(Projections.Max(parameter.Name));
                    break;
                case Finder.GroupingCriteria.Group:
                    projList.Add(Projections.GroupProperty(parameter.Name));
                    break;
                case Finder.GroupingCriteria.Min:
                    projList.Add(Projections.Min(parameter.Name));
                    break;
                case Finder.GroupingCriteria.Avg:
                    projList.Add(Projections.Avg(parameter.Name));
                    break;
                case Finder.GroupingCriteria.Sum:
                    projList.Add(Projections.Sum(parameter.Name));
                    break;
            }

            subCriteria.SetProjection(projList);
            
            if (parameter.Value != SearchParameter.NullValue)
            {
                SearchParameter[] searchParameters = (SearchParameter[])parameter.Value;
                foreach (SearchParameter param in searchParameters.Where(x => x.AddToCriteria))
                {
                    if (param.Value is SearchParameter[] && param.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                    {
                        if (param.Behaviour != SearchParameter.BehaviourClause.Default)
                            ElapseJunctionParameter(subCriteria, param);
                        else
                            ElapseNonLeafParameter(subCriteria, param);
                        
                    }
                    else
                        ElapseLeafParameter(subCriteria, param);
                }
            }

            if (parameter.Parent != null)
            {
                query.Add(Subqueries.PropertyEq(parameter.Parent.Name + "." + parameter.Name, subCriteria));
            }
            else query.Add(Subqueries.PropertyEq(parameter.Name, subCriteria));
        }


        private void ElapseGroupParameter(DetachedCriteria query, SearchParameter parameter)
        {
            Type criteriaType;
            if (parameter.Parent != null)
            {
                PropertyInfo prop = query.GetRootEntityTypeIfAvailable().GetProperty(parameter.Parent.Name);
                criteriaType = prop.PropertyType;
            }
            else
                criteriaType = query.GetRootEntityTypeIfAvailable();

            DetachedCriteria subCriteria = DetachedCriteria.For(criteriaType);

            ProjectionList projList = Projections.ProjectionList();

            switch (parameter.GroupingCriteria)
            {
                case Finder.GroupingCriteria.Max:
                    projList.Add(Projections.Max(parameter.Name));
                    break;
                case Finder.GroupingCriteria.Group:
                    projList.Add(Projections.GroupProperty(parameter.Name));
                    break;
                case Finder.GroupingCriteria.Min:
                    projList.Add(Projections.Min(parameter.Name));
                    break;
                case Finder.GroupingCriteria.Avg:
                    projList.Add(Projections.Avg(parameter.Name));
                    break;
                case Finder.GroupingCriteria.Sum:
                    projList.Add(Projections.Sum(parameter.Name));
                    break;
            }

            subCriteria.SetProjection(projList);

            if (parameter.Value != SearchParameter.NullValue)
            {
                SearchParameter[] searchParameters = (SearchParameter[])parameter.Value;
                foreach (SearchParameter param in searchParameters.Where(x => x.AddToCriteria))
                {
                    if (param.Value is SearchParameter[] && param.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                    {
                        if (param.Behaviour != SearchParameter.BehaviourClause.Default)
                            ElapseJunctionParameter(subCriteria, param);
                        else
                            ElapseNonLeafParameter(subCriteria, param);

                    }
                    else
                        ElapseLeafParameter(subCriteria, param);
                }
            }

            if (parameter.Parent != null)
            {
                query.Add(Subqueries.PropertyEq(parameter.Parent.Name + "." + parameter.Name, subCriteria));
            }
            else query.Add(Subqueries.PropertyEq(parameter.Name, subCriteria));
        }

        private void ElapseGroupParameterProperty(ICriteria query, SearchParameter parameter)
        {       
            ProjectionList projList = Projections.ProjectionList();
                        
            string paramName = parameter.Parent != null ? parameter.Parent.Name.ToLower() + "." + parameter.Name : parameter.Name;

            switch (parameter.GroupingCriteria)
            {
                case Finder.GroupingCriteria.Max:
                    projList.Add(Projections.Max(paramName));
                    break;
                case Finder.GroupingCriteria.Group:
                    projList.Add(Projections.GroupProperty(paramName));
                    break;
                case Finder.GroupingCriteria.Min:
                    projList.Add(Projections.Min(paramName));
                    break;
                case Finder.GroupingCriteria.Avg:
                    projList.Add(Projections.Avg(paramName));
                    break;
                case Finder.GroupingCriteria.Sum:
                    projList.Add(Projections.Sum(paramName));
                    break;
            }

            if (parameter.Value != SearchParameter.NullValue)
            {
                SearchParameter[] searchParameters = (SearchParameter[])parameter.Value;
                foreach (SearchParameter param in searchParameters.Where(x => x.AddToCriteria))
                {
                    if (param.Value is SearchParameter[] && param.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                    {
                        if (param.Behaviour != SearchParameter.BehaviourClause.Default)
                            ElapseJunctionParameter(query.GetCriteriaByAlias("this"), param);
                        else
                            ElapseNonLeafParameter(query.GetCriteriaByAlias("this"), param);

                    }
                    else
                        ElapseLeafParameter(query.GetCriteriaByAlias("this"), param);
                }
            }

            query.SetProjection(projList);
        }

        private void ElapseGroupParameterProperty(DetachedCriteria query, SearchParameter parameter)
        {
            ProjectionList projList = Projections.ProjectionList();

            string paramName = parameter.Parent != null ? parameter.Parent.Name.ToLower() + "." + parameter.Name : parameter.Name;

            switch (parameter.GroupingCriteria)
            {
                case Finder.GroupingCriteria.Max:
                    projList.Add(Projections.Max(paramName));
                    break;
                case Finder.GroupingCriteria.Group:
                    projList.Add(Projections.GroupProperty(paramName));
                    break;
                case Finder.GroupingCriteria.Min:
                    projList.Add(Projections.Min(paramName));
                    break;
                case Finder.GroupingCriteria.Avg:
                    projList.Add(Projections.Avg(paramName));
                    break;
                case Finder.GroupingCriteria.Sum:
                    projList.Add(Projections.Sum(paramName));
                    break;
            }

            if (parameter.Value != SearchParameter.NullValue)
            {
                SearchParameter[] searchParameters = (SearchParameter[])parameter.Value;
                foreach (SearchParameter param in searchParameters.Where(x => x.AddToCriteria))
                {
                    if (param.Value is SearchParameter[] && param.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                    {
                        if (param.Behaviour != SearchParameter.BehaviourClause.Default)
                            ElapseJunctionParameter(query.GetCriteriaByAlias("this"), param);
                        else
                            ElapseNonLeafParameter(query.GetCriteriaByAlias("this"), param);

                    }
                    else
                        ElapseLeafParameter(query.GetCriteriaByAlias("this"), param);
                }
            }

            query.SetProjection(projList);
        }


        private NHibernate.SqlCommand.JoinType GetJoinType(SearchParameter parameter)
        {
            NHibernate.SqlCommand.JoinType joinType;
            switch (parameter.JoinType)
            {
                case SearchParameter.JoinTypes.Inner:
                    joinType = NHibernate.SqlCommand.JoinType.InnerJoin;
                    break;
                case SearchParameter.JoinTypes.Left:
                    joinType = NHibernate.SqlCommand.JoinType.LeftOuterJoin;
                    break;
                case SearchParameter.JoinTypes.Right:
                    joinType = NHibernate.SqlCommand.JoinType.RightOuterJoin;
                    break;
                case SearchParameter.JoinTypes.Full:
                    joinType = NHibernate.SqlCommand.JoinType.FullJoin;
                    break;
                default: joinType = NHibernate.SqlCommand.JoinType.InnerJoin;
                    break;
            }
            return joinType;
        }
    }
}
