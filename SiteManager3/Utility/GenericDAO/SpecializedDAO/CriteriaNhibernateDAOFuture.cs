﻿using System;
using System.Collections;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using System.Linq;
using NetService.Utility.RecordsFinder;
using System.Reflection;



namespace GenericDAO.DAO
{
    public class CriteriaNhibernateDAOFuture<T> : IGenericDAOFuture<T>
    {
        private ISession _Session;
        private Type persistentType = typeof(T);

        public CriteriaNhibernateDAOFuture(ISession session)
        {
            _Session = session;
        }

        public IFutureValue<T> GetById(int id)
        {
            IFutureValue<T> entity;            
            try
            {
               ISession session = _Session;
               entity = session.CreateCriteria(persistentType).Add(Restrictions.IdEq(id)).FutureValue<T>();
            }
            catch (Exception ex) 
            {                
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca di un entità di tipo " + typeof(T) + " con id: " + id.ToString() + ". " + ex.Message);
                return null;
            }
            return entity;
        }

        public IFutureValue<T> GetById(string id)
        {
            //T entity = default(T);
            IFutureValue<T> entity;  
            try
            {
                ISession session = _Session;
                //entity = (T)session.Get(persistentType, id);
                entity = session.CreateCriteria(persistentType).Add(Restrictions.IdEq(id)).FutureValue<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca di un entità di tipo " + typeof(T) + " con id: " + id.ToString() + ". " + ex.Message);
                return null;
            }
            return entity;
        }

        public IEnumerable<T> FindAll()
        {
            try
            {
                ICriteria query = BuildCriteria(null);
                return query.List<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public void Delete(T entity)
        {
            try
            {
                ISession session = _Session;
                session.Delete(entity);
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la cancellazione di un'entità di tipo " + entity.GetType().Name + ". " + ex.Message);
            }
        }

        public T SaveOrUpdate(T entity)
        {
            try
            {
                ISession session = _Session;
                session.SaveOrUpdate(entity);
                
                return entity;
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante il salvataggio o l'update di un'entità di tipo " + entity.GetType().Name + ". " + ex.Message);
                return default(T);
            }
        }

        public ICriteria BuildCriteria(SearchParameter[] criteria)
        {
            ICriteria query = _Session.CreateCriteria(persistentType);
            //query.SetTimeout(60);

            if (criteria == null || criteria.Length == 0)
            {
                return query;
            }

            foreach (SearchParameter parameter in criteria.Where(x => x.AddToCriteria))
            {
                if (parameter.Value is SearchParameter[] && parameter.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                    ElapseNonLeafParameter(query, parameter);
                else
                {
                    if (parameter.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                        ElapseLeafParameter(query, parameter);
                    else
                    {
                        if (parameter.ReturnPropertyValue)
                        {
                            ElapseGroupParameterProperty(query, parameter);
                        }
                        else
                            ElapseGroupParameter(query, parameter);
                    }
                }
            }

            return query;
        }

        private ICriteria BuildCriteria(string type, SearchParameter[] criteria)
        {
            ICriteria query = _Session.CreateCriteria(type);
            //query.SetTimeout(60);

            if (criteria == null || criteria.Length == 0)
            {
                return query;
            }

            foreach (SearchParameter parameter in criteria.Where(x => x.AddToCriteria))
            {
                if (parameter.Value is SearchParameter[] && parameter.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                    ElapseNonLeafParameter(query, parameter);
                else
                {
                    if (parameter.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                        ElapseLeafParameter(query, parameter);
                    else
                    {
                        if (parameter.ReturnPropertyValue)
                        {
                            ElapseGroupParameterProperty(query, parameter);
                        }
                        else
                            ElapseGroupParameter(query, parameter);
                    }
                }
            }

            return query;
        }

        public IEnumerable<T> FindAll(int pageStart, int pageSize, string sortBy, bool descending)
        {
            try
            {
                ICriteria query = BuildCriteria(null);
                return query.AddOrder(new Order(sortBy, !descending)).SetFirstResult(pageStart).SetMaxResults(pageSize).Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }
        public IEnumerable<T> FindAll(int pageStart, int pageSize)
        {
            try
            {
                ICriteria query = BuildCriteria(null);
                return query.SetFirstResult(pageStart).SetMaxResults(pageSize).Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }
        public IEnumerable<T> FindAll(string sortBy, bool descending) 
        {
            try
            {
                ICriteria query = BuildCriteria(null);
                return query.AddOrder(new Order(sortBy, !descending)).Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public IEnumerable<T> FindByCriteria(SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                return query.Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
            
        }

        /// <summary>
        /// Metodo usato per le sessioni di tipo Map
        /// </summary>
        /// <param name="type"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public IEnumerable<T> FindByCriteria(string type, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(type, criteria);
                return query.Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }

        }

        /// <summary>
        /// Metodo usato per le sessioni di tipo Map
        /// </summary>
        /// <param name="type"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public IEnumerable<T> FindByCriteria(int pageStart, int pageSize, string type, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(type, criteria);
                return query.SetFirstResult(pageStart).SetMaxResults(pageSize).Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public IEnumerable<T> FindByCriteria(SearchParameter[] criteria, bool distinct)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                if (distinct)
                    query = query.SetResultTransformer(new NHibernate.Transform.DistinctRootEntityResultTransformer());
                return query.Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }

        }

        public IEnumerable<T> FindByCriteria(int pageStart, int pageSize, string sortBy, bool descending, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                return query.AddOrder(new Order(sortBy, !descending)).SetFirstResult(pageStart).SetMaxResults(pageSize).Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public IEnumerable<T> FindByCriteria(int pageStart, int pageSize, IList<string> sortBy, bool descending, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);

                foreach (string sort in sortBy) 
                {
                    query.AddOrder(new Order(sort, !descending));
                }

                return query.SetFirstResult(pageStart).SetMaxResults(pageSize).Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public IEnumerable<T> FindByCriteria(int pageStart, int pageSize, string sortBy, bool descending, SearchParameter[] criteria, bool distinct)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);

                if (distinct)
                    query = query.SetResultTransformer(new NHibernate.Transform.DistinctRootEntityResultTransformer());

                return query.AddOrder(new Order(sortBy, !descending)).SetFirstResult(pageStart).SetMaxResults(pageSize).Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public IEnumerable<T> FindByCriteria(int pageStart, int pageSize, IList<string> sortBy, bool descending, SearchParameter[] criteria, bool distinct)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);

                if (distinct)
                    query = query.SetResultTransformer(new NHibernate.Transform.DistinctRootEntityResultTransformer());

                foreach (string sort in sortBy)
                {
                    query.AddOrder(new Order(sort, !descending));
                }

                return query.SetFirstResult(pageStart).SetMaxResults(pageSize).Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public IEnumerable<T> FindByCriteria(int pageStart, int pageSize, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                return query.SetFirstResult(pageStart).SetMaxResults(pageSize).Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public IEnumerable<T> FindByCriteria(string sortBy, bool descending, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                return query.AddOrder(new Order(sortBy, !descending)).Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public IEnumerable<T> FindByCriteria(IList<string> sortBy, bool descending, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                foreach (string sort in sortBy)
                {
                    query.AddOrder(new Order(sort, !descending));
                }
                return query.Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public IEnumerable<T> FindByCriteria(IDictionary<string, bool> sortByWithDescending, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                foreach (KeyValuePair<string, bool> sort in sortByWithDescending)
                {
                    query.AddOrder(new Order(sort.Key, !sort.Value));
                }
                return query.Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public IEnumerable<T> FindByCriteria(IDictionary<string, bool> sortByWithDescending, SearchParameter[] criteria, bool distinct)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                if (distinct)
                    query = query.SetResultTransformer(new NHibernate.Transform.DistinctRootEntityResultTransformer());

                foreach (KeyValuePair<string, bool> sort in sortByWithDescending)
                {
                    query.AddOrder(new Order(sort.Key, !sort.Value));
                }
                return query.Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public IEnumerable<T> FindByCriteria(int pageStart, int pageSize, IDictionary<string, bool> sortByWithDescending, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                foreach (KeyValuePair<string, bool> sort in sortByWithDescending)
                {
                    query.AddOrder(new Order(sort.Key, !sort.Value));
                }
                return query.SetFirstResult(pageStart).SetMaxResults(pageSize).Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public IEnumerable<T> FindByCriteria(string sortBy, bool descending, SearchParameter[] criteria, bool distinct)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                if (distinct)
                    query = query.SetResultTransformer(new NHibernate.Transform.DistinctRootEntityResultTransformer());

                return query.AddOrder(new Order(sortBy, !descending)).Future<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public IFutureValue<T> GetByCriteria(SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                //return query.UniqueResult<T>();
                return query.FutureValue<T>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public object GetPropertyValueByGroupParameter(SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                return query.UniqueResult();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca per raggruppamento " + typeof(T) + ". " + ex.Message);
                return default(T);
            }
        }

        public IFutureValue<int> FindByCriteriaCount(SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);
                ICriteria countCriteria = CriteriaTransformer.TransformToRowCount(query);
                return countCriteria.FutureValue<int>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public IFutureValue<int> FindByCriteriaCount(SearchParameter[] criteria, bool distinct)
        {
            try
            {
                ICriteria query = BuildCriteria(criteria);

                if (distinct)
                    query = query.SetResultTransformer(new NHibernate.Transform.DistinctRootEntityResultTransformer());

                ICriteria countCriteria = CriteriaTransformer.TransformToRowCount(query);
                return countCriteria.FutureValue<int>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        public IFutureValue<int> FindByCriteriaCount(string type, SearchParameter[] criteria)
        {
            try
            {
                ICriteria query = BuildCriteria(type,criteria);
                ICriteria countCriteria = CriteriaTransformer.TransformToRowCount(query);
                return countCriteria.FutureValue<int>();
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la ricerca entità di tipo " + typeof(T) + ". " + ex.Message);
                return null;
            }
        }

        private void ElapseNonLeafParameter(ICriteria query, SearchParameter parameter)
        {
            switch (parameter.RelationType)
            {
                case SearchParameter.RelationTypes.OneToOne:
                    {
                        ElapseOneToOne(query, parameter);
                        break;
                    }
                case SearchParameter.RelationTypes.OneToMany:
                    {
                        ElapseOneToMany(query, parameter);
                        break;
                    }
                case SearchParameter.RelationTypes.ManyToMany:
                    {
                        ElapseManyToMany(query, parameter);
                        break;
                    }
            }
        }

        private void ElapseOneToOne(ICriteria query, SearchParameter parameter)
        {
            NHibernate.SqlCommand.JoinType joinType = GetJoinType(parameter);

            ICriteria oneToOneCriteria = query.CreateCriteria(parameter.Name, parameter.Name.ToLower(), joinType);
            SearchParameter[] searchParameters = (SearchParameter[])parameter.Value;
            foreach (SearchParameter param in searchParameters.Where(x => x.AddToCriteria))
            {
                if (param.Value is SearchParameter[])
                {
                    ElapseNonLeafParameter(oneToOneCriteria, param);
                }
                else
                {
                    if (param.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                        ElapseLeafParameter(oneToOneCriteria, param);
                    else
                    {
                        if (param.ReturnPropertyValue)
                        {
                            ElapseGroupParameterProperty(oneToOneCriteria, param);
                        }
                        else
                            ElapseGroupParameter(oneToOneCriteria, param);
                    }
                }
            }
        }

        private void ElapseOneToMany(ICriteria query, SearchParameter parameter)
        {
            NHibernate.SqlCommand.JoinType joinType = GetJoinType(parameter);

            ICriteria oneToManyCriteria = query.CreateCriteria(parameter.Name, parameter.Name.ToLower(), joinType);
            SearchParameter[] searchParameters = (SearchParameter[])parameter.Value;
            foreach (SearchParameter param in searchParameters.Where(x => x.AddToCriteria))
            {
                if (param.Value is SearchParameter[])
                {
                    ElapseNonLeafParameter(oneToManyCriteria, param);
                }
                else
                {
                    if (param.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                        ElapseLeafParameter(oneToManyCriteria, param);
                    else                    
                    {
                        if (param.ReturnPropertyValue)
                        {
                            ElapseGroupParameterProperty(oneToManyCriteria, param);
                        }
                        else
                        ElapseGroupParameter(oneToManyCriteria, param); 
                    }
                }
            }
        }

        private void ElapseManyToMany(ICriteria query, SearchParameter parameter)
        {
            // temporaneamente l'implementazione è la stessa dell'one to many. DA VERIFICARE

            ICriteria manyToManyCriteria = query.CreateCriteria(parameter.Name, parameter.Name.ToLower());
            SearchParameter[] searchParameters = (SearchParameter[])parameter.Value;
            foreach (SearchParameter param in searchParameters.Where(x => x.AddToCriteria))
            {
                if (param.Value is SearchParameter[])
                {
                    ElapseNonLeafParameter(manyToManyCriteria, param);
                }
                else
                {
                    if (param.GroupingCriteria == Finder.GroupingCriteria.NoGroup)
                        ElapseLeafParameter(manyToManyCriteria, param);
                    else
                    {
                        if (param.ReturnPropertyValue)
                        {
                            ElapseGroupParameterProperty(manyToManyCriteria, param);
                        }
                        else
                        ElapseGroupParameter(manyToManyCriteria, param);
                    }
                }
            }
        }

        private void ElapseLeafParameter(ICriteria query, SearchParameter parameter)
        {
            object value = parameter.Value.ToString() == SearchParameter.NullValue ? null : parameter.Value;
            switch (parameter.ComparisonCriteria)
            {
                case Finder.ComparisonCriteria.Equals:
                    query.Add(Restrictions.Eq(parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.EqualsIgnoreCase:
                    query.Add(Restrictions.Eq(parameter.Name, value).IgnoreCase());
                    break;

                case Finder.ComparisonCriteria.GreaterEquals:
                    query.Add(Restrictions.Ge(parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.GreaterThan:
                    query.Add(Restrictions.Gt(parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.In:
                    {
                        ICollection values = null;
                        if (value is ICollection)
                        {
                            values = (ICollection)value;
                            query.Add(Restrictions.In(parameter.Name, values));
                        }
                        break;
                    }

                case Finder.ComparisonCriteria.LessEquals:
                    query.Add(Restrictions.Le(parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.LessThan:
                    query.Add(Restrictions.Lt(parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.Like:
                    query.Add(Restrictions.InsensitiveLike(parameter.Name, value.ToString(), MatchMode.Anywhere));
                    break;

                case Finder.ComparisonCriteria.LikeStart:
                    query.Add(Restrictions.InsensitiveLike(parameter.Name, value.ToString(), MatchMode.Start));
                    break;

                case Finder.ComparisonCriteria.LikeEnd:
                    query.Add(Restrictions.InsensitiveLike(parameter.Name, value.ToString(), MatchMode.End));
                    break;

                case Finder.ComparisonCriteria.Not:
                    query.Add(Restrictions.Not(Restrictions.Eq(parameter.Name, value)));
                    break;

                case Finder.ComparisonCriteria.NotNull:
                    query.Add(Restrictions.IsNotNull(parameter.Name));
                    break;

                case Finder.ComparisonCriteria.Null:
                    query.Add(Restrictions.IsNull(parameter.Name));
                    break;

                case Finder.ComparisonCriteria.IsEmpty:
                    query.Add(Restrictions.IsEmpty(parameter.Name));
                    break;

                case Finder.ComparisonCriteria.IsNotEmpty:
                    query.Add(Restrictions.IsNotEmpty(parameter.Name));
                    break;

            }
        }

        private void ElapseLeafParameter(DetachedCriteria query, SearchParameter parameter)
        {
            object value = parameter.Value.ToString() == SearchParameter.NullValue ? null : parameter.Value;
            switch (parameter.ComparisonCriteria)
            {
                case Finder.ComparisonCriteria.Equals:
                    query.Add(Restrictions.Eq(parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.EqualsIgnoreCase:
                    query.Add(Restrictions.Eq(parameter.Name, value).IgnoreCase());
                    break;

                case Finder.ComparisonCriteria.GreaterEquals:
                    query.Add(Restrictions.Ge(parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.GreaterThan:
                    query.Add(Restrictions.Gt(parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.In:
                    {
                        ICollection values = null;
                        if (value is ICollection)
                        {
                            values = (ICollection)value;
                            query.Add(Restrictions.In(parameter.Name, values));
                        }
                        break;
                    }

                case Finder.ComparisonCriteria.LessEquals:
                    query.Add(Restrictions.Le(parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.LessThan:
                    query.Add(Restrictions.Lt(parameter.Name, value));
                    break;

                case Finder.ComparisonCriteria.Like:
                    query.Add(Restrictions.InsensitiveLike(parameter.Name, value.ToString(), MatchMode.Anywhere));
                    break;

                case Finder.ComparisonCriteria.LikeStart:
                    query.Add(Restrictions.InsensitiveLike(parameter.Name, value.ToString(), MatchMode.Start));
                    break;

                case Finder.ComparisonCriteria.LikeEnd:
                    query.Add(Restrictions.InsensitiveLike(parameter.Name, value.ToString(), MatchMode.End));
                    break;

                case Finder.ComparisonCriteria.Not:
                    query.Add(Restrictions.Not(Restrictions.Eq(parameter.Name, value)));
                    break;

                case Finder.ComparisonCriteria.NotNull:
                    query.Add(Restrictions.IsNotNull(parameter.Name));
                    break;

                case Finder.ComparisonCriteria.Null:
                    query.Add(Restrictions.IsNull(parameter.Name));
                    break;

                case Finder.ComparisonCriteria.IsEmpty:
                    query.Add(Restrictions.IsEmpty(parameter.Name));
                    break;

                case Finder.ComparisonCriteria.IsNotEmpty:
                    query.Add(Restrictions.IsNotEmpty(parameter.Name));
                    break;

            }
        }

        private void ElapseGroupParameter(ICriteria query, SearchParameter parameter)
        {            
            Type criteriaType;
            if (parameter.Parent != null)
            {
                PropertyInfo prop = query.GetRootEntityTypeIfAvailable().GetProperty(parameter.Parent.Name);
                criteriaType = prop.PropertyType;
            }
            else
                criteriaType = query.GetRootEntityTypeIfAvailable();

            DetachedCriteria subCriteria = DetachedCriteria.For(criteriaType);

            ProjectionList projList = Projections.ProjectionList();

            switch (parameter.GroupingCriteria)
            {
                case Finder.GroupingCriteria.Max:
                    projList.Add(Projections.Max(parameter.Name));
                    break;
                case Finder.GroupingCriteria.Group:
                    projList.Add(Projections.GroupProperty(parameter.Name));
                    break;
                case Finder.GroupingCriteria.Min:
                    projList.Add(Projections.Min(parameter.Name));
                    break;
            }

            subCriteria.SetProjection(projList);
            
            if (parameter.Value != SearchParameter.NullValue)
            {
                SearchParameter[] searchParameters = (SearchParameter[])parameter.Value;
                foreach (SearchParameter param in searchParameters.Where(x => x.AddToCriteria))
                {
                    ElapseLeafParameter(subCriteria, param);
                }
            }

            if (parameter.Parent != null)
            {
                query.Add(Subqueries.PropertyEq(parameter.Parent.Name + "." + parameter.Name, subCriteria));
            }
            else query.Add(Subqueries.PropertyEq(parameter.Name, subCriteria));
        }

        private void ElapseGroupParameterProperty(ICriteria query, SearchParameter parameter)
        {       
            ProjectionList projList = Projections.ProjectionList();

            string paramName = parameter.Parent != null ? parameter.Parent.Name + "." + parameter.Name : parameter.Name;

            switch (parameter.GroupingCriteria)
            {
                case Finder.GroupingCriteria.Max:
                    projList.Add(Projections.Max(paramName));
                    break;
                case Finder.GroupingCriteria.Group:
                    projList.Add(Projections.GroupProperty(paramName));
                    break;
                case Finder.GroupingCriteria.Min:
                    projList.Add(Projections.Min(paramName));
                    break;
            }

            query.SetProjection(projList);
        }

        private NHibernate.SqlCommand.JoinType GetJoinType(SearchParameter parameter)
        {
            NHibernate.SqlCommand.JoinType joinType;
            switch (parameter.JoinType)
            {
                case SearchParameter.JoinTypes.Inner:
                    joinType = NHibernate.SqlCommand.JoinType.InnerJoin;
                    break;
                case SearchParameter.JoinTypes.Left:
                    joinType = NHibernate.SqlCommand.JoinType.LeftOuterJoin;
                    break;
                case SearchParameter.JoinTypes.Right:
                    joinType = NHibernate.SqlCommand.JoinType.RightOuterJoin;
                    break;
                case SearchParameter.JoinTypes.Full:
                    joinType = NHibernate.SqlCommand.JoinType.FullJoin;
                    break;
                default: joinType = NHibernate.SqlCommand.JoinType.InnerJoin;
                    break;
            }
            return joinType;
        }
    }
}
