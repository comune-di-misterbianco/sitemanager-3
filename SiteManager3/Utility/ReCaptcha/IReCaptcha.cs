﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReCaptcha
{
    public interface IReCaptcha<T>
    {
        T Verify();
    }
}
