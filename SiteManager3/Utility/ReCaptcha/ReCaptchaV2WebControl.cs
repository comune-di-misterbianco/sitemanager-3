﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ReCaptcha
{
    public class ReCaptchaV2WebControl : WebControl
    {
        public ReCaptchaV2WebControl() : base (System.Web.UI.HtmlTextWriterTag.Div)
        {
            this.CssClass = "re-container";
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.Controls.Add(new LiteralControl("<script src=\""+ReCaptchaSettings.ApiUrl+"\" async defer></script>"));
            this.Controls.Add(new LiteralControl(callback));

            WebControl gReCaptcha = new WebControl(HtmlTextWriterTag.Div);
            gReCaptcha.CssClass = "g-recaptcha";
            gReCaptcha.Attributes["data-sitekey"] = ReCaptchaSettings.GooglePublicKey;

            this.Controls.Add(gReCaptcha);
        }

        // codificare la callback javascript
        string callback = @"
                            <script type=""text/javascript"">  
                            var onloadCallback = function() {  
                            alert(""reCaptcha is ready !!!"");  
                            };  
                            </script>";
    }
}
