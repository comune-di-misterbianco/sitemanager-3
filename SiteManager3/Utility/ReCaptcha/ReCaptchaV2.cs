﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;

namespace ReCaptcha
{
    public class ReCaptchaV2 : IReCaptcha<ReCaptchaV2Result>
    {
        /// <summary>
        /// Gets or sets the _recaptchaV2Data field. This field indicates that this is the input data for Google Recaptcha version 2.
        /// </summary>
        private readonly ReCaptchaV2Data _recaptchaV2Data;

        /// <summary>
		/// Initializes a new instance of the <see cref="T:GoogleRecaptcha.RecaptchaV2"/> class with the specified input data.
		/// </summary>
		/// <param name="recaptchaV2Data">the input data for Google Recaptcha version 2</param>
		public ReCaptchaV2(ReCaptchaV2Data recaptchaV2Data)
        {
            // Oops! Not be able to accept null for the input data
            if (recaptchaV2Data == null)
            {
                throw new ArgumentNullException("recaptchaV2Data");
            }
            _recaptchaV2Data = recaptchaV2Data;
        }

        public ReCaptchaV2Result Verify()
        {
            // Prepares the data to implement the POST request
            var postData = String.Format("secret={0}&response={1}&remoteip={2}", ReCaptchaSettings.GooglePrivateKey, _recaptchaV2Data.Response, _recaptchaV2Data.RemoteIp);

            using (var webClient = new WebClient())
            {
                webClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                // Sends a POST request to verify the recaptcha.
                var jsonResult = webClient.UploadString(ReCaptchaSettings.GoogleSiteVerify, postData);

                // Init the json serializer
                var dataContractJsonSerializer = new DataContractJsonSerializer(typeof(ReCaptchaV2Result));

                // Deserialize the result from the server into the strongly-type object
                using (var ms = new MemoryStream(Encoding.ASCII.GetBytes(jsonResult)))
                {
                    return dataContractJsonSerializer.ReadObject(ms) as ReCaptchaV2Result;
                }
            }
        }

        private ReCaptchaV2WebControl control;
        public ReCaptchaV2WebControl GetControl
        {
            get
            {
                if (control == null)
                    control = new ReCaptchaV2WebControl();
                return control;
            }
        }
    }
}
