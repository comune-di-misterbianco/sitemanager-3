﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ReCaptcha
{
    public class ReCaptchaSettings
    {        
        private static NameValueCollection Section
        {
            get
            {
                try
                {
                    return (NameValueCollection)ConfigurationManager.GetSection("ReCaptcha");
                }
                catch (Exception)
                {
                    
                    throw new ArgumentNullException("ReCaptcha", "La section ReCaptcha  non è stata settata nel web.config.");
                }
            }
        }

        public static string ApiUrl
        {
            get
            {
                string apiurl = string.Empty;
                try
                {
                    if (Section["ApiUrl"] != null)
                        apiurl = Section["ApiUrl"].ToString();
                }
                catch(Exception)
                {
                    throw new ArgumentNullException("ApiUrl", "La key ApiUrl non è stata settata nella section ReCaptcha nel web.config.");
                }
                return apiurl;
            }
        }

        public static string GoogleSiteVerify
        {
            get
            {
                string siteVerify = string.Empty;
                try
                {
                    if (Section["SiteVerify"] != null)
                        siteVerify = Section["SiteVerify"].ToString();                
                }
                catch(Exception)
                {
                    throw new ArgumentNullException("SiteVerify", "La key SiteVerify non è stata settata nella section ReCaptcha nel web.config.");
                }
                return siteVerify;
            }
        }

        public static string GooglePublicKey
        {
            get
            {
                string publicKey = string.Empty;
                try
                {
                    if (Section["PublicKey"] != null)
                        publicKey = Section["PublicKey"].ToString();
                }
                catch(Exception)
                {
                    throw new ArgumentNullException("PublicKey", "La key PublicKey non è stata settata nella section ReCaptcha nel web.config.");
                }

                return publicKey;
            }
        }

        public static string GooglePrivateKey
        {
            get
            {
                string privateKey = string.Empty;

                try
                {
                    if (Section["PrivateKey"] != null)
                        privateKey = Section["PrivateKey"].ToString();
                }
                catch(Exception)
                {
                    throw new ArgumentNullException("PrivateKey", "La key PrivateKey non è stata settata nella section ReCaptcha nel web.config.");
                }
                return privateKey;
            }
        }
    }
}
