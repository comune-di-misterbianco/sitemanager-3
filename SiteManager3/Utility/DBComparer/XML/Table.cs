﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Xml;

namespace DBComparer.XML
{
    public class XMLTable:Table
    {
        private XmlNode _TableNode;
        public XmlNode TableNode
        {
            get { return _TableNode; }
        }

        private string networkName;

        public XMLTable(string name,XmlNode tabNode, string networkName)
            : base(name, null)
        {
            _TableNode = tabNode;
            this.networkName = networkName;
        }

        protected override void InitConstraints()
        {
            if (TableNode != null)
            {
                XmlNode cs = TableNode.SelectSingleNode("constraints");
                XmlNodeList constraints = cs.SelectNodes("constraint");
                foreach (XmlNode constraint in constraints)
                {
                    string constraintText = string.Format(constraint.InnerXml, networkName);
                    this.Constraints.Add(constraintText);
                }
            }
        }

        public override string BuildAlterTableAdd(string args)
        {
            return "ALTER TABLE `" + Name + "` ADD " + args + ";\n";
        }

        public override string BuildAlterTableDrop(string args)
        {
            return "ALTER TABLE `" + Name + "` DROP `" + args + "`;\n";
        }

        public override string BuildAlterTableDropConstraint(string con)
        {
            return "ALTER TABLE `" + Name + "` DROP FOREIGN KEY `" + con + "`;\n";
        }

        protected override void InitFields()
        {
            if (TableNode != null)
            {
                XmlNode fs = TableNode.SelectSingleNode("fields");
                XmlNodeList fields = fs.SelectNodes("field");

                foreach (XmlNode field in fields)
                {
                    DataTable auxTab = new DataTable();
                    auxTab.Columns.Add("Field");
                    auxTab.Columns.Add("Type");
                    auxTab.Columns.Add("Key");
                    auxTab.Columns.Add("Null");
                    auxTab.Columns.Add("default");
                    auxTab.Columns.Add("extra");

                    string name = field.Attributes["Name"].InnerXml.ToString();
                    string key = "";
                    if (bool.Parse(field.Attributes["PrimaryKey"].InnerXml.ToString()))
                        key = "pri";
                    else
                        if (bool.Parse(field.Attributes["ExternalKey"].InnerXml.ToString()))
                            key = "mul";
                    string isNull = "";
                    if (field.Attributes["NotNull"].InnerXml.ToString() != "")
                        isNull = "no";
                    else
                        isNull = "yes";
                    string extra = "";
                    string def = "";
                    if (field.Attributes["Default"].InnerXml.ToString().Contains("AUTO_INCREMENT"))
                        extra = "auto_increment";
                    else
                    {
                        string[] vals = field.Attributes["Default"].InnerXml.ToString().Trim().Split(' ');
                        if (vals[1] == "NULL")
                            def = "";
                        else
                        {
                            for (int i = 1; i < vals.Length; i++)
                                def += vals[i] + " ";
                            def = def.Trim().Replace("'", "");
                        }
                    }
                    string tipo = "";
                    tipo = field.Attributes["Type"].InnerXml.ToString();

                    auxTab.Rows.Add(name,tipo, key, isNull, def, extra);

                                      

                    this.Fields.Add(new XMLField(auxTab.Rows[0],this));
                }
            }
        }

        public override string BuildCreateSQL()
        {
            string sql = "";
            string tab = "  ";
            string pkey = "";
            string keys = "";
            string constraints = "";

            foreach (Field f in this.Fields)
            {
                sql += (sql.Length > 0 ? "," : "") + "\n" + tab + f.BuildCreateString();

                if (f.PrimaryKey)
                    pkey += "," + "\n" + tab + "PRIMARY KEY (`" + f.Name + "`)";
                if (f.ExternalKey)
                    pkey += "," + "\n" + tab + "KEY `" + f.Name + "` (`" + f.Name + "`)";
            }

            foreach (string constraint in Constraints)
                constraints += constraints;
            sql = "CREATE TABLE IF NOT EXISTS `" + this.Name + "` (" + sql + pkey + keys + constraints + "\n) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;";

            return sql;
        }
    }
}
