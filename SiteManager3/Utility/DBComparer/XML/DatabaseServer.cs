﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Xml;

namespace DBComparer.XML
{
    public class XMLDatabaseServer:DatabaseServer
    {
        private  XmlDocument document;
        public  XmlDocument Document
        {
            get
            {
                if (document == null)
                {
                    document = new XmlDocument();
                    try
                    {
                        document.Load(ConnectionString);
                    }
                    catch (Exception ex)
                    {
                    }
                }
                return document;
            }
        }  

        public XMLDatabaseServer(string connectionString):base(connectionString)
        {
        }

        protected override void InitDatabases()
        {
            XmlNode root = Document.DocumentElement;
            XmlNode dbs = root.SelectSingleNode("databases");
            XmlNodeList databases = dbs.SelectNodes("database");

            foreach(XmlNode db in databases)
            {
                this.Databases.Add(db.Attributes["DBName"].InnerXml.ToString(), new XMLDatabase(db.Attributes["DBName"].InnerXml.ToString(), db));
            }


            //string sql = "SHOW DATABASES";
            //DataTable table = this.Connection.SqlQuery(sql);

            //foreach(DataRow row in table.Rows)
                
        }
    }
}
