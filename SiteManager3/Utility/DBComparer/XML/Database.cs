﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Xml;

namespace DBComparer.XML
{
    public class XMLDatabase:Database
    {
        private XmlNode _DBNode;
        public XmlNode DBNode
        {
            get { return _DBNode; }
        }

        public XMLDatabase(string name, XmlNode dbnode)
            : base(name)
        {
            _DBNode = dbnode;
        }

        public override string DisableForeignKeyCheck()
        {
            return "SET FOREIGN_KEY_CHECKS = 0;\n";
        }

        public override string EnableForeignKeyCheck()
        {
            return "SET FOREIGN_KEY_CHECKS = 1;\n";
        }

        protected override void InitTables()
        {
            if (DBNode != null)
            {
                XmlNode tabs = DBNode.SelectSingleNode("tables");
                XmlNodeList tables = tabs.SelectNodes("table");

                foreach (XmlNode tab in tables)
                {
                    this.Tables.Add(tab.Attributes["TableName"].InnerXml.ToString(), new XMLTable(tab.Attributes["TableName"].InnerXml.ToString(), tab, ""));
                }

            }
            //string sql = "SHOW TABLES FROM " + this.Name + "";
            //DataTable table = ((MySqlDatabaseServer)Server).Connection.SqlQuery(sql);

            //foreach(DataRow row in table.Rows)
            //    this.Tables.Add(row[0].ToString(), new MySqlTable(row[0].ToString(), this));
        }
    }
}
