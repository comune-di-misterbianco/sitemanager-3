﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBComparer
{
    public sealed class FactoryManager : IDisposable
    {
        public enum DataSourceType
        {
            MySql,
            XML
        }

        private string ConnectionString;

        private DBComparer.DatabaseServer _Server;
        public DBComparer.DatabaseServer Server
        {
            get { return _Server; }
        }


        public FactoryManager(DataSourceType Type,string ConnString)
        {
            ConnectionString = ConnString;
            switch(Type)
            {
                case DataSourceType.MySql: _Server = new MySql.MySqlDatabaseServer(ConnectionString);
                    break;
                case DataSourceType.XML: _Server = new XML.XMLDatabaseServer(ConnectionString);
                    break;
            };
        }


        public void Dispose()
        {
            _Server = null;
            ConnectionString = null;
        }
    }
}
