﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Xml;

namespace DBComparer
{
    public abstract class Database
    {
        public TableCollection Tables
        {
            get
            {
                if (_Tables == null)
                {
                    _Tables = new TableCollection();
                    InitTables();
                }
                return _Tables;
            }
        }
        private TableCollection _Tables;
        /*
        public DatabaseServer Server
        {
            get
            {
                return _Server;
            }
            set
            {
                _Server = value;
            }
        }
        private DatabaseServer _Server;
        */
        public string Name
        {
            get
            {
                return _Name;
            }
            private set
            {
                _Name = value;
            }
        }
        private string _Name; 

        public Database(string name)
        {
            this.Name = name;
        }

        protected abstract void InitTables();

        public XmlElement getXMLNode(XmlDocument document)
        {
            var rootNode = document.CreateElement("database");

            var dbNameAttribute = document.CreateAttribute("DBName");
            dbNameAttribute.InnerText = Name;
            rootNode.Attributes.Append(dbNameAttribute);

            var tablesNode = document.CreateElement("tables");
            rootNode.AppendChild(tablesNode);

            foreach (KeyValuePair<string, Table> t in Tables)
                tablesNode.AppendChild(t.Value.getXMLNode(document));

            return rootNode;
        }

        public string SaveXML()
        {
            XmlDocument document = new XmlDocument();

            var rootNode = document.CreateElement("tables");
            document.AppendChild(rootNode);

            foreach (KeyValuePair<string,Table> t in Tables)
                rootNode.AppendChild(t.Value.getXMLNode(document));

            document.Save("Database\\" + Name + ".xml");
            return document.InnerXml.ToString();
        }

        public abstract string DisableForeignKeyCheck();

        public abstract string EnableForeignKeyCheck();
    }
}
