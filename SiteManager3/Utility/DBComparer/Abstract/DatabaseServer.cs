﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Xml;

namespace DBComparer
{
    public abstract class DatabaseServer
    {
        public DatabaseCollection Databases
        {
            get
            {
                if (_Databases == null)
                {
                    _Databases = new DatabaseCollection();
                    InitDatabases();
                }
                return _Databases;
            }
        }
        private DatabaseCollection _Databases;

        public string ConnectionString
        {
            get
            {
                return _ConnectionString;
            }
            private set
            {
                _ConnectionString = value;
            }
        }
        private string _ConnectionString;

        public string Name
        {
            get
            {
                return _Name;
            }
            private set
            {
                _Name = value;
            }
        }
        private string _Name;

        public DatabaseServer(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        protected abstract void InitDatabases();

        public string SaveXML()
        {
            XmlDocument document = new XmlDocument();

            var serverNode = document.CreateElement("server");
            document.AppendChild(serverNode);
            var ServerNameAttribute = document.CreateAttribute("ServerName");
            ServerNameAttribute.InnerText = Name;
            serverNode.Attributes.Append(ServerNameAttribute);

            var rootNode = document.CreateElement("databases");
            serverNode.AppendChild(rootNode);

            foreach (KeyValuePair<string,Database> db in Databases)
                rootNode.AppendChild(db.Value.getXMLNode(document));

            document.Save("Server.xml");
            return document.InnerXml.ToString();
        }
    }
}
