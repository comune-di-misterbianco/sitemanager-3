﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Xml;

namespace DBComparer
{
    public abstract class Table
    {
        public FieldCollection Fields
        {
            get
            {
                if (_Fields == null)
                {
                    _Fields = new FieldCollection();
                    InitFields();
                }
                return _Fields;
            }
        }
        private FieldCollection _Fields;

        public Database Database
        {
            get
            {
                return _Database;
            }
            private set
            {
                _Database = value;
            }
        }
        private Database _Database;

        public string Name
        {
            get
            {
                return _Name;
            }
            private set
            {
                _Name = value;
            }
        }
        private string _Name;

        private List<String> _Constraints;
        public List<String> Constraints
        {
            get
            {
                if (_Constraints == null)
                {
                    _Constraints = new List<string>();
                    InitConstraints();
                }
                return _Constraints;
            }
        }

        public Table(string name, Database ownerDatabase)
        {
            this.Database = ownerDatabase;
            this.Name = name;
        }

        protected abstract void InitFields();

        protected abstract void InitConstraints();

        public abstract string BuildCreateSQL();

        public abstract string BuildAlterTableAdd(string args);

        public abstract string BuildAlterTableDrop(string args);

        public abstract string BuildAlterTableDropConstraint(string con);

        public XmlElement getXMLNode(XmlDocument document)
        {
            //XmlDocument document = new XmlDocument();
      
            var rootNode = document.CreateElement("table");

            var tableNameAttribute = document.CreateAttribute("TableName");
            tableNameAttribute.InnerText = Name;
            rootNode.Attributes.Append(tableNameAttribute);

            var constraintsNodes = document.CreateElement("constraints");
            rootNode.AppendChild(constraintsNodes);

            foreach (string constraint in Constraints)
            {
                var cNode = document.CreateElement("constraint");
                cNode.InnerText = constraint.Replace("&amp;","&");
                constraintsNodes.AppendChild(cNode);
            }

            var fieldsNodes = document.CreateElement("fields");
            rootNode.AppendChild(fieldsNodes);

            //document.AppendChild(rootNode);
            foreach(Field f in Fields)
                fieldsNodes.AppendChild(f.getXMLNode(document));


            return rootNode;
        }

        public string SaveXML()
        {
            XmlDocument document = new XmlDocument();
            
            document.AppendChild(getXMLNode(document));
            document.Save("Tabelle\\" + Name + ".xml");
            return document.InnerXml.ToString();
        }

        public bool Compare(Table tabToCompare, out string script)
        {
            script = "";
            bool equal = true;
            /*
            #region Elimino le chiavi esterne non presenti nella tabella con cui faccio il confronto
            foreach (string fKey in Constraints)
            {
                string key = fKey.Split(' ')[3].Replace("`", "");
                if (!tabToCompare.Constraints.Contains(fKey)) //&& !KeyToExclude.Contains(key))
                {
                    equal = false;
                    script += BuildAlterTableDropConstraint(key);
                }
            }
            #endregion
            */

            #region Confronto i field delle tabelle
            //List<string> KeyToExclude = new List<string>();
            int fieldCount = Fields.Count;
            foreach (Field field in Fields)
            {
                if (tabToCompare.Fields.Contains(field))
                {
                    #region Se entrambe le tabelle hanno il field passo alla comparazione degli attributi
                    string command;
                    Field toCompare = tabToCompare.Fields.Find(f => f.GetHashCode() == field.GetHashCode());
                    equal = equal & field.Compare(toCompare, out command);
                    script += command;
                    #endregion
                }
                else
                {
                    #region Se la tabella con cui confronto non contiene il field allora lo elimino e se è legato a chiavi esterne, elimino anche quelle
                    equal = false;
                    //bool linked = false;
                    //foreach (string fKey in Constraints)
                    //{
                    //    string key = "";
                    //    if (fKey.Contains("FOREIGN KEY (`" + field.Name + "`)"))
                    //    {
                    //        linked = true;
                    //        key = fKey.Split(' ')[3].Replace("`", "");
                    //        KeyToExclude.Add(key);
                    //        script += BuildAlterTableDropConstraint(key);
                    //    }
                    //}
                    //if (linked)
                    script += BuildAlterTableDrop(field.Name);
                    fieldCount--;
                    #endregion
                }
            }
            #endregion
           
            #region Se il numero di field è minore del numero di field della tabella con cui faccio il confronto, aggiungo quelli che mancano
            if (fieldCount < tabToCompare.Fields.Count)
            {
                foreach (Field field in tabToCompare.Fields)
                {
                    if (!Fields.Contains(field))
                    {
                        equal = false;
                        script += BuildAlterTableAdd(field.BuildCreateString());
                    }
                }
            }
            #endregion
 /*
            #region Aggiungo le chiavi esterne mancanti, presenti nella tabella con cui faccio il confronto
            foreach (string fKey in tabToCompare.Constraints)
            {
                string key = "CONSTRAINT " + fKey.Split(new string[] { "CONSTRAINT " }, StringSplitOptions.None)[1];
                if (!Constraints.Contains(fKey))
                {
                    equal = false;
                    script += BuildAlterTableAdd(key);
                }
            }
            #endregion
            */
            return equal;
        }

        public override int GetHashCode()
        {
            return Name.ToLower().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Name.ToLower() == ((Table)obj).Name.ToLower();
        }
    }
}
