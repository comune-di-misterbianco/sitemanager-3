﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Xml;

namespace DBComparer
{
    public abstract class Field
    {
        protected DataRow Row
        {
            get
            {
                return _Row;
            }
            set
            {
                _Row = value;
            }
        }
        private DataRow _Row;

        public Table Table
        {
            get
            {
                return _Table;
            }
            private set
            {
                _Table = value;
            }
        }
        private Table _Table;

        public bool PrimaryKey
        {
            get
            {
                string value = Row["Key"].ToString().ToLower();

                return value == "pri";
            }
        }
        public bool ExternalKey
        {
            get
            {
                string value = Row["Key"].ToString().ToLower();

                return value == "mul";
            }
        }
        public string Name
        {
            get
            {
                return Row["Field"].ToString();
            }
        }

        public string Type
        {
            get
            {
                return Row["Type"].ToString();
            }
        }

        protected string Default
        {
            get
            {
                string defaultValue = Row["default"].ToString();
                string value = Row["extra"].ToString().ToLower();
                if(value.Contains("auto_increment"))
                    return " AUTO_INCREMENT";
                else
                    return " DEFAULT " + (((defaultValue.Length > 0 && NotNull.Length == 0) || NotNull.Length > 0) ? "'" + defaultValue + "'" : "NULL");
            }
        }

        protected string NotNull
        {
            get
            {
                string value = Row["Null"].ToString().ToLower();

                return value == "yes" ? "" : " NOT NULL";
            }
        }


        public Field(DataRow row, Table table)
        {
            this.Row = row;
            this.Table = table;
        }

        public abstract string BuildCreateString();

        public abstract string BuildModifyString(string tipo, string def, string notNull);

        public XmlElement getXMLNode(XmlDocument doc)
        {
            var fieldNode = doc.CreateElement("field");
            
            var primaryKeyAttribute = doc.CreateAttribute("PrimaryKey");
            primaryKeyAttribute.InnerText = PrimaryKey.ToString();
            fieldNode.Attributes.Append(primaryKeyAttribute);

            var externalKeyAttribute = doc.CreateAttribute("ExternalKey");
            externalKeyAttribute.InnerText = ExternalKey.ToString();
            fieldNode.Attributes.Append(externalKeyAttribute);

            var nameAttribute = doc.CreateAttribute("Name");
            nameAttribute.InnerText = Name;
            fieldNode.Attributes.Append(nameAttribute);

            var typeAttribute = doc.CreateAttribute("Type");
            typeAttribute.InnerText = Type;
            fieldNode.Attributes.Append(typeAttribute);

            var defaultAttribute = doc.CreateAttribute("Default");
            defaultAttribute.InnerText = Default;
            fieldNode.Attributes.Append(defaultAttribute);

            var notNullAttribute = doc.CreateAttribute("NotNull");
            notNullAttribute.InnerText = NotNull;
            fieldNode.Attributes.Append(notNullAttribute);

            return fieldNode;
        }

        public override int GetHashCode()
        {
            return Name.ToLower().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Name.ToLower() == ((Field)obj).Name.ToLower();
        }

        public bool Compare(Field fToCompare,out string script)
        {
            bool equal = true;
            if (PrimaryKey != fToCompare.PrimaryKey)
                equal = false;
            //if (ExternalKey != fToCompare.ExternalKey)
            //    equal = false;
            if (Name != fToCompare.Name)
                equal = false;

            string tipo = Type;
            if (Type != fToCompare.Type)
            {
                equal = false;
                tipo = fToCompare.Type;
            }

            string def = Default;
            if (Default != fToCompare.Default)
            {
                equal = false;
                def = fToCompare.Default;
            }

            string notNull = NotNull;
            if (NotNull != fToCompare.NotNull)
            {
                equal = false;
                notNull = fToCompare.NotNull;                                
            }
            script = "";
            if (!equal)
                script = BuildModifyString(tipo,def,notNull);
            return equal;
        }

        
    }
}
