﻿using System;
using System.Collections.Generic;
using System.Data;

namespace DBComparer.MySql
{
    public class MySqlField:Field
    {
        public MySqlField(DataRow row, Table table):base(row,table)
        {
        }

        public override string BuildModifyString(string tipo, string def, string notNull)
        {
            return "ALTER TABLE " + Table.Name + " MODIFY `" + Name + "` " + tipo + " " + def + " " + notNull + ";\n";
        }

        public override string BuildCreateString()
        {
            string sql = "";
            sql += "`" + Name + "`";//name 
            sql += " " + Type + ""; //data_type 
            sql += NotNull ; //[NOT NULL | NULL]  
            sql += Default; //[DEFAULT default_value][AUTO_INCREMENT] 
 
            return sql;
        }
    }
}
