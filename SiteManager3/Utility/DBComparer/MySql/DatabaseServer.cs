﻿using System.Data;

namespace DBComparer.MySql
{
    public class MySqlDatabaseServer:DatabaseServer
    {
        public NetCms.Connections.Connection ServerConnection
        {
            get
            {
                if (_Connection == null)
                {
                    _Connection = new NetCms.Connections.MySqlConnection(ConnectionString);
                }
                return _Connection;
            }
        }
        private NetCms.Connections.Connection _Connection;

        public MySqlDatabaseServer(string connectionString):base(connectionString)
        {
        }

        protected override void InitDatabases()
        {
            string sql = "SHOW DATABASES";
            DataTable table = this.ServerConnection.SqlQuery(sql);

            foreach(DataRow row in table.Rows)
                this.Databases.Add(row["database"].ToString(), new MySqlDatabase(row["database"].ToString(), this.ServerConnection));
        }
    }
}
