﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace DBComparer.MySql
{
    public class MySqlTable:Table
    {
        public new MySqlDatabase Database
        {
            get
            {
                return (MySqlDatabase)base.Database;
            }
        }

        public MySqlTable(string name, Database ownerDatabase):base(name,ownerDatabase)
        {
        }

        protected override void InitFields()
        {
            string sql = "DESC " + this.Database.Name + "." + this.Name;
            DataTable table = ((MySqlDatabase)this.Database).Connection.SqlQuery(sql);

            foreach (DataRow row in table.Rows)
                this.Fields.Add(new MySqlField(row, this));
        }

        protected override void InitConstraints()
        {
            string tab = "  ";
            string constraint = "";
            DataTable constraintsTable = ((MySqlDatabase)this.Database).Connection.SqlQuery(@"
select * from
information_schema.key_column_usage 
WHERE information_schema.key_column_usage.table_schema = '" + this.Database.Name + @"' AND
information_schema.key_column_usage.table_name = '" + this.Name + @"' AND
CONSTRAINT_NAME <> 'PRIMARY'
");

            foreach (DataRow row in constraintsTable.Rows)
            {
                constraint = "," + "\n" + tab;
                constraint += "CONSTRAINT `" + row["CONSTRAINT_NAME"] + "` FOREIGN KEY (`" + row["COLUMN_NAME"] + "`) REFERENCES `" + row["REFERENCED_TABLE_NAME"] + "` (`" + row["REFERENCED_COLUMN_NAME"] + "`)";
                constraint += " ON DELETE CASCADE ON UPDATE CASCADE";
                this.Constraints.Add(constraint);
            }
        }

        public override string BuildAlterTableAdd(string args)
        {
            return "ALTER TABLE `" + Name + "` ADD " + args + ";\n";
        }

        public override string BuildAlterTableDrop(string args)
        {
            return "ALTER TABLE `" + Name + "` DROP `" + args + "`;\n";
        }

        public override string BuildAlterTableDropConstraint(string con)
        {
            return "ALTER TABLE `" + Name + "` DROP FOREIGN KEY `" + con + "`;\n";
        }

        public override string BuildCreateSQL()
        {
            string sql = "";
            string tab = "  ";
            string pkey = "";
            string keys = "";
            string constraints = "";

            foreach (Field f in this.Fields)
            {
                sql += (sql.Length > 0 ? "," : "") + "\n" + tab + f.BuildCreateString();

                if (f.PrimaryKey)
                    pkey += "," + "\n" + tab + "PRIMARY KEY (`" + f.Name + "`)";
                if (f.ExternalKey)
                    pkey += "," + "\n" + tab + "KEY `" + f.Name + "` (`" + f.Name + "`)";
            }

//            DataTable constraintsTable = ((MySqlDatabaseServer)this.Database.Server).Connection.SqlQuery(@"
//select * from
//information_schema.key_column_usage 
//WHERE information_schema.key_column_usage.table_schema = '" + this.Database.Name + @"' AND
//information_schema.key_column_usage.table_name = '" + this.Name + @"' AND
//CONSTRAINT_NAME <> 'PRIMARY'
//");

//            foreach (DataRow row in constraintsTable.Rows)
//            {
//                constraints += "," + "\n" + tab;
//                constraints += "CONSTRAINT `" + row["CONSTRAINT_NAME"] + "` FOREIGN KEY (`" + row["COLUMN_NAME"] + "`) REFERENCES `" + row["REFERENCED_TABLE_NAME"] + "` (`" + row["REFERENCED_COLUMN_NAME"] + "`)";
//                constraints += " ON DELETE CASCADE ON UPDATE CASCADE";
//            }
            foreach (string constraint in Constraints)
                constraints += constraints;
            sql = "CREATE TABLE `" + this.Name + "` (" + sql + pkey + keys + constraints + "\n) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;";

            return sql;
        }
    }
}
