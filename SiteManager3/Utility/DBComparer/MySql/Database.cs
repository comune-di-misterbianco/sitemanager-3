﻿using System.Data;

namespace DBComparer.MySql
{
    public class MySqlDatabase:Database
    {
        public NetCms.Connections.Connection Connection
        {
            get
            {
                return _Connection;
            }
        }
        private NetCms.Connections.Connection _Connection;

        public MySqlDatabase(string name, NetCms.Connections.Connection connection)
            : base(name)
        {
            _Connection = connection;
        }

        protected override void InitTables()
        {
            string sql = "SHOW TABLES FROM " + this.Name + "";
            DataTable table = Connection.SqlQuery(sql);

            foreach(DataRow row in table.Rows)
                this.Tables.Add(row[0].ToString(), new MySqlTable(row[0].ToString(), this));
        }

        public override string DisableForeignKeyCheck()
        {
            return "SET FOREIGN_KEY_CHECKS = 0;\n";
        }

        public override string EnableForeignKeyCheck()
        {
            return "SET FOREIGN_KEY_CHECKS = 1;\n";
        }
    }
}
