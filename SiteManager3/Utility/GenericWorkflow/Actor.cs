using System;
using System.Collections;
using System.Collections.Generic;

namespace GenericWorkflow.DAO
{
    public class Actor
    {
        public Actor()
        {
            AllowedSteps = new HashSet<Step>();
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Name
        {
            get;
            set;
        }

        public virtual ICollection<Step> AllowedSteps
        {
            get;
            set;
        }
    }
}

