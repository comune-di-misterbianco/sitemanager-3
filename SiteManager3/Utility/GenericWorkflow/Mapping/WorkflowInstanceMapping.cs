﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace GenericWorkflow.DAO
{
    public class WorkflowInstanceMapping : ClassMap<WorkflowInstance>
    {
        public WorkflowInstanceMapping()
        {
            Table("gw_workflowsinstances");
            Id(x => x.ID);
            Map(x => x.Name);
            Map(x => x.Description);
            Map(x => x.CreationDate);
            Map(x => x.Status);
            References(x => x.Parent)
                 .Column("Parent")
                 .Nullable()
                 .Fetch.Select();
            HasMany(x => x.Childs)
               .KeyColumn("Parent")
               .Fetch.Select().AsSet()
               .Cascade.All();
            References(x => x.Workflow)
                .Column("IDworkflow")
                .Fetch.Select();
            HasMany(x => x.Hystory)
                .Inverse()
                .KeyColumn("IDworkflowinstance")
                .Fetch.Select().AsSet()
                .Cascade.All();
        }
    }
}
