﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace GenericWorkflow.DAO
{
    public class StepMapping : ClassMap<Step>
    {
        public StepMapping()
        {
            Table("gw_steps");
            Id(x => x.ID);
            Map(x => x.Name);
            Map(x => x.Order).Column("orderstep");
            References(x => x.Workflow)
                .Column("IDworkflow")
                .Fetch.Select();
            HasManyToMany(x => x.Actors)
                .ChildKeyColumn("IDactor")
                .ParentKeyColumn("IDstep")
                .Table("gw_roles")
                .Inverse()
                .Fetch.Select()
                .AsSet();
        }
    }
}
