﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace GenericWorkflow.DAO
{
    public class WorkflowMapping : ClassMap<Workflow>
    {
        public WorkflowMapping()
        {
            Table("gw_workflows");
            Id(x => x.ID);
            Map(x => x.Name);
            Map(x => x.Key).Column("workflowkey");
            HasMany(x => x.Steps)
                .Inverse()
                .KeyColumn("IDworkflow")
                .OrderBy("Order")
                .Fetch.Select().AsSet()
                .Cascade.All();
            HasMany(x => x.Instances)
                .Inverse()
                .KeyColumn("IDworkflow")
                .Fetch.Select().AsSet()
                .Cascade.All();
        }
    }
}
