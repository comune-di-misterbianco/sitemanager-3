﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace GenericWorkflow.DAO
{
    public class ActorMapping : ClassMap<Actor>
    {
        public ActorMapping()
        {
            Table("gw_actors");
            Id(x => x.ID);
            Map(x => x.Name);
            HasManyToMany(x => x.AllowedSteps)
                .ChildKeyColumn("IDstep")
                .ParentKeyColumn("IDactor")
                .Table("gw_roles")
                .Fetch.Select()
                .AsSet();
        }
    }
}
