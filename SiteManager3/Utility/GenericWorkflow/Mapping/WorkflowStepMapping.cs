﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace GenericWorkflow.DAO
{
    public class WorkflowStepMapping : ClassMap<WorkflowStep>
    {
        public WorkflowStepMapping()
        {
            Table("gw_workflow_steps");
            Id(x => x.ID);
            Map(x => x.IsCurrent);
            Map(x => x.Value);
            Map(x => x.Direction);
            Map(x => x.Date);
            References(x => x.Step)
                .Column("IDstep")
                .Fetch.Select();
            References(x => x.WorkflowInstance)
                .Column("IDworkflowinstance")
                .Fetch.Select();
            References(x => x.PreviousState)
                .Column("IDpreviousworkflowstep")
                .Fetch.Select();
        }
    }
}
