﻿using System.Linq;
using System;
using System.Collections;

namespace GenericWorkflow.DAO
{
    public partial class WorkflowStep
    {
        public WorkflowStep()
        {
            this.Date = DateTime.Now;
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual bool IsCurrent
        {
            get;
            set;
        }

        public virtual string Value
        {
            get;
            set;
        }

        public enum StepDirection
        {
            Start, Forward, Backward, Branch, Merge, End
        }

        public virtual StepDirection Direction
        {
            get;
            set;
        }

        public virtual DateTime Date
        {
            get;
            set;
        }

        public virtual WorkflowStep PreviousState
        {
            get;
            set;
        }

        public virtual WorkflowInstance WorkflowInstance
        {
            get;
            set;
        }

        public virtual Step Step
        {
            get;
            set;
        }
    }
}

