﻿using System;
using System.Collections;
using GenericDAO.DAO;
using NetCms.DBSessionProvider;

namespace GenericWorkflow.DAO
{
    public partial class WorkflowInstance
    {
        private bool StepCanBePerformed()
        {
            if (this.Status == WorkflowStatus.Complete) return false;

            if (this.Parent != null)
            {
                if (this.Parent.Status == WorkflowStatus.Complete) return true;
                else
                {
                    Step stepToPerform = this.GetCurrentWorkflowStep().Step.Next;

                    bool canBePerformed = false;

                    foreach (WorkflowStep WorkflowStep in this.Parent.Hystory)
                        if (WorkflowStep.Step.ID == stepToPerform.ID) canBePerformed = true;

                    return canBePerformed;
                }
            }

            return true;
        }

        public virtual void PerformStepForward()
        {
            PerformStepForward(null);
        }

        public virtual void PerformStepForward(string value)
        {
            if (StepCanBePerformed())
            {
                IGenericDAO<WorkflowStep> sDao = new CriteriaNhibernateDAO<WorkflowStep>(FluentSessionProvider.Instance.GetSession());
                IGenericDAO<WorkflowInstance> iDao = new CriteriaNhibernateDAO<WorkflowInstance>(FluentSessionProvider.Instance.GetSession());

                WorkflowStep current = this.GetCurrentWorkflowStep();
                WorkflowStep workflowStep = new WorkflowStep();
                workflowStep.Step = current == null ? this.Workflow.GetFirstStep() : current.Step.Next;
                workflowStep.Date = DateTime.Now;
                workflowStep.IsCurrent = true;
                workflowStep.PreviousState = current;
                workflowStep.WorkflowInstance = this;
                workflowStep.Value = value;

                if (workflowStep.Step.IsEnd)
                    workflowStep.Direction = WorkflowStep.StepDirection.End;
                else
                {
                    if (this.Parent != null)
                    {
                        WorkflowStep parentCurrent = this.Parent.GetCurrentWorkflowStep();
                        if (parentCurrent != null && workflowStep.Step.Order >= parentCurrent.Step.Order)
                            workflowStep.Direction = WorkflowStep.StepDirection.Merge;
                    }
                    else
                        workflowStep.Direction = WorkflowStep.StepDirection.Forward;
                }

                sDao.SaveOrUpdate(workflowStep);

                if (current != null)
                {
                    current.IsCurrent = false;
                    sDao.SaveOrUpdate(current);
                }

                if (workflowStep.Direction == WorkflowStep.StepDirection.End || workflowStep.Direction == WorkflowStep.StepDirection.Merge)
                {
                    this.Status = WorkflowStatus.Complete;
                    iDao.SaveOrUpdate(this);
                }
            }
        }
        
        public virtual void PerformStepBack(string value)
        {
            WorkflowStep current = this.GetCurrentWorkflowStep();
            if (current != null)
            {
                IGenericDAO<WorkflowStep> sDao = new CriteriaNhibernateDAO<WorkflowStep>(FluentSessionProvider.Instance.GetSession());

                WorkflowStep workflowStep = new WorkflowStep();
                workflowStep.Step = current.Step.Prev;
                workflowStep.Date = DateTime.Now;
                workflowStep.IsCurrent = true;
                workflowStep.PreviousState = current;
                workflowStep.WorkflowInstance = this;
                workflowStep.Value = value;
                workflowStep.Direction = WorkflowStep.StepDirection.Backward;
                sDao.SaveOrUpdate(workflowStep);

                if (current != null)
                {
                    current.IsCurrent = false;
                    sDao.SaveOrUpdate(current);
                }
            }
        }

        public virtual void PerformStepBack(string value, int targetCardinal)
        {
            WorkflowStep current = this.GetCurrentWorkflowStep();
            if (current != null)
            {
                IGenericDAO<WorkflowStep> sDao = new CriteriaNhibernateDAO<WorkflowStep>(FluentSessionProvider.Instance.GetSession());

                Step targetStep = current.Step;
                while (targetStep != null && targetCardinal < targetStep.Order)
                    targetStep = targetStep.Prev;

                WorkflowStep workflowStep = new WorkflowStep();
                workflowStep.Step = targetStep;
                workflowStep.Date = DateTime.Now;
                workflowStep.IsCurrent = true;
                workflowStep.PreviousState = current;
                workflowStep.WorkflowInstance = this;
                workflowStep.Value = value;
                workflowStep.Direction = WorkflowStep.StepDirection.Backward;
                sDao.SaveOrUpdate(workflowStep);

                if (current != null)
                {
                    current.IsCurrent = false;
                    sDao.SaveOrUpdate(current);
                }
            }
        }
        /// <summary>
        /// Permette di effettuare un fork sul workflow corrente creando un workflow figlio.
        /// Il workflow figlio inizierà dallo step passato per paramentro e sarà vivo finchè non raggiungera lo stato attuale del workflow genitore.
        /// </summary>
        /// <param name="step">Lo stato iniziale del workflow figlio.</param>
        /// <returns>Il workflow figlio</returns>
        public virtual WorkflowInstance ForkBack(Step step, string value)
        {
            IGenericDAO<WorkflowInstance> iDao = new CriteriaNhibernateDAO<WorkflowInstance>(FluentSessionProvider.Instance.GetSession());

            WorkflowStep current = this.GetCurrentWorkflowStep();
            
            if (step.ID == current.Step.ID)
                return null; //Controllo che il nuovo step sia diverso dal precendete.

            //Duplico il workflow corrente
            WorkflowInstance child = new WorkflowInstance();
            child.Description = this.Description;
            child.Name = this.Name;
            child.Status = WorkflowStatus.Open;
            child.Workflow = this.Workflow;
            child.Parent = this;
            this.Childs.Add(child);

            WorkflowStep wstep = new WorkflowStep();
            wstep.Step = step;
            wstep.IsCurrent = true;
            wstep.Value = value;
            wstep.WorkflowInstance = child;
            wstep.Direction = WorkflowStep.StepDirection.Branch;

            child.Hystory.Add(wstep);

            return iDao.SaveOrUpdate(child);
        }
    }
}

