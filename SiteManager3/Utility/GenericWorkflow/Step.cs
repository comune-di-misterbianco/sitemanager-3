﻿using System.Collections.Generic;
using System.Linq;

namespace GenericWorkflow.DAO
{
    public class Step
    {
        public Step()
        {
            Actors = new HashSet<Actor>();
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual int Order
        {
            get;
            set;
        }

        //public virtual bool IsEnd
        //{
        //    get;
        //    set;
        //}

        public virtual string Name
        {
            get;
            set;
        }

        public virtual ICollection<Actor> Actors
        {
            get;
            set;
        }

        public virtual Workflow Workflow
        {
            get;
            set;
        }

        //public virtual Step Next
        //{
        //    get;
        //    set;
        //}

        public virtual bool IsStart
        {
            get { return Order == 1; }
        }

        public virtual bool IsEnd
        {
            get
            {
                return Next == null;
            }
        }
        
        public virtual Step Prev
        {
            get
            {
                try
                {
                    return this.Workflow.Steps.First(x => x.Order == this.Order - 1);
                }
                catch
                {
                    return null;
                }
                //IGenericDAO<Step> dao = new CriteriaNhibernateDAO<Step>(FluentSessionProvider.Instance.GetSession());
                //SearchParameter workflowSP = new SearchParameter(null, "Workflow", this.Workflow, Finder.ComparisonCriteria.Equals, false);
                //SearchParameter orderSP = new SearchParameter(null, "Order", this.Order - 1, Finder.ComparisonCriteria.Equals, false);

                //return dao.GetByCriteria(new SearchParameter[] { workflowSP, orderSP });
            }
        }

        public virtual Step Next
        {
            get
            {
                try
                {
                    return this.Workflow.Steps.First(x => x.Order == this.Order + 1);
                }
                catch
                {
                    return null;
                }
                //IGenericDAO<Step> dao = new CriteriaNhibernateDAO<Step>(FluentSessionProvider.Instance.GetSession());
                //SearchParameter workflowSP = new SearchParameter(null, "Workflow", this.Workflow, Finder.ComparisonCriteria.Equals, false);
                //SearchParameter orderSP = new SearchParameter(null, "Order", this.Order + 1, Finder.ComparisonCriteria.Equals, false);

                //return dao.GetByCriteria(new SearchParameter[] { workflowSP, orderSP });
            }
        }
    }
}

