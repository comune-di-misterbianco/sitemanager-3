﻿using System;
using System.Collections;
using System.Collections.Generic;
using NetCms.DBSessionProvider;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using System.Linq;

namespace GenericWorkflow.DAO
{
    public class Workflow
    {
        public Workflow()
        {
            Steps = new HashSet<Step>();
            Instances = new HashSet<WorkflowInstance>();
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Name
        {
            get;
            set;
        }

        public virtual string Key
        {
            get;
            set;
        }

        public virtual ICollection<WorkflowInstance> Instances
        {
            get;
            set;
        }

        public virtual ICollection<Step> Steps
        {
            get;
            set;
        }

        public virtual WorkflowInstance CreateInstance(string name, string description)
        {
            IGenericDAO<WorkflowInstance> dao = new CriteriaNhibernateDAO<WorkflowInstance>(FluentSessionProvider.Instance.GetSession());
            WorkflowInstance workflowInstance = new WorkflowInstance();
            workflowInstance.Name = name;
            workflowInstance.Description = description;
            workflowInstance.Workflow = this;
         
            return dao.SaveOrUpdate(workflowInstance);
        }


        public virtual Step GetFirstStep()
        {
            return GetStepByOrder(1);
        }

        public virtual Step GetStepByOrder(int order)
        {
            return Steps.First(x => x.Order == order);

            //IGenericDAO<Step> dao = new CriteriaNhibernateDAO<Step>(FluentSessionProvider.Instance.GetSession());
            
            //SearchParameter workflowSP = new SearchParameter(null, "Workflow", this, Finder.ComparisonCriteria.Equals, false);
            //SearchParameter orderSP = new SearchParameter(null, "Order", order, Finder.ComparisonCriteria.Equals, false);

            //return dao.GetByCriteria(new SearchParameter[] { workflowSP, orderSP });
        }

        //public static IList ListByPage(int page, int pageSize, string orderPropertyName)
        //{
        //    //using (var session = ActiveRecordMediator.GetSessionFactoryHolder().GetSessionFactory(typeof(Workflow)).OpenSession())
        //    using (var scope = new SessionScope())
        //    {
        //        var holder = ActiveRecordMediator.GetSessionFactoryHolder();
        //        ISession session = holder.CreateSession(typeof(Workflow));
        //        ISession session2 = holder.CreateSession(typeof(Workflow));

        //        int start = ((page + 1) * (pageSize)) - pageSize;

        //        try
        //        {
        //            ICriteria criteria = session.CreateCriteria(typeof(Workflow));
        //            criteria.SetMaxResults(pageSize);
        //            criteria.SetFirstResult(start);
        //            criteria.AddOrder(new Order(orderPropertyName, false));

        //            return criteria.List();
        //        }
        //        catch { throw new Exception("Could not perform ListByPage for Workflow"); }
        //        //finally { holder.ReleaseSession(session); }
        //        return null;
        //    }
        //}

        public static ICollection<Workflow> ListByPage(int page, int pageSize, string orderPropertyName)
        {
            IGenericDAO<Workflow> dao = new CriteriaNhibernateDAO<Workflow>(FluentSessionProvider.Instance.GetSession());

            int start = ((page + 1) * (pageSize)) - pageSize;

            return dao.FindByCriteria(start, pageSize, orderPropertyName, true, new SearchParameter[]{});
        }


        public static int GetRecordCount()
        {
            IGenericDAO<Workflow> dao = new CriteriaNhibernateDAO<Workflow>(FluentSessionProvider.Instance.GetSession());
            return dao.FindByCriteriaCount(new SearchParameter[] { });
        }
    }
}

