﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using GenericDAO.DAO;
using NetCms.DBSessionProvider;

namespace GenericWorkflow.DAO
{
    public partial class WorkflowInstance
    {
        public WorkflowInstance()
        {
            CreationDate = DateTime.Now;
            Hystory = new HashSet<WorkflowStep>();
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual WorkflowInstance Parent
        {
            get;
            set;
        }

        public virtual ICollection<WorkflowInstance> Childs
        {
            get;
            set;
        }

        public virtual Workflow Workflow
        {
            get;
            set;
        }

        public virtual DateTime CreationDate
        {
            get;
            set;
        }

        public virtual string Name
        {
            get;
            set;
        }

        public virtual string Description
        {
            get;
            set;
        }

        public enum WorkflowStatus
        {
            NotYetStarted = 0, Open = 1, Complete = 2, Suspended = 3
        }

        public virtual WorkflowStatus Status
        {
            get;
            set;
        }

        public virtual ICollection<WorkflowStep> Hystory
        {
            get;
            set;
        }

        public virtual WorkflowStep CurrentWorkflowStep
        {
            get
            {
                if (_CurrentWorkflowStep == null)
                {
                    _CurrentWorkflowStep = GetCurrentWorkflowStep();
                }
                return _CurrentWorkflowStep;
            }
            set { _CurrentWorkflowStep = value; }
        }
        private WorkflowStep _CurrentWorkflowStep;

        public virtual WorkflowStep GetCurrentWorkflowStep()
        {
            return this.Hystory.FirstOrDefault(x => x.IsCurrent == true);
            
            //NHibernate.Criterion.Conjunction dis = new Conjunction();
            //dis.Add(Expression.Eq("WorkflowInstance", this));
            //dis.Add(Expression.Eq("IsCurrent", true));

            //var res = ActiveRecordMediator<WorkflowStep>.FindAll(dis);

            //if (res.Length == 1)
            //{
            //    return (WorkflowStep)res.GetValue(0);
            //}
            //return null;
        }

        public virtual WorkflowStep GetWorkflowStepByStep(Step step)
        {
            return this.Hystory.FirstOrDefault(x => x.Step == step);

            //NHibernate.Criterion.Conjunction dis = new Conjunction();
            //dis.Add(Expression.Eq("WorkflowInstance", this));
            //dis.Add(Expression.Eq("Step", step));

            //var res = ActiveRecordMediator<WorkflowStep>.FindAll(new Order[] { new Order("ID", true) }, dis);

            //if (res.Length > 0)
            //{
            //    return (WorkflowStep)res.GetValue(0);
            //}
            //return null;
        }

        public virtual void StartWorkflow()
        {
            if (this.Status == WorkflowStatus.NotYetStarted)
            {
                IGenericDAO<WorkflowInstance> dao = new CriteriaNhibernateDAO<WorkflowInstance>(FluentSessionProvider.Instance.GetSession());

                WorkflowStep workflowStep = new WorkflowStep();
                workflowStep.Step = this.Workflow.GetFirstStep();
                workflowStep.Date = DateTime.Now;
                workflowStep.IsCurrent = true;
                workflowStep.PreviousState = null;
                workflowStep.WorkflowInstance = this;
                workflowStep.Value = null;
                //dao.SaveOrUpdate(workflowStep);

                this.Hystory.Add(workflowStep);

                this.Status = WorkflowStatus.Open;

                dao.SaveOrUpdate(this);
            }
            else
                throw new Exception("Il workflow è già stato avviato.");
        }
    }
}

