﻿using NetService.Utility.ValidatedFields;
using System;
namespace NetService.Utility.RecordsFinder
{
    public class VfTextBoxSearchParameter : VfSearchParameter
    {
        public override object Value
        {
            get
            {
                if (!string.IsNullOrEmpty(Field.Request.StringValue))
                {
                    VfTextBox field = (VfTextBox) Field;
                    if (field.InputType == InputTypes.Number && field.Request.IsValidInteger)
                        return field.Request.IntValue;
                    if (field.InputType == InputTypes.Valuta && field.Request.IsValidDecimal)
                        return field.Request.DecimalValue;
                    if (field.InputType == InputTypes.SingleFloating)
                    {
                        float tempSingleFloat = 0;
                        if (Single.TryParse(field.Request.OriginalValue, out tempSingleFloat))
                        return Single.Parse(field.Request.OriginalValue);
                    }
                    if (field.InputType == InputTypes.Floating && field.Request.IsValidDouble)
                        return field.Request.DoubleValue;
                    return Field.Request.StringValue;
                }

                return null;
            }
            protected set { }
        }

        public VfTextBoxSearchParameter(VfTextBox field, Finder.ComparisonCriteria comparisonCriteria)
            : base(field, comparisonCriteria)
        {
        }

    }
}
