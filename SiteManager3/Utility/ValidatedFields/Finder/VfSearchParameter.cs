﻿using System;
namespace NetService.Utility.RecordsFinder
{
    public class VfSearchParameter : SearchParameter
    {
        public override SearchParameter.RelationTypes RelationType
        {
            get
            {
                return RelationTypes.OneToMany;
            }
        }

        public ValidatedFields.VfGeneric Field { get; protected set; }

        public override object Value
        {
            get
            {
                object value = null;
                //Codice nuovo aggiunto da Angelo per gestire i return type
                if (Field.ReturnObjectType != null)
                {
                    if (Field.ReturnObjectType == typeof(int))
                        value = Field.Request.IntValue;
                    //Se non si controlla che la stringa sia piena, restituisce un'eccezione, pertanto ho aggiunto il controllo
                    if (Field.ReturnObjectType == typeof(DateTime) && !string.IsNullOrEmpty(Field.Request.StringValue))
                        value = DateTime.Parse(Field.Request.StringValue);
                    if (Field.ReturnObjectType == typeof(string))
                        value = Field.Request.StringValue;
                }
                else
                {
                    //Questo deve essere così per mantenere la compatibilità con la vecchia modalità
                    value = string.IsNullOrEmpty(Field.Request.StringValue) ? null : Field.Request.StringValue;
                }
                
                //return string.IsNullOrEmpty(Field.Request.StringValue) ? null : Field.Request.StringValue;
                return value;
            }
            protected set {}
        }

        public VfSearchParameter(ValidatedFields.VfGeneric field, Finder.ComparisonCriteria comparisonCriteria)
            : base(field.Label, field.ActiveRecord_ReadPropertyName, null, comparisonCriteria, false)
        {
            this.Field = field;
        }

    }
}
