﻿using NetService.Utility.ValidatedFields;

namespace NetService.Utility.RecordsFinder
{
    public class VfAutocompleteTextBoxSearchParameter : VfSearchParameter
    {
         public override object Value
        {
            get
            {
                if (!string.IsNullOrEmpty(Field.Request.StringValue))
                {
                    VfAutocompleteTextBox field = (VfAutocompleteTextBox) Field;
                    if (field.InputType == InputTypes.Number && field.Request.IsValidInteger)
                        return field.Request.IntValue;
                    if (field.InputType == InputTypes.Valuta && field.Request.IsValidDecimal)
                        return field.Request.DecimalValue;
                    if (field.InputType == InputTypes.Floating && field.Request.IsValidDouble)
                        return field.Request.DoubleValue;
                    return Field.Request.StringValue;
                }

                return null;
            }
            protected set { }
        }

         public VfAutocompleteTextBoxSearchParameter( VfAutocompleteTextBox field, Finder.ComparisonCriteria comparisonCriteria)
            : base(field, comparisonCriteria)
        {
        }
    }
}
