﻿using System.Linq;
namespace NetService.Utility.RecordsFinder
{
    public class VfCheckBoxSearchParameter : VfSearchParameter
    {
        public override object Value
        {
            get
            {
                if (new[] { "ON", "TRUE", "1" }.Any(x => x == Field.Request.StringValue.ToUpper()))
                    return 1;

                return null;
            }
            protected set { }
        }

        public VfCheckBoxSearchParameter(ValidatedFields.VfGeneric field, Finder.ComparisonCriteria comparisonCriteria)
            : base(field, comparisonCriteria)
        {
        }

    }
}
