﻿using System;
using System.ComponentModel;

namespace NetService.Utility.RecordsFinder
{
    public class VfDropDownListSearchParameter : VfSearchParameter
    {
        public override object Value
        {
            get
            {
                if (!string.IsNullOrEmpty(Field.Request.StringValue))
                {
                    if (Field.ReturnObjectType == null)
                    {
                        if (Field.Request.IsValidInteger)
                            return Field.Request.IntValue;
                        return Field.Request.StringValue;
                    }
                    else
                    {
                        //Questo codice permette di restituire il return object type anche nel search parameter ed evitare eccezioni - Giovanni
                        var converter = TypeDescriptor.GetConverter(Field.ReturnObjectType);
                        var result = converter.ConvertFrom(Field.Request.OriginalValue);
                        return result;
                    }
                        
                }

                return null;
            }
            protected set {}
        }

        public VfDropDownListSearchParameter(ValidatedFields.VfGeneric field, Finder.ComparisonCriteria comparisonCriteria)
            : base(field, comparisonCriteria)
        {
        }

    }
}
