﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web;

namespace NetService.Utility.UI
{
    public class EliminationConfirmControl : WebControl
    {
        private string _TitoloMessaggio;
        private string _Messaggio;
        private string _UrlBack;

        public bool UsedOnFrontend
        {
            get { return _UsedOnFrontend; }
            set { _UsedOnFrontend = value; }
        }
        private bool _UsedOnFrontend = false;


        public EliminationConfirmControl(string titoloMessaggio, string messaggio,string urlBack, bool isOnFrontend = false)
            : base(System.Web.UI.HtmlTextWriterTag.Div)
        {
            _TitoloMessaggio = titoloMessaggio;
            _Messaggio = messaggio;
            _UrlBack = urlBack;
            UsedOnFrontend = isOnFrontend;
        }

        public Button ConfirmButton
        {
            get 
            {
                if (_ConfirmButton == null)
                {
                    _ConfirmButton = new Button();
                    _ConfirmButton.ID = "confirm";
                    _ConfirmButton.Text = "Si";
                    _ConfirmButton.CssClass = UsedOnFrontend ? "btn btn-danger confirm-negation" : "submit"; ;
                }
                return _ConfirmButton;
            }
            set { _ConfirmButton = value; }
        }
        private Button _ConfirmButton;

        public Button NegationButton
        {
            get
            {
                if (_NegationButton == null)
                {
                    _NegationButton = new Button();
                    _NegationButton.ID = "negation";
                    _NegationButton.Text = "No";
                    _NegationButton.CssClass = UsedOnFrontend ? "btn btn-default confirm-negation" : "submit";
                    _NegationButton.Click += new EventHandler(_NegationButton_Click);
                }
                return _NegationButton;
            }
            set { _NegationButton = value; }
        }

        void _NegationButton_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Redirect(_UrlBack);
        }

        private Button _NegationButton;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!UsedOnFrontend)
            {    
                WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
                control.CssClass = "fieldset elimination_confirm";
                control.Controls.Add(new TextWebControl(HtmlTextWriterTag.Legend, _TitoloMessaggio));
                var p = new TextWebControl(HtmlTextWriterTag.P, _Messaggio);
                control.Controls.Add(p);

                control.Controls.Add(ConfirmButton);
                control.Controls.Add(NegationButton);

                this.Controls.Add(control);
            }
            else
            {
                //WebControl EliminationPanel = new WebControl(HtmlTextWriterTag.Div);
                this.CssClass = "panel panel-default elimination_confirm";

                WebControl EliminationHeading = new WebControl(HtmlTextWriterTag.Div);
                EliminationHeading.CssClass = "panel-heading";
                EliminationHeading.Attributes.Add("role", "tab");
                EliminationHeading.ID = "at-ambitiHeading";

                WebControl EliminationTitle = new WebControl(HtmlTextWriterTag.H4);
                EliminationTitle.CssClass = "panel-title";
                EliminationTitle.Controls.Add(new LiteralControl(_TitoloMessaggio));

                EliminationHeading.Controls.Add(EliminationTitle);
                this.Controls.Add(EliminationHeading);

                WebControl EliminationBody = new WebControl(HtmlTextWriterTag.Div);
                EliminationBody.CssClass = "panel-body";

                var p = new TextWebControl(HtmlTextWriterTag.P, _Messaggio);
                EliminationBody.Controls.Add(p);
                this.Controls.Add(EliminationBody);

                WebControl EliminationFooter = new WebControl(HtmlTextWriterTag.Div);
                EliminationFooter.CssClass = "panel-footer ";

                var formgroupbutton = new WebControl(HtmlTextWriterTag.Div);
                formgroupbutton.CssClass = "form-group text-right";

                formgroupbutton.Controls.Add(NegationButton);
                formgroupbutton.Controls.Add(ConfirmButton);
                      
                EliminationFooter.Controls.Add(formgroupbutton);

                this.Controls.Add(EliminationFooter);

            }
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);            
        }
    }
}
