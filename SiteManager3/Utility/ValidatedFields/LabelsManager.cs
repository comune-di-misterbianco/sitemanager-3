﻿using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web;
using System.Xml;
using System;
using System.Collections.Generic;

namespace NetService.Localization
{
    public class LabelsManager
    {
        private static Dictionary<string,string> _LabelsCache;
        public static Dictionary<string, string> LabelsCache
        {
            get
            {
                if (_LabelsCache == null)
                {
                    _LabelsCache = new Dictionary<string, string>();
                    _LabelsCache.Add("infoMandatory", "I campi con * sono obbligatori.");
                }
                return _LabelsCache;
            }
        }

        private static XmlNode xmlLabelsRoot;
        public static XmlNode XmlLabelsRoot
        {
            get
            {
                if (xmlLabelsRoot == null)
                    FindRoot();
                return xmlLabelsRoot;
            }
        }

        
        private static string _LabelsRoot;
        public static string LabelsRoot
        {
            get
            {
                if (_LabelsRoot == null)
                {
                    _LabelsRoot = string.Empty;
                    string RootPathValue = System.Web.Configuration.WebConfigurationManager.AppSettings["VfLabelsPath"].ToLower();
                    if (RootPathValue.Length > 0)
                        _LabelsRoot += RootPathValue.ToLower();
                }
                return _LabelsRoot;
            }
        }


        private static void FindRoot()
        {
            XmlDocument oXmlDoc = new XmlDocument();
            bool failed = false;
            try
            {                
                // bisogna sistemarla a regime perchè manca il riferimento al NetCms.Configuration e alla CurrentNetWork
                //string xmlFilePath = absoluteWebRoot + "\\config\\" + xmlFileName;
                string xmlFilePath = LabelsRoot;//LabelPath + "\\" + xmlFileName;
                oXmlDoc.Load(xmlFilePath);
            }
            catch (Exception ex)
            {
                //xmlConfigs = null;
                failed = true;
            }
            
            xmlLabelsRoot = oXmlDoc.DocumentElement;
        }

        protected static string FormatLabel(string label)
        {
            string outLabel = label.Replace("<![CDATA[", "").Replace("]]>", "");
            return outLabel;
        }

        public static string GetLabel(string labelResourceKey)
        {


            if (LabelsCache.ContainsKey(labelResourceKey.ToString()))
            {
                return FormatLabel(LabelsCache[labelResourceKey.ToString()]);
            }
            else
            {
                if (labelResourceKey != "" && !labelResourceKey.Contains(" "))
                {
                    try
                    {
                        XmlNodeList oNodeList = XmlLabelsRoot.SelectNodes("/labels/vf/" + labelResourceKey.ToString());
                        if (oNodeList != null && oNodeList.Count > 0)
                        {
                            string value = oNodeList[0].InnerXml;
                            LabelsCache.Add(labelResourceKey.ToString(), value);
                            return FormatLabel(value);
                        }
                    }
                    catch (Exception ex) 
                    {
                        return labelResourceKey;
                    }
                }
            }
            return labelResourceKey;

            //if(string.IsNullOrEmpty(labelResourceKey)) return "";
            //try
            //{
            //    return HttpContext.GetGlobalResourceObject("LocalizedText", labelResourceKey).ToString();
            //    //return "<span style=\"color:#009900\">" + HttpContext.GetGlobalResourceObject("LocalizedText", labelResourceKey).ToString() + "</span>" ;
            //}
            //catch 
            //{
            //    return labelResourceKey;
            //    //throw new Exception("Impossibile trovare la label '" + labelResourceKey + "', nel file di lingua");
            //}
        }

        public static string GetLabel(string labelResourceKey,params string[] args)
        {
            return string.Format(GetLabel(labelResourceKey), args);
        }

        public static LiteralControl GetLabelControl(string labelResourceKey)
        {
            return new LiteralControl(GetLabel(labelResourceKey));
        }

        public static LiteralControl GetLabelControl(string labelResourceKey, params string[] args)
        {
            return new LiteralControl(string.Format(GetLabel(labelResourceKey,args)));
        }

        public static WebControl GetWebControl(string labelResourceKey,HtmlTextWriterTag tag)
        {
            WebControl ctr = new WebControl(tag);
            ctr.Controls.Add(GetLabelControl(labelResourceKey));
            return ctr;
        }

        public static WebControl GetWebControl(string labelResourceKey, HtmlTextWriterTag tag, params string[] args)
        {

            WebControl ctr = new WebControl(tag);
            ctr.Controls.Add(GetLabelControl(labelResourceKey,args));
            return ctr;
        }
        public static WebControl GetWebControl(string labelResourceKey, string cssClass, HtmlTextWriterTag tag)
        {
            WebControl ctr = new WebControl(tag);
            ctr.CssClass = cssClass;
            ctr.Controls.Add(GetLabelControl(labelResourceKey));
            return ctr;
        }

        public static WebControl GetWebControl(string labelResourceKey, string cssClass, HtmlTextWriterTag tag, params string[] args)
        {

            WebControl ctr = new WebControl(tag);
            ctr.CssClass = cssClass;
            ctr.Controls.Add(GetLabelControl(labelResourceKey, args));
            return ctr;
        }
    }
}
