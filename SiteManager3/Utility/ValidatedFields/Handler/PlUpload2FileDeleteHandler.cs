﻿using System;
using System.Web;
using System.IO;
using System.Collections.Generic;
using System.Web.SessionState;


namespace ValidatedFields.Handler 
{
    public class PlUpload2FileDeleteHandler : IHttpHandler, IReadOnlySessionState
    {
        protected HttpSessionState Session;
        protected HttpContext Context;
        protected HttpResponse Response;
        protected HttpRequest Request;

        public void ProcessRequest(HttpContext context)
        {
            Context = context;
            Response = context.Response;
            Request = context.Request;
            Session = context.Session;

            // verifico se l'utente è autenticato
            if (Session["sitemanagerbackaccountsessionkey"] == null)
            {
                //var currentUser = Session["sitemanagerbackaccountsessionkey"];
                WriteErrorResponse("La richiesta non è consentita senza l'autenticazione al sistema.", 401);
                return;
            }

            Response.ContentType = "text/plain"; //???
            Response.Expires = -1;
            try
            {
                var jsonString = "";

                Request.InputStream.Position = 0;
                using (var inputStream = new StreamReader(Request.InputStream))
                {
                    jsonString = inputStream.ReadToEnd();
                }
                //string filePath = Request.Form["FileNameTOBeDeleted"];
                //var path = (JToken)jsonString["FileNameTOBeDeleted"];

                //dynamic deserializedValue = JsonConvert.DeserializeObject(jsonString);

                var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                PlUploadDeleteObj deserializedValue = ser.Deserialize<PlUploadDeleteObj>(jsonString);

                string path = deserializedValue.FileNameTOBeDeleted; // deserializedValue["FileNameTOBeDeleted"];
                string filePathFull = context.Server.MapPath(path);

                if (File.Exists(filePathFull))
                {
                    File.Delete(filePathFull);

                    //context.Response.Write("filedeleted");
                    WriteSucessResponse("filedeleted");
                }
                else
                {
                    WriteErrorResponse("filenotfound", 500);
                    //context.Response.Write("filenotfound");
                }
                Response.StatusCode = 200;
            }
            catch (Exception ex)
            {
                Response.Write("Error: " + ex.Message);
                WriteErrorResponse("Error: " + ex.Message, 500);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Sends a message to the client for each chunk
        /// </summary>
        /// <param name="message"></param>
        protected void WriteSucessResponse(string message = null)
        {
            Response.ContentType = "application/json";
            string json = null;
            if (!string.IsNullOrEmpty(message))
                json = JsonEncode(message);
            else
                json = "null";

            Response.Write("{\"jsonrpc\" : \"2.0\", \"result\" : " + json + ", \"id\" : \"id\"}");
        }

        protected void WriteErrorResponse(string message, int statusCode = 500, bool endResponse = false)
        {
            Response.ContentType = "application/json";
            Response.StatusCode = statusCode;

            // Write out raw JSON string to avoid JSON requirement
            Response.Write("{\"jsonrpc\" : \"2.0\", \"error\" : {\"code\": " + statusCode.ToString() + ", \"message\": " + JsonEncode(message) + "}, \"id\" : \"id\"}");
            if (endResponse)
                Response.End();
        }

        /// <summary>
        /// Encode JavaScript
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string JsonEncode(object value)
        {
            var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
            return ser.Serialize(value);
        }
    }

    public class PlUploadDeleteObj
    {
        public string FileNameTOBeDeleted { get; set; }
    }

    /// <summary>
    /// Writes out an error response
    /// </summary>
    /// <param name="message"></param>
    /// <param name="statusCode"></param>
    /// <param name="endResponse"></param>
  
}
