﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidatedFields.Handler
{
    public class PlUpload2FileFilter
    {
        public PlUpload2FileFilter(string title, string extensions)
        {
            Title = title;
            Extensions = extensions;
        }

        public string Title { get; protected set; }
        public string Extensions { get; protected set; }
    }
}
