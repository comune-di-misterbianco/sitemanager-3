﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;
using System.Web.SessionState;

namespace ValidatedFields.Handler
{
    public class PlUpload2Handler : IHttpHandler, IReadOnlySessionState
    {
        protected HttpContext Context;
        protected HttpResponse Response;
        protected HttpRequest Request;
        protected HttpSessionState Session;

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Maximum upload size in bytes
        /// default: 0 = unlimited
        /// </summary>
        private double _MaxUploadSize = 0;
        public double MaxUploadSize
        {
            get
            {
                if (UploadSetting.FileSizeLimit > 0)
                    _MaxUploadSize = UploadSetting.FileSizeLimit;
                return _MaxUploadSize;
            }
        }


        /// <summary>
        /// Comma delimited list of extensions allowed,
        /// extension preceded by a dot.
        /// Example: .jpg,.png
        /// </summary>
        private string _AllowedExtensions;// = ".jpg,.jpeg,.png,.gif,.bmp";
        public string AllowedExtensions
        {
            get
            {
                if (_AllowedExtensions == null)
                    _AllowedExtensions = UploadSetting.AllowedExtensions;
                return _AllowedExtensions;
            }

        }


        /// <summary>
        /// Physical folder location where the file will be uploaded.
        /// 
        /// Note that you can assign an IIS virtual path (~/path)
        /// to this property, which automatically translates to a 
        /// physical path.
        /// </summary>
        public string FileUploadPhysicalPath
        {
            get
            {
                // if (_FileUploadPhysicalPath.StartsWith("~"))
                if (_FileUploadPhysicalPath == null)
                    _FileUploadPhysicalPath = Context.Server.MapPath(UploadSetting.DestinationPath);
                return _FileUploadPhysicalPath;
            }
            set
            {
                _FileUploadPhysicalPath = value;
            }
        }
        private string _FileUploadPhysicalPath;


        private PlUpload2Setting _UploadSetting;
        public PlUpload2Setting UploadSetting
        {
            get
            {

                if (_UploadSetting == null)
                {
                    _UploadSetting = Session["UploadSettings"] as PlUpload2Setting;
                }
                return _UploadSetting;
            }
            //set { _UploadSetting = value; }
        }

        public void ProcessRequest(HttpContext context)
        {
            Context = context;
            Request = context.Request;
            Response = context.Response;
            Session = context.Session;

            // verifico se l'utente è autenticato
            if (Session["sitemanagerbackaccountsessionkey"] == null)
            {
                //var currentUser = Session["sitemanagerbackaccountsessionkey"];
                WriteErrorResponse("La richiesta non è consentita senza l'autenticazione al sistema.", 401);
                return;
            }
            
            // Check to see whether there are uploaded files to process them
            if (Request.Files.Count > 0)
            {
                //HttpPostedFile fileUpload = Request.Files[0];

                HttpPostedFile fileUpload = Request.Files[0];

                //string fileName = fileUpload.FileName;
                string fileName = Request.Form["filenameRenamed"].ToString();

                if (string.IsNullOrEmpty(fileName) || string.Equals(fileName, "blob"))
                    fileName = Request["name"] ?? string.Empty;

                // normalize file name to avoid directory traversal attacks            
                fileName = Path.GetFileName(fileName);

                // check for allowed extensions and block
                string ext = Path.GetExtension(fileName);
                if (!("," + AllowedExtensions.ToLower() + ",").Contains("," + ext.ToLower().Remove(0) + ","))
                {
                    WriteErrorResponse("Invalid file extension uploaded.",415);
                    return;
                }

                string tstr = Request["chunks"] ?? string.Empty;
                int chunks = -1;
                if (!int.TryParse(tstr, out chunks))
                    chunks = -1;
                tstr = Request["chunk"] ?? string.Empty;
                int chunk = -1;
                if (!int.TryParse(tstr, out chunk))
                    chunk = -1;

                // If there are no chunks sent the file is sent as one 
                // this likely a plain HTML 4 upload (ie. 1 single file)
                if (chunks == -1)
                {
                    if (MaxUploadSize == 0 || Request.ContentLength <= MaxUploadSize)
                    {
                        if (!OnUploadChunk(fileUpload.InputStream, 0, 1, fileName))
                            return;
                    }
                    else
                    {
                        WriteErrorResponse("Uploaded file is too large.", 413);
                        return;
                    }

                    OnUploadCompleted(fileName);

                    return;
                }
                else
                {
                    // this isn't exact! We can't see the full size of the upload
                    // and don't know the size of the last chunk
                    if (chunk == 0 && MaxUploadSize > 0 && Request.ContentLength * (chunks - 1) > MaxUploadSize)
                        WriteErrorResponse("Uploaded file is too large.", 413);
                }

                if (!OnUploadChunkStarted(chunk, chunks, fileName))
                    return;

                // chunk 0 is the first one
                if (chunk == 0)
                {
                    if (!OnUploadStarted(chunk, chunks, fileName))
                        return;
                }

                if (!OnUploadChunk(fileUpload.InputStream, chunk, chunks, fileName))
                    return;

                // last chunk
                if (chunk >= chunks - 1)
                {
                    // final response should just return
                    // the output you generate
                    OnUploadCompleted(fileName);
                    return;
                }

                // if no response has been written yet write a success response
                WriteSucessResponse();
            }
        }

        protected void OnUploadCompleted(string fileName)
        {
            var Server = Context.Server;

            // Physical Path is auto-transformed
            var path = FileUploadPhysicalPath;
            var fullUploadedFileName = Path.Combine(path, fileName);

            // return just a string that contains the url path to the file
            WriteUploadCompletedMessage(fullUploadedFileName);
        }

        protected bool OnUploadStarted(int chunk, int chunks, string name)
        {
            // time out files after 15 minutes - temporary upload files
            // DeleteTimedoutFiles(Path.Combine(FileUploadPhysicalPath, "*.*"), 900);                

            return true;
        }

        /// <summary>
        /// Fired on every chunk that is sent
        /// </summary>
        /// <param name="chunk"></param>
        /// <param name="chunks"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        protected virtual bool OnUploadChunkStarted(int chunk, int chunks, string fileName)
        {
            return true;
        }



        /// <summary>
        /// Writes out an error response
        /// </summary>
        /// <param name="message"></param>
        /// <param name="statusCode"></param>
        /// <param name="endResponse"></param>
        protected void WriteErrorResponse(string message, int statusCode = 500, bool endResponse = false)
        {
            Response.ContentType = "application/json";
            Response.StatusCode = statusCode;

            // Write out raw JSON string to avoid JSON requirement
            Response.Write("{\"jsonrpc\" : \"2.0\", \"error\" : {\"code\": " + statusCode.ToString() + ", \"message\": " + JsonEncode(message) + "}, \"id\" : \"id\"}");
            if (endResponse)
                Response.End();
        }

        /// <summary>
        /// Sends a message to the client for each chunk
        /// </summary>
        /// <param name="message"></param>
        protected void WriteSucessResponse(string message = null)
        {
            Response.ContentType = "application/json";
            string json = null;
            if (!string.IsNullOrEmpty(message))
                json = JsonEncode(message);
            else
                json = "null";

            Response.Write("{\"jsonrpc\" : \"2.0\", \"result\" : " + json + ", \"id\" : \"id\"}");
        }

        /// <summary>
        /// Use this method to write the final output in the OnUploadCompleted method
        /// to pass back a result string to the client when a file has completed
        /// uploading
        /// </summary>
        /// <param name="data"></param>
        protected void WriteUploadCompletedMessage(string data)
        {
            Response.Write(data);
        }

        /// <summary>
        /// Encode JavaScript
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string JsonEncode(object value)
        {
            var ser = new System.Web.Script.Serialization.JavaScriptSerializer();
            return ser.Serialize(value);
        }

        /// <summary>
        /// Stream each chunk to a file and effectively append it. 
        /// </summary>
        /// <param name="chunkStream"></param>
        /// <param name="chunk"></param>
        /// <param name="chunks"></param>
        /// <param name="uploadedFilename"></param>
        /// <returns></returns>
        protected bool OnUploadChunk(Stream chunkStream, int chunk, int chunks, string uploadedFilename)
        {
            var path = FileUploadPhysicalPath;

            // try to create the path
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch
                {
                    WriteErrorResponse("Upload directory doesn't exist and couldn't create directory.",500);
                    return false;
                }
            }

            // ensure that the filename is normalized and doesn't contain
            // any path traversal hacks
            uploadedFilename = Path.GetFileName(uploadedFilename);

            string uploadFilePath = Path.Combine(path, uploadedFilename);
            if (chunk == 0)
            {
                if (File.Exists(uploadFilePath))
                    File.Delete(uploadFilePath);
            }

            Stream stream = null;
            try
            {
                stream = new FileStream(uploadFilePath, (chunk == 0) ? FileMode.CreateNew : FileMode.Append);
                chunkStream.CopyTo(stream, 16384);
            }
            catch
            {
                WriteErrorResponse("Unable to write out file.",500);
                return false;
            }
            finally
            {
                if (stream != null)
                    stream.Dispose();
            }

            return true;
        }
    }
}
