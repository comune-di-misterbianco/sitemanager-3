﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;

namespace NetService.Utilities
{
    public class CodiceFiscaleFormValidator : Validator
    {
        private VfGeneric nome;
        private VfGeneric cognome;
        private VfGeneric dataNascita;
        private VfGeneric isMale;
        private VfGeneric codiceCatastaleComune;

        public CodiceFiscaleFormValidator(string errorMessage, VfGeneric nome, VfGeneric cognome, VfGeneric dataNascita, VfGeneric male, VfGeneric codiceCatastaleComune)
            :base(errorMessage)
        {
            this.nome = nome;
            this.cognome = cognome;
            this.dataNascita = dataNascita;
            this.isMale = male;
            this.codiceCatastaleComune = codiceCatastaleComune;
        }

        public override bool Validate(string value)
        {
            if (nome.Request.IsValidString &&
                cognome.Request.IsValidString &&
                dataNascita.Request.IsValid(Utility.Common.RequestVariable.VariableType.Date) &&
                isMale.Request.IsValidInteger &&
                codiceCatastaleComune.Request.IsValidString)
            {
                CodiceFiscaleGenerator generator = new CodiceFiscaleGenerator(nome.Request.StringValue, cognome.Request.StringValue, dataNascita.Request.DateTimeValue, isMale.Request.IntValue == 1, codiceCatastaleComune.Request.StringValue);
                return generator.CheckCodiceFiscale(value);
            }
            else
            {
                return false;
            }
        }
    }
}
