﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace System.Web.UI.WebControls
{
    public class TextWebControl:WebControl
    {
        public string Text
        {
            get
            {
                return _Text;
            }
            set
            {
                _Text = value;
            }
        }
        private string _Text;

        private LiteralControl LiteralControl
        {
            get
            {
                return _LiteralControl;
            }
            set
            {
                _LiteralControl = value;
            }
        }
        private LiteralControl _LiteralControl; 

        public TextWebControl(HtmlTextWriterTag tagName,string text)
            : base(tagName)
        {
            this.Text = text;
            this.LiteralControl = new LiteralControl();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            LiteralControl.Text = Text;
            this.Controls.Add(LiteralControl);
        }
    }
}
