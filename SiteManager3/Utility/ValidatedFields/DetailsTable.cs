using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace NetService.Utility.UI
{
    public class DetailsSheet:WebControl
    {
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
        private string _Title;

        public string Description
        {
            get { return _Description; }
            private set { _Description = value; }
        }
        private string _Description;

        public enum SeparateColumnsOptions { No, OnlyFirst, All }

        public SeparateColumnsOptions SeparateColumns
        {
            get { return _SeparateColumns; }
            set { _SeparateColumns = value; }
        }
        private SeparateColumnsOptions _SeparateColumns;

        public DetailsSheet(string title, string description):base(HtmlTextWriterTag.Div)
        {
            this.Title = title;
            this.Description = description; 
            this.SeparateColumns = SeparateColumnsOptions.OnlyFirst;
        }
        public DetailsSheet(string title, string description, Table innerTable)
            : base(HtmlTextWriterTag.Div)
        {
            this.Title = title;
            this.Description = description;
            this.SeparateColumns = SeparateColumnsOptions.OnlyFirst;
            this._InnerTable = innerTable;
        }

        public bool Alternate
        {
            get { return _Alternate; }
            private set { _Alternate = value; }
        }
        private bool _Alternate = true;
        
        public Table InnerTable
        {
            get
            {
                if (_InnerTable == null)
                {
                    _InnerTable = new Table();
                    _InnerTable.CssClass = "DetailsContainer";
                    if (this.ID!=null && this.ID != string.Empty)
                        _InnerTable.ID = this.ID + "_Table";
                    _InnerTable.CellPadding = 0;
                    _InnerTable.CellSpacing = 0;
                    _InnerTable.Rows.Add(HeaderRow);
                }
                return _InnerTable;
            }
        }
        private Table _InnerTable;

        public WebControl InnerFieldset
        {
            get
            {
                if (_InnerFieldset == null)
                {
                    _InnerFieldset = new WebControl(HtmlTextWriterTag.Fieldset);
                    if (!string.IsNullOrEmpty(this.Description))
                    {
                        HtmlGenericControl text = new HtmlGenericControl("div");
                        text.Attributes["class"] = "DetailsText";
                        InnerFieldset.Controls.Add(text);
                        text.InnerHtml = this.Description;
                    }
                }
                return _InnerFieldset;
            }
        }
        private WebControl _InnerFieldset;

        public enum Colors { Default,Green, Red, Yellow, Blue }

        private TableHeaderRow HeaderRow
        {
            get
            {
                if (_HeaderRow == null)
                    _HeaderRow = new TableHeaderRow();
                return _HeaderRow;
            }
        }
        private TableHeaderRow _HeaderRow;

        public void AddColumn(string Caption, Colors color, int columnSpan)
        {
            TableHeaderCell cell = new TableHeaderCell();
            cell.Text = Caption;
            cell.CssClass = "fill_" + color.ToString().ToLower();
            if (columnSpan > 1) cell.ColumnSpan = columnSpan;
            if (this.HeaderRow.Cells.Count == 0 && this.SeparateColumns == SeparateColumnsOptions.OnlyFirst ||(this.SeparateColumns == SeparateColumnsOptions.All))
                cell.CssClass += " first";
            this.HeaderRow.Cells.Add(cell);
        }
        public void AddColumn(string Caption, Colors color)
        {
            AddColumn(Caption, color, 1);
        }
        public void AddTitleTextRow(string title, string text)
        {
            AddTitleTextRow(title, text, Colors.Default);
        }
        public void AddTitleTextRow(string title, string text, Colors color)
        {
            TableRow row = new TableRow();

            if (title != null)
            {
                TableCell cell = new TableCell();
                if (title.Length > 0)
                    cell.Text = "<strong>" + title + ":</strong>";
                else
                    cell.Text = "&nbsp;";
                row.Cells.Add(cell);
            }
            if (text != null)
            {
                TableCell cell = new TableCell();
                if (HeaderRow.Cells.Count > 2)
                    cell.ColumnSpan = HeaderRow.Cells.Count - 1;
                if (text.Length > 0)
                    cell.Text = text;
                else
                    cell.Text = "&nbsp;";

                row.Cells.Add(cell);
            }
            AddRow(row, color);
        }
        public void AddRow(TableRow row, Colors color)
        {
            InnerTable.Rows.Add(row);

            int i = 0;
            foreach (TableCell cell in row.Cells)
            {
                if (this.HeaderRow.Cells.Count > i)
                    cell.CssClass = this.HeaderRow.Cells[i].CssClass + " " + cell.CssClass;
                i++;
            }
            row.CssClass = (row.CssClass + (this.Alternate ? " alternate" : "") + " fill_" + color.ToString().ToLower()).Trim();

            Alternate = !Alternate;
        }
        public void AddRow(params string[] cellsText)
        {
            AddRow(Colors.Default,cellsText);
        }
        public void AddRow(Colors color,params string[] cellsText)
        {
            TableRow row = new TableRow();
            int i = 0;
            foreach (string text in cellsText)
            {
                TableCell cell = new TableCell();

                if (!string.IsNullOrEmpty(text))
                        cell.Text = text;
                else
                    cell.Text = "&nbsp;";
                row.Cells.Add(cell);
                if (++i == cellsText.Length && cellsText.Length < this.HeaderRow.Cells.Count)
                {
                    cell.ColumnSpan = (this.HeaderRow.Cells.Count - cellsText.Length) + 1;
                }
            }
            AddRow(row,color);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.CssClass = "Details";
            //InnerFieldset.CssClass = "DetailsTable";

            HtmlGenericControl legend = new HtmlGenericControl("legend");
            InnerFieldset.Controls.Add(legend);
            legend.InnerHtml = this.Title;

            InnerFieldset.Controls.Add(InnerTable);
            this.Controls.Add(InnerFieldset);
            
        }
    }
}