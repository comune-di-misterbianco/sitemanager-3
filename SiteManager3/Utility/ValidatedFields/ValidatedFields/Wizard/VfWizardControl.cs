﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web;
using System.Web.UI;

namespace NetService.Utility.ValidatedFields
{
    public class VfWizardControl : Panel
    {
        const string sessionStepName = "VfWizCurrentStep_";

        public VfWizardControl(string id)
        {
            this.ID = id;
        }

        private int _CurrentStep = 0;
        public int CurrentStep
        {
            get
            {
                //NetUtility.RequestVariable currentStep = new NetUtility.RequestVariable("currentStep", NetUtility.RequestVariable.RequestType.Form);
                //if (currentStep.IsValidInteger)
                //    _CurrentStep = currentStep.IntValue;

                if (HttpContext.Current.Session[sessionStepName + this.ID] != null)
                    _CurrentStep = int.Parse(HttpContext.Current.Session[sessionStepName + this.ID].ToString());
                return _CurrentStep;
            }
            set
            {
                // verificare se posso settare il valore del campo hidden via jquery
                _CurrentStep = value;
                HttpContext.Current.Session[sessionStepName + this.ID] = _CurrentStep;
            }
        }

        private WebControl _StepScript;
        private WebControl StepScript
        {
            get
            {
                if (_StepScript == null)
                {
                    string script = @"
                        function SetCurrentStep(string valore)
                        {
                            alert(" + CurrentStep.ToString() + @" + valore);
                            ('#currentStep').value = " + CurrentStep.ToString() + @" + valore;
                        }
                    ";
                    _StepScript = new WebControl(HtmlTextWriterTag.Script);
                    _StepScript.Controls.Add(new LiteralControl(script));
                }
                return _StepScript;
            }

        }

        #region Bottoni e eventi click

        private Button _NextButton;
        public Button NextButton
        {
            get
            {
                if (_NextButton == null)
                {
                    _NextButton = new Button();
                    _NextButton.ID = this.ID + "_next";
                    _NextButton.Text = "Avanti";
                }
                return _NextButton;
            }
            set
            {
                _NextButton = value;
            }
        }

        private Button _PrevButton;
        public Button PrevButton
        {
            get
            {
                if (_PrevButton == null)
                {
                    _PrevButton = new Button();
                    _PrevButton.ID = this.ID + "_prev";
                    _PrevButton.Text = "Indietro";
                }
                return _PrevButton;
            }
            set
            {
                _PrevButton = value;
            }
        }

        private Button _EndButton;
        public Button EndButton
        {
            get
            {
                if (_EndButton == null)
                {
                    _EndButton = new Button();
                    _EndButton.ID = this.ID + "_end";
                    _EndButton.Text = "Fine";
                }
                return _EndButton;
            }
            set
            {
                _EndButton = value;
            }
        }
       

        #endregion

        private WebControl _ResultsCtrl;
        public WebControl ResultsCtrl
        {
            get
            {
                if (_ResultsCtrl == null)
                {
                    _ResultsCtrl = new WebControl(HtmlTextWriterTag.Div);
                    _ResultsCtrl.ID = "wizardresults";
                    _ResultsCtrl.CssClass = "wizardresults";
                }
                return _ResultsCtrl;
            }
            set { _ResultsCtrl = value; }
        }

        private List<VfWizardStep> _Steps;
        public List<VfWizardStep> Steps
        {
            get
            {
                if (_Steps == null)
                    _Steps = new List<VfWizardStep>();
                return _Steps;
            }
        }

        public void AddStep(VfWizardStep step)
        {
            Steps.Add(step);
        }
        // implementare il metodo Remove Step

        private HiddenField _FlCurrentStep;
        public HiddenField FlCurrentStep
        {
            get
            {
                if (_FlCurrentStep == null)
                {
                    _FlCurrentStep = new HiddenField();
                    _FlCurrentStep.ID = "currentStep";
                }
                return _FlCurrentStep;
            }
            set { _FlCurrentStep = value; }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            NetService.Utility.Common.RequestVariable btnNext = new NetService.Utility.Common.RequestVariable(NextButton.ID, NetService.Utility.Common.RequestVariable.RequestType.Form);
            NetService.Utility.Common.RequestVariable btnBack = new NetService.Utility.Common.RequestVariable(PrevButton.ID, NetService.Utility.Common.RequestVariable.RequestType.Form);
            NetService.Utility.Common.RequestVariable btnEnd = new NetService.Utility.Common.RequestVariable(EndButton.ID, NetService.Utility.Common.RequestVariable.RequestType.Form);

            if (btnNext.IsPostBack && btnNext.StringValue == NextButton.Text)
            {
                Steps[CurrentStep].NextAction();

                if (Steps[CurrentStep].PostBackStatus == VfWizardStep.StepStatus.Ok)
                    CurrentStep++;
            }
            if (btnBack.IsPostBack && btnBack.StringValue == PrevButton.Text)
            {
                Steps[CurrentStep].PrevAction();
                CurrentStep--;
            }
            if (btnEnd.IsPostBack && btnEnd.StringValue == EndButton.Text)
            {
                Steps[CurrentStep].EndAction();
            }

            // Current Step
            this.Controls.Add(new LiteralControl("Step corrente: " + Steps[CurrentStep].Label));

            // Control per notifiche ed errori
            this.Controls.Add(ResultsCtrl);

            // controllo step corrente            
            this.Controls.Add(Steps[CurrentStep]);

            this.Controls.Add(PrevButton);
            PrevButton.Visible = false;

            this.Controls.Add(EndButton);
            EndButton.Visible = false;

            this.Controls.Add(NextButton);
            NextButton.Visible = false;

            FlCurrentStep.Value = CurrentStep.ToString();

            this.Controls.Add(FlCurrentStep);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // bottoni
            if (CurrentStep == 0 || CurrentStep < Steps.Count - 1)
            {
                NextButton.Visible = true;
            }

            if (CurrentStep > 0)
            {
                PrevButton.Visible = true;
            }

            if (CurrentStep == Steps.Count - 1)
            {
                EndButton.Visible = true;
            }

        }        
    }

}
