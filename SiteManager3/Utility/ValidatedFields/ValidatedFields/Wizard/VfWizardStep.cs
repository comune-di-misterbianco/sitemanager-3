﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace NetService.Utility.ValidatedFields
{
    public class VfWizardStep : Panel
    {
        private string _Label;
        public string Label
        {
            get
            {
                return _Label;
            }
        }

        public enum StepStatus
        {
            NotSet,
            Ok,
            Error
        }

        private StepStatus postbackStatus = StepStatus.NotSet;
        public StepStatus PostBackStatus
        {
            get
            {
                return postbackStatus;
            }
            set { postbackStatus = value; }
        }

        public VfWizardStep(string id, string label)
            : base()
        {
            this.ID = id;
            _Label = label;
        }

        public delegate void PerformStep();

        private PerformStep _NextAction;
        public PerformStep NextAction
        {
            get
            {
                return _NextAction;
            }
            set
            {
                _NextAction = value;
            }
        }

        private PerformStep _PrevAction;
        public PerformStep PrevAction
        {
            get
            {
                return _PrevAction;
            }
            set
            {
                _PrevAction = value;
            }
        }

        private PerformStep _EndAction;
        public PerformStep EndAction
        {
            get
            {
                return _EndAction;
            }
            set
            {
                _EndAction = value;
            }
        }
    }
}
