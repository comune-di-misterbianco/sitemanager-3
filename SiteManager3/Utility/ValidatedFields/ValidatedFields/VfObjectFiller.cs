﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using NHibernate;
using System.Reflection;

namespace NetService.Utility.ValidatedFields
{
    public class VfObjectFiller    
    {
        public System.Type ObjectType
        {
            get
            {
                return ObjectToFill.GetType();
            }
        }
        private System.Type _ObjectType;

        public object ObjectToFill
        {
            get
            {
                return _ObjectToFill;
            }
            set
            {
                _ObjectToFill = value;
                _ObjectType = value.GetType();
            }
        }
        private object _ObjectToFill; 
        /*
        public VfManager Manager
        {
            get
            {
                return _Manager;
            }
            private set
            {
                _Manager = value;
            }
        }
        private VfManager _Manager;
        */

        public IEnumerable<VfGeneric> Fields { get; private set; } 

        public VfObjectFiller(VfManager manager, object objectToFill)
        {
            this.Fields = manager.Fields;
            this.ObjectToFill = objectToFill;
        }

        public VfObjectFiller(VfManager manager)
        {
            this.Fields = manager.Fields;
        }

        public VfObjectFiller(IEnumerable<VfGeneric> fields, object objectToFill)
        {
            this.Fields = fields;
            this.ObjectToFill = objectToFill;
        }
        public VfObjectFiller(IEnumerable<VfGeneric> fields)
        {
            this.Fields = fields;
        }

        public bool FillObject()
        {
            bool ok = true;

            foreach (var field in Fields.Where(x=>x.BindField))
            {
                object value = null;
                
                try { value = field.PostbackValueObject; }
                catch { ok = false; }
                
                if(value != null)
                    ok = FillProperty(field.ActiveRecord_WritePropertyName, value) && ok;
            }

            return ok;
        }

        public bool FillObjectAdvanced(ISession session)
        {
            bool ok = true;

            foreach (var field in Fields.Where(x => x.BindField))
            {
                object value = null;

                try {
                    value = field.PostbackValueObject;
                }
                catch {
                    ok = false;
                }

                if (value != null)
                    ok = FillPropertyAdvanced(field.ActiveRecord_WritePropertyName, value, session) && ok;
            }

            return ok;
        }

        
        public bool FillFields()
        {
            bool ok = true;

            foreach (var field in Fields.Where(x=>x.BindField))
            {
                object value = null;

                try { value = GetPropertyValue(field.ActiveRecord_ReadPropertyName); }
                catch { ok = false; }

                if (value != null)
                {
                    if (value is string)
                        value = ((string)value).Replace("''", "'");
                    field.DefaultValue = value;
                }
            }

            return ok;
        }

        public bool FillFieldsAdvanced()
        {
            bool ok = true;

            foreach (var field in Fields.Where(x => x.BindField))
            {
                object value = null;

                try { value = GetPropertyValueAdvanced(field.ActiveRecord_ReadPropertyName); }
                catch { ok = false; }

                if (value != null)
                {
                    if (value is string)
                        value = ((string)value).Replace("''", "'");
                    field.DefaultValue = value;
                }
            }

            return ok;
        }

        public bool FillProperty(string propertyName, object value)
        {
            System.Reflection.PropertyInfo prop = ObjectType.GetProperty(propertyName);            

            if (prop == null)
                throw new Exception("La classe " + ObjectType.FullName + " non contiene la proprietà "+propertyName+"");

            object[] parameters = { value };

            try
            {
                if (prop.PropertyType.IsEnum)
                {
                    parameters[0] = Enum.Parse(prop.PropertyType, value.ToString());
                }
                if (prop.PropertyType.FullName.Contains("String"))
                {
                    parameters[0] = value.ToString();
                }
                var setMethod =  prop.GetSetMethod();
                setMethod.Invoke(this.ObjectToFill, parameters);
                return true;
            }
            catch { return false; }
        }


        public bool FillPropertyAdvanced(string propertyName, object value, ISession session)
        {
            System.Reflection.PropertyInfo prop = ObjectType.GetProperty(propertyName);

            if (prop == null)
                throw new Exception("La classe " + ObjectType.FullName + " non contiene la proprietà " + propertyName + "");

            object[] parameters = { value };

            try
            {
                if (prop.PropertyType.IsEnum)
                {
                    parameters[0] = Enum.Parse(prop.PropertyType, value.ToString());
                }
                if (prop.PropertyType.FullName.Contains("String"))
                {
                    parameters[0] = value.ToString();
                }
                if (prop.PropertyType.FullName.Contains("Int32"))
                {
                    parameters[0] = int.Parse(value.ToString());
                }

                if (prop.PropertyType.FullName.Contains("Decimal"))
                {
                    parameters[0] = decimal.Parse(value.ToString());
                }

                if (prop.PropertyType.FullName.Contains("Boolean"))
                {
                    parameters[0] = bool.Parse(value.ToString());
                }

                if (prop.PropertyType.FullName.Contains("System.Double"))
                {
                    double result;
                    parameters[0] = double.TryParse(value.ToString(), out result) ? (double?)result : null;
                    //parameters[0] = double.Parse(value.ToString());
                }

                if (!prop.PropertyType.Namespace.Contains("System") && !prop.PropertyType.IsEnum)
                {
                    var daoType = typeof(GenericDAO.DAO.CriteriaNhibernateDAO<>);
                    Type[] typeArgs = { prop.PropertyType };
                    var makeDao = daoType.MakeGenericType(typeArgs);
                    var dao = Activator.CreateInstance(makeDao, session);
                    MethodInfo mi = dao.GetType().GetMethod("GetById",new Type[]{typeof(int)});
                    parameters[0] = mi.Invoke(dao, new object[]{int.Parse(value.ToString())});               
                    
                }
                var setMethod = prop.GetSetMethod();
                setMethod.Invoke(this.ObjectToFill, parameters);
                return true;
            }
            catch { return false; }
        }

        //public object GetPropertyValue(string propertyName)
        //{
        //    System.Reflection.PropertyInfo prop = ObjectType.GetProperty(propertyName);
        //    if (prop == null)
        //        throw new Exception("La classe " + ObjectType.FullName + " non contiene la proprietà " + propertyName + "");

        //    object[] parameters = { };

        //    try
        //    {
        //        return prop.GetGetMethod().Invoke(this.ObjectToFill, parameters);
        //    }
        //    catch { return null; }
        //}

        protected virtual object GetPropertyValue(object objectInstance, string Property)
        {
            var path = Property.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

            object result = objectInstance;

            foreach (string propName in path)
                result = GetPropertyValueByReflection(result, propName);

            return result == objectInstance ? null : result;
        }

        protected virtual object GetPropertyValueAdvanced(object objectInstance, string Property)
        {
            var path = Property.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

            object result = objectInstance;

            foreach (string propName in path)
                result = GetPropertyValueByReflectionAdvanced(result, propName);

            return result == objectInstance ? null : result;
        }

        public object GetPropertyValue(string Property)
        {
            return GetPropertyValue(ObjectToFill, Property);
        }

        public object GetPropertyValueAdvanced(string Property)
        {
            return GetPropertyValueAdvanced(ObjectToFill, Property);
        }
       
        private object GetPropertyValueByReflection(object objectInstance, string Property)
        {
            System.Type ObjectType = objectInstance.GetType();
            System.Reflection.PropertyInfo prop = ObjectType.GetProperty(Property);

            if (prop == null)
                throw new Exception("La classe " + ObjectType.FullName + " non contiene la proprietà " + Property + "");

            object[] parameters = { };

            try
            {
                return prop.GetGetMethod().Invoke(objectInstance, parameters);
            }
            catch { return null; }
        }

        private object GetPropertyValueByReflectionAdvanced(object objectInstance, string Property)
        {
            System.Type ObjectType = objectInstance.GetType();
            System.Reflection.PropertyInfo prop = ObjectType.GetProperty(Property);

            if (prop == null)
                throw new Exception("La classe " + ObjectType.FullName + " non contiene la proprietà " + Property + "");

            object[] parameters = { };

            try
            {
                object obj;
                if (!prop.PropertyType.Namespace.Contains("System") && !prop.PropertyType.IsEnum)
                {
                    object local_obj = prop.GetGetMethod().Invoke(objectInstance, parameters);
                    string propname = "ID";
                    System.Reflection.PropertyInfo myprop = local_obj.GetType().GetProperty(propname, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                    obj = myprop.GetValue(local_obj, null);
                }
                else
                    obj = prop.GetGetMethod().Invoke(objectInstance, parameters);
                return obj;
            }
            catch { return null; }
        }
    }
}
