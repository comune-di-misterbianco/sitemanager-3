﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetService.Utility.ValidatedFields
{
    /// <summary>
    /// Classe che eredita da VfManager ma renderizza i controlli tramite DetailsSheet
    /// </summary>
    public class VfDetailsSheetManager : VfManager
    {
        public VfDetailsSheetManager(string controlID, bool isPostBack)
            : base(controlID, isPostBack)
        {
        }
        public VfDetailsSheetManager(string controlID)
            : base(controlID)
        {
        }
        
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (AutoValidation && IsPostBack)
                Validate();

            if (TitleControl.Controls.Count > 0)
                this.Controls.Add(TitleControl);

            if (InfoControl.Controls.Count > 0)
                this.Controls.Add(InfoControl);

            foreach (var field in this.Fields)
            {
                this.Controls.Add(field);
            }
            if (this.ShowMandatoryInfo)
                this.Controls.Add(MandatoryControl);

            if (AddSubmitButton)
            {
                WebControl buttonContainer = new WebControl(HtmlTextWriterTag.Div);
                buttonContainer.CssClass = "vf submit-vf";
                buttonContainer.Controls.Add(SubmitButton);
                this.Controls.Add(buttonContainer);
                this.DefaultButton = SubmitButton.ID;
            }
        }
    }
}
