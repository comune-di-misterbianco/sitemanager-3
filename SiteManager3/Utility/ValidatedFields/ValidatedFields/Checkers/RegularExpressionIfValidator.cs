﻿using System.Text.RegularExpressions;

namespace NetService.Utility.ValidatedFields
{
    /// <summary>
    /// Questo validatore consente di specificare una espressione regolare per il campo, che verrà presa in considerazione solo nel caso in cui
    /// il valore di ExternalValue è uguale a quello di CompareValue; Ad esempio se imposto per ExternalValue il postback di una dropdown e per CompareValue
    /// una stringa contenente uno dei valori della dropdown, quando i valori coincidono, il valore del campo viene testato con l'espressione regolare e validato.
    /// In caso contrario non viene fatta nessuna validazione perchè si assume che il campo non è necessario per altri valori della dropdown.
    /// </summary>
    public class RegularExpressionIfValidator : Validator
    {
        // il valore passato esternamente da considerare nella validazione
        public object ExternalValue
        {
            get
            {
                return _ExternalValue;
            }
            private set
            {
                _ExternalValue = value;
            }
        }
        private object _ExternalValue;

        // valore opzionale per confrontare l'ExternalValue
        public object CompareValue
        {
            get
            {
                return _CompareValue;
            }
            private set
            {
                _CompareValue = value;
            }
        }
        private object _CompareValue;

        public string RegularExpression
        {
            get { return _RegularExpression; }
            private set { _RegularExpression = value; }
        }
        private string _RegularExpression;

        public enum CompareTypes { ValueNotNull, ValueNull, ValueFalse, ValueTrue }

        public RegularExpressionIfValidator(string errorMessage,string regularExpression, object externalValue, object compareValue)
            : base(errorMessage)
        {
            this.ExternalValue = externalValue;
            this.RegularExpression = regularExpression;
            this.CompareValue = compareValue;
        }

        public override bool Validate(string value)
        {
            Regex rgx = new Regex(RegularExpression);
            Match m = rgx.Match(value);
            return (ExternalValue.ToString() != CompareValue.ToString()) || (ExternalValue.ToString() == CompareValue.ToString() && m.Success);              
        }
    }
}