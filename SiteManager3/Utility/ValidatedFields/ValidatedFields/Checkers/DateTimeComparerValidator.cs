﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetService.Utility.ValidatedFields
{
    public class DateTimeComparerValidator : Validator
    {
        public DateTime CompareValue
        {
            get
            {
                return _CompareValue;
            }
            private set
            {
                _CompareValue = value;
            }
        }
        private DateTime _CompareValue;

        public Comparatore Criterio
        {
            get
            {
                return _Criterio;
            }
            private set
            {
                _Criterio = value;
            }
        }
        private Comparatore _Criterio;

        public enum Comparatore
        {
            MaggioreUguale,
            MinoreUguale,
            Uguale,
            Maggiore,
            Minore
        }

        public DateTimeComparerValidator(string errorMessage, DateTime compareValue, Comparatore criterio)
            : base(errorMessage)
        {
            CompareValue = compareValue;
            Criterio = criterio;
        }

        public override bool Validate(string value)
        {
            DateTime data;
            if (DateTime.TryParse(value, out data))
            {
                switch (Criterio)
                {
                    case Comparatore.Maggiore:
                        {
                            return data > CompareValue;
                        }
                    case Comparatore.Minore:
                        {
                            return data < CompareValue;
                        }
                    case Comparatore.MaggioreUguale:
                        {
                            return data >= CompareValue;
                        }
                    case Comparatore.MinoreUguale:
                        {
                            return data <= CompareValue;
                        }
                    case Comparatore.Uguale:
                        {
                            return data == CompareValue;
                        }
                }
            }
            else return true;
            return false;
        }
    }
}
