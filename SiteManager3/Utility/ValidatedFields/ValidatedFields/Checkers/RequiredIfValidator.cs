﻿namespace NetService.Utility.ValidatedFields
{
    public class RequiredIfValidator : Validator
    {
        // il valore passato esternamente da considerare nella validazione
        public object ExternalValue
        {
            get
            {
                return _ExternalValue;
            }
            private set
            {
                _ExternalValue = value;
            }
        }
        private object _ExternalValue;

        // valore opzionale per confrontare l'ExternalValue
        public object CompareValue
        {
            get
            {
                return _CompareValue;
            }
            private set
            {
                _CompareValue = value;
            }
        }
        private object _CompareValue;

        public CompareTypes ValidIf
        {
            get
            {
                return _ValidIf;
            }
            set
            {
                _ValidIf = value;
            }
        }
        private CompareTypes _ValidIf = CompareTypes.ValueNotNull;

        public enum CompareTypes { ValueNotNull, ValueNull, ValueFalse, ValueTrue }

        public RequiredIfValidator(string errorMessage, object externalValue, CompareTypes validIf, object compareValue = null)
            : base(errorMessage)
        {
            this.ExternalValue = externalValue;
            this.ValidIf = validIf;
            this.CompareValue = compareValue;
        }

        public override bool Validate(string value)
        {
            if (CompareValue != null)
            {
                return (ExternalValue != null && ExternalValue.ToString() != CompareValue.ToString()) || (ExternalValue != null && ExternalValue.ToString() == CompareValue.ToString() && !string.IsNullOrEmpty(value));
            }
            else
            {
                // in questo caso non mi interessa il value, ma l'ExternalValue passato come paramentro al costruttore
                // il value è il valore del field, quindi non mi interessa qui
                switch (this.ValidIf)
                {
                    case CompareTypes.ValueNotNull: return ((ExternalValue != null && !string.IsNullOrEmpty(value)) || ExternalValue == null);
                    case CompareTypes.ValueNull: return ((ExternalValue == null && !string.IsNullOrEmpty(value)) || ExternalValue != null);
                    case CompareTypes.ValueFalse: return ((!bool.Parse(ExternalValue.ToString()) && !string.IsNullOrEmpty(value)) || bool.Parse(ExternalValue.ToString()));
                    case CompareTypes.ValueTrue: return ((bool.Parse(ExternalValue.ToString()) && !string.IsNullOrEmpty(value)) || !bool.Parse(ExternalValue.ToString()));
                }
            }
            return false;
        }
    }
}
