﻿
using System.Globalization;
namespace NetService.Utility.ValidatedFields
{
    public class ValutaComparerValidator : Validator
    {
        public decimal CompareValue
        {
            get
            {
                return _CompareValue;
            }
            private set
            {
                _CompareValue = value;
            }
        }
        private decimal _CompareValue;

        public CompareTypes ValidIf
        {
            get
            {
                return _ValidIf;
            }
            set
            {
                _ValidIf = value;
            }
        }
        private CompareTypes _ValidIf = CompareTypes.Equal;

        public enum CompareTypes { Equal, Major, Minor, MajorEqual, MinorEqual, Different }

        public ValutaComparerValidator(string errorMessage, decimal compareValue, CompareTypes validIf)
            : base(errorMessage)
        {
            this.CompareValue = compareValue;
            this.ValidIf = validIf;
        }

        public override bool Validate(string value)
        {
            decimal decimalValue;
            if (!value.Contains("."))
            {
                if (decimal.TryParse(value.Replace(",", "."), System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out decimalValue))
                {
                    switch (this.ValidIf)
                    {
                        case CompareTypes.Equal: return decimalValue == this.CompareValue;
                        case CompareTypes.Major: return decimalValue > this.CompareValue;
                        case CompareTypes.MajorEqual: return decimalValue >= this.CompareValue;
                        case CompareTypes.Minor: return decimalValue < this.CompareValue;
                        case CompareTypes.MinorEqual: return decimalValue <= this.CompareValue;
                        case CompareTypes.Different: return decimalValue != this.CompareValue;
                    }
                }
                else
                {
                    ErrorMessage = "";
                }
            }
            else
            {
                ErrorMessage = "Per le valute inserisci la virgola e nessun punto";
            }
            return false;
        }
    }
}
