﻿using System;
namespace NetService.Utility.ValidatedFields
{
    public class IsHolidayValidator : Validator
    {
        public IsHolidayValidator(string errorMessage)
            : base(errorMessage)
        {
        }

        public override bool Validate(string value)
        {
            DateTime data;
            if (DateTime.TryParse(value, out data))
            {
                return IsNotHoliday(data);
            }
            else return true;
        }

        private bool IsNotHoliday(DateTime data)
        {
            if (data.DayOfWeek == DayOfWeek.Sunday)
                return false;
            
            if ((data.Day == 25 || data.Day == 26) && data.Month == 12)
                return false;

            if (data.Day == 25 && data.Month == 4)
                return false;

            if (data.Day == 1 && (data.Month == 1 || data.Month == 5 || data.Month == 11))
                return false;

            if (data.Day == 2 && data.Month == 6)
                return false;

            if (data.Day == 6 && data.Month == 1)
                return false;

            if (data.Day == 15 && data.Month == 8)
                return false;

            if (data.Day == 8 && data.Month == 12)
                return false;

            return true;
        }
    }
}
