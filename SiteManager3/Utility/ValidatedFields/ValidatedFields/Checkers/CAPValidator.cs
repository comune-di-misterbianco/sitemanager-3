﻿
namespace NetService.Utility.ValidatedFields
{
    public class CAPValidator : Validator
    {
        public CAPValidator(string errorMessage)
            : base(errorMessage)
        {
            
        }

        public override bool Validate(string value)
        {
            if (string.IsNullOrEmpty(value))
                return true;

            int result;
            return int.TryParse(value,out result) && value.Length == 5;
        }
    }
}
