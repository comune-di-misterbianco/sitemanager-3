﻿using System;

namespace NetService.Utility.ValidatedFields
{
    public class DatabaseValueChecker : Validator
    {
        public System.Type ActiveRecordType
        {
            get
            {
                return _ActiveRecordType;
            }
            set
            {
                _ActiveRecordType = value;
            }
        }
        private System.Type _ActiveRecordType;

        public string PropertyName
        {
            get
            {
                return _PropertyName;
            }
            private set
            {
                _PropertyName = value;
            }
        }
        private string _PropertyName;

        public DatabaseValueChecker(string errorMessage,string propertyName, System.Type activeRecordType):base(errorMessage)
        {
            this.PropertyName = propertyName;
            this.ActiveRecordType = activeRecordType;
        }

        public override bool Validate(string value)
        {
            System.Reflection.MemberInfo method =  ActiveRecordType.GetMethod("GetByProperty");
            if(method == null)
                throw new Exception("La classe "+ActiveRecordType.FullName +" non contiene il metodo statico GetByProperty(string propertyName, string value).");

            return  ActiveRecordType.GetMethod("GetByProperty").Invoke(null, new object[] { PropertyName, value }) != null;
        }
    }
}
