﻿using NetService.Utility.ValidatedFields;
using System.Collections.Generic;
using System.Linq;

namespace NetService.Utility.ValidatedFields
{
   
    public class RequiredIfValidatorList : Validator
    {
        /// <summary>
        /// ICollection di object da comparare
        /// </summary>
        public IList<object> ExternalValueList
        {
            get
            {
                return _ExternalValueList;
            }
            private set
            {
                _ExternalValueList = value;
            }
        }
        private IList<object> _ExternalValueList;
                                    
        /// <summary>
        /// Elemento di comparazione
        /// </summary>
        public object CompareValue
        {
            get
            {
                return _CompareValue;
            }
            private set
            {
                _CompareValue = value;
            }
        }
        private object _CompareValue;

        /// <summary>
        /// Metodo di comparazione
        /// </summary>
        public CompareTypes ValidIf
        {
            get
            {
                return _ValidIf;
            }
            set
            {
                _ValidIf = value;
            }
        }
        private CompareTypes _ValidIf = CompareTypes.ValueNotNull;

        public enum CompareTypes
        {
            /// <summary>
            /// Compara tutti gli elementi se sono != null
            /// </summary>
            ValueNotNull,
            /// <summary>
            /// Compara tutti gli elementi se sono == null
            /// </summary> 
            ValueNull,
            /// <summary>
            /// Compara se tutti gli elementi di una collezione di booleani sono tutti == true
            /// </summary>
            ValueTrue,
            /// <summary>
            /// Compara se tutti gli elementi di una collezione di booleani sono tutti == false
            /// </summary>
            ValueFalse,
            /// <summary>
            /// Verifica la collezione di oggetti contiene l'oggetto da confrontare
            /// </summary>
            ValueIn,
            /// <summary>
            /// Verifica se l'oggetto da confrontare non è inlcuso nella collezione di oggetti
            /// </summary>
            ValueExcluded,
            /// <summary>
            /// Compara se tutti gli elementi della collezione di oggetti sono tutti uguali all'oggetto da comparare
            /// </summary>
            ValueAllEquals,
            /// <summary>
            /// Verifica se l'oggetto da confrontare non è inlcuso nella collezione di oggetti e nega il risultato
            /// </summary>
            ValueNegateExcluded,
            /// <summary>
            /// Compara se tutti gli elementi della collezione di oggetti sono tutti uguali all'oggetto da comparare  e nega il risultato
            /// </summary>
            ValueNegateAllEquals,
            /// <summary>
            /// Verifica se esiste almeno un oggetto della collezione è uguale all'elemento comparatore. 
            /// </summary>
            ValueOr,
            /// <summary>
            /// Verfica se almeno uno degli elementi della collezione è uguale all'oggetto da comparare
            /// </summary>
            ValueAnyEquals,
            /// <summary>
            /// Verifica se almeno un'oggetto della collezione verifica è diverso dall'oggetto da comparare 
            /// </summary>
            ValueAnyExcluded,
            /// <summary>
            /// Verfica se almeno uno degli elementi della collezione è uguale all'oggetto da comparare e nega il risultato
            /// </summary>
            ValueNegateAnyEquals,
            /// <summary>
            /// Verifica se almeno un'oggetto della collezione verifica è diverso dall'oggetto da comparare e nega il risultato
            /// </summary>
            ValueNegateAnyExcluded,
            /// <summary>
            /// Verifica se il campo è settato a false e tutti gli elementi non sono settati a true
            /// </summary>
            ValueSelfAndAllAreFalse,
        }

             
        /// <summary>
        /// Required If ValidatorList 
        /// Compara lista di object con un valore 
        /// </summary>
        /// <param name="errorMessage">MessageError</param>
        /// <param name="externalValueList">Collection di object da confrontare</param>
        /// <param name="validIf">Metodo di confronto</param>
        /// <param name="compareValue">valore da comparare</param>
        public RequiredIfValidatorList(string errorMessage, IList<object> externalValueList, CompareTypes validIf, object compareValue = null)
            : base(errorMessage)
        {
            ExternalValueList = externalValueList;
            ValidIf = validIf;
            CompareValue = compareValue;
        }

        public override bool Validate(string value)
        {
            bool validate = false;

            if (ExternalValueList != null && ExternalValueList.Count > 0)
            {
                switch (ValidIf)
                {
                    case CompareTypes.ValueNotNull:
                        validate = ExternalValueList.Any(x => x != null) ? false : true;
                        break;
                    case CompareTypes.ValueNull:
                        validate = ExternalValueList.Any(x => x == null) ? false : true;
                        break;
                    case CompareTypes.ValueFalse:
                        validate = ExternalValueList.Any(x => bool.Parse(x.ToString()) == false) ? false : true;
                        break;
                    case CompareTypes.ValueTrue:
                        validate = ExternalValueList.Any(x => bool.Parse(x.ToString()) == true) ? false : true;
                        break;
                    case CompareTypes.ValueIn:
                        if (CompareValue != null)
                            validate = ExternalValueList.Contains(CompareValue) ? false : true;
                        else
                            validate = false;
                        break;
                    case CompareTypes.ValueExcluded:
                        if (CompareValue != null)
                            validate = !ExternalValueList.All(x => x != CompareValue);
                        else
                            validate = false;
                        break;
                    case CompareTypes.ValueAllEquals:
                        if (CompareValue != null)
                            validate = !ExternalValueList.All(x => x == CompareValue);
                        else
                            validate = false;
                        break;
                    case CompareTypes.ValueNegateExcluded:
                        if (CompareValue != null)
                            validate = ExternalValueList.All(x => x != CompareValue);
                        else
                            validate = false;
                        break;
                    case CompareTypes.ValueNegateAllEquals:
                        if (CompareValue != null)
                            validate = ExternalValueList.All(x => x == CompareValue);
                        else
                            validate = false;
                        break;
                    case CompareTypes.ValueOr:
                        if (CompareValue != null)
                          validate = ExternalValueList.Where(x => x == CompareValue).Count() == ExternalValueList.Count ? false : true;
                        else
                            validate = false;
                        break;
                    case CompareTypes.ValueAnyEquals:
                        if (CompareValue != null)
                            validate = !ExternalValueList.Any(x => x == CompareValue);
                        else
                            validate = false;
                        break;
                    case CompareTypes.ValueAnyExcluded:
                        if (CompareValue != null)
                            validate = !ExternalValueList.Any(x => x != CompareValue);
                        else
                            validate = false;
                        break;

                    case CompareTypes.ValueNegateAnyEquals:
                        if (CompareValue != null)
                            validate = ExternalValueList.Any(x => x == CompareValue);
                        else
                            validate = false;
                        break;
                    case CompareTypes.ValueNegateAnyExcluded:
                        if (CompareValue != null)
                            validate = ExternalValueList.Any(x => x != CompareValue);
                        else
                            validate = false;
                        break;
                    case CompareTypes.ValueSelfAndAllAreFalse:
                        if (CompareValue != null && (!(bool)CompareValue))
                            validate = ExternalValueList.All(x => bool.Parse(x.ToString()) == false) ? false : true;
                        else
                            validate = true;
                        break;
                    default:
                        validate = ExternalValueList.Any(x => x != null) ? true : false;
                        break;
                }
            }
            else
                validate = false;
            return validate;
        }
    }
}
