﻿
namespace NetService.Utility.ValidatedFields
{
    public class StringComparerValidator : Validator
    {
        public string CompareValue
        {
            get
            {
                return _CompareValue;
            }
            private set
            {
                _CompareValue = value;
            }
        }
        private string _CompareValue;

        public bool CheckAsMD5
        {
            get
            {
                return _CheckAsMD5;
            }
            set
            {
                _CheckAsMD5 = value;
            }
        }
        private bool _CheckAsMD5;

        public bool ValidIfEqual
        {
            get
            {
                return _ValidIfEqual;
            }
            set
            {
                _ValidIfEqual = value;
            }
        }
        private bool _ValidIfEqual = true; 

        public StringComparerValidator(string errorMessage, string compareValue)
            : base(errorMessage)
        {
            this.CompareValue = compareValue;
        }

        public override bool Validate(string value)
        {
            if(CheckAsMD5)
                value = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(value, "MD5");

            return (CompareValue == value) ? ValidIfEqual : !ValidIfEqual;
        }
    }
}
