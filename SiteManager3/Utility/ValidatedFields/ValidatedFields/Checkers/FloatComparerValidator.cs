﻿using System.Globalization;
namespace NetService.Utility.ValidatedFields
{
    public class FloatComparerValidator : Validator
    {
        public float CompareValue
        {
            get
            {
                return _CompareValue;
            }
            private set
            {
                _CompareValue = value;
            }
        }
        private float _CompareValue;

        public CompareTypes ValidIf
        {
            get
            {
                return _ValidIf;
            }
            set
            {
                _ValidIf = value;
            }
        }
        private CompareTypes _ValidIf = CompareTypes.Equal;

        public enum CompareTypes { Equal, Major, Minor, MajorEqual, MinorEqual, Different }

        public FloatComparerValidator(string errorMessage, float compareValue, CompareTypes validIf)
            : base(errorMessage)
        {
            this.CompareValue = compareValue;
            this.ValidIf = validIf;
        }

        public override bool Validate(string value)
        {
            float doubleValue;
            if (!value.Contains("."))
            {
                if (float.TryParse(value.Replace(",", "."), System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out doubleValue))
                {
                    switch (this.ValidIf)
                    {
                        case CompareTypes.Equal: return doubleValue == this.CompareValue;
                        case CompareTypes.Major: return doubleValue > this.CompareValue;
                        case CompareTypes.MajorEqual: return doubleValue >= this.CompareValue;
                        case CompareTypes.Minor: return doubleValue < this.CompareValue;
                        case CompareTypes.MinorEqual: return doubleValue <= this.CompareValue;
                        case CompareTypes.Different: return doubleValue != this.CompareValue;
                    }
                }
                else
                {
                    ErrorMessage = "";
                }
            }
            else
            {
                ErrorMessage = "Per i numeri decimali inserisci la virgola e nessun punto";
            }
            return false;
        }
    }
}
