﻿
namespace NetService.Utility.ValidatedFields
{
    public abstract class Validator
    {
        public string ErrorMessage
        {
            get
            {
                return _ErrorMessage;
            }
            protected set
            {
                _ErrorMessage = value;
            }
        }
        private string _ErrorMessage;

        public Validator(string errorMessage)
        {
            this.ErrorMessage = errorMessage;
        }

        public abstract bool Validate(string value);
    }
}
