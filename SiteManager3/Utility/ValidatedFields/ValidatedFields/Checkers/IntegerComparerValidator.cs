﻿using System.Globalization;
namespace NetService.Utility.ValidatedFields
{
    public class IntegerComparerValidator : Validator
    {
        public int CompareValue
        {
            get
            {
                return _CompareValue;
            }
            private set
            {
                _CompareValue = value;
            }
        }
        private int _CompareValue;

        public CompareTypes ValidIf
        {
            get
            {
                return _ValidIf;
            }
            set
            {
                _ValidIf = value;
            }
        }
        private CompareTypes _ValidIf = CompareTypes.Equal;

        public enum CompareTypes { Equal, Major, Minor, MajorEqual, MinorEqual, Different }

        public IntegerComparerValidator(string errorMessage, int compareValue, CompareTypes validIf)
            : base(errorMessage)
        {
            this.CompareValue = compareValue;
            this.ValidIf = validIf;
        }

        public override bool Validate(string value)
        {
            int intValue;
            
            if (int.TryParse(value.Replace(",", "."), System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out intValue))
            {
                switch (this.ValidIf)
                {
                    case CompareTypes.Equal: return intValue == this.CompareValue;
                    case CompareTypes.Major: return intValue > this.CompareValue;
                    case CompareTypes.MajorEqual: return intValue >= this.CompareValue;
                    case CompareTypes.Minor: return intValue < this.CompareValue;
                    case CompareTypes.MinorEqual: return intValue <= this.CompareValue;
                    case CompareTypes.Different: return intValue != this.CompareValue;
                }
            }
            else
            {
                ErrorMessage = "";
            }
            
            return false;
        }
    }
}