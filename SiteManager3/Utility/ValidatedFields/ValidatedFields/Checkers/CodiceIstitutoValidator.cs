﻿
namespace NetService.Utility.ValidatedFields
{
    public class CodiceIstitutoValidator : Validator
    {
        private string inizialiProvincia;
        public CodiceIstitutoValidator(string errorMessage, string inizialiProvincia)
            : base(errorMessage)
        {
            this.inizialiProvincia = inizialiProvincia;
        }

        public override bool Validate(string value)
        {
            if (value.Length != 10)
            {
                ErrorMessage="Il codice deve essere di 10 caratteri";
                return false;
            }

            if (value.Substring(0, 2).Equals(inizialiProvincia, System.StringComparison.CurrentCultureIgnoreCase))
                return true;
            else return false;
        }
    }
}
