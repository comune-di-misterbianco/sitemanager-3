﻿
namespace NetService.Utility.ValidatedFields
{
    public class CodiceFiscaleValidator : Validator
    {
        public CodiceFiscaleValidator(string errorMessage)
            : base(errorMessage)
        {
        }

        public override bool Validate(string value)
        {
            return NetService.Utility.Common.FormatValidator.ValidateCodiceFiscale(value);
        }
    }
}
