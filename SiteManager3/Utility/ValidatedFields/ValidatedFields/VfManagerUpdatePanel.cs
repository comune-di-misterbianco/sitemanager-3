﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NHibernate;

namespace NetService.Utility.ValidatedFields
{
    /// <summary>
    /// VfManagerUpdatePanel Inserisce un UpdatePanel nella generazione delle righe dei fields in modo da prevenire i postback ad esempio mandati dalla dropdownComune
    /// ATTENZIONE: l'utilizzodi un oggetto di tipo VfManagerUpdatePanel all'interno di una pagina richiede che la stessa contenga uno ScriptManager,
    /// opzione abilitabile settando a true il flag "AddScriptManager" nel costruttore della pagina.
    /// </summary>
    public class VfManagerUpdatePanel : Panel
    {
        public delegate void PostbackEventHandler(object sender, EventArgs e);
        public enum VfManagerRenderType
        {
            Frontend,
            Backoffice
        }
        public bool UsedOnFrontend = false;

        public event PostbackEventHandler AfterValidation;

        public List<string> ValidationErrors
        {
            get
            {
                if (_ValidationErrors == null)
                    _ValidationErrors = new List<string>();
                return _ValidationErrors;
            }
        }
        private List<string> _ValidationErrors;

        public List<VfGeneric> Fields
        {
            get
            {
                if (_Fields == null)
                {
                    _Fields = new List<VfGeneric>();
                }
                return _Fields;
            }
        }
        private List<VfGeneric> _Fields;

        private int rows = 0;
        /// <summary>
        /// Rappresenta il numero di righe da stampare nel form. Se il valore è maggiore di zero la routine inclusa nel metodo OnInit
        /// eseguira un ciclo sui campi field inserendoli nella row corrispondente al valore field.RowOrder.
        /// </summary>
        public int Rows
        {
            get { return rows; }
            set { rows = value; }
        }


        public Button SubmitButton
        {
            get
            {
                if (_SubmitButton == null)
                {
                    if (UsedOnFrontend)
                        _SubmitButton = new Button { ID = SubmitButtonID, Text = "Submit", CssClass = "btn btn-success button" };
                    else
                    _SubmitButton = new Button { ID = SubmitButtonID, Text = "Submit", CssClass = "button" };
                }
                return _SubmitButton;
            }
        }
        private Button _SubmitButton;

        public string SubmitButtonID { get; set; }

        public Button BackButton
        {
            get
            {
                if (_BackButton == null)
                {
                    if (UsedOnFrontend)
                        _BackButton = new Button { ID = BackButtonID, Text = "Back", CssClass = "btn btn-success button" };
                    else
                    _BackButton = new Button { ID = BackButtonID, Text = "Back", CssClass = "button" };
                }
                return _BackButton;
            }
        }
        private Button _BackButton;

        public string BackButtonID { get; set; }

        public bool IsPostBack
        {
            get
            {
                if (_IsPostBack == false)
                {
                    NetService.Utility.Common.RequestVariable buttonPostback = new NetService.Utility.Common.RequestVariable(SubmitButtonID);
                    _IsPostBack = (buttonPostback.IsPostBack && buttonPostback.StringValue == SubmitButton.Text);
                }
                return _IsPostBack;
            }
            set
            {
                _IsPostBack = value;
            }
        }
        private bool _IsPostBack;

        public List<Validator> Validators
        {
            get
            {
                if (_Validators == null)
                {
                    _Validators = new List<Validator>();
                }
                return _Validators;
            }
        }
        private List<Validator> _Validators;

        public bool AutoValidation { get; set; }

        public VfGeneric.ValidationStates IsValid { get; private set; }

        public bool ShowMandatoryInfo
        {
            get
            {
                return _ShowMandatoryInfo;
            }
            //era private
            set
            {
                _ShowMandatoryInfo = value;
            }
        }
        private bool _ShowMandatoryInfo = true;

        public WebControl MandatoryControl
        {
            get
            {
                if (_MandatoryControl == null)
                {
                    _MandatoryControl = new WebControl(HtmlTextWriterTag.Div);
                    _MandatoryControl.CssClass = "mandatory-vfm";
                    _MandatoryControl.Controls.Add(new LiteralControl(NetService.Localization.LabelsManager.GetLabel("infoMandatory")));
                }
                return _MandatoryControl;
            }
        }
        private WebControl _MandatoryControl;

        public WebControl InfoControl
        {
            get
            {
                if (_InfoControl == null)
                {
                    _InfoControl = new WebControl(HtmlTextWriterTag.Div);
                    _InfoControl.CssClass = "info-vfm";
                }
                return _InfoControl;
            }
        }
        private WebControl _InfoControl;

        public WebControl TitleControl
        {
            get
            {
                if (_TitleControl == null)
                {
                    _TitleControl = new WebControl(HtmlTextWriterTag.Div);
                    _TitleControl.CssClass = "title-vfm";
                }
                return _TitleControl;
            }
        }
        private WebControl _TitleControl;

        #region Preloader
        /// <summary>
        /// Testo del preloader, qualora si voglia personalizzarlo.
        /// </summary>
        public string PreLoaderText
        {
            get;
            set;
        }

        public WebControl PreLoadControl
        {
            get
            {
                if (_PreLoadControl == null)
                {
                    _PreLoadControl = new WebControl(HtmlTextWriterTag.Div);
                    _PreLoadControl.ID = "preLoader";
                    _PreLoadControl.CssClass = "vf-preloader";
                    _PreLoadControl.Attributes.Add("style", "display:none;");

                    WebControl img = new WebControl(HtmlTextWriterTag.Img);
                    img.Attributes.Add("src", NetCms.Configurations.Paths.AbsoluteRoot + "/cms/css/admin/images/preload.gif");

                    _PreLoadControl.Controls.Add(img);

                    WebControl h1 = new WebControl(HtmlTextWriterTag.H1);
                    string PreloadMessage = "Caricamento in corso ...";
                    if (!string.IsNullOrEmpty(PreLoaderText))
                        PreloadMessage = PreLoaderText;
                    h1.Controls.Add(new LiteralControl(PreloadMessage));

                    _PreLoadControl.Controls.Add(h1);
                }

                return _PreLoadControl;
            }
        }
        private WebControl _PreLoadControl;

        private LiteralControl PreLoadScript
        {
            get
            {
                string script = @"<script type='text/javascript'>

                    $(document).ready(function() {
                        $('#" + this.SubmitButton.ID + @"').click(function () {
                            $.blockUI({ message: $('#preLoader') });
                        });
                    }); 
                </script>";

                return new LiteralControl(script);
            }
        }

        /// <summary>
        /// Attiva per il VfManager la modalità preload, in cui viene effettuato il lock della UI durante il submit,
        /// utile nel caso di Form con tempi di caricamento lunghi, specie form di importazione dati.
        /// </summary>
        public bool AddPreLoader { get; set; }

        #endregion

        /// <summary>
        /// Labels per i fields, messe in cache dal LabelsManager
        /// </summary>
        public LabelsManager.Labels FormLabels
        {
            get;
            set;
        }

        /// <summary>
        /// Abilita il il rendering di tutti i pulsanti dentro un unico div, se disabilitata verrà renderizzato un div contenitore per ogni pulsante
        /// </summary>
        public bool EnableInLineButtonsStyle { get; set; }

        private bool AllFieldsAreEmpty { get; set; }

        public bool AddSubmitButton { get; set; }

        public bool AddBackButton { get; set; }

        /// <summary>
        /// VfManagerUpdatePanel Constructor with Postback
        /// </summary>
        /// <param name="controlID">Control ID</param>
        /// <param name="isPostBack">Boolean value for postback of VfManager</param>
        /// <param name="managerRenderType"> Render Type for VfManager: Backoffice or Frontend </param>
        public VfManagerUpdatePanel(string controlID, bool isPostBack, VfManagerRenderType managerRenderType = VfManagerRenderType.Backoffice)
            : this(controlID)
        {
            this.IsPostBack = isPostBack;
            UsedOnFrontend = managerRenderType == VfManagerRenderType.Frontend ? true : false;
        }

        /// <summary>
        /// VfMangerUpdatePanel Default Constructor 
        /// </summary>
        /// <param name="controlID">Control ID</param>
        /// <param name="managerRenderType">Render Type for VfManager: Backoffice or Frontend</param>
        public VfManagerUpdatePanel(string controlID, VfManagerRenderType managerRenderType = VfManagerRenderType.Backoffice)
        {
            this.ID = controlID;
            SubmitButtonID = controlID + "_submit";
            BackButtonID = controlID + "_back";
            this.AutoValidation = true;
            this.AddSubmitButton = true;
            this.AddBackButton = false;
            this.IsValid = VfGeneric.ValidationStates.NotYetExecuted;
            UsedOnFrontend = managerRenderType == VfManagerRenderType.Frontend ? true : false;
        }

        /// <summary>
        /// Deprecato - Usare DataBindAdvanced - Metodo per l'auto popolamento delle proprietà dell'oggetto associato alla form 
        /// </summary>
        /// <param name="relatedObject"></param>
        /// <returns></returns>
        public bool DataBind(object relatedObject)
        {
            VfObjectFillerUpdatePanel filler = new VfObjectFillerUpdatePanel(this, relatedObject);


            return filler.FillFields();
        }

        /// <summary>
        /// Metodo per l'auto popolamento delle proprietà dell'oggetto associato alla form 
        /// </summary>
        /// <param name="relatedObject"></param>
        /// <returns></returns>
        public bool DataBindAdvanced(object relatedObject)
        {
            VfObjectFillerUpdatePanel filler = new VfObjectFillerUpdatePanel(this, relatedObject);

            return filler.FillFieldsAdvanced();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (UsedOnFrontend)
            {

                if (AutoValidation && IsPostBack)
                    Validate();

                if (IsPostBack && AfterValidation != null)
                    AfterValidation(this, new EventArgs());

                WebControl divContainer = new WebControl(HtmlTextWriterTag.Div);


                WebControl rowVFManager = new WebControl(HtmlTextWriterTag.Div);
                rowVFManager.CssClass = "row";

                WebControl colmd12VFM = new WebControl(HtmlTextWriterTag.Div);
                colmd12VFM.CssClass = "col-md-12";

                if (TitleControl.Controls.Count > 0)
                {
                    TitleControl.CssClass = "page-header " + TitleControl.CssClass;
                    colmd12VFM.Controls.Add(TitleControl);
                }

                if (InfoControl.Controls.Count > 0)
                {
                    InfoControl.CssClass = "form-group " + InfoControl.CssClass;
                    colmd12VFM.Controls.Add(InfoControl);
                }

                if (this.ShowMandatoryInfo)
                {
                    MandatoryControl.CssClass = "alert alert-warning " + MandatoryControl.CssClass;
                    colmd12VFM.Controls.Add(MandatoryControl);
                }

                   
                if (Rows > 0)
                {
                    var orderedFieldList = this.Fields.OrderBy(x => x.RowOrder).ToList();

                    foreach (var field in orderedFieldList)
                        colmd12VFM.Controls.Add(field);

                    ////inserimento di un div che fa da container dei field della form che si sta costruendo
                    //WebControl divContainerRow = new WebControl(HtmlTextWriterTag.Div);

                    //// creo le row della form
                    //for (int i = 1; i <= Rows; i++)
                    //{
                    //    WebControl row = new WebControl(HtmlTextWriterTag.Div);
                    //    row.CssClass = "row";
                    //    row.ID = "row_" + i;

                    //    //this.Controls.Add(row);

                    //    //aggiunta delle righe field al container
                    //    divContainerRow.Controls.Add(row);
                    //}

                    //// aggiungo i field nelle righe corrispondenti
                    //foreach (var field in this.Fields)
                    //{
                    //    if (field.RowOrder > 0)
                    //    {
                    //        this.FindControl("row_" + field.RowOrder).Controls.Add(field);
                    //    }
                    //}
                        
                }
                else
                {                                      
                    foreach (var field in this.Fields)
                        colmd12VFM.Controls.Add(field);
                }
                                                                  

                if (this.IsValid == VfGeneric.ValidationStates.NotValid)
                {
                    rowVFManager.Controls.Add(ValidationResultsControl());
                }


                rowVFManager.Controls.Add(colmd12VFM);
                //this.Controls.Add(rowVFManager);
                divContainer.Controls.Add(rowVFManager);


                WebControl rowButton = new WebControl(HtmlTextWriterTag.Div);
                rowButton.CssClass = "row";
                   
                if (AddSubmitButton && AddBackButton)
                {

                    WebControl colBack = new WebControl(HtmlTextWriterTag.Div);
                    colBack.CssClass = "col-md-6";

                    WebControl buttoBackContainer = new WebControl(HtmlTextWriterTag.Div);
                    buttoBackContainer.CssClass = "form-group text-center vf back-vf";

                    buttoBackContainer.Controls.Add(BackButton);
                    colBack.Controls.Add(buttoBackContainer);
                    rowButton.Controls.Add(colBack);

                    WebControl colSubmit = new WebControl(HtmlTextWriterTag.Div);
                    colSubmit.CssClass = "col-md-6";

                    WebControl buttonSubmitContainer = new WebControl(HtmlTextWriterTag.Div);
                    buttonSubmitContainer.CssClass = "form-group text-center vf submit-vf";

                    buttonSubmitContainer.Controls.Add(SubmitButton);
                    colSubmit.Controls.Add(buttonSubmitContainer);
                    this.DefaultButton = SubmitButton.ID;

                    rowButton.Controls.Add(colSubmit);
                    divContainer.Controls.Add(rowButton);
                }
                else
                {
                    WebControl col = new WebControl(HtmlTextWriterTag.Div);
                    col.CssClass = "col-md-12";

                    WebControl buttonContainer = new WebControl(HtmlTextWriterTag.Div);

                    if (AddSubmitButton)
                    {
                        buttonContainer.CssClass = "form-group text-center vf submit-vf";
                        buttonContainer.Controls.Add(SubmitButton);
                        this.DefaultButton = SubmitButton.ID;
                    }

                    if (AddBackButton)
                    {
                        buttonContainer.CssClass = "form-group text-center vf back-vf";
                        buttonContainer.Controls.Add(BackButton);
                    }

                    col.Controls.Add(buttonContainer);

                    rowButton.Controls.Add(col);
                    divContainer.Controls.Add(rowButton);
                }


                //if (!EnableInLineButtonsStyle)
                //{
                //    //Un div per ogni bottone
                //    if (AddSubmitButton)
                //    {
                //        WebControl buttonContainer = new WebControl(HtmlTextWriterTag.Div);
                //        buttonContainer.CssClass = "vf submit-vf";
                //        buttonContainer.Controls.Add(SubmitButton);
                //        this.Controls.Add(buttonContainer);
                //        this.DefaultButton = SubmitButton.ID;
                //    }

                //    if (AddBackButton)
                //    {
                //        WebControl buttonContainer = new WebControl(HtmlTextWriterTag.Div);
                //        buttonContainer.CssClass = "vf back-vf";
                //        buttonContainer.Controls.Add(BackButton);
                //        this.Controls.Add(buttonContainer);
                //    }
                //}
                //else
                //{
                //    //Singolo div con più pulsanti
                //    WebControl buttonsContainer = new WebControl(HtmlTextWriterTag.Div);
                //    buttonsContainer.CssClass = "vf buttons-container";

                //    if (AddSubmitButton)
                //        buttonsContainer.Controls.Add(SubmitButton);
                //    if (AddBackButton)
                //        buttonsContainer.Controls.Add(BackButton);

                //    this.Controls.Add(buttonsContainer);
                //}

                if (AddPreLoader && AddSubmitButton)
                {
                    Page.Header.Controls.Add(new LiteralControl("<script type=\"text/javascript\" src=\"" + NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/shared/jquery-blockUI/jquery.blockUI.js\"></script>"));
                    Page.Header.Controls.Add(PreLoadScript);
                    divContainer.Controls.Add(PreLoadControl);
                }
                                  
                UpdatePanel panel = new UpdatePanel();
                panel.RenderMode = UpdatePanelRenderMode.Block;
                panel.ContentTemplateContainer.Controls.Add(divContainer);
                panel.ChildrenAsTriggers = true;
                panel.UpdateMode = UpdatePanelUpdateMode.Always;
                //aggiunta dell'updatepanel al control della pagina
                this.Controls.Add(panel);
                 
            }
            else
            {
            if (AutoValidation && IsPostBack)
                Validate();

            if (IsPostBack && AfterValidation != null)
                AfterValidation(this, new EventArgs());

            if (TitleControl.Controls.Count > 0)
                this.Controls.Add(TitleControl);

            if (InfoControl.Controls.Count > 0)
                this.Controls.Add(InfoControl);

            if (Rows > 0)
            {
                //inserimento di un div che fa da container dei field della form che si sta costruendo
                WebControl divContainerRow = new WebControl(HtmlTextWriterTag.Div);

                // creo le row della form
                for (int i = 1; i <= Rows; i++)
                {
                    WebControl row = new WebControl(HtmlTextWriterTag.Div);
                    row.CssClass = "row";
                    row.ID = "row_" + i;

                    //this.Controls.Add(row);

                    //aggiunta delle righe field al container
                    divContainerRow.Controls.Add(row);
                }

                // aggiungo i field nelle righe corrispondenti
                foreach (var field in this.Fields)
                {
                    if (field.RowOrder > 0)
                    {
                        this.FindControl("row_" + field.RowOrder).Controls.Add(field);
                    }
                }

                //Creazione dell'UpdatePanel che contiene come ContentTemplateContainer il div contenitore delle righe
                UpdatePanel panel = new UpdatePanel();
                panel.RenderMode = UpdatePanelRenderMode.Block;
                panel.ContentTemplateContainer.Controls.Add(divContainerRow);
                panel.ChildrenAsTriggers = true;
                panel.UpdateMode = UpdatePanelUpdateMode.Always;
                //aggiunta dell'updatepanel al control della pagina
                this.Controls.Add(panel);

            }
            else
            {
                //inserimento di un div che fa da container dei field della form che si sta costruendo
                WebControl divContainerRow = new WebControl(HtmlTextWriterTag.Div);

                foreach (var field in this.Fields)
                {
                  
                    //this.Controls.Add(field);
                    //aggiunta delle righe field al container
                    divContainerRow.Controls.Add(field);

                }

                //Creazione dell'UpdatePanel che contiene come ContentTemplateContainer il div contenitore delle righe
                UpdatePanel panel = new UpdatePanel();
                panel.RenderMode = UpdatePanelRenderMode.Block;
                panel.ContentTemplateContainer.Controls.Add(divContainerRow);
                panel.ChildrenAsTriggers = true;
                panel.UpdateMode = UpdatePanelUpdateMode.Always;
                //aggiunta dell'updatepanel al control della pagina
                this.Controls.Add(panel);
            }



            if (this.ShowMandatoryInfo)
                this.Controls.Add(MandatoryControl);

            if (this.IsValid == VfGeneric.ValidationStates.NotValid)
            {
                this.Controls.Add(ValidationResultsControl());
            }

            if (!EnableInLineButtonsStyle)
            {
                //Un div per ogni bottone
                if (AddSubmitButton)
                {
                    WebControl buttonContainer = new WebControl(HtmlTextWriterTag.Div);
                    buttonContainer.CssClass = "vf submit-vf";
                    buttonContainer.Controls.Add(SubmitButton);
                    this.Controls.Add(buttonContainer);
                    this.DefaultButton = SubmitButton.ID;
                }

                if (AddBackButton)
                {
                    WebControl buttonContainer = new WebControl(HtmlTextWriterTag.Div);
                    buttonContainer.CssClass = "vf back-vf";
                    buttonContainer.Controls.Add(BackButton);
                    this.Controls.Add(buttonContainer);
                }
            }
            else
            {
                //Singolo div con più pulsanti
                WebControl buttonsContainer = new WebControl(HtmlTextWriterTag.Div);
                buttonsContainer.CssClass = "vf buttons-container";

                if (AddSubmitButton)
                    buttonsContainer.Controls.Add(SubmitButton);
                if (AddBackButton)
                    buttonsContainer.Controls.Add(BackButton);

                this.Controls.Add(buttonsContainer);
            }

            if (AddPreLoader && AddSubmitButton)
            {
                Page.Header.Controls.Add(new LiteralControl("<script type=\"text/javascript\" src=\"" + NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/shared/jquery-blockUI/jquery.blockUI.js\"></script>"));
                Page.Header.Controls.Add(PreLoadScript);
                this.Controls.Add(PreLoadControl);
            }
        }
        }

        public void Validate()
        {
            this.IsValid = VfGeneric.ValidationStates.Valid;
            AllFieldsAreEmpty = true;
            foreach (VfGeneric field in this.Fields.Where(x => x.Enabled))
            {
                field.Validate();
                if (field.ValidationState == VfGeneric.ValidationStates.NotValid)
                    this.IsValid = VfGeneric.ValidationStates.NotValid;

                if (!string.IsNullOrEmpty(field.PostBackValue))
                    AllFieldsAreEmpty = false;
            }
        }
        private void ValidateForm()
        {
            foreach (Validator validator in this.Validators)
            {
                if (validator != null && !validator.Validate(null) && validator.ErrorMessage != "")
                    this.AddError(validator.ErrorMessage);
            }
        }

        /// <summary>
        /// Deprecato - Usare FillObjectAdvanced
        /// </summary>
        /// <param name="objInstance"></param>
        /// <returns></returns>
        public bool FillObject(object objInstance)
        {
            VfObjectFillerUpdatePanel filler = new VfObjectFillerUpdatePanel(this, objInstance);
            return filler.FillObject();
        }

        public bool FillObjectAdvanced(object objInstance, ISession session)
        {
            VfObjectFillerUpdatePanel filler = new VfObjectFillerUpdatePanel(this, objInstance);
            return filler.FillObjectAdvanced(session);
        }

        protected void AddError(string s)
        {
            ValidationErrors.Add(s);
        }

        protected WebControl ValidationResultsControl()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            WebControl ul = new WebControl(HtmlTextWriterTag.Ul);

            if (UsedOnFrontend)
            {
                div.CssClass = "form-validation-results";
                ul.CssClass = "list-unstyled";

                foreach (var validation in this.ValidationErrors)
                {
                    ul.Controls.Add(new LiteralControl("<li><div class=\"alert alert-danger\" role=\"alert\">" + validation + "</div></li>"));
                }
                div.Controls.Add(ul);
                return div;
            }
            else
            {
            div.CssClass = "form-validation-results";

            foreach (var validation in this.ValidationErrors)
            {
                ul.Controls.Add(new LiteralControl("<li><span>" + validation + "</span></li>"));
            }
            div.Controls.Add(ul);
            return div;
        }
        }

        public WebControl GetDetailsSheet(object objInstance, string title, string description)
        {

            VfObjectFillerUpdatePanel filler = new VfObjectFillerUpdatePanel(this, objInstance);

            NetService.Utility.UI.DetailsSheet sheet = new NetService.Utility.UI.DetailsSheet(title, description)
            {
                SeparateColumns = NetService.Utility.UI.DetailsSheet.SeparateColumnsOptions.All
            };

            foreach (var field in this.Fields.Where(x => x.ShowsInDetailsSheets))
            {
                try
                {
                    string label = field.Label;
                    var value = filler.GetPropertyValue(field.DetailsPropertyName);

                    string valueToPrint = value is DateTime ? ((DateTime)value).ToLocalTime().ToShortDateString() : value.ToString();

                    if (value is DateTime)
                        valueToPrint = ((DateTime)value).ToLocalTime().ToShortDateString();
                    else if (value is float || value is double)
                        valueToPrint = String.Format("{0:0,0.00}", value);
                    else
                        valueToPrint = value.ToString();

                    sheet.AddTitleTextRow(label, valueToPrint);
                }
                catch (Exception)
                { }
            }
            WebControl control = sheet;


            return control;
        }
    }
}
