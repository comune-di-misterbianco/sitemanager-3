﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using NetService.Utility.RecordsFinder;
using System.Web.UI;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

namespace NetService.Utility.ValidatedFields
{
    public class VfReCaptcha : VfGeneric
    {

        public VfReCaptcha(string id) : base(id, "")
        {
            BindField = false;
            Required = false;
        }

        private ReCaptcha.ReCaptchaV2 reCaptchaV2
        {
            get
            {
                return new ReCaptcha.ReCaptchaV2(new ReCaptcha.ReCaptchaV2Data());
            }
        }


        private WebControl _Field;
        public override WebControl Field
        {
            get
            {
                if (_Field == null)
                {
                    _Field = new WebControl(HtmlTextWriterTag.Span);
                    _Field.Controls.Add(this.HiddenField);
                }
                return _Field;
            }
        }

        public System.Web.UI.WebControls.HiddenField HiddenField
        {
            get
            {
                if (_HiddenField == null)
                {
                    _HiddenField = new HiddenField();
                    _HiddenField.ID = this.ID;

                    if (this.Request.IsValidString)
                    {
                        _HiddenField.Value = this.Request.StringValue.Replace("''", "'");
                    }
                }
                return _HiddenField;
            }
        }
        private HiddenField _HiddenField;


        public override string LocalCssClass
        {
            get
            {
                return "captcha-vf";
            }
        }

        public override object PostbackValueObject
        {
            get { throw new NotImplementedException(); }            
        }
        public string CurrentValue { get; private set; }

        public override Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set { _SearchComparisonCriteria = value; }
        }
        private RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = RecordsFinder.Finder.ComparisonCriteria.Equals;

        protected override void FillFieldValue()
        {

        }

        protected override void LocalValidate()
        {
            // provare a invocare la web api di google per validare il componente
            ReCaptcha.ReCaptchaV2Result results = this.reCaptchaV2.Verify();
            if (!results.Success)
            {
                this.AddError("Dimostra di non essere un robot");
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            //var div = this;

            //WebControl colmsg = new WebControl(HtmlTextWriterTag.Div);
            //colmsg.CssClass = "col-md-12";

            //string callback = @"
            //                <script type=""text/javascript"">  
            //                var onloadCallback = function() {  
            //                alert(""reCaptcha is ready !!!"");  
            //                };  
            //                </script>";

            //WebControl msg = new WebControl(HtmlTextWriterTag.P);
            //msg.Controls.Add(new LiteralControl("<script src=\"https://www.google.com/recaptcha/api.js\" async defer></script>"));
            //msg.Controls.Add(new LiteralControl(callback));
            ////msg.Controls.Add(new LiteralControl("Digita i caratteri visualizzati nell'immagine.")); //LABELDAUSCIRE

            //colmsg.Controls.Add(msg);
            //div.Controls.Add(colmsg);

            //WebControl divImageCap = new WebControl(HtmlTextWriterTag.Div);
            //divImageCap.Attributes["class"] = "g-recaptcha";
            //divImageCap.Attributes["data-sitekey"] = googleKey;


            //div.Controls.Add(divImageCap);

            this.Controls.Add(reCaptchaV2.GetControl);
        }
    }
}
