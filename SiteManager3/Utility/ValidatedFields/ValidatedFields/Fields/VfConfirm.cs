﻿using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetService.Utility.ValidatedFields
{
    public class VfConfirm: VfGeneric
    {
        public override WebControl Field
        {
            get { return Password; }
        }

        public override object PostbackValueObject
        {
            get
            {
                return this.Request.DatabaseStringValue; 
            }
        }
        public TextBox Password
        {
            get
            {
                if (_Password == null)
                {
                    _Password = new TextBox();
                    _Password.ID = this.ID;
                    _Password.TextMode = TextBoxMode.SingleLine;
                }
                return _Password;
            }
        }
        private TextBox _Password;

        public TextBox Confirm
        {
            get
            {
                if (_Confirm == null)
                {
                    _Confirm = new TextBox();
                    _Confirm.ID = this.ID + "_confirm";
                    _Confirm.TextMode = TextBoxMode.SingleLine;
                }
                return _Confirm;
            }
        }
        private TextBox _Confirm;

        public string ConfirmLabel
        {
            get
            {
                return _ConfirmLabel;
            }
            private set
            {
                _ConfirmLabel = NetService.Localization.LabelsManager.GetLabel(value);
            }
        }
        private string _ConfirmLabel; 

        public NetService.Utility.Common.RequestVariable ConfirmRequest
        {
            get
            {
                if (_ConfirmRequest == null)
                {
                    _ConfirmRequest = new NetService.Utility.Common.RequestVariable(Confirm.ID, NetService.Utility.Common.RequestVariable.RequestType.Form);
                }
                return _ConfirmRequest;
            }
        }
        private NetService.Utility.Common.RequestVariable _ConfirmRequest;

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return RecordsFinder.Finder.ComparisonCriteria.Equals; }
        }

        public InputTypes InputType
        {
            get
            {
                return _InputType;
            }
            set
            {
                _InputType = value;
            }
        }
        private InputTypes _InputType;

        public int MaxLenght
        {
            get
            {
                return _MaxLenght;
            }
            private set
            {
                _MaxLenght = value;
            }
        }
        private int _MaxLenght = 255;

        public int MinLenght
        {
            get
            {
                return _MinLenght;
            }
            private set
            {
                _MinLenght = value;
            }
        }
        private int _MinLenght = 5;

        public override string LocalCssClass
        {
            get
            {
                return "password-vf";
            }
        }

        protected override void FillFieldValue()
        {
            
        }

        public VfConfirm(string id,string label, string confirmlabel, InputTypes type)
            : base(id, label)
        {
            this.Required = true;
            this.ConfirmLabel = confirmlabel;
            this.InputType = type;
        }

        protected override void AttachFieldControls()
        {
            Label label = new Label();
            label.AssociatedControlID = this.ID;
            label.Text = Label + (this.Required ? "*" : ""); ;
            this.Controls.Add(label);
            this.Controls.Add(Field);

            label = new Label();
            label.AssociatedControlID = this.Confirm.ID;
            label.Text = ConfirmLabel + (this.Required ? "*" : ""); ;
            this.Controls.Add(label);
            this.Controls.Add(Confirm);
        }
        protected override void LocalValidate()
        {
            if (this.Request.IsValidString)
            {
                string value = this.Request.StringValue;

                if (this.InputType == InputTypes.Email && !NetService.Utility.Common.FormatValidator.ValidateEmail(value))
                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidEmail"), Label));

                int temp = 0;
                if (this.InputType == InputTypes.Number && !int.TryParse(value, out temp))
                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidNumber"), Label));

            }

            if (!this.Request.IsValidString && this.Required)
                this.AddError(string.Format(Localization.LabelsManager.GetLabel("isrequired"), Label));

            if (!this.ConfirmRequest.IsValidString && this.Required)
                this.AddError(string.Format(Localization.LabelsManager.GetLabel("isrequired"), ConfirmLabel));

            if (this.Request.IsValidString && this.ConfirmRequest.IsValidString)
            {
                string value = this.Request.StringValue;
                string cvalue = this.ConfirmRequest.StringValue;

                if (value.Length < this.MinLenght && MinLenght > 0)
                    this.AddError(string.Format(Localization.LabelsManager.GetLabel("isshorterthancharacters"), Label, MinLenght));

                if (cvalue.Length < this.MinLenght && MinLenght > 0)
                    this.AddError(string.Format(Localization.LabelsManager.GetLabel("isshorterthancharacters"), ConfirmLabel, MinLenght));

                if (cvalue != value)
                    this.AddError(string.Format(Localization.LabelsManager.GetLabel("passwordnotmatchconfirm"), Label, ConfirmLabel));
            }
        }
    }
}
