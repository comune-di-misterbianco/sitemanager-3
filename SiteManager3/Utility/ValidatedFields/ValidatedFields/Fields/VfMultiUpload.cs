﻿using NetUpload.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetService.Utility.RecordsFinder;
using System.Web;
using System.IO;
using static NetService.Utility.ValidatedFields.Uploader;

namespace NetService.Utility.ValidatedFields
{
    public class VfMultiUpload : VfGeneric
    {
        private bool _AllowDelete = true;

        public bool AllowDelete
        {
            get { return _AllowDelete; }
            set { _AllowDelete = value; }
        }


        public override WebControl Field
        {
            get
            {
                return MainControl;
            }
        }
        private WebControl _MainControl;
        public WebControl MainControl
        {

            get
            {
                if (_MainControl == null)
                {

                    _MainControl = new WebControl(HtmlTextWriterTag.Div);

                    _MainControl.CssClass = "maincontroluploadify";

                    _MainControl.Controls.Add(Uploadify);
                    if (this.FileList != null && DisplayFileList)
                    {
                        WebControl fileContainer = new WebControl(HtmlTextWriterTag.Div);
                        fileContainer.ID = "FileContentInDirectory";
                        WebControl fileL = new WebControl(HtmlTextWriterTag.Legend);
                        fileL.Controls.Add(new LiteralControl("Allegati"));
                        fileContainer.Controls.Add(fileL);
                        WebControl list = new WebControl(HtmlTextWriterTag.Ul);
                        list.ID = "filelist_ID";
                        list.CssClass = Uploadify.ID + "filelist";
                        foreach (IDictionary sf in FileList)
                        {
                            string[] fileArray = sf["FullPath"].ToString().Split('\\');
                            int i = fileArray.Length;
                            string filename = fileArray.GetValue(i - 1).ToString();
                            WebControl li = new WebControl(HtmlTextWriterTag.Li);

                            WebControl link = new WebControl(HtmlTextWriterTag.A);
                            link.Attributes.Add("onclick", "javascript:deleteFile('" + filename + "')");
                            link.Attributes.Add("style", "cursor:pointer");
                            WebControl img_x = new WebControl(HtmlTextWriterTag.Img);
                            img_x.Attributes.Add("src", "/cms/css/admin/images/icons/delete-icon.png");
                            link.Controls.Add(img_x);
                            li.Controls.Add(link);

                            li.Controls.Add(new LiteralControl(filename));

                            //           string script = @"<script>function deleteFile(filename){ 
                            //                          var params = {
                            //               FileNameTOBeDeleted: '/"   + this.UploadFolder + @"/' + filename
                            //               };

                            //$.ajax({
                            //               url: '/charts/deleteFilePlupload.ashx',
                            //           type: 'POST',
                            //           data: JSON.stringify(params),
                            //           contentType: 'application/json',
                            //           dataType: 'json',
                            //           async: false,
                            //           success: function(response){
                            //                   results = response;
                            //               }
                            //           });


                            //            }</script>";
                            if (AllowDelete)
                            {

                                string modal = @"<script>$(function() {
                                   $('#" + this.CtrlID + @"dialogDelete').dialog({
                                        autoOpen:false,
                                        hide: {
                                             effect: 'explode',
                                             duration: 500
                                        },
                                        closeOnEscape: false,
                                        height : 300,
                                        width : 'auto',
                                        resizable: false,
                                        draggable: false,
                                        modal: true,
                                        title: 'Eliminazione Allegato',
                                        buttons: {
                                                  Conferma: function () {
                                                    
                                                          
                                                      var ok= new Boolean(true);
                                                      var rege = /^[_àèéìòù0-9a-zA-Z\'\,\.\- ]*$/;
                                                      

                                                      if(ok)
                                                      {
														 $('#" + this.CtrlID + @"deleteButton').click();
													
                                                      }
                                                   

                                                },
                                                Annulla: function (){
                                                  $('#div_errore_nv').hide();  
                                                  $('#div_errore_regex_rg').hide();  
                                                  $(this).dialog('close');
                                                }
                                        }
                                    });
                                   
									
			                    });</script>";
                                // string script3 = "/scripts/shared/plupload/Prova/modal.js";
                                string script2 = @"<script>function deleteFile(filename){  
                                        $('#" + this.CtrlID + @"filename').val(filename);
                                        $('#" + this.CtrlID + @"dialogDelete').dialog('open');
                                        $('#" + this.CtrlID + @"dialogDelete').parent().appendTo($('form:first'));
                                        

                                        
                                  } </script>";
                                //string headJsImport2 = @"<script type=""text/javascript"" src=""" + script3 + "\"></script>";

                                base.Page.Header.Controls.Add(new LiteralControl(modal));
                                base.Page.Header.Controls.Add(new LiteralControl(script2));
                                //base.Page.Header.Controls.Add(new LiteralControl(script));
                                //WebControl link = new WebControl(HtmlTextWriterTag.A);
                                //link.CssClass = "deleteFile";
                                //////link.Controls.Add(new LiteralControl("&action=allegatoDelete&file=" + filename));

                                ////link.Attributes.Add("href", "javascript:deleteFile('" + filename + "')");
                                //link.Attributes.Add("onclick", "javascript:deleteFile('" + filename + "')");
                                //link.Attributes.Add("style", "cursor:pointer");
                                ////link.Controls.Add(new LiteralControl("Elimina"));

                                //WebControl img_x = new WebControl(HtmlTextWriterTag.Img);
                                //img_x.Attributes.Add("src", "/cms/css/admin/images/icons/delete-icon.png");
                                //link.Controls.Add(img_x);

                                //li.Controls.Add(link);
                            }
                            //li.Controls.Add(delete);
                            list.Controls.Add(li);



                        }
                        fileContainer.Controls.Add(list);

                        // _MainControl.Controls.Add(divModale);
                        _MainControl.Controls.Add(fileContainer);

                    }

                }
                return _MainControl;
            }

        }
        private string _CtrlID;
        public string CtrlID
        {
            get { return _CtrlID; }
            set { _CtrlID = value; }

        }
        private bool FilterFiles
        {
            get;
            set;
        }
        private List<string> FilenamesFilerFiles
        {
            get;
            set;
        }


        private List<IDictionary> _FileList;
        /// <summary>
        /// Files presenti nella cartella "UploadFolder"
        /// </summary>
        public List<IDictionary> FileList
        {
            get
            {
                if (_FileList == null || _FileList.Count == 0)
                {
                    if (System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(this.UploadFolder)))
                    {
                        _FileList = new List<IDictionary>();

                        string[] templist = Directory.GetFiles(HttpContext.Current.Server.MapPath(this.UploadFolder));

                        foreach (string s in templist)
                        {
                            var item = new Hashtable();
                            string[] fileArray = s.Split('\\');
                            int i = fileArray.Length;
                            string filename = fileArray.GetValue(i - 1).ToString();
                            item.Add("FileName", filename);
                            item.Add("FullPath", s);
                            if (!FilterFiles)                               
                                _FileList.Add(item);
                            else
                            {
                                if( FilenamesFilerFiles.Contains (filename) )
                                    _FileList.Add(item);
                            }
                        }
                    }
                    else
                        _FileList = null;

                }
                return _FileList;
            }
            set
            {
                List<IDictionary> _FileList_AllegatiSelection = value;
                if (System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(this.UploadFolder)))
                {
                    _FileList = new List<IDictionary>();
                    string[] templist = Directory.GetFiles(HttpContext.Current.Server.MapPath(this.UploadFolder));
                   
                    foreach (string s in templist)
                    {
                        var item = new Hashtable();
                        string[] fileArray = s.Split('\\');
                        int i = fileArray.Length;
                        string filename = fileArray.GetValue(i - 1).ToString();                       

                        //if (_FileList_AllegatiSelection.Select(x=>x.Keys == "FileName"))

                        foreach (IDictionary values in _FileList_AllegatiSelection)
                        {
                            if (values["FileName"].ToString() == filename)
                            {
                                item = new Hashtable();
                                item.Add("FileName", filename);
                                item.Add("FullPath", s);
                                _FileList.Add(item);
                            }
                        }
                    }
                }
                else
                    _FileList = null;
            }

        }


      
        private List<IDictionary> _FileTempList;
        /// <summary>
        /// File presenti nella cartella "UploadTempFolder"
        /// </summary>
        public List<IDictionary> FileTempList
        {

            get
            {
                if (_FileTempList == null || _FileTempList.Count == 0)
                {
                    //if (System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(this.UploadTempFolder)))
                    //{
                    //    _FileTempList = new List<IDictionary>();

                    //    string [] templist = Directory.GetFiles(HttpContext.Current.Server.MapPath( this.UploadTempFolder));
                    //    foreach(string s in templist)
                    //    {
                    //        var item = new Hashtable();
                    //        string[] fileArray = s.Split('\\');
                    //        int i = fileArray.Length;
                    //        string filename = fileArray.GetValue(i - 1).ToString();
                    //        item.Add("FileName", filename);
                    //        _FileTempList.Add(item);
                    //    }
                    //}
                    //else
                    //    _FileTempList = null;
                    // var prova = new NetService.Utility.Common.RequestVariable(fileList.ID);
                    if (!String.IsNullOrEmpty(fileList.Value))
                    {
                        _FileTempList = new List<IDictionary>();
                        string[] templist = fileList.Value.Split(',');
                        foreach (string s in templist)
                        {
                            var item = new Hashtable();
                            string[] fileArray = s.Split('\\');
                            int i = fileArray.Length;
                            string filename = fileArray.GetValue(i - 1).ToString();
                            item.Add("FileName", filename);
                            _FileTempList.Add(item);
                        }
                    }
                    else
                        _FileTempList = null;

                }
                return _FileTempList;
            }

        }
        public override string LocalCssClass
        {
            get
            {
                return "multiuploadify-vf";
            }
        }

        public override NetService.Utility.Common.RequestVariable Request
        {
            get
            {
                if (_filesRequest == null)
                {
                    _filesRequest = new NetService.Utility.Common.RequestVariable(fileList.ID, NetService.Utility.Common.RequestVariable.RequestType.Form);
                }
                return _filesRequest;
            }
        }
        private NetService.Utility.Common.RequestVariable _filesRequest;
        protected HiddenField fileList
        {
            get
            {
                if (_fileList == null)
                {
                    _fileList = new HiddenField();
                    _fileList.ID = this.Uploadify.CtrlId + "fileList";
                    //_fileList.ID = "provafilelist";
                    //_delete.OnClientClick += new EventHandler(DeleteClickHandler);
                    //_delete.Attributes.Add("style","display:none");
                }
                return _fileList;
            }
        }
        private HiddenField _fileList;

        public override object PostbackValueObject
        {
            get
            {

                return FileTempList;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }
        public virtual void DeleteClickHandler(object sender, EventArgs e)
        {

            try
            {
                DeleteFileFromDisk();

            }
            catch (Exception ex)
            {

            }
        }

        public bool DeleteFileFromDisk()
        {
            string filename = field.Value;

            string pathorigine = this.UploadTempFolder;
            string pathdestinazione = this.UploadFolder;
            if (File.Exists(HttpContext.Current.Server.MapPath("/" + pathdestinazione) + "\\" + filename))
            {
                File.Delete(HttpContext.Current.Server.MapPath("/" + pathdestinazione) + "\\" + filename);
                return true;
            }
            return false;
        }

        public override Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set { _SearchComparisonCriteria = value; }
        }
        private Finder.ComparisonCriteria _SearchComparisonCriteria = Finder.ComparisonCriteria.Like;
        protected override void FillFieldValue()
        {
            throw new NotImplementedException();
        }

        protected override void LocalValidate()
        {

        }

        /// <summary>
        /// Numero massimo di file, 
        /// Valore di Default 0 (nessun limite)
        /// </summary>
        public int MaxFileNumber
        {
            get { return _maxFileNumber; }
            set { _maxFileNumber = value; }
        }
        private int _maxFileNumber = 0;
        private Uploader _Uploadify;
        protected Uploader Uploadify
        {
            get
            {
                if (_Uploadify == null)
                {
                    _Uploadify = new Uploader(this.CtrlID + "customUploader", "/scripts/shared/plupload/js", UploadFolder, UploadTempFolder, Filters, NetCms.Configurations.PortalData.UploadFileSizeLimit / 1000, MaxFileNumber, CtrlRuntimes.All);
                    //_Uploadify.MaxFileNumber = MaxFileNumber;
                    //_Uploadify.SaveButton = false;
                    //_Uploadify.SaveClickHandler += save_Click;
                }
                return _Uploadify;

            }

        }
        private string _UploadTempFolder;
        public string UploadTempFolder
        {
            get { return _UploadTempFolder; }
            set { _UploadTempFolder = value; }
        }

        private string _UploadFolder;
        public string UploadFolder
        {
            get { return _UploadFolder; }
            set { _UploadFolder = value; }
        }
        //booleano (di default a false) per abilitare la visualizzazione degli allegati presenti nella cartella indicata in uploadFolder</param>
        private bool _DisplayFileList;
        public bool DisplayFileList
        {
            get { return _DisplayFileList; }
            set { _DisplayFileList = value; }
        }
        private List<FileFilter> _Filters;
        public List<FileFilter> Filters
        {
            get
            {
                if (_Filters == null)
                {
                    _Filters = new List<FileFilter>();
                    _Filters.Add(new FileFilter("Image files", "jpg,jpeg,gif,png"));
                    _Filters.Add(new FileFilter("Document files", "doc,docx,xls,xlsx,pdf,p7f"));
                    _Filters.Add(new FileFilter("Media", "mp3,mp4,wmv,asf"));
                    _Filters.Add(new FileFilter("Zip files", "zip,rar"));
                }
                return _Filters;
            }
            set { _Filters = value; }

        }

        public Button delete
        {
            get
            {
                if (_delete == null)
                {
                    _delete = new Button();
                    _delete.Click += DeleteClickHandler;
                    //_delete.OnClientClick += new EventHandler(DeleteClickHandler);
                    //_delete.Attributes.Add("style","display:none");
                }
                return _delete;
            }
        }
        private Button _delete;
        protected HiddenField field
        {
            get
            {
                if (_field == null)
                {
                    _field = new HiddenField();
                    _field.ID += this.CtrlID + "filename";
                    //_field.ID += this.CtrlID + "filename";
                    //_delete.OnClientClick += new EventHandler(DeleteClickHandler);
                    //_delete.Attributes.Add("style","display:none");
                }
                return _field;
            }
        }
        private HiddenField _field;


        #region Costruttori
        /// <summary>
        /// VfMultiUpload
        /// </summary>
        /// <remarks>Si può gestire il trasferimento dei files manualmente oppure richiamare il metodo 'save_click' che non fa altro che trasferire i files dalla cartella temporanea
        /// alla cartella definitiva(uploadFolder).
        /// </remarks>
        /// <param name="id">Id componente (Univoco)</param>
        /// <param name="label">Label</param>
        /// <param name="uploadTempFolder">Path cartella temporanea di upload.Se non settata l'upload verrà effettuato in '/repository/UploadTmpFolder/{yyyyMMddhhmmss}'</param>
        /// <param name="uploadFolder">Path cartella dove verranno trasferiti i file. Se non settata i files verranno trasferiti in '/repository/UploadFolder/{idcomponente}</param>
        /// <param name="filters">Array di stringhe contenente i formati accettati.Lasciando il valora a null accetta tutti i formati principali.</param>
        /// <param name="FilterFiles">Booleano che mi indica la possibilità di selezionare solo alcuni dei documenti contenuti all'interno della folder specificata .</param>
        /// <param name="FilenamesFilerFiles">Lista dei filename dei file da filtrare nel caso in cui il booleano  FilterFiles sia impostato a tue.</param>

        public VfMultiUpload(string id, string label, string uploadTempFolder = null, string uploadFolder = null, string[] filters = null, int maxfilenumber = 0, bool FilterFiles = false, List<string> FilenamesFilerFiles = null) : base(id, label)
        {

            this.FilterFiles = FilterFiles;
            this.FilenamesFilerFiles = FilenamesFilerFiles;


            if (uploadTempFolder == null)
            {
                UploadTempFolder = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/UploadTmpFolder/" + DateTime.Now.ToString("yyyyMMdd");

            }
            else
            {
                UploadTempFolder = NetCms.Configurations.Paths.AbsoluteRoot + uploadTempFolder;

            }

            if (uploadFolder == null)
            {
                UploadFolder = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/UploadFolder/" + id;
            }
            else
                UploadFolder = NetCms.Configurations.Paths.AbsoluteRoot + uploadTempFolder;
            if (filters != null)
            {
                Filters = new List<FileFilter>();
                foreach (string s in filters)
                {
                    Filters.Add(new FileFilter(s, s));
                }

            }
            
            this.MaxFileNumber = maxfilenumber;
            this.BindField = false;
            this.CtrlID = id;
            //Button delete = new Button();

            delete.Click += DeleteClickHandler;

            delete.ID = id + "deleteButton";
            delete.Attributes.Add("style", "display:none");
            WebControl divModale = new WebControl(HtmlTextWriterTag.Div);
            divModale.ID = this.CtrlID + "dialogDelete";
            divModale.Style.Add(HtmlTextWriterStyle.Display, "none");
            divModale.CssClass = "dialogDelete";
            //EliminationConfirmControl controlElimination = new EliminationConfirmControl("Eliminazione Allegato", "Vuoi eliminare l'allegato?", "");

            //divModale.Controls.Add(delete);
            //divModale.Controls.Add(controlElimination);
            divModale.Controls.Add(new LiteralControl("<strong>ATTENZIONE</strong>: Sei sicuro di voler eliminare l'allegato DEFINITIVAMENTE?"));
            divModale.Controls.Add(delete);
            divModale.Controls.Add(field);
            divModale.Controls.Add(fileList);
            this.Controls.Add(divModale);
            //this.Controls.Add(delete);
        }

        public VfMultiUpload(string id, string label, string uploadTempFolder = null, string[] filters = null) : base(id, label)
        {

            if (uploadTempFolder == null)
            {
                UploadTempFolder = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/UploadTmpFolder/" + DateTime.Now.ToString("yyyyMMdd");
            }
            else
                UploadTempFolder = NetCms.Configurations.Paths.AbsoluteRoot + uploadTempFolder;

            if (filters != null)
            {
                Filters = new List<FileFilter>();
                foreach (string s in filters)
                {
                    Filters.Add(new FileFilter(s, s));
                }

            }
            this.BindField = false;
            this.CtrlID = id;            
            //Button delete = new Button();

            delete.Click += DeleteClickHandler;

            delete.ID = id + "deleteButton";
            delete.Attributes.Add("style", "display:none");
            WebControl divModale = new WebControl(HtmlTextWriterTag.Div);
            divModale.ID = this.CtrlID + "dialogDelete";
            divModale.Style.Add(HtmlTextWriterStyle.Display, "none");
            divModale.CssClass = "dialogDelete";
            //EliminationConfirmControl controlElimination = new EliminationConfirmControl("Eliminazione Allegato", "Vuoi eliminare l'allegato?", "");

            //divModale.Controls.Add(delete);
            //divModale.Controls.Add(controlElimination);
            divModale.Controls.Add(new LiteralControl("<strong>ATTENZIONE</strong>: Sei sicuro di voler eliminare l'allegato dal DEFINITIVAMENTE?"));
            divModale.Controls.Add(delete);
            divModale.Controls.Add(field);
            divModale.Controls.Add(fileList);
            this.Controls.Add(divModale);
            //this.Controls.Add(delete);
        }

        #endregion 
        public enum Status
        {
            Success,
            Failed,
            Overwritten,
            ModifiedFileName
        }
        /// <summary>
        /// Elenco dei filtri possibili,
        /// ImageFiles -> jpg,jpeg,gif,png
        /// DocumentFiles -> doc,docx,xls,xlsx,pdf,pdf7
        /// Media -> mp3,mp4,wmv,asf
        /// </summary>
        public enum FileFilters
        {
            ImageFiles,
            DocumentFiles,
            Media,
            ZipFiles

        }
        /// <summary>
        /// Metodo di trasferimento files da cartella temporanea a cartella definitiva (definite nel costruttore)
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">EventArgs e</param>
        /// <param name="overwrite">Se true, sovrascrive i file con lo stesso nome</param>
        /// <returns>Torna una List di 'KeyValuePair' contenente i file trasferiti e lo status del trasferimento.
        ///  Success => trasferito con successo
        ///  Failed => trasferimento fallito
        ///  Overwritten => file sovrascritto (era presente un file con lo stesso nome)
        ///  ModifiedFileName => non sovrascrive il file, ma ne modifica il nome 
        ///  </returns>
        public List<KeyValuePair<string, Status>> save_Click(string uploadDestFolder,bool overwrite = true)
        {
            string pathorigine = this.UploadTempFolder;
            string pathdestinazione = uploadDestFolder;
            var list = new List<KeyValuePair<string, Status>>();
            try
            {
                if (!System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(pathdestinazione)))
                    System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath(pathdestinazione));

                string[] filePaths = Directory.GetFiles(HttpContext.Current.Server.MapPath(pathorigine));
                // string[] fileprova = Directory.GetFiles(uploadSettings.DestinationPath);
                foreach (string file in filePaths)
                {
                    string[] fileArray = file.Split('\\');
                    int i = fileArray.Length;
                    string filename = fileArray.GetValue(i - 1).ToString();
                    try
                    {

                        if (File.Exists(HttpContext.Current.Server.MapPath(pathdestinazione) + "\\" + filename))
                        {
                            if (overwrite) // Se true elimina il vecchio file con lo stesso nome 
                            {
                                File.Delete(HttpContext.Current.Server.MapPath(pathdestinazione) + "\\" + filename);
                                File.Move(file, HttpContext.Current.Server.MapPath(pathdestinazione) + "\\" + filename);
                                list.Add(new KeyValuePair<string, Status>(filename, Status.Overwritten));
                            }
                            else // altrimenti cambia il filename aggiungendo un suffisso (ciclo finchè non trova quello corretto)
                            {
                                string nameTemp = filename;
                                string ext = nameTemp.Split('.').Last();
                                nameTemp = nameTemp.Replace("." + ext, "");
                                do
                                {

                                    filename = nameTemp + "_" + DateTime.Now.TimeOfDay.ToString().Replace(":","").Replace(".","") + "." +ext;
                                }
                                while (File.Exists(HttpContext.Current.Server.MapPath(pathdestinazione) + "\\" + filename));
                                File.Move(file, HttpContext.Current.Server.MapPath(pathdestinazione) + "\\" + filename);
                                list.Add(new KeyValuePair<string, Status>(filename, Status.ModifiedFileName));

                            }
                        }
                        else
                        {
                            File.Move(file, HttpContext.Current.Server.MapPath(pathdestinazione) + "\\" + filename);
                            list.Add(new KeyValuePair<string, Status>(filename, Status.Success));
                        }
                    }
                    catch (Exception ex)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message);
                        list.Add(new KeyValuePair<string, Status>(filename, Status.Failed));
                    }
                }

            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message);
                return list;


            }
            return list;

        }

    }
    public class Uploader : WebControl
    {

        protected override void OnInit(EventArgs e)
        {

            base.OnInit(e);
            UploadSetting uploadSetting = new UploadSetting(
              UploadTmpFolder,
              MaxFileSize,
              AcceptedExtensions
             );
            HttpContext.Current.Session["UploadSettings"] = uploadSetting;
            this.ID = CtrlId;
            #region Script&Css

            string headCssImport = @"<link rel=""stylesheet"" type=""text/css"" href=""" + ScriptFolderPath + @"/jquery.ui.plupload/css/jquery.ui.plupload.css"" />";
            //headCssImport += @"<link rel=""stylesheet"" type=""text/css"" href=""" + ScriptFolderPath + @"/custom/fileupload.css"" /><!-- fileupload.css -->";
            string headJsImport = @"<script type=""text/javascript"" src=""" + ScriptFolderPath + @"/plupload.full.min.js""></script>";
            headJsImport += @"<script type=""text/javascript"" src=""" + ScriptFolderPath + @"/jquery.ui.plupload/jquery.ui.plupload.js""></script>";

            headJsImport += @"<script type=""text/javascript"" src=""" + ScriptFolderPath + @"/i18n/it.js""></script>";
            //string script2 = "/scripts/shared/plupload/Prova/multiuploadify.js";
            //string headJsImport2 = @"<script type=""text/javascript"" src=""" + script2 + "\"></script>";

            base.Page.Header.Controls.Add(new LiteralControl(headCssImport));
            base.Page.Header.Controls.Add(new LiteralControl(headJsImport));
            base.Page.Header.Controls.Add(new LiteralControl(jsCode()));
            //base.Page.Header.Controls.Add(new LiteralControl(headJsImport2));

            #endregion



        }
        public Uploader(string id, string scriptFolderPath, string uploadFolder, string uploadtempfolder, List<FileFilter> acceptedFiles, int maxFileSize, int maxfilenumber, CtrlRuntimes runtimes)
        {
            CtrlId = id;
            ScriptFolderPath = scriptFolderPath;
            UploadTmpFolder = uploadtempfolder;
            AcceptedFiles = acceptedFiles;
            MaxFileSize = maxFileSize;
            MaxFileNumber = maxfilenumber;
            Runtime = runtimes;
        }
        internal string CtrlId;
        protected string ScriptFolderPath;
        public int MaxFileSize
        {
            get { return _maxFileSize; }
            protected set { _maxFileSize = value; }
        }
        private int _maxFileSize = 2;
        /// <summary>
        /// Numero massimo di file, 
        /// Valore di Default 0 (nessun limite)
        /// </summary>
        public int MaxFileNumber
        {
            get { return _maxFileNumber; }
            set { _maxFileNumber = value; }
        }
        public int _maxFileNumber = 0;
        protected CtrlRuntimes Runtime;
        protected List<FileFilter> AcceptedFiles
        {
            get;
            set;
        }

        protected string UploadTmpFolder;
        public bool MultiSelection
        {
            get { return _multiSelection; }
            set { _multiSelection = value; }
        }
        private bool _multiSelection = true;
        protected string AcceptedExtensions
        {
            get
            {
                if (_AcceptedExtensions == null)
                    _AcceptedExtensions = GetExtensionFiltersSerialized();
                return _AcceptedExtensions;
            }
        }
        private string _AcceptedExtensions;
        public bool PreventDuplicates
        {
            get { return preventDuplicates; }
            set { preventDuplicates = value; }
        }
        private bool preventDuplicates = false;
        public string ChunkSize
        {
            get { return _chunkSize; }
            set { _chunkSize = value; }
        }
        private string _chunkSize = "100mb";
        public bool Dragdrop
        {
            get { return _dragdrop; }
            set { _dragdrop = value; }
        }
        private bool _dragdrop = true;
        private string GetFileFilter()
        {
            string strFilter = "";

            foreach (FileFilter filter in AcceptedFiles)
            {
                strFilter += "{ title: '" + filter.Title + "', extensions: '" + filter.Extensions + "' }" + ((filter != AcceptedFiles.Last()) ? "," : "");
            }

            return strFilter;
        }
        public string GetRuntimes(CtrlRuntimes runtimes)
        {
            string strRuntime = "";
            switch (runtimes)
            {
                case CtrlRuntimes.All:
                    strRuntime = "html5,flash,silverlight,html4";
                    break;
                case CtrlRuntimes.Html5FlashSilverlight:
                    strRuntime = "html5,flash,silverlight";
                    break;
                case CtrlRuntimes.Html5Flash:
                    strRuntime = "html5,flash";
                    break;
                case CtrlRuntimes.Html5Silverlight:
                    strRuntime = "html5,silverlight";
                    break;
                case CtrlRuntimes.Html5:
                    strRuntime = "html5";
                    break;
                case CtrlRuntimes.Flash:
                    strRuntime = "flash";
                    break;
                case CtrlRuntimes.FlashSilverlight:
                    strRuntime = "flash,silverlight";
                    break;
                case CtrlRuntimes.Silverlight:
                    strRuntime = "silverlight";
                    break;
                case CtrlRuntimes.Html4:
                    strRuntime = "html4";
                    break;
                default:
                    strRuntime = "html5,flash,silverlight,html4";
                    break;
            }
            return strRuntime;

        }
        protected string jsCode()
        {
            string script = @"<script type=""text/javascript"">$(document).ready(function(){
                 var maxfiles = " + MaxFileNumber + @";
	             var files_selected = new Array();
	            var uploader = $(" + this.CtrlId + @").plupload({
        runtimes: 'html5,silverlight,flash,html4',
        url: '/uploader/plupload_handler.ashx',
        max_file_size: '" + MaxFileSize + @"mb',
        chunk_size: '" + ChunkSize + @"',
        unique_names: true,
		prevent_duplicates: true,";
            if (MaxFileNumber > 0)
                script += @"max_file_count : " + MaxFileNumber + ",";
            //container: document.getElementById('DropArea_customUploader'),
            script += @"views:
            {
                list: false,
            thumbs: true, 
            active: 'thumbs'
        },
		
        filters: {            
            mime_types: [           
            " + GetFileFilter() + @"
            ]
        },
        
        silverlight_xap_url: '/scripts/shared/plupload/js/plupload.silverlight.xap',
        multiple_queues: true,
		
		init:
            {
                PostInit: function(up){

                    
                    //$('#plupload_customeUploader').empty();
                    var filesPending = $('#" + this.CtrlId + @"fileList')[0].value;
                    // Recupero l'html element che contiene i files che ho caricato precedentemente nell'Uploader e che 
                    // per qualche errore non sono stati caricati
                
                    if(filesPending)
                     {
                            var files = new Array();
                            var filesToShow = [];
                            filesToShow = filesPending.split(',');
                            $.each(filesToShow,function(index,element) // Se ho files Pendenti li carico nell'uploader
                            {
                                if(element)
                                {
                                    var f = new plupload.File({name:element});
                                    up.addFile(f);
                                    
                                }
                            });
                            
                            
                           
                            //files.push(f);
                            //up.trigger('FilesAdded', files);
                            //up.refresh();

                          // uploader.UploadFile( $('#" + this.CtrlId + @"fileList')[0].value);
                          //alert( $('#" + this.CtrlId + @"fileList')[0].value);
        }
                    plupload.addI18n({
                        'File extension error.': 'Il file selezionato non è supportato.',
                    'File size error.': 'La dimensione del file supera i limiti consentiti.'
                    });

                },
			BeforeUpload: function(up, file){


                    up.settings.multipart_params = {
                        filenameRenamed: file.name
                    };

                },
			FilesRemoved: function(up, file)

            {

                    var params = {
                        FileNameTOBeDeleted: '" + UploadTmpFolder + @"/' + file[0].name
                            };

				 $.ajax({
                        url: '/charts/deleteFilePlupload.ashx',
                            type: 'POST',
                            data: JSON.stringify(params),
                            contentType: 'application/json',
                            dataType: 'json',
                            async: false,
                            success: function(response){
                            results = response;
                        }
                    });
                    files_selected.pop();  
                    $('#" + this.CtrlId + @"fileList').val(files_selected.toString());

                },
			FilesAdded: function(up, files) {

                    plupload.each(files, function(file)
                {
                        var results;
                        var exist = true;
                        var app = '';
                        var count = 0;
                        var ext = file.name.substr(file.name.lastIndexOf('.'));
                        var filename = file.name.substr(0, file.name.lastIndexOf('.'));

                        while (exist)
                        {
                            var params = {
                                ""folderId"": $('#currentFolder_customUploader').val(),
                                ""filename"": filename + app + ext,
                                ""filenamecheck"" : 'true'
                            };
						//if (files.filter(function(e) {e.name == filename;}).length > 0) {
						//	errorFile = true;
						//}
                        $.ajax({
                                url: '/api/UploadManager/checkfile/',
                            type: 'POST',
                            data: JSON.stringify(params),
                            contentType: 'application/json',
                            dataType: 'json',
                            async: false,
                            success: function(response){
                                    results = response;
                                }
                            });

                            if (!results.exist)
                            {
                                exist = false;
                                file.name = filename + app + ext;
                                file.name = filename.replace(',','-') + app + ext;
                            }
                            else
                            {
                                count++;
                                app = '_' + count;
                            }

                        }
                        if (app != '')
                        {
                            var errorMsg = '<li> Il file ' + file.name.replace(app, '') + ' è stato rinominato in ' + file.name + '</li>';
                            if ($('#console_customUploader').hasClass('hide'))
                            $('#console_customUploader').removeClass('hide');
                        $('#console_customUploader ul').append(errorMsg);

                        }
                        if ($.inArray(file.name, files_selected) == -1) {

                            if ($('#files-selected-container_customUploader').hasClass('hide'))
                                $('#files-selected-container_customUploader').removeClass('hide');

                          //  if(files_selected.length > maxfiles)
                           //     alert('Massimo numero di file raggiunto');
                            //else 
                            files_selected.push(file.name);
                            
                            //setTimeout(up.start(), 100);
                        }
                        else {

                            alert('Il documento selezionato è già presente nella cartella');

                        }
                    });
                    up.start();
                    

                },
                FileUploaded: function (up, file, response) {
              
			        if( maxfiles > 0)
				    {
					    if (up.files.length > maxfiles) {
                            up.removeFile(file);
                            alert('Massimo numero di file raggiunto');
                        }
                       else    
                       {
                            
                            $('#" + this.CtrlId + @"fileList').val(files_selected.toString());

                       }
				    }
                    else    
                    {
                           $('#" + this.CtrlId + @"fileList').val(files_selected.toString()); 
                            
                    }
                    
                },
		
			Error: function(up, err) {
                    var errorMsg = '<li>Attenzione: ' + err.message + ' (cod.err ' + err.code + ') </li>';
                    if ($('#console_customUploader').hasClass('hide'))
                    $('#console_customUploader').removeClass('hide');
                $('#console_customUploader ul').append(errorMsg);
                }
            }

        });
	    uploader.init();
	
	

        });</script>";
            return script;

        }
        private string GetExtensionFiltersSerialized()
        {
            string strExtensionsSerialized = "";

            foreach (FileFilter filter in AcceptedFiles)
            {
                strExtensionsSerialized += filter.Extensions + ",";
            }
            return strExtensionsSerialized;
        }
        public enum CtrlRuntimes
        {
            All,
            Html5FlashSilverlight,
            Html5Flash,
            Html5Silverlight,
            Html5,
            Flash,
            FlashSilverlight,
            Silverlight,
            Html4
        }
    }
}