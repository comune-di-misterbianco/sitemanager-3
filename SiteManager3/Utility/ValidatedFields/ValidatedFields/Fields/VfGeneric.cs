﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetService.Utility.RecordsFinder;

[assembly: WebResource("ValidatedFields.ValidatedFields.Fields.Errors.Images.error.png", "image/png")]

namespace NetService.Utility.ValidatedFields 
{
    public abstract class VfGeneric : WebControl
    {
        public List<string> ValidationErrors
        {
            get
            {
                if (_ValidationErrors == null)
                    _ValidationErrors = new List<string>();
                return _ValidationErrors;
            }
        }
        private List<string> _ValidationErrors;
        
        public enum ValidationStates { NotYetExecuted, Valid, NotValid }

        public virtual NetService.Utility.Common.RequestVariable Request
        {
            get
            {
                if (_Request == null)
                {
                    _Request = new NetService.Utility.Common.RequestVariable(Key, NetService.Utility.Common.RequestVariable.RequestType.Form_QueryString);
                }
                return _Request;
            }
        }
        private NetService.Utility.Common.RequestVariable _Request;

        public ValidationStates ValidationState
        {
            get
            {
                return !ValidationExecuted ? ValidationStates.NotYetExecuted :
                       this.ValidationErrors.Count == 0 ? ValidationStates.Valid : ValidationStates.NotValid;
            }
        }
        public bool ValidationExecuted { get; private set; }

        public abstract WebControl Field { get; }

        public virtual string ValueForDetails
        {
            get
            {
                return Request.OriginalValue;
            }
        }

        public string ActiveRecord_WritePropertyName { get; set; }
        public string ActiveRecord_ReadPropertyName { get; set; }
        
        private string detailsPropertyName;
        public string DetailsPropertyName 
        { 
            get
            {
                return detailsPropertyName.HasContent() ? detailsPropertyName : ActiveRecord_ReadPropertyName;
            }
            set
            {
                detailsPropertyName = value;
            }
        }

        public bool Required { get; set; }

        public List<Validator> Validators
        {
            get
            {
                if (_Validators == null)
                {
                    _Validators = new List<Validator>();
                }
                return _Validators;
            }
        }
        private List<Validator> _Validators;
                
        public string BaseCssClass
        {
            get
            {
                return _BaseCssClass;
            }
            set
            {
                _BaseCssClass = value;
            }
        }
        private string _BaseCssClass = "validatedfield";

        public abstract string LocalCssClass { get; }
        public string CustomCssClass { get; set; }

        public string ColumCssClass { get; set; }

        public bool OverrideFieldCssClass
        {
            get;
            set;
        }


        public string HelpInlineText { get; set; }
        public WebControl InfoControl
        {
            get
            {
                if (_InfoControl == null)
                {
                    _InfoControl = new WebControl(HtmlTextWriterTag.Div);
                    _InfoControl.CssClass = "info-vfm";
                }
                return _InfoControl;
            }
        }
        private WebControl _InfoControl;

        public WebControl TitleControl
        {
            get
            {
                if (_TitleControl == null)
                {
                    _TitleControl = new WebControl(HtmlTextWriterTag.Div);
                    _TitleControl.CssClass = "title-vfm";
                }
                return _TitleControl;
            }
        }
        private WebControl _TitleControl;

        public abstract object PostbackValueObject
        {
            get;
        }

        public List<Control> ControlsToAppend
        {
            get
            {
                if (_ControlsToAppend == null)
                {
                    _ControlsToAppend = new List<Control>();
                }
                return _ControlsToAppend;
            }
        }
        private List<Control> _ControlsToAppend;

        public bool ColorOnValidationOK
        {
            get
            {
                return _ColorOnValidationOK;
            }
            set
            {
                _ColorOnValidationOK = value;
            }
        }
        private bool _ColorOnValidationOK = true; 

        public string PostBackValue
        {
            get
            {
                return this.Request.StringValueNoReplace;
            }
        }

        public string Label
        {
            get
            {
                return _Label;
            }
            private set
            {
                _Label = NetService.Localization.LabelsManager.GetLabel(value);
            }
        }
        private string _Label;

        protected bool DataBinded
        {
            get
            {
                return DefaultValue != null;
            }
        }

        public bool BindField { get; set; } 

        public object DefaultValue
        {
            get
            {
                return _DefaultValue;
            }
            set
            {
                _DefaultValue = value;
            }
        }
        private object _DefaultValue;

        public abstract Finder.ComparisonCriteria SearchComparisonCriteria { get; set; }
        public bool ShowsInDetailsSheets { get; set; }

        public virtual VfSearchParameter SearchParameter
        {
            get
            {
                if (_SearchParameter == null)
                {
                    _SearchParameter = new VfSearchParameter(this, SearchComparisonCriteria);
                }
                return _SearchParameter;
            }
        }
        private VfSearchParameter _SearchParameter;

        public string Key
        {
            get
            {
                return _Key;
            }
            private set
            {
                _Key = value;
            }
        }
        private string _Key;

        public VfGeneric(string key, string labelResourceKey, string activeRecordPropertyName, Type returnObjectType)
            : base(HtmlTextWriterTag.Div)
        {
            this.Key = key;
            this.ActiveRecord_WritePropertyName = activeRecordPropertyName;
            this.ActiveRecord_ReadPropertyName = activeRecordPropertyName;
            this.Label = labelResourceKey;
            this.Required = true;
            this.CssClass = "vf";
            this.BindField = true;
            this.ShowsInDetailsSheets = true;
            this.ReturnObjectType = returnObjectType;
            this.OverrideFieldCssClass = false;
        }

        public VfGeneric(string key, string labelResourceKey, string activeRecordPropertyName)
            : base(HtmlTextWriterTag.Div)
        {
            this.Key = key;
            this.ActiveRecord_WritePropertyName = activeRecordPropertyName;
            this.ActiveRecord_ReadPropertyName = activeRecordPropertyName;
            this.Label = labelResourceKey;
            this.Required = true;
            this.CssClass = "vf";
            this.BindField = true;
            this.ShowsInDetailsSheets = true;
            this.OverrideFieldCssClass = false;
        }

        public VfGeneric(string key, string labelResourceKey)
            : this(key, labelResourceKey, key)
        {
            this.OverrideFieldCssClass = false;
        }


        private bool _UseBootstrapItalia;
        public bool UseBootstrapItalia
        {
            get { return _UseBootstrapItalia; }
            set { _UseBootstrapItalia = value; }
        }

        private Label _LabelControl;
        public Label LabelControl
        {
            get
            {
                if (_LabelControl == null)
                {
                    _LabelControl = new Label();
                    _LabelControl.AssociatedControlID = this.Key;
                    _LabelControl.CssClass = "control-label";
                    _LabelControl.Text = Label + (this.Required ? "*" : "") + " ";
                    
                }
                return _LabelControl;
            }
        }

        protected void AddError(string s)
        {
            ValidationErrors.Add(s);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            this.Field.Enabled = this.Enabled;

            if (TitleControl.Controls.Count > 0)
                this.Controls.Add(TitleControl);

            if (InfoControl.Controls.Count > 0)
                this.Controls.Add(InfoControl);

            string cssValidState = "";

            if (this.ValidationState == ValidationStates.NotValid)
            {
                if (EnableErrorsInPopupMode)
                {
                    WebControl script = new WebControl(HtmlTextWriterTag.Script);
                    script.Attributes["type"] = "text/javascript";

                    string str_script = @"             
	            $(function() {    
                    $(""#image-error-" + this.Key + @""")
                      .tooltip({
                        position: ""center right"",
                        relative: true,
                        offset: [5,2],
                        content: $(""#error-dialog-" + this.Key + @""").html(),
                        items: 'div'
                        })
                      .off(""mouseover"")
                      .on(""click"", function(){
                          $( this ).tooltip(""open"");
                          return false;
                        })


	            });";
                    
                    
                    script.Controls.Add(new LiteralControl(str_script));

                    this.Controls.Add(script);
                 
                    this.Controls.Add(ValidationResultsInPopupControl());
                }
                else
                    this.Controls.Add(ValidationResultsControl());

                cssValidState += " has-error has-feedback not-valid";
            }
            if (ColorOnValidationOK && this.ValidationState == ValidationStates.Valid)
            { 
                cssValidState += " has-success has-feedback valid"; 
            }

            if (!OverrideFieldCssClass)
                this.CssClass = "form-group " + BaseCssClass + " " + LocalCssClass + " " + cssValidState + (string.IsNullOrEmpty(CustomCssClass) ? "" : " " + CustomCssClass);
            else
                this.CssClass = cssValidState + " " + (string.IsNullOrEmpty(CustomCssClass) ? "" : " " + CustomCssClass);
           
            this.AttachFieldControls();

            foreach (Control ctr in this.ControlsToAppend)
                this.Controls.Add(ctr);


            if (!this.Request.IsPostBack && this.DataBinded && !this.Request.IsValidString) this.FillFieldValue();
        }
        protected virtual void AttachFieldControls()
        {
            if (UseBootstrap)
            {
                
                //Label label = new Label();
                //label.AssociatedControlID = this.Key;
                //label.CssClass = "control-label";
                //label.Text = Label + (this.Required ? "*" : "") + " ";
                

                if (!ShowLabelAfterField && !HideLabel)
                    this.Controls.Add(this.LabelControl);

                if (!string.IsNullOrEmpty(HelpInlineText))
                {
                    WebControl inputGroup = new WebControl(HtmlTextWriterTag.Div);
                    inputGroup.CssClass = "input-group";

                    inputGroup.Controls.Add(Field);

                    WebControl inputGroupAppender = new WebControl(HtmlTextWriterTag.Div);
                    inputGroupAppender.CssClass = "input-group-append";
                    //inputGroupAppender.Attributes["title"] = HelpInlineText;

                    // codice popover                  
                    inputGroupAppender.Controls.Add(new LiteralControl(@"<a class=""input-group-text""
                                                                            role=""button"" 
                                                                            data-toggle=""popover"" 
                                                                            data-placement=""top"" 
                                                                            data-trigger=""click"" 
                                                                            title=""Aiuto"" 
                                                                            data-content=""" + HelpInlineText.Replace("\"", "&quot;") +@""">
                                                                            <i class=""fa fa-question-circle"" aria-hidden=""true""></i>
                                                                         </a>"));

                    inputGroup.Controls.Add(inputGroupAppender);

                    this.Controls.Add(inputGroup);
                }
                else
                {
                   this.Controls.Add(Field);
                }

                if (ShowLabelAfterField && !HideLabel)
                    this.Controls.Add(LabelControl);              

                this.Field.ID = this.Key;
            }
            else
            {

                //Label label = new Label();
                //label.AssociatedControlID = this.Key;
                //label.CssClass = "control-label";
                //label.Text = Label + (this.Required ? "*" : "") + "  ";


                if (!ShowLabelAfterField && !HideLabel)
                {
                    this.Controls.Add(LabelControl);

                    if (!string.IsNullOrWhiteSpace(HelpInlineText))
                    {
                        WebControl aPop = new WebControl(HtmlTextWriterTag.A);
                        // aPop.CssClass = "btn btn-info";
                        aPop.CssClass = "field-help";
                        aPop.Controls.Add(new LiteralControl("<i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i>"));

                        //a.Attributes.Add("role", "button");
                        aPop.Attributes.Add("data-toggle", "popover");
                        aPop.Attributes.Add("data-trigger", "hover");
                        aPop.Attributes.Add("data-content", HelpInlineText.Replace("\"", "&quot;"));

                        this.Controls.Add(aPop);
                    }
                }

               
               this.Controls.Add(Field);
               

                if (ShowLabelAfterField && !HideLabel)
                {
                    this.Controls.Add(LabelControl);
                    if (!string.IsNullOrWhiteSpace(HelpInlineText))
                    {
                        WebControl aPop = new WebControl(HtmlTextWriterTag.A);
                        //aPop.CssClass = "btn btn-info";
                        aPop.Controls.Add(new LiteralControl("<i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i>"));
                        //a.Attributes.Add("role", "button");
                        aPop.Attributes.Add("data-toggle", "popover");
                        aPop.Attributes.Add("data-trigger", "hover");
                        aPop.Attributes.Add("data-content", HelpInlineText);

                        this.Controls.Add(aPop);
                    }
                }

                this.Field.ID = this.Key;
            }
        }

        private bool showLabelAfterField = false;
        public bool ShowLabelAfterField
        {
            get { return showLabelAfterField; }
            set { showLabelAfterField = value; }
        }

        private bool hideLabel = false;
        public bool HideLabel
        {
            get { return hideLabel; }
            set { hideLabel = value; }
        }

        private bool showLabelAsPlaceholder = false;
        public bool ShowLabelAsPlaceholder
        {
            get { return showLabelAsPlaceholder; }
            set { showLabelAsPlaceholder = value; }
        }

        public void Validate()
        {
            
            ValidationExecuted = true;
            string value = this.Request.StringValue;
            if (this.Required && string.IsNullOrEmpty(value))
            {
                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
            }
            else
            {
                LocalValidate();

                foreach (Validator validator in this.Validators)
                {
                    if (validator != null && !validator.Validate(value) && validator.ErrorMessage!="")
                        this.AddError(validator.ErrorMessage);
                }
            }
        }
        protected abstract void LocalValidate();

        protected WebControl ValidationResultsControl()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);

            if (UseBootstrapItalia)
            {
                div.CssClass = "invalid-feedback";
                foreach (var validation in this.ValidationErrors)
                {
                    div.Controls.Add(new LiteralControl(validation));
                }

                    //"<div class=""valid-feedback"">Validato!</div>"
                    
            }
            else
            {
                div.CssClass = "validation-results";
                WebControl ul = new WebControl(HtmlTextWriterTag.Ul);             

                ul.CssClass = "list-unstyled";

                //WebControl a = new WebControl(HtmlTextWriterTag.A);
                //a.Attributes["class"] = "close";
                //a.Attributes["data-dismiss"] = "alert";
                //a.Attributes["aria-label"] = "close";
                //a.Controls.Add(new LiteralControl("&times"));
                //div.Controls.Add(a);

                foreach (var validation in this.ValidationErrors)
                {
                    ul.Controls.Add(new LiteralControl("<li><div class=\"alert alert-danger\" role=\"alert\">" + validation + "</div></li>"));
                }

                div.Controls.Add(ul);
            }
            return div;
        }

        protected WebControl ValidationResultsInPopupControl()
        {
            WebControl span = new WebControl(HtmlTextWriterTag.Span);
            WebControl image = new WebControl(HtmlTextWriterTag.Img);
            image.ID = "image-error-" + this.Key;
            image.Attributes["alt"] = "Errore validazione";
            image.Attributes["src"] = Page.ClientScript.GetWebResourceUrl(this.GetType(), "ValidatedFields.ValidatedFields.Fields.Errors.Images.error.png");
            //WebControl ul = new WebControl(HtmlTextWriterTag.Ul);
            span.CssClass = "validation-results";
            span.Controls.Add(image);

            WebControl divModale = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
            divModale.ID = "error-dialog-" + this.Key;
            divModale.Style.Add(HtmlTextWriterStyle.Display, "none");

            WebControl ul = new WebControl(HtmlTextWriterTag.Ul);
            
            foreach (var validation in this.ValidationErrors)
            {
                ul.Controls.Add(new LiteralControl("<li><span>" + validation + "</span></li>"));
            }
            divModale.Controls.Add(ul);

            //divModale.Controls.Add(new LiteralControl("Errore"));
            span.Controls.Add(divModale);            //foreach (var validation in this.ValidationErrors)
            //{
            //    ul.Controls.Add(new LiteralControl("<li><span>" + validation + "</span></li>"));
            //}
            //div.Controls.Add(ul);
            return span;
        }

        protected abstract void FillFieldValue();
        public virtual string GetFieldQueryString()
        {
            if (string.IsNullOrEmpty(this.Request.StringValue))
                return null;
            else
                return string.Format("{0}={1}", this.Key, this.Request.StringValue);
        }

        public Type ReturnObjectType
        {
            get;
            protected set;
        }

        private int rowOrder = 1;
        /// <summary>
        /// Rappresenta l'ordine del campo nella riga 
        /// </summary>
        public int RowOrder 
        {
            get { return rowOrder; }
            set { rowOrder = value; }
        }
          
        /// <summary>
        /// Rappresenta l'ordine del field, se non viene impostato verrà impostato a valore di default.
        /// </summary>
        public int FieldOrder
        {
            get { return _FieldOrder; }
            set { _FieldOrder = value; }
        }
        private int _FieldOrder = 99999;

        /// <summary>
        /// Abilità la modalità di visualizzazione degli errori di validazione attraverso una popup al click/passaggio su un'immagine di alert
        /// </summary>
        public bool EnableErrorsInPopupMode
        {
            get { return _EnableErrorsInPopupMode; }
            set { _EnableErrorsInPopupMode = value; }
        }            
        private bool _EnableErrorsInPopupMode = false;

        private bool _UseBootstrap = false;
        public bool UseBootstrap {
            get { return _UseBootstrap; }
            set {
                _UseBootstrap = value;
            }
        }
      
    }
}
