﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetService.Utility.ValidatedFields
{

    /// <summary>
    /// VF DateCalendar with Boostrap 3 DatePicker 
    /// </summary>
    /// <see cref="http://eonasdan.github.io/bootstrap-datetimepicker/"/>
    public class VfTimeSpan : VfGeneric
    {     
        #region Property

        private bool _UsedOnFrontend = false;
        /// <summary>
        /// Vf used or not used on frontend
        /// </summary>       
        /// <remarks>Default Value false</remarks>
        public bool UsedOnFrontend
        {
            get { return _UsedOnFrontend; }
            set { _UsedOnFrontend = value; }
        }

        private bool _AddAccessoryInputGroup = false;
        /// <summary>
        /// Add Accessory Input Group
        /// </summary>
        public bool AddAccessoryInputGroup
        {
            get { return _AddAccessoryInputGroup; }
            set { _AddAccessoryInputGroup = value; }
        }

        private string _Locale = "it";
        /// <summary>
        /// Set DateTimePicker Localization
        /// </summary>
        public string Locale
        {
            get { return _Locale; }
            set { _Locale = value; }

        }

        //private DateTime _MaxDate = DateTime.MaxValue;
        ///// <summary>
        ///// Set Maximum date of DateTimePicker
        ///// </summary>
        //public DateTime MaxDate
        //{
        //    get { return _MaxDate; }
        //    set { _MaxDate = value; }
        //}

        //private DateTime _MinDate = DateTime.MinValue;
        ///// <summary>
        ///// Min Date
        ///// </summary>
        ///// <remarks>Set Minimum date of DateTimePicker </remarks>
        //public DateTime MinDate
        //{
        //    get { return _MinDate; }
        //    set { _MinDate = value; }
        //}
                                       

        private bool _ShowInline = false;
        /// <summary>
        /// Show DateTimePicker Inline
        /// </summary>                
        /// <remarks> Will display the picker inline without the need of a input field. This will also hide borders and shadows. </remarks>
        public bool ShowInline
        {
            get { return _ShowInline; }
            set { _ShowInline = value; }
        }

        private bool _ShowTimePickerSideBySide = false;
        /// <summary>
        /// Show TimePicker Side By Side
        /// </summary>
        /// <remarks> Shows the picker side by side when using the time and date together. </remarks>
        public bool ShowTimePickerSideBySide
        {
            get { return _ShowTimePickerSideBySide; }
            set { _ShowTimePickerSideBySide = value; }
        }

        private bool _ShowCalendarWeekNumber = false;
        /// <summary>
        /// Show Calendar Week Number
        /// </summary>
        public bool ShowCalendarWeekNumber
        {
            get { return _ShowCalendarWeekNumber; }
            set { _ShowCalendarWeekNumber = value; }
        }

        private bool _ShowClearButton = false;
        /// <summary>
        /// Show Clear Button
        /// </summary>
        /// <remarks>Show the "Clear" button in the icon toolbar. Clicking the "Clear" button will set the calendar to null.</remarks>   
        public bool ShowClearButton
        {
            get { return _ShowClearButton; }
            set { _ShowClearButton = value; }
        }

        private bool _ShowCloseButton = false;
        /// <summary>
        /// Show Close Button
        /// </summary>
        /// <remarks>Show the "Close" button in the icon toolbar. Clicking the "Close" button will hide datetimepicker.</remarks>   
        public bool ShowCloseButton
        {
            get { return _ShowCloseButton; }
            set { _ShowCloseButton = value; }
        }

        private bool _ShowTodayButton = false;
        /// <summary>
        /// Show Today Button
        /// </summary>
        /// <remarks>Show the "Today" button in the icon toolbar. Clicking the "Today" button will set the calendar view and set the date to now</remarks>   
        public bool ShowTodayButton
        {
            get { return _ShowTodayButton; }
            set { _ShowTodayButton = value; }
        }

        /// <summary>
        /// Toolbar Placement Option
        /// </summary>
        public enum DTPToolBarPlacement
        {
            Default,
            Top,
            Bottom
        }

        private DTPToolBarPlacement _ToolbarPlacement = DTPToolBarPlacement.Default;
        /// <summary>
        /// Toolbar Placement
        /// </summary>
        /// <remarks>Changes the placement of the icon toolbar.</remarks>
        public DTPToolBarPlacement ToolbarPlacement
        {
            get { return _ToolbarPlacement; }
            set { _ToolbarPlacement = value; }
        }
           
        /// <summary>
        /// LocalCssClass
        /// </summary>
        public override string LocalCssClass
        {
            get
            {
                return "textbox-vf";
            }
        }
         
        private TimeSpan _MaxValue = TimeSpan.MaxValue;
        /// <summary>
        ///  Set Maximum Time of TimePicker
        /// </summary>
        public TimeSpan MaxValue
        {
            get { return _MaxValue; }
            set { _MaxValue = value; }
        }
                
        private TimeSpan _MinValue = TimeSpan.MinValue;
        /// <summary>
        ///  Set Minimum Time of TimePicker
        /// </summary>
        public TimeSpan MinValue
        {
            get { return _MinValue; }
            set { _MinValue = value; }
        }
             
        private int _MinuteStepper = 1;
        /// <summary>
        /// Set Minute Stepper
        /// </summary>
        /// <remarks> Number of minutes the up/down arrow's will move the minutes value in the time picker </remarks>
        public int MinuteStepper
        {
            get { return _MinuteStepper; }
            set { _MinuteStepper = value; }
        }

        private int _MinHour = 0;
        /// <summary>
        /// Set Minimum Hour
        /// </summary>
        public int MinHour
        {
            get { return _MinHour; }
            set { _MinHour = value; }
        }

        private int _MaxHour = 23;
        /// <summary>
        /// Set Maximum Hour
        /// </summary>
        public int MaxHour
        {
            get { return _MaxHour; }
            set { _MaxHour = value; }
        }

        private int _MinMinute = 0;
        /// <summary>
        /// Set Minimum Minute
        /// </summary>
        public int MinMinute
        {
            get
            {
                return _MinMinute;
            }
            set
            {
                _MinMinute = value;
            }
        }

        private int _MaxMinute = 59;
        /// <summary>
        /// Set Maximum Minute
        /// </summary>
        public int MaxMinute
        {
            get
            {
                return _MaxMinute;
            }
            set
            {
                _MaxMinute = value;
            }
        }

        private int _MinSecond = 0;
        /// <summary>
        /// Set Minimum Second
        /// </summary>
        public int MinSecond
        {
            get
            {
                return _MinSecond;
            }
            set
            {
                _MinSecond = value;
            }
        }

        private int _MaxSecond = 59;
        /// <summary>
        /// Set Maximum Second
        /// </summary>
        public int MaxSecond
        {
            get
            {
                return _MaxSecond;
            }
            set
            {
                _MaxSecond = value;
            }
        }
       

        #endregion
          

        public override WebControl Field
        {
            get
            {
                if (UsedOnFrontend)
                {
                    if (AddAccessoryInputGroup)
                    {
                        WebControl InputGroup = new WebControl(HtmlTextWriterTag.Div);
                        InputGroup.CssClass = "input-group date";
                        InputGroup.ID = this.Key;

                        InputGroup.Controls.Add(new LiteralControl("<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-time\"></span></span>"));

                        InputGroup.Controls.Add(TextBox);
                        return InputGroup;
                    }
                    else
                    {
                        TextBox.ID = this.Key;
                        return TextBox;
                    }
                }
                else
                    return TextBox;
            }
        }

        public override object PostbackValueObject
        {
            get
            {
                //Aggiunto il try/catch affinchè quando la data non è inserita correttamente invece di generare un'eccezione 
                //ritorni null
                // return DateTime.Parse(this.Request.OriginalValue);  
                try
                {
                    return TimeSpan.Parse(this.Request.OriginalValue);
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set
            { _SearchComparisonCriteria = value; }
        }
        private RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = RecordsFinder.Finder.ComparisonCriteria.Equals;


        private TextBox _TextBox;
        public TextBox TextBox
        {
            get
            {
                if (_TextBox == null)
                {
                    _TextBox = new TextBox();
                    _TextBox.ID = this.ID;
                    _TextBox.CssClass = "form-control ";

                    if (this.Request.IsValidString)
                    {
                        _TextBox.Text = this.Request.StringValue;
                    }
                }
                return _TextBox;
            }
        }


        protected override void AttachFieldControls()
        {
            if (UsedOnFrontend)
            {
                Label datecalendarlabel = new Label();
                datecalendarlabel.AssociatedControlID = this.Key;
                datecalendarlabel.CssClass = "control-label";
                datecalendarlabel.Text = Label + (this.Required ? " *" : "");

                if (!ShowLabelAfterField)
                    this.Controls.Add(datecalendarlabel);

                this.Controls.Add(Field);

                if (ShowLabelAfterField)
                    this.Controls.Add(datecalendarlabel);
            }
            else
                base.AttachFieldControls();

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            WebControl script = new WebControl(HtmlTextWriterTag.Script);
            script.Attributes["type"] = "text/javascript";

            string str_script = "";
        
            string timeIntervalArray = "";

            if(MinHour != 0)
                timeIntervalArray += "[moment:({hour:0}), moment({hour: " + MinHour.ToString() +"})],";
            
            if(MaxHour != 23)
                timeIntervalArray += "[moment:({hour:" + MaxHour.ToString() + "}), moment({hour: 23})],";




            if (UsedOnFrontend)
            {
                str_script = @"
                   $(document).ready(function() {
                      $(""#" + this.Key + @""").datetimepicker({
                            locale:'" + Locale + @"',
                            format: 'HH:mm',
                            inline: " + (ShowInline ? "true" : "false") + @" ,
                            stepping: " + MinuteStepper.ToString()  + @",
                            sideBySide: " + (ShowTimePickerSideBySide ? "true" : "false") + @",
                            calendarWeeks: " + (ShowCalendarWeekNumber ? "true" : "false") + @",
                            showClear: " + (ShowClearButton ? "true" : "false") + @",
                            showTodayButton: " + (ShowTodayButton ? "true" : "false") + @",
                            showClose: " + (ShowTodayButton ? "true" : "false") + @",
                            toolbarPlacement: '" + ToolbarPlacement.ToString().ToLower() + @"',
                            disabledTimeIntervals: [ " + timeIntervalArray + @" ]
                      })
                   });";

            }
            else
            {

                 str_script = @"             
	                $(document).ready(function() {                
		                $(""#" + this.Key + @""").timepicker({

                            renderer: $.ui.datepicker.defaultRenderer,
                            monthNames: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno',
                                    'Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],
                            monthNamesShort: ['Gen','Feb','Mar','Apr','Mag','Giu',
                                    'Lug','Ago','Set','Ott','Nov','Dic'],
                            dayNames: ['Domenica','Luned&#236','Marted&#236','Mercoled&#236','Gioved&#236','Venerd&#236','Sabato'],
                            dayNamesShort: ['Dom','Lun','Mar','Mer','Gio','Ven','Sab'],
                            dayNamesMin: ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                            dateFormat: 'dd/mm/yyyy',
                            firstDay: 1,
                            prevText: '&#x3c;Prec', prevStatus: '',
                            prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                            nextText: 'Succ&#x3e;', nextStatus: '',
                            nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                            currentText: 'Oggi', currentStatus: '',
                            todayText: 'Oggi', todayStatus: '',
                            clearText: '-', clearStatus: '',
                            closeText: 'Chiudi', closeStatus: '',
                            yearStatus: '', monthStatus: '',
                            weekText: 'Sm', weekStatus: '',
                            dayStatus: 'DD d MM',
                            defaultStatus: '',
                            isRTL: false,
			                changeMonth: true,
                            changeYear: true,
                            stepMinute:" + MinuteStepper.ToString() + @",
                            hourMin:" + MinHour.ToString() + @",
                            hourMax:" + MaxHour.ToString() + @",
                            minuteMin:" + MinMinute.ToString() + @",
                            minuteMax:" + MaxMinute.ToString() + @",
                            secondMin:" + MinSecond.ToString() + @",
                            secondMax:" + MaxSecond.ToString() + @",
			                showOn: ""focus"",
                            timeFormat: 'hh:mm:ss'			           			            
		                });
	                });";


            }
            script.Controls.Add(new LiteralControl(str_script));

            this.Controls.Add(script);

        }

        #region  Constructor
        public VfTimeSpan(string id, string labelResourceKey, bool isForFrontend = false)
            : base(id, labelResourceKey)
        {
            UsedOnFrontend = isForFrontend;
        }

        public VfTimeSpan(string id, string labelResourceKey, string activeRecordPropertyName, bool isForFrontend = false)
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
            UsedOnFrontend = isForFrontend;
        }

        public VfTimeSpan(string id, string labelResourceKey, string activeRecordPropertyName, Type returnObjectType, bool isForFrontend = false)
            : base(id, labelResourceKey, activeRecordPropertyName, returnObjectType)
        {
            UsedOnFrontend = isForFrontend;
        }

        #endregion

        protected override void FillFieldValue()
        {
            if (this.DefaultValue is TimeSpan)
            {
                TimeSpan time = (TimeSpan)this.DefaultValue;

                this.TextBox.Text = time.ToString();
              
            }
        }

        protected override void LocalValidate()
        {
            if (this.Request.IsValidString && this.Request.StringValue != "")
            {
                if (!this.Request.IsValid(NetService.Utility.Common.RequestVariable.VariableType.TimeSpan))
                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("{0} non contiene un timespan valido"), Label));

                    if (MinValue > TimeSpan.MinValue && this.Request.TimeSpanValue < MinValue)
                        this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("{0} deve contenere una timespan successivo al (" + (MinValue.ToString()) + ")"), Label));

                    if (MaxValue < TimeSpan.MaxValue && this.Request.TimeSpanValue > MaxValue)
                        this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("{0} deve contenere una timespan antecedente al (" + (MaxValue.ToString()) + ")"), Label));

            }
            else if (this.Required)
            {
                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("{0} è obbligatorio"), Label));
            }
        }
    }
}
