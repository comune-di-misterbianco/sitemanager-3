﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Linq;

namespace NetService.Utility.ValidatedFields
{
    public class VfRichTextBox : VfGeneric
    {

        #region Property

        private bool _FullPage = false;
        public bool FullPage
        {
            get;
            set;
        }

        public bool ReadOnly
        {
            get;
            set;
        }
        private bool _ReadOnly = false;
        
        private bool _FilterURL = true;
        public bool FilterURL
        {
            get { return _FilterURL; }
            set { _FilterURL = value; }
        }

        //NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteAdminRoot + @"/cms/popup/navigate.aspx?targetctrid=" + FieldName
        /// <summary>
        /// NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteAdminRoot + @"/cms/popup/navigate.aspx?targetctrid=" + FieldName
        /// </summary>
        public string FileBrowseURL
        {
            get;
            set;
        }

        //NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteAdminRoot + @"/cms/popup/navigate.aspx?type=" + (int)TipoSelettore.Images + "&targetctrid=" + FieldName
        /// <summary>
        /// NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteAdminRoot + @"/cms/popup/navigate.aspx?type=" + (int)TipoSelettore.Images + "&targetctrid=" + FieldName
        /// </summary>
        public string ImageBrowseURL
        {
            get;
            set;
        }

        //NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteAdminRoot + @"/cms/popup/navigate.aspx?type=" + (int)TipoSelettore.Flash + "&targetctrid=" + FieldName
        /// <summary>
        /// NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteAdminRoot + @"/cms/popup/navigate.aspx?type=" + (int)TipoSelettore.Flash + "&targetctrid=" + FieldName
        /// </summary>
        public string FlashBrowseURL
        {
            get;
            set;
        }
                 
        public int MaxLenght
        {
            get
            {
                return _MaxLenght;
            }
            set
            {
                _MaxLenght = value;
            }
        }
        private int _MaxLenght = 160000;

        public int MinLenght
        {
            get
            {
                return _MinLenght;
            }
            set
            {
                _MinLenght = value;
            }
        }
        private int _MinLenght = 0;

        public override string LocalCssClass
        {
            get
            {
                return "richtextbox-vf";
            }
        }

        public string Width
        {
            get
            {
                try
                {
                    if (Configuration != null && Configuration["width"] != null)
                        return Configuration["width"];
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Configurazione di ckeditor nel web.config mancante!");
                }
                return "720";
            }
        }

        public string Height
        {
            get
            {
                try
                {
                    if (Configuration != null && Configuration["height"] != null)
                        return Configuration["height"];
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Configurazione di ckeditor nel web.config mancante!");
                }
                return "440";
            }
        }

        #endregion


        #region Constructor

        public VfRichTextBox(string id, string labelResourceKey, string activeRecordPropertyName, string toolbarConfigToUse = "full")
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
            ToolbarConfigToUse = toolbarConfigToUse;
        }

        public VfRichTextBox(string id, string labelResourceKey)
            : base(id, labelResourceKey, id)
        {
            ToolbarConfigToUse = "full";
        }

        #endregion

        public override WebControl Field
        {
            get
            {
                return RichTextBox;
            }
        }

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set
            { _SearchComparisonCriteria = value; }
        }
        private RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = RecordsFinder.Finder.ComparisonCriteria.Like;

        public override object PostbackValueObject
        {
            get
            {
                return this.Request.DatabaseStringValue;
            }
        }

        private TextBox _RichTextBox;
        public TextBox RichTextBox
        {
            get
            {
                if (_RichTextBox == null)
                {
                    initField();

                    if (this.Request.IsValidString)
                    {
                        RichTextBox.Text = this.Request.StringValue.Replace("''", "'");
                    }
                }

                return _RichTextBox;
            }
        }

        private static NetCms.Vertical.Configuration.Configuration Configuration
        {
            get
            {
                return _Configuration ?? (_Configuration = new NetCms.Vertical.Configuration.Configuration("ckeditor"));
            }
        }
        private static NetCms.Vertical.Configuration.Configuration _Configuration;

        private List<string> ToolbarConfig
        {
            get
            {
                try
                {
                    if (Configuration != null && Configuration["ToolbarConfigurations"] != null)
                        return Configuration["ToolbarConfigurations"].ToLower().Split(',').ToList();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Configurazione di ckeditor nel web.config mancante!");
                }
                return null;
            }
        }

        public string ToolbarConfigToUse
        {
            get;
            set;
        }

        private string VerifyToolbarConfig()
        {
            if (ToolbarConfig != null && ToolbarConfig.Contains(ToolbarConfigToUse.ToLower()))
                return ToolbarConfigToUse;
            return "default";
        }
                                     
        protected override void FillFieldValue()
        {
            this.RichTextBox.Text = this.DefaultValue.ToString().Replace("''", "'");
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);   
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            WebControl script = new WebControl(HtmlTextWriterTag.Script);
            script.Attributes["type"] = "text/javascript";

            string str_script = "";

            str_script = @"$(document).ready(function() {    
                                CKEDITOR.replace('" + RichTextBox.ID + @"', {
                                    toolbar: '" + VerifyToolbarConfig() + @"',";
            if (FileBrowseURL != null && FileBrowseURL.Length > 0)                        
                str_script += @"filebrowserBrowseUrl: '" + FileBrowseURL + @"',";
            if (ImageBrowseURL != null && ImageBrowseURL.Length > 0)
                str_script += @"filebrowserImageBrowseUrl: '" + ImageBrowseURL + @"',";
            if (FlashBrowseURL != null && FlashBrowseURL.Length > 0)
                str_script += @"filebrowserFlashBrowseUrl: '" + FlashBrowseURL + @"',";
            if (FullPage)
                str_script += @"fullPage : true,";

            str_script += @"    filebrowserWindowWidth : '" + Width + @"',
                                filebrowserWindowHeight : '" + Height + @"'
                            });    
                         });";

            script.Controls.Add(new LiteralControl(str_script));
            this.Controls.Add(script);
        }

        private void initField()
        {
            _RichTextBox = new TextBox();
            RichTextBox.TextMode = TextBoxMode.MultiLine;
            RichTextBox.ID = Key;
            RichTextBox.Attributes["class"] = "form-control";

            RichTextBox.Text = DefaultValue != null ? DefaultValue.ToString() : "";
            
            RichTextBox.ReadOnly = ReadOnly;
        }

        protected override void LocalValidate()
        {
            if (this.Request.IsValidString)
            {
                string value = this.Request.StringValue;

                if (this.Required && string.IsNullOrEmpty(value))
                {
                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
                }
                else
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (this.MinLenght == this.MaxLenght && value.Length > 0 && value.Length != MinLenght)
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("hasNotExactlyLength"), Label, MaxLenght));
                        if (this.MinLenght != this.MaxLenght && value.Length > this.MaxLenght && MaxLenght > 0)
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isLongerThan"), Label, MaxLenght));
                        if (this.MinLenght != this.MaxLenght && value.Length < this.MinLenght && MinLenght > 0)
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isShorterThan"), Label, MinLenght));
                    }
                }
            }
            else if (this.Required)
            {
                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
            }
        }
    }
}
