﻿using System;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace NetService.Utility.ValidatedFields
{
    public class VfTermsPrivacyRadioButton : VfGeneric
    {
        #region Property

        private bool _UsedOnFrontend = false;
        /// <summary>
        /// Vf used or not used on frontend
        /// </summary>       
        /// <remarks>Default Value false</remarks>
        public bool UsedOnFrontend
        {
            get { return _UsedOnFrontend; }
            set { _UsedOnFrontend = value; }
        }


        public static string LabelInformativaPrivacy = "Informativa e consenso";                   //LABELDAUSCIRE
        public static string DomandaAccettazionePrivacy = "Accetti le condizioni di utilizzo? ";

        public override string LocalCssClass
        {
            get
            {
                return "terms-radiobutton-vf";
            }
        }


        public string LabelInformativa
        {
            get { return _LabelInformativa; }
            set { _LabelInformativa = value; }
        }
        private string _LabelInformativa = VfTermsPrivacyRadioButton.LabelInformativaPrivacy;

        public string TestoInformativa
        {
            get;
            set;
        }
                                                                              
        public int Columns
        {
            get
            { return _Columns; }
            set { _Columns = value; }
        }
        private int _Columns = 60;

        public int Rows
        {
            get
            { return _Rows; }
            set { _Rows = value; }
        }
        private int _Rows = 10;

        public string LabelDomandaAccettazione
        {
            get
            { return _LabelDomandaAccettazione; }
            set { _LabelDomandaAccettazione = value; }
        }
        private string _LabelDomandaAccettazione = VfTermsPrivacyRadioButton.DomandaAccettazionePrivacy;

        #endregion
                    

        #region Constructor
        public VfTermsPrivacyRadioButton(string id, string labelResourceKey, bool repeathorizontal = true, bool isForFrontend = false)
            : base(id, labelResourceKey, id)
        {
            
            UsedOnFrontend = isForFrontend;
            
            if (UsedOnFrontend)
            {
                RadioButtonList.RepeatLayout = RepeatLayout.UnorderedList;
                if (repeathorizontal)                
                    RadioButtonList.CssClass = "list-inline radiobuttonlist";                
                else
                    RadioButtonList.CssClass = "list-unstyled radiobuttonlist";
            }
            else
            {

                if (repeathorizontal)
                {
                    RadioButtonList.RepeatLayout = RepeatLayout.Flow;
                    RadioButtonList.RepeatDirection = RepeatDirection.Horizontal;                    
                }
            }
        }


        public VfTermsPrivacyRadioButton(string id, string labelResourceKey, string activeRecordPropertyName, bool repeathorizontal = true, bool isForFrontend = false)
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
            UsedOnFrontend = isForFrontend;
            if (UsedOnFrontend)
            {
                RadioButtonList.RepeatLayout = RepeatLayout.UnorderedList;
                if (repeathorizontal)
                    RadioButtonList.CssClass = "list-inline radiobuttonlist";
                else
                    RadioButtonList.CssClass = "list-unstyled radiobuttonlist";
            }
            else
            {
                if (repeathorizontal)
                {
                    RadioButtonList.RepeatLayout = RepeatLayout.Flow;
                    RadioButtonList.RepeatDirection = RepeatDirection.Horizontal;
                }
            }
        }

        public VfTermsPrivacyRadioButton(string id, string labelResourceKey, string activeRecordPropertyName, Type returnObjectType, bool repeathorizontal = true, bool isForFrontend = false)
            : base(id, labelResourceKey, activeRecordPropertyName, returnObjectType)
        {
            UsedOnFrontend = isForFrontend;
            if (UsedOnFrontend && !UseBootstrapItalia)
            {
                RadioButtonList.RepeatLayout = RepeatLayout.UnorderedList;
                if (repeathorizontal)
                    RadioButtonList.CssClass = "list-inline radiobuttonlist";
                else
                    RadioButtonList.CssClass = "list-unstyled radiobuttonlist";

            }
            else
            {
                if (repeathorizontal)
                {
                    RadioButtonList.RepeatLayout = RepeatLayout.Flow;
                    RadioButtonList.RepeatDirection = RepeatDirection.Horizontal;
                }
            }
        }

        #endregion


        public override WebControl Field
        {
            get { return RadioButtonList; }
        }

        public RadioButtonList RadioButtonList
        {
            get
            {
                if (_RadioButtonList == null)
                {
                    _RadioButtonList = new RadioButtonList();
                    _RadioButtonList.ID = this.ID;
                    //_RadioButtonList.RepeatLayout = RepeatLayout.OrderedList;
                    //_RadioButtonList.RepeatDirection = RepeatDirection.Horizontal;
                }
                return _RadioButtonList;
            }
        }
        private RadioButtonList _RadioButtonList;
                                                            

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.addItem("Si", "Y", false);
            this.addItem("No", "N", true);
        }

        public void addItem(string Label, string Value, bool selected)
        {
            ListItem item = new ListItem(Label, Value);
            item.Selected = selected;
            item.Attributes.Add("class", "form-check form-check-inline radio-inline");
            
            RadioButtonList.Items.Add(item);
        }

        protected override void LocalValidate()
        {
            if (this.Request.IsValidString)
            {
                if (this.Request.StringValue != "Y")
                {
                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isRequiredTermsPrivacy"), Label));
                }
            }
        }

        private bool _AutoPostBack;
        public bool AutoPostBack
        {
            get
            {
                return _AutoPostBack;
            }
            set
            {
                _AutoPostBack = value;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (AutoPostBack)
                RadioButtonList.AutoPostBack = true;

            if (PostBackValue.Length > 0)
            {
                foreach (ListItem item in RadioButtonList.Items)
                {
                    if (string.Compare(item.Value, PostBackValue, true) == 0)
                    {
                        item.Selected = true;
                        RadioButtonList.SelectedIndex = RadioButtonList.Items.IndexOf(item);
                    }
                }
            }
        }

        protected override void FillFieldValue()
        {
            this.RadioButtonList.SelectedValue = this.DefaultValue.ToString();
        }

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set { _SearchComparisonCriteria = value; }
        }
        private RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = RecordsFinder.Finder.ComparisonCriteria.Equals;

        public override object PostbackValueObject
        {
            get
            {                
                return this.Request.OriginalValue;
            }
        }

        protected override void AttachFieldControls()
        {
            this.Field.ID = this.Key;

            //if (UsedOnFrontend)
            //{
            //    WebControl rowLabelText = new WebControl(HtmlTextWriterTag.Div);
            //    rowLabelText.CssClass = "row";

            //    WebControl colLabelText = new WebControl(HtmlTextWriterTag.Div);
            //    colLabelText.CssClass = "col-md-12";
                                                     
            //    Label labelInfo = new Label();
            //    labelInfo.AssociatedControlID = TextBoxInformativa.ID;
            //    labelInfo.Text = LabelInformativa;
            //    colLabelText.Controls.Add(labelInfo);
               
            //    WebControl divTextInformativa = new WebControl(HtmlTextWriterTag.Div);
            //    divTextInformativa.ID = TextBoxInformativa.ID;
               
            //    WebControl parTextInformativa = new WebControl(HtmlTextWriterTag.P);
            //    parTextInformativa.Controls.Add(new LiteralControl(TestoInformativa));

            //    divTextInformativa.Controls.Add(parTextInformativa);
            //    colLabelText.Controls.Add(divTextInformativa);

            //    rowLabelText.Controls.Add(colLabelText);

            //    this.Controls.Add(rowLabelText);

            //    WebControl rowLabelQuest = new WebControl(HtmlTextWriterTag.Div);
            //    rowLabelQuest.CssClass = "row";

            //    WebControl colLabelQuest  = new WebControl(HtmlTextWriterTag.Div);
            //    colLabelQuest.CssClass = "col-md-12";
               
            //    Label label = new Label();
            //    label.AssociatedControlID = this.Key;
            //    label.Text = LabelDomandaAccettazione;
            //    label.CssClass = "control-label ";

            //    colLabelQuest.Controls.Add(label);

            //    colLabelQuest.Controls.Add(Field);

            //    rowLabelQuest.Controls.Add(colLabelQuest);

            //    this.Controls.Add(rowLabelQuest);
            //}
            //else
            //{   
                WebControl div = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
                div.CssClass = "form-group";
                Label labelInfo = new Label();
                labelInfo.AssociatedControlID = TextBoxInformativa.ID;
                labelInfo.Text = LabelInformativa;
                div.Controls.Add(labelInfo);
                div.Controls.Add(TextBoxInformativa);
               
                this.Controls.Add(div);

                WebControl div_2 = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
                div_2.CssClass = "form-group mt-3";
                Label label = new Label();
                label.AssociatedControlID = this.Key;
                label.Text = LabelDomandaAccettazione;
                label.CssClass = "label-privacy-condition";

                div_2.Controls.Add(label);                
                div_2.Controls.Add(Field);

                this.Controls.Add(div_2);
            //}
        }

        public TextBox TextBoxInformativa
        {
            get
            {
                if (_TextBoxInformativa == null)
                {
                    _TextBoxInformativa = new TextBox();
                    _TextBoxInformativa.ID = this.Key + "_info";
                    
                    if (Columns > 0)
                        _TextBoxInformativa.Columns = Columns;
                    else
                        _TextBoxInformativa.Columns = 60;

                    _TextBoxInformativa.TextMode = TextBoxMode.MultiLine;

                    if (Rows > 0)
                        _TextBoxInformativa.Rows = Rows;
                    else
                        _TextBoxInformativa.Rows = 10;

                    _TextBoxInformativa.Text = TestoInformativa;
                    _TextBoxInformativa.ReadOnly = true;
                    _TextBoxInformativa.CssClass = "form-control";
                }
                return _TextBoxInformativa;
            }
        }
        private TextBox _TextBoxInformativa;

     
    }
}