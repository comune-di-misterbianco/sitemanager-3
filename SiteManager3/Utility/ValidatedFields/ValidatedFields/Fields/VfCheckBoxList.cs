﻿using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using System.Collections;
using SharedUtilities;
using System.Reflection;


namespace NetService.Utility.ValidatedFields
{
    public class VfCheckBoxList : VfGeneric
    {

        #region Property

        private bool _UsedOnFrontend = false;
        /// <summary>
        /// Vf used or not used on frontend
        /// </summary>       
        /// <remarks>Default Value false</remarks>
        public bool UsedOnFrontend
        {
            get { return _UsedOnFrontend; }
            set { _UsedOnFrontend = value; }
        }

        private string KeyId
        {
            get;
            set;
        }
                 
        public enum PostBackValueObjectTypes
        {
            ListOfStrings,
            ListOfDictionaries,
            ListOfObjects
        }

        private PostBackValueObjectTypes PostBackValueObjectType
        {
            get;
            set;
        }

        public override string LocalCssClass
        {
            get
            {
                return "checkboxlist-vf";
            }
        }

        private string EntityNameForDictionaries
        {
            get;
            set;
        }
        #endregion

        #region Constructor
        public VfCheckBoxList(string id, string labelResourceKey, bool repeathorizontal, bool isForFrontend = false)
            : base(id, labelResourceKey, id)
        {
            UsedOnFrontend = isForFrontend;
            this.Required = false;

            if (UsedOnFrontend)
            {
                CheckBoxList.RepeatLayout = RepeatLayout.UnorderedList;
                if (repeathorizontal)
                    CheckBoxList.CssClass = "list-inline checkboxlist";
                else
                    CheckBoxList.CssClass = "list-unstyled checkboxlist";
            }
            else
            {
            if (repeathorizontal)
            {
                CheckBoxList.RepeatLayout = RepeatLayout.Flow;
                CheckBoxList.RepeatDirection = RepeatDirection.Horizontal;
            }
            }
        
            PostBackValueObjectType = PostBackValueObjectTypes.ListOfStrings;
        }

        public VfCheckBoxList(string id, string labelResourceKey, bool repeathorizontal, int column, bool isForFrontend = false)
            : base(id, labelResourceKey, id)
        {
            UsedOnFrontend = isForFrontend;
            this.Required = false;

            if (UsedOnFrontend)
            {                        
                CheckBoxList.RepeatLayout = RepeatLayout.Table;
                CheckBoxList.RepeatColumns = column;
                CheckBoxList.CssClass = "table checkboxtable";

                if (repeathorizontal)
                    CheckBoxList.RepeatDirection = RepeatDirection.Horizontal;
            }
            else
            {
            if (repeathorizontal)
            {
                CheckBoxList.RepeatLayout = RepeatLayout.Table;
                if (column == 0)
                    column = 1;
                CheckBoxList.RepeatColumns = column;
                CheckBoxList.RepeatDirection = RepeatDirection.Horizontal;
            }
            }

            PostBackValueObjectType = PostBackValueObjectTypes.ListOfStrings;
        }

        public VfCheckBoxList(string id, string labelResourceKey, bool repeathorizontal, string entityNameForDictionaries, bool isForFrontend = false)
            : base(id, labelResourceKey, id)
        {
            UsedOnFrontend = isForFrontend;
            this.Required = false;

            if (UsedOnFrontend)
            {
                CheckBoxList.RepeatLayout = RepeatLayout.UnorderedList;
                if (repeathorizontal)
                    CheckBoxList.CssClass = "list-inline checkboxlist";
                else
                    CheckBoxList.CssClass = "list-unstyled checkboxlist";
            }
            else
            {
            if (repeathorizontal)
            {
                CheckBoxList.RepeatLayout = RepeatLayout.Flow;
                CheckBoxList.RepeatDirection = RepeatDirection.Horizontal;
            }
            }

            PostBackValueObjectType = PostBackValueObjectTypes.ListOfDictionaries;
            EntityNameForDictionaries = entityNameForDictionaries;
        }
        public VfCheckBoxList(string id, string labelResourceKey, bool repeathorizontal, string entityNameForDictionaries, int column, bool isForFrontend = false)
            : base(id, labelResourceKey, id)
        {
            UsedOnFrontend = isForFrontend;
            this.Required = false;

            if (UsedOnFrontend)
            {
                CheckBoxList.RepeatLayout = RepeatLayout.Table;
                CheckBoxList.RepeatColumns = column;
                CheckBoxList.CssClass = "table checkboxtable";

                if (repeathorizontal)
                    CheckBoxList.RepeatDirection = RepeatDirection.Horizontal;
            }
            else
            {
            if (repeathorizontal)
            {
                CheckBoxList.RepeatLayout = RepeatLayout.Table;
                if (column == 0)
                    column = 1;
                CheckBoxList.RepeatColumns = column;
                CheckBoxList.RepeatDirection = RepeatDirection.Horizontal;
            }
            }

            PostBackValueObjectType = PostBackValueObjectTypes.ListOfDictionaries;
            EntityNameForDictionaries = entityNameForDictionaries;
        }
        
        /// <summary>
        /// Restituisce un controllo VfCheckBoxList e utilizza 
        /// </summary>
        /// <param name="id">Identificativo del controllo</param>
        /// <param name="labelResourceKey">Etichetta del controllo (label mostrata nella form)</param>
        /// <param name="keyId">Inserire la chiave primaria (esempio ID, IdDocument etc ...)</param>
        /// <param name="repeathorizontal">Indica se stampare in orizzontale i checkbox</param>
        public VfCheckBoxList(string id, string labelResourceKey, string keyId, Type returnObjectType, bool repeathorizontal, bool isForFrontend = false)
            : base(id, labelResourceKey, id, returnObjectType)
        {
            UsedOnFrontend = isForFrontend;

            this.Required = false;

            if (UsedOnFrontend)
            {
                CheckBoxList.RepeatLayout = RepeatLayout.UnorderedList;
                if (repeathorizontal)
                    CheckBoxList.CssClass = "list-inline checkboxlist";
                else
                    CheckBoxList.CssClass = "list-unstyled checkboxlist";
            }
            else
            {
            if (repeathorizontal)
            {
                CheckBoxList.RepeatLayout = RepeatLayout.Flow;
                CheckBoxList.RepeatDirection = RepeatDirection.Horizontal;
            }
            }

            PostBackValueObjectType = PostBackValueObjectTypes.ListOfObjects;
            KeyId = keyId;
        }

        public VfCheckBoxList(string id, string labelResourceKey, string keyId, Type returnObjectType, bool repeathorizontal, int column, bool isForFrontend = false)
            : base(id, labelResourceKey, id, returnObjectType)
        {
            UsedOnFrontend = isForFrontend;
            this.Required = false;

            if (UsedOnFrontend)
            {
                CheckBoxList.RepeatLayout = RepeatLayout.Table;
                CheckBoxList.RepeatColumns = column;
                CheckBoxList.CssClass = "table checkboxtable";

                if (repeathorizontal)
                    CheckBoxList.RepeatDirection = RepeatDirection.Horizontal;
            }
            else
            {
            if (repeathorizontal)
            {
                CheckBoxList.RepeatLayout = RepeatLayout.Table;
                if (column == 0)
                    column = 1;
                CheckBoxList.RepeatColumns = column;
                CheckBoxList.RepeatDirection = RepeatDirection.Horizontal;
            }
            }

            PostBackValueObjectType = PostBackValueObjectTypes.ListOfObjects;
            KeyId = keyId;
        }
        #endregion


        public CheckBoxList CheckBoxList
        {
            get
            {
                if (_CheckBoxList == null)
                {
                    _CheckBoxList = new System.Web.UI.WebControls.CheckBoxList();
                    _CheckBoxList.ID = this.ID;
                }
                return _CheckBoxList;
            }
        }
        private CheckBoxList _CheckBoxList;

        public override WebControl Field
        {
            get { return CheckBoxList; }
        }

        public override object PostbackValueObject
        {
            get
            {
                switch (PostBackValueObjectType)
                {
                    case PostBackValueObjectTypes.ListOfStrings:
                        {
                            List<string> selected = new List<string>();
                            for (int i = 0; i < this.CheckBoxList.Items.Count; i++)
                            {
                                NetService.Utility.Common.RequestVariable item = new NetService.Utility.Common.RequestVariable(this.Key + "$" + i.ToString());
                                if (item.IsValidString)
                                    selected.Add(CheckBoxList.Items[i].Value);
                            }
                            return selected;
                        }

                    case PostBackValueObjectTypes.ListOfObjects:
                        {
                            IList selected = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(base.ReturnObjectType));

                            ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.GetSession();

                            var daoType = typeof(GenericDAO.DAO.CriteriaNhibernateDAO<>);
                            Type[] typeArgs = { base.ReturnObjectType };
                            var makeDao = daoType.MakeGenericType(typeArgs);
                            
                            var dao = Activator.CreateInstance(makeDao, session);
                            MethodInfo mi = dao.GetType().GetMethod("GetById", new Type[] { typeof(int) });
                                                       
                            for (int i = 0; i < this.CheckBoxList.Items.Count; i++)
                            {
                                NetService.Utility.Common.RequestVariable item = new NetService.Utility.Common.RequestVariable(this.Key + "$" + i.ToString());
                                if (item.IsValidString)
                                {
                                    object obj = mi.Invoke(dao, new object[] { int.Parse(CheckBoxList.Items[i].Value) });    
                                    selected.Add(obj);
                                }
                            }
                            return selected;

                        }
                    case PostBackValueObjectTypes.ListOfDictionaries:
                        {
                            List<IDictionary> selected = new List<IDictionary>();
                            using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                            using (ITransaction tx = sess.BeginTransaction())
                            {
                                ISession mapSession = sess.GetSession(EntityMode.Map);
                                for (int i = 0; i < this.CheckBoxList.Items.Count; i++)
                                {
                                    NetService.Utility.Common.RequestVariable item = new NetService.Utility.Common.RequestVariable(this.Key + "$" + i.ToString());
                                    if (item.IsValidString)
                                    {
                                        IDictionary selection = (IDictionary)mapSession.Get(EntityNameForDictionaries, int.Parse(CheckBoxList.Items[i].Value));
                                        selected.Add(selection);
                                    }
                                }
                                tx.Commit();
                            }
                            return selected;
                        }
                    default: return null;
                }
            }
        }

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set
            { _SearchComparisonCriteria = value; }
        }
        private RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = RecordsFinder.Finder.ComparisonCriteria.Equals;

        /**
         * Utilizzare questo attributo per settare se almeno un elemento della lista deve essere selezionato.
         * NB: Non settare a true l'attributo Required, altrimenti si ha un comportamento non corretto.
         * */
        public bool AtLeastOneRequired
        {
            get { return _AtLeastOneRequired; }
            set { _AtLeastOneRequired = value; }
        }
        private bool _AtLeastOneRequired;

        protected override void LocalValidate()
        {
            string errors = "";
            bool areAllEmpty = true;
            if (AtLeastOneRequired)
            {
                for (int i = 0; i < this.CheckBoxList.Items.Count; i++)
                {
                    NetService.Utility.Common.RequestVariable item = new NetService.Utility.Common.RequestVariable(this.Key + "$" + i.ToString());
                    if (item.IsValidString)
                        areAllEmpty = false;
                    else
                        areAllEmpty = areAllEmpty && true;
                }
            }

            if (areAllEmpty && AtLeastOneRequired)
            {
                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
            }
        }

        protected override void FillFieldValue()
        {
            switch (PostBackValueObjectType)
            {
                case PostBackValueObjectTypes.ListOfStrings:
                    {
                        List<string> values = (List<string>)this.DefaultValue;
                        foreach (ListItem item in CheckBoxList.Items)
                        {
                            if (values.Contains(item.Value))
                                item.Selected = true;
                        }
                        break;
                    }
                case PostBackValueObjectTypes.ListOfDictionaries:
                    {
                        IList list = (IList)this.DefaultValue;
                        foreach (ListItem item in CheckBoxList.Items)
                        {
                            foreach (Hashtable hash in list)
                            {
                                if (hash["ID"].ToString() == item.Value)
                                    item.Selected = true;
                            }
                        }
                        break;
                    }
                case PostBackValueObjectTypes.ListOfObjects:
                    {                        
                        //object objList = TypeUtilities.CreateGenericList(TypeOfItemObjectType);
                        //MethodInfo mListContains = objList.GetType().GetMethod("Contains");

                        IList values = (IList)this.DefaultValue;
                        
                        foreach (ListItem item in CheckBoxList.Items)
                        {
                            for (int i = 0; i < values.Count; i++)
                            {
                                PropertyInfo propertyID = values[i].GetType().GetProperty(KeyId);
                                string id = propertyID.GetValue(values[i], null).ToString();
                                if (id == item.Value)
                                   item.Selected = true;
                            }
                        }
                        break; 
                    }                    
            }

        }

        public void addItem(string Label, string Value, bool selected, bool enabled = true)
        {
            ListItem item = new ListItem(Label, Value);
            item.Selected = selected;
            item.Enabled = enabled;
            item.Attributes.Add("class", "checkbox-inline");
            CheckBoxList.Items.Add(item);
        }

        private bool _AutoPostBack;
        public bool AutoPostBack
        {
            get
            {
                return _AutoPostBack;
            }
            set
            {
                _AutoPostBack = value;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (AutoPostBack)
                CheckBoxList.AutoPostBack = true;

            switch (PostBackValueObjectType)
            {
                case PostBackValueObjectTypes.ListOfStrings:
                    {
                        List<string> values = (List<string>)this.PostbackValueObject;
                        if (values.Capacity > 0)
                        {

                            foreach (ListItem item in CheckBoxList.Items)
                            {
                                if (values.Contains(item.Value))
                                {
                                    item.Selected = true;
                                }
                            }
                        }
                        break;
                    }
                case PostBackValueObjectTypes.ListOfDictionaries:
                    {
                        List<IDictionary> values = (List<IDictionary>)this.PostbackValueObject;
                        if (values.Count > 0)
                        {
                            using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                            using (ITransaction tx = sess.BeginTransaction())
                            {
                                ISession mapSession = sess.GetSession(EntityMode.Map);

                                foreach (ListItem item in CheckBoxList.Items)
                                {
                                    IDictionary selection = (IDictionary)mapSession.Get(EntityNameForDictionaries, int.Parse(item.Value));

                                    if (values.Contains(selection))
                                    {
                                        item.Selected = true;
                                    }
                                }
                                tx.Commit();
                            }
                        }
                        break;
                    }
            }
        }
    }
}
