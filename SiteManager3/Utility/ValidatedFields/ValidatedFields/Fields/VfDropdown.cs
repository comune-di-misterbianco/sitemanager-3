﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetService.Utility.RecordsFinder;

namespace NetService.Utility.ValidatedFields
{
    public class VfDropDown : VfGeneric
    {
        public override WebControl Field
        {
            get { return DropDownList; }
        }

        public override object PostbackValueObject
        {
            get
            {
                int res;
                bool val;
                if (int.TryParse(this.Request.OriginalValue, out res))
                    return int.Parse(this.Request.OriginalValue);
                if (bool.TryParse(this.Request.OriginalValue, out val))
                    return bool.Parse(this.Request.OriginalValue);
                return this.Request.OriginalValue;
            }
        }

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set
            { _SearchComparisonCriteria = value; }
        }
        private RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = Finder.ComparisonCriteria.Equals;

        public Dictionary<string, string> Options
        {
            get
            {
                if (_Options == null)
                {
                    _Options = new Dictionary<string, string>();
                }
                return _Options;
            }
            set
            {
                _Options = value;
            }
        }
        private Dictionary<string, string> _Options;

        public DropDownList DropDownList
        {
            get
            {
                if (_DropDownList == null)
                {
                    _DropDownList = new DropDownList();
                    _DropDownList.CssClass = "form-control ";
                    _DropDownList.ID = this.ID;
                }
                return _DropDownList;
            }
        }
        private DropDownList _DropDownList;

        public override VfSearchParameter SearchParameter
        {
            get
            {
                if (_SearchParameter == null)
                {
                    _SearchParameter = new VfDropDownListSearchParameter(this, SearchComparisonCriteria);
                }
                return _SearchParameter;
            }
        }
        private VfSearchParameter _SearchParameter;

        public override string LocalCssClass
        {
            get
            {
                return "dropdown-vf";
            }
        }

        public VfDropDown(string key, string labelResourceKey, string activeRecordPropertyName, Type returnObjectType)
            : base(key, labelResourceKey, activeRecordPropertyName, returnObjectType)
        {
        }

        public VfDropDown(string key, string labelResourceKey, string activeRecordPropertyName)
            : base(key, labelResourceKey, activeRecordPropertyName)
        {
        }
        public VfDropDown(string id, string label)
            : base(id, label)
        {
        }

        protected override void FillFieldValue()
        {
            //this.DropDownList.SelectedIndex = (int)this.DefaultValue;
            this.DropDownList.SelectedValue = this.DefaultValue.ToString();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            this.DropDownList.Items.Clear();

            foreach (var entry in this.Options)
            {
                ListItem item = new ListItem(NetService.Localization.LabelsManager.GetLabel(entry.Key), entry.Value);
                this.DropDownList.Items.Add(item);

                if (this.Request.IsValidString && this.Request.StringValue == entry.Value)
                    item.Selected = true;
            }

            bool selectedFound = this.DropDownList.Items.Cast<ListItem>().Any(x => x.Selected);
            if (!selectedFound)
            {
                foreach (ListItem entry in this.DropDownList.Items)
                {
                    string value = (DefaultValue != null) ? DefaultValue.GetType().IsEnum ? ((int)DefaultValue).ToString() : DefaultValue.ToString() : null;

                    //Grazie a questo blocco di codice è possibile gestire il default value di dropdown che stampano enumeratori il cui value non è numerico ma stringa
                    int n;
                    if (DefaultValue != null && DefaultValue.GetType().IsEnum && !int.TryParse(entry.Value, out n))
                    {
                        value = Enum.Parse(DefaultValue.GetType(), DefaultValue.ToString()).ToString();
                    }
                    //Fine blocco

                    if (value != null && value == entry.Value)
                    {
                        entry.Selected = true;
                        break;
                    }
                }
            }
        }

        protected override void LocalValidate()
        {
            if (!this.Request.IsValidString && this.Required)
            {
                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
            }
        }
    }
}
