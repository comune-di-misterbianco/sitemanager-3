﻿using NetService.Utility.ValidatedFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.RecordsFinder;
using System.Web.UI.WebControls;
using System.Web.UI;
using NetService.Utility.Common;

namespace ValidatedFields.ValidatedFields.Fields
{
    public class VfTags : VfGeneric
    {

        public override WebControl Field
        {
            get
            {
                return MainControl;
            }
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }
        public override string LocalCssClass
        {
            get
            {
                return "tag-vf";
            }
        }

        
        public override object PostbackValueObject
        {
            get
            {
                
                return this.Request;
            }
        }
        
        public override Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        protected override void FillFieldValue()
        {
            this.tagBox.Value = this.DefaultValue.ToString();
        }

        protected override void LocalValidate()
        {
        //    if (this.Required)
        //    {
        //        this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
        //    }
        }

        private HiddenField _tagBox;
        public HiddenField tagBox
        {
            get
            {
                if(_tagBox == null)
                {
                    _tagBox = new HiddenField();
                    _tagBox.ID = this.CtrlID + "_tags";
                 
                    if (this.DefaultValue == null)
                    {
                        if (tagList.Count > 0)
                        {
                            foreach (string s in tagList)
                            {
                                _tagBox.Value += s;
                                if (!(tagList.Last() == s))
                                {
                                    _tagBox.Value += ",";
                                }

                            }
                        }
                    }
                    else
                        _tagBox.Value = this.DefaultValue.ToString();

                }
                return _tagBox;
            }
        }

        private ICollection<string> _tagList;
        /// <summary>
        /// Lista dei tag da far visualizzare come DefaultValue.
        /// </summary>
        public ICollection<string> tagList
        {
            get
            {
                if(_tagList == null)
                {
                    _tagList = new List<string>();
                }
                return _tagList;
            }
            set { _tagList = value; }
        }
        public override RequestVariable Request
        {
            get
            {
                var request = new RequestVariable(this.CtrlID + "_tags",RequestVariable.RequestType.Form_QueryString);
                return request;
            }
        }
        private ICollection<string> _AvaibleTags;
        /// <summary>
        /// Lista dei tags disponibili per il suggest
        /// </summary>
        public ICollection<string> AvaibleTags
        {
            get
            {
                if (_AvaibleTags == null)
                {
                    _AvaibleTags = new List<string>();
                }
                return _AvaibleTags;
            }
            set { _AvaibleTags = value; }
        }
        private string _CtrlID;
        public string CtrlID
        {
            get { return _CtrlID; }
            set { _CtrlID = value; }

        }


        private bool _IsPostBack;
     
        public bool IsPostBack
        {
            get { return _IsPostBack; }
            set { _IsPostBack = value; }

        }


        private bool _AllowSpaces;
        /// <summary>
        /// Se true permette l'inserimento dello spazio nei tag
        /// </summary>
        public bool AllowSpaces
        {
            get { return _AllowSpaces; }
            set { _AllowSpaces = value; }

        }

        private bool _AllowNewTags;
        /// <summary>
        /// Se true permette l'inserimento di tags differenti da quelli disponibili (AvaibleTags)
        /// </summary>
        public bool AllowNewTags
        {
            get { return _AllowNewTags; }
            set { _AllowNewTags = value; }

        }

        private bool _CaseSensitive;
        /// <summary>
        /// Se true differenzia i tag con maiuscole da quelli con minuscole
        /// </summary>
        public bool CaseSensitive
        {
            get { return _CaseSensitive; }
            set { _CaseSensitive = value; }

        }
        #region Costruttori
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Id Componente (deve essere univoco)</param>
        /// <param name="label">Label</param>
        /// <param name="addedtaglist">Elenco dei Tag da visualizzare come DafaultValue</param>
        /// <param name="avaibletags">Elenco dei Tag disponibili come suggest per l'utente.</param>
        public VfTags(string id,string label,List<string> addedtaglist = null,List<string> avaibletags = null,bool ispostBack = false) : base(id,label)
        {
            this.BindField = false;
            this.Required = true;
            this.CtrlID = id;
            this.IsPostBack = ispostBack;
            this.CaseSensitive = false;
            if (addedtaglist != null)
            {
                tagList = addedtaglist;
            }
            if (avaibletags != null)
            {
                AvaibleTags = avaibletags;
            }
            if (IsPostBack)
                this.DefaultValue = this.PostbackValueObject;
        }
        public VfTags(string id, string label,string activeRecordPropertyName, List<string> addedtaglist = null, List<string> avaibletags = null, bool ispostBack = false) : base(id, label,activeRecordPropertyName)
        {
            this.BindField = true;
            this.Required = true;
            this.IsPostBack = ispostBack;
            this.CaseSensitive = false;
            this.CtrlID = id;
            if (addedtaglist != null)
            {
                tagList = addedtaglist;
            }
            if (avaibletags != null)
            {
                AvaibleTags = avaibletags;
            }
            if (IsPostBack)
                this.DefaultValue = this.PostbackValueObject;

        }

        #endregion


        public WebControl MainControl
        {
            get
            {
                if (_MainControl == null)
                {
                    _MainControl = new WebControl(HtmlTextWriterTag.Div);
                    //TextBox textBox = new TextBox();
                    //textBox.ID = this.CtrlID + "_tags";
                    WebControl ul = new WebControl(HtmlTextWriterTag.Ul);
                    ul.ID = this.CtrlID + "_list";
                    string[] array = AvaibleTags.ToArray<string>();
                    string headJsImport = @"<script type=""text/javascript"" src=""/scripts/shared/tags/tag-it.js""></script>";
                    //string headJsImport2 = @"<script type=""text/javascript"" src=""/scripts/shared/tags/tagjs.js""></script>";
                    string cssImport = @"<link href = ""/scripts/shared/tags/tagit.ui-zendesk.css"" rel=""stylesheet""/>";
                    string cssImport2 = @"<link href = ""/scripts/shared/tags/jquery.tagit.css"" rel=""stylesheet""/>";
                    

                    
                   
                    
                    string script = @"<script>
                     $(function(){
           
                    var sampleTags = [" + String.Join(",",array.Select(x => string.Format("'{0}'", x.Replace("\'","\\'")))) + @"];
                         
                    $('#"+ this.CtrlID + @"_list').tagit({
                        availableTags: sampleTags,            
                        allowSpaces:" + this.AllowSpaces.ToString().ToLower() +@",
                        singleFieldNode:  $('#" + this.CtrlID + @"_tags'),
                        singleField: true,
                        caseSensitive:" + this.CaseSensitive.ToString().ToLower() + @",
                        beforeTagAdded: function(event, ui) {
                            if ($.inArray(ui.tagLabel, sampleTags) == -1) {
									return " + this.AllowNewTags.ToString().ToLower() + @";
                            }
                        },
                        autocomplete: {
							source: sampleTags,
							minLength: 0,
							select: function (event, ui) {
							itemId = ui.item.id;
                            //$('#" + this.CtrlID + @"_list').tagit(""createTag"", ui.item.value);
                            }
                        }
        });
                   });
                  </script>";
                    base.Page.Header.Controls.Add(new LiteralControl(cssImport2));
                    base.Page.Header.Controls.Add(new LiteralControl(cssImport));
                    base.Page.Header.Controls.Add(new LiteralControl(headJsImport));
                    base.Page.Header.Controls.Add(new LiteralControl(script)); 
                    _MainControl.Controls.Add(tagBox);
                    _MainControl.Controls.Add(ul);
                }
                return _MainControl;
                    
                
              
            }
        }
        private WebControl _MainControl;
    }
}
