﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Collections.Generic;
using System.Web;
using NetService.Utility.Common;
using System.Web.UI.HtmlControls;

namespace NetService.Utility.ValidatedFields
{
    /// <summary>
    /// Deprecato usare VfReCaptcha
    /// </summary>
    public class VfCaptcha : VfGeneric
    {
        public const string CaptchaApplicationIndexerKey = "CaptchaApplicationIndexer";
        public Dictionary<string, string> Keys
        {
            get
            {
                if (_Keys == null)
                {
                    var indexer = HttpContext.Current.Session[VfCaptcha.CaptchaApplicationIndexerKey];
                    if (indexer != null)
                    {
                        _Keys = (Dictionary<string, string>)indexer;
                        if (_Keys.Count > 50)
                        {
                            _Keys.Clear();
                            _Keys = null;
                        }
                    }
                }
                if (_Keys == null)
                {
                    HttpContext.Current.Session[VfCaptcha.CaptchaApplicationIndexerKey] = _Keys = new Dictionary<string, string>();
                }
                return _Keys;
            }
        }
        private Dictionary<string, string> _Keys;

        public InputField KeyHiddenControl
        {
            get
            {
                if (_KeyHiddenControl == null)
                {
                    _KeyHiddenControl = new InputField("k");
                    _KeyHiddenControl.Value = GeneratedKey.ToString();
                    _KeyHiddenControl.Control.Attributes["class"] += " form-control ";
                    _KeyHiddenControl.CssClass = " form-control ";
                }
                return _KeyHiddenControl;
            }
        }
        private InputField _KeyHiddenControl;

        public string GeneratedKey
        {
            get
            {
                if (_GeneratedKey == null)
                {
                    RequestVariable req = new RequestVariable("k", RequestVariable.RequestType.Form);
                    _GeneratedKey = req.IsValidInteger ? req.IntValue.ToString() : this.GetHashCode().ToString();
                }
                return _GeneratedKey;
            }
        }
        private string _GeneratedKey;

        private System.Web.UI.WebControls.Image image = new System.Web.UI.WebControls.Image();

        private bool _IsPostBack;
        private bool IsPostBack
        {
            get
            {
                return _IsPostBack;
            }
        }

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set { _SearchComparisonCriteria = value; }
        }
        private RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = RecordsFinder.Finder.ComparisonCriteria.Equals;

        public override object PostbackValueObject
        {
            get
            {
                return this.Request.OriginalValue;
            }
        }

        public override WebControl Field
        {
            get
            {
                return InputField;
            }
        }

        public TextBox InputField
        {
            get
            {
                if (_InputField == null)
                {
                    _InputField = new TextBox();
                    _InputField.ID = this.Key;
                    _InputField.CssClass = "form-control ";
                }
                return _InputField;
            }
        }
        private TextBox _InputField;

        public override string LocalCssClass
        {
            get
            {
                return "captcha-vf";
            }
        }
        public string CurrentValue { get; private set; }

        public VfCaptcha(string id, string labelResourceKey)
            : base(id, labelResourceKey)
        {
            _IsPostBack = this.Request.IsPostBack;
            BindField = false;
            CurrentValue = this.Keys.ContainsKey(this.GeneratedKey) ? this.Keys[this.GeneratedKey] : null;
            //if (Keys.ContainsKey(GeneratedKey))
            //    Keys.Remove(GeneratedKey.ToString());
            //Keys.Add(GeneratedKey.ToString(), generaPw(5));
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (Keys.ContainsKey(GeneratedKey))
                Keys.Remove(GeneratedKey.ToString());
            Keys.Add(GeneratedKey.ToString(), generaPw(5));
        }



        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            var div = this;

            //WebControl row = new WebControl(HtmlTextWriterTag.Div);
            //row.CssClass = "row";

            WebControl colmsg = new WebControl(HtmlTextWriterTag.Div);
            colmsg.CssClass = "col-md-12";
                       
            WebControl msg = new WebControl(HtmlTextWriterTag.P);
            msg.Controls.Add(new LiteralControl("Digita i caratteri visualizzati nell'immagine.")); //LABELDAUSCIRE

            colmsg.Controls.Add(msg);
            div.Controls.Add(colmsg);

            //string key = !IsPostBack ? genimgkey() : this.Request.StringValue;

            image.ImageUrl = "image.cap?k=" + GeneratedKey.ToString();
            image.Attributes["alt"] = "*";

            WebControl divImageCap = new WebControl(HtmlTextWriterTag.Div);
            divImageCap.Attributes["class"] = "col-xs-12 col-md-3 imageCap";
            divImageCap.Controls.Add(image);

            divImageCap.Controls.Add(KeyHiddenControl.Control);

            div.Controls.Add(divImageCap);

            //div.Controls.Add(row);
        }

        protected override void LocalValidate()
        {
            if (this.Request.IsValidString)
            {
                if (Required && this.Request.StringValue == "")
                {
                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
                }
                else
                {
                    //string code = (String)System.Web.HttpRuntime.Cache.Get("key");
                    string typed_code = this.Request.StringValue;

                    if (CurrentValue.ToLower() != typed_code.ToLower())
                        this.AddError("Il Codice di controllo digitato non corrisponde a quello riprodotto nell'immagine.");    //LABELDAUSCIRE
                }
            }
        }      

        protected override void FillFieldValue()
        {
            //this.InputField.Value = this.DefaultValue.ToString();
        }
        private string generaPw(int maxlen)
        {
            string strNewPass = null;
            int whatsNext;
            int upper;
            int lower;

            Random rnd = new Random();

            for (int intCounter = 1; intCounter <= maxlen; intCounter++)
            {
                whatsNext = rnd.Next(2);

                if (whatsNext == 0)
                {
                    upper = 90;
                    lower = 65;
                }
                else
                {
                    upper = 57;
                    lower = 49;
                }

                int k = 0;

                do
                {
                    k = (int)((upper - lower + 1) * rnd.NextDouble() + lower);
                }
                while (k == 79 || k == 73);

                string strA = new string((Char)k, 1);

                strNewPass = strNewPass + strA;

            }

            return strNewPass;
        }

        private string genimgkey()
        {

            string key = generaPw(5);
            //HttpRuntime.Cache.Insert(this.FieldName, key);
            HttpRuntime.Cache.Insert("key", key);
            return key;
        }
    }
}

