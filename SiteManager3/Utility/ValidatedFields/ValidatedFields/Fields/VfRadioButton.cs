﻿using System;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web.UI;

namespace NetService.Utility.ValidatedFields
{
    public class VfRadioButton : VfGeneric
    {

        #region Property

        private bool _UsedOnFrontend = false;
        /// <summary>
        /// Vf used or not used on frontend
        /// </summary>       
        /// <remarks>Default Value false</remarks>
        public bool UsedOnFrontend
        {
            get { return _UsedOnFrontend; }
            set { _UsedOnFrontend = value; }
        }


        public override string LocalCssClass
        {
            get
            {
                return "radiobutton-vf";
            }
        }
        #endregion


        #region Constructor

        public VfRadioButton(string id, string labelResourceKey, bool repeathorizontal, bool isForFrontend = false)
          : base(id, labelResourceKey, id)
        {

            UsedOnFrontend = isForFrontend;

            if (UsedOnFrontend)
            {
                RadioButtonList.RepeatLayout = RepeatLayout.UnorderedList;
                if (repeathorizontal)
                    RadioButtonList.CssClass = "list-inline radiobuttonlist";
                else
                    RadioButtonList.CssClass = "list-unstyled radiobuttonlist";
            }
            else
            {
                if (repeathorizontal)
                {
                    RadioButtonList.RepeatLayout = RepeatLayout.Flow;
                    RadioButtonList.RepeatDirection = RepeatDirection.Horizontal;
                }
            }
        }

        public VfRadioButton(string id, string labelResourceKey, string activeRecordPropertyName, bool repeathorizontal, bool isForFrontend = false)
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
            UsedOnFrontend = isForFrontend;

            if (UsedOnFrontend)
            {
                RadioButtonList.RepeatLayout = RepeatLayout.UnorderedList;
                if (repeathorizontal)
                    RadioButtonList.CssClass = "list-inline radiobuttonlist";
                else
                    RadioButtonList.CssClass = "list-unstyled radiobuttonlist";
            }
            else
            {
                if (repeathorizontal)
                {
                    RadioButtonList.RepeatLayout = RepeatLayout.Flow;
                    RadioButtonList.RepeatDirection = RepeatDirection.Horizontal;
                }
            }
        }

        public VfRadioButton(string id, string labelResourceKey, string activeRecordPropertyName, bool repeathorizontal, Type returnObjectType, bool isForFrontend = false )
            : base(id, labelResourceKey, activeRecordPropertyName, returnObjectType)
        {
            UsedOnFrontend = isForFrontend;
        
            if (UsedOnFrontend)
            {
                RadioButtonList.RepeatLayout = RepeatLayout.UnorderedList;
                if (repeathorizontal)
                    RadioButtonList.CssClass = "list-inline radiobuttonlist";
                else
                    RadioButtonList.CssClass = "list-unstyled radiobuttonlist";
            }
            else
            {
                if (repeathorizontal)
                {
                    RadioButtonList.RepeatLayout = RepeatLayout.Flow;
                    RadioButtonList.RepeatDirection = RepeatDirection.Horizontal;
                }
            }
        }

        public VfRadioButton(string id, string labelResourceKey, bool repeathorizontal, int column, bool isForFrontend = false)
            : base(id, labelResourceKey, id)
        {
            UsedOnFrontend = isForFrontend;

            if (UsedOnFrontend)
            {
                RadioButtonList.RepeatLayout = RepeatLayout.Table;
                RadioButtonList.RepeatColumns = column;
                RadioButtonList.CssClass = "table radiobuttontable"; 

                if (repeathorizontal)
                    RadioButtonList.RepeatDirection = RepeatDirection.Horizontal;
            }
            else
            {
                if (repeathorizontal)
                {
                    RadioButtonList.RepeatLayout = RepeatLayout.Table;
                    if (column == 0)
                        column = 1;
                    RadioButtonList.RepeatColumns = column;
                    RadioButtonList.RepeatDirection = RepeatDirection.Horizontal;
                }
            }
        }

        public VfRadioButton(string id, string labelResourceKey, string activeRecordPropertyName, bool repeathorizontal, int column, bool isForFrontend = false)
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
            UsedOnFrontend = isForFrontend;
            if (UsedOnFrontend)
            {
                RadioButtonList.RepeatLayout = RepeatLayout.Table;
                RadioButtonList.RepeatColumns = column;
                RadioButtonList.CssClass = "table radiobuttontable";

                if (repeathorizontal)
                    RadioButtonList.RepeatDirection = RepeatDirection.Horizontal;
            }
            else
            {
                if (repeathorizontal)
                {
                    RadioButtonList.RepeatLayout = RepeatLayout.Table;
                    if (column == 0)
                        column = 1;
                    RadioButtonList.RepeatColumns = column;
                    RadioButtonList.RepeatDirection = RepeatDirection.Horizontal;
                }
            }
        }

        public VfRadioButton(string id, string labelResourceKey, string activeRecordPropertyName, bool repeathorizontal, Type returnObjectType, int column, bool isForFrontend = false)
            : base(id, labelResourceKey, activeRecordPropertyName, returnObjectType)
        {

            UsedOnFrontend = isForFrontend;

            if (UsedOnFrontend)
            {


                

                if (repeathorizontal)
                {
                    RadioButtonList.RepeatDirection = RepeatDirection.Horizontal;
                    RadioButtonList.RepeatLayout = RepeatLayout.Table;
                    RadioButtonList.RepeatColumns = column;
                    RadioButtonList.CssClass = "table radiobuttontable";
                }
                else
                {
                    RadioButtonList.RepeatLayout = RepeatLayout.UnorderedList;
                    RadioButtonList.CssClass = "list-unstyled radiobuttonlist";
                }
            }
            else
            {
                if (repeathorizontal)
                {
                    RadioButtonList.RepeatLayout = RepeatLayout.Table;
                    if (column == 0)
                        column = 1;
                    RadioButtonList.RepeatColumns = column;
                    RadioButtonList.RepeatDirection = RepeatDirection.Horizontal;
                }
            }
        }

        #endregion

       
        public override WebControl Field
        {
            get { return RadioButtonList; }
        }

        public RadioButtonList RadioButtonList
        {
            get
            {
                if (_RadioButtonList == null)
                {
                    _RadioButtonList = new RadioButtonList();
                    _RadioButtonList.ID = this.ID;
                }
                return _RadioButtonList;
            }
        }
        private RadioButtonList _RadioButtonList;

     

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (FieldDescription != null)
             this.Controls.Add(FieldDescription);
        }

        public WebControl FieldDescription
        {
            get;
            set;
        }

        public void addItem(string Label, string Value, bool selected, string CssClass = "")
        {
            ListItem item = new ListItem(Label, Value);
            item.Selected = selected;
            item.Attributes.Add("class", (string.IsNullOrEmpty(CssClass) ? "radio-inline" : CssClass));

            RadioButtonList.Items.Add(item);

        }

        protected override void LocalValidate()
        {
            if (this.Request.IsValidString)
            {
                if (Required && this.Request.StringValue == "")
                {
                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
                }
            }
        }

        private bool _AutoPostBack;
        public bool AutoPostBack
        {
            get
            {
                return _AutoPostBack;
            }
            set
            {
                _AutoPostBack = value;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (AutoPostBack)
                RadioButtonList.AutoPostBack = true;

            if (PostBackValue.Length > 0)
            {
                foreach (ListItem item in RadioButtonList.Items)
                {
                    if (string.Compare(item.Value, PostBackValue, true) == 0)
                    {
                        item.Selected = true;
                        RadioButtonList.SelectedIndex = RadioButtonList.Items.IndexOf(item);
                    }
                }
            }
        }

        protected override void FillFieldValue()
        {
            this.RadioButtonList.SelectedValue = this.DefaultValue.ToString();
        }

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set { _SearchComparisonCriteria = value; }
        }
        private RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = RecordsFinder.Finder.ComparisonCriteria.Equals;

        public override object PostbackValueObject
        {
            get
            {                
                return this.Request.OriginalValue;
            }
        }
    }
}

