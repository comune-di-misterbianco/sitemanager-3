﻿using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace NetService.Utility.ValidatedFields
{
    public class VfPassword: VfGeneric
    {
        public override WebControl Field
        {
            get { return Password; }
        }

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { throw new System.NotImplementedException("Non è possibile utilizzare un VfPassword per le ricerche"); }
            set { }
        }
        public override object PostbackValueObject
        {
            get 
            {
                return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(this.Request.DatabaseStringValue, "MD5");
            }
        }

        public TextBox Password
        {
            get
            {
                if (_Password == null)
                {
                    _Password = new TextBox();
                    _Password.CssClass = "form-control ";
                    _Password.ID = this.Key;
                    _Password.TextMode = TextBoxMode.Password;
                }
                return _Password;
            }
        }
        private TextBox _Password;

        public TextBox Confirm
        {
            get
            {
                if (_Confirm == null)
                {
                    _Confirm = new TextBox();
                    _Confirm.CssClass = "form-control ";
                    _Confirm.ID = this.Key + "_confirm";
                    _Confirm.TextMode = TextBoxMode.Password;
                }
                return _Confirm;
            }
        }
        private TextBox _Confirm;


        public string SpanToggleBtnPassword
        {
            get
            {
                return @"<span toggle=""#" + Key + @""" class=""fa fa-fw fa-eye field-icon toggle-password""></span>";
            }
        }
        public string SpanToggleBtnConfirmPW 
        { 
            get { 
                return @"<span toggle=""#" + Key + "_confirm" + @""" class=""fa fa-fw fa-eye field-icon toggle-password""></span>"; 
            } 
        }

        public string ConfirmLabel
        {
            get
            {
                return _ConfirmLabel;
            }
            private set
            {
                _ConfirmLabel = NetService.Localization.LabelsManager.GetLabel(value);
            }
        }
        private string _ConfirmLabel; 

        public NetService.Utility.Common.RequestVariable ConfirmRequest
        {
            get
            {
                if (_ConfirmRequest == null)
                {
                    _ConfirmRequest = new NetService.Utility.Common.RequestVariable(Confirm.ID, NetService.Utility.Common.RequestVariable.RequestType.Form);
                }
                return _ConfirmRequest;
            }
        }
        private NetService.Utility.Common.RequestVariable _ConfirmRequest;

        public int MaxLenght
        {
            get
            {
                return _MaxLenght;
            }
            set
            {
                _MaxLenght = value;
            }
        }
        private int _MaxLenght = 255;

        public int MinLenght
        {
            get
            {
                return _MinLenght;
            }
            set
            {
                _MinLenght = value;
            }
        }
        private int _MinLenght = 8;

        public override string LocalCssClass
        {
            get
            {
                return "password-vf";
            }
        }

        public VfPassword(string id)
            : base(id, "Password")
        {        
            this.Required = true;
            this.Type = FieldType.Default;
            this.ConfirmLabel = "passwordconfirm";                    
            this.InfoControl.Controls.Add(new LiteralControl("La password deve essere di almeno " + MinLenght + " caratteri e deve contenere almeno 3 di questi elementi: Lettera Maiuscola, Lettera Minuscola, Numero, Simbolo"));
        }
        public VfPassword(string id, bool showInfocontrol)
            : base(id, "Password")
        {           
            this.Required = true;
            this.Type = FieldType.Default;
            this.ConfirmLabel = "passwordconfirm";

            if (showInfocontrol)
                this.InfoControl.Controls.Add(new LiteralControl("La password deve essere di almeno " + MinLenght + " caratteri e deve contenere almeno 3 di questi elementi: Lettera Maiuscola, Lettera Minuscola, Numero, Simbolo"));
        }
        
        ///// <summary>
        ///// Mostra il controllo "InfoControl" prima del campo password: default = true;
        ///// </summary>
        //public bool ShowInfocontrol
        //{
        //    get { return this._ShowInfocontrol; }
        //    set { this._ShowInfocontrol = value; }
        //}
        //private bool _ShowInfocontrol = true;

        protected override void AttachFieldControls()
        {
            Label label = new Label();

            if (this.Type == FieldType.NoConfirmField)
            {                
                label.CssClass = "control-label";
                label.AssociatedControlID = this.Password.ID;
                label.Text = Label + (this.Required ? "*" : "");
                this.Controls.Add(label);
                this.Controls.Add(Password);
                this.Controls.Add(new LiteralControl(SpanToggleBtnPassword));
            }            
            else //(this.Type == FieldType.Default)
            {
                WebControl div = new WebControl(HtmlTextWriterTag.Div);
                div.CssClass = "form-group";

                label.CssClass = "control-label";
                label.AssociatedControlID = this.Password.ID;
                label.Text = Label + (this.Required ? "*" : "");
                div.Controls.Add(label);
                div.Controls.Add(Password);
                div.Controls.Add(new LiteralControl(SpanToggleBtnPassword));

                this.Controls.Add(div);

                WebControl div2 = new WebControl(HtmlTextWriterTag.Div);
                div2.CssClass = "form-group";            

                label = new Label();
                label.CssClass = "control-label";
                label.AssociatedControlID = this.Confirm.ID;
                label.Text = ConfirmLabel + (this.Required ? "*" : "");
                //this.Controls.Add(new LiteralControl("<br/>"));
                div2.Controls.Add(label);
                div2.Controls.Add(Confirm);
                div2.Controls.Add(new LiteralControl(SpanToggleBtnConfirmPW));
                this.Controls.Add(div2);
            }

        }
 
        protected override void LocalValidate()
        {
            //Controllo inutile, in quanto viene già effettuato nell'oggetto padre (VfGeneric)
            //if (!this.Request.IsValidString && this.Required)
            //    this.AddError(string.Format(Localization.LabelsManager.GetLabel("isrequired"), Label));


            if (this.Required && this.Type == FieldType.Default && (string.IsNullOrEmpty(this.ConfirmRequest.StringValue) || !this.ConfirmRequest.IsValidString))
                this.AddError(string.Format(Localization.LabelsManager.GetLabel("isrequired"), ConfirmLabel));

            if (this.Type == FieldType.Default)
            {

                if (this.Request.IsValidString && this.ConfirmRequest.IsValidString)
                {
                    string value = this.Request.StringValue;
                    string cvalue = this.ConfirmRequest.StringValue;

                    if (!string.IsNullOrEmpty(value) && !string.IsNullOrEmpty(cvalue))
                    {
                        if (value.Length < this.MinLenght && MinLenght > 0)
                            this.AddError(string.Format(Localization.LabelsManager.GetLabel("isshorterthancharacters"), Label, MinLenght));

                        if (cvalue.Length < this.MinLenght && MinLenght > 0 && this.Type == FieldType.Default)
                            this.AddError(string.Format(Localization.LabelsManager.GetLabel("isshorterthancharacters"), ConfirmLabel, MinLenght));

                        if (cvalue != value)
                            this.AddError(string.Format(Localization.LabelsManager.GetLabel("passwordnotmatchconfirm"), Label, ConfirmLabel));

                        if (cvalue == value)
                        {
                            Regex regex = new Regex(@"(?=^.{6,255}$)((?=.*\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*", RegexOptions.IgnorePatternWhitespace);
                            if (!regex.IsMatch(cvalue))
                                this.AddError(string.Format(Localization.LabelsManager.GetLabel("passwordnotvalid"), Label));
                        }
                    }
                }
            }
            else 
            {
                if (this.Request.IsValidString)
                {
                    string value = this.Request.StringValue;
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (value.Length < this.MinLenght && MinLenght > 0)
                            this.AddError(string.Format(Localization.LabelsManager.GetLabel("isshorterthancharacters"), Label, MinLenght));
                       
                        Regex regex = new Regex(@"(?=^.{6,255}$)((?=.*\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*", RegexOptions.IgnorePatternWhitespace);
                        if (!regex.IsMatch(value))
                            this.AddError(string.Format(Localization.LabelsManager.GetLabel("passwordnotvalid"), Label));
                    }
                }
            }
        }

        protected override void FillFieldValue()
        {
            
        }

        private FieldType _Type;
        public FieldType Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        
        public enum FieldType
        {
            Default,
            NoConfirmField
        }
    }
}
