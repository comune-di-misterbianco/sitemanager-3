﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using NetService.Utilities;

namespace NetService.Utility.ValidatedFields
{
    public class VfCodFiscalePartIVA : VfGeneric
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public DateTime DataNascita { get; set; }
        public bool? IsMale { get; set; }
        public string CodiceCatastaleComune { get; set; }

        /// <summary>
        /// Se impostato a true essettua la validazione verificando la congruenza del codice inserito coi dati delle proprietà:
        /// public string Nome { get; set; }
        /// public string Cognome { get; set; }
        /// public DateTime DataNascita { get; set; }
        /// public bool IsMale { get; set; }
        /// public string CodiceCatastaleComune { get; set; }
        /// 
        /// Se impostato a false viene fatta una validazione solo sul formato.
        /// </summary>
        public bool ValidateAgaistData { get; set; }

        public override WebControl Field
        {
            get { return TextBox; }
        }

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria;}
            set { _SearchComparisonCriteria = value; }
        }
        private RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = RecordsFinder.Finder.ComparisonCriteria.Like;

        public override object PostbackValueObject
        {
            get
            {   
                return this.Request.DatabaseStringValue;
            }
        }

        public TextBox TextBox
        {
            get
            {
                if (_TextBox == null)
                {
                    _TextBox = new TextBox();
                    _TextBox.CssClass = "form-control ";
                    _TextBox.ID = this.ID;

                    if (this.Request.IsValidString)
                    {
                        _TextBox.Text = this.Request.StringValue.Replace("''", "'");
                    }
                }
                return _TextBox;
            }
        }
        private TextBox _TextBox;

       
        public override string LocalCssClass
        {
            get
            {
                return "textbox-vf codicefiscale-vf partitaiva-vf";
            }
        }

        public VfCodFiscalePartIVA(string id, string labelResourceKey, string activeRecordPropertyName)
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
        }

        public VfCodFiscalePartIVA(string id, string labelResourceKey)
            : base(id, labelResourceKey, id)
        {
        }

        protected override void FillFieldValue()
        {
            this.TextBox.Text = this.DefaultValue.ToString();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }

        protected override void LocalValidate()
        {
            if (this.Request.IsValidString)
            {
                string value = this.Request.StringValue;

                if (this.Required && string.IsNullOrEmpty(value))
                {
                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
                }
                else
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (value.Length != 16 && value.Length != 11)
                            this.AddError(string.Format("la lunghezza del campo '{0}' deve essere 11 caratteri per la partita IVA o 16 per il codice fiscale", this.Label));  //LABELDAUSCIRE

                        if (value.Length == 16)
                        {
                            if (!NetService.Utility.Common.FormatValidator.ValidateCodiceFiscale(value))
                                this.AddError(NetService.Localization.LabelsManager.GetLabel("isNotValidCodiceFiscale"));
                        }

                        if (value.Length == 11)
                        {
                            if (!NetService.Utility.Common.FormatValidator.ValidatePartitaIVA(value))
                                this.AddError(NetService.Localization.LabelsManager.GetLabel("isNotValidPartitaIVA"));
                        }
                    }
                }
            }
            else if (this.Required)
            {
                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
            }
        }

        
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

    }
}
