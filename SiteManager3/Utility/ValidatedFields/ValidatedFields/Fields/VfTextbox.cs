﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Globalization;
using NetService.Utility.RecordsFinder;

namespace NetService.Utility.ValidatedFields
{
    public enum InputTypes
    {
        [Description("Testo Semplice")]
        Text,
        [Description("Testo Semplice per nome e cognome")]
        TextForName,
        [Description("Testo Semplice senza spazi")]
        TextWithoutSpace,
        [Description("Testo, numeri e spazi senza caratteri speciali")]
        TextWithoutSpecialCharacter,
        Email,
        [Description("Numerico Intero")]
        Number,
        [Description("Numerico Intero non Negativo senza lo 0")]
        PositiveNumber,
        [Description("Numerico Intero non Negativo compreso lo 0")]
        NotNegativeNumber,
        [Description("Numerico Decimale negativo, positivo o nullo")]
        Decimal,
        [Description("Numerico Decimale non Negativo compreso lo 0")]
        NotNegativeDecimal,
        [Description("Numerico Decimale non Negativo")]
        Floating,
        [Description("Numerico in virgola mobile con precisione singola")]
        SingleFloating,
        //Double
        [Description("Codice Fiscale")]
        CodiceFiscale,
        //Blob,
        [Description("Telefono Fisso")]
        Phone,
        [Description("Valuta (decimal)")]
        Valuta,
        [Description("CAP")]
        Cap,
        [Description("Partita IVA")]
        PartitaIva,
        [Description("Codice Fiscale o Partita IVA")]
        CodiceFiscaleOPartitaIva,
        [Description("Codice Ateco")]
        CodiceAteco,
        [Description("Codice IBAN")]
        IBAN,
        [Description("CIN")]
        CIN,
        [Description("ABI")]
        ABI,
        [Description("CAB")]
        CAB,
        [Description("Absolute URI")]
        AbsoluteURI,
        [Description("CustomUserID")]
        CustomUserID,
        [Description("Absolute or relative URI")]
        AbsoluteOrRelativeURI,
        [Description("Double")]
        Double
    }

    public class Description : Attribute
    {
        public string Text;

        public Description(string text)
        {
            Text = text;
        }
    }

    public class VfTextBox : VfGeneric
    {
        public override VfSearchParameter SearchParameter
        {
            get
            {
                if (_SearchParameter == null)
                {
                    _SearchParameter = new VfTextBoxSearchParameter(this, SearchComparisonCriteria);
                }
                return _SearchParameter;
            }
        }
        private VfSearchParameter _SearchParameter;

        public override WebControl Field
        {
            get { return TextBox; }
        }

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set { _SearchComparisonCriteria = value; }
        }
        private RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = RecordsFinder.Finder.ComparisonCriteria.Like;

        public override object PostbackValueObject
        {
            get
            {
                switch (this.InputType)
                {
                    //case InputTypes.Double: return double.Parse(this.Request.OriginalValue);
                    case InputTypes.Floating: return float.Parse(this.Request.OriginalValue);
                    case InputTypes.SingleFloating: return float.Parse(this.Request.OriginalValue);
                    case InputTypes.Number: return int.Parse(this.Request.OriginalValue);
                    case InputTypes.Valuta:
                        {
                            if (string.IsNullOrEmpty(this.Request.OriginalValue))
                                return 0;

                            return decimal.Parse(this.Request.OriginalValue);
                        }
                    case InputTypes.Decimal: return decimal.Parse(this.Request.OriginalValue);
                    case InputTypes.NotNegativeNumber:
                        {
                            if (string.IsNullOrEmpty(this.Request.OriginalValue))
                                return 0;

                            return int.Parse(this.Request.OriginalValue);
                        }
                    case InputTypes.AbsoluteOrRelativeURI:
                        return this.Request.OriginalValue;
                    case InputTypes.Double:
                        return double.Parse(this.Request.OriginalValue);

                    default: return this.Request.OriginalValue.Trim(); //this.Request.DatabaseStringValue;
                }
            }
        }

        public InputTypes InputType
        {
            get
            {
                return _InputType;
            }
            set
            {
                _InputType = value;
            }
        }
        private InputTypes _InputType;

        public bool ShowLabelAsHint
        {
            get
            {
                return _ShowLabelAsHint;
            }
            set
            {
                _ShowLabelAsHint = value;
            }
        }
        private bool _ShowLabelAsHint;

        public TextBox TextBox
        {
            get
            {
                if (_TextBox == null)
                {
                    _TextBox = new TextBox();

                    _TextBox.CssClass = "form-control ";

                    if (Columns > 0)
                        _TextBox.Columns = Columns;

                    _TextBox.TextMode = TextMode;

                    if (TextMode == TextBoxMode.MultiLine)
                    {                        
                        if (Rows > 0)
                            _TextBox.Rows = Rows;
                    }

                    if (ShowLabelAsPlaceholder)
                        _TextBox.Attributes["placeholder"] = this.Label;

                    //if (this.InputType == InputTypes.Valuta || this.InputType == InputTypes.Number || this.InputType == InputTypes.PositiveNumber || this.InputType == InputTypes.Floating || this.InputType == InputTypes.SingleFloating)
                    //    _TextBox.Attributes["type"] = "number";
                    //if (this.InputType == InputTypes.Phone)
                    //    _TextBox.Attributes["type"] = "tel";

                    if (this.Request.IsValidString)
                    {
                        _TextBox.Text = this.Request.StringValue.Replace("''", "'");
                    }
                }
                return _TextBox;
            }
        }
        private TextBox _TextBox;

        public TextBoxMode TextMode
        {
            get { return _TextMode; }
            set { _TextMode = value; }
        }
        private TextBoxMode _TextMode = TextBoxMode.SingleLine;

        public int Columns
        {
            get
            { return _Columns; }
            set { _Columns = value; }
        }
        private int _Columns = -1;

        public int Rows
        {
            get
            { return _Rows; }
            set { _Rows = value; }
        }
        private int _Rows = -1;

        public int MaxLenght
        {
            get
            {
                return _MaxLenght;
            }
            set
            {
                _MaxLenght = value;
            }
        }
        private int _MaxLenght = 255;

        public int MinLenght
        {
            get
            {
                return _MinLenght;
            }
            set
            {
                _MinLenght = value;
            }
        }
        private int _MinLenght = 0;

        public override string LocalCssClass
        {
            get
            {
                return "textbox-vf";
            }
        }

        public VfTextBox(string id, string labelResourceKey, string activeRecordPropertyName, InputTypes inputType)
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
            this.InputType = inputType;

            this.ConfirmLabel = "Conferma " + Label;
        }

        public VfTextBox(string id, string labelResourceKey, string activeRecordPropertyName)
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
            this.InputType = InputTypes.Text;
            this.ConfirmLabel = "Conferma " + Label;
        }

        public VfTextBox(string id, string labelResourceKey, InputTypes inputType)
            : base(id, labelResourceKey, id)
        {
            this.InputType = inputType;
            this.ConfirmLabel = "Conferma " + Label;
        }

        public VfTextBox(string id, string labelResourceKey)
            : base(id, labelResourceKey, id)
        {
            this.InputType = InputTypes.Text;
            this.ConfirmLabel = "Conferma " + Label;
        }

        protected override void FillFieldValue()
        {
            if (this.InputType == InputTypes.Valuta && !string.IsNullOrEmpty(this.DefaultValue.ToString()))
            {
                decimal value = decimal.Parse(this.DefaultValue.ToString());
                this.TextBox.Text = String.Format("{0:0.00}", value).Replace("''", "'");
            }
            else
                this.TextBox.Text = this.DefaultValue.ToString().Replace("''", "'");
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }

        protected override void LocalValidate()
        {
            //COMMENTATO PERCHE' APPARIVA DUE VOLTE IL MESSAGGIO "CODICE FISCALE NON VALIDO" IN QUANTO EFFETTUA LO STESSO CONTROLLO ANCHE SUCCESSIVAMENTE
            //switch (InputType)
            //{
            //    case InputTypes.CodiceFiscale:
            //        this.Validators.Add(new CodiceFiscaleValidator(NetService.Localization.LabelsManager.GetLabel("isNotValidCodiceFiscale")));
            //        break;
            //}

            if (this.Request.IsValidString)
            {
                string value = this.Request.StringValue;

                if (this.Required && string.IsNullOrEmpty(value))
                {
                    this.AddError(string.Format(Localization.LabelsManager.GetLabel("isrequired"), Label));
                }
                else
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (/* this.InputType == InputTypes.Text &&*/ this.MinLenght == this.MaxLenght && value.Length > 0 && value.Length != MinLenght)
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("hasNotExactlyLength"), Label, MaxLenght));
                        if (/* this.InputType == InputTypes.Text &&*/ this.MinLenght != this.MaxLenght && value.Length > this.MaxLenght && MaxLenght > 0)
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isLongerThan"), Label, MaxLenght));
                        if (/*this.InputType == InputTypes.Text &&*/ this.MinLenght != this.MaxLenght && value.Length < this.MinLenght && MinLenght > 0)
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isShorterThan"), Label, MinLenght));

                        if (this.InputType == InputTypes.Email && !NetService.Utility.Common.FormatValidator.ValidateEmail(value))
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidEmail"), Label));

                        int temp = 0;
                        if (this.InputType == InputTypes.Number && !int.TryParse(value, out temp))
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidNumber"), Label));

                        if ((this.InputType == InputTypes.PositiveNumber && !int.TryParse(value, out temp)) || (this.InputType == InputTypes.PositiveNumber && int.TryParse(value, out temp) && temp <= 0))
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidNumber"), Label));

                        if ((this.InputType == InputTypes.NotNegativeNumber && !int.TryParse(value, out temp)) || (this.InputType == InputTypes.NotNegativeNumber && int.TryParse(value, out temp) && temp < 0))
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidNumber"), Label));
                        else if ((this.InputType == InputTypes.NotNegativeNumber && temp >0))
                        {
                            Regex valore = new Regex(@"^\d+$");//^(\+)?permette l'inserimento solo di valute positive senza mettere il segno + davanti
                            if (!valore.IsMatch(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidNumber"), Label));
                        }

                        decimal tempDecimal = 0;
                        if (this.InputType == InputTypes.Decimal && !decimal.TryParse(value, out tempDecimal))
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidDecimal"), Label));

                        tempDecimal = 0;
                        if (this.InputType == InputTypes.NotNegativeDecimal && !decimal.TryParse(value, out tempDecimal) || (this.InputType == InputTypes.NotNegativeDecimal && value.Contains(".")) || (this.InputType == InputTypes.NotNegativeDecimal && decimal.TryParse(value, out tempDecimal) && tempDecimal < 0))
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidFloating"), Label));

                        float tempFloat = 0;
                        if (this.InputType == InputTypes.Floating && (!float.TryParse(value, out tempFloat) || value.Contains(".")))
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidFloating"), Label));

                        float tempSingleFloat = 0;
                        if (this.InputType == InputTypes.SingleFloating && (!Single.TryParse(value, out tempSingleFloat) || value.Contains(".")))
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidFloating"), Label));

                        double tempDouble = 0;
                        if(this.InputType == InputTypes.Double && (!double.TryParse(value, out tempDouble) || value.Contains(".")))
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidDouble"), Label));

                        if (this.InputType == InputTypes.Valuta)
                        {
                            value = value.Replace(".", string.Empty);
                            Regex moneyR = new Regex(@"^\d+,\d{2}$");//^(\+)?permette l'inserimento solo di valute positive
                            if (!moneyR.IsMatch(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidValuta"), Label));
                        }
                        //double tempDouble = 0;
                        //if (this.InputType == InputTypes.Double && !double.TryParse(value, out tempDouble))
                        //    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidDouble"), Label));

                        if (this.InputType == InputTypes.Phone)
                        {
                            Regex regex = new Regex(@"^([0-9]{8,12})$", RegexOptions.IgnorePatternWhitespace);
                            if (!regex.IsMatch(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidPhoneNumber"), Label));
                        }

                        if (this.InputType == InputTypes.Cap)
                        {
                            Regex regex = new Regex(@"\d{5}");
                            if (!regex.IsMatch(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidCap"), Label));
                        }

                        if (this.InputType == InputTypes.PartitaIva)
                        {
                            if (!NetService.Utility.Common.FormatValidator.ValidatePartitaIVA(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidPartitaIVA"), Label));
                        }

                        if (this.InputType == InputTypes.CodiceFiscale)
                        {
                            if (!NetService.Utility.Common.FormatValidator.ValidateCodiceFiscale(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidCodiceFiscale"), Label));
                        }

                        if (this.InputType == InputTypes.CodiceFiscaleOPartitaIva)
                        {
                            if (value.Length != 16 && value.Length != 11)
                                this.AddError(string.Format("la lunghezza del campo '{0}' deve essere 11 caratteri per la partita IVA o 16 per il codice fiscale", Label));

                            if (value.Length == 16)
                            {
                                if (!NetService.Utility.Common.FormatValidator.ValidateCodiceFiscale(value))
                                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidCodiceFiscale"), Label));
                            }

                            if (value.Length == 11)
                            {
                                if (!NetService.Utility.Common.FormatValidator.ValidatePartitaIVA(value))
                                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidPartitaIVA"), Label));

                            }
                        }

                        if (this.InputType == InputTypes.CodiceAteco)
                        {
                            Regex regex = new Regex(@"\d{2}[.]{1}\d{2}[.]{1}\d{2}");
                            if (!regex.IsMatch(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidAteco"), Label));
                        }

                        if (this.InputType == InputTypes.IBAN)
                        {
                            Regex regex = new Regex(@"[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}");
                            if (!regex.IsMatch(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidIBAN"), Label));
                        }

                        if (this.InputType == InputTypes.CIN)
                        {
                            Regex regex = new Regex(@"[A-Z]");
                            if (!regex.IsMatch(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidCIN"), Label));
                        }

                        if (this.InputType == InputTypes.ABI)
                        {
                            Regex regex = new Regex(@"[0][0-9][0-9][0-9][0-9]");
                            if (!regex.IsMatch(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidABI"), Label));
                        }

                        if (this.InputType == InputTypes.CAB)
                        {
                            Regex regex = new Regex(@"[0-9][0-9][0-9][0-9][0-9]");
                            if (!regex.IsMatch(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidCAB"), Label));
                        }

                        if (this.InputType == InputTypes.TextForName)
                        {
                            Regex regex = new Regex(@"^[\p{L} ']+$");
                            if (!regex.IsMatch(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidName"), Label));
                        }

                        if (this.InputType == InputTypes.TextWithoutSpace)
                        {
                            Regex regex = new Regex(@"^[\w]+$");
                            if (!regex.IsMatch(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidString"), Label));
                        }

                        if (this.ConfermaValoreInserito)
                        {
                            string cvalue = this.ConfirmRequest.StringValue;
                            if (cvalue != value)
                                this.AddError(string.Format(Localization.LabelsManager.GetLabel("passwordnotmatchconfirm"), Label, ConfirmLabel));
                        }

                        if (this.InputType == InputTypes.AbsoluteURI)
                        {
                            bool validated = false;
                            try
                            {
                                Uri ParsedUri;

                                validated = Uri.TryCreate(value, UriKind.Absolute, out ParsedUri) && (ParsedUri.Scheme == Uri.UriSchemeHttp || ParsedUri.Scheme == Uri.UriSchemeHttps);
                            }
                            catch
                            {
                                validated &= false;
                            }
                                
                            if(!validated)
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidAbsoluteURI"), Label));
                        }

                        if (this.InputType == InputTypes.CustomUserID)
                        {
                            Regex regexCustomUID = new Regex(@"^[a-z0-9_-]{3,15}$");
                            if(!regexCustomUID.IsMatch(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidCustomUserID"), Label));
                        }

                        if(this.InputType == InputTypes.TextWithoutSpecialCharacter)
                        {
                            Regex regexCustomTesxtWithoutSpecialCharacter = new Regex(@"^[_àèéìòùa-zA-Z0-9\' ]*$");
                            if (!regexCustomTesxtWithoutSpecialCharacter.IsMatch(value))
                                this.AddError(string.Format(Localization.LabelsManager.GetLabel("isNotValidTextWithoutSpecialCharacter"), Label));
                        }

                        if (this.InputType == InputTypes.AbsoluteOrRelativeURI)
                        {
                            bool validated = false;
                            try
                            {
                                Uri ParsedUri;

                                validated = Uri.TryCreate(value, UriKind.RelativeOrAbsolute, out ParsedUri) && (ParsedUri.Scheme == Uri.UriSchemeHttp || ParsedUri.Scheme == Uri.UriSchemeHttps);
                            }
                            catch
                            {
                                validated &= false;
                            }

                            if (!validated)
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidAbsoluteOrRelativeURI"), Label));
                        }
                    }
                }
            }
            else if (this.Required)
            {
                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //if (this.InputType == InputTypes.Blob)
            //    this.TextBox.Rows = 5;

            if (this.ShowLabelAsHint)
            {
                this.CssClass += " hint";
                this.TextBox.Attributes.Add("title", this.Label);
            }

            if (this.InputType == InputTypes.Valuta)
            {
                string fieldMask = @"<script>$(document).ready(function(){ $('#"+ base.Key +"').mask('#.##0,00', {reverse: true}); });</script>";

                this.Controls.Add(new LiteralControl(fieldMask));
            }
        }

        protected override void AttachFieldControls()
        {
            if (UseBootstrap)
            {
                //Label label = new Label();
                //label.AssociatedControlID = this.Key;
                //label.CssClass = "control-label";
                //label.Text = Label + (this.Required ? "*" : "") + " ";

                if (!ShowLabelAfterField && !HideLabel)
                    this.Controls.Add(LabelControl);

                if (!string.IsNullOrEmpty(HelpInlineText))
                {
                    WebControl inputGroup = new WebControl(HtmlTextWriterTag.Div);
                    inputGroup.CssClass = "input-group";
                   
                    inputGroup.Controls.Add(Field);

                    WebControl inputGroupAppender = new WebControl(HtmlTextWriterTag.Div);
                    inputGroupAppender.CssClass = "input-group-append";
                    //inputGroupAppender.Attributes["title"] = HelpInlineText;
                    //inputGroupAppender.Controls.Add(new LiteralControl("<span class=\"input-group-text\"><i class=\"fa fa-question-circle\" aria-hidden=\"true\"></i></span>"));

                    // codice popover                  
                    inputGroupAppender.Controls.Add(new LiteralControl(@"<a class=""input-group-text""
                                                                            role=""button"" 
                                                                            data-toggle=""popover"" 
                                                                            data-placement=""top"" 
                                                                            data-trigger=""click"" 
                                                                            title=""Aiuto"" 
                                                                            data-content=""" + HelpInlineText.Replace("\"", "&quot;") + @""">
                                                                            <i class=""fa fa-question-circle"" aria-hidden=""true""></i>
                                                                         </a>"));
                    inputGroup.Controls.Add(inputGroupAppender);

                    this.Controls.Add(inputGroup);
                }
                else
                {                    
                        this.Controls.Add(Field);
                }

                if (ShowLabelAfterField && !HideLabel)
                    this.Controls.Add(LabelControl);

                this.Field.ID = this.Key;
            }
            else
            {
                //Label label = new Label();
                //label.CssClass = "control-label";
                //label.AssociatedControlID = this.Key;
                //label.Text = Label + (this.Required ? "*" : "");

                if (!ShowLabelAfterField && !HideLabel)
                    this.Controls.Add(LabelControl);

                if (!string.IsNullOrWhiteSpace(HelpInlineText))
                {
                    WebControl aPop = new WebControl(HtmlTextWriterTag.A);
                    //aPop.CssClass = "btn btn-info";
                    aPop.Controls.Add(new LiteralControl("<i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i>"));
                    //a.Attributes.Add("role", "button");
                    aPop.Attributes.Add("data-toggle", "popover");
                    aPop.Attributes.Add("data-trigger", "hover");
                    aPop.Attributes.Add("data-content", HelpInlineText);

                    this.Controls.Add(aPop);
                }

                this.Controls.Add(Field);
                //this.Controls.Add(new WebControl(HtmlTextWriterTag.Span) { CssClass = "vf-clear" });
                this.Field.ID = this.Key;

                if (this.ConfermaValoreInserito)
                {
                    //label = new Label();
                    //label.CssClass = "control-label";
                    //label.AssociatedControlID = this.TextBoxConfirm.ID;
                    //label.Text = ConfirmLabel;

                    LabelControl.Text = ConfirmLabel;

                    this.Controls.Add(new LiteralControl("<br/>"));
                    this.Controls.Add(LabelControl);
                    this.Controls.Add(TextBoxConfirm);
                    //   this.Controls.Add(new WebControl(HtmlTextWriterTag.Span) { CssClass = "vf-clear" });
                }
            }
        }

        public bool ConfermaValoreInserito
        {
            get;
            set;
        }

        public TextBox TextBoxConfirm
        {
            get
            {
                if (_TextBoxConfirm == null)
                {
                    _TextBoxConfirm = new TextBox();
                    _TextBoxConfirm.CssClass = "form-control ";
                    _TextBoxConfirm.ID = this.Key + "_confirm";

                    if (Columns > 0)
                        _TextBoxConfirm.Columns = Columns;

                    _TextBoxConfirm.TextMode = TextMode;

                    if (TextMode == TextBoxMode.MultiLine)
                    {
                        if (Rows > 0)
                            _TextBoxConfirm.Rows = Rows;
                    }

                    if (this.Request.IsValidString)
                    {
                        _TextBoxConfirm.Text = this.ConfirmRequest.StringValue.Replace("''", "'");
                    }
                }
                return _TextBoxConfirm;
            }
        }
        private TextBox _TextBoxConfirm;

        public string ConfirmLabel
        {
            get
            {
                return _ConfirmLabel;
            }
            private set
            {
                _ConfirmLabel = value;
            }
        }
        private string _ConfirmLabel;

        public NetService.Utility.Common.RequestVariable ConfirmRequest
        {
            get
            {
                if (_ConfirmRequest == null)
                {
                    _ConfirmRequest = new NetService.Utility.Common.RequestVariable(TextBoxConfirm.ID, NetService.Utility.Common.RequestVariable.RequestType.Form);
                }
                return _ConfirmRequest;
            }
        }
        private NetService.Utility.Common.RequestVariable _ConfirmRequest;
    }
}
