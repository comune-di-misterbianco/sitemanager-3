﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using NetService.Utility.RecordsFinder;
using System.Web;
using System.Web.Script.Serialization;
using System.Collections;
using NHibernate;
using System.Reflection;
using System.Web.UI;

namespace NetService.Utility.ValidatedFields
{
    /// <summary>
    /// Permette di gestire una lista contenente una proprietà di un oggetto che si vuole esporre.
    /// La gestione avviene tramite web API.
    /// </summary>
    public class VfAddRemoveList : VfGeneric
    {
        public override string LocalCssClass
        {
            get { return "addremovelist-vf"; }
        }

        public enum PostBackValueObjectTypes
        {
            ListOfDTO,
            ListOfObjects
        }

        private PostBackValueObjectTypes PostBackValueObjectType
        {
            get;
            set;
        }

        public WebControl AutocompleteControl
        {
            get
            {
                if (_AutocompleteControl == null)
                {
                    _AutocompleteControl = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
                    _AutocompleteControl.CssClass = "search-add-element";
                }
                return _AutocompleteControl;
            }
        }
        private WebControl _AutocompleteControl;

        public WebControl ListControl
        {
            get
            {
                if (_ListControl == null)
                {
                    _ListControl = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
                }
                return _ListControl;
            }
        }
        private WebControl _ListControl;

        protected WebControl Ul
        {
            get
            {
                if (_Ul == null)
                {
                    _Ul = new WebControl(System.Web.UI.HtmlTextWriterTag.Ul);
                    _Ul.ID = this.Key + "-ul-vf-list";
                    _Ul.CssClass = "list";
                }
                return _Ul;
            }
        }
        protected WebControl _Ul;

        protected HiddenField Hidden
        {
            get
            {
                if (_Hidden == null)
                {
                    _Hidden = new HiddenField();
                    _Hidden.ID = this.Key + "-element-list";
                }
                return _Hidden;
            }
        }
        protected HiddenField _Hidden;

        public override WebControl Field
        {
            get { return ListControl; }
        }


        protected ICollection<VfAddRemoveListDTO> Items
        {
            get
            {
                if (_Items == null)
                {
                    _Items = new List<VfAddRemoveListDTO>();
                }
                return _Items;
            }
            set
            {
                _Items = value;
            }
        }
        private ICollection<VfAddRemoveListDTO> _Items;

        public override object PostbackValueObject
        {
            get 
            {
                switch (PostBackValueObjectType)
                {
                    case PostBackValueObjectTypes.ListOfDTO:
                        {
                            IList<VfAddRemoveListDTO> Items = new List<VfAddRemoveListDTO>();

                            NetService.Utility.Common.RequestVariable value = new NetService.Utility.Common.RequestVariable(Hidden.ID);

                            if (!string.IsNullOrEmpty(value.StringValue))
                            {
                                var jsSerializer = new JavaScriptSerializer();

                                var itemList = jsSerializer.Deserialize<List<VfAddRemoveListDTO>>(value.StringValue);

                                Items = itemList.ToList<VfAddRemoveListDTO>();

                            }

                            return Items;
                        }

                    case PostBackValueObjectTypes.ListOfObjects:
                        {
                            IList<VfAddRemoveListDTO> Items = new List<VfAddRemoveListDTO>();

                            NetService.Utility.Common.RequestVariable value = new NetService.Utility.Common.RequestVariable(Hidden.ID);


                            if (!string.IsNullOrEmpty(value.StringValue))
                            {
                                var jsSerializer = new JavaScriptSerializer();

                                var itemList = jsSerializer.Deserialize<List<VfAddRemoveListDTO>>(value.StringValue);

                                Items = itemList.ToList<VfAddRemoveListDTO>();

                            }

                            IList added = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(base.ReturnObjectType));

                            ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.GetSession();

                            var daoType = typeof(GenericDAO.DAO.CriteriaNhibernateDAO<>);
                            Type[] typeArgs = { base.ReturnObjectType };

                            var makeDao = daoType.MakeGenericType(typeArgs);

                            var dao = Activator.CreateInstance(makeDao, session);

                            MethodInfo mi = dao.GetType().GetMethod("GetById", new Type[] { typeof(int) });

                            foreach (VfAddRemoveListDTO item in Items)
                            {
                                object obj = mi.Invoke(dao, new object[] { item.ID });
                                added.Add(obj);
                            }

                            return added;

                        }
                    default: return null;
                }

            
            }
        }

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set
            { _SearchComparisonCriteria = value; }
        }
        private RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = RecordsFinder.Finder.ComparisonCriteria.Equals;

        public string AddButtonText;

        public Button AddItemButton
        {
            get
            {
                if (_AddItemButton == null)
                {
                    _AddItemButton = new Button();
                    _AddItemButton.ID = this.Key + "-add-list-element-button";
                    _AddItemButton.CssClass ="add-button";
                    _AddItemButton.Enabled = false;
                    _AddItemButton.Text = string.IsNullOrEmpty(AddButtonText) ? (ItemsMaxNumber == 1 ? "Seleziona" : "Aggiungi" ): AddButtonText;
                }
                return _AddItemButton;
            }
        }
        private Button _AddItemButton;

        public TextBox HelpTextBox
        {
            get
            {
                if (_HelpTextBox == null)
                {
                    _HelpTextBox = new TextBox();
                    _HelpTextBox.ID = this.Key + "-autocomplete-input";
                    _HelpTextBox.Enabled = this.Enabled;
                    if (this.Request.IsValidString)
                    {
                        _HelpTextBox.Text = this.Request.StringValue.Replace("''", "'");
                    }
                }
                return _HelpTextBox;
            }
        }
        private TextBox _HelpTextBox;

        /// <summary>
        /// Definisce il nome dell'entità da ricercare con l'input textbox, permette di personalizare gli errori in output 
        /// </summary>
        public string HelpTextBoxEntityName
        {
            get;
            set;
        }

        #region Tooltip

        public bool EnableHelpTooltip
        {
            get;
            set;
        }

        public string HelpTooltipText
        {
            get
            {
                if(string.IsNullOrEmpty(_HelpTooltipText))
                    _HelpTooltipText = "Per aggiungere '" + this.Label + "' digitare l'iniziale o una parte del nome e selezionare dall'elenco che apparirà uno degli " + this.Label + ", quindi cliccare sul pulsante di aggiunta.";

                return _HelpTooltipText;
            }
            set
            {
                _HelpTooltipText = value;
            }
                
        }
        private string _HelpTooltipText;

        public WebControl HelpTooltip
        {
            get
            {
                if (_HelpTooltip == null)
                {
                    _HelpTooltip = new WebControl(HtmlTextWriterTag.A);
                    //_HelpTooltip.Attributes.Add("href", "");
                    //_HelpTooltip.ToolTip = HelpTooltipText;
                    //_HelpTooltip.CssClass = "tooltip-cms3";

                    //WebControl span = new WebControl(HtmlTextWriterTag.Span);
                    //span.Attributes.Add("title", "");
                    //span.Controls.Add(new LiteralControl(" ? "));

                    //_HelpTooltip.Attributes.Add("href", "");

                    _HelpTooltip.CssClass = "tooltip-cms3";
                    _HelpTooltip.Controls.Add(new LiteralControl(" ? "));

                    _HelpTooltip.Controls.Add(new WebControl(HtmlTextWriterTag.B));

                    WebControl span = new WebControl(HtmlTextWriterTag.Span);
                    span.Controls.Add(new LiteralControl(HelpTooltipText));

                    _HelpTooltip.Controls.Add(span);

                }
                return _HelpTooltip;
            }
        }
        private WebControl _HelpTooltip;

        #endregion

        /// <summary>
        /// Eventuale chiave primaria per il postbackvalue di tipo ListOfObject
        /// </summary>
        private string KeyId
        {
            get;
            set;
        }

        /// <summary>
        /// Nome della proprietà da esporre come testo nell'elenco
        /// </summary>
        public string PropertyToExposeInText
        {
            get;
            private set;
        }
            

        public int MaxNumberOfElementBeforeScroll
        {
            get;
            set;
        }

        public int HeightForScroll
        {
            get;
            set;
        }

        public int ItemsMaxNumber
        {
            get;
            set;
        }

        protected virtual string Script
        {
            get
            {               

                string text = @"<script type='text/javascript'>

                    $(document).ready(function () {

                        var NotValidItem = false;
                        var array= new Array();

                        var options = {
                            item: '<li class=list-line><span style=display:none class=index></span><span class=deletex></span><span class=elementtext></span></li>',
                            page: 500
                        };

                        
                        var list = new List('" + this.Key + @"', options);


                        function initialize()   {    

                            if(($('#" + this.Hidden.ID + @"').val() != '') && (array.length == 0))
                            {
                                var initValue = $.parseJSON($('#" + this.Hidden.ID + @"').val());

                                $.each(initValue, function(i, item) {                              
                                    var del_id = '" + this.Key + @"_rem_' + item.ID + '_' +item.Type; // #MODIFICA 
                                    var deleteButton = '<input type=submit id=' + del_id + ' class=item-delete-button " + this.Key + " " + (this.Enabled ? "" : "disabled=disabled") + @" value= X >';
                                    list.add({ index: item.ID + '_' +item.Type, deletex: deleteButton, elementtext: item.Text }); //#MODIFICA
                                    array.push({ id: item.ID, text: item.Text,type: item.Type });
                                    
                                });
                            }
                            $('#" + this.Hidden.ID + @"').val(JSON.stringify(array));
                        }

                        $(function() {    
                                

                            initialize();

	                        function log(id, message, identifier ) {	
                                $('#" + this.HelpTextBox.ID + @"').val(message);
                                $('#" + this.AddItemButton.ID + @"').attr('name', identifier.toString());
	                        }

                            $( '#" + this.HelpTextBox.ID + @"' ).autocomplete({
		                            source: function( request, response ) {
			                            $.ajax({
			                            url: '" + this.TextBoxWebAPIPath + @"',
			                            contentType: 'json',
                                                data: {
                                                        //featureClass: 'P',
                                                        //style: 'full',                            
                                                        value: request.term
                                        },			
					                    success: function( data ) {
										    //$.each(data, function(index, item)
                                            
                                            

                                            if(data.length == 0)
                                                    NotValidItem = true;
                                                else
                                                    NotValidItem = false;

                                            

										    response( $.map( data, function( item ) {
										    return {
												    id: item.ID,
												    label: item.Text,
												    value: item.Text,
                                                    type : item.Type // #MODIFICA
												    }
										    }));
									    },
                                         error: function(jqXHR,textStatus,errorThrown)
                                        {
                                            alert(textStatus);
                                        }
					                    });
		                            },
		                            minLength: 3,
		                            select: function( event, ui ) {
                                        
                                        var id_tag = '" + this.HelpTextBox.ID + @"'+
										log(id_tag, ui.item ?
										'Selected: ' + ui.item.label :
										'Nothing selected, input was ' + this.value, ui.item.id + '_' + ui.item.type); // #MODIFICA aggiunto il type
                                        $('#" + this.AddItemButton.ID + @"').removeAttr('disabled');
									}
		                            //open: function() {
							                            //$( this ).removeClass( 'ui-corner-all' ).addClass( 'ui-corner-top' );
						                            //},
		                            //close: function() {
							                            //$( this ).removeClass( 'ui-corner-top' ).addClass( 'ui-corner-all' );
						                            //}
	                            });
                        });

                        

                        $('#" + this.AddItemButton.ID + @"').click(function () {

                            var text = $('#" + this.HelpTextBox.ID + @"').val();
                            var errors = false;
                            var complete_id = $(this).attr('name');
                            var id = $(this).attr('name').split('_')[0]; // #MODIFICA, aggiunto type dell'oggetto a id
                            var type = $(this).attr('name').split('_')[1];

                            if (text == '') {
                                errors = true;
                                $('.error').remove();
                                var errorCode = '<div class=error><li><span>Il campo " + (string.IsNullOrEmpty(this.HelpTextBoxEntityName) ? "testo" : HelpTextBoxEntityName) + @" è obbligatorio</span></li></div>';
                                $('." + this.AutocompleteControl.CssClass + @"').append(errorCode);
                            }


                            if(!errors && NotValidItem) {
                                errors = true;
                                $('.error').remove();
                                var errorCode = '<div class=error><li><span>Il campo " + (string.IsNullOrEmpty(this.HelpTextBoxEntityName) ? "testo" : HelpTextBoxEntityName) + @"  immesso non è valido</span></li></div>';
                                $('." + this.AutocompleteControl.CssClass + @"').append(errorCode);
                            }


                            var node = list.get('elementtext', text);

                            if (!errors && node != null) {
                                errors = true;
                                $('.error').remove();
                                var errorCode = '<div class=error><li><span>Il campo " + (string.IsNullOrEmpty(this.HelpTextBoxEntityName) ? "testo" : HelpTextBoxEntityName) + @" selezionato è stato già aggiunto all\'elenco</span></li></div>';
                                $('." + this.AutocompleteControl.CssClass + @"').append(errorCode);
                            }

                            if (!errors) {
            
                                $('.error').remove();
                                var del_id = '" + this.Key + @"_rem_' +complete_id;// #MODIFICA+ id;
                                var deleteButton = '<input type=submit id=' + del_id + ' class=item-delete-button value= X >';

                               // #MODIFICA  list.add({ index: id, deletex: deleteButton, elementtext: text });
                                list.add({ index: complete_id, deletex: deleteButton, elementtext: text });
                                array.push({ id : id, text: text,type : type }); // #MODIFICA aggiunto type
                                $('#" + this.Hidden.ID + @"').val(JSON.stringify(array));
                            }

                            $('#" + this.HelpTextBox.ID + @"').val('');
                            $('#" + this.AddItemButton.ID + @"').prop('disabled', true);
                            return false;
                        });

                        $('#" + this.Ul.ID + @"').on('click', '.item-delete-button', function() {
                            var index = $(this).prop('id').split('_')[2];
                            var complete_index = $(this).prop('id').split('_')[2] + '_' + $(this).prop('id').split('_')[3];
                            var type = $(this).prop('id').split('_')[3];
                            //#MODIFICA list.remove('index', index);
                            list.remove('index',complete_index);
                            for(var  i in array) {
                                if(array[i].id == index && array[i].type == type) // #MODIFICA
                                    array.splice(i, 1);
                            }
                            
                            $('#" + this.Hidden.ID + @"').val(JSON.stringify(array));
                            $('#" + this.Key + @"_rem_' + complete_index).parent().parent().remove(); // #MODIFICA
                                                                                               
                            
                            return false;
                        });
                    });

                    

                </script>";

                return text;
            }
        }

        /// <summary>
        /// Permette di abilitare o meno l'obbligo di aggiungere almeno un elemento
        /// NB: Non settare a true l'attributo Required, altrimenti si ha un comportamento non corretto.
        /// </summary>
        public bool AtLeastOneRequired
        {
            get { return _AtLeastOneRequired; }
            set { _AtLeastOneRequired = value; }
        }
        private bool _AtLeastOneRequired;

        /// <summary>
        /// La web API deve restituire oltre al testo da inserire nella textbox di aiuto, l'id del relativo oggetto
        /// da utilizzare per il calcolo del post back value. Per far questo è possibile utilizzare come DTO la classe
        /// VfAutocompleteElementDTO, magari sfrutttando l'automapper
        /// </summary>
        public string TextBoxWebAPIPath;

        /// <summary>
        /// Costruttore base per elaborare come postbackvalueobject una lista di DTO contenenti id e testo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="LabelResourceKey"></param>
        /// <param name="TextBoxWebAPIPath">Url path per la web api</param>
        public VfAddRemoveList(string id, string LabelResourceKey, string TextBoxWebAPIPath)
            : base(id, LabelResourceKey)
        {
            this.Required = false;

            this.TextBoxWebAPIPath = TextBoxWebAPIPath;

            this.PostBackValueObjectType = PostBackValueObjectTypes.ListOfDTO;

            this.HeightForScroll = 400;
        }
        //usando l'oggetto creandolo con questo costruttore ho riscontrato dei funzionamenti anomali settando la prop maxitemnumber =1, required = false ed atleastonerequired = true.
        //se compilavo la VfAddRemoveList e i restanti campi obbligatori vuoti, la validate genera gli errori. se compilavo i campi obbligatori e cambiavo l'elemento prima
        //selezionato nella vf, la validate falliva lo stesso perchè si trovava due elementi nel campi Items e superando il maxitemnumber dava errore.
        /// <summary>
        /// Costruttore per elaborare come postbackvalueobject una lista di oggetti in base alla proprietà ID ed al tipo passato
        /// </summary>
        /// <param name="id"></param>
        /// <param name="LabelResourceKey"></param>
        /// <param name="TextBoxWebAPIPath"></param>
        /// <param name="PropertyToExposeInText">Nome della proprietà da esporre in formato testo</param>
        /// <param name="keyId">proprietà che identifica l'ID dell'oggetto (es: ID)</param>
        /// <param name="returnObjectType">tipo dell'oggetto elaborato</param>
        public VfAddRemoveList(string id, string LabelResourceKey, string TextBoxWebAPIPath, string PropertyToExposeInText, string keyId, Type returnObjectType)
            : base(id, LabelResourceKey, id, returnObjectType)
        {
            this.Required = false;

            this.TextBoxWebAPIPath = TextBoxWebAPIPath;

            this.PostBackValueObjectType = PostBackValueObjectTypes.ListOfObjects;

            this.PropertyToExposeInText = PropertyToExposeInText;

            this.KeyId = keyId;

            this.HeightForScroll = 400;
        }

        

        protected override void FillFieldValue()
        {
            this.Items.Clear();

            switch (PostBackValueObjectType)
            {
                case PostBackValueObjectTypes.ListOfDTO:
                    {
                        Items = ((List<VfAddRemoveListDTO>) this.DefaultValue);                       

                        break;
                    }
                case PostBackValueObjectTypes.ListOfObjects:
                    {
                        IList values = (IList)this.DefaultValue;

                        foreach (object obj in values)
                        {
                            var id = obj.GetType().GetProperty(KeyId).GetValue(obj, null);
                            var text = obj.GetType().GetProperty(PropertyToExposeInText).GetValue(obj, null);

                            VfAddRemoveListDTO Item = new VfAddRemoveListDTO();
                            Item.ID = (int) id;
                            Item.Text = text.ToString();                        
                            Items.Add(Item);
                        }

                        break;
                    }
            }
        }

        protected override void LocalValidate()
        {
            IEnumerable<VfAddRemoveListDTO> addedItems = Enumerable.Empty<VfAddRemoveListDTO>();

            NetService.Utility.Common.RequestVariable value = new NetService.Utility.Common.RequestVariable(Hidden.ID);

            if (!string.IsNullOrEmpty(value.StringValue))
            {
                var jsSerializer = new JavaScriptSerializer();

                var itemList = jsSerializer.Deserialize<List<VfAddRemoveListDTO>>(value.StringValue);

                addedItems = itemList.ToList();

            }

            if (AtLeastOneRequired && addedItems.Count() == 0)
            {
                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
            }

            if (this.ItemsMaxNumber != 0 && addedItems.Count() > this.ItemsMaxNumber)
            {
                this.AddError("Il numero di elementi " + (string.IsNullOrEmpty(this.HelpTextBoxEntityName) ? "della lista" : "di tipo " + this.HelpTextBoxEntityName) + " non può essere maggiore di " + ItemsMaxNumber);
            }

            //Verifica che gli id non siano stati alterati con stringhe da console di debug
            if (this.ValidationErrors.Count == 0 && addedItems.Count() != 0)
            {
                try
                {
                    IList<int> listInt = addedItems.Select(x => x.ID).ToList();
                    if (listInt.Count != addedItems.Count())
                        throw new Exception();
                }
                catch(Exception)
                {
                    this.AddError("Errore nel campo " + Label);
                }
            }

        }

        /// <summary>
        /// Aggiunge un elemento all'elenco
        /// </summary>
        /// <param name="id">Valore dell'identificativo dell'oggetto</param>
        /// <param name="text">Valore della proprietà da esporre dell'oggetto</param>
        public void AddItem(int id, string text,string type = "")
        {
            VfAddRemoveListDTO Item = new VfAddRemoveListDTO();
            Item.ID = id;
            Item.Text = text;
            Item.Type = type;
            this.Items.Add(Item);
        }
        

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);


            //switch (PostBackValueObjectType)
            //{
            //    case PostBackValueObjectTypes.ListOfDTO:
            //        {
            //            Items = ((List<VfAddRemoveListDTO>)this.PostbackValueObject);
            //            break;
            //        }
            //    case PostBackValueObjectTypes.ListOfObjects:
            //        {
            //            IList values = (IList)this.PostbackValueObject;

            //            foreach (object obj in values)
            //            {
            //                var id = obj.GetType().GetProperty(KeyId).GetValue(obj, null);
            //                var text = obj.GetType().GetProperty(PropertyToExposeInText).GetValue(obj, null);

            //                VfAddRemoveListDTO Item = new VfAddRemoveListDTO();
            //                Item.ID = (int)id;
            //                Item.Text = text.ToString();
            //                Items.Add(Item);
            //            }

            //            break;
            //        }
            //}

            

            if (Items.Count > 0)
            {
                JavaScriptSerializer Serializer = new JavaScriptSerializer();
                this.Hidden.Value = Serializer.Serialize(Items);
            }


            
            
            this.AutocompleteControl.Controls.Add(HelpTextBox);           
            this.AutocompleteControl.Controls.Add(AddItemButton);
            this.AutocompleteControl.Controls.Add(Hidden);

            if (this.EnableHelpTooltip)
                this.AutocompleteControl.Controls.Add(HelpTooltip);

            this.Controls.Add(AutocompleteControl);


            if (Items.Count > this.MaxNumberOfElementBeforeScroll)
            {
                this.ListControl.Height = HeightForScroll;
                this.ListControl.Attributes.Add("style", "overflow-y:auto");
            }

            this.ListControl.Controls.Add(Ul);

            
            this.Controls.Add(ListControl);


            Page.Header.Controls.Add(new System.Web.UI.LiteralControl(Script));
            Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<script type=\"text/javascript\" src=\"" + NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/shared/list/list.js\"></script>"));
            //Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<script type=\"text/javascript\" src=\"" + NetCms.Configurations.Paths.AbsoluteRoot + "/scripts/admin/temp.js\"></script>"));
        }


        
    }

    /// <summary>
    /// Utility da utilizzare come DTO per la web API della classe VFAutocompleteList
    /// </summary>
    public class VfAddRemoveListDTO
    {
        public int ID
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }

        public string Type
        {
            get;
            set;
        }
    }
}
