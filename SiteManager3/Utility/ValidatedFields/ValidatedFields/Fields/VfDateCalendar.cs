﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;


[assembly: WebResource("ValidatedFields.ValidatedFields.Fields.Calendar.Images.calendar.png", "image/png")]

namespace NetService.Utility.ValidatedFields
{

    /// <summary>
    /// VF DateCalendar with Boostrap 3 DatePicker 
    /// </summary>
    /// <see cref="http://eonasdan.github.io/bootstrap-datetimepicker/"/>
    public class VfDateCalendar: VfGeneric
    {
        const string field_prefix = "DC_";

        #region Property

        private bool _UsedOnFrontend = false;
        /// <summary>
        /// Vf used or not used on frontend
        /// </summary>       
        /// <remarks>Default Value false</remarks>
        public bool UsedOnFrontend
        {
            get { return _UsedOnFrontend; }
            set { _UsedOnFrontend = value; }
        }

        private bool _AddAccessoryInputGroup = false;
        /// <summary>
        /// Add Accessory Input Group
        /// </summary>
        public bool AddAccessoryInputGroup
        {
            get { return _AddAccessoryInputGroup; }
            set { _AddAccessoryInputGroup = value; }
        }
        
        private string _Locale = "it";
        /// <summary>
        /// Set DateTimePicker Localization
        /// </summary>
        public string Locale
        {
            get { return _Locale; }
            set { _Locale = value; }

        }
                 
        private DateTime _MaxDate = DateTime.MaxValue;
        /// <summary>
        /// Set Maximum date of DateTimePicker
        /// </summary>
        public DateTime MaxDate
        {
            get { return _MaxDate; }
            set { _MaxDate = value; }
        }
                   
        private DateTime _MinDate = DateTime.MinValue;
        /// <summary>
        /// Min Date
        /// </summary>
        /// <remarks>Set Minimum date of DateTimePicker </remarks>
        public DateTime MinDate
        {
            get { return _MinDate; }
            set { _MinDate = value; }
        }
                             
        private bool _UseDateTimePicker = false;
        /// <summary>
        /// Enable/Disable Time Picker
        /// </summary>
        public bool UseDateTimePicker
        {
            get { return _UseDateTimePicker; }
            set { _UseDateTimePicker = value; }
        }
                             
        private bool _ShowInline = false;
        /// <summary>
        /// Show DateTimePicker Inline
        /// </summary>                
        /// <remarks> Will display the picker inline without the need of a input field. This will also hide borders and shadows. </remarks>
        public bool ShowInline
        {
            get { return _ShowInline; }
            set { _ShowInline = value; }
        }
                             
        private bool _ShowTimePickerSideBySide = false;
        /// <summary>
        /// Show TimePicker Side By Side
        /// </summary>
        /// <remarks> Shows the picker side by side when using the time and date together. </remarks>
        public bool ShowTimePickerSideBySide
        {
            get { return _ShowTimePickerSideBySide; }
            set { _ShowTimePickerSideBySide = value; }
        }

        private bool _ShowCalendarWeekNumber = false;
        /// <summary>
        /// Show Calendar Week Number
        /// </summary>
        public bool ShowCalendarWeekNumber
        {
            get { return _ShowCalendarWeekNumber; }
            set { _ShowCalendarWeekNumber = value; }
        }
           
        private bool _ShowClearButton = false;
        /// <summary>
        /// Show Clear Button
        /// </summary>
        /// <remarks>Show the "Clear" button in the icon toolbar. Clicking the "Clear" button will set the calendar to null.</remarks>   
        public bool ShowClearButton
        {
            get { return _ShowClearButton; }
            set { _ShowClearButton = value; }
        }
                  
        private bool _ShowCloseButton = false;
        /// <summary>
        /// Show Close Button
        /// </summary>
        /// <remarks>Show the "Close" button in the icon toolbar. Clicking the "Close" button will hide datetimepicker.</remarks>   
        public bool ShowCloseButton
        {
            get { return _ShowCloseButton; }
            set { _ShowCloseButton = value; }
        }
                        
        private bool _ShowTodayButton = false;
        /// <summary>
        /// Show Today Button
        /// </summary>
        /// <remarks>Show the "Today" button in the icon toolbar. Clicking the "Today" button will set the calendar view and set the date to now</remarks>   
        public bool ShowTodayButton
        {
            get { return _ShowTodayButton; }
            set { _ShowTodayButton = value; }
        }
       
        /// <summary>
        /// Toolbar Placement Option
        /// </summary>
        public enum DTPToolBarPlacement
        {
            Default,
            Top,
            Bottom
        }

        private DTPToolBarPlacement _ToolbarPlacement = DTPToolBarPlacement.Default;
        /// <summary>
        /// Toolbar Placement
        /// </summary>
        /// <remarks>Changes the placement of the icon toolbar.</remarks>
        public DTPToolBarPlacement ToolbarPlacement
        {
            get { return _ToolbarPlacement; }
            set { _ToolbarPlacement = value; }
        }


        /// <summary>
        /// LocalCssClass
        /// </summary>
        public override string LocalCssClass
        {
            get
            {
                return "textbox-vf";
            }
        }
             
        #endregion

        public override WebControl Field
        {
            get 
            {
                if (UsedOnFrontend)
                {
                    if (AddAccessoryInputGroup)
                    {
                        WebControl InputGroup = new WebControl(HtmlTextWriterTag.Div);
                        InputGroup.CssClass = "input-group date";
                        InputGroup.ID = field_prefix + this.Key;

                        InputGroup.Controls.Add(new LiteralControl("<span class=\"input-group-addon\"><i class=\"fa fa-calendar-o\" aria-hidden=\"true\"></i></span>"));

                        InputGroup.Controls.Add(TextBox);
                        return InputGroup;
                    }
                    else
                    {
                        TextBox.ID = this.Key;
                        return TextBox;
                    }
                }
                else
                return TextBox;             
            }
        }
       

        //private Calendar _Calendar;
        //public Calendar Calendar 
        //{
        //    get
        //    {
        //        if (_Calendar == null)
        //        {
        //            _Calendar = new Calendar(this.ID);
        //            if (this.Request.IsValidString)
        //            {
        //                _Calendar.TextBox.Text = this.Request.StringValue;
        //            }
        //        }
        //        return _Calendar;
        //    }
        //    set { _Calendar = value; }
        //}

        public override object PostbackValueObject
        {
            get
            {
                return DateTime.Parse(this.Request.OriginalValue);  
            }
        }

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set { _SearchComparisonCriteria = value; }
        }
        private RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = RecordsFinder.Finder.ComparisonCriteria.Equals;


        public TextBox TextBox
        {
            get
            {
                if (_TextBox == null)
                {
                    _TextBox = new TextBox();
                    _TextBox.CssClass = "form-control";
                    //_TextBox.ID = this.ID;
                    _TextBox.ID = this.Key;

                    if (this.Request.IsValidString)
                    {
                        _TextBox.Text = this.Request.StringValue;
                    }
                }
                return _TextBox;
            }
        }
        private TextBox _TextBox;

        // aggiungere vari settings del componente jquery-ui come proprietà
        // date format
        // button image
        // ...


        protected override void AttachFieldControls()
        {
            if (UsedOnFrontend)
            {
                Label datecalendarlabel = new Label();
                datecalendarlabel.AssociatedControlID = this.Key;
                datecalendarlabel.CssClass = "control-label";
                datecalendarlabel.Text = Label + (this.Required ? " *" : "");
                        
                if (!ShowLabelAfterField)
                    this.Controls.Add(datecalendarlabel);

                this.Controls.Add(Field);

                if (ShowLabelAfterField)
                    this.Controls.Add(datecalendarlabel);
            }
            else
                base.AttachFieldControls();
            
        }
        
        public string WidgetMaxDate
        {
            get;
            set;
        }
        
        public string WidgetMinDate
        {
            get;
            set;
        }
           

        #region Constructor
        public VfDateCalendar(string id, string labelResourceKey, bool isForFrontend = false)
            : base(id, labelResourceKey)
        {
            UsedOnFrontend = isForFrontend;
        }

        public VfDateCalendar(string id, string labelResourceKey, string activeRecordPropertyName, bool isForFrontend = false)
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
            UsedOnFrontend = isForFrontend;
        }

        public VfDateCalendar(string id, string labelResourceKey, string activeRecordPropertyName, Type returnObjectType, bool isForFrontend = false)
            : base(id, labelResourceKey, activeRecordPropertyName, returnObjectType)
        {
            UsedOnFrontend = isForFrontend;
        }

        public VfDateCalendar(string id, string labelResourceKey, string activeRecordPropertyName, bool useDateTimePicker, bool isForFrontend = false)
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
            UseDateTimePicker = useDateTimePicker;
            UsedOnFrontend = isForFrontend;
        }

        public VfDateCalendar(string id, string labelResourceKey, string activeRecordPropertyName, bool useDateTimePicker, Type returnObjectType, bool isForFrontend = false)
            : base(id, labelResourceKey, activeRecordPropertyName, returnObjectType)
        {
            UseDateTimePicker = useDateTimePicker;
            UsedOnFrontend = isForFrontend;
        }

        #endregion

        protected override void FillFieldValue()
        {
            if (this.DefaultValue is DateTime)
            {
                if (!UseDateTimePicker)
                    this.TextBox.Text = ((DateTime)this.DefaultValue).ToShortDateString();
                //this.TextBox.Text = ((DateTime)this.DefaultValue).ToLocalTime().ToShortDateString();
                else
                    this.TextBox.Text = ((DateTime)this.DefaultValue).ToShortDateString() + " " + ((DateTime)this.DefaultValue).ToShortTimeString();
                    //this.TextBox.Text = ((DateTime)this.DefaultValue).ToLocalTime().ToShortDateString() + " " + ((DateTime)this.DefaultValue).ToLocalTime().ToShortTimeString();
            }
        }

        protected override void LocalValidate()
        {
            if (this.Request.IsValidString && this.Request.StringValue != "")
            {
                if(!this.Request.IsValid(NetService.Utility.Common.RequestVariable.VariableType.Date))
                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("{0} non contiene una data valida"), Label));

                if (MinDate > DateTime.MinValue && this.Request.DateTimeValue < MinDate)
                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("{0} deve contenere una data successiva al (" + (MinDate.ToLocalTime().ToShortDateString()) + ")"), Label));

                if (MaxDate < DateTime.MaxValue && this.Request.DateTimeValue > MaxDate)
                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("{0} deve contenere una data antecedente al (" + (MaxDate.ToLocalTime().ToShortDateString()) + ")"), Label));
            }
            else if (this.Required)
            {
                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("{0} è obbligatorio"), Label));
            }
        }




        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (LanguageManager.Utility.Config.EnableMultiLanguage)
            {
                Locale = LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.LangTwoLetterCode;
            }

            WebControl script = new WebControl(HtmlTextWriterTag.Script);
            script.Attributes["type"] = "text/javascript";
            
            string str_script = "";
            int currentYear = DateTime.Now.Year;
            int initYear = currentYear - 115;
            int endYear = currentYear + 10;


            if (UsedOnFrontend)
            {
                /* bootstrap ui*/
                str_script = @"
                   $(document).ready(function() {
                      $(""#" + field_prefix + this.Key + @""").datetimepicker({
                            locale:'" + Locale + @"',
                            format: '" + (UseDateTimePicker ? "DD/MM/YYYY HH:mm" : "DD/MM/YYYY") + @"',
                            inline: " + (ShowInline ? "true" : "false") + @" ,
                            sideBySide: " + (ShowTimePickerSideBySide ? "true" : "false") + @",
                            minDate: '" + ((MinDate != DateTime.MinValue) ? MinDate.ToShortDateString() : "01/01/" + initYear) + @"',
                            maxDate: '" + ((MaxDate != DateTime.MaxValue) ? MaxDate.ToShortDateString() : "01/01/" + endYear) + @"',
                            calendarWeeks: " + (ShowCalendarWeekNumber ? "true" : "false") + @",
                            showClear: " + (ShowClearButton ? "true" : "false") + @",
                            showTodayButton: " + (ShowTodayButton ? "true" : "false") + @",
                            showClose: " + (ShowTodayButton ? "true" : "false") + @",
                            toolbarPlacement: '" + ToolbarPlacement.ToString().ToLower() + @"'
                      })
                   });";


            }
            else
            {
                #region OLD DateTimePicker (jquery-ui)
                if (!UseDateTimePicker)
                {
                    str_script = @"  
	            $(document).ready(function() {             
		            $(""#" + this.Key + @""").datepicker({
                        
                        renderer: $.ui.datepicker.defaultRenderer,
                        monthNames: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno',
                                'Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],
                        monthNamesShort: ['Gen','Feb','Mar','Apr','Mag','Giu',
                                'Lug','Ago','Set','Ott','Nov','Dic'],
                        dayNames: ['Domenica','Luned&#236','Marted&#236','Mercoled&#236','Gioved&#236','Venerd&#236','Sabato'],
                        dayNamesShort: ['Dom','Lun','Mar','Mer','Gio','Ven','Sab'],
                        dayNamesMin: ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                        dateFormat: 'dd/mm/yyyy',
                        firstDay: 1,
                        prevText: '&#x3c;Prec', prevStatus: '',
                        prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                        nextText: 'Succ&#x3e;', nextStatus: '',
                        nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                        currentText: 'Oggi', currentStatus: '',
                        todayText: 'Oggi', todayStatus: '',
                        clearText: '-', clearStatus: '',
                        closeText: 'Chiudi', closeStatus: '',
                        yearStatus: '', monthStatus: '',
                        weekText: 'Sm', weekStatus: '',
                        dayStatus: 'DD d MM',
                        defaultStatus: '',
                        isRTL: false,
			            showOn: ""focus"",
                        dateFormat: 'dd/mm/yy',
                        changeMonth: true,
                        changeYear: true,
                        yearRange: """ + initYear + ":" + endYear + @"""			           			            
		            });
	            });";


                }
                else
                {
                    string minDate = (WidgetMinDate != null) ? "minDate: " + WidgetMinDate + "," : "";
                    string maxDate = (WidgetMaxDate != null) ? "maxDate: " + WidgetMaxDate + "," : "";

                    str_script = @"
                $(document).ready(function() {    
                    $('#" + this.Key + @"').datetimepicker({
                        renderer: $.ui.datepicker.defaultRenderer,
                        monthNames: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno',
                                'Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],
                        monthNamesShort: ['Gen','Feb','Mar','Apr','Mag','Giu',
                                'Lug','Ago','Set','Ott','Nov','Dic'],
                        dayNames: ['Domenica','Luned&#236','Marted&#236','Mercoled&#236','Gioved&#236','Venerd&#236','Sabato'],
                        dayNamesShort: ['Dom','Lun','Mar','Mer','Gio','Ven','Sab'],
                        dayNamesMin: ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                        dateFormat: 'dd/mm/yyyy',
                        firstDay: 1,
                        prevText: '&#x3c;Prec', prevStatus: '',
                        prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                        nextText: 'Succ&#x3e;', nextStatus: '',
                        nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                        currentText: 'Oggi', currentStatus: '',
                        todayText: 'Oggi', todayStatus: '',
                        clearText: '-', clearStatus: '',
                        closeText: 'Chiudi', closeStatus: '',
                        yearStatus: '', monthStatus: '',
                        weekText: 'Sm', weekStatus: '',
                        dayStatus: 'DD d MM',
                        defaultStatus: '',
                        isRTL: false,
                            showOn: ""focus""," +
                                // " + minDate + @"
                                // " + maxDate + @"
                                @"dateFormat: 'dd/mm/yy',
                            changeMonth: true,
                            changeYear: true,
                            yearRange: """ + initYear + ":" + endYear + @"""			                
                    });
                });";

                    //			                buttonImage: """ + Page.ClientScript.GetWebResourceUrl(this.GetType(), "ValidatedFields.ValidatedFields.Fields.Calendar.Images.calendar.png") + @""",

                }

                #endregion

            }
        

            script.Controls.Add(new LiteralControl(str_script));

            this.Controls.Add(script);
            //this.Controls.Add(TextBox);
        }

    }
}
