﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using Uploadify;
using System.Collections;
using System.Collections.Generic;
using NetService.Utility.UI;
using System.Linq;


namespace NetService.Utility.ValidatedFields
{
    
    public class VfUploadify : VfGeneric
    {
        public override WebControl Field
        {
            get {
                return Uploadify;                 
            }
        }

        private FieldMode _Mode = FieldMode.FileSystem;
        public FieldMode Mode
        {
            get
            {
                return _Mode;
            }
            set
            {
                _Mode = value;
            }
        }

        public enum FieldMode
        {
            FileSystem,
            Database
        }

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set { _SearchComparisonCriteria = value; }
        }
        private RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = RecordsFinder.Finder.ComparisonCriteria.Like;

        public override NetService.Utility.Common.RequestVariable Request
        {
            get
            {
                if (_Request == null)
                {
                    _Request = new NetService.Utility.Common.RequestVariable("file_list_" + Key, NetService.Utility.Common.RequestVariable.RequestType.Form_QueryString);
                }
                return _Request;
            }
        }
        private NetService.Utility.Common.RequestVariable _Request;


        public override object PostbackValueObject
        {
            get
            {
                NetService.Utility.Common.RequestVariable FileList = new Common.RequestVariable("file_list_" + this.Key);
                switch (Mode)
                {
                    case FieldMode.FileSystem:
                        
                            IList<Uploadify.FileUploaded> FilesList = new List<Uploadify.FileUploaded>();
                            
                            if (FileList.IsValidString)
                            {
                                char split_simbol = ';';
                                string[] allegati = FileList.StringValue.Split(split_simbol);
                                if (allegati.Length > 0)
                                {
                                    foreach (string allegato in allegati)
                                    {
                                        if (allegato.Length > 0)
                                        {
                                            FilesList.Add(new FileUploaded(allegato.Replace("''","'")));
                                        }
                                    }
                                }
                            }
                            return FilesList;
                        
                    case FieldMode.Database:

                        List<IDictionary> dizionario = new List<IDictionary>();

                        if (FileList.IsValidString)
                        {
                            char split_simbol = ';';
                            string[] allegati = FileList.StringValue.Split(split_simbol);
                            if (allegati.Length > 0)
                            {
                                foreach (string allegato in allegati)
                                {
                                    if (allegato.Length > 0)
                                    {
                                        IDictionary diz = new Hashtable();
                                        diz.Add("ID", null);
                                        //diz.Add("File", FileToByteArray(UploadFolder + "/" + allegato));
                                        diz.Add("File", FileToArrayByte(UploadFolder + "/" + allegato));
                                        diz.Add("FileName", allegato.Replace("''", "'"));
                                        diz.Add("ContentType", GetContentType(UploadFolder + "/" + allegato));
                                        dizionario.Add(diz);
                                    }
                                }
                            }
                        }

                        if (this.DefaultValue != null)
                        {
                            foreach (Hashtable file in this.DefaultValue as IList)
                                dizionario.Add(file);
                        }

                        return dizionario;
                }
                return null;
            }
        }

        public byte[] FileToByteArray(string filePath)
        {
            byte[] _Buffer = null;

            filePath = System.Web.HttpContext.Current.Server.MapPath(filePath);

            try
            {
                // Open file for reading
                System.IO.FileStream _FileStream = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                
                // attach filestream to binary reader
                System.IO.BinaryReader _BinaryReader = new System.IO.BinaryReader(_FileStream);

                // get total byte length of the file
                long _TotalBytes = new System.IO.FileInfo(filePath).Length;

                // read entire file into buffer
                _Buffer = _BinaryReader.ReadBytes((Int32)_TotalBytes);

                // close file reader
                _FileStream.Close();
                _FileStream.Dispose();
                _BinaryReader.Close();
            }
            catch (Exception _Exception)
            {
                // Error
            }

            return _Buffer;
        }

        public byte[] FileToArrayByte(string filePath)
        {
            byte[] fileData = null;

            string fileMapPath = System.Web.HttpContext.Current.Server.MapPath(filePath);

            using (System.IO.FileStream fs = new System.IO.FileStream(fileMapPath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                fileData = new byte[fs.Length];
                // read in the file stream to the byte array
                fs.Read(fileData, 0, System.Convert.ToInt32(fs.Length));
                // close the file stream
                fs.Close();
            }

            return fileData;
        }


        private string GetContentType(string filePath)
        {
            filePath = System.Web.HttpContext.Current.Server.MapPath(filePath);

            string contentType = "application/octetstream";
            string ext = System.IO.Path.GetExtension(filePath).ToLower();

            Microsoft.Win32.RegistryKey registryKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);

            if (registryKey != null && registryKey.GetValue("Content Type") != null)
            {
                contentType = registryKey.GetValue("Content Type").ToString();                
            }
            return contentType;
        }

        //private void GetFileList() 
        //{
        //    NetService.Utility.Common.RequestVariable FileList = new Common.RequestVariable("file_list_" + this.Key);
        //    if (FileList.IsValidString)
        //    {
        //        char split_simbol = ';';
        //        string[] allegati = FileList.StringValue.Split(split_simbol);
        //         if (allegati.Length > 0)
        //         {
        //             foreach (string allegato in allegati) 
        //             {
        //                 if (allegato.Length > 0)
        //                _FilesList.Add(new FileUploaded(allegato));
        //             }
        //         }
        //    }
        //}

        public bool ShowLabelAsHint
        {
            get
            {
                return _ShowLabelAsHint;
            }
            set
            {
                _ShowLabelAsHint = value;
            }
        }
        private bool _ShowLabelAsHint;

        private IList<Uploadify.FileUploaded> filesList;        

        public override string LocalCssClass
        {
            get
            {
                return "uploadify-vf";
            }
        }

        private Uploadify.Uploadify _Uploadify;
        public Uploadify.Uploadify Uploadify
        {
            get
            {
                if (_Uploadify == null)
                {
                    _Uploadify = new Uploadify.Uploadify(this.Key, FileType, FileDesc, UploadButtonText, UploadFileHandlerPath,DeleteFileHandlerPath, UploadFolder,SwfObjectFilePath,UploadifyScriptsFolderPath, Multi, Auto);
                    _Uploadify.QueueSizeLimit = QueueSizeLimit;
                    _Uploadify.UploadLimit = UploadLimit;
                }
                return _Uploadify;
            }
        }

        //public override string ID
        //{
        //    get
        //    {
        //        return base.ID;
        //    }
        //    set
        //    {
        //        base.ID = value;
        //    }
        //}

        #region Proprietà del controllo Uploadify
        private string _FileType;
        /// <summary>
        /// Get or Set File types allowed to upload: "*.doc; *.docx; *.pdf; *.p7m; *.rtf"
        /// </summary>
        public string FileType
        {
            get { return _FileType; }
            set { _FileType = value; }
        }

        private string _FileDesc;
        /// <summary>
        /// Get or Set File description: "File accettati: *.doc; *.docx; *.pdf; *.p7m; *.rtf"
        /// </summary>
        public string FileDesc
        {
            get { return _FileDesc; }
            set { _FileDesc = value; }
        }

        private string _UploadButtonText;
        /// <summary>
        /// Get or set label of submit button
        /// </summary>
        public string UploadButtonText
        {
            get { return _UploadButtonText; }
            set { _UploadButtonText = value; }
        }

        private string _UploadFileHandlerPath;
        /// <summary>
        /// Get or set absolute path of ashx upload module script
        /// </summary>
        public string UploadFileHandlerPath
        {
            get { return _UploadFileHandlerPath; }
            set { _UploadFileHandlerPath = value; }
        }

        private string _UploadDeleteFilePath;
        /// Get or set relative path of aspx delete page
        public string UploadDeleteFilePath
        {
            get { return _UploadDeleteFilePath; }
            set { _UploadDeleteFilePath = value; }
        }

        /// <summary>
        /// Percorso relativo dell'handler che effettua l'eliminazione
        /// </summary>
        private string _DeleteFileHandlerPath;
        public string DeleteFileHandlerPath
        {
            get
            {
                return _DeleteFileHandlerPath;
            }
            set
            {
                _DeleteFileHandlerPath = value;
            }
        }

        private string _UploadFolder;
        /// <summary>
        /// Get or set absolute path where upload files
        /// </summary>
        public string UploadFolder
        {
            get { return _UploadFolder; }
            set { _UploadFolder = value; }
        }

        private bool _Multi;
        public bool Multi
        {
            get { return _Multi; }
            set { _Multi = value; }
        }

        private bool _Auto;
        public bool Auto
        {
            get { return _Auto; }
            set { _Auto = value; }
        }

        private int _QueueSizeLimit;
        public int QueueSizeLimit 
        {
            get { return _QueueSizeLimit; }
            set { _QueueSizeLimit = value; }
        }

        private int _UploadLimit;
        public int UploadLimit
        {
            get { return _UploadLimit; }
            set { _UploadLimit = value; }
        }

        private bool _AllowDelete;
        public bool AllowDelete
        {
            get { return _AllowDelete; }
            set { _AllowDelete = value; }
        }

        private string _swfObjectFilePath;
        /// <summary>
        /// Percorso relativo del file swfobject.js
        /// </summary>
        public string SwfObjectFilePath
        {
            get { return _swfObjectFilePath; }
            set { _swfObjectFilePath = value; }
        }

        //private string _jqueryFilePath;
        ///// <summary>
        ///// Percorso relativo del file jquery.js
        ///// </summary>
        //public string JqueryFilePath
        //{
        //    get { return _jqueryFilePath; }
        //    set { _jqueryFilePath = value; }
        //}

        private string _uploadifyScriptFolder;
        /// <summary>
        /// Percorso relativo della cartella contenente i file necessari al widget uploadify
        /// </summary>
        public string UploadifyScriptsFolderPath
        {
            get { return _uploadifyScriptFolder; }
            set { _uploadifyScriptFolder = value; }
        }

        #endregion

        #region Costruttori
        
        /// <summary>
        /// VfUploadify: field per l'upload di file con la progress bar che fa uso plugin jquey uploadify
        /// </summary>
        /// <param name="id">Identificativo</param>
        /// <param name="labelResourceKey">Etichetta da mostrare</param>
        /// <param name="fileType">Estensioni di file accettate es.: *.doc; *.docx; *.pdf;</param>
        /// <param name="fileDesc">Descrizione mostrata nel pannello di selezione es.: File accettati: *.doc; *.docx; *.pdf;</param>
        /// <param name="uploadButtonText">Testo del bottone di scelta del/i file da trasferire</param>
        /// <param name="uploadFileHandlerPath">Percorso relativo dell'handler ashx di upload</param>
        /// <param name="deleteFileHandlerPath">Percorso relativo dell'handler ashx di eliminazione</param>        
        /// <param name="uploadFolder">Percorso relativo della cartella per il salvataggio dei file</param>
        /// <param name="swfobjectFilePath">Percorso relativo allo script swfobject.js</param>
        /// <param name="uploadifyScriptsFolderPath">Percorso relativo alla cartella contente gli script di uploadify</param>
        /// <param name="multi">Traferimento di file multipli</param>
        /// <param name="auto">Traferimento automatico del/i file</param>
        public VfUploadify(string id, 
            string labelResourceKey, 
            string fileType, 
            string fileDesc, 
            string uploadButtonText, 
            string uploadFileHandlerPath,
            string deleteFileHandlerPath,
            string uploadFolder,
            string swfobjectFilePath,            
            string uploadifyScriptsFolderPath,
            bool multi, 
            bool auto)
            : base(id, labelResourceKey)
        {
            FileType = fileType;
            FileDesc = fileDesc;
            UploadButtonText = uploadButtonText;
            UploadFileHandlerPath = uploadFileHandlerPath;
            DeleteFileHandlerPath = deleteFileHandlerPath;
            UploadFolder = uploadFolder;
            SwfObjectFilePath = swfobjectFilePath;            
            UploadifyScriptsFolderPath = uploadifyScriptsFolderPath;
            Auto = auto;
            Multi = multi;
        }

        /// <summary>
        /// VfUploadify: field per l'upload di file con la progress bar che fa uso plugin jquey uploadify
        /// </summary>
        /// <param name="id">Identificativo</param>
        /// <param name="labelResourceKey">Etichetta da mostrare</param>
        /// <param name="activeRecordPropertyName">Identificativo per il mapping dell'oggetto</param>
        /// <param name="fileType">Estensioni di file accettate es.: *.doc; *.docx; *.pdf;</param>
        /// <param name="fileDesc">Descrizione mostrata nel pannello di selezione es.: File accettati: *.doc; *.docx; *.pdf;</param>
        /// <param name="uploadButtonText">Testo del bottone di scelta del/i file da trasferire</param>
        /// <param name="uploadFileHandlerPath">Percorso relativo dell'handler ashx di upload</param>
        /// <param name="deleteFileHandlerPath">Percorso relativo dell'handler ashx di eliminazione</param>
        /// <param name="uploadFolder">Percorso relativo della cartella per il salvataggio dei file</param>  
        /// <param name="swfobjectFilePath">Percorso relativo allo script swfobject.js</param>
        /// <param name="uploadifyScriptsFolderPath">Percorso relativo alla cartella contente gli script di uploadify</param>
        /// <param name="multi">Traferimento di file multipli</param>
        /// <param name="auto">Traferimento automatico del/i file</param>
        public VfUploadify(string id, 
            string labelResourceKey, 
            string activeRecordPropertyName, 
            string fileType, 
            string fileDesc, 
            string uploadButtonText, 
            string uploadFileHandlerPath,
            string deleteFileHandlerPath,
            string uploadFolder,
            string swfobjectFilePath,            
            string uploadifyScriptsFolderPath,
            bool multi, 
            bool auto)
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
            FileType = fileType;
            FileDesc = fileDesc;
            UploadButtonText = uploadButtonText;
            UploadFileHandlerPath = uploadFileHandlerPath;
            DeleteFileHandlerPath = deleteFileHandlerPath;
            UploadFolder = uploadFolder;
            SwfObjectFilePath = swfobjectFilePath;            
            UploadifyScriptsFolderPath = uploadifyScriptsFolderPath;
            Auto = auto;
            Multi = multi;
        }

        /// <summary>
        /// VfUploadify: field per l'upload di file con la progress bar che fa uso plugin jquey uploadify
        /// </summary>
        /// <param name="id">Identificativo</param>
        /// <param name="labelResourceKey">Etichetta da mostrare</param>
        /// <param name="activeRecordPropertyName">Identificativo per il mapping dell'oggetto</param>
        /// <param name="fileType">Estensioni di file accettate es.: *.doc; *.docx; *.pdf;</param>
        /// <param name="fileDesc">Descrizione mostrata nel pannello di selezione es.: File accettati: *.doc; *.docx; *.pdf;</param>
        /// <param name="uploadButtonText">Testo del bottone di scelta del/i file da trasferire</param>
        /// <param name="uploadFileHandlerPath">Percorso relativo dell'handler ashx di upload</param>
        /// <param name="deleteFileHandlerPath">Percorso relativo dell'handler ashx di eliminazione</param>
        /// <param name="uploadFolder">Percorso relativo della cartella per il salvataggio dei file</param>
        /// <param name="swfobjectFilePath">Percorso relativo allo script swfobject.js</param>
        /// <param name="uploadifyScriptsFolderPath">Percorso relativo alla cartella contente gli script di uploadify</param>
        /// <param name="multi">Traferimento di file multipli</param>
        /// <param name="auto">Traferimento automatico del/i file</param>
        public VfUploadify(string id,
            string labelResourceKey, 
            string activeRecordPropertyName, 
            string fileType, 
            string fileDesc, 
            string uploadButtonText, 
            string uploadFileHandlerPath,
            string deleteFileHandlerPath,
            string uploadFolder,
            string swfobjectFilePath,            
            string uploadifyScriptsFolderPath,
            bool multi, 
            bool auto, 
            FieldMode mode)
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
            FileType = fileType;
            FileDesc = fileDesc;
            UploadButtonText = uploadButtonText;
            UploadFileHandlerPath = uploadFileHandlerPath;
            DeleteFileHandlerPath = deleteFileHandlerPath;
            UploadFolder = uploadFolder;
            SwfObjectFilePath = swfobjectFilePath;            
            UploadifyScriptsFolderPath = uploadifyScriptsFolderPath;
            Auto = auto;
            Multi = multi;
            Mode = mode;
        } 
        #endregion
        
        protected override void FillFieldValue()
        {
            //
            //this.filesList = (IList<FileUploaded>)this.DefaultValue;
            this.Uploadify.FileListPostBackValue = Request.StringValue;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }

        protected override void LocalValidate()
        {
          
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);


            this.Controls.Add(GetFileAllegati());

            if (this.ShowLabelAsHint)
            {
                this.CssClass += " hint";
                this.Uploadify.Attributes.Add("title", this.Label);
            }

            NetService.Utility.Common.RequestVariable FileList = new Common.RequestVariable("file_list_" + this.Key);
            if (FileList.IsValidString)
                this.Uploadify.FileListPostBackValue = FileList.StringValue;
        
            this.Controls.Add(Uploadify);

            if (CheckNumAttachs())
                this.Controls.Add(AddDisableBtnScript());

        }

        private WebControl GetFileAllegati() 
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "div_allegati";

            WebControl lista;

            //if (filesList != null && this.filesList.Count > 0)
            if (this.DefaultValue != null)
            {
                lista = new WebControl(HtmlTextWriterTag.Ul);
                WebControl li;
                WebControl p;

                switch (Mode)
                {
                    case FieldMode.FileSystem:
                        foreach (FileUploaded file in (IList<FileUploaded>)this.DefaultValue)
                        {
                            li = new WebControl(HtmlTextWriterTag.Li);
                            li.CssClass = "file " + file.Ext;

                            string strCrtl = "<span class=\"name\">" + file.FileName + "</span>";

                            if (AllowDelete)
                            {
                                strCrtl += "<span class=\"del\">";
                                strCrtl += "<a href=\"" + UploadDeleteFilePath + (UploadDeleteFilePath.Contains("?") ? "&" : "?") + "action=allegatoDelete&allegato=";
                                strCrtl += file.ID + "\">Rimuovi</a></span>";
                            }

                            p = new WebControl(HtmlTextWriterTag.P);
                            p.Controls.Add(new LiteralControl(strCrtl));
                            p.CssClass = "fileinfo";

                            li.Controls.Add(p);
                            lista.Controls.Add(li);
                        }
                        break;
                    case FieldMode.Database:
                        foreach (Hashtable file in (IList)this.DefaultValue)
                        {
                            li = new WebControl(HtmlTextWriterTag.Li);
                            //li.CssClass = "file " + file.Ext;

                            string strCrtl = "<span class=\"name\">" + file["FileName"] + "</span>";

                            if (AllowDelete)
                            {
                                strCrtl += "<span class=\"del\">";
                                strCrtl += "<a href=\"" + UploadDeleteFilePath + (UploadDeleteFilePath.Contains("?") ? "&" : "?") + "action=allegatoDelete&allegato=";
                                strCrtl += file["ID"] + "\">Rimuovi</a></span>";
                            }

                            p = new WebControl(HtmlTextWriterTag.P);
                            p.Controls.Add(new LiteralControl(strCrtl));
                            p.CssClass = "fileinfo";

                            li.Controls.Add(p);
                            lista.Controls.Add(li);
                        }
                        break;                   
                }
                
            }
            else
            { 
                lista = new WebControl(HtmlTextWriterTag.P);
                lista.Controls.Add(new LiteralControl("Nessun allegato presente"));
            }

            div.Controls.Add(lista);

            return div;
        }

        public bool EliminationRequest
        {
            get
            {
                NetService.Utility.Common.RequestVariable request = new NetService.Utility.Common.RequestVariable("action", NetService.Utility.Common.RequestVariable.RequestType.QueryString);
                if (request.IsValidString && request.StringValue == "allegatoDelete")
                    _EliminationRequest = true;
                return _EliminationRequest;
            }
        }
        private bool _EliminationRequest = false;

        private EliminationConfirmControl _DeleteControl;
        public EliminationConfirmControl DeleteControl 
        {
            get 
            {
                if (_DeleteControl == null)
                _DeleteControl = new EliminationConfirmControl("Conferma eliminazione allegato atto", "Sei sicuro di voler eliminare l'allegato", UploadDeleteFilePath);
               
                return _DeleteControl;
            }
            set {
                _DeleteControl = value;
            } 
        }

        private WebControl AddDisableBtnScript()
        {
            WebControl script = new WebControl(HtmlTextWriterTag.Script);
            script.Attributes["type"] = "text/javascript";
            script.Controls.Add(new LiteralControl(
                                @"$(document).ready(function () {
                                   // $(""#uploadify_" + this.Key + @""").uploadify('disable', true);
                                    $(""#uploadify_" + this.Key + @""").hide();
                                });"));
            return script;
        }
        private bool CheckNumAttachs()
        {
            int numAttachs = -1;

            switch (Mode)
            {
                case FieldMode.FileSystem:
                    IList<FileUploaded> fileAllegati = (IList<FileUploaded>)this.DefaultValue;
                    if (fileAllegati != null && fileAllegati.Count > 0)
                        numAttachs = fileAllegati.Count;
                    break;
                case FieldMode.Database:
                    // cambiata la conversione del defaultValue, commentato quello che c'era prima da List<IDictionary> a List<Hashtable>
                    //List<IDictionary> allegati = (List<IDictionary>)this.DefaultValue ;
                    List<Hashtable> allegati = (List<Hashtable>)this.DefaultValue;                    
                    if (allegati != null && allegati.Count > 0)
                        numAttachs = allegati.Count;
                    break;
            }

            if (numAttachs > 0 && !Multi)
                return true;
            return false;
        }
    }
}
