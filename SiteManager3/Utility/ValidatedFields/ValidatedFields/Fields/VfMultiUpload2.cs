﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetService.Utility.RecordsFinder;
using System.Web;
using System.IO;
using static NetService.Utility.ValidatedFields.Uploader;
using ValidatedFields.Handler;

namespace NetService.Utility.ValidatedFields
{
    public class VfMultiUpload2 : VfGeneric
    {
        private bool _AllowDelete = true;
        public bool AllowDelete
        {
            get { return _AllowDelete; }
            set { _AllowDelete = value; }
        }

        public override WebControl Field
        {
            get
            {
                return MainControl;
            }
        }

        private string _FileListPostBackValue;
        public string FileListPostBackValue
        {
            get { return _FileListPostBackValue; }
            set { _FileListPostBackValue = value; }
        }

        private WebControl _MainControl;
        public WebControl MainControl
        {

            get
            {
                if (_MainControl == null)
                {
                    _MainControl = new WebControl(HtmlTextWriterTag.Div);
                    _MainControl.CssClass = "maincontroluploadify";


                    if (this.FileList != null && DisplayFileList || this.filesList != null && DisplayFileList)
                    {
                        WebControl fileContainer = new WebControl(HtmlTextWriterTag.Div);
                        fileContainer.ID = "filesuploaded" + "_" + this.CtrlID;                        

                        WebControl fileL = new WebControl(HtmlTextWriterTag.Div);
                        fileL.CssClass = "fileslist_header";
                        fileL.Controls.Add(new LiteralControl("<div class=\"title_header\">Allegati</div>"));
                        fileContainer.Controls.Add(fileL);

                        WebControl list = new WebControl(HtmlTextWriterTag.Ul);
                        list.ID = "filelist_ID" + "_" + this.CtrlID;
                        //list.ID = "filelist_" + this.CtrlID;
                        list.CssClass = Uploader.ID + "filelist";

                        switch (Mode)
                        {
                            case FieldMode.FileSystem:
                                {
                                    foreach (IDictionary sf in FileList)
                                    {
                                        string[] fileArray = sf["FullPath"].ToString().Split('\\');
                                        int i = fileArray.Length;
                                        string filename = fileArray.GetValue(i - 1).ToString();
                                        WebControl li = new WebControl(HtmlTextWriterTag.Li);

                                        WebControl link = new WebControl(HtmlTextWriterTag.A);
                                        link.Attributes.Add("onclick", "javascript:deleteFile" + this.CtrlID + @"('" + filename + "')");
                                        link.Attributes.Add("style", "cursor:pointer");
                                        WebControl img_x = new WebControl(HtmlTextWriterTag.Img);
                                        img_x.Attributes.Add("src", "/cms/css/admin/images/icons/delete-icon.png");
                                        link.Controls.Add(img_x);
                                        li.Controls.Add(link);

                                        li.Controls.Add(new LiteralControl("<span>" + filename + "</span>"));

                                        if (AllowDelete)
                                        {
                                            string modal = @"<script>$(function() {
                                   $('#" + this.CtrlID + @"dialogDelete').dialog({
                                        autoOpen:false,
                                        hide: {
                                             effect: 'explode',
                                             duration: 500
                                        },
                                        closeOnEscape: false,
                                        height : 300,
                                        width : 'auto',
                                        resizable: false,
                                        draggable: false,
                                        modal: true,
                                        title: 'Eliminazione Allegato',
                                        buttons: {
                                                  Conferma: function () {                                                          
                                                      var ok= new Boolean(true);
                                                      var rege = /^[_àèéìòù0-9a-zA-Z\'\,\.\- ]*$/;

                                                      if(ok)
                                                      {
														 $('#" + this.CtrlID + @"deleteButton').click();													
                                                      }                                                  
                                                },
                                                Annulla: function (){
                                                  $('#div_errore_nv').hide();  
                                                  $('#div_errore_regex_rg').hide();  
                                                  $(this).dialog('close');
                                                }
                                        }
                                    });
			                    });</script>";

                                            string script2 = @"<script>function deleteFile(filename){  
                                        $('#" + this.CtrlID + @"filename').val(filename);
                                        $('#" + this.CtrlID + @"dialogDelete').dialog('open');
                                        $('#" + this.CtrlID + @"dialogDelete').parent().appendTo($('form:first'));                                        
                                  } </script>";

                                            base.Page.Header.Controls.Add(new LiteralControl(modal));
                                            base.Page.Header.Controls.Add(new LiteralControl(script2));
                                        }

                                        list.Controls.Add(li);
                                    }
                                }
                                break;
                            case FieldMode.FileSystemWithJavascript:
                                {
                                    string strFileUploadedList = string.Empty;

                                    foreach (FileUploadedV2 file in this.filesList)
                                    {
                                        WebControl li = new WebControl(HtmlTextWriterTag.Li);
                                        li.CssClass = "file file-"+ file.ID + " " + file.Ext.ToLower();

                                        WebControl p = new WebControl(HtmlTextWriterTag.P);
                                        p.CssClass = "fileinfo";

                                        p.Controls.Add(new LiteralControl("<span class=\"filename\">" + file.FileName + "</span>"));

                                        strFileUploadedList += file.FileName + ";";

                                        if (AllowDelete)
                                        {

                                            WebControl link = new WebControl(HtmlTextWriterTag.A);
                                            link.Attributes.Add("onclick", "javascript:removeFile" + this.CtrlID + @"('" + file.ID + "','" + file.FileName + "')");
                                            link.Attributes.Add("style", "cursor:pointer");

                                            WebControl img_x = new WebControl(HtmlTextWriterTag.Img);
                                            img_x.Attributes.Add("src", "/scripts/shared/plupload/icons/trash.gif");
                                            link.Controls.Add(img_x);

                                            p.Controls.Add(link);


                                            string modal = @"<script>$(function() {
                                   $('#" + this.CtrlID + @"dialogDelete').dialog({
                                        autoOpen:false,
                                        hide: {
                                             effect: 'explode',
                                             duration: 500
                                        },
                                        closeOnEscape: false,
                                        height : 300,
                                        width : 'auto',
                                        resizable: false,
                                        draggable: false,
                                        modal: true,
                                        title: 'Eliminazione Allegato',
                                        buttons: {
                                                  Conferma: function () {                                                          
                                                      var ok= new Boolean(true);
                                                      if(ok)
                                                      {
                                                        var fileID = $('#"+ this.CtrlID + @"_fileid').val();
                                                        var uploadedFiles = $('#uploadedfiles_" + this.CtrlID + @"').val();
                                                        var selected_filename = $('#" + this.CtrlID + @"filename').val();                                                       

                                                        $('#filelist_ID_" + this.CtrlID + @" .file-' + fileID).remove();

                                                        if(uploadedFiles != undefined){
                                                            uploadedFiles = uploadedFiles.split(';')
                                                            uploadedFiles.splice($.inArray(selected_filename, uploadedFiles),1);
                                                            $('#uploadedfiles_" + this.CtrlID + @"').val(uploadedFiles.join(';'));
                                                        }
                                                        $(this).dialog('close');
                                                      }                                                  
                                                },
                                                Annulla: function (){
                                                  $('#div_errore_nv').hide();  
                                                  $('#div_errore_regex_rg').hide();  
                                                  $(this).dialog('close');
                                                }
                                        }
                                    });
			                    });</script>";

                                            string script2 = @"<script>
                                                            function removeFile" + this.CtrlID + @"(fileid,filename){  

                                                                var msg = $('.deleteFileMessage" + this.CtrlID + @"').text();
                                                                $('.deleteFileMessage" + this.CtrlID + @"').html('');

                                                                $('#" + this.CtrlID + @"filename').val(filename);
                                                                $('#" + this.CtrlID + @"_fileid').val(fileid);
                                                                $('#" + this.CtrlID + @"dialogDelete').dialog('open');
                                                                $('#" + this.CtrlID + @"dialogDelete').parent().appendTo($('form:first'));                                        

                                                                
                                                                var msgFilled = msg.replace('{{filename}}', '<strong>' + filename + '</strong>');
                                                                
                                                                $('.deleteFileMessage" + this.CtrlID + @"').html(msgFilled);
                                                                

                                                        } </script>";

                                            // verificare se esiste un modo per testare la presenza degli script
                                            base.Page.Header.Controls.Add(new LiteralControl(modal));
                                            base.Page.Header.Controls.Add(new LiteralControl(script2));
                                        }

                                        li.Controls.Add(p);
                                        list.Controls.Add(li);
                                    }

                                    fileContainer.Controls.Add(new LiteralControl("<input id=\"uploadedfiles_" + this.Key + "\" name=\"uploadedfiles_" + this.Key + "\" type=\"hidden\" value=\"" + strFileUploadedList + "\" />"));
                                }
                                break;
                            case FieldMode.Database:
                                // not implement
                                break;
                            case FieldMode.Hybrid:
                                {
                                    string strFileUploadedList = string.Empty;

                                    foreach (FileUploadedV2 file in this.filesList)
                                    {
                                        WebControl li = new WebControl(HtmlTextWriterTag.Li);
                                        li.CssClass = "file " + file.Ext.ToLower();

                                        WebControl p = new WebControl(HtmlTextWriterTag.P);
                                        p.CssClass = "fileinfo";

                                        p.Controls.Add(new LiteralControl("<span class=\"filename\">" + file.FileName + "</span>"));

                                        strFileUploadedList += file.FileName + ";";

                                        if (AllowDelete)
                                        {

                                            WebControl link = new WebControl(HtmlTextWriterTag.A);
                                            link.Attributes.Add("onclick", "javascript:deleteFile" + this.CtrlID + @"('" + file.ID + "','" + file.FileName + "')");
                                            link.Attributes.Add("style", "cursor:pointer");

                                            WebControl img_x = new WebControl(HtmlTextWriterTag.Img);
                                            img_x.Attributes.Add("src", "/scripts/shared/plupload/icons/trash.gif");
                                            link.Controls.Add(img_x);

                                            p.Controls.Add(link);
                                            
                                        
                                            string modal = @"<script>$(function() {
                                   $('#" + this.CtrlID + @"dialogDelete').dialog({
                                        autoOpen:false,
                                        hide: {
                                             effect: 'explode',
                                             duration: 500
                                        },
                                        closeOnEscape: false,
                                        height : 300,
                                        width : 'auto',
                                        resizable: false,
                                        draggable: false,
                                        modal: true,
                                        title: 'Eliminazione Allegato',
                                        buttons: {
                                                  Conferma: function () {                                                          
                                                      var ok= new Boolean(true);
                                                      var rege = /^[_àèéìòù0-9a-zA-Z\'\,\.\- ]*$/;

                                                      if(ok)
                                                      {
														 $('#" + this.CtrlID + @"deleteButton').click();													
                                                      }                                                  
                                                },
                                                Annulla: function (){
                                                  $('#div_errore_nv').hide();  
                                                  $('#div_errore_regex_rg').hide();  
                                                  $(this).dialog('close');
                                                }
                                        }
                                    });
			                    });</script>";

                                            string script2 = @"<script>
                                                            function deleteFile" + this.CtrlID + @"(fileid,filename){  

                                                                var msg = $('.deleteFileMessage" + this.CtrlID + @"').text();
                                                                $('.deleteFileMessage" + this.CtrlID + @"').html('');

                                                                $('#" + this.CtrlID + @"filename').val(filename);
                                                                $('#" + this.CtrlID + @"_fileid').val(fileid);
                                                                $('#" + this.CtrlID + @"dialogDelete').dialog('open');
                                                                $('#" + this.CtrlID + @"dialogDelete').parent().appendTo($('form:first'));                                        

                                                                
                                                                var msgFilled = msg.replace('{{filename}}', '<strong>' + filename + '</strong>');
                                                                
                                                                $('.deleteFileMessage" + this.CtrlID + @"').html(msgFilled);
                                                                

                                                        } </script>";
                                            
                                            // verificare se esiste un modo per testare la presenza degli script
                                            base.Page.Header.Controls.Add(new LiteralControl(modal));   
                                            base.Page.Header.Controls.Add(new LiteralControl(script2));
                                        }

                                        li.Controls.Add(p);
                                        list.Controls.Add(li);
                                    }

                                    fileContainer.Controls.Add(new LiteralControl("<input id=\"uploadedfiles_" + this.Key + "\" name=\"uploadedfiles_" + this.Key + "\" type=\"hidden\" value=\"" + strFileUploadedList + "\" />"));
                                }
                                break;
                        }

                        fileContainer.Controls.Add(list);
                    
                        _MainControl.Controls.Add(fileContainer);
                    }
                    _MainControl.Controls.Add(Uploader);
                    _MainControl.Controls.Add(new LiteralControl("<input id=\"files_" + this.Key + "\" name=\"files_" + this.Key + "\" type=\"hidden\" value=\"" + FileListPostBackValue + "\" />"));                    
                }
                return _MainControl;
            }
        }

        private FieldMode _Mode = FieldMode.FileSystem;
        public FieldMode Mode
        {
            get
            {
                return _Mode;
            }
            set
            {
                _Mode = value;
            }
        }

        public enum FieldMode
        {
            FileSystem, // modalità solo disco - i file sono salvati in un path e devono essere gestiti manualmente dall'utilizzatore  

            FileSystemWithJavascript,  // modalità solo disco dove la rimozione dei file avviene solo lato client agendo sul campo hidden xxx
                                       // che contiene la collezione dei file passati in face di popolamente del form nel costrutture del componente,
                                       //in formato json. L'utilizzatore dovrà recuperare l'informazione dal suddetto campo via request e gestirla nel codice locale 
                                        
            Database,   // modalità solo db - i file sono salvati in un campo tipo blob su db e devono essere gestiti come recod
            Hybrid,     // modalità ibrida - i file sono salvati in un path e gli estremi filename, ext, e id vanno salvati su db 
                        // e il componente si fa carico della gestione: move, delete etc...
        }

        private WebControl GetFileAllegati()
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "div_allegati";

            WebControl lista;

            //if (filesList != null && this.filesList.Count > 0)
            if (this.DefaultValue != null)
            {
                lista = new WebControl(HtmlTextWriterTag.Ul);
                WebControl li;
                WebControl p;

                switch (Mode)
                {
                    case FieldMode.FileSystem:
                        foreach (FileUploadedV2 file in (IList<FileUploadedV2>)this.DefaultValue)
                        {
                            li = new WebControl(HtmlTextWriterTag.Li);
                            li.CssClass = "file " + file.Ext;

                            string strCrtl = "<span class=\"name\">" + file.FileName + "</span>";

                            if (AllowDelete)
                            {
                                strCrtl += "<span class=\"del\">";
                                //strCrtl += "<a href=\"" + UploadDeleteFilePath + (UploadDeleteFilePath.Contains("?") ? "&" : "?") + "action=allegatoDelete&allegato=";
                                strCrtl += "<a href=\"" + "action=allegatoDelete&allegato=";
                                strCrtl += file.ID + "\">Rimuovi</a></span>";
                            }

                            p = new WebControl(HtmlTextWriterTag.P);
                            p.Controls.Add(new LiteralControl(strCrtl));
                            p.CssClass = "fileinfo";

                            li.Controls.Add(p);
                            lista.Controls.Add(li);
                        }
                        break;
                    case FieldMode.Database:
                        foreach (Hashtable file in (IList)this.DefaultValue)
                        {
                            li = new WebControl(HtmlTextWriterTag.Li);
                            //li.CssClass = "file " + file.Ext;

                            string strCrtl = "<span class=\"name\">" + file["FileName"] + "</span>";

                            if (AllowDelete)
                            {
                                strCrtl += "<span class=\"del\">";
                                //strCrtl += "<a href=\"" + UploadDeleteFilePath + (UploadDeleteFilePath.Contains("?") ? "&" : "?") + "action=allegatoDelete&allegato=";
                                strCrtl += "<a href=\"" + "action=allegatoDelete&allegato=";
                                strCrtl += file["ID"] + "\">Rimuovi</a></span>";
                            }

                            p = new WebControl(HtmlTextWriterTag.P);
                            p.Controls.Add(new LiteralControl(strCrtl));
                            p.CssClass = "fileinfo";

                            li.Controls.Add(p);
                            lista.Controls.Add(li);
                        }
                        break;
                }

            }
            else
            {
                lista = new WebControl(HtmlTextWriterTag.P);
                lista.Controls.Add(new LiteralControl("Nessun allegato presente"));
            }

            div.Controls.Add(lista);

            return div;
        }

        private string _CtrlID;
        public string CtrlID
        {
            get { return _CtrlID; }
            set { _CtrlID = value; }
        }

        private bool FilterFiles
        {
            get;
            set;
        }
        private List<string> FilenamesFilerFiles
        {
            get;
            set;
        }

        private List<IDictionary> _FileList;
        /// <summary>
        /// Files presenti nella cartella "UploadFolder"
        /// </summary>
        public List<IDictionary> FileList
        {
            get
            {
                if (_FileList == null || _FileList.Count == 0)
                {
                    if (System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(this.UploadFolder)))
                    {
                        _FileList = new List<IDictionary>();

                        string[] templist = Directory.GetFiles(HttpContext.Current.Server.MapPath(this.UploadFolder));

                        foreach (string s in templist)
                        {
                            var item = new Hashtable();
                            string[] fileArray = s.Split('\\');
                            int i = fileArray.Length;
                            string filename = fileArray.GetValue(i - 1).ToString();
                            item.Add("FileName", filename);
                            item.Add("FullPath", s);
                            if (!FilterFiles)
                                _FileList.Add(item);
                            else
                            {
                                if (FilenamesFilerFiles.Contains(filename))
                                    _FileList.Add(item);
                            }
                        }
                    }
                    else
                        _FileList = null;

                }
                return _FileList;
            }
            set
            {
                List<IDictionary> _FileList_AllegatiSelection = value;
                if (System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(this.UploadFolder)))
                {
                    _FileList = new List<IDictionary>();
                    string[] templist = Directory.GetFiles(HttpContext.Current.Server.MapPath(this.UploadFolder));

                    foreach (string s in templist)
                    {
                        var item = new Hashtable();
                        string[] fileArray = s.Split('\\');
                        int i = fileArray.Length;
                        string filename = fileArray.GetValue(i - 1).ToString();

                        //if (_FileList_AllegatiSelection.Select(x=>x.Keys == "FileName"))

                        foreach (IDictionary values in _FileList_AllegatiSelection)
                        {
                            if (values["FileName"].ToString() == filename)
                            {
                                item = new Hashtable();
                                item.Add("FileName", filename);
                                item.Add("FullPath", s);
                                _FileList.Add(item);
                            }
                        }
                    }
                }
                else
                    _FileList = null;
            }

        }
        
        /// <summary>
        /// File presenti nella cartella "UploadTempFolder"
        /// </summary>
        public List<IDictionary> FileTempList
        {

            get
            {
                if (_FileTempList == null || _FileTempList.Count == 0)
                {                    
                    if (!String.IsNullOrEmpty(fileList.Value))
                    {
                        _FileTempList = new List<IDictionary>();
                        string[] templist = fileList.Value.Split(',');
                        foreach (string s in templist)
                        {
                            var item = new Hashtable();
                            string[] fileArray = s.Split('\\');
                            int i = fileArray.Length;
                            string filename = fileArray.GetValue(i - 1).ToString();
                            item.Add("FileName", filename);
                            _FileTempList.Add(item);
                        }
                    }
                    else
                        _FileTempList = null;

                }
                return _FileTempList;
            }

        }
        private List<IDictionary> _FileTempList;

        public override string LocalCssClass
        {
            get
            {
                return "multiuploadify-vf2";
            }
        }

        public override NetService.Utility.Common.RequestVariable Request
        {
            get
            {
                if (_Request == null)
                {
                    string requestKey = string.Empty;

                    switch (this.Mode)
                    {
                        case FieldMode.FileSystem:
                            requestKey = fileList.ID;
                            break;
                        case FieldMode.Database:
                            requestKey = fileList.ID;
                            break;
                        case FieldMode.Hybrid:
                            requestKey = "files_" + this.Key;
                            break;
                        default:
                            requestKey = fileList.ID;
                            break;
                    }

                    _Request = new NetService.Utility.Common.RequestVariable(requestKey, NetService.Utility.Common.RequestVariable.RequestType.Form);

                }
                return _Request;
            }
        }
        private NetService.Utility.Common.RequestVariable _Request;

        protected HiddenField fileList
        {
            get
            {
                if (_fileList == null)
                {
                    _fileList = new HiddenField();
                    _fileList.ID = this.Uploader.CtrlId + "fileList";                    
                }
                return _fileList;
            }
        }
        private HiddenField _fileList;

        private string deleteFileMessage = "<strong>ATTENZIONE</strong>: Sei sicuro di voler eliminare l'allegato {{filename}} DEFINITIVAMENTE?";
        public string DeleteFileMessage
        {
            get { return deleteFileMessage; }
            set { deleteFileMessage = value; }
        }

        public override object PostbackValueObject
        {
            get
            {
                NetService.Utility.Common.RequestVariable currentData = new Common.RequestVariable(this.Key);
                IList<FileUploadedV2> FilesList;

                object returnObj = null;

                switch (Mode)
                {
                    case FieldMode.FileSystem:
                        returnObj = FileTempList;
                        break;
                    case FieldMode.Database:
                        List<IDictionary> dizionario = new List<IDictionary>();

                        if (currentData.IsValidString)
                        {
                            char split_simbol = ';';
                            string[] allegati = currentData.StringValue.Split(split_simbol);
                            if (allegati.Length > 0)
                            {
                                foreach (string allegato in allegati)
                                {
                                    if (allegato.Length > 0)
                                    {
                                        IDictionary diz = new Hashtable();
                                        diz.Add("ID", null);                                        
                                        diz.Add("File", FileToArrayByte(UploadFolder + "/" + allegato));
                                        diz.Add("FileName", allegato.Replace("''", "'"));
                                        diz.Add("ContentType", GetContentType(UploadFolder + "/" + allegato));
                                        dizionario.Add(diz);
                                    }
                                }
                            }
                        }
                        returnObj = dizionario;
                        break;
                    case FieldMode.FileSystemWithJavascript:
                        FilesList = new List<FileUploadedV2>();
                        currentData = new Common.RequestVariable("files_" + this.Key);
                        if (currentData.IsValidString)
                        {
                            char split_simbol = ';';
                            string[] allegati = currentData.StringValue.Split(split_simbol);
                            if (allegati.Length > 0)
                            {
                                foreach (string allegato in allegati)
                                {
                                    if (allegato.Length > 0)
                                    {
                                        FilesList.Add(new FileUploadedV2(allegato.Replace("''", "'"))); // è necessario sanificare il nome del file
                                    }
                                }
                            }
                        }
                        returnObj = FilesList;
                        break;
                    case FieldMode.Hybrid:
                        FilesList = new List<FileUploadedV2>();
                        currentData = new Common.RequestVariable("files_" + this.Key);
                        if (currentData.IsValidString)
                        {
                            char split_simbol = ';';
                            string[] allegati = currentData.StringValue.Split(split_simbol);
                            if (allegati.Length > 0)
                            {
                                foreach (string allegato in allegati)
                                {
                                    if (allegato.Length > 0)
                                    {
                                        FilesList.Add(new FileUploadedV2(allegato.Replace("''", "'"))); // è necessario sanificare il nome del file
                                    }
                                }
                            }
                        }
                        returnObj = FilesList;
                        break;
                }
                return returnObj;
            }
        }

        private byte[] FileToArrayByte(string filePath)
        {
            byte[] fileData = null;

            string fileMapPath = System.Web.HttpContext.Current.Server.MapPath(filePath);

            using (System.IO.FileStream fs = new System.IO.FileStream(fileMapPath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                fileData = new byte[fs.Length];
                // read in the file stream to the byte array
                fs.Read(fileData, 0, System.Convert.ToInt32(fs.Length));
                // close the file stream
                fs.Close();
            }

            return fileData;
        }

        private string GetContentType(string filePath)
        {
            filePath = System.Web.HttpContext.Current.Server.MapPath(filePath);

            string contentType = "application/octetstream";
            string ext = System.IO.Path.GetExtension(filePath).ToLower();

            Microsoft.Win32.RegistryKey registryKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);

            if (registryKey != null && registryKey.GetValue("Content Type") != null)
            {
                contentType = registryKey.GetValue("Content Type").ToString();
            }
            return contentType;
        }


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        public virtual void DeleteClickHandler(object sender, EventArgs e)
        {
            try
            {
                switch (Mode)
                {
                    case FieldMode.FileSystem:
                        DeleteFileFromDisk();
                        break;
                    case FieldMode.Database:
                        break;
                    case FieldMode.Hybrid:
                        //DeleteFileFromDB();
                        //DeleteFileFromDisk();
                        break;
                    default:
                        break;
                }
                
            }
            catch (Exception ex)
            {
                // aggiungere inoltro dell'exception
            }
        }

        public bool DeleteFileFromDisk()
        {
            string filename = field.Value;

            string pathorigine = this.UploadTempFolder;
            string pathdestinazione = this.UploadFolder;
            if (File.Exists(HttpContext.Current.Server.MapPath("/" + pathdestinazione) + "\\" + filename))
            {
                File.Delete(HttpContext.Current.Server.MapPath("/" + pathdestinazione) + "\\" + filename);
                return true;
            }
            return false;
        }
      
        //public bool DeleteFileFromDB()
        //{
        //    throw new NotImplementedException();
        //}          
        
      

        public override Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set { _SearchComparisonCriteria = value; }
        }
        private Finder.ComparisonCriteria _SearchComparisonCriteria = Finder.ComparisonCriteria.Like;

        protected override void FillFieldValue()
        {            
            this.FileListPostBackValue = Request.StringValue;
        }

        protected override void LocalValidate()
        {

        }

        /// <summary>
        /// Numero massimo di file, 
        /// Valore di Default 0 (nessun limite)
        /// </summary>
        public int MaxFileNumber
        {
            get { return _maxFileNumber; }
            set { _maxFileNumber = value; }
        }
        private int _maxFileNumber = 0;

        private PlUploader _Uploader;
        protected PlUploader Uploader
        {
            get
            {
                if (_Uploader == null)
                {
                    _Uploader = new PlUploader(
                        this.CtrlID,// + "customUploader", 
                        "/scripts/shared/plupload/js",//"/scripts/shared/plupload-2.3.6/js", 
                        UploadFolder,
                        UploadTempFolder, 
                        Filters, 
                        NetCms.Configurations.PortalData.UploadFileSizeLimit / 1000, 
                        MaxFileNumber,
                       PlUploader.CtrlRuntimes.All);                    
                }
                return _Uploader;

            }

        }

        private string _UploadTempFolder;
        public string UploadTempFolder
        {
            get { return _UploadTempFolder; }
            set { _UploadTempFolder = value; }
        }

        private string _UploadFolder;
        public string UploadFolder
        {
            get { return _UploadFolder; }
            set { _UploadFolder = value; }
        }

        /// <summary>
        /// Booleano (di default a false) per abilitare la visualizzazione degli allegati presenti nella cartella indicata in uploadFolder</param>
        /// </summary>
        public bool DisplayFileList
        {
            get { return _DisplayFileList; }
            set { _DisplayFileList = value; }
        }
        private bool _DisplayFileList;

        private List<PlUpload2FileFilter> _Filters;
        public List<PlUpload2FileFilter> Filters
        {
            get
            {
                if (_Filters == null)
                {
                    _Filters = new List<PlUpload2FileFilter>();
                    _Filters.Add(new PlUpload2FileFilter("Image files", "jpg,jpeg,gif,png"));
                    _Filters.Add(new PlUpload2FileFilter("Document files", "doc,docx,xls,xlsx,pdf,p7f"));
                    _Filters.Add(new PlUpload2FileFilter("Media", "mp3,mp4,wmv,asf"));
                    _Filters.Add(new PlUpload2FileFilter("Zip files", "zip,rar"));
                }
                return _Filters;
            }
            set { _Filters = value; }

        }

        public Button delete
        {
            get
            {
                if (_delete == null)
                {
                    _delete = new Button();
                    _delete.Click += DeleteClickHandler;
                }
                return _delete;
            }
        }
        private Button _delete;

        protected HiddenField field
        {
            get
            {
                if (_field == null)
                {
                    _field = new HiddenField();
                    _field.ID += this.CtrlID + "filename";
                    //_field.ID += this.CtrlID + "filename";
                    //_delete.OnClientClick += new EventHandler(DeleteClickHandler);
                    //_delete.Attributes.Add("style","display:none");
                }
                return _field;
            }
        }
        private HiddenField _field;

        protected HiddenField FileID
        {
            get
            {
                if (_FileID == null)
                {
                    _FileID = new HiddenField();
                    _FileID.ID += this.CtrlID + "_fileid";                 
                }
                return _FileID;
            }
        }
        private HiddenField _FileID;

        #region Costruttori
        /// <summary>
        /// VfMultiUpload
        /// </summary>
        /// <remarks>Si può gestire il trasferimento dei files manualmente oppure richiamare il metodo 'save_click' che non fa altro che trasferire i files dalla cartella temporanea
        /// alla cartella definitiva(uploadFolder).
        /// </remarks>
        /// <param name="id">Id componente (Univoco)</param>
        /// <param name="label">Label</param>
        /// <param name="uploadTempFolder">Path cartella temporanea di upload.Se non settata l'upload verrà effettuato in '/repository/UploadTmpFolder/{yyyyMMddhhmmss}'</param>
        /// <param name="uploadFolder">Path cartella dove verranno trasferiti i file. Se non settata i files verranno trasferiti in '/repository/UploadFolder/{idcomponente}</param>
        /// <param name="filters">Array di stringhe contenente i formati accettati.Lasciando il valora a null accetta tutti i formati principali.</param>
        /// <param name="FilterFiles">Booleano che mi indica la possibilità di selezionare solo alcuni dei documenti contenuti all'interno della folder specificata .</param>
        /// <param name="FilenamesFilerFiles">Lista dei filename dei file da filtrare nel caso in cui il booleano  FilterFiles sia impostato a tue.</param>

        public VfMultiUpload2(string id, string label, string uploadTempFolder = null, string uploadFolder = null, string[] filters = null, int maxfilenumber = 0, bool FilterFiles = false, List<string> FilenamesFilerFiles = null) : base(id, label)
        {

            this.FilterFiles = FilterFiles;
            this.FilenamesFilerFiles = FilenamesFilerFiles;


            if (uploadTempFolder == null)
            {
                UploadTempFolder = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/UploadTmpFolder/" + DateTime.Now.ToString("yyyyMMdd");

            }
            else
            {
                UploadTempFolder = NetCms.Configurations.Paths.AbsoluteRoot + uploadTempFolder;

            }

            if (uploadFolder == null)
            {
                UploadFolder = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/UploadFolder/" + id;
            }
            else
                UploadFolder = NetCms.Configurations.Paths.AbsoluteRoot + uploadTempFolder;
            if (filters != null)
            {
                Filters = new List<PlUpload2FileFilter>();
                foreach (string s in filters)
                {
                    Filters.Add(new PlUpload2FileFilter(s, s));
                }

            }

            this.MaxFileNumber = maxfilenumber;
            this.BindField = false;
            this.CtrlID = id;

            delete.Click += DeleteClickHandler;

            delete.ID = id + "deleteButton";
            delete.Attributes.Add("style", "display:none");
            WebControl divModale = new WebControl(HtmlTextWriterTag.Div);
            divModale.ID = this.CtrlID + "dialogDelete";
            divModale.Style.Add(HtmlTextWriterStyle.Display, "none");
            divModale.CssClass = "dialogDelete";

            divModale.Controls.Add(new LiteralControl("<p class=\"deleteFileMessage" + this.CtrlID + "\" > " + DeleteFileMessage + "</p>"));

            divModale.Controls.Add(delete);
            divModale.Controls.Add(field);
            divModale.Controls.Add(FileID);
            divModale.Controls.Add(fileList);
            this.Controls.Add(divModale);          
        }

        public VfMultiUpload2(string id, string label, string uploadTempFolder = null, string[] filters = null) : base(id, label)
        {

            if (uploadTempFolder == null)
            {
                UploadTempFolder = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/UploadTmpFolder/" + DateTime.Now.ToString("yyyyMMdd");
            }
            else
                UploadTempFolder = NetCms.Configurations.Paths.AbsoluteRoot + uploadTempFolder;

            if (filters != null)
            {
                Filters = new List<PlUpload2FileFilter>();
                foreach (string s in filters)
                {
                    Filters.Add(new PlUpload2FileFilter(s, s));
                }

            }

            this.BindField = false;
            this.CtrlID = id;
            
            delete.Click += DeleteClickHandler;
            delete.ID = id + "deleteButton";
            delete.Attributes.Add("style", "display:none");

            WebControl divModale = new WebControl(HtmlTextWriterTag.Div);
            divModale.ID = this.CtrlID + "dialogDelete";
            divModale.Style.Add(HtmlTextWriterStyle.Display, "none");
            divModale.CssClass = "dialogDelete";            

            divModale.Controls.Add(new LiteralControl("<p class=\"deleteFileMessage" + this.CtrlID + "\">" + DeleteFileMessage + "</p>"));            

            divModale.Controls.Add(delete);
            divModale.Controls.Add(field);
            divModale.Controls.Add(FileID);
            divModale.Controls.Add(fileList);
            this.Controls.Add(divModale);
            
        }

        private IList<FileUploadedV2> filesList;

        public VfMultiUpload2(string id, string label, string uploadTempFolder = null, string uploadFolder = null, string[] filters = null, int maxfilenumber = 0, bool FilterFiles = false, IList<FileUploadedV2> FilesList = null) : base(id, label)
        {

            this.FilterFiles = FilterFiles;
            this.filesList = FilesList; 

            if (uploadTempFolder == null)
            {
                UploadTempFolder = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/UploadTmpFolder/" + DateTime.Now.ToString("yyyyMMdd");

            }
            else
            {
                UploadTempFolder = NetCms.Configurations.Paths.AbsoluteRoot + uploadTempFolder;

            }

            if (uploadFolder == null)
            {
                UploadFolder = NetCms.Configurations.Paths.AbsoluteRoot + "/repository/UploadFolder/" + id;
            }
            else
                UploadFolder = NetCms.Configurations.Paths.AbsoluteRoot + uploadTempFolder;
            if (filters != null)
            {
                Filters = new List<PlUpload2FileFilter>();
                foreach (string s in filters)
                {
                    Filters.Add(new PlUpload2FileFilter(s, s));
                }

            }

            this.MaxFileNumber = maxfilenumber;
            this.BindField = false;
            this.CtrlID = id;

            delete.Click += DeleteClickHandler;

            delete.ID = id + "deleteButton";
            delete.Attributes.Add("style", "display:none");
            WebControl divModale = new WebControl(HtmlTextWriterTag.Div);
            divModale.ID = this.CtrlID + "dialogDelete";
            divModale.Style.Add(HtmlTextWriterStyle.Display, "none");
            divModale.CssClass = "dialogDelete";
        
            divModale.Controls.Add(new LiteralControl("<p class=\"deleteFileMessage" + this.CtrlID + "\">" + DeleteFileMessage+"</p>"));
            
            divModale.Controls.Add(delete);
            divModale.Controls.Add(field);
            divModale.Controls.Add(FileID);
            divModale.Controls.Add(fileList);

            this.Controls.Add(divModale);           
        }

        #endregion

        public enum Status
        {
            Success,
            Failed,
            Overwritten,
            ModifiedFileName
        }
        
        /// <summary>
        /// Elenco dei filtri possibili,
        /// ImageFiles -> jpg,jpeg,gif,png
        /// DocumentFiles -> doc,docx,xls,xlsx,pdf,pdf7
        /// Media -> mp3,mp4,wmv,asf
        /// </summary>
        public enum FileFilters
        {
            ImageFiles,
            DocumentFiles,
            Media,
            ZipFiles

        }
       
        /// <summary>
        /// Metodo di trasferimento files da cartella temporanea a cartella definitiva (definite nel costruttore)
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">EventArgs e</param>
        /// <param name="overwrite">Se true, sovrascrive i file con lo stesso nome</param>
        /// <returns>Torna una List di 'KeyValuePair' contenente i file trasferiti e lo status del trasferimento.
        ///  Success => trasferito con successo
        ///  Failed => trasferimento fallito
        ///  Overwritten => file sovrascritto (era presente un file con lo stesso nome)
        ///  ModifiedFileName => non sovrascrive il file, ma ne modifica il nome 
        ///  </returns>
        public List<KeyValuePair<string, Status>> save_Click(string uploadDestFolder, bool overwrite = true)
        {
            string pathorigine = this.UploadTempFolder;
            string pathdestinazione = uploadDestFolder;
            var list = new List<KeyValuePair<string, Status>>();
            try
            {
                if (!System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(pathdestinazione)))
                    System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath(pathdestinazione));

                string[] filePaths = Directory.GetFiles(HttpContext.Current.Server.MapPath(pathorigine));
                // string[] fileprova = Directory.GetFiles(uploadSettings.DestinationPath);
                foreach (string file in filePaths)
                {
                    string[] fileArray = file.Split('\\');
                    int i = fileArray.Length;
                    string filename = fileArray.GetValue(i - 1).ToString();
                    try
                    {

                        if (File.Exists(HttpContext.Current.Server.MapPath(pathdestinazione) + "\\" + filename))
                        {
                            if (overwrite) // Se true elimina il vecchio file con lo stesso nome 
                            {
                                File.Delete(HttpContext.Current.Server.MapPath(pathdestinazione) + "\\" + filename);
                                File.Move(file, HttpContext.Current.Server.MapPath(pathdestinazione) + "\\" + filename);
                                list.Add(new KeyValuePair<string, Status>(filename, Status.Overwritten));
                            }
                            else // altrimenti cambia il filename aggiungendo un suffisso (ciclo finchè non trova quello corretto)
                            {
                                string nameTemp = filename;
                                string ext = nameTemp.Split('.').Last();
                                nameTemp = nameTemp.Replace("." + ext, "");
                                do
                                {

                                    filename = nameTemp + "_" + DateTime.Now.TimeOfDay.ToString().Replace(":", "").Replace(".", "") + "." + ext;
                                }
                                while (File.Exists(HttpContext.Current.Server.MapPath(pathdestinazione) + "\\" + filename));
                                File.Move(file, HttpContext.Current.Server.MapPath(pathdestinazione) + "\\" + filename);
                                list.Add(new KeyValuePair<string, Status>(filename, Status.ModifiedFileName));

                            }
                        }
                        else
                        {
                            File.Move(file, HttpContext.Current.Server.MapPath(pathdestinazione) + "\\" + filename);
                            list.Add(new KeyValuePair<string, Status>(filename, Status.Success));
                        }
                    }
                    catch (Exception ex)
                    {
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message);
                        list.Add(new KeyValuePair<string, Status>(filename, Status.Failed));
                    }
                }

            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message);
                return list;


            }
            return list;

        }

    }

    /// <summary>
    /// Wrapper del componente javascript PlUpload
    /// </summary>
    public class PlUploader : WebControl
    {

        protected override void OnInit(EventArgs e)
        {

            base.OnInit(e);
            PlUpload2Setting uploadSetting = new PlUpload2Setting(
              UploadTmpFolder,
              MaxFileSize,
              AcceptedExtensions
             );
            HttpContext.Current.Session["UploadSettings"] = uploadSetting;
            this.ID = CtrlId;
            #region Script&Css

            string headCssImport = @"<link rel=""stylesheet"" type=""text/css"" href=""" + ScriptFolderPath + @"/jquery.ui.plupload/css/jquery.ui.plupload.css"" />";
            //headCssImport += @"<link rel=""stylesheet"" type=""text/css"" href=""" + ScriptFolderPath + @"/custom/fileupload.css"" /><!-- fileupload.css -->";
            string headJsImport = @"<script type=""text/javascript"" src=""" + ScriptFolderPath + @"/plupload.full.min.js""></script>";
            headJsImport += @"<script type=""text/javascript"" src=""" + ScriptFolderPath + @"/jquery.ui.plupload/jquery.ui.plupload.js""></script>";

            headJsImport += @"<script type=""text/javascript"" src=""" + ScriptFolderPath + @"/i18n/it.js""></script>";
            //string script2 = "/scripts/shared/plupload/Prova/multiuploadify.js";
            //string headJsImport2 = @"<script type=""text/javascript"" src=""" + script2 + "\"></script>";

            base.Page.Header.Controls.Add(new LiteralControl(headCssImport));
            base.Page.Header.Controls.Add(new LiteralControl(headJsImport));
            base.Page.Header.Controls.Add(new LiteralControl(jsCode()));
            //base.Page.Header.Controls.Add(new LiteralControl(headJsImport2));

            #endregion



        }
        public PlUploader(string id, string scriptFolderPath, string uploadFolder, string uploadtempfolder, List<PlUpload2FileFilter> acceptedFiles, int maxFileSize, int maxfilenumber, CtrlRuntimes runtimes)
        {
            CtrlKey = id;
            CtrlId = id + "customUploader";
            ScriptFolderPath = scriptFolderPath;
            UploadTmpFolder = uploadtempfolder;
            AcceptedFiles = acceptedFiles;
            MaxFileSize = maxFileSize;
            MaxFileNumber = maxfilenumber;
            Runtime = runtimes;
        }

        internal string CtrlId;

        protected string CtrlKey;

        protected string ScriptFolderPath;
        public int MaxFileSize
        {
            get { return _maxFileSize; }
            protected set { _maxFileSize = value; }
        }
        private int _maxFileSize = 2;
        /// <summary>
        /// Numero massimo di file, 
        /// Valore di Default 0 (nessun limite)
        /// </summary>
        public int MaxFileNumber
        {
            get { return _maxFileNumber; }
            set { _maxFileNumber = value; }
        }
        public int _maxFileNumber = 0;
        protected CtrlRuntimes Runtime;
        protected List<PlUpload2FileFilter> AcceptedFiles
        {
            get;
            set;
        }

        protected string UploadTmpFolder;

        public bool MultiSelection
        {
            get { return _multiSelection; }
            set { _multiSelection = value; }
        }
        private bool _multiSelection = true;

        protected string AcceptedExtensions
        {
            get
            {
                if (_AcceptedExtensions == null)
                    _AcceptedExtensions = GetExtensionFiltersSerialized();
                return _AcceptedExtensions;
            }
        }
        private string _AcceptedExtensions;

        public bool PreventDuplicates
        {
            get { return preventDuplicates; }
            set { preventDuplicates = value; }
        }
        private bool preventDuplicates = false;

        public string ChunkSize
        {
            get { return _chunkSize; }
            set { _chunkSize = value; }
        }
        private string _chunkSize = "200mb";

        public bool Dragdrop
        {
            get { return _dragdrop; }
            set { _dragdrop = value; }
        }
        private bool _dragdrop = true;

        private string GetFileFilter()
        {
            string strFilter = "";

            foreach (PlUpload2FileFilter filter in AcceptedFiles)
            {
                strFilter += "{ title: '" + filter.Title + "', extensions: '" + filter.Extensions + "' }" + ((filter != AcceptedFiles.Last()) ? "," : "");
            }

            return strFilter;
        }

        public string GetRuntimes(CtrlRuntimes runtimes)
        {
            string strRuntime = "";
            switch (runtimes)
            {
                case CtrlRuntimes.All:
                    strRuntime = "html5,flash,silverlight,html4";
                    break;
                case CtrlRuntimes.Html5FlashSilverlight:
                    strRuntime = "html5,flash,silverlight";
                    break;
                case CtrlRuntimes.Html5Flash:
                    strRuntime = "html5,flash";
                    break;
                case CtrlRuntimes.Html5Silverlight:
                    strRuntime = "html5,silverlight";
                    break;
                case CtrlRuntimes.Html5:
                    strRuntime = "html5";
                    break;
                case CtrlRuntimes.Flash:
                    strRuntime = "flash";
                    break;
                case CtrlRuntimes.FlashSilverlight:
                    strRuntime = "flash,silverlight";
                    break;
                case CtrlRuntimes.Silverlight:
                    strRuntime = "silverlight";
                    break;
                case CtrlRuntimes.Html4:
                    strRuntime = "html4";
                    break;
                default:
                    strRuntime = "html5,flash,silverlight,html4";
                    break;
            }
            return strRuntime;

        }

        protected string jsCode()
        {
            string script = @"<script type=""text/javascript"">
$(document).ready(function(){
            var maxfiles = " + MaxFileNumber + @";
	        var files_selected = new Array();
	        var uploader = $(" + this.CtrlId + @").plupload({
                            runtimes: 'html5,silverlight,flash,html4',
                            url: '/uploader/plupload2_handler.ashx',
                            max_file_size: '" + MaxFileSize + @"mb',
                            chunk_size: '" + ChunkSize + @"',
                            unique_names: true,
		                    prevent_duplicates: true,";

                            if (MaxFileNumber > 0)
                                script += @"max_file_count : " + MaxFileNumber + ",";            
                                script += @"
                                          views:
                                          {
                                               list: true,
                                               thumbs: false, 
                                               active: 'list'
                                          },		
                                          filters: 
                                          {            
                                               mime_types: [ " + GetFileFilter() + @" ]
                                          },        
                           silverlight_xap_url: '/scripts/shared/plupload/js/plupload.silverlight.xap', 
                            multiple_queues: true,		
		init:
            {
                PostInit: function(up){
                    var filesPending = $('#" + this.CtrlId + @"fileList')[0].value;
                    // Recupero l'html element che contiene i files che ho caricato precedentemente nell'Uploader e che 
                    // per qualche errore non sono stati caricati
                
                    if(filesPending)
                    {
                            var files = new Array();
                            var filesToShow = [];
                            filesToShow = filesPending.split(',');
                            $.each(filesToShow,function(index,element) // Se ho files Pendenti li carico nell'uploader
                            {
                                if(element)
                                {
                                    var f = new plupload.File({name:element});
                                    up.addFile(f);
                                }
                            });                                                                                
                    }
                    plupload.addI18n({
                        'File extension error.': 'Il file selezionato non è supportato.',
                        'File size error.': 'La dimensione del file supera i limiti consentiti.'
                    });

            },
			BeforeUpload: function(up, file){
                    up.settings.multipart_params = {
                        filenameRenamed: file.name
                    };
            },
			FilesRemoved: function(up, files)
            {
                 var params = {
                               FileNameTOBeDeleted: '" + UploadTmpFolder + @"/' + files[0].name
                            };
				 $.ajax({
                        url: '/uploader/plupload2_deletehandler.ashx',
                            type: 'POST',
                            data: JSON.stringify(params),
                            contentType: 'application/json',
                            dataType: 'json',
                            async: false,
                            success: function(response){
                            results = response;
                        }
                    });
                    files_selected.pop();  
                    
                    $('#" + this.CtrlId + @"fileList').val(files_selected.toString());

                    var elencoFiles = $('#files_" + this.CtrlKey + @"').val();
                    // remove from elencoFiles current files[0].name
                    elencoFiles = elencoFiles.replace(files[0].name + ';','');
                    $('#files_" + this.CtrlKey + @"').val(elencoFiles);                            
            },
			FilesAdded: function(up, files) {
                plupload.each(files, function(file)
                {
                        var results;
                        var exist = true;
                        var app = '';
                        var count = 0;
                        var ext = file.name.substr(file.name.lastIndexOf('.'));
                        var filename = file.name.substr(0, file.name.lastIndexOf('.'));

                        while (exist)
                        {
                            var params = {
                                ""folderId"": $('#currentFolder_customUploader').val(),
                                ""filename"": filename + app + ext,
                                ""filenamecheck"" : 'true',
                                ""folderPath"": '"+ UploadTmpFolder + @"'
                            };
						
                        $.ajax({
                                url: '/api/UploadManager/checkfile_status/',
                                type: 'POST',
                                data: JSON.stringify(params),
                                contentType: 'application/json',
                                dataType: 'json',
                                async: false,
                                success: function(response){
                                    results = response;
                                }
                            });
                            if (!results.filenameIsValid)
                            {
                                alert(results.message);
                                up.stop();
                                up.removeFile(file);
                            }

                            if (!results.exist)
                            {
                                exist = false;
                                file.name = filename + app + ext;
                                file.name = filename.replace(',','-') + app + ext;                                
                            }
                            else
                            {
                                count++;
                                app = '_' + count;
                            }
                        }
                        if (app != '')
                        {
                            var errorMsg = '<li> Il file ' + file.name.replace(app, '') + ' è stato rinominato in ' + file.name + '</li>';
                            if ($('#console_customUploader').hasClass('hide'))
                               $('#console_customUploader').removeClass('hide');
                      
                            $('#console_customUploader ul').append(errorMsg);
                        }

                        // Controllo duplicati sui file già trasferiti/associati
                        var uploadedFiles = $('#uploadedfiles_" + this.CtrlKey + @"').val();
                        if(uploadedFiles != undefined)
                            uploadedFiles = uploadedFiles.split(';')
                        
                        if (($.inArray(file.name, files_selected) == -1) && ($.inArray(file.name, uploadedFiles) == -1)) {

                            if ($('#files-selected-container_customUploader').hasClass('hide'))
                                $('#files-selected-container_customUploader').removeClass('hide');                            
                            files_selected.push(file.name);                           
                        }
                        else {
                            alert('Il documento ' + file.name + ' selezionato è già associato: verrà rimosso dalla coda. Per trasferire il file è necessario cambiare nome.');
                            var elencoFiles = $('#files_" + this.CtrlKey + @"').val();
                            // remove from elencoFiles current file.name
                            elencoFiles = elencoFiles.replace(file.name + ';','');
                            $('#files_" + this.CtrlKey + @"').val(elencoFiles); 
                            up.stop();
                            up.removeFile(file);
                        }
                    });
                    up.start();                   
                },
                FileUploaded: function (up, file, response) {              
			        if( maxfiles > 0)
				    {
					   if (up.files.length > maxfiles) {
                            up.removeFile(file);
                            alert('Massimo numero di file raggiunto');
                       }
                       else    
                       {                            
                            $('#" + this.CtrlId + @"fileList').val(files_selected.toString());
                           
                            var elencoFiles = $('#files_" + this.CtrlKey + @"').val();
                            $('#files_" + this.CtrlKey + @"').val(elencoFiles + file.name +';');                            
                            
                       }
				    }
                    else    
                    {
                           $('#" + this.CtrlId + @"fileList').val(files_selected.toString());   
                          
                           var elencoFiles = $('#files_" + this.CtrlKey + @"').val();
                           $('#files_" + this.CtrlKey + @"').val(elencoFiles + file.name +';');
                    }                    
                },
		
			Error: function(up, err) {
                    var errorMsg = '<li>Attenzione: ' + err.message + ' (cod.err ' + err.code + ') </li>';
                    if ($('#console_customUploader').hasClass('hide'))
                        $('#console_customUploader').removeClass('hide');
                    $('#console_customUploader ul').append(errorMsg);
                }
            }

        });
	    uploader.init();
        });</script>";
            return script;
        }
        private string GetExtensionFiltersSerialized()
        {
            string strExtensionsSerialized = "";

            foreach (PlUpload2FileFilter filter in AcceptedFiles)
            {
                strExtensionsSerialized += filter.Extensions + ",";
            }
            return strExtensionsSerialized;
        }
        public enum CtrlRuntimes
        {
            All,
            Html5FlashSilverlight,
            Html5Flash,
            Html5Silverlight,
            Html5,
            Flash,
            FlashSilverlight,
            Silverlight,
            Html4
        }
    }

    public class FileUploadedV2
    {
        public FileUploadedV2()
        {

        }

        public FileUploadedV2(string filename)
        {
            FileName = filename;
        }

        private int _ID;
        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _Ext;
        public string Ext
        {
            get
            {
                if (_Ext == null)
                {
                    if (FileName != null)
                    {
                        _Ext = FileName.Substring(FileName.LastIndexOf('.') + 1, 3);
                    }
                }
                return _Ext;
            }
            set { _Ext = value; }
        }

        private string _FileName;
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }
    }
}
