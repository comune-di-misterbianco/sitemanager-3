﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI.WebControls;
using NetService.Utility.RecordsFinder;
using System.Text.RegularExpressions;
using System.Web.UI;

namespace NetService.Utility.ValidatedFields
{

    public class VfAutocompleteTextBox : VfGeneric
    {
        public enum ClientSideFramework
        {
            JQuery,
            Bootstrap
        }

        private ClientSideFramework Framework
        {
            get;
            set;
        }
        public override VfSearchParameter SearchParameter
        {
            get
            {
                if (_SearchParameter == null)
                {
                    _SearchParameter = new VfAutocompleteTextBoxSearchParameter(this, SearchComparisonCriteria);
                }
                return _SearchParameter;
            }
        }
        private VfSearchParameter _SearchParameter;
        public override WebControl Field
        {
            get { return TextBox; }
        }
        public override NetService.Utility.RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set { _SearchComparisonCriteria = value; }
        }
        private NetService.Utility.RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = NetService.Utility.RecordsFinder.Finder.ComparisonCriteria.Like;
        public override object PostbackValueObject
        {
            get
            {
                switch (this.InputType)
                {
                    //case InputTypes.Double: return double.Parse(this.Request.OriginalValue);
                    case InputTypes.Floating: return float.Parse(this.Request.OriginalValue);
                    case InputTypes.Number: return int.Parse(this.Request.OriginalValue);
                    case InputTypes.Valuta: return decimal.Parse(this.Request.OriginalValue);
                    default: return this.Request.OriginalValue.Trim(); //this.Request.DatabaseStringValue;
                }
            }
        }
        public string idTextBox
        {
            get
            {
                return _idTextBox;
            }
            set
            {
                _idTextBox = value;
            }
        }
        private string _idTextBox;
        public InputTypes InputType
        {
            get
            {
                return _InputType;
            }
            set
            {
                _InputType = value;
            }
        }
        private InputTypes _InputType;
        public bool ShowLabelAsHint
        {
            get
            {
                return _ShowLabelAsHint;
            }
            set
            {
                _ShowLabelAsHint = value;
            }
        }
        private bool _ShowLabelAsHint;
        public TextBox TextBox
        {
            get
            {
                if (_TextBox == null)
                {
                    _TextBox = new TextBox();

                    if (Framework == ClientSideFramework.JQuery)
                        _TextBox.CssClass = "form-control ";
                    else
                        _TextBox.CssClass = "form-control ui-autocomplete-input";

                    if (this.Request.IsValidString)
                    {
                        _TextBox.Text = this.Request.StringValue.Replace("''", "'");
                    }
                }
                return _TextBox;
            }
        }
        private TextBox _TextBox;
        public int MaxLenght
        {
            get
            {
                return _MaxLenght;
            }
            set
            {
                _MaxLenght = value;
            }
        }
        private int _MaxLenght = 255;
        public string token
        {
            get
            {
                return _token;
            }
            set
            {
                _token = value;
            }
        }
        private string _token;
        public string resourcePath
        {
            get
            {
                return _resourcePath;
            }
            set
            {
                _resourcePath = value;
            }
        }
        private string _resourcePath;
        public int MinLenght
        {
            get
            {
                return _MinLenght;
            }
            set
            {
                _MinLenght = value;
            }
        }
        private int _MinLenght = 0;
        public override string LocalCssClass
        {
            get
            {
                return "textbox-vf";
            }
        }
        
        public VfAutocompleteTextBox(string id, string labelResourceKey, InputTypes inputType, string resourcePath, ClientSideFramework Framework)
            : base(id, labelResourceKey, id)
        {
            this.idTextBox = id;
            this.InputType = inputType;
            this.resourcePath = resourcePath;
            this.Framework = Framework;
        }
        public VfAutocompleteTextBox(string id, string labelResourceKey, InputTypes inputType, string resourcePath, string token, ClientSideFramework Framework)
            : base(id, labelResourceKey, id)
        {
            this.idTextBox = id;
            this.InputType = inputType;
            this.resourcePath = resourcePath;
            this.Framework = Framework;
            this.token = token;
        }
        public VfAutocompleteTextBox(string id, string labelResourceKey, string activeRecordPropertyName, InputTypes inputType, string resourcePath)
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
            this.idTextBox = id;
            this.InputType = inputType;
            this.resourcePath = resourcePath;

            this.Framework = ClientSideFramework.JQuery;
        }

        public VfAutocompleteTextBox(string id, string labelResourceKey, string activeRecordPropertyName, string resourcePath)
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
            this.idTextBox = id;
            this.InputType = InputTypes.Text;
            this.resourcePath = resourcePath;

            this.Framework = ClientSideFramework.JQuery;
        }

        public VfAutocompleteTextBox(string id, string labelResourceKey, InputTypes inputType, string resourcePath)
            : base(id, labelResourceKey, id)
        {
            this.idTextBox = id;
            this.InputType = inputType;
            this.resourcePath = resourcePath;

            this.Framework = ClientSideFramework.JQuery;
        }

        public VfAutocompleteTextBox(string id, string labelResourceKey, string resourcePath)
            : base(id, labelResourceKey, id)
        {
            this.idTextBox = id;
            this.InputType = InputTypes.Text;
            this.resourcePath = resourcePath;

            this.Framework = ClientSideFramework.JQuery;
        }

        protected override void FillFieldValue()
        {
            if (this.InputType == InputTypes.Valuta)
            {
                decimal value = (decimal)this.DefaultValue;
                this.TextBox.Text = String.Format("{0:0.00}", value).Replace("''", "'");
            }
            else
                this.TextBox.Text = this.DefaultValue.ToString().Replace("''", "'");
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }
        protected override void LocalValidate()
        {
            switch (InputType)
            {
                case InputTypes.CodiceFiscale:
                    this.Validators.Add(new CodiceFiscaleValidator(NetService.Localization.LabelsManager.GetLabel("isNotValidCodiceFiscale")));
                    break;
            }

            if (this.Request.IsValidString)
            {
                string value = this.Request.StringValue;

                if (this.Required && string.IsNullOrEmpty(value))
                {
                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
                }
                else
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (/* this.InputType == InputTypes.Text &&*/ this.MinLenght == this.MaxLenght && value.Length > 0 && value.Length != MinLenght)
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("hasNotExactlyLength"), Label, MaxLenght));
                        if (/* this.InputType == InputTypes.Text &&*/ this.MinLenght != this.MaxLenght && value.Length > this.MaxLenght && MaxLenght > 0)
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isLongerThan"), Label, MaxLenght));
                        if (/*this.InputType == InputTypes.Text &&*/ this.MinLenght != this.MaxLenght && value.Length < this.MinLenght && MinLenght > 0)
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isShorterThan"), Label, MinLenght));

                        if (this.InputType == InputTypes.Email && !NetService.Utility.Common.FormatValidator.ValidateEmail(value))
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidEmail"), Label));

                        int temp = 0;
                        if (this.InputType == InputTypes.Number && !int.TryParse(value, out temp))
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidNumber"), Label));

                        if ((this.InputType == InputTypes.PositiveNumber && !int.TryParse(value, out temp)) || (this.InputType == InputTypes.PositiveNumber && int.TryParse(value, out temp) && temp <= 0))
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidNumber"), Label));

                        float tempFloat = 0;
                        if (this.InputType == InputTypes.Floating && (!float.TryParse(value, out tempFloat) || value.Contains(".")))
                            this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidFloating"), Label));

                        if (this.InputType == InputTypes.Valuta)
                        {
                            Regex moneyR = new Regex(@"^(\+)?\d+\,\d{2}");//^(\+)?permette l'inserimento solo di valute positive
                            if (!moneyR.IsMatch(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidValuta"), Label));
                        }
                        //double tempDouble = 0;
                        //if (this.InputType == InputTypes.Double && !double.TryParse(value, out tempDouble))
                        //    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidDouble"), Label));

                        if (this.InputType == InputTypes.Phone)
                        {
                            Regex regex = new Regex(@"^([0-9]{8,12})$", RegexOptions.IgnorePatternWhitespace);
                            if (!regex.IsMatch(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidPhoneNumber"), Label));
                        }

                        if (this.InputType == InputTypes.Cap)
                        {
                            Regex regex = new Regex(@"\d{5}");
                            if (!regex.IsMatch(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidCap"), Label));
                        }

                        if (this.InputType == InputTypes.PartitaIva)
                        {
                            if (!NetService.Utility.Common.FormatValidator.ValidatePartitaIVA(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidPartitaIVA"), Label));
                        }

                        if (this.InputType == InputTypes.CodiceFiscale)
                        {
                            if (!NetService.Utility.Common.FormatValidator.ValidateCodiceFiscale(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidCodiceFiscale"), Label));
                        }

                        if (this.InputType == InputTypes.CodiceFiscaleOPartitaIva)
                        {
                            if (value.Length != 16 && value.Length != 11)
                                this.AddError(string.Format("la lunghezza del campo '{0}' deve essere 11 caratteri per la partita IVA o 16 per il codice fiscale", Label));

                            if (value.Length == 16)
                            {
                                if (!NetService.Utility.Common.FormatValidator.ValidateCodiceFiscale(value))
                                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidCodiceFiscale"), Label));
                            }

                            if (value.Length == 11)
                            {
                                if (!NetService.Utility.Common.FormatValidator.ValidatePartitaIVA(value))
                                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidPartitaIVA"), Label));

                            }
                        }

                        if (this.InputType == InputTypes.CodiceAteco)
                        {
                            Regex regex = new Regex(@"\d{2}[.]{1}\d{2}[.]{1}\d{2}");
                            if (!regex.IsMatch(value))
                                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isNotValidAteco"), Label));
                        }
                    }
                }
            }
            else if (this.Required)
            {
                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //if (this.InputType == InputTypes.Blob)
            //    this.TextBox.Rows = 5;

            if (this.ShowLabelAsHint)
            {
                this.CssClass += " hint";
                this.TextBox.Attributes.Add("title", this.Label);
            }
            bool isFront = Framework == ClientSideFramework.JQuery ? false : true;
            if (string.IsNullOrEmpty(token))
            {
                string text = @"<script type='text/javascript'>
                        $(function() 
                        {
	                        function log(id, message ) 
                            {	
                                $('#" + this.idTextBox + @"').val(message);
	                        }
                            $( '#" + this.idTextBox + @"' ).autocomplete({
		                            source: function( request, response ) {
			                            $.ajax({
                                        type: 'POST',
                                        contentType: 'application/json;charset=utf-8',
                                        accept: 'application/json',

			                            url: '" + resourcePath + @"',
                                        dataType: 'json',
                                        data: JSON.stringify(
                                        { 
                                            Framework :  '" + isFront + @"',
                                            Value : request.term
                                        }),			                            	
					                            success: function( data ) {
												            response( $.map( data, function( item ) {
												            return {
														
														            label: item,
														
														            value: item
														            }
												            }));
											            },
                                                error: function(jqXHR,textStatus,errorThrown)
                                                {
                                                    alert(textStatus);
                                                }
					                    });
		                            },
		                            minLength: 3,
		                            select: function( event, ui ) {
                                                                    var id_tag = '" + this.idTextBox + @"'+
										                            log(id_tag, ui.item ?
										                            'Selected: ' + ui.item.label :
										                            'Nothing selected, input was ' + this.value);
										                            },
	                        });
                        });
                    </script>";

                Page.Header.Controls.Add(new LiteralControl(text));
            }
            else
            {
                string text = @"<script type='text/javascript'>
                        $(function() 
                        {
	                        function log(id, message ) 
                            {	
                                $('#" + this.idTextBox + @"').val(message);
	                        }
                            $( '#" + this.idTextBox + @"' ).autocomplete({
		                            source: function( request, response ) {
			                            $.ajax({
                                        type: 'POST',
                                        contentType: 'application/json;charset=utf-8',
                                        accept: 'application/json',
                                        beforeSend: xhr => xhr.setRequestHeader('Authorization', 'Bearer " + token+@"'),

                                        url: '" + resourcePath + @"',
                                        dataType: 'json',
                                        data: JSON.stringify(
                                        { 
                                            Framework :  '" + isFront + @"',
                                            Value : request.term
                                        }),			                           		
					                            success: function( data ) {
												            response( $.map( data, function( item ) {
												            return {
														
														            label: item,
														
														            value: item
														            }
												            }));
											            },
                                                error: function(jqXHR,textStatus,errorThrown)
                                                {
                                                    alert(textStatus);
                                                }
					                    });
		                            },
		                            minLength: 3,
		                            select: function( event, ui ) {
                                                                    var id_tag = '" + this.idTextBox + @"'+
										                            log(id_tag, ui.item ?
										                            'Selected: ' + ui.item.label :
										                            'Nothing selected, input was ' + this.value);
										                            },
	                        });
                        });
                    </script>";

                Page.Header.Controls.Add(new LiteralControl(text));
            }
        }
    }
}
