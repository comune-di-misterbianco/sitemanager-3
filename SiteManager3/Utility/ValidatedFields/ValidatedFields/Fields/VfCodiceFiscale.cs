﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using NetService.Utilities;

namespace NetService.Utility.ValidatedFields
{
    public class VfCodiceFiscale : VfGeneric
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public DateTime DataNascita { get; set; }
        public bool? IsMale { get; set; }
        public string CodiceCatastaleComune { get; set; }

        /// <summary>
        /// Se impostato a true essettua la validazione verificando la congruenza del codice inserito coi dati delle proprietà:
        /// public string Nome { get; set; }
        /// public string Cognome { get; set; }
        /// public DateTime DataNascita { get; set; }
        /// public bool IsMale { get; set; }
        /// public string CodiceCatastaleComune { get; set; }
        /// 
        /// Se impostato a false viene fatta una validazione solo sul formato.
        /// </summary>
        public bool ValidateAgaistData { get; set; }

        public override WebControl Field
        {
            get { return TextBox; }
        }

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set
            {
                _SearchComparisonCriteria = value;
            }
        }
        private RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = RecordsFinder.Finder.ComparisonCriteria.Like;

        public override object PostbackValueObject
        {
            get
            {   
                return this.Request.DatabaseStringValue;
            }
        }

        public TextBox TextBox
        {
            get
            {
                if (_TextBox == null)
                {
                    _TextBox = new TextBox();
                    _TextBox.CssClass = "form-control ";
                    _TextBox.ID = this.ID;

                    if (this.Request.IsValidString)
                    {
                        _TextBox.Text = this.Request.StringValue.Replace("''", "'");
                    }
                }
                return _TextBox;
            }
        }
        private TextBox _TextBox;

        public CheckBox NonLegalCodeCheckBox
        {
            get
            {
                if (_NonLegalCodeCheckBox == null)
                {
                    _NonLegalCodeCheckBox = new CheckBox()
                    {
                Checked = false,
                        CssClass = "form-control ",
                ID = this.Key + "_nonLglChk"
            };
                }
                return _NonLegalCodeCheckBox;
            }
        }
        private CheckBox _NonLegalCodeCheckBox;

        public Request.FormVar<string> NonLegalCodeCheckBoxRequest
        {
            get
            {
                if (_NonLegalCodeCheckBoxRequest == null)
                {
                    _NonLegalCodeCheckBoxRequest = new Request.FormVar<string>(NonLegalCodeCheckBox.ID);
                }
                return _NonLegalCodeCheckBoxRequest;
            }
        }
        private Request.FormVar<string> _NonLegalCodeCheckBoxRequest;

        public override string LocalCssClass
        {
            get
            {
                return "textbox-vf codicefiscale-vf ";
            }
        }

        public VfCodiceFiscale(string id, string labelResourceKey, string activeRecordPropertyName)
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
        }

        public VfCodiceFiscale(string id, string labelResourceKey)
            : base(id, labelResourceKey, id)
        {
        }

        protected override void FillFieldValue()
        {
            this.TextBox.Text = this.DefaultValue.ToString();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }

        protected override void LocalValidate()
        {
            if (this.Request.IsValidString)
            {
                string value = this.Request.StringValue;

                if (this.Required && string.IsNullOrEmpty(value))
                {
                    this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
                }
                else
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (value.Length != 16)
                            this.AddError(string.Format("la lunghezza del campo '{0}' deve essere 16 caratteri", this.Label));          //LABELDAUSCIRE

                        if (value.Length == 16 && !UserTellsThatHasNonLegalCode)
                        {
                            if (!NetService.Utility.Common.FormatValidator.ValidateCodiceFiscale(value))
                                this.AddError(NetService.Localization.LabelsManager.GetLabel("isNotValidCodiceFiscale"));

                            if (this.ValidateAgaistData)
                                ValidateFullCode(value);
                        }
                    }
                }
            }
            else if (this.Required)
            {
                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
            }
        }

        public bool UserTellsThatHasNonLegalCode
        {
            get
            {
                return (NonLegalCodeCheckBoxRequest.HasValidValue && new[] { "ON", "TRUE", "1" }.Any(x => x == NonLegalCodeCheckBoxRequest.Value.ToUpper()));
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            WebControl p = new WebControl(HtmlTextWriterTag.P);
            p.CssClass = "non-legal-code-declaration";

            WebControl label = new WebControl(HtmlTextWriterTag.Label);
            label.Attributes["for"] = NonLegalCodeCheckBox.ID;
            label.Controls.Add(new LiteralControl("Dichiaro che il codice nel campo '"+this.Label+"' non rispetta il decreto del Presidente della Repubblica 'n. 7842 del 2 novembre 1976'"));  //LABELDAUSCIRE

            p.Controls.Add(NonLegalCodeCheckBox);
            p.Controls.Add(label);
            this.Controls.Add(p);
        }

        private void ValidateFullCode(string value)
        {
            if (!string.IsNullOrWhiteSpace(this.Nome) &&
                !string.IsNullOrWhiteSpace(this.Cognome) &&
                this.DataNascita != DateTime.MinValue &&
                !string.IsNullOrWhiteSpace(CodiceCatastaleComune) &&
                IsMale != null
                )
            {
                CodiceFiscaleGenerator codiceFiscaleGenerator = new CodiceFiscaleGenerator(this.Nome, this.Cognome, this.DataNascita, this.IsMale.Value, this.CodiceCatastaleComune);
                if (!codiceFiscaleGenerator.CheckCodiceFiscale(value))
                {
                    this.AddError(NetService.Localization.LabelsManager.GetLabel("isNotValidCodiceFiscale"));
                }
            }
            else
                throw new InvalidOperationException("Non è possibile eseguire la validazione del codice fiscale, in quanto non sono stati forniti tutti i dati necessari alla generazione dello stesso."); //LABELDAUSCIRE
        }
    }
}
