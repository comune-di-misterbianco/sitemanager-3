﻿using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;
using NetService.Utility.RecordsFinder;

namespace NetService.Utility.ValidatedFields
{
    public class VfCheckBox :VfGeneric
    {
        public VfCheckBox(string id, string labelResourceKey)
            : base(id, labelResourceKey, id)
        {
            this.Required = false;    
        }

        public VfCheckBox(string id, string labelResourceKey, string activeRecordPropertyName)
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
            this.Required = false;
        }


        public CheckBox CheckBox
        {
            get
            {
                if (_CheckBox == null)
                {
                    _CheckBox = new CheckBox();
                    _CheckBox.ID = this.ID;
                    _CheckBox.CssClass = "checkbox-inline";
                }
                return _CheckBox;
            }
        }
        private CheckBox _CheckBox;

        public override WebControl Field
        {
            get { return CheckBox; }
        }

        public override string LocalCssClass
        {
            get
            {
                return "checkbox-vf";
            }
        }
        
        public override object PostbackValueObject
        {
            get 
            {

                if (this.Request.IsValidString && new[] { "ON", "TRUE", "1" }.Any(x => x == this.Request.StringValue.ToUpper()))
                    return true;
                return false;
            }
        }

        // DA TESTARE
        public override VfSearchParameter SearchParameter
        {
            get
            {
                if (_SearchParameter == null)
                {
                    _SearchParameter = new VfCheckBoxSearchParameter(this, SearchComparisonCriteria);
                }
                return _SearchParameter;
            }
        }
        private VfSearchParameter _SearchParameter;

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set
            { _SearchComparisonCriteria = value; }
        }
        private RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = Finder.ComparisonCriteria.Equals;

       
        protected override void LocalValidate()
        {
            string errors = "";

            if (Required && !this.Request.IsValidString)
            {
                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
            }
        }

        protected override void FillFieldValue()
        {
            CheckBox.Checked = bool.Parse(this.DefaultValue.ToString());
        }

        private bool _AutoPostBack;
        public bool AutoPostBack
        {
            get
            {
                return _AutoPostBack;
            }
            set
            {
                _AutoPostBack = value;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (AutoPostBack)
                CheckBox.AutoPostBack = true;

            bool keepDefaultValue = false;
            if (this.DefaultValue != null)
            {
                try
                {
                    keepDefaultValue = (bool)this.DefaultValue;
                }
                catch (Exception ex)
                {
                    if(this.DefaultValue.Equals("False"))
                        keepDefaultValue = false;
                    else
                        keepDefaultValue = true;
                }
            }

            CheckBox.Checked = (bool)this.PostbackValueObject || keepDefaultValue; 
            
        }
    }
}
