﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetService.Utility.ValidatedFields
{
    public class VfHidden : VfGeneric
    {
        public override WebControl Field
        {
            get
            {
                if (_Field == null)
                {
                    _Field = new WebControl(HtmlTextWriterTag.Span);
                    _Field.Controls.Add(this.HiddenField);
                }
                return _Field;
            }
        }
        private WebControl _Field;

        public override RecordsFinder.Finder.ComparisonCriteria SearchComparisonCriteria
        {
            get { return _SearchComparisonCriteria; }
            set { _SearchComparisonCriteria = value; }
        }
        private RecordsFinder.Finder.ComparisonCriteria _SearchComparisonCriteria = RecordsFinder.Finder.ComparisonCriteria.Like;

        public override object PostbackValueObject
        {
            get
            {
                return this.Request.DatabaseStringValue;
            }
        }

        public System.Web.UI.WebControls.HiddenField HiddenField
        {
            get
            {
                if (_HiddenField == null)
                {
                    _HiddenField = new HiddenField();
                    _HiddenField.ID = this.ID;

                    if (this.Request.IsValidString)
                    {
                        _HiddenField.Value = this.Request.StringValue.Replace("''", "'");
                    }
                }
                return _HiddenField;
            }
        }
        private HiddenField _HiddenField;

        public override string LocalCssClass
        {
            get
            {
                return "textbox-vf";
            }
        }

        public VfHidden(string id, string labelResourceKey, string activeRecordPropertyName, Type returnObjectType)
            : base(id, labelResourceKey, activeRecordPropertyName, returnObjectType)
        {
            ShowsInDetailsSheets = false;
        }

        public VfHidden(string id, string labelResourceKey, string activeRecordPropertyName)
            : base(id, labelResourceKey, activeRecordPropertyName)
        {
            ShowsInDetailsSheets = false;
        }

        public VfHidden(string id, string labelResourceKey)
            : base(id, labelResourceKey, id)
        {
            ShowsInDetailsSheets = false;
        }

        protected override void FillFieldValue()
        {
            this.HiddenField.Value = this.DefaultValue.ToString();
        }

        protected override void LocalValidate()
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            this.AttachFieldControls();

            if (!this.Request.IsPostBack && this.DataBinded) this.FillFieldValue();
        }

        protected virtual void AttachFieldControls()
        {
            this.Controls.Add(Field);
            this.HiddenField.ID = this.Key;
        }
    }
}
