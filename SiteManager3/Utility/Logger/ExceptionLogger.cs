﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Text;
using System.Web;
using System.Diagnostics;
using System.Net.Mail;
using System.Net;
using System.Management;
using System.Collections;

namespace NetCms.LogAndTrance
{
    /// <summary>
    /// La classe ExceptionLogger consente di registrare gli errori di tipo Exception 
    /// registrandoli su file, nomiti con la seguente sintassi: exAAMMGG.log e contenuti nella
    /// cartella principale del cms sotto la cartella logs.
    /// </summary>
    public class ExceptionLogger
    {
        /*private bool _MailEnabled = true;

        private string _MailFrom = "saxerrors@net-serv.it";

        private string NameFileLog 
        {
            get {
                string namefile = "";

                string anno = DateTime.Today.Year.ToString();

                anno = anno.Substring(2, 2);

                string mese = DateTime.Today.Month.ToString();
                if (mese.Length == 1)
                    mese = "0" + mese;

                string giorno = DateTime.Today.Day.ToString();
                if (giorno.Length == 1)
                    giorno = "0" + giorno;

                namefile = "ex" + anno + mese + giorno + ".log";

                return namefile;
            }
        }

        private string _AbsoluteRoot;
        public string AbsoluteRoot
        {
            get
            {
                if (_AbsoluteRoot == null)
                {
                    _AbsoluteRoot = string.Empty;
                    string RootPathValue = System.Web.Configuration.WebConfigurationManager.AppSettings["RootPath"].ToLower();
                    if (RootPathValue.Length > 0)
                        _AbsoluteRoot += RootPathValue.ToLower();
                }
                return _AbsoluteRoot;
            }
        }

        private string _AbsoluteLogsRoot;
        public string AbsoluteLogsRoot
        {
            get
            {
                if (_AbsoluteLogsRoot == null)
                {
                    string serverPath = HttpContext.Current.Server.MapPath(AbsoluteRoot).ToLower();
                    string configServerPath = serverPath + "\\logs";
                    if (!System.IO.Directory.Exists(configServerPath))
                    {
                        string rootPathValue = ("website");
                        configServerPath = serverPath.Replace(rootPathValue, rootPathValue + "_logs");
                    }
                    _AbsoluteLogsRoot = configServerPath;
                }

                return _AbsoluteLogsRoot;
            }
        }

        private string _smtphost;
        public string SmtpHost
        {
            get
            {
                if (_smtphost == null)
                    _smtphost = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpHost"].ToString();
                return _smtphost;
            }
        }

        private  string _smtpport;
        public  string SmtpPort
        {
            get
            {
                if (_smtpport == null)
                    _smtpport = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpPort"].ToString();

                return _smtpport;
            }
        }

        private  string _smtpUser;
        public string SmtpUser
        {
            get
            {
                if (_smtpUser == null)
                    _smtpUser = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpUser"].ToString();

                return _smtpUser;
            }
        }

        private string _smtpPassword;
        public string SmtpPassword
        {
            get
            {
                if (_smtpPassword == null)
                    _smtpPassword = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpPassword"].ToString();
                return _smtpPassword;
            }
        }

        private string _emailToReport;
        public string EmailToReport
        {
            get
            {
                if (_emailToReport == null)
                    _emailToReport = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailToReport"].ToString();
                return _emailToReport;
            }
        }
                
        private string LogFilePath 
        {
            get 
            {
                return AbsoluteLogsRoot + "\\" + NameFileLog;
            }
            
        }*/

        /// <summary>
        /// Inizializzazione di una nuova istanza dell'oggetto ExceptionLogger
        /// </summary>
        /// <param name="UserID">ID utente loggato al sistema</param>
        public ExceptionLogger() 
        {
          //FileStream fs = new FileStream(LogFilePath, FileMode.Append, FileAccess.Write, FileShare.Write);
          //fs.Close();
        }
        
        /// <summary>
        /// Scrive la riga di log con seguente sintassi: data; ora; componente; livello log; utente loggato;livello errore; messaggio di log
        /// </summary>
        /// <param name="message">Indica il messaggio di errore da scrivere nel file di log</param>
        /// <param name="componente">Indica il namespace completo dell'oggetto che genera l'errore</param>
        /// <param name="errorlevel">Indica il livello errore che si vuole loggare</param>
        public void SaveLog(int currentUserID, string message, object componente, ErrorLevel errorlevel,Exception ex)
        {
            NetCms.Diagnostics.Diagnostics.TraceMessage((NetCms.Diagnostics.TraceLevel)errorlevel, message + " " + ex.Message, currentUserID.ToString(), "","",1);
            /*SaveLog(currentUserID, message, componente, errorlevel);

            sendEmail mail_log = null;
            if (_MailEnabled)
            {
                string data = DateTime.Now.ToShortDateString();
                string time = DateTime.Now.ToLongTimeString();

                string query = "SELECT * FROM Win32_NetworkAdapterConfiguration"
                + " WHERE IPEnabled = 'TRUE'";
                ManagementObjectSearcher moSearch = new ManagementObjectSearcher(query);
                ManagementObjectCollection moCollection = moSearch.Get();

                ArrayList ips = new ArrayList();
                // Every record in this collection is a network interface
                foreach (ManagementObject mo in moCollection)
                {

                    // IPAddresses, probably have more than one value
                    string[] addresses = (string[])mo["IPAddress"];
                    foreach (string ipaddress in addresses)
                    {
                        ips.Add(ipaddress);
                    }
                } 

                
                string body = @"Eccezione generata il " + data + " alle ore " + time + "</br>" +
                              "<br>\n\r" +
                              "Componente : " + componente.GetType().ToString() + "</br>" +
                              "<br>\n\r" +
                              "ID utente corrente : " + currentUserID + "</br>" +
                              "<br>\n\r" +
                              "Error level : " + errorlevel.ToString() + "</br>" +
                              "<br>\n\r" +
                              "Message : " + message + "</br>";

                if (HttpContext.Current != null && HttpContext.Current.Request != null)
                {
                    if (HttpContext.Current.Request.Url != null)
                        body += "Url : " + HttpContext.Current.Request.Url.ToString() + "</br>";
                    if (HttpContext.Current.Request.UrlReferrer != null)
                        body += "Url Referer: " + HttpContext.Current.Request.UrlReferrer.ToString() + "</br>";
                }           
                            if (ex!=null)
                            {
                                body += "<br>\n\r";
                                if (ex.StackTrace != null)
                                    body += "StackTrace : <p>" + ex.StackTrace.Replace("\n", "</p><p>") + "</p></br>";
                                body += "<br>\n\r";
                                if (ex.InnerException != null)
                                    body += "InnerException : <p>" + ex.InnerException.StackTrace.Replace("\n", "</p><p>") + "</p></br>";
                                body += "<br>\n\r";
                                if (ex.Source != null)
                                    body += "Sorgente dell'eccezione : <p>" + ex.Source.Replace("\n", "</p><p>") + "</p></br>";
                            } 
                            body += "<br>\n\r" + 
                            "Generata sulla macchina con i seguenti indirizzi ip : ";
                            foreach (string ip in ips)
                                body += ip + " - ";
                            body += "<br>\n\r";
                            mail_log = new sendEmail(EmailToReport, message, body, _MailFrom, "Exception Error");
                            mail_log.SetSmtpHost = SmtpHost;
                            mail_log.SetSmtpPort = int.Parse(SmtpPort);
                            if (SmtpUser != null && SmtpPassword != null && SmtpUser.Length > 0 && SmtpPassword.Length > 0)
                            {
                                mail_log.SmtpUser = SmtpUser;
                                mail_log.SmtpPassword = SmtpPassword;
                            }
                            mail_log.Send();
            }*/
            //throw ex;
        }
        /// <summary>
        /// Scrive la riga di log con seguente sintassi: data; ora; componente; livello log; utente loggato;livello errore; messaggio di log
        /// </summary>
        /// <param name="message">Indica il messaggio di errore da scrivere nel file di log</param>
        /// <param name="componente">Indica il namespace completo dell'oggetto che genera l'errore</param>
        /// <param name="errorlevel">Indica il livello errore che si vuole loggare</param>
        public void SaveLog(int currentUserID, string message, object componente, ErrorLevel errorlevel) 
        {
            NetCms.Diagnostics.Diagnostics.TraceMessage((NetCms.Diagnostics.TraceLevel)errorlevel, message, currentUserID.ToString(), "","", 1);

            /*try
            {
                StreamWriter StreamWriter = new StreamWriter(LogFilePath, true, Encoding.ASCII);

                string data = DateTime.Now.ToShortDateString();
                string time = DateTime.Now.ToLongTimeString();

                string NextLine = data + ";" + time + ";" + componente.GetType().ToString() + ";" + currentUserID + ";" + errorlevel.ToString() + ";" + message;

                StreamWriter.WriteLine(NextLine);
                StreamWriter.Close();

                switch (errorlevel)
                {
                    case ErrorLevel.Off:
                        break;
                    case ErrorLevel.Error:
                        break;
                    case ErrorLevel.Warning:
                        break;
                    case ErrorLevel.Info:
                        break;
                    case ErrorLevel.Verbose:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex) { }*/

            
        }

        public enum ErrorLevel {
            Off = 0,
            Fatal=1,
            Error = 2,
            Warning = 3,
            Info = 4,
            Verbose = 5
        }
    }

    /*public class sendEmail
    {
        private string _emailTo = null;
        private string _subject = null;
        private string _body = null;
        private string _emailFrom = null;
        private string _lblMittente = null;
        private string smtphost;
        private int smtpport;

        public sendEmail(string emailTo, string subject, string body, string emailFrom, string lblMittente)
        {
            _emailTo = emailTo;
            _subject = subject;
            _body = body;
            _emailFrom = emailFrom;
            _lblMittente = lblMittente;
        }

        public string SetSmtpHost
        {
            set { smtphost = value; }
        }

        public int SetSmtpPort
        {
            set { smtpport = value; }
        }
        private string smtpuser;
        public string SmtpUser
        {
            get { return smtpuser; }
            set { smtpuser = value; }
        }
        private string smtppassword;
        public string SmtpPassword
        {
            get { return smtppassword; }
            set { smtppassword = value; }
        }

        public void Send()
        {

            MailMessage messaggio = new MailMessage();

            messaggio.From = new MailAddress(_emailFrom, _lblMittente);

            messaggio.To.Add(new MailAddress(_emailTo));

            messaggio.Subject = _subject;

            messaggio.SubjectEncoding = System.Text.Encoding.UTF8;

            messaggio.Body = _body;

            messaggio.BodyEncoding = System.Text.Encoding.UTF8;

            messaggio.IsBodyHtml = true;

            SmtpClient smtpClient = new SmtpClient(smtphost, smtpport); 
            //if (SmtpUser.Length > 0 && SmtpPassword.Length > 0)
            //{
            //    smtpClient.Credentials = new System.Net.NetworkCredential(SmtpUser, SmtpPassword);
            //    smtpClient.UseDefaultCredentials = true;
            //}
            smtpClient.Send(messaggio);
        }      
    }   
    */
    
    public static class ExceptionManager
    {
        public static ExceptionLogger ExceptionLogger
        {
            get
            {
                if (_ExceptionLogger == null)
                    _ExceptionLogger = new ExceptionLogger();
                return _ExceptionLogger;
            }
        }
        private static ExceptionLogger _ExceptionLogger;
    }
}
