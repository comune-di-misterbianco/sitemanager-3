using System.IO;
using System.Xml;
using NMG.Core.Domain;

namespace NMG.Core.Generator
{
    public abstract class MappingGenerator : AbstractGenerator
    {
        protected MappingGenerator(ApplicationPreferences applicationPreferences, Table table)
            : base(
                applicationPreferences.FolderPath, applicationPreferences.TableName, /*applicationPreferences.NameSpace,
                applicationPreferences.AssemblyName,*/ applicationPreferences.Sequence, applicationPreferences.ClassName, table, applicationPreferences)
        {
        }

        protected abstract void AddIdGenerator(XmlDocument xmldoc, XmlElement idElement);

        public override void Generate()
        {
            //string fileName = filePath + Formatter.FormatSingular(tableName) + ".hbm.xml";
            string fileName = filePath + Formatter.FormatSingular(className) + ".hbm.xml";
            
            using (var stringWriter = new StringWriter())
            {
                XmlDocument xmldoc = CreateMappingDocument();
                xmldoc.Save(stringWriter);
                string generatedXML = RemoveEmptyNamespaces(stringWriter.ToString());

                using (var writer = new StreamWriter(fileName))
                {
                    writer.Write(generatedXML);
                    writer.Flush();
                }
            }
        }

        private static string RemoveEmptyNamespaces(string mappingContent)
        {
            mappingContent = mappingContent.Replace("utf-16", "utf-8");
            return mappingContent.Replace("xmlns=\"\"", "");
        }

        public XmlDocument CreateMappingDocument()
        {
            var xmldoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmldoc.CreateXmlDeclaration("1.0", string.Empty, string.Empty);
            xmldoc.AppendChild(xmlDeclaration);
            XmlElement root = xmldoc.CreateElement("hibernate-mapping", "urn:nhibernate-mapping-2.2");
            //root.SetAttribute("assembly", assemblyName);
            //root.SetAttribute("namespace", nameSpace);
            xmldoc.AppendChild(root);

            XmlElement classElement = xmldoc.CreateElement("class","urn:nhibernate-mapping-2.2");
            //classElement.SetAttribute("name", Formatter.FormatSingular(tableName));
            classElement.SetAttribute("entity-name", className);
            classElement.SetAttribute("table", tableName);
            //classElement.SetAttribute("lazy", "true");
            root.AppendChild(classElement);
            PrimaryKey primaryKey = Table.PrimaryKey;


            if (primaryKey.Type == PrimaryKeyType.CompositeKey)
            {
                XmlElement idElement = xmldoc.CreateElement("composite-id");
                foreach (Column key in primaryKey.Columns)
                {
                    XmlElement keyProperty = xmldoc.CreateElement("key-property");
                    keyProperty.SetAttribute("name", Formatter.FormatText(key.Name));
                    keyProperty.SetAttribute("column", key.Name);

                    idElement.AppendChild(keyProperty);

                    classElement.AppendChild(idElement);
                }
            }

            foreach (Column column in Table.Columns)
            {
                BuildProperties(xmldoc, classElement, column);
            }

            foreach (var hasMany in Table.HasManyRelationships)
            {
                BuildHasMany(xmldoc, classElement, hasMany);
            }

            foreach (var oneToOne in Table.OneToOneRelationships)
            {
                BuildOneToOne(xmldoc, classElement, oneToOne);
            }

            foreach (var hasManyToMany in Table.HasManyToManyRelationships)
            {
                BuildHasManyToMany(xmldoc, classElement, hasManyToMany);
            }

            foreach (var joinedSubClass in Table.JoinedSubClasses)
            {
                XmlElement subclassElement = xmldoc.CreateElement("joined-subclass");
                //subclassElement.SetAttribute("name", joinedSubClass.ClassName);
                subclassElement.SetAttribute("entity-name", joinedSubClass.ClassName);
                subclassElement.SetAttribute("table", joinedSubClass.TableName);

                XmlElement keyElement = xmldoc.CreateElement("key");

                XmlElement columnElement = xmldoc.CreateElement("column");
                columnElement.SetAttribute("name", joinedSubClass.ColumnName.Replace(" ","_"));

                keyElement.AppendChild(columnElement);
                subclassElement.AppendChild(keyElement);

                foreach (Column column in joinedSubClass.Columns)
                {
                    BuildProperties(xmldoc, subclassElement, column);
                }

                foreach (var hasMany in joinedSubClass.HasManyRelationships)
                {
                    BuildHasMany(xmldoc, subclassElement, hasMany);
                }

                foreach (var oneToOne in joinedSubClass.OneToOneRelationships)
                {
                    BuildOneToOne(xmldoc, subclassElement, oneToOne);
                }

                foreach (var hasManyToMany in joinedSubClass.HasManyToManyRelationships)
                {
                    BuildHasManyToMany(xmldoc, subclassElement, hasManyToMany);
                }

                classElement.AppendChild(subclassElement);
            }

            return xmldoc;
        }

        private void BuildProperties(XmlDocument xmldoc, XmlElement classElement, Column column)
        {
            XmlElement property = null;

            if (column.IsForeignKey)
            {
                ForeignKeyColumn col = column as ForeignKeyColumn;
                property = xmldoc.CreateElement("many-to-one");
                property.SetAttribute("fetch", "select");
                property.SetAttribute("foreign-key", col.ForeignKeyString);
                property.SetAttribute("class", col.ClassName);
                property.SetAttribute("lazy", "false");
            }
            else if (column.IsPrimaryKey)
            {
                property = xmldoc.CreateElement("id");
            }
            else
            {
                property = xmldoc.CreateElement("property");
            }

            if (property != null)
                property.SetAttribute("name", Formatter.FormatText(column.Name));

            if (!column.IsForeignKey)
            {
                property.SetAttribute("type", column.PropertyType);
            }

            XmlElement columnProperty = xmldoc.CreateElement("column");
            if (property != null)
                property.AppendChild(columnProperty);

            columnProperty.SetAttribute("name", column.Name.Replace(" ","_"));

            if (!column.IsPrimaryKey && !column.IsForeignKey)
            {
                columnProperty.SetAttribute("sql-type", column.SqlType);
            }
            
            columnProperty.SetAttribute("not-null", (!column.IsNullable).ToString().ToLower());

            if (column.IsPrimaryKey)
            {
                XmlElement generatorElement = xmldoc.CreateElement("generator");
                generatorElement.SetAttribute("class", "identity");
                property.AppendChild(generatorElement);
            }

            if (property != null)
                classElement.AppendChild(property);
        }

        private void BuildHasMany(XmlDocument xmldoc, XmlElement classElement, HasMany hasMany)
        {
            XmlElement setElement = xmldoc.CreateElement("bag");

            setElement.SetAttribute("name", hasMany.Reference);
            //setElement.SetAttribute("inverse", "true");
            setElement.SetAttribute("cascade", "all");
            setElement.SetAttribute("fetch", "select");

            classElement.AppendChild(setElement);

            XmlElement keyElement = xmldoc.CreateElement("key");

            keyElement.SetAttribute("column", hasMany.ReferenceColumn);

            setElement.AppendChild(keyElement);

            XmlElement oneToManyElement = xmldoc.CreateElement("one-to-many");

            oneToManyElement.SetAttribute("class", hasMany.ReferenceClass);

            setElement.AppendChild(oneToManyElement);
        }

        private void BuildOneToOne(XmlDocument xmldoc, XmlElement classElement, OneToOne oneToOne)
        {
            XmlElement setElement = xmldoc.CreateElement("one-to-one");

            setElement.SetAttribute("cascade", "all");
            setElement.SetAttribute("class", oneToOne.ReferenceClass);
            setElement.SetAttribute("property-ref", oneToOne.ReferenceProperty);
            setElement.SetAttribute("name", oneToOne.Name);

            classElement.AppendChild(setElement);
        }

        private void BuildHasManyToMany(XmlDocument xmldoc, XmlElement classElement, HasManyToMany hasManyToMany)
        {
            XmlElement setElement = xmldoc.CreateElement("bag");

            setElement.SetAttribute("name", hasManyToMany.SetName);
            setElement.SetAttribute("table", hasManyToMany.TableName);
            //setElement.SetAttribute("inverse", "true");
            setElement.SetAttribute("cascade", "all");
            setElement.SetAttribute("fetch", "select");

            classElement.AppendChild(setElement);

            XmlElement keyElement = xmldoc.CreateElement("key");
            XmlElement columnElement = xmldoc.CreateElement("column");
            columnElement.SetAttribute("name", hasManyToMany.KeyColumnName);
            columnElement.SetAttribute("not-null", "true");

            keyElement.AppendChild(columnElement);

            setElement.AppendChild(keyElement);

            XmlElement manyToManyElement = xmldoc.CreateElement("many-to-many");
            manyToManyElement.SetAttribute("entity-name", hasManyToMany.ManyToManyEntityName);
            columnElement = xmldoc.CreateElement("column");
            columnElement.SetAttribute("name", hasManyToMany.ManyToManyColumnName); 
            columnElement.SetAttribute("not-null", "true");

            manyToManyElement.AppendChild(columnElement);

            setElement.AppendChild(manyToManyElement);
        }
    }
}