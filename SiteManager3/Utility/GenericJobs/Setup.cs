﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Vertical;

namespace GenericJobs
{
    public class GenericJobsAppSetup : VerticalApplicationSetup
    {
        public override string[,] Pages
        {
            get { return null;  }
        }

        public override string[,] Criteria
        {
            get { return null; }
        }

        public override bool IsNetworkDependant
        {
            get { return false; }
        }

        protected override bool LocalInstall(bool isInsidePackage = false)
        {
            bool done = true;
            return done;
        }

        public GenericJobsAppSetup(VerticalApplication baseApp)
            : base(baseApp)
        {
        }
    }
}
