﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericJobs.Model
{
    public class JobState
    {

        public virtual int ID
        {
            get;
            set;
        }

        public virtual DateTime Data
        {
            get;
            set;
        }

        public virtual GenericJob Job
        {
            get;
            set;
        }

        public virtual JobStatus Stato
        {
            get;
            set;
        }

        /// <summary>
        /// Da associare allo stato del trigger
        /// </summary>
        public enum JobStatus
        {
            Schedulato,
            In_Esecuzione,
            In_Pausa,
            Stop,
            Completo,
            Errore,
            Non_Schedulato,
            No_Stato
        }

        public enum Column 
        {
            ID,
            Data,
            Job,
            Stato
        }
    }
}
