﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GenericJobs.Model;

namespace GenericJobs.Model
{
    interface IGenericJob
    {        
        void Build();

        //void SetCallBackMethod(GenericJob.ExecuteCallBack callback);

        void Save();
    }
}
