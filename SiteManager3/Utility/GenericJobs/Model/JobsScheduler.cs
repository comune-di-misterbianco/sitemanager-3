﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using Quartz.Impl;
using System.Collections.Specialized;
using Quartz.Impl.Matchers;
using NetCms.DBSessionProvider;
using NHibernate;
using System.Threading.Tasks;

namespace GenericJobs.Model
{
    public static class JobsScheduler
    {               
        //private static IScheduler _Scheduler;
        private static Task<IScheduler> Scheduler 
        {
            get {
                
                StdSchedulerFactory factory = new StdSchedulerFactory();
                return factory.GetScheduler();                
            }
        }

        public static async void AddSimpleJob(GenericJob job) 
        {
            ITrigger trigger = null;

            if (job.TriggerInfo.ExecuteNow) 
            {
                trigger = TriggerBuilder
                                    .Create()                                    
                                    .StartNow()
                                    .WithIdentity(job.TriggerInfo.Name, job.TriggerInfo.Gruppo)
                                    .WithSimpleSchedule()
                                    .ForJob(job.Nome, job.TriggerInfo.Gruppo)
                                    .Build();
            }
            else
            {
              trigger = TriggerBuilder
                                .Create()
                                .StartAt(job.TriggerInfo.StartTime)
                                .WithIdentity(job.TriggerInfo.Name, job.TriggerInfo.Gruppo)
                                .WithSimpleSchedule()                                
                                .ForJob(job.Nome, job.TriggerInfo.Gruppo)
                                .Build();
            }

            job.QuartzTrigger = trigger;
            job.ChangeState(JobState.JobStatus.Schedulato);

            await Scheduler.GetAwaiter().GetResult().ScheduleJob(job.QuartzJob, trigger);
            Scheduler.GetAwaiter().GetResult().ListenerManager.AddJobListener(new GenericJobListener(job), KeyMatcher<JobKey>.KeyEquals(new JobKey(job.Nome, job.Gruppo)));
            Scheduler.Start();
            
        }

        public static async void AddSecondsJob(GenericJob job)
        {
            // istanziare un quartz trigger in funzione jobtriggerinfo
            ITrigger trigger = Quartz.TriggerBuilder
                           .Create()
                           .StartAt(job.TriggerInfo.StartTime)
                           .WithIdentity(job.TriggerInfo.Name)
                           .WithSimpleSchedule(x => x.WithIntervalInSeconds(job.TriggerInfo.Seconds)                           
                           )
                           .ForJob(job.Nome)
                           .Build();

            await Scheduler.GetAwaiter().GetResult().ScheduleJob(job.QuartzJob, trigger);
            Scheduler.GetAwaiter().GetResult().ListenerManager.AddJobListener(new GenericJobListener(job));
            job.ChangeState(JobState.JobStatus.Schedulato);
        }

        public static async void AddSecondsForeverJob(GenericJob job)
        {
            // istanziare un quartz trigger in funzione jobtriggerinfo
            ITrigger trigger = Quartz.TriggerBuilder
                           .Create()
                           .StartAt(job.TriggerInfo.StartTime)
                           .WithIdentity(job.TriggerInfo.Name)
                           .WithSimpleSchedule(x => x.WithIntervalInSeconds(job.TriggerInfo.Seconds)
                           .RepeatForever()
                           )
                           .ForJob(job.Nome)
                           .Build();

            await Scheduler.GetAwaiter().GetResult().ScheduleJob(job.QuartzJob, trigger);
            Scheduler.GetAwaiter().GetResult().ListenerManager.AddJobListener(new GenericJobListener(job));
            job.ChangeState(JobState.JobStatus.Schedulato);
        }

        public static async void AddMinuteJob(GenericJob job)
        {
            // istanziare un quartz trigger in funzione jobtriggerinfo
            ITrigger trigger = Quartz.TriggerBuilder
                           .Create()
                           .StartAt(job.TriggerInfo.StartTime)
                           .WithIdentity(job.TriggerInfo.Name)
                           .WithSimpleSchedule(x => x.WithIntervalInMinutes(job.TriggerInfo.Minutes))
                           .ForJob(job.QuartzJob)
                           .Build();
            
            job.QuartzTrigger = trigger;
            job.ChangeState(JobState.JobStatus.Schedulato);

            await Scheduler.GetAwaiter().GetResult().ScheduleJob(job.QuartzJob, trigger);
            Scheduler.GetAwaiter().GetResult().ListenerManager.AddJobListener(new GenericJobListener(job));
            Scheduler.Start();

        }

        public static async void AddMinuteForeverJob(GenericJob job)
        {
            // istanziare un quartz trigger in funzione jobtriggerinfo
            ITrigger trigger = Quartz.TriggerBuilder
                           .Create()
                           .StartAt(job.TriggerInfo.StartTime)
                           .WithIdentity(job.TriggerInfo.Name)
                           .WithSimpleSchedule(x => x
                               .WithIntervalInMinutes(job.TriggerInfo.Minutes)
                               .RepeatForever()
                           )
                           .ForJob(job.QuartzJob)
                           .Build();

            await Scheduler.GetAwaiter().GetResult().ScheduleJob(job.QuartzJob, trigger);            
            Scheduler.GetAwaiter().GetResult().ListenerManager.AddJobListener(new GenericJobListener(job));
            job.QuartzTrigger = trigger;
            job.ChangeState(JobState.JobStatus.Schedulato);
        }

        public static async void AddHoursJob(GenericJob job)
        {
            // istanziare un quartz trigger in funzione jobtriggerinfo
            ITrigger trigger = Quartz.TriggerBuilder
                           .Create()
                           .StartAt(job.TriggerInfo.StartTime)
                           .WithIdentity(job.TriggerInfo.Name)
                           .WithSimpleSchedule(x => x.WithIntervalInHours(job.TriggerInfo.Hours)                           
                           )
                           .ForJob(job.QuartzJob)
                           .Build();

            await Scheduler.GetAwaiter().GetResult().ScheduleJob(job.QuartzJob, trigger);
            Scheduler.GetAwaiter().GetResult().ListenerManager.AddJobListener(new GenericJobListener(job));
            job.QuartzTrigger = trigger;
            job.ChangeState(JobState.JobStatus.Schedulato);
        }

        public static async void AddHoursForeverJob(GenericJob job)
        {
            // istanziare un quartz trigger in funzione jobtriggerinfo
            ITrigger trigger = Quartz.TriggerBuilder
                           .Create()
                           .StartAt(job.TriggerInfo.StartTime)
                           .WithIdentity(job.TriggerInfo.Name)
                           .WithSimpleSchedule(x => x.WithIntervalInHours(job.TriggerInfo.Hours)
                           .RepeatForever()
                           )
                           .ForJob(job.QuartzJob)
                           .Build();

            await Scheduler.GetAwaiter().GetResult().ScheduleJob(job.QuartzJob, trigger);
            Scheduler.GetAwaiter().GetResult().ListenerManager.AddJobListener(new GenericJobListener(job));
            job.QuartzTrigger = trigger;
            job.ChangeState(JobState.JobStatus.Schedulato);
        }

        public static async void Start()
        {
           
            // lettura di tutti i job programmati da db
            // inserire codice
            ISession session = FluentSessionProvider.Instance.OpenInnerSession();
            try
            {
                IList<GenericJob> jobs = BusinessLogic.JobsBL.GetJobsNotExecuted(session);

                if (jobs != null && jobs.Count > 0)
                {
                    foreach (GenericJob job in jobs)
                    {                        
                        #region DataMap
                        // recupero il quartzjob e lo riassegno al genericjob
                        JobDetailImpl quartzJob = new JobDetailImpl(job.Nome, job.Gruppo, job.GetType());
                        quartzJob.Description = job.Descrizione;

                        IDictionary<string, object> datamap = new Dictionary<string, object>();

                        if (quartzJob.JobDataMap == null || quartzJob.JobDataMap.Count == 0)
                        {
                            datamap.Add("ID", job.ID.ToString());
                            datamap.Add("ObjType", job.JobType);

                            // leggere il datamapconcrete e deserializzare gli oggetti json
                            foreach (KeyValuePair<string, string> entry in job.DataMapConcrete)
                            {
                                var key = entry.Key;
                                var value = entry.Value;
                                datamap.Add(key, value);
                            }

                            quartzJob.JobDataMap = new JobDataMap(datamap);
                        }
                        #endregion

                        job.QuartzJob = quartzJob;

                        switch (job.TriggerInfo.TypeOfTrigger)
                        {
                            case JobTriggerInfo.TriggerType.Simple:
                                AddSimpleJob(job);
                                break;
                            case JobTriggerInfo.TriggerType.Seconds:
                                if (!job.TriggerInfo.RepeatForEver)
                                    AddSecondsJob(job);
                                else
                                    AddSecondsForeverJob(job);
                                break;
                            case JobTriggerInfo.TriggerType.Minutes:
                                if (!job.TriggerInfo.RepeatForEver)
                                    AddMinuteJob(job);
                                else
                                    AddMinuteForeverJob(job);
                                break;
                            case JobTriggerInfo.TriggerType.Hours:
                                if (!job.TriggerInfo.RepeatForEver)
                                    AddHoursJob(job);
                                else
                                    AddHoursForeverJob(job);
                                break;
                        }
                    }
                }

                await Scheduler.GetAwaiter().GetResult().Start();

            }
            catch (Exception ex) 
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante l'avvio dello scheduler dei job " + ex.Message);
            }
        }
        
        public static async void Standby()
        {
           await Scheduler.GetAwaiter().GetResult().Standby();
        }

        public static async void Shutdown()
        {
           await Scheduler.GetAwaiter().GetResult().PauseAll();
           await Scheduler.GetAwaiter().GetResult().Shutdown();            
        }

        public static async void StartDelayed(TimeSpan delay) 
        {
           await Scheduler.GetAwaiter().GetResult().StartDelayed(delay);
        }

        /// <summary>
        /// Mette in pausa tutti i job
        /// </summary>
        public static async void PauseAllJob()
        {
           await Scheduler.GetAwaiter().GetResult().PauseAll();

            // cambiare lo stato a tutti i job in db attivi e non ancora eseguiti
        }

        /// <summary>
        /// Mette in pausa il job
        /// </summary>
        /// <param name="job">Job che si vuole metter in pausa</param>
        public static async void PauseJob(GenericJob job) 
        {
            JobKey jobkey = new JobKey(job.Nome + job.ID.ToString());
            // codice per salvare le modifiche su db
            await Scheduler.GetAwaiter().GetResult().PauseJob(jobkey);

            job.ChangeState(JobState.JobStatus.In_Pausa);
        }

        /// <summary>
        /// Ripristina lo stato di tutti i job
        /// </summary>
        public static async void ResumeAllJob()
        {
           await Scheduler.GetAwaiter().GetResult().ResumeAll();
        }

        /// <summary>
        /// Ripristina lo stato del job 
        /// </summary>
        /// <param name="job">Job che si vuole ripristinare</param>
        public static async void ResumeJob(GenericJob job)
        {
            JobKey jobkey = new JobKey(job.Nome + job.ID.ToString());          
            // codice per salvare le modifiche su db
            await Scheduler.GetAwaiter().GetResult().ResumeJob(jobkey);

            job.ChangeState(JobState.JobStatus.Schedulato);
        }

        /// <summary>
        /// Rimuove dallo scheduler tutti i job passati per parametro
        /// </summary>
        /// <param name="jobs">I job da rimuovere</param>
        public static async void RemoveAllJob(List<GenericJob> jobs)
        {             
            //var jobKeyList = jobs.Select(x => new JobKey(x.Nome)).ToList();
            var jobKeyList = jobs.ConvertAll(x => new JobKey(x.Nome));

            // codice per salvare le modifiche su db
            await Scheduler.GetAwaiter().GetResult().DeleteJobs(jobKeyList);
        }

        public static async void RemoveJob(GenericJob job)
        {
            JobKey jobkey = new JobKey(job.Nome + job.ID.ToString());
            await Scheduler.GetAwaiter().GetResult().DeleteJob(jobkey);            
        }

        public static async void StopJob(GenericJob job)
        {
            //JobKey jobkey = new JobKey(job.Nome + job.ID.ToString());
            // codice per salvare le modifiche su db
            await Scheduler.GetAwaiter().GetResult().Interrupt(job.QuartzJob.Key);

            job.ChangeState(JobState.JobStatus.Stop);
        }


        public static async Task<IJobDetail> GetJobDetail(JobKey jobkey)
        {
           return await Scheduler.GetAwaiter().GetResult().GetJobDetail(jobkey);           
        }

        public static async Task<IReadOnlyCollection<ITrigger>> GetTriggersOfJob(JobKey jobkey)
        {
            return await Scheduler.GetAwaiter().GetResult().GetTriggersOfJob(jobkey);            
        }

        public static async Task<TriggerState> GetTriggerState(TriggerKey triggerkey)
        {
            return await Scheduler.GetAwaiter().GetResult().GetTriggerState(triggerkey);            
        }

        public static async Task<IReadOnlyCollection<string>> GetJobGroupNames() 
        {
            return await Scheduler.GetAwaiter().GetResult().GetJobGroupNames();           
        }

        public static async Task<IReadOnlyCollection<string>> GetTriggerGroupNames() 
        {
          return await Scheduler.GetAwaiter().GetResult().GetTriggerGroupNames();         
        }


        public static Task<IReadOnlyCollection<JobKey>> GetAllJob(string group) 
        {
            var groupMatcher = GroupMatcher<JobKey>.GroupContains(group);
            Task<IReadOnlyCollection<JobKey>> jobKeys = Scheduler.GetAwaiter().GetResult().GetJobKeys(groupMatcher);
            return jobKeys;
        }      
    }
}
