﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using Quartz.Impl;
using GenericJobs.Model;
using NHibernate;
using System.Threading.Tasks;
//using Quartz;

namespace GenericJobs.Model
{
    public abstract class GenericJob : IGenericJob, IJob
    {
        public GenericJob()
        {
            Stati = new HashSet<JobState>();
        }

        public GenericJob(string nome, string descrizione, string gruppo, IDictionary<string, string> dataMap)
        {
            Nome = nome;
            Gruppo = gruppo;
            DataMapConcrete = dataMap;
            Descrizione = descrizione;
            //ExecuteNow = executeNow; //spostati nel JobTriggerInfo
            //ScheduleAt = scheduleAt;
            JobType = this.GetType().ToString();

            Stati = new HashSet<JobState>();

            Save();
            Build();
        }

        #region Proprietà

        /// <summary>
        /// Identificativo univoco del job
        /// </summary>
        public virtual int ID
        {
            get;
            set;
        }

        /// <summary>
        /// Chiave identificativa del job
        /// </summary> 
        public virtual string Nome
        {
            get;
            set;
        }

        /// <summary>
        /// Nome del gruppo del job
        /// </summary>        
        public virtual string Gruppo
        {
            get;
            set;
        }

        /// <summary>
        /// Descrizione del job
        /// </summary>
        public virtual string Descrizione
        {
            get;
            set;
        }

        public virtual string JobType
        {
            get;
            set;
        }

        ///// <summary>
        ///// Data ora di esecuzione del job: se ExecuteNow e true viene ignorata
        ///// </summary>
        //public virtual DateTime ScheduleAt
        //{
        //    get;
        //    protected set;
        //}
        /// <summary>
        /// Data ora di creazione del job
        /// </summary>
        public virtual DateTime Creation
        {
            get;
            set;
        }


        //public virtual bool RepeatForEver
        //{
        //    get;
        //    set;
        //}      

        ///// <summary>
        ///// Indica l'esecuzione istantanea del job
        ///// </summary>
        //public virtual bool ExecuteNow
        //{
        //    get;
        //    protected set;
        //}


        public virtual Model.JobState.JobStatus StatoCorrente
        {
            get;
            set;
        }

        public virtual Model.JobState.JobStatus StatoPrecedente
        {
            get;
            set;
        }

        /// <summary>
        /// Lista di stati del job
        /// </summary>
        public virtual ICollection<JobState> Stati
        {
            get;
            set;
        }

        /// <summary>
        /// Permette di passare informazioni di vario tipo al job Quartz - utilizzando simpletype string,string
        /// </summary>
        public virtual IDictionary<string, string> DataMapConcrete
        {
            get;
            set;
        }

        /// <summary>
        /// Permette di passare informazioni di vario tipo al job Quartz 
        /// </summary>
        protected IDictionary<string, object> DataMap
        {
            get;
            set;
        }
        /// <summary>
        /// Quartz Job
        /// </summary>
        public virtual JobDetailImpl QuartzJob
        {
            get;
            set; // era private
        }

        public virtual JobTriggerInfo TriggerInfo
        {
            get;
            set;
        }

        //public virtual JobListener Listener 
        //{
        //    get 
        //    {
        //        return new JobListener(){ Name = this.QuartzJob.Name };
        //    }
        //}
        /// <summary>
        /// Quartz trigger
        /// </summary>
        public virtual ITrigger QuartzTrigger
        {
            get;
            set;
        }

        //protected virtual ExecuteCallBack ExecuteJob
        //{
        //    set;
        //    get;
        //}

        #endregion

        #region Metodi

        //public delegate bool ExecuteCallBack(IJobExecutionContext context);

        /// <summary>
        /// Salva il job nel db
        /// </summary>
        public virtual void Save()
        {
            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    JobState jobstate = new JobState();
                    jobstate.Data = DateTime.Now;
                    jobstate.Stato = JobState.JobStatus.No_Stato;
                    jobstate.Job = this;

                    this.Stati.Add(jobstate);
                    this.Creation = DateTime.Now;

                    // salvo il job con lo stato settato a no_stato                
                    BusinessLogic.JobsBL.SaveJob(this, session);
                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
            }
        }

        /// <summary>
        /// Costruisce il job e il trigger e li aggiunge allo scheduler
        /// </summary>
        public virtual void Build()
        {
            // Creazione del QuarzJob
            //+ ID.ToString()

            QuartzJob = new JobDetailImpl(this.Nome, this.Gruppo, this.GetType());

            if (!string.IsNullOrEmpty(this.Descrizione))
                QuartzJob.Description = Descrizione;

            if (DataMap != null)
            {
                DataMap.Add("ID", this.ID.ToString());
                DataMap.Add("ObjType", this.JobType);

                // leggere il datamapconcrete e deserializzare gli oggetti json
                foreach (KeyValuePair<string, string> entry in this.DataMapConcrete)
                {
                    var key = entry.Key;
                    var value = entry.Value;

                    DataMap.Add(key, value);
                }

                QuartzJob.JobDataMap = new JobDataMap(DataMap);
            }

            #region MyRegion
            //// Definizione dello scheduler
            //if (QuartzTrigger == null)
            //{
            //    if (ExecuteNow)
            //    {
            //        QuartzTrigger = TriggerBuilder
            //                        .Create()
            //                        .StartNow()
            //                        .WithSimpleSchedule()
            //                        .ForJob(QuartzJob)
            //                        .Build();
            //    }                 
            //    else
            //    {
            //        QuartzTrigger = TriggerBuilder
            //                       .Create()
            //                       .StartAt(ScheduleAt)
            //                       .WithSimpleSchedule()
            //                       .ForJob(QuartzJob)
            //                       .Build();
            //    }                  

            //}  

            //  
            #endregion

        }

        public virtual Task Execute(IJobExecutionContext context)
        {
            // recupero l'id del job dal context              
            int JobID = context.JobDetail.JobDataMap.GetIntValue("ID");

            return Task.CompletedTask;
        }

        public virtual void ChangeState(JobState.JobStatus newstate)
        {
            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    JobState jobstate = new JobState();
                    jobstate.Data = DateTime.Now;
                    jobstate.Stato = newstate;

                    // salvo lo stato
                    BusinessLogic.JobsBL.ChangeStato(this.ID, jobstate, session);
                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
            }
        }

        // public virtual void SetCallBackMethod(ExecuteCallBack callback)
        // {
        //     // conservo nel contesto del job corrente la funzione di callback da invocare
        //     QuartzJob.JobDataMap["CallbackMethod"] = callback;

        //     int idJob = QuartzJob.JobDataMap.GetIntValue("ID");

        ////     JobsScheduler.AddJob(QuartzJob, QuartzTrigger);

        //     using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
        //     using (ITransaction tx = session.BeginTransaction())
        //     {
        //         try
        //         {
        //             // salvo lo stato del job corrente                       
        //             JobState jobstate = new JobState();
        //             jobstate.Data = DateTime.Now;
        //             jobstate.Stato = JobState.JobStatus.Schedulato;

        //             BusinessLogic.JobsBL.ChangeStato(idJob, jobstate, session);
        //             tx.Commit();
        //         }
        //         catch (Exception ex)
        //         {
        //             tx.Rollback();
        //         }
        //     } 
        // }

        #endregion

        #region Enumeratori

        public enum Column
        {
            ID,
            Nome,
            Gruppo,
            Descrizione,
            JobType,
            ScheduleAt,
            Creation,
            Stati,
            Stato,
            StatoCorrente,
            StatoPrecedente,
            ExecuteNow,
            TriggerInfo
        }

        //public enum TriggerType
        //{
        //    Simple,


        //}
        #endregion
    }
}
