﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using NHibernate;
using System.Threading;
using System.Threading.Tasks;

namespace GenericJobs.Model
{
    public class GenericJobListener : IJobListener
    {
        public GenericJobListener(GenericJob job) 
        {
            this.currentJob = job;
            this.Name = job.Nome;
        }

        private GenericJob currentJob;

        Task IJobListener.JobExecutionVetoed(IJobExecutionContext context, CancellationToken cancellationToken)
        {
            JobKey jobkey = context.JobDetail.Key;

            return Task.CompletedTask;
        }

        Task IJobListener.JobToBeExecuted(IJobExecutionContext context, CancellationToken cancellationToken)
        {
            JobKey jobkey = context.JobDetail.Key;

            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    //JobState jobstate = new JobState() { Data = DateTime.Now, Stato = JobState.JobStatus.In_Esecuzione };
                    //ChangeStato(currentJob.ID, jobstate, session);
                    ChangeStato(currentJob.ID, JobState.JobStatus.In_Esecuzione, session);
                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    // loggare l'errore
                }
            }
            return Task.CompletedTask;
        }

        Task IJobListener.JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException, CancellationToken cancellationToken)
        {
            JobKey jobkey = context.JobDetail.Key;

            #region MyRegion
            //using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            //using (ITransaction tx = session.BeginTransaction())
            //{
            //    try
            //    {
            //        JobState jobstate = new JobState();
            //        jobstate.Data = DateTime.Now;
            //        jobstate.Stato = JobState.JobStatus.Completo;

            //        // salvo lo stato
            //        ChangeStato(currentJob.ID, jobstate, session);
            //        tx.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        tx.Rollback();
            //        // loggare l'errore
            //    }
            //} 
            #endregion

            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                  //  JobState jobstate = new JobState() { Data = DateTime.Now, Stato = JobState.JobStatus.Completo};
                  //  ChangeStato(currentJob.ID, jobstate, session);
                    ChangeStato(currentJob.ID, JobState.JobStatus.Completo, session);

                    if (currentJob.TriggerInfo != null && currentJob.TriggerInfo.RepeatForEver)
                    {
                        JobState jobstateRepeat = new JobState() { Data = DateTime.Now, Stato = JobState.JobStatus.Schedulato };
                        //ChangeStato(currentJob.ID, jobstateRepeat, session);
                        ChangeStato(currentJob.ID, JobState.JobStatus.Schedulato, session);
                    }

                    tx.Commit();

                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
            }
            return Task.CompletedTask;
        }

        public void ChangeStato(int jobID, JobState.JobStatus statoDaCambiare, ISession innerSession)
        {
            GenericJob job = BusinessLogic.JobsBL.GetJob(jobID, innerSession);

            JobState.JobStatus oldstato = job.StatoCorrente;

            //GenericDAO.DAO.CriteriaNhibernateDAO<JobState> daoJobState = new GenericDAO.DAO.CriteriaNhibernateDAO<JobState>(innerSession);

            //JobState newstato = new JobState();
            //newstato.Data = DateTime.Now;
            //newstato.Stato = statoDaCambiare;
            //newstato.Job = job;
            //daoJobState.SaveOrUpdate(newstato);

            job.StatoPrecedente = oldstato;   // setto lo stato precedente ricopiandolo
            job.StatoCorrente = statoDaCambiare;        // aggiorno lo stato corrente
            
            GenericDAO.DAO.CriteriaNhibernateDAO<GenericJob> dao = new GenericDAO.DAO.CriteriaNhibernateDAO<GenericJob>(innerSession);            
           // job.Stati.Add(newstato);
            dao.SaveOrUpdate(job);
        }                

        public virtual string Name
        {
            get;
            private set;
        }
    }
}
