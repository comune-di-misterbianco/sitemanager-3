﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericJobs.Model
{
    public class JobTriggerInfo
    {             
        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Name
        {
            get;
            set;
        }

        public virtual int Seconds
        {
            get;
            set;
        }

        public virtual int Minutes
        {
            get;
            set;
        }

        public virtual int Hours
        {
            get;
            set;
        }

        public virtual bool ExecuteNow
        {
            get;
            set;
        }

        public virtual GenericJob Job
        {
            get;
            set;
        }

        public virtual DateTime StartTime
        {
            get;
            set;
        }

        public virtual DateTime EndTime
        {
            get;
            set;
        }

        public virtual TriggerType TypeOfTrigger
        {
            get;
            set;
        }
        
        public virtual bool RepeatForEver
        {
            get;
            set;
        }

        public virtual string Gruppo
        {
            get;
            set;
        }


        public enum TriggerType
        {
            Simple,
            Seconds,
            Minutes,            
            Hours
        }

        public enum Column 
        {
            ID,
            Name,
            Seconds,
            Minutes,
            Hours,
            ExecuteNow,
            Job,
            StartTime,
            EndTime,
            TypeOfTrigger,
            RepeatForEver,
            Gruppo,
        }

        public JobTriggerInfo()
        {

        }       
    }
}
