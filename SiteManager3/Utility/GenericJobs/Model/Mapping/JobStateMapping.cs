﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace GenericJobs.Model.Mapping
{
    public class JobStateMapping : ClassMap<JobState>
    {
        public JobStateMapping()
        {
            Table("scheduler_jobs_stati");
            Id(x => x.ID);
            Map(x => x.Data).Not.Nullable();
            Map(x => x.Stato);
            References(x => x.Job)
                .Class(typeof(GenericJob))
                .Column("IdJob")
                .Fetch.Select(); 
        }
    }
}
