﻿using System;
using FluentNHibernate.Mapping;

namespace GenericJobs.Model.Mapping
{
    public class GenericJobMapping : ClassMap<GenericJob>
    {
        public GenericJobMapping()
        {
            Table("scheduler_jobs");
            Id(x => x.ID);
            Map(x => x.Nome);
            Map(x => x.Descrizione);
            Map(x => x.JobType);
            Map(x => x.Creation);
            Map(x => x.Gruppo).Nullable();            
            //Map(x => x.ScheduleAt);
            //Map(x => x.ExecuteNow);
            Map(x => x.StatoCorrente); // verificare comportamento
            Map(x => x.StatoPrecedente);     
                
            
            HasOne(x => x.TriggerInfo)                
                .PropertyRef(x => x.Job)
                .Fetch.Select()
                .Cascade.All();

            HasMany(x => x.Stati)
                .KeyColumn("IdJob")
                .Fetch.Select()                
                .AsSet()
                .Cascade.All();

            HasMany(x => x.DataMapConcrete)
                .Table("scheduler_datamaps")
                //.AsMap("key")
                .AsMap<string>(
                    index => index.Column("Chiave").Type<string>(),
                    element => element.Length(20000).Column("Data").Type<string>())
                .KeyColumn("IdJob")
                .Cascade.AllDeleteOrphan(); 
                //.Element("data");

            
        }
    }
}
