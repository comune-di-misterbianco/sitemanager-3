﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericJobs.Model.Mapping
{
    public class JobTriggerInfoMapping : ClassMap<JobTriggerInfo>
    {
        public JobTriggerInfoMapping()
        {
            Table("scheduler_triggerinfo");
            Id(x => x.ID);
            Map(x => x.Name);
            Map(x => x.Hours);
            Map(x => x.Minutes);
            Map(x => x.Seconds);
            Map(x => x.StartTime);
            Map(x => x.EndTime);
            Map(x => x.ExecuteNow);
            Map(x => x.RepeatForEver);
            Map(x => x.TypeOfTrigger);
            Map(x => x.Gruppo);

            References(x => x.Job, "RifJob")
              .Nullable();//.Cascade.All();
        }
    }
}
