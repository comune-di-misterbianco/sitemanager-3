﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Vertical;
using System.Reflection;

namespace GenericJobs
{
    public class GenericJobApps : VerticalApplication
    {
        public GenericJobApps() : base() { }
        public GenericJobApps(Assembly assembly)
            : base(assembly)
        {

        }
        public static NetCms.Vertical.Configuration.Configuration Configuration
        {
            get
            {
                if (_Configuration == null)
                {
                    var type = typeof(GenericJobApps);
                    if (!Configs.ContainsKey(type.FullName))
                        Configs.Add(type.FullName, new NetCms.Vertical.Configuration.Configuration(SystemNameKey));
                    _Configuration = (NetCms.Vertical.Configuration.Configuration)Configs[type.FullName];
                }
                return _Configuration;
            }
        }
        private static NetCms.Vertical.Configuration.Configuration _Configuration;

        public override bool NeedAdditionalConfiguration
        {
            get { return false; }
        }

        public override int SecurityLevel
        {
            get { return 0; }
        }

        public override int HomepageGroup
        {
            get { return 2; }
        }

        public override string SystemName
        {
            get { return SystemNameKey; }
        }

        public override string Label
        {
            get { return "Task Scheduler"; }
        }

        public override string Description
        {
            get { return "Area di gestione dei task del cms"; }
        }

        public override bool NetworkDependant
        {
            get { return false; }
        }

        public override VerticalApplication.AccessModes AccessMode
        {
            get { return AccessModes.Admin; }
        }

        public override bool CheckForDefaultGrant
        {
            get { return false; }
        }

        public override VerticalApplicationSetup Installer
        {
            get
            {
                if (_Installer == null)
                    _Installer = new GenericJobsAppSetup(this);
                return _Installer;
            }
        }
        private GenericJobsAppSetup _Installer;

        public override int ID
        {
            get { return IDKey; }
        }
        public const string SystemNameKey = "genericjobsapp";
        public const int IDKey = 2216;

        public override bool IsSystemApplication
        {
            get
            {
                return false;
            }
        }

        public override bool DoAdditionalConfigurationForExternalDB()
        {
            return false;
        }
        public override bool UseCmsSessionFactory
        {
            get { return true; }
        }

        public override ICollection<Assembly> FluentAssembliesRelated
        {
            get
            {
                ICollection<Assembly> coll = new List<Assembly>();
                coll.Add(this.Assembly);
                return coll;
            }
        }
       
        public override string BackofficeUrl
        {
            get
            {
                return "/applications/" + SystemNameKey + "/admin/default.aspx";
            }
        }
    }

}
