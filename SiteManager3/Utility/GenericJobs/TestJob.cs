﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.IO;
//using Quartz;
//using GenericJobs.Model;
//using GenericJobs.BusinessLogic;
//using System.Web.Script.Serialization;

//namespace GenericJobs
//{
//    public class TestJob
//    {

//        public List<string> Destinatari
//        {
//            get
//            {
//                return new List<string> { "angelo.strano@net-serv.it", "angelo.strano@gmail.com", "gicorsaro@gmail.com", "castagna@net-serv.it", "massa@net-serv.it", "micale@net-serv.it" };
//            }
//        }

//        /// <summary>
//        /// Esempio di implementazione
//        /// </summary>
//        public TestJob(bool executeNow, DateTime dateTimeToExecute)
//        {
//            IDictionary<string, string> DataMap = new Dictionary<string, string>();
//            DataMap.Add("subject", "Test invio 1");
//            DataMap.Add("body", "Test di prova");

//            var json = new JavaScriptSerializer().Serialize(Destinatari);

//            DataMap.Add("destinatari", json);
//            DataMap.Add("provider", "Google");           

//            CustomJob myJob = new CustomJob("Testing", "Prova nuovo custom job", "TestJob", DataMap);

//            // inserire il trigger info
//            GenericJobs.Model.JobTriggerInfo triggerInfo = new GenericJobs.Model.JobTriggerInfo();

//            triggerInfo.Job = myJob;
//            triggerInfo.Minutes = 15;
//            triggerInfo.StartTime = TimeZone.CurrentTimeZone.ToLocalTime(dateTimeToExecute);
//            triggerInfo.Name = "Trigger di prova";
//            triggerInfo.RepeatForEver = true;
//            triggerInfo.ExecuteNow = executeNow;
//            triggerInfo.TypeOfTrigger = JobTriggerInfo.TriggerType.Simple;
            
//            myJob.TriggerInfo = triggerInfo;
//       //     myJob.SetCallBackMethod(ExecuteJob);

//            JobTriggerInfoBL.Save(triggerInfo);
//            JobsScheduler.AddSimpleJob(myJob);
//        }

//        //public bool ExecuteJob(IJobExecutionContext context)
//        //{
//        //    bool status = false;

//        //    try
//        //    {
//        //        JobDataMap jMap = context.JobDetail.JobDataMap;

//        //        string subject = jMap.GetString("subject");
//        //        string body = jMap.GetString("body");
//        //        List<string> Destinatari = jMap.Get("destinatari") as List<string>;
//        //        string provider = jMap.GetString("provider");

//        //        string pathFile = System.Web.HttpContext.Current.Server.MapPath(NetCms.Configurations.Paths.AbsoluteConfigRoot + "\test.txt");

//        //        StreamWriter file = new StreamWriter(pathFile, true);
//        //        foreach (string destinatario in Destinatari)
//        //        {
//        //            file.WriteLine(destinatario + " ------ " + DateTime.Now.ToString());
//        //        }
                
//        //        file.Close();
//        //        status = true;
//        //    }
//        //    catch (Exception ex)
//        //    {

//        //    }

//        //    return status;
//        //}
//    }
//}
