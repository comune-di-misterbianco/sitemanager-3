﻿using GenericJobs.BusinessLogic;
using GenericJobs.Example;
using GenericJobs.Forms;
using GenericJobs.Model;
using NetCms.GUI;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GenericJobs.Views
{
    [DynamicUrl.PageControl("/jobs/addcustomjob.aspx")]
    public class ConsoleJobAdd : BasePage
    {
        public ConsoleJobAdd(StateBag viewstate, bool IsPostBack, NetCms.GUI.Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewstate, IsPostBack, toolbar, informationBox, networkKey)
        { 
        }

        private void SetToolbar()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("../default.aspx", NetCms.GUI.Icons.Icons.Arrow_Right, "Indietro"));
        }

        public override string PageTitle
        {
            get { return "Add new Console Job"; }
        }


        private ConsoleJobForm _TaskForm;
        public ConsoleJobForm TaskForm
        {
            get
            {
                if (_TaskForm == null)
                {
                    _TaskForm = new ConsoleJobForm("taskform");
                    _TaskForm.SubmitButton.Text = "Aggiungi";
                    _TaskForm.CssClass = "Axf";
                    _TaskForm.SubmitButton.Click += SubmitButton_Click;
                }

                return _TaskForm;
            }
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            bool result = false;

            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    if (this.TaskForm.IsValid == NetService.Utility.ValidatedFields.VfGeneric.ValidationStates.Valid)
                    {
                        DateTime dataJob = (DateTime)this.TaskForm.StartDateTime.PostbackValueObject;
                        string testo = this.TaskForm.Text.PostBackValue;

                        IDictionary<string, string> DataMap = new Dictionary<string, string>();
                        DataMap.Add("text", testo);

                        Random jobRndIdentify = new Random();
                        int jobIdentify = jobRndIdentify.Next(3, 5);
                        ConsoleJob job = new ConsoleJob("Test job" + jobIdentify, "Console job di esempio","Examples", DataMap);

                        GenericJobs.Model.JobTriggerInfo triggerInfo = new GenericJobs.Model.JobTriggerInfo();
                        triggerInfo.Job = job;
                        triggerInfo.StartTime = dataJob;
                        triggerInfo.Name = job.Nome;
                        triggerInfo.ExecuteNow = false;
                        triggerInfo.Gruppo = job.Gruppo;
                        triggerInfo.TypeOfTrigger = GenericJobs.Model.JobTriggerInfo.TriggerType.Minutes;
                        triggerInfo.Minutes = 30;
                        triggerInfo.RepeatForEver = true;
                       
                        job.TriggerInfo = triggerInfo;
                        
                        JobTriggerInfoBL.Save(triggerInfo, session);
                        JobsScheduler.AddMinuteForeverJob(job);

                        tx.Commit();

                        result = true;
                    }
                }
                catch (Exception ex)
                {                    
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la creazione del job." + typeof(ConsoleJobAdd) + ". " + ex.Message);
                    tx.Rollback();
                }
            }
            if (result)
            {
                PopupBox.AddSessionMessage("Task salvato con successo.", PostBackMessagesType.Success, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(ConsoleJobAdd)));
            }
            else
            {
                PopupBox.AddSessionMessage("Errore durante il salvataggio del task", PostBackMessagesType.Error, NetCms.Vertical.PagesHandler.GetPageAddress(typeof(ConsoleJobAdd)));
            }
        }

        public override System.Web.UI.WebControls.WebControl BuildPage()
        {
            SetToolbar();

            WebControl webcontrol = new WebControl(HtmlTextWriterTag.Div);
            webcontrol.ID = Application.SystemName;
            WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            legend.Controls.Add(new LiteralControl("<strong>Nuovo Task per eliminazione referti</strong>"));

            fieldset.Controls.Add(legend);
            fieldset.Controls.Add(TaskForm);

            webcontrol.Controls.Add(fieldset);

            return webcontrol;
        }


    }
}
