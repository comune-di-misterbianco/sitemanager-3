﻿using NetService.Utility.ValidatedFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericJobs.Forms
{
    public class ConsoleJobForm : VfManager
    {
        public ConsoleJobForm(string controlID) : base(controlID) 
        {
            this.Fields.Add(StartDateTime);
            this.Fields.Add(Text);
        }

        private VfDateCalendar _StartDateTime;
        public VfDateCalendar StartDateTime
        {
            get
            {
                if (_StartDateTime == null)
                    _StartDateTime = new VfDateCalendar("startDateTime", "Data avvio", "startDateTime", true, typeof(DateTime));
                return _StartDateTime;
            }

        }

        private VfTextBox _Text;
        public VfTextBox Text
        {
            get
            {
                if (_Text == null)
                {
                    _Text = new VfTextBox("textConsole", "Testo");
                    _Text.Required = true;
                    _Text.Rows = 3;
                    _Text.Columns = 50;
                    _Text.TextBox.TextMode = System.Web.UI.WebControls.TextBoxMode.MultiLine;
                }
                return _Text;
            }
        }
    }
}
