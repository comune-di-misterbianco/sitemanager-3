﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using GenericJobs.Model;
using GenericDAO.DAO.Utility;
using System.Web.UI.WebControls;

namespace GenericJobs.Views
{
    [DynamicUrl.PageControl("/detail.aspx")]
    public class JobDetail : BasePage
    {
        public JobDetail(StateBag viewstate, bool IsPostBack, NetCms.GUI.Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewstate, IsPostBack, toolbar, informationBox, networkKey)
        {
            CurrentJob = new NhRequestObject<GenericJob>( GenericJobs.BusinessLogic.JobsBL.GetSession(), "job");
        }

        public override string PageTitle
        {
            get { return "Dettaglio task"; }
        }

        private NhRequestObject<GenericJob> CurrentJob; 

        public override System.Web.UI.WebControls.WebControl BuildPage()
        {
            WebControl dettaglio = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
            dettaglio.ID = Application.SystemName;

            string txtStato = "<li>Data ora: {0} Stato: {1}</li>";
            string txtStati = "";

            List<JobState> stati = CurrentJob.Instance.Stati.OrderByDescending(x => x.Data).ThenByDescending(x=> x.ID).ToList();

            foreach (JobState stato in stati)
            { 
                txtStati += string.Format(txtStato,stato.Data.ToString(), stato.Stato.ToString().Replace("_"," "));
            }

            string txtDetail = @"
                                <fieldset>
                                    <legend>Detteglio task: "+ CurrentJob.Instance.Nome+@"</legend>
                                    <p>
                                       Descrizione: " + CurrentJob.Instance.Descrizione + @"
                                       Data ora creazione: " + CurrentJob.Instance.Creation.ToString() + @" 
                                       Data ora esecuzione: " + CurrentJob.Instance.TriggerInfo.StartTime.ToString() + @"                    
                                    </p>                                    
                                    "+txtStati+@"
                                </fieldset>
                                ";


            dettaglio.Controls.Add(new LiteralControl(txtDetail));

            return dettaglio;
        }
    }
}
