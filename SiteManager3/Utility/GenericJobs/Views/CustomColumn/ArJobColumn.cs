﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ArTable;
using System.Web.UI.WebControls;
using GenericJobs.Model;

namespace GenericJobs.Views.CustomColumn
{
    public class ArJobColumn : ArAbstractColumn
    {
        public ArJobColumn(string caption, string key)
            : base(caption, key)
        {
        
        }

        public override bool ExportData
        {
            get { return false; }
        }

        public override System.Web.UI.WebControls.TableCell GetCellControl(object objectInstance)
        {
            var value = objectInstance is GenericJob ? objectInstance as GenericJob : GetValue(objectInstance, this.Key) as GenericJob;
        
            TableCell td = new TableCell();
            if (!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;

            var txtTd = @"<span class=""jobName""><a href=""detail.aspx?job=" + value.ID.ToString() + @""">" + value.Nome + @"</a></span><br />
                          <span class=""jobDescription"">" + value.Descrizione + @"</span>";
            if (txtTd == null)
                td.Text = string.Format("");
            else 
                td.Text = string.Format(txtTd);

            return td;
        }

        protected override object GetValue(object objectInstance, string Property)
        {
            var value = GetValue(objectInstance, this.Key);
            string CellText = value == null ? "" : value.ToString();
            return CellText;
        }

        public override string GetExportValue(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
