﻿using System;
using System.Web.UI.WebControls;
using System.Linq;
using NetService.Utility.ArTable;
using System.Web.UI;

namespace GenericJobs.Views.CustomColumn
{
    /// <summary>
    /// Colonna Checkbox per ArTable con l'id del controllo uguale al valore della proprietà CheckBoxIDOffset e valore uguale a quello della proprietà 
    /// primaryKeyPropertyName passata nel costrutture.
    /// Viene usato per associare liste di oggetti identificati con l'id ad una entità specifica, solo in fase di inserimento. 
    /// </summary>
    public class ArCheckBoxColumnWithValue : ArAbstractColumn
    {
        public string CheckBoxIDOffset
        {
            get
            {
                return _CheckBoxIDOffset;
            }
            set
            {
                _CheckBoxIDOffset = value;
            }
        }
        private string _CheckBoxIDOffset = "chk";

        private string IdControl;

        public ArCheckBoxColumnWithValue(string caption, string primaryKeyPropertyName, string idControl)
            : base(caption, primaryKeyPropertyName)
        {
            IdControl = idControl;
        }

        public override TableCell GetCellControl(object objectInstance)
        {

            string CellID = GetValue(objectInstance, this.Key).ToString();
            string Stato = "";
            NetService.Utility.Common.RequestVariable valore = new NetService.Utility.Common.RequestVariable(CheckBoxIDOffset, NetService.Utility.Common.RequestVariable.RequestType.Form);
            if (valore.IsValidString)
            {
                string[] valori = valore.StringValue.Split(',');
                if (valori.Contains(CellID))
                    Stato = @" checked=""checked""";
            }
            string checkModel = @"<input type=""checkbox"" name=""{0}""  id=""{0}"" value=""{1}"" {2}>";


            TableCell td = new TableCell();
            if (!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;
            td.Text = "<span>" + string.Format(checkModel, CheckBoxIDOffset, CellID, Stato) + "</span>";

            return td;
        }

        public override TableHeaderCell GetHeaderCellControl()
        {
            TableHeaderCell headerCell = new TableHeaderCell();

            string checkModel = @"<input type=""checkbox"" name=""checkAll""  id=""checkAll"" onclick=""jqCheckAll(this.id,'" + IdControl + @"');"">
                                  <label for=""checkAll"">" +ColumnCaption+@"</label>
                                      <script type=""text/javascript"">
                                       function jqCheckAll(id, containerID) {
                                                var checkboxes = $(""#""+containerID+"" input:checkbox"");
	                                            checkboxes.attr('checked', $('#' + id).is(':checked'));
                                      }
                                      </script>
                                     ";
            headerCell.Text = checkModel;

            return headerCell;
        }

        public override bool ExportData
        {
            get { return false; }
        }
        public override string GetExportValue(object obj)
        {
            throw new NotImplementedException();
        }
    }
}