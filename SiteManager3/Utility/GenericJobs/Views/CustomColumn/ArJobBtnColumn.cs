﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ArTable;
using System.Web.UI.WebControls;
using GenericJobs.Model;

namespace GenericJobs.Views.CustomColumn
{
    public class ArJobBtnColumn : ArAbstractColumn
    {
        public ArJobBtnColumn(string caption, string key, string idOffset)
            : base(caption, key)
        {
            BtnIDOffset = idOffset;
        }  

        public string BtnIDOffset
        {
            get
            {
                return _BtnIDOffset;
            }
            set { _BtnIDOffset = value; }
        }
        private string _BtnIDOffset = "btn";

        #region Btns

        private Button EliminaJobBtn;
        //public Button EliminaJobBtn 
        //{
        //    get {
        //        if (_EliminaJobBtn == null)
        //        {
        //            _EliminaJobBtn = new Button();
        //            _EliminaJobBtn.Text = "Elimina";
        //        }
        //        return _EliminaJobBtn;
        //    }
        //    set {
        //        _EliminaJobBtn = value;
        //    }
        //}

        #endregion

        #region BtnsEventHandler

        public event EventHandler AvviaClick;
        public event EventHandler StopClick;
        public event EventHandler SchedulaClick;
        public event EventHandler PausaClick;        
        public event EventHandler RiavviaClick;       

        public EventHandler EliminaClickHandler
        {
            get
            {
                return _EliminaClickHandler;
            }
            set
            {
                _EliminaClickHandler = value;
            }
        }
        private EventHandler _EliminaClickHandler;

        //private void SetEliminaClickEvent()
        //{
        //    if (EliminaClickHandler != null)
        //        EliminaJobBtn.Click += EliminaClickHandler;
        //}

        #endregion

        public override bool ExportData
        {
            get { return false; }
        }
            
        public override TableCell GetCellControl(object objectInstance)
        {
            var value = objectInstance is GenericJob ? objectInstance as GenericJob : GetValue(objectInstance   , this.Key) as GenericJob;

            TableCell td = new TableCell();
            if (!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;

            #region Settings Btn Attributes 
            /* Btn Settings */

            Button Pausa = new Button();
            Pausa.Text = "Pausa"; 
            Pausa.ID = this.BtnIDOffset + "_pausa_" + value.ID.ToString();
            Pausa.Attributes["name"] = this.BtnIDOffset + "_pausa_" + value.ID.ToString();
            Pausa.Click += PausaClick;

            Button Avvia = new Button();
            Avvia.Text = "Avvia";  
            Avvia.ID = this.BtnIDOffset + "_avvia_" + value.ID.ToString();
            Avvia.Attributes["name"] = this.BtnIDOffset + "_avvia_" + value.ID.ToString();
            Avvia.Click += AvviaClick;

            //Button Elimina = new Button();
            EliminaJobBtn = new Button();
            EliminaJobBtn.Text = "Elimina";
            EliminaJobBtn.ID = this.BtnIDOffset + "_elimina_" + value.ID.ToString();
            EliminaJobBtn.Attributes["name"] = this.BtnIDOffset + "_elimina_" + value.ID.ToString();
            //SetEliminaClickEvent();
            EliminaJobBtn.Click += EliminaClickHandler;

            Button Stop = new Button();
            Stop.Text = "Arresta";         
            Stop.ID = this.BtnIDOffset + "_stop_" + value.ID.ToString();
            Stop.Attributes["name"] = this.BtnIDOffset + "_stop_" + value.ID.ToString();
            Stop.Click += StopClick;

            Button Schedula = new Button();
            Schedula.Text = "Programma";       
            Schedula.ID = this.BtnIDOffset + "_schedula_" + value.ID.ToString();
            Schedula.Attributes["name"] = this.BtnIDOffset + "_schedula_" + value.ID.ToString();
            Schedula.Click += SchedulaClick;

            Button Riesegui = new Button();
            Riesegui.Text = "Riavvia"; 
            Riesegui.ID = this.BtnIDOffset + "_riesegui_" + value.ID.ToString();
            Riesegui.Attributes["name"] = this.BtnIDOffset + "_riesegui_" + value.ID.ToString();
            Riesegui.Click += RiavviaClick;

            #endregion
            
            WebControl btnDiv = new WebControl(System.Web.UI.HtmlTextWriterTag.Span);
            btnDiv.CssClass = "jobsBtn";

            JobState statoCorrente = value.Stati.OrderByDescending(x => x.Data).ThenByDescending(x => x.ID).ToList().FirstOrDefault();

            #region Appesa btn
            switch (statoCorrente.Stato)
            {
                case JobState.JobStatus.Schedulato:
                    {
                        btnDiv.Controls.Add(Avvia);
                        btnDiv.Controls.Add(EliminaJobBtn);
                    }
                    break;
                case JobState.JobStatus.In_Esecuzione:
                    {
                        btnDiv.Controls.Add(Pausa);
                        btnDiv.Controls.Add(Stop);
                        btnDiv.Controls.Add(EliminaJobBtn);
                    }
                    break;
                case JobState.JobStatus.In_Pausa:
                    {
                        btnDiv.Controls.Add(Avvia);
                        btnDiv.Controls.Add(Stop);
                        btnDiv.Controls.Add(EliminaJobBtn);
                    }
                    break;
                case JobState.JobStatus.Stop:
                    {
                        //btnDiv.Controls.Add(Avvia); non eseguibile
                        btnDiv.Controls.Add(EliminaJobBtn);
                    }
                    break;
                case JobState.JobStatus.Completo:
                    {
                        //btnDiv.Controls.Add(Riesegui); // non implementato
                        btnDiv.Controls.Add(EliminaJobBtn);
                    }
                    break;
                case JobState.JobStatus.Errore:
                    {
                        //btnDiv.Controls.Add(Riesegui); // non implementato
                        btnDiv.Controls.Add(EliminaJobBtn);
                    }
                    break;
                case JobState.JobStatus.Non_Schedulato:
                    {
                        // btnDiv.Controls.Add(Schedula); // non implementato
                        btnDiv.Controls.Add(EliminaJobBtn);
                    }
                    break;
                case JobState.JobStatus.No_Stato:
                    {
                        // btnDiv.Controls.Add(Schedula); // non implementato
                        btnDiv.Controls.Add(EliminaJobBtn);
                    }
                    break;
            } 
            #endregion

            td.Controls.Add(btnDiv);

            return td;
        }
       
        protected override object GetValue(object objectInstance, string Property)
        {
            var value = GetValue(objectInstance, this.Key);
            string CellText = value == null ? "" : value.ToString();
            return CellText;
        }

        public override string GetExportValue(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
