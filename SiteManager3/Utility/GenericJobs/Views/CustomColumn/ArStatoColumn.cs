﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ArTable;
using System.Web.UI.WebControls;
using GenericJobs.Model;

namespace GenericJobs.Views.CustomColumn
{
    public class ArStatoColumn : ArAbstractColumn
    {
        public ArStatoColumn(string caption, string key)
            : base(caption, key)
        {
        
        }

        public override bool ExportData
        {
            get { return false; }
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            var CellText = GetValue(objectInstance, this.Key);

            TableCell td = new TableCell();
            if (!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;
            td.Text = @"<span class=""jobState"">" + CellText.ToString().Replace("_", " ") + @"</span>";

            return td;
        }

        //public override TableCell GetCellControl(object objectInstance)
        //{
        //    var value = objectInstance is GenericJob ? objectInstance as GenericJob : GetValue(objectInstance, this.Key) as GenericJob;

        //    TableCell td = new TableCell();
        //    if (!string.IsNullOrEmpty(this.CssClass))
        //        td.CssClass = this.CssClass;

        //    //var maxData = (from stato in value.Stati select stato.Data).Max();

        //    //var statoJob =(from p in value.Stati
        //    //               where (p.Data == maxData)
        //    //               select p.Stato).First();

        //    JobState statoJob = value.Stati.OrderByDescending(x => x.Data).ThenByDescending(x => x.ID).ToList().FirstOrDefault();

        //    string txtTd = null;
           
        //    if (statoJob != null)
        //    txtTd = @"<span class=""jobState"">" + statoJob.Stato.ToString().Replace("_", " ") + @"</span>";
                          
        //    if (txtTd == null)
        //        td.Text = string.Format("");
        //    else
        //        td.Text = string.Format(txtTd);

        //    return td;
        //}

        //protected override object GetValue(object objectInstance, string Property)
        //{
        //    var value = GetValue(objectInstance, this.Key);
        //    string CellText = value == null ? "" : value.ToString();
        //    return CellText;
        //}

        public override string GetExportValue(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
