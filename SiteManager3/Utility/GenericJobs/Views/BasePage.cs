﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.Vertical;
using NetCms.Users;


namespace GenericJobs.Views
{
    public abstract class BasePage : NetCms.Vertical.ApplicationPageEx
    {

        public BasePage(StateBag viewstate, bool IsPostBack, NetCms.GUI.Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewstate, IsPostBack, toolbar, informationBox, networkKey)
        {

        }

        public override string PageTitle
        {
            get { return ""; }
        }

        private GenericJobApps _Application;
        public GenericJobApps Application
        {
            get 
            {
                if (_Application == null)
                {
                    _Application = new GenericJobApps();
                }
                return _Application; 
            }
          
        }    

        public override WebControl Control
        {
            get
            {
                CheckStatus();
                switch (this.CurrentView)
                {
                    case ViewsTypes.NotGranted:
                        return GetErrorControl();
                    case ViewsTypes.Error:
                        return GetErrorControl();
                    default:
                        var control = BuildPage();
                        return control;
                }
            }
        }

        public ApplicationPageStatusValidator StatusValidatorInException { get; private set; }

        public enum ViewsTypes { Default, Error, NotGranted, Login }

        public ViewsTypes CurrentView { get; private set; }

        private void CheckStatus()
        {
            StatusValidatorInException = ValidateStatus();

            bool error = StatusValidatorInException == null;
            if (!error) CurrentView = ViewsTypes.Error;
        }
        protected ApplicationPageStatusValidator ValidateStatus()
        {
            return StatusValidators.FirstOrDefault(validator => !validator.Validate());
        }

        public List<ApplicationPageStatusValidator> StatusValidators
        {
            get
            {
                return _StatusValidators ?? (_StatusValidators = new List<ApplicationPageStatusValidator>());
            }
        }
        private List<ApplicationPageStatusValidator> _StatusValidators;        

        protected WebControl GetErrorControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.CssClass = Application.SystemName + "_fieldset";
            control.Controls.Add(new TextWebControl(HtmlTextWriterTag.Legend, StatusValidatorInException.ErrorTitle));
            var p = new TextWebControl(HtmlTextWriterTag.P, StatusValidatorInException.ErrorMessage);
            control.Controls.Add(p);
            return control;
        }

        protected WebControl GetErrorControl(string titoloErrore, string messaggioErrore)
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.CssClass = Application.SystemName + "_fieldset";
            control.Controls.Add(new TextWebControl(HtmlTextWriterTag.Legend, titoloErrore));
            var p = new TextWebControl(HtmlTextWriterTag.P, messaggioErrore);
            control.Controls.Add(p);
            return control;
        }

        protected WebControl GetErrorControl(string titoloErrore, string messaggioErrore, string urlBack)
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Fieldset);
            control.CssClass = Application.SystemName + "_fieldset";
            control.Controls.Add(new TextWebControl(HtmlTextWriterTag.Legend, titoloErrore));
            var p = new TextWebControl(HtmlTextWriterTag.P, messaggioErrore);
            control.Controls.Add(p);

            HyperLink backLink = new HyperLink();
            backLink.NavigateUrl = urlBack;
            backLink.Text = "Torna Indietro";
            control.Controls.Add(backLink);

            return control;
        }

        public abstract WebControl BuildPage();

        public User CurrentCmsUser
        {
            get
            {
                if (NetCms.Users.AccountManager.Logged)
                {
                    // recupero il current account dalla businessLogic di user in modo da individuare l'utente associato alla session di nhibernate
                    User currentCmsUser = NetCms.Users.UsersBusinessLogic.GetById(NetCms.Users.AccountManager.CurrentAccount.ID);
                    return currentCmsUser;
                }
                return null;
            }
        }
    }
}
