﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetService.Utility;
using NetService.Utility.ArTable;
using NetService.Utility.Controls;
using Quartz;
using Quartz.Impl.Matchers;
using GenericJobs.Views.CustomColumn;
using NetService.Utility.Common;
using NHibernate;
using NetCms.GUI;

namespace GenericJobs.Views
{
    [DynamicUrl.PageControl("/default.aspx")]
    public class JobList : BasePage
    {
        public JobList(StateBag viewstate, bool IsPostBack, NetCms.GUI.Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewstate, IsPostBack, toolbar, informationBox, networkKey)
        {
            this.AddScriptManager = true;
            this.ScriptManager = new ScriptManager();
            this.ScriptManager.ID = "jobScriptManager";            
        }

        private void SetToolbar()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("jobs/addcustomjob.aspx", NetCms.GUI.Icons.Icons.Add, "Nuovo Task di Esempio"));
        }


        public override string PageTitle
        {
            get { return "Archivio task"; }
        }      

        private UpdatePanel _JobsPanel;
        public UpdatePanel JobsPanel
        {
            get
            {
                if (_JobsPanel == null)
                {
                    _JobsPanel = new UpdatePanel();
                    
                    _JobsPanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
                    _JobsPanel.ContentTemplateContainer.Controls.Add(Buttons());
                    _JobsPanel.ContentTemplateContainer.Controls.Add(JobListControl);
                    _JobsPanel.RenderMode = UpdatePanelRenderMode.Block;                   
                }
                return _JobsPanel;
            }
            set { _JobsPanel = value; }
        }

        public override System.Web.UI.WebControls.WebControl BuildPage()
        {
            SetToolbar();

            WebControl webcontrol = new WebControl(HtmlTextWriterTag.Div);
            webcontrol.ID = Application.SystemName;

            UpdatePanel panelForTimer = new UpdatePanel();
            panelForTimer.UpdateMode = UpdatePanelUpdateMode.Conditional;
            panelForTimer.ContentTemplateContainer.Controls.Add(UpdateTimer);
            panelForTimer.RenderMode = UpdatePanelRenderMode.Block;
          
            webcontrol.Controls.Add(panelForTimer);
            webcontrol.Controls.Add(JobsPanel);

            return webcontrol;
        }

        public WebControl Buttons() 
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "btn-group";
            div.Attributes["role"] = "group";            

            // messaggi di notifica
            //div.Controls.Add(Message);

            //Button newtask = new Button();
            //newtask.ID = "newTask";
            //newtask.Text = "Nuovo task di prova";
            //newtask.Click += new EventHandler(newtask_Click);
            //div.Controls.Add(newtask);

            //this.ScriptManager.RegisterAsyncPostBackControl(newtask);

            // bottone elimina tutti i task selezionati
            //Button deleteTasks = new Button();
            //deleteTasks.ID = "deleteTasks";
            //deleteTasks.CssClass = "btn btn-secondary ";
            //deleteTasks.Text = "Elimina";
            //deleteTasks.Click += new EventHandler(deleteTasks_Click);
            //div.Controls.Add(deleteTasks);

            //// bottone pausa 
            //Button pauseTasks = new Button();
            //pauseTasks.ID = "pauseTasks";
            //pauseTasks.Text = "Pausa";
            //pauseTasks.Click += new EventHandler(pauseTasks_Click);
            //div.Controls.Add(pauseTasks);

            //// bottone resume
            //Button resumeTasks = new Button();
            //resumeTasks.ID = "resumeTasks";
            //resumeTasks.Text = "Ripristina";
            //resumeTasks.Click += new EventHandler(resumeTasks_Click);
            //div.Controls.Add(resumeTasks);

            WebControl time = new WebControl(HtmlTextWriterTag.Span);
            time.CssClass = " float-right";
            time.Controls.Add(new LiteralControl(DateTime.Now.ToString()));
            div.Controls.Add(time);           

            return div;
        }       

        void deleteTasks_Click(object sender, EventArgs e)
        {
            // aggiungere il messaggio di conferma

            RequestVariable valore = new NetService.Utility.Common.RequestVariable("chkJob", RequestVariable.RequestType.Form);            
            if (valore.IsValidString)
            {
                 using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                 using (ITransaction tx = session.BeginTransaction())
                 {
                    try
                    {
                        List<string> idJobs = valore.StringValue.Split(',').ToList();
                        BusinessLogic.JobsBL.DeleteAllJob(idJobs, session);
                        tx.Commit();
                        
                        this.JobsPanel.Update();
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                    }
                 }
            }

        }

        void newtask_Click(object sender, EventArgs e)
        {
           // TestJob testJob = new TestJob(false, TimeZone.CurrentTimeZone.ToLocalTime(DateTime.Now.AddMinutes(1)));
           // JobsPanel.Update();
            
        }       

        private Timer _UpdateTimer;
        public Timer UpdateTimer
        {
            get
            {
                if (_UpdateTimer == null)
                {
                    _UpdateTimer = new Timer();
                    _UpdateTimer.Tick += new EventHandler<EventArgs>(timer_Tick);
                    _UpdateTimer.Interval = 20000; // 5 sec.
                }
                return _UpdateTimer;
            }
            set { _UpdateTimer = value; }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            JobsPanel.Update();
        }

        #region Oldcode2
        //private WebControl _Message;
        //private WebControl Message
        //{
        //    get 
        //    {
        //        if (_Message == null)
        //        {
        //            _Message = new WebControl(HtmlTextWriterTag.Span);
        //            _Message.CssClass = "message";
        //        }
        //        return _Message;
        //    }
        //    set 
        //    { 
        //        _Message = value; 
        //    }
        //}

        //private WebControl GetTables()
        //{

        //    WebControl div = new WebControl(HtmlTextWriterTag.Div);

        //    //div.Controls.Add(Pulsantiera());

        //    WebControl time = new WebControl(HtmlTextWriterTag.Span);
        //    time.Controls.Add(new LiteralControl(DateTime.Now.ToString()));
        //    div.Controls.Add(time);

        //    div.Controls.Add(new LiteralControl("<br /><br />"));

        //    div.Controls.Add(GetTableJobs());

        //    div.Controls.Add(new LiteralControl("<br /><br />"));

        //    div.Controls.Add(GetSchedulerQueue());
        //    return div;
        //} 
        #endregion



        private JobListControl _JobListControl;
        private JobListControl JobListControl
        {
            get
            {
                JobListControl _JobListControl = new Views.JobListControl();
                _JobListControl.CssClass = "tablesControl";
                return _JobListControl;
            }
            set { _JobListControl = value; }
        }

        #region MyRegion_oldcode
        //void GetTables_PreRender(object sender, EventArgs e)
        //{
        //    WebControl time = new WebControl(HtmlTextWriterTag.Span);
        //    time.Controls.Add(new LiteralControl(DateTime.Now.ToString()));
        //    GetTables.Controls.Add(time);

        //    GetTables.Controls.Add(new LiteralControl("<br /><br />"));

        //    GetTables.Controls.Add(GetTableJobs());

        //    GetTables.Controls.Add(new LiteralControl("<br /><br />"));

        //    GetTables.Controls.Add(GetSchedulerQueue());           
        //}



        //private ArTable GetTableJobs() 
        //{                                
        //    ArTable jobTable = new ArTable();
        //    jobTable.ID = "jobTableCtrl";
        //    jobTable.EnablePagination = false;
        //    jobTable.InnerTableCssClass = "tab";
        //    jobTable.NoRecordMsg = "Nessun Job in archivio.";
        //    //using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
        //    //{
        //        var jobs = BusinessLogic.JobsBL.GetJobs();

        //        if (jobs != null && jobs.Count() > 0)
        //        {
        //            jobTable.Records = jobs;

        //            GenericJobs.Views.CustomColumn.ArCheckBoxColumnWithValue idJob = new GenericJobs.Views.CustomColumn.ArCheckBoxColumnWithValue("", Model.GenericJob.Column.ID.ToString(), jobTable.ID);
        //            idJob.CheckBoxIDOffset = "chkJob";
        //            jobTable.AddColumn(idJob);

        //            ArJobColumn nomeJob = new ArJobColumn("Task", Model.GenericJob.Column.Nome.ToString());
        //            jobTable.AddColumn(nomeJob);

        //            ArTextColumn gruppoJob = new ArTextColumn("Gruppo", Model.GenericJob.Column.Gruppo.ToString());
        //            jobTable.AddColumn(gruppoJob);

        //            ArStatoColumn statoJob = new ArStatoColumn("Stato", Model.GenericJob.Column.ID.ToString());
        //            jobTable.AddColumn(statoJob);

        //            ArTextColumn dataScheduleJob = new ArTextColumn("Attivazione", Model.GenericJob.Column.ScheduleAt.ToString());
        //            jobTable.AddColumn(dataScheduleJob);

        //            //ArTextColumn dataNextExecutionJob = new ArTextColumn("Prossima esecuzione", Model.GenericJob.Column.ScheduleAt.ToString());
        //            //jobTable.AddColumn(dataNextExecutionJob);         

        //            ArJobBtnColumn btns = new ArJobBtnColumn("Azioni", Model.GenericJob.Column.ID.ToString(), "jobsBtn");
        //            btns.PausaClick += new EventHandler(PauseJob);
        //            btns.EliminaClick += new EventHandler(DeleteJob);

        //            jobTable.AddColumn(btns);
        //        //}
        //    }
        //    return jobTable;
        //}

        //        private WebControl GetSchedulerQueue()
        //        {
        //            WebControl table = new WebControl(HtmlTextWriterTag.Table);

        //            IScheduler scheduler = GenericJobs.Model.JobsScheduler.Scheduler;
        //            table.CssClass = "tab";

        //            IList<string> jobGroups = scheduler.GetJobGroupNames();
        //            IList<string> triggerGroups = scheduler.GetTriggerGroupNames();

        //            WebControl tr = new WebControl(HtmlTextWriterTag.Tr);
        //            string trString = @"
        //                               <th scope=""col"">&nbsp;Group name</th>
        //                               <th scope=""col"">&nbsp;Job name</th>
        //                               <th scope=""col"">&nbsp;Job description</th>
        //                               <th scope=""col"">&nbsp;Trigger name</th>
        //                               <th scope=""col"">&nbsp;Trigger group name</th>
        //                               <th scope=""col"">&nbsp;Trigger type</th>
        //                               <th scope=""col"">&nbsp;Trigger state</th>
        //                               <th scope=""col"">&nbsp;NextFireTime</th>
        //                               <th scope=""col"">&nbsp;PreviousFireTime</th>";

        //            tr.Controls.Add(new LiteralControl(trString));
        //            table.Controls.Add(tr);

        //            if (jobGroups.Count > 0)
        //            {
        //                foreach (string group in jobGroups)
        //                {
        //                    var groupMatcher = GroupMatcher<JobKey>.GroupContains(group);
        //                    var jobKeys = scheduler.GetJobKeys(groupMatcher);
        //                    foreach (var jobKey in jobKeys)
        //                    {
        //                        var detail = scheduler.GetJobDetail(jobKey);
        //                        var triggers = scheduler.GetTriggersOfJob(jobKey);

        //                        foreach (ITrigger trigger in triggers)
        //                        {
        //                            string time1 = "";
        //                            string time2 = "";
        //                            DateTimeOffset? nextFireTime = trigger.GetNextFireTimeUtc();

        //                            if (nextFireTime.HasValue)
        //                            {
        //                                time1 = nextFireTime.Value.DateTime.ToString();
        //                                //time1 = TimeZone.CurrentTimeZone.ToLocalTime(nextFireTime.Value.DateTime).ToString();
        //                            }

        //                            DateTimeOffset? previousFireTime = trigger.GetPreviousFireTimeUtc();

        //                            if (previousFireTime.HasValue)
        //                            {
        //                                time2 = previousFireTime.Value.DateTime.ToString();
        //                                //time2 = TimeZone.CurrentTimeZone.ToLocalTime(previousFireTime.Value.DateTime).ToString();
        //                            }

        //                            WebControl trContent = new WebControl(HtmlTextWriterTag.Tr);
        //                            string trContentStr = @"
        //                                            <td>&nbsp;" + group + @"</td>
        //                                            <td>&nbsp;" + jobKey.Name + @"</td>
        //                                            <td>&nbsp;" + detail.Description + @"</td>
        //                                            <td>&nbsp;" + trigger.Key.Name + @"</td>
        //                                            <td>&nbsp;" + trigger.Key.Group + @"</td>
        //                                            <td>&nbsp;" + trigger.GetType().Name + @"</td>
        //                                            <td>&nbsp;" + scheduler.GetTriggerState(trigger.Key) + @"</td>
        //                                            <td>&nbsp;" + time1 + @"</td>
        //                                            <td>&nbsp;" + time2 + @"</td>";

        //                            trContent.Controls.Add(new LiteralControl(trContentStr));
        //                            table.Controls.Add(trContent);
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                WebControl trContent = new WebControl(HtmlTextWriterTag.Tr);
        //                string trNorecord = @"
        //                                    <td collspan=""9"">Nessun job schedulato</td>
        //                                    ";
        //                trContent.Controls.Add(new LiteralControl(trNorecord));
        //                table.Controls.Add(trContent);
        //            }


        //            return table;
        //        }





        //void DeleteJob(object sender, EventArgs e)
        //{
        //    string idBtn = ((Button)sender).ID;
        //    idBtn = idBtn.Replace("jobsBtn_", "");

        //    int jobId = int.Parse(idBtn.Split('_')[1]);

        //    if (BusinessLogic.JobsBL.DeleteJob(jobId))
        //    {                
        //        Message.CssClass += " Success";
        //        Message.Controls.Add(new LiteralControl("Il Job selezionato è stato eliminato con successo."));                
        //    }
        //    else
        //    {
        //        Message.CssClass += " Error";
        //        Message.Controls.Add(new LiteralControl("Errore non è stato possibile eseguire l'operazione."));
        //    }
        //    JobsPanel.Update();
        //}

        //void PauseJob(object sender, EventArgs e) 
        //{
        //    string idBtn = ((Button)sender).ID.Replace("jobsBtn_","");
        //    string jobId = idBtn.Split('_')[1];

        //    if (BusinessLogic.JobsBL.PauseJob(int.Parse(jobId)))
        //    {
        //        Message.CssClass += " Success";
        //        Message.Controls.Add(new LiteralControl("Il Job selezionato è stato messo in pausa."));                
        //    }
        //    else
        //    {
        //        Message.CssClass += " Error";
        //        Message.Controls.Add(new LiteralControl("Errore non è stato possibile eseguire l'operazione."));
        //    }

        //    JobsPanel.Update();
        //} 
        #endregion

        void AvviaJob(object sender, EventArgs e)
        {
            string idBtn = ((Button)sender).ID.Replace("jobsBtn_", "");
            string jobId = idBtn.Split('_')[1];
            bool esito = BusinessLogic.JobsBL.ResumeJob(int.Parse(jobId));
            this.JobsPanel.Update();
        }

        void StopJob(object sender, EventArgs e)
        {
            string idBtn = ((Button)sender).ID.Replace("jobsBtn_", "");
            string jobId = idBtn.Split('_')[1];

            bool esito = BusinessLogic.JobsBL.StopJob(int.Parse(jobId));
        }

        void RestartJob(object sender, EventArgs e)
        {
            string idBtn = ((Button)sender).ID.Replace("jobsBtn_", "");
            string jobId = idBtn.Split('_')[1];

            // non implementato
        }

        void SchedulatJob(object sender, EventArgs e)
        {
            string idBtn = ((Button)sender).ID.Replace("jobsBtn_", "");
            string jobId = idBtn.Split('_')[1];
        }
    }
}
