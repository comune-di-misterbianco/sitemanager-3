﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using NetService.Utility.ArTable;
using GenericJobs.Views.CustomColumn;
using Quartz;
using Quartz.Impl.Matchers;
using GenericJobs.Model;
using System.Threading.Tasks;

namespace GenericJobs.Views
{
    public class JobListControl : WebControl
    {
        public JobListControl() : base (HtmlTextWriterTag.Div)
        {
        
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            WebControl divJobListControl = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
            divJobListControl.ID = "divJobListControl";

            WebControl time = new WebControl(HtmlTextWriterTag.Span);
            time.Controls.Add(new LiteralControl(DateTime.Now.ToString()));

            divJobListControl.Controls.Add(Message);
            //divJobListControl.Controls.Add(time);
            divJobListControl.Controls.Add(GetTableJobs());
            divJobListControl.Controls.Add(GetSchedulerQueue());

            this.Controls.Add(divJobListControl);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //Control divJobListControl = this.FindControl("divJobListControl");

            //WebControl time = new WebControl(HtmlTextWriterTag.Span);
            //time.Controls.Add(new LiteralControl(DateTime.Now.ToString()));
            
            //divJobListControl.Controls.Add(Message);
            ////divJobListControl.Controls.Add(time);
            //divJobListControl.Controls.Add(GetTableJobs());
            //divJobListControl.Controls.Add(GetSchedulerQueue());
        }      

        private WebControl _Message;
        private WebControl Message
        {
            get
            {
                if (_Message == null)
                {
                    _Message = new WebControl(HtmlTextWriterTag.Span);
                    _Message.CssClass = "message";
                }
                return _Message;
            }
            set
            {
                _Message = value;
            }
        }

        private ArTable GetTableJobs()
        {
            ArTable jobTable = new ArTable();
            jobTable.ID = "jobTableCtrl";
            jobTable.EnablePagination = false;
            jobTable.InnerTableCssClass = "tab";
            jobTable.NoRecordMsg = "Nessun Job in archivio.";
         
            var jobs = BusinessLogic.JobsBL.GetJobs();

            if (jobs != null && jobs.Count() > 0)
            {
                jobTable.Records = jobs;

                GenericJobs.Views.CustomColumn.ArCheckBoxColumnWithValue idJob = new GenericJobs.Views.CustomColumn.ArCheckBoxColumnWithValue("", Model.GenericJob.Column.ID.ToString(), jobTable.ID);
                idJob.CheckBoxIDOffset = "chkJob";
                jobTable.AddColumn(idJob);

                ArJobColumn nomeJob = new ArJobColumn("Task", Model.GenericJob.Column.Nome.ToString());
                jobTable.AddColumn(nomeJob);

                ArTextColumn gruppoJob = new ArTextColumn("Gruppo", Model.GenericJob.Column.Gruppo.ToString());
                jobTable.AddColumn(gruppoJob);

                ArStatoColumn statoJob = new ArStatoColumn(Model.GenericJob.Column.StatoCorrente.ToString(), Model.GenericJob.Column.StatoCorrente.ToString());
                jobTable.AddColumn(statoJob);

                ArStatoColumn statoJobPre = new ArStatoColumn(Model.GenericJob.Column.StatoPrecedente.ToString(), Model.GenericJob.Column.StatoPrecedente.ToString());
                jobTable.AddColumn(statoJobPre);


                

                ArTextColumn dataScheduleJob = new ArTextColumn("Attivazione", Model.GenericJob.Column.ScheduleAt.ToString());
                jobTable.AddColumn(dataScheduleJob);

                //ArTextColumn dataNextExecutionJob = new ArTextColumn("Prossima esecuzione", Model.GenericJob.Column.ScheduleAt.ToString());
                //jobTable.AddColumn(dataNextExecutionJob);         

                ArJobBtnColumn btns = new ArJobBtnColumn("Azioni", Model.GenericJob.Column.ID.ToString(), "jobsBtn");
                btns.PausaClick += new EventHandler(PauseJob);
                btns.EliminaClickHandler += new EventHandler(DeleteJob);
                btns.AvviaClick += new EventHandler(ExecuteJob);
                jobTable.AddColumn(btns);
          
            }
            return jobTable;
        }

        void ExecuteJob(object sender, EventArgs e)
        {
            //string idBtn = ((Button)sender).ID;
            //idBtn = idBtn.Replace("jobsBtn_", "");

            //int jobId = int.Parse(idBtn.Split('_')[1]);

            //GenericJob currentJob = BusinessLogic.JobsBL.GetJob(jobId);
            
            
        }

        void DeleteJob(object sender, EventArgs e)
        {
            string idBtn = ((Button)sender).ID;
            idBtn = idBtn.Replace("jobsBtn_", "");

            int jobId = int.Parse(idBtn.Split('_')[1]);

            if (BusinessLogic.JobsBL.DeleteJob(jobId))
            {
                Message.CssClass += " Success";
                Message.Controls.Add(new LiteralControl("Il Job selezionato è stato eliminato con successo."));
            }
            else
            {
                Message.CssClass += " Error";
                Message.Controls.Add(new LiteralControl("Errore non è stato possibile eseguire l'operazione."));
            }
            //JobsPanel.Update();
        }

        void PauseJob(object sender, EventArgs e)
        {
            string idBtn = ((Button)sender).ID.Replace("jobsBtn_", "");
            string jobId = idBtn.Split('_')[1];

            if (BusinessLogic.JobsBL.PauseJob(int.Parse(jobId)))
            {
                Message.CssClass += " Success";
                Message.Controls.Add(new LiteralControl("Il Job selezionato è stato messo in pausa."));
            }
            else
            {
                Message.CssClass += " Error";
                Message.Controls.Add(new LiteralControl("Errore non è stato possibile eseguire l'operazione."));
            }

            //JobsPanel.Update();
        }

        private WebControl GetSchedulerQueue()
        {
            WebControl table = new WebControl(HtmlTextWriterTag.Table);

           // IScheduler scheduler = GenericJobs.Model.JobsScheduler.;
            table.CssClass = "tab";

            Task<IReadOnlyCollection<string>> jobGroups = GenericJobs.Model.JobsScheduler.GetJobGroupNames();
            Task<IReadOnlyCollection<string>> triggerGroups = GenericJobs.Model.JobsScheduler.GetTriggerGroupNames();

            WebControl tr = new WebControl(HtmlTextWriterTag.Tr);
            string trString = @"
                               <th scope=""col"">&nbsp;Group name</th>
                               <th scope=""col"">&nbsp;Job name</th>
                               <th scope=""col"">&nbsp;Job description</th>
                               <th scope=""col"">&nbsp;Trigger name</th>
                               <th scope=""col"">&nbsp;Trigger group name</th>
                               <th scope=""col"">&nbsp;Trigger type</th>
                               <th scope=""col"">&nbsp;Trigger state</th>
                               <th scope=""col"">&nbsp;NextFireTime</th>
                               <th scope=""col"">&nbsp;PreviousFireTime</th>";

            tr.Controls.Add(new LiteralControl(trString));
            table.Controls.Add(tr);

            if (jobGroups.GetAwaiter().GetResult().Count > 0)
            {
                foreach (string group in jobGroups.GetAwaiter().GetResult())
                {
                   // var groupMatcher = GroupMatcher<JobKey>.GroupContains(group);
                    var jobKeys = GenericJobs.Model.JobsScheduler.GetAllJob(group);
                    foreach (var jobKey in jobKeys.GetAwaiter().GetResult())
                    {
                        var detail = GenericJobs.Model.JobsScheduler.GetJobDetail(jobKey);
                        var triggers = GenericJobs.Model.JobsScheduler.GetTriggersOfJob(jobKey);

                        foreach (ITrigger trigger in triggers.GetAwaiter().GetResult())
                        {
                            string time1 = "";
                            string time2 = "";
                            DateTimeOffset? nextFireTime = trigger.GetNextFireTimeUtc();

                            if (nextFireTime.HasValue)
                            {
                                time1 = nextFireTime.Value.DateTime.ToString();
                                //time1 = TimeZone.CurrentTimeZone.ToLocalTime(nextFireTime.Value.DateTime).ToString();
                            }

                            DateTimeOffset? previousFireTime = trigger.GetPreviousFireTimeUtc();

                            if (previousFireTime.HasValue)
                            {
                                time2 = previousFireTime.Value.DateTime.ToString();
                                //time2 = TimeZone.CurrentTimeZone.ToLocalTime(previousFireTime.Value.DateTime).ToString();
                            }

                            WebControl trContent = new WebControl(HtmlTextWriterTag.Tr);
                            string trContentStr = @"
                                            <td>&nbsp;" + group + @"</td>
                                            <td>&nbsp;" + jobKey.Name + @"</td>
                                            <td>&nbsp;" + detail.GetAwaiter().GetResult().Description + @"</td>
                                            <td>&nbsp;" + trigger.Key.Name + @"</td>
                                            <td>&nbsp;" + trigger.Key.Group + @"</td>
                                            <td>&nbsp;" + trigger.GetType().Name + @"</td>
                                            <td>&nbsp;" + JobsScheduler.GetTriggerState(trigger.Key).GetAwaiter().GetResult() + @"</td>
                                            <td>&nbsp;" + time1 + @"</td>
                                            <td>&nbsp;" + time2 + @"</td>";

                            trContent.Controls.Add(new LiteralControl(trContentStr));
                            table.Controls.Add(trContent);
                        }
                    }
                }
            }
            else
            {
                WebControl trContent = new WebControl(HtmlTextWriterTag.Tr);
                string trNorecord = @"
                                    <td collspan=""9"">Nessun job schedulato</td>
                                    ";
                trContent.Controls.Add(new LiteralControl(trNorecord));
                table.Controls.Add(trContent);
            }


            return table;
        }
    }
}
