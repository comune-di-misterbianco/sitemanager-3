﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace GenericJobs.Example
{
    public class ConsoleJobMapping : SubclassMap<ConsoleJob>
    {
        public ConsoleJobMapping()
        {
            Table("console_scheduler_jobs");
            KeyColumn("IdJob");
        }
    }
}
