﻿using GenericDAO.DAO;
using GenericJobs.Model;
using NHibernate;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericJobs.Example
{
    public class ConsoleJob : GenericJob
    {
        public ConsoleJob()
        {
        }
        public ConsoleJob(string nome, string descrizione, string gruppo, IDictionary<string, string> dataMap)
            : base(nome, descrizione, gruppo, dataMap)
        {
        }
        public override async Task Execute(IJobExecutionContext context)
        {
            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    JobDataMap jMap = context.JobDetail.JobDataMap;
                    
                    string testo = jMap.GetString("text");
                    //await Console.Out.WriteLineAsync(testo);
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, testo);                   
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante l'esecuzione del job " + this.ToString() + " " + ex.Message);                   
                }
            }

        }
        public static bool SaveJob(ConsoleJob job, ISession innerSession)
        {
            bool success = false;

            CriteriaNhibernateDAO<ConsoleJob> dao = new CriteriaNhibernateDAO<ConsoleJob>(innerSession);
            dao.SaveOrUpdate(job);
            success = true;

            return success;
        }
    }
}
