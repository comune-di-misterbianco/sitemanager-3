﻿using GenericDAO.DAO;
using GenericJobs.Model;
using NetCms.DBSessionProvider;
using NetService.Utility.RecordsFinder;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericJobs.BusinessLogic
{
    public static class JobTriggerInfoBL
    {
        public static ISession GetSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static JobTriggerInfo GetJobTriggerInfo(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetSession();

            CriteriaNhibernateDAO<JobTriggerInfo> dao = new CriteriaNhibernateDAO<JobTriggerInfo>(innerSession);
            return dao.GetById(id);
        }

        public static JobTriggerInfo GetJobTriggerInfoByJobID(int jobID, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetSession();

            CriteriaNhibernateDAO<JobTriggerInfo> dao = new CriteriaNhibernateDAO<JobTriggerInfo>(innerSession);
            GenericJob currentJob = BusinessLogic.JobsBL.GetJob(jobID, innerSession);

            SearchParameter spJobId = new SearchParameter("", JobTriggerInfo.Column.Job.ToString(), currentJob, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { spJobId });
        }


        public static bool Save(JobTriggerInfo triggerinfo, ISession innerSession = null)
        {
            bool success = false;

            if (innerSession == null)
                innerSession = GetSession();

            try
            {
                CriteriaNhibernateDAO<JobTriggerInfo> dao = new CriteriaNhibernateDAO<JobTriggerInfo>(innerSession);
                dao.SaveOrUpdate(triggerinfo);         
                success = true;
            }
            catch(Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante il salvataggiodel JobTriggerInfo " + typeof(JobTriggerInfo) + ". " + ex.Message);
            }

            return success;
        }
    }

    
}
