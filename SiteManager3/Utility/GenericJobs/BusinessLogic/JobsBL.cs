﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NetCms.DBSessionProvider;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using GenericJobs.Model;
using NHibernate.Criterion;

namespace GenericJobs.BusinessLogic
{
    public static class JobsBL
    {
        public static ISession GetSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static GenericJob GetJob(int id, ISession innerSession = null) 
        {
            if (innerSession == null)
                innerSession = GetSession();

            CriteriaNhibernateDAO<GenericJob> dao = new CriteriaNhibernateDAO<GenericJob>(innerSession);
            return dao.GetById(id);            
        }

        public static GenericJob GetJob(string name, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetSession();

            CriteriaNhibernateDAO<GenericJob> dao = new CriteriaNhibernateDAO<GenericJob>(innerSession);

            SearchParameter spName = new SearchParameter("", GenericJob.Column.Nome.ToString(), name, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { spName });
        }

        public static IList<GenericJob> GetJobs(ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetSession();

            CriteriaNhibernateDAO<GenericJob> dao = new CriteriaNhibernateDAO<GenericJob>(innerSession);                       

            return dao.FindAll(GenericJob.Column.ID.ToString(), true);
        }

        //public static IList<GenericJob> GetJobsNotExecuted(ISession innerSession = null)
        //{
        //    if (innerSession == null)
        //        innerSession = GetSession();

        //    Disjunction or = new Disjunction();
        //    or.Add(Restrictions.Eq(Model.JobState.Column.Stato.ToString(), Model.JobState.JobStatus.Schedulato));
        //    or.Add(Restrictions.Eq(Model.JobState.Column.Stato.ToString(), Model.JobState.JobStatus.In_Esecuzione));

        //    ICriteria criteria = innerSession.CreateCriteria<JobState>();
        //    criteria.SetProjection(Projections.ProjectionList()
        //        .Add(Projections.GroupProperty(Model.JobState.Column.Job.ToString()))
        //    );

        //    criteria.Add(or);

        //    IList<GenericJob> results = criteria.List<GenericJob>();

        //    return results;
        //}

        public static IList<GenericJob> GetJobsNotExecuted(ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetSession();

            CriteriaNhibernateDAO<GenericJob> dao = new CriteriaNhibernateDAO<GenericJob>(innerSession);

            SearchParameter spStato = new SearchParameter("", Model.GenericJob.Column.StatoCorrente.ToString(), Model.JobState.JobStatus.Schedulato, Finder.ComparisonCriteria.Equals, false);

            IList<GenericJob> results = dao.FindByCriteria(new SearchParameter[] { spStato });

            return results;
        }


        public static bool SaveJob(GenericJob job, ISession innerSession = null)
        {
            bool success = false;

            if (innerSession == null)
                innerSession = GetSession();

            CriteriaNhibernateDAO<GenericJob> dao = new CriteriaNhibernateDAO<GenericJob>(innerSession);
            dao.SaveOrUpdate(job);
            success = true;
           
            return success;
        }

        public static void ChangeStato(int jobID, JobState newstato, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetSession();

            CriteriaNhibernateDAO<GenericJob> dao = new CriteriaNhibernateDAO<GenericJob>(innerSession);

            var job = BusinessLogic.JobsBL.GetJob(jobID, innerSession);

            JobState.JobStatus oldstato = job.StatoCorrente;

            job.StatoPrecedente = oldstato;   // setto lo stato precedente ricopiandolo
            job.StatoCorrente = newstato.Stato;        // aggiorno lo stato corrente

            newstato.Job = job;
            job.Stati.Add(newstato);
            dao.SaveOrUpdate(job);
        }

        public static bool DeleteJob(int id) 
        {
            bool success = false;
        
            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    CriteriaNhibernateDAO<GenericJob> dao = new CriteriaNhibernateDAO<GenericJob>(session);
                    var job = GetJob(id, session);

                    // rimuovo il job dallo scheduler di Quartz
                    JobsScheduler.RemoveJob(job);

                    // rimuovo il job dal db                
                    dao.Delete(job);
                    tx.Commit();
                    success = true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la rimozione del record GenericJob " + typeof(GenericJob) + ". " + ex.Message);
                }
            }
            return success;
        }

        public static bool DeleteAllJob(List<string> idJobs, ISession innerSession = null)
        {
            bool success = false;

            if (innerSession == null)
                innerSession = GetSession();

            try
            {
                
                CriteriaNhibernateDAO<GenericJob> dao = new CriteriaNhibernateDAO<GenericJob>(innerSession);
                foreach (string id in idJobs)
                {
                    var job = GetJob(int.Parse(id), innerSession);

                    // rimuovo il job dallo scheduler di Quartz
                    JobsScheduler.RemoveJob(job);

                    // rimuovo il job dal db
                    dao.Delete(job);
                }
                success = true;
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la rimozione del record GenericJob " + typeof(GenericJob) + ". " + ex.Message);
            }

            return success;
        }

        public static bool PauseJob(int id, ISession innerSession = null)
        {
            bool success = false;

            if (innerSession == null)
                innerSession = GetSession();

            GenericJob job = GetJob(id,innerSession);
            
            JobState jobstate = new JobState();
            jobstate.Data = DateTime.Now;
            jobstate.Stato = JobState.JobStatus.In_Pausa;           

            JobsScheduler.PauseJob(job);
            // salvo lo stato nel db
            ChangeStato(id, jobstate, innerSession);

            return success;
        }

        public static bool ResumeJob(int id, ISession innerSession = null)
        {
            bool success = false;

            if (innerSession == null)
                innerSession = GetSession();

            GenericJob job = GetJob(id, innerSession);

            JobState jobstate = new JobState();
            jobstate.Data = DateTime.Now;
            jobstate.Stato = JobState.JobStatus.In_Esecuzione;
            //jobstate.Job = job;

            JobsScheduler.ResumeJob(job);
           
            // salvo lo stato nel db
            ChangeStato(id, jobstate, innerSession);

            return success;
        }

        public static bool StopJob(int id, ISession innerSession = null)
        {
            bool success = false;

            if (innerSession == null)
                innerSession = GetSession();

            GenericJob job = GetJob(id, innerSession);
            JobsScheduler.StopJob(job);
            
            JobState jobstate = new JobState();
            jobstate.Data = DateTime.Now;
            jobstate.Stato = JobState.JobStatus.Stop;
                                    
            // salvo lo stato nel db
            ChangeStato(id, jobstate, innerSession);

            return success;
        }
    }
}
