﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabelsManager
{
    /// <summary>
    /// Enumerativo che fa da contenitore per le labels contenute su un file xml
    /// </summary>
    public class LabelsCollection : IEnumerable
    {
        private CustomKeyValueCollection coll;
        public int Count { get { return coll.Count; } }
        public LabelsCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new CustomKeyValueCollection();
        }

        public void Add(Labels str, string key)
        {
            coll.Add(key, str);
        }

        public void Remove(string key)
        {
            coll.Remove(key);
        }

        public void Clear()
        {
            coll.Clear();
        }

        public Labels this[int i]
        {
            get
            {
                Labels str = (Labels)coll[coll.Keys[i]];
                return str;
            }
        }

        public Labels this[string str]
        {
            get
            {
                Labels val = (Labels)coll[str];
                return val;
            }
        }

        public bool Contains(string key)
        {
            return coll[key] != null;
        }

        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private LabelsCollection Collection;
            public CollectionEnumerator(LabelsCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}
