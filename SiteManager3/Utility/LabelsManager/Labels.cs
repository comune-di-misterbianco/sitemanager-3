﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using LabelsManager.CustomObjects;
using NetCms.Diagnostics;

namespace LabelsManager
{
    /// <summary>
    /// Classe da estendere per utilizzare delle labels in applicativi verticali o pagine. Le labels sono separate per lingua
    /// </summary>
    public class Labels
    {
        /// <summary>
        /// Linguaggio a cui l'istanza fa riferimento
        /// </summary>
        private string LabelsLanguage;

        /// <summary>
        /// Chiave da utilizzare nella cache per individuare le labels, che rappresenta anche il nome del file di labels
        /// </summary>
        public string CacheKey
        {
            get;
            private set;
        }

        /// <summary>
        /// Cache locale da riempire a runtime, la collezione conterrà le labels man mano che vengono richieste, per l'istanza corrente, che sarà quindi monolingua
        /// </summary>
        private StringCollection _LabelsCache;
        public StringCollection LocalLabelsCache
        {
            get
            {
                if (_LabelsCache == null)
                    _LabelsCache = new StringCollection();

                return _LabelsCache;
            }
        }

        /// <summary>
        /// Inizializzata in fase di start, conterrà il nodo di root del file XML
        /// </summary>
        protected XmlNode RootNode
        {
            get;
            private set;
        }

        /// <summary>
        /// Il costruttore di default viene reso private per non permettere l'istanziazione dell'oggetto
        /// </summary>
        private Labels()
        {

        }

        /// <summary>
        /// Costruttore che verrà utilizzato dalle labels custom nelle varie applicazioni
        /// </summary>
        /// <param name="CacheKey">Chiave principale corrispondente al root node name</param>
        protected Labels(string CacheKey)
        {
            this.CacheKey = CacheKey;
        }

        /// <summary>
        /// Costruttore utilizzato in fase di start per mettere in cache le labels
        /// </summary>
        /// <param name="CacheKey">Chiave principale corrispondente al root node name</param>
        /// <param name="RootNode">Nodo principale</param>
        /// <param name="LanguageISOString">Stringa rappresentante il linguaggio a cui l'istanza si riferisce</param>
        protected internal Labels(string CacheKey, XmlNode RootNode, string LanguageISOString)
        {
            this.CacheKey = CacheKey;
            this.RootNode = RootNode;
            this.LabelsLanguage = LanguageISOString;
        }

        /// <summary>
        /// Formatta la label in input in formato usabile
        /// </summary>
        /// <param name="label"></param>
        /// <returns></returns>
        protected string FormatLabel(string label)
        {
            string outLabel = label.Replace("<![CDATA[", "").Replace("]]>", "");
            return outLabel;
        }

        /// <summary>
        /// Restituisce la label corrispondente, se la label non è di primo livello, ma è annidata, l'enumeratore usato deve utilizzare l'Xpath Attribute del Labels Manager
        /// </summary>
        /// <param name="EntryKey">Chiave corrispondente alla label desiderata</param>
        /// <returns>Stringa</returns>
        public string this[Enum EntryKey]
        {
            get
            {
                string Xpath = string.Empty;

                XPathAttribute XpathAttribute = (XPathAttribute)EntryKey.GetType().GetCustomAttributes(typeof(XPathAttribute), false).FirstOrDefault();

                if (XpathAttribute != null)
                    Xpath += XpathAttribute.PartialXPath;

                Xpath += EntryKey.ToString();

                if (this.LocalLabelsCache.Contains(Xpath))
                {
                    return this.FormatLabel(this.LocalLabelsCache[Xpath]);
                }
                else
                {
                    XmlNode TargetNode = RootNode.SelectSingleNode(Xpath);
                    if (TargetNode == null)
                    {
                        Diagnostics.TraceMessage(TraceLevel.Error, "Errore richiesta labels da parte di '" + CacheKey + "'. Richiesta la label '" + EntryKey + "' sull'Xpath '" + Xpath + "' per la lingua '" + LabelsLanguage + "'.");
                        throw new Exception();
                    }
                    else
                    {
                        string value = TargetNode.InnerXml;
                        LocalLabelsCache.Add(value, Xpath);
                        return this.FormatLabel(value);
                    }

                }
            }
        }

        /// <summary>
        /// Restituisce la label a partire dall'Xpath passato
        /// </summary>
        /// <param name="Xpath"></param>
        /// <returns></returns>
        protected string this[string Xpath]
        {
            get
            {

                if (this.LocalLabelsCache.Contains(Xpath))
                {
                    return this.FormatLabel(this.LocalLabelsCache[Xpath]);
                }
                else
                {
                    XmlNode TargetNode = RootNode.SelectSingleNode(Xpath);
                    if (TargetNode == null)
                    {
                        Diagnostics.TraceMessage(TraceLevel.Error, "Errore richiesta labels da parte di '" + CacheKey + "'. Richiesta la label '" + Xpath + "' per la lingua '" + LabelsLanguage + "'.");
                        throw new Exception("Errore richiesta labels da parte di '" + CacheKey + "'. Richiesta la label '" + Xpath + "' per la lingua '" + LabelsLanguage + "'.");
                    }
                    else
                    {
                        string value = TargetNode.InnerXml;
                        LocalLabelsCache.Add(value, Xpath);
                        return this.FormatLabel(value);
                    }
                }
            }
        }

        /// <summary>
        /// Restituisce un Nodo XML considerandolo di primo livello, introdotto per chi volesse labels custom organizzate in sottosezioni
        /// </summary>
        /// <param name="EntryKey">Chiave corrispondente alla label desiderata</param>
        /// <returns>Nodo XML</returns>
        protected XmlNode GetFirstLevelNode(string EntryKey)
        {
            XmlNode TargetNode = RootNode.SelectSingleNode("/" + EntryKey.ToString());

            if (TargetNode == null)
                throw new Exception("Errore richiesta NODO labels da parte di '" + CacheKey + "'. Richiesta la label '" + EntryKey + "' per la lingua '" + LabelsLanguage + "'.");

            return TargetNode;
        }


    }
}
