﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using NetCms.Configurations;
using NetCms.DBSessionProvider;
using NetCms.Diagnostics;
using NetCms.Networks;
using NHibernate;

namespace LabelsManager
{
    public static class LabelsCache
    {
        //public const string LabelsCacheKey = "LabelsCacheObj";

        //private static IDictionary<int, LabelsCollection> _Labels;
        //public static IDictionary<int, LabelsCollection> Labels
        //{
        //    get
        //    {
        //        if (_Labels == null)
        //        {
        //            _Labels = new Dictionary<int, LabelsCollection>();

        //            //Network di base
        //            _Labels.Add(0, new LabelsCollection());

        //            foreach (KeyValuePair<int, string> networkKey in NetworksManager.GlobalIndexer.NetworkKeys)
        //            {
        //                LabelsCollection LabelsForNetwork = new LabelsCollection();
        //                _Labels.Add(networkKey.Key, LabelsForNetwork);
        //            }

        //        }

        //        return _Labels;
        //    }
        //}

        /// <summary>
        /// Cache globale delle labels riempita allo start dell'applicativo, la collezione è chiave valore;
        /// CHIAVE: stringa ISO corrispondente alla lingua
        /// VALORE: collezione delle labels corrispondente
        /// </summary>
        private static IDictionary<string, LabelsCollection> _LabelsGlobalCache;
        public static IDictionary<string, LabelsCollection> GlobalCache
        {
            get
            {
                if (_LabelsGlobalCache == null)
                {
                    _LabelsGlobalCache = new Dictionary<string, LabelsCollection>();
                }

                return _LabelsGlobalCache;
            }
        }


        //private static string absoluteConfigsRoot;

        //public static string AbsoluteConfigsRoot
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(absoluteConfigsRoot))
        //        {
        //            absoluteConfigsRoot = HttpContext.Current.Application["LabelsPath"].ToString();
        //        }
        //        return absoluteConfigsRoot;
        //    }
        //}
        /// <summary>
        /// Path delle labels impostato da web config
        /// </summary>
        public static string LabelsPathRoot
        {
            get
            {
                if (string.IsNullOrEmpty(_LabelsPathRoot))
                {
                    _LabelsPathRoot = Paths.LabelsPath;

                    if (string.IsNullOrEmpty(_LabelsPathRoot))
                        throw new Exception("Errore web config relativamente al path delle labels.");
                }
                return _LabelsPathRoot;
            }
        }
        private static string _LabelsPathRoot;



        //public static object GetTypedLabels(Type type)
        //{
        //    var key = type.FullName + "_LabelsCache";

        //    int CurrentNetworkID = NetworksManager.CurrentActiveNetwork != null ? NetworksManager.CurrentActiveNetwork.ID : 0;
        //    if (!Labels[CurrentNetworkID].Contains(key))
        //    {
        //        try
        //        {
        //            var constructor = type.GetConstructor(new[] { typeof(string) });

        //            string LabelsPath = CurrentNetworkID == 0 ? AbsoluteConfigsRoot : AbsoluteConfigsRoot + "\\" + NetworksManager.CurrentActiveNetwork.SystemName;
        //            var labels = constructor.Invoke(new[] { LabelsPath }) as Labels;
        //            Labels[CurrentNetworkID].Add(labels, key);
        //        }
        //        catch(Exception ex)
        //        {
        //            Diagnostics.TraceMessage(TraceLevel.Error, ex.Message + ": " + ex.StackTrace);
        //        }
        //    }
        //    return Labels[CurrentNetworkID][key];
        //}

        /// <summary>
        /// Restituisce le labels relativamente alla lingua di default, a meno che non venga passato il culture code
        /// </summary>
        /// <param name="type">Type della classe che implementa l'oggetto astratto Labels del LabelsManager</param>
        /// <param name="LanguageCultureCode">Culture code della lingua nel formato xx-XX</param>
        /// <returns>Object di tipo Labels</returns>
        public static object GetTypedLabels(Type type, string LanguageCultureCode = "")
        {
            var key = "";

            try
            {
                key = type.GetField("ApplicationKey").GetRawConstantValue().ToString();
            }
            catch (Exception ex)
            {
                Diagnostics.TraceMessage(TraceLevel.Error, "Richiesta delle labels di tipo '" + type.Name.ToString() + "' Chiave di cache non trovata.");
            }

            //Parametro non passato
            if(string.IsNullOrEmpty(LanguageCultureCode))
                LanguageCultureCode = LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.CultureCode_Lang;

            //Parametro passato ma non presente nella collezione
            if(!GlobalCache.Keys.Contains(LanguageCultureCode))
            {
                Diagnostics.TraceMessage(TraceLevel.Error, "Richiesta delle labels con chiave '" + key + "' per il linguaggio: '" + LanguageCultureCode + "'. Labels non trovate");
                LanguageCultureCode = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetDefaultLanguage().CultureCode_Lang;
            }

            //Parametro passato, presente nella collezione, ma di cui manca la entry nella cache
            if (!GlobalCache[LanguageCultureCode].Contains(key))
            {
                Diagnostics.TraceMessage(TraceLevel.Error, "Richiesta delle labels con chiave '" + key + "' per il linguaggio: '" + LanguageCultureCode + "'. Labels non trovate");
                throw new Exception("Richiesta delle labels con chiave '" + key + "' per il linguaggio: '" + LanguageCultureCode + "'. Labels non trovate");
            }

            return GlobalCache[LanguageCultureCode][key];
        }

        ///// <summary>
        ///// Restituisce le labels relativamente alla lingua di default.
        ///// La classe che implementa l'oggetto astratto Labels deve avere il costruttore di default.
        ///// </summary>
        ///// <param name="LanguageCultureCode">Stringa rappresentante </param>
        ///// <param name="type">Type della classe che implementa l'oggetto astratto Labels del LabelsManager</param>
        ///// <returns></returns>
        //public static object GetTypedLabels(Type type, string LanguageCultureCode)
        //{
        //    var key = "";

        //    try
        //    {
        //        key = type.GetField("ApplicationKey").GetRawConstantValue().ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        Diagnostics.TraceMessage(TraceLevel.Error, "Richiesta delle labels di tipo '" + type.Name.ToString() + "' Chiave di cache non trovata.");
        //    }


        //    if (string.IsNullOrEmpty(LanguageCultureCode))
        //    {
        //        Diagnostics.TraceMessage(TraceLevel.Error, "Richiesta delle labels con chiave '" + key + "'. Non è stato specificato il linguaggio nella chiamata.");
        //        throw new Exception();
        //    }

        //    if (!GlobalCache[LanguageCultureCode].Contains(key))
        //    {
        //        try
        //        {
        //            Diagnostics.TraceMessage(TraceLevel.Error, "Richiesta delle labels con chiave '" + key + "' per il linguaggio: '" + LanguageCultureCode + "'. Labels non trovate");
        //            throw new Exception();
        //        }
        //        catch (Exception ex)
        //        {
        //            Diagnostics.TraceMessage(TraceLevel.Error, ex.Message + ": " + ex.StackTrace);
        //        }
        //    }
        //    return GlobalCache[LanguageCultureCode][key];
        //}

        /// <summary>
        /// Metodo che effettua il caricamento massivo delle Labels all'avvio dell'applicativo popolando la cache per lingua.
        /// Le labels devono essere disposte su disco secondo lo standard I18N, quindi sottocartelle con il codice della lingua ed 
        /// all'interno i vari XML contenenti le labels.
        /// </summary>
        public static void LoadLabels()
        {
            using (ISession session = FluentSessionProvider.Instance.OpenInnerSession())
            {
                try
                {
                    ICollection<LanguageManager.Model.Language> ActiveLanguages = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages(session);

                    LanguageManager.Model.Language DefaultLang = ActiveLanguages.Where(x => x.Default_Lang == 1 && x.Enabled).FirstOrDefault();

                    if (DefaultLang == null)
                        throw new Exception("Non è stato abilitato o settato un linguaggio di default per il CMS.");

                    IEnumerable<string> subDirs = Directory.GetDirectories(LabelsPathRoot, "*", SearchOption.TopDirectoryOnly);

                    foreach (LanguageManager.Model.Language lang in ActiveLanguages)
                    {
                        if (!subDirs.Contains(LabelsPathRoot + "\\" + lang.CultureCode_Lang))
                            throw new Exception("Non sono stata rilevata la folder contenente le labels corrispondenti al linguaggio attivato (" + lang.CultureCode_Lang + "). Si consiglia di aggiornare le labels o disabilitare il linguaggio.");
                        else
                        {
                            LabelsCollection LangLabelsCollection = new LabelsCollection();
                            XmlDocument XmlLabels = new XmlDocument();

                            foreach(string XmlLabelsFileName in Directory.GetFiles(LabelsPathRoot + "\\" + lang.CultureCode_Lang, "*.xml", SearchOption.TopDirectoryOnly))
                            {
                                XmlLabels.Load(XmlLabelsFileName);
                                XmlNode FirstNode = XmlLabels.ChildNodes[1];
                                
                                Labels Labels = new Labels(FirstNode.Name, FirstNode, lang.CultureCode_Lang);

                                LangLabelsCollection.Add(Labels, FirstNode.Name);
                            }

                            GlobalCache.Add(lang.CultureCode_Lang, LangLabelsCollection);
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    Diagnostics.TraceMessage(TraceLevel.Fatal, "Errore nel caricamento delle labels: " + ex.Message + ". Dettagli: " + ex.StackTrace);
                    throw new Exception("Labels loading error.");
                }
                finally
                {
                    session.Close();
                }
            }

        }
    }
}
