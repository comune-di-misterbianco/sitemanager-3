﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LabelsManager
{
    /// <summary>
    /// Attributo introdotto per fornire al meccanismo di caching informazioni relativamente al ramo, in caso di labels innestate.
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Enum)]
    public class XPathAttribute : System.Attribute
    {
        public string PartialXPath
        {
            get;
            set;
        }

        public XPathAttribute(string XpathString)
            : base()
        {
            this.PartialXPath = XpathString;
        }
    }
}
