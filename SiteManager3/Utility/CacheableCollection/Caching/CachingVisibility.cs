﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Data;

namespace G2Core.Caching.Visibility
{
    public enum CachingVisibility 
    {
        User, Global, Page
    }
    
    /// <summary>
    /// La classe CachingVisibilityUtility fornisce metodi per generare le chiavi di visibilità da utilizzare per conservare gli oggetti in Cache
    /// </summary>
    public static class CachingVisibilityUtility
    {
        public static string BuildVisibilityKey(CachingVisibility visibility)
        {
            string visibilityKey = "";
            
            if (visibility == CachingVisibility.Global)
                visibilityKey = "(" + visibility.ToString() + ")";

            if (visibility == CachingVisibility.User)
                visibilityKey = "(" + visibility.ToString() + "_" + SessionIdentifier + ")";

            if (visibility == CachingVisibility.Page)
                visibilityKey = "(" + visibility.ToString() + "_" + PageIdentifier + ")";
            
            return visibilityKey;
        }

        private const string SessionIdentifierKey = "ASP.NET_SessionId";
        private static string SessionIdentifier
        {
            get
            {
                // al logout genera exception a causa del reset del context
                if (HttpContext.Current != null)                                    
                    return HttpContext.Current.Request[SessionIdentifierKey];                
                else
                    return "";
            }
        }

        private const string PageIdentifierKey = "CachingVisibilityPageID";
        private static string PageIdentifier
        {
            get
            {
                if (HttpContext.Current.Items[PageIdentifierKey] == null)
                    HttpContext.Current.Items[PageIdentifierKey] = new Random().Next(10000000, 99999999).ToString();
                
                return (string)HttpContext.Current.Items[PageIdentifierKey];
            }
        }
    }

}
