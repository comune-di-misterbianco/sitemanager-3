﻿using System;
using System.Text.RegularExpressions;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.Caching;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace G2Core.Caching
{
    public class Cache 
    {
        private static CacheBackstore _CacheStorage;
        private static CacheBackstore CacheStorage
        {
            get
            {
                if (_CacheStorage == null)
                    _CacheStorage = new CacheBackstore();

                return _CacheStorage;
            }
        }
        public DataTable AllKeys
        {
            get
            {
                return CacheStorage.AllKeys;
            }
        }

        /// <summary>
        /// La chiave di indicizzazione della tabella delle chiavi nella Application Pool
        /// </summary>
        private const string CacheStorageKey = "G2Core.Caching.Cache.AllKeysDataTable";

        /// <summary>
        /// Costruttore della classe Cache
        /// </summary>
        public Cache()
        {
        }

        /// <summary>
        /// Aggiunge un oggetto alla Cache
        /// </summary>
        /// <param name="key">Stringa rappresentante la chiave di indicizzazione nella cache</param>
        /// <param name="obj">L'oggetto da immagazzinare nelal cache</param>
        /// <param name="cachingTime">Un Intero che rappresenta il tempo di vita dell'oggetto in cache (dall'ultimo accesso)</param>
        public void Insert(string key, object obj)
        {
            Insert(key, obj, new TimeSpan(365, 0, 0, 0), null);
        }

        /// <summary>
        /// Aggiunge un oggetto alla Cache
        /// </summary>
        /// <param name="key">Stringa rappresentante la chiave di indicizzazione nella cache</param>
        /// <param name="obj">L'oggetto da immagazzinare nelal cache</param>
        /// <param name="cachingTime">Un Intero che rappresenta il tempo di vita dell'oggetto in cache (dall'ultimo accesso)</param>
        public void Insert(string key, object obj, int cachingTime)
        {
            Insert(key, obj, new TimeSpan(0, cachingTime, 0), null);
        }

        /// <summary>
        /// Aggiunge un oggetto alla Cache
        /// </summary>
        /// <param name="key">Stringa rappresentante la chiave di indicizzazione nella cache</param>
        /// <param name="obj">L'oggetto da immagazzinare nelal cache</param>
        /// <param name="cachingTime">Un Intero che rappresenta il tempo di vita dell'oggetto in cache (dall'ultimo accesso)</param>
        /// <param name="callback">Metodo delegato che gestisce la logica l'evento di eliminazione dell'oggetto dalla cache</param>
        public void Insert(string key, object obj, int cachingTime, CacheItemRemovedCallback callback)
        {
            Insert(key, obj, new TimeSpan(0, cachingTime, 0), callback);
        }

        /// <summary>
        /// Aggiunge un oggetto alla Cache
        /// </summary>
        /// <param name="key">Stringa rappresentante la chiave di indicizzazione nella cache</param>
        /// <param name="obj">L'oggetto da immagazzinare nelal cache</param>
        /// <param name="cachingTime">Un Intero che rappresenta il tempo di vita dell'oggetto in cache (dall'ultimo accesso)</param>
        /// <param name="callback">Metodo delegato che gestisce la logica l'evento di eliminazione dell'oggetto dalla cache</param>
        public void Insert(string key, object obj, TimeSpan cachingTime, CacheItemRemovedCallback callback)
        {
            lock (CacheStorage)
            {
                AddKey(key);
                if (callback != null && !CacheStorage.Callbacks.ContainsKey(key))
                    CacheStorage.Callbacks.Add(key, callback);
                CacheItemRemovedCallback keycallback = new CacheItemRemovedCallback(RemovedCallback);
                CacheStorage.DotNetCache.Insert(key, obj, null, System.Web.Caching.Cache.NoAbsoluteExpiration, cachingTime, CacheItemPriority.Default, keycallback);
            }
        }


        /// <summary>
        /// Metodo delegato, legato all'evento di rimozione del'elemento indicizzato dalla Cache.
        /// </summary>
        public void RemovedCallback(String k, Object v, CacheItemRemovedReason r)
        {
            lock (CacheStorage)
            {
                this.RemoveKey(k);
                if (CacheStorage.Callbacks != null && CacheStorage.Callbacks.ContainsKey(k))
                {
                    CacheStorage.Callbacks[k].Invoke(k, v, r);
                    CacheStorage.Callbacks.Remove(k);
                }
            }
        }


        /// <summary>
        /// Questo metodo restituisce true se l'oggetto associato alla chiave passata per paramentro esiste nella cache, altrimenti restituisce false.
        /// </summary>
        /// <param name="key">Stringa rappresentante la chiave di indicizzazione nella cache</param>
        /// <returns>True se l'oggetto associato alla chiave passata per paramentro esiste nella cache, altrimenti restituisce false</returns>
        public bool Contains(string key)
        {
            return this.Contains(key, true);
        }
        private bool Contains(string key, bool add2dotnetcache)
        {
            bool contains = false;
            lock (CacheStorage)
            {
                DataRow[] rows = CacheStorage.AllKeys.Select("key = '" + key + "'");
                if (rows.Length > 0)
                {
                    if (CacheStorage.DotNetCache[key] != null) contains = true;
                    else this.RemoveKey(key);
                }
                else
                {
                    if (add2dotnetcache && CacheStorage.DotNetCache[key] != null)
                    {
                        this.AddKey(key);
                        return true;
                    }
                }
            }
            return contains;
        }
        /// <summary>
        /// Questo metodo permette di rimuovere un oggetto presente in cache.
        /// </summary>
        /// <param name="key">id della riga nella tabella AllKeys contenente la chiave di indicizzazione nella cache</param>
        public void Remove(int id)
        {
            lock (CacheStorage)
            {
                DataRow[] rows = CacheStorage.AllKeys.Select("id = " + id);
                if (rows.Length == 1)
                {
                    Remove(rows[0]["key"].ToString());
                }
            }
        }

        /// <summary>
        /// Questo metodo permette di rimuovere un oggetto presente in cache.
        /// </summary>
        /// <param name="key">Stringa rappresentante la chiave di indicizzazione nella cache</param>
        public void Remove(string key)
        {
            lock (CacheStorage)
            {
                CacheStorage.DotNetCache.Remove(key);
                RemoveKey(key);
            }
        }

        /// <summary>
        /// Questo metodo permette di rimuovere un oggetto presente in cache.
        /// </summary>
        /// <param name="key">Stringa rappresentante la chiave di indicizzazione nella cache</param>
        public void RemoveCurrentUserCachedObjects()
        {
            string key = G2Core.Caching.Visibility.CachingVisibilityUtility.BuildVisibilityKey(G2Core.Caching.Visibility.CachingVisibility.User);
            this.RemoveAll("key LIKE '" + key + "%'");
        }
        /// <summary>
        /// Questo metodo permette di rimuovere una chiave dalla tabella delle chiavi "AllKeys".
        /// </summary>
        /// <param name="key">Stringa rappresentante la chiave di indicizzazione nella cache</param>
        public void RemoveKey(string key)
        {
            lock (CacheStorage)
            {
                DataRow[] rows = CacheStorage.AllKeys.Select("key = '" + key + "'");

                for (int i = 0; i < rows.Length; i++)
                    CacheStorage.AllKeys.Rows.Remove(rows[i]);
            }
        }

        /// <summary>
        /// Questo metodo permette di rimuovere una molteplici oggetti dalla cache, selezionandoli attraverso una stringa sql.
        /// La stringa verra girata come parametro alla tabella delle  chiavi (AllKeys).
        /// </summary>
        /// <param name="sqlSelect">Una stringa rappresentante una query Sql su DataTable (metodo DataTable.Select)</param>
        public void RemoveAll(string sqlSelect)
        {
            DataRow[] rows = CacheStorage.AllKeys.Select(sqlSelect);

            for (int i = 0; i < rows.Length; i++)
            {
                if (rows[i]!= null)
                   this.Remove(rows[i]["key"].ToString());
            }
        }

        /// <summary>
        /// Quest metodo permette di aggiungere una chiave alla tabella delle chiavi "AllKeys".
        /// </summary>
        /// <param name="key">Stringa rappresentante la chiave di indicizzazione nella cache</param>
        private void AddKey(string key)
        {
            lock (CacheStorage)
            {
                if (!this.Contains(key, false))
                {
                    DataRow row = CacheStorage.AllKeys.NewRow();
                    row["id"] = CacheStorage.AllKeys.Rows.Count;
                    row["key"] = key;
                    row["scope"] = key.Substring(1, key.IndexOf(')') - 1);
                    row["object"] = key;// Colorize(key.Substring(key.IndexOf(')') + 1, key.Length - key.IndexOf(')') - 1));
                    row["date"] = DateTime.Now.Day + "/" + DateTime.Now.Month;
                    row["time"] = DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Millisecond;

                    CacheStorage.AllKeys.Rows.Add(row);
                }
            }

        }

        /// <summary>
        /// Restituisce l'oggetto in cache orrispondente alla chiave passata per parametro.
        /// </summary>
        /// <param name="key">Stringa rappresentante la chiave di indicizzazione nella cache</param>
        /// <returns>L'oggetto in cache orrispondente alla chiave passata per parametro, null se la chiave non è presente nella cache</returns>
        public object this[string key]
        {
            get
            {
                lock (CacheStorage)
                {
                    if (this.Contains(key))
                        return CacheStorage.DotNetCache[key];

                    else return null;
                }
            }
        }
        /*
        public System.Web.UI.HtmlControls.HtmlGenericControl EmptyCacheView()
        {
            System.Web.UI.HtmlControls.HtmlGenericControl ctr = new System.Web.UI.HtmlControls.HtmlGenericControl("div");

            //ctr.Controls.Add(this.CacheDebugTable());

            //this.RemoveAll("");

            return ctr;
        }

        public string Colorize(string str)
        {
            string colorfullStr = str;

            string[] colors = { "red", "blue", "green", "fucsia" };

            string[] splits = {"[","{","("};

            string[] splitted = str.Split(splits, StringSplitOptions.RemoveEmptyEntries);

            int i = 0;
            foreach (string element in splitted)
            {
                if (element.Length > 1)
                {
                    string color = "style=\"color: " + colors[i % 5] + "\"";
                    colorfullStr = colorfullStr.Replace(element.Substring(0, element.Length - 1), "<strong " + color + ">" + element.Substring(0, element.Length - 1) + "</strong>");
                    i++;
                }
            }

            return colorfullStr;
        }*/



        private class CacheBackstore
        {
            /// <summary>
            /// La chiave di indicizzazione della tabella delle chiavi nella Application Pool
            /// </summary>
            private const string ApplicationKey = "G2Core.Caching.Cache.AllKeysDataTable";
            private const string CallbacksKey = "G2Core.Caching.Cache.CallbacksKey";

            /// <summary>
            /// Restituisci una DataTable che immagazzina le chiavi di indicizzazione della Cache
            /// </summary>
            public Dictionary<string, CacheItemRemovedCallback> Callbacks
            {
                get
                {
                    return _Callbacks;
                }
            }
            private Dictionary<string, CacheItemRemovedCallback> _Callbacks;

            /// <summary>
            /// Restituisci una DataTable che immagazzina le chiavi di indicizzazione della Cache
            /// </summary>
            public DataTable AllKeys
            {
                get
                {
                    return _AllKeys;
                }
            }
            private DataTable _AllKeys;

            /// <summary>
            /// Restiruisce il referimento ad HttpContext.Current.Cache
            /// </summary>
            public System.Web.Caching.Cache DotNetCache
            {
                get { return _DotNetCache; }
            }
            private System.Web.Caching.Cache _DotNetCache;
            /// <summary>
            /// Inizializza la DataTable che immagazzina le chiavi di indicizzazione della Cache
            /// </summary>
            private void InitKeysTable()
            {
                //lock (AllKeys)
                //lock (Callbacks)
                //lock (DotNetCache)
                {
                    _AllKeys = new DataTable();

                    DataColumn col;

                    col = new DataColumn("id", Type.GetType("System.Int32"));
                    _AllKeys.Columns.Add(col);

                    col = new DataColumn("scope", Type.GetType("System.String"));
                    _AllKeys.Columns.Add(col);

                    col = new DataColumn("key", Type.GetType("System.String"));
                    _AllKeys.Columns.Add(col);

                    col = new DataColumn("object", Type.GetType("System.String"));
                    _AllKeys.Columns.Add(col);

                    col = new DataColumn("Date", Type.GetType("System.String"));
                    _AllKeys.Columns.Add(col);

                    col = new DataColumn("Time", Type.GetType("System.String"));
                    _AllKeys.Columns.Add(col);

                    HttpContext.Current.Application[ApplicationKey] = _AllKeys;
                }
            }

            public CacheBackstore()
            {
                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Application[CallbacksKey] == null)
                        HttpContext.Current.Application[CallbacksKey] = _Callbacks = new Dictionary<string, CacheItemRemovedCallback>();
                    else
                        _Callbacks = (Dictionary<string, CacheItemRemovedCallback>)HttpContext.Current.Application[CallbacksKey];

                    if (HttpContext.Current.Application[ApplicationKey] == null)
                        InitKeysTable();
                    else
                        _AllKeys = (DataTable)HttpContext.Current.Application[ApplicationKey];

                    this._DotNetCache = HttpContext.Current.Cache;
                }
                else _Callbacks = new Dictionary<string, CacheItemRemovedCallback>();
            }
        }
    }
}
