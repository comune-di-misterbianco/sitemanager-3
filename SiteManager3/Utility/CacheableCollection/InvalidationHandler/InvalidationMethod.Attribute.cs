
namespace G2Core.Caching.Invalidation
{
    [System.AttributeUsage(System.AttributeTargets.Method)]
    public class InvalidationMethod : System.Attribute
    {
        public InvalidationMethod(string invalidationKey)//, StoreTypes storetype)
        {
            InvalidationKey = invalidationKey;
            //StoreTypes = storetype;
        }

        public string InvalidationKey { get; private set; }
        //public StoreTypes StoreTypes { get; private set; }
    }
}