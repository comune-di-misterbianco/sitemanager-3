using System;
using System.Web;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace G2Core.Caching.Invalidation
{
    public class MethodsCollection : Dictionary<string, MethodInfo> { }
    public class StoresCollection : Dictionary<string, StoreTypes> { }

    public static class InvalidationHandler
    {
        //private const string StoresKeyAssociationsKey = "G2Core.Caching.Invalidation.InvalidationHandler.StoresKeyAssociations";
        private const string MethodsKeyAssociationsKey = "G2Core.Caching.Invalidation.InvalidationHandler.MethodsKeyAssociations";

        //public static StoresCollection StoresKeysAssociations
        //{
        //    get
        //    {
        //        if (_StoresKeysAssociations == null)
        //        {
        //            if (HttpContext.Current.Application[StoresKeyAssociationsKey] != null)
        //                _StoresKeysAssociations = (StoresCollection)HttpContext.Current.Application[StoresKeyAssociationsKey];
        //            else
        //            {
        //                HttpContext.Current.Application[StoresKeyAssociationsKey] = _StoresKeysAssociations = new StoresCollection();
        //            }
        //        }

        //        return _StoresKeysAssociations;
        //    }
        //}
        //private static StoresCollection _StoresKeysAssociations;

        public static MethodsCollection MethodsKeysAssociations
        {
            get
            {
                if (_MethodsKeysAssociations == null)
                {
                    if (HttpContext.Current.Application[MethodsKeyAssociationsKey] != null)
                        _MethodsKeysAssociations = (MethodsCollection)HttpContext.Current.Application[MethodsKeyAssociationsKey];
                    else
                    {
                        HttpContext.Current.Application[MethodsKeyAssociationsKey] = _MethodsKeysAssociations = new MethodsCollection();
                    }
                }

                return _MethodsKeysAssociations;
            }
        }
        private static MethodsCollection _MethodsKeysAssociations;

        public static void Invalidate(string key, params object[] parameters)
        {
            if (MethodsKeysAssociations.ContainsKey(key))
                //if (!StoresKeysAssociations.ContainsKey(key) && !StoresKeysAssociations.ContainsKey(key))
            {
                //var store = StoresKeysAssociations[key];
                var method = MethodsKeysAssociations[key];
                method.Invoke(null, parameters);
            }
        }

        public static void AnalyzeType(Type type)
        {
            var metodi = type.GetMethods();
                //.GetCustomAttributes()typeof(InvalidationMethod), true) != null);
                         //.ToDictionary(x => x.GetCustomAttributes(typeof(InvalidationMethod), true).FirstOrDefault() as InvalidationMethod);
            foreach (var element in metodi)
            {
                var attribute = element.GetCustomAttributes(typeof(InvalidationMethod), false).FirstOrDefault() as InvalidationMethod;
                if (attribute != null && !MethodsKeysAssociations.ContainsKey(attribute.InvalidationKey))
                {
                    //aggiungo allo store associato il metodo selezionato
                    //StoresKeysAssociations.Add(element.Key.InvalidationKey, element.Key.StoreTypes);
                    MethodsKeysAssociations.Add(attribute.InvalidationKey, element);
                }
            }
        }
    }
}