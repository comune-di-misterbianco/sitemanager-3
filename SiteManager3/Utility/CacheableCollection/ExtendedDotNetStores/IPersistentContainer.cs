﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using System.Text;

namespace G2Core.Caching
{
    public interface IPersistentContainer
    {
        bool Exists { get; }
        
        bool Contains(string key);
        void Add(string key, object value);
        object Get(string key);

        void Remove(string key);
    }
}
