﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using System.Text;
using System.Collections;

namespace G2Core.Caching
{
    public class ExtendedPageItems : IPersistentContainer
    {
        private IDictionary Store { get { return HttpContext.Current.Items; } }
        public bool Exists { get { return HttpContext.Current != null && HttpContext.Current.Items != null; } }

        public ExtendedPageItems()
        {
        }

        public object this[string key]
        {
            get
            {
                return Exists ? Store[key] : null;
            }
        }

        public bool Contains(string key)
        {
            return Exists && Store[key] != null;
        }

        public void Add(string key, object value)
        {
            if(Exists && !Store.Contains(key)) 
                Store.Add(key, value);
        }

        public void Remove(string key)
        {
            if (Exists) Store.Remove(key);
        }

        public object Get(string key)
        {
            return Exists ? Store[key] : null;
        }
    }
}
