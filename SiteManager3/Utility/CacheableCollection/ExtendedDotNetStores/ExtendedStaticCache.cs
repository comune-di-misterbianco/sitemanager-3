﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using System.Text;
using G2Core.Caching.Visibility;

namespace G2Core.Caching
{
    public class ExtendedStaticCache : IPersistentContainer
    {
        public G2Core.Caching.StaticCache Store { get; private set; }

        public bool Exists { get { return true; } }

        public ExtendedStaticCache()
        {
            if (Exists) 
                Store = StaticCache.Current;
        }

        public object this[string key]
        {
            get
            {
                return Exists ? Store[FormatKey(key)] : null;
            }
        }

        public bool Contains(string key)
        {
            return Exists && Store[FormatKey(key)] != null;
        }

        public void Add(string key, object value)
        {
            if (Exists) Store.Insert(FormatKey(key), value);
        }

        public void Add(string key, object value, TimeSpan expirationTime)
        {
            if (Exists) Store.Insert(FormatKey(key), value, expirationTime);
        }

        public void Remove(string key)
        {
            if (Exists) Store.Remove(FormatKey(key));
        }

        public object Get(string key)
        {
            return Exists ? Store[FormatKey(key)] : null;
        }

        public string FormatKey(string key)
        {
            return CachingVisibilityUtility.BuildVisibilityKey(CachingVisibility.Global) + "ExtendedStaticCache_" + key;
        }
    }
}
