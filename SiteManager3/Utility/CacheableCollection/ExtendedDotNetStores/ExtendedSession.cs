﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using System.Text;

namespace G2Core.Caching
{
    public class ExtendedSession : IPersistentContainer
    {
        private HttpSessionState Store { get { return HttpContext.Current.Session; } }
        public bool Exists { get { return HttpContext.Current != null && HttpContext.Current.Session != null; } }
        
        public ExtendedSession()
        {
        }

        public object this[string key]
        {
            get
            {
                return Exists ? Store[key] : null;
            }
        }

        public bool Contains(string key)
        {
            return Exists && Store[key] != null;
        }

        public void Add(string key, object value)
        {
            if(Exists) Store[key] = value;
        }

        public void Remove(string key)
        {
            if (Exists) Store.Remove(key);
        }
        
        public object Get(string key)
        {
            return Exists ? Store[key] : null;
        }
    }
}
