﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Text;

namespace G2Core.Caching
{
    public class PersistentCacheString
    {
        protected IPersistentContainer Store{ get; private set; } 

        public bool Exists
        { 
            get { return (Store.Exists) && Store.Contains(Key); } 
        }
        public string Key 
        { get; private set; }
        protected string DefaultValue { get; private set; } 

        public string Instance
        {
            get
            {
                if (_Instance == null && Store.Exists)
                {
                    if (Store.Contains(Key))
                        _Instance = (string)Store.Get(Key);
                    else
                    {
                        _Instance = InitializingMethod();
                        Store.Add(Key, _Instance);
                    }
                }
                return _Instance;
            }
        }
        private string _Instance;

        public StoreTypes StoreType { get; private set; }

        public PersistentCacheString(string key, StoreTypes storeType, string defaultValue = "")
        {
            StoreType = storeType;
            Key = key;
            DefaultValue = defaultValue;

            switch(StoreType)
            {
                case StoreTypes.Application: Store = new ExtendedApplication(); break;
                case StoreTypes.Session: Store = new ExtendedSession();break;
                case StoreTypes.Cache: Store = new ExtendedDotNetCache(); break;
                case StoreTypes.Page: Store = new ExtendedPageItems(); break;
            }
        }

        public Func<string> InitializingMethod
        {
            get
            {
                if (_InitializingMethod == null)
                {
                    _InitializingMethod = () => { return DefaultValue; };
                }
                return _InitializingMethod;
            }
            set
            {
                _InitializingMethod = value;
            }
        }
        private Func<string> _InitializingMethod;
    }
}
