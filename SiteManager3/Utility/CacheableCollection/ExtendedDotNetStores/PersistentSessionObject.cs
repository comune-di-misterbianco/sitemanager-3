﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Text;

namespace G2Core.Caching
{
    public enum StoreTypes { Session, Application, Page, Cache }
    public class PersistentCacheObject<T>:IDisposable
    {
        public const string InitializationStoreValue = "initialized";

        protected IPersistentContainer Store { get; private set; }

        public bool AutoInitObjectInstance { get; set; } 

        public bool Exists
        { 
            get { return (Store.Exists) && Store.Contains(Key); } 
        }
        public string Key 
        { get; private set; }
        protected object[] Parameters { get; private set; }
        
        protected string InitializationStoreKey
        {
            get { return BuildInitializationKey(Key); }
        }
        public bool Initialized
        {
            get 
            { 
                return Store.Exists && 
                       Store.Contains(InitializationStoreKey) &&
                       Store.Get(InitializationStoreKey).ToString() == InitializationStoreValue; 
            }
        }

        public T Instance
        {
            get
            {
                if (Store.Exists)
                {
                    if (!Initialized && AutoInitObjectInstance && !Store.Contains(Key))
                    {
                        Store.Add(InitializationStoreKey, InitializationStoreValue);
                        Store.Add(Key, ObjectBuilder());
                    }
                    if (Store.Contains(Key))
                        return (T)Store.Get(Key);
                    else
                        return default(T);
                }
                return default(T);
            }
        }

        public StoreTypes StoreType { get; private set; }

        public PersistentCacheObject(string key, StoreTypes storeType, params object[] parameters)
        {
            StoreType = storeType;
            Key = key;
            Parameters = parameters;
            AutoInitObjectInstance = true;
            switch(StoreType)
            {
                case StoreTypes.Application: Store = new ExtendedApplication(); break;
                case StoreTypes.Session: Store = new ExtendedSession();break;
                case StoreTypes.Cache: Store = new ExtendedDotNetCache(); break;
                case StoreTypes.Page: Store = new ExtendedPageItems(); break;
            }
        }

        public Func<T> ObjectBuilder
        {
            get
            {
                if (_ObjectBuilder == null)
                {
                    _ObjectBuilder = () => 
                    {
                        var type = typeof(T);
                        Type[] parametersTypes = Parameters.Select(x => x.GetType()).ToArray();
                        var costruttore = type.GetConstructor(parametersTypes);
                        T myObject = (T)System.Activator.CreateInstance(type, Parameters);
                        return myObject != null ? myObject : default(T);
                    };
                }
                return _ObjectBuilder;
            }
            set
            {
                _ObjectBuilder = value;
            }
        }
        private Func<T> _ObjectBuilder;

        public void Dispose()
        {
            Store = null;
            Key = null;
            ObjectBuilder = null;
        }

        public static void Invalidate(string key, StoreTypes storeType)
        {
            IPersistentContainer store = null;
            switch (storeType)
            {
                case StoreTypes.Application: store = new ExtendedApplication(); break;
                case StoreTypes.Session: store = new ExtendedSession(); break;
                case StoreTypes.Cache: store = new ExtendedDotNetCache(); break;
                case StoreTypes.Page: store = new ExtendedPageItems(); break;
            }
            store.Remove(key);
            store.Remove(BuildInitializationKey(key));
        }

        public static string BuildInitializationKey(string key)
        {
            return key + "_Initialized";
        }


        public void SetObjectInstance(T objectInstance)
        {
            Store.Add(Key, objectInstance);
        }
    }
}
