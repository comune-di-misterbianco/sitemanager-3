﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Data;
//using System.Web.Caching;
//using System.Web;
//using G2Core.Caching.Visibility;
//using System.Web.Security;

//namespace G2Core.Caching
//{
//    public abstract partial class CacheableCollection
//    {
//        /// <summary>
//        /// La classe CacheableDataTableManager, permette di effettuare il caching di una DataTable.
//        /// </summary>
//        public class CacheableDataTableManager
//        {
//            /// <summary>
//            /// Rappresenta la visibilità degli oggetti in cache. 
//            /// E' possibile impostare una visibilità, Locale alla sessione utente o Globale all'intera appicazione.
//            /// </summary>
//            public CacheableCollection CacheableCollection
//            {
//                get
//                {
//                    return _CacheableCollection;
//                }
//                private set { _CacheableCollection = value; }
//            }
//            private CacheableCollection _CacheableCollection;

//            /// <summary>
//            /// Questa proprietà contiene un riferimento alla DataTable che verrà utilizzata per popolare la collezione.
//            /// </summary>
//            public DataTable DataTable
//            {
//                get
//                {
//                    DataTable dt;
//                    if (this.CacheableCollection.Cache[this.CacheKey] == null)
//                    {
//                        dt = this.CacheableCollection.FillData(this.CommandData.CommandText, this.CommandData.CommandType, this.CommandData.Parameters);
//                        AddToCacheDataTable(dt);
//                    }
//                    else dt = (DataTable)this.CacheableCollection.Cache[this.CacheKey];

//                    return dt;
//                }
//            }


//            /// <summary>
//            /// L'istanza di un oggetto CacheableCommandData, utilizzato dalla proprietà DataTable per popolare se stessa.
//            /// </summary>
//            public CacheableCommandData CommandData
//            {
//                get { return _CommandData; }
//                private set { _CommandData = value; }
//            }
//            private CacheableCommandData _CommandData;

//            /// <summary>
//            /// Costruttore della classe CacheableDataTableManager
//            /// </summary>
//            /// <param name="commandData">Istanza dell'oggetto CacheableCommandData da utilizzare per popolare la collezione</param>
//            /// <param name="cacheableCollection">Il riferimento all'istanza della CacheableCollection da popolare</param>
//            public CacheableDataTableManager(CacheableCommandData commandData, CacheableCollection cacheableCollection)
//            {
//                CacheableCollection = cacheableCollection;
//                CommandData = commandData;
//            }

//            /// <summary>
//            /// Rappresenta la chiave da utilizzare per indicizzare la DataTable nella Cache.
//            /// </summary>
//            public string CacheKey
//            {
//                get
//                {
//                    if (_CacheKey == null)
//                    {
//                        _CacheKey = CachingVisibilityUtility.BuildVisibilityKey(this.CacheableCollection.SourceTableVisibility)
//                                 + this.CacheableCollection.BuildCommandAssociatedKey(this.CommandData.CommandText.Replace("'",""), this.CommandData.CommandType, this.CommandData.Parameters);
//                    }
//                    return _CacheKey;
//                }
//            }
//            private string _CacheKey;


//            /// <summary>
//            /// Rappresenta l'hash MD5 della proprietà CacheKey.
//            /// </summary>
//            public string CacheKeyMD5
//            {
//                get
//                {
//                    if(_CacheKeyMD5 == null)
//                        _CacheKeyMD5 = FormsAuthentication.HashPasswordForStoringInConfigFile(CacheKey, "MD5").ToString();
//                    return _CacheKeyMD5;
//                }
//            }
//            private string _CacheKeyMD5;

//            /// <summary>
//            /// Questo metodo elimina la DataTable dalla Cache di ASP.NET
//            /// </summary>
//            public void Invalidate()
//            {
//                this.CacheableCollection.Cache.Remove(this.CacheKey);
//            }

//            /// <summary>
//            /// Questo metodo aggiunge alla Cache la DataTable indicizzata da questa classe
//            /// </summary>
//            /// <param name="dt">Un oggetto DataTable da aggiungere alla Cache</param>
//            private void AddToCacheDataTable(DataTable dt)
//            {
//                this.CacheableCollection.Cache.Insert(this.CacheKey, dt, this.CacheableCollection.TableCachingTime, new CacheItemRemovedCallback(RemovedCallback));
//            }

//            /// <summary>
//            /// Metodo delegato, legato all'evento di rimozione della DataTable dalla Cache.
//            /// </summary>
//            public void RemovedCallback(String k, Object v, CacheItemRemovedReason r)
//            {
//                this.CacheableCollection.Cache.RemoveKey(this.CacheKey);                
//            }

//        }
//    }

//}
