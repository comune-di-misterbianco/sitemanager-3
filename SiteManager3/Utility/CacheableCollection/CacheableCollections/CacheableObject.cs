﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Web;
//using System.Data;

//namespace G2Core.Caching
//{
//    /// <summary>
//    /// La classe CacheableObject estende l'interfaccia ICacheableObject, aggiungendo delle funzionalità base,
//    /// utili al corretto funzionamento degli oggetti da conservare in Cache attraverso un CacheableItem.
//    /// </summary>
//    public abstract class CacheableObject: ICacheableObject,IDisposable
//    {
//        /// <summary>
//        /// Questa proprietà deve rappresentare la chiave di accesso al relativo oggetto negli ogetti di tipo CacheableCollection che estende DictionaryEntry
//        /// </summary>
//        private string _Key ;
//        public string Key 
//        { 
//            get { return _Key; } 
//            set { _Key = value; } 
//        }

//        //private string _ID;
//        //public string ID
//        //{
//        //    get { return _ID; }
//        //    protected set { _ID = value; }
//        //}

//        public virtual string ID
//        {
//            get;
//            set;
//        }

//        /// <summary>
//        /// Rappresenta l'istanza della CacheableCollection che ha generato questo CacheableObject
//        /// </summary>
//        public CacheableCollection OwnerCollection
//        {
//            get { return _OwnerCollection; }
//            set
//            {
//                if (_OwnerCollection == null)
//                    _OwnerCollection = value;
//                else
//                {
//                    Exception ex = new Exception("La proprietà OwnerCollection è già stata inizializzata: impossibile assegnare due volte il suo valore");
//                    throw ex;
//                }
//            }
//        }
//        private CacheableCollection _OwnerCollection;

//        /// <summary>
//        /// Rappresenta l'hash MD5 della DataRow che ha generato questo oggetto.
//        /// </summary>
//        public string SourceRecordHash
//        {
//            get { return _SourceRecordHash; }
//            protected set { _SourceRecordHash = value; }
//        }
//        private string _SourceRecordHash;

//        /// <summary>
//        /// Questa proprietà serve per eseguire una generica funzione di inizializzazione
//        /// </summary>
//        /// NON DEVE ESISTERE PIù
//        //public abstract ICacheableObject BuildItem(DataRow data);

//        /// <summary>
//        /// Questo metodo rimuove questo oggetto dalla Cache di ASP.NET
//        /// </summary>
//        public virtual void Invalidate()
//        {
//            var item = this.OwnerCollection.GetCacheableItem(this.ID);
//            if(item!=null) item.Invalidate();
//        }

//        public void Dispose()
//        {
//            this._OwnerCollection = null;
//            _SourceRecordHash = null;
//        }
//    }
//}
