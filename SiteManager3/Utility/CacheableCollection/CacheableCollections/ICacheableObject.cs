﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Web;
//using System.Data;

//namespace G2Core.Caching
//{
//    public interface ICacheableObject
//    {
//        string ID { get; }

//        /// <summary>
//        /// Questa proprietà deve rappresentare la chiave di accesso al relativo oggetto negli ogetti di tipo CacheableCollection che estende DictionaryEntry
//        /// </summary>
//        string Key { get; set; }

//        /// <summary>
//        /// Rappresenta l'istanza della CacheableCollection che ha generato questo CacheableObject
//        /// </summary>
//        CacheableCollection OwnerCollection { get; set; }

//        /// <summary>
//        /// Rappresenta l'hash MD5 della DataRow che ha generato questo oggetto.
//        /// </summary>
//        string SourceRecordHash { get; }

//        /// <summary>
//        /// Questa proprietà serve per eseguire una generica funzione di inizializzazione
//        /// </summary>
//        /// // NON DEVE ESISTERE PIù
//        //ICacheableObject BuildItem(DataRow data);

//        /// <summary>
//        /// Questo metodo rimuove questo oggetto dalla Cache di ASP.NET
//        /// </summary>
//        void Invalidate();
//        void Dispose();
//    }
//}
