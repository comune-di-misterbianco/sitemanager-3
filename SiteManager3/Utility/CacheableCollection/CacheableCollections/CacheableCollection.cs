﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Data;
//using System.Web.Caching;
//using System.Web;
//using G2Core.Caching.Visibility;
//using System.Web.Security;

//namespace G2Core.Caching
//{
//    /// <summary>
//    /// La classe CacheableCollection, gestisce una Collezione di tipo Dizionario, aggiungendo funzionalità avanzate di Caching.
//    /// </summary>
//    public abstract partial class CacheableCollection
//    {
//        /// <summary>
//        /// Riferimento ad un istanza dell' oggetto G2Core.Caching.Cache, che fa da proxy alla Cache di ASP.NET e ne estede le funzionalita.
//        /// </summary>
//        public G2Core.Caching.Cache Cache
//        {
//            get
//            {
//                if (_Cache == null)
//                    _Cache = new G2Core.Caching.Cache();
//                return _Cache;
//            }
//        }
//        private G2Core.Caching.Cache _Cache;
        
//        /// <summary>
//        /// Questo oggetto contiene gli elementi inizializzati dall'istanza di questa classe e fornisce i metodi di:
//        /// aggiunta, rimozione ed accesso degli elementi.
//        /// </summary>
//        private CacheableItemCollection Collection
//        {
//            get
//            {
//                return _Collection;
//            }
//            set
//            {
//                _Collection = value;
//            }
//        }
//        private CacheableItemCollection _Collection;

//        /// <summary>
//        /// Questa proprietà serve nel caso di utilizzo gerarchico delle CacheableCollection, e contiene il riferimento al CacheableItem che ha generato questa collezione.
//        /// Nel caso di collezioni NON gerarchiche va mantenuto NULL.
//        /// </summary>
//        public CacheableItem OwnerItem
//        {
//            get { return _OwnerItem; }
//            set 
//            {
//                if (_OwnerItem == null)
//                    _OwnerItem = value;
//                else
//                {
//                    Exception ex = new Exception("La proprietà OwnerItem è già stata inizializzata: impossibile assegnare due volte un valore");
//                }
//            }
//        }
//        private CacheableItem _OwnerItem;
        
//        /// <summary>
//        /// Questa proprietà deve contenere una stringa contenente il System.Type relativo agli oggetti contenuti nella CacheableCollection. Deve essere sovrascritta per contratto.
//        /// </summary>
//        public abstract string ElementsType { get; }

//        /// <summary>
//        /// Questa proprietà deve contenere una stringa che rappresenti il nome del campo chiave primaria nella tabella 
//        /// dal quale verranno prelevati i record col quale vengono riempiti gli elementi della collezione.
//        /// Deve essere sovrascritta per contratto.
//        /// </summary>
//        public abstract string PrimaryKey { get; }

//        /// <summary>
//        /// Rappresenta la visibilità degli oggetti in cache. 
//        /// E' possibile impostare una visibilità, Locale alla sessione utente o Globale all'intera appicazione.
//        /// </summary>
//        public CachingVisibility Visibility
//        {
//            get { return _Visibility; }
//            private set { _Visibility = value; }
//        }
//        private CachingVisibility _Visibility;

//        /// <summary>
//        /// Rappresenta la visibilità della tabella sorgente degli oggetti in cache. 
//        /// E' possibile impostare una visibilità, Locale alla sessione utente o Globale all'intera appicazione.
//        /// </summary>
//        public CachingVisibility SourceTableVisibility
//        {
//            get { return _SourceTableVisibility; }
//            protected set { _SourceTableVisibility = value; }
//        }
//        private CachingVisibility _SourceTableVisibility;

//        /// <summary>
//        /// Se impostata a true: le istanze degli elementi della collezione saranno indicizzate in modo da
//        /// essere condivise con altre CacheableCollection, che istanziano lo stesso tipo di elementi.
//        /// Se impostata a false: le istanze degli elementi della collezione saranno indicizzate in modo da venire condivise
//        /// solo con altre CacheableCollection di oggetti che vengono costruiti con la medesima Query SQL.
//        /// </summary>
//        public bool ShareItemsWithAllQuerys
//        {
//            get
//            {
//                return _ShareItemsWithAllQuerys;
//            }
//            private set { _ShareItemsWithAllQuerys = value;}
//        }
//        private bool _ShareItemsWithAllQuerys;

//        /// <summary>
//        /// Rappresenta la durata del ciclo vita di ogni singolo elemento della collezione nella cache.
//        /// Va riempito con un intero che definisce quanti minuti (dall'ultimo accesso) l'oggetto rimarrà in
//        /// cache prima di essere rimosso.
//        /// </summary>
//        public virtual int ItemsCachingTime
//        {
//            get { return _ItemsCachingTime; }
//            set { _ItemsCachingTime = value; }
//        }
//        private int _ItemsCachingTime;

//        /// <summary>
//        /// Rappresenta la durata del ciclo vita della tabella sorgente.
//        /// Va riempito con un intero che definisce quanti minuti (dall'ultimo accesso) l'oggetto rimarrà in
//        /// cache prima di essere rimosso.
//        /// </summary>
//        public virtual int TableCachingTime
//        {
//            get { return _TableCachingTime; }
//            set { _TableCachingTime = value; }
//        }
//        private int _TableCachingTime;

//        public CacheableCommandData LastCommand
//        {
//            get { return _LastCommand; }
//            private set { _LastCommand = value; }
//        }
//        private CacheableCommandData _LastCommand;

//        /// <summary>
//        /// Numero degli elementi attualmente presenti nella collezione
//        /// </summary>
//        public int Count
//        {
//            get { return Collection.Count; }
//        }

//        /// <summary>
//        /// La Classe CacheableCollection fornisce un sistema integrato completo di accesso ai dati di un database e di caching degli oggetti derivati dai record richiesti alla CacheableCollection.
//        /// </summary>
//        public CacheableCollection(CachingVisibility Visibility,bool ShareItemsWithAllQuerys)
//        {
//            this.Visibility = Visibility;
//            this.Collection = new CacheableItemCollection();
//            this.ShareItemsWithAllQuerys = ShareItemsWithAllQuerys;
//            this.SourceTableVisibility = Visibility;
//        }
        
//        /// <summary>
//        /// La Classe CacheableCollection fornisce un sistema integrato completo di accesso ai dati di un database e di caching degli oggetti derivati dai record richiesti alla CacheableCollection.
//        /// </summary>
//        public CacheableCollection(CachingVisibility Visibility, bool ShareItemsWithAllQuerys, CacheableItem ownerItem)
//        {
//            this.OwnerItem = ownerItem;
//            this.Visibility = Visibility;
//            this.Collection = new CacheableItemCollection();
//            this.ShareItemsWithAllQuerys = ShareItemsWithAllQuerys;
//            this.SourceTableVisibility = Visibility;
//        }

//        /// <summary>
//        /// Restituisce uno degli elementi della collezione.
//        /// E' necessario fornire un indice sotto forma di Int che rappresenta la posizione
//        /// all'interno della collezione.
//        /// </summary>
//        /// <param name="i">Intero che rappresenta l'indice della collezione al quale si vuole accedere</param>
//        /// <returns>L'elemento associato all'indice dato.</returns>
//        protected Object this[int i]
//        {
//            get
//            {
//                return this.Collection[i].CachedObject;
//            }
//        }

//        /// <summary>
//        /// Restituisce uno degli elementi della collezione.
//        /// E' necessario fornire una chiave sotto forma di String corrispondente alla 
//        /// stringa associata all'elemento a cui si vuole accedere.
//        /// </summary>
//        /// <param name="Key">Un oggetto di tipo String corrispondente alla stringa associata all'elemento a cui si vuole accedere.</param>
//        /// <returns>L'elemento associato alla stringa data. Se non esiste nessun elemento corrispondente alla stringa data verrà restituito NULL</returns>
//        public CacheableItem GetCacheableItem(string Key)
//        {
//            return this.Collection[Key];
//        }

//        /// <summary>
//        /// Restituisce uno degli elementi della collezione.
//        /// E' necessario fornire un indice sotto forma di Int che rappresenta la posizione
//        /// all'interno della collezione.
//        /// </summary>
//        /// <param name="i">Intero che rappresenta l'indice della collezione al quale si vuole accedere</param>
//        /// <returns>L'elemento associato all'indice dato.</returns>
//        public CacheableItem GetCacheableItem(int i)
//        {
//                return this.Collection[i];
//        }

//        /// <summary>
//        /// Restituisce uno degli elementi della collezione.
//        /// E' necessario fornire una chiave sotto forma di String corrispondente alla 
//        /// stringa associata all'elemento a cui si vuole accedere.
//        /// </summary>
//        /// <param name="Key">Un oggetto di tipo String corrispondente alla stringa associata all'elemento a cui si vuole accedere.</param>
//        /// <returns>L'elemento associato alla stringa data. Se non esiste nessun elemento corrispondente alla stringa data verrà restituito NULL</returns>
//        protected Object this[string Key]
//        {
//            get
//            {
//                return this.Collection[Key] != null ? this.Collection[Key].CachedObject : null;
//            }
//        }

//        /// <summary>
//        /// Aggiunge un nuovo elemento alla collezione
//        /// </summary>
//        /// <param name="obj">CacheableItem</param>
//        public virtual void Add(CacheableItem obj)
//        {
//            Collection.Add(obj);
//        }

//        /// <summary>
//        /// Rimuove un elemento dalla collezione, se la stringa passata per parametro non è associata ad alcun elemento la funzione non fa nulla.
//        /// </summary>
//        /// <param name="obj">La stringa associata all'elemento che si vuole rimuovere</param>
//        public virtual void Remove(string Key)
//        {
//            Collection.Remove(Key);
//        }

//        /// <summary>
//        /// Rimuove un elemento dalla collezione, se l'oggetto passato per parametro non fa parte della collezione la funzione non fa nulla.
//        /// </summary>
//        /// <param name="obj">Un riferimento ad un CacheableItem che si vuole rimuovere</param>
//        public virtual void Remove(CacheableItem obj)
//        {
//            Collection.Remove(obj.ID);
//        }

//        /// <summary>
//        /// Scorre la collezione fino a trovare l'elemento con la chiave associata e ne restituisce l'indice a base 0.
//        /// Se l'elemento nn viene trovato la funzione restituisce -1
//        /// </summary>
//        /// <param name="obj">La stringa associata all'elemento del quale si vuole l'indice</param>
//        /// <returns>Indice dell'elemento associato alla stringa data. Se l'elemento nn viene trovato la funzione restituisce -1</returns>
//        public int IndexOf(string key)
//        {
//            return this.Collection.IndexOf(key);
//        }

//        /// <summary>
//        /// Controlla che se la collezione contiene un elemento con la stringa passata per paramentro.
//        /// </summary>
//        /// <param name="obj">La stringa associata del quale si vuole verificare la presenza nella collezione</param>       
//        public bool Contains(string Key)
//        {
//            return Collection[Key] != null;
//        }

//        /// <summary>
//        /// Controlla che se la collezione contiene già l'elemento passato per paramentro.
//        /// </summary>
//        /// <param name="obj">La stringa associata del quale si vuole verificare la presenza nella collezione</param>   
//        public bool Contains(CacheableItem obj)
//        {
//            return Collection[obj.Key] != null;
//        }

//        /// <summary>
//        /// Questo metodo elimina tutti gli elementi indicizzati dalla CacheableCollection nella Cache di ASP.NET
//        /// </summary>
//        public void Invalidate()
//        {
//            this.DataTableManager.Invalidate();
            
//            foreach (CacheableItem item in this.Collection)
//                item.Invalidate();

//            UpdateCollection(LastCommand);
//        }

//        /// <summary>
//        /// Questa proprietà gestisce l'accesso alla DataTable dalla quale vengono istanziati gli elementi della collezione.
//        /// </summary>
//        public CacheableDataTableManager DataTableManager
//        {
//            get { return _DataTableManager; }
//            private set { _DataTableManager = value; }
//        }
//        private CacheableDataTableManager _DataTableManager;

//        /// <summary>
//        /// Questo metodo permette di popolare la collezione attraverso una query SQL o una Stored Procedure 
//        /// </summary>
//        /// <param name="commandText">Stringa contenente nome della  Stored Procedure o la query SQL da eseguire</param>
//        /// <param name="commandType">Tipo di command da eseguire.</param>
//        /// <param name="parameters">Lista degli eventuali paramentri della Stored Procedure</param>
//        /// <returns>Restituisce "true" il la collezione è stata popolata con successo, altrimenti false</returns>
//        protected bool FillCollection(string commandText, CommandType commandType, List<System.Data.Common.DbParameter> parameters)
//        {
//            CacheableCommandData command = new CacheableCommandData();
//            command.CommandText = commandText;
//            command.CommandType = commandType;
//            command.Parameters = parameters;
//            return FillCollection(command);
//        }

//        /// <summary>
//        /// Questo metodo permette di popolare la collezione attraverso un istanza di un oggetto CacheableCommandData 
//        /// </summary>
//        /// <param name="commandData">Riferimento ad un istanza di un oggetto CacheableCommandData</param>
//        /// <returns>Restituisce "true" il la collezione è stata popolata con successo, altrimenti false</returns>
//        protected bool FillCollection(CacheableCommandData commandData)
//        {
//            this.LastCommand = commandData;

//            DataTableManager = new CacheableDataTableManager(commandData, this);
//            int index = 0;
//            foreach (DataRow RecordData in DataTableManager.DataTable.Rows)
//            {
//                string id = RecordData[this.PrimaryKey].ToString();
//                if (!this.Contains(id))
//                    this.Add(BuildElement(RecordData, index++, this));
//            }

//            return true;
//        }

//        /// <summary>
//        /// Questo metodo permette di aggiornare una collezione popolata popolare la collezione attraverso un istanza di un oggetto CacheableCommandData 
//        /// </summary>
//        /// <param name="commandData">Riferimento ad un istanza di un oggetto CacheableCommandData</param>
//        /// <returns>Restituisce "true" il la collezione è stata popolata con successo, altrimenti false</returns>
//        protected bool UpdateCollection(CacheableCommandData commandData)
//        {
//            DataTableManager = new CacheableDataTableManager(commandData, this);
//            int index = 0;
//            foreach (DataRow RecordData in DataTableManager.DataTable.Rows)
//            {
//                string id = RecordData[this.PrimaryKey].ToString();
//                if (!this.Contains(id))
//                    this.Add(BuildElement(RecordData, index++, this));
//            }
//            //Se nella collezione ci sono più elementi di quanti presenti nella nuova query, rimuovo gli elementi di troppo
//            if (DataTableManager.DataTable.Rows.Count < this.Count)
//            {
//                int ElementsToRemoveCount = this.Count - DataTableManager.DataTable.Rows.Count;
//                int i=0;
//                while (i < this.Count && ElementsToRemoveCount > 0)
//                {
//                    //CacheableObject cobj = (CacheableObject)this[i];
//                    CacheableItem item = this.GetCacheableItem(i);
//                    if (this.DataTableManager.DataTable.Select(this.PrimaryKey + " = " + item.ID).Length == 0)
//                    {
//                        this.Remove(item);
//                        ElementsToRemoveCount--;
//                    }
//                    else i++;
//                }
//            }

//            return true;
//        }

//        /// <summary>
//        /// Questo metodo deve essere ridefinito in modo da definire la logica di costruzione dei singoli elementi della Collection
//        /// </summary>
//        /// <param name="index">Il record relativo all'oggetto da istanziare</param>
//        /// <returns>L'oggetto opportunamente costruito</returns>
//        protected abstract CacheableItem BuildElement(DataRow recordData, int index, CacheableCollection ownerCollection);
//        public abstract string BuildCommandAssociatedKey(string commandText, CommandType commandType, List<System.Data.Common.DbParameter> parameters);
//        public abstract DataTable FillData(string commandText, CommandType commandType, List<System.Data.Common.DbParameter> parameters);
//    }
//}
