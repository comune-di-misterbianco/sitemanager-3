//using System;
//using System.Collections;

///// <summary>
///// Summary description for NetFieldsCollection
///// </summary>
//namespace G2Core.Caching
//{

//    public abstract partial class CacheableCollection
//    {
//        private class CacheableItemCollection : IEnumerable
//        {
//            private G2CacheCollection coll;
//            public int Count { get { return coll.Count; } }
//            public CacheableItemCollection()
//            {
//                //
//                // TODO: Add constructor logic here
//                //
//                coll = new G2CacheCollection();
//            }

//            public void Add(string str, string key)
//            {
//                coll.Add(key, str);
//            }
//            public void Add(CacheableItem item)
//            {
//                coll.Add(item.ID, item);
//            }

//            public void Remove(CacheableItem item)
//            {
//                coll.Remove(item.ID);
//            }
//            public void Remove(string Key)
//            {
//                coll.Remove(Key);
//            }

//            public void Clear()
//            {
//                coll.Clear();
//            }

//            public CacheableItem this[int i]
//            {
//                get
//                {
//                    CacheableItem str = (CacheableItem)coll[coll.Keys[i]];
//                    return str;
//                }
//            }

//            public int IndexOf(string key)
//            {
//                for (int i = 0; i < this.Count; i++)
//                {
//                    if (this.coll.Keys[i] == key)
//                        return i;
//                }
//                return -1;
//            }

//            public CacheableItem this[string key]
//            {
//                get
//                {
//                    CacheableItem val = (CacheableItem)coll[key];
//                    return val;
//                }
//            }

//            public bool Contains(string key)
//            {
//                return coll[key] != null;
//            }

//            public bool Contains(CacheableItem item)
//            {
//                return coll[item.ID] != null;
//            }
//            #region Enumerator

//            public IEnumerator GetEnumerator()
//            {
//                return new CollectionEnumerator(this);
//            }

//            private class CollectionEnumerator : IEnumerator
//            {
//                private int CurentPos = -1;
//                private CacheableItemCollection Collection;
//                public CollectionEnumerator(CacheableItemCollection coll)
//                {
//                    Collection = coll;
//                }
//                public object Current
//                {
//                    get
//                    {
//                        return Collection[CurentPos];
//                    }
//                }
//                public bool MoveNext()
//                {
//                    if (CurentPos < Collection.Count - 1)
//                    {
//                        CurentPos++;
//                        return true;
//                    }
//                    else
//                        return false;
//                }
//                public void Reset()
//                {
//                    CurentPos = -1;
//                }
//            }
//            #endregion
//        }
//    }
//}