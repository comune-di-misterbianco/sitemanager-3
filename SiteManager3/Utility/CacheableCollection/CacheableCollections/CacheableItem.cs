﻿//using System;
//using System.Collections;
//using System.Web.Caching;
//using System.Collections.Generic;
//using System.Web;
//using System.Data;

//namespace G2Core.Caching
//{
//    /// <summary>
//    /// La classe CacheableItem permette di generare elementi indicizzabili dalla classe CacheableCollection.
//    /// Gestisce inoltre Caching, Costruzione e Validazione di un singolo oggetto indicizzato da una CacheableCollection.
//    /// </summary>
//    public abstract class CacheableItem
//    {
//        /// <summary>
//        /// Rappresenta il valore del campo impostato come chiave primaria nel record associato al CacheableItem.
//        /// </summary>
//        public string ID
//        {
//            get
//            {
//                return _ID;
//            }
//            private set { _ID = value; }
//        }
//        private string _ID;

//        /// <summary>
//        /// Rappresenta la chiave col quale il CacheableItem è indicizzato all'interno della Cache.
//        /// </summary>
//        public string Key 
//        { 
//            get
//            {
//                return _Key;
//            }
//            protected set { _Key = value; }
//        }
//        private string _Key ;

//        /// <summary>
//        /// Rappresenta l'indice della DataRow immagazzinata nella proprietà CurrentRecordData, nella DataTable sorgente.
//        /// </summary>
//        public int LastIndex
//        {
//            get
//            {
//                return _LastIndex;
//            }
//            private set { _LastIndex = value; }
//        }
//        private int _LastIndex;

//        /// <summary>
//        /// Restituisce l'istanza dell'oggetto conservata in cache.
//        /// </summary> 
//        public ICacheableObject CachedObject
//        {
//            get
//            {
               
//                    if (this.OwnerCollection.Cache.Contains(this.Key))
//                    {
//                        ICacheableObject tempItem = (ICacheableObject)this.OwnerCollection.Cache[this.Key];
//                        string drh = this.CurrentRecordData.GetHashCode().ToString();
//                        if (tempItem.SourceRecordHash != drh)
//                        {
//                            this.OwnerCollection.Cache.Remove(this.Key);

//                            ICacheableObject ico = BuildItemProcedure();
//                            ico.OwnerCollection = this.OwnerCollection;
//                            ico.Key = this.Key;
//                            AddObjectToCache(ico);
//                        }
//                    }
//                    else
//                    { 
//                        try
//                        {
//                        ICacheableObject ico = BuildItemProcedure();
//                        ico.OwnerCollection = this.OwnerCollection;
//                        ico.Key = this.Key;
//                        AddObjectToCache(ico);
//                        }
//                        catch (Exception ex)
//                        {}
//                    }
                
//                return (ICacheableObject)this.OwnerCollection.Cache[this.Key];
//            }
//            //private set { _CachedObject = value; }
//        }
        
//        /// <summary>
//        /// Restituisce il riferimento all'istanza della DataRow dal qualè è stato generato l'elemento indicizzato
//        /// </summary>
//        public DataRow CurrentRecordData
//        {
//            get
//            {
//                /*
//                 Costruisco il record se:
//                 * 1) E' uguale a NULL
//                 * 2) La tabella nella OwnerCollection contiene meno record dell'indice attualmente memorizzato (evidentemente sono stati rimossi record)
//                 * 3) Il record nella tabella nella OwnerCollection non corrisponde a quello attualmente memorizzato in questa istanza di CacheableItem
//                 */

//                if (_CurrentRecordData == null) //Caso 1
//                    BuildRecordData();
//                else
//                {
//                    if (this.OwnerCollection.DataTableManager.DataTable.Rows.Count <= this.LastIndex) //Caso 2
//                        BuildRecordData();
//                    else
//                    {
//                        //Caso 3
//                        string hashCurrent = _CurrentRecordData.GetHashCode().ToString();
//                        string hashNew = this.OwnerCollection.DataTableManager.DataTable.Rows[this.LastIndex].GetHashCode().ToString();
//                        if (hashCurrent != hashNew)
//                            BuildRecordData();
//                    }
//                }

//                return _CurrentRecordData;
//            }
//            private set { _CurrentRecordData = value; }
//        }
//        private DataRow _CurrentRecordData;
//        private void BuildRecordData()
//        {
//            DataRow[] rows = OwnerCollection.DataTableManager.DataTable.Select(OwnerCollection.PrimaryKey + " = " + this.ID);
//            if (rows.Length == 1)
//                CurrentRecordData = rows[0];
//            else
//            {
//                this.OwnerCollection.Remove(this);
//            }
//        }

//        /// <summary>
//        /// Restituisce il riferimento alla CacheableCollection che ha generato questa istanza di CacheableItem
//        /// </summary>
//        public CacheableCollection OwnerCollection
//        {
//            get
//            {
//                return _OwnerCollection;
//            }
//            private set 
//            {
//                if (_OwnerCollection == null)
//                    _OwnerCollection = value;
//                else
//                {
//                  Exception ex = new Exception("Impossibile associare l'oggetto alla CacheableCollection. L'oggetto CacheableItem '" + Key + "' appartiene già ad una CacheableCollection");
//                }
//            }
//        }
//        private CacheableCollection _OwnerCollection;

//        /// <summary>
//        /// Costruttore della classe CacheableItem
//        /// </summary>
//        /// <param name="recordData">Il record dal quale verrà generata l'istanza delloggetto da mettere in Cache</param>
//        /// <param name="index">Indice della DataRow nella DataTable sorgente</param>
//        /// <param name="ownerCollection">Riferimento alla CacheableCollection che genera questa istanza di CacheableItem</param>
//        public CacheableItem(DataRow recordData,int index ,CacheableCollection ownerCollection)
//        {            
//            this.LastIndex = index;
//            this.CurrentRecordData = recordData;
//            this.OwnerCollection = ownerCollection;
//            this.ID = recordData[ownerCollection.PrimaryKey].ToString();
//            this.Key = BuildItemKey(this.OwnerCollection, this.ID);
//        }
//        /// <summary>
//        /// Costruttore della classe CacheableItem
//        /// </summary>
//        /// <param name="recordData">Il record dal quale verrà generata l'istanza delloggetto da mettere in Cache</param>
//        /// <param name="index">Indice della DataRow nella DataTable sorgente</param>
//        /// <param name="ownerCollection">Riferimento alla CacheableCollection che genera questa istanza di CacheableItem</param>
//        /// <param name="key">stringa contenente la chiave associata all'oggetto in cache</param>
//        public CacheableItem(DataRow recordData, int index, CacheableCollection ownerCollection,string key)
//            :this(recordData,index ,ownerCollection)
//        {
           
//            this.Key = key;
//        }
//        /// <summary>
//        /// Questo metodo aggiunge alla Cache l'oggetto indicizzato da questa classe
//        /// </summary>
//        /// <param name="obj"></param>
//        private void AddObjectToCache(ICacheableObject obj)
//        {
//            this.OwnerCollection.Cache.Insert(this.Key,//La chiave univoca di accesso all'oggetto in chache
//                                          obj,//Il valore dell'oggetto in cache
//                                          this.OwnerCollection.ItemsCachingTime,//L'oggetto viene rimosso dalla cache 20 minuti dopo l'ultimo utilizzo
//                                          new CacheItemRemovedCallback(RemovedCallback)//Alla rimozione dell'oggetto dalla cache viene chiamato il metodo RemovedCallback() di questa classe
//                                          );
//        }

//        /// <summary>
//        /// Metodo delegato, legato all'evento di rimozione del'elemento indicizzato dalla Cache.
//        /// </summary>
//        public void RemovedCallback(String k, Object v, CacheItemRemovedReason r)
//        {
//            //CachedObject = null;
//            this.OwnerCollection.Cache.RemoveKey(this.Key);
//        }

//        /// <summary>
//        /// Questo metodo deve implementate la logica di generazione di una nuova istanza dell'Oggetto da mettere in Cache
//        /// </summary>
//        /// <returns>Un ICacheableObject generato dalla funzione stessa</returns>
//        protected abstract ICacheableObject BuildItemProcedure();
        
//        /// <summary>
//        /// Questo metodo rimuove l'oggetto dalla Cache di ASP.NET
//        /// </summary>
//        public void Invalidate()
//        {
//            this.CachedObject.Dispose();
//            this.OwnerCollection.Cache.Remove(this.Key);
//        }

//        /// <summary>
//        /// Questo metodo permette di generare una nuova chiave di indicizzazione in Cache.
//        /// </summary>
//        /// <param name="OwnerCollection">La collezione che indicizza l'oggetto.</param>
//        /// <param name="ID">Un valore che identifichi univocamente l'istanza dell'oggetto</param>
//        /// <returns>La chiave per la Cache generata da questa funzione</returns>
//        public static string BuildItemKey(CacheableCollection OwnerCollection, string ID)
//        {
//            string key = "";

//            key = G2Core.Caching.Visibility.CachingVisibilityUtility.BuildVisibilityKey(OwnerCollection.Visibility) +//Modificatore di visibilità
//            "[" + OwnerCollection.ElementsType + "]" + //Tipo dell'oggetto
//            "[" + ID + "]";//ID del record;
//            if(!OwnerCollection.ShareItemsWithAllQuerys)
//                key += "[" + OwnerCollection.DataTableManager.CacheKeyMD5 + "]"; //Hash MD5 della key associata alla datatable dal quale proviene il record relativo all'oggetto

//            return key;
//        }
//    }
//}
