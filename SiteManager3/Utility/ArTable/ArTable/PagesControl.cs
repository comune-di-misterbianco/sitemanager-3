using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NetService.Utility.Common;

namespace NetService.Utility.Controls
{
    public class PaginationHandler : WebControl
    {
        public int CurrentPage
        {
            get
            {
                return _CurrentPage;
            }
            set
            {
                _CurrentPage = value < 1 ? 1 : (value > PagesCount ? 1:value);
            }
        }
        private int _CurrentPage;

        public int PageSize
        {
            get
            {
                return _PageSize;
            }
            set
            {
                _PageSize = value;
            }
        }
        private int _PageSize = 25;

        public int PagesCount
        {
            get
            {
                if (_PagesCount == -1)
                {
                    if (this.PageSize > 0)
                    {
                        _PagesCount = (this.RecordsCount / this.PageSize);
                        if (_PagesCount > 0 && (this.RecordsCount % this.PageSize)>0)
                            _PagesCount += 1;
                    }
                }
                if (_PagesCount <= 0)
                    _PagesCount = 1;

                return _PagesCount;
            }
        }
        private int _PagesCount = -1;

        public int RecordsCount
        {
            get
            {
                return _RecordsCount;
            }
            set
            {
                _RecordsCount = value;
            }
        }
        private int _RecordsCount;

        /// <summary>
        /// Deve contenere l'url base per navigare la pagina.
        /// </summary>
        public string BaseLink
        {
            get
            {
                return _BaseLink;
            }
            set
            {
                _BaseLink = value;
            }
        }
        private string _BaseLink;

        /// <summary>
        /// Serve per settare l'anchor da query string qualora la pagina 
        /// contenesse pi� di una tabella
        /// </summary>
        public string AnchorString
        {
            get
            {
                return _AnchorString;
            }
            set
            {
                _AnchorString = value;
            }
        }
        private string _AnchorString;

        //private string WorkedAddress
        //{
        //    get
        //    {
        //        if (_WorkedAddress == null)
        //        {
        //            string firstSeparator = BaseLink.Contains("?") ? "&" : "?";
        //            _WorkedAddress = BaseLink + firstSeparator + this.PaginationKey + "={0}";                    
        //        }
        //        return _WorkedAddress;
        //    }
        //}
        //private string _WorkedAddress;

        public string NextLabel
        {
            get
            {
                return _NextLabel;
            }
            set
            {
                _NextLabel = value;
            }
        }
        private string _NextLabel = "&rsaquo;";

        public string LabelCurrentPage
        {
            get
            {
                return _LabelCurrentPage;
            }
            private set
            {
                _LabelCurrentPage = value;
            }
        }
        private string _LabelCurrentPage = "Page {0} of {1}";

        public string PrevLabel
        {
            get
            {
                return _PrevLabel;
            }
            set
            {
                _PrevLabel = value;
            }
        }
        private string _PrevLabel = "&lsaquo;";

        public string FirstLabel
        {
            get
            {
                return _FirstLabel;
            }
            set
            {
                _FirstLabel = value;
            }
        }
        private string _FirstLabel = "&lsaquo;&lsaquo;";

        public string Lastlabel
        {
            get
            {
                return _Lastlabel;
            }
            set
            {
                _Lastlabel = value;
            }
        }
        private string _Lastlabel = "&rsaquo;&rsaquo;";

        public int NumbersThoShowOffset
        {
            get
            {
                return _NumbersThoShowOffset;
            }
            set
            {
                _NumbersThoShowOffset = value;
            }
        }
        private int _NumbersThoShowOffset = 4;

        public string PaginationKey
        {
            get
            {
                return _PaginationKey;
            }
            set
            {
                _PaginationKey = value;
            }
        }
        private string _PaginationKey = "page";
        
        //TODO eseguire il refactory con il nuovo tipo enumerativo al posto dell'intero
        public int Style
        {
            get
            {
                return _Style;
            }
            set
            {
                _Style = value;
            }
        }
        private int _Style;      

        public PaginationStyle PaginationTheme
        {
            get
            {
                return (PaginationStyle)Style;
            }            
        }

        public Button GoToButton
        {
            get
            {
                if (_GoToButton == null)
                {
                    _GoToButton = new Button();
                    _GoToButton.ID = this.PaginationKey + "_di_button";
                    _GoToButton.CssClass = "pag-button";
                    _GoToButton.Text = "Go To";
                }
                return _GoToButton;
            }
        }
        private Button _GoToButton;

        public bool GoToFirstPageOnSearch { get; set; }

        public RequestVariable PageRequest
        {
            get
            {
                if (_PageRequest == null)
                {
                    _PageRequest = new RequestVariable(PaginationKey, RequestVariable.RequestType.Form_QueryString);
                }
                return _PageRequest;
            }
        }
        private RequestVariable _PageRequest;

        public PaginationHandler()
            : base(HtmlTextWriterTag.Div)
        {
        }

        public bool IsPostBack
        {
            get
            {
                return _IsPostBack;
            }
            set 
            {
                _IsPostBack = value;
            }
        }
        private bool _IsPostBack = false;

        public PaginationHandler(int pageSize, bool gotoFirstPageOnSearch = true)
            : base(HtmlTextWriterTag.Div)
        {
            this.PageSize = pageSize;
            this.GoToFirstPageOnSearch = gotoFirstPageOnSearch;
            this.IsPostBack = (GoToFirstPageOnSearch && Page.IsPostBack);
            this.CurrentPage = (GoToFirstPageOnSearch && IsPostBack) ? 1 : this.PageRequest.IsValidInteger ? this.PageRequest.IntValue : 1;
            this.Style = 0;
        }

        public PaginationHandler(int pageSize, int recordCount, bool gotoFirstPageOnSearch = true)
            : base(HtmlTextWriterTag.Div)
        {
            this.PageSize = pageSize;
            this.GoToFirstPageOnSearch = gotoFirstPageOnSearch;
            this.RecordsCount = recordCount;
            this.IsPostBack = (GoToFirstPageOnSearch && Page.IsPostBack);
            this.CurrentPage = (GoToFirstPageOnSearch && IsPostBack) ? 1 : this.PageRequest.IsValidInteger ? this.PageRequest.IntValue : 1;
            this.Style = 0;
            
        }

        /// <summary>
        /// Deprecato: utilizzare il costruttore con il parametro isPostBack
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="recordCount"></param>
        /// <param name="paginationKey"></param>
        /// <param name="style">parametro per  Stile grafico. 0 per il default, 1 per Bootstrap</param>
        public PaginationHandler(int pageSize, int recordCount, string paginationKey,int style = 0, bool gotoFirstPageOnSearch = true)
            : base(HtmlTextWriterTag.Div)
        {
            this.PaginationKey = paginationKey;
            this.GoToFirstPageOnSearch = gotoFirstPageOnSearch;
            this.PageSize = pageSize;
            this.RecordsCount = recordCount;
            this.IsPostBack = (GoToFirstPageOnSearch && IsPostBack);
            this.CurrentPage = (GoToFirstPageOnSearch && IsPostBack) ? 1 : this.PageRequest.IsValidInteger ? this.PageRequest.IntValue : 1;
            this.Style = style;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="recordCount"></param>
        /// <param name="paginationKey"></param>
        /// <param name="isPostBack"></param>
        /// <param name="style">parametro per  Stile grafico. 0 per il default, 1 per Bootstrap</param>
        public PaginationHandler(int pageSize, int recordCount, string paginationKey, bool isPostBack, int style = 0, bool gotoFirstPageOnSearch = true)
            : base(HtmlTextWriterTag.Div)
        {
            this.PaginationKey = paginationKey;
            this.GoToFirstPageOnSearch = gotoFirstPageOnSearch;
            this.PageSize = pageSize;
            this.RecordsCount = recordCount;
            this.IsPostBack = isPostBack;
            this.CurrentPage = (GoToFirstPageOnSearch && IsPostBack) ? 1 : this.PageRequest.IsValidInteger ? this.PageRequest.IntValue : 1;
            this.Style = style;                     
        }



        //protected override void OnInit(EventArgs e)
        //{
        //    this.CurrentPage = (GoToFirstPageOnSearch && Page.IsPostBack)?1: this.PageRequest.IntValue;
        //    base.OnInit(e);
        //        this.Controls.Add(IndexControl());
        //    if (this.PagesCount > 1)
        //    {
        //        if (CurrentPage > 1)
        //        {
        //            this.Controls.Add(FirstPageButton());
        //            this.Controls.Add(PrevPageButton());
        //        }

        //        int before = (CurrentPage - this.NumbersThoShowOffset);
        //        before = before > 1 ? before : 1;

        //        int after = (CurrentPage + this.NumbersThoShowOffset) + 1;
        //        after = after < this.PagesCount ? after : this.PagesCount;
        //        for (int i = before; i <= after; i++)
        //        {
        //            this.Controls.Add(PageButton(i));
        //        }

        //        if (CurrentPage < PagesCount)
        //        {
        //            this.Controls.Add(NextPageButton());
        //            this.Controls.Add(LastPageButton());
        //        }
        //    }
        //}



        protected override void OnPreRender(EventArgs e)
        {

            this.CurrentPage = (GoToFirstPageOnSearch && IsPostBack) ? 1 : this.PageRequest.IntValue;
            base.OnPreRender(e);                

            switch (PaginationTheme)
            {
                case PaginationStyle.Default:
                    BuildDefault();
                    break;
                case PaginationStyle.Bootstrap_3:
                    BuildBootstrap();
                    break;
                case PaginationStyle.Bootstrap_4:
                    BuildBootstrap4();
                    break;
                default:
                    BuildDefault();
                    break;
            }

        }

        public enum PaginationStyle
        {
            Default = 0,
            Bootstrap_3 = 1,
            Bootstrap_4 = 2
        }

        public string BuildHref(int pageNumber)
        {
            string href = "";
            if (string.IsNullOrEmpty(AnchorString))
                href = string.Format(BaseLink, pageNumber);
            else
                href = string.Format(BaseLink, pageNumber) + AnchorString ;
            return href;
        }

        private WebControl IndexControl()
        {
            
            WebControl span = new TextWebControl(HtmlTextWriterTag.Span, string.Format(LabelCurrentPage, CurrentPage, this.PagesCount));
            span.CssClass = "pagination-overview";

            return span;
        }
        private WebControl NextPageButton()
        {
            string label = NextLabel;
            WebControl a = new WebControl(HtmlTextWriterTag.A);
            a.Attributes.Add("href", BuildHref(CurrentPage + 1));

            WebControl span = new TextWebControl(HtmlTextWriterTag.Span, label);
            a.CssClass = "pagination-next";
            a.Controls.Add(span);

            return a;
        }
        private WebControl PrevPageButton()
        {
            string label = PrevLabel;
            WebControl a = new WebControl(HtmlTextWriterTag.A);
            a.Attributes.Add("href", BuildHref(CurrentPage - 1));

            WebControl span = new TextWebControl(HtmlTextWriterTag.Span, label);
            a.CssClass = "pagination-prev";
            a.Controls.Add(span);

            return a;
        }
        private WebControl FirstPageButton()
        {
            string label = FirstLabel;
            WebControl a = new WebControl(HtmlTextWriterTag.A);
            a.Attributes.Add("href", BuildHref(0));

            WebControl span = new TextWebControl(HtmlTextWriterTag.Span, label);
            a.CssClass = "pagination-first";
            a.Controls.Add(span);

            return a;
        }
        private WebControl LastPageButton()
        {
            string label = Lastlabel;
            WebControl a = new WebControl(HtmlTextWriterTag.A);
            a.Attributes.Add("href", BuildHref(PagesCount));

            WebControl span = new TextWebControl(HtmlTextWriterTag.Span, label);
            a.CssClass = "pagination-last";
            a.Controls.Add(span);

            return a;
        }

        private void BuildDefault()
        {
            this.Controls.Add(IndexControl());
            if (this.PagesCount > 1)
            {
                if (CurrentPage > 1)
                {
                    this.Controls.Add(FirstPageButton());
                    this.Controls.Add(PrevPageButton());
                }

                int before = (CurrentPage - this.NumbersThoShowOffset);
                before = before > 1 ? before : 1;

                int after = (CurrentPage + this.NumbersThoShowOffset) + 1;
                after = after < this.PagesCount ? after : this.PagesCount;
                for (int i = before; i <= after; i++)
                {
                    this.Controls.Add(PageButton(i));
                }

                if (CurrentPage < PagesCount)
                {
                    this.Controls.Add(NextPageButton());
                    this.Controls.Add(LastPageButton());
                }
            }
        }

        private void BuildBootstrap()
        {
            WebControl ul = new WebControl(HtmlTextWriterTag.Ul);
            ul.CssClass = "pagination";
            if (this.PagesCount > 1)
            {
                

                int before = (CurrentPage - this.NumbersThoShowOffset);
                before = before > 1 ? before : 1;

                int after = (CurrentPage + this.NumbersThoShowOffset) + 1;
                after = after < this.PagesCount ? after : this.PagesCount;
                WebControl li = new WebControl(HtmlTextWriterTag.Li);
                if (CurrentPage == 1)
                    li.Attributes.Add("class", "disabled");
                WebControl a = new WebControl(HtmlTextWriterTag.A);
                a.Attributes.Add("href", BuildHref(CurrentPage - 1));
                a.Attributes.Add("aria-label", PrevLabel);
                WebControl span = new WebControl(HtmlTextWriterTag.Span);
                span.Attributes.Add("aria-hidden", "true");
                span.Controls.Add(new LiteralControl("&laquo;"));
                a.Controls.Add(span);
                li.Controls.Add(a);
                ul.Controls.Add(li);
                WebControl span2 = new WebControl(HtmlTextWriterTag.Span);
                span2.Attributes.Add("class", "sr-only");
                span2.Controls.Add(new LiteralControl("(current)"));
                for (int i = before; i <= after; i++)
                {
                    li = new WebControl(HtmlTextWriterTag.Li);
                    span = new WebControl(HtmlTextWriterTag.Span);
                    
                    a = new WebControl(HtmlTextWriterTag.A);
                    a.Attributes.Add("href", BuildHref(i));
                    a.Controls.Add(new LiteralControl(i.ToString()));
                    if (CurrentPage == i)
                    {
                        li.Attributes.Add("class", "active");
                        a.Controls.Add(span2);
                    }
                    li.Controls.Add(a);
                    ul.Controls.Add(li);

                }
                li = new WebControl(HtmlTextWriterTag.Li);
                if (CurrentPage == this.PagesCount)
                    li.Attributes.Add("class", "disabled");
                a = new WebControl(HtmlTextWriterTag.A);
                a.Attributes.Add("href", BuildHref(CurrentPage + 1));
                a.Attributes.Add("aria-label", NextLabel);
                span = new WebControl(HtmlTextWriterTag.Span);
                span.Attributes.Add("aria-hidden", "true");
                span.Controls.Add(new LiteralControl("&raquo;"));
                a.Controls.Add(span);
                li.Controls.Add(a);
                ul.Controls.Add(li);
                //if (CurrentPage < PagesCount)
                //{
                //    this.Controls.Add(NextPageButton());
                //    this.Controls.Add(LastPageButton());
                //}
            }
        



            this.Controls.Add(ul);
        }

        private void BuildBootstrap4()
        {                       
            HtmlGenericControl nav = new HtmlGenericControl("nav");
            nav.Attributes["class"] = "pagination-wrapper";
            nav.Attributes["aria-labels"] = "Records pagination";

            WebControl ul = new WebControl(HtmlTextWriterTag.Ul);
            ul.CssClass = "pagination";
            if (this.PagesCount > 1)
            {


                int before = (CurrentPage - this.NumbersThoShowOffset);
                before = before > 1 ? before : 1;

                int after = (CurrentPage + this.NumbersThoShowOffset) + 1;
                after = after < this.PagesCount ? after : this.PagesCount;
                WebControl li = new WebControl(HtmlTextWriterTag.Li);
                li.CssClass = "page-item";
                //if (CurrentPage == 1)
                //    li.Attributes.Add("class", "disabled");
                
                WebControl a = new WebControl(HtmlTextWriterTag.A);
                a.Attributes.Add("class","page-link");
                a.Attributes.Add("href", BuildHref(CurrentPage - 1));
                a.Attributes.Add("aria-label", PrevLabel);
                
                WebControl span = new WebControl(HtmlTextWriterTag.Span);
                span.Attributes.Add("aria-hidden", "true");
                span.Controls.Add(new LiteralControl("&laquo;"));
                a.Controls.Add(span);
                li.Controls.Add(a);
                ul.Controls.Add(li);

                WebControl span2 = new WebControl(HtmlTextWriterTag.Span);
                span2.Attributes.Add("class", "sr-only");
                span2.Controls.Add(new LiteralControl("(current)"));
                for (int i = before; i <= after; i++)
                {
                    li = new WebControl(HtmlTextWriterTag.Li);
                    li.CssClass = "page-item";
                    span = new WebControl(HtmlTextWriterTag.Span);

                     

                    a = new WebControl(HtmlTextWriterTag.A);
                    a.Attributes.Add("href", BuildHref(i));
                    a.CssClass = "page-link";

                    a.Controls.Add(new LiteralControl(i.ToString()));
                    if (CurrentPage == i)
                    {
                        a.Attributes.Add("aria-current", "page");
                        a.Controls.Add(span2);
                    }
                    li.Controls.Add(a);
                    ul.Controls.Add(li);

                }
                li = new WebControl(HtmlTextWriterTag.Li);
                li.CssClass = "page-item";
                if (CurrentPage == this.PagesCount)
                    li.Attributes.Add("class", "disabled");
                a = new WebControl(HtmlTextWriterTag.A);
                a.Attributes.Add("href", BuildHref(CurrentPage + 1));
                a.CssClass = "page-link";
                a.Attributes.Add("aria-label", NextLabel);
                span = new WebControl(HtmlTextWriterTag.Span);
                span.Attributes.Add("aria-hidden", "true");
                span.Controls.Add(new LiteralControl("&raquo;"));
                a.Controls.Add(span);
                li.Controls.Add(a);
                ul.Controls.Add(li);
                //if (CurrentPage < PagesCount)
                //{
                //    this.Controls.Add(NextPageButton());
                //    this.Controls.Add(LastPageButton());
                //}
            }


            nav.Controls.Add(ul);

            this.Controls.Add(nav);
        }


        private WebControl PageButton(int pageNumber)
        {
            string label = pageNumber.ToString();
            if (this.CurrentPage == pageNumber)
            {
                WebControl span = new TextWebControl(HtmlTextWriterTag.Span, label);
                span.CssClass = "pagination-number current";
                return span;
            }
            else
            {
                WebControl a = new WebControl(HtmlTextWriterTag.A);
                a.Attributes.Add("href", BuildHref(pageNumber));

                WebControl span = new TextWebControl(HtmlTextWriterTag.Span, label);
                a.CssClass = "pagination-number";
                a.Controls.Add(span);
                return a;
            }
        }


        public string BuildPaginationBaseAddress()
        {
            var addr = BuildBaseAddress();
            if (addr.EndsWith("?"))
                return addr + this.PaginationKey + "={0}";

            return string.Join("&", addr, this.PaginationKey + "={0}");
        }
        public string BuildBaseAddress()
        {
            QueryStringManager.QueryStringManager qsmanager = new QueryStringManager.QueryStringManager();

            var legacyArray = GetLegacyQueryString().Select(x => x + "=" + qsmanager[x]).ToArray();
            var searchArray = BuildSearchParameterForQueryString();
            var allKeys = (searchArray != null) ? legacyArray.Union(searchArray) : legacyArray;

            string pagename = new System.IO.FileInfo(System.Web.HttpContext.Current.Request.Url.LocalPath).Name;            

            string newQs = string.Join("&", allKeys);

            return pagename + "?" + newQs;
        }

        private string[] GetLegacyQueryString()
        {
            QueryStringManager.QueryStringManager qsmanager = new QueryStringManager.QueryStringManager();
            var qsKeys = qsmanager.Keys;
                   
            return qsKeys.Where(x=> x != this.PaginationKey).ToArray();
        }

        private string[] BuildSearchParameterForQueryString()
        {
            var searchArray = (SearchFields != null) ? SearchFields.Select(x => x.GetFieldQueryString()).Where(x => x != null).ToArray() : null;

            return searchArray;
        }

        public List<ValidatedFields.VfGeneric> SearchFields { get; set; }
    }
}