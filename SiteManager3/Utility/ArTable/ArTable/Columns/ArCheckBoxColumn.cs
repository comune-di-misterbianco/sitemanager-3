using System;
using System.Web.UI.WebControls;

namespace NetService.Utility.ArTable
{
    public class ArCheckBoxColumn : ArAbstractColumn
    {
        public string CheckBoxIDOffset
        {
            get
            {
                return _CheckBoxIDOffset;
            }
            set
            {
                _CheckBoxIDOffset = value;
            }
        }
        private string _CheckBoxIDOffset = "chk"; 

        public ArCheckBoxColumn(string caption, string primaryKeyPropertyName)
            : base(caption, primaryKeyPropertyName)
        {
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            string checkModel = @"<input type=""checkbox"" name=""{1}_{0}""  id=""{1}_{0}"">";
            string CellID = GetValue(objectInstance, this.Key).ToString();

            TableCell td = new TableCell();
            if(!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;
            td.Text = "<span>" + string.Format(checkModel,CellID, CheckBoxIDOffset) + "</span>";

            return td;
        }

        public override bool ExportData
        {
            get { return false; }
        }
        public override string GetExportValue(object obj)
        {
            throw new NotImplementedException();
        }
    }
}