using System;
using System.Web.UI.WebControls;

namespace NetService.Utility.ArTable
{
    public class ArConditionedActionColumn : ArAbstractColumn
    {
        public virtual string Href { get; protected set; }

        public Icons Icon { get; protected set; }

        public string AnchorCssClass { get; set; }

        public string ToolTip { get; set; }

        public Predicate<object> _CanExecute;

        public Predicate<object> WarningExecutePredicate;
                   
        public string LabelWarningIfCanExecute { get; set; }

        public string LabelIfCanNotExecute { get; set; }

        public string LabelIfCanExecute { get; set; }

        public bool DeactivateLinkIfCanNotExecute = false;

        public string TooltipText
        {
            get;
            set;
        }

        public enum Icons
        {
            Custom,
            Ok,
            Enable,
            Disable,
            Edit,
            Delete,
            Details,
            Add,
            Remove,
            Search,
            Table,
            Go,
            Stop
        }
        /// <summary>
        /// ArActionColumn Costruttore
        /// </summary>
        /// <param name="caption">Etichetta della colonna</param>
        /// <param name="icon">Icona da associare al bottone (nel caso in cui si vuole personalizzare l'icona, usare "Custom" e riempire la properiet� "CssClass")</param>
        /// <param name="href">L'indirizzo a cui punta la singola cella. Deve essere una stringa contenente un url o una stringa del tipo "javascript: ;". E' possibile minserire dei marker del tipo {NomePropriet�ActiveRecord}, che verranno opportunamente rimpiazzati a runtime.</param>
        /// <param name="tootltipText">Se valorizzato, sar� il testo nel tooltip in caso di impossibilit� di esecuzione dell'azione.( canExecute == false ? inserisce un tooltip che 
        /// dovrebbe spiegare il perch�.Se non valorizzato, la colonna si comporta normalmente.)</param>
        public ArConditionedActionColumn(string caption, Icons icon, string href, Predicate<object> canExecute, string labelIfCanNotExecute, string labelIfCanExecute="", string tootltipText = "")
            : base(caption, "")
        {
            this.Href = href;
            this.Icon = icon;
            _CanExecute = canExecute;
            this.TooltipText = tootltipText;
            this.LabelIfCanNotExecute = labelIfCanNotExecute;
            this.LabelIfCanExecute = labelIfCanExecute;
        }
        public ArConditionedActionColumn(string caption, string key, string href, Predicate<object> canExecute, string labelIfCanNotExecute)
            :base(caption,key)
        {
            this.Href = href;
            this.ToolTip = caption;
            _CanExecute = canExecute;
            this.LabelIfCanNotExecute = labelIfCanNotExecute;
        }
        /// <summary>
        ///  Questa costruttore, nel caso in cui non sia ammessa l'esecuzione dell'azione associata (canExecute = false),
        ///  inserisce un tooltip con un help con il testo passato in tooltipText.
        ///  Se il testo � vuoto si comporta normalmente.
        /// </summary>
        /// <param name="caption"></param>
        /// <param name="icon"></param>
        /// <param name="href"></param>
        /// <param name="canExecute"></param>
        /// <param name="labelIfCanNotExecute"></param>
        /// <param name="labelIfCanExecute"></param>
        /// <param name="tootltipText"></param>
        

        private bool CanExecute(object parameter)
        {
            return _CanExecute == null ? true : _CanExecute(parameter);
        }


        private bool CanExecuteWaring(object parameter)
        {
            return WarningExecutePredicate == null ? true : WarningExecutePredicate(parameter);
        }


        public override TableCell GetCellControl(object objectInstance)
        {          
            if (string.IsNullOrEmpty(Key))
                return GetCellControl(objectInstance, Caption, Href, Icon);
            else               
                return GetCellControl(objectInstance, Caption, Key, Href, Icon);
        }

        protected virtual TableCell GetCellControl(object objectInstance, string label, string href, Icons icon)
        {
            bool canExecute = this.CanExecute(objectInstance);
            bool canExecuteWarning = this.CanExecuteWaring(objectInstance);
            
            if (!canExecute)
                icon = Icons.Stop;

            TableCell td = new TableCell();

            if (icon != null)
            {
                if (icon == Icons.Custom)
                {
                    if (!string.IsNullOrEmpty(this.CssClass)) td.CssClass = this.CssClass;
                }
                else
                    td.CssClass = "action " + icon.ToString().ToLower();
            }
           
            string anchorCssClassHtml = string.IsNullOrEmpty(AnchorCssClass) ? "" : "class=\"" + this.AnchorCssClass + "\"";
            string anchorTitle = string.IsNullOrEmpty(ToolTip) ? "" : "title=\"" + base.ParseString(objectInstance, ToolTip) + "\"";

            //if (CanExecute(objectInstance))
            if (canExecute)
            {
                td.Text = "<a " + anchorCssClassHtml + " href=\"" + base.ParseString(objectInstance, href) + "\"><span>" + ((this.LabelIfCanExecute.Length == 0) ? base.ParseString(objectInstance, label) : this.LabelIfCanExecute) + "</span></a>";

                if (!canExecuteWarning)
                    td.Text += "<span>"+ this.LabelWarningIfCanExecute + "</span>";
            }
            else
            {
                if (!String.IsNullOrEmpty(this.TooltipText))
                {
                    //td.Text = "<a class=\"tooltip-cms3\"> ?<b></b> <span style=\"display:inline-block;width:350px;\">" +
                    td.Text = "<a class=\"tooltip-cms3\"> ?<b></b> <span class=\"ArConditionedSpanClass\">" +
                        TooltipText + "</span></a>";
                    td.CssClass = "";
                    
                }
                else
                {
                    if (!DeactivateLinkIfCanNotExecute)
                        td.Text = "<a " + anchorCssClassHtml + " href=\"#\"><span>" + this.LabelIfCanNotExecute + "</span></a>";
                    else
                        td.Text = "<span>" + this.LabelIfCanNotExecute + "</span>";
                }

            }   
                          
            return td;
        }

        protected virtual TableCell GetCellControl(object objectInstance, string label, string key, string href, Icons icon)
        {
            bool canExecute = this.CanExecute(objectInstance);
            bool canExecuteWarning = this.CanExecuteWaring(objectInstance);

            if (!canExecute)
                icon = Icons.Stop;

            TableCell td = new TableCell();
            if (icon != null)
            {
                if (icon == Icons.Custom)
                {
                    if (!string.IsNullOrEmpty(this.CssClass)) td.CssClass = this.CssClass;
                }
                else
                    td.CssClass = "action " + icon.ToString().ToLower();
            }

            string anchorCssClassHtml = string.IsNullOrEmpty(AnchorCssClass) ? "" : "class=\"" + this.AnchorCssClass + "\"";
            string anchorTitle = string.IsNullOrEmpty(ToolTip) ? "" : "title=\"" + base.ParseString(objectInstance, ToolTip) + "\"";

            //if (CanExecute(objectInstance))
            if (canExecute)
            {
                td.Text = "<a " + anchorCssClassHtml + " href=\"" + base.ParseString(objectInstance, href) + "\"><span>" + GetValue(objectInstance, Key) + "</span></a>";

                if (!canExecuteWarning)
                    td.Text += "<span>" + this.LabelWarningIfCanExecute + "</span>";
            }
            else
            {
                if (!DeactivateLinkIfCanNotExecute)
                    td.Text = "<a " + anchorCssClassHtml + " href=\"#\"><span>" + this.LabelIfCanNotExecute + "</span></a>";
                else
                    td.Text = "<span>" + this.LabelIfCanNotExecute + "</span>";
            }

            return td;
        }

        public override bool ExportData
        {
            get { return false; }
        }
        public override string GetExportValue(object obj)
        {
            throw new NotImplementedException();
        }
    }
}