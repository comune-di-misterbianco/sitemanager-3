using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Data;
using System;

namespace NetService.Utility.ArTable
{
    public class ArMultiColumn : ArAbstractColumn
    {
        public ArMultiColumn(string caption, List<string> keys)
            : base(caption, keys)
        {
        }

        /// <summary>
        /// Con questo costruttore si possono stampare Liste direttamente dalla propriet�
        /// </summary>
        /// <param name="caption"></param>
        /// <param name="keys">Usare il format tipo "Propriet�.Nome"</param>
        public ArMultiColumn(string caption, string keys)
          : base(caption, keys)
        {
        }

        /// <summary>
        /// Se true il rendering � di tipo ul horizontal 
        /// </summary>
        public bool HorizontalRendering
        {
            get { return _HorizontalRendering; }
            set { _HorizontalRendering = value; }
        }
        private bool _HorizontalRendering  = false;
        


        public enum DataTypes { PlainText, DateTime, Date, Integer, FileSize }
        public DataTypes DataType { get; set; } 

        public override TableCell GetCellControl(object objectInstance)
        {
            string classUsedOnFrontend = HorizontalRendering ? "list-inline" : "list-unstyled";
            string CellText = UsedForFrontend ? "<ul class=\""+classUsedOnFrontend + "\">": "";
            TableCell td = new TableCell();
            if (string.IsNullOrEmpty(this.Key) || this.Keys != null)
            {
                foreach (string key in this.Keys)
                {
                    if(!UsedForFrontend)
                        CellText += "<span class=\"" + key + "\">" + this.GetValue(objectInstance, key) + "</span>";
                    else
                        CellText += "<li class=\"" + key + "\">" + this.GetValue(objectInstance, key) + "</li>";
                }

                CellText += UsedForFrontend ? "</ul>" : "";
                
                if (!string.IsNullOrEmpty(this.CssClass))
                    td.CssClass = this.CssClass;

                td.Text = CellText;

                
            }
            else
            {
                string[] splittedValue = this.Key.Split('.');
                var objList = this.GetPropertyValue(objectInstance, splittedValue[0].ToString());
                System.Type typeObj = objList.GetType();

                

                var p = objList as IEnumerable<object>;
               
                
                List<string> Values = new List<string>();
                foreach(var value in p)
                {
                    Values.Add(this.GetPropertyValue(value, splittedValue[1].ToString()).ToString());

                }
                this.Keys = Values;
                td = this.GetCellControl(objectInstance);
                this.Keys = null;

            }
            return td;
        }

        public override bool ExportData
        {
            get { return true; }
        }

        public override string GetExportValue(object obj)
        {
            string CellText = "";
            var value = "";
            foreach(string key in this.Keys)
                value += " " + this.GetValue(obj, key);
            
            CellText += value.ToString();
            return CellText;
        }

        protected override object GetValue(object objectInstance, string Property)
        {
            var path = Property.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

            object result = objectInstance;

            foreach (string propName in path)
                result = GetPropertyValue(result, propName);

            //if (result == objectInstance)
            //    return null;
            //if (result == null)
            //    return Property;
            //else
            //    return FilterDataType(result);

            return result == objectInstance ? null : result == null ? Property : FilterDataType(result);
        }


        private string FilterDataType(object value)
        {
            if (value != null)
            {
                if (DataType == DataTypes.FileSize && value is long)
                {
                    return FormatBytes((long)value);
                }

                if (DataType == DataTypes.Date && value is DateTime)
                    return ((DateTime)value).ToLocalTime().ToShortDateString();

                if (DataType == DataTypes.DateTime && value is DateTime)
                {
                    var date = (DateTime)value;
                    return string.Format("{0:dd/MM/yyyy HH':'mm}", (DateTime)value);
                }
                return value.ToString();
            }
            else return "";
        }

        public string FormatBytes(long bytes)
        {
            const int scale = 1024;
            string[] orders = new string[] { "GB", "MB", "KB", "Bytes" };
            long max = (long)Math.Pow(scale, orders.Length - 1);

            foreach (string order in orders)
            {
                if (bytes > max)
                    return string.Format("{0:##.##} {1}", decimal.Divide(bytes, max), order);

                max /= scale;
            }
            return "0 Bytes";
        }

        private object GetPropertyValue(object objectInstance, string Property)
        {
            if (objectInstance is DataRow)
            {
                System.Data.DataRow row = objectInstance as DataRow;
                if (row.Table.Columns.Contains(Property))
                    return row[Property].ToString();
                return null;
            }
            else
            {
                try
                {
                    System.Type ObjectType = objectInstance.GetType();
                    System.Reflection.PropertyInfo prop = ObjectType.GetProperty(Property);

                    if (prop == null)
                        throw new Exception("La classe " + ObjectType.FullName + " non contiene la propriet� " + Property + "");

                    return prop.GetGetMethod().Invoke(objectInstance, new object[] { });
                }
                catch { return null; }
            }
        }       
    }
}