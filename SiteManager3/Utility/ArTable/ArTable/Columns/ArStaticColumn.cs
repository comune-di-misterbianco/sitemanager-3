using System.Web.UI.WebControls;
using System;

namespace NetService.Utility.ArTable
{
    public class ArStaticColumn : ArAbstractColumn
    {
        public override bool ExportData
        {
            get { return true; }
        }

        public string Value { get; private set; } 

        public ArStaticColumn(string caption, string key, string value)
            : base(caption, key)
        {
            this.Value = value;
        }
        public override TableCell GetCellControl(object objectInstance)
        {
            var CellText = Value.ToString();

            TableCell td = new TableCell();
            if(!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;            
            td.Text = "<span>" + CellText + "</span>";

            return td;
        }

        public override string GetExportValue(object obj)
        {
            var value = Value.ToString();
            string CellText = value == null ? "" : value.ToString();
            return CellText;
        }
    }
}