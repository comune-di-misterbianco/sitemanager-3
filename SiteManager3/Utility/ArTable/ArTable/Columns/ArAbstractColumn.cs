using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Data;
using System.Collections.Generic;
using NetUtility.Report.Exporter;

namespace NetService.Utility.ArTable
{
    public abstract class ArAbstractColumn
    {
        public string Caption
        {
            get;
            protected set;
        }
        public string ColumnCaption { get; set; }

      
        /// <summary>
        /// ArColumn Used Or Not Used for frontend
        /// </summary>
        public bool UsedForFrontend
        {
            get { return _UsedForFrontend; }
            set { _UsedForFrontend = value; }
        }
        private bool _UsedForFrontend = false;


        public string Key
        {
            get
            {
                return _Key;
            }
            private set
            {
                _Key = value;
            }
        }
        private string _Key;

        private List<string> _Keys;
        public List<string> Keys
        {
            get { return _Keys; }
            set { _Keys = value; }
        }
        

        public string UniqueKey
        {
            get
            {
                return _UniqueKey ?? (_UniqueKey = Key + this.GetHashCode());
            }
        }
        private string _UniqueKey;

        public List<ExportFormats> ExportFormatsEnabled { get; set; }

        public string CssClass
        {
            get { return _CssClass; }
            set { _CssClass = value; }
        }
        private string _CssClass;

        public abstract bool ExportData { get; }

        public ArTable.DataHide dataHide
        {
            get
            {
                return _dataHide;
            }
            set
            {
                _dataHide = value;
            }
        }
        private ArTable.DataHide _dataHide;

        public bool showToggle
        {
            get
            {
                return _showToggle;
            }
            set
            {
                _showToggle = value;
            }
        }
        private bool _showToggle;
        public ArAbstractColumn(string caption, string key)
        {
            ColumnCaption = Caption = caption;
            this.Key = key;
        }

        public ArAbstractColumn(string caption, List<string> keys)
        {
            ColumnCaption = Caption = caption;
            this.Keys = keys;
        }

        public abstract TableCell GetCellControl(object objectInstance);
        
        public virtual TableHeaderCell GetHeaderCellControl()
        {
            TableHeaderCell headerCell = new TableHeaderCell();

            headerCell.Text = ColumnCaption;
            
            if (this.CssClass != null)
                headerCell.CssClass = this.CssClass;

            return headerCell;
        }
        public virtual TableCell GetHeaderCellResponsiveControl()
        {
            TableHeaderCell headerCell = new TableHeaderCell();

            headerCell.Text = ColumnCaption;
           switch(dataHide)
            {
                case ArTable.DataHide.all:
                    headerCell.Attributes.Add("data-hide", "all");
                    break;
                case ArTable.DataHide.phone:
                    headerCell.Attributes.Add("data-hide", "phone");
                    break;
                case ArTable.DataHide.phone_tablet:
                    headerCell.Attributes.Add("data-hide", "phone,tablet");
                    break;
                case ArTable.DataHide.tablet:
                    headerCell.Attributes.Add("data-hide", "tablet");
                    break;
                default:
                    break;
            }
            if (showToggle)
                headerCell.Attributes.Add("data-toggle", "true");


            if (this.CssClass != null)
                headerCell.CssClass = this.CssClass;

            return headerCell;

        }

        protected virtual object GetValue(object objectInstance, string Property)
        {
            var path = Property.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

            object result = objectInstance;

            foreach (string propName in path)
                result = GetPropertyValue(result, propName);

            return result == objectInstance ? null : result;
        }

        private object GetPropertyValue(object objectInstance, string Property)
        {
            if (objectInstance is DataRow)
            {
                System.Data.DataRow row = objectInstance as DataRow;
                if (row.Table.Columns.Contains(Property))
                    return row[Property].ToString();
                return null;
            }
            else
            { 
                try
                {
                    System.Type ObjectType = objectInstance.GetType();
                    System.Reflection.PropertyInfo prop = ObjectType.GetProperty(Property, System.Reflection.BindingFlags.NonPublic| System.Reflection.BindingFlags.Public| System.Reflection.BindingFlags.Instance);

                    if (prop == null)
                        throw new Exception("La classe " + ObjectType.FullName + " non contiene la proprietÓ " + Property + "");

                    return prop.GetValue(objectInstance, null);

                    //return prop.GetGetMethod().Invoke(objectInstance, new object[]{});
                }
                catch { return null; }
            }
        }

        protected string ParseString(object objectInstance, string stringToParse)
        {
            string parsedString = stringToParse;

            var Matches = Regex.Matches(parsedString, @"{[^{]+?}");

            if (Matches != null)
            {
                foreach (var key in Matches.Cast<Match>().Select(x=>x.Value))
                {
                    var property = key.Substring(1, key.Length - 2);
                    var value = GetValue(objectInstance, property);
                    parsedString = parsedString.Replace(key, value != null ?  value.ToString():"");
                }
            }
            /*
            var props = objectInstance.GetType().GetProperties();

            foreach (var p in props)
            {
                string key = "{" + p.Name.Replace("Proxy", "") + "}";
                if (parsedString.Contains(key)) parsedString = parsedString.Replace(key, GetValue(objectInstance, p.Name).ToString());
            }*/

            return parsedString;
        }
        /// <summary>
        /// Restituisce una stringa contenente il valore nel caso di esportazione della cella.
        /// Se la classe non permette l'esportazione dei dati, implementare quest o metodo con return null, es:
        /// public override string GetExportValue(DataRow row)
        /// {
        ///     return null;
        /// }
        /// </summary>
        /// <param name="row">La DataRow sorgente della cella.</param>
        /// <returns>Stringa contenente il valore da rappresentare in  caso diesportazione.</returns>
        public abstract string GetExportValue(object obj);
    }
}