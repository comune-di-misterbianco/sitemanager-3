using System.Web.UI.WebControls;
using System;

namespace NetService.Utility.ArTable
{
    public class ArTextColumn : ArAbstractColumn
    {
        public override bool ExportData
        {
            get { return true; }
        }

        /// <summary>
        /// Taglia il testo nelle singole celle della colonna all'ennesimo carattere.
        /// Se impostato a zero, non effettua il taglio.
        /// Default = 0;
        /// </summary>
        public int CutTextAt { get; set; }
        
        /// <summary>
        /// Taglia il testo a partire dal valore della posizione indicata
        /// </summary>
        public int CutTextFrom { get; set; }

        public enum DataTypes { PlainText, DateTime, Date, Integer, FileSize, Currency, TimeSpan, Time, DateNoLocal, TimeNoLocal, Percent, ReplaceUnderscore, RemoveCssStyle}
        public DataTypes DataType { get; set; } 

        public ArTextColumn(string caption, string key)
            : base(caption, key)
        {
            DataType = DataTypes.PlainText;
        }

        protected string FilterDataType(object value)
        {
            if (value != null)
            {
                if (DataType == DataTypes.FileSize && value is long)
                {
                    return FormatBytes((long)value);
                }

                //Date e Time Localizzati
                if (DataType == DataTypes.Date && value is DateTime)
                    return ((DateTime)value).ToLocalTime().ToShortDateString();

                if (DataType == DataTypes.Time && value is DateTime)
                    return ((DateTime)value).ToLocalTime().ToShortTimeString();

                //Date e Time NON Localizzati
                if(DataType == DataTypes.DateNoLocal && value is DateTime)
                    return ((DateTime)value).ToShortDateString();

                if (DataType == DataTypes.TimeNoLocal && value is DateTime)
                    return ((DateTime)value).ToShortTimeString();

                if (DataType == DataTypes.TimeSpan && value is TimeSpan)
                    return ((TimeSpan)value).ToString();

                if (DataType == DataTypes.DateTime && value is DateTime)
                {                    
                    return string.Format("{0:dd/MM/yyyy HH':'mm}", (DateTime)value);
                }

                if (DataType == DataTypes.Currency && value.ToString() != "0")
                {
                    double importo_euro = double.Parse(value.ToString());
                    return string.Format("{0:C}", importo_euro);
                }

                if (DataType == DataTypes.Percent)
                {
                    double val = double.Parse(value.ToString());
                    return string.Format("{0:P}", val/100);
                }
                if (DataType == DataTypes.ReplaceUnderscore)
                    return value.ToString().Replace("_", " ");

                if (DataType == DataTypes.RemoveCssStyle)
                    return System.Text.RegularExpressions.Regex.Replace(value.ToString(), "(style[^<]*\")", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline);

                return value.ToString().Replace("''","'");
            }
            else return "";
        }

        public string FormatBytes(long bytes)
        {
            const int scale = 1024;
            string[] orders = new string[] { "GB", "MB", "KB", "Bytes" };
            long max = (long)Math.Pow(scale, orders.Length - 1);

            foreach (string order in orders)
            {
                if (bytes > max)
                    return string.Format("{0:##.##} {1}", decimal.Divide(bytes, max), order);

                max /= scale;
            }
            return "0 Bytes";
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            var CellText = FilterCellValue(FilterDataType(GetValue(objectInstance, this.Key)));

            TableCell td = new TableCell();
            if(!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;            
            td.Text = "<span>" + CellText + "</span>";

            return td;
        }

        public override string GetExportValue(object obj)
        {
            var value = GetValue(obj, this.Key);
            return value == null ? string.Empty : FilterCellValue(FilterDataType(value));
        }

        protected virtual string FilterCellValue(string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty;

            if (value.Length > CutTextAt && CutTextAt > 0)
                return value.Substring(0, CutTextAt - 1) + "...";

            if (value.Length > CutTextFrom && CutTextFrom > 0)
                return value.Substring(CutTextFrom);

            return value;
        }
    }
}