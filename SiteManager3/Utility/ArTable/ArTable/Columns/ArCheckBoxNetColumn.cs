using System;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace NetService.Utility.ArTable
{
    public class ArCheckBoxNetColumn : ArAbstractColumn
    {
        public string CheckBoxIDOffset
        {
            get
            {
                return _CheckBoxIDOffset;
            }
            set
            {
                _CheckBoxIDOffset = value;
            }
        }
        private string _CheckBoxIDOffset = "chk";

        public ArCheckBoxNetColumn(string caption, string primaryKeyPropertyName)
            : base(caption, primaryKeyPropertyName)
        {
            //_InnerCheckBox = new CheckBox();
            
        }

        private CheckBox _InnerCheckBox;
        public CheckBox InnerCheckBox
        {
            get 
            {
                return _InnerCheckBox; 
            }
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            
            string CellID = GetValue(objectInstance, this.Key).ToString();

            _InnerCheckBox = new CheckBox();
            _InnerCheckBox.AutoPostBack = true;
            InnerCheckBox.ID = CheckBoxIDOffset + "_" + CellID;
            SetPreRenderEvent();
            SetCheckedChangedEvent();

            TableCell td = new TableCell();
            if(!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;
            HtmlGenericControl span = new HtmlGenericControl("span");
            span.Controls.Add(InnerCheckBox);
            td.Controls.Add(span);

            return td;
        }

        public override bool ExportData
        {
            get { return false; }
        }
        public override string GetExportValue(object obj)
        {
            throw new NotImplementedException();
        }

        public EventHandler PreRenderHandler
        {
            get 
            {
                return _PreRenderHandler;
            }
            set 
            {
                _PreRenderHandler = value;
            }
        }
        private EventHandler _PreRenderHandler;

        public EventHandler CheckedChangedHandler
        {
            get
            {
                return _CheckedChangedHandler;
            }
            set
            {
                _CheckedChangedHandler = value;
            }
        }
        private EventHandler _CheckedChangedHandler;

        private void SetPreRenderEvent()
        {
            if (PreRenderHandler != null)
                _InnerCheckBox.PreRender += PreRenderHandler;
        }

        private void SetCheckedChangedEvent()
        {
            if (CheckedChangedHandler != null)
                _InnerCheckBox.CheckedChanged += CheckedChangedHandler;
        }
    }
}