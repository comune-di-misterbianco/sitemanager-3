﻿using System.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NetService.Utility.ArTable
{
    public class ArGroupTreeColumn : ArAbstractColumn
    {
        public override bool ExportData
        {
            get { return true; }
        }

        /// <summary>
        /// Taglia il etsto nelle singole celle della colonna all'ennesimo carattere.
        /// Se impostato a zero, non effettua il taglio.
        /// Default = 0;
        /// </summary>
        public int CutTextAt { get; set; }

        public ArGroupTreeColumn(string caption, string key, IList<object> groups)
            : base(caption, key)
        {
            _Groups = groups;
        }

        private IList<object> _Groups;

        protected string FilterDataType(object value)
        {
            if (value != null)
            {
                return value.ToString().Replace("''","'");
            }
            else return "";
        }

        private string _Icon;
        public string Icon
        {
            set { _Icon = value; }
            get { return _Icon; }
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            int Depth = int.Parse(FilterCellValue(FilterDataType(GetValue(objectInstance,"Depth"))));

            string Name = FilterCellValue(FilterDataType(GetValue(objectInstance, this.Key)));
           
            TableCell td = new TableCell();
            //if(!string.IsNullOrEmpty(this.CssClass))
            //    td.CssClass = this.CssClass;            



            int RowIndex = _Groups.IndexOf(objectInstance);

            int NextRowDepth = 0;

            bool HasNextRow = _Groups.Count > RowIndex + 1;

            if (HasNextRow)
                NextRowDepth = int.Parse(FilterCellValue(FilterDataType(GetValue(_Groups.ElementAt(RowIndex + 1), "Depth"))));


            string lines = "";

            for (int i = 0; i < Depth; i++)
            {
                string ClassName = " ";

                if (RowIndex == 0) ClassName = " TreeColumn_LinesVoid";
                else
                {
                    if (!HasNextRow) ClassName = " TreeColumn_LinesBottom";
                    else
                    {
                        if (NextRowDepth < Depth && i == 0) ClassName = " TreeColumn_LinesBottom";
                        else
                            if (NextRowDepth == Depth && i == 0) ClassName = " TreeColumn_LinesJoin";

                    }
                }
                lines = "<span class=\"TreeColumn_Lines" + ClassName + "\">&nbsp;</span>" + lines;
            }

            string icon = "";

            icon = "<span class=\"TreeColumn_Icon " + this.Icon + "\">&nbsp;</span>";

            string label = "<span class=\"TreeColumn_Content\">&nbsp;" + Name + "</span>";

            
            td.Text += lines + icon + label;

            return td;


        }

        public override string GetExportValue(object obj)
        {
            var value = GetValue(obj, this.Key);
            return value == null ? string.Empty : FilterCellValue(FilterDataType(value));
        }

        protected virtual string FilterCellValue(string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty;

            if (value.Length > CutTextAt && CutTextAt > 0)
                return value.Substring(0, CutTextAt - 1) + "...";

            return value;
        }
    }
}