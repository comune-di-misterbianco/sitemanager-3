﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;

namespace NetService.Utility.ArTable
{
    public class ArButtonConditionedColumn : ArAbstractColumn
    {
        public string ButtonIDOffset
        {
            get
            {
                return _ButtonIDOffset;
            }
            set
            {
                _ButtonIDOffset = value;
            }
        }
        private string _ButtonIDOffset = "bt";

        public string ButtonText
        {
            get
            {
                return _ButtonText;
            }
            set
            {
                _ButtonText = value;
            }
        }
        private string _ButtonText = "";

        #region Preloader
        /// <summary>
        /// Testo del preloader, qualora si voglia personalizzarlo.
        /// </summary>
        public string PreLoaderText
        {
            get
            {
                if (_PreLoaderText == null)
                    _PreLoaderText = "Operazione in corso ...";
                return _PreLoaderText;
            }
            set
            {
                _PreLoaderText = value;
            }
        }
        private string _PreLoaderText;

        private string PreLoaderContent
        {
            get
            {
             
                string BlockGUI = "<div class=\"ar-preloader\"><img src=\"" + NetCms.Configurations.Paths.AbsoluteRoot + "/cms/css/admin/images/ar-preload.gif" + @"\"" /><h1>" + PreLoaderText + @"</h1></div>";

                return BlockGUI;
            }
        }

        private LiteralControl PreLoadScript
        {
            get
            {
                string script = @"<script type='text/javascript'>
                    
                    $(document).ready(function() {                        

                        $('#" + this.InnerButton.ID + @"').click(function () {
                            $.blockUI({ message: '" + PreLoaderContent + @"' });
                        });
                    }); 

                </script>";

                return new LiteralControl(script);
            }
        }

        /// <summary>
        /// Se settato a true, viene appeso il preload control insieme alla tabella
        /// </summary>
        public bool NeedPreLoader { get; set; }

        #endregion

        public ArButtonConditionedColumn(string caption, string primaryKeyPropertyName, Predicate<object> canExecute, string labelIfCanNotExecute)
            : base(caption, primaryKeyPropertyName)
        {
            this.LabelIfCanNotExecute = labelIfCanNotExecute;
            _CanExecute = canExecute;
        }

        public string LabelIfCanNotExecute { get; set; }


        public Predicate<object> _CanExecute;

        public Predicate<object> WarningExecutePredicate;

        private bool CanExecute(object parameter)
        {
            return _CanExecute == null ? true : _CanExecute(parameter);
        }


        private bool CanExecuteWaring(object parameter)
        {
            return WarningExecutePredicate == null ? true : WarningExecutePredicate(parameter);
        }


        private string _InnerButtonCssClass;

        public string InnerButtonCssClass
        {
            get { return _InnerButtonCssClass; }
            set { _InnerButtonCssClass = value; }
        }


        private Button _InnerButton;
        public Button InnerButton
        {
            get
            {
                return _InnerButton;
            }
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            bool canExecute = this.CanExecute(objectInstance);
            bool canExecuteWarning = this.CanExecuteWaring(objectInstance);

            string CellID = GetValue(objectInstance, this.Key).ToString();

            _InnerButton = new Button();

            InnerButton.ID = ButtonIDOffset + "_" + CellID;
            InnerButton.CssClass = InnerButtonCssClass;

            if (ButtonText.Length == 0)
                InnerButton.Text = Caption;
            else
                InnerButton.Text = ButtonText;

            SetPreRenderEvent();
            SetClickEvent();

            TableCell td = new TableCell();
            if (!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;

            HtmlGenericControl span = new HtmlGenericControl("span");

            if (canExecute)
            {
                span.Controls.Add(InnerButton);
                if (NeedPreLoader)
                {
                    span.Controls.Add(PreLoadScript);
                }
            }
            else
                span.Controls.Add(new LiteralControl(this.LabelIfCanNotExecute));

           
            td.Controls.Add(span);

            return td;
        }

        public override bool ExportData
        {
            get { return false; }
        }
        public override string GetExportValue(object obj)
        {
            throw new NotImplementedException();
        }

        public EventHandler PreRenderHandler
        {
            get
            {
                return _PreRenderHandler;
            }
            set
            {
                _PreRenderHandler = value;
            }
        }
        private EventHandler _PreRenderHandler;

        public EventHandler ClickHandler
        {
            get
            {
                return _ClickHandler;
            }
            set
            {
                _ClickHandler = value;
            }
        }
        private EventHandler _ClickHandler;

        private void SetPreRenderEvent()
        {
            if (PreRenderHandler != null)
                _InnerButton.PreRender += PreRenderHandler;
        }

        private void SetClickEvent()
        {
            if (ClickHandler != null)
                _InnerButton.Click += ClickHandler;
        }
    }
}