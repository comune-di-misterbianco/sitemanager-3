using System;
using System.Web.UI.WebControls;

namespace NetService.Utility.ArTable
{
    public class ArActionColumn : ArAbstractColumn
    {
        public virtual string Href { get; protected set; }
        public Icons Icon { get; protected set; }
        public string AnchorCssClass { get; set; }
        public string ToolTip { get; set; }

        public bool OpenNewTab { get; protected set; }

        public enum Icons
        {
            Custom,
            Ok,
            Enable,
            Disable,
            Edit,
            Delete,
            Details,
            Add,
            Remove,
            Search,
            Table,
            Go
        }
        /// <summary>
        /// ArActionColumn Costruttore
        /// </summary>
        /// <param name="caption">Etichetta della colonna</param>
        /// <param name="icon">Icona da associare al bottone (nel caso in cui si vuole personalizzare l'icona, usare "Custom" e riempire la properiet� "CssClass")</param>
        /// <param name="href">L'indirizzo a cui punta la singola cella. Deve essere una stringa contenente un url o una stringa del tipo "javascript: ;". E' possibile minserire dei marker del tipo {NomePropriet�ActiveRecord}, che verranno opportunamente rimpiazzati a runtime.</param>
        public ArActionColumn(string caption, Icons icon, string href)
            : base(caption, "")
        {
            this.Href = href;
            this.Icon = icon;
        }
        public ArActionColumn(string caption, string key, string href)
            :base(caption,key)
        {
            this.Href = href;
            this.ToolTip = caption;
        }

        /// <summary>
        /// ArActionColumn Costruttore
        /// </summary>
        /// <param name="caption">Etichetta della colonna</param>
        /// <param name="icon">Icona da associare al bottone (nel caso in cui si vuole personalizzare l'icona, usare "Custom" e riempire la properiet� "CssClass")</param>
        /// <param name="href">L'indirizzo a cui punta la singola cella. Deve essere una stringa contenente un url o una stringa del tipo "javascript: ;". E' possibile minserire dei marker del tipo {NomePropriet�ActiveRecord}, che verranno opportunamente rimpiazzati a runtime.</param>
        /// <param name="openNewTab">Se messo a true permette di aprire ci� che indirizza il link in un nuovo tab del browser</param>
        public ArActionColumn(string caption, string key, string href, bool openNewTab)
            : base(caption, key)
        {
            this.Href = href;
            this.ToolTip = caption;
            this.OpenNewTab = openNewTab;
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            if (string.IsNullOrEmpty(Key))
                return GetCellControl(objectInstance, Caption, Href, Icon);
            else               
                return GetCellControl(objectInstance, Caption, Key, Href, Icon);
        }

        protected virtual TableCell GetCellControl(object objectInstance, string label, string href, Icons icon)
        {
            TableCell td = new TableCell();
            if (icon != null)
            {
                if (icon == Icons.Custom)
                {
                    if (!string.IsNullOrEmpty(this.CssClass)) td.CssClass = base.ParseString(objectInstance,this.CssClass);
                }
                else
                    td.CssClass = "action " + icon.ToString().ToLower();
            }

            string anchorCssClassHtml = string.IsNullOrEmpty(AnchorCssClass) ? "" : "class=\"" + this.AnchorCssClass + "\"";
            string anchorTitle = string.IsNullOrEmpty(ToolTip) ? "" : "title=\"" + base.ParseString(objectInstance, ToolTip) + "\"";
            if (OpenNewTab)
                td.Text = "<a target=\"_blank\" " + anchorCssClassHtml + " href=\"" + base.ParseString(objectInstance, href) + "\"><span>" + FilterCellValue(base.ParseString(objectInstance, label).ToString()) + "</span></a>";
            else
                td.Text = "<a "+anchorCssClassHtml+" href=\"" + base.ParseString(objectInstance, href) + "\"><span>" + FilterCellValue(base.ParseString(objectInstance, label).ToString()) + "</span></a>";

            return td;
        }

        protected virtual TableCell GetCellControl(object objectInstance, string label, string key, string href, Icons icon)
        {
            TableCell td = new TableCell();
            if (icon != null)
            {
                if (icon == Icons.Custom)
                {
                    if (!string.IsNullOrEmpty(this.CssClass)) td.CssClass = base.ParseString(objectInstance, this.CssClass);
                }
                else
                    td.CssClass = "action " + icon.ToString().ToLower();
            }

            string anchorCssClassHtml = string.IsNullOrEmpty(AnchorCssClass) ? "" : "class=\"" + this.AnchorCssClass + "\"";
            string anchorTitle = string.IsNullOrEmpty(ToolTip) ? "" : "title=\"" + base.ParseString(objectInstance, ToolTip) + "\"";
            if (OpenNewTab)
                td.Text = "<a target=\"_blank\" " + anchorCssClassHtml + " href=\"" + base.ParseString(objectInstance, href) + "\"><span>" + FilterCellValue(GetValue(objectInstance, Key).ToString()) + "</span></a>";
            else
                td.Text = "<a " + anchorCssClassHtml + " href=\"" + base.ParseString(objectInstance, href) + "\"><span>" + FilterCellValue(GetValue(objectInstance,Key).ToString()) + "</span></a>";

            return td;
        }

        public override bool ExportData
        {
            get { return false; }
        }
        public override string GetExportValue(object obj)
        {
            throw new NotImplementedException();
        }

        public int CutTextAt { get; set; }

        protected virtual string FilterCellValue(string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty;

            if (value.Length > CutTextAt && CutTextAt > 0)
                return value.Substring(0, CutTextAt - 1) + "...";

            return value;
        }
    }
}