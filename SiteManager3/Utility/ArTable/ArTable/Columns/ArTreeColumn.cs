﻿using System;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NetService.Utility.ArTable
{
    public class ArTreeColumn : ArAbstractColumn
    {
        public override bool ExportData
        {
            get { return true; }
        }

        /// <summary>
        /// Taglia il testo nelle singole celle della colonna all'ennesimo carattere.
        /// Se impostato a zero, non effettua il taglio.
        /// Default = 0;
        /// </summary>
        public int CutTextAt { get; set; }

        public ArTreeColumn(string caption, string key, IList<object> objList)
            : base(caption, key)
        {
            _ObjList = objList;
        }

        private IList<object> _ObjList;

        protected string FilterDataType(object value)
        {
            if (value != null)
            {
                return value.ToString().Replace("''","'");
            }
            else return "";
        }

        private string _Icon;
        public string Icon
        {
            set { _Icon = value; }
            get { return _Icon; }
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            int Depth = int.Parse(FilterCellValue(FilterDataType(GetValue(objectInstance,"Depth"))));

            string Name = FilterCellValue(FilterDataType(GetValue(objectInstance, this.Key)));
           
            TableCell td = new TableCell();
         
            int RowIndex = _ObjList.IndexOf(objectInstance);

            int NextRowDepth = 0;

            bool HasNextRow = _ObjList.Count > RowIndex + 1;

            if (HasNextRow)
                NextRowDepth = int.Parse(FilterCellValue(FilterDataType(GetValue(_ObjList.ElementAt(RowIndex + 1), "Depth"))));

            string lines = "";

            for (int i = 0; i < Depth; i++)
            {
                string ClassName = " ";

                if (RowIndex == 0) ClassName = " TreeColumn_LinesVoid";
                else
                {
                    if (!HasNextRow) ClassName = " TreeColumn_LinesBottom";
                    else
                    {
                        if (NextRowDepth < Depth && i == 0) ClassName = " TreeColumn_LinesBottom";
                        else
                            if (NextRowDepth == Depth && i == 0) ClassName = " TreeColumn_LinesJoin";

                    }
                }
                lines = "<span class=\"TreeColumn_Lines" + ClassName + "\">&nbsp;</span>" + lines;
            }

            string icon = "";

            icon = "<span class=\"TreeColumn_Icon " + this.Icon + "\">&nbsp;</span>";

            string label = "<span class=\"TreeColumn_Content\">&nbsp;" + Name + "</span>";

            
            td.Text += lines + icon + label;

            return td;


        }

        public override string GetExportValue(object obj)
        {
            var value = GetValue(obj, this.Key);
            return value == null ? string.Empty : FilterCellValue(FilterDataType(value));
        }

        protected virtual string FilterCellValue(string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty;

            if (value.Length > CutTextAt && CutTextAt > 0)
                return value.Substring(0, CutTextAt - 1) + "...";

            return value;
        }
    }
}
