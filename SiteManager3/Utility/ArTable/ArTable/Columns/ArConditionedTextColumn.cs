﻿using System;
using System.Web.UI.WebControls;

namespace NetService.Utility.ArTable
{
    public class ArConditionedTextColumn : ArAbstractColumn
    {
        public Icons Icon { get; protected set; }
        public string AnchorCssClass { get; set; }
        public string ToolTip { get; set; }
        public Predicate<object> _CanExecute;
        public string LabelIfCanNotExecute { get; set; }
        public string LabelIfCanExecute { get; set; }

        public enum Icons
        {
            Custom,
            Ok,
            Enable,
            Disable,
            Edit,
            Delete,
            Details,
            Add,
            Remove,
            Search,
            Table,
            Go,
            Stop
        }
        /// <summary>
        /// ArActionColumn Costruttore
        /// </summary>
        /// <param name="caption">Etichetta della colonna</param>
        /// <param name="icon">Icona da associare al bottone (nel caso in cui si vuole personalizzare l'icona, usare "Custom" e riempire la properietà "CssClass")</param>
        /// <param name="href">L'indirizzo a cui punta la singola cella. Deve essere una stringa contenente un url o una stringa del tipo "javascript: ;". E' possibile minserire dei marker del tipo {NomeProprietàActiveRecord}, che verranno opportunamente rimpiazzati a runtime.</param>
        public ArConditionedTextColumn(string caption, Icons icon, Predicate<object> canExecute, string labelIfCanNotExecute, string labelIfCanExecute = "")
            : base(caption, "")
        {
            this.Icon = icon;
            _CanExecute = canExecute;

            this.LabelIfCanNotExecute = labelIfCanNotExecute;
            this.LabelIfCanExecute = labelIfCanExecute;
        }
      
        public ArConditionedTextColumn(string caption, string key, Predicate<object> canExecute, string labelIfCanNotExecute)
            : base(caption, key)
        { 
            this.ToolTip = caption;
            _CanExecute = canExecute;
            this.LabelIfCanNotExecute = labelIfCanNotExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _CanExecute == null ? true : _CanExecute(parameter);
        }


        public override TableCell GetCellControl(object objectInstance)
        {
            if (string.IsNullOrEmpty(Key))
            {
                return GetCellControl(objectInstance, Caption, Icon);
            }
            else
            {
                return GetCellControl(objectInstance, Caption, Key, Icon);
            }
        }

        protected virtual TableCell GetCellControl(object objectInstance, string label, Icons icon)
        {
            bool canExecute = this.CanExecute(objectInstance);

            if (!canExecute)
                icon = Icons.Stop;

            TableCell td = new TableCell();
            if (icon != null)
            {
                if (icon == Icons.Custom)
                {
                    if (!string.IsNullOrEmpty(this.CssClass)) td.CssClass = this.CssClass;
                }
                else
                    td.CssClass = "action " + icon.ToString().ToLower();
            }

            string anchorCssClassHtml = string.IsNullOrEmpty(AnchorCssClass) ? "" : "class=\"" + this.AnchorCssClass + "\"";
            string anchorTitle = string.IsNullOrEmpty(ToolTip) ? "" : "title=\"" + base.ParseString(objectInstance, ToolTip) + "\"";

            //if (CanExecute(objectInstance))
            if (canExecute)
                td.Text = ((this.LabelIfCanExecute.Length == 0) ? base.ParseString(objectInstance, label) : this.LabelIfCanExecute);
            else
                td.Text = this.LabelIfCanNotExecute;
            return td;
        }

        protected virtual TableCell GetCellControl(object objectInstance, string label, string key, Icons icon)
        {
            bool canExecute = this.CanExecute(objectInstance);

            if (!canExecute)
                icon = Icons.Stop;

            TableCell td = new TableCell();
            if (icon != null)
            {
                if (icon == Icons.Custom)
                {
                    if (!string.IsNullOrEmpty(this.CssClass)) td.CssClass = this.CssClass;
                }
                else
                    td.CssClass = "action " + icon.ToString().ToLower();
            }

            string anchorCssClassHtml = string.IsNullOrEmpty(AnchorCssClass) ? "" : "class=\"" + this.AnchorCssClass + "\"";
            string anchorTitle = string.IsNullOrEmpty(ToolTip) ? "" : "title=\"" + base.ParseString(objectInstance, ToolTip) + "\"";

            //if (CanExecute(objectInstance))
            if (canExecute)
                td.Text = "<span>" + GetValue(objectInstance, Key) + "</span>";
            else
                td.Text = "<span>" + this.LabelIfCanNotExecute + "</span>";
            return td;
        }

        public override bool ExportData
        {
            get { return false; }
        }
        public override string GetExportValue(object obj)
        {
            throw new NotImplementedException();
        }
    }
}