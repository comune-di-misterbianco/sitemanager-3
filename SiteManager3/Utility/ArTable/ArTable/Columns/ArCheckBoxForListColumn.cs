﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;

namespace NetService.Utility.ArTable
{
    /// <summary>
    /// Classe creata per visualizzare una checkbox selezionabile accanto ai record con un numero progressivo per value in modo da 
    /// selezionare l'elemento in funzione dell'indice che possiede nella lista che lo contiene.
    /// </summary>
    public class ArCheckBoxForListColumn : ArAbstractColumn
    {
        public string CheckBoxIDOffset
        {
            get
            {
                return _CheckBoxIDOffset;
            }
            set
            {
                _CheckBoxIDOffset = value;
            }
        }
        private string _CheckBoxIDOffset = "chk";

        public bool AutoPostBack
        {
            get
            {
                return _AutoPostBack;
            }
            set
            {
                _AutoPostBack = value;
            }
        }
        private bool _AutoPostBack = false;

        private IList _Lista;

        public ArCheckBoxForListColumn(string caption, string primaryKeyPropertyName, IList lista)
            : base(caption, primaryKeyPropertyName)
        {
            //_InnerCheckBox = new CheckBox();
            _Lista = lista;
        }

        private CheckBox _InnerCheckBox;
        public CheckBox InnerCheckBox
        {
            get 
            {
                return _InnerCheckBox; 
            }
        }

        public override TableCell GetCellControl(object objectInstance)
        {

            string CellID = _Lista.IndexOf(objectInstance).ToString();

            _InnerCheckBox = new CheckBox();
            _InnerCheckBox.AutoPostBack = AutoPostBack;
            InnerCheckBox.ID = CheckBoxIDOffset + "_" + CellID;

            SetPreRenderEvent();
            SetCheckedChangedEvent();

            TableCell td = new TableCell();
            if(!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;
            HtmlGenericControl span = new HtmlGenericControl("span");
            span.Controls.Add(InnerCheckBox);
            td.Controls.Add(span);

            return td;
        }

        public override bool ExportData
        {
            get { return false; }
        }
        public override string GetExportValue(object obj)
        {
            throw new NotImplementedException();
        }

        public EventHandler PreRenderHandler
        {
            get 
            {
                return _PreRenderHandler;
            }
            set 
            {
                _PreRenderHandler = value;
            }
        }
        private EventHandler _PreRenderHandler;

        public EventHandler CheckedChangedHandler
        {
            get
            {
                return _CheckedChangedHandler;
            }
            set
            {
                _CheckedChangedHandler = value;
            }
        }
        private EventHandler _CheckedChangedHandler;

        private void SetPreRenderEvent()
        {
            if (PreRenderHandler != null)
                _InnerCheckBox.PreRender += PreRenderHandler;
        }

        private void SetCheckedChangedEvent()
        {
            if (CheckedChangedHandler != null)
                _InnerCheckBox.CheckedChanged += CheckedChangedHandler;
        }
    }
}