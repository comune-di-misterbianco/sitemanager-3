using System;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;

namespace NetService.Utility.ArTable
{
    public class ArButtonColumn : ArAbstractColumn
    {
        public string ButtonIDOffset
        {
            get
            {
                return _ButtonIDOffset;
            }
            set
            {
                _ButtonIDOffset = value;
            }
        }
        private string _ButtonIDOffset = "bt";

        public string ButtonText
        {
            get
            {
                return _ButtonText;
            }
            set
            {
                _ButtonText = value;
            }
        }
        private string _ButtonText = "";

        #region Preloader
        /// <summary>
        /// Testo del preloader, qualora si voglia personalizzarlo.
        /// </summary>
        public string PreLoaderText
        {
            get
            {
                if(_PreLoaderText == null)
                    _PreLoaderText = "Operazione in corso ...";
                return _PreLoaderText;
            }
            set
            {
                _PreLoaderText = value;
            }
        }
        private string _PreLoaderText;

        private string PreLoaderContent
        {
            get
            {
                //if (_PreLoadControl == null)
                //{
                //    _PreLoadControl = new WebControl(HtmlTextWriterTag.Div);
                //    _PreLoadControl.ID = this.Key + "-preLoader";
                //    _PreLoadControl.CssClass = "ar-preloader";
                //    _PreLoadControl.Attributes.Add("style", "display:none;");

                //    WebControl img = new WebControl(HtmlTextWriterTag.Img);
                //    img.Attributes.Add("src", NetCms.Configurations.Paths.AbsoluteRoot + "/cms/css/admin/images/ar-preload.gif");

                //    _PreLoadControl.Controls.Add(img);

                //    WebControl h1 = new WebControl(HtmlTextWriterTag.H1);
                //    string PreloadMessage = "Operazione in corso ...";
                //    if (!string.IsNullOrEmpty(PreLoaderText))
                //        PreloadMessage = PreLoaderText;
                //    h1.Controls.Add(new LiteralControl(PreloadMessage));

                //    _PreLoadControl.Controls.Add(h1);
                //}

                //return _PreLoadControl;
                string BlockGUI = "<div class=\"ar-preloader\"><img src=\"" + NetCms.Configurations.Paths.AbsoluteRoot + "/cms/css/admin/images/ar-preload.gif" + @"\"" /><h1>" + PreLoaderText + @"</h1></div>";

                return BlockGUI;
            }
        }

        private LiteralControl PreLoadScript
        {
            get
            {
                string script = @"<script type='text/javascript'>
                    
                    $(document).ready(function() {                        

                        $('#" + this.InnerButton.ID + @"').click(function () {
                            $.blockUI({ message: '" + PreLoaderContent + @"' });
                        });
                    }); 

                </script>";

                return new LiteralControl(script);
            }
        }

        /// <summary>
        /// Se settato a true, viene appeso il preload control insieme alla tabella
        /// </summary>
        public bool NeedPreLoader { get; set; }

        #endregion

        public ArButtonColumn(string caption, string primaryKeyPropertyName)
            : base(caption, primaryKeyPropertyName)
        {
            //_InnerCheckBox = new CheckBox();
            
        }

        private string  _InnerButtonCssClass;

        public string InnerButtonCssClass
        {
            get { return _InnerButtonCssClass; }
            set { _InnerButtonCssClass = value; }
        }


        private Button _InnerButton;
        public Button InnerButton
        {
            get 
            {                
                return _InnerButton; 
            }
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            
            string CellID = GetValue(objectInstance, this.Key).ToString();

            _InnerButton = new Button();

            InnerButton.ID = ButtonIDOffset + "_" + CellID;
            InnerButton.CssClass = InnerButtonCssClass;

            if (ButtonText.Length == 0)
                InnerButton.Text = Caption;
            else
                InnerButton.Text = ButtonText;

            if(!string.IsNullOrEmpty(SetOnClientClick))
                InnerButton.OnClientClick = SetOnClientClick;

            SetPreRenderEvent();
            SetClickEvent();

            TableCell td = new TableCell();
            if(!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;
            HtmlGenericControl span = new HtmlGenericControl("span");
            span.Controls.Add(InnerButton);
            if (NeedPreLoader)
            {
                span.Controls.Add(PreLoadScript);
            }
            td.Controls.Add(span);

            return td;
        }

        public override bool ExportData
        {
            get { return false; }
        }
        public override string GetExportValue(object obj)
        {
            throw new NotImplementedException();
        }

        public EventHandler PreRenderHandler
        {
            get 
            {
                return _PreRenderHandler;
            }
            set 
            {
                _PreRenderHandler = value;
            }
        }
        private EventHandler _PreRenderHandler;

        public EventHandler ClickHandler
        {
            get
            {
                return _ClickHandler;
            }
            set
            {
                _ClickHandler = value;
            }
        }
        private EventHandler _ClickHandler;

        private void SetPreRenderEvent()
        {
            if (PreRenderHandler != null)
                _InnerButton.PreRender += PreRenderHandler;
        }

        private void SetClickEvent()
        {
            if (ClickHandler != null)
                _InnerButton.Click += ClickHandler;
        }
    
        public string SetOnClientClick
        {
            set;
            get;
        }
    }
}