using System;
using System.Web.UI.WebControls;
using System.Linq;

namespace NetService.Utility.ArTable
{
    /// <summary>
    /// Colonna Checkbox per ArTable con l'id del controllo uguale al valore della propriet� CheckBoxIDOffset e valore uguale a quello della propriet� 
    /// primaryKeyPropertyName passata nel costrutture.
    /// Viene usato per associare liste di oggetti identificati con l'id ad una entit� specifica, solo in fase di inserimento. 
    /// </summary>
    public class ArCheckBoxColumnWithValue : ArAbstractColumn
    {
        public string CheckBoxIDOffset
        {
            get
            {
                return _CheckBoxIDOffset;
            }
            set
            {
                _CheckBoxIDOffset = value;
            }
        }
        private string _CheckBoxIDOffset = "chk";

        public ArCheckBoxColumnWithValue(string caption, string primaryKeyPropertyName)
            : base(caption, primaryKeyPropertyName)
        {
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            
            string CellID = GetValue(objectInstance, this.Key).ToString();
            string Stato = "";
            NetService.Utility.Common.RequestVariable valore = new NetService.Utility.Common.RequestVariable(CheckBoxIDOffset, Common.RequestVariable.RequestType.Form);
            if (valore.IsValidString)
            {
                string[] valori = valore.StringValue.Split(',');
                if (valori.Contains(CellID))
                    Stato = @" checked=""checked""";                
            }
            string checkModel = @"<input type=""checkbox"" name=""{0}""  id=""{0}"" value=""{1}"" {2}>";
            

            TableCell td = new TableCell();
            if(!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;
            td.Text = "<span>" + string.Format(checkModel, CheckBoxIDOffset, CellID, Stato) + "</span>";

            return td;
        }

        public override bool ExportData
        {
            get { return false; }
        }
        public override string GetExportValue(object obj)
        {
            throw new NotImplementedException();
        }
    }
}