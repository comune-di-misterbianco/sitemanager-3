using System.Collections.Generic;
using System.Web.UI.WebControls;
using System;

namespace NetService.Utility.ArTable
{
    public class ArOptionsColumn : ArAbstractColumn
    {
        public Dictionary<int, string> AssociatedLabels
        {
            get
            {
                return _AssociatedLabels ?? (_AssociatedLabels = new Dictionary<int, string>());
            }
        }
        private Dictionary<int, string> _AssociatedLabels;

        public ArOptionsColumn(string caption, string key)
            : base(caption, key)
        {
        }
        
        public ArOptionsColumn(string caption, string key, string[] options)
            : base(caption, key)
        {
            for (int index = 0; index < options.Length; index++)
            {
                AssociatedLabels.Add(index, options[index]);
            }
        }

        public override TableCell GetCellControl(object objectInstance)
        {
            TableCell td = new TableCell();
            if(!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;

            object value = GetValue(objectInstance, this.Key);

            int index;
            if (value is bool)
            {
                bool boolvalue = (bool)value;
                index = Convert.ToInt32(boolvalue);
            }
            else
                index = (int)value;

            //int value = (int) GetValue(objectInstance, this.Key);
            td.Text = "<span>" + AssociatedLabels[index] +"</span>";

            return td;
        }
        public override bool ExportData
        {
            get { return true; }
        }
        public override string GetExportValue(object obj)
        {
            object value = GetValue(obj, this.Key);

            int valueIndex;
            if (value is bool)
            {
                bool boolvalue = (bool)value;
                valueIndex = Convert.ToInt32(boolvalue);
            }
            else
                valueIndex = (int)value;
            return AssociatedLabels[valueIndex];
        }
    }
}