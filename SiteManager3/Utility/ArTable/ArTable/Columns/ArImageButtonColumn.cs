using System;
using System.Web;
using System.IO;
using System.Reflection;
using System.Web.UI;
using System.Resources;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace NetService.Utility.ArTable
{
    public class ArImageButtonColumn : ArAbstractColumn
    {
        public string ButtonIDOffset
        {
            get
            {
                return _ButtonIDOffset;
            }
            set
            {
                _ButtonIDOffset = value;
            }
        }
        private string _ButtonIDOffset = "bt";

        public ArImageButtonColumn(string caption, string primaryKeyPropertyName)
            : base(caption, primaryKeyPropertyName)
        {
            //_InnerCheckBox = new CheckBox();
            
        }

        private ImageButton _InnerButton;
        public ImageButton InnerButton
        {
            get 
            {
                return _InnerButton; 
            }
        }

   
        public override TableCell GetCellControl(object objectInstance)
        {
            
            string CellID = GetValue(objectInstance, this.Key).ToString();
    
            _InnerButton = new ImageButton();

            InnerButton.ID = ButtonIDOffset + "_" + CellID;
            SetPreRenderEvent();
            SetClickEvent();

            TableCell td = new TableCell();
            if(!string.IsNullOrEmpty(this.CssClass))
                td.CssClass = this.CssClass;
            HtmlGenericControl span = new HtmlGenericControl("span");
            span.Controls.Add(InnerButton);
            td.Controls.Add(span);

            return td;
        }

        //public void SetActive()
        //{
        //    _InnerButton.ImageUrl = _InnerButton.Page.ClientScript.GetWebResourceUrl(this.GetType(), "ValidatedFields.images.ck_on.jpg");
        //}

        //public void SetInActive()
        //{
        //    _InnerButton.ImageUrl = _InnerButton.Page.ClientScript.GetWebResourceUrl(this.GetType(), "ValidatedFields.images.ck_off.gif");
        //}

        public override bool ExportData
        {
            get { return false; }
        }
        public override string GetExportValue(object obj)
        {
            throw new NotImplementedException();
        }

        public EventHandler PreRenderHandler
        {
            get 
            {
                return _PreRenderHandler;
            }
            set 
            {
                _PreRenderHandler = value;
            }
        }
        private EventHandler _PreRenderHandler;

        public System.Web.UI.ImageClickEventHandler ClickHandler
        {
            get
            {
                return _ClickHandler;
            }
            set
            {
                _ClickHandler = value;
            }
        }
        private System.Web.UI.ImageClickEventHandler _ClickHandler;

        private void SetPreRenderEvent()
        {
            if (PreRenderHandler != null)
                _InnerButton.PreRender += PreRenderHandler;
        }

        private void SetClickEvent()
        {
            if (ClickHandler != null)
                _InnerButton.Click += ClickHandler;
        }
    }
}