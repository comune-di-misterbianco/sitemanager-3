using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Reflection;
using NetUtility.Report.Exporter;
using NetService.Utility.Controls;
using NetService.Utility.RecordsFinder;
using NetService.Utility.ValidatedFields;
using NetService.Utility.UI;
using GenericDAO.DAO;

namespace NetService.Utility.ArTable
{
    public class ArTable:WebControl
    {
        protected List<ArAbstractColumn> Columns
        {
            get
            {
                return _Columns;
            }
        }
        protected List<ArAbstractColumn> _Columns;

        private bool _EnablePagination = false;
        public bool EnablePagination
        {
            get { return _EnablePagination; }
            set { _EnablePagination = value; }
        }


        private bool _EnableNoMsgRecordTable = false;
        public bool EnableNoMsgRecordTable
        {
            get { return _EnableNoMsgRecordTable; }
            set { _EnableNoMsgRecordTable = value; }
        }

        private bool _NoSearchParameter = false;
        public bool NoSearchParameter
        {
            get { return _NoSearchParameter; }
            set { _NoSearchParameter = value; }
        }

        public string[] CustomSearchArray
        {
            get
            {
                return _CustomSearchArray;
            }
            set
            {
                _CustomSearchArray = value;
            }
        }
        private string[] _CustomSearchArray;

        //public int ResultsCount { get; private set; }

        public string NoRecordMsg
        {
            get
            {
                if (_NoRecordMsg == null)
                    _NoRecordMsg = "Nessun record trovato";
                return _NoRecordMsg;
            }
            set { _NoRecordMsg = value; }
        }
        private string _NoRecordMsg;

        public bool PrintExportLinks
        {
            get
            {
                return _PrintExportLinks;
            }
            set
            {
                _PrintExportLinks = value;
            }
        }
        private bool _PrintExportLinks;        


        public bool ResponsiveTable
        {
            get
            {
                return _ResponsiveTable;
            }
            set
            {
                _ResponsiveTable = value;
            }
        }
        private bool _ResponsiveTable;

        public bool ResponsiveTableBordered
        {
            get
            {
                return _ResponsiveTableBordered;
            }
            set
            {
                _ResponsiveTableBordered = value;
            }
        }
        private bool _ResponsiveTableBordered;


        public bool ResponsiveTableStriped
        {
            get
            {
                return _ResponsiveTableStriped;
            }
            set
            {
                _ResponsiveTableStriped = value;
            }
        }
        private bool _ResponsiveTableStriped;


        public enum DataHide
        {
            None,
            phone,
            tablet,
            phone_tablet,
            all
           
        }
        public string MainTitle { get; set; }

        public string InnerTableCssClass
        {
            get
            {
                if (ResponsiveTable)
                    return _InnerTableCssClass + " footable table toggle-circle table-hover";

                if (ResponsiveTableBordered)
                    return _InnerTableCssClass + " footable table toggle-circle table-hover table-bordered";

                if (ResponsiveTableStriped)
                    return _InnerTableCssClass + " footable table toggle-circle table-hover table-striped";

                return _InnerTableCssClass;
                     
                //return this.ResponsiveTable ? _InnerTableCssClass + " footable table toggle-circle table-hover table-bordered" : _InnerTableCssClass;
            }
            set
            {
                _InnerTableCssClass = value;
            }
        }
        private string _InnerTableCssClass;

        public string TableExportTemplate
        {
            get
            {
                return _TableExportTemplate;
            }
            set
            {
                _TableExportTemplate = value;
            }
        }
        private string _TableExportTemplate;

        /// <summary>
        /// Queste Righe vengono appese alla fine della tabella prima della paginazione.
        /// </summary>
        public List<TableRow> RowsToAppend
        {
            get
            {
                if (_RowsToAppend == null)
                {
                    _RowsToAppend = new List<TableRow>();
                }
                return _RowsToAppend;
            }
        }
        private List<TableRow> _RowsToAppend;

        /// <summary>
        /// Questo metodo serve ad abilitare la possibilit� di stampare i link di esportazione.
        /// solitamente va inizializzato con questa stringa
        /// myArTableInstance.ExportLinksMethodName = addExportLinks;
        /// </summary>
        //public string ExportLinksMethodName { get; set; }

        /// <summary>
        /// Questo metodo serve ad abilitare la possibilit� di stampare i link di esportazione.
        /// solitamente va inizializzato con questo codice
        /// myArTableInstance.ExportLinksClassType = typeof(G2Core.DataTableConvert.DataTableExporterUtility);
        /// </summary>
        //public Type ExportLinksClassType { get; set; }

        public DataTable ExportTable
        {
            get
            {
                if (_ExportTable == null)
                {
                    _ExportTable = new DataTable();

                    foreach (var col in Columns)
                    {
                        if (col.ExportData)
                        {
                            DataColumn dcol = new DataColumn(col.UniqueKey);
                            dcol.DataType = typeof(System.String);
                            dcol.Caption = col.Caption;
                            _ExportTable.Columns.Add(dcol);
                        }
                    }
                }
                return _ExportTable;
            }
        }
        private DataTable _ExportTable;

        public IEnumerable Records 
		{ 
			get
			{
				return _Records;
			} 
			set
			{
				_Records = value;
			}
		}
        private IEnumerable _Records;

        protected bool BuildExportTable
        {
            get
            {
                return (this.PrintExportLinks &&
                        this.ExportController.ExportRequest.HasValidValue &&
                        this.ExportController.ExportRequest.Value == 1);
            }
        }

        /**
         * Settando questa propriet�, verr� stampato il titolo della tabella sopra la stessa nel file esportato.
         * Il template deve contenere il place-holder {title}
         */
        public string TitleForExport
        {
            get
            {
                return _TitleForExport;
            }
            set
            {
                _TitleForExport = value;
            }
        }
        private string _TitleForExport;

        public NetService.Utility.ExportControls ExportController
        {
            get
            {
                if (_ExportController == null)
                {
                    _ExportController = new NetService.Utility.ExportControls("Table" + this.TableIndex + "ExportData");
                    _ExportController.TemplateFileName = this.TableExportTemplate;
                    _ExportController.ID = "Table" + this.TableIndex + "Export";
                    _ExportController.ExportRequestEvent += new NetService.Utility.ExportControls.AskExportEventHandler(ExportController_ExportRequestEvent);
                }
                return _ExportController;
            }
        }

        void ExportController_ExportRequestEvent(object sender, EventArgs e)
        {
            
            _ExportController.ExportFormatsEnabled.Add(ExportFormats.Pdf);
            _ExportController.ExportFormatsEnabled.Add(ExportFormats.Csv);

            try
            {
                foreach (var axtColumn in this.Columns)
                {
                    if (this.ExportTable.Columns.Contains(axtColumn.UniqueKey))
                    {
                        string caption = axtColumn.Caption;
                        if (this.ExportTable.Columns.Contains(axtColumn.Caption))
                        {
                            caption += "_";
                            int i = 1;
                            while (this.ExportTable.Columns.Contains(caption + i)) i++;
                            caption = caption + i;
                        }
                        this.ExportTable.Columns[axtColumn.UniqueKey].ColumnName = caption;
                    }
                }
                this.ExportController.DataTable = this.ExportTable;

                if (this.TitleForExport.Length > 0)
                {
                    Dictionary<string, object> dettagli = new Dictionary<string, object>();
                    dettagli.Add("title", this.TitleForExport);
                    foreach (KeyValuePair<string, object> dict in dettagli)
                        this.ExportController.CustomPlaceholderValues.Add(dict.Key.ToString(), dict.Value);

                }
            }
            catch { }
        }
        private NetService.Utility.ExportControls _ExportController;

        //public NetService.Utility.Common.InputField ExportRequest
        //{
        //    get
        //    {
        //        if (_ExportRequest == null)
        //        {
        //            _ExportRequest = new NetService.Utility.Common.InputField("esportRequest" + this.TableIndex);
        //        }
        //        return _ExportRequest;
        //    }
        //}
        //private NetService.Utility.Common.InputField _ExportRequest;

        /// <summary>
        /// Rappresenta l'indice della tablella all'interno della pagina web (utile quando si mettono pi� tabellle nella stessa pagina)
        /// </summary>
        private const string TableIndexKey = "ArTableCountKey";
        public int? TableIndex
        {
            get
            {
                if (_TableIndex == null)
                {
                    int index = 0;
                    var pageItems = HttpContext.Current.Items;

                    if (pageItems.Contains(TableIndexKey))
                        int.TryParse(pageItems[TableIndexKey].ToString(), out index);
                    
                    
                    pageItems[TableIndexKey] = ++index;
                    
                    _TableIndex = new int?(index);
                }
                return _TableIndex;
            }
        }
        private int? _TableIndex;

        #region Costruttori

        public ArTable(IEnumerable records):base(HtmlTextWriterTag.Div)
        {
           this.Records = records;
           this._Columns = new List<ArAbstractColumn>();
           this.PrintExportLinks = false;
           this.TableExportTemplate = "template-table.xml";
           this.TitleForExport = "";
            this.ResponsiveTable = false;
        }

        public ArTable()
            : base(HtmlTextWriterTag.Div)
        {
            //this.Records = records;
            this._Columns = new List<ArAbstractColumn>();
            this.PrintExportLinks = false;
            this.TableExportTemplate = "template-table.xml";
            this.TitleForExport = "";
            this.ResponsiveTable = false;
        }
        #endregion

        
        private void AddFieldsRecursive(SearchParameter p)
        {
            if (p is VfSearchParameter)
                SearchFields.Add((p as VfSearchParameter).Field);

            if (p.Value is SearchParameter[])
                foreach (var param in (p.Value as SearchParameter[]))
                    AddFieldsRecursive(param);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            
            if (EnablePagination)
            {
                Initialize();
                PagesControl.CssClass = "pagination";                
                PagesControl.BaseLink = BuildPaginationBaseAddress();
                PagesControl.PaginationKey = this.PaginationKey;
                PagesControl.CurrentPage = PaginationRequest.IntValue;
                PagesControl.GoToFirstPageOnSearch = this.GoToFirstPageOnSearch;
            }

            if (this.Records != null && !IsEmpty(this.Records))
            {
                Table table = new Table();

                if (!string.IsNullOrEmpty(this.InnerTableCssClass))
                    table.CssClass = InnerTableCssClass;

                if (ResponsiveTable == true || ResponsiveTableBordered == true || ResponsiveTableStriped == true)
                    this.CssClass = "table-responsive";

                this.Controls.Add(table);

                table.Attributes["border"] = "0";
                table.Attributes["cellspacing"] = "0";
                table.Attributes["cellpadding"] = "0";

                if (!string.IsNullOrEmpty(this.MainTitle))
                    table.Controls.Add(this.BuildMainRow());
                table.Controls.Add(this.BuildHeadRow());
                table.Rows[0].TableSection = TableRowSection.TableHeader;
                bool alternate = false;
                foreach (object record in Records)
                {
                    TableRow row = this.BuildRow(record);
                    row.TableSection = TableRowSection.TableBody;
                    if (row != null)
                    {
                        if (alternate)
                        {
                            row.Attributes["class"] += " alternate";
                            row.Attributes["class"] = row.Attributes["class"].Trim();
                        }
                        alternate = !alternate;
                        table.Controls.Add(row);

                        if (this.BuildExportTable) BuildExportRow(record);
                    }
                }
                foreach (TableRow row in this.RowsToAppend)
                    table.Controls.Add(row);
                
                if (EnablePagination)
                    this.Controls.Add(PagesControl);

                if (PrintExportLinks)                
                    this.Controls.Add(this.ExportController);                                   
            }
            else
            {
                if (EnableNoMsgRecordTable)
                {
                    Table tableNoMSGRecord = new Table();

                    if (!string.IsNullOrEmpty(this.InnerTableCssClass))
                        tableNoMSGRecord.CssClass = InnerTableCssClass + " NoMsgRecordsTable";

                    this.Controls.Add(tableNoMSGRecord);

                    tableNoMSGRecord.Attributes["border"] = "0";
                    tableNoMSGRecord.Attributes["cellspacing"] = "0";
                    tableNoMSGRecord.Attributes["cellpadding"] = "0";
                    if (!string.IsNullOrEmpty(this.MainTitle))
                        tableNoMSGRecord.Controls.Add(this.BuildMainRow());
                    tableNoMSGRecord.Controls.Add(this.BuildHeadRow());
                    tableNoMSGRecord.Rows[0].TableSection = TableRowSection.TableHeader;

                    TableRow row = new TableRow();
                    row.TableSection = TableRowSection.TableBody;

                    TableCell rowCell = new TableCell();
                    rowCell.CssClass = "norecord ";
                    rowCell.Text = this.NoRecordMsg;
                    rowCell.Attributes.Add("style", "text-align:center");

                    row.Cells.Add(rowCell);
                    tableNoMSGRecord.Controls.Add(row);
                }
                else
                    this.Controls.Add(new LiteralControl(NoRecordMsg));
            }
        }
      
        private TableRow BuildRow(object record)
        {
            TableRow trControl = new TableRow();

            foreach (var column in this.Columns)
            {
                TableCell cell = column.GetCellControl(record);
                trControl.Cells.Add(cell);
            }

            return trControl;
        }

        private TableHeaderRow BuildMainRow()
        {
            TableHeaderRow trControl = new TableHeaderRow();
            trControl.CssClass = "Overtitle";
            trControl.TableSection = TableRowSection.TableHeader;
            trControl.Controls.Add(new TableHeaderCell() { Text = this.MainTitle, ColumnSpan = this.Columns.Count });

            return trControl;
        }  
        private TableHeaderRow BuildHeadRow()
        {
            TableHeaderRow trControl = new TableHeaderRow();
            trControl.TableSection = TableRowSection.TableHeader;

            foreach (var column in this.Columns)
            {
                if (!ResponsiveTable)
                    trControl.Controls.Add(column.GetHeaderCellControl());
                else                
                    trControl.Controls.Add(column.GetHeaderCellResponsiveControl());                
            }
            return trControl;
        }
        private void BuildExportRow(object obj)
        {
            DataRow rowToAppend = this.ExportTable.NewRow();

            bool skip = false;
            foreach (var axtColumn in this.Columns)
            {
                if (this.ExportTable.Columns.Contains(axtColumn.UniqueKey))
                {
                    string value = axtColumn.GetExportValue(obj);

                    rowToAppend[axtColumn.UniqueKey] = (axtColumn.ExportData && value != null) ? value : string.Empty;
                }
            }
            if (!skip)
                this.ExportTable.Rows.Add(rowToAppend);
        }

        public void AddColumn(ArAbstractColumn column)
        {
            this.Columns.Add(column);
        }
        public void AddColumns( params ArAbstractColumn[] columns)
        {
            foreach (var column in columns)
                if (column != null) 
                    this.Columns.Add(column);
        }

        #region Paginazione

        public bool GoToFirstPageOnSearch { get; set; }

        public NetService.Utility.Common.InputField CurrentPageField
        {
            get
            {
                return _CurrentPageField ?? (_CurrentPageField = new NetService.Utility.Common.InputField(PaginationKey) { AutoLoadValueAfterPostback = true });
            }
        }
        private NetService.Utility.Common.InputField _CurrentPageField;

        public int RecordPerPagina { get; set; }

        public PaginationHandler PagesControl { get; set; }

        public IList<SearchParameter> SearchParameter { get; set; }

        private string paginationKey = "";
        public string PaginationKey
        {
            get
            {
                if (paginationKey=="")
                    paginationKey = "tp" + TableIndex.Value;
                return paginationKey;
            }
            set
            {
                paginationKey = value;
            }
        }

        public NetService.Utility.Common.RequestVariable PaginationRequest
        {
            get
            {
                return this.CurrentPageField.RequestVariable;
            }
        }

        private void Initialize()
        {
            if (SearchFields == null)
                SearchFields = new List<ValidatedFields.VfGeneric>();
            if (SearchParameter != null)
                foreach (var param in SearchParameter) AddFieldsRecursive(param);
        }

        public string BuildPaginationBaseAddress()
        {
            var addr = BuildBaseAddress(true);
            if (addr.EndsWith("?"))
                return addr + this.PaginationKey + "={0}";

            return string.Join("&", addr, this.PaginationKey + "={0}");
        }

        //Il bool isForPagination � stato inserito per risolvere un bug che comportava l'azzeramento della paginazione quando al click 
        //su "Mostra opzioni di esportazione" mentre ci si trovava in una pagina diversa dalla prima. Il problema sorge perch� il metodo
        //GetLegacyQueryString() elimina l'informazione della pagina e questo � corretto se stiamo costruendo il link di paginazione,
        //ma non va bene se siamo nel BuildBaseAddress() da associare al BaseUrl dell'ExportController.
        public string BuildBaseAddress(bool isForPagination = false)
        {
            QueryStringManager.QueryStringManager qsmanager = new QueryStringManager.QueryStringManager();

            var searchArray = (SearchFields !=null) ? SearchFields.Select(x => x.GetFieldQueryString()).Where(x => x != null).ToArray() : null;
            var legacyArray = GetLegacyQueryString(isForPagination).Select(x => x + "=" + qsmanager[x]).ToArray();

            // se non uso i searchparameter, il comportamento di default � svuotare la querystring precedente e 
            // sovrascrivere la searchArray con la CustomSearchArray
            if (NoSearchParameter)
            {
                legacyArray = new string[0];
                searchArray = CustomSearchArray !=null? CustomSearchArray: new string[0];
            }

            var allKeys = legacyArray.Union(searchArray);

            string newQs = string.Join("&", allKeys);

            string pagename = new System.IO.FileInfo(System.Web.HttpContext.Current.Request.Url.LocalPath).Name;

            return pagename + "?" + newQs;
        }

        public List<ValidatedFields.VfGeneric> SearchFields { get; set; }
        public bool PrintSearchFields { get; set; }

        private string[] GetLegacyQueryString(bool isForPagination)
        {
            QueryStringManager.QueryStringManager qsmanager = new QueryStringManager.QueryStringManager();
            var qsKeys = qsmanager.Keys;
            string[] localKeys;
            if (isForPagination)
            {
                localKeys = SearchForm.Fields.Select(x => x.Key).Union(new[] { this.PaginationKey }).ToArray();
            }
            else
            {
                localKeys = SearchForm.Fields.Select(x => x.Key).ToArray();
            }
            return qsKeys.Where(x => !localKeys.Contains(x)).ToArray();
        }
        
        public VfManager SearchForm
        {
            get
            {
                if (_SearchForm == null)
                {
                    _SearchForm = new VfManager("TabSearch" + TableIndex.Value);
                    _SearchForm.ShowMandatoryInfo = false;
                    _SearchForm.AutoValidation = false;
                    if (SearchFields != null)
                    {
                        _SearchForm.Fields.AddRange(SearchFields);
                        SearchFields.ForEach(x => x.Field.EnableViewState = false);
                    }
                    _SearchForm.SubmitButton.Text = "Cerca";
                }
                return _SearchForm;
            }
        }
        private VfManager _SearchForm;

        //public Control SearchReview()
        //{
        //    DetailsSheet sheet = new DetailsSheet("Parametri di ricerca selezionati", ResultsCount > 0 ? "La ricerca ha prodotto " + ResultsCount + " risultati" : "Spiacente ma la ricerca non ha prodotto alcun risultato.");
        //    //Aggiunto per tamponare un problema di assegnazione della classe al fieldset! Rivedere il codice in maniera approfondita perch� senza di questo quando non ci sono risultati assegna le classi "validatedFields dropdown-vf" (su elenco-richieste.aspx)
        //    sheet.InnerFieldset.PreRender += new EventHandler(delegate(object o, EventArgs args) { sheet.InnerFieldset.CssClass = "DetailsTable"; });
        //    sheet.AddColumn("Parametro", DetailsSheet.Colors.Default);
        //    sheet.AddColumn("Valore", DetailsSheet.Colors.Default);

        //    var parametri = this.Finder.SearchParametersDisplayValues;
        //    foreach (var par in parametri)
        //        sheet.AddTitleTextRow(par.Key, par.Value);

        //    return sheet;
        //}


        #endregion

        public bool IsEmpty(IEnumerable data)
        {
            ICollection list = data as ICollection;
            if (list != null) return list.Count==0;
            IEnumerator iter = data.GetEnumerator();
            return !iter.MoveNext();
        }
    }
}

