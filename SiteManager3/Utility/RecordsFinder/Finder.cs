﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace NetService.Utility.RecordsFinder
{
    public abstract class Finder
    {
        public List<Object> Orders { get; protected set; }

        public Finder(List<SearchParameter> searchParameters = null, List<Object> orders = null)
        {
            this.SearchParameters = searchParameters == null ? new List<SearchParameter>() : searchParameters;
            this.Orders = orders == null ? new List<Object>() : orders;
        }

        public List<SearchParameter> SearchParameters { get; protected set; }

        public Dictionary<string, string> SearchParametersDisplayValues
        {
            get
            {
                return _SearchParametersDisplayValues ?? (_SearchParametersDisplayValues = GetSearchParametersSheet());
            }
        }
        private Dictionary<string, string> _SearchParametersDisplayValues;

        public enum ComparisonCriteria
        {
            Equals,
            EqualsIgnoreCase,
            LessEquals,
            GreaterEquals,
            GreaterThan,
            LessThan,
            In,
            NotIn,
            Like,
            LikeStart,
            LikeEnd,
            Not,
            NotNull,
            Null,
            IsEmpty,
            IsNotEmpty
        }

        public enum GroupingCriteria
        {
            NoGroup,
            Avg,
            Min,
            Max,
            Sum,
            Group
        }


        protected virtual Dictionary<string, string> GetSearchParametersSheet()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();

            foreach (SearchParameter par in SearchParameters.Where(x => x.ShowInSearchReview))
                AddSearchParametersToSheet(dic,par);

            return dic;
        }

        private void AddSearchParametersToSheet(Dictionary<string, string> dic, SearchParameter par)
        {
            if (par.Value is SearchParameter[])
            {
                foreach (SearchParameter p in (par.Value as SearchParameter[]).Where(x => x.ShowInSearchReview))
                    AddSearchParametersToSheet(dic, p);
            }
            else
                if (par.ShowInSearchReview)
                    dic.Add(par.Label, par.DisplayValue.ToString());

        }

        public abstract Array Find();
        public abstract Array Find(int firstResult, int maxResults);
        public Array FindByPage(int pageNumber, int pageSize)
        {
            pageNumber = pageNumber <= 1 ? 1 : pageNumber;
            pageSize = pageSize <= 1 ? 1 : pageSize;

            int bottom = pageSize * pageNumber - pageSize;
            int top = pageSize * pageNumber;

            return Find(bottom, pageSize);
        }
        public abstract int Count();
    }
}
