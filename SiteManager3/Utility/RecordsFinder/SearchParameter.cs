﻿using System.Linq;
using System;
using System.Collections.Generic;

namespace NetService.Utility.RecordsFinder
{
    public class SearchParameter
    {
        public const string NullValue = "null";

        public string Label
        {
            get;
            protected set;
        }

        public virtual string Name
        {
            get;
            protected set;
        }

        public virtual object Value
        {
            get;
            protected set;
        }

        public bool IsProducedByDefault
        {
            get;
            protected set;
        }

        public IDictionary<string, bool> OrdersForJoinSp
        {
            get;
            set;
        }

        public bool HasChildParameters
        {
            get { return Value is SearchParameter[]; }
        }

        public bool ShowInSearchReview
        {
            get { return this.HasChildParameters || (this.DisplayValue != null && this.Label != null); }
        }

        public bool VerifyByLinq
        {
            get
            {
                return _VerifyByLinq || (Parent != null && Parent.VerifyByLinq);
            }
            set
            {
                _VerifyByLinq = value;
            }
        }
        private bool _VerifyByLinq;

        public SearchParameter Parent { get; set; }

        public Finder.ComparisonCriteria ComparisonCriteria
        {
            get;
            set;
        }

        public Finder.GroupingCriteria GroupingCriteria
        {
            get;
            protected set;
        }

        public enum RelationTypes
        {
            OneToOne,
            OneToMany,
            ManyToMany
        }

        public enum JoinTypes
        {
            Full,
            Left,
            Right,
            Inner
        }

        public enum BehaviourClause
        {
            And,
            Or,
            Default
        }

        public BehaviourClause Behaviour
        {
            get
            {
                return _Behaviour;
            }
            set
            {
                _Behaviour = value;
            }
        }
        private BehaviourClause _Behaviour;

        public JoinTypes JoinType
        {
            get
            {
                return _JoinType;
            }
            set
            {
                _JoinType = value;
            }
        }
        private JoinTypes _JoinType;

        private RelationTypes relationType;
        public virtual RelationTypes RelationType
        {
            get { return relationType; }
        }

        public virtual bool AddToCriteria
        {
            get
            {
                if (_AddToCriteria == null)
                {
                    _AddToCriteria = new bool?(CheckAddToCriteria());
                }
                return _AddToCriteria.Value;
            }
        }
        private bool? _AddToCriteria;


        /// <summary>
        /// Search Parameter per confrontare il valore finale di una proprietà, specificando il criterio di comparazione. Inner Join di default.
        /// </summary>
        /// <param name="label">null</param>
        /// <param name="name">Nome della proprietà</param>
        /// <param name="value">Valore da confrontare</param>
        /// <param name="criteria">Criterio di comparazione</param>
        /// <param name="isProducedByDefault"></param>
        public SearchParameter(string label, string name, object value, Finder.ComparisonCriteria criteria, bool isProducedByDefault)
        {
            Label = label;
            Name = name;
            Value = value;
            IsProducedByDefault = isProducedByDefault;
            ComparisonCriteria = criteria;
            JoinType = JoinTypes.Inner;
            Behaviour = BehaviourClause.Default;
        }

        /// <summary>
        /// Costruttore che permette di specificare oltre al tipo di relazione, anche i search parameters interni per la ricerca. Inner Join di default.
        /// </summary>
        /// <param name="label">null</param>
        /// <param name="name">Nome della proprietà annidata dell'oggetto, con cui fare la join</param>
        /// <param name="relationType">Tipo di relazione (OneToOne, OneToMany, ManyToMany)</param>
        /// <param name="parameters">Search parameters di ricerca sull'oggetto a cui appartiene la proprietà specificata in name</param>
        public SearchParameter(string label, string name, RelationTypes relationType, params SearchParameter[] parameters)
        {
            Label = label;
            Name = name;
            IsProducedByDefault = false;
            Value = parameters;
            foreach (var parameter in parameters)
                parameter.Parent = this;
            this.relationType = relationType;
            JoinType = JoinTypes.Inner;
            Behaviour = BehaviourClause.Default;
        }

        /// <summary>
        /// Costruttore che permette di specificare oltre al tipo di relazione, anche il tipo di join da effettuare, nonchè i search parameters interni per la ricerca.
        /// </summary>
        /// <param name="label">null</param>
        /// <param name="name">Nome della proprietà annidata dell'oggetto, con cui fare la join</param>
        /// <param name="relationType">Tipo di relazione (OneToOne, OneToMany, ManyToMany)</param>
        /// <param name="joinType">Tipo di join (Full, Left, Right, Inner)</param>
        /// <param name="parameters">Search parameters di ricerca sull'oggetto a cui appartiene la proprietà specificata in name</param>
        public SearchParameter(string label, string name, RelationTypes relationType, JoinTypes joinType, params SearchParameter[] parameters)
        {
            Label = label;
            Name = name;
            IsProducedByDefault = false;
            Value = parameters;
            foreach (var parameter in parameters)
                parameter.Parent = this;
            this.relationType = relationType;
            JoinType = joinType;
            Behaviour = BehaviourClause.Default;
        }

        /// <summary>
        /// Costruttore che permette di specificare oltre al tipo di relazione, anche l'ordinamento interno, nonchè i search parameters interni per la ricerca. Inner Join di default.
        /// </summary>
        /// <param name="label">null</param>
        /// <param name="name">Nome della proprietà annidata dell'oggetto, con cui fare la join</param>
        /// <param name="relationType">Tipo di relazione (OneToOne, OneToMany, ManyToMany)</param>
        /// <param name="orders">Eventuali ordinamenti da effettuare su proprietà dell'oggetto a cui appartiene la proprietà annidata specificata in name</param>
        /// <param name="parameters">Search parameters di ricerca sull'oggetto a cui appartiene la proprietà specificata in name</param>
        public SearchParameter(string label, string name, RelationTypes relationType, IDictionary<string,bool> orders, params SearchParameter[] parameters)
        {
            Label = label;
            Name = name;
            IsProducedByDefault = false;
            Value = parameters;
            foreach (var parameter in parameters)
                parameter.Parent = this;
            this.relationType = relationType;
            JoinType = JoinTypes.Inner;
            OrdersForJoinSp = orders;
            Behaviour = BehaviourClause.Default;
        }

        /// <summary>
        /// Costruttore che permette di specificare oltre al tipo di relazione, anche il tipo di join da effettuare e l'ordinamento interno, nonchè i search parameters interni per la ricerca.
        /// </summary>
        /// <param name="label">null</param>
        /// <param name="name">Nome della proprietà annidata dell'oggetto, con cui fare la join</param>
        /// <param name="relationType">Tipo di relazione (OneToOne, OneToMany, ManyToMany)</param>
        /// <param name="joinType">Tipo di join (Full, Left, Right, Inner)</param>
        /// <param name="orders">Eventuali ordinamenti da effettuare su proprietà dell'oggetto a cui appartiene la proprietà annidata specificata in name</param>
        /// <param name="parameters">Search parameters di ricerca sull'oggetto a cui appartiene la proprietà specificata in name</param>
        public SearchParameter(string label, string name, RelationTypes relationType, JoinTypes joinType, IDictionary<string, bool> orders, params SearchParameter[] parameters)
        {
            Label = label;
            Name = name;
            IsProducedByDefault = false;
            Value = parameters;
            foreach (var parameter in parameters)
                parameter.Parent = this;
            this.relationType = relationType;
            JoinType = joinType;
            OrdersForJoinSp = orders;
            Behaviour = BehaviourClause.Default;
        }

        /// <summary>
        /// Ritorna gli oggetti dopo aver effettuato un raggruppamento. Inner Join di default.
        /// </summary>
        /// <param name="label">null</param>
        /// <param name="name">Nome della proprietà</param>
        /// <param name="criteria">Criteria di raggruppamento (Max,Min,Group By,Avg,Sum)</param>
        /// <param name="isProducedByDefault"></param>
        public SearchParameter(string label, string name, Finder.GroupingCriteria criteria, bool isProducedByDefault)
        {
            Label = label;
            Name = name;
            Value = NullValue;
            IsProducedByDefault = isProducedByDefault;
            GroupingCriteria = criteria;
            JoinType = JoinTypes.Inner;
            Behaviour = BehaviourClause.Default;
        }

        /// <summary>
        /// Ritorna gli oggetti dopo aver effettuato un raggruppamento. Inner Join di default.
        /// </summary>
        /// <param name="label">null</param>
        /// <param name="name">Nome della proprietà</param>
        /// <param name="criteria">Criteria di raggruppamento (Max,Min,Group By,Avg,Sum)</param>
        /// <param name="parameters">Search parameters che comporranno la condizione della subquery di raggruppamento</param>
        public SearchParameter(string label, string name, Finder.GroupingCriteria criteria, params SearchParameter[] parameters)
        {
            Label = label;
            Name = name;
            Value = parameters;
            foreach (var parameter in parameters)
                parameter.Parent = this;
            IsProducedByDefault = false;
            GroupingCriteria = criteria;
            JoinType = JoinTypes.Inner;
            Behaviour = BehaviourClause.Default;
        }

        /// <summary>
        /// Ritorna proprietà o oggetti dopo aver effettuato un raggruppamento. Inner Join di default.
        /// </summary>
        /// <param name="label">null</param>
        /// <param name="name">Nome della proprietà</param>
        /// <param name="criteria">Criteria di raggruppamento (Max,Min,Group By,Avg,Sum)</param>
        /// <param name="isProducedByDefault"></param>
        /// <param name="returnPropertyValue">Se è true, ritorna la proprietà specificata nel criterio, altrimenti ritorna l'oggetto su cui si agisce</param>
        public SearchParameter(string label, string name, Finder.GroupingCriteria criteria, bool isProducedByDefault, bool returnPropertyValue)
        {
            Label = label;
            Name = name;
            Value = NullValue;
            IsProducedByDefault = isProducedByDefault;
            GroupingCriteria = criteria;
            JoinType = JoinTypes.Inner;
            _ReturnPropertyValue = returnPropertyValue;
            Behaviour = BehaviourClause.Default;
        }

        /// <summary>
        /// Ritorna proprietà o oggetti dopo aver effettuato un raggruppamento, con la possibilità di inserire condizioni nella subquery del raggruppamento. Inner Join di default.
        /// </summary>
        /// <param name="label">null</param>
        /// <param name="name">Nome della proprietà</param>
        /// <param name="criteria">Criteria di raggruppamento (Max,Min,Group By,Avg,Sum)</param>
        /// <param name="isProducedByDefault"></param>
        /// <param name="returnPropertyValue">Se è true, ritorna la proprietà specificata nel criterio, altrimenti ritorna l'oggetto su cui si agisce</param>
        /// <param name="parameters">Search parameters che comporranno la condizione della subquery di raggruppamento</param>
        public SearchParameter(string label, string name, Finder.GroupingCriteria criteria, bool isProducedByDefault, bool returnPropertyValue, params SearchParameter[] parameters)
        {
            Label = label;
            Name = name;
            Value = parameters;
            foreach (var parameter in parameters)
                parameter.Parent = this;
            IsProducedByDefault = isProducedByDefault;
            GroupingCriteria = criteria;
            JoinType = JoinTypes.Inner;
            _ReturnPropertyValue = returnPropertyValue;
            Behaviour = BehaviourClause.Default;
        }

        
        /// <summary>
        /// Questo costruttore permette di implementare un OR o un AND tra la collezione di search parameters inseriti al suo interno. Nel momento in cui viene usata, quasiasi AND o OR dovrà essere effettuata usando questo costruttore con all'interno i SearchParameters che compongono la condizione.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="behaviour">Tipologia di comportamento: AND, OR o Default. Tutti gli altri SearchParameter sono a Default.</param>
        /// <param name="parameters">Criteri di ricerca. Possono essere annidati e combinati opportunamente attraverso questo costruttore.</param>      
        public SearchParameter(string label, BehaviourClause behaviour, params SearchParameter[] parameters)
        {
            Label = label;
            Behaviour = behaviour;
            IsProducedByDefault = false;
            Value = parameters;
            foreach (var parameter in parameters)
                parameter.Parent = this;

        }

        private bool CheckAddToCriteria()
        {
            bool ok = this.Value != null;

            if (ok && Value is SearchParameter[])
            {
                var pars = Value as SearchParameter[];
                ok = pars.Count(x => x.AddToCriteria) > 0;
            }
            //if (ok && Value is SearchParameter[])
            //{
            //    var sp = Value as SearchParameter[];
            //    if (!this.EnableLeftJoin || sp.Count(x => x.AddToCriteria) > 0)
            //    {
            //        var pars = Value as SearchParameter[];
            //        ok = pars.Count(x => x.AddToCriteria) > 0;
            //    }
            //}

            return ok;
        }

        public Func<SearchParameter, object> DisplayValueBuilder
        {
            get
            {
                if (_DisplayValueBuilder == null)
                {
                    _DisplayValueBuilder = (x) => { return x.Value; };
                }
                return _DisplayValueBuilder;
            }
            set
            {
                _DisplayValueBuilder = value;
            }
        }
        private Func<SearchParameter, object> _DisplayValueBuilder;

        public object DisplayValue
        {
            get
            {
                try
                {
                    var dv = DisplayValueBuilder(this);
                    return dv;
                }
                catch { }

                return null;
            }
        }
        private bool _ReturnPropertyValue = false;
        public bool ReturnPropertyValue
        {
            get 
            {
                return _ReturnPropertyValue;
            }             
        }
    }
}
