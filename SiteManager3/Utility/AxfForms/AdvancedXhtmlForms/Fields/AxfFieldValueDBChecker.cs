using System;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;




namespace G2Core.AdvancedXhtmlForms.Fields
{
    /// <summary>
    /// Implementa una classe per verificare la validit� di dati dal database 
    /// </summary>
    /// <example>    
    /// Segue un esempio di utilizzo della classe.
    /// <code>
    ///  private NetCms.Connections.Connection _oConn;
    ///  protected NetCms.Connections.Connection oConn
    ///  {
    ///     get
    ///     {
    ///       if (_oConn == null)
    ///       {
    ///         _oConn = new G2Core.Connections.OdbcConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["SmNetDB"].ConnectionString);
    ///        }
    ///
    ///        return _oConn;
    ///      }
    ///  }
    ///  AxfTextBox tb = new AxfTextBox("Name", "Name");
    ///  //Imposta la stringa che sar� utilizzata nel verificare se esiste un dato nel database
    ///  tb.ValueDBChecker.SetData("SELECT {0} FROM Users_Frontend WHERE {0} LIKE '{1}'", oConn);
    ///  
    ///  //la seguente funzione esegue la query impostata alla istruzione precedente sostituendo al parametro
    ///  //{0}= tb.FieldName e {1} =il valore passato alla funzione ValueDBChecker.DataExist in modo
    ///  //da ottenere una query valida
    ///  bool exist=tb.ValueDBChecker.DataExist("somename");
    /// </code>
    /// </example>
    public class AxfFieldValueDBChecker
    {
        private string _Sql;
        private string Sql
        {
            get { return _Sql; }
        }

        private NetCms.Connections.Connection _Conn;
        private NetCms.Connections.Connection Conn
        {
            get { return _Conn; }
        }

        private AxfField _Field;
        private AxfField Field
        {
            get { return _Field; }
        }

        /// <summary>
        /// Inizializza una istanza della classe.
        /// </summary>
        /// <param name="owner">Riferimento ad una istanza G2Core.AdvancedXhtmlForms.Fields.AxfField</param>
        public AxfFieldValueDBChecker(AxfField owner)
        {
            
            _Field = owner;
        }

        /// <summary>
        /// Imposta dei valori interni della classe con i valori passati.
        /// </summary>        
        /// <param name="sql">Stringa sqlquery da utilizzare per verificare se un dato esiste nel database</param>
        /// <param name="conn">Connessione ad un database</param>
        public void SetData(string sql, NetCms.Connections.Connection conn)
        {
            _Sql = sql;
            _Conn = conn;
        }

        /// <summary>
        /// Indica se il valore passato esiste nel database
        /// </summary>
        /// <param name="data">dato di cui verificare l'esistenza nel database</param>
        /// <returns>Valore che indica se il dato esiste</returns>
        public bool DataExist(string data)
        {
            if (Conn != null && Sql != null)
            {
                DataTable tab = Conn.SqlQuery(string.Format(Sql, Field.FieldName, data));
                return tab.Rows.Count > 0;
            }
            return false;
        }
    }
}