using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Connections;


namespace G2Core.AdvancedXhtmlForms.Fields
{
    /// <summary>
    /// La classe AxfDropDownList implementa Rappresenta un controllo che consente all'utente di selezionare un singolo elemento da un elenco a discesa. 
    /// </summary>
    public class AxfDropDownList : AxfField
    {

        private DataTable BindingSource;
        private string DataMember;
        private string DataSource;
        private string SqlQuery;
        private string DataValue;
        
        private bool _AutoPostBack;
        private bool _SetOnClientClick;

        private string RowFilter = "";

        public bool Visible = true;
        public bool IncludeEmptyItem = true;

        private AxfDropDownList _ParentDropDownListID;
        private AxfDropDownList ParentDropDownListID
        {
            get
            {
                return (_ParentDropDownListID);
            }
            set
            {

                _ParentDropDownListID = value;

            }

        }
        string defaultfiltervalue = "";

       

        /// <summary>
        /// Ottiene o imposta un valore per determinare se al cambio della selezione avviene 
        /// un Post automatico del form.
        /// </summary>
        public bool AutoPostBack
        {
            get
            {
                return _AutoPostBack;
            }
            set
            {
                _AutoPostBack = value;
            }
        }

        public bool SetOnClientClick
        {
            get
            {
                return _SetOnClientClick;
            }
            set
            {
                _SetOnClientClick = value;
            }
        }

        public bool _hideIfNoItems;
        public bool hideIfNoItems
        {
            get
            {
                return _hideIfNoItems;
            }
            set
            {
                _hideIfNoItems = value;
            }
        }

        private string _OnClientClick;

        /// <summary>
        /// 
        /// </summary>
        public string OnClientClick
        {
            get {
                if (_OnClientClick == null)
                {
                    _OnClientClick = "setSubmitSource('" + FieldName + "')";
                }
                return _OnClientClick; 
            }
            set { _OnClientClick = value; }
        }
	
        private Connection Conn;

        private int bindings;

        private DropDownList DropDownList;

        /// <summary>
        /// Inizializza una nuova istanza del campo DropDownList.
        /// </summary>
        /// <param name="label">Etichetta testuale del campo: apparir� nel form</param>
        /// <param name="fieldname">Rappresenta l'identificativo (ID) del campo nella form. 
        /// Generalmente si usa per mappare il campo corrispondente a quello del database</param>
        /// <param name="infoText">Testo informativo che illustra cosa fa il campo</param>
        public AxfDropDownList(string label, string fieldname,string infoText)
            : base(label, fieldname,infoText)
        {
            DropDownList = new DropDownList();
            DropDownList.ID = FieldName;            
            bindings = 0;

            ListItem item = new ListItem("", "");
            DropDownList.Items.Add(item);
        }
        /// <summary>
        /// Inizializza una nuova istanza del campo DropDownList.
        /// </summary>
        /// <param name="label">Etichetta testuale del campo: apparir� nel form</param>
        /// <param name="fieldname">Rappresenta l'identificativo (ID) del campo nella form. 
        /// Generalmente si usa per mappare il campo corrispondente a quello del database</param>
        public AxfDropDownList(string label, string fieldname)
            : this(label, fieldname, null)
        {
        }
        
        /*/// <summary>
        /// Svuota il contenuto del DropDownList
        /// </summary>
        public void ClearList()
        {
            DropDownList.Items.Clear();
        }*/

        public DropDownList InnerDropDownList
        {
            get
            {
                return DropDownList;
            }
        }
        

        private string getFilterByParentValue()
        {
            string filter = "";
            if (ParentDropDownListID != null)
            {
                filter = "Convert(" + RowFilter + ",System.String)='{0}'";
                filter = (String.Format(filter, ParentDropDownListID.PostBackValue != null && ParentDropDownListID.PostBackValue.Length > 0 ? ParentDropDownListID.PostBackValue : ParentDropDownListID.Value.Length > 0 ? ParentDropDownListID.Value : defaultfiltervalue));
            }
            return (filter);
        }

       /* public void SetParentDropDown(AxfDropDownList parentID, string rowfilter, TypeCode type)
        {
            this.ParentDropDownListID = parentID;
            switch (type)
            {
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                    rowfilter += "={0}";
                    defaultfiltervalue = "-1";
                    break;
                case TypeCode.String:
                case TypeCode.Boolean:
                    rowfilter += "='{0}'";
                    defaultfiltervalue = "";
                    break;
                default:
                    break;
            }
            this.RowFilter = rowfilter;
        }
        */
        public void SetParentDropDown(AxfDropDownList parentID, string filterByFieldName)
        {
            this.ParentDropDownListID = parentID;
            //rowfilter += "='{0}'";            
            defaultfiltervalue = "";
            this.RowFilter = filterByFieldName;
        }


        private void bindList()
        {
            string sql = "";
            DataTable dt;
            switch (bindings)
            {
                case 1:
                    sql = "SELECT * FROM " + DataSource;
                    dt = Conn.SqlQuery(sql);
                    break;
                case 2:
                    sql = SqlQuery;
                    dt = Conn.SqlQuery(sql);
                    break;
                case 3:
                    dt = BindingSource;
                    break;
                default:
                    sql = "SELECT * FROM " + DataSource;
                    dt = Conn.SqlQuery(sql);
                    break;

            }
            ListItem item;
            DataView dv = dt.DefaultView;                        
            dv.RowFilter = getFilterByParentValue();

            hideIfNoItems = hideIfNoItems && dv.Count==0;
            if(!IncludeEmptyItem)
                DropDownList.Items.Clear();
            foreach (DataRowView row in dv)            
            //foreach (DataRow row in dt.Rows)
            {
                if (bindings == 2)
                    item = new ListItem(row[0].ToString(), row[1].ToString());
                else
                    item = new ListItem(row[DataMember].ToString(), row[DataValue].ToString());
                item.Attributes.Add("title", item.Text);
                DropDownList.Items.Add(item);
            }

        }

        /// <summary>
        /// Imposta l'associazione tra i campi del database e la coppia testo/valori
        /// </summary>
        /// <remarks>
        /// Si usa generalmente quando si ha la necessit� di associare il campo DropDownList 
        /// a testo/valore recuperato direttamente da una prefissata tabella del database.
        /// </remarks>
        /// <param name="dataMember">Il nome sul database del campo dal quale ottenere il valore da stampare</param>
        /// <param name="dataValue">Il nome sul database del campo associato al valore del campo indicizzato</param>
        /// <param name="dataSource">Il nome della Tabella sul database da cui prendere i valori da associare</param>
        /// <param name="conn">Connessione al database <see cref="NetCms.Connections.Connection"/></param>
        public void setDataBind(string dataMember, string dataValue, string dataSource, Connection conn)
        {
            DataMember = dataMember;
            DataValue = dataValue;
            DataSource = dataSource;
            Conn = conn;
            bindings = 1;
        }
        /// <summary>
        /// Imposta l'associazione tra i campi del database e la coppia testo/valori
        /// </summary>
        /// <remarks>
        /// Si usa generalmente quando si ha la necessit� di alimentare il campo con valori generati da una query
        /// anche complessa.
        /// </remarks>
        /// <param name="sqlQuery">Query sql 
        /// <example>
        /// Si usa in questo modo
        /// <code>
        /// SELECT [DisplayMember],[ValueMember] FROM nometabella WHERE ....
        /// </code>
        /// </example>
        /// </param>
        /// <param name="conn">Connessione al database <see cref="NetCms.Connections.Connection"/></param>
        /// 
        public void setDataBind(string sqlQuery, Connection conn)
        {
            SqlQuery = sqlQuery;
            Conn = conn;
            bindings = 2;
        }


        /// <summary>
        /// Imposta l'associazione tra i campi del database e la coppia testo/valori
        /// </summary>
        /// <remarks>
        /// Si usa generalmente quando si ha la necessit� di alimentare il campo con valori provenienti da una DataTable
        /// </remarks>
        /// <param name="dataMember">Il nome sul database del campo dal quale ottenere il valore da stampare</param>
        /// <param name="dataValue">Il nome sul database del campo associato al valore del campo indicizzato</param>
        /// <param name="table">La DataTable da cui prendere i valori da associare</param>
        public void setDataBind(string dataMember, string dataValue, DataTable table)
        {
            DataMember = dataMember;
            DataValue = dataValue;
            BindingSource = table;
            bindings = 3;
        }

        /// <summary>
        /// Metodo che permette l'aggiunta di un nuovo valore alla DropDownList
        /// </summary>
        /// <param name="Label">Testo da visualizzare nel campo</param>
        public void addItem(string Label)
        {
            DropDownList.Items.Add(Label);
        }

        /// <summary>
        /// Metodo che permette l'aggiunta di un nuovo WebControl ListItem
        /// </summary>
        /// <param name="item"></param>
        public void addItem(ListItem item)
        {
            DropDownList.Items.Add(item);
        }

        /// <summary>
        /// Metodo che permette l'aggiunta di un nuovo valore alla DropDownList
        /// </summary>
        /// <param name="Label">Testo da visualizzare nel campo</param>
        /// <param name="Value">Valore da associare al testo visualizzato</param>       
        public void addItem(string Label, string Value)
        {
            ListItem item = new ListItem(Label, Value);
            DropDownList.Items.Add(item);
        }

        /// <summary>
        /// Metodo che permette l'aggiunta di un nuovo valore alla DropDownList
        /// </summary>
        /// <param name="Label">Testo da visualizzare nel campo</param>
        /// <param name="Value">Valore da associare al testo visualizzato</param>
        /// <param name="selected">Indicazione se il valore � selezionato</param>
        public void addItem(string Label, string Value,bool selected)
        {
            ListItem item = new ListItem(Label, Value);
            item.Selected = selected;
            DropDownList.Items.Add(item);
        }

        /// <summary>
        /// Restituisce il controllo xhtml relativo al campo 
        /// </summary>
        /// <returns>Un HtmlGenericControl contenente tutti i controlli del campo</returns>
        protected override HtmlGenericControl getFieldContent()
        {
            HtmlGenericControl control = new HtmlGenericControl("div");
            HtmlGenericControl ddlControl = getControlSimple();

            if (bindings != 0)
                bindList();

            if (Value.Length > 0)
            {
                foreach (ListItem item in DropDownList.Items)
                {
                    if (item.Value.ToLower()== Value.ToLower())
                    {
                        item.Selected = true;
                        DropDownList.SelectedIndex = DropDownList.Items.IndexOf(item);
                    }
                }
            }
            if (ReadOnly)
            {
                ListItem item = DropDownList.SelectedItem;
                DropDownList.Items.Clear();
                if(item!=null)
                    DropDownList.Items.Add(item);
            }
            control.Controls.Add(ddlControl);
            ddlControl.Visible = (!hideIfNoItems && Visible) ;
            return control;
        }
    
        private HtmlGenericControl getControlSimple()
        {
            HtmlGenericControl par = new HtmlGenericControl("div");
            if(Visible)
                par.Attributes["class"] = "Axf_FieldBox";

            Label lb = new Label();
            lb.AssociatedControlID = _FieldName;
            if (Required)
                lb.Text = _Label + "*";
            else
                lb.Text = _Label;

            lb.Attributes["class"] = "formlabel";

            if (_Label.Length > 0)
                par.Controls.Add(lb);
            DropDownList.AutoPostBack = AutoPostBack;
            if (AutoPostBack || SetOnClientClick)
                DropDownList.Attributes["onchange"] =OnClientClick;
            DropDownList.ID = _FieldName;            
            par.Controls.Add(DropDownList);

            return par;
        }


        /// <summary>
        /// Verifica se il valore inserito soddisfa i criteri pre-impostati.
        /// </summary>
        /// <param name="input">Valore da verificare</param>
        /// <returns>Messaggio di errore racchiuso all'interno di un tag html "li" </returns>
        protected override string checkInput(string input)
        {
            string errors = "";

            if (Required && input == string.Empty)
            {
                errors += "<li>";
                errors += string.Format(this.Labels[AxfLabels.LabelsList.IlCampoEObbligatorio], this.Label);
                errors += "</li>";
            }

            return errors ;

        }


        /// <summary>
        /// Restituisce il valore del campo strutturato per il database
        /// </summary>
        /// <param name="value">Valore del campo</param>
        /// <returns>Restituisce il valore del campo trutturato per il database</returns>
        public override string getFilter(string value)
        {
            string filter = " ";
            if (value.Length > 0)
            {
                int val = 0;
                if (int.TryParse(value, out val))
                {
                    filter += _FieldName;

                    filter += " = ";
                    filter += value;
                    filter += " ";
                }
                else
                {
                    filter += _FieldName;

                    filter += " LIKE '";
                    filter += value;
                    filter += "' ";
                }
            }
            return filter;
        }

        /// <summary>
        /// Metodo che restituisce eventuali messaggi in un controllo HtmlGenericControl "li"
        /// </summary>
        /// <returns>Un HtmlGenericControl contenente gli eventuali messaggi da visualizzare</returns>      
        public override HtmlGenericControl GetOverviewControl()
        {
            HtmlGenericControl fieldControl = new HtmlGenericControl("li");
            fieldControl.InnerHtml = "<strong>" + this.Label + ": </strong>" + DropDownList.Items.FindByValue(this.Value).Text;
            return fieldControl;
        }
    }

}