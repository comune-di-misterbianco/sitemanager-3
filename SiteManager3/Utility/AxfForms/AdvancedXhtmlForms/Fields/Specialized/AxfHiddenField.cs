using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;


namespace G2Core.AdvancedXhtmlForms.Fields
{
    /// <summary>
    /// La classe AxfHiddenField implementa un controllo web testuale nascosto
    /// </summary>
    public class AxfHiddenField : AxfField
    {
        public override bool ShowInInOverview{get{return false;}}

        private bool _ValidValue = false;
        /// <summary>
        /// Ottiene o imposta un valore booleano che indica se il valore del campo � valido
        /// </summary>
        public bool ValidValue
        {
            get { return _ValidValue; }
            set { _ValidValue = value; }
        }
       
        /// <summary>
        /// Ottiene o imposta un Valore del contenuto del campo
        /// </summary>
        public override string Value
        {
            set
            {
                if (!init)
                {
                    init = !init;
                    _Value = value;
                    
                }
            }
            get { return _Value; }
        }
        private string _Value;
        private bool init = false;

        /// <summary>
        /// Inizializza una nuova istanza del campo AxfHiddenField
        /// </summary>
        /// <param name="fieldname">Nome del campo nella form</param>
        public AxfHiddenField(string fieldname)
            : base("", fieldname)
        {
            Required = false;
        }
        
        /// <summary>
        /// Inizializza una nuova istanza del campo AxfHiddenField
        /// </summary>
        /// <param name="fieldname">Nome del campo nella form</param>
        /// <param name="value">Valore iniziale del campo</param>
        public AxfHiddenField(string fieldname, string value)
            : base("", fieldname)
        {
            Required = false;
            this.Value = value;
        }


        /// <summary>
        /// Restituisce il controllo xhtml relativo al campo 
        /// </summary>
        /// <returns>Un HtmlGenericControl contenente tutti i controlli del campo</returns>
        protected override HtmlGenericControl getControl()
        {
            HtmlGenericControl output = new HtmlGenericControl("div");
            HtmlInputHidden hidden = new HtmlInputHidden();
            hidden.ID = FieldName;
            hidden.Value = Value;

            output.Controls.Add(hidden);
            return output;
        }
       /// <summary>
       /// Non implementato
       /// </summary>
        /// <returns>HtmlGenericControl</returns>
        protected override HtmlGenericControl getFieldContent()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Verifica se il valore inserito soddisfa i criteri pre-impostati.
        /// </summary>
        /// <param name="input">Valore da verificare</param>
        /// <returns>Messaggio di errore racchiuso all'interno di un tag html "li" </returns>
        protected override string checkInput(string input)
        {
            return "";
        }

        /// <summary>
        /// Restituisce il valore del campo filtrato in funzione del parametro di comparazione
        /// </summary>
        /// <param name="value">Valore del campo</param>
        /// <returns>Restituisce il valore del campo filtrato</returns>
        public override string getFilter(string value)
        {
            return string.Empty;
        }

        /// <summary>
        /// Restituisce il valore del campo filtrato in modo appropriato per l'inserimento nel database.
        /// </summary>
        /// <param name="value">Valore del campo</param>
        /// <returns>Restituisce il valore del campo filtrato</returns>
        public override string filterValueForDB(string value)
        {
            if (init)
                return this.Value;

            if (!ValidValue)
                return base.filterValueForDB(value);
            else
                return value;
        }
    }

}