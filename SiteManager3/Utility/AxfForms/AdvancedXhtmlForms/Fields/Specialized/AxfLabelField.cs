﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Connections;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace G2Core.AdvancedXhtmlForms.Fields
{
    public class AxfLabelField : AxfField
    {          
       

        public AxfLabelField(string label, string fieldname)
            : base(label, fieldname)
        {
            //non è connesso con un dato di database
            this.JumpField = true;           
        }


        protected override HtmlGenericControl getFieldContent()
        {
            HtmlGenericControl control = new HtmlGenericControl("div");  
        
            HtmlGenericControl span = new HtmlGenericControl("span");
            span.Attributes["class"] = "Axf_FieldBox";          
            Label lb = new Label();
            lb.AssociatedControlID = this.FieldName;
            lb.Text = this.Label;
            lb.Attributes["class"] = "formlabel";
            span.Controls.Add(lb);

            Label lbvalue = new Label();            
            lbvalue.ID=FieldName;
            lbvalue.Text = this.Value;
            lbvalue.Attributes["class"] = "labelvalue";
            span.Controls.Add(lbvalue);

            control.Controls.Add(span);
            return control;
        }

        protected override string checkInput(string input)
        {
            return "";
        }

        public override string getFilter(string value)
        {
            return "";
        }
    }

}