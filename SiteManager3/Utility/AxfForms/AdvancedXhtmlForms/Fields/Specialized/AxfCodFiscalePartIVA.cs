using System;
using System.Linq;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetService.Utility.Request;


namespace G2Core.AdvancedXhtmlForms.Fields
{ 
    /// <summary>
    /// La classe AxfTextBox implementa un controllo di casella testo che accetta ed elabora varie tipologie di stringhe testuali per l'input dell'utente
    /// come telefoni, email, valute etc... <see cref="G2Core.AdvancedXhtmlForms.Fields.AxfTextBox.ContentTypes"/>
    /// </summary>
    /// <remarks>
    /// Implementa dei controlli sul formato <see cref="G2Core.AdvancedXhtmlForms.Fields.AxfTextBox.ContentTypes"/> del campo 
    /// e ne verifca la corretta sintassi comunicando gli eventuali errori.
    /// </remarks>
    public class AxfCodFiscalePartIVA : AxfField
    {
        public TextBox InnerTextBox
        {
            get
            {
                if (_InnerTextBox == null)
                {
                    _InnerTextBox = new TextBox();
                    _InnerTextBox.ID = this.FieldName;                     
                }
                return _InnerTextBox;
            }
        }
        private TextBox _InnerTextBox;

        
        public AxfCodFiscalePartIVA(string label, string fieldname)
            : base(label, fieldname)
        {
        }
        
        /// <summary>
        /// Metodo che verifca la corretta sintassi del campo in funzione alla tipologia indicata.
        /// </summary>
        /// <param name="input">Valore del campo</param>
        /// <returns>Restituisce gli eventuali errori rilevati all'interno di una o pi� stringhe html "li"</returns>
        protected override string checkInput(string input)
        {
            string errors = string.Empty;

            input = input.Trim();

            if (this.RequestVariable.IsValidString)
            {
                string value = input;
                if (this.Required && string.IsNullOrEmpty(value))
                {
                    errors += "<li>";
                    errors += string.Format(this.Labels[AxfLabels.LabelsList.IlCampoEObbligatorio], this.Label);
                    errors += "</li>";
                }
                else
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (value.Length != 16 && value.Length != 11)
                        {
                            errors += "<li>";
                            errors += string.Format("la lunghezza del campo '{0}' deve essere 11 caratteri per la partita IVA o 16 per il codice fiscale", this.Label);
                            errors += "</li>";
                        }

                        if (value.Length == 16)
                        {
                            if (!NetService.Utility.Common.FormatValidator.ValidateCodiceFiscale(value))
                            {
                                errors += "<li>";
                                errors += string.Format("Il campo {0} non contiene un codice fiscale valido", this.Label);
                                errors += "</li>";
                            }
                        }

                        if (value.Length == 11)
                        {
                            if (!NetService.Utility.Common.FormatValidator.ValidatePartitaIVA(value))
                            {
                                errors += "<li>";
                                errors += string.Format("Il campo {0} non contiene una partita IVA valida", this.Label);
                                errors += "</li>";
                            }
                        }
                    }
                }
            }
            else
            {
                if (this.Required && string.IsNullOrEmpty(input))
                {
                    errors += "<li>";
                    errors += string.Format(this.Labels[AxfLabels.LabelsList.IlCampoEObbligatorio], this.Label);
                    errors += "</li>";
                }
            }
            return errors;

        }

        /// <summary>
        /// Restituisce il valore del campo strutturato per il database
        /// </summary>
        /// <param name="value">Valore del campo</param>
        /// <returns>Restituisce il valore del campo strutturato per il database
        /// applicando controlli di sql injection</returns>
        public override string getFilter(string value)
        {
            return value.Replace("'", "''").Trim();
        }
        
        /// <summary>
        /// Restituisce il valore del campo strutturato per il database
        /// </summary>
        /// <param name="value">Valore del campo</param>
        /// <returns>Restituisce il valore del campo strutturato per il database
        /// applicando controlli di sql injection</returns>
        public override string  filterValueForDB(string value)
        {
            value = value.Trim();

            return value = "'" + value.Replace("'", "''") + "'";
        }
        
        /// <summary>
        /// Restituisce il controllo xhtml relativo al campo 
        /// </summary>
        /// <returns>Un HtmlGenericControl contenente tutti i controlli del campo</returns>
        protected override HtmlGenericControl getFieldContent()
        {
            HtmlGenericControl control = new HtmlGenericControl("div");
            control.Controls.Add(this.BuildBox( this.Label));
            return control;
        }
        
        private HtmlGenericControl BuildBox(string label)
        {
            HtmlGenericControl span = new HtmlGenericControl("span");
            span.Attributes["class"] = "Axf_FieldBox";          

            Label lb = new Label();
            lb.AssociatedControlID = this.FieldName;
            if (Required)
                lb.Text = label + "*";
            else
                lb.Text = label;
            lb.Attributes["class"] = "formlabel";
            if (lb.Text != "")
                span.Controls.Add(lb);

            InnerTextBox.ID = this.FieldName;
            InnerTextBox.Columns = 16;            
            InnerTextBox.TextMode = TextBoxMode.SingleLine;

            if (ReadOnly)
            {
                InnerTextBox.ReadOnly = true;
                InnerTextBox.CssClass = "readonly";                
            }

            if (OnChange != null)
                InnerTextBox.Attributes["onchange"] = OnChange;

            if (OnKeyup != null)
                InnerTextBox.Attributes["onkeyup"] = OnKeyup;
            
            InnerTextBox.Text = Value;

            span.Controls.Add(InnerTextBox);

            return span;
        }
    }
}