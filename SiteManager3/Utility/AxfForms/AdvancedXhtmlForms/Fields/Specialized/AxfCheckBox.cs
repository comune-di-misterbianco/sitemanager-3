using System;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace G2Core.AdvancedXhtmlForms.Fields
{
    public class AxfCheckBox : AxfField
    {
        public CheckBox InnerCheckBox
        {
            get
            {
                if (_InnerCheckBox == null)
                {
                    _InnerCheckBox = new CheckBox();
                    _InnerCheckBox.ID = this.FieldName;
                }
                return _InnerCheckBox;
            }
        }
        private CheckBox _InnerCheckBox;

        public bool Visible = true;

        private bool _ValidValue = false;
        public bool ValidValue
        {
            get { return _ValidValue; }
            set { _ValidValue = value; }
        }

        public override bool ShowInInOverview { get { return false; } }

        private bool _AutoPostBack;
        public bool AutoPostBack
        {
            get
            {
                return _AutoPostBack;
            }
            set
            {
                _AutoPostBack = value;
            }
        }

        private string _OnClientClick;
        public string OnClientClick
        {
            get
            {
                if (_OnClientClick == null)
                {
                    _OnClientClick = "setSubmitSource('" + FieldName + "')";
                }
                return _OnClientClick;
            }
            set { _OnClientClick = value; }
        }

        private bool init = false;

        public AxfCheckBox(string label, string fieldname)
            : base(label, fieldname)
        {
            _InnerCheckBox = new CheckBox();                       
        }

        protected override string checkInput(string input)
        {
            string errors = "";

            if (Required && input == string.Empty)
            {
                errors += "<li>";
                errors += string.Format(this.Labels[AxfLabels.LabelsList.IlCampoEObbligatorio], this.Label);
                errors += "</li>";
            }

            return errors;

        }

        public override string getFilter(string value)
        {
            string filter = " ";
            return filter;
        }      
        
        public override string filterValueForDB(string value)
        {
            if (init)
                return this.Value;

            if (!ValidValue)
                return base.filterValueForDB(value);
            else
                return value;
        }

        /// <summary>
        /// Restituisce il controllo xhtml relativo al campo 
        /// </summary>
        /// <returns>Un HtmlGenericControl contenente tutti i controlli del campo</returns>
        protected override HtmlGenericControl getFieldContent()
        {
            HtmlGenericControl control = new HtmlGenericControl("div");
            HtmlGenericControl rblControl = getControlSimple();

            if (Value.Length > 0)
            {
                InnerCheckBox.Checked = bool.Parse(Value);
                
            }
            control.Controls.Add(rblControl);

            return control;
        }

        private HtmlGenericControl getControlSimple()
        {
            HtmlGenericControl par = new HtmlGenericControl("div");
            par.Attributes["class"] = "Axf_FieldBox";
            par.ID = _FieldName + "Div";

            Label lb = new Label();
            lb.AssociatedControlID = _FieldName;
            if (Required)
                lb.Text = _Label + "*";
            else
                lb.Text = _Label;

            lb.Attributes["class"] = "formlabel";

            par.Controls.Add(lb);
            _InnerCheckBox.AutoPostBack = AutoPostBack;
            if (AutoPostBack)
                _InnerCheckBox.Attributes["onchange"] = OnClientClick;
            _InnerCheckBox.ID = _FieldName;                              

            _InnerCheckBox.Visible = Visible;
            lb.Visible = Visible;

            par.Controls.Add(_InnerCheckBox);
            return par;
        }

    }
}