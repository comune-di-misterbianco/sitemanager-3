using System;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


namespace G2Core.AdvancedXhtmlForms.Fields
{ 
    /// <summary>
    /// La classe AxfTextBox implementa un controllo di casella testo che accetta ed elabora varie tipologie di stringhe testuali per l'input dell'utente
    /// come telefoni, email, valute etc... <see cref="G2Core.AdvancedXhtmlForms.Fields.AxfTextBox.ContentTypes"/>
    /// </summary>
    /// <remarks>
    /// Implementa dei controlli sul formato <see cref="G2Core.AdvancedXhtmlForms.Fields.AxfTextBox.ContentTypes"/> del campo 
    /// e ne verifca la corretta sintassi comunicando gli eventuali errori.
    /// </remarks>
    public class AxfTextBox : AxfField
    {
        private bool _CheckForDataExistance;
        /// <summary>
        /// Ottiene o imposta un parametro che indica se il valore contenuto 
        /// nel campo � gia presente nel campo corrispondente nel database
        /// </summary>
        public bool CheckForDataExistance
        {
            get { return _CheckForDataExistance; }
            set { _CheckForDataExistance = value; }
        }

        public TextBoxMode TextMode;
        public TextBox InnerTextBox
        {
            get
            {
                if (_InnerTextBox == null)
                {
                    _InnerTextBox = new TextBox();
                    _InnerTextBox.ID = this.FieldName;                     
                }
                return _InnerTextBox;
            }
        }
        private TextBox _InnerTextBox;

        public bool Visible { get; set; }

        private AxfFieldValueDBChecker _ValueDBChecker;
        /// <summary>
        /// Ottiene o imposta l'oggetto AxfFieldValueDBChecker ???
        /// </summary>
        public AxfFieldValueDBChecker ValueDBChecker
        {
            get { return _ValueDBChecker; }
            set { _ValueDBChecker = value; }
        }	

        private bool _AddComfirmField;
        /// <summary>
        /// Ottiene o imposta un parametro che indica se prima di eseguire la post del 
        /// form richiedere la conferma d'inserimento/modifica
        /// </summary>
        public bool AddComfirmField
        {
            get { return _AddComfirmField; }
            set { _AddComfirmField = value; }
        }	

        private int _Rows = 1;
        
        /// <summary>
        /// Ottiene o imposta le righe del campo.
        /// </summary>
        public int Rows
        {
            get { return _Rows; }
            set { _Rows = value; }
        }

        protected bool Multiline
        {
            get
            {
                return _Rows > 1;
            }
        }
        /// <summary>
        /// Ottiene o imposta il tipo di valore da interpretare.
        /// </summary>
        public ContentTypes ContentType
        {
            get
            {
                return _ContentType;
            }
            set
            {
                _ContentType = value;
            }
        }
        private ContentTypes _ContentType;

        /// <summary>
        /// Elenco delle tipologia di valori accettati
        /// </summary>
        public enum ContentTypes
        {
            NormalText = 0,
            eMail = 1,
            Phone = 2,
            Numeric = 3,
            File = 4,
            Folder = 5,
            Floating = 6,
            Valuta = 7,
            CodiceFiscale=8,
            AlphaNumeric= 9,
            Url = 10
        }

        /// <summary>
        /// Ottiene o imposta il valore del campo
        /// </summary>
        public override string Value
        {
            get
            {
                return base.Value;
            }
            set
            {
                if(LoadValue)
                    base.Value = value;
            }
        }

        public int MaxLenght {get ; set; }
        public int MinLenght { get; set; }
        public int Size { get; set; }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="label">Etichetta testuale del campo: apparir� nel form</param>
        /// <param name="fieldname">Rappresenta l'identificativo (ID) del campo nella form. 
        /// Generalmente si usa per mappare il campo corrispondente a quello del database</param>


        /// <summary>
        /// Inizializza una nuova istanza del campo testo
        /// </summary>
        /// <param name="label">Etichetta testuale del campo: apparir� nel form</param>
        /// <param name="fieldname">Rappresenta l'identificativo (ID) del campo nella form. 
        /// Generalmente si usa per mappare il campo corrispondente a quello del database</param>
        public AxfTextBox(string label, string fieldname)
            : this(label, fieldname,ContentTypes.NormalText)
        {
            _ValueDBChecker = new AxfFieldValueDBChecker(this);
        }


        /// <summary>
        /// Inizializza una nuova istanza del campo testo
        /// </summary>
        /// <remarks>
        /// Si usa quando si desidera indicare la tipologia di dato da inserire
        /// </remarks>
        /// <param name="label">Etichetta testuale del campo: apparir� nel form</param>
        /// <param name="fieldname">Rappresenta l'identificativo (ID) del campo nella form. 
        /// Generalmente si usa per mappare il campo corrispondente a quello del database</param>
        /// <param name="type">Tipologia di dato da inserire</param>
        public AxfTextBox(string label, string fieldname,ContentTypes type)
            : base(label, fieldname)
        {
            ContentType = type;
            _ValueDBChecker = new AxfFieldValueDBChecker(this);
            Required = false;
            MaxLenght = 255;
            MinLenght = 0;
            Size = 60;
            Visible = true;
        }

        /// <summary>
        /// Metodo che verifca la corretta sintassi del campo in funzione alla tipologia indicata.
        /// </summary>
        /// <param name="input">Valore del campo</param>
        /// <returns>Restituisce gli eventuali errori rilevati all'interno di una o pi� stringhe html "li"</returns>
        protected override string checkInput(string input)
        {
            string errors = string.Empty;

            input = input.Trim();
            if (input.Length > 0)
            switch (ContentType)
            {
                case ContentTypes.eMail:
                {
                    #region eMail
		
                    Regex regex = new Regex(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$",
                               RegexOptions.IgnorePatternWhitespace);

                    if (!regex.IsMatch(input))
                    {
                        errors += "<li>";
                        errors += string.Format(this.Labels[AxfLabels.LabelsList.CampoEmailNonValido], this.Label);
                        errors += "</li>";
                    }
                    break;
	                #endregion
                }
                case ContentTypes.File:
                {
                    #region File
                    bool ok = true;
                    string[] chars = { "#", "@", "%", "=", "&", "/", "\\", "*", "?", ":", "|", "<", ">", "\"" };
                    for (int i = 0; i < chars.Length && ok; i++)
                        if (input.Contains(chars[i])) ok = false;
                    if (!ok)
                    {
                        errors += "<li>";
                        errors += string.Format(this.Labels[AxfLabels.LabelsList.CampoFileNonValido], this.Label);
                        errors += "</li>";
                    }
                    break;
                    #endregion
                }
                case ContentTypes.Folder:
                {
                    #region Folder
/*
                    bool ok = true;
                    string[] chars = { "#", "@", "%", "=", "&", ".", "/", "\\", "*", "?", ":", "|", "<", ">", "\"" };
                    for (int i = 0; i < chars.Length && ok; i++)
                        if (input.Contains(chars[i])) ok = false;
            

                    if (!ok)
                    {
                        errors += "<li>";
                        errors += string.Format(this.Labels[AxfLabels.LabelsList.CampoFileNonValido], this.Label);
                        errors += "</li>";
                    }*/
                    break;
                    #endregion
                }
                case ContentTypes.Phone:
                {
                    #region Phone
                    Regex regex = new Regex(@"^([0-9]{8,12})$",
                                               RegexOptions.IgnorePatternWhitespace);

                    if (!regex.IsMatch(input))
                    {
                        errors += "<li>";
                        errors += string.Format(this.Labels[AxfLabels.LabelsList.CampoTelefonoNonValido], this.Label);
                        errors += "</li>";
                    }
                    break;
                    #endregion
                }                
                case ContentTypes.Numeric:
                {
                    #region Numeric

                    Regex regex = new Regex(@"^([0-9]*)$",
                    RegexOptions.IgnorePatternWhitespace);

                    if (!regex.IsMatch(input))
                    {
                        errors += "<li>";
                        errors += string.Format(this.Labels[AxfLabels.LabelsList.CampoNumericoNonValido], this.Label);
                        errors += "</li>";
                    }
                    break;
                    #endregion
                }
                case ContentTypes.Floating:
                {
                    #region Floating

                    Regex regex = new Regex(@"^[-+]?\s*\d+((\.|,)\d+)?\s*$",
                    RegexOptions.IgnorePatternWhitespace);

                    if (!regex.IsMatch(input))
                    {
                        errors += "<li>";
                        errors += string.Format(this.Labels[AxfLabels.LabelsList.CampoNumericoNonValido], this.Label);
                        errors += "</li>";
                    }

                    break;
                    #endregion
                }
                case ContentTypes.Valuta:
                {
                    #region Valuta

                    Regex regex = new Regex(@"^\s*\d+((\.|,)\d+)?\s*$",
                    RegexOptions.IgnorePatternWhitespace);

                    if (!regex.IsMatch(input))
                    {
                        errors += "<li>";
                        errors += string.Format(this.Labels[AxfLabels.LabelsList.CampoValutaNonValido], this.Label);
                        errors += "</li>";
                    }

                    break;
                    #endregion
                }
                case ContentTypes.CodiceFiscale:
                {
                    #region CodiceFiscale

                    //Regex regex = new Regex(@"\w{16}", RegexOptions.IgnorePatternWhitespace);

                    Regex regex = new Regex(@"^[A-Za-z]{6}[0-9]{2}[A-Za-z][0-9]{2}[A-Za-z][0-9]{3}[A-Za-z]$", RegexOptions.IgnorePatternWhitespace);
                    

                    if (!regex.IsMatch(input))
                    {
                        errors += "<li>";
                        errors += string.Format(this.Labels[AxfLabels.LabelsList.CampoCodiceFiscaleNonValido], this.Label);
                        errors += "</li>";
                    }

                    break;
                    #endregion
                }
                case ContentTypes.AlphaNumeric:
                {
                    #region AlphaNumeric
                    //Regex regex = new Regex(@"^([0-9a-zA-Z]*)$",
                    //RegexOptions.IgnorePatternWhitespace);
                    //Regex regex = new Regex(@"^(?=.*[a-zA-Z0-9]+.*)",
                    //RegexOptions.IgnorePatternWhitespace);

                    Regex regex = new Regex(@"^[a-zA-Z0-9\s]+$",
                    RegexOptions.IgnorePatternWhitespace);                    
                    if (!regex.IsMatch(input))
                    {
                        errors += "<li>";
                        errors += string.Format(this.Labels[AxfLabels.LabelsList.CampoAlphaNumericoNonValido], this.Label);
                        errors += "</li>";
                    }
                    break;
                    #endregion
                }
                case ContentTypes.Url:
                {
                    #region Url

                    string urlPattern = @"^(((ht|f)tp(s?))\://)?((([a-zA-Z0-9_\-]{2,}\.)+[a-zA-Z]{2,})|((?:(?:25[0-5]|2[0-4]\d|[01]\d\d|\d?\d)(?(\.?\d)\.)){4}))(:[a-zA-Z0-9]+)?(/[a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~]*)?$";
                    Regex regex = new Regex(urlPattern, RegexOptions.IgnorePatternWhitespace);
                    if (!regex.IsMatch(input))
                    {
                        errors += "<li>";
                        errors += string.Format(this.Labels[AxfLabels.LabelsList.CampoUrlNonValido], this.Label);
                        errors += "</li>";
                    }
                    break;
                    #endregion
                }
            }
            //^(?=.*[a-zA-Z0-9]+.*)

            //Regex regex1 = new Regex(@"^(?=.*[a-zA-Z0-9]+.*)", RegexOptions.IgnorePatternWhitespace);
            //Regex regex1 = new Regex(@"^[a-zA-Z0-9\s]+$", RegexOptions.IgnorePatternWhitespace);
            //if (input.Length > 0 && !regex1.IsMatch(input))
            //{
            //    if (!errors.Contains(string.Format(this.Labels[AxfLabels.LabelsList.CampoAlphaNumericoNonValido], this.Label)))
            //    {
            //        errors += "<li>";
            //        errors += string.Format(this.Labels[AxfLabels.LabelsList.CampoAlphaNumericoNonValido], this.Label);
            //        errors += "</li>";
            //    }
            //}

            if (Required && input.Length == 0)
            {
                errors += "<li>";
                errors += string.Format(this.Labels[AxfLabels.LabelsList.IlCampoEObbligatorio], this.Label);
                errors += "</li>";
            }

            if (!string.IsNullOrEmpty(input) && MaxLenght>0 && MinLenght == MaxLenght && input.Length != MinLenght)
            {
                errors += "<li>";
                errors += string.Format(this.Labels[AxfLabels.LabelsList.LunghezzaEsattaCampo], this.Label, MaxLenght);
                errors += "</li>";
            }

            if (MinLenght != MaxLenght && MinLenght > 0 && input.Length > 0 && input.Length < MinLenght)
            {
                errors += "<li>";
                errors += string.Format(this.Labels[AxfLabels.LabelsList.LunghezzaMinimaCampo], this.Label, MinLenght.ToString());
                errors += "</li>";
            }
            if (MinLenght != MaxLenght && MaxLenght > 0 && input.Length > MaxLenght)
            {
                errors += "<li>";
                errors += string.Format(this.Labels[AxfLabels.LabelsList.LunghezzaMassimaCampo], this.Label,MaxLenght.ToString());
                errors += "</li>";
            }

            if (this.CheckForDataExistance && !string.IsNullOrEmpty(input) && ValueDBChecker.DataExist(input))
            {
                errors += "<li>";
                errors += string.Format(this.Labels[AxfLabels.LabelsList.DatiRipetuti], this.Label);
                errors += "</li>";
            }

            if (this.AddComfirmField)
            {
                G2Core.Common.RequestVariable confirm = new G2Core.Common.RequestVariable(this.FieldName + "_Confirm",this.CurrentRequest);
                if (confirm.StringValue != this.RequestVariable.StringValue)
                {
                    errors += "<li>";
                    errors += string.Format(this.Labels[AxfLabels.LabelsList.CampiNonCoincidenti], this.Label, string.Format(this.Labels[AxfLabels.LabelsList.Confirm], this.Label));
                    errors += "</li>";
                }
            }

            return errors;

        }

        /// <summary>
        /// Restituisce il valore del campo strutturato per il database
        /// </summary>
        /// <param name="value">Valore del campo</param>
        /// <returns>Restituisce il valore del campo strutturato per il database
        /// applicando controlli di sql injection</returns>
        public override string getFilter(string value)
        {
            string filter = " ";
            value = value.Replace("'", "''").Trim();
            if (value.Length > 0)
            {
                filter += _FieldName;
                
                if (ContentTypes.Numeric == ContentType || ContentTypes.Floating == ContentType)               
                {
                    if (ContentTypes.Floating == ContentType)
                    {
                        value = value.Replace(",", ".");
                    }
                    filter += " = ";
                    filter += value;
                    return filter;
                }
                else
                    if (ContentTypes.Phone == ContentType || ContentTypes.eMail == ContentType || ContentTypes.Valuta == ContentType)
                {
                    filter += " = '";
                    filter += value;
                    filter += "'";
                    return filter;
                }

                filter += " Like '%";
                filter += value;
                filter += "%'";
            }
            return filter;
        }


        /// <summary>
        /// Restituisce il valore del campo strutturato per il database
        /// </summary>
        /// <param name="value">Valore del campo</param>
        /// <returns>Restituisce il valore del campo strutturato per il database
        /// applicando controlli di sql injection</returns>
        public override string  filterValueForDB(string value)
        {
            value = value.Trim();
            
            if (ContentTypes.Valuta == this.ContentType)
                return "'" + value.Replace(",", ".") + "'";
            
            if (ContentTypes.Floating == this.ContentType)
                return value.Replace(",", ".");

            if (ContentTypes.Numeric == this.ContentType)
                return (value.Length == 0) ? null : value;

            return value = "'" + value.Replace("'", "''") + "'";
        }
        
        /// <summary>
        /// Restituisce il controllo xhtml relativo al campo 
        /// </summary>
        /// <returns>Un HtmlGenericControl contenente tutti i controlli del campo</returns>
        protected override HtmlGenericControl getFieldContent()
        {
            HtmlGenericControl control = new HtmlGenericControl("div");
            control.Controls.Add(this.BuildBox( this.Label));
            return control;
        }

        private HtmlGenericControl BuildBox(string label)
        {
            HtmlGenericControl span = new HtmlGenericControl("span");
            if(Visible)
                span.Attributes["class"] = "Axf_FieldBox";          
            Label lb = new Label();
            lb.AssociatedControlID = this.FieldName;
            if (Required)
                lb.Text = label + "*";
            else
                lb.Text = label;
            lb.Attributes["class"] = "formlabel";
            if (lb.Text != "")
                span.Controls.Add(lb);

            InnerTextBox.ID = this.FieldName;
            InnerTextBox.Columns = Size;            
            InnerTextBox.TextMode = TextMode;

            if (ReadOnly)
            {
                InnerTextBox.ReadOnly = true;
                InnerTextBox.CssClass = "readonly";                
            }


            if (OnChange != null)
                InnerTextBox.Attributes["onchange"] = OnChange;

            if (OnKeyup != null)
                InnerTextBox.Attributes["onkeyup"] = OnKeyup;

            if (Multiline)
            {
                InnerTextBox.TextMode = TextBoxMode.MultiLine;
                InnerTextBox.Rows = Rows;
            }

            InnerTextBox.Text = Value;
            InnerTextBox.Visible = Visible;
            lb.Visible = Visible;

            span.Controls.Add(InnerTextBox);
            return span;
        }
    }
}