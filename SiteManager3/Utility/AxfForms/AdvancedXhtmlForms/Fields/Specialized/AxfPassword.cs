using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;


namespace G2Core.AdvancedXhtmlForms.Fields
{ 
    /// <summary>
    /// La classe AxfPassword e implementa un controllo web per la inserimento di password.
    /// </summary>
    /// <remarks>
    /// Il controllo visualizza due campi testuali, uno per la password e l'altro la conferma.
    /// Inoltre consente la criptazione in MD5 o SHA1.
    /// </remarks>
    public class AxfPassword : AxfField
    {

        public int _MaxLenght = 255;
        /// <summary>
        /// Lunghezza massima del testo imputabile: default = 255.
        /// </summary>
        public int MaxLenght { get { return _MaxLenght; } set { _MaxLenght = value; } }

        public int _MinLenght = 8;
        /// <summary>
        /// Lunghezza minima del testo imputabile: default = 8.
        /// </summary>
        public int MinLenght { get { return _MinLenght; } set { _MinLenght = value; } }
        /// <summary>
        /// Indica la dimensione del campo
        /// </summary>
        public int Size = 60;
        
        public TextBox InnerTextBox
        {
            get
            {
                if (_InnerTextBox == null)
                {
                    _InnerTextBox = new TextBox();
                    _InnerTextBox.ID = this.FieldName;
                }
                return _InnerTextBox;
            }
        }
        private TextBox _InnerTextBox;

        /// <summary>
        /// Ottiene o imposta un parametro che indica se il sistema di validazione deve validare la nuova password con quella presente ne campo OldPassword
        /// </summary>
        public bool OldPasswordCheckWithoutShowOldPasswordField { get; set; }

        private bool _OldPasswordCheck;

        /// <summary>
        /// Ottiene o imposta un parametro che indica se visualizzare oltre i controlli testuali di default
        /// anche un ulteriore controllo testuale per la vecchia password.
        /// </summary>
        public bool OldPasswordCheck
        {
            get { return _OldPasswordCheck; }
            set { _OldPasswordCheck = value; }
        }
        /// <summary>
        /// Ottiene o imposta il valore della vecchia password
        /// </summary>
        private string _OldPassword;
        public string OldPassword
        {
            get { return _OldPassword; }
            set { _OldPassword = value; }
        }
        /// <summary>
        /// Ottine o imposta un parametro che indica se utilizzare la modalit� alta sicurezza.
        /// </summary>
        /// <remarks>
        /// Questo parametro indica che la password utilizza utilizza una sintassi contenente caratteri alfanumerice e almeno un carattere maiuscolo.
        /// </remarks>
        private bool _HighSecurityField;
        public bool HighSecurityField
        {
            get { return _HighSecurityField; }
            set { _HighSecurityField = value; }
        }
      
        /// <summary>
        /// Ottiene o imposta un parametro che indica se visualizzare il testo inserito nel campo dopo il Post del form.
        /// </summary>
        private bool _RememberPostbackValue;
        public bool RememberPostbackValue
        {
            get { return _RememberPostbackValue; }
            set { _RememberPostbackValue = value; }
        }

        /// <summary>
        /// Indica se ignorare il campo in fase di scrittura nel database.
        /// </summary>
        public override bool JumpField
        {
            get
            {
                if (!Required)
                {
                    Common.RequestVariable pwd = new Common.RequestVariable(this.FieldName, HttpContext.Current.Request.Form);
                    Common.RequestVariable pwd2 = new Common.RequestVariable(this.FieldName + "_Confirm", HttpContext.Current.Request.Form);

                    return pwd.IsValid() && pwd2.IsValid() && pwd.StringValue == pwd2.StringValue && pwd.StringValue.Length == 0;
                }
                return false;
            }
        }

        /// <summary>
        /// Enumeratore che elenca gli algoritmi di criptazione che il controllo usa.
        /// </summary>
        public enum CyperingType
        {
            None,
            Default,
            MD5,
            SHA1
        }

        private CyperingType _CyperType;
        /// <summary>
        /// Imposta o ottiene il metodo di criptazione da usare
        /// </summary>
        public CyperingType CyperType
        {
            get { return _CyperType; }
            set { _CyperType = value; }
        }
        /// <summary>
        /// Restituisce il valore del campo
        /// </summary>
        public override string Value
        {
            get
            {
                return base.Value;
            }
            set
            {
                if(LoadValue)
                    base.Value = value;
            }
        }


        /// <summary>
        /// Inizializza una nuova istanza del campo 
        /// </summary>
        /// <param name="label">Etichetta testuale del campo: apparir� nel form</param>
        /// <param name="fieldname">Rappresenta l'identificativo (ID) del campo nella form. 
        /// Generalmente si usa per mappare il campo corrispondente a quello del database</param>
        public AxfPassword(string label, string fieldname)
            :base(label, fieldname)
        {
        }

        /// <summary>
        /// Verifica se il valore inserito soddisfa i criteri pre-impostati <see cref="G2Core.AdvancedXhtmlForms.Fields.AxfPassword.HighSecurityField"/>
        /// </summary>
        /// <param name="input">Valore da verificare</param>
        /// <returns>Messaggio di errore racchiuso all'interno di un tag html "li" </returns>
        protected override string checkInput(string input)
        {
            string errors = "";
            input = input.Trim();

            if (Required && input.Length == 0)
            {
                errors += "<li>";
                errors += string.Format(this.Labels[AxfLabels.LabelsList.IlCampoEObbligatorio], this.Label);
                errors += "</li>";
            }

            if (HighSecurityField && input.Length > 0)
            {
                string[] Big = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "L", "M", "N", "O", "P", "Q", "S", "R", "T", "U", "V", "Z", "X", "Y", "W", "J", "K" };
                string[] Small = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "l", "m", "n", "o", "p", "q", "s", "r", "t", "u", "v", "z", "x", "y", "w", "j", "k" };
                string[] Num = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

                bool ok = true;

                if (ok)
                {
                    bool found = false;
                    for (int i = 0; i < Big.Length && !found; i++)
                    {
                        if (input.Contains(Big[i])) found = true;
                    }
                    ok = found;
                }

                if (ok)
                {
                    bool found = false;
                    for (int i = 0; i < Small.Length && !found; i++)
                    {
                        if (input.Contains(Small[i])) found = true;
                    }
                    ok = found;
                }

                if (ok)
                {
                    bool found = false;
                    for (int i = 0; i < Num.Length && !found; i++)
                    {
                        if (input.Contains(Num[i])) found = true;
                    }
                    ok = found;
                }
                if (!ok)
                {
                    errors += "<li>";
                    errors += string.Format(this.Labels[AxfLabels.LabelsList.FormatoPasswordNonCorretto], this.Label);
                    errors += "</li>";
                }
            }

            if (MaxLenght != 0 && input.Length > MaxLenght)
            {
                errors += "<li>";
                errors += string.Format(this.Labels[AxfLabels.LabelsList.LunghezzaMassimaCampo], this.Label, MaxLenght);
                errors += "</li>";
            }

            if (MinLenght > input.Length && (Required || input.Length > 0))
            {
                errors += "<li>";
                errors += string.Format(this.Labels[AxfLabels.LabelsList.LunghezzaMinimaCampo], this.Label, MinLenght);
                errors += "</li>";
            }

            Common.RequestVariable pwd = new Common.RequestVariable(this.RequestKey, HttpContext.Current.Request.Form);
            Common.RequestVariable pwd2 = new Common.RequestVariable(this.RequestKey + "_Confirm", HttpContext.Current.Request.Form);

            if ((OldPasswordCheck && OldPassword != null))
            {
                bool ok = false;
                bool oldEqualNew = false;
                Common.RequestVariable old_pwd = new Common.RequestVariable(this.RequestKey + "_Old", HttpContext.Current.Request.Form);
                if (old_pwd.IsValid())
                {
                    if (CyperFilter(old_pwd.StringValue) == OldPassword)
                        ok = true;
                    if (pwd.IsValid() && CyperFilter(old_pwd.StringValue) == CyperFilter(pwd.StringValue))
                    {
                        ok = false;
                        oldEqualNew = true;
                    }
                }
                if (!ok)
                {
                    errors += "<li>";
                    if (!oldEqualNew)
                        errors += Labels[AxfLabels.LabelsList.VecchiaPasswordErrata];
                    else
                        errors += Labels[AxfLabels.LabelsList.VecchiaPasswordNuovaPasswordCoincidenti];
                    errors += "</li>";
                }
            }
            else
            if (OldPasswordCheckWithoutShowOldPasswordField && OldPassword != null)
            {
                bool oldEqualNew = CyperFilter(pwd.StringValue) == OldPassword;
                if (oldEqualNew)
                {
                    errors += "<li>";
                    errors += "La nuova password non pu� coincidere con la password attuale.";
                    errors += "</li>";
                }
            }

            if (!pwd.IsValid() || !pwd2.IsValid())
            {
                errors += "<li>";
                errors += Labels[AxfLabels.LabelsList.PasswordNonValide];
                errors += "</li>";
            }
            else
            {
                if (pwd.StringValue != pwd2.StringValue)
                {
                    errors += "<li>";
                    errors += string.Format(Labels[AxfLabels.LabelsList.PasswordNonCoincidenti], Label);
                    errors += "</li>";
                }
            }


            return errors;

        }


        /// <summary>
        /// Restituisce il valore del campo strutturato per il database
        /// </summary>
        /// <param name="value">Valore del campo</param>
        /// <returns>Restituisce il valore del campo strutturato per il database
        /// applicando controlli di sql injection</returns>
        public override string getFilter(string value)
        {
            string filter = " ";

            if (CyperType != CyperingType.None)
                filter = this.CyperFilter(value);

            filter = filter.Replace("'", "''");

            return filter;
        }

        

        public override string  filterValueForDB(string value)
        {
            string output = value.Trim();

            if (CyperType != CyperingType.None)
                output = this.CyperFilter(output);

            output = "'" + output + "'";

            return output;
        }

        /// <summary>
        /// Restituisce il controllo xhtml relativo al campo 
        /// </summary>
        /// <returns>Un HtmlGenericControl contenente tutti i controlli del campo</returns>
        protected override HtmlGenericControl getFieldContent()
        {
            HtmlGenericControl par = new HtmlGenericControl("div");
            if (this.CssClass != string.Empty)
                par.Attributes["class"] = this.CssClass;

            Label lb;
            TextBox textBoxOld;

            if (OldPasswordCheck)
            {
                lb = new Label();
                lb.AssociatedControlID = FieldName;
                if (Required)
                    lb.Text = string.Format(Labels[AxfLabels.LabelsList.Vecchia], this.Label) + "*";
                else
                    lb.Text = string.Format(Labels[AxfLabels.LabelsList.VecchiaLasciareIlCampoVuoto], this.Label);
                lb.Attributes["class"] = "formlabel";
                par.Controls.Add(lb);

                textBoxOld = new TextBox();
                textBoxOld.ID = FieldName + "_Old";
                textBoxOld.Columns = Size;
                textBoxOld.TextMode = TextBoxMode.Password;

                par.Controls.Add(textBoxOld);
            }


            lb = new Label();
            lb.AssociatedControlID = FieldName;
            if (Required)
                lb.Text = Label + "*";
            else
                lb.Text = Label + this.Labels[AxfLabels.LabelsList.LasciareIlCampoVuotoPerMantentenereQuellaAttuale];
            lb.Attributes["class"] = "formlabel";
            par.Controls.Add(lb);

            InnerTextBox.ID = FieldName;
            InnerTextBox.Columns = Size;
            InnerTextBox.TextMode = TextBoxMode.Password;

            if (this.CyperType != CyperingType.None)
                InnerTextBox.Text = Value;

            par.Controls.Add(InnerTextBox);

            TextBox textBoxConfirm;

            lb = new Label();
            lb.AssociatedControlID = FieldName + "_Confirm";
            lb.Text = this.Labels[AxfLabels.LabelsList.Conferma] + " " + Label;
            if (Required)
                lb.Text += "*";
            else
                lb.Text += " " + this.Labels[AxfLabels.LabelsList.LasciareIlCampoVuotoPerMantentenereQuellaAttuale];
            lb.Attributes["class"] = "formlabel";

            par.Controls.Add(lb);

            textBoxConfirm = new TextBox();
            textBoxConfirm.ID = FieldName + "_Confirm";
            textBoxConfirm.Columns = Size;
            textBoxConfirm.TextMode = TextBoxMode.Password;

            if (this.CyperType != CyperingType.None)
                textBoxConfirm.Text = Value;

            par.Controls.Add(textBoxConfirm);

            if (RememberPostbackValue && this.checkInput(this.RequestVariable.StringValue).Length == 0)
            {
                textBoxConfirm.Text = this.RequestVariable.StringValue;
                InnerTextBox.Text = this.RequestVariable.StringValue;
            }

            return par;
        }


        private string CyperFilter(string input)
        {
            string strCyper = null;
            switch (_CyperType)
            {
                case CyperingType.None:
                    strCyper = FormsAuthentication.HashPasswordForStoringInConfigFile(input, "MD5").ToString();
                    break;
                case CyperingType.Default:
                    strCyper = FormsAuthentication.HashPasswordForStoringInConfigFile(input, "MD5").ToString();
                    break;
                case CyperingType.MD5:
                    strCyper = FormsAuthentication.HashPasswordForStoringInConfigFile(input, "MD5").ToString();
                    break;
                case CyperingType.SHA1:
                    strCyper = FormsAuthentication.HashPasswordForStoringInConfigFile(input, "SHA1").ToString();
                    break;
            }
            return strCyper;
        }
    }
}