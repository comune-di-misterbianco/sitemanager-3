using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


namespace G2Core.AdvancedXhtmlForms.Fields
{ 
    /// <summary>
    /// La classe AxfDateField implementa un controllo casella di testo che accetta esclusivamente date secondo il formato gg/mm/aaaa.
    /// </summary>
    public class AxfDateField : AxfField
    {
        public enum SearchTypes { Equal = 0, Major = 1, Minor = 2 };
        private SearchTypes _SearchType;

        public SearchTypes SearchType
        {
            get { return _SearchType; }
            set { _SearchType = value; }
        }
        private NetCms.Connections.Connection Conn;
        /// <summary>
        /// Ottiene o imposta un Valore del contenuto del campo
        /// </summary>
        public override string Value
        {
            get
            {
                return base.Value;
            }
            set
            {
                if(LoadValue)
                    base.Value = value;
            }
        }
        private DateTime _MinDate;
        public DateTime MinDate
        {
            get { return _MinDate; }
            set { _MinDate = value; }
        }

        private DateTime _MaxDate;
        public DateTime MaxDate
        {
            get { return _MaxDate; }
            set { _MaxDate = value; }
        }

        public TextBox InnerTextBox
        {
            get
            {
                if (_InnerTextBox == null)
                {
                    _InnerTextBox = new TextBox();
                    _InnerTextBox.ID = this.FieldName;
                }
                return _InnerTextBox;
            }
        }
        private TextBox _InnerTextBox;

        public int Size = 60;
        /// <summary>
        /// Inizializza una nuova istanza del campo data AxfDateField
        /// </summary>
        /// <param name="label">Etichetta testuale del campo: apparir� nel form</param>
        /// <param name="fieldname">Rappresenta l'identificativo (ID) del campo nella form. 
        /// Generalmente si usa per mappare il campo corrispondente a quello del database</param>
        /// <param name="conn">Connessione da utilizzare per accedere al database<c>NetCms.Connections.Connection</c></param>
        public AxfDateField(string label, string fieldname,NetCms.Connections.Connection conn)
            : this(label, fieldname,conn, SearchTypes.Equal)
        {
           
        }
        /// <summary>
        /// Inizializza una nuova istanza del campo data AxfDateField (overload)
        /// </summary>
        /// <remarks>Generalmente si impiega nelle form di ricerca</remarks>
        /// <param name="label">Etichetta testuale del campo: apparir� nel form</param>
        /// <param name="fieldname">Rappresenta l'identificativo (ID) del campo nella form. 
        /// Generalmente si usa per mappare il campo corrispondente a quello del database</param>
        /// <param name="conn">Connessione al database<c>NetCms.Connections.Connection</c></param>
        /// <param name="type">Criterio di comparazione: Uguale a, Maggiore di, Minore di </param>
        public AxfDateField(string label, string fieldname, NetCms.Connections.Connection conn, SearchTypes type)
            : base(label, fieldname)
        {
            SearchType = type;
            Required = false;
            Conn = conn;         
        }

        /// <summary>
        /// Verifica se il valore inserito soddisfa i criteri pre-impostati.
        /// </summary>
        /// <param name="input">Valore da verificare</param>
        /// <returns>Messaggio di errore racchiuso all'interno di un tag html "li" </returns>
        protected override string checkInput(string input)
        {
            string errors = string.Empty;

            input = input.Trim();
            if (Required)
            {
                if (input.Length == 0)
                {
                    errors += "<li>";
                    errors += string.Format(Labels[AxfLabels.LabelsList.IlCampoEObbligatorio], this.Label);
                    errors += "</li>";
                }
            }
            if (input.Length > 0)
            {
                DateTime date;
                if (!DateTime.TryParse(input, out date))
                {
                    errors += "<li>";
                    errors += string.Format(Labels[AxfLabels.LabelsList.IlCampoNonEValido], this.Label);
                    errors += "</li>";
                }
                else if (MinDate != DateTime.MinValue && DateTime.Compare(date, MinDate) < 0)
                {
                    errors += "<li>";
                    errors += String.Format(this.Labels[AxfLabels.LabelsList.DataSuccessivaAl], Label, MinDate);
                    errors += "</li>";
                }
                else if (MaxDate != DateTime.MinValue && DateTime.Compare(date, MaxDate) > 0)
                {
                    errors += "<li>";
                    errors += String.Format(this.Labels[AxfLabels.LabelsList.DataAntecedenteAl], Label, MaxDate);
                    errors += "</li>";
                }
            }
            return errors;

        }

        /// <summary>
        /// Restituisce il valore del campo filtrato in funzione del parametro di comparazione
        /// </summary>
        /// <param name="value">Valore del campo</param>
        /// <returns>Restituisce il valore del campo filtrato</returns>
        public override string getFilter(string value)
        {
            value = value.Trim();
            string filter = " ";
            if (value.Length > 0)
            {
                filter += FieldName;
                filter += " " + (SearchType == SearchTypes.Equal ? "=" : (SearchType == SearchTypes.Major ? ">" : "<"));
                if (Conn != null)
                    filter += Conn.FilterDate(value);
                else
                    filter += " '" + value + "'";

                return filter;
            }
            return filter;
        }
        

        /// <summary>
        /// Restituisce il valore del campo filtrato in modo appropriato per l'inserimento nel database.
        /// </summary>
        /// <param name="value">Valore del campo</param>
        /// <returns>Restituisce il valore del campo formattato secondo la sintassi accettata dal database in uso</returns>
        public override string  filterValueForDB(string value)
        {
            string output = value.Trim();
            output = output.Replace("'", "''");
            output = output.Trim();
            if (Conn != null)
                output = Conn.FilterDate(output);
            else
            {
                DateTime date;
                if (DateTime.TryParse(output, out date))
                    output = "'" + (date.Year + "-" + date.Month + "-" + date.Day + " " + date.Hour + "." + date.Minute + "." + date.Second) + "'";
                else
                    output = "'" + (output) + "'";
            }
            return output;
        }

        /// <summary>
        /// Restituisce il controllo xhtml relativo al campo 
        /// </summary>
        /// <returns>Un HtmlGenericControl contenente tutti i controlli del campo</returns>
        protected override HtmlGenericControl getFieldContent()
        {
            HtmlGenericControl control = new HtmlGenericControl("div");
            control.Controls.Add(this.BuildBox(this.FieldName, this.Label));
            return control;
        }

        /// <summary>
        /// Metodo che assembla gli elementi che costituiscono il campo AxfDateField
        /// </summary>
        /// <param name="id">Riferimento del </param>
        /// <param name="label"></param>
        /// <returns></returns>
        private HtmlGenericControl BuildBox(string id,string label)
        {
            HtmlGenericControl span = new HtmlGenericControl("span");
            span.Attributes["class"] = "Axf_FieldBox";
            Label lb = new Label();
            lb.AssociatedControlID = id;
            if (Required)
                lb.Text = label + "*";
            else
                lb.Text = label;
            lb.Attributes["class"] = "formlabel";
            span.Controls.Add(lb);

            TextBox textBox = new TextBox();
            textBox.ID = id;
            textBox.Columns = Size;

            if (OnChange != null)
                textBox.Attributes["onchange"] = OnChange;

            if (OnKeyup != null)
                textBox.Attributes["onkeyup"] = OnKeyup;

            DateTime date;
            //if (DateTime.TryParse(Value, out date))
            //{
            //    textBox.Text = date.Day + "/" + date.Month + "/" + date.Year;

            //}
            //else
            //{
            string ReqValue = "";
            if (HttpContext.Current.Request.Form[this.RequestKey] != null)
            {
                ReqValue = HttpContext.Current.Request.Form[this.RequestKey].ToString();
                //    if (HttpContext.Current.Session[this.RequestKey] == null)
                //        HttpContext.Current.Session.Add(this.RequestKey, ReqValue);
                //}


                //if (HttpContext.Current.Session[this.RequestKey] != null)
                textBox.Text = ReqValue;//HttpContext.Current.Session[this.RequestKey].ToString();
            }
            else
                textBox.Text = Value;
            InnerTextBox.Text = textBox.Text;
            
            
            //}
            span.Controls.Add(textBox);

            
            return span;
        }
    }
}

/*
 using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using NetConnections;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetDateField2 : NetField
    {
        public enum SearchTypes { Equal = 0, Major = 1, Minor = 2};

        private SearchTypes _SearchType;

        public SearchTypes SearchType
        {
            get { return _SearchType; }
            set { _SearchType = value; }
        }
	
        private Connection Conn;
        public override string Value
        {
            set
            {
                if (value.Length > 10)
                    _Value = value.Remove(10);
                else
                    _Value = value;
            }
            get { return _Value; }
        }

        private int _Size;
        public int Size
        {
            get { return _Size; }
            set { _Size = value; }
        }
	

        public NetDateField2(string label, string fieldname)
            : base(label, fieldname)
        {
            Required = false;
            _Size = 60;
        }
        public NetDateField2(string label, string fieldname, Connection conn)
            : base(label, fieldname)
        {
            Required = false;
            _Size = 60;
            Conn = conn;
        }

        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl par = new HtmlGenericControl(DefaultControlTag);
            par.Attributes["class"] = DefaultControlTag + "_" + _FieldName;

            Label lb = new Label();
            lb.AssociatedControlID = _FieldName;
            if (Required)
                lb.Text = _Label + " " + Labels[NetFormLabels.NetFormLabelsList.FormatoData] + "*";
            else
                lb.Text = _Label + " " + Labels[NetFormLabels.NetFormLabelsList.FormatoData];
            lb.Attributes["class"] = "formlabel";
            par.Controls.Add(lb);

            TextBox textBox = new TextBox();
            textBox.ID = _FieldName;
            textBox.Columns = Size;
            textBox.Text = Value;

            par.Controls.Add(textBox);

            return par;
        }

        public override String getFieldName()
        {
            return _FieldName;
        }

        public override string validateInput(string input)
        {
            string errors = "";

            DateTime date;
            if (Required)
            {
                if (input.Length == 0)
                {
                    errors += "<li>";
                    errors += string.Format(Labels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio],this.Label);
                    errors += "</li>";
                }
                else
                    if (!DateTime.TryParse(input, out date))
                    {
                        errors += "<li>";
                        errors += string.Format(Labels[NetFormLabels.NetFormLabelsList.IlCampoNonEValido], this.Label);
                        errors += "</li>";
                    }
            }
            return errors;
        }

        public override string getFilter(string value)
        {
            value = value.Trim();
            string filter = " ";
            if (value.Length > 0)
            {
                filter += FieldName;
                filter += " " + (SearchType == SearchTypes.Equal ? "=" : (SearchType == SearchTypes.Major ? ">" : "<")) ;
                if (Conn != null)
                    filter += Conn.FilterDate(value);
                else
                    filter += " '" +value+"'";
                
                return filter;
            }
            return filter;
        }

        public override string validateValue(string value)
        {
            string output = value.Trim();
            output = output.Replace("'", "''");
            output = output.Trim();
            if (Conn != null)
                output = Conn.FilterDate(output);
            else
            {
                DateTime date;
                if (DateTime.TryParse(output, out date))
                    output = "'" + (date.Year + "-" + date.Month + "-" + date.Day + " " + date.Hour + "." + date.Minute + "." + date.Second) + "'";
                else
                    output = "'" + (output) + "'";
            }
            return output;
        }
    }

}
 */