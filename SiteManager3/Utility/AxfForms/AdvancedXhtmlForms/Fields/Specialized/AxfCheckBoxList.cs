﻿using System;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace G2Core.AdvancedXhtmlForms.Fields
{
    public class AxfCheckBoxList : AxfField
    {
       
        public CheckBoxList InnerCheckBoxList
        {
            get
            {                
                return _InnerCheckBoxList;
            }
        }
        private CheckBoxList _InnerCheckBoxList;

        private bool _ValidValue = false;
        public bool ValidValue
        {
            get { return _ValidValue; }
            set { _ValidValue = value; }
        }

        public override bool ShowInInOverview { get { return false; } }       

        private bool _AutoPostBack;
        public bool AutoPostBack
        {
            get
            {
                return _AutoPostBack;
            }
            set
            {
                _AutoPostBack = value;
            }
        }

        private string _OnClientClick;
        public string OnClientClick
        {
            get
            {
                if (_OnClientClick == null)
                {
                    _OnClientClick = "setSubmitSource('" + FieldName + "')";
                }
                return _OnClientClick;
            }
            set { _OnClientClick = value; }
        }

        private bool init = false;

        public AxfCheckBoxList(string label, string fieldname, bool repeathorizontal)
            : base(label, fieldname)
        {
            _InnerCheckBoxList = new CheckBoxList();
            _InnerCheckBoxList.ID = this.FieldName;
            if (repeathorizontal)
            {
                InnerCheckBoxList.RepeatLayout = RepeatLayout.Flow;
                InnerCheckBoxList.RepeatDirection = RepeatDirection.Horizontal;
            }
        }

        protected override string checkInput(string input)
        {
            /*string errors = "";

            if (Required && input == string.Empty)
            {
                errors += "<li>";
                errors += string.Format(this.Labels[AxfLabels.LabelsList.IlCampoEObbligatorio], this.Label);
                errors += "</li>";
            }

            return errors;*/

            string errors = "";
            bool areAllEmpty = true;
            if (Required)
            {
                for (int i = 0; i < this.InnerCheckBoxList.Items.Count; i++)
                {
                    G2Core.Common.RequestVariable item = new G2Core.Common.RequestVariable(this.FieldName + "$" + i.ToString());
                    if (item.IsValidString)
                        areAllEmpty = false;
                    else
                        areAllEmpty = areAllEmpty && true;
                }
            }

            if (areAllEmpty && Required)
            {
                errors += "<li>";
                errors += string.Format(this.Labels[AxfLabels.LabelsList.IlCampoEObbligatorio], this.Label);
                errors += "</li>";
            }

            return errors;
        }

        public override string getFilter(string value)
        {
            string filter = " ";
            return filter;
        }

        public void addItem(string Label, string Value, bool selected)
        {
            ListItem item = new ListItem(Label, Value);
            item.Selected = selected;
            _InnerCheckBoxList.Items.Add(item);            
        }

        private bool _JumpField;
        public override bool JumpField
        {
            get
            {
                return _JumpField;
            }
            set
            {
                _JumpField = value;
            }
        }

        public override string filterValueForDB(string value)
        {
            if (init)
                return this.Value;

            if (!ValidValue)
                return base.filterValueForDB(value);
            else
                return value;
        }

        /// <summary>
        /// Restituisce il controllo xhtml relativo al campo 
        /// </summary>
        /// <returns>Un HtmlGenericControl contenente tutti i controlli del campo</returns>
        protected override HtmlGenericControl getFieldContent()
        {
            HtmlGenericControl control = new HtmlGenericControl("div");
            HtmlGenericControl rblControl = getControlSimple();                    

            if (Value.Length > 0)
            {
                foreach (ListItem item in _InnerCheckBoxList.Items)
                {
                    if (string.Compare(item.Value,Value,true)==0)
                    {
                        item.Selected = true;
                        _InnerCheckBoxList.SelectedIndex = _InnerCheckBoxList.Items.IndexOf(item);
                    }
                }
            }
            control.Controls.Add(rblControl);

            return control;
        }

        private HtmlGenericControl getControlSimple()
        {
            HtmlGenericControl par = new HtmlGenericControl("div");
            par.Attributes["class"] = "Axf_FieldBox";
            par.ID = _FieldName + "Div";

            Label lb = new Label();
            lb.AssociatedControlID = _FieldName;
            if (Required)
                lb.Text = _Label + "*";
            else
                lb.Text = _Label;

            lb.Attributes["class"] = "formlabel";

            par.Controls.Add(lb);
            _InnerCheckBoxList.AutoPostBack = AutoPostBack;
            if (AutoPostBack)
                _InnerCheckBoxList.Attributes["onchange"] = OnClientClick;           
            par.Controls.Add(_InnerCheckBoxList);
            return par;
        }

    }
}