﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Connections;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace G2Core.AdvancedXhtmlForms.Fields
{
    public class AxfDatePicker : AxfField
    {
        private NetCms.Connections.Connection Conn;
        private int year_rangeback = 100;
        private int year_rangeforward = 3;

        public DateTime minDate = DateTime.MinValue;
        public DateTime maxDate = DateTime.MaxValue;
                
        
        private string scriptString =@"<script language=""JavaScript"">         
        function SetDatePickerValue(hiddenID,ddldayID,ddlmonthID,ddlyearID)
        {
            var hidden = document.getElementById(hiddenID); 
            var ddlday = document.getElementById(ddldayID);  
            var ddlmonth = document.getElementById(ddlmonthID);  
            var ddlyear = document.getElementById(ddlyearID);
            hidden.value="""";
            if(ddlday.options[ddlday.selectedIndex].value.length>0)
                if(ddlmonth.options[ddlmonth.selectedIndex].value.length>0)  
                    if(ddlyear.options[ddlyear.selectedIndex].value.length>0)    
                        hidden.value=ddlday.options[ddlday.selectedIndex].value+""/""+ddlmonth.options[ddlmonth.selectedIndex].value+""/""+ddlyear.options[ddlyear.selectedIndex].value;  
    
        }
        </script>";

        private string _OnClientClick;
        public string OnClientClick
        {
            get
            {
                if (_OnClientClick == null)
                {
                    _OnClientClick = string.Format("SetDatePickerValue('{0}','{1}','{2}','{3}')", FieldName, FieldName + "Day", FieldName + "Month", FieldName + "Year");
                }
                return _OnClientClick;
            }
            set { _OnClientClick = value; }
        }
        

        private DateTime _valueDatetime;
        private DateTime valueDatetime
        {
            get
            {
                DateTime.TryParse(Value, out _valueDatetime);
                return (_valueDatetime);
            }
        }
        private bool SetDefaultDate = false;
        private DropDownList ddlDay;
        private DropDownList ddlMonth;
        private DropDownList ddlYear;
        public DatePickerTypes DatePickerType
        {
            get
            {
                return _DatePickerType;
            }
            set
            {
                _DatePickerType = value;
            }
        }
        private DatePickerTypes _DatePickerType;

        public enum DatePickerTypes
        {
            dropdown = 0,
            calendar = 1
        }

        protected void buildCaledarDatePicker()
        {
        }

        public void Disable()
        {
            if (DatePickerType == DatePickerTypes.dropdown)
            {
                ddlDay.Enabled = false;
                ddlMonth.Enabled = false;
                ddlYear.Enabled = false;
            }
        }

        public void Enable()
        {
            if (DatePickerType == DatePickerTypes.dropdown)
            {
                ddlDay.Enabled = true;
                ddlMonth.Enabled = true;
                ddlYear.Enabled = true;
            }
        }

        protected void setUpDropDownDatePicker()
        {           

            #region days            
            string strDay = "";
            for (int i = 1; i <= 31; i++)
            {
                if (i.ToString().Length < 2)
                {
                    strDay = "0" + i.ToString();
                    ddlDay.Items.Add(new ListItem(strDay, strDay));
                }
                else
                {
                    ddlDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
            }
            ddlDay.Items.Insert(0, (new ListItem("", "")));
            ddlDay.Attributes["onchange"] = OnClientClick;            
            #endregion                     

            #region months
            
            string strMonth = "";
            for (int i = 1; i <= 12; i++)
            {
                if (i.ToString().Length < 2)
                {
                    strMonth = "0" + i.ToString();
                    ddlMonth.Items.Add(new ListItem(strMonth, strMonth));
                }
                else
                {
                    ddlMonth.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
            }
            ddlMonth.Items.Insert(0, (new ListItem("", "")));
            ddlMonth.Attributes["onchange"] = OnClientClick;
            #endregion

            #region year            
            int startYear = minDate == DateTime.MinValue ? 1900 : minDate.Year;
            int endYear = maxDate == DateTime.MaxValue ? DateTime.Now.Year + year_rangeforward : endYear = maxDate.Year;
            
            //Modificato su richiesta di Carla per visualizzare prima gli anni più recenti
            //for (int j = startYear; j <= endYear; j++)
            //{
            for (int j = endYear; j >= startYear; j--)
            {
                ddlYear.Items.Add(new ListItem(j.ToString(), j.ToString()));
            }
            ddlYear.Items.Insert(0, (new ListItem("", "")));
            ddlYear.Attributes["onchange"] = OnClientClick;
            #endregion

            ddlDay.ID = FieldName + "Day";
            ddlMonth.ID = FieldName + "Month";
            ddlYear.ID = FieldName + "Year";

            if (Value.Length > 0)
            {
                var item = ddlDay.Items.FindByValue(valueDatetime.ToString("dd"));
                if (item != null)
                    item.Selected = true;
            }
            if (Value.Length > 0)
            {
                var item = ddlMonth.Items.FindByValue(valueDatetime.ToString("MM"));
                if (item != null)
                    item.Selected = true;
            }
            if (Value.Length > 0)
            {
                var item = ddlYear.Items.FindByText(valueDatetime.ToString("yyyy"));
                if(item!=null)
                    item.Selected = true;
            }
        }
        

        public AxfDatePicker(string label, string fieldname, NetCms.Connections.Connection conn, DatePickerTypes datepickertype, bool setdefaultdate)
            : this(label, fieldname, conn, datepickertype, setdefaultdate, 70, 3)
        {
        }


        public AxfDatePicker(string label, string fieldname, NetCms.Connections.Connection conn, DatePickerTypes datepickertype, bool setdefaultdate, int year_rangeback, int year_rangeforward)
            : base(label, fieldname)
        {
            Page p = HttpContext.Current.Handler as Page;
            if (p != null)
            {
                if (!p.ClientScript.IsClientScriptBlockRegistered("axfdatepickerscript"))
                    p.ClientScript.RegisterClientScriptBlock(typeof(Page), "axfdatepickerscript", scriptString);
            }

            Conn = conn;
            DatePickerType = datepickertype;
            SetDefaultDate = setdefaultdate;
            this.year_rangeback = year_rangeback;
            this.year_rangeforward = year_rangeforward;
            if (SetDefaultDate)
                Value = DateTime.Now.ToShortDateString();

            switch (DatePickerType)
            {
                case DatePickerTypes.dropdown:
                    ddlDay = new DropDownList();
                    ddlMonth = new DropDownList();
                    ddlYear = new DropDownList(); 
                    break;
                case DatePickerTypes.calendar:
                    buildCaledarDatePicker();
                    break;
                default:
                    break;
            }

        }

        public override string filterValueForDB(string value)
        {
            string output = value.Trim();
            output = output.Replace("'", "''");
            output = output.Trim();
            if (Conn != null)
                output = Conn.FilterDate(output);
            else
            {
                DateTime date;
                if (DateTime.TryParse(output, out date))
                    output = "'" + (date.Year + "-" + date.Month + "-" + date.Day + " " + date.Hour + "." + date.Minute + "." + date.Second) + "'";
                else
                    output = "'" + (output) + "'";
            }
            return output;
        }

        public void ClearList()
        {
            ddlDay.Items.Clear();
            ddlMonth.Items.Clear();
            ddlYear.Items.Clear();
        }

        protected override HtmlGenericControl getFieldContent()
        {
            HtmlGenericControl control = new HtmlGenericControl("div");
            HtmlGenericControl datepickercontrol = getControl();
            control.Controls.Add(datepickercontrol);

            return control;
        }

        private HtmlGenericControl getControl()
        {
            
            HtmlGenericControl par = new HtmlGenericControl("div");
            par.Attributes["class"] = "Axf_FieldBox";
            Label lb = new Label();
            lb.AssociatedControlID = _FieldName;
            if (Required)
                lb.Text = _Label + "*";
            else
                lb.Text = _Label;

            lb.Attributes["class"] = "formlabel";

            if (_Label.Length > 0)
                par.Controls.Add(lb);


            HtmlInputHidden hidden = new HtmlInputHidden();
            hidden.ID = _FieldName;
            hidden.Value = Value;

            switch (DatePickerType)
            {
                case DatePickerTypes.dropdown:
                    setUpDropDownDatePicker();                    
                    par.Controls.Add(hidden);
                    par.Controls.Add(ddlDay);
                    par.Controls.Add(ddlMonth);
                    par.Controls.Add(ddlYear);
                    break;
                case DatePickerTypes.calendar:
                    break;
                default:
                    break;
            }

            if (ReadOnly)
            {
                ListItem day = ddlDay.SelectedItem;
                ddlDay.Items.Clear();
                if (day != null)
                    ddlDay.Items.Add(day);

                ListItem month = ddlMonth.SelectedItem;
                ddlMonth.Items.Clear();
                if (month != null)
                    ddlMonth.Items.Add(month);

                ListItem year = ddlYear.SelectedItem;
                ddlYear.Items.Clear();
                if (year != null)
                    ddlYear.Items.Add(year);
            }

            return par;
        }

        protected override string checkInput(string input)
        {
            //get day,month,year value 
            /*string newinput="";
            string ddlday="";
            string ddlmonth="";
            string ddlyear="";

            ddlday = HttpContext.Current.Request.Form[this.FieldName+"day"]!=null? HttpContext.Current.Request.Form[this.FieldName+"day"]:"";   
            ddlmonth = HttpContext.Current.Request.Form[this.FieldName+"day"]!=null? HttpContext.Current.Request.Form[this.FieldName+"month"]:"";    
            ddlyear = HttpContext.Current.Request.Form[this.FieldName+"day"]!=null? HttpContext.Current.Request.Form[this.FieldName+"year"]:"";               
            newinput=ddlday+"/"+ddlmonth+"/"+ddlyear;     */

            string errors = string.Empty;

            input = input.Trim();
            if (Required)
            {
                if (input.Length == 0)
                {
                    errors += "<li>";
                    errors += string.Format(Labels[AxfLabels.LabelsList.IlCampoEObbligatorio], this.Label);
                    errors += "</li>";
                }
            }
            if (input.Length > 0)
            {
                DateTime date;          
                if (!DateTime.TryParse(input, out date))
                {
                    errors += "<li>";
                    errors += string.Format(Labels[AxfLabels.LabelsList.IlCampoNonEValido], this.Label);
                    errors += "</li>";
                }
                else if (minDate != DateTime.MinValue && DateTime.Compare(date, minDate) < 0)
                {
                    errors += "<li>";
                    errors += String.Format(Labels[AxfLabels.LabelsList.DataSuccessivaAl], Label, minDate.ToShortDateString());
                    errors += "</li>";
                }
                else if (maxDate != DateTime.MinValue && DateTime.Compare(date, maxDate) > 0)
                {
                    errors += "<li>";
                    errors += String.Format(Labels[AxfLabels.LabelsList.DataAntecedenteAl], Label, maxDate.ToShortDateString());
                    errors += "</li>";
                }
            }
            
            return errors;
        }

        public override string getFilter(string value)
        {
            string filter = " ";
            if (value.Length > 0)
            {
                int val = 0;
                if (int.TryParse(value, out val))
                {
                    filter += _FieldName;

                    filter += " = ";
                    filter += value;
                    filter += " ";
                }
                else
                {
                    filter += _FieldName;

                    filter += " LIKE '";
                    filter += value;
                    filter += "' ";
                }
            }
            return filter;
        }

        public override HtmlGenericControl GetOverviewControl()
        {
            HtmlGenericControl fieldControl = new HtmlGenericControl("li");
            //fieldControl.InnerHtml = "<strong>" + this.Label + ": </strong>" + DropDownList.Items.FindByValue(this.Value).Text;
            return fieldControl;
        }
    }

}