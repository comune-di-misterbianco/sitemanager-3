﻿using System;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace G2Core.AdvancedXhtmlForms.Fields
{
    public class AxfRadioGroup : AxfField
    {
        public RadioButtonList radiobuttonlist;
        private bool _ValidValue = false;
        public bool ValidValue
        {
            get { return _ValidValue; }
            set { _ValidValue = value; }
        }

        public override bool ShowInInOverview { get { return false; } }       

        private bool _AutoPostBack;
        public bool AutoPostBack
        {
            get
            {
                return _AutoPostBack;
            }
            set
            {
                _AutoPostBack = value;
            }
        }

        private string _OnClientClick;
        public string OnClientClick
        {
            get
            {
                if (_OnClientClick == null)
                {
                    _OnClientClick = "setSubmitSource('" + FieldName + "')";
                }
                return _OnClientClick;
            }
            set { _OnClientClick = value; }
        }

        private bool init = false;

        public AxfRadioGroup(string label,string fieldname,bool repeathorizontal)
            : base(label, fieldname)
        {
            radiobuttonlist = new RadioButtonList();
            if (repeathorizontal)
            {
                radiobuttonlist.RepeatLayout = RepeatLayout.Flow;
                radiobuttonlist.RepeatDirection = RepeatDirection.Horizontal;
            }
        }

        protected override string checkInput(string input)
        {
            string errors = "";

            if (Required && input == string.Empty)
            {
                errors += "<li>";
                errors += string.Format(this.Labels[AxfLabels.LabelsList.IlCampoEObbligatorio], this.Label);
                errors += "</li>";
            }

            return errors;

        }

        public override string getFilter(string value)
        {
            string filter = " ";
            return filter;
        }

        public void addItem(string Label, string Value, bool selected)
        {
            ListItem item = new ListItem(Label, Value);
            item.Selected = selected;
            radiobuttonlist.Items.Add(item);
        }



        public override string filterValueForDB(string value)
        {
            if (init)
                return this.Value;

            if (!ValidValue)
                return base.filterValueForDB(value);
            else
                return value;
        }

        /// <summary>
        /// Restituisce il controllo xhtml relativo al campo 
        /// </summary>
        /// <returns>Un HtmlGenericControl contenente tutti i controlli del campo</returns>
        protected override HtmlGenericControl getFieldContent()
        {
            HtmlGenericControl control = new HtmlGenericControl("div");
            HtmlGenericControl rblControl = getControlSimple();                    

            if (Value.Length > 0)
            {
                foreach (ListItem item in radiobuttonlist.Items)
                {
                    if (string.Compare(item.Value,Value,true)==0)
                    {
                        item.Selected = true;
                        radiobuttonlist.SelectedIndex = radiobuttonlist.Items.IndexOf(item);
                    }
                }
            }
            control.Controls.Add(rblControl);

            return control;
        }

        private HtmlGenericControl getControlSimple()
        {
            HtmlGenericControl par = new HtmlGenericControl("div");
            par.Attributes["class"] = "Axf_FieldBox";
            par.ID = _FieldName + "Div";

            Label lb = new Label();
            lb.AssociatedControlID = _FieldName;
            if (Required)
                lb.Text = _Label + "*";
            else
                lb.Text = _Label;

            lb.Attributes["class"] = "formlabel";

            par.Controls.Add(lb);
            radiobuttonlist.AutoPostBack = AutoPostBack;
            if (AutoPostBack)
                radiobuttonlist.Attributes["onchange"] = OnClientClick;
            radiobuttonlist.ID = _FieldName;
            par.Controls.Add(radiobuttonlist);
            return par;
        }

    }
}