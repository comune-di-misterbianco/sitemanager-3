﻿using System;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace G2Core.AdvancedXhtmlForms.Fields
{
    /// <summary>
    /// La classe AxfTreeView implementa un controllo web che visualizza dati gerarchici.
    /// </summary>
    public class AxfTreeView : AxfField
    {
        private AxfFieldValueDBChecker _ValueDBChecker;
        /// <summary>
        /// 
        /// </summary>
        public AxfFieldValueDBChecker ValueDBChecker
        {
            get { return _ValueDBChecker; }
            set { _ValueDBChecker = value; }
        }
        
        private AxfTreeNode _Tree;
        /// <summary>
        /// Ottiene il nodo base ?????????
        /// </summary>
        public AxfTreeNode Tree
        {
            get
            {
                return _Tree;
            }
        }
        
        private bool _AutoPostBack = false;
        /// <summary>
        /// Ottiene o imposta un valore che indica se a un evento associato al campo viene invocato automaticamente un Post della form
        /// </summary>
        public bool AutoPostBack
        {
            get
            {
                return _AutoPostBack;
            }
            set
            {
                _AutoPostBack = value;
            }
        }

        private bool _CheckAllows = false;
        /// <summary>
        /// Ottiene o imposta un valore che indica se cambiare l'icona del nodo corrispondente in funzione dei diritti associati al profilo utente 
        /// </summary>
        public bool CheckAllows
        {
            get
            {
                return _CheckAllows;
            }
            set
            {
                _CheckAllows = value;
            }
        }
        private bool _IsStatic = false;
        /// <summary>
        /// Ottiene o imposta un parametro che indica quando l'albero è usato in modo statico
        /// </summary>
        public bool IsStatic
        {
            get
            {
                return _IsStatic;
            }
            set
            {
                _IsStatic = value;
            }
        }
       

        private string _NotAllowedMsg;
        /// <summary>
        /// Ottiene o imposta il messaggi di notifica, mostrato quando non la proprietà <c>AxfTreeNode.Grant</c> e impostato a true.
        /// </summary>       
        public string NotAllowedMsg
        {
            get
            {
                if (_NotAllowedMsg == null)
                    _NotAllowedMsg = "Attenzione non si possedono i permessi per la cartella selezionata";
                return _NotAllowedMsg;
            }
            set { _NotAllowedMsg = value; }
        }

        /// <summary>
        /// Ottiene o imposta la distanza in termini di "../" dalla radice dell'applicazione.
        /// </summary>
        public string RootDist
        {
            get
            {
                return _RootDist;
            }
            set
            {
                _RootDist = value;
            }
        }
        private string _RootDist;

        private int idSeed = 0;

        /// <summary>
        /// Ottiene o imposta il valore del campo
        /// </summary>
        public override string Value
        {
            get
            {
                return base.Value;
            }
            set
            {
                if (LoadValue)
                    base.Value = value;
            }
        }

        /// <summary>
        /// Inizializza una nuova istanza del campo
        /// </summary>
        /// <param name="label">Etichetta testuale del campo: apparirà nel form</param>
        /// <param name="fieldname">appresenta l'identificativo (ID) del campo nella form. 
        /// Generalmente si usa per mappare il campo corrispondente a quello del database</param>
        /// <param name="rootNode">Nodo base che rappresenta la radice dell'albero</param>
        public AxfTreeView(string label, string fieldname, AxfTreeNode rootNode)
            : base(label, fieldname)
        {
            _Tree = rootNode;
            _ValueDBChecker = new AxfFieldValueDBChecker(this);
        }

        /// <summary>
        /// Verifica se il valore inserito soddisfa i criteri pre-impostati.
        /// </summary>
        /// <param name="input">Valore da verificare</param>
        /// <returns>Messaggio di errore racchiuso all'interno di un tag html "li" </returns>
        protected override string checkInput(string input)
        {
            string errors = string.Empty;

            input = input.Trim();
            if (Required && input.Length == 0)
            {
                errors += "<li>";
                errors += string.Format(this.Labels[AxfLabels.LabelsList.IlCampoEObbligatorio], this.Label);
                errors += "</li>";
            }

            return errors;

        }

        /// <summary>
        /// Restituisce il valore del campo filtrato in funzione del parametro di comparazione
        /// </summary>
        /// <param name="value">Valore del campo</param>
        /// <returns>Restituisce il valore del campo filtrato</returns>
        public override string getFilter(string value)
        {
            string filter = " ";
            value = value.Replace("'", "''").Trim();
            if (value.Length > 0)
            {
                filter += this.FieldName;
                filter += " = ";
                filter += value;
                filter += "";
            }
            return filter;
        }


        /// <summary>
        /// Restituisce il valore del campo filtrato in modo appropriato per l'inserimento nel database.
        /// </summary>
        /// <param name="value">Valore del campo</param>
        /// <returns>Restituisce il valore del campo formattato secondo la sintassi accettata dal database in uso</returns>
        public override string filterValueForDB(string value)
        {
            value = value.Trim();
            return value = "'" + value.Replace("'", "''") + "'";
        }

        /// <summary>
        /// Ritorna il controllo xhtml del campo.
        /// </summary>
        /// <returns>Un HtmlGenericControl contenente tutti i nodi del controllo </returns>
        protected override HtmlGenericControl getFieldContent()
        {
            HtmlGenericControl control = new HtmlGenericControl("div");
            HtmlInputHidden hidden;

            hidden = new HtmlInputHidden();
            hidden.ID = _FieldName;
            hidden.Value = Value;
            control.Controls.Add(hidden);
            control.Controls.Add(getTree());
            return control;
        }

        private Control getTree()
        {
            idSeed = 0;
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "width:200px;text-align: left;";
            HtmlGenericControl script = new HtmlGenericControl("script");

            div.Controls.Add(script);
            script.Attributes["type"] = "text/javascript";
            string html = "\n";
            html += "<!--\n";

            html += "\nfunction setSelectedNode(value){\n";
            html += "   document.getElementById(\"" + _FieldName + "\").value = value; \n";
            if (AutoPostBack)
            {
                html += "   setSubmitSource(\"" + _FieldName + "\");\n";
                html += "   if (document.getElementById(\"page\")!=null){\n";
                html += "       setpage(0);}\n";
                html += "   document.getElementById(\"PageForm\").submit();\n";
            }
            html += "}\n";
            if (IsStatic)
                html += "	d_" + _FieldName + " = new dTree('d_" + _FieldName + "',true);\n";
            else
                html += "	d_" + _FieldName + " = new dTree('d_" + _FieldName + "');\n";
            html += getNodes(Tree, idSeed++, -1);

            html += "		document.write(d_" + _FieldName + ");\n";
            if (IsStatic)
                html += "		javascript: d_" + _FieldName + ".openAll();\n";
            html += "//-->\n";
            script.InnerHtml = html;

            return div;
        }

        private string getNodes(AxfTreeNode node, int id, int padre)
        {
            string image = RootDist + "App_images/dtree/folder.gif";
            string image_open = RootDist + "App_images/dtree/folderopen.gif";
            string js_command = "javascript: setSelectedNode(\\\'" + node.Value + "\\\')";
            /*if (node.ImageUrl != "")
                image = node.ImageUrl;
            if (node.NavigateUrl != "")
                image_open = node.NavigateUrl;*/

            if (CheckAllows && !node.Grant)
            {
                image = RootDist + "App_images/dtree/redfolder.gif";
                image_open = RootDist + "App_images/dtree/redfolderopen.gif";
                js_command = "javascript: alert(\\\'" + NotAllowedMsg + "\\\')";
            }

            string html = "d_" + _FieldName + ".add(" + id + "," + padre + ",'" + node.Label.Replace("'", "\\\'") + "','" + js_command + "','','','" + image + "','" + image_open + "'); \n";
            foreach (AxfTreeNode childnode in node.ChildNodes)
                html += getNodes(childnode, idSeed++, id);

            return html;
        }
    }
}