using System;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


namespace G2Core.AdvancedXhtmlForms.Fields
{
    /// <summary>
    /// Rappresenta un nodo nel controllo AxfTreeView
    /// </summary>
    public class AxfTreeNode
    {
        private string _ID;
        /// <summary>
        /// Ottiene o imposta l'identificativo univoco del nodo.
        /// </summary>
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _Icon;
        /// <summary>
        /// Ottiene o imposta il nome dell'icona visualizzata accanto al nodo.
        /// </summary>
        public string Icon
        {
            get { return _Icon; }
            set { _Icon = value; }
        }
        /// <summary>
        /// Ottiene un valore che indica se il nodo contiene nodi figli.
        /// </summary>
        public bool HasChildNodes
        {
            get
            {
                return ChildNodes.Count>0;
            }
        }
        
        private bool _Expand;
        /// <summary>
        /// Ottiene un valore che indica se il nodo � espanso.
        /// </summary>
        public bool Expand
        {
            get
            {
                if(_Expand) 
                    return _Expand;
                else
                {
                    if (this.HasChildNodes)
                        for (int i = 0; i < this.ChildNodes.Count;i++ )
                            if(ChildNodes[i].Expand) 
                                   return ChildNodes[i].Expand;      
                }
                return false;
            }
            set
            {
                _Expand = value;
            }
        }

     
        private bool _Highlight;
        /// <summary>
        /// Ottiene un valore che indica se il nodo � selezionato.
        /// </summary>
        public bool Highlight
        {
            get
            {
                return _Highlight;
            }
            set
            {
                _Highlight = value;
            }
        }
        
        private bool _AddToTree = true;
        /// <summary>
        /// ??? Non usato
        /// </summary>
        public bool AddToTree
        {
            get
            {
                if (_AddToTree)
                    return _AddToTree;
                else
                {
                    if (this.HasChildNodes)
                        for (int i = 0; i < this.ChildNodes.Count; i++)
                            if (ChildNodes[i].AddToTree)
                                return ChildNodes[i].AddToTree;
                }
                return false;
            }
            set
            {
                _AddToTree = value;
            }
        }

        private string _Href;
        /// <summary>
        /// Ottiene o imposta l'indirizzo web a cui puntare quando si fa click sul nodo. 
        /// </summary>
        public string Href
        {
            get { return _Href; }
            set { _Href = value; }
        }

        private string _Value;
        /// <summary>
        /// Ottiene o imposta il valore non visualizzato; � utilizzato per memorizzare ulteriori valori al nodo.
        /// </summary>
        /// <remarks>
        /// Generalmente si usa per la gestione di eventi nel postback
        /// </remarks>
        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        private string _Label;
        /// <summary>
        /// Ottiene o imposta l'etichetta visualizza per il nodo nel controllo AxfTreeView.
        /// </summary>
        public string Label
        {
            get { return _Label; }
            set { _Label = value; }
        }

        private bool _Grant;
        /// <summary>
        /// Ottiene o imposta il livello di visibilit� del nodo.
        /// </summary>
        public bool Grant
        {
            get { return _Grant; }
            set { _Grant = value; }
        }

        private AxfTreeNode _ParentNode;
        /// <summary>
        /// Ottiene o imposta il nodo padre del nodo corrente.
        /// </summary>
        public AxfTreeNode ParentNode
        {
            get { return _ParentNode; }
        }
        /// <summary>
        /// Ottiene un valore che indica la profondit� del nodo. 
        /// </summary>
        public int Depth
        {
            get { return ParentNode!=null ? ParentNode.Depth + 1 : 0; }
        }
        /// <summary>
        /// Imposta il nodo padre del nodo corrente
        /// </summary>
        /// <param name="parent">Nodo padre</param>
        public void SetParentNode(AxfTreeNode parent)
        {
            if (_ParentNode == null)
                _ParentNode = parent;
            else
            {
                Exception ex = new Exception("Parent Node Alredy Set");
                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, "", "", "97");
            }
        }

        private AxfTreeNodeCollection _ChildNodes;
        /// <summary>
        /// Ottiene la collezione di nodi contenuti nel nodo corrente 
        /// </summary>
        public AxfTreeNodeCollection ChildNodes
        {
            get { return _ChildNodes; }
        }
        /// <summary>
        /// Inizializza un nuova istanza del nodo AxfTreeNode
        /// </summary>
        public AxfTreeNode()
        {
            _ChildNodes = new AxfTreeNodeCollection(this);
        }

        /// <summary>
        /// Ritorna il controllo xhtml .
        /// </summary>
        /// <returns>Un HtmlGenericControl contenente tutti i controlli del campo</returns>
        public HtmlGenericControl getControl()
        {
            HtmlGenericControl li = new HtmlGenericControl("li");           

            return li;
        }

    }
}