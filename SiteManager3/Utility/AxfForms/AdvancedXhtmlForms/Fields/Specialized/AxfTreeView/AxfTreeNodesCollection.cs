using System.Collections;
using System;

/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>
namespace G2Core.AdvancedXhtmlForms.Fields
{
    public class AxfTreeNodeCollection : IEnumerable
    {
        private G2Collection coll;
        public int Count { get { return coll.Count; } }
        private AxfTreeNode OwnerNode;
        public AxfTreeNodeCollection(AxfTreeNode ownerNode)
        {
            //
            // TODO: Add constructor logic here
            //
            OwnerNode = ownerNode;
            coll = new G2Collection();
        }

        public void Add(AxfTreeNode node)
        {
            coll.Add(node.ID, node);
            node.SetParentNode(this.OwnerNode);
        }
        public void Remove(AxfTreeNode node)
        {
            coll.Remove(node.ID);
        }
        public void Remove(string nodeID)
        {
            coll.Remove(nodeID);
        }
        public void Remove(int index)
        {
            coll.Remove(index);
        }
        public AxfTreeNode this[int i]
        {
            get
            {
                AxfTreeNode node = (AxfTreeNode)coll[coll.Keys[i]];
                return node;
            }
        }

        public AxfTreeNode this[string key]
        {
            get
            {
                AxfTreeNode node = (AxfTreeNode)coll[key];
                return node;
            }
        }


        public int count()
        {
            return coll.Count;
        }
        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private G2Core.AdvancedXhtmlForms.Fields.AxfTreeNodeCollection Collection;
            public CollectionEnumerator(G2Core.AdvancedXhtmlForms.Fields.AxfTreeNodeCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}