using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Connections;


namespace G2Core.AdvancedXhtmlForms.Fields
{
    /// <summary>
    /// La classe AxfImageUpload implementa un controllo che consente agli utenti di selezionare e traferire file grafici sul server. 
    /// </summary>
    /// <remarks>
    /// Visualizza un controllo casella di testo e un pulsante Sfoglia che consentono agli utenti di selezionare un file sul client e di caricarlo sul server Web. L'utente specifica il file da caricare immettendo il percorso completo del file nel computer locale, ad esempio C:\MyFiles\TestFile.txt, nella casella di testo del controllo. In alternativa, l'utente pu� selezionare il file facendo clic sul pulsante Sfoglia e in seguito posizionarlo nella finestra di dialogo Scegli file.
    /// </remarks>
    public class AxfImageUpload : G2Core.AdvancedXhtmlForms.Fields.AxfField
    {

        public bool addTransferbutton = true;
        //bytes
        public int maxBytesFileLength = 0;
        //use with  comma divider example validExstensionList=" .jpeg,.png" 
        public string validExstensionList = "";
 
        protected string FolderPath;
        protected string TargetControlID = "FileUpload";
       
        private FileUpload _Uploadform;
        private FileUpload Uploadform
        {
            get
            {
                if (_Uploadform == null)
                {
                    _Uploadform = new FileUpload();
                    _Uploadform.ID = "FileUpload";
                }

                return _Uploadform;
            }
        }

        /// <summary>
        /// Inizializza una nuova istanza del campo
        /// </summary>       
        /// <param name="label">Etichetta testuale del campo: apparir� nel form</param>
        /// <param name="fieldname">Rappresenta l'identificativo (ID) del campo nella form. 
        /// Generalmente si usa per mappare il campo corrispondente a quello del database</param>
        /// <param name="folderPath">Percorso completo della directory nella quale salvare il file caricato</param>
        /// <param name="conn">Connessione al database <see cref="NetCms.Connections.Connection"/></param>
        public AxfImageUpload(string label, string fieldname, string folderPath, NetCms.Connections.Connection conn)
            : base(label, fieldname)
        {
            FolderPath = folderPath;
        }

        /// <summary>
        /// Verifica se il valore inserito soddisfa i criteri pre-impostati.
        /// </summary>
        /// <param name="input">Valore da verificare</param>
        /// <returns>Messaggio di errore racchiuso all'interno di un tag html "li" </returns>
        protected override string checkInput(string input)
        {
            string errors = "";
            HttpPostedFile file=HttpContext.Current.Request.Files["FileUpload"];
            if (file != null)
            {
                if (maxBytesFileLength > 0 && file.ContentLength > maxBytesFileLength)
                {
                    errors += "<li>";
                    errors += "La dimensione del file non deve superare " + (maxBytesFileLength / 1000).ToString() + " kb.";
                    errors += "</li>";
                }                    
                string[] validExt = validExstensionList.Split(',');
                if (validExt.Length > 0)
                {
                    bool found=false;
                    string strExtn = System.IO.Path.GetExtension(file.FileName).ToLower();
                    foreach (string  extension in validExt)
                    {
                        if (strExtn == extension.ToLower())
                        {
                            found = true;
                            break;
                        }
                    }
                    if(!found)
                    {
                        errors += "<li>";
                        errors += "Selezionare un file con le seguenti estensioni :"+validExstensionList+".";
                        errors += "</li>";
                    }
                }
                

            }                           
            else if (Required)
            {
                errors += "<li>";
                errors += "Il campo '" + _Label + "' � obbligatorio";
                errors += "</li>";
            }
            return errors;
        }
        

        /// <summary>
        /// Restituisce il valore del campo filtrato in modo appropriato per l'inserimento nel database.
        /// </summary>
        /// <param name="value">Valore del campo</param>
        /// <returns>Restituisce il valore del campo formattato secondo la sintassi accettata dal database in uso</returns>
        public override string filterValueForDB(string value)
        {
            string val = value;
            int i;
            if (!int.TryParse(val, out i))
                val = "'" + val + "'";

            return val;
        }

        /// <summary>
        /// Restituisce il valore del campo filtrato in funzione del parametro di comparazione
        /// </summary>
        /// <param name="value">Valore del campo</param>
        /// <returns>Restituisce il valore del campo filtrato</returns>
        public override string getFilter(string value)
        {
            string filter = " ";
            if (value.Length > 0)
            {
                filter += _FieldName;

                filter += " = ";
                filter += value;
                filter += " ";
            }
            return filter;
        }
        
        /// <summary>
        /// Restituisce il controllo xhtml relativo al campo 
        /// </summary>
        /// <returns>Un HtmlGenericControl contenente tutti i controlli del campo</returns>
        protected override HtmlGenericControl getFieldContent()
        {
            HtmlGenericControl p = new HtmlGenericControl("p");

            p.Controls.Add(getTransferView());
            return p;
        }
        /// <summary>
        /// Gestisce l'envento click sul bottone Trasferisci e salva il file inviato in un percorso del server web
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Trasferisci_Click(object sender, EventArgs e)
        {
            if (Uploadform.HasFile)
            {
                //Uncomment this line to Save the uploaded file
                string SavePath = FolderPath;
                if (!SavePath.EndsWith("/"))
                    SavePath += "/";
                string FileName = Uploadform.FileName;
                SavePath = SavePath + FileName;
                Uploadform.SaveAs(SavePath);
            }
        }

        public string  transferFile(string overridingFileName)
        {
           
            string finalFileName="";
            HttpPostedFile file=HttpContext.Current.Request.Files["FileUpload"];
            if (file!=null)
            {                
                string SavePath = FolderPath;
                if (!SavePath.EndsWith("/"))
                    SavePath += "/";
                string FileName = file.FileName;
                int extractPos = FileName.LastIndexOf("\\") + 1;
                string justFileName = FileName.Substring(extractPos, FileName.Length - extractPos).ToLower();
                finalFileName = justFileName;
                //justFileName = fileName.Substring(0, fileName.LastIndexOf('.'));
                if (overridingFileName != String.Empty)
                    finalFileName =overridingFileName + System.IO.Path.GetExtension(file.FileName);               
                file.SaveAs(SavePath + finalFileName);               
            }
            return (finalFileName);
            
        }
        private HtmlGenericControl errors = new HtmlGenericControl("div");

        /// <summary>
        /// Restituisce il controllo xhtml relativo al campo 
        /// </summary>
        /// <returns>Un HtmlGenericControl contenente tutti i controlli del campo</returns>
        public HtmlGenericControl getTransferView()
        {

            HtmlGenericControl div = new HtmlGenericControl("div");

            Label lb = new Label();
            div.Controls.Add(lb);
            lb.Style.Add("display", "block");
            if (Required)
                lb.Text = Label + "*";
            else
                lb.Text = Label;
            lb.AssociatedControlID = "FileUpload";
            div.Controls.Add(Uploadform);            

            if (addTransferbutton)
            {
                Button Trasferisci = new Button();
                Trasferisci.Text = "Trasferisci";
                Trasferisci.ID = "Trasferisci";
                Trasferisci.Click += Trasferisci_Click;
                div.Controls.Add(Trasferisci);
            }
            return div;
        }

    }
}