using System.Collections;
using System;

namespace G2Core.AdvancedXhtmlForms.Fields
{
    /// <summary>
    /// Fornisce funzionalitą standard per la creazione e la gestione di un insieme di oggetti <see cref="G2Core.AdvancedXhtmlForms.Fields.AxfField"></see> 
    /// ordinato.    
    /// </summary>
    /// <remarks>
    /// Questo insieme supporta l'indicizzazione in base zero standard.
    /// </remarks>
    public class AxfFieldsCollection : IEnumerable
    {

        public AxfTable OwnerTable
        {
            get
            {
                return _OwnerTable;
            }
            private set { _OwnerTable = value; }
        }
        private AxfTable _OwnerTable;

        private G2Collection coll;
        /// <summary>
        /// Ritorna il numero di oggetti nella collezione 
        /// </summary>
        public int Count { get { return coll.Count; } }
        /// <summary>
        /// Inizializza una instanza della classe
        /// </summary>
        public AxfFieldsCollection()
        {
            coll = new G2Collection();
        }
        public AxfFieldsCollection(AxfTable ownerTable)
        {
            //
            // TODO: Add constructor logic here
            //
            this.OwnerTable = ownerTable;
            coll = new G2Collection();
        }
        /// <summary>
        /// Aggiunge alla lista un nuovo oggetto</see>
        /// </summary>
        /// <param name="field">Oggetto da aggiungere alla lista</param>      
        public void Add(AxfField field)
        {
            coll.Add(field.FieldName, field);
            if (this.OwnerTable != null) field.OwnerTable = this.OwnerTable;
        }
        /// <summary>
        /// Permette di accedere ad un oggetto della collezione tramite un indice.
        /// </summary>
        /// <param name="i">indice della lista in base zero dell'oggetto da restituire.</param>
        /// <returns>Oggetto contenuto alla posizione specificata della lista.</returns>        
        public AxfField this[int i]
        {
            get
            {
                AxfField field = (AxfField)coll[coll.Keys[i]];
                return field;
            }
        }

        /// <summary>
        /// Permette di accedere ad un oggetto della collezione tramite la chiave associata 
        /// all'oggetto.
        /// </summary>
        /// <param name="key">Chiave dell'oggetto da restiture.</param>
        /// <returns>Oggetto associato alla chiave specificata.</returns>
        public AxfField this[string key]
        {
            get
            {
                AxfField field = (AxfField)coll[key];
                return field;
            }
        }

        /// <summary>
        /// Ritorna il numero di oggetti nella collezione 
        /// </summary>
        public int count()
        {
            return coll.Count;
        }
        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private Fields.AxfFieldsCollection Collection;
            public CollectionEnumerator(Fields.AxfFieldsCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}