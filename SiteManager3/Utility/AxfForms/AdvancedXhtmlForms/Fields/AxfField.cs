using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;


namespace G2Core.AdvancedXhtmlForms.Fields
{
    /// <summary>
    /// Classe astratta con propriet� e metodi di base per costruire dei campi corrispondenti 
    /// a campi di una tabella di un database e gestire la raccolta dati ed il loro inserimento/aggiornamento in     
    /// un database.
    /// </summary>    
    public abstract class AxfField:Interfaces.IInvalidable
    {
        private System.Collections.Specialized.NameValueCollection _CurrentRequest;
        public System.Collections.Specialized.NameValueCollection CurrentRequest
        {
            get 
            {
                if (_CurrentRequest == null)
                    _CurrentRequest = HttpContext.Current.Request.Form;
                return _CurrentRequest; 
            }
            set { _CurrentRequest = value; }
        }

        public string stringFormat = "{0}";
        /// <summary>
        /// Imposta o ottiene se permettere di aggiornare il valore del campo.
        /// Di default questo valore � true.
        /// </summary>
        public bool LoadValue
        {
            get { return _LoadValue; }
            set { _LoadValue = value; }
        }
        private bool _LoadValue = true;

        private HtmlGenericControl _ControlsToAppend;
        private HtmlGenericControl ControlsToAppend
        {
            get
            {
                if (_ControlsToAppend == null)
                {
                    _ControlsToAppend = new HtmlGenericControl("div");
                }
                return _ControlsToAppend;
            }
        }

        /// <summary>
        /// Metodo con opzione di overload che ritorna se mostrare l'oggetto in modalit� overview.
        /// </summary>
        public virtual bool ShowInInOverview { get { return true; } }

        protected AxfLabels Labels
        {
            get
            {
                return AxfLabels.CurrentLabels;
            }
        }

        /// <summary>
        /// Permette di impostare il controllo annidato nell'AxfField come in sola lettura.
        /// Mettendo questa propriet� a true viene anche evitato che in fase di insert o di
        /// update venga usato questo AxfField dalla relativa AxfTable
        /// </summary>
        public bool ReadOnly
        {
            get { return _ReadOnly; }
            set { _ReadOnly = value; }
        }
        private bool _ReadOnly;

        public List<IAxfValidator> Validators
        {
            get
            {
                return _Validators ?? (_Validators = new List<IAxfValidator>());
            }
        }
        private List<IAxfValidator> _Validators;

        /// <summary>
        /// Metodo con opzione di overload per indicare se escludere o meno il campo dalle istruzioni
        /// di inserimento/aggiornamento al database.
        /// </summary>
        public virtual bool JumpField
        {
            get { return _JumpField && ReadOnly; }
            set { _JumpField  = value; }
        }
        private bool _JumpField = false;

        /// <summary>
        /// Metodo con opzione di overload per indicare se escludere o meno il campo dalle istruzioni
        /// di inserimento/aggiornamento al database.
        /// Questo propriet� � simile a JumpField, ma il comportamento di JumpField � stato erroneamente 
        /// modificato aggiungendo "&& ReadOnly", compromettendone l'utilizzo, ed � stato necessario aggiungere questa condizione.
        /// </summary>
        public virtual bool ParseInSqlQueries
        {
            get { return _ParseInSqlQueries; }
            set { _ParseInSqlQueries = value; }
        }
        private bool _ParseInSqlQueries = true;

        /// <summary>
        /// Imposta o ottiene il nome della classe CSS da usare.
        /// </summary>
        public string CssClass
        {
            get 
            {
                if (_CssClass == null)
                    _CssClass = "";
                return _CssClass; 
            }
            set { _CssClass = value; }
        }	
        private string _CssClass;

        public string ErrorCssClass
        {
            get
            {
                if (_ErrorCssClass == null)
                    _ErrorCssClass = "";
                return _ErrorCssClass;
            }
            set { _ErrorCssClass = value; }
        }
        private string _ErrorCssClass;

        /// <summary>
        /// Metodo con opzione di overload per indicare se escludere o meno il campo dalle funzioni di rendering.
        /// Di default  tale valore � true. 
        /// </summary>
        public virtual bool RenderField
        {
            get { return _RenderField; }
            set { _RenderField = value; }
        }
        private bool _RenderField = true;
                
        /// <summary>
        /// Descrizione da aggiungere al campo. La descrizione sar� accessibile tramite un hyperlink affiancato 
        /// al campo renderizato. 
        /// </summary>
        public string InfoText
        {
            get { return _InfoText; }
            set { _InfoText = value; }
        }
        private string _InfoText;
     
        /// <summary>
        /// Codice script da eseguire sul lato client in seguito ad un cambiamento del valore del campo. 
        /// </summary>
        public string OnChange
        {
            get { return _OnChange; }
            set { _OnChange = value; }
        }
        private string _OnChange;  

        /// <summary>
        /// Codice script da eseguire sul lato client in seguito ad un evento di Key Up sul campo. 
        /// </summary>             
        public string OnKeyup
        {
            get { return _OnKeyup; }
            set { _OnKeyup = value; }
        }
        private string _OnKeyup;  
                
        /// <summary>
        /// Imposta o ottiene il valore del campo.
        /// </summary>
        public virtual string Value
        {
            set
            {
                _Value = value;
            }
            get { return _Value; }
        }
        protected string _Value = "";

        /// <summary>
        /// Indica se il campo deve essere valorizato.
        /// </summary>
        public virtual bool Required
        {
            get { return _Required; }
            set { _Required = value; }
        }
        private bool _Required;

        /// <summary>
        /// Restituisce, dopo una postback, il valore del campo valorizzato dall'utente. 
        /// </summary>
        public string PostBackValue
        {
            get
            {
                if (_PostBackValue == null)
                {
                    if (RequestVariable.IsValid())
                    {
                        _PostBackValue = RequestVariable.StringValue;
                    }
                }
                return _PostBackValue;
            }
        }
        private string _PostBackValue;

        /// <summary>
        /// Lista di possibili stati di validazione del campo
        /// </summary>
        public enum ValidationStates { NotValidated, Ok, Error }


        /// <summary>
        /// Restituisce lo stato di validazione del campo.
        /// </summary>
        public ValidationStates ValidationState
        {
            get
            {
                return validationState;
            }
        }
        protected ValidationStates validationState;

        /// <summary>
        /// Restituisce un controllo XHTML con un messaggio di errore sull'operazione di validazione del campo
        /// </summary>
        public WebControl LastValidationErrorsControl
        {
            get
            {
                if (lastValidationErrorsControl == null)
                {
                    lastValidationErrorsControl = new WebControl(System.Web.UI.HtmlTextWriterTag.Div);
                    lastValidationErrorsControl.CssClass = "Axt_ErrorList" + " " + ErrorCssClass;
                }
                return lastValidationErrorsControl;
            }
        }
        protected WebControl lastValidationErrorsControl;

        /// <summary>
        /// Metodo con opzione di overload che restituisce il valore del campo dalle variabili form 
        /// (HttpRequest.Form)
        /// </summary>
        public virtual G2Core.Common.RequestVariable RequestVariable
        {
            get
            {
                if (_RequestVariable == null)
                {
                    _RequestVariable = new G2Core.Common.RequestVariable(FieldName, this.CurrentRequest);                    
                }
                return _RequestVariable;
            }
        }
        protected G2Core.Common.RequestVariable _RequestVariable;

        /// <summary>
        /// Metodo con opzione di overload per impostare o ottenere 
        /// l'etichetta di testo del campo visualizzata nella form
        /// </summary>
        public virtual string Label
        {
            get { return _Label; }
            set { _Label = value; }
        }
        protected string _Label;

        /// <summary>
        /// Nome identificativo del campo.
        /// </summary>
        /// <remarks>
        /// Questo valore sar� utilizzato come ID del controllo se l'istanza � utilizzata in una web form.
        /// Se l'istanza si riferisce ad un campo di una tabella fisica di un databaase, il  FieldName 
        /// dovrebbe corrispondere al nome del campo nella tabella.  
        /// </remarks>
        public virtual string FieldName
        {
            get { return _FieldName; }
        }
        protected string _FieldName;

        public string RequestKey
        {
            get
            {
                if (_RequestKey == null)
                {
                    _RequestKey = "";
                    if(this.OwnerTable!=null && !string.IsNullOrEmpty( this.OwnerTable.WizardsOffsetFieldName))
                        _RequestKey += this.OwnerTable.WizardsOffsetFieldName+"$";
                    _RequestKey += FieldName;
                }
                return _RequestKey;
            }
        }
        private string _RequestKey;

        public AxfTable OwnerTable
        {
            get
            {
                return _OwnerTable;
            }
            set { _OwnerTable = value; }
        }
        private AxfTable _OwnerTable;

        public bool RenderValidationErrorsControl { get; set; } 

        /// <summary>
        /// Restituisce una versione HtmlGenericControl del campo.        
        /// </summary>
        /// <remarks>
        /// Utilizza questo metodo per ottenere componenti XHTML di input (come <input></input>) per 
        /// operazioni di inserimento dati o modifica dati.
        /// </remarks>
        /// <returns>controllo contenente elementi della classe in versione XHTML per inserire/modificare dati.</returns>         
        public HtmlGenericControl Control
        {
            get
            {
                if (_Control == null)
                    _Control = getControl();
                return _Control;
            }
        }
        private HtmlGenericControl _Control;

        /// <summary>
        /// Restituisce l'oggetto <see cref="G2Core.DatabaseFields.DatabaseField"></see>
        /// che descrive le specifiche del campo come un Database Field. 
        /// </summary>
        public DatabaseFields.DatabaseField DatabaseField
        {
            get { return _DatabaseField; }
        }
        protected DatabaseFields.DatabaseField _DatabaseField;

        /// <summary>
        /// Federico ControlsToAppend non sembra essere usato
        /// Appende  il controllo specificato ad un controllo interno della classe.
        /// </summary>
        /// <param name="control"></param>
        public void AppendControl(HtmlGenericControl control)
        {
            this.ControlsToAppend.Controls.Add(control);
        }

        /// <summary>
        /// Costruttore di una nuova istanza. 
        /// </summary>
        /// <param name="label">Etichetta del campo</param>
        /// <param name="fieldname">Nome identificativo del campo</param>
        public AxfField(string label, string fieldname) : this(label, fieldname, null) { }

        /// <summary>
        /// Costruttore di una nuova istanza. 
        /// </summary>
        /// <param name="label">Etichetta del campo</param>
        /// <param name="fieldname">Nome identificativo del campo</param>
        /// <param name="infoText">Testo di informazione da associare al campo</param>
        public AxfField(string label, string fieldname, string infoText)
        {
            ParseInSqlQueries = true;
            _Label = label;
            _FieldName = fieldname;
            _InfoText = infoText;
            validationState = ValidationStates.NotValidated;
            RenderValidationErrorsControl = true;
        }

        /// <summary>
        /// Costruttore di una nuova istanza.
        /// </summary>
        /// <param name="field">Nome identificativo del campo</</param>
        /// <param name="infoText">Testo di informazione da associare al campo</param>
        public AxfField(DatabaseFields.DatabaseField field, string infoText):this(field.Label,field.FieldName,infoText)
        {
            _DatabaseField = field;
        }

        /// <summary>
        /// Costruttore di una nuova istanza.
        /// </summary>
        /// <param name="field">Nome identificativo del campo</param>
        public AxfField(DatabaseFields.DatabaseField field) : this(field, null) { }

        protected virtual HtmlGenericControl getControl()
        {
            HtmlGenericControl control = new HtmlGenericControl("div");

            control.Attributes["class"] = ("Axf_Control " + (this.Required ? "Axf_Control_Required" : "") + " " + this.CssClass).Trim();
            
            if (this.RenderValidationErrorsControl && ValidationState != ValidationStates.NotValidated)
            {
                if (ValidationState == ValidationStates.Error)
                {
                    control.Controls.Add(LastValidationErrorsControl);
                    control.Attributes["class"] += " Axf_ValidationError";
                }
                else
                    control.Attributes["class"] += " Axf_ValidationOk";
            }

            control.Controls.Add(getFieldContent());
       
            control.Controls.Add(GetInfoControl());

            HtmlGenericControl clear = new HtmlGenericControl("span");
            clear.Attributes["class"] = "Axf_Clear";
            control.Controls.Add(clear);

            foreach ( System.Web.UI.Control ctr in this.ControlsToAppend.Controls)
                control.Controls.Add(ctr);

            return control;
        }

        protected abstract HtmlGenericControl getFieldContent();

        /// <summary>
        /// Verifica che l'imput dell'utente all'interno della form sia corretto. In caso negativo restituisce un elenco di errori
        /// </summary>
        /// <param name="input">Il valore inserito dall'utente all'interno del campo nella form.</param>
        /// <returns>Stringa vuota se non ci sono errori, in caso contrario un elenco di errori.</returns>
        protected abstract string checkInput(string input);

        protected string checkInputFromValidators(string input)
        {
            string errors = string.Empty;
            input = input.Trim();

            foreach (var validator in this.Validators)
            {
                if (validator.Validate(input))
                {
                    errors += "<li>";
                    errors += validator.ResultLog;
                    errors += "</li>";
                }
            }

            return errors;
        }
       
        /// <summary>
        /// Verifica se la stringa di input � valida.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public bool validateInput(string input)
        {
            string errors = this.checkInput(input) + checkInputFromValidators(input);
            validationState = (errors == string.Empty) ? ValidationStates.Ok : ValidationStates.Error;

            if (validationState == ValidationStates.Error)
            {
                LastValidationErrorsControl.Controls.Add(new LiteralControl( "<ul>" + errors + "</ul>"));
                if (this.OwnerTable != null) //caso searchForm is null
                {
                    if (this.PostBackValue != "")
                        FieldStringForLog = "L'utente ha inserito nel campo " + this.Label.Replace(":", "") + " della tabella " + this.OwnerTable.TableName + " il valore: " + this.PostBackValue;
                    else FieldStringForLog = "L'utente non ha valorizzato il campo " + this.Label.Replace(":", "") + " della tabella " + this.OwnerTable.TableName;
                }
            }

            return validationState == ValidationStates.Ok;
        }

        // <summary>
        /// Il messaggio da loggare in caso di validazione non riuscita del campo
        /// </summary>
        public string FieldStringForLog
        {
            get
            {
                if (_FieldStringForLog == null)
                    _FieldStringForLog = "";
                return _FieldStringForLog;
            }
            set { _FieldStringForLog = value; }
        }
        private string _FieldStringForLog;

        /// <summary>
        /// Restituisce una stringa che rappresenta la parte di filtro in base al valore passato 
        /// da concatenare ad una query nella sezione WHERE
        /// </summary>
        /// <example>
        /// <c> 
        /// string sqlquery="select * from Sometable Where " + Somefield.getFilter("valore");
        /// </c>
        /// </example>
        /// <param name="value"></param>
        /// <returns></returns>
        public abstract string getFilter(string value);

        /// <summary>
        /// Metodo con opzione di overload che filtra il valore passato prima di essere inserito nel database 
        /// </summary>
        /// <param name="value">valore da filtrare</param>
        /// <returns>valore filtrato</returns>
        public virtual string filterValueForDB(string value)
        {
            string output = value;
            output = output.Trim();
            output = output.Replace("'", "''");
            output = "'" + output + "'";
            return output;
        }
        
        protected HtmlGenericControl GetInfoControl()
        {
            if (this.InfoText != null && InfoText != string.Empty)
            {
                HtmlGenericControl infoContainer = new HtmlGenericControl("div");
                HtmlGenericControl infoText = new HtmlGenericControl("div");
                infoText.Attributes["style"] = "display: none;";
                infoText.Attributes["class"] = "overlay overlayOnDemand";
                infoText.ID = "Axf_Field_" + this.FieldName + "_Info";

                infoText.InnerHtml = this.InfoText;
                infoText.InnerHtml += "<span class=\"overlay_info_icon\">&nbsp;</span>";

                infoContainer.Controls.Add(infoText);

                HtmlGenericControl info = new HtmlGenericControl("a");

                info.Attributes["class"] = "Axf_InfoButton";
                info.Attributes["rel"] = infoText.ID;
                info.Attributes["href"] = "javascript:;";
                //info.Attributes["onclick"] = "javascript: ShowHUDMessage('Informazioni sul campo <em>" + this.Label + "</em>','" + this.InfoText.Replace("'", "\\\'") + "')";
                info.InnerHtml = "<span>Informazioni su questo campo</span>";

                infoContainer.Controls.Add(info);

                return infoContainer;
            }
            else return new HtmlGenericControl("span");
        }

        /// <summary>
        /// Metodo con opzione di overload che restituisce una versione HtmlGenericControl del campo 
        /// da utilizzare in output nella web form in modalit� di lettura.  
        /// </summary>       
        /// <returns>Controllo in versione XHTML in sola lettura.</returns>                
        public virtual HtmlGenericControl GetOverviewControl()
        {
            HtmlGenericControl fieldControl = new HtmlGenericControl("li");
            fieldControl.InnerHtml = "<strong>" + this.Label + ": </strong>" + this.Value;
            return fieldControl;
        }

        /// <summary>
        /// Invalida la riga passata reinizializzando 
        /// l'oggetto <see cref="G2Core.DatabaseFields.DatabaseData"></see> dell'elemento  
        /// DatabaseField della classe
        /// </summary>
        /// <param name="row"></param>
        public void Invalidate(DataRow row)
        {
            if (this.DatabaseField != null) this.DatabaseField.Invalidate(row);
        }
    }

}