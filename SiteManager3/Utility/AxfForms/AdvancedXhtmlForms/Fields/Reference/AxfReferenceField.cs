using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


namespace G2Core.AdvancedXhtmlForms.Fields.Reference
{
    /// <summary>
    /// Rappresenta un campo di riferimento ad una tabella esterna.    
    /// </summary>
    /// <remarks>
    /// Utilizza questa classe per costruire relazioni a tabelle con chiavi esterne. 
    /// </remarks>
    /// <example>
    /// Segue un esempio di utilizzo della classe per creare una relazione tra due tabelle.
    /// <code>    
    /// private G2Core.AdvancedXhtmlForms.AxfGenericTable _AnagraficaFormTable;
    /// private G2Core.AdvancedXhtmlForms.AxfGenericTable AnagraficaFormTable
    /// {
    ///    get
    ///    {
    ///        if (_AnagraficaFormTable == null)
    ///        {
    ///            AxfGenericTable anagrafica = new AxfGenericTable("Anagrafica", "id_Anagrafica", Conn);
    ///            
    ///            AxfTextBox nome = new AxfTextBox("Nome", "Nome_Anagrafica");
    ///            nome.Size = 60;
    ///            nome.MaxLenght = 1000;            
    ///            anagrafica.addField(nome);
    ///            
    ///            AxfTextBox cognome = new AxfTextBox("Cognome", "Cognome_Anagrafica");
    ///            cognome.Size = 60;
    ///            cognome.Required = true;
    ///            anagrafica.addField(cognome);
    ///                        
    ///            AxfTextBox indirizzo = new AxfTextBox("Indirizzo", "Indirizzo_Anagrafica");
    ///            indirizzo.Size = 60;
    ///            indirizzo.MaxLenght = 1000;
    ///            //citta.Required = true;
    ///            anagrafica.addField(indirizzo);        
    ///                        
    ///            _AnagraficaFormTable = anagrafica;
    ///        }
    ///        return _AnagraficaFormTable;
    ///    }
    /// }
    /// private G2Core.AdvancedXhtmlForms.AxfGenericTable _UserFormTable;
    /// private G2Core.AdvancedXhtmlForms.AxfGenericTable UserFormTable
    /// {
    ///    get
    ///    {
    ///        if (_UserFormTable == null)
    ///        {
    ///            AxfGenericTable user = new AxfGenericTable("Users", "id_User", Conn);
    ///            
    ///
    ///            AxfTextBox Nomeutente = new AxfTextBox("Nome Utente", "Name_User");
    ///            Nomeutente.Size = 60;
    ///            Nomeutente.Required = true;
    ///            user.addField(Nomeutente);
    ///
    ///            AxfPassword pwd = new AxfPassword("Password", "Password_User");
    ///            pwd.CyperType = AxfPassword.CyperingType.MD5;
    ///            pwd.HighSecurityField = true;
    ///            if (ID == 0)
    ///                pwd.Required = true;
    ///            else
    ///                pwd.Required = false;
    ///            user.addField(pwd);
    ///            
    ///            //aggiungi una chiave esterna logica alla tabella anagrafe indicando il nome della tabella esterna
    ///            // e il campo di id esterna
    ///            
    ///            AxfReferenceField idanagrafe = new AxfReferenceField("Anagrafica", "Anagrafica_User");
    ///            user.addExternalField(idanagrafe);                 
    ///
    ///            _UserFormTable = user;
    ///        }
    ///        return _UserFormTable;
    ///    }
    /// }          
    /// </code> 
    /// </example>
    public class AxfReferenceField
    {
        private string _TableName;
        /// <summary>
        /// Nome della tabella esterna a cui il campo si riferisce.
        /// </summary>
        public string TableName
        {
            get { return _TableName; }
        }

        private string _FieldName;
        /// <summary>
        /// Nome del campo.
        /// </summary>
        /// <remarks>
        /// Questo valore sar� utilizzato come ID del controllo se l'istanza � utilizzata in una web form.
        /// Se l'istanza si riferisce ad un campo di una tabella fisica di un databaase, il  FieldName 
        /// dovrebbe corrispondere al nome del campo nella tabella.  
        /// </remarks>
        
        public string FieldName
        {
            get { return _FieldName; }
        }

        private string _Value;
        /// <summary>
        /// Valore del campo.
        /// </summary>
        public virtual string Value
        {
            get
            {
                return _Value;
            }
            set
            {
                _Value = value;
            }
        }

        /// <summary>
        /// Inizializza un'istanza della classe.
        /// </summary>
        /// <param name="table">Nome della tabella esterna referenziata</param>
        /// <param name="fieldname">Nome del campo</param>
        public AxfReferenceField(string table, string fieldname)
        {
            _TableName = table;
            _FieldName = fieldname;
        }

        /// <summary>
        /// Inizializza un'istanza della classe.
        /// </summary>
        /// <param name="table">Nome della tabella esterna referenziata</param>
        /// <param name="fieldname">Nome del campo</param>
        /// <param name="value">Valore del campo</param>
        public AxfReferenceField(string table, string fieldname, string value)
            : this(table, fieldname)
        {
            Value = value;
        }
    }
}