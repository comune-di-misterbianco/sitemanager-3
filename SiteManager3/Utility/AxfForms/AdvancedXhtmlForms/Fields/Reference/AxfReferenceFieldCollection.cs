using System.Collections;
using System;


namespace G2Core.AdvancedXhtmlForms.Fields.Reference
{
    /// <summary>
    /// Fornisce funzionalitą standard per la creazione e la gestione di un insieme di oggetti <see cref="G2Core.AdvancedXhtmlForms.Fields.Reference.AxfReferenceField"></see> 
    /// ordinato.    
    /// </summary>
    /// <remarks>
    /// Questo insieme supporta l'indicizzazione in base zero standard.
    /// </remarks>
    public class AxfReferenceFieldCollection : IEnumerable
    {
        private G2Collection coll;
        /// <summary>
        /// Ritorna il numero di oggetti nella collezione 
        /// </summary>
        public int Count { get { return coll.Count; } }
        /// <summary>
        /// Inizializza una instanza della classe
        /// </summary>
        public AxfReferenceFieldCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new G2Collection();
        }

        /// <summary>
        /// Aggiunge alla lista un nuovo oggetto.
        /// </summary>
        /// <param name="field">Oggetto da aggiungere alla lista</param>        
        public void Add( AxfReferenceField field)
        { 
            coll.Add(field.TableName, field);
        }

        /// <summary>
        /// Permette di accedere ad un oggetto della collezione tramite un indice.
        /// </summary>
        /// <param name="i">indice della lista in base zero dell'oggetto da restituire.</param>
        /// <returns>Oggetto contenuto alla posizione specificata della lista.</returns>        
        public AxfReferenceField this[int i]
        {
            get
            {
                AxfReferenceField field = (AxfReferenceField)coll[coll.Keys[i]];
                return field;
            }
        }

        /// <summary>
        /// Permette di accedere ad un oggetto della collezione tramite la chiave associata 
        /// all'oggetto.
        /// </summary>
        /// <param name="str">Chiave dell'oggetto da restiture.</param>
        /// <returns>Oggetto associato alla chiave specificata.</returns>
        public AxfReferenceField this[string str]
        {
            get
            {
                AxfReferenceField field = (AxfReferenceField)coll[str];
                return field;
            }
        }

        /// <summary>
        /// Ritorna il numero di oggetti nella collezione 
        /// </summary>
        public int count()
        {
            return coll.Count;
        }

        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private AxfReferenceFieldCollection Collection;
            public CollectionEnumerator(AxfReferenceFieldCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}