using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace G2Core.AdvancedXhtmlForms.Fields
{
    public class AxfCaptcha : AxfField
    {
        public const string CaptchaApplicationIndexerKey = "CaptchaApplicationIndexer";
        public Dictionary<string, string> Keys
        {
            get
            {
                if (_Keys == null)
                {
                    var indexer = HttpContext.Current.Session[AxfCaptcha.CaptchaApplicationIndexerKey];
                    if (indexer != null)
                    {
                        _Keys = (Dictionary<string, string>)indexer;
                        if (_Keys.Count > 50)
                        {
                            _Keys.Clear();
                            _Keys = null;
                        }
                    }
                }
                if (_Keys == null)
                {
                    HttpContext.Current.Session[AxfCaptcha.CaptchaApplicationIndexerKey] = _Keys = new Dictionary<string, string>();
                }
                return _Keys;
            }
        }
        private Dictionary<string, string> _Keys;

        public G2Core.XhtmlControls.InputField KeyHiddenControl
        {
            get
            {
                if (_KeyHiddenCOntrol == null)
                {
                    _KeyHiddenCOntrol = new G2Core.XhtmlControls.InputField("k");
                    _KeyHiddenCOntrol.Value = Key.ToString();
                }
                return _KeyHiddenCOntrol;
            }
        }
        private G2Core.XhtmlControls.InputField _KeyHiddenCOntrol;

        public string Key
        {
            get
            {
                if (_Key == null)
                {
                    G2Core.Common.RequestVariable req = new G2Core.Common.RequestVariable("k", G2Core.Common.RequestVariable.RequestType.Form);
                    _Key = req.IsValidInteger ? req.IntValue.ToString() : this.GetHashCode().ToString();
                }
                return _Key;
            }
        }
        private string _Key;

        public string CurrentValue
        {
            get;
            private set;
        }

        private bool _DebugMode;
        public bool DebugMode
        {
            get { return _DebugMode; }
            set { _DebugMode = value; }
        }

        private System.Web.UI.WebControls.Image image = new System.Web.UI.WebControls.Image();

        private bool _IsPostBack;
        private bool IsPostBack
        {
            get
            {
                return _IsPostBack;
            }
        }

        public AxfCaptcha(string label, string fieldname, bool ispostback)
            : base(label, fieldname)
        {
            //IsPostBack = ispostback;
            _IsPostBack = this.RequestVariable.IsPostBack;
            _DebugMode = false;
            CurrentValue = this.Keys.ContainsKey(this.Key) ? this.Keys[this.Key] : null;
            if (Keys.ContainsKey(Key))
                Keys.Remove(Key.ToString());
            Keys.Add(Key.ToString(), generaPw(5));
        }

        protected override HtmlGenericControl getFieldContent()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");

            string cssclass = "Axf_FieldBox";
            if (CssClass != null && CssClass.Length > 0)
                cssclass += " " + CssClass;

            div.Attributes["class"] = "Axf_FieldBox" ;

            HtmlGenericControl p1 = new HtmlGenericControl("p");            
            Label lb = new Label();
            lb.AssociatedControlID = this.FieldName;            
            lb.Attributes["class"] = "formlabel";
            lb.Text = this.Label;

            HtmlGenericControl msg = new HtmlGenericControl("p");
            msg.InnerHtml = "Digita i caratteri visualizzati nell'immagine sottostante.";

            string key = !IsPostBack ? genimgkey() : this.RequestVariable.StringValue;

            image.ImageUrl = "image.cap?k=" + Key.ToString();
            image.Attributes["alt"] = "*";

            HtmlGenericControl p = new HtmlGenericControl("p");
            p.Attributes["class"] = "imageCap";
            p.Controls.Add(image);

            TextBox codice = new TextBox();
            codice.ID = this.FieldName;
            //codice.Text = CurrentValue;

            p1.Controls.Add(lb);
            p1.Controls.Add(codice);            


            div.Controls.Add(msg);
            div.Controls.Add(p);
            div.Controls.Add(p1);
            div.Controls.Add(KeyHiddenControl.Control);

            return div;
        }
       
        protected override string checkInput(string input)
        {
            string errors = "";
            if (this.Required && input.Length == 0)
            {
                errors += "<li>";
                errors += "Il campo <strong>" + Label.Replace(":", "") + "</strong> � obbligatorio";
                errors += "</li>";
            }

            string code = (String)System.Web.HttpRuntime.Cache.Get("key");
            string typed_code = this.RequestVariable.StringValue;

            if (CurrentValue.ToLower() != input.ToLower())
                errors += "<li>Il <strong>Codice di controllo</strong> digitato non corrisponde a quello riprodotto nell'immagine!</li>";
            
            //if (errors.Length != 0)
                //_IsPostBack = false;
            
            return errors;            
        }


        public override string getFilter(string value)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        private string generaPw(int maxlen)
        {
            string strNewPass = null;
            int whatsNext;
            int upper;
            int lower;

            Random rnd = new Random();

            for (int intCounter = 1; intCounter <= maxlen; intCounter++)
            {
                whatsNext = rnd.Next(2);

                if (whatsNext == 0)
                {
                    upper = 90;
                    lower = 65;
                }
                else
                {
                    upper = 57;
                    lower = 49;
                }

                int k = 0;

                do
                {
                    k = (int)((upper - lower + 1) * rnd.NextDouble() + lower);
                }
                while (k == 79 || k == 73);

                string strA = new string((Char)k, 1);

                strNewPass = strNewPass + strA;

            }

            return strNewPass;
        }

        private string genimgkey()
        {

            string key = generaPw(5);
            //HttpRuntime.Cache.Insert(this.FieldName, key);
            HttpRuntime.Cache.Insert("key", key);
            return key;
        }
        
    }
}
