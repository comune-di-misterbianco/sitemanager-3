using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Connections;
using G2Core.AdvancedXhtmlForms.Fields.Reference;

namespace G2Core.AdvancedXhtmlForms
{
    /// <summary>
    /// Classe astratta con propriet� e metodi di base per costruire forme di inserimento e modifica dati 
    /// in database     
    /// </summary>
    public abstract class AxfTable
    {

        private G2Core.XhtmlControls.InputField _PostbackCheckField;
        public G2Core.XhtmlControls.InputField PostbackCheckField
        {
            get
            {
                if (_PostbackCheckField == null)
                {
                    _PostbackCheckField = new G2Core.XhtmlControls.InputField("PostbackCheckField_" + this.TableName);
                    _PostbackCheckField.AutoLoadValueAfterPostback = true;
                    _PostbackCheckField.Value = "PostedBack";
                }
                return _PostbackCheckField;
            }
        }

        /// <summary>
        /// Indica se si � verificata una postback.
        /// </summary>
        public bool IsPostBack
        {
            get
            {
                return PostbackCheckField.RequestVariable.IsPostBack;
            }
        }
        
        /// <summary>
        /// ArrayList che contiene i messaggi da loggare per i campi della tabella che non hanno ottenuto la validazione
        /// </summary>
        public ArrayList TableStringsForLog
        {
            get
            {
                if (_TableStringsForLog == null)
                    _TableStringsForLog = new ArrayList();
                return _TableStringsForLog;
            }
            set { _TableStringsForLog = value; }
        }
        private ArrayList _TableStringsForLog;
        
        protected AxfLabels Labels
        {
            get
            {
                return AxfLabels.CurrentLabels;
            }
        }

        /// <summary>
        /// Nel caso in cui la form girasse all'interno di un form UI.WebControls.Wizard � necessario specificare l'offset per ottenere il 
        /// valore dalla Request.
        /// </summary>
        public string WizardsOffsetFieldName
        {
            get
            {
                if (_WizardsOffsetFieldName == null)
                    _WizardsOffsetFieldName = "";
                return _WizardsOffsetFieldName;
            }
            set { _WizardsOffsetFieldName = value; }
        }
        private string _WizardsOffsetFieldName;

        private G2Core.Languages.LanguageData _CurrentLang;
        protected G2Core.Languages.LanguageData CurrentLang
        {
            get 
            {
                if (_CurrentLang == null)
                {
                    _CurrentLang = new G2Core.Languages.LanguageData(this.Conn);
                }
                return _CurrentLang; 
            }
        }
	
        protected int _DefaultLangID;
        /// <summary>
        /// Ritorna l'identificatore del linguaggio di default per l'applicazione
        /// dalla tabella Lang.
        /// </summary>
        public int DefaultLangID
        {
            get
            {
                if (_DefaultLangID == 0)
                {
                    DataTable tab = this.Conn.SqlQuery("SELECT * FROM Lang WHERE Default_Lang = 1");
                    if (tab.Rows.Count > 0)
                    {
                        _DefaultLangID = int.Parse(tab.Rows[0]["id_Lang"].ToString());
                    }
                }
                return _DefaultLangID;
            }
        }

        protected AxfReferenceFieldCollection _ExternalFields;
        public virtual AxfReferenceFieldCollection ExternalFields
        {
            get
            {
                return _ExternalFields;
            }
        }

        private NetCms.Connections.Connection _Conn;
        protected NetCms.Connections.Connection Conn
        {
            get
            {
                return _Conn;
            }
        }

        /// <summary>
        /// metodo da implementare nella classe derivata per avere il nome della 
        /// tabella associata alla classe.
        /// </summary>
        public abstract string TableName { get;}

        /// <summary>
        /// metodo da implementare nella classe derivata per specificare la logica che 
        ///  determina il valore ID dell'ultimo record inserito in database.       
        /// </summary>
        public abstract int LastInsert { get; }
        /// <summary>
        /// metodo da implementare nella classe derivata per impostare e/o ottenere il nome della 
        /// label per la tabella associata alla classe.  
        /// </summary>
        public abstract string TableLabel { get;set;}
        protected string _InfoText;

        /// <summary>
        /// Ottiene o imposta un test di descrizione da visualizzare nella web form.
        /// </summary>
        public virtual string InfoText 
        {
            get
            {
                if (_InfoText == null)
                    _InfoText = this.Labels[ AxfLabels.LabelsList.DefaultInfoText];
                return _InfoText;
            }
            set
            {
                _InfoText = value;
            }
        }

        /// <summary>
        /// Inizializza una nuova istanza della classe AxfTable.
        /// </summary>
        public AxfTable()
        {
            _ExternalFields = new Fields.Reference.AxfReferenceFieldCollection();
        }

        /// <summary>
        /// Inizializza una nuova istanza della classe AxfTable specificando una variabile 
        /// <see cref="NetCms.Connections.Connection"></see> per connettersi al database contenente i dati
        /// con cui interagire.
        /// </summary>
        public AxfTable(NetCms.Connections.Connection conn)
            :this()
        { _Conn = conn; }
        
        /// <summary>
        /// Metodo, con opzione di overload, per aggiungere alla tabella dei campi che 
        /// rappresentano delle chiavi esterne ad altre tabella 
        /// </summary>        
        /// <param name="field">la chiave esterna da aggiungere</param>
        public virtual void addExternalField(AxfReferenceField field)
        {
            ExternalFields.Add(field);
        }

        /// <summary>
        /// metodo da implementare nella classe derivata per specificare una 
        /// logica di databinding con dati provenienti da un oggetto di 
        /// tipo System.Collections.Specialized.NameValueCollection 
        /// </summary>
        /// <param name="nameValueCollection">collezione contentente i dati da associare.</param>
        public abstract void DataBind(System.Collections.Specialized.NameValueCollection nameValueCollection);


        /// <summary>
        /// metodo da implementare nella classe derivata per renderizzare gli elementi della classe,  
        /// da utilizzare in output, in un controllo HtmlGenericControl per inserimento/modifica dei dati.  
        /// </summary>
        /// <remarks>
        /// Utilizza questo metodo per ottenere componenti XHTML di input (come <input></input>) per 
        /// operazioni di inserimento dati o modifica dati.
        /// </remarks>
        /// <returns>controllo contenente elementi della classe in versione XHTML per inserire/modificare dati.</returns>         
        public abstract HtmlGenericControl GetControl(params AxfForm[] parentForm);


        /// <summary>
        /// metodo da implementare nella classe derivata per renderizzare gli elementi della classe,  
        /// da utilizzare in output, in un controllo HtmlGenericControl in modalit� di lettura.  
        /// </summary>
        /// <remarks>
        /// Utilizza questo metodo per ottenere componenti XHTML di sola lettura di dati. Questo metodo serve ad avere 
        /// una overview dei dati.
        /// </remarks>
        /// <returns>Controllo contenente elementi della classe in versione XHTML in sola lettura.</returns>
        public abstract HtmlGenericControl GetOverviewControl();

        /// <summary>
        /// metodo da implementare nella classe derivata per effettuare l'inserimento dei dati 
        /// nel database.
        /// </summary>
        /// <returns>Indica se � stato effettuato l'inserimento dei dati nel database o meno.</returns>
        public abstract bool Insert();


        /// <summary>
        /// metodo da implementare nella classe derivata per effettuare l'inserimento di dati 
        /// nel database prelevandoli da un oggetto collezione di tipo System.Collections.Specialized.NameValueCollection
        /// </summary>
        /// <param name="PostData">Oggetto contenente i dati da inserire</param>
        /// <returns>Indica se � stato effettuato l'inserimento dei dati nel database o meno.</returns>        
        public abstract bool Insert(NameValueCollection PostData);

        /// <summary>
        /// metodo da implementare nella classe derivata per effettuare l'aggiornamento dei dati 
        /// nel database. 
        /// </summary>
        /// <returns>Indica se � stato effettuato l'aggiornamento dei dati nel database o meno.</returns>
        public abstract bool Update();

        /// <summary>
        /// metodo da implementare nella classe derivata per effettuare l'aggiornamento di dati 
        /// nel database prelevandoli da un oggetto collezione di tipo System.Collections.Specialized.NameValueCollection
        /// </summary>
        /// <param name="PostData">Oggetto contenente i dati da usare per l'aggiornamento</param>
        /// <returns>Indica se � stato effettuato l'aggiornamento dei dati nel database o meno.</returns>
        public abstract bool Update(NameValueCollection PostData);

        /// <summary>
        /// metodo da implementare nella classe derivata per effettuare l'aggiornamento di dati 
        /// nel database indicando l'ID del record da aggiornare.  
        /// </summary>
        /// <param name="RecordID">ID del record da aggiornare</param>
        /// <returns>Indica se � stato effettuato l'aggiornamento dei dati nel database o meno.</returns>
        public abstract bool Update(int RecordID);


        /// <summary>
        /// Implementa la logica di aggiornamento dei dati nel database indicando l'ID del record da aggiornare.  
        /// ed un oggetto contenente i dati per l'aggiornamento.
        /// </summary>
        /// <param name="RecordID">ID del record da aggiornare</param>
        /// <param name="PostData">Oggetto contenente i dati da usare per l'aggiornamento</param>
        /// <returns>Indica se � stato effettuato l'aggiornamento dei dati nel database o meno.</returns>
        public abstract bool Update(int RecordID, NameValueCollection PostData);
         

        /// <summary>
        /// Verifica se i dati inseriti tramite web form  sono validi.
        /// </summary>
        /// <param name="parentForm">Parametro opzionale contenente il riferimento alla form</param>
        /// <returns>Indica se i dati inseriti sono validi.</returns>
        public bool Validate(params AxfForm[] parentForm)
        {
            return Validate(HttpContext.Current.Request.Form, parentForm);
        }


        /// <summary>
        /// metodo da implementare nella classe derivata per verificare se i dati passati 
        /// in un oggetto di tipo System.Collections.Specialized.NameValueCollection sono validi o meno.
        /// </summary>
        /// <param name="values">Oggetto contenente i dati da validare</param>
        /// <param name="parentForm">Parametro opzionale contenente il riferimento alla form</param>
        /// <returns>Indica se i dati inseriti sono validi.</returns>
        public abstract bool Validate(NameValueCollection values, params AxfForm[] parentForm);

    }
}