using System;
using System.Web;
using System.Xml;

namespace G2Core.AdvancedXhtmlForms
{
    /// <summary>
    /// Questa classe implementa la gestione di stringhe da utilizzare nell'applicazione.
    /// </summary>
    /// <remarks>
    /// Deriva dalla classe base <see cref="G2Core.Languages.Labels"></see>
    /// Utilizza questa classe per accedere ad una lista di stringhe utilizzabili in vari scenari, come ad esempio per
    /// impostare il testo di bottoni o varie tipologie di messaggi all'utente.
    /// Le stringhe sono memorizzate in un file xml, a cui si accede tramite delle chiavi contenute
    /// nell'enumerazione G2Core.AdvancedXhtmlForms.AxfLabels.LabelsList    /// 
    /// </remarks>    
    /// <example>
    /// Segue un esempio di utilizzo e accesso alle lista di label.
    /// <code>
    /// protected string _Label;
    /// public virtual string Label
    ///{
    /// get { return _Label; }
    /// set { _Label = value; }
    ///}
    /// protected AxfLabels Labels
    /// {
    ///    get
    ///    {
    ///         return AxfLabels.CurrentLabels;
    ///     }
    /// }
    /// string error= string.Format(Labels[AxfLabels.LabelsList.IlCampoNonEValido], this.Label);     
    /// string ButtonLabel = Labels[AxfLabels.LabelsList.SubmitLabel];
    /// </code>
    /// </example>
    public class AxfLabels : G2Core.Languages.Labels
    {

        public const String ApplicationKey = "AxfLabelsCacheObj";

        /// <summary>
        /// Inizializza una istanza della classe AxfLabels. 
        /// </summary>
        /// <remarks>Tale classe viene inizializzata caricando un file XML.</remarks>
        /// <param name="absoluteWebRoot">percorso viruale del web server.A tale percorso sar� aggiunto il nome del 
        /// file per ottenere l'url completo del  file XML da caricare: absoluteWebRoot/filename.xaml</param>
        public AxfLabels(string absoluteWebRoot)
            : base(absoluteWebRoot)
        {
        }
        /// <summary>
        /// Inizializza un'istanza della classe AxfLabels. 
        /// </summary>
        /// <remarks>Tale classe viene inizializzata tramite un riferimento al nodo di origine di un file XML.</remarks>
        /// <param name="rootNode">Nodo di origine del documento XML contenente le stringhe</param>
        public AxfLabels(XmlNode rootNode)
            : base(rootNode)
        {
        }


        /// <summary>
        /// Lista di chiavi con cui accedere specifiche stringhe. 
        /// </summary>
        /// <example>
        /// Segue un esempio di accesso alle lista di label.
        /// <code>       
        /// protected AxfLabels Labels         
        /// //ottieni la stringa al nodo SubmitLabel del file XML
        /// string ButtonLabel = Labels[AxfLabels.LabelsList.SubmitLabel];
        /// </code>
        /// </example>
        public enum LabelsList
        {
            CampiConAsteriscoObbligatori, SubmitLabel,
            InserimentoEffettuatoConSuccesso, InserimentoFallito,
            AggiornamentoEffettuatoConSuccesso, AggiornamentoFallito,
            IlCampoEObbligatorio, IlCampoNonEValido, FormatoData,
            Mese, Anno, Ora, Minuti, Giorno,
            IlCampoEObbligatorioEDeveEssereDiQuattroCifre,
            IlCampoNonContieneUnaDataValida,
            Vecchia, VecchiaLasciareIlCampoVuoto,
            LasciareIlCampoVuotoPerMantentenereQuellaAttuale,
            Conferma, FormatoPasswordNonCorretto,
            LunghezzaMassimaCampo, LunghezzaMinimaCampo,LunghezzaEsattaCampo,
            VecchiaPasswordErrata, PasswordNonCoincidenti,
            PasswordNonValide,VecchiaPasswordNuovaPasswordCoincidenti,
            CampoEmailNonValido, CampoNumericoNonValido, CampoTelefonoNonValido,
            CampoFileNonValido, CampoValutaNonValido,CampoAlphaNumericoNonValido,CampoUrlNonValido,
            Modifica, Elimina, Scheda, SchedaOpzioni,
            CompletamentoProcedura,
            OperazioneCompletata,
            CampoDataNonValido,
            CampoCodiceFiscaleNonValido,
            DataSuccessivaAl,
            DataAntecedenteAl,
            Avanti, Indietro, Completa,
            DefaultInfoText,
            MessaggioProceduraCompletata,
            TestoAvanzamentoProcedura,
            StatoAvanzamentoProcedura,
            ProceduraCompletataConSuccesso,
            RiepilogoDatiInseriti,
            CampiNonCoincidenti,
            Confirm, NecessarioAccettareCondizione, DatiRipetuti
        }

        /// <summary>
        /// Restituisce la stringa relativa alla label specificata
        /// </summary>
        /// <param name="label">label con cui indicare la stringa da restituire </param>
        /// <returns>testo della stringa</returns>
        public string this[LabelsList label]
        {
            get
            {
                if (this.LabelsCache.Contains(label.ToString()))
                {
                    return this.FormatLabel(this.LabelsCache[label.ToString()]);
                }
                else
                {
                    XmlNodeList oNodeList = this.XmlLabelsRoot.SelectNodes("/labels/netform/" + label.ToString());
                    if (oNodeList != null && oNodeList.Count > 0)
                    {
                        string value = oNodeList[0].InnerXml;
                        LabelsCache.Add(value, label.ToString());
                        return this.FormatLabel(value);
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Metodo statico che restituisce un'istanza della classe con le stringe contenute in 
        /// un documento xml di default labels_it.xml
        /// </summary>
        public static AxfLabels CurrentLabels
        {
            get
            {
                if (!G2Core.Languages.LabelsCacher.Labels.Contains(AxfLabels.ApplicationKey))
                    G2Core.Languages.LabelsCacher.Labels.Add(new AxfLabels(G2Core.Languages.LabelsCacher.RootNode), AxfLabels.ApplicationKey);

                return (AxfLabels)G2Core.Languages.LabelsCacher.Labels[AxfLabels.ApplicationKey];
            }
        }
    }

}