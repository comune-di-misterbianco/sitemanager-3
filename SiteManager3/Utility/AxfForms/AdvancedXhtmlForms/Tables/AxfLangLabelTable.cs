using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using G2Core.AdvancedXhtmlForms.Fields;
using NetCms.Diagnostics;


namespace G2Core.AdvancedXhtmlForms
{
    /// <summary>
    /// Permette di gestire automaticamente la creazione/aggiornamento di Label 
    /// </summary>
    public class AxfLangLabelTable : AxfTable
    {
        private string _TableLabel;

        /// <summary>
        /// 
        /// </summary>
        public override string TableLabel
        {
            get { return _TableLabel; }
            set { _TableLabel = value; }
        }

        private string _TableName;
        /// <summary>
        /// Imposta o ottiene il la label della tabella. 
        /// </summary>
        public override string TableName
        {
            get
            {
                return _TableName;
            }
        }

        #region Prorpiet� Specifiche per la gestione della Label

        private int _LabelID;
        /// <summary>
        /// Imposta o ottiene l'ID di un record della tabella labels sui cui operare.
        /// </summary>
        public int LabelID
        {
            get { return _LabelID; }
            set { _LabelID = value; }
        }

        private bool _Multiline;
        /// <summary>
        /// Indica se utilizzare multiple linee nei textbox.
        /// </summary>
        public bool Multiline
        {
            get { return _Multiline; }
            set { _Multiline = value; }
        }

        private int _FieldRows = 1;
        /// <summary>
        /// indica il numero di righe da utilizzare per AxfTextBox Field.
        /// </summary>
        public int FieldRows
        {
            get { return _FieldRows; }
            set { _FieldRows = value; }
        }

        private int _FieldColumns = 60;
        /// <summary>
        ///Imposta o ottieni la larghezza in caratteri per l'AxfTextBox Field.  
        /// </summary>
        public int FieldColumns
        {
            get { return _FieldColumns; }
            set { _FieldColumns = value; }
        }

        private int _LabelRecordID;
        /// <summary>
        /// Imposta o ottiene ID di referenza ad un record  della tabella labels.
        /// </summary>
        public int LabelRecordID
        {
            get { return _LabelRecordID; }
            set { _LabelRecordID = value; }
        }

        private AxfTextBox _Field;
        /// <summary>
        /// Elemento di input della form in cui inserire un testo in una specifica lingua.   
        /// </summary>
        public AxfTextBox Field
        {
            get
            {
                if (_Field == null)
                {
                    _Field = new AxfTextBox(this.TableLabel, "LabelField" + this.TableName);
                    _Field.MaxLenght = 0;
                    if (this.Multiline)
                    {
                        _Field.Rows = this.FieldRows;
                    }
                    _Field.Size = this.FieldColumns;
                }
                return _Field;
            }
        }

        private int _Lang;  
        /// <summary>
        /// Valore che identifica una lingua.
        /// </summary>
        public int Lang
        {
            get { return _Lang; }
        }
        
        #endregion

        /*private G2Core.XhtmlControls.InputField _PostbackCheckField;
        private G2Core.XhtmlControls.InputField PostbackCheckField
        {
            get
            {
                if (_PostbackCheckField == null)
                {
                    _PostbackCheckField = new G2Core.XhtmlControls.InputField("PostbackCheckField_" + this.TableName);
                    _PostbackCheckField.AutoLoadValueAfterPostback = true;
                    _PostbackCheckField.Value = "PostedBack";
                }
                return _PostbackCheckField;
            }
        }

        /// <summary>
        /// Indica se � avvenuta una postback sulla form.
        /// </summary>
        public bool IsPostBack
        {
            get
            {
                return PostbackCheckField.RequestVariable.IsPostBack;
            }
        }*/

        private string _PrimaryKey;
        /// <summary>
        /// Ritorna il nome del campo che rappresenta la chaive primaria della tabella.
        /// </summary>
        public string PrimaryKey
        {
            get
            {
                return _PrimaryKey;
            }
        }

        private int _RelatedRecordID = 0;
        /// <summary>
        /// Imposta o ottiene il valore di chiave primaria (ID) del record con su cui effettuare 
        /// gli aggiornamenti.                
        /// </summary>  
        public int RelatedRecordID
        {
            get
            {
                return _RelatedRecordID;
            }
            set
            {
                _RelatedRecordID = value;
            }
        }

        /// <summary>
        /// Indica se l'AxfTextBox Field deve essere valorizzato nella web form.
        /// </summary>
        public bool Required
        {
            get
            {
                return  Field.Required;
            }
            set
            {
                 Field.Required = value;
            }
        }

        private bool BindingsLoaded;

        protected int _LastInsert;
        /// <summary>
        /// Ottiene l'ID dell'ultimo record inserito
        /// </summary>
        public override int LastInsert
        {
            get
            {
                return _LastInsert;
            }
        }

        /// <summary>
        /// Inizializza una nuova instanza della classe.
        /// </summary>
        /// <param name="tableName">Nome della tabella del database associata all'istanza.</param>
        /// <param name="primaryKey">Nome della chiave primaria.</param>
        /// <param name="lang">ID della lingua</param>
        public AxfLangLabelTable(string tableName, string primaryKey, int lang)
            : this(tableName, primaryKey, lang,null)
        {        
        }

        /// <summary>
        /// Inizializza una nuova instanza della classe.
        /// </summary>
        /// <param name="tableName">Nome della tabella del database associata all'istanza.</param>
        /// <param name="primaryKey">Nome della chiave primaria.</param>
        /// <param name="lang">ID della lingua</param>
        /// <param name="conn">Connessione da usare per accedere al database.</param>
        public AxfLangLabelTable(string tableName, string primaryKey,int lang, NetCms.Connections.Connection conn)
            :base(conn)
        {
            _TableName = tableName;
            _TableLabel = tableName;
            _PrimaryKey = primaryKey;
            _Lang = lang;
        }


        /// <summary>
        /// Valorizza l'elemento Field della classe con il valore corrispondente alla propriet� Lang 
        /// nella collezione di label dell'oggetto di tipo G2Core.DatabaseFields.LanguageDatabaseFieldData specificato.
        /// </summary>
        /// <param name="label">Collezione di label.</param>
        public void DataBind(G2Core.DatabaseFields.LanguageDatabaseFieldData label)
        {
            if (!BindingsLoaded)
            {
                Field.Value = label.Values[Lang.ToString()];
                this._LabelID = int.Parse(label.LabelsIDs[Lang.ToString()]);
                _RelatedRecordID = int.Parse(label.RecordID);
                BindingsLoaded = true;
            }
        }

        /// <summary>
        /// Valorizza l'elemento Field della classe con il valore corrispondente alla propriet� Lang 
        /// nella collezione di label dell'oggetto di tipo G2Core.Languages.LangLabel specificato.
        /// </summary>
        /// <param name="label">Collezione di label.</param>
        public void DataBind(G2Core.Languages.LangLabel label)
        {
            if (!BindingsLoaded)
            {
                Field.Value = label.Values[Lang.ToString()];
                BindingsLoaded = true;
                this._LabelID = int.Parse(label.LabelsIDs[Lang.ToString()]);
                _RelatedRecordID = label.ID;
            }
        }

        /// <summary>
        /// Valorizza l'elemento Field della classe con il valore prelevato da un oggetto di collezione
        /// di tipo System.Collections.Specialized.NameValueCollection corrispondente 
        /// alla chiave Field.FieldName.
        /// </summary>
        /// <param name="label">OggettoCollezione di label contenente i dati da associare.</param>
        public override void DataBind(NameValueCollection nameValueCollection)
        {
            if (!BindingsLoaded)
            {
                if (nameValueCollection[this.Field.FieldName] != null)
                    Field.Value = nameValueCollection[this.Field.FieldName];
                BindingsLoaded = true;
            }
        }
        /// <summary>
        /// Restituisce il nome della tabella in database associata all'istanza.
        /// </summary>
        /// <returns>Nome della tabella.</returns>
        public string getTableName()
        {
            return TableName;
        }

        /// <summary>
        /// Restituisce un controllo HtmlGenericControl contenente la versione XHTML
        /// dell'elemento Field della classe per inserire/aggiornarne il valore nel database.  
        /// </summary>
        /// <returns>controllo per inserire/modificare dati.</returns>         
        public override HtmlGenericControl GetControl(params AxfForm[] parentForm)
        {
            if (!IsPostBack && parentForm.Length == 0)
                Diagnostics.TraceMessage(TraceLevel.Verbose, "Accesso alla tabella " + this.TableName, "", "","");

            return this.Field.Control;
        }

        /// <summary>
        /// Restituisce un controllo HtmlGenericControl contenente contenente la versione XHTML
        /// dell'elemento Field della classe in modalit� di sola lettura.  
        /// </summary>
        /// </remarks>
        /// <returns>controllo per visualizzare dati.</returns>                 
        public override HtmlGenericControl GetOverviewControl()
        {
            G2Core.XhtmlControls.Fieldset control = new G2Core.XhtmlControls.Fieldset(this.TableLabel);
            control.Class = "OverviewFieldset";
            
            HtmlGenericControl list = new HtmlGenericControl("ul");
            control.Controls.Add(list);

            list.Controls.Add(Field.GetOverviewControl());

            return control;
        }

        /// <summary>
        /// Verifica se il dato passato in un oggetto di tipo System.Collections.Specialized.NameValueCollection
        /// relativi all'elemento Field � valido o meno.
        /// </summary>
        /// <param name="values">Oggetto contenente il dato da validare</param>
        /// <returns>Indica se il dato inserito � valido.</returns>
        public override bool Validate(NameValueCollection values, params AxfForm[] parentForm)
        {
            bool valid = true;

            if (!this.Field.Required || values.Get(Field.FieldName) != null)
            {
                string input = values.Get(Field.FieldName).ToString();
                bool resultFieldValidation;
                valid = valid & (resultFieldValidation = Field.validateInput(input));
                if (this.Field.FieldStringForLog != "" && parentForm.Length != 0)
                    this.TableStringsForLog.Add(this.Field.FieldStringForLog);
                if (parentForm.Length == 0 && !resultFieldValidation)
                {
                    Diagnostics.TraceMessage(TraceLevel.Warning, Field.FieldStringForLog, "", "","");
                }
            }
            return valid;
        }
              
        #region Insert
        /// <summary>
        /// Implementa la logica d'inserimento di un nuovo record label nel database.                
        /// </summary>
        /// <remarks>       
        /// I valori d'inserimento vengono prelevati dall'oggetto HttpContext.Current.Request.Form
        /// </remarks>
        /// <returns>Indica se � stato effettuato l'inserimento dei dati nel database o meno.</returns>
        public override bool Insert()
        {
            return Insert(HttpContext.Current.Request.Form);
        }

        /// <summary>
        /// Implementa la logica d'inserimento dei dati nel database prelevandoli da un oggetto collezione 
        /// di tipo System.Collections.Specialized.NameValueCollection
        /// </summary>       
        /// <param name="PostData">Oggetto contenente i dati da inserire</param>
        /// <returns>Indica se � stato effettuato l'inserimento dei dati nel database o meno.</returns>        
        public override bool Insert(NameValueCollection PostData)
        {
            if (LabelRecordID > 0)
                _LastInsert = LabelRecordID;
            else
            {
                string sqlrecord = "INSERT INTO labelsrecords (DefaultLang_LabelRecord) VALUES(" + this.Lang + ");";

                _LastInsert = Conn.ExecuteInsert(sqlrecord);
            }
            if (this.Field.RequestVariable.StringValue.Length > 0 || !this.Field.Required)
            {
                string sql = "";
                sql += "INSERT INTO labels";
                sql += " (Text_Label,Record_Label,Lang_Label)";
                sql += " VALUES('" + this.Field.RequestVariable.StringValue + "'," + LastInsert + "," + this.Lang + ");";

                return Conn.Execute(sql);
            }
            return false;
        }
        
        #endregion

        #region Update


        /// <summary>
        /// Implementa la logica di aggiornamento dei dati nel database. 
        /// </summary>
        /// <remarks>       
        /// I valori d'aggiornamento vengono prelevati dall'oggetto HttpContext.Current.Request.Form
        /// </remarks>
        /// <returns>Indica se � stato effettuato l'aggiornamento dei dati nel database o meno.</returns>
        public override bool Update()
        {
            Update(HttpContext.Current.Request.Form);
            return true;
        }

        /// <summary>
        /// Implementa la logica di aggiornamento dei dati nel database indicando l'ID del record da aggiornare.  
        /// </summary>
        /// <param name="RecordID">ID del record da aggiornare</param>
        /// <returns>Indica se � stato effettuato l'aggiornamento dei dati nel database o meno.</returns>        
        public override bool Update(int RecordID)
        {
            this.RelatedRecordID = RecordID;
            Update(HttpContext.Current.Request.Form);
            return true;
        }

        /// <summary>
        /// Implementa la logica di aggiornamento dei dati nel database indicando l'ID del record da aggiornare.  
        /// ed un oggetto contenente i dati per l'aggiornamento.
        /// </summary>
        /// <param name="RecordID">ID del record da aggiornare</param>
        /// <param name="PostData">Oggetto contenente i dati da usare per l'aggiornamento</param>
        /// <returns>Indica se � stato effettuato l'aggiornamento dei dati nel database o meno.</returns>
        public override bool Update(int RecordID, NameValueCollection PostData)
        {
            this.RelatedRecordID = RecordID;
            Update(PostData);
            return true;
        }

        /// <summary>
        /// Implementa la logica di aggiornamento dei dati nel database prelevandoli da un oggetto collezione di tipo System.Collections.Specialized.NameValueCollection
        /// </summary>
        /// <param name="PostData">Oggetto contenente i dati da usare per l'aggiornamento</param>
        /// <returns>Indica se � stato effettuato l'aggiornamento dei dati nel database o meno.</returns>
        public override bool Update(NameValueCollection PostData)
        {
            if (RelatedRecordID == 0)
                return false;

            if (Validate(PostData))
            {
               /* string value = "";

                if (PostData.Get(Field.FieldName) != null)
                    value = PostData.Get(Field.FieldName).ToString();

                value = Field.filterValueForDB(value);*/
                string sql = "";

                sql += "UPDATE labels SET ";
                sql += " Text_Label = '" + this.Field.RequestVariable.StringValue + "'";
                sql += " WHERE Record_Label = " + RelatedRecordID;

                if (Conn != null)
                    return Conn.Execute(sql);
            }
            return false;
        }
        

        #endregion
    }
}