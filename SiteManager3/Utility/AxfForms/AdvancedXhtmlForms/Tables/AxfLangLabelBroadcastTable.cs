using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using G2Core.AdvancedXhtmlForms.Fields;
using NetCms.Diagnostics;


namespace G2Core.AdvancedXhtmlForms
{
    /// <summary>
    /// La classe AxfLangLabelBroadcastTable permette di gestire automaticamente la creazione di un record di tipo Lang Label su database
    /// e di estendere la creazione dell'etichetta automaticamente a tutte le lingue del sistema.
    /// </summary>
    public class AxfLangLabelBroadcastTable : AxfTable
    {
        private string _TableLabel;

        /// <summary>
        /// Imposta o ottiene il la label della tabella. 
        /// </summary>
        public override string TableLabel
        {
            get { return _TableLabel; }
            set { _TableLabel = value; }
        }

        private string _TableName;


        /// <summary>
        /// Ottiene  il nome della tabella associata alla classe.
        /// </summary>
        public override string TableName
        {
            get
            {
                return _TableName;
            }
        }

        #region Prorpiet� Specifiche per la gestione della Label

        private int _LabelID;
        /// <summary>
        /// Imposta o ottiene l'ID di un record della tabella labels sui cui operare.
        /// </summary>
        public int LabelID
        {
            get { return _LabelID; }
            set { _LabelID = value; }
        }

        private bool _Multiline;
        /// <summary>
        /// Indica se utilizzare multiple linee nei textbox.
        /// </summary>
        public bool Multiline
        {
            get { return _Multiline; }
            set { _Multiline = value; }
        }

        private int _FieldRows = 1;
        /// <summary>
        /// indica il numero di righe da utilizzare in un textbox multilinee.
        /// </summary>
        public int FieldRows
        {
            get { return _FieldRows; }
            set { _FieldRows = value; }
        }

        private int _FieldColumns = 60;

        /// <summary>
        ///Imposta o ottieni la larghezza in caratteri per i text box.  
        /// </summary>
        public int FieldColumns
        {
            get { return _FieldColumns; }
            set { _FieldColumns = value; }
        }

        private int _LabelRecordID;
        /// <summary>
        /// Imposta o ottiene ID di referenza ad un record  della tabella labels.
        /// </summary>
        public int LabelRecordID
        {
            get { return _LabelRecordID; }
            set { _LabelRecordID = value; }
        }
        
        #endregion

        /*private G2Core.XhtmlControls.InputField _PostbackCheckField;
        private G2Core.XhtmlControls.InputField PostbackCheckField
        {
            get
            {
                if (_PostbackCheckField == null)
                {
                    _PostbackCheckField = new G2Core.XhtmlControls.InputField("PostbackCheckField_" + this.TableName);
                    _PostbackCheckField.AutoLoadValueAfterPostback = true;
                    _PostbackCheckField.Value = "PostedBack";
                }
                return _PostbackCheckField;
            }
        }

        /// <summary>
        /// Indica se � avvenuta una postback sulla form.
        /// </summary>
        public bool IsPostBack
        {
            get
            {
                return PostbackCheckField.RequestVariable.IsPostBack;
            }
        }*/

        private AxfFieldsCollection _Fields;

        /// <summary>
        /// Collezione di <see cref="G2Core.AdvancedXhtmlForms.Fields.AxfField"></see>
        /// </summary>
        /// <remarks>
        /// Questa collezione contiene almeno un  TextBox di base.  
        /// </remarks>
        public AxfFieldsCollection Fields
        {
            get
            {
                if (_Fields == null)
                {

                    _Fields = new AxfFieldsCollection(this);
                    foreach (G2Core.Languages.Language lang in LangData.Languages)
                    {
                        AxfTextBox Field = new AxfTextBox(this.TableLabel + " in " + lang.Name, "LabelField" + this.TableName + lang.ID);
                        Field.Required = true;
                        Field.MaxLenght = 0;
                        if (this.Multiline)
                            Field.Rows = this.FieldRows;
                        
                        Field.Size = this.FieldColumns;
                        _Fields.Add(Field);
                    }
                }
                return _Fields;
            }
        }
        private G2Core.Languages.LangDataSource LangData;

        private string _PrimaryKey;
        /// <summary>
        /// Ritorna il nome del campo che rappresenta la chaive primaria della tabella.
        /// </summary>
        public string PrimaryKey
        {
            get
            {
                return _PrimaryKey;
            }
        }

        private int _RelatedRecordID = 0;
        /// <summary>
        /// Imposta o ottiene il valore di chiave primaria (ID) del record con su cui effettuare 
        /// gli aggiornamenti.                
        /// </summary>  
        public int RelatedRecordID
        {
            get
            {
                return _RelatedRecordID;
            }
            set
            {
                _RelatedRecordID = value;
            }
        }

        private bool BindingsLoaded;

        protected int _LastInsert;
        /// <summary>
        /// Ottiene l'ID dell'ultimo record inserito
        /// </summary>
        public override int LastInsert
        {
            get
            {
                return _LastInsert;
            }
        }

        /// <summary>
        /// Inizializza una nuova instanza della classe.
        /// </summary>
        /// <param name="tableName">Nome della tabella del database associata all'istanza.</param>
        /// <param name="primaryKey">Nome della chiave primaria</param>
        /// <param name="langData">Insieme dei lingiaggi da utilizzare.</param>
        /// <param name="conn">Connessione da usare per accedere al database.</param>
        public AxfLangLabelBroadcastTable(string tableName, string primaryKey, G2Core.Languages.LangDataSource langData, NetCms.Connections.Connection conn)
            :base(conn)
        {
            _TableName = tableName;
            _PrimaryKey = primaryKey;
            LangData = langData;
        }


        /// <summary>
        /// Implementa la logica di valorizzazione dei campi 
        /// della corrente instanza con dati provenienti da un oggetto di 
        /// tipo G2Core.Languages.LangLabel
        /// </summary>        
        /// <param name="label">Oggetto contenente i dati.</param>
        public void DataBind(G2Core.Languages.LangLabel label)
        {
            if (!BindingsLoaded)
            {
                int i = 0;
                foreach (G2Core.Languages.Language lang in LangData.Languages)
                {
                    AxfField Field = this.Fields[i++];
                    Field.Value = label.Values[lang.ToString()];
                    this.LabelID = int.Parse(label.LabelsIDs[lang.ToString()]);
                }
                BindingsLoaded = true;
            }
        }
        
        /// <summary>
        /// Questo metodo non pu� essere utilzzato per questa classe
        /// </summary>
        /// <param name="nameValueCollection"></param>
        public override void DataBind(NameValueCollection nameValueCollection)
        {
            Exception ex = new Exception("Impossibile utilizzare questo tipo di databind per gli oggetti della classe AxfLangLabelBroadcastTable");
            /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
            ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Warning, ex);*/
            Diagnostics.TraceMessage(TraceLevel.Warning, ex.Message, "", "", "");
        }


        /// <summary>
        /// Restituisce il nome della tabella in database associata all'istanza.
        /// </summary>
        /// <returns>Nome della tabella.</returns>
        public string getTableName()
        {
            return TableName;
        }


        /// <summary>
        /// Restituisce un controllo HtmlGenericControl contenente in versione XHTML 
        /// gli elementi dell'istanza da utilizzare in output nella web form per inserimento/modifica dei dati. 
        /// </summary>
        /// <remarks>
        /// Utilizza questo metodo per ottenere componenti XHTML di input (come <input></input>) per 
        /// operazioni di inserimento dati o modifica dati.
        /// </remarks>
        /// <returns>controllo contenente elementi della classe in versione XHTML per inserire/modificare dati.</returns>         
        public override HtmlGenericControl GetControl(params AxfForm[] parentForm)
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            for (int i = 0; i < this.Fields.Count; i++)
                div.Controls.Add(this.Fields[i].Control);
            
            if (!IsPostBack && parentForm.Length == 0)
                Diagnostics.TraceMessage(TraceLevel.Verbose, "Accesso alla tabella " + this.TableName, "", "","");

            return div;
        }


        /// <summary>
        /// Restituisce un controllo HtmlGenericControl contenente in versione XHTML 
        /// gli elementi dell'istanza da utilizzare in output nella web form per inserimento/modifica dei dati. 
        /// </summary>
        /// <remarks>
        /// Utilizza questo metodo per ottenere componenti XHTML di input (come <input></input>) per 
        /// operazioni di inserimento dati o modifica dati.
        /// </remarks>
        /// <returns>controllo contenente elementi della classe in versione XHTML per inserire/modificare dati.</returns>         
        public override HtmlGenericControl GetOverviewControl()
        {
            G2Core.XhtmlControls.Fieldset control = new G2Core.XhtmlControls.Fieldset(this.TableLabel);
            control.Class = "OverviewFieldset";
            for (int i = 0; i < this.Fields.Count; i++)
            {
                AxfField Field = this.Fields[i];
                HtmlGenericControl gtitle = new HtmlGenericControl("p");
                control.Controls.Add(gtitle);
                gtitle.InnerHtml = "<em>" + Field.Label + "</em>";

                HtmlGenericControl list = new HtmlGenericControl("ul");
                control.Controls.Add(list);

                list.Controls.Add(Field.GetOverviewControl());
            }

            return control;
        }


        /// <summary>
        /// Verifica se i dati passati in un oggetto di tipo System.Collections.Specialized.NameValueCollection sono validi o meno.
        /// </summary>
        /// <param name="values">Oggetto contenente i dati da validare</param>
        /// <returns>Indica se i dati inseriti sono validi.</returns>
        public override bool Validate(NameValueCollection values, params AxfForm[] parentForm)
        {
            bool valid = true;
            int i = 0;

            foreach (AxfField field in this.Fields)
            {
                if (values.Get(field.FieldName) != null)
                {
                    string input = values.Get(field.FieldName).ToString();
                    bool resultFieldValidation;
                    valid = valid & (resultFieldValidation = field.validateInput(input));
                    if (field.FieldStringForLog != "" && parentForm.Length != 0)
                        this.TableStringsForLog.Add(field.FieldStringForLog);
                    if (parentForm.Length == 0 && !resultFieldValidation)
                    {
                        Diagnostics.TraceMessage(TraceLevel.Warning, field.FieldStringForLog, "", "","");
                    }
                }
            }
            return valid;
        }

        #region Insert

        /// <summary>
        /// Implementa la logica d'inserimento dei dati nel database.                
        /// </summary>
        /// <remarks>
        /// Il metodo costruisce una query sql di insert dalle propriet� della classe quali
        /// TableName ed i campi defini nella collezione Fields. 
        /// I valori d'inserimento vengono prelevati dall'oggetto HttpContext.Current.Request.Form
        /// </remarks>
        /// <returns>Indica se � stato effettuato l'inserimento dei dati nel database o meno.</returns>
        public override bool Insert()
        {
            return Insert(HttpContext.Current.Request.Form);
        }

        /// <summary>
        /// Implementa la logica d'inserimento dei dati nel database prelevandoli da un oggetto collezione 
        /// di tipo System.Collections.Specialized.NameValueCollection e di creare addizionalmente una entry per 
        /// ogni lingua specificata in LangData. 
        /// </summary>        
        /// <param name="PostData">Oggetto contenente i dati da inserire</param>
        /// <returns>Indica se � stato effettuato l'inserimento dei dati nel database o meno.</returns>        
        public override bool Insert(NameValueCollection PostData)
        {
            bool result = Validate(PostData);
            if (!result)
                return false;//Dati inseriti dall'utente nelle form non validi
            
            if (LabelRecordID > 0)
                _LastInsert = LabelRecordID;
            else
            {
                string sqlrecord = "INSERT INTO labelsrecords (DefaultLang_LabelRecord) VALUES(" + LangData.DefaultLang + ");";
                _LastInsert = Conn.ExecuteInsert(sqlrecord);
            }
            int i = 0;
            foreach (G2Core.Languages.Language lang in LangData.Languages)
            {
                AxfField Field = this.Fields[i++];
                string value = Field.filterValueForDB(Field.RequestVariable.StringValue);
                if (value.Length > 0)
                {
                    string sql = "";
                    sql += "INSERT INTO labels";
                    sql += " (Text_Label,Record_Label,Lang_Label)";
                    sql += " VALUES(" + value + "," + LastInsert + "," + lang + ");";

                    result = result & Conn.Execute(sql);
                }
            }

            return result;
        }


        #endregion

        #region Update

        /// <summary>
        /// Questo metodo non pu� essere utilzzato per questa classe
        /// </summary>
        /// <returns></returns>
        public override bool Update()
        {
            Update(HttpContext.Current.Request.Form);
            return true;
        }

        /// <summary>
        /// Questo metodo non pu� essere utilzzato per questa classe
        /// </summary>
        /// <param name="RecordID"></param>
        /// <returns></returns>
        public override bool Update(int RecordID)
        {
            this.RelatedRecordID = RecordID;
            Update(HttpContext.Current.Request.Form);
            return true;
        }

        /// <summary>
        /// Questo metodo non pu� essere utilzzato per questa classe
        /// </summary>
        /// <param name="RecordID"></param>
        /// <param name="PostData"></param>
        /// <returns></returns>
        public override bool Update(int RecordID, NameValueCollection PostData)
        {
            this.RelatedRecordID = RecordID;
            Update(PostData);
            return true;
        }

        /// <summary>
        /// Questo metodo non pu� essere utilzzato per questa classe 
        /// </summary>
        /// <param name="PostData"></param>
        /// <returns></returns>
        public override bool Update(NameValueCollection PostData)
        {
            throw new Exception("Non � possibile  eseguire il metodo Update su un istanza di un oggetto della classe AxfLangLabelBroadcastTable");

        }
        #endregion
    }
}