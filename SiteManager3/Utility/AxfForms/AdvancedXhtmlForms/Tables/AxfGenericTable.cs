using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using G2Core.AdvancedXhtmlForms.Fields;
using G2Core.AdvancedXhtmlForms.Fields.Reference;
using NetCms.Diagnostics;

namespace G2Core.AdvancedXhtmlForms
{
    /// <summary>
    /// Permette di costruire delle web form di inserimento/aggiornamento dati
    /// specificandone i campi ed i relativi metodi di interazione con il database. 
    /// </summary>
    /// <remarks>
    /// Usa questa classe per casi generici di web form di inserimento ed aggiornamento dati.
    /// Puoi aggiungere un 'istanza  di tale classe ad un'ogetto di tipo <see cref="G2Core.AdvancedXhtmlForms.AxfForm"></see> 
    /// per configurare elementi addizionali quali bottoni di submit, disclaimer da aggiungere alla web form  e per 
    /// legare tabelle logicamente connesse tra loro in operazioni di inserimento ed aggiornamento dati.
    /// </remarks>
    /// <example>Segue un esempio di utilizzo della classe AxfGenericTable.
    /// <code>    
    /// private G2Core.AdvancedXhtmlForms.AxfGenericTable _AnagraficaFormTable;
    /// private G2Core.AdvancedXhtmlForms.AxfGenericTable AnagraficaFormTable
    /// {
    ///    get
    ///    {
    ///        if (_AnagraficaFormTable == null)
    ///        {
    ///            AxfGenericTable anagrafica = new AxfGenericTable("Anagrafica", "id_Anagrafica", Conn);
    ///            #region Campi Anagrafica
    ///
    ///
    ///            AxfTextBox nome = new AxfTextBox("Nome", "Nome_Anagrafica");
    ///            nome.Size = 60;
    ///            nome.MaxLenght = 1000;            
    ///            anagrafica.addField(nome);
    ///            
    ///            AxfTextBox cognome = new AxfTextBox("Cognome", "Cognome_Anagrafica");
    ///            cognome.Size = 60;
    ///            cognome.Required = true;
    ///            anagrafica.addField(cognome);
    ///                        
    ///            AxfTextBox indirizzo = new AxfTextBox("Indirizzo", "Indirizzo_Anagrafica");
    ///            indirizzo.Size = 60;
    ///            indirizzo.MaxLenght = 1000;
    ///            //citta.Required = true;
    ///            anagrafica.addField(indirizzo);
    ///            
    ///            AxfTextBox citta = new AxfTextBox("Citt�", "Citta_Anagrafica");
    ///            citta.Size = 60;
    ///            //citta.Required = true;
    ///            anagrafica.addField(citta);
    ///            
    ///            AxfDropDownList provincia = new AxfDropDownList("Provincia", "Provincia_Anagrafica");
    ///            provincia.ClearList();
    ///            provincia.addItem("", "0");
    ///            provincia.setDataBind("SELECT Nome_Provincia,id_Provincia FROM Provincie ORDER BY Nome_Provincia", Conn);
    ///            anagrafica.addField(provincia);
    ///               
    ///            AxfTextBox codicefiscale = new AxfTextBox("CodiceFiscale", "CodiceFiscale_Anagrafica");
    ///            codicefiscale.Size = 16;
    ///            codicefiscale.MaxLenght = 16;
    ///            anagrafica.addField(codicefiscale);
    ///            
    ///            AxfDropDownList sesso = new AxfDropDownList("Sesso", "Sesso_Anagrafica");
    ///            sesso.addItem("M", "0");
    ///            sesso.addItem("F", "1");
    ///            sesso.Required = true;
    ///            anagrafica.addField(sesso);
    ///            
    ///            #endregion
    ///            _AnagraficaFormTable = anagrafica;
    ///        }
    ///        return _AnagraficaFormTable;
    ///    }
    /// }
    /// private G2Core.AdvancedXhtmlForms.AxfGenericTable _UserFormTable;
    /// private G2Core.AdvancedXhtmlForms.AxfGenericTable UserFormTable
    /// {
    ///    get
    ///    {
    ///        if (_UserFormTable == null)
    ///        {
    ///            AxfGenericTable user = new AxfGenericTable("Users", "id_User", Conn);
    ///
    ///            #region Campi user
    ///
    ///            AxfTextBox Nomeutente = new AxfTextBox("Nome Utente", "Name_User");
    ///            Nomeutente.Size = 60;
    ///            Nomeutente.Required = true;
    ///            user.addField(Nomeutente);
    ///
    ///            AxfPassword pwd = new AxfPassword("Password", "Password_User");
    ///            pwd.CyperType = AxfPassword.CyperingType.MD5;
    ///            pwd.HighSecurityField = true;
    ///            if (ID == 0)
    ///                pwd.Required = true;
    ///            else
    ///                pwd.Required = false;
    ///            user.addField(pwd);
    ///            //aggiungi una chiave esterna logica alla tabella anagrafe indicando il nome della tabella esterna
    ///            // e il campo di id esterna
    ///            AxfReferenceField anagrafica = new AxfReferenceField("Anagrafica", "Anagrafica_User");
    ///            user.addExternalField(anagrafica);       
    ///            
    ///            #endregion
    ///
    ///            _UserFormTable = user;
    ///        }
    ///        return _UserFormTable;
    ///    }
    /// }    
    /// 
    /// //metodo per costruire un layout di inserimento di una nuova entit� Utente
    /// //che utilizza i campi distribuiti su pi� tabelle 
    /// public HtmlGenericControl NewView()
    /// {
    ///     HtmlGenericControl div = new HtmlGenericControl("div");
    ///     AxfForm form = new AxfForm();
    ///     //aggiungi alla form le tabelle che contengono dati necessari per creare un utente
    ///     form.Tables.Add(AnagraficaFormTable);
    ///     form.Tables.Add(UserFormTable);    
    ///     //se � stata fatta una postback della forma, procedi con la validazione ed inserimento
    ///     //dei dati nel database
    ///     if (CommonData.IsPostBack)
    ///         form.serializedInsert();
    ///     div.Controls.Add(form.getFieldset("Nuovo Utente"));
    ///     return div;        
    /// }
    /// </code> 
    /// </example>
    public class AxfGenericTable : AxfTable
    {
        protected AxfFieldsCollection _Fields;
        /// <summary>
        /// Collezione di <see cref="G2Core.AdvancedXhtmlForms.Fields.AxfField"></see>
        /// </summary>
        /// <remarks>
        /// Questa collezione rapprenta i campi di raccolta dati della web form.  
        /// </remarks>
        public AxfFieldsCollection Fields
        {
            get
            {
                return _Fields;
            }
        }

        protected AxfFieldsCollection _FieldsRenderOrder;
        /// <summary>
        /// Collezione di <see cref="G2Core.AdvancedXhtmlForms.Fields.AxfField"></see>
        /// da utilizzare se si vuole specificare uno specifico ordine di rendering della lista di 
        /// campi.Se tale collezione � vuota, l'ordine seguito � quello della collezione Fields.
        /// </summary>
        public AxfFieldsCollection FieldsRenderOrder
        {
            get
            {
                return _FieldsRenderOrder;
            }
        }

        private string _TableLabel;
        /// <summary>
        /// Imposta o ottiene il la label della tabella. 
        /// </summary>
        public override string TableLabel
        {
            get { return _TableLabel; }
            set { _TableLabel = value; }
        }

        private string _TableName;

        /// <summary>
        /// Ottiene  il nome della tabella associata alla classe.
        /// </summary>
        public override string TableName
        {
            get
            {
                return _TableName;
            }
        }
		

        /*private G2Core.XhtmlControls.InputField _PostbackCheckField;        
        private G2Core.XhtmlControls.InputField PostbackCheckField
        {
            get
            {
                if (_PostbackCheckField == null)
                {
                    _PostbackCheckField = new G2Core.XhtmlControls.InputField("PostbackCheckField_" + this.TableName);
                    _PostbackCheckField.AutoLoadValueAfterPostback = true;
                    _PostbackCheckField.Value = "PostedBack";
                }
                return _PostbackCheckField;
            }
        }

        /// <summary>
        /// Indica se si � verificata una postback.
        /// </summary>
        public bool IsPostBack
        {
            get
            {
                return PostbackCheckField.RequestVariable.IsPostBack;
            }
        }*/

        private string _PrimaryKey;
        /// <summary>
        /// Ritorna il nome del campo che rappresenta la chaive primaria della tabella.
        /// </summary>
        public string PrimaryKey
        {
            get
            {
                return _PrimaryKey;
            }
        }

        private int _RelatedRecordID = 0;
        /// <summary>
        /// Imposta o ottiene il valore di chiave primaria (ID) del record con su cui effettuare 
        /// gli aggiornamenti.                
        /// </summary>       
        public int RelatedRecordID
        {
            get
            {
                return _RelatedRecordID;
            }
            set
            {
                _RelatedRecordID = value;
            }
        }

        private bool BindingsLoaded;

        protected int _LastInsert;
        /// <summary>
        /// Ottiene l'ID dell'ultimo record inserito
        /// </summary>
        public override int LastInsert
        {
            get
            {
                return _LastInsert;
            }
        }

     /*   public AxfGenericTable(string tableName, string primaryKey)
            : this(tableName, primaryKey, null)
        {
        }
        */
        /// <summary>
        /// Inizializza una nuova instanza della classe.
        /// </summary>
        /// <param name="tableName">Nome della tabella del database associata all'istanza.</param>
        /// <param name="primaryKey">Nome della chiave primaria</param>
        /// <param name="conn">Connessione da usare per accedere al database.</param>
        public AxfGenericTable(string tableName, string primaryKey, NetCms.Connections.Connection conn)
            :base(conn)
        {
            _TableName = tableName;
            _PrimaryKey = primaryKey;
            _Fields = new AxfFieldsCollection(this);
            _FieldsRenderOrder = new AxfFieldsCollection(this);
        }

        /// <summary>
        /// Implementa la logica di valorizzazione dei campi 
        /// della corrente instanza con i dati provenienti dalla tabella 
        /// TableName e record ID specificato in RelatedRecordID.
        /// </summary>
        public virtual void bindFields()
        {
            if (RelatedRecordID != 0 && !BindingsLoaded)
            {
                string sql = "";
                sql += "SELECT * FROM " + TableName;
                sql += " WHERE " + PrimaryKey + " = " + RelatedRecordID;

                DataTable table = Conn.SqlQuery(sql);
                foreach (DataRow row in table.Rows)
                {
                    foreach (AxfField field in this.Fields)
                    {
                        if(table.Columns.Contains(field.FieldName))
                        field.Value = row[field.FieldName].ToString();
                    }
                }

                BindingsLoaded = true;
            }
        }

        /// <summary>
        /// Implementa la logica di valorizzazione dei campi 
        /// della corrente instanza con i dati contenuti nell'oggeto DataRow specificato      
        /// </summary>
        /// <param name="row">Elemento contenente i dati da associare.</param>
        public virtual void bindFields(DataRow row)
        {
            if (row != null && !BindingsLoaded)
            {
                foreach ( AxfField field in this.Fields )
                    field.Value = row[field.FieldName].ToString();
                BindingsLoaded = true;
            }
        }

        /// <summary>
        /// Implementa la logica di valorizzazione dei campi 
        /// della corrente instanza estraendo i dati dalla tabella passata corrispondenti
        /// alla riga relativa a RelatedRecordID.                
        /// </summary>
        /// <param name="table">tabella contenente i dati da associare.</param>
        public virtual void bindFields(DataTable table)
        {
            if (table != null && !BindingsLoaded)
            {
                DataRow[] row = table.Select(this.PrimaryKey + " = " + RelatedRecordID);
                if(row.Length>0)
                    bindFields(row[0]);
                BindingsLoaded = true;
            }
        }

        /// <summary>
        /// Implementa la logica di valorizzazione dei campi 
        /// della corrente instanza con dati provenienti da un oggetto di 
        /// tipo System.Collections.Specialized.NameValueCollection 
        /// </summary>
        /// <param name="nameValueCollection">Collezione contenente i dati da associare</param>
        public override void DataBind(NameValueCollection nameValueCollection)
        {
            if (!BindingsLoaded)
            {
                foreach (AxfField field in this.Fields)
                {
                    if (nameValueCollection[field.FieldName] != null)
                        field.Value = nameValueCollection[field.FieldName];
                }

                BindingsLoaded = true;
            }
        }

        /// <summary>
        /// Implementa la logica di valorizzazione dei campi 
        /// della corrente instanza con dati provenienti da un oggetto di 
        /// tipo System.Data.DataRow 
        /// </summary>
        /// <param name="row">Oggetto contente i valori</param>
        public void DataBind(DataRow row)
        {
            if (!BindingsLoaded)
            {
                if (row != null)
                {
                    foreach (AxfField field in this.Fields)
                    {
                        if (row.Table.Columns.Contains(field.FieldName))
                            field.Value =string.Format(field.stringFormat,row[field.FieldName]);
                    }
                }
                BindingsLoaded = true;
            }
        }


        /// <summary>
        /// Riempie gli attributi di un oggetto con i valori in  HttpContext.Current.Request.Form       
        /// </summary>
        /// <param name="objToFill">oggetto che sar� valorizzato</param>        
        public void FillObjFromFormValues(Object objToFill)
        {
            NameValueCollection post = HttpContext.Current.Request.Form;
            Type objType = objToFill.GetType();
            System.Reflection.PropertyInfo[] objPropertiesArray = objType.GetProperties();
            
            string value = "";
            foreach (System.Reflection.PropertyInfo objProperty in objPropertiesArray)
            {
                value = "";
                if (post.Get(objProperty.Name) != null && post.Get(objProperty.Name).ToString().Length > 0)
                {
                    value = post.Get(objProperty.Name).ToString();
                    //value = Fields[objProperty.Name].filterValueForDB(value);
                    //objProperty.SetValue(objToFill, G2Core.Common.Utility.ChangeType(post.Get(objProperty.Name), objProperty.PropertyType), null);
                    objProperty.SetValue(objToFill, G2Core.Common.Utility.ChangeType(value, objProperty.PropertyType), null);
                }
            }
        }

        /// <summary>
        /// Imposta a stringhe vuote i valori dei campi dell'istanza.
        /// </summary>
        public void ResetField()
        {
            foreach (AxfField field in this.Fields)
            {
                field.Value = String.Empty;
            }
        }

        /// <summary>
        /// Aggiunge un elemnto di tipo <see cref="G2Core.AdvancedXhtmlForms.Fields.AxfField"></see> alla collezione 
        /// <see cref="G2Core.AdvancedXhtmlForms.Fields.AxfField">.
        /// </summary>
        /// <param name="field">Oggetto da aggiungere alla collezione.</param>
        public virtual void addField(AxfField field)
        {
            Fields.Add(field);
        }

        /// <summary>
        /// Restituisce il nome della tabella in database associata all'istanza.
        /// </summary>
        /// <returns>Nome della tabella.</returns>
        public string getTableName()
        {
            return TableName;
        }


        /// <summary>
        /// Restituisce un controllo HtmlGenericControl contenente in versione XHTML 
        /// gli elementi dell'istanza da utilizzare in output nella web form 
        /// </summary>
        /// <returns>Controllo contenente elementi della classe in versione XHTML</returns>
        public HtmlGenericControl getForm(params AxfForm[] parentForm)
        {
            bindFields();
            HtmlGenericControl div = new HtmlGenericControl("div");

            if (!string.IsNullOrEmpty(this.InfoText))
            {
                HtmlGenericControl p = new HtmlGenericControl("p");
                p.Attributes.Add("class", "infotext");
                p.InnerHtml = this.InfoText;
                div.Controls.Add(p);
            }

            div.Controls.Add(this.PostbackCheckField.Control);

            AxfFieldsCollection fieldToRender = this.FieldsRenderOrder.Count > 0 ? this.FieldsRenderOrder : this.Fields;
            foreach(AxfField field in fieldToRender)
            {
                if (field.RenderField)
                    div.Controls.Add(field.Control);
            }

            if (!IsPostBack && parentForm.Length==0)
                Diagnostics.TraceMessage(TraceLevel.Verbose, "Accesso alla tabella " + this.TableName, "", "","");

            return div;
        }


        /// <summary>
        /// Restituisce un controllo HtmlGenericControl contenente la versione XHTML
        /// degli elementi dell'istanza da utilizzare in output nella web form per inserimento/modifica dei dati.  
        /// </summary>
        /// <remarks>
        /// Utilizza questo metodo per ottenere componenti XHTML di input (come <input></input>) per 
        /// operazioni di inserimento dati o modifica dati.
        /// </remarks>
        /// <returns>controllo contenente elementi della classe in versione XHTML per inserire/modificare dati.</returns>         
        public override HtmlGenericControl GetControl(params AxfForm[] parentForm)
        {
            return getForm(parentForm);
        }

        /// <summary>
        /// Restituisce un controllo HtmlGenericControl contenente in versione XHTML 
        /// gli elementi dell'istanza da utilizzare in output nella web form in modalit� di lettura.  
        /// </summary>
        /// <remarks>
        /// Utilizza questo metodo per ottenere componenti XHTML di sola lettura di dati. Questo metodo serve ad avere 
        /// una overview dei dati.
        /// </remarks>
        /// <returns>Controllo contenente elementi della classe in versione XHTML in sola lettura.</returns>
        /// </summary>
        /// <remarks>
        /// Utilizza questo metodo per ottenere componenti XHTML di input (come <input></input>) per 
        /// operazioni di inserimento dati o modifica dati.
        /// </remarks>
        /// <returns>controllo contenente elementi della classe in versione XHTML per inserire/modificare dati.</returns>         
        public override HtmlGenericControl GetOverviewControl()
        {
            G2Core.XhtmlControls.Fieldset control = new G2Core.XhtmlControls.Fieldset(this.TableLabel);
            control.Class = "OverviewFieldset";
            
            HtmlGenericControl list = new HtmlGenericControl("ul");
            control.Controls.Add(list);

            foreach (AxfField field in this.Fields)
                list.Controls.Add(field.GetOverviewControl());

            return control;
        }


        /// <summary>
        /// Verifica se i dati passati in un oggetto di tipo System.Collections.Specialized.NameValueCollection sono validi o meno.
        /// </summary>
        /// <param name="values">Oggetto contenente i dati da validare</param>
        /// <returns>Indica se i dati inseriti sono validi.</returns>
        public override bool Validate(NameValueCollection values, params AxfForm[] parentForm)
        {
            bool valid = true;
            int i = 0; 

            foreach(AxfField field in this.Fields)
            {
                string input="";
                if (values.Get(field.RequestKey) != null)
                    input = values.Get(field.RequestKey);

                bool resultFieldValidation;
                valid = valid & (resultFieldValidation = field.validateInput(input));
                
                if (field.FieldStringForLog != "" && parentForm.Length!=0)
                    this.TableStringsForLog.Add(field.FieldStringForLog);

                if (parentForm.Length == 0 && !resultFieldValidation)
                {
                    Diagnostics.TraceMessage(TraceLevel.Warning, field.FieldStringForLog, "", "","");
                }

                /*if (values.Get(field.FieldName) == null)                
                {
                    string input = values.Get(field.FieldName).ToString();
                    valid = valid & field.validateInput(input);
                }*/
            }
            return valid;
        }
              
        /// <summary>
        /// Imposta la label di uno dei field della tabella avente la chiave specificata.  
        /// </summary>
        /// <param name="ControlID">chiave del Field di cui modificare la Label</param>
        /// <param name="NewLabel">Label da impostare.</param>
        public void SetLabel(string ControlID, string NewLabel)
        {
            Fields[ControlID].Label = NewLabel;
        }

        #region Insert

        /// <summary>
        /// Implementa la logica d'inserimento dei dati nel database.                
        /// </summary>
        /// <remarks>
        /// Il metodo costruisce una query sql di insert dalle propriet� della classe quali
        /// TableName ed i campi defini nella collezione Fields. 
        /// I valori d'inserimento vengono prelevati dall'oggetto HttpContext.Current.Request.Form
        /// </remarks>
        /// <returns>Indica se � stato effettuato l'inserimento dei dati nel database o meno.</returns>
        public override bool Insert()
        {
            return Insert(HttpContext.Current.Request.Form);
        }

        /// <summary>
        /// Implementa la logica d'inserimento dei dati nel database prelevandoli da un oggetto collezione 
        /// di tipo System.Collections.Specialized.NameValueCollection
        /// </summary>
        /// <remarks>
        /// Il metodo costruisce una query sql di insert dai dati dell'istanza quali
        /// tablename ed i campi defini nella collezione Fields. 
        /// </remarks>
        /// <param name="PostData">Oggetto contenente i dati da inserire</param>
        /// <returns>Indica se � stato effettuato l'inserimento dei dati nel database o meno.</returns>        
        public override bool Insert(NameValueCollection PostData)
        {
            string sql = BuildInsertQueryString(PostData);

            this._LastInsert = Conn.ExecuteInsert(sql);
            return LastInsert>0;
        }
        
        private string BuildInsertQueryString(NameValueCollection post)
        {
            string values = "";
            string where = "";
            string fields = "";
            bool first = true;

            for (int i = 0; i < Fields.Count; i++)
            {
                AxfField field = Fields[i];
                if (!field.JumpField && field.ParseInSqlQueries)
                {
                    if (first) first = false;
                    else
                    {
                        values += ",";
                        fields += ",";
                        where += " AND ";
                    }
                    string value = "";
                    if (post.Get(field.FieldName) != null)
                    {
                        value = post.Get(field.FieldName).ToString();

                    }

                    value = field.filterValueForDB(value);

                    values += value;
                    fields += field.FieldName;
                    where += field.FieldName + " = " + value + "";
                }

            }

            for (int i = 0; i < ExternalFields.Count; i++)
            {
                if (first) first = false;
                else
                {
                    values += ",";
                    fields += ",";
                    where += " AND ";
                }
                values +=  ExternalFields[i].Value;
                fields +=  ExternalFields[i].FieldName;
            }

            string sql = "";

            sql += "INSERT INTO " + TableName;
            sql += " (" + fields + ")";
            sql += " VALUES(" + values + ")";

            return sql;
        }
        
        #endregion

        #region Update

        /// <summary>
        /// Implementa la logica di aggiornamento dei dati nel database. 
        /// </summary>
        /// <remarks>
        /// Il metodo costruisce una query sql di update dalle propriet� della classe quali
        /// TableName ed i campi defini nella collezione Fields.
        /// I valori d'aggiornamento vengono prelevati dall'oggetto HttpContext.Current.Request.Form
        /// </remarks>
        /// <returns>Indica se � stato effettuato l'aggiornamento dei dati nel database o meno.</returns>
        public override bool Update()
        {
            Update(HttpContext.Current.Request.Form);
            return true;
        }

        /// <summary>
        /// Implementa la logica di aggiornamento dei dati nel database indicando l'ID del record da aggiornare.  
        /// </summary>
        /// <param name="RecordID">ID del record da aggiornare</param>
        /// <returns>Indica se � stato effettuato l'aggiornamento dei dati nel database o meno.</returns>        
        public override bool Update(int RecordID)
        {
            this.RelatedRecordID = RecordID;
            Update(HttpContext.Current.Request.Form);
            return true;
        }

        /// <summary>
        /// Implementa la logica di aggiornamento dei dati nel database indicando l'ID del record da aggiornare.  
        /// ed un oggetto contenente i dati per l'aggiornamento.
        /// </summary>
        /// <param name="RecordID">ID del record da aggiornare</param>
        /// <param name="PostData">Oggetto contenente i dati da usare per l'aggiornamento</param>
        /// <returns>Indica se � stato effettuato l'aggiornamento dei dati nel database o meno.</returns>
        public override bool Update(int RecordID, NameValueCollection PostData)
        {
            this.RelatedRecordID = RecordID;
            Update(PostData);
            return true;
        }


        /// <summary>
        /// Implementa la logica di aggiornamento dei dati nel database prelevandoli da un oggetto collezione di tipo System.Collections.Specialized.NameValueCollection
        /// </summary>
        /// <param name="PostData">Oggetto contenente i dati da usare per l'aggiornamento</param>
        /// <returns>Indica se � stato effettuato l'aggiornamento dei dati nel database o meno.</returns>
        public override bool Update(NameValueCollection PostData)
        {
            if (RelatedRecordID == 0)   return false;
            if (this.Fields.Count == 0) return true;

            string updateSql = BuildUpdateQueryString(PostData);
            if (!updateSql.HasContent()) return true;
            
            bool success = Conn.Execute(updateSql);

            if (success)
            {
                DataRow row = Conn.SqlQuery("SELECT " + this.PrimaryKey + " FROM " + this.TableName + " WHERE " + this.PrimaryKey + " = " + this.RelatedRecordID).Rows[0];
                foreach(AxfField field in this.Fields)
                    field.Invalidate(row);
            }

            return success;

        }
        
        private string BuildUpdateQueryString(NameValueCollection post)
        {
            string values = "";

            foreach (AxfField field in this.Fields)
            {
                if (!field.JumpField && field.ParseInSqlQueries)
                {
                    string value = "";

                    if (post.Get(field.FieldName) != null)
                        value = post.Get(field.FieldName).ToString();

                    value = field.filterValueForDB(value);
                    if (value != null)
                    {
                        if (values.Length > 0) values += ",";
                        values += field.FieldName + " = " + value;
                    }
                    else
                    {
                        values += "," + field.FieldName + " = NULL";
                    }
                }
            }

            string sql = "";
            if (values.HasContent())
            {
                sql += "UPDATE " + TableName + " SET ";
                sql += " " + values + "";
                sql += " WHERE " + PrimaryKey + " = " + RelatedRecordID;
            }
            return sql;
        }

        #endregion
    }
}