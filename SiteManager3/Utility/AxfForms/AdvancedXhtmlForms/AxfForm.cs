using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Specialized;
using G2Core.AdvancedXhtmlForms;
using G2Core.AdvancedXhtmlForms.Fields;
using NetCms.Diagnostics;


namespace G2Core.AdvancedXhtmlForms
{
    /// <summary>
    /// Costruisce una web form per inserire o modificare dati specificati in uno o pi� oggetti di tipo
    /// <see cref="G2Core.AdvancedXhtmlForms.AxfTable"></see>
    /// </summary>
    /// <remarks>
    /// Usa questa classe per creare una forma con bottone di Submit della forma  e/o Indietro  per 
    /// inserire o modificare dati in un database.
    /// La collezione AxfTableCollection contiene la lista delle tabelle in cui sono specificati i campi 
    /// da visualizzare nella form. 
    /// Questa classe implementa una logica per creare relazioni tra gli oggetti della 
    /// collezione AxfTableCollection in modo da rispettare eventuali esistenze di chiavi 
    /// esterne nel database ed eseguire opportuni inserimenti e aggiornamenti dei dati.   
    /// </remarks>
    /// <example>Segue un esempio di creazione di una forma di inserimento dati
    /// <code>
    /// private NetCms.Connections.Connection _Conn;
    /// public NetCms.Connections.Connection Conn
    /// {
    /// get
    /// {
    ///     if (_Conn == null)
    ///     {
    ///         string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["yourconf"].ConnectionString;
    ///         _Conn = new G2Core.Connections.MySqlConnection(connstr);
    ///      }
    ///      return _Conn;
    ///    }
    /// }
    /// public HtmlGenericControl FormaDiInserimentoNuovoUtente()
    /// {
    ///     HtmlGenericControl div = new HtmlGenericControl("div");
    /// 
    ///     AxfForm form = new AxfForm();
    ///     form.SubmitLabel = "Inserisci Dati";
    ///     // Inizializza una tabella contenente i campi della form
    /// 
    ///     AxfGenericTable table = new AxfGenericTable("Offerte", "id_Offerta", Conn));
    /// 
    ///     AxfTextBox nome = new AxfTextBox("Nome", "Nome", AxfTextBox.ContentTypes.NormalText);
    ///     nome.Required = true;
    ///     nome.Size = 40;
    ///     table.addField(nome);
    /// 
    ///     AxfTextBox cognome = new AxfTextBox("Cognome", "cognome", AxfTextBox.ContentTypes.NormalText);
    ///     nome.Required = true;
    ///     nome.Size = 40;
    ///     table.addField(cognome);
    /// 
    ///     AxfDropDownList stato = new AxfDropDownList("stato", "stato");
    ///     tipo.addItem("Attivo","1");
    ///     tipo.addItem("Inattivo", "1");
    ///     table.addField(stato);
    /// 
    ///     //aggiungi la tabella alla forma. Si potrebbe aggiungere un altra tabella 
    ///     //logicamente si riferisce a dati in un altra tabella nel database
    ///
    ///     form.Tables.Add(table);
    ///     if (IsPostBack)
    ///     {
    ///         form.serializedInsert();
    ///         if (form.LastOperation == AxfForm.LastOperationValues.OK)
    ///         {
    ///             G2Core.XhtmlControls.Fieldset ok = new G2Core.XhtmlControls.Fieldset("Creazione Utente");
    ///             G2Core.XhtmlControls.Div content = new G2Core.XhtmlControls.Div();
    ///             content.InnerHtml = "<![CDATA[<p><strong>Inserimento effettuato con successo.</strong></p>]]>";
    ///             content.InnerHtml += "<![CDATA[<p><a href=\"default.aspx\">Torna alla gestione Offerte.</a>]]>";
    ///             content.InnerHtml += "<![CDATA[<br /><a href=\"Offerte_add.aspx\">Crea nuova Offerta.</a></p>]]>";
    ///             ok.Controls.Add(content);
    ///             return ok;
    ///          }
    ///     }         
    ///     form.SubmitLabel = "Crea Offerta";
    ///     div.Controls.Add(form.getFieldset("Creazione Offerta"));
    ///     return div;
    /// }
    /// </code>
    /// </example>
    public class AxfForm
    {

        private Hashtable _FormStringsForLog;
        /// <summary>
        /// Tabella hash usata per i log che memorizza, per ogni tabella associata alla form,
        /// i messaggi di log per gli eventuali campi che non hanno ottenuto la validazione.
        /// </summary>
        public Hashtable FormStringsForLog
        {
            get
            {
                if (_FormStringsForLog == null)
                    _FormStringsForLog = new Hashtable(); 
                return _FormStringsForLog;
            }
            set { _FormStringsForLog = value; }
        }
        
        private AxfTableCollection _Tables;
        /// <summary>
        /// Collezione di <see cref="G2Core.AdvancedXhtmlForms.AxfTable"></see> 
        /// </summary>
        public AxfTableCollection Tables
        {
            get
            {
                return _Tables;
            }
        }

        private AxfTableCollection _TablesRenderOrder;
        /// <summary>
        /// Aggiungi a questa collezione tabelle appartenti alla collezione  Tables, se si vuole indicare 
        /// uno specifico ordine di inserimento nel database dei dati di ogni tabella.
        /// Se questa collezione � vuota, si seguir� l'ordine della collezione Tables. 
        /// </summary>
        public AxfTableCollection TablesRenderOrder
        {
            get
            {
                return _TablesRenderOrder;
            }
        }

        protected AxfLabels Labels
        {
            get
            {
                return AxfLabels.CurrentLabels;
            }
        }
           
        /// <summary>
        /// lista di possibili opearazioni eseguite. 
        /// </summary>
        public enum LastOperationValues
        {
            NotYetExecute = 0,
            OK = 1,
            Error = 2
        }

        private LastOperationValues lastOperation;
        /// <summary>
        /// Ritorna l'ultima operazione eseguita
        /// </summary>
        public LastOperationValues LastOperation
        {
            get { return lastOperation; }
        }
        

        private string  _TestoCampiObbligatori;
        /// <summary>
        /// Ottiene o imposta il valore di testo da usare per i campi obbligatori.
        /// Se nessun valore � impostato sar� utilizzata una string di default.
        /// </summary>
        public string  TestoCampiObbligatori
        {
            get {
                if (_TestoCampiObbligatori == null)
                    _TestoCampiObbligatori = Labels[AxfLabels.LabelsList.CampiConAsteriscoObbligatori];
                return _TestoCampiObbligatori; 
            }
            set { _TestoCampiObbligatori = value; }
        }
        
        private bool _AddDisclaimer;
        /// <summary>
        /// Ottiene o imposta un valore per indicare se aggiungere un disclaimer alla form.
        /// </summary>
        public bool AddDisclaimer
        {
            get { return _AddDisclaimer; }
            set { _AddDisclaimer = value; }
        }

        private DisclaimerControl _DisclaimerControl;
        /// <summary>
        /// Rappresenta un oggetto DisclaimerControl  da aggiungere alla form.
        /// </summary>
        /// <remarks
        public DisclaimerControl DisclaimerControl
        {
            get { if (_DisclaimerControl == null)_DisclaimerControl = new DisclaimerControl(); return _DisclaimerControl; }
        }

        private string _InfoText;
        /// <summary>
        /// Inserisce un testo descrittivo alla forma. 
        /// </summary>
        public string InfoText
        {
            get
            {
                return _InfoText;
            }
            set { _InfoText = value; }
        }
        	
        #region Back Button

        private bool _AddBackButton;
        /// <summary>
        /// Permette di aggiungere un bottone alla forma per navigare indietro.
        /// </summary>
        public bool AddBackButton
        {
            get { return _AddBackButton; }
            set { _AddBackButton = value; }
        }

        private Button _BackButton;
        private Button BackButton
        {
            get
            {
                if (_BackButton == null)
                {
                    _BackButton = new Button();
                    _BackButton.Text = BackLabel;
                    _BackButton.ID = BackSourceID;
                    _BackButton.CssClass = "button";
                    if(BackHref!=null)
                        _BackButton.OnClientClick = "javascript:setSubmitSource('" + BackSourceID + "')";
                    else
                        _BackButton.OnClientClick = "javascript:history.go(-1);return(false)";
                }
                return _BackButton;
            }
        }

        private string _BackSourceID;
        /// <summary>
        /// Ottiene o imposta il valore di ID del bottone di navigazione indietro. Se nessun valore � indicato, viene utulizzato un 
        /// valore di default.
        /// </summary>
        public string BackSourceID
        {
            get { return _BackSourceID; }
            set
            {
                _BackSourceID = value;
                BackButton.ID = value;

                BackButton.OnClientClick = "javascript: setBackSource('" + value + "')";
            }
        }

        private string _BackHref;
        /// <summary>
        /// Ottiene o imposta l'url a cui navigare cliccando il bottone Indietro.
        /// </summary>
        public string BackHref
        {
            get { return _BackHref; }
            set
            {
                _BackHref = value;
            }
        }

        private string _BackLabel;
        /// <summary>
        /// Label per il bottone di navigazione indietro.
        /// </summary>
        public string BackLabel
        {
            get
            {
                if (_BackLabel == null)
                    _BackLabel = "Torna Indietro";
                return _BackLabel;
            }
            set
            {
                _BackLabel = value;
                BackButton.Text = value;
            }
        } 

        private void CheckBackButtonPress()
        {
            if (AddBackButton)
            {
                string request = HttpContext.Current.Request.Form[BackSourceID];
                G2Core.Common.RequestVariable backrq = new G2Core.Common.RequestVariable(BackSourceID, G2Core.Common.RequestVariable.RequestType.Form);
                if (backrq.IsValid() && backrq.StringValue == BackLabel)
                   NetCms.Diagnostics.Diagnostics.Redirect(BackHref);
            }
        }

        #endregion

        #region SubmitButton

        private bool _AddSubmitButton;
        /// <summary>
        /// Determina se aggiungere il bottone per il Submit della forma.
        /// </summary>
        public bool AddSubmitButton
        {
            get { return _AddSubmitButton; }
            set { _AddSubmitButton = value; }
        }

        private Button _SubmitButton;
        private Button SubmitButton
        {
            get
            {
                if (_SubmitButton == null)
                {
                    _SubmitButton = new Button();
                    _SubmitButton.Text = SubmitLabel;
                    _SubmitButton.ID = SubmitSourceID;
                    _SubmitButton.CssClass = "button";
                    _SubmitButton.OnClientClick = "javascript: setSubmitSource('" + SubmitSourceID + "')";
                }
                return _SubmitButton;
            }
        }

        private string _SubmitSourceID;
        /// <summary>
        /// Ottiene o imposta il valore di ID del bottone di Submit. Se nessun valore � indicato, viene utilizzato un 
        /// valore di default.
        /// </summary>
        public string SubmitSourceID
        {
            get { return _SubmitSourceID; }
            set
            {
                _SubmitSourceID = value;
                SubmitButton.ID = value;

                SubmitButton.OnClientClick = "javascript: setSubmitSource('" + value + "')";
            }
        }

        private string _SubmitLabel;
        /// <summary>
        /// Ottiene o imposta la Label per il bottone di Submit.
        /// </summary>
        public string SubmitLabel
        {
            get
            {
                if (_SubmitLabel == null)
                    _SubmitLabel = Labels[AxfLabels.LabelsList.SubmitLabel];
                return _SubmitLabel;
            }
            set
            {
                _SubmitLabel = value;
                SubmitButton.Text = value;
            }
        } 

        #endregion

        /// <summary>
        /// Ottiene un valore per indicare se il bottone di Submit � stato cliccato.  
        /// </summary>
        public bool CheckSubmitSource
        {
            get
            {
                G2Core.Common.RequestVariable sb = new G2Core.Common.RequestVariable(SubmitSourceID, G2Core.Common.RequestVariable.RequestType.Form);
                if (sb.IsValid() && sb.StringValue == SubmitLabel)
                    return true;
                return false;
            }
        }

        /// <summary>
        /// Inizializza una nuova istanza della classe AxfForm.
        /// </summary>
        public AxfForm()
        {
            _Tables = new AxfTableCollection();
            _TablesRenderOrder = new AxfTableCollection();
            _AddSubmitButton = true;
            _SubmitSourceID = "send";
            _BackSourceID = "back";

            lastOperation = LastOperationValues.NotYetExecute;
        }

        /// <summary>
        /// metodo, con opzione di overload, per renderizzare gli elementi dell'istanza,  
        /// da utilizzare in output, in un controllo HtmlGenericControl. 
        /// </summary>
        /// <returns>controllo contenente elementi della classe in versione XHTML.</returns>
        public virtual HtmlGenericControl getControl()
        {
            if (AddBackButton)
                CheckBackButtonPress();

            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "Axf";

            HtmlGenericControl campiobbligatori = new HtmlGenericControl("div");
            campiobbligatori.Attributes["class"] = "Axf_Control Axf_RequiredInfo";

            if(InfoText!=null && InfoText!= string.Empty)
                campiobbligatori.InnerHtml = "<p>" + InfoText + "</p>";
            
            campiobbligatori.InnerHtml += "<strong>" + this.TestoCampiObbligatori + "</strong>";

            div.Controls.Add(campiobbligatori);

            AxfTableCollection tables = this.TablesRenderOrder.Count >0?this.TablesRenderOrder:this.Tables;

            bool isPostBack=false;
            foreach(AxfTable table in tables)
            {
                div.Controls.Add(table.GetControl(this));
                isPostBack = isPostBack | table.IsPostBack;
            }

            if (AddSubmitButton || AddBackButton)
            {
                G2Core.XhtmlControls.Div buttons = new G2Core.XhtmlControls.Div();
                buttons.Class = "Axf_Control Axf_SubmitButtons";

                div.Controls.Add(buttons);

                if (AddSubmitButton)
                    buttons.Controls.Add(SubmitButton);

                if (AddBackButton)
                    buttons.Controls.Add(BackButton);
            }

            if (!isPostBack)
                Diagnostics.TraceMessage(TraceLevel.Verbose, "Accesso alla form","","","");
            
            return div;
        }
        /// <summary>
        /// Restituisce un fieldset contenente i vari componenti xhtml della forma.
        /// </summary>
        /// <param name="legendText">La legenda da utilizzare per il fieldset</param>
        /// <returns>un fieldset</returns>
        public HtmlGenericControl getFieldset(string legendText)
        {
            return getFieldset(legendText, "");
        }


        /// <summary>
        /// Restituisce un fieldset contenente i vari componenti xhtml della forma ed una stringa di 
        /// descrizione di errore. 
        /// </summary>
        /// <param name="legendText">La legenda da utilizzare per il fieldset</param>
        /// <param name="errorsHtml"></param>
        /// <returns></returns>
        public HtmlGenericControl getFieldset(string legendText, string errorsHtml)
        {
            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            HtmlGenericControl errors = new HtmlGenericControl("p");
            legend.InnerText = legendText;
            errors.InnerHtml = errorsHtml;
            fieldset.Controls.Add(legend);
            fieldset.Controls.Add(errors);
            fieldset.Controls.Add(getControl());
            return fieldset;
        }
        
        /// <summary>
        /// Restituisce un valore booleano per indicare se la forma ha valori validi. 
        /// </summary>
        /// <remarks>Tale metodo controller� la validit� dei dati inseriti in ogni tabella nella collezione Tables
        /// Tali valori saranno restituiti dall'oggetto
        /// HttpContext.Current.Request.
        /// </remarks>    
        /// <returns>Valore per indicare se la forma ha valori validi.</returns>
        public bool serializedValidation()
        {
            return serializedValidation(HttpContext.Current.Request.Form);
        }    
        /// <summary>
        /// Restituisce un valore booleano per indicare se l'oggetto post ha valori validi .
        /// In caso contrario scrive anche i log.
        /// </summary>
        /// <param name="post">Oggetto NameValueCollection da validare</param>
        /// <returns>Esito della validazione</returns>   
        public bool serializedValidation(NameValueCollection post)
        {
            bool valid = true;
            
            int i = 0;
            while (i < Tables.Count)
            {
                AxfTable table = Tables[i++];
                //valid = table.Validate(); katja non funziona in questo modo prende ultimo valore di validit� delle tabelle
                valid = valid & table.Validate(this);

                if (table.TableStringsForLog.Count!=0)
                    this.FormStringsForLog.Add(table.TableName, table.TableStringsForLog);
            }

            if (valid == false)
            {
                ICollection tabelle = FormStringsForLog.Values;
                foreach (ArrayList tabella in tabelle)
                {
                    string[] log_campi = (string[])tabella.ToArray(typeof(string));
                    foreach (string log_campo in log_campi)
                    {
                        Diagnostics.TraceMessage(TraceLevel.Warning, log_campo, "", "","");
                    }
                }
            }

            return valid;
        }

        /// <summary>
        /// Restituisce l'esito della operazione di validazione ed inserimento dei dati nel database 
        /// dei valori inseriti nella forma. Tali valori saranno restituiti dall'oggetto
        /// HttpContext.Current.Request.
        /// </summary>
        /// <returns></returns>
        public LastOperationValues serializedInsert()
        {
            return serializedInsert(HttpContext.Current.Request.Form);
        }
        /// <summary>
        /// Restituisce l'esito della operazione di validazione ed inserimento nel database dei dati contenuti nell'oggetto post
        /// specificato.
        /// </summary>
        /// <param name="post">oggetto contenente i valori inseriti nella forma</param>
        /// <returns>Esito della operazione di validazione ed inserimento</returns>   
        public LastOperationValues serializedInsert(NameValueCollection post)
        {
            if (!serializedValidation(post))
                lastOperation = LastOperationValues.Error;    
            else
            {
                bool esito = true;

                foreach (AxfTable table in this.Tables)
                {
                    if (esito)
                    {
                        for (int j = 0; j < table.ExternalFields.Count; j++)
                        {
                            G2Core.AdvancedXhtmlForms.Fields.Reference.AxfReferenceField field = table.ExternalFields[j];
                            if (Tables[field.TableName] != null)
                                if (Tables[field.TableName].LastInsert != 0)
                                    field.Value = Tables[field.TableName].LastInsert.ToString();
                        }
                        esito &= table.Insert();
                    }
                }

                lastOperation =  esito ? LastOperationValues.OK : LastOperationValues.Error;
            }

            if (lastOperation == LastOperationValues.OK)
                Diagnostics.TraceMessage(TraceLevel.Verbose, "Inserimento nella form avvenuto con successo", "", "","");
            /*else if (lastOperation == LastOperationValues.Error)
                Diagnostics.TraceMessage(TraceLevel.Error, "Errore durante l'inserimento nella form", "", "");
            */
            return LastOperation;
        }

        /// <summary>
        /// Restituisce l'esito della operazione di validazione ed aggiornamento dei dati nel database 
        /// dei valori inseriti nella forma. Tali valori saranno restituiti dall'oggetto
        /// HttpContext.Current.Request.
        /// </summary>
        /// <returns>Esito della operazione di validazione ed inserimento</returns> 
        public LastOperationValues serializedUpdate()
        {
            return serializedUpdate(HttpContext.Current.Request.Form);
        }



        /// <summary>
        /// Restituisce l'esito della operazione di validazione ed aggiornamento nel database dei dati contenuti nell'oggetto post
        /// specificato.
        /// </summary>
        /// <param name="post">oggetto contenente i valori inseriti nella forma</param>
        /// <returns>Esito della operazione di validazione ed inserimento</returns>   
        public LastOperationValues serializedUpdate(NameValueCollection post)
        {
            if (!serializedValidation(post))
                lastOperation = LastOperationValues.Error;
            else
            {
                bool esito = true;

                foreach (AxfTable table in this.Tables)
                    esito &= table.Update();

                lastOperation = esito ? LastOperationValues.OK : LastOperationValues.Error;
               
            }

            if (lastOperation == LastOperationValues.OK)
                Diagnostics.TraceMessage(TraceLevel.Verbose, "Aggiornamento della form avvenuto con successo", "", "","");
            /*else if (lastOperation == LastOperationValues.Error)
                Diagnostics.TraceMessage(TraceLevel.Error, "Errore durante l'aggiornamento della form", "", "");
            */
            return LastOperation;
        }

    }

    /// <summary>
    /// Rappresente un oggetto contenente un disclaimer 
    /// </summary>
    public class DisclaimerControl
    {
        private string _Title;
        /// <summary>
        /// Imposta il titolo del disclaimer
        /// </summary>
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _Subtitle;
        /// <summary>
        /// Imposta il sottotitolo del disclaimer
        /// </summary>
        public string Subtitle
        {
            get { return _Subtitle; }
            set { _Subtitle = value; }
        }

        private string _Infotext;
        /// <summary>
        /// Imposta il corpo del disclaimer
        /// </summary>
        public string Infotext
        {
            get { return _Infotext; }
            set { _Infotext = value; }
        }

        private string _ErrorText;
        /// <summary>
        /// Imposta il titolo del disclaimer
        /// </summary>
        public string ErrorText
        {
            get { return _ErrorText; }
            set { _ErrorText = value; }
        }

        private string _LabelAcceptControl;
        /// <summary>
        /// Imposta il testo di  richiesta consenso  delle condizioni.
        /// </summary>
        public string LabelAcceptControl
        {
            get { return _LabelAcceptControl; }
            set { _LabelAcceptControl = value; }
        }

        private string _LabelAccept;
        /// <summary>
        /// Imposta il testo per indicare  consenso alle condizioni.
        /// </summary>
        public string LabelAccept
        {
            get { return _LabelAccept; }
            set { _LabelAccept = value; }
        }

        private string _LabelDeny;
        /// <summary>
        /// Imposta il testo per indicare non consenso alle condizioni.
        /// </summary>
        public string LabelDeny
        {
            get { return _LabelDeny; }
            set { _LabelDeny = value; }
        }

        private string _AcceptControID;
        /// <summary>
        /// Imposta l'ID del controllo.
        /// </summary>
        public string AcceptControID
        {
            get { return _AcceptControID; }
        }

        /// <summary>
        /// Inizializza un'instanza della classe DisclaimerControl con valori di default.
        /// </summary>
        public DisclaimerControl()
        {
            _Infotext = "";
            _LabelAccept = "Si";
            _LabelDeny = "No";
            _Subtitle = "Informativa e consenso - DLG 196/03";
            _LabelAcceptControl = "Accetti le condizioni di utilizzo?";
            _Title = "Condizioni d'uso e privacy";
            _AcceptControID = "accettazione";
            _ErrorText = "E' necessario accettare le condizione d'uso.";
        }

        /// <summary>
        /// Restituisce l'adesione o meno alle condizioni del disclaimer.
        /// </summary>
        /// <returns>valore di adesione</returns>
        public bool CheckAcceptance()
        {
            Common.RequestVariable request = new Common.RequestVariable(this.AcceptControID);
            return request.IsValidInteger && request.IntValue == 1;
        }


        /// <summary>
        /// Restituisce un HtmlGenericControl contenente i controlli Xhtml del disclaimer dentro un fielset.
        /// </summary>
        ///<returns>Fieldset contenente i controlli Xhtml del disclaimer </returns>
        public HtmlGenericControl GetControl
        {
            get
            {
                G2Core.XhtmlControls.Div content = new G2Core.XhtmlControls.Div();
                G2Core.XhtmlControls.Fieldset set = new G2Core.XhtmlControls.Fieldset(this.Title, content);
                set.Class = "DisclaimerFieldset";

                TextBox infotextbox = new TextBox();
                infotextbox.Text = this.Infotext;
                infotextbox.Rows = 10;
                infotextbox.Columns = 70;
                infotextbox.TextMode = TextBoxMode.MultiLine;

                Label label = new Label();
                label.Text = this.Subtitle;
                label.AssociatedControlID = infotextbox.ID;

                content.Controls.Add(label);
                content.Controls.Add(infotextbox);

                HtmlGenericControl accettazione = new HtmlGenericControl("p");

                accettazione.InnerHtml = "<label>" + this.LabelAcceptControl + "</label>"
                + " <label for=\"" + this.AcceptControID + "1\">" + this.LabelAccept + "</label> <input id=\"" + this.AcceptControID + "1\" name=\"" + this.AcceptControID + "\" type=\"radio\" value=\"1\" />"
                + " <label for=\"" + this.AcceptControID + "2\">" + this.LabelDeny + "</label> <input id=\"" + this.AcceptControID + "2\" name=\"" + this.AcceptControID + "\" type=\"radio\" value=\"0\" checked=\"checked\" />";

                content.Controls.Add(accettazione);

                return set;
            }
        }

        /// <summary>
        /// Restituisce un HtmlGenericControl div contenente i controlli Xhtml del disclaimer dentro un fielset.
        /// </summary>
        ///<returns>Div contenente i controlli Xhtml del disclaimer </returns>
        public HtmlGenericControl GetControlNoFieldset
        {
            get
            {
                G2Core.XhtmlControls.Div content = new G2Core.XhtmlControls.Div();
                content.Class = "DisclaimerBlock";

                TextBox infotextbox = new TextBox();
                infotextbox.Text = this.Infotext;
                infotextbox.Rows = 10;
                infotextbox.Columns = 70;
                infotextbox.TextMode = TextBoxMode.MultiLine;

                Label label = new Label();
                label.Text = this.Subtitle;
                label.AssociatedControlID = infotextbox.ID;

                content.Controls.Add(label);
                content.Controls.Add(infotextbox);

                HtmlGenericControl accettazione = new HtmlGenericControl("p");

                accettazione.InnerHtml = "<label>" + this.LabelAcceptControl + "</label>"
                + " <label for=\"" + this.AcceptControID + "1\">" + this.LabelAccept + "</label> <input id=\"" + this.AcceptControID + "1\" name=\"" + this.AcceptControID + "\" type=\"radio\" value=\"1\" />"
                + " <label for=\"" + this.AcceptControID + "2\">" + this.LabelDeny + "</label> <input id=\"" + this.AcceptControID + "2\" name=\"" + this.AcceptControID + "\" type=\"radio\" value=\"0\" checked=\"checked\" />";

                content.Controls.Add(accettazione);

                return content;
            }
        }
    }
}