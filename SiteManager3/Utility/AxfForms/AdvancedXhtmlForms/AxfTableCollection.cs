using System.Collections;
using System;



namespace G2Core.AdvancedXhtmlForms
{
    /// <summary>
    /// Fornisce funzionalit� standard per la creazione e la gestione di un insieme di oggetti <see cref="G2Core.AdvancedXhtmlForms.AxfTable"></see> 
    /// ordinato.    
    /// </summary>
    /// <remarks>
    /// Questo insieme supporta l'indicizzazione in base zero standard.
    /// </remarks>
    public class AxfTableCollection : IEnumerable
    {
        private G2Collection coll;

        /// <summary>
        /// Ritorna il numero di oggetti nella collezione 
        /// </summary>
        public int Count { get { return coll.Count; } }

        /// <summary>
        /// Inizializza una instanza della classe.
        /// </summary>
        public AxfTableCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new G2Collection();
        }

        /// <summary>
        /// Aggiunge alla lista un nuovo oggetto.
        /// </summary>
        /// <param name="table">Oggetto da aggiungere alla lista</param>
        public void Add(AxfTable table)
        {
            coll.Add(table.TableName, table);
        }

        /// <summary>
        /// Permette di accedere ad un oggetto della collezione tramite un indice.
        /// </summary>
        /// <param name="i">indice della lista in base zero dell'oggetto da restituire.</param>
        /// <returns>Oggetto contenuto alla posizione specificata della lista.</returns>
        public AxfTable this[int i]
        {
            get
            {
                AxfTable table = (AxfTable)coll[coll.Keys[i]];
                return table;
            }
        }

        /// <summary>
        /// Permette di accedere ad un oggetto della collezione tramite la chiave associata 
        /// all'oggetto.
        /// </summary>
        /// <param name="str">Chiave dell'oggetto da restiture.</param>
        /// <returns>Oggetto associato alla chiave specificata.</returns>
        public AxfTable this[string str]
        {
            get
            {
                AxfTable table = (AxfTable)coll[str];
                return table;
            }
        }

        #region Enumerator

        /// <summary>
        /// Restituisce un enumeratore che scorre un insieme.
        /// </summary>
        /// <returns>Oggetto IEnumerator che pu� essere utilizzato per scorrere l'insieme.</returns>
        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        /// <summary>
        /// Implementa una enumeratore per scorrere uno specifico insieme. 
        /// </summary>
        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private AxfTableCollection Collection;

            /// <summary>
            /// Instanzia un oggetto di tipo CollectionEnumerator.
            /// </summary>
            /// <param name="coll">Collezione a cui aggiungere le funzionalit� di enumerazione</param>
            public CollectionEnumerator(AxfTableCollection coll)
            {
                Collection = coll;
            }


            /// <summary>
            /// Restituisce l'oggetto memorizzato nell'insieme al corrente indice di 
            /// scorrimento.
            /// </summary>
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }


            /// <summary>
            /// Avanza l'indice di scorrimento degli oggetti nell'insieme.
            /// </summary>
            /// <returns>vero se si � avanzati nello scorrimento dell'indice, falso se si � 
            /// giunti alla fine dello scorrimento</returns>
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }


            /// <summary>
            /// Resetta l'indice di scorrimento all'inizio.
            /// </summary>
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}