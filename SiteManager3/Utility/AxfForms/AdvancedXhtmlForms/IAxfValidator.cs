﻿
namespace G2Core.AdvancedXhtmlForms
{
    public interface IAxfValidator
    {
        string ResultLog { get; }
        bool Validate(object value);
    }
}
