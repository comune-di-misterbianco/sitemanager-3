﻿using System;
using System.Web;
using System.Data;

namespace G2Core.DatabaseFields
{
    public class LanguageDatabaseFieldData : DatabaseFieldData
    {
        private string _ValueByDefaultLang;
        public string ValueByDefaultLang
        {
            get
            {
                if (_ValueByDefaultLang == null && Values.Contains(DefaultLangID))
                {
                    _ValueByDefaultLang = Values[DefaultLangID].ToString();
                }
                return _ValueByDefaultLang;
            }

        }

        private string _IDByDefaultLang;
        public string IDByDefaultLang
        {
            get
            {
                if (_IDByDefaultLang == null && LabelsIDs.Contains(DefaultLangID))
                    _IDByDefaultLang = LabelsIDs[DefaultLangID].ToString();

                return _IDByDefaultLang;
            }

        }

        private G2Core.Common.StringCollection _Values;
        public G2Core.Common.StringCollection Values
        {
            get
            {
                if (_Values == null)
                {
                    _Values = new G2Core.Common.StringCollection();
                    foreach (DataRow label in DataSource.Labels.Select("Record_Label = " + this.RecordData[this.RelativeField.FieldName]))
                    {
                        _Values.Add(label["Text_Label"].ToString(), label["Lang_Label"].ToString());
                    }
                }

                return _Values;
            }
        }

        private string _DefaultLangID;
        public string DefaultLangID
        {
            get
            {
                if (_DefaultLangID == null)
                    this._DefaultLangID = DataSource.LabelsRecords.Select("id_LabelRecord = " + this.RecordData[this.RelativeField.FieldName])[0]["DefaultLang_LabelRecord"].ToString();

                return _DefaultLangID;
            }
        }

        private string _RecordID;
        public string RecordID
        {
            get
            {
                if (_RecordID == null) {
                    //this._RecordID = this.RecordData["Record_Label"].ToString();
                    DataRow RowLab = DataSource.Labels.Select("Record_Label = " + this.RecordData[this.RelativeField.FieldName])[0];
                    this._RecordID = RowLab["Record_Label"].ToString();
                }
                return _RecordID;
            }
        }
        private DataRow _LangRecordData; /*
        public DataRow LangRecordData
        {
            get
            {
                if (_RecordID == null)
                    this._RecordID = this.RecordData["Record_Label"].ToString();

                return _RecordID;
            }
        }*/
        private G2Core.Common.StringCollection _LabelsIDs;
        public G2Core.Common.StringCollection LabelsIDs
        {
            get
            {
                if (_LabelsIDs == null)
                {
                    _LabelsIDs = new G2Core.Common.StringCollection();
                    foreach (DataRow label in DataSource.Labels.Select("Record_Label = " + this.RecordData[this.RelativeField.FieldName]))
                    {
                        _LabelsIDs.Add(label["id_Label"].ToString(), label["Lang_Label"].ToString());
                    }
                }

                return _LabelsIDs;
            }
        }

        private bool ValueByCurrentLangLoaded;
        private string _ValueByCurrentLang;
        public string ValueByCurrentLang
        {
            get
            {
                if (!ValueByCurrentLangLoaded)
                {
                    ValueByCurrentLangLoaded = true;
                    if (Values.Contains(DataSource.CurrentLang.ToString()))
                        _ValueByCurrentLang = Values[DataSource.CurrentLang.ToString()].ToString();
                    else
                        _ValueByCurrentLang = this.ValueByDefaultLang;
                }
                return _ValueByCurrentLang;
            }
        }
              

        private bool IDByCurrentLangLoaded;
        private string _IDByCurrentLang;
        public string IDByCurrentLang
        {
            get
            {
                if (!IDByCurrentLangLoaded)
                {
                    IDByCurrentLangLoaded = true;
                    if (Values.Contains(DataSource.CurrentLang.ToString()))
                        _IDByCurrentLang = LabelsIDs[DataSource.CurrentLang.ToString()].ToString();
                    else
                            _IDByCurrentLang = this.IDByDefaultLang;
                }
                return _ValueByCurrentLang;
            }
        }


        private G2Core.Languages.LangDataSource _DataSource;
        protected G2Core.Languages.LangDataSource DataSource
        {
            get {return _DataSource;}
            private set {_DataSource = value;}
        }
       			
        /// <summary>
        /// Ritorna il valore rispetto alla lingua corrente, se questo non esistesse ritorna il valore rispetto alla lingua di default
        /// </summary>
        public override string Value
        {
            get
            {
                return (ValueByCurrentLang != null) ? ValueByCurrentLang : ValueByDefaultLang;
            }
        }
        public override string StringValue
        {
            get
            {
                return ValueByDefaultLang;
            }
        }

        public LanguageDatabaseFieldData(DatabaseField field, DataRow row, G2Core.Languages.LangDataSource languageData)
            : base(field, row)
        {
            this.DataSource = languageData;
        }
    }

}
