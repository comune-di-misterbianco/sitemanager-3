﻿using System;
using System.Web;
using System.Data;

namespace G2Core.DatabaseFields
{    
    public class DatabaseFieldData
    {
        private DataRow _RecordData;
        public DataRow RecordData
        {
            get { return _RecordData; }
            private set { _RecordData = value; }
        }

        public virtual string Value
        {
            get
            {
                return StringValue;
            }
        }

        private bool intValueLoaded;
        private int _IntValue;
        public virtual int IntValue
        {
            get
            {
                if (!intValueLoaded)
                {
                    intValueLoaded = true;
                    if (!int.TryParse(this.Value, out _IntValue)) _IntValue = 0;
                }
                return _IntValue;
            }
        }
        public virtual bool BoolValue
        {
            get
            {
                return IntValue == 1;
            }
        }

        private bool doubleValueLoaded;
        private double _DoubleValue;
        public virtual double DoubleValue
        {
            get
            {
                if (!doubleValueLoaded)
                {
                    doubleValueLoaded = true;
                    if (!double.TryParse(this.Value, out _DoubleValue)) _DoubleValue = 0;
                }
                return _DoubleValue;
            }
        }

        private bool floatValueLoaded;
        private float _FloatValue;
        public virtual float FloatValue
        {
            get
            {
                if (!floatValueLoaded)
                {
                    floatValueLoaded = true;
                    if (!float.TryParse(this.Value, out _FloatValue)) _FloatValue = 0;
                }
                return _FloatValue;
            }
        }

        private string _StringValue;
        public virtual string StringValue
        {
            get
            {
                if (_StringValue == null)
                    _StringValue = RecordData[this.RelativeField.FieldName].ToString();
                return _StringValue;
            }
        }
        private DatabaseField _RelativeField;
        public DatabaseField RelativeField
        {
            get
            {
                return _RelativeField;
            }
        }

        public DatabaseFieldData(DatabaseField field, DataRow row)
        {
            _RelativeField = field;
            RecordData = row;
        }

        public override string ToString()
        {
            return this.Value;
        }
    }

}
