﻿using System;
using System.Web;
using System.Data;

namespace G2Core.DatabaseFields
{
    public class LanguageDatabaseField: DatabaseField
    {
        private G2Core.Languages.LangDataSource _LanguageData;
        public G2Core.Languages.LangDataSource LanguageData
        {
            get { return _LanguageData; }
            private set { _LanguageData = value; }
        }

        public override DatabaseFieldData InitDatabaseData()
        {
            return LanguageDatabaseData;
        }

        public LanguageDatabaseFieldData LanguageDatabaseData
        {
            get 
            {
                if(_LanguageDatabaseData == null)
                    _LanguageDatabaseData = new LanguageDatabaseFieldData(this, Record, this.LanguageData);
                return _LanguageDatabaseData; 
            }
            private set { _LanguageDatabaseData = value; }
        }
        private LanguageDatabaseFieldData _LanguageDatabaseData;
        
        public LanguageDatabaseField(string label, string fieldName, G2Core.Languages.LangDataSource languageData, DatabaseFieldTypeData.DatabaseFieldTypes type)
            : base(label, fieldName, type)
        {
            this.LanguageData = languageData;
        }
        public LanguageDatabaseField(string label, string fieldName, G2Core.Languages.LangDataSource languageData, DataRow row)
            : base(label, fieldName, row)
        {
            this.LanguageData = languageData;
        }
        public LanguageDatabaseField(string label, string fieldName, G2Core.Languages.LangDataSource languageData, string idFieldName, string recordID, DataTable table)
            : this(label, fieldName, languageData, table.Select(idFieldName + " = " + recordID)[0])
        {
        }
        public LanguageDatabaseField(string label, string fieldName, G2Core.Languages.LangDataSource languageData, string idFieldName, string recordID, string sql, NetCms.Connections.Connection conn)
            : this(label, fieldName,languageData, idFieldName, recordID, conn.SqlQuery(sql))
        {
        }

    }
}
