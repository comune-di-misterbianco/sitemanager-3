﻿using System;
using System.Web;
using System.Data;

namespace G2Core.DatabaseFields
{
    public class DatabaseFieldTypeData
    {
        public enum DatabaseFieldTypes
        {
            Integer, String, Double
        }
        private int _MaxLength = 255;
        public int MaxLength
        {
            get { return _MaxLength; }
            set { _MaxLength = value; }
        }

        private void InitType(DatabaseField RelativeField)
        {
            DataColumn RelativeColumn = RelativeField.DatabaseData.RecordData.Table.Columns[RelativeField.FieldName];
            MaxLength = RelativeColumn.MaxLength;
            switch (RelativeColumn.DataType.ToString())
            {
                case "String": _Value = DatabaseFieldTypes.String; break;
                case "Double": _Value = DatabaseFieldTypes.Double; break;
                case "Int": _Value = DatabaseFieldTypes.Integer; break;
                default: _Value = DatabaseFieldTypes.String; break;
            }
        }

        private DatabaseFieldTypes _Value;
        public DatabaseFieldTypes Value
        {
            get
            {
                return _Value;
            }
        }

        public DatabaseFieldTypeData(DatabaseField field)
        {
            InitType(field);
        }
        public DatabaseFieldTypeData(DatabaseFieldTypes type)
        {
            _Value = type;
        }
    }

   
}
