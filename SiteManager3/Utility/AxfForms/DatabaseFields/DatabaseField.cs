﻿using System;
using System.Web;
using System.Data;

namespace G2Core.DatabaseFields
{ 
    public class DatabaseField: Interfaces.IBootable,Interfaces.IInvalidable
    {
        private string _FieldName;
        public string FieldName
        {
            get { return _FieldName; }
            private set { _FieldName = value; }
        }
        
        private string _Label;
        public string Label
        {
            get { return _Label; }
        }

        private DatabaseFieldTypeData _Type;
        public DatabaseFieldTypeData Type
        {
            get 
            {
                if (_Type == null) _Type = new DatabaseFieldTypeData(this);
                return _Type;
            }
            private set
            {
                _Type = value;
            }
        }

        private DatabaseFieldData _DatabaseData;
        public DatabaseFieldData DatabaseData
        {
            get 
            {
                if (_DatabaseData == null) _DatabaseData = this.InitDatabaseData();
                return _DatabaseData; 
            }
            private set
            {
                _DatabaseData = value;
            }
        }

        public virtual DatabaseFieldData  InitDatabaseData()
        {
            return new DatabaseFieldData(this, Record);
        }

        private DatabaseFieldAxfControls _AxfControls;
        public DatabaseFieldAxfControls AxfControls
        {
            get
            {
                if (_AxfControls == null) _AxfControls = new DatabaseFieldAxfControls(this);
                return _AxfControls;
            }
            private set
            {
                _AxfControls = value;
            }
        }

        private DataRow record;
        protected DataRow Record
        {
            get { return record; }
            private set { record = value; }
        }

        public bool ContainsDatabaseData
        {
            get
            {
                return Record != null;
            }
        }

        public DatabaseField(string label, string fieldName, DatabaseFieldTypeData.DatabaseFieldTypes type)
        {
            FieldName = fieldName;
            _Label = label;
            _Type = new DatabaseFieldTypeData(type);
        }
        public DatabaseField(string label, string fieldName, DataRow row)
        {
            Record = row;
            FieldName = fieldName;
            _Label = label;
        }
        public DatabaseField(string label, string fieldName, string idFieldName, string recordID, DataTable table)
            : this(label, fieldName, table.Select(idFieldName + " = " + recordID)[0])
        {
        }
        public DatabaseField(string label, string fieldName, string idFieldName, string recordID, string sql, NetCms.Connections.Connection conn)
            : this(label, fieldName, idFieldName, recordID, conn.SqlQuery(sql))
        {
        }

        public override string ToString()
        {
            if (this.DatabaseData != null) return this.DatabaseData.Value;
            else return string.Empty;
        }

        public void Invalidate(DataRow row)
        {
            Record = row;
            this.Boot();
        }

        public void Boot()
        {
            if (Record != null) DatabaseData = new DatabaseFieldData(this, Record);
            Type = new DatabaseFieldTypeData(this);
        }

        private Interfaces.IBootable[] _ChildBootableObjects = new Interfaces.IBootable[0];
        public Interfaces.IBootable[] ChildBootableObjects
        {
            get
            {
                return _ChildBootableObjects;
            }
        }
    }
}
