﻿namespace G2Core.DatabaseFields
{
    public class DatabaseFieldAxfControls
    {        
        private G2Core.AdvancedXhtmlForms.Fields.AxfTextBox _AxfTextBox;
        public G2Core.AdvancedXhtmlForms.Fields.AxfTextBox AxfTextBox
        {
            get
            {
                if (_AxfTextBox == null)
                {
                    _AxfTextBox = new G2Core.AdvancedXhtmlForms.Fields.AxfTextBox(this.RelativeField.Label, this.RelativeField.FieldName);
                    switch (this.RelativeField.Type.Value)
                    {
                        case DatabaseFieldTypeData.DatabaseFieldTypes.Integer: _AxfTextBox.ContentType = G2Core.AdvancedXhtmlForms.Fields.AxfTextBox.ContentTypes.Numeric; break;
                        case DatabaseFieldTypeData.DatabaseFieldTypes.String: _AxfTextBox.ContentType = G2Core.AdvancedXhtmlForms.Fields.AxfTextBox.ContentTypes.NormalText; break;
                        case DatabaseFieldTypeData.DatabaseFieldTypes.Double: _AxfTextBox.ContentType = G2Core.AdvancedXhtmlForms.Fields.AxfTextBox.ContentTypes.Floating; break;
                    }
                    _AxfTextBox.MaxLenght = this.RelativeField.Type.MaxLength>0?this.RelativeField.Type.MaxLength:0;
                }
                return _AxfTextBox;
            }
        }
        public G2Core.AdvancedXhtmlForms.Fields.AxfImageUpload AxfImageUpload(string folderPhysicalPath, NetCms.Connections.Connection conn)
        {
            return new G2Core.AdvancedXhtmlForms.Fields.AxfImageUpload(this.RelativeField.Label, this.RelativeField.FieldName,folderPhysicalPath,conn);
        }
        private DatabaseField _RelativeField;
        public DatabaseField RelativeField
        {
            get
            {
                return _RelativeField;
            }
        }

        public DatabaseFieldAxfControls(DatabaseField field)
        {
            _RelativeField = field;
        }
    }
}
