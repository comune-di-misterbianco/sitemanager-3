﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Web;
using System.Configuration;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Security.Cryptography;


namespace NetCms.Diagnostics
{
    public class DiagnosticUserData
    {
        public const string DiagnosticCurrentUserName = "DiagnosticCurrentUserNameSessionKey";
        public const string DiagnosticCurrentUserID = "DiagnosticCurrentUserIDSessionKey";

        public bool DataExists { get; private set; }
        public string UserName { get; private set; }
        public string UserID { get; private set; }

        public DiagnosticUserData(string userID, string username)
        {
            if (!string.IsNullOrEmpty(username))
                UserName = username;

            if (!string.IsNullOrEmpty(userID))
                UserID = userID;

            if (string.IsNullOrEmpty(UserName) && HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[DiagnosticCurrentUserName] != null)
                UserName = HttpContext.Current.Session[DiagnosticCurrentUserName].ToString();

            if (string.IsNullOrEmpty(UserID) && HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[DiagnosticCurrentUserID] != null)
                UserID = HttpContext.Current.Session[DiagnosticCurrentUserID].ToString();

            DataExists = !string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(UserID);
        }
    }
}
