﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Cryptography;

namespace NetCms.Diagnostics
{
    public class DecodePass
    {
        // costanti decript
        const string passPhrase = "Pas5pr@se";           // can be any string
        const string saltValue = "s@1tValue";            // can be any string
        const string hashAlgorithm = "SHA1";             // can be "MD5"
        const int passwordIterations = 2;                // can be any number
        const string initVector = "@1B2c3D4e5F6g7H8";    // must be 16 bytes
        const int keySize = 256;                         // can be 192 or 128

        public string DecodeStringConn(string strConn)
        {
            bool decode = false;
            string pw_enc = "";
            string pw_plain = "";
            string strConnOut = "";

            Regex regex = new Regex("pwd=.*;");
            Match test_match = regex.Match(strConn);
            if (test_match.Success)
            {
                pw_enc = test_match.Value;
                // rimuovo i caratteri pwd= e ;
                string tmp_pw_enc = pw_enc.Replace("pwd=", "").Replace(";", "");

                // descripto la password
                pw_plain = Rijndael.Decrypt(tmp_pw_enc, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
                // applico la password alla conn
                pw_plain = "pwd=" + pw_plain + ";";

                strConnOut = strConn.Replace(pw_enc, pw_plain);
                decode = true;
            }
            else
            {
                regex = new Regex("password=.*;");
                test_match = regex.Match(strConn);
                if (test_match.Success)
                {
                    pw_enc = test_match.Value;
                    // rimuovo i caratteri pwd= e ;
                    string tmp_pw_enc = pw_enc.Replace("password=", "").Replace(";", "");

                    // descripto la password
                    pw_plain = Rijndael.Decrypt(tmp_pw_enc, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
                    // applico la password alla conn
                    pw_plain = "password=" + pw_plain + ";";

                    strConnOut = strConn.Replace(pw_enc, pw_plain);
                    decode = true;
                }
            }

            if (!decode)
            {
                regex = new Regex("pwd=.*");
                test_match = regex.Match(strConn);
                if (test_match.Success)
                {
                    pw_enc = test_match.Value;
                    // rimuovo i caratteri pwd= e ;
                    string tmp_pw_enc = pw_enc.Replace("pwd=", "");

                    // descripto la password
                    pw_plain = Rijndael.Decrypt(tmp_pw_enc, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
                    // applico la password alla conn
                    pw_plain = "pwd=" + pw_plain;

                    strConnOut = strConn.Replace(pw_enc, pw_plain);
                    decode = true;
                }
            }
            return strConnOut;
        }

        public string DecodeString(string strCript)
        {
            return Rijndael.Decrypt(strCript, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
        }
    }
}
