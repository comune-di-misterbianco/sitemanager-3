﻿using System;
using System.Web;
using log4net;
using log4net.Core;
using System.Security.Cryptography;


namespace NetCms.Diagnostics
{
    public interface IPageLogEntry
    {
        DateTime LogDateTime { get; }
        string LogMessage {get;}
        string LogErrorCode { get; }
        string LogID { get; }
    }
}
