﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Web;
using System.Configuration;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Threading;


namespace NetCms.Diagnostics
{
    public enum TraceLevel
    {
        Off = 0,
        Fatal = 1,
        Error = 2,
        Warning = 3,
        Info = 4,
        Verbose = 5,
        Request = 6
    }

    public static class Diagnostics
    {
        private enum Writers { DotNet, Log4Net }
        public const string PageGUIDEntryKey = "PageGUIDEntryKey";
        public const string ConfigWriterEntry = "logWriter";
        public const string ExceptionsForThisPageEntryKey = "NetCms.Diagnostics.ExceptionsForThisPage";
        public const string ApplicationContextCurrentComponentItemsKey = "NetCms.ApplicationContext.CurrentComponent";

        public static List<IPageLogEntry> ExceptionsForThisPage
        {
            get
            {

                if (HttpContext.Current != null && !HttpContext.Current.Items.Contains(ExceptionsForThisPageEntryKey))
                {
                    HttpContext.Current.Items[ExceptionsForThisPageEntryKey] = new List<IPageLogEntry>();
                    return (List<IPageLogEntry>)HttpContext.Current.Items[ExceptionsForThisPageEntryKey];
                }
                else
                    return new List<IPageLogEntry>();
            }
        }

        private static Writers CurrentWriter
        {
            get
            {
                if(_CurrentWriter == null)
                {
                    var value = System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains(ConfigWriterEntry) ? System.Configuration.ConfigurationManager.AppSettings[ConfigWriterEntry] : "";
                    if (!string.IsNullOrEmpty(value))
                    {
                        switch (value.ToLower())
                        {
                            case "dotnet": _CurrentWriter = new Writers?(Writers.DotNet); break;
                            case "log4net": _CurrentWriter = new Writers?(Writers.Log4Net); break;
                        }
                    }
                    else _CurrentWriter = new Writers?(Writers.DotNet);
                }
                return _CurrentWriter.Value;
            }
        }
        private static Writers? _CurrentWriter;

        public static void Redirect(string url)
        {
            if (HttpContext.Current != null)
            {
                try
                {
                    //Aggiungere se necessario il booleano come work around all'eccezione di tipo Thread Aborted in seguito a redirect,
                    //passando false il contesto della pagina non viene perso ed il codice di diagnostica viene eseguito, così che il thread non dia eccezione, come indicato 
                    //su questo link: http://support.microsoft.com/kb/312629/en-us
                    //HttpContext.Current.Response.Redirect(url, false);
                    HttpContext.Current.Response.Redirect(url);
                }
                catch (ThreadAbortException tex)
                {
                }
                catch (Exception ex)
                {
                    Diagnostics.TraceException(ex, "", "");
                }

                NetCms.Diagnostics.Diagnostics.TraceMessage(TraceLevel.Info, "Redirect a " + url, "", "", "");
            }
            else
                NetCms.Diagnostics.Diagnostics.TraceMessage(TraceLevel.Error, "Redirect Impossibile a Causa di Mancanza del Context verso " + url, "", "", "");
        }

        //public static string FilterForm(NameValueCollection myForm)
        //{
        //    string myViewstate = "";
        //    string myEventValidation = "";
        //    string allValue = HttpContext.Current.Server.UrlDecode(myForm.ToString());

        //    if (myForm["__VIEWSTATE"] != null)
        //    {
        //        myViewstate = "__VIEWSTATE=" + myForm["__VIEWSTATE"].ToString() + "&";
        //        allValue = allValue.Replace(myViewstate, "");
        //    }
        //    if (myForm["__EVENTVALIDATION"] != null)
        //    {
        //        myEventValidation = "__EVENTVALIDATION=" + myForm["__EVENTVALIDATION"].ToString() + "&";
        //        allValue = allValue.Replace(myEventValidation, "");
        //    }
        //    if (allValue.Contains("password_LoginControl1="))
        //    {
        //        string pattern = @"password_LoginControl1=*.*&";
        //        allValue = System.Text.RegularExpressions.Regex.Replace(allValue, pattern, "password_LoginControl1=************;");
        //    }          
        //    if (allValue.Contains("username_LoginControl2="))
        //    {
        //        string pattern = @"password_LoginControl2=*.*&";
        //        allValue = System.Text.RegularExpressions.Regex.Replace(allValue, pattern, "password_LoginControl2=************;");
        //    }
        //    if (allValue.Contains("Password_User="))
        //    {
        //        string pattern = @"Password_User=*.*&";
        //        allValue = System.Text.RegularExpressions.Regex.Replace(allValue, pattern, "Password_User=************;");
        //    }
        //    if (allValue.Contains("Password_User_Confirm="))
        //    {
        //        string pattern = @"Password_User_Confirm=*.*&";
        //        allValue = System.Text.RegularExpressions.Regex.Replace(allValue, pattern, "Password_User_Confirm=************;");
        //    }
        //    if (allValue.Contains("Password_User_Old="))
        //    {
        //        string pattern = @"Password_User_Old=*.*&";
        //        allValue = System.Text.RegularExpressions.Regex.Replace(allValue, pattern, "Password_User_Old=************;");
        //    }
        //    return allValue;
        //}
        //public static string FilterMessage(string input)
        //{
        //    string output = input;

        //    if (input.Contains("Pwd="))
        //    {
        //        string pattern = @"Pwd=*.*;";
        //        output = System.Text.RegularExpressions.Regex.Replace(input, pattern, "Pwd=**********");
        //    }

        //    return output;
        //}

        public static void TraceMessage(TraceLevel messageLevel, string message)
        {
            TraceLog(messageLevel, 2, message, "", "", "", "");
        }
        public static void TraceMessage(TraceLevel messageLevel, string message, string userID, string username, string errorcode)
        {
            TraceLog(messageLevel, 2, message, "", userID, username, errorcode);
        }
        public static void TraceMessage(TraceLevel messageLevel, string message, string userID, string username, Exception ex = null, object callerObject = null)
        {
            TraceLog(messageLevel, 2, message, "", userID, username, "", ex, callerObject);
        }
        public static void TraceMessage(TraceLevel messageLevel, string message, string userID, string username, string errorcode, Exception ex = null, object callerObject = null)
        {
            TraceLog(messageLevel, 2, message, "", userID, username, errorcode, ex, callerObject);
        }
        public static void TraceMessage(TraceLevel messageLevel, string message, string userID, string username, string errorcode, int jumpFrameN, Exception ex = null, object callerObject = null)
        {
            TraceLog(messageLevel, jumpFrameN, message, "", userID, username, errorcode, ex, callerObject);
        }
        public static void TraceMessage(TraceLevel messageLevel, string message, string component, string userID, string username, string errorcode, Exception ex = null, object callerObject = null)
        {
            TraceLog(messageLevel, 2, message, component, userID, username, errorcode, ex, callerObject);
        }
        private static void TraceLog(TraceLevel messageLevel, int jumpFrameN, string message, string component, string userID, string username, string errorcode, Exception ex = null, object callerObject = null)
        {
            if (ex == null || ex.Message == null || ex.Message.ToLower() != "Thread was being aborted.")
            {
                switch (CurrentWriter)
                {
                    //case Writers.DotNet:
                    //    DotNet.DotNetDiagnostics.TraceLog(messageLevel, jumpFrameN, message, component, userID, username, errorcode, ex);
                    //    break;
                    case Writers.Log4Net:
                        Log4Net.Log4NetDiagnostics.TraceLog(messageLevel, message, component, userID, username, errorcode, ex, callerObject);
                        break;
                }
            }
        }
        public static void TraceException(Exception ex, string userID, string username, object callerObject = null, string errorCode = null)
        {
            if (ex == null || ex.Message == null || ex.Message.ToLower() != "Thread was being aborted.")
            {
                switch (CurrentWriter)
                {
                    //case Writers.DotNet:
                    //    DotNet.DotNetDiagnostics.TraceException(ex, userID, username);
                    //    break;
                    case Writers.Log4Net:
                        Log4Net.Log4NetDiagnostics.TraceException(ex, userID, username, callerObject, errorCode);
                        break;
                }
            }
        }
        public static void TraceUnhandledError(StackFrame sf, String message, String userID, String username, Exception ex = null, object callerObject = null)
        {
            if (ex == null || ex.Message == null || ex.Message.ToLower() != "Thread was being aborted.")
            {
                switch (CurrentWriter)
                {
                    //case Writers.DotNet:
                    //    DotNet.DotNetDiagnostics.TraceUnhandledError(sf, message, userID, username);
                    //    break;
                    case Writers.Log4Net:
                        Log4Net.Log4NetDiagnostics.TraceUnhandledError(sf, message, userID, username, ex, callerObject);
                        break;
                }
            }
        }

        public static string GetPageGUID()
        {
            string pageid = "";
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Items[PageGUIDEntryKey] == null)
                    HttpContext.Current.Items[PageGUIDEntryKey] = pageid = Guid.NewGuid().ToString();
                else
                    pageid = HttpContext.Current.Items[PageGUIDEntryKey].ToString();
            }
            else pageid = Guid.NewGuid().ToString();

            return pageid;
        }

        //public static void sendEmail(LogObject logObj)
        //{
        //    string _emailToReport = ConfigurationManager.AppSettings["EmailToReport"].ToString();
        //    string _emailFrom = ConfigurationManager.AppSettings["EmailFrom"].ToString();
        //    string _smtphost = ConfigurationManager.AppSettings["SmtpHost"].ToString();
        //    string _smtpport = ConfigurationManager.AppSettings["SmtpPort"].ToString();
        //    string _smtpUser = ConfigurationManager.AppSettings["SmtpUser"].ToString();            
        //    string _smtpPassword = ConfigurationManager.AppSettings["SmtpPassword"].ToString(); // considera il caso in cui la pw è criptata            
        //    string _enableSSL = ConfigurationManager.AppSettings["EnableSsl"].ToString();

        //    if (_emailToReport.Length > 0 &&
        //        _emailFrom.Length > 0 &&
        //        _smtphost.Length > 0 &&
        //        _smtpport.Length > 0)
        //    {

        //        #region prepare email body
        //        string body = "" +
        //        "Log ID :" +
        //        logObj.LogID + "\n\r" +
        //        "Data ed ora  di generazione del log :" +
        //        logObj.datetime.ToString() + "\n\r" +
        //        "Livello Errore :" +
        //        logObj.level + "\n\r" +
        //        "URL :" +
        //        logObj.URL + "\n\r" +
        //            /*"Form :" +
        //            logObj.Form + "\n\r" +*/
        //        "Query string :" +
        //        logObj.QueryString + "\n\r" +
        //            /*"File :" +
        //            logObj.File + "\n\r" +
        //            "Linea :" +
        //            logObj.Line + "\n\r" +
        //            "Classe :" +
        //            logObj.Class + "\n\r" +
        //            "Methodo :" +
        //            logObj.Method + "\n\r" +*/
        //        "Messaggio :" +
        //        logObj.Message + "\n\r" +
        //        "Stack Trace :" +
        //        logObj.StackTrace + "\n\r" +
        //        "User IP :" +
        //        logObj.UserIP + "\n\r" +
        //        "User ID :" +
        //        logObj.UserID + "\n\r";
        //        /*"UserName :" +
        //        logObj.UserName + "\n\r";*/
        //        #endregion

        //        #region prepare/send email
        //        MailMessage messaggio = new MailMessage();
        //        messaggio.From = new MailAddress(_emailFrom, "Exception Error");
        //        string[] recipients = _emailToReport.Split(';');
        //        for (int i = 0; i < recipients.Length; i++)
        //        {
        //            messaggio.To.Add(new MailAddress(recipients[i]));
        //        }
        //        messaggio.Subject = logObj.PublicErrorCode + " / SRV: " + System.Net.Dns.GetHostName() + " / Level: " + logObj.level + " / " + CleanString(logObj.Message);
        //        messaggio.SubjectEncoding = System.Text.Encoding.UTF8;
        //        messaggio.Body = body;
        //        messaggio.BodyEncoding = System.Text.Encoding.UTF8;
        //        messaggio.IsBodyHtml = false;

        //        SmtpClient smtpClient = new SmtpClient(_smtphost, int.Parse(_smtpport));
        //        if (_smtpUser != null && _smtpPassword != null && _smtpUser.Length > 0 && _smtpPassword.Length > 0)
        //        {
        //            smtpClient.EnableSsl = bool.Parse(_enableSSL);
        //            smtpClient.UseDefaultCredentials = false;
        //            smtpClient.Credentials = new System.Net.NetworkCredential(_smtpUser, _smtpPassword);
        //        }
        //        smtpClient.Send(messaggio);
        //        #endregion
        //    }
        //}

        public static string CleanString(string value, string separator)
        {
            char[] chars = value.ToCharArray();
            string newValue = "";

            //MAIUSCOLE 65/90
            //MINUSCOLE 97/122

            foreach (char c in chars)
            {
                char charValue = c;//char.GetNumericValue(c);
                if ((charValue >= 65 && charValue <= 90) || (charValue >= 97 && charValue <= 122) || (charValue >= 48 && charValue <= 57))
                    newValue += c;
                else newValue += separator;
            }

            while (newValue.Contains(separator + separator))
                newValue = newValue.Replace(separator + separator, separator);

            while (newValue.StartsWith(separator))
                newValue = newValue.Substring(1, newValue.Length - 1);

            while (newValue.EndsWith(separator))
                newValue = newValue.Substring(0, newValue.Length - 1);

            return newValue;
        }
        public static string CleanString(string value)
        {
            return CleanString(value, " ");
        }
    }
}
