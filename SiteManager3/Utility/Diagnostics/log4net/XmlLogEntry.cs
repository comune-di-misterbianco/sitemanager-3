﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Web;
using System.Configuration;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Security.Cryptography;
using log4net.Layout;
using System.Text;
using System.Xml;
using log4net.Core;
using log4net.Util;
using System.Collections;
using System.Threading;


namespace NetCms.Diagnostics.Log4Net
{
    public class LogEntry : XmlLayoutBase
    {
        public const string VerbosityLevelsEntry = "log4netVerbosityLevels";
        private string mElmData = "data";
        private string mElmEvent = "event";
        private string mElmException = "exception";
        private string mElmLocation = "locationInfo";
        private string mElmMessage = "message";
        private string mElmProperties = "properties";

        private enum VerbosityLevels { Min = 1, Low = 2, Normal = 3, High = 4, Max = 5 }

        private static VerbosityLevels VerbosityLevel
        {
            get
            {
                if (_VerbosityLevel == null)
                {
                    var value = System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains(VerbosityLevelsEntry) ? System.Configuration.ConfigurationManager.AppSettings[VerbosityLevelsEntry] : "";
                    if (!string.IsNullOrEmpty(value))
                    {
                        switch (value.ToLower())
                        {
                            case "1": _VerbosityLevel = new VerbosityLevels?(VerbosityLevels.Min); break;
                            case "2": _VerbosityLevel = new VerbosityLevels?(VerbosityLevels.Low); break;
                            case "3": _VerbosityLevel = new VerbosityLevels?(VerbosityLevels.Normal); break;
                            case "4": _VerbosityLevel = new VerbosityLevels?(VerbosityLevels.High); break;
                            case "5": _VerbosityLevel = new VerbosityLevels?(VerbosityLevels.Max); break;
                            default: _VerbosityLevel = new VerbosityLevels?(VerbosityLevels.Max); break;
                        }
                    }
                    else _VerbosityLevel = new VerbosityLevels?(VerbosityLevels.Max);
                }
                return _VerbosityLevel.Value;
            }
        }
        private static VerbosityLevels? _VerbosityLevel;

        public bool Base64EncodeMessage { get; set; }
        public bool Base64EncodeProperties { get; set; }
        public string Prefix { get; set; }

        // Methods
        public LogEntry()
        {
            Prefix = "log4net";
            Base64EncodeMessage = false;
            Base64EncodeProperties = false;

        }

        public LogEntry(bool locationInfo)
            : base(locationInfo)
        {
            Prefix = "log4net";
            Base64EncodeMessage = false;
            Base64EncodeProperties = false;
        }

        public override void ActivateOptions()
        {
            base.ActivateOptions();
            if (!string.IsNullOrEmpty(Prefix))
            {
                mElmEvent = string.Concat(Prefix, ":event");
                mElmMessage = string.Concat(Prefix, ":message");
                mElmProperties = string.Concat(Prefix, ":properties");
                mElmData = string.Concat(Prefix, ":data");
                mElmException = string.Concat(Prefix, ":exception");
                mElmLocation = string.Concat(Prefix, ":locationInfo");
            }
        }

        private static string GetVariables(NameValueCollection coll, PropertiesDictionary properties = null)
        {
            StringBuilder sb = new StringBuilder();

            if (properties == null)
            {
                foreach (string key in coll)                
                    sb.AppendFormat(string.Format("{0}:{1}" + System.Environment.NewLine, key, coll[key]));                
            }
            else
                foreach (string key in coll)
                    sb.AppendFormat(string.Format("{0}:{1}" + System.Environment.NewLine, key, CleanSensitiveData(key, coll[key])));



            return sb.ToString();
        }

        private string BuildLoggerValue(string localPageGUID = "")
        {
            string result = "";

            //HttpRequest currentRequest = HttpContext.Current.Request;
            
           
            if (System.Web.HttpContext.Current != null )
            {
                //result = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                result = HttpRuntime.AppDomainAppVirtualPath;
                result = result.Replace(".aspx", "");
                result = result.Replace(".", "_");
                result += ".";
            }
            result += localPageGUID;

            return result;
        }

        protected override void FormatXml(XmlWriter writer, LoggingEvent loggingEvent)
        {
            if (loggingEvent is ExtendedLoggingEvent)
                FormatXmlExtended(writer, loggingEvent as ExtendedLoggingEvent);
            else
                FormatXmlGeneric(writer, loggingEvent);

        }
        protected void FormatXmlGeneric(XmlWriter writer, LoggingEvent loggingEvent)
        {
            if (VerbosityLevel >= VerbosityLevels.High)
            {
                if (System.Web.HttpContext.Current != null)
                {
                    try { loggingEvent.Properties["Url"] = System.Web.HttpContext.Current.Request.Url.AbsoluteUri; }
                    catch { }
                    try { loggingEvent.Properties["UserAgent"] = System.Web.HttpContext.Current.Request.UserAgent; }
                    catch { }
                    try { loggingEvent.Properties["UserHostName"] = System.Web.HttpContext.Current.Request.UserHostName; }
                    catch { }
                    try { loggingEvent.Properties["ServerVariables"] = GetVariables(System.Web.HttpContext.Current.Request.ServerVariables); }
                    catch { }
                    try { loggingEvent.Properties["Form"] = GetVariables(System.Web.HttpContext.Current.Request.Form); }
                    catch { }
                    try { loggingEvent.Properties["ServerName"] = System.Web.HttpContext.Current.Request.ServerVariables["Server_Name"]; }
                    catch { }
                    try { loggingEvent.Properties["QueryString"] = System.Web.HttpContext.Current.Request.ServerVariables["Query_String"]; }
                    catch { }
                    try { loggingEvent.Properties["ThreadLanguage"] = Thread.CurrentThread.CurrentCulture.DisplayName; }
                    catch { }
                }
            }
            writer.WriteStartElement(mElmEvent);
            writer.WriteAttributeString("logger", BuildLoggerValue());
            writer.WriteAttributeString("timestamp", XmlConvert.ToString(loggingEvent.TimeStamp, XmlDateTimeSerializationMode.Local));
            writer.WriteAttributeString("level", loggingEvent.Level.DisplayName);
            //writer.WriteAttributeString("thread", loggingEvent.ThreadName);

            if (!string.IsNullOrEmpty(loggingEvent.Domain))
                writer.WriteAttributeString("domain", loggingEvent.Domain);

            if (!string.IsNullOrEmpty(loggingEvent.Identity))
                writer.WriteAttributeString("identity", loggingEvent.Identity);

            writer.WriteStartElement(mElmMessage);

            if (!Base64EncodeMessage)
                Transform.WriteEscapedXmlString(writer, loggingEvent.RenderedMessage, InvalidCharReplacement);
            else
            {
                byte[] bytes = Encoding.UTF8.GetBytes(loggingEvent.RenderedMessage);
                string textData = Convert.ToBase64String(bytes, 0, bytes.Length);
                Transform.WriteEscapedXmlString(writer, textData, InvalidCharReplacement);
            }
            writer.WriteEndElement();

            PropertiesDictionary properties = loggingEvent.GetProperties();
            if (properties.Count > 0)
            {
                writer.WriteStartElement(mElmProperties);
                foreach (DictionaryEntry entry in properties)
                {
                    writer.WriteStartElement(mElmData);
                    writer.WriteAttributeString("name", Transform.MaskXmlInvalidCharacters((string)entry.Key, InvalidCharReplacement));
                    string str2;
                    if (!Base64EncodeProperties)
                        str2 = Transform.MaskXmlInvalidCharacters(loggingEvent.Repository.RendererMap.FindAndRender(entry.Value), InvalidCharReplacement);
                    else
                    {
                        byte[] inArray = Encoding.UTF8.GetBytes(loggingEvent.Repository.RendererMap.FindAndRender(entry.Value));
                        str2 = Convert.ToBase64String(inArray, 0, inArray.Length);
                    }
                    writer.WriteAttributeString("value", str2);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }

            if (loggingEvent.ExceptionObject != null)
            {
                StringBuilder exceptionString = new StringBuilder();

                exceptionString.Append(loggingEvent.ExceptionObject);
                exceptionString.Append(Environment.NewLine);

                foreach (DictionaryEntry de in loggingEvent.ExceptionObject.Data)
                {
                    exceptionString.Append(de.Key.ToString());
                    exceptionString.Append(": ");
                    exceptionString.Append(de.Value.ToString());
                    exceptionString.Append(Environment.NewLine);
                }
                if ((exceptionString.Length > 0))
                {
                    writer.WriteStartElement(mElmException);
                    Transform.WriteEscapedXmlString(writer, exceptionString.ToString(), InvalidCharReplacement);
                    writer.WriteEndElement();
                }
            }
            if (LocationInfo)
            {
                LocationInfo locationInformation = loggingEvent.LocationInformation;
                writer.WriteStartElement(mElmLocation);
                writer.WriteAttributeString("class", locationInformation.ClassName);
                writer.WriteAttributeString("method", locationInformation.MethodName);
                writer.WriteAttributeString("file", locationInformation.FileName);
                writer.WriteAttributeString("line", locationInformation.LineNumber);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }
        protected void FormatXmlExtended(XmlWriter writer, ExtendedLoggingEvent customEvent)
        {
            if (System.Web.HttpContext.Current != null)
            {
                try { customEvent.Properties["Url"] = System.Web.HttpContext.Current.Request.Url.AbsoluteUri; }
                catch { }

                if (VerbosityLevel >= VerbosityLevels.High)
                {
                    try { customEvent.Properties["UserAgent"] = System.Web.HttpContext.Current.Request.UserAgent; }
                    catch { }
                    try { customEvent.Properties["UserHostName"] = System.Web.HttpContext.Current.Request.UserHostName; }
                    catch { }
                    try { customEvent.Properties["ServerVariables"] = GetVariables(System.Web.HttpContext.Current.Request.ServerVariables); }
                    catch { }
                    try { 
                        customEvent.Properties["Form"] = GetVariables(System.Web.HttpContext.Current.Request.Form, customEvent.Properties); 
                    }
                    catch { 
                    }
                    try { customEvent.Properties["ServerName"] = System.Web.HttpContext.Current.Request.ServerVariables["Server_Name"]; }
                    catch { }
                    try { customEvent.Properties["QueryString"] = System.Web.HttpContext.Current.Request.ServerVariables["Query_String"]; }
                    catch { }
                    try { customEvent.Properties["ThreadLanguage"] = Thread.CurrentThread.CurrentCulture.DisplayName; }
                    catch { }
                }
            }

            writer.WriteStartElement(mElmEvent);
            if (!string.IsNullOrEmpty(customEvent.LocalPageGUID))
                writer.WriteAttributeString("logger", BuildLoggerValue(customEvent.LocalPageGUID));
            writer.WriteAttributeString("timestamp", XmlConvert.ToString(customEvent.TimeStamp, XmlDateTimeSerializationMode.Local));
            // ho modificato qui
            writer.WriteAttributeString("level", customEvent.Level.DisplayName);

            //if (VerbosityLevel >= VerbosityLevels.High)
            //    writer.WriteAttributeString("thread", customEvent.ThreadName);

            if (VerbosityLevel >= VerbosityLevels.High)
                if (!string.IsNullOrEmpty(customEvent.Domain))
                    writer.WriteAttributeString("domain", customEvent.Domain);

            if (VerbosityLevel >= VerbosityLevels.High)
                if (!string.IsNullOrEmpty(customEvent.Identity))
                    writer.WriteAttributeString("identity", customEvent.Identity);

            if (!string.IsNullOrEmpty(customEvent.LocalUserName))
                // ho modificato qui
                writer.WriteAttributeString("localaccount", customEvent.UserName);

            if (!string.IsNullOrEmpty(customEvent.LocalLogID))
                writer.WriteAttributeString("logID", customEvent.LocalLogID);

            if (!string.IsNullOrEmpty(customEvent.LocalPageGUID))
                writer.WriteAttributeString("httpRequestID", customEvent.LocalPageGUID);

            if (!string.IsNullOrEmpty(customEvent.LocalComponent))
                writer.WriteAttributeString("component", customEvent.LocalComponent);

            writer.WriteAttributeString("smLevel", customEvent.Level.ToString());

            if (!string.IsNullOrEmpty(customEvent.LocalErrorCode))
                writer.WriteAttributeString("errorCode", customEvent.LocalErrorCode);

            if (!string.IsNullOrEmpty(customEvent.LocalUserName))
                writer.WriteAttributeString("username", customEvent.LocalUserName);

            if (!string.IsNullOrEmpty(customEvent.LocalUserID))
                writer.WriteAttributeString("userid", customEvent.LocalUserID);

            writer.WriteStartElement(mElmMessage);

            if (!Base64EncodeMessage)
                Transform.WriteEscapedXmlString(writer, customEvent.RenderedMessage, InvalidCharReplacement);
            else
            {
                byte[] bytes = Encoding.UTF8.GetBytes(customEvent.RenderedMessage);
                string textData = Convert.ToBase64String(bytes, 0, bytes.Length);
                Transform.WriteEscapedXmlString(writer, textData, InvalidCharReplacement);
            }
            writer.WriteEndElement();

            PropertiesDictionary properties = customEvent.GetProperties();
            if (VerbosityLevel >= VerbosityLevels.High)
                if (properties.Count > 0)
                {
                    writer.WriteStartElement(mElmProperties);
                    foreach (DictionaryEntry entry in properties)
                    {
                        writer.WriteStartElement(mElmData);
                        writer.WriteAttributeString("name", Transform.MaskXmlInvalidCharacters((string)entry.Key, InvalidCharReplacement));
                        string str2;
                        if (!Base64EncodeProperties)
                            str2 = Transform.MaskXmlInvalidCharacters(customEvent.Repository.RendererMap.FindAndRender(entry.Value), InvalidCharReplacement);
                        else
                        {
                            byte[] inArray = Encoding.UTF8.GetBytes(customEvent.Repository.RendererMap.FindAndRender(entry.Value));
                            str2 = Convert.ToBase64String(inArray, 0, inArray.Length);
                        }                            
                        
                        writer.WriteAttributeString("value", str2);
                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();
                }

            if (VerbosityLevel >= VerbosityLevels.High)
                if (customEvent.ExceptionObject != null)
                {
                    StringBuilder exceptionString = new StringBuilder();

                    exceptionString.Append(customEvent.ExceptionObject);
                    exceptionString.Append(Environment.NewLine);

                    foreach (DictionaryEntry de in customEvent.ExceptionObject.Data)
                    {
                        exceptionString.Append(de.Key.ToString());
                        exceptionString.Append(": ");
                        exceptionString.Append(de.Value.ToString());
                        exceptionString.Append(Environment.NewLine);
                    }
                    if ((exceptionString.Length > 0))
                    {
                        writer.WriteStartElement(mElmException);
                        Transform.WriteEscapedXmlString(writer, exceptionString.ToString(), InvalidCharReplacement);
                        writer.WriteEndElement();
                    }
                }
            if (VerbosityLevel >= VerbosityLevels.Max)
                if (LocationInfo)
                {
                    LocationInfo locationInformation = customEvent.LocationInformation;
                    if (locationInformation.ClassName.Length > 1)
                    {
                        writer.WriteStartElement(mElmLocation);
                        writer.WriteAttributeString("class", locationInformation.ClassName);
                        writer.WriteAttributeString("method", locationInformation.MethodName);
                        writer.WriteAttributeString("file", locationInformation.FileName);
                        writer.WriteAttributeString("line", locationInformation.LineNumber);
                        writer.WriteEndElement();
                    }
                }
                writer.WriteEndElement();
        }    

        private static string CleanSensitiveData(string key, string value)
        {
            if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value))
            {
                if (key.ToLower().Contains("__VIEWSTATE".ToLower()) || key.ToLower().Contains("__VIEWSTATEGENERATOR".ToLower()))
                    value = string.Empty;

                if (key.ToLower().Contains("password_logincontrol")
                || key.ToLower().Contains("password_user")
                || key.ToLower().Contains("password_user_confirm")
                || key.ToLower().Contains("password_user_old")
                || key.ToLower().Contains("password"))
                    value = "************;";
            }
            
            return value;
        }
    }
    public class RandomNums
    {
        private RNGCryptoServiceProvider _rand = new RNGCryptoServiceProvider();
        private byte[] _rd4 = new byte[4], _rd8 = new byte[8];

        /// <summary>
        /// Genera un numero intero positivo casuale minore di max
        /// </summary>
        /// <param name="max">Limite superiore del numero casuale generato</param>
        /// <returns>Il numero casuale generato</returns>
        public int Next(int max)
        {
            if (max <= 0)
            {
                Exception ex = new ArgumentOutOfRangeException("Max");
                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Warning, ex);*/
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, ex.Message, "", "", "");
            }
            _rand.GetBytes(_rd4);
            int val = BitConverter.ToInt32(_rd4, 0) % max;
            if (val < 0) val = -val;
            return val;
        }

        /// <summary>
        /// Genera un numero intero positivo casuale compreso tra min e max
        /// </summary>
        /// <param name="min">Valore minimo possibile</param>
        /// <param name="max">Valore massimo possibile</param>
        /// <returns>Il numero casuale generato</returns>
        public int Next(int min, int max)
        {
            if (min > max)
            {
                Exception ex = new ArgumentOutOfRangeException();
                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Warning, ex);*/
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, ex.Message, "", "", "");
            }
            return Next(max - min + 1) + min;
        }

        /// <summary>
        /// Genera un numero (double) compreso tra 0.0 e 1.1
        /// </summary>
        /// <returns>Il numero casuale generato</returns>
        public double NextDouble()
        {
            _rand.GetBytes(_rd8);
            return BitConverter.ToUInt64(_rd8, 0) / (double)UInt64.MaxValue;
        }

    }
}
