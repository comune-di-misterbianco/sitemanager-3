﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Web;
using System.Configuration;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Security.Cryptography;


namespace NetCms.Diagnostics
{
    public static class VerticalDiagnostics
    {
        public static void TraceMessage(TraceLevel messageLevel, string message)
        {
            Diagnostics.TraceMessage(messageLevel, message, "", "");
        }


        public static void TraceException(string message, Exception ex)
        {
            Diagnostics.TraceMessage(TraceLevel.Error, message, "", "", ex);
        }

        public static void TraceException(string message, string component, string errorcode, Exception ex)
        {
            Diagnostics.TraceMessage(TraceLevel.Error, message, component, "", "", errorcode, ex);
        }

        public static void TraceMessage(TraceLevel messageLevel, string message, string component, string userid, string username, string errorcode)
        {
            Diagnostics.TraceMessage(messageLevel, message, component, userid, username, errorcode);
        }
    }
}
