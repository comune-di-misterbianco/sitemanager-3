﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Web;
using System.Configuration;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Security.Cryptography;
using log4net;


namespace NetCms.Diagnostics.Log4Net
{
    public static class Log4NetDiagnostics
    {
        public static void Redirect(string url)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Response.Redirect(url);
                NetCms.Diagnostics.Diagnostics.TraceMessage(TraceLevel.Error, "Redirect a " + url, "", "", "");
            }
            else
                NetCms.Diagnostics.Diagnostics.TraceMessage(TraceLevel.Error, "Redirect Impossibile a Causa di Mancanza del Context verso " + url, "", "", "");
        }

        public static void TraceMessage(TraceLevel messageLevel, string message, string userID, string username)
        {
            TraceLog(messageLevel, message, "", userID, username, "");
        }
        public static void TraceMessage(TraceLevel messageLevel, string message, string userID, string username, int jumpFrameN)
        {
            TraceLog(messageLevel, message, "", userID, username, "");
        }
        public static void TraceMessage(TraceLevel messageLevel, string message, string userID, string username, string errorcode)
        {
            TraceLog(messageLevel, message, "", userID, username, errorcode);
        }
        public static void TraceMessage(TraceLevel messageLevel, string message, string userID, string username, string errorcode, int jumpFrameN)
        {
            TraceLog(messageLevel, message, "", userID, username, errorcode);
        }
        public static void TraceMessage(TraceLevel messageLevel, string message, string component, string userID, string username, string errorcode)
        {
            TraceLog(messageLevel, message, component, userID, username, errorcode);
        }
        public static void TraceMessage(TraceLevel messageLevel, string message, string component, string userID, string username, string errorcode, int jumpFrameN)
        {
            TraceLog(messageLevel, message, component, userID, username, errorcode);
        }
        public static void TraceException(Exception ex, string userID, string username, object callerObject = null, string errorCode = null)
        {
            TraceLog(TraceLevel.Error, ex.Message, "", userID, username, errorCode, ex,callerObject);
        }
        public static void TraceUnhandledError(StackFrame sf, String message, String userID, String username, Exception ex = null, object callerObject = null)
        {
            TraceLog(TraceLevel.Error, message, "", "", "", "", ex);
        }
        public static void TraceLog(TraceLevel messageLevel, string message, string component, string userID, string username, string errorcode, Exception ex = null, object callerObject = null)
        {
            ILog logger = LogManager.GetLogger("rollingFile");
            DiagnosticUserData userdata = new DiagnosticUserData(userID, username);
            var loggingEvent = new ExtendedLoggingEvent(logger, message, messageLevel, component, userdata.UserID, userdata.UserName, errorcode, ex);
           
            logger.Logger.Log(loggingEvent);

            if (messageLevel == TraceLevel.Error || messageLevel == TraceLevel.Fatal || messageLevel == TraceLevel.Warning)
                Diagnostics.ExceptionsForThisPage.Add(loggingEvent);
        }

        public static string CleanString(string value, string separator)
        {
            char[] chars = value.ToCharArray();
            string newValue = "";

            //MAIUSCOLE 65/90
            //MINUSCOLE 97/122

            foreach (char c in chars)
            {
                char charValue = c;//char.GetNumericValue(c);
                if ((charValue >= 65 && charValue <= 90) || (charValue >= 97 && charValue <= 122) || (charValue >= 48 && charValue <= 57))
                    newValue += c;
                else newValue += separator;
            }

            while (newValue.Contains(separator + separator))
                newValue = newValue.Replace(separator + separator, separator);

            while (newValue.StartsWith(separator))
                newValue = newValue.Substring(1, newValue.Length - 1);

            while (newValue.EndsWith(separator))
                newValue = newValue.Substring(0, newValue.Length - 1);

            return newValue;
        }
        public static string CleanString(string value)
        {
            return CleanString(value, " ");
        }

        private static string FilterForm(NameValueCollection myForm)
        {
            string myViewstate = "";
            string myEventValidation = "";
            string allValue = HttpContext.Current.Server.UrlDecode(myForm.ToString());

            if (myForm["__VIEWSTATE"] != null)
            {
                myViewstate = "__VIEWSTATE=" + myForm["__VIEWSTATE"].ToString() + "&";
                allValue = allValue.Replace(myViewstate, "");
            }
            if (myForm["__EVENTVALIDATION"] != null)
            {
                myEventValidation = "__EVENTVALIDATION=" + myForm["__EVENTVALIDATION"].ToString() + "&";
                allValue = allValue.Replace(myEventValidation, "");
            }
            if (allValue.Contains("password_LoginControl1="))
            {
                string pattern = @"password_LoginControl1=*.*&";
                allValue = System.Text.RegularExpressions.Regex.Replace(allValue, pattern, "password_LoginControl1=************;");
            }
            if (allValue.Contains("username_LoginControl2="))
            {
                string pattern = @"password_LoginControl2=*.*&";
                allValue = System.Text.RegularExpressions.Regex.Replace(allValue, pattern, "password_LoginControl2=************;");
            }
            if (allValue.Contains("password_LoginControl1:"))
            {
                string pattern = @"password_LoginControl1:*.*&";
                allValue = System.Text.RegularExpressions.Regex.Replace(allValue, pattern, "password_LoginControl1:************;");
            }
            if (allValue.Contains("Password_User="))
            {
                string pattern = @"Password_User=*.*&";
                allValue = System.Text.RegularExpressions.Regex.Replace(allValue, pattern, "Password_User=************;");
            }
            if (allValue.Contains("Password_User_Confirm="))
            {
                string pattern = @"Password_User_Confirm=*.*&";
                allValue = System.Text.RegularExpressions.Regex.Replace(allValue, pattern, "Password_User_Confirm=************;");
            }
            if (allValue.Contains("Password_User_Old="))
            {
                string pattern = @"Password_User_Old=*.*&";
                allValue = System.Text.RegularExpressions.Regex.Replace(allValue, pattern, "Password_User_Old=************;");
            }
            return allValue;
        }
        private static string FilterMessage(string input)
        {
            string output = input;

            if (input.Contains("Pwd="))
            {
                string pattern = @"Pwd=*.*;";
                output = System.Text.RegularExpressions.Regex.Replace(input, pattern, "Pwd=**********");
            }

            return output;
        }
    }
}
