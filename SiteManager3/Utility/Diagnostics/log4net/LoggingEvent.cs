﻿using System;
using System.Web;
using log4net;
using log4net.Core;


namespace NetCms.Diagnostics.Log4Net
{
    public class ExtendedLoggingEvent : LoggingEvent, IPageLogEntry
    {
        public const string PageGUIDEntryKey = "PageGUIDEntryKey";

        public DateTime LocalLogTime { get; set; }
        public string LocalUserName { get; set; }
        public string LocalUserID { get; set; }
        public string LocalPageGUID { get; set; }
        public TraceLevel LocalLevel { get; set; }
        public string LocalErrorCode { get; set; }
        public string LocalComponent { get; set; }
        public string LocalLogID { get; set; }
        public string LocalMessage { get; set; }

        public DateTime LogDateTime { get { return LocalLogTime; } }
        public string LogMessage { get { return LocalMessage; } }
        public string LogID { get { return LocalLogID; } }
        public string LogErrorCode { get { return LocalErrorCode; } }

        public static string GetPageGUID()
        {
            string pageid = "";
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Items[PageGUIDEntryKey] == null)
                    HttpContext.Current.Items[PageGUIDEntryKey] = pageid = Guid.NewGuid().ToString();
                else
                    pageid = HttpContext.Current.Items[PageGUIDEntryKey].ToString();
            }
            else pageid = Guid.NewGuid().ToString();

            return pageid;
        }

        public string PublicErrorCode
        {
            get { return !string.IsNullOrEmpty(LocalErrorCode) ? "EK#" + FormatNumber(LocalErrorCode, 6) : "EU#0000UNK"; }
        }

        public ExtendedLoggingEvent(ILog logger, string message, TraceLevel messageLevel, string component, string userID, string username, string errorcode)
            : this(logger, message, messageLevel, component, userID, username, errorcode, null)
        {
        }
        public ExtendedLoggingEvent(ILog logger, string message, TraceLevel messageLevel, string component, string userID, string username, string errorcode, Exception ex)
            : base(typeof( NetCms.Diagnostics.Diagnostics), logger.Logger.Repository, logger.Logger.Name, SwitchLevelAttribute(messageLevel), message, ex)
        {
            this.LocalUserName = username;
            this.LocalUserID = userID;
            this.LocalPageGUID = GetPageGUID();
            this.LocalLevel = messageLevel;
            this.LocalErrorCode = errorcode;
            this.LocalComponent = component;
            this.LocalLogID = BuildLogID();
            this.LocalMessage = message;
            this.LocalLogTime = DateTime.Now;

            if (!string.IsNullOrWhiteSpace(component))
            {
                this.LocalComponent = component;
            }
            else
            {
                if (HttpContext.Current != null && 
                    HttpContext.Current.Items != null && 
                    HttpContext.Current.Items.Contains(Diagnostics.ApplicationContextCurrentComponentItemsKey)&&
                    HttpContext.Current.Items[Diagnostics.ApplicationContextCurrentComponentItemsKey] is string )
                {
                    this.LocalComponent = HttpContext.Current.Items[Diagnostics.ApplicationContextCurrentComponentItemsKey] as string;
                }
            }
        }

        private string BuildLogID()
        {
            DateTime time = DateTime.Now;
            RandomNums nums = new RandomNums();
            string compactTime = ""; //es: 0911032251420
            compactTime += time.Year.ToString().Substring(2, 2);
            compactTime += FormatNumber(time.Month, 2);
            compactTime += FormatNumber(time.Day, 2);
            compactTime += FormatNumber(time.Hour, 2);
            compactTime += FormatNumber(time.Minute, 2);
            compactTime += FormatNumber(time.Second, 2);
            compactTime += FormatNumber(time.Millisecond, 3);

            return compactTime.Substring(0, 4) + "-" + compactTime.Substring(4, 4) + "-" + compactTime.Substring(8, 4) + "-" + compactTime.Substring(12, 3) + "-" + FormatNumber(nums.Next(int.MaxValue), 10);
        }

        public static log4net.Core.Level SwitchLevelAttribute(TraceLevel messageLevel)
        {
            switch (messageLevel)
            {
                case TraceLevel.Error: return log4net.Core.Level.Error;
                case TraceLevel.Fatal: return log4net.Core.Level.Fatal;
                case TraceLevel.Info: return log4net.Core.Level.Info;
                case TraceLevel.Request: return log4net.Core.Level.Debug;
                case TraceLevel.Verbose: return log4net.Core.Level.Verbose;
                case TraceLevel.Warning: return log4net.Core.Level.Warn;
                default: return log4net.Core.Level.Error;
            }
        }

        private static string FormatNumber(int number, int lenght)
        {
            return FormatNumber(number.ToString(), lenght);
        }
        private static string FormatNumber(string number, int lenght)
        {
            string value = number;

            while (value.Length < lenght)
                value = "0" + value;

            return value;
        }
    }
}
