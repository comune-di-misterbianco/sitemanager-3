﻿//using System;
//using System.Web;
//using log4net;
//using log4net.Core;
//using System.Security.Cryptography;


//namespace NetCms.Diagnostics
//{
//    public class LogObject : IPageLogEntry
//    {
//        public DateTime LogDateTime { get { return datetime; } }
//        public string LogMessage { get { return Message; } }
//        public string LogErrorCode { get { return errorCode; } }

//        public DateTime datetime = DateTime.MinValue;
//        public TraceLevel level = TraceLevel.Off;
//        public string errorCode = "";
//        public string URL = "";
//        public string QueryString = "";
//        public string File = "";
//        public string Class = "";
//        public string Message = "";
//        public string Component = "";
//        public string StackTrace = "";

//        public string AllKeys = "";
//        public string AllForms = "";

//        public string UserIP = "";
//        public string UserID = "";
//        public string UserName = "";

//        public string PageGUID = "";

//        private string _LogID;
//        public string LogID
//        {
//            get
//            {
//                if (_LogID == null)
//                {
//                    DateTime time = datetime;
//                    RandomNums nums = new RandomNums();
//                    string compactTime = ""; //es: 0911032251420
//                    compactTime += time.Year.ToString().Substring(2, 2);
//                    compactTime += FormatNumber(time.Month, 2);
//                    compactTime += FormatNumber(time.Day, 2);
//                    compactTime += FormatNumber(time.Hour, 2);
//                    compactTime += FormatNumber(time.Minute, 2);
//                    compactTime += FormatNumber(time.Second, 2);
//                    compactTime += FormatNumber(time.Millisecond, 3);

//                    _LogID = compactTime.Substring(0, 4) + "-" + compactTime.Substring(4, 4) + "-" + compactTime.Substring(8, 4) + "-" + compactTime.Substring(12, 3) + "-" + FormatNumber(nums.Next(int.MaxValue), 10);

//                }
//                return _LogID;
//            }
//        }

//        public string PublicErrorCode
//        {
//            get { return !string.IsNullOrEmpty(errorCode) ? "EK#" + FormatNumber(errorCode, 6) : "EU#0000UNK"; }
//        }

//        private static string FormatNumber(int number, int lenght)
//        {
//            return FormatNumber(number.ToString(), lenght);
//        }
//        private static string FormatNumber(string number, int lenght)
//        {
//            string value = number;

//            while (value.Length < lenght)
//                value = "0" + value;

//            return value;
//        }


//        /// <summary>
//        /// Numeri pseudo-casuali e cryptographically-strong
//        /// </summary>
//        private class RandomNums
//        {
//            private RNGCryptoServiceProvider _rand = new RNGCryptoServiceProvider();
//            private byte[] _rd4 = new byte[4], _rd8 = new byte[8];

//            /// <summary>
//            /// Genera un numero intero positivo casuale minore di max
//            /// </summary>
//            /// <param name="max">Limite superiore del numero casuale generato</param>
//            /// <returns>Il numero casuale generato</returns>
//            public int Next(int max)
//            {
//                if (max <= 0)
//                {
//                    Exception ex = new ArgumentOutOfRangeException("Max");
//                    /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
//                    ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Warning, ex);*/
//                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, ex.Message, "", "", "");
//                }
//                _rand.GetBytes(_rd4);
//                int val = BitConverter.ToInt32(_rd4, 0) % max;
//                if (val < 0) val = -val;
//                return val;
//            }

//            /// <summary>
//            /// Genera un numero intero positivo casuale compreso tra min e max
//            /// </summary>
//            /// <param name="min">Valore minimo possibile</param>
//            /// <param name="max">Valore massimo possibile</param>
//            /// <returns>Il numero casuale generato</returns>
//            public int Next(int min, int max)
//            {
//                if (min > max)
//                {
//                    Exception ex = new ArgumentOutOfRangeException();
//                    /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
//                    ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Warning, ex);*/
//                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, ex.Message, "", "", "");
//                }
//                return Next(max - min + 1) + min;
//            }

//            /// <summary>
//            /// Genera un numero (double) compreso tra 0.0 e 1.1
//            /// </summary>
//            /// <returns>Il numero casuale generato</returns>
//            public double NextDouble()
//            {
//                _rand.GetBytes(_rd8);
//                return BitConverter.ToUInt64(_rd8, 0) / (double)UInt64.MaxValue;
//            }

//        }
//    }
//}
