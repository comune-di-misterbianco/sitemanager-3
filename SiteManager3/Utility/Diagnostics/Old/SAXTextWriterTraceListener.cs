﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Diagnostics;
//using System.Web;
//using System.Configuration;
//using System.Collections.Specialized;
//using System.Data;
//using System.Data.SqlClient;
//using MySql.Data.MySqlClient;
//using MySql.Data;
////using Microsoft.VisualBasic.Logging;
//using System.IO;


//namespace NetCms.Diagnostics
//{

//    class SAXTextWriterTraceListener : TextWriterTraceListener //FileLogTraceListener 
//    {
//        private int _iMaximumRequests;
//        private StringCollection _objCollection;
//        private int fileCount = 0;
//        private static object locker = new Object();

//        #region RollOver
//        string FileName = "";
//        private System.DateTime CurrentDate;

//        public System.IO.StreamWriter TraceWriter
//        {
//            get
//            {
//                return _TraceWriter;
//            }
//            set
//            {
//                _TraceWriter = value;
//            }
//        }
//        private System.IO.StreamWriter _TraceWriter;

//        #endregion

//        public SAXTextWriterTraceListener()
//            : base()
//        {
//            Inizialize();
//        }

//        public SAXTextWriterTraceListener(string filename)
//            : base(filename)
//        {
//            FileName = filename;
//            Inizialize();
//        }

//        private void Inizialize()
//        {
//            _iMaximumRequests = Convert.ToInt32(ConfigurationSettings.AppSettings["TraceTextMaximumRequests"]);
//            _objCollection = new StringCollection();
//            TraceWriter = new StreamWriter(generateFilename(), true);

//        }

//        private void AddToCollection(LogObject logObj, string category, string newline)
//        {
//            string strLog = LogObjectToString(logObj);
//            _objCollection.Add(strLog + newline);
//            if (_objCollection.Count >= _iMaximumRequests)
//            {
//                FlushData();
//            }

//        }

//        /*public SAXTextWriterTraceListener(System.IO.Stream stream)
//            : base(stream)
//        {
//        }

//        public SAXTextWriterTraceListener(System.IO.TextWriter text)
//            : base(text)
//        {
//        }

//        public SAXTextWriterTraceListener(string filename, string name)
//            : base(filename,name)
//        {
//        }

//        public SAXTextWriterTraceListener(System.IO.Stream stream, string name)
//            : base(stream,name)
//        {
//        }

//        public SAXTextWriterTraceListener(System.IO.TextWriter text, string name)
//            : base(text, name)
//        {
//        }*/

//        private void FlushData()
//        {
//            try
//            {
//                while (_objCollection.Count > 0)
//                {
//                    this.WriteLine(_objCollection[0]);
//                    _objCollection.RemoveAt(0);
//                }
//            }
//            catch (System.InvalidOperationException ex)
//            {
//                string current_filename = ((FileStream)TraceWriter.BaseStream).Name;
//                current_filename = Path.GetFileNameWithoutExtension(current_filename);
//                int.TryParse(current_filename.Substring(current_filename.IndexOf("_") + 1), out fileCount);
//                fileCount++;
//                checkRollover(true);
//                //create new file                
//                //this.BaseFileName = this.BaseFileName.Substring(0, 3) + (int.Parse(this.BaseFileName.Substring(3)) + 1).ToString();                          

//            }
//            while (_objCollection.Count > 0)
//            {
//                this.WriteLine(_objCollection[0]);
//                _objCollection.RemoveAt(0);
//            }
//            if (TraceWriter != null) TraceWriter.Flush();
//            //base.Flush();            
//        }

//        private string LogObjectToString(LogObject logobj)
//        {
//            DateTime dataora_log = logobj.datetime;

//            string anno = dataora_log.Year.ToString();
//            string mese = dataora_log.Month.ToString();
//            if (mese.Length == 1)
//                mese = "0" + mese;
//            string giorno = dataora_log.Day.ToString();
//            if (giorno.Length == 1)
//                giorno = "0" + giorno;

//            string ora = dataora_log.Hour.ToString();
//            if (ora.Length == 1)
//                ora = "0" + ora;
//            string min = dataora_log.Minute.ToString();
//            if (min.Length == 1)
//                min = "0" + min;
//            string sec = dataora_log.Second.ToString();
//            if (sec.Length == 1)
//                sec = "0" + sec;
//            string ms = dataora_log.Millisecond.ToString();
//            if (ms.Length == 1)
//                ms = "0" + ms;

//            string out_dataora = anno + "/" + mese + "/" + giorno + " " + ora + ":" + min + ":" + sec + ":" + ms;

//            StringBuilder sb = new StringBuilder();
//            sb.Append("<tracelog ");
//            sb.Append(" LogID=\"");
//            sb.Append(logobj.LogID);
//            sb.Append("\" datetime=\"");
//            sb.Append(out_dataora); // aggiungere i millisecondi
//            sb.Append("\" level=\"");
//            sb.Append(logobj.level);
//            sb.Append("\" errorcode=\"");
//            sb.Append(logobj.errorCode);
//            sb.Append("\" URL=\"");
//            sb.Append(System.Security.SecurityElement.Escape(logobj.URL));
//            sb.Append("\" QueryString=\"");
//            sb.Append(System.Security.SecurityElement.Escape(logobj.QueryString));
//            sb.Append("\" File=\"");
//            sb.Append(System.Security.SecurityElement.Escape(logobj.File));
//            /*sb.Append("\" Line=\"");
//            sb.Append(System.Security.SecurityElement.Escape(logobj.Line));*/
//            sb.Append("\" Class=\"");
//            sb.Append(System.Security.SecurityElement.Escape(logobj.Class));
//            sb.Append("\" Component=\"");
//            sb.Append(System.Security.SecurityElement.Escape(logobj.Component));
//            /*sb.Append("\" Method=\"");
//            sb.Append(logobj.Method);*/
//            sb.Append("\" Message=\"");
//            sb.Append(System.Security.SecurityElement.Escape(logobj.Message));
//            sb.Append("\" AllHeadKeys=\"");
//            sb.Append(System.Security.SecurityElement.Escape(logobj.AllKeys));
//            sb.Append("\" AllForms=\"");
//            sb.Append(System.Security.SecurityElement.Escape(logobj.AllForms));
//            sb.Append("\" StackTrace=\"");
//            sb.Append(System.Security.SecurityElement.Escape(logobj.StackTrace));
//            sb.Append("\" UserIP=\"");
//            sb.Append(logobj.UserIP);
//            sb.Append("\" UserID=\"");
//            sb.Append(System.Security.SecurityElement.Escape(logobj.UserID));
//            sb.Append("\" UserName=\"");
//            sb.Append(System.Security.SecurityElement.Escape(logobj.UserName));
//            sb.Append("\" HttpRequestID=\"");
//            sb.Append(System.Security.SecurityElement.Escape(logobj.PageGUID));
//            sb.Append("\"/>");
//            return (sb.ToString());
//        }

//        public override void Write(object o)
//        {
//            AddToCollection((LogObject)o, "", "");
//        }

//        public override void Write(object o, string category)
//        {
//            AddToCollection((LogObject)o, category, "");
//        }

//        public override void WriteLine(object o)
//        {
//            AddToCollection((LogObject)o, "", "\n");
//        }

//        public override void WriteLine(object o, string category)
//        {
//            AddToCollection((LogObject)o, category, "\n");
//        }

//        public override void Flush()
//        {
//            FlushData();
//        }

//        #region RollOver
//        private string generateFilename()
//        {

//            CurrentDate = System.DateTime.Today;
//            return Path.Combine(Path.GetDirectoryName(FileName),
//                    Path.GetFileNameWithoutExtension(FileName) + "-" +
//                    CurrentDate.Day.ToString() + "-" +
//                    CurrentDate.Month.ToString() + "-" +
//                    CurrentDate.Year.ToString() +
//                    "_" + fileCount.ToString() + Path.GetExtension(FileName));
//        }

//        private void checkRollover(bool forcenew)
//        {
//            // If the date has changed, close the current stream and create a new file for today's date
//            if (CurrentDate.CompareTo(System.DateTime.Today) != 0 || forcenew)
//            {
//                lock (locker)
//                {
//                    try
//                    {
//                        if (TraceWriter != null)
//                            TraceWriter.Close();
//                        TraceWriter = new StreamWriter(generateFilename(), true);
//                    }
//                    catch { }
//                }
//            }
//        }


//        public override void Write(string value)
//        {
//            checkRollover(false);
//            TraceWriter.Write(value);
//        }

//        public override void WriteLine(string value)
//        {
//            checkRollover(false);
//            if (TraceWriter == null) checkRollover(true);
//            else TraceWriter.WriteLine(value);
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                TraceWriter.Close();
//            }
//        }


//        #endregion

//    }

//}
