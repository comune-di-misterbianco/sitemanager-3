﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Diagnostics;
//using System.Web;
//using System.Configuration;
//using System.Collections.Specialized;
//using System.Net.Mail;
//using System.Security.Cryptography;


//namespace NetCms.Diagnostics.DotNet
//{
//    public static class DotNetDiagnostics
//    {
//        public static void TraceLog(TraceLevel messageLevel, int jumpFrameN, string message, string component, string userID, string username, string errorcode, Exception ex = null)
//        {
//            SAXSwitch traceSwitch = new SAXSwitch("SAXTraceSwitch", "SAXTraceSwitch");
//            SAXSwitch traceEmailSwitch = new SAXSwitch("SAXEmailSwitch", "SAXEmailSwitch");
//            //int frameN = 1 + jumpFrameN;

//            DiagnosticUserData userdata = new DiagnosticUserData(userID, username);

//            if (traceSwitch.Level >= messageLevel)
//            {
//                LogObject _logObj = new LogObject();

//                _logObj.datetime = DateTime.Now;
//                _logObj.level = messageLevel;
//                _logObj.errorCode = errorcode;
//                if (HttpContext.Current != null)
//                {
//                    _logObj.URL = HttpContext.Current.Request.Url.AbsoluteUri;
//                    _logObj.QueryString = HttpContext.Current.Request.QueryString.ToString();
//                }
//                _logObj.Message = NetCms.Diagnostics.Diagnostics.FilterMessage(message);


//                try
//                {
//                    StackTrace st = null;
//                    StackFrame sf = null;
//                    st = new StackTrace(true);
//                    sf = st.GetFrame(jumpFrameN);
//                    _logObj.File = sf.GetFileName();
//                    _logObj.Class = sf.GetMethod().DeclaringType.Name;
//                    _logObj.Component = (component != null && component.Length == 0) ? sf.GetMethod().DeclaringType.Assembly.GetName().Name.ToString() : component;
//                    _logObj.StackTrace = (messageLevel == TraceLevel.Fatal || messageLevel == TraceLevel.Error) ? st.ToString() : sf.ToString();
//                }
//                catch
//                {
//                    _logObj.File = "Impossibile ottenere la Stack Trace";
//                    _logObj.Class = "Impossibile ottenere la Stack Trace";
//                    _logObj.Component = "Impossibile ottenere la Stack Trace";
//                    _logObj.StackTrace = "Impossibile ottenere la Stack Trace";
//                }

//                _logObj.UserIP = HttpContext.Current.Request.UserHostAddress;
//                _logObj.UserID = userdata.UserID == null ? "" : userdata.UserID;
//                _logObj.UserName = userdata.UserName == null ? "" : userdata.UserName;
//                _logObj.PageGUID = NetCms.Diagnostics.Diagnostics.GetPageGUID();

//                // Log dati nell'head e campi inviati nel form
//                if (HttpContext.Current.Request.Headers != null)
//                    _logObj.AllKeys = HttpContext.Current.Server.UrlDecode(HttpContext.Current.Request.Headers.ToString());
//                if (HttpContext.Current.Request.Form != null)
//                {
//                    _logObj.AllForms = NetCms.Diagnostics.Diagnostics.FilterForm(HttpContext.Current.Request.Form);
//                }
//                Trace.WriteLine(_logObj);
//                if (traceEmailSwitch.Level >= messageLevel)
//                    try
//                    {
//                        NetCms.Diagnostics.Diagnostics.sendEmail(_logObj);
//                    }
//                    catch { }
//                if (messageLevel != TraceLevel.Info && messageLevel != TraceLevel.Verbose)
//                    NetCms.Diagnostics.Diagnostics.ExceptionsForThisPage.Add(_logObj);

//            }
//        }

//        public static void TraceException(Exception ex, string userID, string username)
//        {
//            DiagnosticUserData userdata = new DiagnosticUserData(userID, username);

//            SAXSwitch traceSwitch = new SAXSwitch("SAXTraceSwitch", "SAXTraceSwitch");
//            SAXSwitch traceEmailSwitch = new SAXSwitch("SAXEmailSwitch", "SAXEmailSwitch");
//            //int frameN = 1 + jumpFrameN;

//            var messageLevel = TraceLevel.Error;
//            if (traceSwitch.Level >= messageLevel)
//            {
//                LogObject _logObj = new LogObject();

//                _logObj.datetime = DateTime.Now;
//                _logObj.level = messageLevel;
//                _logObj.errorCode = "";
//                _logObj.URL = HttpContext.Current.Request.Url.AbsoluteUri;
//                _logObj.QueryString = HttpContext.Current.Request.QueryString.ToString();

//                _logObj.Message = NetCms.Diagnostics.Diagnostics.FilterMessage(ex.Message);
//                _logObj.UserIP = HttpContext.Current.Request.UserHostAddress;
//                _logObj.UserID = userdata.UserID == null ? "" : userdata.UserID;
//                _logObj.UserName = userdata.UserName == null ? "" : userdata.UserName;
//                _logObj.PageGUID = NetCms.Diagnostics.Diagnostics.GetPageGUID();

//                try
//                {
//                    StackTrace st = null;
//                    StackFrame sf = null;
//                    st = new StackTrace(true);
//                    sf = st.GetFrame(0);
//                    _logObj.File = sf.GetFileName();
//                    _logObj.Class = sf.GetMethod().DeclaringType.Name;
//                    _logObj.Component = sf.GetMethod().DeclaringType.Assembly.GetName().Name.ToString();
//                    _logObj.StackTrace = (messageLevel == TraceLevel.Fatal || messageLevel == TraceLevel.Error) ? st.ToString() : sf.ToString();
//                }
//                catch
//                {
//                    _logObj.File = "Impossibile ottenere la Stack Trace";
//                    _logObj.Class = "Impossibile ottenere la Stack Trace";
//                    _logObj.Component = "Impossibile ottenere la Stack Trace";
//                    _logObj.StackTrace = "Impossibile ottenere la Stack Trace";
//                }

//                // Log dati nell'head e campi inviati nel form
//                if (HttpContext.Current.Request.Headers != null)
//                    _logObj.AllKeys = HttpContext.Current.Server.UrlDecode(HttpContext.Current.Request.Headers.ToString());
//                if (HttpContext.Current.Request.Form != null)
//                {
//                    _logObj.AllForms = NetCms.Diagnostics.Diagnostics.FilterForm(HttpContext.Current.Request.Form);
//                }

//                Trace.WriteLine(_logObj);
//                if (traceEmailSwitch.Level >= messageLevel)
//                    try
//                    {
//                        NetCms.Diagnostics.Diagnostics.sendEmail(_logObj);
//                    }
//                    catch { }
//                if (messageLevel != TraceLevel.Info && messageLevel != TraceLevel.Verbose)
//                    NetCms.Diagnostics.Diagnostics.ExceptionsForThisPage.Add(_logObj);
//            }
//        }

//        public static void TraceUnhandledError(StackFrame sf, String message, String userID, String username)
//        {
//            SAXSwitch traceSwitch = new SAXSwitch("SAXTraceSwitch", "SAXTraceSwitch");
//            SAXSwitch traceEmailSwitch = new SAXSwitch("SAXEmailSwitch", "SAXEmailSwitch");
//            DiagnosticUserData userdata = new DiagnosticUserData(userID, username);

//            if (traceSwitch.Level >= TraceLevel.Error)
//            {
//                LogObject _logObj = new LogObject();
//                _logObj.datetime = DateTime.Now;
//                _logObj.level = TraceLevel.Error;
//                _logObj.errorCode = "";
//                _logObj.URL = HttpContext.Current.Request.Url.AbsoluteUri;
//                _logObj.QueryString = HttpContext.Current.Request.QueryString.ToString();
//                _logObj.File = sf.GetFileName();
//                _logObj.Class = sf.GetMethod().DeclaringType.Name;
//                _logObj.Component = sf.GetMethod().DeclaringType.Assembly.GetName().Name.ToString();
//                _logObj.Message = NetCms.Diagnostics.Diagnostics.FilterMessage(message);
//                _logObj.StackTrace = sf.ToString();
//                _logObj.PageGUID = NetCms.Diagnostics.Diagnostics.GetPageGUID();
//                _logObj.UserIP = HttpContext.Current.Request.UserHostAddress;
//                _logObj.UserID = userdata.UserID == null ? "" : userdata.UserID;
//                _logObj.UserName = userdata.UserName == null ? "" : userdata.UserName;

//                // Log dati nell'head e campi inviati nel form
//                if (HttpContext.Current.Request.Headers != null)
//                    _logObj.AllKeys = HttpContext.Current.Server.UrlDecode(HttpContext.Current.Request.Headers.ToString());
//                if (HttpContext.Current.Request.Form != null)
//                {
//                    _logObj.AllForms = NetCms.Diagnostics.Diagnostics.FilterForm(HttpContext.Current.Request.Form);
//                }

//                Trace.WriteLine(_logObj);
//                if (traceEmailSwitch.Level >= TraceLevel.Error)
//                    try
//                    {
//                        NetCms.Diagnostics.Diagnostics.sendEmail(_logObj);
//                    }
//                    catch { }
//                NetCms.Diagnostics.Diagnostics.ExceptionsForThisPage.Add(_logObj);
//            }
//        }
//    }
//}

