﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Diagnostics;
//using System.Web;
//using System.Configuration;
//using System.Collections.Specialized;
//using System.Data;
//using System.Data.SqlClient;
//using MySql.Data.MySqlClient;
//using MySql.Data;


//namespace NetCms.Diagnostics
//{

//    class DBWriterTraceListener : TraceListener
//    {
//        private const string COLUMN_SEPARATOR = "|";
//        private string _strConnectionString;
//        private int _iMaximumRequests;
//        private StringCollection _objCollection;

//        public DBWriterTraceListener()
//        {
//            InitializeListener();
//        }

//        public DBWriterTraceListener(string _strListenerName)
//            : base(_strListenerName)
//        {
//            InitializeListener();
//        }

//        private void InitializeListener()
//        {
//            _strConnectionString = ConfigurationManager.AppSettings["TraceConnectionString"];
//            _iMaximumRequests = Convert.ToInt32(ConfigurationSettings.AppSettings["TraceDBMaximumRequests"]);
//            _objCollection = new StringCollection();
//        }

//        private void FlushtoDB()
//        {
//            MySqlConnection objConnection = new MySqlConnection(_strConnectionString);
//            MySqlCommand objCommand = new MySqlCommand();
//            try
//            {
//                objCommand.Connection = objConnection;
//                objCommand.CommandText = "sp_Diagnostics_Log_Insert";
//                objCommand.CommandType = CommandType.StoredProcedure;
//                objConnection.Open();

//                foreach (string _strError in _objCollection)
//                {
//                    CreateParameters(objCommand, _strError);
//                    objCommand.ExecuteNonQuery();
//                }
//                _objCollection.Clear();
//            }
//            catch (Exception ex)
//            {
//                string message = ex.Message;
//            }
//            finally
//            {
//                if (objConnection != null)
//                {
//                    if (objConnection.State == ConnectionState.Open)
//                        objConnection.Close();
//                }
//                objConnection = null;
//                objCommand = null;
//            }
//        }

//        /*private void AddToCollection(string _strTraceDateTime,
//            string _strTraceCategory,
//            string _strTraceDescription,
//            string _strStackTrace,
//            string _strDetailedErrorDescription)
//        {
//            string strError = _strTraceDateTime + COLUMN_SEPARATOR +
//                _strTraceCategory + COLUMN_SEPARATOR +
//                _strTraceDescription + COLUMN_SEPARATOR +
//                _strStackTrace + COLUMN_SEPARATOR +
//                _strDetailedErrorDescription;
//            _objCollection.Add(strError);
//            if (_objCollection.Count == _iMaximumRequests)
//            {
//                FlushtoDB();
//            }
//        }*/

//        private void AddToCollection(string message, string category)
//        {
//            StackTrace st = new StackTrace(true);
//            StackFrame sf = st.GetFrame(1);

//            string strLog = "" +
//                DateTime.Now.ToString() + COLUMN_SEPARATOR + //datetime               
//                HttpContext.Current.Request.Url.AbsoluteUri + COLUMN_SEPARATOR + //level
//                "" + COLUMN_SEPARATOR + //error code
//                HttpContext.Current.Request.Form.ToString() + COLUMN_SEPARATOR + //URL
//                HttpContext.Current.Request.QueryString.ToString() + COLUMN_SEPARATOR + //QueryString
//                sf.GetFileName() + COLUMN_SEPARATOR + //File
//                sf.GetMethod().DeclaringType.Name + COLUMN_SEPARATOR + //class                
//                message + COLUMN_SEPARATOR +//message
//                sf.ToString() + COLUMN_SEPARATOR +//StackTrace
//                HttpContext.Current.Request.UserHostAddress + COLUMN_SEPARATOR +//UserIP
//                "" + COLUMN_SEPARATOR + //UserID
//                "";//UserName
//            _objCollection.Add(strLog);
//            if (_objCollection.Count == _iMaximumRequests)
//            {
//                FlushtoDB();
//            }
//        }

//        private void AddToCollection(LogObject logObj, string category)
//        {
//            string strLog = "" +
//                    logObj.LogID + COLUMN_SEPARATOR +
//                    logObj.datetime.ToString() + COLUMN_SEPARATOR +
//                    logObj.level + COLUMN_SEPARATOR +
//                    logObj.errorCode + COLUMN_SEPARATOR +
//                    logObj.URL + COLUMN_SEPARATOR +
//                    logObj.QueryString + COLUMN_SEPARATOR +
//                    logObj.File + COLUMN_SEPARATOR +
//                //logObj.Line + COLUMN_SEPARATOR +
//                    logObj.Class + COLUMN_SEPARATOR +
//                //logObj.Method + COLUMN_SEPARATOR +
//                    logObj.Message + COLUMN_SEPARATOR +
//                    logObj.StackTrace + COLUMN_SEPARATOR +
//                    logObj.UserIP + COLUMN_SEPARATOR +
//                    logObj.UserID + COLUMN_SEPARATOR;
//            //logObj.UserName;
//            _objCollection.Add(strLog);
//            if (_objCollection.Count >= _iMaximumRequests)
//            {
//                FlushtoDB();
//            }
//        }

//        private void CreateParameters(MySqlCommand _objCommand, string _strError)
//        {
//            if ((_objCommand != null) && (!_strError.Equals("")))
//            {
//                string[] strColumns;
//                MySqlParameterCollection objParameters = _objCommand.Parameters;

//                strColumns = _strError.Split(COLUMN_SEPARATOR.ToCharArray());
//                objParameters.Clear();
//                objParameters.Add(new MySqlParameter("in_logID", MySqlDbType.VarChar));
//                objParameters.Add(new MySqlParameter("in_datetime", MySqlDbType.DateTime));
//                objParameters.Add(new MySqlParameter("in_level", MySqlDbType.VarChar));
//                objParameters.Add(new MySqlParameter("in_errorcode", MySqlDbType.VarChar));
//                objParameters.Add(new MySqlParameter("in_URL", MySqlDbType.VarChar));
//                //objParameters.Add(new MySqlParameter("in_form", MySqlDbType.VarChar, 2048));
//                objParameters.Add(new MySqlParameter("in_querystring", MySqlDbType.VarChar));
//                objParameters.Add(new MySqlParameter("in_file", MySqlDbType.VarChar));
//                //objParameters.Add(new MySqlParameter("in_line", MySqlDbType.VarChar));
//                objParameters.Add(new MySqlParameter("in_class", MySqlDbType.VarChar));
//                //objParameters.Add(new MySqlParameter("in_method", MySqlDbType.VarChar));
//                objParameters.Add(new MySqlParameter("in_message", MySqlDbType.VarChar));
//                objParameters.Add(new MySqlParameter("in_stacktrace", MySqlDbType.Text));
//                objParameters.Add(new MySqlParameter("in_userIP", MySqlDbType.VarChar));
//                objParameters.Add(new MySqlParameter("in_userID", MySqlDbType.VarChar));
//                //objParameters.Add(new MySqlParameter("in_username", MySqlDbType.VarChar));
//                int iCount = strColumns.GetLength(0);
//                for (int i = 0; i < iCount; i++)
//                {
//                    objParameters[i].IsNullable = true;
//                    objParameters[i].Direction = ParameterDirection.Input;
//                    objParameters[i].Value = strColumns.GetValue(i).ToString().Trim();
//                }
//            }
//        }


//        public override void Write(object o)
//        {
//            AddToCollection((LogObject)o, "");
//        }

//        public override void Write(object o, string category)
//        {
//            AddToCollection((LogObject)o, category);
//        }

//        public override void WriteLine(object o)
//        {
//            AddToCollection((LogObject)o, "");
//        }


//        public override void WriteLine(object o, string category)
//        {
//            AddToCollection((LogObject)o, category);
//        }


//        public override void Write(string message)
//        {
//            AddToCollection(message, "");
//        }

//        public override void Write(string message, string category)
//        {
//            AddToCollection(message, category);
//        }


//        public override void WriteLine(string message)
//        {
//            Write(message);
//        }

//        public override void WriteLine(string message, string category)
//        {
//            Write(message, category);
//        }

//        /*public override void Fail(string message)
//        {
//            StackTrace objTrace = new StackTrace(true);
//            AddToCollection(DateTime.Now.ToString(), "Fail", message, objTrace.ToString(), "");
//        }

//        public override void Fail(string message, string detailMessage)
//        {
//            StackTrace objTrace = new StackTrace(true);
//            AddToCollection(DateTime.Now.ToString(), "Fail", message, objTrace.ToString(), detailMessage);
//        }*/

//        public override void Close()
//        {
//            FlushtoDB();
//        }

//        public override void Flush()
//        {
//            FlushtoDB();
//        }

//    }

//}
