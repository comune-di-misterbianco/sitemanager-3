using System;
using System.IO;
using System.Web;
using System.Xml;
using System.Reflection;


namespace G2Core.Languages
{
    /// <summary>
    /// classe statica per memorizzare una collezione di Label 
    /// </summary>
    public static class LabelsCacher
    {
        private static G2Core.Languages.LabelsCollection labels;
        /// <summary>
        /// Collezione  <see cref="G2Core.Languages.LabelsCollection"/>
        /// </summary>
        public static G2Core.Languages.LabelsCollection Labels
        {
            get
            {
                if (labels == null)
                    labels = new G2Core.Languages.LabelsCollection();

                return labels;
            }
        }

        private static XmlNode RootNodeObj;
        /// <summary>
        /// Ottiene il nodo origine,System.XML.XmlNode, ad un documento XML di default labels_it.xml  
        /// </summary>
        public static XmlNode RootNode
        {
            get
            {
                if (RootNodeObj == null)
                {
                    Assembly dll = Assembly.GetExecutingAssembly();
                    XmlDocument xml = new XmlDocument();
                    xml.Load(new StreamReader(dll.GetManifestResourceStream("G2Core.labels_it.xml")));
                    RootNodeObj = xml;
                }
                return RootNodeObj;
            }
        }
    }

}