using System;
using System.Web;
using System.Xml;


namespace G2Core.Languages
{
    /// <summary>
    /// Permette di accedere ad una lista di stringhe da un file XML.    
    /// </summary>
    /// <remarks>
    /// La classe Labels � una classe base che permette il caricamento di un file XML specificando 
    /// l'url del file o attraverso un riferimento ad un oggetto <see cref="System.Xml.XmlNode"/> nel documento XML.
    /// La classe di pratico utilizzo � <see cref="G2Core.AdvancedXhtmlForms.AxfLabels"/> che estende le propriet� di q
    /// questa classe base 
    /// </remarks>  
    public class Labels
    {
        protected const bool LabelsDebugMode = true;

        private G2Core.Common.StringCollection _LabelsCache;
        /// <summary>
        /// Collezione <see cref="G2Core.Common.StringCollection"></see> per memorizzare la lista di 
        /// stringhe caricate dal file XML.
        /// </summary>
        public G2Core.Common.StringCollection LabelsCache
        {
            get
            {
                if (_LabelsCache == null)
                    _LabelsCache = new G2Core.Common.StringCollection();

                return _LabelsCache;
            }
        }

        private XmlNode xmlLabelsRoot;
        /// <summary>
        /// Ottiene il nodo System.Xml.XmlNode impostato come nodo radice del documento XML.
        /// /// </summary>        
        public XmlNode XmlLabelsRoot
        {
            get
            {
                return xmlLabelsRoot;
            }
        }

        /// <summary>
        /// Inizializza un'istanza della classe Labels.Questo costruttore ipotizza che il documento
        /// XML abbia un filename di default label.xml.
        /// </summary> 
        /// <param name="absoluteWebRoot">l'indirizzo virtuale del web server in cui si trova il file xml a cui
        /// si acceder� come absoluteWebRoot/filename.xml        
        /// </param>
        public Labels(string absoluteWebRoot)
            : this(absoluteWebRoot, "labels.xml")
        {
        }

        /// <summary>
        /// Inizializza un'istanza della classe Labels specificando il percorso virtuale del web server ed il 
        /// nome del file XML da caricare
        /// </summary>
        /// <param name="absoluteWebRoot">percorso virtuale del web server</param>
        /// <param name="xmlFileName">nome del file da caricare dal web server</param>
        /// <remarks>
        /// L'url del file XML da caricare sar� composto da absoluteWebRoot/xmlFileName.
        /// </remarks>
        public Labels(string absoluteWebRoot, string xmlFileName)
        {
            XmlDocument oXmlDoc = new XmlDocument();
            try
            {
                string xmlFilePath = HttpContext.Current.Server.MapPath(absoluteWebRoot + "/" + xmlFileName);
                oXmlDoc.Load(xmlFilePath);
            }
            catch (Exception ex)
            {
                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, "Non � stato possibile caricare il file '" + xmlFileName + "' delle labels","","","69");
                //xmlConfigs = null;
            }
            xmlLabelsRoot = oXmlDoc.DocumentElement;
        }


        /// <summary>
        /// Inizializza un'istanza della classe Labels specificando un riferimento System.Xml.XmlNode al
        /// nodo radice di un documento XML.
        /// </summary>
        /// <param name="rootNode">Il nodo radice del documento XML da considerare per recuperare le stringhe.</param>
        public Labels(XmlNode rootNode)
        {
            xmlLabelsRoot = rootNode;
        }

        /// <summary>
        /// Ripulisce il valore di una label contenuto in una sezione CDATA di un documento XML. 
        /// </summary>
        /// <param name="label">stringa da formattare</param>
        /// <returns>la stringa formattata.</returns>
        protected string FormatLabel(string label)
        {
            string outLabel = label.Replace("<![CDATA[", "").Replace("]]>", "");
            return outLabel;
        }
    }

}