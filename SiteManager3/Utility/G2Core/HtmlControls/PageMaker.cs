using System;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using G2Core.XhtmlControls;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace G2Core.Common
{
    public partial class PageMaker
    {
        public string Key
        {
            get 
            {
                if (_Key == null)
                {
                    _Key = "page";
                }
                return _Key; 
            }
            set { _Key = value; }
        }
        private string _Key;

        private int _Range = 2;
        public int Range
        {
            get { return _Range; }
            set { _Range = value; }
        }

        public string ID
        {
            get
            {
                if (_ID == null)
                    _ID = "PageNavigation";
                return _ID;
            }
            set
            { 
                _ID = value; 
            }
        }
        private string _ID;

        private int _RPP;
        public int RPP
        {
            get { return _RPP; }
            set { _RPP = value; }
        }
        
        public VariabileType Type
        {
            get
            {                
                return _Type;
            }
        }
        private VariabileType _Type;
        
        private int _Page = -1;
        public int Page
        {
            get
            {
                if (_Page == -1)
                {
                    string page;
                    if(Type == VariabileType.QueryString)
                        page = Request.QueryString[Key];
                    else
                        page = Request.Form[Key];
                    if (page != null && page.Length > 0 && int.TryParse(page, out _Page)) ;
                    else _Page = 1;
                }
                if (_Page == 0) _Page++;
                return _Page;
            }
        }
        private HttpRequest _Request;
        private HttpRequest Request
        {
            get
            {
                if (_Request == null) 
                    _Request = HttpContext.Current.Request;
                return _Request;
            }
            set { _Request = value; }
        }

        public PageMaker(int page):this(page, VariabileType.QueryString) { _Page = page; }
        public PageMaker(int page, VariabileType type) { _Page = page; _Type = type; }
        public PageMaker() : this("page", VariabileType.QueryString) { }
        public PageMaker(VariabileType VarType) : this( "page", VarType) { }
        public PageMaker(string key) : this(key, VariabileType.QueryString) { }
        public PageMaker(string key, VariabileType VarType)
        {
            _Key = key;
            _RPP = 10;
            _Type = VarType;
        }
        
        public PageMaker(HttpRequest request) : this(request, "page", VariabileType.QueryString) {}
        public PageMaker(HttpRequest request,VariabileType VarType): this(request, "page",VarType){}
        public PageMaker(HttpRequest request, string key) : this(request, key, VariabileType.QueryString) { }
        public PageMaker(HttpRequest request, string key, VariabileType VarType)
        {
            _Key = key;
            _RPP = 10;
            Request = request;
            _Type = VarType;
        }

        public DataRow[] MakeTablePagination(DataTable table)
        {
            return MakeTablePagination(table.Select());
        }

        public DataRow[] MakeTablePagination(DataRow[] Rows)
        {

            /*double pagecounttmp = (double)Rows.Length / (double)RPP;

            string[] pagecountstr = pagecounttmp.ToString().Split(',');
            
            int pagecount = int.Parse(pagecountstr[0]);*/

            double mod = (double)Rows.Length % (double)RPP;
            int pagecount = Rows.Length / RPP;
            if(mod>0)
            //if (pagecountstr.Length > 1)
                pagecount++;
            //)pagecounttmp;
            if (pagecount < 1)
                pagecount = 1;
            if (Page > pagecount)
                _Page = 1;

            int i = 0;
            int pageoffset = RPP * (Page - 1);
            int count = Rows.Length - pageoffset > RPP ? RPP : Rows.Length - pageoffset;
            DataRow[] OutputRows = new DataRow[count];
            if (Page <= pagecount)
            {
                

                for (i = 0; i < count; i++)
                {
                    OutputRows[i] = Rows[i + pageoffset];
                }
            }
            return OutputRows;
        }
        public HtmlGenericControl PageNavigation(string href,DataTable table) 
        {
            return PageNavigation(href, table.Rows.Count);
        }
        public HtmlGenericControl PageNavigation(string href, DataRow[] Rows)
        { return PageNavigation(href, Rows.Length); }
        public HtmlGenericControl PageNavigation(string querystring, int RowsCount)
        {
            string href = "<a href=\"" + querystring + "";
            #region Calcoli Aritmetici sulla paginazione
            /*
            double pagecounttmp = (double)RowsCount / (double)RPP;
            string[] pagecountstr = pagecounttmp.ToString().Split(',');
            int pagecount = int.Parse(pagecountstr[0]);*/

            double mod = (double)RowsCount % (double)RPP;
            int pagecount = RowsCount / RPP;
            if (mod > 0)
            //if(pagecountstr.Length>1)
                pagecount++;
                //)pagecounttmp;
            if (pagecount < 1)
                pagecount = 1;
            int i = 0;
            int pageoffset = RPP * (Page - 1);
            int count = RowsCount - pageoffset > RPP ? RPP : RowsCount - pageoffset;
            
            #endregion

            int TopRange = Page + Range <= pagecount ? Page + Range : pagecount==0?1:pagecount;
            
            int BottomRange = Page - Range > 0 ? Page - Range : 1;

            G2Core.XhtmlControls.Div pages = new G2Core.XhtmlControls.Div();
            pages.ID = ID;
            pages.Attributes["class"] = "PageNavigation";
            pages.InnerHtml = "<span>Pagina " + Page + " di "+pagecount;
            if (pagecount <= 1)
                pages.InnerHtml += "</span>";
            else
            {
                pages.InnerHtml += ":</span> ";
                
                if (Page > 1)
                    pages.InnerHtml += CreateNavigationButton(querystring, "prima pagina", 1, "prima pagina", "firstpage") + "<span class=\"sep\"> | </span>";

                if (Page != BottomRange)
                    pages.InnerHtml += CreateNavigationButton(querystring, "pagina precedente", (Page - 1), "pagina precedente", "previouspage") + "<span class=\"sep\"> | </span>";
                

                for (i = BottomRange; i <= TopRange; i++)
                {
                    string classe = "numbers";
                    string tag ;
 
                    if (i == Page)
                    {
                        classe += " selected";
                        tag ="span";
                    }
                    else
                        tag = "a";

                    pages.InnerHtml += CreateNavigationButton(querystring, i.ToString(), i, "Vai a pagina" + i,classe,tag);
                }
                if (Page != TopRange)
                    pages.InnerHtml += "<span class=\"sep\"> | </span>" +CreateNavigationButton(querystring, "pagina successiva", (Page + 1), "Vai alla pagina successiva", "nextpage") + "<span class=\"sep\"> | </span>";
                    
                if (Page < pagecount)
                    pages.InnerHtml += CreateNavigationButton(querystring, "ultima pagina", pagecount, "Vai all'ultima pagina", "lastpage") + "<span class=\"sep\"> | </span>";
            }
            pages.InnerHtml += "<div class=\"clear\"></div>";
            return pages;
        }


        public string PageNavigationString(string querystring, int RowsCount)
        {
            string href = "<a href=\"" + querystring + "";
            #region Calcoli Aritmetici sulla paginazione

            double pagecounttmp = (double)RowsCount / (double)RPP;

            string[] pagecountstr = pagecounttmp.ToString().Split(',');


            int pagecount = int.Parse(pagecountstr[0]);
            if (pagecountstr.Length > 1)
                pagecount++;
            //)pagecounttmp;
            if (pagecount < 1)
                pagecount = 1;
            int i = 0;
            int pageoffset = RPP * (Page - 1);
            int count = RowsCount - pageoffset > RPP ? RPP : RowsCount - pageoffset;

            #endregion

            int TopRange = Page + Range <= pagecount ? Page + Range : pagecount == 0 ? 1 : pagecount;

            int BottomRange = Page - Range > 0 ? Page - Range : 1;

            G2Core.XhtmlControls.Div pages = new G2Core.XhtmlControls.Div();
            pages.InnerHtml = "<span>Pagina " + Page + " di " + pagecount;
            if (pagecount <= 1)
                pages.InnerHtml += "</span>";
            else
            {
                pages.InnerHtml += ":</span> ";

                if (Page > 1)
                    pages.InnerHtml += CreateNavigationButton(querystring, "Prima Pagina", 1, "Vai alla prima pagina");

                if (Page != BottomRange)
                    pages.InnerHtml += CreateNavigationButton(querystring, "&lt;", (Page - 1), "Vai alla pagina precedente");


                for (i = BottomRange; i <= TopRange; i++)
                {
                    string classe = "numbers";
                    string tag;

                    if (i == Page)
                    {
                        classe += " selected";
                        tag = "span";
                    }
                    else
                        tag = "a";

                    pages.InnerHtml += CreateNavigationButton(querystring, i.ToString(), i, "Vai a pagina " + i, classe, tag);
                }
                if (Page != TopRange)
                    pages.InnerHtml += CreateNavigationButton(querystring, "&gt;", (Page + 1), "Vai alla pagina successiva");

                if (Page < pagecount)
                    pages.InnerHtml += CreateNavigationButton(querystring, "Ultima Pagina", pagecount, "Vai all'ultima pagina");
            }
            pages.InnerHtml += "<div class=\"clear\"></div>";
            pages.InnerHtml = "<div class=\"PageNavigator\">" + pages.InnerHtml + "</div>";
            
            return pages.InnerHtml;
        }

        public enum VariabileType
        {
            QueryString,
            Form
        }

        private string CreateNavigationButton(string querystring, string label, int value)
        {
            return CreateNavigationButton(querystring, label, value, label);
        }
        private string CreateNavigationButton(string querystring, string label, int value, string title)
        {
            return CreateNavigationButton(querystring, label, value, title, "", "a");
        }
        private string CreateNavigationButton(string querystring, string label, int value, string title, string classe)
        {
            return CreateNavigationButton(querystring, label, value, title, classe, "a");
        }
        private string CreateNavigationButton(string querystring, string label, int value, string title, string classe, string tag)
        {
            string button = "<" + tag;
            if (tag == "a")
            {
                button += " href=\"" + querystring + "";
                if (querystring.Contains("{0}"))
                {
                    button = button.Replace("{0}", value.ToString()) + "\"";
                }
                else
                {
                    if (Type == VariabileType.QueryString)
                    {
                        if (querystring.Contains("?"))
                        {
                            if (querystring.Contains("="))
                                button += "&amp;";
                        }
                        else
                            button += "?";
                        button += this.Key + "=" + (value) + "\"";
                    }
                    else
                        button = button.Replace("{0}", value.ToString()) + "\"";
                }

                button += " title=\"" + title + "\"";
                
            }
            if (classe.Length > 0)
                button += " class=\"" + classe + "\"";

            button += ">" + label + "</" + tag + ">";
            
            /*if (tag == "a")
                button = "<span>" + button + "</span>";*/

            return button;
        }
    }

}