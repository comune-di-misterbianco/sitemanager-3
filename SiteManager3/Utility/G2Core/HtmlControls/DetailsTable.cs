using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


namespace G2Core.UI
{
    /// <summary>
    /// G2Core.UI DetailsSheet
    /// </summary>
    public class DetailsSheet : WebControl
    {
        /// <summary>
        /// DetailsSheet Title
        /// </summary>
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
        private string _Title;

        /// <summary>
        /// DetailsSheet 
        /// </summary>
        public string Description
        {
            get { return _Description; }
            private set { _Description = value; }
        }
        private string _Description;

        /// <summary>
        /// Separate Column Option Enumerator
        /// </summary>
        public enum SeparateColumnsOptions { No, OnlyFirst, All }
       
        /// <summary>
        /// Separate Column Option 
        /// </summary>
        public SeparateColumnsOptions SeparateColumns
        {
            get { return _SeparateColumns; }
            set { _SeparateColumns = value; }
        }
        private SeparateColumnsOptions _SeparateColumns;
                    
        /// <summary>
        /// Used on Frontend
        /// </summary>
        public bool UsedOnFrontend
        {
            get { return _UsedOnFrontend; }
            set { _UsedOnFrontend = value; }
        }
        private bool _UsedOnFrontend = false;

        /// <summary>
        ///  DetailsSheet Constuctor
        /// </summary>
        /// <param name="title"></param>
        /// <param name="description"></param>
        public DetailsSheet(string title, string description) : base(HtmlTextWriterTag.Fieldset)
        {
            this.Title = title;
            this.Description = description;
            this.SeparateColumns = SeparateColumnsOptions.OnlyFirst;
        }
                  
        public bool Alternate
        {
            get { return _Alternate; }
            private set { _Alternate = value; }
        }
        private bool _Alternate = true;
        
        public Table InnerTable
        {
            get
            {
                if (_InnerTable == null)
                {
                    _InnerTable = new Table();
                    if(this.ID!=null && this.ID != string.Empty)
                        _InnerTable.ID = this.ID + "_Table";
                    _InnerTable.CellPadding = 0;
                    _InnerTable.CellSpacing = 0;

                    _InnerTable.CssClass = UsedOnFrontend ? "table DetailsContainer " : "";
                    _InnerTable.Rows.Add(HeaderRow);
                }
                return _InnerTable;
            }
        }
        private Table _InnerTable;

        public enum Colors { Default, Green, Red, Yellow, Blue }

        private TableHeaderRow HeaderRow
        {
            get
            {
                if (_HeaderRow == null)
                    _HeaderRow = new TableHeaderRow();
                return _HeaderRow;
            }
        }
        private TableHeaderRow _HeaderRow;

        public void AddColumn(string Caption, Colors color, int columnSpan)
        {
            TableHeaderCell cell = new TableHeaderCell();
            cell.Text = Caption;
            cell.CssClass = "fill_" + color.ToString().ToLower();
            if (columnSpan > 1) cell.ColumnSpan = columnSpan;
            if (this.HeaderRow.Cells.Count == 0 && this.SeparateColumns == SeparateColumnsOptions.OnlyFirst ||(this.SeparateColumns == SeparateColumnsOptions.All))
                cell.CssClass += " first";
            this.HeaderRow.Cells.Add(cell);
        }
        public void AddColumn(string Caption, Colors color)
        {
            AddColumn(Caption, color, 1);
        }
        public void AddTitleTextRow(string title, string text)
        {
            AddTitleTextRow(title, text, Colors.Default);
        }
        public void AddTitleTextRow(string title, string text, Colors color)
        {
            TableRow row = new TableRow();

            if (title != null)
            {
                TableCell cell = new TableCell();
                if (title.Length > 0)
                    cell.Text = "<strong>" + title + ":</strong>";
                else
                    cell.Text = "&nbsp;";
                row.Cells.Add(cell);
            }
            if (text != null)
            {
                TableCell cell = new TableCell();
                if (HeaderRow.Cells.Count > 2)
                    cell.ColumnSpan = HeaderRow.Cells.Count - 1;
                if (text.Length > 0)
                    cell.Text = text;
                else
                    cell.Text = "&nbsp;";

                row.Cells.Add(cell);
            }
            AddRow(row, color);
        }
        public void AddRow(TableRow row, Colors color)
        {
            InnerTable.Rows.Add(row);

            int i = 0;
            foreach (TableCell cell in row.Cells)
            {
                if (this.HeaderRow.Cells.Count > i)
                    cell.CssClass = this.HeaderRow.Cells[i].CssClass + " " + cell.CssClass;
                i++;
            }
            row.CssClass = (row.CssClass + (this.Alternate ? " alternate" : "") + " fill_" + color.ToString().ToLower()).Trim();

            Alternate = !Alternate;
        }
        public void AddRow(params string[] cellsText)
        {
            AddRow(Colors.Default,cellsText);
        }
        public void AddRow(Colors color,params string[] cellsText)
        {
            TableRow row = new TableRow();
            int i = 0;
            foreach (string text in cellsText)
            {
                TableCell cell = new TableCell();
                if (text.Length > 0)
                    cell.Text = text;
                else
                    cell.Text = "&nbsp;";
                row.Cells.Add(cell);
                if (++i == cellsText.Length && cellsText.Length < this.HeaderRow.Cells.Count)
                {
                    cell.ColumnSpan = (this.HeaderRow.Cells.Count - cellsText.Length) + 1;
                }
            }
            AddRow(row,color);
        }

        /// <summary>
        /// Enable Elements rendering with separator 
        /// </summary>
        //public bool MultipleElement
        //{
        //    get { return _MultipleElement; }
        //    private set { _MultipleElement = value; }
        //}
        //private bool _MultipleElement = false;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            //if (!MultipleElement)
            //{
                this.CssClass = "DetailsTable " + (UsedOnFrontend ? " col-md-12 " : "");

                HtmlGenericControl legend = new HtmlGenericControl("legend");
                this.Controls.Add(legend);
                legend.InnerHtml = this.Title;

                HtmlGenericControl text = new HtmlGenericControl("div");
                text.Attributes["class"] = "DetailsText";
                this.Controls.Add(text);
                text.InnerHtml = this.Description;

                this.Controls.Add(InnerTable);
            //}
            //else
            //{

            //}
        }
    }
}