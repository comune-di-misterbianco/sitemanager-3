using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace G2Core.XhtmlControls
{
    public class Div : HtmlGenericControl
    {
        public string Class
        {
            get
            {
                return base.Attributes["class"];
            }
            set
            {
                base.Attributes["class"] = value;
            }
        }

        public Div()
            : base("div")
        {
        }

        public Div(string id)
            : base("div")
        {
            base.ID = id;
        }
    }

    public class Par : HtmlGenericControl
    {
        public string Class
        {
            get
            {
                return base.Attributes["class"];
            }
            set
            {
                base.Attributes["class"] = value;
            }
        }


        public Par()
            : base("p")
        {
        }


    }
    public class Span : HtmlGenericControl
    {
        public string CssClass
        {
            get
            {
                return base.Attributes["class"];
            }
            set
            {
                base.Attributes["class"] = value;
            }
        }


        public Span()
            : base("span")
        {
        }


    }

    public class JavaScript : HtmlGenericControl
    {
        public string Class
        {
            get
            {
                return base.Attributes["class"];
            }
            set
            {
                base.Attributes["class"] = value;
            }
        }
        public string ScriptText
        {
            get { return this.InnerHtml; }
            set
            {
                this.InnerHtml = value;
            }
        }
	

        public JavaScript()
            : base("script")
        {
            this.Attributes["type"] = "text/javascript";
        }


    }


    public class Fieldset : HtmlGenericControl
    {
        public string Class
        {
            get
            {
                return base.Attributes["class"];
            }
            set
            {
                base.Attributes["class"] = value;
            }
        }

        private HtmlGenericControl _Legend;
        public HtmlGenericControl Legend
        {
            get
            {
                if (_Legend == null)
                    _Legend = new HtmlGenericControl("legend");
                return _Legend;
            }
        }

        private HtmlGenericControl _Content;
        public HtmlGenericControl Content
        {
            get
            {
                if (_Content == null)
                    _Content = new HtmlGenericControl("div");
                return _Content;
            }
        }
        public string LegendText
        {
            get { return Legend.InnerHtml; }
            set { Legend.InnerHtml = value; }
        }
	

        public Fieldset()
            : base("fieldset")
        {
            base.Controls.Add(Legend);
        }

        public Fieldset(string legendText)
            : base("fieldset")
        {
            base.Controls.Add(Legend);
            LegendText = legendText;
        }

        public Fieldset(string legendText,HtmlGenericControl content)
            : base("fieldset")
        {
            base.Controls.Add(Legend);
            base.Controls.Add(content);
            _Content = content;
            LegendText = legendText;
        }

    }

    public class InputField
    {
        private string _CssClass;
        public string CssClass
        {
            get
            {
                return _CssClass;
            }
            set
            {
                _CssClass = value;
            }
        }

        private string _ID;
        public string ID
        {
            get { return _ID; }
        }

        private string _Value;

        public string Value
        {
            get 
            {
                if (AutoLoadValueAfterPostback && _Value == null && this.RequestVariable.IsValidString)
                {
                    _Value = this.RequestVariable.StringValue;
                }
                return _Value; 
            }
            set { _Value = value; }
        }

        private bool _AutoLoadValueAfterPostback;
        public bool AutoLoadValueAfterPostback
        {
            get { return _AutoLoadValueAfterPostback; }
            set { _AutoLoadValueAfterPostback = value; }
        }


        private G2Core.Common.RequestVariable _RequestVariable;
        public G2Core.Common.RequestVariable RequestVariable
        {
            get 
            {
                if (_RequestVariable == null)
                {
                    _RequestVariable = new G2Core.Common.RequestVariable(this.ID, G2Core.Common.RequestVariable.RequestType.Form_QueryString);
                }
                return _RequestVariable; 
            }
        }
	

        public enum FieldTypes
        {
            Hidden,
            Text,
            Button,
            Submit
        }

        private FieldTypes _FieldType;
        public FieldTypes FieldType
        {
            get { return _FieldType; }
        }


        public InputField(string id)
            :this(id,FieldTypes.Hidden)
        {
        }

        public InputField(string id, FieldTypes fieldType)
        {
            _ID = id;
            _FieldType = fieldType;
            _AutoLoadValueAfterPostback = false;
        }

        public override string ToString()
        {
            string format = "<input type=\"{0}\" name=\"{1}\" id=\"{1}\" {2} {3}/>";
            
            string cssclass = "";
            if (CssClass != null && CssClass.Length > 0)
                cssclass = " class=\"" + CssClass + "\"";

            string value = "";
            if (this.Value != null && this.Value.Length > 0)
                value = " value=\"" + this.Value + "\"";

            return  string.Format(format, this.FieldType.ToString().ToLower(), this.ID, cssclass, value);
        }

        public HtmlGenericControl Control
        {
            get
            {
                HtmlGenericControl span = new HtmlGenericControl("span");
                span.InnerHtml = this.ToString();
                return span;
            }
        }

        public string getSetFormValueJScript(string value)
        {
            return "javascript: setFormValue(" + value + ",'" + this.ID + "',1)";
        }
        public string getSetFormValueJScript(string value, bool submit)
        {
            return "javascript: setFormValue(" + value + ",'" + this.ID + "'," + (submit ? 1 : 0) + ")";
        }
        public string getSetFormValueConfirmJScript(string value, string message)
        {
            return "javascript: setFormValueConfirm(" + value + ",'" + this.ID + "',1,'" + message + "')";
        }
    }



    public class TabbedPanel : Panel
    {

        private HtmlGenericControl Tabs;
        private HtmlGenericControl Panels;        
             
        public void addPanel(string tabheader, Control panelctr, string tabID)
        {
            HtmlGenericControl tab = new HtmlGenericControl("li");            
            HtmlGenericControl a = new HtmlGenericControl("a");
            a.Attributes.Add("href", "#" + tabID);
            a.InnerHtml = tabheader;
            tab.Controls.Add(a);

            HtmlGenericControl panel = new HtmlGenericControl("div");
            panel.Controls.Add(panelctr);
            panel.Attributes.Add("class", "pandiv");

            this.Tabs.Controls.Add(tab);
            this.Panels.Controls.Add(panel); 
        }

        public void clearPanels()
        {
            Tabs.Controls.Clear();
            Panels.Controls.Clear();
        }        
          

        public TabbedPanel()
            : base()
        {
            Tabs = new HtmlGenericControl("ul");
            Tabs.Attributes.Add("class", "tabs");
            Panels = new HtmlGenericControl("div");
            Panels.Attributes.Add("class", "panes");
            this.Controls.Add(Tabs);
            this.Controls.Add(Panels);
        }
        

    }
}