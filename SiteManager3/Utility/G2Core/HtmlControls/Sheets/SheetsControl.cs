﻿using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace G2Core.XhtmlControls.Sheets
{
    public class SheetsControl
    {
        private SheetsCollection _Sheets;
        public SheetsCollection Sheets
        {
            get
            {
                if (_Sheets == null)
                    _Sheets = new SheetsCollection(this);
                return _Sheets;
            }
        }

        private bool _AddScriptCode;
        public bool AddScriptCode
        {
            get
            {
                return _AddScriptCode;

            }
            set
            {
                _AddScriptCode = value;
            }
        }

        private G2Core.Common.RequestVariable _SelectedSheetControlRequest;
        public G2Core.Common.RequestVariable SelectedSheetControlRequest
        {
            get
            {
                if (_SelectedSheetControlRequest == null)
                {
                    _SelectedSheetControlRequest = new G2Core.Common.RequestVariable(SelectedSheetControlID, G2Core.Common.RequestVariable.RequestType.Form);
                }
                return _SelectedSheetControlRequest;
            }

        }

        private String _ID;
        public String ID
        {
            get { return _ID; }
        }

        public SheetsControl(string id)
        {
            _ID = id;
            _AddScriptCode = true;
        }

        public HtmlGenericControl GetControl()
        {
            HtmlGenericControl control = new HtmlGenericControl("div");
            control.ID = ID;
            control.Attributes["class"] = "SheetsControl";

            HtmlGenericControl select = new HtmlGenericControl("div");
            select.Attributes["class"] = "SheetsLabels";
            HtmlGenericControl sheetsControls = new HtmlGenericControl("div");
            sheetsControls.ID = this.ID + "_SheetsContainer";
            sheetsControls.Attributes["class"] = "SheetsContainer";
            for (int i = 0; i < Sheets.Count; i++)
            {
                HtmlGenericControl Label = Sheets[i].LabelControl();
                select.Controls.Add(Label);

                HtmlGenericControl sheet = new HtmlGenericControl("div");
                sheet.Controls.Add(Sheets[i].Control);
                sheet.ID = ID + "_Sheet" + Sheets[i].Index;
                sheet.Attributes["class"] = "SingleSheet";

                sheetsControls.Controls.Add(sheet);
                if (SelectedSheetControlRequest.IsValid() && SelectedSheetControlRequest.StringValue.Length > 0)
                {
                    if (SelectedSheetControlRequest.StringValue == Sheets[i].ID)
                        Label.Attributes["class"] += " SheetLabelSelected";
                    else
                        sheet.Style.Add("display", "none");
                }
                else
                {
                    if (i > 0)
                        sheet.Style.Add("display", "none");
                    else
                        Label.Attributes["class"] += " SheetLabelSelected";
                }
            }
            control.Controls.Add(SelectedSheetControl);
            control.Controls.Add(Scripts);
            control.Controls.Add(select);
            control.Controls.Add(sheetsControls);

            return control;
        }
        private HtmlGenericControl Scripts
        {
            get
            {
                HtmlGenericControl script = new HtmlGenericControl("script");
                script.Attributes["type"] = "text/javascript";
                if (AddScriptCode)
                    script.InnerHtml += @"
                                        function SelectSheet(NewSheetID,SelectedSheetControlID,SheetsControlID)
                                        {
                                            oSheetsContainer = document.getElementById(SheetsControlID+'_SheetsContainer');
                                            if(oSheetsContainer)
                                            {
                                              Sheets = oSheetsContainer.childNodes
                                              
                                              var i;
                                              for (i in Sheets)
                                              {
                                                   oOldSheetControl = document.getElementById(Sheets[i].id);
                                                   oOldSheetControlLabel = document.getElementById(Sheets[i].id+'_Label');

                                                   if(oOldSheetControl)
                                                   {
                                                      oOldSheetControl.style.display = 'none';
                                                      if(oOldSheetControlLabel) 
                                                        if(oOldSheetControlLabel.className)
                                                            oOldSheetControlLabel.className  = oOldSheetControlLabel.className.replace(' SheetLabelSelected','');
                                                   }
                                              } 

                                              oSelectedSheetControl = document.getElementById(SelectedSheetControlID);
                                              var oSheetControl = document.getElementById(NewSheetID);
                                              if(oSheetControl)
                                              {
                                                  var oSheetControlLabel = document.getElementById(NewSheetID+'_Label');
                                                  oSheetControl.style.display = 'block';
                                                  if(oSheetControlLabel)
                                                    oSheetControlLabel.className = oSheetControlLabel.className  + ' SheetLabelSelected';
                                                  if(oSelectedSheetControl)
                                                    oSelectedSheetControl.value = NewSheetID;
                                              }
                                            }
                                        }
                        ";

                return script;
            }
        }

        private HtmlGenericControl SelectedSheetControl
        {
            get
            {
                string value = "";
                if (SelectedSheetControlRequest.IsValid() && SelectedSheetControlRequest.StringValue.Length > 0)
                {
                    value = SelectedSheetControlRequest.StringValue;
                }
                else
                    if (Sheets.Count > 0)
                        value = Sheets[0].ID;

                HtmlGenericControl hidden = new HtmlGenericControl("span");
                hidden.InnerHtml = "<input ";
                hidden.InnerHtml += "name=\"" + SelectedSheetControlID + "\" ";
                hidden.InnerHtml += "id=\"" + SelectedSheetControlID + "\" ";
                hidden.InnerHtml += "type=\"hidden\" ";
                hidden.InnerHtml += "value=\"" + value + "\" />";

                return hidden;
            }
        }
        public string SelectedSheetControlID
        {
            get
            {
                return ID + "_SelectedSheetControl";
            }
        }
    }
}
