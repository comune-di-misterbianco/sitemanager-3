﻿using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace G2Core.XhtmlControls.Sheets
{
    public class HtmlSheet
    {
        private HtmlControl _Control;
        public HtmlControl Control
        {
            get { return _Control; }
            set { _Control = value; }
        }

        private string _Label;
        public string Label
        {
            get { return _Label; }
            set { _Label = value; }
        }

        private int _Index;
        public int Index
        {
            get { return _Index; }
            set { _Index = value; }
        }

        public string ID
        {
            get
            {
                string id = "";
                if (this.SheetsControlOwner != null)
                    id = SheetsControlOwner.ID + "_";
                id += "Sheet" + this.Index;

                return id;
            }
        }

        private SheetsControl _SheetsControlOwner;
        public SheetsControl SheetsControlOwner
        {
            get { return _SheetsControlOwner; }
        }


        public HtmlSheet(string label, HtmlControl control)
        {
            _Label = label;
            _Control = control;
        }

        public HtmlSheet()
        {
        }

        public HtmlGenericControl LabelControl()
        {
            HtmlGenericControl LabelControl_Container = new HtmlGenericControl("div");
            HtmlGenericControl LabelControl_Sx = new HtmlGenericControl("div");
            HtmlGenericControl LabelControl_Body = new HtmlGenericControl("div");
            HtmlGenericControl LabelControl_Dx = new HtmlGenericControl("div");

            LabelControl_Container.ID = "";
            if (this.SheetsControlOwner != null)
                LabelControl_Container.ID = SheetsControlOwner.ID + "_";
            LabelControl_Container.ID += "Sheet" + this.Index + "_Label";

            LabelControl_Container.Attributes["class"] = "SheetLabelContainer";
            LabelControl_Sx.Attributes["class"] = "SheetLabelSx";
            LabelControl_Dx.Attributes["class"] = "SheetLabelDx";
            LabelControl_Body.Attributes["class"] = "SheetLabelBody";

            LabelControl_Container.Controls.Add(LabelControl_Sx);
            LabelControl_Sx.Controls.Add(LabelControl_Dx);
            LabelControl_Dx.Controls.Add(LabelControl_Body);

            string href = "#";
            if (this.SheetsControlOwner != null)
            {
                href = SheetsControlOwner.ID + "_";
                href += "Sheet" + this.Index;
                href = "javascript:SelectSheet('" + href + "','" + SheetsControlOwner.SelectedSheetControlID + "','" + this.SheetsControlOwner.ID + "');";
            }
            LabelControl_Body.InnerHtml = "<a href=\"" + href + "\">";
            LabelControl_Body.InnerHtml += Label;
            LabelControl_Body.InnerHtml += "</a>";

            return LabelControl_Container;
        }

        public void SetOwner(SheetsControl owner)
        {
            _SheetsControlOwner = owner;
        }
    }
}
