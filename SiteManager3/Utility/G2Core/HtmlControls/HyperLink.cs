using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace G2Core.XhtmlControls
{
    public class Hyperlink
    {
        public string Class
        {
            get
            {
                return _Class;
            }
            set
            {
                _Class = value;
            }
        }
        public string _Class;

        private string href;
        public string Href
        {
            get { return href; }
            set { href = value; }
        }

        private string _Label;
        public string Label
        {
            get { return _Label; }
            set { _Label = value; }
        }

        public string IncapsulateLink(string href,string label)
        {            
            string outval = "<a href=\"";
            outval += href;

            outval += "\"";
            if (Class != null && Class.Length > 0)
                outval += " class=\"" + Class + "\" ";
            outval += ">" + label + "</a>";
            return outval;            
        }

        public string HyperLink
        {
            get
            {
                return IncapsulateLink(Href, Label);
            }
        }

        public string GetHyperLink()
        {
            return GetHyperLink(true, "", "");
        }
        public string GetHyperLink(string label)
        {
            return IncapsulateLink(GetHyperLink(true, "", ""), label);
        }
        public string GetHyperLink(bool useqs, string key, string value)
        {
            string outval = href;
            if (useqs)
            { 
                for (int i = 0; i < QueryStringItems.Count; i++)
                {
                    if (key.ToLower() != QueryStringItems[i].Key.ToLower())
                    {
                        if (outval == href)
                            outval += "?";
                        else
                            outval += "&amp;";
                        outval += QueryStringItems[i].Key + "=" + QueryStringItems[i].Value;
                    }
                }
            }
            if (key.Length > 0 && value.Length > 0)
            {
                outval += outval.Contains("?") ? "&amp;" : "?";
                outval += key + "=" + value;
            }
            
            return outval;
        }
        public string GetHyperLink(bool useqs, string key, string value, string label)
        {
            return IncapsulateLink(GetHyperLink(useqs, key, value), label);
        }

        private QueryStringCollection _QueryStringItems;
        public QueryStringCollection QueryStringItems
        {
            get 
            {
                if (_QueryStringItems == null)
                    _QueryStringItems = new QueryStringCollection();
                return _QueryStringItems; 
            }
        }
    }

    public class QueryStringCollection
    {
        private G2Collection coll;

        public int Count { get { return coll.Count; } }
        public QueryStringCollection()
        {
            coll = new G2Collection();
        }

        public void Add(QueryStringItem str)
        {
            coll.Add(str.GetHashCode().ToString(), str);            
        }

        public void Add(QueryStringItem str, out string ritorna)
        {
            ritorna = str.GetHashCode().ToString();
            coll.Add(ritorna, str);            
        }

        public void Remove(string key)
        {
            coll.Remove(key);
        }

        public void RemoveAll()
        {
            coll.Clear();
        }

        public String[] AllKeys
        {
            get
            {
                return coll.AllKeys;
            }
        }

        public QueryStringItem this[int i]
        {
            get
            {
                QueryStringItem value = (QueryStringItem)coll[coll.Keys[i]];
                return value;
            }
        }

        public QueryStringItem this[string key]
        {
            get
            {
                QueryStringItem field = (QueryStringItem)coll[key];
                return field;
            }
        }

    }

    public class QueryStringItem
    {
        private string _Key;
        public string Key
        {
            get { return _Key; }
            set {  _Key = value; }
        }

        private string _Value;
        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
        public QueryStringItem() :this("","")
        {

        }

        public QueryStringItem(string key)
            :this(key,"")
        {
        }

        public QueryStringItem(string key,string value)
        {
            _Key = key;
            _Value = value;
        }
    }
}