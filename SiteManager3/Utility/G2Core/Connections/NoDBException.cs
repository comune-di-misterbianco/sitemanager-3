﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2Core.Connections
{
    public class NoDBException : System.Exception
    {
        public NoDBException()
        { }

        public NoDBException(string message) : base(message)
        { }

        public void TraceError(Exception ex, string username="", string userid="")
        {
            System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);
            System.Diagnostics.StackFrame sf = st.GetFrame(0);

            NetCms.Diagnostics.Diagnostics.TraceUnhandledError(sf, ex.Message, username, userid, ex);
        }
    }
}
