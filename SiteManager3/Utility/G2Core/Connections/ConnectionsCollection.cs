using System;
using System.Data;
using System.Collections;


namespace G2Core.Connections
{
    /// <summary>
    /// Fornisce funzionalit� standard per la creazione e la gestione di un insieme di oggetti <see cref="NetCms.Connections.Connection"></see> 
    /// ordinato.    
    /// </summary>
    /// <remarks>
    /// Questo insieme supporta l'indicizzazione in base zero standard.
    /// </remarks>
    public partial class ConnectionsCollection : IEnumerable
    {
        private NsvGenericCollection coll;
        /// <summary>
        /// Ritorna il numero di oggetti nella collezione 
        /// </summary>
        public int Count { get { return coll.Count; } }
        /// <summary>
        /// Inizializza una instanza della classe
        /// </summary>
        public ConnectionsCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new NsvGenericCollection();
        }

        /// <summary>
        /// Aggiunge alla lista un nuovo oggetto.
        /// </summary>
        /// <param name="conn">Oggetto da aggiungere alla lista</param>
        /// <param name="key">Chiave legata all'oggetto</param>
        public void Add(Connection conn, string key)
        {
            coll.Add(key, conn);
        }

        /// <summary>
        /// Aggiunge alla lista un nuovo oggetto.
        /// </summary>
        /// <param name="conn">Oggetto da aggiungere alla lista</param>
        public void Add(Connection conn)
        {
            coll.Add(conn.ConnectionString.ToLower(), conn);
        }
        /// <summary>
        /// Rimuove un oggetto dalla lista.
        /// </summary>
        /// <param name="key">Chiave per accedere all'oggetto da rimuovere.</param>
        public void Remove(string key)
        {
            coll.Remove(key);
        }
        /// <summary>
        /// Rimuove tutti gli oggetti  dalla collezione
        /// </summary>
        public void Clear()
        {
            coll.Clear();
        }

        /// <summary>
        /// Permette di accedere ad un oggetto della collezione tramite un indice.
        /// </summary>
        /// <param name="i">indice della lista in base zero dell'oggetto da restituire.</param>
        /// <returns>Oggetto contenuto alla posizione specificata della lista.</returns>        
        public Connection this[int i]
        {
            get
            {
                Connection obj = (Connection)coll[coll.Keys[i]];
                return obj;
            }
        }

        /// <summary>
        /// Permette di accedere ad un oggetto della collezione tramite la chiave associata 
        /// all'oggetto.
        /// </summary>
        /// <param name="str">Chiave dell'oggetto da restiture.</param>
        /// <returns>Oggetto associato alla chiave specificata.</returns>
        public Connection this[string str]
        {
            get
            {
                Connection obj = (Connection)coll[str];
                return obj;
            }
        }
        /// <summary>
        /// Indica se la collezione contiene un oggetto con la chiave specificata.
        /// </summary>
        /// <param name="key">Chiave dell'oggetto</param>
        /// <returns>Vero se la collezione contiene l'oggetto, falso altrimenti.</returns>
        public bool Contains(string str)
        {
            return coll[str] != null;
        }

        #region Enumerator

        /// <summary>
        /// Restituisce un enumeratore che scorre un insieme.
        /// </summary>
        /// <returns>Oggetto IEnumerator che pu� essere utilizzato per scorrere l'insieme.</returns>
        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private ConnectionsCollection Collection;
            public CollectionEnumerator(ConnectionsCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}