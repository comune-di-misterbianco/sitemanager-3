using System;
using System.Data;
using System.Linq;
using System.IO;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;
using NetCms.Diagnostics;



namespace G2Core.Connections
{
    /// <summary>
    /// Deriva la classe base <see cref="NetCms.Connections.Connection"></see> implementando i metodi e propriet�
    /// per gestire la connessione e comandi per un database di tipo MySQL
    /// </summary>  
    /// <example>
    /// Segue un esempio di creazione di un'istanza della classe.
    /// <code>
    /// private NetCms.Connections.Connection _Conn;
    /// public NetCms.Connections.Connection Conn
    /// {
    ///     get
    ///     {
    ///        if (_Conn == null)
    ///        {
    ///             string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["eComm-DB"].ConnectionString;
    ///             _Conn = new G2Core.Connections.MySqlConnection(connstr);
    ///        }
    ///        return _Conn;
    ///     }
    /// }
    /// </code>
    /// </example>
    public class MySqlConnection : Connection
    {

        protected G2Core.Connections.MySqlConnectionSchema _DbSchema;
        protected G2Core.Connections.MySqlConnectionSchema DbSchema
        { 
            get 
            { 
                if(_DbSchema == null)
                    _DbSchema = new MySqlConnectionSchema(this);
                return _DbSchema;
            } 
        }

        /// <summary>
        /// Implementa la logica per creare un parametro di un oggetto DbCommand 
        /// per un database MySQL.
        /// </summary>
        /// <param name="parameterName">Nome del parametro.</param>
        /// <param name="value">Valore da assegnare al parametro.</param>
        /// <param name="direction">Direzione del parametro.</param>
        /// <returns>Il parametro creato</returns>
        public override DbParameter CreateParameter(string parameterName, object value, ParameterDirection direction)
        {
            MySqlParameter parameter = new MySqlParameter();
            parameter.ParameterName = parameterName;
            parameter.Value = value;
            parameter.Direction = direction;
            if (value == null)
            {              
              parameter.Value = DBNull.Value;
            }
            return parameter;
        }

        /// <summary>
        /// Implementa la logica di connessione ad un database MySQL restituendo un oggetto
        /// di connessione specifico MySql.Data.MySqlClient.MySqlConnection.
        /// </summary>
        /// <returns>connessione al database MySQL</returns>
        protected override DbConnection BuildConnection()
        {
            return new MySql.Data.MySqlClient.MySqlConnection(this.ConnectionString);
        }

        /// <summary>
        /// Implementa la logica per costruire un oggetto specifico di tipo System.Data.Common.DbDataAdapter
        /// in grado di eseguire delle istruzioni nei confronti di un database MySQL.
        /// </summary>
        /// <returns>L'oggetto di tipo MySql.Data.MySqlClient.MySqlDataAdapter creato.</returns>
        protected override DbDataAdapter BuildDataAdapter()
        {
            MySqlDataAdapter DataAdapterObj = new MySqlDataAdapter();
            DataAdapterObj.SelectCommand = (MySql.Data.MySqlClient.MySqlCommand)SelectCommand;
            return DataAdapterObj;
        }

        /// <summary>
        /// Implementa la logica per costruire un comando MySQL.
        /// </summary>
        /// <returns>Un'istruzione SQL o una stored procedure da eseguire su un database.</returns>
        protected override DbCommand BuildSelectCommand()
        {
            MySqlCommand SelectCommandObj = new MySqlCommand();
            SelectCommandObj.Connection = (MySql.Data.MySqlClient.MySqlConnection)Conn;            
            return SelectCommandObj;
        }


        /// <summary>
        /// Inizializza una instanza della classe.
        /// </summary>
        /// <param name="connection_string">stringa di connessione ad un database MySQL</param>
        public MySqlConnection(string connection_string)
            : base(ParseConnectionString(connection_string))
        {
        }

        private static string ParseConnectionString(string connection_string)
        {
            try
            {
                if (connection_string.ToLower().Contains("MySQL ODBC 3.51 Driver".ToLower())
                    || connection_string.ToLower().Contains("MySQL ODBC 5.1 Driver".ToLower())
                    || connection_string.ToLower().Contains("MySQL ODBC 5.2 Driver".ToLower()))
                {
                    string newConnStr = "server={0};user id={1};password={2};database={3};port={4}";

                    var server = Regex.Match(connection_string, ParseWordForRegex("Server") + "=[^;]+?;").Value;
                    var user = Regex.Match(connection_string, ParseWordForRegex("Uid") + "=[^;]+?;").Value;
                    var password = Regex.Match(connection_string, ParseWordForRegex("Pwd") + "=[^;]+?;").Value;
                    var database = Regex.Match(connection_string, ParseWordForRegex("db") + "=[^;]+?;").Value;
                    var port = Regex.Match(connection_string, ParseWordForRegex("port") + "=[^;]+?;").Value;

                    server = server.Substring("Server".Length + 1, (server.Length - "Server".Length) - 2);
                    user = user.Substring("Uid".Length + 1, (user.Length - "Uid".Length) - 2);
                    password = password.Substring("Pwd".Length + 1, (password.Length - "Pwd".Length) - 2);
                    database = database.Substring("db".Length + 1, (database.Length - "db".Length) - 2);
                    port = port.Substring("port".Length + 1, (port.Length - "port".Length) - 2);

                    return string.Format(newConnStr, server, user, password, database, port);
                }
                else if (connection_string.ToLower().Contains("MySQL ODBC 5.2 Unicode Driver".ToLower()) || connection_string.ToLower().Contains("MySQL ODBC 5.2w Driver".ToLower()))
                {
                    string newConnStr = "server={0};user={1};password={2};database={3};port={4}";

                    var server = Regex.Match(connection_string, ParseWordForRegex("Server") + "=[^;]+?;").Value;
                    var user = Regex.Match(connection_string, ParseWordForRegex("Uid") + "=[^;]+?;").Value;
                    var password = Regex.Match(connection_string, ParseWordForRegex("Pwd") + "=[^;]+?;").Value;
                    var database = Regex.Match(connection_string, ParseWordForRegex("db") + "=[^;]+?;").Value;
                    var port = Regex.Match(connection_string, ParseWordForRegex("port") + "=[^;]+?;").Value;

                    server = server.Substring("Server".Length + 1, (server.Length - "Server".Length) - 2);
                    user = user.Substring("Uid".Length + 1, (user.Length - "Uid".Length) - 2);
                    password = password.Substring("Pwd".Length + 1, (password.Length - "Pwd".Length) - 2);
                    database = database.Substring("db".Length + 1, (database.Length - "db".Length) - 2);
                    port = port.Substring("port".Length + 1, (port.Length - "port".Length) - 2);

                    return string.Format(newConnStr, server, user, password, database, port);
                }
            }
            catch(Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(TraceLevel.Error, "Errore durante il parsing della stringa di connessione.","","", ex);                
            }
            return connection_string;
        }

        private static string ParseWordForRegex(string word)
        {
            string res = "";
            foreach (char c in word.ToCharArray())
                res += "[" + c.ToString().ToUpper() + "|" + c.ToString().ToLower() + "]";
            return res;
        }

        /// <summary>
        /// Implementa la logica per determinare l'ID del record inserito nell'ultima operazione 
        /// di insert.
        /// </summary>
        /// <returns>ID del record inserito nel database dall'ultima operazione di insert.</returns>
        public override int GetLastInsertID()
        {
            bool keepConnectionAlive = false;
            int lastInsert = 0;
            MySqlDataReader rdr;

            if (!this.ConnectionOpen)
                Open();
            else
                keepConnectionAlive = true;

            SelectCommand.CommandText = "SELECT LAST_INSERT_ID();";
            SelectCommand.CommandType = CommandType.Text;
            rdr = (MySqlDataReader)SelectCommand.ExecuteReader();


            if (rdr.Read())
                lastInsert = int.Parse(rdr[0].ToString());
            else
                lastInsert = 0;

            rdr.Close();
            rdr.Dispose();

            if (!keepConnectionAlive) Close();

            return lastInsert;
        }

        /// <summary>
        /// Restituisce una stringa rappresentante un dato di tipo DateTime conforme ai database 
        /// MySQL.    
        /// </summary>
        /// <param name="date">stringa da formattare</param>
        /// <returns>stringa formattata</returns>
        public override string FilterDate(DateTime date)
        {

            string day = date.Day.ToString();
            if (day.Length == 1) day = "0" + day;
            string month = date.Month.ToString();
            if (month.Length == 1) month = "0" + month;
            string year = date.Year.ToString();
            //katja added the time value
            string hour = date.Hour.ToString();
            string minute = date.Minute.ToString();
            string second = date.Second.ToString();
            if (String.Compare(hour + minute + second, "000") == 0)
                //return "'" + date.Year.ToString() + month + day + "'";
                return "'" + year + "-" + month + "-" + day + "'";
            else
                //return "'" + date.Year.ToString() + month + day + hour + minute + second + "'";
                return "'" + year + "-" + month + "-" + day + " " + date.Hour + "." + date.Minute + "." + date.Second + "'";
         }

        /// <summary>
        /// Restituisce una stringa rappresentante un dato di tipo DateTime conforme ai database 
        /// MySQL. 
        /// </summary>
        /// <param name="date">stringa da formattare</param>
        /// <returns>stringa formattata</returns>   
        public override string FilterDateTime(DateTime date)
        {
            string day = date.Day.ToString();
            if (day.Length == 1) day = "0" + day;

            string month = date.Month.ToString();
            if (month.Length == 1) month = "0" + month;

            string year = date.Year.ToString();
            return "'" + date.Year.ToString() + "/" + month + "/" + day + " " + date.Hour + ":" + date.Minute + ":" + date.Second + "'";
        }
    }


    /// <summary>
    /// Costruiusce lo schema delle tabelle di un database MySQL
    /// </summary>
    public class MySqlConnectionSchema : Schema
    {
        
        private SchemaTableCollection _Tables;
        public override SchemaTableCollection Tables
        {
            get
            {
                return _Tables;
            }
        }

        private MySqlConnection Connection;

        public MySqlConnectionSchema(MySqlConnection conn)
        {
            Connection = conn;
            _Tables = new SchemaTableCollection();
            string sql = "SHOW TABLES";
            DataTable tab = this.Connection.SqlQuery(sql);

            foreach (DataRow tableData in tab.Rows)
            {
                SchemaTable table = new SchemaTable(tableData[0].ToString());

                sql = "SHOW COLUMNS IN " + table.TableName;
                DataTable cols = this.Connection.SqlQuery(sql);
                foreach (DataRow columnData in cols.Rows)
                {
                    table.Columns.Add(new SchemaColumn(columnData[0].ToString(), columnData[1].ToString()));
                }

                _Tables.Add(table);
            }
        }
    }

    public abstract class Schema
    {
        public abstract SchemaTableCollection Tables{get;}
        public Schema()
        {
        }
    }
    public class SchemaTable
    {
        private SchemaColumnCollection _Columns;
        public SchemaColumnCollection Columns
        {
            get
            {
                return _Columns;
            }
        }
        private string _TableName;
        public string TableName
        {
            get { return _TableName; }
        }
	
        public SchemaTable(string tableName)
        {
            _TableName = tableName;
            _Columns = new SchemaColumnCollection();
        }
    }
    
    /// <summary>
    /// Classe per generare lo schema di una colonna
    /// </summary>
    public class SchemaColumn
    {
        private string _ColumnName;
        /// <summary>
        /// Nome della colonna
        /// </summary>
        public string ColumnName
        {
            get { return _ColumnName; }
        }

        private string _Type;
        /// <summary>
        /// Tipo di dato memorizzato dalla colonna 
        /// </summary>
        public string Type
        {
            get { return _Type; }
        }
        /// <summary>
        /// Inizializza una istanza della classe.
        /// </summary>
        /// <param name="columnName">Nome della colonna</param>
        /// <param name="type">Tipo di dato memorizzato nella colonna</param>
        public SchemaColumn(string columnName,string type)
        {
            _ColumnName = columnName;
            _Type = type;

        }
    }

    /// <summary>
    /// Fornisce funzionalit� standard per la creazione e la gestione di un insieme di oggetti <see cref="G2Core.Connections.SchemaTable"></see> 
    /// ordinato.    
    /// </summary>
    /// <remarks>
    /// Questo insieme supporta l'indicizzazione in base zero standard.
    /// </remarks>
    public class SchemaTableCollection
    {
        private NsvGenericCollection coll;
        /// <summary>
        /// Ritorna il numero di oggetti nella collezione 
        /// </summary>
        public int Count { get { return coll.Count; } }

        /// <summary>
        /// Inizializza una instanza della classe
        /// </summary>
        public SchemaTableCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new NsvGenericCollection();
        }

        /// <summary>
        /// Aggiunge alla lista un nuovo oggetto indicando la chiave con cui riferenziarlo
        /// e l'oggetto da inserire di tipo <see cref="G2Core.Connections.SchemaTable"> </see>
        /// </summary>
        /// </summary>
        /// <param name="Table">Oggetto da aggiungere alla lista/param>
        public void Add(SchemaTable Table)
        {
            coll.Add(Table.GetHashCode().ToString(), Table);
        }

        /// <summary>
        /// Rimuove un oggetto dalla lista.
        /// </summary>
        /// <param name="Table">Oggetto da rimuovere</param>
        public void Remove(SchemaTable Table)
        {
            coll.Remove(Table.GetHashCode());
        }


        /// <summary>
        /// Rimuove tutti gli oggetti  dalla collezione
        /// </summary>
        public void Clear()
        {
            coll.Clear();
        }

        /// <summary>
        /// Permette di accedere ad un oggetto della collezione tramite un indice.
        /// </summary>
        /// <param name="i">indice della lista in base zero dell'oggetto da restituire.</param>
        /// <returns>Oggetto contenuto alla posizione specificata della lista.</returns>
        public SchemaTable this[int i]
        {
            get
            {
                SchemaTable str = (SchemaTable)coll[coll.Keys[i]];
                return str;
            }
        }

        /// <summary>
        /// Indica se la collezione contiene un oggetto con la chiave specificata.
        /// </summary>
        /// <param name="key">Chiave dell'oggetto</param>
        /// <returns>Vero se la collezione contiene l'oggetto, falso altrimenti.</returns>
        public bool Contains(SchemaTable Table)
        {
            return coll[Table.GetHashCode().ToString()] != null;
        }

        #region Enumerator
        /// <summary>
        /// Restituisce un enumeratore che scorre un insieme.
        /// </summary>
        /// <returns>Oggetto IEnumerator che pu� essere utilizzato per scorrere l'insieme.</returns>        
        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }


        /// <summary>
        /// Implementa una enumeratore per scorrere uno specifico insieme. 
        /// </summary>
        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private SchemaTableCollection Collection;
            /// <summary>
            /// Instanzia un oggetto di tipo CollectionEnumerator.
            /// </summary>
            /// <param name="coll">Collezione a cui aggiungere le funzionalit� di enumerazione</param>
            public CollectionEnumerator(SchemaTableCollection coll)
            {
                Collection = coll;
            }
            /// <summary>
            /// Restituisce l'oggetto memorizzato nell'insieme al corrente indice di 
            /// scorrimento.
            /// </summary>
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }

            /// <summary>
            /// Avanza l'indice di scorrimento degli oggetti nell'insieme.
            /// </summary>
            /// <returns>vero se si � avanzati nello scorrimento dell'indice, falso se si � 
            /// giunti alla fine dello scorrimento</returns>
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            /// <summary>
            /// Resetta l'indice di scorrimento all'inizio.
            /// </summary>
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }  
 
     /// <summary>
    /// Fornisce funzionalit� standard per la creazione e la gestione di un insieme di oggetti <see cref="G2Core.Connections.SchemaColumn"></see> 
    /// ordinato.    
    /// </summary>
    /// <remarks>
    /// Questo insieme supporta l'indicizzazione in base zero standard.
    /// </remarks>
    public class SchemaColumnCollection : IEnumerable
    {
        private NsvGenericCollection coll;
        public int Count { get { return coll.Count; } }
        public SchemaColumnCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new NsvGenericCollection();
        }

        public void Add(SchemaColumn Column)
        {
            coll.Add(Column.GetHashCode().ToString(), Column);
        }

        public void Remove(SchemaColumn Column)
        {
            coll.Remove(Column.GetHashCode());
        }

        public void Clear()
        {
            coll.Clear();
        }

        public SchemaColumn this[int i]
        {
            get
            {
                SchemaColumn str = (SchemaColumn)coll[coll.Keys[i]];
                return str;
            }
        }

        public bool Contains(SchemaColumn Column)
        {
            return coll[Column.GetHashCode().ToString()] != null;
        }

        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private SchemaColumnCollection Collection;
            public CollectionEnumerator(SchemaColumnCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }


}