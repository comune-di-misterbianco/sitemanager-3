using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml;


/// <summary>
/// Summary description for Configs
/// </summary>
namespace G2Core.Connections.Configurations
{
    public static class Debug
    {
        public static System.Collections.Specialized.NameValueCollection Options
        {
            get
            {
                return _Options ?? (_Options = InitCollection());
            }
        }
        private static System.Collections.Specialized.NameValueCollection _Options;
        private static System.Collections.Specialized.NameValueCollection InitCollection()
        {
            System.Collections.Specialized.NameValueCollection coll = null;
            try
            {
                coll = (System.Collections.Specialized.NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("NetServiceDebug");
            }
            catch {}
            if(coll == null)
                coll = new System.Collections.Specialized.NameValueCollection();
            return coll;
        }
         
        public static bool GetOptionEnableStatus(string key)
        {
            lock (Options)
            {
                bool result = false;
                string opt = Options[key];
                if (opt != null) bool.TryParse(opt, out result);
                return result;
            }
        }
        
        public static bool SqlExceptionsEnabled
        {
            get
            {
                return (_SqlExceptionsEnabled ?? (_SqlExceptionsEnabled = new bool?(GetOptionEnableStatus("enableSqlExceptions")))).Value;
            }
        }
        private static bool? _SqlExceptionsEnabled;
    }
}