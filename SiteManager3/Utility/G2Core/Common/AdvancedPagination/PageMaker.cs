﻿using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Xml;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace G2Core.AdvancedPagination
{
    public abstract class PageMaker
    {
        public string Key
        {
            get
            {
                if (_Key == null)
                {
                    _Key = "page";
                }
                return _Key;
            }
            set { _Key = value; }
        }
        private string _Key;

        public string ID
        {
            get
            {
                if (_ID == null)
                    _ID = "PageNavigation" + this.GetHashCode();
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }
        private string _ID;

        public string CssClass
        {
            get
            {
                if (_CssClass == null)
                    _CssClass = "PageNavigation";
                return _CssClass;
            }
            set
            {
                _CssClass = value;
            }
        }
        private string _CssClass;

        public int PagesButtonsRange { get; set; }
        public bool EnableGotoControls { get; set; } 

        public VariabileType Type
        {
            get
            {
                return _Type;
            }
        }
        private VariabileType _Type;

        public G2Core.Common.RequestVariable PageRequest
        {
            get
            {
                return _PageRequest ?? (_PageRequest = new G2Core.Common.RequestVariable(Key, VariabileType.Form == Type ? G2Core.Common.RequestVariable.RequestType.Form : Common.RequestVariable.RequestType.QueryString));
            }
        }
        private G2Core.Common.RequestVariable _PageRequest;

        private HttpRequest _Request;
        private HttpRequest Request
        {
            get
            {
                if (_Request == null)
                    _Request = HttpContext.Current.Request;
                return _Request;
            }
            set { _Request = value; }
        }

        public int RPP { get; set; }
        public abstract int RecordCount { get; }
        public abstract PaginationInfo Info { get; }

        #region Costruttori

        public PageMaker(string key, VariabileType VarType)
        {
            _Key = key;
            RPP = 10;
            _Type = VarType;
            PagesButtonsRange = 2;
        } 
        #endregion
        
        public HtmlGenericControl PageNavigation(string querystring)
        {
            string href = "<a href=\"" + querystring + "";

            #region Calcoli Aritmetici sulla paginazione

            int Range = PagesButtonsRange;

            int TopRange = Info.CurrentPageToShow + Range <= Info.Pages ? Info.CurrentPageToShow + Range : Info.Pages == 0 ? 1 : Info.Pages;

            int BottomRange = Info.CurrentPageToShow - Range > 0 ? Info.CurrentPageToShow - Range : 1;

            #endregion

            var pages = new HtmlGenericControl("div");
            pages.ID = ID;
            pages.Attributes["class"] = CssClass;
            pages.InnerHtml = "<span>Pagina " + Info.CurrentPageToShow + " di " + Info.Pages;
            if (Info.Pages <= 1)
                pages.InnerHtml += "</span>";
            else
            {
                pages.InnerHtml += ":</span> ";

                if (Info.CurrentPageToShow > 1)
                    pages.InnerHtml += CreateNavigationButton(querystring, "prima pagina", 1, "prima pagina", "firstpage") + "<span class=\"sep\"> | </span>";

                if (Info.CurrentPageToShow != BottomRange)
                    pages.InnerHtml += CreateNavigationButton(querystring, "pagina precedente", (Info.CurrentPageToShow - 1), "pagina precedente", "previouspage") + "<span class=\"sep\"> | </span>";

                int i = 0;
                for (i = BottomRange; i <= TopRange; i++)
                {
                    string classe = "numbers";
                    string tag;

                    if (i == Info.CurrentPageToShow)
                    {
                        classe += " selected";
                        tag = "span";
                    }
                    else
                        tag = "a";

                    pages.InnerHtml += CreateNavigationButton(querystring, i.ToString(), i, "Vai a pagina" + i, classe, tag);
                }
                if (Info.CurrentPageToShow != TopRange)
                    pages.InnerHtml += "<span class=\"sep\"> | </span>" + CreateNavigationButton(querystring, "pagina successiva", (Info.CurrentPageToShow + 1), "Vai alla pagina successiva", "nextpage") + "<span class=\"sep\"> | </span>";

                if (Info.CurrentPageToShow < Info.Pages)
                    pages.InnerHtml += CreateNavigationButton(querystring, "ultima pagina", Info.Pages, "Vai all'ultima pagina", "lastpage") + "<span class=\"sep\"> | </span>";
            }
            if (Type == VariabileType.Form && EnableGotoControls)
            {
                pages.InnerHtml += @"<span class=""goto""><input type=""text"" id=""" + Key + @"_gotofield"" value=""" + Info.CurrentPageToShow + @"""><input type=""button"" onclick=""javascript: setFormValue(document.getElementById('" + Key + @"_gotofield').value,'" + this.Key + @"',1)"" id=""" + Key + @"_gotofield"" value=""Vai a pagina""></span>";
                pages.InnerHtml += "<span class=\"sep\"> | </span>";
            }
            pages.InnerHtml += "<div class=\"clear\"></div>";
            return pages;
        }
        
        public string PageNavigationString(string querystring)
        {
            string href = "<a href=\"" + querystring + "";

            int Range = PagesButtonsRange;
            int TopRange = Info.CurrentPageToShow + Range <= Info.Pages ? Info.CurrentPageToShow + Range : Info.Pages == 0 ? 1 : Info.Pages;

            int BottomRange = Info.CurrentPageToShow - Range > 0 ? Info.CurrentPageToShow - Range : 1;

            HtmlGenericControl pages = new HtmlGenericControl("div");
            pages.InnerHtml = "<span>Pagina " + Info.CurrentPageToShow + " di " + Info.Pages;
            if (Info.Pages <= 1)
                pages.InnerHtml += "</span>";
            else
            {
                pages.InnerHtml += ":</span> ";

                if (Info.CurrentPageToShow > 1)
                    pages.InnerHtml += CreateNavigationButton(querystring, "Prima Pagina", 1, "Vai alla prima pagina");

                if (Info.CurrentPageToShow != BottomRange)
                    pages.InnerHtml += CreateNavigationButton(querystring, "&lt;", (Info.CurrentPageToShow - 1), "Vai alla pagina precedente");


                for (int i = BottomRange; i <= TopRange; i++)
                {
                    string classe = "numbers";
                    string tag;

                    if (i == Info.CurrentPageToShow)
                    {
                        classe += " selected";
                        tag = "span";
                    }
                    else
                        tag = "a";

                    pages.InnerHtml += CreateNavigationButton(querystring, i.ToString(), i, "Vai a pagina " + i, classe, tag);
                }
                if (Info.CurrentPageToShow != TopRange)
                    pages.InnerHtml += CreateNavigationButton(querystring, "&gt;", (Info.CurrentPageToShow + 1), "Vai alla pagina successiva");

                if (Info.CurrentPageToShow < Info.Pages)
                    pages.InnerHtml += CreateNavigationButton(querystring, "Ultima Pagina", Info.Pages, "Vai all'ultima pagina");
            }
            pages.InnerHtml += "<div class=\"clear\"></div>";
            pages.InnerHtml = "<div class=\"PageNavigator\">" + pages.InnerHtml + "</div>";

            return pages.InnerHtml;
        }

        public enum VariabileType
        {
            QueryString,
            Form
        }

        private string CreateNavigationButton(string querystring, string label, int value)
        {
            return CreateNavigationButton(querystring, label, value, label);
        }
        private string CreateNavigationButton(string querystring, string label, int value, string title)
        {
            return CreateNavigationButton(querystring, label, value, title, "", "a");
        }
        private string CreateNavigationButton(string querystring, string label, int value, string title, string classe)
        {
            return CreateNavigationButton(querystring, label, value, title, classe, "a");
        }
        private string CreateNavigationButton(string querystring, string label, int value, string title, string classe, string tag)
        {
            string button = "<" + tag;
            if (tag == "a")
            {
                button += " href=\"" + querystring + "";
                if (Type == VariabileType.QueryString)
                {
                    if (querystring.Contains("?"))
                    {
                        if (querystring.Contains("="))
                            button += "&amp;";
                    }
                    else
                        button += "?";
                    button += this.Key + "=" + (value) + "\"";
                }
                else
                    button = button.Replace("{0}", value.ToString()) + "\"";

                button += " title=\"" + title + "\"";

            }
            if (classe.Length > 0)
                button += " class=\"" + classe + "\"";

            button += ">" + label + "</" + tag + ">";

            /*if (tag == "a")
                button = "<span>" + button + "</span>";*/

            return button;
        }
    }


    public class PaginationInfo
    {
        public int Pages { get; private set; }
        public int PageSize { get; private set; }
        public int CurrentPage { get; private set; }
        public int CurrentPageToShow { get; private set; }
        public int RecordCount { get; private set; }

        public PaginationInfo(int recordCount, int pageSize, int requestedPage)
        {
            PageSize = pageSize;
            RecordCount = recordCount;

            double pageCountTmp = (double)RecordCount / (double)PageSize;
            int absolutePageCountTmp = (int)Math.Abs(pageCountTmp);
            Pages = pageCountTmp > absolutePageCountTmp ? absolutePageCountTmp + 1 : absolutePageCountTmp;
            if (Pages < 1) Pages = 1;

            CurrentPage = requestedPage <= Pages ? (requestedPage < 1 ? 1 : requestedPage) - 1 : 0;
            CurrentPageToShow = CurrentPage + 1;
        }
    }
}