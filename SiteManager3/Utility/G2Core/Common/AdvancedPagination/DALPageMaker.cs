﻿using System.Data;
using System.Linq;
using System.Reflection;
using System;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace G2Core.AdvancedPagination
{
    public class DALPageMaker : PageMaker
    {
        public MethodInfo DalSelectMethod { get; private set; }
        public MethodInfo DalCountMethod { get; private set; }
        public string SqlConditions { get; private set; }
        public string OrderBy { get; set; }
        
        public override int RecordCount
        {
            get
            {
                return (_RecordCount ?? (_RecordCount = new int?((int) DalCountMethod.Invoke(null, new[] { SqlConditions })))).Value;
            }
        }
        private int? _RecordCount;

        private PaginationInfo _Info;
        public override PaginationInfo Info
        {
            get 
            { 
                if(_Info == null)
                    _Info = new PaginationInfo(this.RecordCount, this.RPP, this.PageRequest.IntValue);
                return _Info; 
            }
        }

        public DALPageMaker(MethodInfo dalSelectMethod, MethodInfo dalCountMethod,string sqlConditions, string paginationKey, VariabileType VarType, string order = null)
            : base(paginationKey, VarType)
        {
            SqlConditions = sqlConditions;
            DalSelectMethod = dalSelectMethod;
            DalCountMethod = dalCountMethod;
            OrderBy = order;
        }
        
        public DataRow[] GetPagedRecords()
        {
            object[] parametri = 
                                {
                                    SqlConditions,
                                    Info.CurrentPage,
                                    Info.PageSize,
                                    OrderBy
                                };

            var OutputRows = DalSelectMethod.Invoke(null, parametri) as DataRow[];
            return OutputRows;
        }

        public DataRow[] GetAllRecords()
        {
            object[] parametri = 
                                {
                                    SqlConditions,
                                    0,
                                    0,
                                    OrderBy
                                };

            var OutputRows = DalSelectMethod.Invoke(null, parametri) as DataRow[];
            return OutputRows;
        }
    }
}