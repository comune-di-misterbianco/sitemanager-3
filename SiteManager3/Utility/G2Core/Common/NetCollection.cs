using System;
using System.Collections;
using System.Collections.Specialized;


// <summary>
/// Fornisce funzionalit� standard per la creazione e la gestione di un insieme di oggetti <see cref="System.String"></see> 
/// ordinato.    
/// </summary>
/// <remarks>
/// Questo insieme supporta l'indicizzazione in base zero standard.
/// </remarks>
public class G2Collection : NameObjectCollectionBase  {

   private DictionaryEntry _de = new DictionaryEntry();

   /// <summary>
   /// Inizializza un'istanza della classe con una collezione vuota
   /// </summary>
   public G2Collection()  {
   }

   /// <summary>
   /// Inizializza un'istanza della classe creando una collezione
   /// con dat presenti nella collezione passata.
   /// </summary>
   /// <param name="d">Collezione da cui prendere gli oggetti</param>
   /// <param name="bReadOnly">Indica se la nuova collezione creata � di sola lettura.</param>
    public G2Collection(IDictionary d, Boolean bReadOnly)
    {
      foreach ( DictionaryEntry de in d )  {
         this.BaseAdd( (String) de.Key, de.Value );
      }
      this.IsReadOnly = bReadOnly;
   }

    /// <summary>
    /// Ottiene la coppia chiave/ogetto utilizzando un indice per la ricerca.
    /// </summary>
    /// <param name="index">Indice dell'elemento a cui accedere</param>
    /// <returns>Coppia chiave/oggetto corrispondente all'indice specificato</returns>
   public DictionaryEntry this[ int index ]  {
      get  {
         _de.Key = this.BaseGetKey(index);
         _de.Value = this.BaseGet(index);
         return( _de );
      }
   }

   /// <summary>
   /// Imposta o ottiene l'oggetto associato con la chiave specificata
   /// </summary>
   /// <param name="key">Chiave con cui cercare l'oggetto nella lista</param>
   /// <returns>Oggetto della lista corrispondente alla chiave</returns>
   public Object this[ String key ]  {
      get  {
         return( this.BaseGet( key ) );
      }
      set  {
         this.BaseSet( key, value );
      }
   }

   /// <summary>
   /// Ottiene un array di string contenente tutte le chiavi della collezione 
   /// </summary>
   public String[] AllKeys  {
      get  {
         return( this.BaseGetAllKeys() );
      }
   }

   /// <summary>
   /// Ottiene un array di oggetti contenuti nella collezione
   /// </summary>
   public Array AllValues  {
      get  {
         return( this.BaseGetAllValues() );
      }
   }

   /// <summary>
   /// Ottiene un array di stringhe contenente i valori contenuti nella collezione
   /// </summary>
   public String[] AllStringValues  {
      get  {
         return( (String[]) this.BaseGetAllValues( Type.GetType( "System.String" ) ) );
      }
   }

   // <summary>
   /// Indica se la collezione ha chiavi non nulle.
   /// </summary>
   public Boolean HasKeys  {
      get  {
         return( this.BaseHasKeys() );
      }
   }

   /// <summary>
   /// Aggiunge un nuovo oggetto alla collezione.
   /// </summary>
   /// <param name="key">chiave dell'oggetto</param>
   /// <param name="value">oggetto da aggiungere</param>
   public void Add( String key, Object value )  {
      this.BaseAdd( key, value );
   }


   /// <summary>
   /// Rimuove l'oggetto con la chiave specificata dalla collezione. 
   /// </summary>
   /// <param name="key">chiave dell'oggetto da rimuovere</param>
   public void Remove( String key )  {
      this.BaseRemove( key );
   }

   /// <summary>
   /// Rimuove l'oggetto con l'indice specificato dalla collezione. 
   /// </summary>
   /// <param name="index">Indice dell'oggetto da rimuovere</param>
   public void Remove( int index )  {
      this.BaseRemoveAt( index );
   }

   /// <summary>
   /// Elimina tutti gli elementi della collezione
   /// </summary>
   public void Clear()  {
      this.BaseClear();
   }

}
