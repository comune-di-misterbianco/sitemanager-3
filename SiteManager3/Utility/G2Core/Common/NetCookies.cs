using System;
using System.Web;
using NetCms.Connections;

namespace G2Core.Common
{
    /// <summary>
    /// La classe NetCookies consente la gestione dei cookie
    /// </summary>
    public class NetCookies
    {
        private HttpResponse Response = HttpContext.Current.Response;
        private HttpRequest Request = HttpContext.Current.Request;


        /// <summary>
        /// Inizializza una nuova istanza della classe NetCookies
        /// </summary>
        public NetCookies()
        {

        }
        /// <summary>
        /// Ottiene il riferimento testuale per il cookie
        /// </summary>
        /// <param name="key">Riferimento testuale</param>
        /// <returns>Riferimento testuale per il cookie</returns>
        public string this[string key]
        {
            get
            {
                return this.Contains(key) ? Request.Cookies[key].Value : "";
            }
        }
        /// <summary>
        /// Verifica se esiste un cookie
        /// </summary>
        /// <param name="key">Chiave key di riferimento</param>
        /// <returns></returns>
        public bool Contains(string key)
        {
            return (Request.Cookies[key] != null);
        }
        /// <summary>
        /// Permette di impostare il valore
        /// </summary>
        /// <param name="key">Chiave key di riferimento</param>
        /// <param name="value">Valore da scrivere nel cookie</param>
        public void Set(string key, string value)
        {
            Set(key, value, DateTime.Now.AddYears(30));
        }
        /// <summary>
        /// Permette di impostare il valore e la durata del cookie
        /// </summary>
        /// <param name="key">Chiave key di riferimento del cookie</param>
        /// <param name="value">Valore da scrivere nel cookie</param>
        /// <param name="expire">Data in formato gg/mm/aaaa</param>
        public void Set(string key, string value, DateTime expire)
        {
            HttpCookie cookie = new HttpCookie(key, value);
            cookie.Expires = expire;
            Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// Rimuove il cookie indicato dalla chiave key
        /// </summary>
        /// <param name="key"></param>
        public void Remove(string key)
        {
            HttpCookie cookie = new HttpCookie(key);
            if (cookie != null)
            {
                cookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(cookie);
            }
        }
    }
}