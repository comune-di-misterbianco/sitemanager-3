using System;
using System.Collections;



/// <summary>
/// Fornisce funzionalit� standard per la creazione e la gestione di un insieme di oggetti <see cref="System.Int32"></see> 
/// ordinato.    
/// </summary>
/// <remarks>
/// Questo insieme supporta l'indicizzazione in base zero standard.
/// </remarks>
public class IntCollection : IEnumerable
{
    private G2Collection coll;
    /// <summary>
    /// Ritorna il numero di oggetti nella collezione 
    /// </summary>
    public int Count { get { return coll.Count; } }
    /// <summary>
    /// Inizializza una instanza della classe
    /// </summary>
    public IntCollection()
    {
        //
        // TODO: Add constructor logic here
        //
        coll = new G2Collection();
    }

    /// <summary>
    /// Aggiunge alla lista un nuovo oggetto indicando la chiave con cui riferenziarlo
    /// e l'oggetto da inserire di tipo <see cref="G2Core.AdvancedXhtmlForms.Fields.AxfField"> </see>
    /// </summary>
    /// <param name="number"></param>
    /// <param name="key"></param>    
    public void Add(int number, string key)
    {
        coll.Add(key, number);
    }

    public void Remove(string key)
    {
        coll.Remove(key);
    }

    public void Clear()
    {
        coll.Clear();
    }

    /// <summary>
    /// Permette di accedere ad un oggetto della collezione tramite un indice.
    /// </summary>
    /// <param name="i">indice della lista in base zero dell'oggetto da restituire.</param>
    /// <returns>Oggetto contenuto alla posizione specificata della lista.</returns>  
    public int this[int i]
    {
        get
        {
            int str = (int)coll[coll.Keys[i]];
            return str;
        }
    }

   /// <summary>
   /// Ritorna l'indice corrispondente alla chiave specificata.
   /// </summary>
   /// <param name="key">Chiave di cui cercare l'indice nella lista.</param>
   /// <returns>Indice relativa alla chiave cercata</returns>
    public int IndexOf(string key)
    {
        for (int i = 0; i < this.Count; i++)
        {
            if (this.coll.Keys[i] == key)
                return i;
        }
        return -1;
    }

    /// <summary>
    /// Permette di accedere ad un oggetto della collezione tramite la chiave associata 
    /// all'oggetto.
    /// </summary>
    /// <param name="key">Chiave dell'oggetto da restiture.</param>
    /// <returns>Oggetto associato alla chiave specificata.</returns>
    public int this[string key]
    {
        get
        {
            int val = (int)coll[key];
            return val;
        }
    }

    /// <summary>
    /// Indica se la lista contiene la chiave specificata
    /// </summary>
    /// <param name="key">chiave da cercare nella lista</param>
    /// <returns>Indica se la chiave � presente nella lista</returns>
    public bool Contains(string key)
    {
        return coll[key] != null;
    }

    #region Enumerator

    public IEnumerator GetEnumerator()
    {
        return new CollectionEnumerator(this);
    }

    private class CollectionEnumerator : IEnumerator
    {
        private int CurentPos = -1;
        private IntCollection Collection;
        public CollectionEnumerator(IntCollection coll)
        {
            Collection = coll;
        }
        public object Current
        {
            get
            {
                return Collection[CurentPos];
            }
        }
        public bool MoveNext()
        {
            if (CurentPos < Collection.Count - 1)
            {
                CurentPos++;
                return true;
            }
            else
                return false;
        }
        public void Reset()
        {
            CurentPos = -1;
        }
    }
    #endregion
}
