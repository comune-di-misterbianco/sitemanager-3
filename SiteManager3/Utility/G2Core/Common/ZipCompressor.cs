
using System.IO;
using ICSharpCode.SharpZipLib.Checksums;
using ICSharpCode.SharpZipLib.Zip;


namespace G2Core.Common
{
    /// <summary>
    /// Fornisce dei metodi statici di varia utilit�
    /// </summary>
    public class G2Zip
    {
        public DirectoryInfo Folder
        {
            get { return _Folder; }
            private set { _Folder = value; }
        }
        private DirectoryInfo _Folder;
        			
        public string ZipFileName
        {
            get { return _ZipFileName; }
            private set { _ZipFileName = value; }
        }
        private string _ZipFileName;

        public G2Zip(string zipFileName,string folderPath)
        {
            this.ZipFileName = zipFileName;
            Folder = new DirectoryInfo(folderPath);
        }
			
        public void Save()
        {
            //Creo l'oggetto ZipOutputStream per la creazione del file zip
            //e creo anche il file
            ZipOutputStream strmZipOutputStream = new ZipOutputStream(File.Create(ZipFileName));
            strmZipOutputStream.SetLevel(5);
            Crc32 objCrc32 = new Crc32();


            foreach (FileInfo file in Folder.GetFiles())
            {

                string fileName = file.Name;
                // nuova entry nel file ZIP
                ZipEntry newZIPEntry = new ZipEntry(fileName);

                // apre il file sorgente in lettura
                FileStream strmFile = file.OpenRead();

                // bytes da leggere e gi� letti
                int bytesRead;

                // definisco un buffer per la lettura da 200000 bytes
                byte[] mBuffer = new byte[200000];

                // azzera il CRC
                objCrc32.Reset();

                // inserisce l'entry nello ZIP
                strmZipOutputStream.PutNextEntry(newZIPEntry);

                // ciclo per la lettura del file sorgente
                while (strmFile.Position < strmFile.Length)
                {
                    bytesRead = strmFile.Read(mBuffer, 0, mBuffer.Length);
                    strmZipOutputStream.Write(mBuffer, 0, bytesRead);
                    objCrc32.Update(mBuffer, 0, bytesRead);
                }

                // imposta il CRC del nuovo file all'interno dello ZIP
                newZIPEntry.Crc = objCrc32.Value;

                // imposta le caratteristiche del nuovo file
                newZIPEntry.DateTime = File.GetCreationTime(fileName);
                newZIPEntry.Size = strmFile.Length;

                // chiude il file sorgente
                strmFile.Close();
            }

            strmZipOutputStream.Finish();
            strmZipOutputStream.Close();

        }

        public void Estract()
        {
            FastZip fz = new FastZip();
            fz.ExtractZip(ZipFileName, Folder.FullName , "");
        }
    }
}

