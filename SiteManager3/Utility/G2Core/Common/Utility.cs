using System;
using System.Data;
using NetCms.Connections;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Linq;
using System.Linq.Expressions;


namespace G2Core.Common
{
    /// <summary>
    /// Fornisce dei metodi statici di varia utilit�
    /// </summary>
    public static class Utility
    {
        /// <summary>
        /// Restituisce una stringa contenente il numero dato per parametro, ma riempito di zeri fino alla lunghezza data
        /// </summary>
        /// <param name="number">Il numero da formattare</param>
        /// <param name="lenght">La lunghezza minima della stringa</param>
        /// <returns></returns>
        public static string FormatNumber(string number, int lenght)
        {
            string value = number;

            while (value.Length < lenght)
                value = "0" + value;

            return value;
        }

        /// <summary>
        /// Restituisce una stringa contenente il numero dato per parametro, ma riempito di zeri fino al sesto carattere
        /// </summary>
        /// <param name="number">Il numero da formattare</param>
        /// <returns></returns>
        public static string FormatNumber(string number)
        {
            return FormatNumber(number, 6);
        }

        /// <summary>
        /// Restituisce le prime NumOfWords parole della stringa passata per paramentro
        /// </summary>
        /// <param name="str">La stringa da tagliare</param>
        /// <param name="NumOfWords">Il numero massimo di parole che la stringa di ritorno pu� contenere.</param>
        /// <returns>Le prime NumOfWords parole della stringa passata per paramentro</returns>
        public static string CutWords(string str, int NumOfWords)
        {
            string[] Words = str.Split(' ');
            string filteredString = "";
            for (int i = 0; i < Words.Length && i <= 10; i++)
            {
                filteredString += Words[i] + " ";
            }
            return filteredString.Trim();
        }

        /// <summary>
        /// Permette di ottenere la sotto stringa a destra della prima istanza della parola passata per parametro
        /// es: text = "Stringa da elaborare col metodo in questione", word = elaborare, risultato = "col metodo in questione"
        /// </summary>
        /// <param name="text"></param>
        /// <param name="word"></param>
        /// <returns></returns>
        public static string SubstringAtRightOf(this string text, string word)
        {
            return text.Substring(text.IndexOf(word) + word.Length, (text.Length - (text.IndexOf(word) + word.Length))).Trim();
        }

        private static string ParseWordForRegex(string word)
        {
            string res = "";
            foreach (char c in word.ToCharArray())
                res += "[" + c.ToString().ToUpper() + "|" + c.ToString().ToLower() + "]";
            return res;
        }

        /// <summary>
        /// Permette di ottenere la sotto stringa a sinistra della prima istanza della parola passata per parametro
        /// es: text = "Stringa da elaborare col metodo in questione", word = elaborare, risultato = "Stringa da "
        /// </summary>
        /// <param name="text"></param>
        /// <param name="word"></param>
        /// <returns></returns>
        public static string SubstringAtLeftOf(this string text, string word)
        {
            return text.Substring(0, text.IndexOf(word)).Trim();
        }

        /// <summary>
        /// Permette di ottenere la sotto stringa tra le due parole passate per parametro.
        /// es: text = "Stringa da elaborare col metodo in questione", from = da, to = in, risultato = "elaborare col metodo"
        /// </summary>
        /// <returns></returns>
        public static string SubstringAtBetween(this string text, string from, string to)
        {
            return text.SubstringAtLeftOf(to).SubstringAtRightOf(from).Trim();
        }

        public static WebControl GetMessageDivControl(string legend, string message)
        {
            G2Core.XhtmlControls.Fieldset output = new G2Core.XhtmlControls.Fieldset(legend);
            Panel div = new Panel();
            HtmlGenericControl messagediv = new HtmlGenericControl();
            messagediv.InnerHtml = message;
            output.Controls.Add(messagediv);
            div.Controls.Add(output);
            return div;
        }

        public static System.Web.UI.HtmlControls.HtmlGenericControl GetDetailViewControl(string Label, string Text)
        {
            HtmlGenericControl ctr = new HtmlGenericControl("p");
            ctr.InnerHtml = "<strong>" + Label + ":</strong> " + (Text != null ? Text : "");
            return ctr;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        public static void CopyRows(DataTable source, DataTable destination)
        {
            if (source != null)
            {
                foreach (DataRow row in source.Rows)
                {
                    DataRow newrow = destination.NewRow();
                    foreach (DataColumn col in destination.Columns)
                    {
                        if (source.Columns.Contains(col.ColumnName))
                            newrow[col.ColumnName] = row[col.ColumnName];

                    }
                    destination.Rows.Add(newrow);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="conversionType"></param>
        /// <returns></returns>
        public static object ChangeType(object value, Type conversionType)
        {
            object obj = value;
            if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                {
                    return null;
                }
                System.ComponentModel.NullableConverter nullableConverter = new System.ComponentModel.NullableConverter(conversionType);
                conversionType = nullableConverter.UnderlyingType;
            }            
                obj = Convert.ChangeType(value, conversionType);
            return obj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string Encrypt(string text)
        {
            string stringKey = "!33!SERUYT";
            try
            {
                byte[] key = { };
                byte[] IV = { 38, 55, 206, 48, 28, 64, 20, 16 };
                key = System.Text.Encoding.UTF8.GetBytes(stringKey.Substring(0, 8));
                System.Security.Cryptography.DESCryptoServiceProvider des = new System.Security.Cryptography.DESCryptoServiceProvider();
                Byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(text);
                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                System.Security.Cryptography.CryptoStream cryptoStream = new System.Security.Cryptography.CryptoStream(memoryStream, des.CreateEncryptor(key, IV), System.Security.Cryptography.CryptoStreamMode.Write);
                cryptoStream.Write(byteArray, 0, byteArray.Length);
                cryptoStream.FlushFinalBlock();
                return Convert.ToBase64String(memoryStream.ToArray());
            }
            catch (Exception ex)
            {
                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(0, ex.Message, "Utility", NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non � stato possibile criptare la stringa", "", "", "64");
            }
            return string.Empty;
        }

        public static string Decrypt(string text)
        {
            string stringKey = "!33!SERUYT";
            try
            {
                byte[] key = { };
                byte[] IV = { 38, 55, 206, 48, 28, 64, 20, 16 };
                key = System.Text.Encoding.UTF8.GetBytes(stringKey.Substring(0, 8));
                System.Security.Cryptography.DESCryptoServiceProvider des = new System.Security.Cryptography.DESCryptoServiceProvider();
                Byte[] byteArray = Convert.FromBase64String(text);
                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                System.Security.Cryptography.CryptoStream cryptoStream = new System.Security.Cryptography.CryptoStream(memoryStream, des.CreateDecryptor(key, IV), System.Security.Cryptography.CryptoStreamMode.Write);
                cryptoStream.Write(byteArray, 0, byteArray.Length);
                cryptoStream.FlushFinalBlock();
                return System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
            }
            catch (Exception ex)
            {
                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(0, ex.Message, "Utility", NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Non � stato possibile decriptare la stringa", "", "", "65");
            }
            return string.Empty;
        }

        public static string RemoveCDATA(string text)
        {
            if (text == null)
                return "";

            string str = text.Replace("<![CDATA[", "");
            str = str.Replace("]]>", "");

            return str;
        }

        public static DateTime? ParseGeneralizedTime(string time)
        {
            string mask = "";
            switch (time.Length)
            {

                case 13:
                    // RFC3280: 4.1.2.5.1  UTCTime
                    int year = Convert.ToInt16(time.Substring(0, 2));
                    if (year >= 50)
                        time = "19" + time;
                    else
                        time = "20" + time;
                    mask = "yyyyMMddHHmmssZ";
                    break;
                case 14:
                    mask = "yyyyMMddHHmmss";
                    break;
                case 15:
                    mask = "yyyyMMddHHmmssZ";
                    break;
                case 18:
                    mask = "yyyyMMddHHmmss.fff";
                    break;
            }
            try
            {
                return DateTime.ParseExact(time, mask, null);
            }
            catch (Exception)
            {

                return null;
            }


        }

        public static bool GetDatafromCodiceFiscale(string codicefiscale, out DateTime? birth, out string sesso, out string codCatastaleComune)
        {
            birth = null;
            sesso = "M";
            codCatastaleComune = "";
            string _day = "";
            string _month = "";
            string _year = "";
            string monthMap = "ABCDEHLMPRST";
            bool calculated = false;
            if (codicefiscale.Length == 16)
            {
                try
                {
                    _year = codicefiscale.Substring(6, 2);
                    if (DateTime.Now.Year >= Convert.ToInt16("20" + _year))
                        _year = "20" + _year;
                    else
                        _year = "19" + _year;

                    _month = (monthMap.IndexOf(codicefiscale.Substring(8, 1).ToUpper()) + 1).ToString();
                    _day = codicefiscale.Substring(9, 2);
                    if (Convert.ToInt16(_day) > 40)
                    {
                        _day = (Convert.ToInt16(_day) - 40).ToString();
                        sesso = "F";
                    }
                    birth = new DateTime(Convert.ToInt16(_year), Convert.ToInt16(_month), Convert.ToInt16(_day));
                    codCatastaleComune = codicefiscale.Substring(11, 4);
                    calculated = true;
                }
                catch (Exception ex)
                {

                    calculated = false;
                }

            }
            return calculated;
        }

        public static string GeneratePassword(int Length)
        {
            char[] ValidCharacters = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            string password = string.Empty;
            Random RndGenerator = new Random();
            for (int i = 0; i < Length; i++)
            {
                int x = RndGenerator.Next(1, ValidCharacters.Length);
                if (!password.Contains(ValidCharacters.GetValue(x).ToString()))
                {
                    password += ValidCharacters.GetValue(x);
                }
                else
                {
                    i--;
                }
            }
            return password;
        }

        public static string RemoveEmptyValue(string originalstring, char splitcharacter)
        {
            string result = "";
            string[] stringArray = originalstring.Split(splitcharacter);
            for (int i = 0; i < stringArray.Length; i++)
            {
                if (!String.IsNullOrEmpty(stringArray[i]))
                {
                    result += stringArray[i];
                    result += splitcharacter;
                }
            }
            if (result.Length > 1)
                result = result.Substring(0, result.Length - 1);
            return (result);
        }

        public static void CreateMessageAlert(System.Web.UI.Control control, string alertMsg, string alertKey)
        {
            alertMsg = alertMsg.Replace("'", "\'");
            string script = "alert(\"" + alertMsg + "\");\n";                             
            System.Web.UI.ScriptManager.RegisterStartupScript(control, control.GetType(), alertKey, script, true);
        }

        public static string RenderControl(System.Web.UI.Control ctrl)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter tw = new System.IO.StringWriter(sb);
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

            ctrl.RenderControl(hw);
            return sb.ToString();
        }

        public static string ValidateTextValue(string value)
        {
            value = value.Trim();
            return value = "'" + value.Replace("'", "''") + "'";
        }

        public static string CleanString(string value, string separator)
        {
            char[] chars = value.ToCharArray();
            string newValue = "";

            //MAIUSCOLE 65/90
            //MINUSCOLE 97/122
                        
            foreach (char c in chars)
            {
                char charValue = c;//char.GetNumericValue(c);
                if ((charValue >= 65 && charValue <= 90) || (charValue >= 97 && charValue <= 122) || (charValue >= 48 && charValue <= 57))
                    newValue += c;
                else newValue += separator;
            }

            while (newValue.Contains(separator + separator))
                newValue = newValue.Replace(separator + separator, separator);

            while (newValue.StartsWith(separator))
                newValue = newValue.Substring(1, newValue.Length - 1);

            while (newValue.EndsWith(separator))
                newValue = newValue.Substring(0, newValue.Length - 1);

            return newValue;
        }
        public static string CleanString(string value)
        {
            return CleanString(value, " ");
        }
        public static string CleanKey(string value)
        {
            return CleanString(value, "-").ToLower();
        }

        /// <summary>
        /// Controlla se una data � compresa in un intervallo di tempo
        /// </summary>
        /// <param name="date">data da controllare</param>
        /// <param name="startRange">data inizio intervallo</param>
        /// <param name="endRange">data fine intervallo</param>
        /// <returns>vero/falso</returns>
        public static bool IsDateWithingRange(DateTime date, DateTime startRange, DateTime endRange)
        {
            return ((DateTime.Compare(date, startRange) >= 0 && DateTime.Compare(date, endRange) <= 0));  
        }

        /// <summary>
        /// Calcolo dell'eta in base ad una data specifica
        /// </summary>
        /// <param name="dataRiferimento"></param>
        /// <returns></returns>
        public static int CalcolaEta(DateTime DataNascita, DateTime dataRiferimento)
        {
            //Effettuo un primo calcolo dell'et� sottraendo semplicemente l'anno di riferimento con l'anno di nascita
            int eta = dataRiferimento.Year - DataNascita.Year;

            //Decremento il calcolo precedente qualora il mese del compleanno sia maggiore del mese di riferimento o se uguale, considero il giorno di compleanno rispetto al giorno corrente
            if (DataNascita.Month > dataRiferimento.Month || (DataNascita.Month == dataRiferimento.Month && DataNascita.Day > dataRiferimento.Day))
                eta--;

            return eta;
        }

        /// <summary>
        /// Metodo che estende l'ordinamento LINQ per effettuare un ordinamento specificando la propriet� tramite una stringa
        /// e l'orientamento tramite booleano. In ingresso ci sar� una collezione IQueryable di oggetti che contengono il nome
        /// della propriet� passata come parametro
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="propertyName"></param>
        /// <param name="asc"></param>
        /// <returns></returns>
        public static IQueryable<T> OrderBy<T>(
         this IQueryable<T> source, string propertyName, bool asc)
        {
            var type = typeof(T);
            string methodName = asc ? "OrderBy" : "OrderByDescending";
            var property = type.GetProperty(propertyName);
            var parameter = Expression.Parameter(type, "p");
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExp = Expression.Lambda(propertyAccess, parameter);
            MethodCallExpression resultExp = Expression.Call(typeof(Queryable), methodName,
                              new Type[] { type, property.PropertyType },
                              source.Expression, Expression.Quote(orderByExp));
            return source.Provider.CreateQuery<T>(resultExp);
        }
    }
}
