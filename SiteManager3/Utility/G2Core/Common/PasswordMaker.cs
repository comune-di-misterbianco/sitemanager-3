using System;
using System.Data;
using NetCms.Connections;
using System.Security.Cryptography;
using System.Linq;

namespace G2Core.Common
{
    public class PasswordGenerator
    {
        public int Lenght
        {
            get { return _Lenght; }
            private set { _Lenght = value; }
        }
        private int _Lenght;


        public PasswordGenerator(int PasswordLenght)
        {
            this.Lenght = PasswordLenght;
        }

        /// <summary>
        /// Metodo per generazione passaword con caratteri speciali
        /// </summary>
        /// <returns>Una stringa contenente la password</returns>
        public string Generate()
        {
            string password = "";

            string PASSWORD_CHARS_LCASE = "abcdefgijkmnopqrstwxyz";
            string PASSWORD_CHARS_UCASE = "ABCDEFGHJKLMNPQRSTWXYZ";
            string PASSWORD_CHARS_NUMERIC = "0123456789";            
            string PASSWORD_CHARS_SPECIAL = "#@!";

            RandomNumbers ran = new RandomNumbers();
            int offset = ran.Next(0, 4);
            for (int i = offset; i < Lenght + offset; i++)
            {
                Random Random = new Random(DateTime.Now.Millisecond);
                switch (i % 4)
                {
                    case 0: password += PASSWORD_CHARS_LCASE[ran.Next(0, PASSWORD_CHARS_LCASE.Length - 1)]; break;
                    case 1: password += PASSWORD_CHARS_UCASE[ran.Next(0, PASSWORD_CHARS_UCASE.Length - 1)]; break;
                    case 2: password += PASSWORD_CHARS_NUMERIC[ran.Next(0, PASSWORD_CHARS_NUMERIC.Length - 1)]; break;
                    case 3: password += PASSWORD_CHARS_SPECIAL[ran.Next(0, PASSWORD_CHARS_SPECIAL.Length - 1)]; break;
                }
            }

            return password;
        }

        /// <summary>
        /// Metodo alternativo per generazione password semplificata
        /// </summary>
        /// <param name="passLength">Lunghezza massima della password</param>
        /// <returns>Una stringa contenente la password</returns>
        public string Generate(int passLength)
        {
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, passLength)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }
    }


    /// <summary>
    /// Numeri pseudo-casuali e cryptographically-strong
    /// </summary>
    public class RandomNumbers
    {
        private RNGCryptoServiceProvider _rand = new RNGCryptoServiceProvider();
        private byte[] _rd4 = new byte[4], _rd8 = new byte[8];

        /// <summary>
        /// Genera un numero intero positivo casuale minore di max
        /// </summary>
        /// <param name="max">Limite superiore del numero casuale generato</param>
        /// <returns>Il numero casuale generato</returns>
        public int Next(int max)
        {
            if (max <= 0)
            {
                Exception ex = new ArgumentOutOfRangeException("Max");
                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Warning, ex);*/
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, ex.Message, "", "", "");
            }
            _rand.GetBytes(_rd4);
            int val = BitConverter.ToInt32(_rd4, 0) % max;
            if (val < 0) val = -val;
            return val;
        }

        /// <summary>
        /// Genera un numero intero positivo casuale compreso tra min e max
        /// </summary>
        /// <param name="min">Valore minimo possibile</param>
        /// <param name="max">Valore massimo possibile</param>
        /// <returns>Il numero casuale generato</returns>
        public int Next(int min, int max)
        {
            if (min > max)
            {
                Exception ex = new ArgumentOutOfRangeException();
                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Warning, ex);*/
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, ex.Message, "", "", "");
            }
            return Next(max - min + 1) + min;
        }

        /// <summary>
        /// Genera un numero (double) compreso tra 0.0 e 1.1
        /// </summary>
        /// <returns>Il numero casuale generato</returns>
        public double NextDouble()
        {
            _rand.GetBytes(_rd8);
            return BitConverter.ToUInt64(_rd8, 0) / (double)UInt64.MaxValue;
        }

    }
}