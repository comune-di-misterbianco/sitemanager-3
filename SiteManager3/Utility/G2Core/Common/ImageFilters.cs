using System;
using System.Data;
using System.Drawing.Imaging;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace G2Core.Common
{
    /// <summary>
    /// La classe ImageFilters esegue il resize di una immagine, creando nella cartella indicata dalla proprietÓ <c>FilePathInformation</c>
    /// una miniatura e una immagine di dimensioni ridotte rispetto all'originale.
    /// </summary>
    public class ImageFilters
    {
        /// <summary>
        /// Ottiene la larghezza dell'immagine
        /// </summary>
        public int Width
        {
            get { return Image.Width; }
        }
        // Ottiene l'altezza dell'immagine
        public int Height
        {
            get { return Image.Height; }
        }

        /// <summary>
        /// Ottiene la larghezza dell'immagine miniatura
        /// </summary>
        public int ThumbWidth
        {
            get { return _ThumbWidth; }
        }
        /// <summary>
        /// Ottiente l'altezza dell'immagine miniatura
        /// </summary>
        public int ThumbHeight
        {
            get { return _ThumbHeight; }
        }

        private int _ThumbWidth;
        private int _ThumbHeight;

        /// <summary>
        /// Ottiene il nome file dell'immagine miniatura 
        /// </summary>
        public string ThumbName
        {
            get
            {
                if (_NameOverride == null)
                {
                    string _ThumbName = TargetPath.JustName;

                    if (NameOffset.Length > 0)
                        _ThumbName += "_" + NameOffset;

                    _ThumbName += "." + TargetPath.Extension;

                    return _ThumbName;
                }
                return _NameOverride;
            }
        }

        /// <summary>
        /// Ottiene o imposta il nome file dell'immagine
        /// </summary>
        private string _NameOverride;
        public string NameOverride
        {
            get
            {
                return _NameOverride;
            }
            set { _NameOverride = value; }
        }

        private string _NameOffset;
        /// <summary>
        /// Ottiene o imposta il suffiso al nome immagine
        /// </summary>
        public string NameOffset
        {
            get
            {
                if (_NameOffset == null)
                    _NameOffset = "tmb";
                return _NameOffset;
            }
            set { _NameOffset = value; }
        }

        private System.Drawing.Image Image;

        private FilePathInformation SourcePath;
        private FilePathInformation _TargetPath;
        
        /// <summary>
        /// Ottiene o imposta l'oggetto <c>FilePathInformation</c> che racchiude i riferiementi al percorso delle cartella sul server dove si trova l'immagine.
        /// </summary>
        public FilePathInformation TargetPath
        {
            get
            {
                return _TargetPath;
            }
            set
            {
                _TargetPath = value;
            }
        }
        /// <summary>
        /// Inizializzazione dell'istanza dell'oggetto ImageFilters
        /// </summary>
        /// <param name="path">Percorso delle cartella sul server dove si trova l'immagine</param>
        public ImageFilters(string path)
        {
            SourcePath = new FilePathInformation(path);
            _TargetPath = SourcePath;
            Image = System.Drawing.Image.FromFile(path);
        }

        /// <summary>
        /// Il metodo consente il ridimensionamento dell'immagine in modo proporzionale alla larghezza indicata 
        /// </summary>
        /// <param name="NewWidthSize">La nuova larghezza che l'immagine assumerÓ</param>
        /// <returns>La nuova immagine ridimensionata</returns>
        public System.Drawing.Image ResizeOnWidth(int NewWidthSize)
        {
            int WX = NewWidthSize;
            int HX = Image.Height * WX / Image.Width;

            _ThumbWidth = WX;
            _ThumbHeight = HX;

            return Resize(WX, HX);
        }

        /// <summary>
        /// Il metodo consente il ridimensionamento dell'immagine in modo proporzionale all'altezza indicata 
        /// </summary>
        /// <param name="NewWidthSize">La nuova altezza che l'immagine assumerÓ</param>
        /// <returns>La nuova immagine ridimensionata</returns>
        public System.Drawing.Image ResizeOnHeight(int NewHeigthSize)
        {
            int HX = NewHeigthSize;
            int WX = Image.Width * HX / Image.Height;

            _ThumbWidth = WX;
            _ThumbHeight = HX;

            return Resize(WX, HX);
        }

        /// <summary>
        /// Il metodo consente il ridimensionamento dell'immagine
        /// </summary>
        /// <param name="Width">La nuova larghezza</param>
        /// <param name="Heigth">La nuova altezza</param>
        /// <returns>La nuova immagine ridimensionata</returns>
        public System.Drawing.Image Resize(int Width, int Heigth)
        {
            System.Drawing.Image.GetThumbnailImageAbort dummyCallBack = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback);
            System.Drawing.Image thumbNailImg = Image.GetThumbnailImage(Width, Heigth, dummyCallBack, IntPtr.Zero);

            _ThumbWidth = Width;
            _ThumbHeight = Height;

            return thumbNailImg;
        }

        /// <summary>
        /// Il metodo crea l'immagine miniatura in funzione della misura massima
        /// </summary>
        /// <param name="MaxSize">Misura massima</param>
        public void CreateThumbnail(int MaxSize)
        {
            System.Drawing.Image Thumbnail;
            if (Width > Height)
                Thumbnail = ResizeOnWidth(MaxSize);
            else
                Thumbnail = ResizeOnHeight(MaxSize);
            Save(Thumbnail);
        }

        /// <summary>
        ///  Il metodo crea l'immagine miniatura in modo proporzionale alla larghezza indicata
        /// </summary>
        /// <param name="NewWidthSize">Larghezza massima della miniatura</param>
        public void CreateThumbnailWidth(int NewWidthSize)
        {
            System.Drawing.Image Thumbnail = ResizeOnWidth(NewWidthSize);
            Save(Thumbnail);
        }

        /// <summary>
        ///  Il metodo crea l'immagine miniatura in modo proporzionale all'altezza indicata
        /// </summary>
        /// <param name="NewHeigthSize">Altezza massima della miniatura</param>
        public void CreateThumbnailHeight(int NewHeigthSize)
        {
            System.Drawing.Image Thumbnail = ResizeOnHeight(NewHeigthSize);
            Save(Thumbnail);
        }

        /// <summary>
        /// Il metodo crea l'immagine miniatura
        /// </summary>
        /// <param name="Width">Larghezza massima della miniatura</param>
        /// <param name="Heigth">Altezza massima della miniatura</param>
        public void CreateThumbnail(int Width, int Heigth)
        {
            System.Drawing.Image Thumbnail = Resize(Width, Heigth);
            Save(Thumbnail);
        }

        /// <summary>
        /// Il metodo ricava il tipo codifica dell'immagine in funzione del tipo mime passato
        /// </summary>
        /// <param name="MimeType">Tipo di immagine</param>
        /// <returns>Restituisce l'oggetto ImageCodecInfo contenente informazioni sui codificatori di immagini incorporati in GDI+.</returns>
        public ImageCodecInfo GetEncoderInfo(string MimeType)
        {
            ImageCodecInfo[] encoders = ImageCodecInfo.GetImageEncoders();
            for (int j = 0; j < encoders.Length; j++)
            {
                if (encoders[j].MimeType == MimeType)
                {
                    return encoders[j];
                }
            }
            return null;
        }
        /// <summary>
        /// Salva l'immagine sul disco
        /// </summary>
        public void Save()
        {
            Save(Image);
        }

        /// <summary>
        /// Salva in jpeg l'immagine 
        /// </summary>
        /// <param name="imageToSave">Immagine System.Drawing.Image </param>
        public void Save(System.Drawing.Image imageToSave)
        {
            EncoderParameters encoderParams = new EncoderParameters();
            long quality = new long();
            quality = 100;

            EncoderParameter encoderParam = new EncoderParameter(Encoder.Quality, quality);
            encoderParams.Param[0] = encoderParam;
            ImageCodecInfo[] arrayICI = ImageCodecInfo.GetImageEncoders();
            ImageCodecInfo jpegICI = null;

            for (int x = 0; x < arrayICI.Length - 1; x++)
            {
                if ((arrayICI[x].FormatDescription.Equals("JPEG")))
                {
                    jpegICI = arrayICI[x];
                    break;
                }
            }


            imageToSave.Save(TargetPath.Path + ThumbName, jpegICI, encoderParams);
            //imageToSave.Dispose();

        }
        /// <summary>
        /// Salva l'immagine con il nome indicato
        /// </summary>
        /// <param name="filename"></param>
        public void SaveAs(string filename)
        {
            Image.Save(TargetPath.Path + filename, Image.RawFormat);
        }

       
        public bool ThumbnailCallback()
        {
            return false;
        }
    }

    /// <summary>
    /// La classe FilePathInformation implementa un oggetto che gestisce file
    /// </summary>
    public class FilePathInformation
    {
        private string _Extension;
        /// <summary>
        /// Ottiene l'estensione del file
        /// </summary>
        public string Extension
        {
            get
            {
                if (_Extension == null)
                {
                    int start = FullName.LastIndexOf('.') + 1;
                    int end = (FullName.Length - FullName.LastIndexOf('.')) - 1;
                    _Extension = FullName.Substring(start, end);
                }
                return _Extension;
            }
        }

        private string _FullName;
        /// <summary>
        /// Restituisce il nome completo del file
        /// </summary>
        public string FullName
        {
            get
            {
                if (_FullName == null)
                {
                    int ExtractPos = FullPath.LastIndexOf("\\");
                    int start = ExtractPos + 1;
                    int end = (FullPath.Length - ExtractPos - 1);
                    _FullName = FullPath.Substring(start, end);
                }
                return _FullName;
            }
        }

        private string _JustName;
        /// <summary>
        /// Restituisce il nome del file privo di estensione
        /// </summary>
        public string JustName
        {
            get
            {
                if (_JustName == null)
                {
                    int start = 0;
                    int end = FullName.LastIndexOf('.');
                    _JustName = FullName.Substring(start, end);
                }
                return _JustName;
            }
        }

        private string _Path;
        /// <summary>
        /// Restituisce il percorso del file escludendone il nome
        /// </summary>
        public string Path
        {
            get
            {
                if (_Path == null)
                {
                    int ExtractPos = FullPath.LastIndexOf("\\") + 1;
                    _Path = FullPath.Substring(0, ExtractPos);
                }
                return _Path;
            }
        }

        private string _FullPath;
        /// <summary>
        /// Restituisce il percorso completo del file
        /// </summary>
        public string FullPath
        {
            get
            {
                return _FullPath;
            }
        }
        /// <summary>
        /// Inizializzazione dell'istanza del oggetto FilePathInformation
        /// </summary>
        /// <param name="path">Percorso del file</param>
        public FilePathInformation(string path)
        {
            _FullPath = path;
        }
    }
}