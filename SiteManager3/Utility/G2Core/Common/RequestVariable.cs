using System;
using System.Web;
using System.Collections.Specialized;

namespace G2Core.Common
{
    /// <summary>
    /// Permette di leggere i valori di HTTP valori HTTP inviati da un client durante una richiesta Web. 
    /// tramite la classe System.Web.HttpRequest
    /// </summary>
    public class RequestVariable
    {
        private DateTime NullDate = new DateTime(0001, 1, 1);
        private string RequestedVariable;
        /// <summary>
        /// Restituisce il valore della variabile HTTP
        /// </summary>
        public string OriginalValue
        {
            get
            {
                return RequestedVariable;
            }
        }
        /// <summary>
        /// Lista del tipo di variabile
        /// </summary>
        public enum VariableType
        {
            Integer,
            String,
            Date
        }

        /// <summary>
        /// Tipo di collezione di variabili HTTP in cui cercare la variabile
        /// </summary>
        public enum RequestType
        {
            Form,
            QueryString,
            Form_QueryString,
            QueryString_Form        
        }
        private string _Key;
        /// <summary>
        /// Chiave con cui cercare la variabile
        /// </summary>
        public string Key
        {
            get { return _Key; }
        }
	

        private bool ValidContent;
        /// <summary>
        /// Inizializza un'istanza della classe.
        /// </summary>
        /// <param name="key">Chiave con cui cercare la variabile</param>
        public RequestVariable(string key)
            :this(key,RequestType.Form)
        {
           
        }

        /// <summary>
        /// Inizializza un'istanza della classe.
        /// </summary>
        /// <param name="key">Chiave con cui cercare la variabile</param>
        /// <param name="Request">Oggetto HttpRequest contenente la variabile </param>
        public RequestVariable(string key,HttpRequest Request)
        {
            _Key = key;
            if (Request != null && Request.Form != null && Request.Form[key] != null && key != null && key.Length > 0)
            {
                RequestedVariable = Request.Form[key];
                ValidContent = true;
            }
            else
                if (Request != null && Request.QueryString != null && Request.QueryString[key] != null && key != null && key.Length > 0)
                {
                    RequestedVariable = Request.QueryString[key];
                    ValidContent = true;
                }
                else
                    ValidContent = false;
        }
        /// <summary>
        /// Inizializza un'istanza della classe.
        /// </summary>
        /// <param name="key">Chiave con cui cercare la variabile</param>
        /// <param name="requestType">Tipo di collezione di variabili HTTP</param>
        public RequestVariable(string key, RequestType requestType)
        {
            _Key = key;
            #region RequestType.Form
            if (requestType == RequestType.Form)
            {
                NameValueCollection Coll = HttpContext.Current.Request.Form;
                if (Coll != null && Coll[key] != null && key != null && key.Length > 0)
                {
                    RequestedVariable = Coll[key];
                    ValidContent = true;
                }
                else
                    ValidContent = false;
            } 
            #endregion

            #region RequestType.QueryString
            if (requestType == RequestType.QueryString)
            {
                NameValueCollection Coll = HttpContext.Current.Request.QueryString;
                if (Coll != null && Coll[key] != null && key != null && key.Length > 0)
                {
                    RequestedVariable = Coll[key];
                    ValidContent = true;
                }
                else
                    ValidContent = false;
            }
            #endregion

            #region RequestType.Form_QueryString

            if (requestType == RequestType.Form_QueryString)
            {
                HttpRequest Request = HttpContext.Current.Request;
                if (Request != null && Request.Form != null && Request.Form[key] != null && key != null && key.Length > 0)
                {
                    RequestedVariable = Request.Form[key];
                    ValidContent = true;
                }
                else
                    if (Request != null && Request.QueryString != null && Request.QueryString[key] != null && key != null && key.Length > 0)
                    {
                        RequestedVariable = Request.QueryString[key];
                        ValidContent = true;
                    }
                    else
                        ValidContent = false;
            }

            #endregion

            #region RequestType.QueryString_Form

            if (requestType == RequestType.QueryString_Form)
            {
                HttpRequest Request = HttpContext.Current.Request;
                if (Request != null && Request.QueryString != null && Request.QueryString[key] != null && key != null && key.Length > 0)
                {
                    RequestedVariable = Request.QueryString[key];

                    ValidContent = true;
                }
                else
                    if (Request != null && Request.Form != null && Request.Form[key] != null && key != null && key.Length > 0)
                    {
                        RequestedVariable = Request.Form[key];
                        ValidContent = true;
                    }
                    else
                        ValidContent = false;
            }

            #endregion
        }  
        
        /// <summary>
        /// Inizializza un'istanza della classe.
        /// </summary>
        /// <param name="key">Chiave con cui cercare la variabile</param>
        /// <param name="Coll">collezione di variabili</param>
        public RequestVariable(string key, NameValueCollection Coll)
        {
            _Key = key;
            if (Coll != null && Coll[key] != null && key != null && key.Length > 0)
            {
                RequestedVariable = Coll[key];
                ValidContent = true;
            }
            else
                ValidContent = false;
        }

        /// <summary>
        /// Indica se � avvenuta una postback.
        /// </summary>
        public bool IsPostBack
        {
            get
            {
                if (HttpContext.Current.Request.Form[this.Key] != null)
                    return true;
                return false;
            }
        }

        /// <summary>
        /// Indica se il valore della variabile � un intero.
        /// </summary>
        public bool IsValidInteger
        {
            get
            {
                return IsValid(VariableType.Integer);
            }
        }

        /// <summary>
        /// Indica se il valore della variabile � una stringa.
        /// </summary>
        public bool IsValidString
        {
            get
            {
                return IsValid(VariableType.String) && this.StringValue.Length>0;
            }
        }

        /// <summary>
        /// Indica se il valore della variabile � valido come stringa.
        /// </summary>
        public bool IsValid()
        {
            return IsValid(VariableType.String);
        }

        /// <summary>
        /// Indica se il valore della variabile � del tipo specificato.
        /// </summary>
        /// <param name="type">Tipo di variabile con cui confrontare il valore della variabile</param>
        /// <returns>Indica se la variabile � del tipo indicato</returns>
        public bool IsValid(VariableType type)
        {
            switch (type)
            {
                case VariableType.Date:
                    GetAsDate();
                    return DateParsed;
                case VariableType.String:
                    GetAsString();
                    return StringParsed;
                case VariableType.Integer:
                    GetAsInt();
                    return IntParsed;
            }
            return false;
        }
        
        #region Int
        /// <summary>
        /// Restituisce il valore della variabile come tipo int.
        /// </summary>
        /// <returns></returns>
        public int GetAsInt()
        {
            int value = 0;
            IntParsed = int.TryParse(OriginalValue, out value);
            return value;
        }
        private bool IntParsed;

        /// <summary>
        /// Restituisce il valore della variabile come tipo int.
        /// </summary>
        /// <returns></returns>
        public int IntValue
        {
            get
            {
                if (_IntValue == 0)
                    _IntValue = GetAsInt();
                return _IntValue;
            }

        }
        private int _IntValue;
        #endregion

        #region String

        /// <summary>
        /// Restituisce il valore della variabile come tipo string.
        /// </summary>
        /// <returns></returns>
        public string GetAsString()
        {
            StringParsed = OriginalValue != null;
            if (StringParsed)
                return OriginalValue.Trim().Replace("'", "''");
            else 
                return "";
        }
        private bool StringParsed;
        /// <summary>
        /// Restituisce il valore della variabile come tipo string.
        /// </summary>
        /// <returns></returns>
        public string StringValue
        {
            get
            {
                if (_StringValue == null)
                    _StringValue = GetAsString();
                return _StringValue;
            }

        }
        private string _StringValue;

        #endregion

        #region DateTime

        /// <summary>
        /// Restituisce il valore della variabile come tipo DateTime.
        /// </summary>
        /// <returns></returns>
        private DateTime GetAsDate()
        {
            DateTime value = new DateTime(1, 1, 1);
            DateParsed = OriginalValue!=null && OriginalValue.Trim().Length > 0 && DateTime.TryParse(OriginalValue, out value);
            if (!DateParsed)
                value = new DateTime(1, 1, 1);
            return value;
        }
        private bool DateParsed;
        /// <summary>
        /// Restituisce il valore della variabile come tipo DateTime.
        /// </summary>
        /// <returns></returns>
        public DateTime DateTimeValue
        {
            get
            {
                if (_DateTimeValue == null || _DateTimeValue == NullDate)
                    _DateTimeValue = GetAsDate();
                return _DateTimeValue;
            }

        }
        private DateTime _DateTimeValue;

        #endregion

        /// <summary>
        /// Restituisce il valore della variabile come tipo string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (IsValid())
                return StringValue;
            else return "";
        }
    }
    /// <summary>
    /// Restituisce un oggetto di tipo G2Core.common.RequestVariable specifico per la collezione di variabili 
    /// HTTP Form.
    /// </summary>
    public class FormRequestVariable:RequestVariable
    {
        /// <summary>
        /// Inizializza una nuova istanza della classe.
        /// </summary>
        /// <param name="key">Chiave relativa alla variabile.</param>
        public FormRequestVariable(string key)
            : base(key, RequestType.Form)
        {

        }
       
    }

    /// <summary>
    /// Restituisce un oggetto di tipo G2Core.common.RequestVariable specifico per la collezione di variabili 
    /// HTTP QueryString.
    /// </summary>
    public class QueryStringRequestVariable : RequestVariable
    {
        /// <summary>
        /// Inizializza una nuova istanza della classe.
        /// </summary>
        /// <param name="key">Chiave relativa alla variabile.</param>
        public QueryStringRequestVariable(string key)
            : base(key, RequestType.QueryString)
        {

        }

    }
}
