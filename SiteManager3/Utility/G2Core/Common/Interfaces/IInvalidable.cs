﻿
using System;
using System.Data;

namespace G2Core.Interfaces
{
    public interface IInvalidable
    {
        void Invalidate(DataRow row);
    }    
}
