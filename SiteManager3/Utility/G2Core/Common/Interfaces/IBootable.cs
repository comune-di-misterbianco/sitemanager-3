﻿using System;
namespace G2Core.Interfaces
{
    public interface IBootable
    {
        void Boot();
        IBootable[] ChildBootableObjects { get; }
    }
}