using System;
using System.Reflection;

namespace G2Core.Interfaces
{
    public class InferfaceImplementationChecker
    {
        public static bool ImplementInterface(object objectToCheck, string interfaceToFind)
        {
            System.Type type = objectToCheck.GetType();
            TypeFilter myFilter = new TypeFilter(MyInterfaceFilter);
            return type.FindInterfaces(myFilter, interfaceToFind).Length == 1;
        }
        private static bool MyInterfaceFilter(Type typeObj, Object criteriaObj)
        {
            return typeObj.ToString() == criteriaObj.ToString();
        }     
    }
}