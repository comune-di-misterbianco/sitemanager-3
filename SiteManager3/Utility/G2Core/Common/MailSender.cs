﻿
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace G2Core.Common
{
    /// <summary>
    /// La classe MailSender consente di inviare messaggi di posta elettronica attraverso un server SMTP.
    /// </summary>
    public class MailSender
    {
        public string EmailTo { get; private set; }
        public string EmailCC { get; private set; }
        public string Subject { get; private set; }
        public string Body { get; private set; }
        public string EmailFrom { get; private set; }
        public string LblMittente { get; private set; }

        /// <summary>
        /// Indirizzo del server SMTP. Accetta anche indirizzi IP.
        /// </summary>    
        public string SetSmtpHost { get; set; }

        /// <summary>
        /// Numero della porta usata dal server SMTP
        /// </summary>
        public int SetSmtpPort { get; set; }

        /// <summary>
        /// Username account smtp server
        /// </summary>
        public string SmtpUser { get; set; }

        /// <summary>
        /// Password account smtp server
        /// </summary>
        public string SmtpPassword { get; set; }

        /// <summary>
        /// Abilitazione Protocollo SSL
        /// </summary>
        public bool EnableSsl { get; set; }      
       
        /// <summary>
        /// Disabilita la convalida del certificato del server smtp - da non usare in produzione - ma solo in casi in cui il certificato e self signed (autogenerato)
        /// </summary>
        public bool DisableServerCertificateValidation 
        {
            get { return _DisableServerCertificateValidation; }
            set { _DisableServerCertificateValidation = value; } 
        }
        private bool _DisableServerCertificateValidation = false;

        /// <summary>
        /// Abilitazione Invio come HTML (di default a false per garantire retrocompatibilità
        /// </summary>
        public bool SendAsHtml
        {
            get { return _SendAsHtml; }
            set { _SendAsHtml = value; } 
        }
        private bool _SendAsHtml = false;

        /// <summary>
        /// Inizializzazione di una nuova istanza
        /// </summary>
        /// <param name="emailTo">Indirizzo di posta elettronica a cui spedire il messaggio</param>
        /// <param name="subject">Oggette del messaggio</param>
        /// <param name="body">Corpo del messaggio in fomato: accetta anche testo formattato in html</param>
        /// <param name="emailFrom">Indirizzo di posta elettronica usato per l'invio del messaggio</param>
        /// <param name="lblMittente">Etichetta testuale visibile nel intestazione del messaggio</param>
        public MailSender(string emailTo, string subject, string body, string emailFrom, string lblMittente)
        {
            EmailTo = emailTo;
            Subject = subject;
            Body = body;
            EmailFrom = emailFrom;
            LblMittente = lblMittente;
        }

        /// <summary>
        /// Inizializzazione di una nuova istanza
        /// </summary>
        /// <param name="emailTo">Indirizzo di posta elettronica a cui spedire il messaggio</param>
        /// <param name="subject">Oggette del messaggio</param>
        /// <param name="body">Corpo del messaggio in fomato: accetta anche testo formattato in html</param>
        /// <param name="emailFrom">Indirizzo di posta elettronica usato per l'invio del messaggio</param>
        /// <param name="lblMittente">Etichetta testuale visibile nel intestazione del messaggio</param>
        public MailSender(string emailTo, string emailCC, string subject, string body, string emailFrom, string lblMittente)
            : this(emailTo, subject, body, emailFrom, lblMittente)
        {
            EmailCC = emailCC;
        }

        /// <summary>
        /// Abilitazione Invio come HTML (di default a false per garantire retrocompatibilità
        /// </summary>
        public bool SendAlsoPlain
        {
            get { return _sendAlsoPlain; }
            set { _sendAlsoPlain = value; }
        }
        private bool _sendAlsoPlain = false;

        private AlternateView textView;
        private AlternateView TextView 
        {
            get {
                if (textView == null)
                {
                    var plainText = StripHTML.fromHTMLtoPlainText(this.Body);
                    textView = AlternateView.CreateAlternateViewFromString(plainText, null, System.Net.Mime.MediaTypeNames.Text.Plain);                    
                }
                return textView;
            }
        }

        /// <summary>
        /// Metodo per l'invio del messaggio
        /// </summary>
        public void Send()
        {
            try
            {
                MailMessage messaggio = new MailMessage();

                messaggio.From = new MailAddress(this.EmailFrom, this.LblMittente);
                
                messaggio.To.Add(new MailAddress(EmailTo));

                if (EmailCC != null)
                    messaggio.CC.Add(new MailAddress(EmailCC));

                messaggio.Subject = Subject;
                messaggio.SubjectEncoding = System.Text.Encoding.UTF8;
                
                // TODO: migliorare il sistema di estrapolazione del testo e dei link (sostituire br con a capo e a href estrarre il link e metterlo per esteso sotto al testo)
                if (SendAlsoPlain)
                {
                    messaggio.AlternateViews.Add(TextView);
                    AlternateView objHTLMAltView = AlternateView.CreateAlternateViewFromString(
                        this.Body,
                        new System.Net.Mime.ContentType("text/html"));
                    messaggio.AlternateViews.Add(objHTLMAltView);
                }
                else 
                {
                    messaggio.Body = Body;
                    messaggio.BodyEncoding = System.Text.Encoding.UTF8;
                    messaggio.IsBodyHtml = SendAsHtml;
                }

                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Host = SetSmtpHost;
                smtpClient.Port = SetSmtpPort;
                smtpClient.EnableSsl = EnableSsl;
        

                if (SmtpUser != null && SmtpPassword != null && SmtpUser.Length > 0 && SmtpPassword.Length > 0)
                {
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new System.Net.NetworkCredential(SmtpUser, SmtpPassword);
                }

                if (EnableSsl && DisableServerCertificateValidation)
                {
                    // trick per risolvere il problema di verifica della convalida del certificato ssl del server di posta
                    ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                }
                smtpClient.Send(messaggio);
                
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Info, string.Format("Spedita email con oggetto: \n'{0}'\n all'indirizzo '{1}' \n con testo: \n'{2}'", Subject, EmailTo, Body), "", "");
            }
            catch (ArgumentNullException ex)
            {
                string message = "Non è stato possibile inviare la mail. Errore: '" + ex.Message + "' destinata all'indirizzo '" + this.EmailTo + "'";
                NetCms.Diagnostics.Diagnostics.TraceException(ex, message, "", "", errorCode: "63");
            }
            catch (ObjectDisposedException ex)
            {
                string message = "Non è stato possibile inviare la mail. Errore: '" + ex.Message + "' destinata all'indirizzo '" + this.EmailTo + "'";
                NetCms.Diagnostics.Diagnostics.TraceException(ex, message, "", "", errorCode: "63");
            }
            catch (InvalidOperationException ex)
            {
                string message = "Non è stato possibile inviare la mail. Errore: '" + ex.Message + "' destinata all'indirizzo '" + this.EmailTo + "'";
                NetCms.Diagnostics.Diagnostics.TraceException(ex, message, "", "", errorCode: "63");
            }
            catch (SmtpFailedRecipientsException ex)
            {
                string message = "Non è stato possibile inviare la mail. Errore: '" + ex.Message + "' destinata all'indirizzo '" + this.EmailTo + "'";
                NetCms.Diagnostics.Diagnostics.TraceException(ex, message, "", "", errorCode: "63");
            }
            catch (SmtpFailedRecipientException ex)
            {
                string message = "Non è stato possibile inviare la mail. Errore: '" + ex.Message + "' destinata all'indirizzo '" + this.EmailTo + "'";
                NetCms.Diagnostics.Diagnostics.TraceException(ex, message, "", "", errorCode: "63");
            }
            catch (SmtpException ex)
            {
                string message = "Non è stato possibile inviare la mail. Errore: '" + ex.Message + "' destinata all'indirizzo '" + this.EmailTo + "'";
                NetCms.Diagnostics.Diagnostics.TraceException(ex, message, "", "", errorCode: "63");
            }
        }

    }
}