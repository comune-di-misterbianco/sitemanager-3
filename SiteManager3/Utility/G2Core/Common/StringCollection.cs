using System;
using System.Collections;


namespace G2Core.Common
{
    /// <summary>
    /// La classe AxtColumnsCollection indicizza oggetti di tipo System.String
    /// </summary>
    public class StringCollection : IEnumerable
    {
        private G2Collection coll;

        /// <summary>
        /// Rappresenta il numero di colonne attualmente presenti nella collezione
        /// </summary>
        public int Count { get { return coll.Count; } }

        /// <summary>
        /// Costruttore della classe StringCollection
        /// </summary>
        public StringCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new G2Collection();
        }

        /// <summary>
        /// Costruttore della classe StringCollection
        /// </summary>
        /// <param name="Values">Un array di stringhe da aggiungere alla collezione</param>
        public StringCollection(string[] Values)
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new G2Collection();
            foreach (string s in Values)
                this.Add(s);
        }
        
        /// <summary>
        /// Aggiunge una stringa alla collezione.
        /// </summary>
        /// <param name="str">La stringa da aggiungere alla collezione</param>
        /// <param name="key">La chiave associata alla stringa da aggiungere alla collezione</param>
        public void Add(string str, string key)
        {
            coll.Add(key, str);
        }

        /// <summary>
        /// Aggiunge una stringa alla collezione. La chiava associata alla stringa sar� la stringa stessa.
        /// </summary>
        /// <param name="str">La stringa da aggiungere alla collezione</param>
        public void Add(string str)
        {
            coll.Add(str, str);
        }

        /// <summary>
        /// Rimuove una stringa dalla collezione.
        /// </summary>
        /// <param name="str">La stringa da rimuovere dalla collezione</param>
        public void Remove(string str)
        {
            coll.Remove(str);
        }

        /// <summary>
        /// Svuota la collezione.
        /// </summary>
        public void Clear()
        {
            coll.Clear();
        }

        /// <summary>
        /// Restituisce la stringa associata al indice passato per paramentro, NULL se la stringa non viene trovata
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string this[int index]
        {
            get
            {
                string str = (string)coll[coll.Keys[index]];
                return str;
            }
        }

        /// <summary>
        /// Restituisce l'indice della stringa associata alla chiave passata per parametro.
        /// </summary>
        /// <param name="Key">La chiave associata alla stringa da cercare</param>
        /// <returns></returns>
        public int IndexOf(string key)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (this.coll.Keys[i] == key)
                    return i;
            }
            return -1;
        }

        /// <summary>
        /// Restituisce la stringa associata alla chiave passata per paramentro, NULL se la stringa non viene trovata
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public string this[string Key]
        {
            get
            {
                string val = (string)coll[Key];
                return val;
            }
        }

        /// <summary>
        /// Restituisce un boolean che specifica se la stringa passata per paramentro � presente nella collezione
        /// </summary>
        /// <param name="str">La stringa del quale si vuole controllare la presenza</param>
        /// <returns>Un boolean che specifica se la stringa passata per paramentro � presente nella collezione</returns>
        public bool Contains(string str)
        {
            return coll[str] != null;
        }

        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private G2Core.Common.StringCollection Collection;
            public CollectionEnumerator(G2Core.Common.StringCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}