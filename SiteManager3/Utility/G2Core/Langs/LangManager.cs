using System;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using NetCms.Connections;

namespace G2Core.Languages
{
    public class LangManager
    {
        private G2Core.XhtmlControls.InputField _ActionField;
        public G2Core.XhtmlControls.InputField ActionField
        {
            get
            {
                if (_ActionField == null)
                {
                    _ActionField = new G2Core.XhtmlControls.InputField("LangSelectedActionField");
                    _ActionField.AutoLoadValueAfterPostback = true;
                }

                return _ActionField;
            }
        }

        private G2Core.XhtmlControls.InputField _IDField;
        public G2Core.XhtmlControls.InputField IDField
        {
            get
            {
                if (_IDField == null)
                {
                    _IDField = new G2Core.XhtmlControls.InputField("LangSelectedIDField");
                    _IDField.AutoLoadValueAfterPostback = true;
                }
                return _IDField;
            }
        }

        private bool _SubmitAfterOptionClick;
        public bool SubmitAfterOptionClick
        {
            get { return _SubmitAfterOptionClick; }
            set { _SubmitAfterOptionClick = value; }
        }
	
        public enum Actions { None, Modify, Add, Delete }
        
        private Actions _CurrentAction;
        public Actions CurrentAction
        {
            get
            {
                return _CurrentAction;
            }
        }
	    
        private void initActions()
    {
       
        if( ActionField.RequestVariable.IsValidInteger)
        {
            switch (ActionField.RequestVariable.IntValue)
            {
                case 1:
                    _CurrentAction = Actions.Add;
                    break;
                case 2:
                    _CurrentAction = Actions.Modify;
                    break;
                case 3:
                    _CurrentAction = Actions.Delete;
                    break;
                default:
                    _CurrentAction = Actions.None;
                    break;

            }
        }
        else
            _CurrentAction = Actions.None;

    }


        private string _SqlFilter;
        public string SqlFilter
        {
            get
            {
                return _SqlFilter;
            }
        }
        private LangDataSource LangDataSource;
        private NetCms.Connections.Connection ConnSource;
        public LangManager(string sqlFilter, LangDataSource langDataSource, NetCms.Connections.Connection connSource)
        {
            _SqlFilter = sqlFilter;
            //CommonData = commonData;
            LangDataSource = langDataSource;
            ConnSource = connSource;
            _SubmitAfterOptionClick = true;
            initActions();
        }

        public HtmlGenericControl ScriptsControl
        {
            get
            {
                HtmlGenericControl Script = new HtmlGenericControl("script");

                Script.Attributes["type"] = "text/javascript";
                Script.InnerHtml = @"
function SetLangData(value,action)
{
    oActionControl = document.getElementById('" + this.ActionField.ID + @"');
    oValueControl = document.getElementById('" + this.IDField.ID + @"');

    if(oActionControl && oValueControl)
    {
        oActionControl.value = action;
        oValueControl.value = value;
        if("+(this.SubmitAfterOptionClick.ToString().ToLower())+ @")
            document.getElementsByTagName('FORM')[0].submit();
    }
}
";

                return Script;
            }
        }
        public HtmlGenericControl BuildControls()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");

            div.Controls.Add(ScriptsControl);
            div.Controls.Add(this.ActionField.Control);
            div.Controls.Add(this.IDField.Control);

            HtmlGenericControl row = new HtmlGenericControl("tr");
            HtmlGenericControl cell = new HtmlGenericControl("td");
            HtmlGenericControl table = new HtmlGenericControl("table");

            table.Attributes["class"] = "tab";
            table.Attributes["cellpadding"] = "0";
            table.Attributes["cellspacing"] = "0";

            #region Intestazione Tabella

            row = new HtmlGenericControl("tr");
            table.Controls.Add(row);

            cell = new HtmlGenericControl("th");
            cell.InnerHtml = "Lingua";
            row.Controls.Add(cell);

            cell = new HtmlGenericControl("th");
            cell.InnerHtml = "Stato Etichetta";
            row.Controls.Add(cell);

            cell = new HtmlGenericControl("th");
            cell.InnerHtml = "Testo Etichetta";
            row.Controls.Add(cell);

            cell = new HtmlGenericControl("th");
            cell.InnerHtml = "Modifica";
            row.Controls.Add(cell);

            cell = new HtmlGenericControl("th");
            cell.InnerHtml = "Rimuovi";
            row.Controls.Add(cell);
            
            #endregion

            foreach (Language lang in LangDataSource.Languages)
            {
                row = new HtmlGenericControl("tr");
                table.Controls.Add(row);

                cell = new HtmlGenericControl("td");
                if (lang.IsDefault)
                    cell.InnerHtml = "<strong>"+lang.Name+"</strong>";
                else
                    cell.InnerHtml = lang.Name;
                cell.Attributes["class"] = "Lang_Cell Lang_" + lang.CultureCode;
                row.Controls.Add(cell);

                DataRow[] rows = ConnSource.SqlQuery(string.Format(this.SqlFilter, lang.ID)).Select();
                bool enabled = rows.Length > 0;


                cell = new HtmlGenericControl("td");
                cell.InnerHtml = enabled ? "Presente" : "Non Presente";
                row.Controls.Add(cell);

                string textValue = enabled ? rows[0][0].ToString() : "&nbsp;";
                if (textValue.Length > 70)
                    textValue = textValue.Remove(67)+"...";
                cell = new HtmlGenericControl("td");
                cell.InnerHtml = textValue;
                row.Controls.Add(cell);

                string ModelloActionCell =  "<a href=\"{0}\"><span>{1}</span></a>";
                cell = new HtmlGenericControl("td");
                if (enabled)
                {
                    cell.Attributes["class"] = "action modify";
                    cell.InnerHtml = string.Format(ModelloActionCell, "javascript: SetLangData(" + rows[0][1].ToString() + ",2);", "Modifica");
                }
                else
                {
                    cell.Attributes["class"] = "action add";
                    cell.InnerHtml = string.Format(ModelloActionCell, "javascript: SetLangData(" + lang.ID + ",1);", "Crea");
                }
                row.Controls.Add(cell);


                cell = new HtmlGenericControl("td");
                if (enabled && !lang.IsDefault)
                {
                    cell.Attributes["class"] = "action delete";
                    cell.InnerHtml = string.Format(ModelloActionCell, "javascript: SetLangData(" + rows[0][1].ToString() + ",3);", "Elimina");
                }
                else
                    cell.InnerHtml = "&nbsp;";
                row.Controls.Add(cell);
            }

            div.Controls.Add(table);

            return div;
        }

    }

}
