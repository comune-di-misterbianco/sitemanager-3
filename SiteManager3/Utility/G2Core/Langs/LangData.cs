using System;
using System.Data;

namespace G2Core.Languages
{
    public class LangDataSource
    {
        private DataTable _Langs;
        public DataTable Langs
        { get { return _Langs; } }

        private DataTable _Labels;
        public DataTable Labels
        { get { return _Labels; } }

        private DataTable _LabelsRecords;
        public DataTable LabelsRecords
        { get { return _LabelsRecords; } }

        private Language _DefaultLang;
        public Language DefaultLang
        {
            get
            {
                return _DefaultLang;
            }
        }

        public Language CurrentLang
        {
            get
            {
                G2Core.Common.RequestVariable cl = new G2Core.Common.RequestVariable("cl", G2Core.Common.RequestVariable.RequestType.QueryString);
                if (cl.IsValidInteger && this.Languages.Contains(cl.IntValue))
                    return this.Languages[cl.StringValue];
                else
                    return this.DefaultLang;
            }
        }

        private LanguagesCollection _Languages;
        public LanguagesCollection Languages
        {
            get
            {
                return _Languages;
            }
        }

        public LangDataSource(DataTable langs, DataTable labels, DataTable labelsRecords)
        {
            _Langs = langs;
            _LabelsRecords = labelsRecords;
            _Labels = labels;

            _Languages = new LanguagesCollection();
            foreach (DataRow lang in this.Langs.Select("","Parent_Lang"))
            {
                Language Language = new Language(lang,this);
                _Languages.Add(Language);
                if (this.Languages[Language.ParentLangID.ToString()] != null)
                    Language.ParentLanguage = this.Languages[Language.ParentLangID.ToString()];
                if (Language.IsDefault)
                    _DefaultLang = Language;
            }
            foreach (Language lang in this.Languages)
                if(!lang.IsDefault)
                    lang.DefaultLanguage = this.DefaultLang;
        }


        /// <summary>
        /// Restituisce 'id_Lang' della lingua da cui deriva la lingua passata per parametro,la funzione ritorna 0 se non trova una lingua genitore. Esempio passando l'id della lingua 'en-US' la funzione ritorna l'id_Lang della lingua 'en'.
        /// </summary>
        /// <param name="LangID">Un intero rappresentante 'id_Lang' di una lingua presa dalla tabella 'Lang'</param>
        /// <returns>'id_Lang della lingua da cui deriva la lingua passata per parametro</returns>
        public int ParentLangOf(int LangID)
        {
            int ParentID = int.Parse(Langs.Select("id_Lang = " + LangID)[0]["Parent_Lang"].ToString());

            return ParentID;
        }
    }
    public class Language
    {
        private int _ID;
        public int ID
        {
            get { return _ID; }
        }

        private int _ParentLangID;
        public int ParentLangID
        {
            get { return _ParentLangID; }
        }

        private Language _ParentLanguage;
        public Language ParentLanguage
        {
            get { return _ParentLanguage; }
            set 
            {
                if (_ParentLanguage == null)
                    _ParentLanguage = value;
                else
                {
                    Exception ex = new Exception("ParentLanguage gi� impostato sull'istanza dell'oggetto");
                    /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                    ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, "", "", "70");
                }
            }
        }

        private Language _DefaultLanguage;
        public Language DefaultLanguage
        {
            get { return _DefaultLanguage; }
            set
            {
                if (_DefaultLanguage == null)
                    _DefaultLanguage = value;
                else 
                { 
                    Exception ex = new Exception("DefaultLanguage gi� impostato sull'istanza dell'oggetto");
                    /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                    ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, "", "", "71");
                }
            }
        }
        private string _CultureCode;
        public string CultureCode
        {
            get { return _CultureCode; }
        }

        private string _Name;
        public string Name
        {
            get { return _Name; }
        }

        private bool _IsDefault;
        public bool IsDefault
        {
            get { return _IsDefault; }
        }

        private bool _IsCurrent;
        public bool IsCurrent
        {
            get 
            { 
                return LSource.CurrentLang == this; 
            }
        }

        private DataRow Data;
        private LangDataSource LSource;
        public Language(DataRow row,LangDataSource source)
        {
            Data = row;
            LSource = source;
            _ID = int.Parse(row["id_Lang"].ToString());
            _IsDefault = row["Default_Lang"].ToString() == "1";
            _CultureCode = row["CultureCode_Lang"].ToString();
            _Name = row["Nome_Lang"].ToString();
            _ParentLangID = int.Parse(row["Parent_Lang"].ToString());
        }

        public override string ToString()
        {
            return this.ID.ToString();
        }
    }
    public class LanguageData
    {
        private DataRow _LangData;
        private DataRow LangData
        {
            get { return _LangData; }
        }

        private int _ParentID = -1;
        public int ParentID
        {
            get
            {

                if (_ParentID == -1)
                {
                    _ParentID = int.Parse(this.LangData["Parent_Lang"].ToString());
                }
                return _ParentID;
            }
        }

        private int _ID;
        public int ID
        {
            get
            {

                if (_ID == 0)
                    _ID = int.Parse(this.LangData["id_Lang"].ToString());
                return _ID;
            }
        }

        private string _CultureCode;
        public string CultureCode
        {
            get
            {
                if (_CultureCode == null)
                {
                    _CultureCode = this.LangData["CultureCode_Lang"].ToString();
                }
                return _CultureCode;
            }
        }

        private string _Nome;
        public string Nome
        {
            get
            {
                if (_Nome == null)
                {
                    _Nome = this.LangData["Nome_Lang"].ToString();
                }
                return _Nome;
            }
        }

        private int _IsDefault = -1;
        public bool IsDefault
        {
            get
            {

                if (_IsDefault == -1)
                {
                    _IsDefault = int.Parse(this.LangData["Parent_Lang"].ToString());
                }
                return _IsDefault == 1;
            }
        }

        private LanguageData _Parent;
        public LanguageData Parent
        {
            get
            {
                if (_Parent == null)
                {
                    _Parent = new LanguageData(this.ParentID, this.Conn);
                }
                return _Parent;
            }
        }

        private NetCms.Connections.Connection Conn;

        public LanguageData(NetCms.Connections.Connection conn)
        {
            Conn = conn;
            InitLangData("SELECT * FROM Lang WHERE CultureCode_Lang = '" + System.Globalization.CultureInfo.CurrentCulture.IetfLanguageTag + "'");
        }
        public LanguageData(int LangID, NetCms.Connections.Connection conn)
        {
            Conn = conn;
            InitLangData("SELECT * FROM Lang WHERE id_Lang = " + LangID);
        }
        public LanguageData(string CultureCode, NetCms.Connections.Connection conn)
        {
            Conn = conn;
            InitLangData("SELECT * FROM Lang WHERE CultureCode_Lang = '" + CultureCode + "'");
        }

        public void InitLangData(string sqlQuery)
        {
            DataTable Langs = Conn.SqlQuery(sqlQuery);
            if (Langs.Rows.Count > 0)
                _LangData = Langs.Rows[0];
        }
    }
}