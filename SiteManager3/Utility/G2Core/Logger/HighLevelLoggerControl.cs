﻿using System;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace NetCms.LogAndTrance
{/*
    public class HighLevelLoggerControl : WebControl
    {
        private NetCms.Connections.Connection _Connection;
        public NetCms.Connections.Connection Connection
        {
            get
            {
                //if (_Connection == null) _Connection = Connections.Sm2Connection.NewConnection(Configurations.ConnectionsStrings.LogDB);
                return _Connection;
            }
        }

        private NetCms.Connections.Connection _LogConnection;
        public NetCms.Connections.Connection LogConnection
        {
            get
            {
                //if (_LogConnection == null) _LogConnection = Connections.Sm2Connection.NewConnection(Configurations.ConnectionsStrings.LogDB);
                return _LogConnection;
            }
        }

        public HighLevelLoggerControl(NetCms.Connections.Connection connection, NetCms.Connections.Connection logconnection)
            : base(System.Web.UI.HtmlTextWriterTag.Div)
        {
            _Connection = connection;
            _LogConnection = logconnection;
        }

        private string _AbsoluteRoot;
        public string AbsoluteRoot
        {
            get
            {
                if (_AbsoluteRoot == null)
                {
                    _AbsoluteRoot = string.Empty;
                    string RootPathValue = System.Web.Configuration.WebConfigurationManager.AppSettings["RootPath"].ToLower();
                    if (RootPathValue.Length > 0)
                        _AbsoluteRoot += RootPathValue.ToLower();
                }
                return _AbsoluteRoot;
            }
        }

        private string _AbsoluteSiteManagerRoot;
        public  string AbsoluteSiteManagerRoot
        {
            get
            {
                if (_AbsoluteSiteManagerRoot == null)
                    _AbsoluteSiteManagerRoot = AbsoluteRoot + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["SiteManagerRoot"].ToLower();

                return _AbsoluteSiteManagerRoot;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            string Span = "5";
            string userlogquery = "SELECT * FROM Gui_Log ORDER BY id_GuiLog DESC";

            DataTable user_logs = LogConnection.SqlQuery(userlogquery);
            DataTable users = Connection.SqlQuery("SELECT * FROM Users INNER JOIN Anagrafica ON id_User = Anagrafica_User");

            HtmlGenericControl table = new HtmlGenericControl("table");
            HtmlGenericControl row = new HtmlGenericControl("tr");
            HtmlGenericControl cell = new HtmlGenericControl("td");
            table.Attributes["cellpadding"] = "0";
            table.Attributes["cellspacing"] = "0";
            table.Attributes["class"] = "tab";

            row = new HtmlGenericControl("tr");
            table.Controls.Add(row);
            cell = new HtmlGenericControl("th");
            row.Controls.Add(cell);
            cell.InnerHtml = "Cognome e Nome";

            cell = new HtmlGenericControl("th");
            row.Controls.Add(cell);
            cell.InnerHtml = "Username";
            cell = new HtmlGenericControl("th");
            row.Controls.Add(cell);
            cell.InnerHtml = "Data/Ora Operazione";

            cell = new HtmlGenericControl("th");
            row.Controls.Add(cell);
            cell.InnerHtml = "Testo";

            cell = new HtmlGenericControl("th");
            row.Controls.Add(cell);
            cell.InnerHtml = "Pagina";

            bool alternate = false;
            G2Core.XhtmlControls.InputField CurrentPage = new G2Core.XhtmlControls.InputField("CurrentPage");
            this.Controls.Add(CurrentPage.Control);
            //NetUtility.RequestVariable page = new NetUtility.RequestVariable("page", NetUtility.RequestVariable.RequestType.Form);

            G2Core.Common.PageMaker Pager = new G2Core.Common.PageMaker(CurrentPage.ID, G2Core.Common.PageMaker.VariabileType.Form);
            Pager.RPP = 25;
            DataRow[] pagerrows = Pager.MakeTablePagination(user_logs);
            for (int i = 0; i < pagerrows.Length; i++)
            {
                DataRow datarow = pagerrows[i];
                row = new HtmlGenericControl("tr");
                row.Attributes["class"] = alternate ? "alternate" : "normal";
                table.Controls.Add(row);

                {
                    cell = new HtmlGenericControl("td");
                    row.Controls.Add(cell);
                    DataRow[] user = users.Select("id_User = " + datarow["User_GuiLog"].ToString());
                    if (user.Length > 0)
                    {
                        cell.InnerHtml = user[0]["Cognome_Anagrafica"].ToString() + " " + user[0]["Nome_Anagrafica"].ToString();
                        cell.InnerHtml = "<a  href=\"" + AbsoluteSiteManagerRoot + "/cms/grants/user_logs.aspx?id=" + datarow["User_GuiLog"].ToString() + "\">" + cell.InnerHtml + "</a>";
                    }

                    cell = new HtmlGenericControl("td");
                    row.Controls.Add(cell);
                    if (user.Length > 0)
                        cell.InnerHtml = user[0]["Name_User"].ToString();
                }

                cell = new HtmlGenericControl("td");
                row.Controls.Add(cell);
                cell.InnerHtml = datarow["Data_GuiLog"].ToString().Replace(" ", " ore ");

                string res = datarow["Text_GuiLog"].ToString();

                cell = new HtmlGenericControl("td");
                row.Controls.Add(cell);
                cell.InnerHtml = res;


                cell = new HtmlGenericControl("td");
                row.Controls.Add(cell);
                cell.InnerHtml = FilterPageName(datarow["Url_GuiLog"].ToString());

                alternate = !alternate;
            }

            row = new HtmlGenericControl("tr");

            cell = new HtmlGenericControl("td");
            cell.Attributes["colspan"] = Span;
            cell.Controls.Add(Pager.PageNavigation("javascript: setFormValue('{0}','" + CurrentPage.ID + "',1)", user_logs.Rows.Count));

            row.Controls.Add(cell);
            table.Controls.Add(row);

            this.Controls.Add(table);
        }

        public string FilterPageName(string url)
        {
            string[] Paths = url.Split('/');
            Paths = Paths[Paths.Length - 1].Split('?');
            return Paths[0];
        }
    }
*/
}
