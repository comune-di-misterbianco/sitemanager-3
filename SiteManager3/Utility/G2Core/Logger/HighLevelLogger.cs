﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.HtmlControls;

namespace NetCms.LogAndTrance
{
    public class HighLevelLogger
    {
        public int UserID
        {
            get { return _UserID; }
            private set { _UserID = value; }
        }
        private int _UserID;

        public bool IsPostBack
        {
            get { return _IsPostBack; }
            private set { _IsPostBack = value; }
        }
        private bool _IsPostBack;

        public HighLevelLogger(int UserID, bool IsPostBack)
        {
            this.UserID = UserID;
            this.IsPostBack = IsPostBack;
        }

        public void SaveLogRecord(string text) 
        {
            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, text, UserID.ToString(), "", "", 2);
        }
        
        public void SaveLogRecord(string text, bool logIfPostBack)
        {
            if (logIfPostBack || !IsPostBack)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, text, UserID.ToString(), "","",2);
            }
        }
    }

}
