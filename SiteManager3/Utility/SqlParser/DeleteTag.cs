﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SqlParser
{
	#region DeleteTag

	[TagType(DeleteTag.cTagName)]
	[MatchDeleteTag]
    internal class DeleteTag : SimpleOneWordTag
	{
		#region Consts

		/// <summary>
		/// The name of the tag (its identifier).
		/// </summary>
		public const string cTagName = "DELETE";

		#endregion

		#region Properties

		/// <summary>
		/// Gets the name of the tag (its identifier and sql text)
		/// </summary>
		protected override string Name
		{
			get 
			{
				return cTagName;
			}
		}

		#endregion
	}

	#endregion

	#region MatchSelectTagAttribute

    internal class MatchDeleteTagAttribute : MatchSimpleOneWordTagAttribute
	{
		#region Properties

		/// <summary>
		/// Gets the name of the tag (its identifier and sql text)
		/// </summary>
		protected override string Name
		{
			get 
			{
				return DeleteTag.cTagName;
			}
		}

		#endregion
	}

	#endregion
}
