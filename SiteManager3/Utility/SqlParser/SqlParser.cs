﻿using System;
using System.Text;
using System.Xml;

namespace SqlParser
{
	#region SqlParser

    public class SqlParser : ParserBase
    {
        #region Base class implementation

        #region Fields

        /// <summary>
        /// Stores the types of all the possible tags.
        /// </summary>
        Type[] fTags;

        #endregion

        #region Properties

        /// <summary>
        /// Indicates whether the white space is a non-valueable character.
        /// </summary>
        protected override bool IsSkipWhiteSpace
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Returns the list of all the available tags.
        /// </summary>
        protected override Type[] Tags
        {
            get
            {
                if (fTags == null)
                {
                    switch (QueryType)
                    {
                        case QueryTypes.Select: fTags = new Type[] { 
                                                typeof(SelectTag),
						                        typeof(FromTag),
						                        typeof(WhereTag),
						                        typeof(OrderByTag),
						                        typeof(BracesTag),
						                        typeof(StringLiteralTag),
						                        typeof(ForUpdateTag),
						                        typeof(StartWith),
						                        typeof(GroupByTag),
						                        typeof(QuotedIdentifierTag)}; break;
                        case QueryTypes.Insert: fTags = new Type[] { 
                                                typeof(InsertIntoTag),
						                        typeof(ValuesTag),						                        
						                        typeof(BracesTag),
						                        typeof(StringLiteralTag),
						                        typeof(QuotedIdentifierTag)}; break;
                        case QueryTypes.Delete: fTags = new Type[] { 
                                                typeof(DeleteTag),                                                
						                        typeof(FromTag),
						                        typeof(WhereTag),
						                        typeof(BracesTag),
						                        typeof(StringLiteralTag),
						                        typeof(QuotedIdentifierTag)}; break;
                        case QueryTypes.Update: fTags = new Type[] { 
                                                typeof(UpdateTag),
						                        typeof(SetTag),
						                        typeof(WhereTag),
						                        typeof(BracesTag),
						                        typeof(StringLiteralTag),
						                        typeof(QuotedIdentifierTag)}; break;
                        default: throw new Exception("Tipologia di query SQL non riconosciuta");
                    }
                    
                }

                return fTags;
            }
        }
        #endregion

        #endregion

        public enum QueryTypes { Insert,Update,Select,Delete, Procedure }
        private QueryTypes _QueryType;
        public QueryTypes QueryType
        {
            get
            {
                return _QueryType;
            }
        }
        public SqlParser(QueryTypes queryType)
        {
            _QueryType = queryType;
        }

        #region Common

        #region Methods

        /// <summary>
        /// Returns the xml node which corresponds to the From tag.
        /// If this node does not exist, this method generates an
        /// exception.
        /// </summary>
        private XmlNode GetFromTagXmlNode()
        {
            XmlNode myFromNode = ParsedDocument.SelectSingleNode(string.Format(@"{0}/{1}[@{2}='{3}']", cRootXmlNodeName, cTagXmlNodeName, cTagTypeXmlAttributeName, FromTag.cTagName));
            if (myFromNode == null)
                throw new Exception(ToText());

            return myFromNode;
        }

        /// <summary>
        /// Returns the xml node which corresponds to the For Update tag.	
        /// </summary>
        private XmlNode GetForUpdateTagXmlNode()
        {
            XmlNode myForUpdateNode = ParsedDocument.SelectSingleNode(string.Format(@"{0}/{1}[@{2}='{3}']", cRootXmlNodeName, cTagXmlNodeName, cTagTypeXmlAttributeName, ForUpdateTag.cTagName));

            return myForUpdateNode;
        }

        /// <summary>
        /// Checks whether there is a tag in the text at the specified position, and returns its tag.
        /// </summary>
        internal new Type IsTag(string text, int position)
        {
            return base.IsTag(text, position);
        }

        #endregion

        #endregion

        #region Where Clause

        #region Methods

        /// <summary>
        /// Returns the xml node which corresponds to the Where tag.
        /// If this node does not exist, creates a new one (if needed).
        /// </summary>
        private XmlNode GetWhereTagXmlNode(bool createNew)
        {
            XmlNode myWhereNode = ParsedDocument.SelectSingleNode(string.Format(@"{0}/{1}[@{2}='{3}']", cRootXmlNodeName, cTagXmlNodeName, cTagTypeXmlAttributeName, WhereTag.cTagName));
            if (myWhereNode == null && createNew)
            {
                WhereTag myWhereTag = new WhereTag();
                myWhereTag.InitializeFromData(this, null, false);
                myWhereNode = CreateTagXmlNode(myWhereTag);

                XmlNode myFromNode = GetFromTagXmlNode();
                myFromNode.ParentNode.InsertAfter(myWhereNode, myFromNode);
            }

            return myWhereNode;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Where clause of the parsed sql.
        /// </summary>
        public string WhereClause
        {
            get
            {
                #region Get the Where xml node

                XmlNode myWhereTagXmlNode = GetWhereTagXmlNode(false);
                if (myWhereTagXmlNode == null)
                    return string.Empty;

                #endregion

                StringBuilder myStringBuilder = new StringBuilder();
                XmlNodesToText(myStringBuilder, myWhereTagXmlNode.ChildNodes);
                return myStringBuilder.ToString();
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    #region Remove the Where xml node

                    XmlNode myWhereXmlNode = GetWhereTagXmlNode(false);
                    if (myWhereXmlNode != null)
                        myWhereXmlNode.ParentNode.RemoveChild(myWhereXmlNode);

                    #endregion
                }
                else
                {
                    #region Modify the Where xml node

                    XmlNode myWhereXmlNode = GetWhereTagXmlNode(true);

                    ClearXmlNode(myWhereXmlNode);

                    TagBase myWhereTag = TagXmlNodeToTag(myWhereXmlNode);

                    ParseBlock(myWhereXmlNode, myWhereTag, value, 0);

                    #endregion
                }
            }
        }

        #endregion

        #endregion

        #region Order By Clause

        #region Methods

        /// <summary>
        /// Returns the xml node which corresponds to the Order By tag.
        /// If this node does not exist, creates a new one (if needed).
        /// </summary>
        private XmlNode GetOrderByTagXmlNode(bool createNew)
        {
            XmlNode myOrderByNode = ParsedDocument.SelectSingleNode(string.Format(@"{0}/{1}[@{2}='{3}']", cRootXmlNodeName, cTagXmlNodeName, cTagTypeXmlAttributeName, OrderByTag.cTagName));
            if (myOrderByNode == null && createNew)
            {
                OrderByTag myOrderByTag = new OrderByTag();
                myOrderByTag.InitializeFromData(this, null, false);
                myOrderByNode = CreateTagXmlNode(myOrderByTag);

                XmlNode myForUpdateNode = GetForUpdateTagXmlNode();
                if (myForUpdateNode != null)
                {
                    myForUpdateNode.ParentNode.InsertBefore(myOrderByNode, myForUpdateNode);
                    return myOrderByNode;
                }

                XmlNode myFromNode = GetFromTagXmlNode();
                myFromNode.ParentNode.AppendChild(myOrderByNode);
            }

            return myOrderByNode;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Order By clause of the parsed sql.
        /// </summary>
        public string OrderByClause
        {
            get
            {
                #region Get the Order By xml node

                XmlNode myOrderByTagXmlNode = GetOrderByTagXmlNode(false);
                if (myOrderByTagXmlNode == null)
                    return string.Empty;

                #endregion

                StringBuilder myStringBuilder = new StringBuilder();
                XmlNodesToText(myStringBuilder, myOrderByTagXmlNode.ChildNodes);
                return myStringBuilder.ToString();
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    #region Remove the Order By xml node

                    XmlNode myOrderByXmlNode = GetOrderByTagXmlNode(false);
                    if (myOrderByXmlNode != null)
                        myOrderByXmlNode.ParentNode.RemoveChild(myOrderByXmlNode);

                    #endregion
                }
                else
                {
                    #region Modify the Order By xml node

                    XmlNode myOrderByXmlNode = GetOrderByTagXmlNode(true);

                    ClearXmlNode(myOrderByXmlNode);

                    TagBase myOrderByTag = TagXmlNodeToTag(myOrderByXmlNode);

                    ParseBlock(myOrderByXmlNode, myOrderByTag, value, 0);

                    #endregion
                }
            }
        }

        #endregion

        #endregion

        #region Select
        /// <summary>
        /// Returns the xml node which corresponds to the Select tag.
        /// If this node does not exist, creates a new one (if needed).
        /// </summary>
        private XmlNode GetSelectTagXmlNode(bool createNew)
        {
            XmlNode mySelectNode = ParsedDocument.SelectSingleNode(string.Format(@"{0}/{1}[@{2}='{3}']", cRootXmlNodeName, cTagXmlNodeName, cTagTypeXmlAttributeName, SelectTag.cTagName));

            if (mySelectNode == null && createNew)
            {
                SelectTag mySelectTag = new SelectTag();
                mySelectTag.InitializeFromData(this, null, false);
                mySelectNode = CreateTagXmlNode(mySelectTag);

                XmlNode myFromNode = GetFromTagXmlNode();
                myFromNode.ParentNode.InsertBefore(mySelectNode, myFromNode);
            }

            return mySelectNode;
        }


        /// <summary>
        /// Gets or sets the Where clause of the parsed sql.
        /// </summary>
        public string Select
        {
            get
            {
                #region Get the Where xml node

                XmlNode mySelectTagXmlNode = GetSelectTagXmlNode(false);
                if (mySelectTagXmlNode == null)
                    return string.Empty;

                #endregion

                StringBuilder myStringBuilder = new StringBuilder();
                XmlNodesToText(myStringBuilder, mySelectTagXmlNode.ChildNodes);
                return myStringBuilder.ToString();
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Impossibile elaborare una query sql senza il tag SELECT");
                }
                else
                {
                    #region Modify the SELECT xml node

                    XmlNode mySELECTXmlNode = GetSelectTagXmlNode(true);

                    ClearXmlNode(mySELECTXmlNode);

                    TagBase mySelectTag = TagXmlNodeToTag(mySELECTXmlNode);

                    ParseBlock(mySELECTXmlNode, mySelectTag, value, 0);

                    #endregion
                }
            }
        }
        #endregion

        #region From
        /// <summary>
        /// Returns the xml node which corresponds to the Select tag.
        /// If this node does not exist, creates a new one (if needed).
        /// </summary>
        private XmlNode GetFromTagXmlNode(bool createNew)
        {
            XmlNode myFromNode = ParsedDocument.SelectSingleNode(string.Format(@"{0}/{1}[@{2}='{3}']", cRootXmlNodeName, cTagXmlNodeName, cTagTypeXmlAttributeName, FromTag.cTagName));

            if (myFromNode == null && createNew)
            {
                FromTag myFromTag = new FromTag();
                myFromTag.InitializeFromData(this, null, false);
                myFromNode = CreateTagXmlNode(myFromTag);

                XmlNode mySelectNode = GetSelectTagXmlNode(false);
                mySelectNode.ParentNode.InsertAfter(myFromNode, mySelectNode);
            }

            return myFromNode;
        }


        /// <summary>
        /// Gets or sets the From clause of the parsed sql.
        /// </summary>
        public string From
        {
            get
            {
                #region Get the From xml node

                XmlNode myFromTagXmlNode = GetFromTagXmlNode(false);
                if (myFromTagXmlNode == null)
                    return string.Empty;

                #endregion

                StringBuilder myStringBuilder = new StringBuilder();
                XmlNodesToText(myStringBuilder, myFromTagXmlNode.ChildNodes);
                return myStringBuilder.ToString();
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Impossibile elaborare una query sql senza il tag From");
                }
                else
                {
                    #region Modify the From xml node

                    XmlNode myFromXmlNode = GetFromTagXmlNode(true);

                    ClearXmlNode(myFromXmlNode);

                    TagBase myFromTag = TagXmlNodeToTag(myFromXmlNode);

                    ParseBlock(myFromXmlNode, myFromTag, value, 0);

                    #endregion
                }
            }
        }
        #endregion

        #region Insert
        /// <summary>
        /// Returns the xml node which corresponds to the Into tag.
        /// If this node does not exist, creates a new one (if needed).
        /// </summary>
        private XmlNode GetInsertTagXmlNode(bool createNew)
        {
            XmlNode myInsertNode = ParsedDocument.SelectSingleNode(string.Format(@"{0}/{1}[@{2}='{3}']", cRootXmlNodeName, cTagXmlNodeName, cTagTypeXmlAttributeName, InsertIntoTag.cTagName));

            if (myInsertNode == null && createNew)
            {
                InsertIntoTag myInsertTag = new InsertIntoTag();
                myInsertTag.InitializeFromData(this, null, false);
                myInsertNode = CreateTagXmlNode(myInsertTag);

                XmlNode myValuesNode = GetValuesTagXmlNode(false);
                myValuesNode.ParentNode.InsertBefore(myInsertNode, myValuesNode);
            }

            return myInsertNode;
        }


        /// <summary>
        /// Gets or sets the INTO clause of the parsed sql.
        /// </summary>
        public string Insert
        {
            get
            {
                #region Get the Insert xml node

                XmlNode myInsertTagXmlNode = GetInsertTagXmlNode(false);
                if (myInsertTagXmlNode == null)
                    return string.Empty;

                #endregion

                StringBuilder myStringBuilder = new StringBuilder();
                XmlNodesToText(myStringBuilder, myInsertTagXmlNode.ChildNodes);
                return myStringBuilder.ToString();
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Impossibile elaborare una query sql senza il tag Insert");
                }
                else
                {
                    #region Modify the Insert xml node

                    XmlNode myInsertXmlNode = GetInsertTagXmlNode(true);

                    ClearXmlNode(myInsertXmlNode);

                    TagBase myInsertTag = TagXmlNodeToTag(myInsertXmlNode);

                    ParseBlock(myInsertXmlNode, myInsertTag, value, 0);

                    #endregion
                }
            }
        }
        #endregion

        #region Values
        /// <summary>
        /// Returns the xml node which corresponds to the Into tag.
        /// If this node does not exist, creates a new one (if needed).
        /// </summary>
        private XmlNode GetValuesTagXmlNode(bool createNew)
        {
            XmlNode myValuesNode = ParsedDocument.SelectSingleNode(string.Format(@"{0}/{1}[@{2}='{3}']", cRootXmlNodeName, cTagXmlNodeName, cTagTypeXmlAttributeName, ValuesTag.cTagName));

            if (myValuesNode == null && createNew)
            {
                ValuesTag myValuesTag = new ValuesTag();
                myValuesTag.InitializeFromData(this, null, false);
                myValuesNode = CreateTagXmlNode(myValuesTag);

                XmlNode myInsertNode = GetInsertTagXmlNode(false);
                myInsertNode.ParentNode.InsertAfter(myValuesNode, myInsertNode);
            }

            return myValuesNode;
        }


        /// <summary>
        /// Gets or sets the INTO clause of the parsed sql.
        /// </summary>
        public string Values
        {
            get
            {
                #region Get the Values xml node

                XmlNode myValuesTagXmlNode = GetValuesTagXmlNode(false);
                if (myValuesTagXmlNode == null)
                    return string.Empty;

                #endregion

                StringBuilder myStringBuilder = new StringBuilder();
                XmlNodesToText(myStringBuilder, myValuesTagXmlNode.ChildNodes);
                return myStringBuilder.ToString();
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Impossibile elaborare una query sql senza il tag Values");
                }
                else
                {
                    #region Modify the Values xml node

                    XmlNode myValuesXmlNode = GetValuesTagXmlNode(true);

                    ClearXmlNode(myValuesXmlNode);

                    TagBase myValuesTag = TagXmlNodeToTag(myValuesXmlNode);

                    ParseBlock(myValuesXmlNode, myValuesTag, value, 0);

                    #endregion
                }
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Returns the xml node which corresponds to the Into tag.
        /// If this node does not exist, creates a new one (if needed).
        /// </summary>
        private XmlNode GetUpdateTagXmlNode(bool createNew)
        {
            XmlNode Node = ParsedDocument.SelectSingleNode(string.Format(@"{0}/{1}[@{2}='{3}']", cRootXmlNodeName, cTagXmlNodeName, cTagTypeXmlAttributeName, UpdateTag.cTagName));

            if (Node == null && createNew)
            {
                UpdateTag Tag = new UpdateTag();
                Tag.InitializeFromData(this, null, false);
                Node = CreateTagXmlNode(Tag);

                XmlNode RelativeNode = GetSetTagXmlNode(false);
                RelativeNode.ParentNode.InsertBefore(Node, RelativeNode);
            }

            return Node;
        }


        /// <summary>
        /// Gets or sets the INTO clause of the parsed sql.
        /// </summary>
        public string Update
        {
            get
            {
                #region Get the Update xml node

                XmlNode myUpdateTagXmlNode = GetUpdateTagXmlNode(false);
                if (myUpdateTagXmlNode == null)
                    return string.Empty;

                #endregion

                StringBuilder myStringBuilder = new StringBuilder();
                XmlNodesToText(myStringBuilder, myUpdateTagXmlNode.ChildNodes);
                return myStringBuilder.ToString();
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Impossibile elaborare una query sql senza il tag Update");
                }
                else
                {
                    #region Modify the Update xml node

                    XmlNode myUpdateXmlNode = GetUpdateTagXmlNode(true);

                    ClearXmlNode(myUpdateXmlNode);

                    TagBase myUpdateTag = TagXmlNodeToTag(myUpdateXmlNode);

                    ParseBlock(myUpdateXmlNode, myUpdateTag, value, 0);

                    #endregion
                }
            }
        }
        #endregion

        #region Set
        /// <summary>
        /// Returns the xml node which corresponds to the Into tag.
        /// If this node does not exist, creates a new one (if needed).
        /// </summary>
        private XmlNode GetSetTagXmlNode(bool createNew)
        {
            XmlNode Node = ParsedDocument.SelectSingleNode(string.Format(@"{0}/{1}[@{2}='{3}']", cRootXmlNodeName, cTagXmlNodeName, cTagTypeXmlAttributeName, SetTag.cTagName));

            if (Node == null && createNew)
            {
                SetTag Tag = new SetTag();
                Tag.InitializeFromData(this, null, false);
                Node = CreateTagXmlNode(Tag);

                XmlNode RelativeNode = GetUpdateTagXmlNode(false);
                RelativeNode.ParentNode.InsertAfter(Node, RelativeNode);
            }

            return Node;
        }


        /// <summary>
        /// Gets or sets the INTO clause of the parsed sql.
        /// </summary>
        public string Set
        {
            get
            {
                #region Get the Set xml node

                XmlNode mySetTagXmlNode = GetSetTagXmlNode(false);
                if (mySetTagXmlNode == null)
                    return string.Empty;

                #endregion

                StringBuilder myStringBuilder = new StringBuilder();
                XmlNodesToText(myStringBuilder, mySetTagXmlNode.ChildNodes);
                return myStringBuilder.ToString();
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Impossibile elaborare una query sql senza il tag Set");
                }
                else
                {
                    #region Modify the Set xml node

                    XmlNode mySetXmlNode = GetSetTagXmlNode(true);

                    ClearXmlNode(mySetXmlNode);

                    TagBase mySetTag = TagXmlNodeToTag(mySetXmlNode);

                    ParseBlock(mySetXmlNode, mySetTag, value, 0);

                    #endregion
                }
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Returns the xml node which corresponds to the Into tag.
        /// If this node does not exist, creates a new one (if needed).
        /// </summary>
        private XmlNode GetDeleteTagXmlNode(bool createNew)
        {
            XmlNode Node = ParsedDocument.SelectSingleNode(string.Format(@"{0}/{1}[@{2}='{3}']", cRootXmlNodeName, cTagXmlNodeName, cTagTypeXmlAttributeName, DeleteTag.cTagName));

            if (Node == null && createNew)
            {
                DeleteTag Tag = new DeleteTag();
                Tag.InitializeFromData(this, null, false);
                Node = CreateTagXmlNode(Tag);

                XmlNode RelativeNode = GetFromTagXmlNode(false);
                RelativeNode.ParentNode.InsertBefore(Node, RelativeNode);
            }

            return Node;
        }


        /// <summary>
        /// Gets or sets the INTO clause of the parsed sql.
        /// </summary>
        public string Delete
        {
            get
            {
                #region Get the Delete xml node

                XmlNode myDeleteTagXmlNode = GetDeleteTagXmlNode(false);
                if (myDeleteTagXmlNode == null)
                    return string.Empty;

                #endregion

                StringBuilder myStringBuilder = new StringBuilder();
                XmlNodesToText(myStringBuilder, myDeleteTagXmlNode.ChildNodes);
                return myStringBuilder.ToString();
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Impossibile elaborare una query sql senza il tag Delete");
                }
                else
                {
                    #region Modify the Delete xml node

                    XmlNode myDeleteXmlNode = GetDeleteTagXmlNode(true);

                    ClearXmlNode(myDeleteXmlNode);

                    TagBase myDeleteTag = TagXmlNodeToTag(myDeleteXmlNode);

                    ParseBlock(myDeleteXmlNode, myDeleteTag, value, 0);

                    #endregion
                }
            }
        }
        #endregion
    }

	#endregion
}
