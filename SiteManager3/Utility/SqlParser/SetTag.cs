﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SqlParser
{
	#region SetTag

	[TagType(SetTag.cTagName)]
	[MatchSetTag]
    internal class SetTag : SimpleOneWordTag
	{
		#region Consts

		/// <summary>
		/// The name of the tag (its identifier).
		/// </summary>
		public const string cTagName = "SET";

		#endregion

		#region Properties

		/// <summary>
		/// Gets the name of the tag (its identifier and sql text)
		/// </summary>
		protected override string Name
		{
			get 
			{
				return cTagName;
			}
		}

		#endregion
	}

	#endregion

	#region MatchSelectTagAttribute

    internal class MatchSetTagAttribute : MatchSimpleOneWordTagAttribute
	{
		#region Properties

		/// <summary>
		/// Gets the name of the tag (its identifier and sql text)
		/// </summary>
		protected override string Name
		{
			get 
			{
				return SetTag.cTagName;
			}
		}

		#endregion
	}

	#endregion
}
