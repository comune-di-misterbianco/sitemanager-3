﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SqlParser
{
	#region InsertInto

    [TagType(InsertIntoTag.cTagName)]
    [MatchInsertIntoTag]
    internal class InsertIntoTag : SimpleTwoWordTag
	{
		#region Consts

		/// <summary>
		/// The name of the tag (its identifier).
		/// </summary>
        public const string cTagName = "INSERT_INTO";

		/// <summary>
		/// The first part of tag.
		/// </summary>
        public const string cTagFirstPart = "INSERT";

		/// <summary>
		/// The second part of tag.
		/// </summary>
        public const string cTagSecondPart = "INTO";

		#endregion

		#region Properties

		/// <summary>
		/// Gets the name of the tag.
		/// </summary>
		protected override string Name 
		{
			get
			{
				return cTagName;
			}
		}

		/// <summary>
		/// Gets the first word of the tag.
		/// </summary>
		protected override string FirstWord
		{
			get
			{
				return cTagFirstPart;
			}
		}

		/// <summary>
		/// Gets the second word of the tag.
		/// </summary>
		protected override string SecondWord
		{
			get
			{
				return cTagSecondPart;
			}
		}

		#endregion
	}

	#endregion

	#region MatchOrderByTagAttribute

	internal class MatchInsertIntoTagAttribute : MatchSimpleTwoWordTagAttribute
	{
		#region Properties

		/// <summary>
		/// Gets the name of the tag (its identifier and sql text)
		/// </summary>
		protected override string Name
		{
			get
			{
                return InsertIntoTag.cTagName;
			}
		}

		/// <summary>
		/// Gets the first word of the tag.
		/// </summary>
		protected override string FirstWord 
		{
			get
			{
                return InsertIntoTag.cTagFirstPart;
			}
		}

		/// <summary>
		/// Gets the second word of the tag.
		/// </summary>
		protected override string SecondWord
		{
			get
			{
                return InsertIntoTag.cTagSecondPart;
			}
		}

		#endregion
	}

	#endregion
}
