﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SqlParser
{
	#region ValuesTag

	[TagType(ValuesTag.cTagName)]
	[MatchValuesTag]
    internal class ValuesTag : SimpleOneWordTag
	{
		#region Consts

		/// <summary>
		/// The name of the tag (its identifier).
		/// </summary>
		public const string cTagName = "VALUES";

		#endregion

		#region Properties

		/// <summary>
		/// Gets the name of the tag (its identifier and sql text)
		/// </summary>
		protected override string Name
		{
			get 
			{
				return cTagName;
			}
		}

		#endregion
	}

	#endregion

	#region MatchSelectTagAttribute

    internal class MatchValuesTagAttribute : MatchSimpleOneWordTagAttribute
	{
		#region Properties

		/// <summary>
		/// Gets the name of the tag (its identifier and sql text)
		/// </summary>
		protected override string Name
		{
			get 
			{
				return ValuesTag.cTagName;
			}
		}

		#endregion
	}

	#endregion
}
