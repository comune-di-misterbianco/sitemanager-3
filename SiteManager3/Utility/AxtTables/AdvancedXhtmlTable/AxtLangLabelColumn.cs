using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace G2Core.AdvancedXhtmlTable
{
    public class AxtLangLabelColumn:AxtColumn
    {
        private string _IconCssClass;
        public string IconCssClass
        {
            get
            {
                if (_IconCssClass == null)
                {
                    _IconCssClass = "TreeColumn_DefaultIcon";
                }
                return _IconCssClass;
            }
            set { _IconCssClass = value; }
        }
        protected override bool ExportDataDefaultValue
        {
            get { return true; }
        }
        private string _IndentBy;
        public string IndentBy
        {
            get { return _IndentBy; }
            set { _IndentBy = value; }
        }

        private int _IndentOffset = 0;
        public int IndentOffset
        {
            get { return _IndentOffset; }
            set { _IndentOffset = value; }
        }

        private bool _LoadDefaultValue;
        public bool LoadDefaultValue
        {
            get
            {
                return _LoadDefaultValue;
            }
            set
            {
                _LoadDefaultValue = value;
            }
        }

        public enum MultiFieldModes { None,Serial,Alternate }

        protected MultiFieldModes _MultiFieldMode;
        public MultiFieldModes MultiFieldMode
        {
            get
            {
                return _MultiFieldMode;
            }
            set
            {
                _MultiFieldMode = value;
            }
        }

        private G2Core.Common.StringCollection _Separators;
        public G2Core.Common.StringCollection Separators
        {
            get
            {
                if (_Separators == null)
                    _Separators = new G2Core.Common.StringCollection();
                return _Separators;
            }
        }

        private G2Core.Languages.LangDataSource LangData;

        /// <summary>
        /// Rappresenta una collonna da agganciare a un ogetto della class AxtTable per visualizzare i record da un database
        /// </summary>
        /// <param name="databaseFieldName">Il nome nel database del campo da rappresentare</param>
        /// <param name="caption">Il titolo della colonna</param>
        public AxtLangLabelColumn(G2Core.Languages.LangDataSource langData,string databaseFieldName, string caption)
            :base(databaseFieldName,caption)
        {
            _MultiFieldMode = MultiFieldModes.None;
            LangData = langData;
        }

        public override HtmlGenericControl GetCellControl(DataRow row)
        {
            string CellText = null;
            string[] fields;

            G2Core.Languages.LangLabel label;
            switch(this.MultiFieldMode)
            {

                case MultiFieldModes.None:
                    if (row.Table.Columns.Contains(this.DatabaseFieldName))
                        CellText = new G2Core.Languages.LangLabel(int.Parse(row[this.DatabaseFieldName].ToString()), 
                                                                  LangData, 
                                                                  this.LoadDefaultValue ? G2Core.Languages.LangLabel.LabelPickModes.LoadDefault : G2Core.Languages.LangLabel.LabelPickModes.OnlyCurrent).ValueByDefaultLang;

                    break;

                case MultiFieldModes.Alternate:
                    fields = this.DatabaseFieldName.Split(',');
                    CellText = null;
                    
                    int i=0;
                    while(CellText == null && i<fields.Length)
                    {
                        if (row.Table.Columns.Contains(fields[i]))
                        {
                            CellText = new G2Core.Languages.LangLabel(int.Parse(row[fields[i]].ToString()),
                                                                LangData,
                                                                this.LoadDefaultValue ? G2Core.Languages.LangLabel.LabelPickModes.LoadDefault : G2Core.Languages.LangLabel.LabelPickModes.OnlyCurrent).ValueByDefaultLang;
                        }
                        i++;
                    }
                    break;

                case MultiFieldModes.Serial:
                    fields = this.DatabaseFieldName.Split(',');
                    CellText = "";
                    int sepsCount = 0;
                    foreach (string field in fields)
                    {
                        if (row.Table.Columns.Contains(field))
                        {
                            CellText += new G2Core.Languages.LangLabel(int.Parse(row[field].ToString()),
                                                                    LangData,
                                                                    this.LoadDefaultValue ? G2Core.Languages.LangLabel.LabelPickModes.LoadDefault : G2Core.Languages.LangLabel.LabelPickModes.OnlyCurrent).ValueByDefaultLang;
                        
                        }
                        if(Separators!=null)
                            CellText += Separators[sepsCount++];
                    }
                    CellText = CellText.Trim();
                    break;
            }

            string CellHref = this.Href;
            string CellTitle = this.HyperlinkTitle;

            HtmlGenericControl td = new HtmlGenericControl("td");
            td.Attributes["class"] = this.CssClass;

            if (ColSpan > 0)
                td.Attributes["colspan"] = ColSpan.ToString();

            if (CutTextAt > 0 && CellText.Length > CutTextAt)
            {
                CellText = CellText.Remove(CutTextAt);
                if (AddDots)
                    CellText += "...";
            }

            if (this.IndentBy != null && this.IndentBy.Length > 0)
            {
                td.InnerHtml += BuildIndentLines(row);

            }

            if (Href!=null && Href.Length > 0)
            {
                if (DatabaseHrefValueFields != null)
                    foreach (string col in DatabaseHrefValueFields)
                    {
                        if (CellHref!=null && CellHref.Length > 0)
                            CellHref = CellHref.Replace("{" + col + "}", row[col].ToString());
                        if (CellTitle!=null && CellTitle.Length > 0)
                            CellTitle = CellTitle.Replace("{" + col + "}", row[col].ToString());
                    }

                td.InnerHtml += "<a href=\"" + CellHref + "\" title=\"" + CellTitle + "\"><span>&nbsp;" + CellText + "</span></a>";
            }
            else
                td.InnerHtml += "<span>&nbsp;" + CellText + "</span>";

            return td;
        }
        private string BuildIndentLines(DataRow row)
        {
            int Depth = int.Parse(row[this.IndentBy].ToString());
            int RowIndex = row.Table.Rows.IndexOf(row);

            int NextRowDepth = 0;

            bool HasNextRow = row.Table.Rows.Count > RowIndex + 1;

            if (HasNextRow)
                NextRowDepth = int.Parse(row.Table.Rows[RowIndex + 1][this.IndentBy].ToString());


            string lines = "";

            for (int i = 0; i < Depth - IndentOffset; i++)
            {
                string ClassName = " ";

                if (RowIndex == 0) ClassName = " TreeColumn_LinesVoid";
                else
                {
                    if (!HasNextRow) ClassName = " TreeColumn_LinesBottom";
                    else
                    {
                        if (NextRowDepth < Depth && i == 0) ClassName = " TreeColumn_LinesBottom";
                        else
                            if (NextRowDepth == Depth && i == 0) ClassName = " TreeColumn_LinesJoin";

                    }
                }
                lines = "<span class=\"TreeColumn_Lines" + ClassName + "\">&nbsp;</span>" + lines;
            }
            string icon = "";
            icon = "<span class=\"TreeColumn_Icon " + this.IconCssClass + "\">&nbsp;</span>";

            return lines + icon;
        }

        public override string GetExportValue(DataRow row)
        {
            string CellText = null;
            string[] fields;

            G2Core.Languages.LangLabel label;
            switch (this.MultiFieldMode)
            {

                case MultiFieldModes.None:
                    if (row.Table.Columns.Contains(this.DatabaseFieldName))
                        CellText = new G2Core.Languages.LangLabel(int.Parse(row[this.DatabaseFieldName].ToString()),
                                                                  LangData,
                                                                  this.LoadDefaultValue ? G2Core.Languages.LangLabel.LabelPickModes.LoadDefault : G2Core.Languages.LangLabel.LabelPickModes.OnlyCurrent).ValueByDefaultLang;

                    break;

                case MultiFieldModes.Alternate:
                    fields = this.DatabaseFieldName.Split(',');
                    CellText = null;

                    int i = 0;
                    while (CellText == null && i < fields.Length)
                    {
                        if (row.Table.Columns.Contains(fields[i]))
                        {
                            CellText = new G2Core.Languages.LangLabel(int.Parse(row[fields[i]].ToString()),
                                                                LangData,
                                                                this.LoadDefaultValue ? G2Core.Languages.LangLabel.LabelPickModes.LoadDefault : G2Core.Languages.LangLabel.LabelPickModes.OnlyCurrent).ValueByDefaultLang;
                        }
                        i++;
                    }
                    break;

                case MultiFieldModes.Serial:
                    fields = this.DatabaseFieldName.Split(',');
                    CellText = "";
                    int sepsCount = 0;
                    foreach (string field in fields)
                    {
                        if (row.Table.Columns.Contains(field))
                        {
                            CellText += new G2Core.Languages.LangLabel(int.Parse(row[field].ToString()),
                                                                    LangData,
                                                                    this.LoadDefaultValue ? G2Core.Languages.LangLabel.LabelPickModes.LoadDefault : G2Core.Languages.LangLabel.LabelPickModes.OnlyCurrent).ValueByDefaultLang;

                        }
                        if (Separators != null)
                            CellText += Separators[sepsCount++];
                    }
                    CellText = CellText.Trim();
                    break;
            }

            string CellHref = this.Href;
            string CellTitle = this.HyperlinkTitle;

            if (CutTextAt > 0 && CellText.Length > CutTextAt)
            {
                CellText = CellText.Remove(CutTextAt);
                if (AddDots)
                    CellText += "...";
            }

            if (Href != null && Href.Length > 0)
            {
                if (DatabaseHrefValueFields != null)
                    foreach (string col in DatabaseHrefValueFields)
                    {
                        if (CellHref != null && CellHref.Length > 0)
                            CellHref = CellHref.Replace("{" + col + "}", row[col].ToString());
                        if (CellTitle != null && CellTitle.Length > 0)
                            CellTitle = CellTitle.Replace("{" + col + "}", row[col].ToString());
                    }

                return CellText;
            }
            else
                return CellText;
        }
    }
}