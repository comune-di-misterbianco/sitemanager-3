using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace G2Core.AdvancedXhtmlTable
{
    /// <summary>
    /// Rappresenta una collonna da agganciare a un oggetto della class AxtTable per visualizzare i record da un database
    /// </summary>
    public abstract class AxtColumn
    {
        /// <summary>
        /// Rappresenta il nome (sul database) del campo da rappresentare.
        /// </summary>
        public string DatabaseFieldName 
        { 
            get 
            {
                return _DatabaseFieldName; 
            } 
        }
        private string _DatabaseFieldName;

        /// <summary>
        /// Rappresenta l'etichetta dell'intestazione della colonna.
        /// </summary>
        public string Caption 
        { 
            get 
            { 
                return _Caption; 
            } 
        }
        private string _Caption;

        /// <summary>
        /// Indica se i dati di questa colonna dovranno essere rappresentati all'interno delle tabelle di esportazione
        /// </summary>
        public bool ExportData
        {
            get
            {
                return _ExportData;
            }
            set
            {
                _ExportData = value;
            }
        }
        private bool _ExportData;

        protected abstract bool ExportDataDefaultValue { get; }

        public string stringFormat = "{0}";

        /// <summary>
        /// Rappresenta il controllo dell'intestazione della colonna.
        /// </summary>
        public HtmlGenericControl CaptionControl
        {
            get
            {
                if (_CaptionControl == null)
                {
                    _CaptionControl = new HtmlGenericControl("span");
                    //_CaptionControl.Attributes["href"] = "javascript: alert('Implementare ordinamendo per colonna');";
                    _CaptionControl.InnerHtml = this.Caption;
                }
                return _CaptionControl;
            }
        }
        private HtmlGenericControl _CaptionControl;

        /// <summary>
        /// Rappresenta un intero che imposta la propriet� xhtml colspan per la colonna.
        /// </summary>
        public int ColSpan
        {
            get { return _ColSpan; }
            set { _ColSpan = value; }
        }
        private int _ColSpan;

        /// <summary>
        /// La quantita massima di caratteri oltre il quale il valore della cella viene tagliato, 0 per non tagliare
        /// </summary>
        public int CutTextAt
        {
            get { return _CutTextAt; }
            set { _CutTextAt = value; }
        }
        private int _CutTextAt = 0;

        /// <summary>
        /// Se messo a 'true', nel caso in cui il contenuto della cella venga tagliato, saranno aggiunti tre punti alla fine del contenuto della cella stessa
        /// </summary>
        public bool AddDots
        {
            get { return _AddDots; }
            set { _AddDots = value; }
        }
        private bool _AddDots = true;

        /// <summary>
        /// Se messo a 'true', l'intera riga a cui appartiene la cella verr� saltata
        /// </summary>
        public bool SkipRow
        {
            get { return _SkipRow; }
            protected set { _SkipRow = value; }
        }
        private bool _SkipRow = false;

        /// <summary>
        /// Se messo a 'true', la cella non verr� riempita
        /// </summary>
        public bool SkipCell
        {
            get { return _SkipCell; }
            protected set { _SkipCell = value; }
        }
        private bool _SkipCell = false;

        /// <summary>
        /// Rappresenta l'href per ogni cella, scrivendo {NOME_CAMPO_DATABASE} si crea un segnaposto per l'utilizzo della propriet� DatabaseHrefValueFieldName
        /// </summary>
        public string Href
        {
            get { return _Href; }
            set { _Href = value; }
        }
        private string _Href;


        public string HrefCssClass
        {
            get { return _HrefCssClass; }
            set { _HrefCssClass = value; }
        }
        private string _HrefCssClass="";

        /// <summary>
        /// Rappresenta il 'title' dell'Hyperlink per ogni cella, scrivendo {NOME_CAMPO_DATABASE} si crea un segnaposto che verr� rimpiazzato dal relativo campo del database
        /// </summary>
        public string HyperlinkTitle
        {
            get { return _HyperlinkTitle; }
            set { _HyperlinkTitle = value; }
        }
        private string _HyperlinkTitle;

        /// <summary>
        /// Rappresenta il un insieme di campi del database dal quale va preso il valore da sostituire ai segnaposto di tipo {NOME_CAMPO_DATABASE} nell'href di ogni cella, 
        /// � possibile non specificare questo valore qualora la propriet� href sia impostata a Null
        /// </summary>
        public G2Core.Common.StringCollection DatabaseHrefValueFields
        {
            get { return _DatabaseHrefValueFields; }
            set { _DatabaseHrefValueFields = value; }
        }
        private G2Core.Common.StringCollection _DatabaseHrefValueFields;

        /// <summary>
        /// Rappresenta l'ID della cella
        /// </summary>
        public string ID
        {
            get 
            {
                if (_ID == null)
                    _ID = this.DatabaseFieldName + "_" + this.GetHashCode();
                return _ID; 
            }
        }
        private string _ID;

        /// <summary>
        /// Rapprestenta la classe css delle celle della colonna
        /// </summary>
        public string CssClass
        {
            get { return _CssClass; }
            set { _CssClass = value; }
        }
        private string _CssClass;

        /// <summary>
        /// Rappresenta una collonna da agganciare a un oggetto della class AxtTable per visualizzare i record da un database
        /// </summary>
        /// <param name="databaseFieldName">Il nome nel database del campo da rappresentare</param>
        /// <param name="caption">Il titolo della colonna</param>
        public AxtColumn(string databaseFieldName, string caption)
        {
            _DatabaseFieldName = databaseFieldName;
            _Caption = caption;
            _DatabaseHrefValueFields = new G2Core.Common.StringCollection();
            this.ExportData = this.ExportDataDefaultValue;
        }

        /// <summary>
        /// Restituisce un HtmlGenericControl contenente i controlli di una cella.
        /// </summary>
        /// <param name="row">La DataRow sorgente della cella.</param>
        /// <returns>HtmlGenericControl contenente i controlli di una cella</returns>
        public abstract HtmlGenericControl GetCellControl(DataRow row);

        /// <summary>
        /// Restituisce una stringa contenente il valore nel caso di esportazione della cella.
        /// Se la classe non permette l'esportazione dei dati, implementare quest o metodo con return null, es:
        /// public override string GetExportValue(DataRow row)
        /// {
        ///     return null;
        /// }
        /// </summary>
        /// <param name="row">La DataRow sorgente della cella.</param>
        /// <returns>Stringa contenente il valore da rappresentare in  caso diesportazione.</returns>
        public abstract string GetExportValue(DataRow row);
    }
}