﻿using System;
using System.Data;
using System.Web.UI.HtmlControls;

namespace G2Core.AdvancedXhtmlTable
{
    public class AxtActionCompareColumn:AxtActionColumn
    {
        public string valueToCompare = "";
        public string columnName = "";

        protected override bool ExportDataDefaultValue
        {
            get { return false; }
        }
        public AxtActionCompareColumn(string databaseFieldName, string caption, string cssclass, string href, string valueToCompare, string columnName)
            : base(databaseFieldName, caption,cssclass,href)
        {
            this.valueToCompare = valueToCompare;
            this.columnName = columnName;
        }

        public override HtmlGenericControl GetCellControl(DataRow row)
        {
            if (row[columnName].ToString() != valueToCompare)
            {
                HtmlGenericControl td = new HtmlGenericControl("td");
                td.InnerHtml = "&nbsp;";
                return td;
            }
            else
            {
                 return base.GetCellControl(row);
            }
        }
    }
}
