using System;
using System.Collections;


namespace G2Core.AdvancedXhtmlTable
{
    /// <summary>
    /// La classe AxtColumnsCollection indicizza oggetti di tipo AxtColumn
    /// </summary>
    public class AxtColumnsCollection : IEnumerable
    {
        private G2Collection coll;
        
        /// <summary>
        /// Rappresenta il numero di colonne attualmente presenti nella collezione
        /// </summary>
        public int Count { get { return coll.Count; } }
        
        /// <summary>
        /// Costruttore della classeAxtColumnsCollection
        /// </summary>
        public AxtColumnsCollection()
        {
            coll = new G2Collection();
        }

        /// <summary>
        /// Aggiunge una AxtColumn all'insieme delle colonne della collezione
        /// </summary>
        /// <param name="column"></param>
        public void Add(AxtColumn column)
        {
            coll.Add(column.ID, column);
        }

        /// <summary>
        /// Restiruisce la colonna associata all'indice dato per parametro.
        /// </summary>
        /// <param name="index">L'indice della colonna desiderata</param>
        /// <returns>La colonna associata all'indice dato per parametro.</returns>
        public AxtColumn this[int index]
        {
            get
            {
                return (AxtColumn)coll[coll.Keys[index]];
            }
        }

        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private AxtColumnsCollection Collection;
            public CollectionEnumerator(AxtColumnsCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}