using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace G2Core.AdvancedXhtmlTable
{
    public class AxtDateColumn:AxtColumn
    {
        public bool ShowDate { get; set; }
        public bool ShowTime { get; set; }
        public string Separator { get; set; } 

        protected override bool ExportDataDefaultValue
        {
            get { return true; }
        }

        /// <summary>
        /// Rappresenta una collonna da agganciare a un oggetto della class AxtTable per visualizzare i record da un database
        /// </summary>
        /// <param name="databaseFieldName">Il nome nel database del campo da rappresentare (� possibile specificare pi� di un campo separandoli con virgole.)</param>
        /// <param name="caption">Il titolo della colonna</param>
        public AxtDateColumn(string databaseFieldName, string caption)
            :base(databaseFieldName,caption)
        {
            ShowDate = true;
            ShowTime = true;
            Separator = "alle";
        }

        /// <summary>
        /// Restituisce il controllo xhtml relativo al record.
        /// </summary>
        /// <param name="row">La DataRow sorgente dati.</param>
        /// <returns>Un HtmlGenericControl contenente i controlli della riga.</returns>
        public override HtmlGenericControl GetCellControl(DataRow row)
        {
            HtmlGenericControl td = new HtmlGenericControl("td");
            td.Attributes["class"] = this.CssClass;

            if (ColSpan > 0)
                td.Attributes["colspan"] = ColSpan.ToString();

            string CellText = row[this.DatabaseFieldName].ToString();
            DateTime date = DateTime.MinValue;
            if (DateTime.TryParse(CellText, out date))
            {
                td.InnerHtml = "<span>&nbsp;";
                if (ShowDate)
                {
                    td.InnerHtml += date.ToShortDateString();
                }
                if (ShowDate && ShowTime)
                    td.InnerHtml += "&nbsp;" + Separator;                

                if (ShowTime)
                {
                    if (ShowDate)
                        td.InnerHtml += "&nbsp;";
                    td.InnerHtml += date.ToShortTimeString();
                }
                td.InnerHtml += "</span>";
            }
            else
                td.InnerHtml = "<span>&nbsp;" + CellText + "</span>";

            return td;
        }


        public override string GetExportValue(DataRow row)
        {
            string CellText = row[this.DatabaseFieldName].ToString();
            DateTime date = DateTime.MinValue;
            if (DateTime.TryParse(CellText, out date))
            {
                CellText = "";
                if (ShowDate)
                {
                    CellText += " ";
                    CellText += date.ToShortDateString();
                }
                if (ShowTime)
                {
                    CellText += " ";
                    CellText += date.ToShortTimeString();
                }
                CellText = CellText.Trim();
            }

            return CellText;
        }
    }
}