using System.Data;
using System.Web.UI.HtmlControls;
using NetCms.Connections;

namespace G2Core.AdvancedXhtmlTable
{
    /// <summary>
    /// La classe AxtExternalColumn estende le funzionalitą di AxtColumn e permette di ottenere valori 
    /// da una tabella di database diversa da quella associata alla relativa AxtTable.
    /// </summary>
    public class AxtExternalQueryColumn:AxtColumn
    {
        private string DisplayMember;

        protected override bool ExportDataDefaultValue
        {
            get { return true; }
        }

        public string SqlQuery { get; private set; } 

        public Connection Connection { get; private set; } 

        /// <summary>
        /// Costruttore della classe AxtExternalQueryColumn
        /// </summary>
        public AxtExternalQueryColumn(string databaseFieldName, string caption, string sqlQuery, string displayMember, NetCms.Connections.Connection connection)
            :base(databaseFieldName,caption)
        {
            SqlQuery = sqlQuery;
            Connection = connection;
            DisplayMember = displayMember;
        }

        /// <summary>
        /// Restituisce il controllo xhtml relativo al record.
        /// </summary>
        /// <param name="row">La DataRow sorgente dati.</param>
        /// <returns>Un HtmlGenericControl contenente i controlli della riga.</returns>
        public override HtmlGenericControl GetCellControl(DataRow row)
        {
            string CellText = row[DatabaseFieldName].ToString();

            if (CellText != null && CellText.Length > 0)  //controllo aggiunto in seguito a eccezione per valori nulli
            {
                var rows = Connection.SqlQuery(string.Format(SqlQuery, CellText)).Rows;
                if (rows.Count > 0)
                    CellText = rows[0][DisplayMember].ToString();
            }
            
            string CellHref = this.Href;
            string CellTitle = this.HyperlinkTitle;

            HtmlGenericControl td = new HtmlGenericControl("td");
            td.Attributes["class"] = this.CssClass;

            if (ColSpan > 0)
                td.Attributes["colspan"] = ColSpan.ToString();

            if (CellHref != null && CellHref.Length > 0)
            {
                if (DatabaseHrefValueFields != null)
                    foreach (string col in DatabaseHrefValueFields)
                    {
                        if (CellHref!=null && CellHref.Length > 0)
                            CellHref = CellHref.Replace("{" + col + "}", row[col].ToString());
                        if (CellTitle!=null && CellTitle.Length > 0)
                            CellTitle = CellTitle.Replace("{" + col + "}", row[col].ToString());
                    }

                td.InnerHtml = "<a href=\"" + CellHref + "\" title=\"" + CellTitle + "\"><span>&nbsp;" + CellText + "</span></a>";
            }
            else
                td.InnerHtml = "<span>&nbsp;" + CellText + "</span>";

            return td;
        }

        public override string GetExportValue(DataRow row)
        {
            string CellText = row[DatabaseFieldName].ToString();

            if (CellText != null && CellText.Length > 0)  //controllo aggiunto in seguito a eccezione per valori nulli
            {
                var rows = Connection.SqlQuery(string.Format(SqlQuery, CellText)).Rows;
                if (rows.Count > 0)
                    CellText = rows[0][DisplayMember].ToString();
            }

            return CellText;
        }
    }
}