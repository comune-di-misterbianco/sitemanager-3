using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace G2Core.AdvancedXhtmlTable
{
    public class AxtGenericColumn:AxtColumn
    {
        /// <summary>
        /// Rappesenta la modalit� di rendering delle celle della colonna.
        /// </summary>
        public enum MultiFieldModes 
        { 
            /// <summary>
            /// Il motore di rendering cercher� nella propriet� DatabaseFieldName un solo nome di campo database
            /// e  provveder� a stamarne il contenuto.
            /// </summary>
            None,
            /// <summary>
            /// Il motore di rendering cercher� nella propriet� DatabaseFieldName una serie di nomi di campi database
            /// separati da virgole e provveder� a stampare in serie tutti i valori separandoli con i valori della propriet� separators
            /// </summary>
            Serial,
            /// <summary>
            /// Il motore di rendering cercher� nella propriet� DatabaseFieldName una serie di nomi di campi database
            /// separati da virgole e provveder� a stampare solo il primo valore non NULL che trover� tra i valori dei campi del record.
            /// </summary>
            Alternate 
        }

        protected override bool ExportDataDefaultValue
        {
            get { return true; }
        }

        /// <summary>
        /// Rappesenta la modalit� di rendering delle celle della colonna. Vedere l'enumeratore MultiFieldModes per ulteriori informazioni
        /// </summary>
        public MultiFieldModes MultiFieldMode
        {
            get
            {
                return _MultiFieldMode;
            }
            set
            {
                _MultiFieldMode = value;
            }
        }
        protected MultiFieldModes _MultiFieldMode;

        /// <summary>
        /// Una collezione di stringhe contenente l'elenco dei separatori da usare nel caso di una colonna impostata come MultiFieldModes.Serial
        /// </summary>
        private G2Core.Common.StringCollection _Separators;
        public G2Core.Common.StringCollection Separators
        {
            get
            {
                if (_Separators == null)
                    _Separators = new G2Core.Common.StringCollection();
                return _Separators;
            }
        }

        /// <summary>
        /// Rappresenta una collonna da agganciare a un oggetto della class AxtTable per visualizzare i record da un database
        /// </summary>
        /// <param name="databaseFieldName">Il nome nel database del campo da rappresentare (� possibile specificare pi� di un campo separandoli con virgole.)</param>
        /// <param name="caption">Il titolo della colonna</param>
        public AxtGenericColumn(string databaseFieldName, string caption)
            :base(databaseFieldName,caption)
        {
            _MultiFieldMode = MultiFieldModes.None;
        }

        /// <summary>
        /// Restituisce il controllo xhtml relativo al record.
        /// </summary>
        /// <param name="row">La DataRow sorgente dati.</param>
        /// <returns>Un HtmlGenericControl contenente i controlli della riga.</returns>
        public override HtmlGenericControl GetCellControl(DataRow row)
        {
            string CellText = null;
            string[] fields;
            switch(this.MultiFieldMode)
            {
                case MultiFieldModes.None:
                    if (row.Table.Columns.Contains(this.DatabaseFieldName))                        
                        CellText =String.Format(stringFormat,row[this.DatabaseFieldName]);
                    else
                        CellText = this.DatabaseFieldName;

                    break;
                case MultiFieldModes.Alternate:
                    fields = this.DatabaseFieldName.Split(',');
                    CellText = null;
                    
                    int i=0;
                    while(CellText == null && i<fields.Length)
                    {
                        if(row.Table.Columns.Contains(fields[i]))
                            CellText = row[fields[i]].ToString();
                        i++;
                    }
                    break;
                case MultiFieldModes.Serial:
                    fields = this.DatabaseFieldName.Split(',');
                    CellText = "";
                    int sepsCount = 0;
                    foreach (string field in fields)
                    {
                        if (row.Table.Columns.Contains(field))
                            CellText += row[field];
                        if(Separators!=null)
                            CellText += Separators[sepsCount++];
                        else
                            CellText += row[field] + " ";
                    }
                    CellText = CellText.Trim();
                    break;
            }

            string CellHref = this.Href;
            string CellTitle = this.HyperlinkTitle;

            HtmlGenericControl td = new HtmlGenericControl("td");
            td.Attributes["class"] = this.CssClass;

            if (ColSpan > 0)
                td.Attributes["colspan"] = ColSpan.ToString();

            if (CutTextAt > 0 && CellText.Length > CutTextAt)
            {
                CellText = CellText.Remove(CutTextAt);
                if (AddDots)
                    CellText += "...";
            }

            if (Href!=null && Href.Length > 0)
            {
                if (DatabaseHrefValueFields != null)
                    foreach (string col in DatabaseHrefValueFields)
                    {
                        if (CellHref!=null && CellHref.Length > 0)
                            CellHref = CellHref.Replace("{" + col + "}", row[col].ToString());
                        if (CellTitle!=null && CellTitle.Length > 0)
                            CellTitle = CellTitle.Replace("{" + col + "}", row[col].ToString());
                    }

                td.InnerHtml = "<a href=\"" + CellHref + "\" title=\"" + CellTitle + "\"><span>&nbsp;" + CellText + "</span></a>";
            }
            else
                td.InnerHtml = "<span>&nbsp;" + CellText + "</span>";

            return td;
        }


        public override string GetExportValue(DataRow row)
        {
            string CellText = null;
            string[] fields;
            switch (this.MultiFieldMode)
            {
                case MultiFieldModes.None:
                    if (row.Table.Columns.Contains(this.DatabaseFieldName))
                        CellText = String.Format(stringFormat, row[this.DatabaseFieldName]);
                    else
                        CellText = this.DatabaseFieldName;

                    break;
                case MultiFieldModes.Alternate:
                    fields = this.DatabaseFieldName.Split(',');
                    CellText = null;

                    int i = 0;
                    while (CellText == null && i < fields.Length)
                    {
                        if (row.Table.Columns.Contains(fields[i]))
                            CellText = row[fields[i]].ToString();
                        i++;
                    }
                    break;
                case MultiFieldModes.Serial:
                    fields = this.DatabaseFieldName.Split(',');
                    CellText = "";
                    int sepsCount = 0;
                    foreach (string field in fields)
                    {
                        if (row.Table.Columns.Contains(field))
                            CellText += row[field];
                        if (Separators != null)
                            CellText += Separators[sepsCount++];
                        else
                            CellText += row[field] + " ";
                    }
                    CellText = CellText.Trim();
                    break;
            }

            string CellHref = this.Href;
            string CellTitle = this.HyperlinkTitle;

            HtmlGenericControl td = new HtmlGenericControl("td");
            td.Attributes["class"] = this.CssClass;

            if (ColSpan > 0)
                td.Attributes["colspan"] = ColSpan.ToString();

            if (CutTextAt > 0 && CellText.Length > CutTextAt)
            {
                CellText = CellText.Remove(CutTextAt);
                if (AddDots)
                    CellText += "...";
            }

            if (Href != null && Href.Length > 0)
            {
                if (DatabaseHrefValueFields != null)
                    foreach (string col in DatabaseHrefValueFields)
                    {
                        if (CellHref != null && CellHref.Length > 0)
                            CellHref = CellHref.Replace("{" + col + "}", row[col].ToString());
                        if (CellTitle != null && CellTitle.Length > 0)
                            CellTitle = CellTitle.Replace("{" + col + "}", row[col].ToString());
                    }

                return CellText;
            }
            else
                return CellText;
        }
    }
}