using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace G2Core.AdvancedXhtmlTable
{
    /// <summary>
    /// Rappresenta una colonna da agganciare a un ogetto della class AxtTable per visualizzare i record da un database
    /// </summary>
    public class AxtCheckBoxColumn : AxtColumn
    {
        private G2Core.Common.StringCollection Relations
        {
            get
            {
                if (_Relations == null)
                    _Relations = new G2Core.Common.StringCollection();
                return _Relations;
            }
        }
        private G2Core.Common.StringCollection _Relations;

        
        public string ControlName
        {
            get
            {
                return _ControlName;
            }
        }
        private string _ControlName;

        protected override bool ExportDataDefaultValue
        {
            get { return false; }
        }

        public string DatatabaseValueFieldName
        {
            get
            {
                return _DatatabaseValueFieldName;
            }
        }
        private string _DatatabaseValueFieldName;

        public bool ControlCheckedRows
        {
            get 
            {
                return _ControlCheckedRows;
            }
            set
            {
                _ControlCheckedRows = value;
            }
        }
        private bool _ControlCheckedRows=true;
                
         public AxtCheckBoxColumn(string controlName, string databaseFieldName, string DatatabaseValueFieldName, string caption)
            : base(databaseFieldName, caption)
        {
            _ControlName = controlName;
            _DatatabaseValueFieldName = DatatabaseValueFieldName;
        }

       /* private string GetOptionByValue(string value)
        {
            //Recupero la posizione del opzione nell'array delle corrispondenze tra valori ed etichette.
            int relativeOpztionIndex = Relations.IndexOf(value);
            //Restituisco il
            return this.Options[relativeOpztionIndex > 0 ? relativeOpztionIndex : 0];
        }*/

        /// <summary>
        /// Restituisce un HtmlGenericControl contenente i controlli della cella.
        /// </summary>
        /// <param name="row">La DataRow Sorgente</param>
        /// <returns>HtmlGenericControl contenente i controlli della cella</returns>
        public override HtmlGenericControl GetCellControl(DataRow row)
        {
            
            //string CellText = GetOptionByValue(row[DatabaseFieldName].ToString());
            //string CellHref = this.Href;
            //string CellTitle = this.HyperlinkTitle;

            HtmlGenericControl td = new HtmlGenericControl("td");
            td.Attributes["class"] = this.CssClass;

            if (ColSpan > 0)
                td.Attributes["colspan"] = ColSpan.ToString();
            string _checked="";
            if (ControlCheckedRows && DatabaseFieldName != "")
            {
                bool result;
                bool value = Boolean.TryParse(row[DatabaseFieldName].ToString(), out result);
                if (result)
                    _checked = "checked";
            }
            td.InnerHtml = "<INPUT TYPE=\"checkbox\" id=\"" + ControlName + "\"  name=\"" + ControlName + "\"" + _checked + " value=\"" + row[DatatabaseValueFieldName] + "\"/>";
            return td;
        }
        public override string GetExportValue(DataRow row)
        {
            return null;
        }
    }
}