using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace G2Core.AdvancedXhtmlTable
{
    /// <summary>
    /// Rappresenta una colonna da agganciare a un ogetto della class AxtTable per visualizzare i record da un database
    /// </summary>
    public class AxtRadioBoxColumn : AxtColumn
    {
        private G2Core.Common.StringCollection Relations
        {
            get
            {
                if (_Relations == null)
                    _Relations = new G2Core.Common.StringCollection();
                return _Relations;
            }
        }
        private G2Core.Common.StringCollection _Relations;

        protected override bool ExportDataDefaultValue
        {
            get { return false; }
        }
        
        public string ControlName
        {
            get
            {
                return _ControlName;
            }
        }
        private string _ControlName;


        public string DatatabaseValueFieldName
        {
            get
            {
                return _DatatabaseValueFieldName;
            }
        }
        private string _DatatabaseValueFieldName;


        public AxtRadioBoxColumn(string controlName, string databaseFieldName, string DatatabaseValueFieldName, string caption)
            : base(databaseFieldName, caption)
        {
            _ControlName = controlName;
            _DatatabaseValueFieldName = DatatabaseValueFieldName;
        }

       

        /// <summary>
        /// Restituisce un HtmlGenericControl contenente i controlli della cella.
        /// </summary>
        /// <param name="row">La DataRow Sorgente</param>
        /// <returns>HtmlGenericControl contenente i controlli della cella</returns>
        public override HtmlGenericControl GetCellControl(DataRow row)
        {           

            HtmlGenericControl td = new HtmlGenericControl("td");
            td.Attributes["class"] = this.CssClass;

            if (ColSpan > 0)
                td.Attributes["colspan"] = ColSpan.ToString();
            string _checked="";
            td.InnerHtml = "<INPUT TYPE=\"radio\" id=\"radio" + row[DatatabaseValueFieldName].ToString() + "\"  name=\"" + ControlName + "\"   value=\"" + row[DatatabaseValueFieldName].ToString() + "\"/>";
            return td;
        }

        public override string GetExportValue(DataRow row)
        {
            return null;
        }
    }
}