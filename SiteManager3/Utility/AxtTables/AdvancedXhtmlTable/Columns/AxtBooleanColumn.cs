using System;
using System.Data;
using System.Web;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace G2Core.AdvancedXhtmlTable
{
    /// <summary>
    /// Rappresenta una colonna da agganciare a un ogetto della class AxtTable per visualizzare i record da un database
    /// </summary>
    public class AxtBooleanColumn:AxtColumn
    {
        /// <summary>
        /// Rappresenta la collezione delle stringhe da associare ai valori su database.
        /// </summary>

        protected override bool ExportDataDefaultValue
        {
            get { return true; }
        }
        public List<Option> Options
        {
            get { return _Options; }
        }
        private List<Option> _Options;

        public bool usetrue_false=false;

        public class Option
        {
            private string _Label;
            public string Label
            {
                get { return _Label; }
                set { _Label = value; }
            }

            private string _Link;
            public string Link
            {
                get { return _Link; }
                set { _Link = value; }
            }

            private string _CssClass;
            public string CssClass
            {
                get { return _CssClass; }
                set { _CssClass = value; }
            }
            public override string ToString()
            {
                return FormatOption();
            }

            private string FormatOption()
            {
                string value = "<td {2}><{0} {1}>{3}</{0}></td>";

                string tagname = "span";
                string cssClass = "";
                string link = "";
                string label = this.Label;

                if (this.CssClass != null && this.CssClass.Length > 0)
                    cssClass = "class=\"" + this.CssClass + "\"";

                if (this.Link != null && this.Link.Length > 0)
                {
                    link = "href\"" + this.Link + "\"";
                    tagname = "a";
                    label = "<span>" + label + "</span>";
                }

                return String.Format(value, tagname, link, cssClass, label);
            }
            public HtmlGenericControl GetControl()
            {
                return GetControl("");
            }
            public HtmlGenericControl GetControl(string LinkValue)
            {
                HtmlGenericControl td = new HtmlGenericControl("td");
                string value = "<{0} {1}>{2}</{0}>";

                string tagname = "span";
                string link = "";
                string label = "&nbsp;" + this.Label;

                if (this.CssClass != null && this.CssClass.Length > 0)
                    td.Attributes["class"] = this.CssClass;

                if (this.Link != null && this.Link.Length > 0)
                {
                    link = "href=\"";
                    if (LinkValue.Length > 0)
                        link += String.Format(this.Link, LinkValue);
                    else
                        link += this.Link;
                    link += "\"";
                    tagname = "a";
                    label = "<span>" + label + "</span>";
                }

                td.InnerHtml = String.Format(value, tagname, link, label);
                return td;
            }
        }

        private G2Core.Common.StringCollection Relations
        {
            get { 
                if(_Relations == null)
                    _Relations = new G2Core.Common.StringCollection();
                return _Relations; 
            }
        }
        private G2Core.Common.StringCollection _Relations;
        /// <summary>
        /// Rappresenta una collonna da agganciare a un ogetto della class AxtTable per visualizzare i record da un database
        /// </summary>
        /// <param name="databaseFieldName">Il nome nel database del campo da rappresentare</param>
        /// <param name="caption">Il titolo della colonna</param>        
        public AxtBooleanColumn(string databaseFieldName, string caption)
            : base(databaseFieldName, caption)
        {
            _Options = new List<Option>();
           
        }
        /// <summary>
        /// Rappresenta una collonna da agganciare a un ogetto della class AxtTable per visualizzare i record da un database
        /// </summary>
        /// <param name="databaseFieldName">Il nome nel database del campo da rappresentare</param>
        /// <param name="caption">Il titolo della colonna</param>        
        /// <param name="optionsValues">L'elenco delle possibili opzioni, come stringa contenente le opzioni separate da virgole.</param>        
        public AxtBooleanColumn(string databaseFieldName, string caption,string optionsValues)
            :base(databaseFieldName,caption)
        {
            _Options = new List<Option>();
            foreach (string s in optionsValues.Split(','))
            {
                Option opt = new Option();
                opt.Label = s;
                _Options.Add(opt);
            }
        }

        private Option GetOptionByValue(string value)
        {            
            //Recupero la posizione del opzione nell'array delle corrispondenze tra valori ed etichette.            
            int relativeOpztionIndex = Relations.IndexOf(value);            
            //Restituisco il
            return this.Options[relativeOpztionIndex>0?relativeOpztionIndex:0]; 
        }
        
        /// <summary>
        /// Restituisce un HtmlGenericControl contenente i controlli della cella.
        /// </summary>
        /// <param name="row">La DataRow Sorgente</param>
        /// <returns>HtmlGenericControl contenente i controlli della cella</returns>
        public override HtmlGenericControl GetCellControl(DataRow row)
        {
            if (Relations.Count == 0)
            {
                for (int i = 0; i < Options.Count; i++)
                    if(!usetrue_false)
                        Relations.Add(i.ToString());                    
                    else
                        Relations.Add(Convert.ToBoolean(i).ToString());                    
            }
            Option CellData = GetOptionByValue(row[DatabaseFieldName].ToString());

            HtmlGenericControl td = new HtmlGenericControl("td");
            td.Attributes["class"] = this.CssClass + " "+CellData.CssClass;

            if (ColSpan > 0)
                td.Attributes["colspan"] = ColSpan.ToString();

            if (CellData.Link != null && CellData.Link.Length > 0)
            {
                string cellDataLink = CellData.Link;
                string cellDataLabel = CellData.Label;

                if (DatabaseHrefValueFields != null)
                    foreach (string col in DatabaseHrefValueFields)
                    {
                        if (CellData.Link != null && CellData.Link.Length > 0)
                            cellDataLink = CellData.Link.Replace("{" + col + "}", row[col].ToString());
                        if (CellData.Label != null && CellData.Label.Length > 0)
                            cellDataLabel = CellData.Label.Replace("{" + col + "}", row[col].ToString());
                    }

                td.InnerHtml = "<a href=\"" + cellDataLink + "\" title=\"" + cellDataLabel + "\"><span>&nbsp;" + cellDataLabel + "</span></a>";
            }
            else
                td.InnerHtml = "<span>&nbsp;" + CellData.Label + "</span>";

            return td;
        }

        public override string GetExportValue(DataRow row)
        {
            if (Relations.Count == 0)
            {
                for (int i = 0; i < Options.Count; i++)
                    if (!usetrue_false)
                        Relations.Add(i.ToString());
                    else
                        Relations.Add(Convert.ToBoolean(i).ToString());
            }
            Option CellData = GetOptionByValue(row[DatabaseFieldName].ToString());

            HtmlGenericControl td = new HtmlGenericControl("td");
            td.Attributes["class"] = this.CssClass + " " + CellData.CssClass;

            if (ColSpan > 0)
                td.Attributes["colspan"] = ColSpan.ToString();

            if (CellData.Link != null && CellData.Link.Length > 0)
            {
                string cellDataLink = CellData.Link;
                string cellDataLabel = CellData.Label;

                if (DatabaseHrefValueFields != null)
                    foreach (string col in DatabaseHrefValueFields)
                    {
                        if (CellData.Link != null && CellData.Link.Length > 0)
                            cellDataLink = CellData.Link.Replace("{" + col + "}", row[col].ToString());
                        if (CellData.Label != null && CellData.Label.Length > 0)
                            cellDataLabel = CellData.Label.Replace("{" + col + "}", row[col].ToString());
                    }

                return cellDataLabel;
            }
            else
                return CellData.Label;
        }
    }
}