using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace G2Core.AdvancedXhtmlTable
{
    /// <summary>
    /// La classe AxtExternalColumn estende le funzionalitą di AxtColumn e permette di ottenere valori 
    /// da una tabella di database diversa da quella associata alla relativa AxtTable.
    /// </summary>
    public class AxtExternalColumn:AxtColumn
    {
        private string DisplayMember;
        private string JoinField;

        protected override bool ExportDataDefaultValue
        {
            get { return true; }
        }

        /// <summary>
        /// La DataTable esterna di riferimento, per i valori
        /// </summary>
        public DataTable ExternalTable
        {
            get
            {
                return _ExternalTable;
            } 
        }
        private DataTable _ExternalTable;

        /// <summary>
        /// Costruttore della classe AxtExternalColumn
        /// </summary>
        /// <param name="databaseFieldName">Il nome sul database  del campo associato al AxtExternalColumn nella tabella di database sorgente</param>
        /// <param name="caption">L'etichetta della colonna</param>
        /// <param name="externalTable">Una datatable contenente i record da associare al valore del campo indicizzato da databaseFieldName</param>
        /// <param name="displayMember">Il nome sul database del campo dal quale ottenere il valore da stampare</param>
        /// <param name="joinField">Il nome sul database del campo associato al valore del campo indicizzato da databaseFieldName</param>
        public AxtExternalColumn(string databaseFieldName, string caption, DataTable externalTable, string displayMember, string joinField)
            :base(databaseFieldName,caption)
        {
            _ExternalTable = externalTable;
            JoinField = joinField;
            DisplayMember = displayMember;
        }

        /// <summary>
        /// Restituisce il controllo xhtml relativo al record.
        /// </summary>
        /// <param name="row">La DataRow sorgente dati.</param>
        /// <returns>Un HtmlGenericControl contenente i controlli della riga.</returns>
        public override HtmlGenericControl GetCellControl(DataRow row)
        {
            string CellText = row[DatabaseFieldName].ToString();

            if (CellText != null && CellText.Length > 0)  //controllo aggiunto in seguito a eccezione per valori nulli
            {
                DataRow[] rows = ExternalTable.Select(JoinField + " = '" + CellText + "'");
                if (rows.Length > 0)
                    CellText = rows[0][DisplayMember].ToString();
            }
            
            string CellHref = this.Href;
            string CellTitle = this.HyperlinkTitle;

            HtmlGenericControl td = new HtmlGenericControl("td");
            td.Attributes["class"] = this.CssClass;

            if (ColSpan > 0)
                td.Attributes["colspan"] = ColSpan.ToString();

            if (CellHref != null && CellHref.Length > 0)
            {
                if (DatabaseHrefValueFields != null)
                    foreach (string col in DatabaseHrefValueFields)
                    {
                        if (CellHref!=null && CellHref.Length > 0)
                            CellHref = CellHref.Replace("{" + col + "}", row[col].ToString());
                        if (CellTitle!=null && CellTitle.Length > 0)
                            CellTitle = CellTitle.Replace("{" + col + "}", row[col].ToString());
                    }

                td.InnerHtml = "<a href=\"" + CellHref + "\" title=\"" + CellTitle + "\"><span>&nbsp;" + CellText + "</span></a>";
            }
            else
                td.InnerHtml = "<span>&nbsp;" + CellText + "</span>";

            return td;
        }

        public override string GetExportValue(DataRow row)
        {
            string CellText = row[DatabaseFieldName].ToString();

            if (CellText != null && CellText.Length > 0)  //controllo aggiunto in seguito a eccezione per valori nulli
            {
                DataRow[] rows = ExternalTable.Select(JoinField + " = '" + CellText + "'");
                if (rows.Length > 0)
                    CellText = rows[0][DisplayMember].ToString();
            }

            return CellText;
        }
    }
}