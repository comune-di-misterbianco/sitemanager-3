﻿using System;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using G2Core.AdvancedXhtmlForms.Fields;

namespace G2Core.AdvancedXhtmlTable
{
    /// <summary>
    /// Gestisce i controlli di ricerca per la classe AxtTable
    /// </summary>
    public class AxtSearchForm
    {
        private G2Core.AdvancedXhtmlForms.Fields.AxfFieldsCollection Fields;

        /// <summary>
        /// Rappesenta una stringa contenente l'etichetta del bottone di submit della form di ricerca.
        /// </summary>
        public string SubmitButtonLabel
        {
            get
            {
                return _SubmitButtonLabel;
            }
            set
            {
                _SubmitButtonLabel = value;
            }
        }
        private string _SubmitButtonLabel;

        public bool CheckSubmitSource
        {
            get
            {
                G2Core.Common.RequestVariable sb = new G2Core.Common.RequestVariable("SearchFormButton", G2Core.Common.RequestVariable.RequestType.Form);
                if (sb.IsValid() && sb.StringValue == SubmitButtonLabel)
                    return true;
                return false;
            }
        }

        private bool _ViewSubmitButton=true;
        public bool ViewSubmitButton
        {
            get
            {
                return _ViewSubmitButton;
            }
            set
            {
                _ViewSubmitButton = value;
            }
        }

        /// <summary>
        /// Rappresenta una boleano che specifica se la stringa di ricerca deve iniziare con la Keyword "WHERE" o "AND"
        /// true: la stringa inizia con la Keyword "AND"
        /// false: la stringa inizia con la Keyword "WHERE"
        /// </summary>
        public bool Link
        {
            get
            {
                return _Link;
            }
            set
            {
                _Link = value;
            }
        }
        private bool _Link = false;
        
        /// <summary>
        /// Costruttore della classe AxtSearchForm
        /// </summary>
        public AxtSearchForm()
        {
            Fields = new AxfFieldsCollection();
            SubmitButtonLabel = "Cerca";
        }

        /// <summary>
        /// Aggiunge un AxfField all'insieme di AxfFields facenti parte la form di ricerca.
        /// </summary>
        /// <param name="field"></param>
        public void addField(AxfField field)
        {
            Fields.Add(field);
        }

        /// <summary>
        /// Restituisce la stringa di ricerca corrente
        /// </summary>
        /// <returns>La stringa di ricerca corrente</returns>
        public string getFilterString()
        {
            return getFilterString(HttpContext.Current.Request.Form);
        }

        /// <summary>
        /// Restituisce la stringa di ricerca corrente
        /// </summary>
        /// <param name="post">Una  NameValueCollection rappresentante la </param>
        /// <returns>La stringa di ricerca corrente</returns>
        public string getFilterString(NameValueCollection post)
        {
            string where = "";

            for (int i = 0; i < Fields.Count; i++)
            {
                string value = "";
                if (post.Get(Fields[i].FieldName) != null)
                    value = post.Get(Fields[i].FieldName).ToString();
                if (value.Length > 0)
                {
                    if (where.Length > 0) where += " AND ";
                    where += Fields[i].getFilter(value);
                }
            }

            if (where.Length > 0 && !Link)
                where = " WHERE" + where;
            if (where.Length > 0 && Link)
                where = " AND" + where;

            return where;
        }

        public bool serializedValidation()
        {
            return serializedValidation(HttpContext.Current.Request.Form);
        }

        public bool serializedValidation(NameValueCollection values)
        {
            bool valid = true;            

            foreach (AxfField field in this.Fields)
            {                      
                if (values.Get(field.FieldName) != null)                
                {
                    string input = values.Get(field.FieldName).ToString();
                    valid = valid & field.validateInput(input);
                }
            }
            return valid;
        }

        /// <summary>
        /// Restituisce un HtmlGenericControl contenente i controlli xhtml della form di ricerca
        /// </summary>
        /// <returns>HtmlGenericControl contenente i controlli xhtml della form di ricerca</returns>
        public HtmlGenericControl getForm()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");


            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerText = "Ricerca";
            fieldset.Controls.Add(legend);

            div.Controls.Add(fieldset);

            for (int i = 0; i < Fields.Count; i++)
            {
                AxfField field = Fields[i];
                fieldset.Controls.Add(field.Control);
            }

            Button bt = new Button();
            bt.Text = SubmitButtonLabel;
            bt.ID = "SearchFormButton";
            bt.CssClass = "btsearch";
            bt.OnClientClick = "javascript: setSubmitSource('" + bt.ID + "')";
            HtmlGenericControl reset = new HtmlGenericControl("p");
            reset.InnerHtml = "<a href=\"" + HttpContext.Current.Request.Url + "\" class=\"btreset\" >Azzera Ricerca</a>";

            if (ViewSubmitButton)
                fieldset.Controls.Add(bt);
            
            fieldset.Controls.Add(reset);

            return div;
        }
    }
}