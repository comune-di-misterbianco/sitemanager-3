using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using G2Core.Common;

namespace G2Core.AdvancedXhtmlTable
{
    /// <summary>
    /// Questa classe rappresenta una tabella xhtml con funzioni di ricerca e paginazione.
    /// </summary>
    public class AxtTableSmart
    {
        /// <summary>
        /// Rappresenta l'insieme delle Colonne che verrano stampate nella tabella.
        /// </summary>
        public AxtColumnsCollection Columns
        {
            get
            {
                return _Columns;
            }
        }
        protected AxtColumnsCollection _Columns;

        public G2Core.AdvancedPagination.PageMaker.VariabileType PagingType = G2Core.AdvancedPagination.PageMaker.VariabileType.Form;

        /// <summary>
        /// Rappresenta l'insieme delle Colonne Attive che verranno stampate nella tabella.
        /// Le Colonne Attive vengono solitamente utilizzate per i collegamenti a pagine di modifica, eliminazione dei record o operazioni analoghe.
        /// </summary>
        public AxtColumnsCollection ActionColumns
        {
            get
            {
                return _ActionColumns;
            }
        }
        protected AxtColumnsCollection _ActionColumns;

        /// <summary>
        /// Rappresenta la tipologia di Rendering delle colonne. 
        /// Vedere l'enumeratore AxtActionColumnRenderMode, per le possibili opzioni.
        /// </summary>
        public AxtActionColumnRenderMode ActionColumnsRenderMode
        {
            get { return _ActionColumnsRenderMode; }
            set { _ActionColumnsRenderMode = value; }
        }
        private AxtActionColumnRenderMode _ActionColumnsRenderMode;

        /// <summary>
        /// Rappresenta il valore che verr� inpostato come id del controllo html <table /> che verr� stampato in fase di HtmlRender
        /// </summary>
        public string TableID
        {
            get { return _TableID; }
            set { _TableID = value; }
        }
        private string _TableID = string.Empty;

        /// <summary>
        /// Rappresenta il valore che verr� inpostato come class del controllo html <table /> che verr� stampato in fase di HtmlRender
        /// </summary>
        public string CssClass
        {
            get
            {
                if (_CssClass == null)
                    _CssClass = "AxtTable";
                return _CssClass;
            }
            set { _CssClass = value; }
        }
        private string _CssClass;

        /// <summary>
        /// imposta la stringa di filtro da applicare alla DataTable, sorgente dati della tabella
        /// </summary>
        public string  FilterExpression
        {
            get 
            { 
                return _FilterExpression; 
            }
            set 
            {
                _FilterExpression = value; 
            }
        }
        private string _FilterExpression;

        RequestVariable SortRequest
        {
            get
            {
                if (_SortRequest == null)
                {
                    _SortRequest = new RequestVariable(SortFieldID, RequestVariable.RequestType.Form_QueryString);
                }
                return _SortRequest;
            }
            set { _SortRequest = value; }
        }
        RequestVariable _SortRequest;

        public bool PrintExportLinks
        {
            get
            {
                return PrintExportLinksPdf || PrintExportLinksCsv;
            }
        }
        private bool _PrintExportLinks;

        public bool PrintExportLinksPdf
        {
            get
            {
                return _PrintExportLinksPdf;
            }
            set
            {
                _PrintExportLinksPdf = value;
            }
        }
        private bool _PrintExportLinksPdf;

        public bool PrintExportLinksCsv
        {
            get
            {
                return _PrintExportLinksCsv;
            }
            set
            {
                _PrintExportLinksCsv = value;
            }
        }
        private bool _PrintExportLinksCsv;
        //protected string ExportLink
        //{
        //    get
        //    {
        //        return _ExportLink;
        //    }
        //    private set
        //    {
        //        _ExportLink = value;
        //    }
        //}
        //private string _ExportLink; 

        string SortFieldID
        {
            get
            {
                if (_SortFieldID == null)
                {
                    _SortFieldID = "SortField"+this.TableID;
                }
                return _SortFieldID;
            }
            set { _SortFieldID = value; }
        }
        string _SortFieldID;

        /// <summary>
        /// Rappresenta l'ID del campo di html <input /> che gestisce l'eliminazione di un record della tabella.
        /// </summary>
        public string DeleteFieldID
        {
            get
            {
                if (_DeleteFieldID == null)
                {
                    _DeleteFieldID = "del";                   
                }
                return _DeleteFieldID;
            }
            set { _DeleteFieldID = value; _DeleteField = null; }
        }
        private string _DeleteFieldID;

        public DataTable ExportTable
        {
            get
            {
                if (_ExportTable == null)
                {
                    _ExportTable = new DataTable();

                    foreach (AxtColumn col in Columns)
                    {
                        if (col.ExportData)
                        {
                            DataColumn dcol = new DataColumn(col.ID);
                            dcol.DataType = typeof(System.String);
                            dcol.Caption = col.Caption;
                            _ExportTable.Columns.Add(dcol);
                        }
                    }
                }
                return _ExportTable;
            }
        }
        private DataTable _ExportTable;

        /// <summary>
        /// Rappresenta l'ID del campo di html <input /> che gestisce lo stato di un record
        /// </summary>
        public string EnableFieldID
        {
            get
            {
                if (_EnableFieldID == null)
                {
                    _EnableFieldID = "enable";
                }
                return _EnableFieldID;
            }
            set { _EnableFieldID = value; _EnableField = null; }
        }
        private string _EnableFieldID;              

        /// <summary>
        /// Rappresenta il campo di html <input /> che gestisce l'eliminazione di un record della tabella.
        /// </summary>
        public G2Core.XhtmlControls.InputField DeleteField
        {
            get
            {
                if (_DeleteField == null)
                    _DeleteField = new G2Core.XhtmlControls.InputField(DeleteFieldID);
                return _DeleteField;
            }
            set { _DeleteField = value; }
        }
        private G2Core.XhtmlControls.InputField _DeleteField;

        /// <summary>
        /// Rappresenta il campo di html <input /> che gestisce lo stato di un record
        /// </summary>
        public G2Core.XhtmlControls.InputField EnableField
        {
            get
            {
                if (_EnableField == null)
                    _EnableField = new G2Core.XhtmlControls.InputField(EnableFieldID);
                return _EnableField;
            }
            set { _EnableField = value; }
        }
        private G2Core.XhtmlControls.InputField _EnableField;
        
        /// <summary>
        /// Rappesenta una stringa che verr� stampata al di sopra dell'intestazione della tabella, come titolo della stessa.
        /// </summary>
        public string OverTitle
        {
            get { return _OverTitle; }
            set { _OverTitle = value; }
        }
        private string _OverTitle;

        /// <summary>
        /// Rappesenta una stringa che nel caso in cui la tabella non contiene record.
        /// </summary>
        public string NoRecordMsg
        {
            get
            {
                if (_NoRecordMsg == null)
                    _NoRecordMsg = "Nessun record trovato";
                return _NoRecordMsg;
            }
            set { _NoRecordMsg = value; }
        }
        private string _NoRecordMsg;

        private G2Core.AdvancedPagination.DALPageMaker PageMaker;

        protected bool BuildExportTable
        {
            get
            {
                return (this.PrintExportLinks &&
                        this.ExportRequest.RequestVariable.IsValidInteger &&
                        this.ExportRequest.RequestVariable.IntValue == 1);
            }
        }

        public G2Core.XhtmlControls.InputField ExportRequest
        {
            get
            {
                if (_ExportRequest == null)
                {
                    _ExportRequest = new G2Core.XhtmlControls.InputField("esportRequest" + this.TableID);
                }
                return _ExportRequest;
            }
        }
        private G2Core.XhtmlControls.InputField _ExportRequest;

        #region Costruttori

        /// <summary>
        /// Questa classe rappresenta una tabella xhtml con funzioni avanzate di ricerca e paginazione.
        /// </summary>
        public AxtTableSmart(G2Core.AdvancedPagination.DALPageMaker pageMaker)
        {
            _ActionColumnsRenderMode = AxtActionColumnRenderMode.IconsAndText;
            PageMaker = pageMaker;
            _Columns = new AxtColumnsCollection();
            _ActionColumns = new AxtColumnsCollection();

            this.PrintExportLinksPdf = false;
            this.PrintExportLinksCsv = false;
        }

        #endregion

        /// <summary>
        /// Restituisce il controllo web della tabella.
        /// </summary>
        public HtmlGenericControl Control
        {
            get
            {
                if (_Control == null)
                {
                    GetControl();
                }
                return _Control;
            }
        }
        private HtmlGenericControl _Control;

        private HtmlGenericControl BuildRow(DataRow dataTableRow)
        {
            HtmlGenericControl trControl = new HtmlGenericControl("tr");

            foreach (AxtColumn column in this.Columns)
            {
                HtmlGenericControl cell = column.GetCellControl(dataTableRow);

                if (column.SkipRow) return null;

                if (column.SkipCell)
                    trControl.Controls.Add(new LiteralControl("<td>&nbsp</td>"));
                else
                    trControl.Controls.Add(cell);
            }

            if (this.ActionColumnsRenderMode == AxtActionColumnRenderMode.IconsAndText)
            {
                foreach (AxtColumn column in this.ActionColumns)
                    trControl.Controls.Add(((AxtColumn)column).GetCellControl(dataTableRow));
            }

            if (this.ActionColumnsRenderMode == AxtActionColumnRenderMode.IconsOnly)
            {
                HtmlGenericControl tdControl = new HtmlGenericControl("td");
                tdControl.Attributes["class"] = "AxtActionColumnIconsOnlyCell";
                foreach (AxtColumn column in this.ActionColumns)
                    tdControl.Controls.Add(((AxtColumn)column).GetCellControl(dataTableRow));
                trControl.Controls.Add(tdControl);
            }

            return trControl;
        }
        private void BuildExportRow(DataRow dataTableRow)
        {
            DataRow rowToAppend = this.ExportTable.NewRow();

            bool skip = false;
            foreach (AxtColumn axtColumn in this.Columns)
            {
                if (this.ExportTable.Columns.Contains(axtColumn.ID))
                {
                    string value = axtColumn.GetExportValue(dataTableRow);

                    if (axtColumn.SkipRow) { skip = true; }
                    else
                        rowToAppend[axtColumn.ID] = (!axtColumn.SkipCell && axtColumn.ExportData && value != null) ? value : string.Empty;
                }
            }
            if(!skip)
                this.ExportTable.Rows.Add(rowToAppend);
        }
        private HtmlGenericControl BuildHeadRow()
        {
            HtmlGenericControl trControl = new HtmlGenericControl("tr");

            foreach (AxtColumn column in this.Columns)
            {
                HtmlGenericControl tdControl = new HtmlGenericControl("th");
                if (column.ColSpan > 0)
                    tdControl.Attributes["colspan"] = column.ColSpan.ToString();
                
                tdControl.Controls.Add(column.CaptionControl);
                trControl.Controls.Add(tdControl);
            }


            if (this.ActionColumnsRenderMode == AxtActionColumnRenderMode.IconsAndText)
                foreach (AxtColumn column in this.ActionColumns)
                {
                    HtmlGenericControl tdControl = new HtmlGenericControl("th");
                    tdControl.Controls.Add(column.CaptionControl);
                    trControl.Controls.Add(tdControl);
                }

            if (this.ActionColumnsRenderMode == AxtActionColumnRenderMode.IconsOnly)
            {
                HtmlGenericControl tdControl = new HtmlGenericControl("th");
                tdControl.InnerHtml = "Opzioni";
                trControl.Controls.Add(tdControl);
            }

            if (this.ActionColumnsRenderMode == AxtActionColumnRenderMode.IconsOnly)
            {
                HtmlGenericControl tdControl = new HtmlGenericControl("th");
                tdControl.InnerHtml = "Dettagli e Opzioni";
                trControl.Controls.Add(tdControl);
            }

            return trControl;
        }
        private HtmlGenericControl OvertitleRow
        {
            get
            {
                HtmlGenericControl trControl = new HtmlGenericControl("tr");
                HtmlGenericControl th = new HtmlGenericControl("th");
                th.Attributes["colspan"] = (this.Columns.Count + this.ActionColumns.Count).ToString();
                th.Attributes["class"] = "overtitle";
                th.InnerHtml = OverTitle;
                trControl.Controls.Add(th);
                return trControl;
            }
        }


        private HtmlGenericControl GetControl()
        {
            G2Core.XhtmlControls.InputField pageField = new G2Core.XhtmlControls.InputField(this.PageMaker.Key);

            _Control = new HtmlGenericControl("div");
            _Control.Attributes["class"] ="AxtTableDivClass";

            //Katja:spostato questa parte sotto per rendere persistente i valori della pagina in 
            //seguito ad azione di postback sulla tabella come ad esempio l'azione elimina
            /*_Control.Controls.Add(pageField.Control);
            _Control.Controls.Add(DeleteField.Control);
            _Control.Controls.Add(this.EnableField.Control);
             */

            if (this.PageMaker.RecordCount > 0)
            {
                HtmlGenericControl table = new HtmlGenericControl("table");
                if (TableID != null && TableID != string.Empty)
                    table.ID = this.TableID;
                _Control.Controls.Add(table);

                table.Attributes["class"] = CssClass;
                table.Attributes["border"] = "0";
                table.Attributes["cellspacing"] = "0";
                table.Attributes["cellpadding"] = "00";

                if (this.OverTitle != null)
                    table.Controls.Add(OvertitleRow);

                table.Controls.Add(this.BuildHeadRow());

                //katja: ricorda il valore della pagina..
                pageField.Value = PageMaker.PageRequest.StringValue;

                DataRow[] Records = PageMaker.GetPagedRecords();

                bool alternate = false;
                foreach (DataRow record in Records)
                {
                    HtmlGenericControl row = this.BuildRow(record);
                    if (row != null)
                    {
                        if (alternate)
                        {
                            row.Attributes["class"] += " alternate";
                            row.Attributes["class"] = row.Attributes["class"].Trim();
                        }
                        alternate = !alternate;
                        table.Controls.Add(row);
                    }
                }
                if (this.BuildExportTable)
                {
                    foreach (DataRow record in this.PageMaker.GetAllRecords())
                        BuildExportRow(record);
                    try
                    {
                        foreach (AxtColumn axtColumn in this.Columns)
                        {
                            if (this.ExportTable.Columns.Contains(axtColumn.ID))
                            {
                                string caption = axtColumn.Caption;
                                if (this.ExportTable.Columns.Contains(axtColumn.Caption))
                                {
                                    caption += "_";
                                    int i = 1;
                                    while (this.ExportTable.Columns.Contains(caption + i)) i++;
                                    caption = caption + i;
                                }
                                this.ExportTable.Columns[axtColumn.ID].ColumnName = caption;
                            }
                        }
                    }
                    catch { }
                }
                _Control.Controls.Add(this.PageMaker.PageNavigation(pageField.getSetFormValueJScript("{0}")));


                if (PrintExportLinks)
                {
                    var exports = new HtmlGenericControl("div");

                    _Control.Controls.Add(this.ExportRequest.Control);
                    if (BuildExportTable)
                    {
                        HtmlGenericControl ExportLinks = NetUtility.Report.DataTableConvert.DataTableExporterUtility.addExportLinks(ExportTable, null, printCsv: this.PrintExportLinksCsv, printPdf: this.PrintExportLinksPdf);
                        if (ExportLinks != null)
                            _Control.Controls.Add(ExportLinks);
                    }
                    else
                    {
                        exports.Attributes.Add("class", "export-buttons");
                        exports.InnerHtml = "<a href=\"javascript: setFormValue(1,'" + this.ExportRequest.ID + "',1)\"><span>Mostra Opzioni di Esportazione</span></a>";
                        _Control.Controls.Add(exports);
                    }
                }

                
            }
            else
                _Control.Controls.Add(new LiteralControl(NoRecordMsg));
            
            _Control.Controls.Add(pageField.Control);
            _Control.Controls.Add(DeleteField.Control);
            _Control.Controls.Add(this.EnableField.Control);
            //SortedField.Value = SortRequest.StringValue;
            return _Control;
        }
    }
}

