﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PingSession
{
    public class PingSession
    {
        string host;
        string port;
        string ms;

        public PingSession()
        {
            //this.host = System.Web.HttpContext.Current.Request.Url.Host;
            //this.port = System.Web.HttpContext.Current.Request.Url.Port.ToString();
            NameValueCollection pingsection = System.Web.Configuration.WebConfigurationManager.GetSection("pingsession") as NameValueCollection;
            string ms = pingsection.Get("timeoutping");
            bool basedOnSessionTimeout = Convert.ToBoolean(pingsection.Get("basedOnSessionTimeout"));

            if (basedOnSessionTimeout)
            {
                this.ms = ((System.Web.HttpContext.Current.Session.Timeout - 1) * 60000).ToString();
            }
            else
            {
                try
                {
                    if (ms != null)
                        this.ms = ms;
                    else
                    {
                        throw new Exception("Controllare che il web.config sia aggiornato con i parametri relativi a pingsession. Saranno utilizzati valori di default");
                    }
                }
                catch (Exception e)
                {
                    this.ms = ((System.Web.HttpContext.Current.Session.Timeout - 1) * 60000).ToString();
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, e.ToString());
                    //NetCms.Diagnostics.Diagnostics.TraceException(e, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName.ToString());
                }
            }
        }

        //public PingSession(string ms)
        //{
        //    //this.host = System.Web.HttpContext.Current.Request.Url.Host;
        //    //this.port = System.Web.HttpContext.Current.Request.Url.Port.ToString();
        //    this.ms = ms;
        //}

        //public PingSession(string host, string port)
        //{
        //    //this.host = host;
        //    //this.port = port;
        //    NameValueCollection pingsection = System.Web.Configuration.WebConfigurationManager.GetSection("pingsession") as NameValueCollection;
        //    this.ms = pingsection.Get("timeoutping");
        //}

        //public PingSession(string host, string port, string ms)
        //{
        //    //this.host = host;
        //    //this.port = port;
        //    this.ms = ms;
        //}

        public WebControl getWebControl()
        {
            //string path = "http://" + host + ":";
            //if (!string.IsNullOrEmpty(port))
            //    path += port;
            //string url = path + "/api/PingSession/pingsession";
            string url = BaseUri + "/api/PingSession/pingsession";

            string jsStart = @"
                                    <script type=""text/javascript"">  
                                        $(document).ready(function () {                
                                            setInterval('pingSession()', " + ms + @");    
                                        });

                                        function pingSession() {
                                            $.ajax({
                                                url: '" + url + @"',
                                                data: {},
                                                type: ""POST"",
                                                processData: false,
                                                contentType: ""application/json; charset=utf-8"",
                                                timeout: 10000,
                                                dataType: ""text"",
                                                success: function(obj) {
                                                }
                                            });
                                        };	
                                    </script>";
            WebControl toReturn = new WebControl(HtmlTextWriterTag.Div);
            toReturn.Controls.Add(new LiteralControl(jsStart));
            return toReturn;
        }

        public static string BaseUri
        {
            get
            {
                string baseUri = string.Empty;

                if (HttpContext.Current != null && HttpContext.Current.Request != null)
                {
                    Uri uri = System.Web.HttpContext.Current.Request.Url;

                    baseUri = uri.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Host;

                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Url.Port.ToString()))
                        baseUri += ":" + System.Web.HttpContext.Current.Request.Url.Port.ToString();                  
                }

                return baseUri;
            }
        }

        public LiteralControl getLiteralControl()
        {
            //string path = "http://" + host + ":";
            //if (!string.IsNullOrEmpty(port))
            //    path += port;
            //string url = path + "/api/PingSession/pingsession";

            string url = BaseUri + "/api/PingSession/pingsession";

            string jsStart = @"        
                                <script type=""text/javascript"">                              
                                        $(document).ready(function () {                
                                            setInterval('pingSession()', " + ms + @");    
                                        });

                                        function pingSession() {
                                            $.ajax({
                                                url: '" + url + @"',
                                                data: {},
                                                type: ""POST"",
                                                processData: false,
                                                contentType: ""application/json; charset=utf-8"",
                                                timeout: 10000,
                                                dataType: ""text"",
                                                success: function(obj) {
                                                }
                                            });
                                 };
                                </script>";
            LiteralControl toReturn = new LiteralControl(jsStart);
            return toReturn;
        }
    }
}
