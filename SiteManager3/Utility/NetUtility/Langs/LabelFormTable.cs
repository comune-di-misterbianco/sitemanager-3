using System;
using System.Data;
using System.Data.Common;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Connections;
using NetForms;
using NetCms.Diagnostics;

/// <summary>
/// Summary description for NetFormTable
/// </summary>
namespace NetUtility.LanguagesUtility
{
    public class LabelFormTable : NetForms.NetAbstractFormTable
    {
        private string _TableLabel;
        public override string TableLabel
        {
            get { return _TableLabel; }
            set { _TableLabel = value; }
        }

        private string _TableName;
        public override string TableName
        {
            get
            {
                return _TableName;
            }
        }

        protected int _LastInsert;
        public override int LastInsert
        {
            get
            {
                return _LastInsert;
            }
        }

        private int _LabelID;
        public int LabelID
        {
            get { return _LabelID; }
            set { _LabelID = value; }
        }

        private bool _Multiline;
        public bool Multiline
        {
            get { return _Multiline; }
            set { _Multiline = value; }
        }
        
        private int _FieldRows = 1;
        public int FieldRows
        {
            get { return _FieldRows; }
            set { _FieldRows = value; }
        }
        
        private int _FieldColumns = 60;
        public int FieldColumns
        {
            get { return _FieldColumns; }
            set { _FieldColumns = value; }
        }

        private int _LabelRecordID;
        public int LabelRecordID
        {
            get { return _LabelRecordID; }
            set { _LabelRecordID = value; }
        }
	
        private NetTextBoxPlus _Field;
        public NetTextBoxPlus Field
        {
            get
            {
                if (_Field == null)
                {
                    _Field = new NetTextBoxPlus(this.TableLabel,"LabelField" + this.TableName);
                    _Field.Required = true;
                    _Field.MaxLenght = 0;
                    if (this.Multiline)
                    {
                        _Field.Rows = this.FieldRows;
                    }
                    _Field.Size = this.FieldColumns;
                }
                return _Field;
            }
        }

        private int _Lang;
        public int Lang
        {
            get { return _Lang; }
        }
	
        #region Costruttori

        /// <summary>
        /// Costruttore utile per le pagine di modifica,il parametro labelKey deve essere univoco e serve agli oggetti della classe NetForm 
        /// per identificare gli oggetti inseriti nella collezione di NetAbstractFormTable, il parametro labelID rappresenta un id_Label preso dalla tabella labels
        /// </summary>
        /// <param name="labelCaption">Una stringa che rappresenta l'etichetta della form</param>
        /// <param name="labelKey">Una stringa univoca che serve agli oggetti della classe NetForm per identificare gli oggetti inseriti nella collezione di NetAbstractFormTable</param>
        /// <param name="labelID">Rappresenta un id_Label preso dalla tabella labels</param>
        /// <param name="conn">Un oggetto della classe NetCms.Connections.Connection</param>
        public LabelFormTable(string labelCaption, string labelKey, int langID ,int labelID, NetCms.Connections.Connection conn)
            : base(conn)
        {
            _TableName = labelKey;
            LabelID = labelID;
            _TableLabel = labelCaption;
        }

        /// <summary>
        /// Costruttore utile per le pagine di creazione, il parametro labelKey deve essere univoco e serve agli oggetti della classe NetForm 
        /// per identificare gli oggetti inseriti nella collezione di NetAbstractFormTable
        /// </summary>
        /// <param name="labelCaption">Una stringa che rappresenta l'etichetta della form</param>
        /// <param name="labelKey">Una stringa univoca che serve agli oggetti della classe NetForm per identificare gli oggetti inseriti nella collezione di NetAbstractFormTable</param>
        /// <param name="conn">Un oggetto della classe NetCms.Connections.Connection</param>
        public LabelFormTable(string labelCaption, string labelKey, int langID, NetCms.Connections.Connection conn)
            : base(conn)
        {
            _TableName = labelKey;
            LabelID = 0;
            _TableLabel = labelCaption;
            _Lang = langID;
        }

        #endregion

        public override void DataBind(NameValueCollection nameValueCollection)
        {
            if (nameValueCollection[this.Field.FieldName] != null)
                    Field.Value = nameValueCollection[this.Field.FieldName];
        }
        public void DataBind(LangLabel label)
        {
            Field.Value = label.Values[Lang.ToString()];
            this.LabelID = int.Parse(label.LabelsIDs[Lang.ToString()]);      
        }
        public override HtmlGenericControl GetControl(params NetForm[] parentForm)
        {
            if (!IsPostBack)
                Diagnostics.TraceMessage(TraceLevel.Verbose, "Accesso alla tabella " + this.TableName, "", "","");
            return this.Field.getControl();
        }
        public override HtmlGenericControl GetOverviewControl()
        {
            HtmlGenericControls.Fieldset control = new HtmlGenericControls.Fieldset(this.TableLabel);
            control.Class = "OverviewFieldset";
            HtmlGenericControl gtitle = new HtmlGenericControl("p");
            control.Controls.Add(gtitle);
            gtitle.InnerHtml = "<em>" + Field.Label + "</em>";

            HtmlGenericControl list = new HtmlGenericControl("ul");
            control.Controls.Add(list);

            list.Controls.Add(Field.GetOverviewControl());


            return control;
        }
        public override string ValidateInput(params NetForm[] parentForm)
        {
            return validateForm(parentForm);
        }

        public string validateForm(params NetForm[] parentForm)
        {
            return validateForm(HttpContext.Current.Request.Form, parentForm);
        }
        public string validateForm(NameValueCollection values, params NetForm[] parentForm)
        {
            string errors = "";
            string input = "";
            Field.CurrentRequest = values;
            input = "";
            if (values.Get(Field.getFieldName()) != null)
                input = values.Get(Field.getFieldName()).ToString();

            string resultFieldValidation;

            errors += (resultFieldValidation = Field.validateInput(input));

            if (resultFieldValidation != String.Empty)
            {
                if (Field.PostBackValue != "")
                    Field.FieldStringForLog = "L'utente ha inserito nel campo " + Field.Label.Replace(":", "") + " della tabella " + this.TableName + " il valore: " + Field.PostBackValue;
                else Field.FieldStringForLog = "L'utente non ha valorizzato il campo " + Field.Label.Replace(":", "") + " della tabella " + this.TableName;

            }

            if (Field.FieldStringForLog != String.Empty && parentForm.Length != 0)
                this.TableStringsForLog.Add(Field.FieldStringForLog);

            if (parentForm.Length == 0 && Field.FieldStringForLog != String.Empty)
            {
                Diagnostics.TraceMessage(TraceLevel.Warning, Field.FieldStringForLog, "", "","");
            }

            if (errors.Length > 0)
                return errors;
            else
                return null;
        }

        public string insertQuery()
        {
            return insertQuery(HttpContext.Current.Request.Form);
        }
        public string insertQuery(NameValueCollection post)
        {
            string valid = validateForm(post);

            if (valid != null)
                return "<ul>" + valid + "</ul>";

            string value = Field.validateValue(Field.RequestVariable.StringValue);
            lock (Conn)
            {
                if (LabelRecordID > 0)
                    _LastInsert = LabelRecordID;
                else
                {
                    string sqlrecord = "INSERT INTO labelsrecords (DefaultLang_LabelRecord) VALUES(" + this.Lang + ");";

                    _LastInsert = Conn.ExecuteInsert(sqlrecord);
                }
                if (value.Length > 0)
                {
                    string sql = "";
                    sql += "INSERT INTO labels";
                    sql += " (Text_Label,Record_Label,Lang_Label)";
                    sql += " VALUES(" + value + "," + LastInsert + "," + this.Lang + ");";

                    Conn.Execute(sql);
                }
            }
            return string.Empty;
        }
        public virtual string updateQuery(NameValueCollection post)
        {
            if (LabelID == 0)
                return "ID del record non impostato";

            string valid = validateForm(post);

            if (valid != null)
                return "<ul>" + valid + "</ul>";

            string value = "";

            if (post.Get(Field.FieldName) != null)
                value = post.Get(Field.FieldName).ToString();

            value = Field.validateValue(value);

            string sql = "";

            sql += "UPDATE labels SET ";
            sql += " Text_Label = " + value + "";
            sql += " WHERE id_Label = " + LabelID;

            if (Conn != null)
                Conn.Execute(sql);

            return sql;
        }

        public override bool Insert()
        {
            insertQuery(HttpContext.Current.Request.Form);
            return true;
        }
        public override bool Insert(NameValueCollection PostData)
        {
            insertQuery(PostData);
            return true;
        }
        public override bool Update(int RecordID)
        {
            this.LabelID = RecordID;
            updateQuery(HttpContext.Current.Request.Form);
            return true;
        }
        public override bool Update()
        {
            updateQuery(HttpContext.Current.Request.Form);
            return true;
        }
        public override bool Update(NameValueCollection PostData)
        {
            updateQuery(PostData);
            return true;
        }
        public override bool Update(int RecordID, NameValueCollection PostData)
        {
            this.LabelID = RecordID;
            updateQuery(PostData);
            return true;
        }
    }
}