using System;
using System.Collections;

/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>

namespace NetUtility.LanguagesUtility
{
    public class LanguagesCollection : IEnumerable
    {
        private NetCollection coll;
        public int Count { get { return coll.Count; } }
        public LanguagesCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new NetCollection();
        }

        public void Add(Language Language)
        {
            coll.Add(Language.ID.ToString(), Language);
        }

        public void Remove(Language Language)
        {
            coll.Remove(Language.CultureCode);
        }

        public void Clear()
        {
            coll.Clear();
        }

        public Language this[int i]
        {
            get
            {
                Language str = (Language)coll[coll.Keys[i]];
                return str;
            }
        }

        public Language this[string langID]
        {
            get
            {
                Language val = (Language)coll[langID];
                return val;
            }
        }

        public bool Contains(Language Language)
        {
            return coll[Language.ID.ToString()] != null;
        }

        public bool Contains(int id)
        {
            return coll[id.ToString()] != null;
        }
        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private LanguagesCollection Collection;
            public CollectionEnumerator(LanguagesCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}