using System;
using System.Xml;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using NetCms.Connections;
using NetForms;
using NetTable;

namespace NetUtility.LanguagesUtility
{   
    public class LangLabel 
    {
        private string _ValueByDefaultLang;
        public string ValueByDefaultLang
        {
            get 
            {
                if (_ValueByDefaultLang == null && Values.Contains(DefaultLangID))
                {
                    _ValueByDefaultLang = Values[DefaultLangID].ToString();
                }
                return _ValueByDefaultLang; 
            }

        }

        private string _IDByDefaultLang;
        public string IDByDefaultLang
        {
            get
            {
                if (_IDByDefaultLang == null && LabelsIDs.Contains(DefaultLangID))
                    _IDByDefaultLang = LabelsIDs[DefaultLangID].ToString();
                
                return _IDByDefaultLang;
            }

        }

        private StringCollection _Values;
        public StringCollection Values
        {
            get
            {
                if (_Values == null)
                {
                    _Values = new StringCollection();
                    foreach (DataRow label in DataSource.Labels.Select("Record_Label = " + this.ID))
                    {
                        _Values.Add(label["Text_Label"].ToString(), label["Lang_Label"].ToString());
                    }
                }

                return _Values;
            }
        }

        private string _DefaultLangID;
        public string DefaultLangID
        {
            get
            {
                if (_DefaultLangID == null)
                    this._DefaultLangID = DataSource.LabelsRecords.Select("id_LabelRecord = " + this.ID)[0]["DefaultLang_LabelRecord"].ToString();

                return _DefaultLangID;
            }
        }


        private StringCollection _LabelsIDs;
        public StringCollection LabelsIDs
        {
            get
            {
                if (_LabelsIDs == null)
                {
                    _LabelsIDs = new StringCollection();
                    foreach (DataRow label in DataSource.Labels.Select("Record_Label = " + this.ID))
                    {
                        _LabelsIDs.Add(label["id_Label"].ToString(), label["Lang_Label"].ToString());
                    }
                }

                return _LabelsIDs;
            }
        }

        private bool ValueByCurrentLangLoaded;
        private string _ValueByCurrentLang;
        public string ValueByCurrentLang
        {
            get
            {
                if (!ValueByCurrentLangLoaded)
                {
                    ValueByCurrentLangLoaded = true;
                    if (Values.Contains(DataSource.CurrentLang.ToString()))
                        _ValueByCurrentLang = Values[DataSource.CurrentLang.ToString()].ToString();
                    else
                        if (LabelPickMode == LabelPickModes.LoadDefault)
                            _ValueByCurrentLang = this.ValueByDefaultLang;
                }
                return _ValueByCurrentLang;
            } 
        }

        /// <summary>
        /// Ritorna il valore rispetto alla lingua corrente, se questo non esistesse ritorna il valore rispetto alla lingua di default
        /// </summary>
        public string Value
        {
            get
            {
                return (ValueByCurrentLang != null) ? ValueByCurrentLang : ValueByDefaultLang;
            }
        }

        private bool IDByCurrentLangLoaded;
        private string _IDByCurrentLang;
        public string IDByCurrentLang
        {
            get
            {
                if (!IDByCurrentLangLoaded)
                {
                    IDByCurrentLangLoaded = true;
                    if (Values.Contains(DataSource.CurrentLang.ToString()))
                        _IDByCurrentLang = LabelsIDs[DataSource.CurrentLang.ToString()].ToString();
                    else
                        if (LabelPickMode == LabelPickModes.LoadDefault)
                            _IDByCurrentLang = this.IDByDefaultLang;
                }
                return _ValueByCurrentLang;
            }
        }

        private int _ID;
        public int ID
        {
            get { return _ID; }
        }

        private LangDataSource _DataSource;
        private LangDataSource DataSource
        {
            get
            {
                return _DataSource;
            }
        }
        /// <summary>
        /// Questo enumeratore permette discegliere il metodo con cui viene scelta la label nel caso in cui 
        /// non venga trovato nessun record con la combinazione di lingua corrente e ID selezionata.
        /// Normal: Ritorna null 
        /// LoadDefault: Ritorna la label associata alla lingua impostata come Default
        /// Inherits: Cerca la label della lingua padre e se non la trova carica ritorna null
        /// Full: Cerca la label della lingua padre e se non la trova carica ritorna la label della lingua Default
        /// </summary>
        public enum LabelPickModes
        {
            OnlyCurrent, LoadDefault
        }
        /// <summary>
        /// Questa proprietÓ permette discegliere il metodo con cui viene scelta la label nel caso in cui 
        /// non venga trovato nessun record con la combinazione di lingua corrente e ID selezionata.
        /// Normal: Ritorna null 
        /// LoadDefault: Ritorna la label associata alla lingua impostata come Default
        /// Inherits: Cerca la label della lingua padre e se non la trova carica ritorna null
        /// Full: Cerca la label della lingua padre e se non la trova carica ritorna la label della lingua Default
        /// </summary>
        private LabelPickModes _LabelPickMode;
        public LabelPickModes LabelPickMode
        {
            get { return _LabelPickMode; }
        }

        public LangLabel(int id, LangDataSource dataSource)
            : this(id, dataSource, LabelPickModes.OnlyCurrent)
        {
        }

        public LangLabel(int id,LangDataSource dataSource,LabelPickModes labelPickMode)
        {
            _ID = id;
            _DataSource = dataSource;
            _LabelPickMode = labelPickMode;
        }
        public LangLabel(int id, LangDataSource dataSource,string defaultLangID)
            : this(id, dataSource, LabelPickModes.OnlyCurrent)
        {
            _DefaultLangID = defaultLangID;
        }

        public LangLabel(int id, LangDataSource dataSource, string defaultLangID, LabelPickModes labelPickMode)
            : this(id, dataSource, LabelPickModes.OnlyCurrent)
        {
            _DefaultLangID = defaultLangID;
        }
        public override string ToString()
        {
            return this.ValueByCurrentLang;
        }
    }    
}
