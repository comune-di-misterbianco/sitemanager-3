using System;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;

/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>


namespace ListTreeView
{
    public class ListNode
    {
        private string _ID;
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _Icon;
        public string Icon
        {
            get { return _Icon; }
            set { _Icon = value; }
        }

        public bool HasChildNodes
        {
            get
            {
                return ChildNodes.Count>0;
            }
        }

        private bool _HasNextNode;
        public bool HasNextNode
        {
            get
            {
                //if(ParentNode==null)
                    return _HasNextNode;
                //else
                  //  return _HasNextNode || !ParentNode.HasChildNodes;

            }
            set
            {
                _HasNextNode = value;
            }
        }
        
        private bool _Expand;
        public bool Expand
        {
            get
            {
                if(_Expand) 
                    return _Expand;
                else
                {
                    if (this.HasChildNodes)
                        for (int i = 0; i < this.ChildNodes.Count;i++ )
                            if(ChildNodes[i].Expand) 
                                   return ChildNodes[i].Expand;      
                }
                return false;
            }
            set
            {
                _Expand = value;
            }
        }

        private bool _Highlight;
        public bool Highlight
        {
            get
            {
                return _Highlight;
            }
            set
            {
                _Highlight = value;
            }
        }

        private bool _AddToTree = true;
        public bool AddToTree
        {
            get
            {
                if (_AddToTree)
                    return _AddToTree;
                else
                {
                    if (this.HasChildNodes)
                        for (int i = 0; i < this.ChildNodes.Count; i++)
                            if (ChildNodes[i].AddToTree)
                                return ChildNodes[i].AddToTree;
                }
                return false;
            }
            set
            {
                _AddToTree = value;
            }
        }

        private string _Href;
        public string Href
        {
            get { return _Href; }
            set { _Href = value; }
        }

        private string _Value;
        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        private string _Label;
        public string Label
        {
            get { return _Label; }
            set { _Label = value; }
        }
	
        private bool _Checked;
        public bool Checked
        {
            get { return _Checked; }
            set { _Checked = value; }
        }

        private ListNode _ParentNode;
        public ListNode ParentNode
        {
            get { return _ParentNode; }
        }

        public int Depth
        {
            get { return ParentNode!=null ? ParentNode.Depth + 1 : 0; }
        }

        public void SetParentNode(ListNode parent)
        {
            if (_ParentNode == null)
                _ParentNode = parent;
            else
            {
                Exception ex = new Exception("Parent Node Alredy Set");
                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error,ex.Message,"","","88");
            }
        }
        
        private ListNodeCollection _ChildNodes;
        public ListNodeCollection ChildNodes
        {
            get { return _ChildNodes; }
        }

        public ListNode()
        {
            _ChildNodes = new ListNodeCollection(this);
        }

        public HtmlGenericControl getControl()
        {
            HtmlGenericControl li = new HtmlGenericControl("li");
            li.Attributes["class"] = "ListTreeNode_Li";
            li.ID = ID;

            HtmlGenericControl icon = new HtmlGenericControl("span");
            icon.ID = this.ID + "_Icon";

            if (this.ParentNode != null)
            {
                icon.Attributes["class"] = "ListTreeNode_Icon";
                if (this.Expand && this.HasChildNodes)
                    icon.Attributes["class"] += " ListTreeNode_IconOpen";
                else
                    icon.Attributes["class"] += " ListTreeNode_IconClose";
            }
            else
            {
                icon.Attributes["class"] = "ListTreeNode_Icon ListTreeNode_IconRoot";
            }

            if (this.Icon != null && Icon.Length > 0)
                icon.Attributes.CssStyle.Add("background-image", "url(" + Icon + ");");

            HtmlGenericControl content = new HtmlGenericControl("span");
            content.Attributes["class"] = "ListTreeNode_Content";

            HtmlGenericControl tree = new HtmlGenericControl("span");
            tree.Attributes["class"] = "ListTreeNode_Tree ListTreeNode_Tree";
            if (this.HasChildNodes)
            {
                if (!this.Expand)
                    tree.Attributes["class"] += "Plus";
                else
                    tree.Attributes["class"] += "Minus";
                
                tree.ID = this.ID + "_Opener";
                tree.TagName = "a";
                tree.Attributes["href"] = "javascript: OpenCloseNode('"+this.ID+"');";
            }
            if (!this.HasNextNode)
                tree.Attributes["class"] += "Bottom";


            HtmlGenericControl container = new HtmlGenericControl("span");
            container.Attributes["class"] = "ListTreeNode_Container";
            if(Highlight)
                container.Attributes["class"] = "ListTreeNode_Container ListTreeNode_Highlight";

            if (ParentNode != null)
            {
                if (Depth > 1)
                {
                    for (int i = 0; i < Depth - 1; i++)
                    {
                        HtmlGenericControl newline = getNewLine(i);

                        li.Controls.Add(newline);
                    }


                    container.Controls.Add(tree);
                    container.Controls.Add(icon);
                    container.Controls.Add(content);
                }
                else
                {
                    container.Controls.Add(tree);
                    container.Controls.Add(icon);
                    container.Controls.Add(content);
                }
            }
            else
            {

                container.Controls.Add(icon);
                container.Controls.Add(content);
            }
            li.Controls.Add(container);

            content.InnerHtml = Label.Length > 70 ? Label.Remove(70)+"..." : Label;
            if (Href != null && Href.Length > 0)
            {
                content.TagName = "a";
                content.Attributes["class"] += " ListTreeNode_ContentLink";
                content.Attributes["href"] = Href;
            }
            
            

            return li;
        }

        private HtmlGenericControl getNewLine(int Depth)
        {
            HtmlGenericControl line = new HtmlGenericControl("span");
            
            ListNode Parent = this.ParentNode;

            bool voidline = true;

            if(Parent!=null)
            {
                while(Parent!=null && Parent.Depth > Depth+1)
                    Parent = Parent.ParentNode;

                if (Parent != null && !Parent.HasNextNode)
                {
                    line.Attributes["class"] = "ListTreeNode_Void";
                    voidline = false;
                }
            
            }
            if(voidline)
                line.Attributes["class"] = "ListTreeNode_Line";
            return line;
        }
    }
}