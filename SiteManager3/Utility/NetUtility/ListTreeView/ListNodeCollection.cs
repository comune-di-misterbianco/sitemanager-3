using System;
using System.Data;
using System.Collections;
using System.Configuration;

/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>

namespace ListTreeView
{
    public class ListNodeCollection : IEnumerable
    {
        private NetCollection coll;
        public int Count { get { return coll.Count; } }
        public ListNodeCollection(ListNode owner)
        {
            //
            // TODO: Add constructor logic here
            //
            Owner = owner;
            coll = new NetCollection();
        }

        private ListNode Owner;

        public void Add(ListNode node)
        {
            if(Owner.ChildNodes.Count>0)
                Owner.ChildNodes[Owner.ChildNodes.Count - 1].HasNextNode = true;
            coll.Add(node.ID, node);
            node.SetParentNode(Owner);
        }

        public void Remove(ListNode node)
        {
            coll.Remove(node.ID);
        }
        public void Remove(string nodeid)
        {
            coll.Remove(nodeid);
        }
        public void Remove(int index)
        {
            coll.Remove(index);
        }

        public void Clear( )
        {
            coll.Clear();
        }

        public ListNode this[int i]
        {
            get
            {
                ListNode str = (ListNode)coll[coll.Keys[i]];
                return str;
            }
        }
        public ListNode this[string nodeid]
        {
            get
            {
                ListNode val = (ListNode)coll[nodeid];
                return val;
            }
        }


        public bool Contains(string id)
        {
            return coll[id]!=null;
        }
        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private ListNodeCollection Collection;
            public CollectionEnumerator(ListNodeCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}