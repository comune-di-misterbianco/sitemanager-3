using System;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;

/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>


namespace ListTreeView
{
    public class ListTreeView
    {
        private ListNode _Root;
        public ListNode Root
        {
            get { return _Root; }
            set { _Root = value; }
        }
        
        private string _ID;
        public string ID
        {
            get { return _ID; }
        }
	

        public ListTreeView(string id)
        {
            _ID = id;
        }
        public ListTreeView(string id,ListNode root)
        {
            _ID = id;
            _Root = root;
        }

        public HtmlGenericControl getTree()
        {
            HtmlGenericControl ctr = new HtmlGenericControl("div");
            ctr.Attributes["class"] = "ListTree";

            HtmlGenericControl ul = new HtmlGenericControl("ul");
            ul.Controls.Add(getSubTree(Root));

            HtmlGenericControl clear = new HtmlGenericControl("div");
            clear.Attributes["class"] = "clearleft";

            ctr.Controls.Add(ul);
            ctr.Controls.Add(Script);
            ctr.Controls.Add(clear);


            return ctr;
        }

        private HtmlGenericControl getSubTree(ListNode node)
        {
            HtmlGenericControl ctr = node.getControl();

            if (node.ChildNodes != null && node.ChildNodes.Count > 0)
            {
                HtmlGenericControl ul = new HtmlGenericControl("ul");
                ul.ID = node.ID + "_ChildNodes";
                if (!node.Expand)
                    ul.Attributes["style"] = "display:none;";

                for (int i = 0; i < node.ChildNodes.Count; i++)
                    if(node.ChildNodes[i].AddToTree)
                        ul.Controls.Add(getSubTree(node.ChildNodes[i]));

                ctr.Controls.Add(ul);
            }

            return ctr;
        }

        private HtmlGenericControl Script
        {
            get
            {
                HtmlGenericControl script = new HtmlGenericControl("script");
                script.Attributes["type"] = "text/javascript"; script.InnerHtml = @"
function OpenCloseNode(id)
{
    oNode = document.getElementById(id);
    oNodeChildList = document.getElementById(id+'_ChildNodes');
    if(oNodeChildList)
    {
        if(oNodeChildList.style.display == ""none"")
            oNodeChildList.style.display = ""block"";
        else
            oNodeChildList.style.display = ""none"";
            
    }
    oNodeOpener = document.getElementById(id+'_Opener');

    if(oNodeOpener)
    {
        if(oNodeOpener.className.match(""Minus""))
            oNodeOpener.className = oNodeOpener.className.replace(""Minus"",""Plus"");
        else
            oNodeOpener.className = oNodeOpener.className.replace(""Plus"",""Minus"");
            
    }
    oNodeIcon = document.getElementById(id+'_Icon');

    if(oNodeIcon)
    {
        if(oNodeIcon.className.match(""Open""))
            oNodeIcon.className = oNodeIcon.className.replace(""Open"",""Close"");
        else
            oNodeIcon.className = oNodeIcon.className.replace(""Close"",""Open"");
            
    }
}";
                return script;
            }
        }
        

    }
}