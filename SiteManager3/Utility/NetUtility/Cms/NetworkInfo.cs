using System;
using System.Data;
using System.Web;
using System.Xml;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetUtility
{
    public class NetworkInfo
    {
        private int _ID;
        public int ID
        {
            get
            {
                if(_ID==0)
                    if(HttpContext.Current.Items["network"]!=null)
                        int.TryParse(HttpContext.Current.Items["network"].ToString(),out _ID);
                return _ID;
            }
        }

        public bool Valid
        {
            get
            {
                return HttpContext.Current.Items.Contains("network") && ID.ToString() == HttpContext.Current.Items["network"].ToString();
            }
        }
        
        private string _Name;
        public string Name
        {
            get
            {
                if (!DataLoaded)
                    InitDBData();
                return _Name;
            }
        }

        private string _CssFolder;
        public string CssFolder
        {
            get
            {
                XmlNode XmlConfigs = this.XmlData;
                if (_CssFolder == null && XmlConfigs != null)
                {
                    XmlNodeList oNodeList = XmlConfigs.SelectNodes("/Portal/configs/Networks/" + this.Name + "/frontend/css");
                    if (oNodeList != null && oNodeList.Count > 0)
                        _CssFolder = oNodeList[0].Attributes["folder"].Value;
                }

                return _CssFolder;
            }
        }

        public string Path
        {
            get
            {
                return RootPath;
            }
        }

        private string _RootPath;
        public string RootPath
        {
            get
            {
                if (!DataLoaded)
                    InitDBData();
                return _RootPath;
            }
        }
        
        private bool _Default;
        public bool Default
        {
            get
            {
                if (!DataLoaded)
                    InitDBData();
                return _Default;
            }
        }

        private bool DataLoaded;
        
        private bool _HasFrontend;
        public bool HasFrontend
        {
            get
            {
                if (!DataLoaded)
                    InitDBData();
                return _HasFrontend;

            }
        }

        private DataTable NetworksTable;

        public NetworkInfo(DataTable networksDataTable)
        {
            NetworksTable = networksDataTable;
        }
        public NetworkInfo(DataTable networksDataTable, int id)
        {
            NetworksTable = networksDataTable;
            _ID = id;
        }
        public NetworkInfo(DataRow networkData)
        {
            NetworkRecord = networkData;
        }

        private NetworksVersions _NetworksVersion = NetworksVersions.Undefined;
        public NetworksVersions NetworksVersion
        {
            get
            {
                if (_NetworksVersion == NetworksVersions.Undefined)
                {
                    if (System.Web.Configuration.WebConfigurationManager.AppSettings["NetworkVersion"] != null)
                    {
                        switch (System.Web.Configuration.WebConfigurationManager.AppSettings["NetworkVersion"])
                        {
                            case "1": _NetworksVersion = NetworksVersions.Classic; break;
                            case "2": _NetworksVersion = NetworksVersions.DuplicatedTables; break;
                        }
                    }
                    else _NetworksVersion = NetworksVersions.Classic;

                }
                return _NetworksVersion;
            }

        }

        public enum NetworksVersions { Undefined, Classic, DuplicatedTables }

        DataRow NetworkRecord = null;
        public void InitDBData()
        {
            if (NetworkRecord == null)
            {
                DataRow[] rows = NetworksTable.Select("id_Network = " + ID + "");
                if (rows.Length > 0)
                {
                    NetworkRecord = rows[0];
                }
                else
                {
                    rows = NetworksTable.Select("Default_Network = 1");
                    if (rows.Length > 0)
                        NetworkRecord = rows[0];
                }
            }
            if (NetworkRecord != null)
            {
                _ID = int.Parse(NetworkRecord["id_Network"].ToString());
                _Name = NetworkRecord["Nome_Network"].ToString();
                _HasFrontend = NetworkRecord["Frontend_Network"].ToString() == "1";
                _Default = NetworkRecord["Default_Network"].ToString() == "1";
                _RootPath = NetworkRecord["Webroot_Network"].ToString();
                DataLoaded = true;
            }
            
            
        }
        
        private XmlNode xmlData;
        public XmlNode XmlData
        {
            get
            {
                if (xmlData == null)
                {
                    XmlDocument oXmlDoc = new XmlDocument();
                    try
                    {
                        string xmlFilePath = HttpContext.Current.Server.MapPath(HttpContext.Current.Application["Root"] + "/configs/config.xml");
                        oXmlDoc.Load(xmlFilePath);
                    }
                    catch (Exception ex)
                    {
                        /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                        ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, "Errore nel caricamento del file xml di configurazione", "", "", "85");
                    }
                    xmlData = oXmlDoc.DocumentElement;
                }
                return xmlData;
            }
        }

        private bool GrantTypeLoaded;

        private NetworkGrantType _GrantType;
        public NetworkGrantType GrantType
        {
            get
            {
                if (!GrantTypeLoaded)
                {
                    string value="";
                    XmlNodeList oNodeList = XmlData.SelectNodes("/Portal/configs/Networks/" + this.Name + "");
                    if (oNodeList != null && oNodeList.Count > 0)
                    {
                        value = oNodeList[0].Attributes["grant"].InnerXml;

                        if (value == "restricted_areas")
                            _GrantType = NetworkGrantType.RestrictedAreas;
                        else
                            if (value == "hierarchy")
                                _GrantType = NetworkGrantType.BackofficeHierarchy;
                            else
                                _GrantType = NetworkGrantType.None;
                    }                            
                    else _GrantType = NetworkGrantType.RestrictedAreas;
                    GrantTypeLoaded = true;
                }
                return _GrantType;
            }
        }

        public enum NetworkGrantType
        {
            None,
            BackofficeHierarchy,
            RestrictedAreas
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}