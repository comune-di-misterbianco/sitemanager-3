//using System.Xml;


///// <summary>
///// Summary description for Configs
///// </summary>
//namespace NetUtility
//{
//    public class PortalData
//    {

//        private int _ID;
//        public int ID
//        {
//            get
//            {
//                if (_ID == 0 && Data != null)
//                {
//                    XmlNodeList oNodeList = Data.SelectNodes("/Portal");
//                    if (oNodeList != null && oNodeList.Count > 0)
//                        int.TryParse(oNodeList[0].Attributes["id"].Value, out _ID);
//                }
//                return _ID;
//            }
//        }

//        private string _Name;
//        public string Name
//        {
//            get 
//            {
//                if (_Name == null && Data != null)
//                {
//                    XmlNodeList oNodeList = Data.SelectNodes("/Portal/configs/Networks/"+Network.Name+"/frontend");
//                    if (oNodeList != null && oNodeList.Count > 0)
//                        _Name = oNodeList[0].Attributes["portalname"].Value;
//                }

//                return _Name;
//            }
//        }

//        private string _smtphost;
//        public string SmtpHost
//        {
//            get
//            {
//                if (_smtphost == null && Data != null)
//                {
//                    XmlNodeList oNodeList = Data.SelectNodes("/Portal/configs/servermail/mail");
//                    if (oNodeList != null && oNodeList.Count > 0)
//                        _smtphost = oNodeList[0].Attributes["SetSmtpHost"].Value;
//                }

//                return _smtphost;
//            }
//        }

//        private string _smtpport;
//        public string SmtpPort
//        {
//            get
//            {
//                if (_smtpport == null && Data != null)
//                {
//                    XmlNodeList oNodeList = Data.SelectNodes("/Portal/configs/servermail/mail");
//                    if (oNodeList != null && oNodeList.Count > 0)
//                        _smtpport = oNodeList[0].Attributes["SetSmtpPort"].Value;
//                }

//                return _smtpport;
//            }
//        }

//        private string _smtpUser;
//        public string SmtpUser
//        {
//            get
//            {
//                if (_smtpUser == null && Data != null)
//                {
//                    XmlNodeList oNodeList = Data.SelectNodes("/Portal/configs/servermail/mail");
//                    if (oNodeList != null && oNodeList.Count > 0)
//                        _smtpUser = oNodeList[0].Attributes["user"].Value;
//                }

//                return _smtpUser;
//            }
//        }
//        private string _smtpPassword;
//        public string SmtpPassword
//        {
//            get
//            {
//                if (_smtpPassword == null && Data != null)
//                {
//                    XmlNodeList oNodeList = Data.SelectNodes("/Portal/configs/servermail/mail");
//                    if (oNodeList != null && oNodeList.Count > 0)
//                        _smtpPassword = oNodeList[0].Attributes["password"].Value;
//                }

//                return _smtpPassword;
//            }
//        }

//        private bool _enableSSL;
//        public bool EnableSSL
//        {
//            get
//            {
//                if (_enableSSL == null && Data != null)
//                {
//                    XmlNodeList oNodeList = Data.SelectNodes("/Portal/configs/servermail/mail");
//                    if (oNodeList != null && oNodeList.Count > 0)
//                        _enableSSL = bool.Parse(oNodeList[0].Attributes["EnableSsl"].Value);
//                }

//                return _enableSSL;
//            }
//        }

//        private string _eMail;
//        public string eMail
//        {
//            get
//            {
//                if (_eMail == null && Data != null)
//                {
//                    XmlNodeList oNodeList = Data.SelectNodes("/Portal/configs/Networks/"+Network.Name+"/frontend");
//                    if (oNodeList != null && oNodeList.Count > 0)
//                        _eMail = oNodeList[0].Attributes["email"].Value;
//                }

//                return _eMail;
//            }
//        }
        
//        private string _Website;
//        public string Website
//        {
//            get
//            {
//                if (_Website == null && Data != null)
//                {
//                    XmlNodeList oNodeList = Data.SelectNodes("/Portal/configs/Networks/" + Network.Name + "/frontend");
//                    if (oNodeList != null && oNodeList.Count > 0 && oNodeList[0].Attributes["website"]!=null)
//                    {
//                        _Website = oNodeList[0].Attributes["website"].Value;
//                    }
//                }

//                return _Website;
//            }
//        }

//        private string _DisclaimerBandi;
//        public string DisclaimerBandi
//        {
//            get
//            {
//                if (_DisclaimerBandi == null && Data != null)
//                {
//                    XmlNodeList oNodeList = Data.SelectNodes("/Portal/configs/Networks/" + Network.Name + "/frontend/externalapplications/DisclaimerBandi");
//                    if (oNodeList != null && oNodeList.Count > 0)
//                        _DisclaimerBandi = oNodeList[0].InnerText;
//                }

//                return _DisclaimerBandi;
//            }
//        }      

//        private string _DisclaimerRegistrazione;
//        public string DisclaimerRegistrazione
//        {
//            get
//            {
//                if (_DisclaimerBandi == null && Data != null)
//                {
//                    XmlNodeList oNodeList = Data.SelectNodes("/Portal/configs/Networks/" + Network.Name + "/frontend/externalapplications/DisclaimerRegistrazione");
//                    if (oNodeList != null && oNodeList.Count > 0)
//                        _DisclaimerRegistrazione = oNodeList[0].InnerText;
//                }

//                return _DisclaimerRegistrazione;
//            }
//        }

//        private string _NewsLetterEnabled;
//        public bool NewsLetterEnabled
//        {
//            get
//            {
//                if (_NewsLetterEnabled == null && Data != null)
//                {
//                    XmlNodeList oNodeList = Data.SelectNodes("/Portal/configs/externalapplications/newsletter");
                            
//                    if (oNodeList != null && oNodeList.Count > 0)
//                        _NewsLetterEnabled = oNodeList[0].Attributes["status"].Value;
//                }

//                return _NewsLetterEnabled == "on";
//            }
//        }

//        private string _PathLogin;
//        public string PathLogin
//        {
//            get
//            {
//                if (_PathLogin == null && Data != null)
//                {
//                    XmlNodeList oNodeList = Data.SelectNodes("/Portal/configs/externalapplications/login");

//                    if (oNodeList != null && oNodeList.Count > 0)
//                        _PathLogin = oNodeList[0].Attributes["path"].Value;                    
//                }

//                return _PathLogin;
//            }
//        }

//        private string _PathICI;
//        public string PathICI
//        {
//            get
//            {
//                if (_PathICI == null && Data != null)
//                {
//                    XmlNodeList oNodeList = Data.SelectNodes("/Portal/configs/externalapplications/ici");

//                    if (oNodeList != null && oNodeList.Count > 0)
//                        _PathICI = oNodeList[0].Attributes["path"].Value;
//                }

//                return _PathICI;
//            }
//        }

//        private string _ICIEnabled;
//        public bool ICIEnabled
//        {
//            get
//            {
//                if (_ICIEnabled == null && Data != null)
//                {
//                    XmlNodeList oNodeList = Data.SelectNodes("/Portal/configs/externalapplications/ici");

//                    if (oNodeList != null && oNodeList.Count > 0)
//                        _ICIEnabled = oNodeList[0].Attributes["status"].Value;
//                }

//                return _ICIEnabled == "on";
//            }
//        }

//        private XmlNode Data;

//        NetUtility.NetworkInfo Network;

//        public PortalData(XmlNode data,NetUtility.NetworkInfo network)
//        {
//            Data = data;
//            Network = network;
//        }


//    }
//}