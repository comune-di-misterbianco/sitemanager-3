using System;
using System.IO;
using System.Web;
using System.Xml;
using System.Reflection;
using NetForms;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetUtility
{
    public static class LabelsCacher
    {
        private static NetUtility.LabelsCollection labels;
        public static NetUtility.LabelsCollection Labels
        {
            get
            {
                if (labels == null)
                    labels = new NetUtility.LabelsCollection();
                
                return labels;
            }
        }

        private static XmlNode RootNodeObj;
        public static XmlNode RootNode
        {
            get
            {
                if (RootNodeObj == null)
                {
                    Assembly dll = Assembly.GetExecutingAssembly();
                    XmlDocument xml = new XmlDocument();
                    xml.Load(new StreamReader(dll.GetManifestResourceStream("NetUtility.Labels.labels_it.xml")));
                    RootNodeObj = xml;
                }
                return RootNodeObj;
            }
        }

        public static NetFormLabels NetFormLabels
        {
            get
            {
                if (!Labels.Contains(NetFormLabels.ApplicationKey))
                    Labels.Add(new NetFormLabels(RootNode), NetFormLabels.ApplicationKey);

                return (NetFormLabels)Labels[NetFormLabels.ApplicationKey];
            }
        }
    }

}