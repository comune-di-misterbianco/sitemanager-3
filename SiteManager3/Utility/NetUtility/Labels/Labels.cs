using System;
using System.Web;
using System.Xml;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetUtility
{
    public class Labels
    {
        protected const bool LabelsDebugMode = true;

        
        private StringCollection _LabelsCache;
        public StringCollection LabelsCache
        {
            get
            {
                if (_LabelsCache == null)
                    _LabelsCache = new StringCollection();

                return _LabelsCache;
            }
        }
        
        
        private XmlNode xmlLabelsRoot;
        public XmlNode XmlLabelsRoot
        {
            get
            {
                return xmlLabelsRoot;
            }
        }

        public Labels(string LabelsPath)
            : this(LabelsPath, "labels.xml")
        {
        }

        //private string labelPath;
        //public string LabelPath 
        //{
        //    get
        //    {
        //        if (labelPath == null)
        //        {
        //            labelPath = HttpContext.Current.Application["LabelsPath"].ToString();
        //        }
        //        return labelPath;
        //    }
        //}

        public Labels(string LabelsPath, string xmlFileName)
        {
            XmlDocument oXmlDoc = new XmlDocument();
            bool failed = false;
            try
            {                
                // bisogna sistemarla a regime perch� manca il riferimento al NetCms.Configuration e alla CurrentNetWork
                //string xmlFilePath = absoluteWebRoot + "\\config\\" + xmlFileName;
                string xmlFilePath = LabelsPath + "\\" + xmlFileName;
                oXmlDoc.Load(xmlFilePath);
            }
            catch (Exception ex)
            {
                //xmlConfigs = null;
                NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);
                failed = true;
            }
            if (failed)
            {
                try
                {
                    string xmlFilePath = LabelsPath + "\\" + xmlFileName;
                    oXmlDoc.Load(xmlFilePath);
                }
                catch (Exception ex)
                {
                    NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                    ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Warning, ex);
                    //xmlConfigs = null;
                }
            }
            xmlLabelsRoot = oXmlDoc.DocumentElement;
        }

        public Labels(XmlNode rootNode)
        {
            xmlLabelsRoot = rootNode;
        }

        protected string FormatLabel(string label)
        {
            string outLabel = label.Replace("<![CDATA[", "").Replace("]]>", "");
            return outLabel;
        }
    }

}