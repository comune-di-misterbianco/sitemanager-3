using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace NetUtility
{
    public class SheetsControl
    {
        private SheetsCollection _Sheets;
        public SheetsCollection Sheets
        {
            get
            {
                if (_Sheets == null)
                    _Sheets = new SheetsCollection(this);
                return _Sheets;
            }
        }

        private bool _AddScriptCode;
        public bool AddScriptCode
        {
            get
            {
                return _AddScriptCode;
                
            }
            set
            {
                _AddScriptCode = value;
            }
        }

        private NetUtility.RequestVariable _SelectedSheetControlRequest;
        public NetUtility.RequestVariable SelectedSheetControlRequest
        {
            get 
            {
                if (_SelectedSheetControlRequest == null)
                {
                    _SelectedSheetControlRequest = new RequestVariable(SelectedSheetControlID, NetUtility.RequestVariable.RequestType.Form);
                }
                return _SelectedSheetControlRequest; 
            }

        }
	
        private String _ID;
        public String ID
        {
            get { return _ID; }
        }
        
        public SheetsControl(string id)
        {
            _ID = id;
            _AddScriptCode = true;
        }

        public HtmlGenericControl GetControl()
        {
            HtmlGenericControl control = new HtmlGenericControl("div");
            control.ID = ID;
            //control.Attributes["class"] = "SheetsControl";
            //control.Attributes["class"] = "row";

            HtmlGenericControl ul = new HtmlGenericControl("ul");
            ul.Attributes["class"] = "nav nav-tabs nav-justified";

            HtmlGenericControl select = new HtmlGenericControl("div");
            select.Attributes["class"] = "SheetsLabels";
            HtmlGenericControl sheetsControls = new HtmlGenericControl("div");
            sheetsControls.ID = this.ID+ "_SheetsContainer";
            //sheetsControls.Attributes["class"] = "SheetsContainer";
            sheetsControls.Attributes["class"] = "tab-content";
            for (int i = 0; i < Sheets.Count; i++)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                if (i == 0)
                    li.Attributes.Add("class","active");
                
                
                
                HtmlGenericControl Label = Sheets[i].LabelControl();
               // select.Controls.Add(Label);
                HtmlGenericControl a = new HtmlGenericControl("a");
                a.Attributes["href"] = "#" + ID + "_Sheet" + Sheets[i].Index;
                a.Attributes["data-toggle"] = "tab";
                a.Controls.Add(new LiteralControl(Sheets[i].Label.ToString()));
                li.Controls.Add(a);
                ul.Controls.Add(li);

                HtmlGenericControl sheet = new HtmlGenericControl("div");
                sheet.Controls.Add(Sheets[i].Control);
                sheet.ID = ID + "_Sheet" + Sheets[i].Index;
                //sheet.Attributes["class"] = "SingleSheet";
                sheet.Attributes["class"] = "col-md-12 tab-pane fade";

                sheetsControls.Controls.Add(sheet);
                //if (SelectedSheetControlRequest.IsValid() && SelectedSheetControlRequest.StringValue.Length > 0)
                //{
                //    if (SelectedSheetControlRequest.StringValue == Sheets[i].ID)
                //        Label.Attributes["class"] += " SheetLabelSelected";                        
                //    else
                //        sheet.Style.Add("display", "none");
                //}
                //else
                //{
                //if (i > 0)
                //sheet.Style.Add("display", "none");
                //else
                //    Label.Attributes["class"] += " SheetLabelSelected";
                //}
                if (i == 0)
                    sheet.Attributes["class"] = "col-md-12 tab-pane fade in active";
            }
            control.Controls.Add(ul);
            control.Controls.Add(SelectedSheetControl);
            //if (AddScriptCode)
            //    control.Controls.Add(Scripts);
            control.Controls.Add(select);
            control.Controls.Add(sheetsControls);

            return control;
        }

       
        private HtmlGenericControl Scripts
        {
            get
            {
                HtmlGenericControl script = new HtmlGenericControl("script");               
                    script.InnerHtml += @"
                                        function SelectSheet(NewSheetID,SelectedSheetControlID,SheetsControlID)
                                        {
                                            oSheetsContainer = document.getElementById(SheetsControlID+'_SheetsContainer');
                                            if(oSheetsContainer)
                                            {
                                              Sheets = oSheetsContainer.childNodes
                                              
                                              var i;
                                              for (i in Sheets)
                                              {
                                                   oOldSheetControl = document.getElementById(Sheets[i].id);
                                                   oOldSheetControlLabel = document.getElementById(Sheets[i].id+'_Label');

                                                   if(oOldSheetControl)
                                                   {
                                                      oOldSheetControl.style.display = 'none';
                                                      if(oOldSheetControlLabel && oOldSheetControlLabel.className )
                                                        oOldSheetControlLabel.className  = oOldSheetControlLabel.className.replace(' SheetLabelSelected','');
                                                   }
                                              } 

                                              oSelectedSheetControl = document.getElementById(SelectedSheetControlID);
                                              var oSheetControl = document.getElementById(NewSheetID);
                                              if(oSheetControl)
                                              {
                                                  var oSheetControlLabel = document.getElementById(NewSheetID+'_Label');
                                                  oSheetControl.style.display = 'block';
                                                  if(oSheetControlLabel)
                                                    oSheetControlLabel.className = oSheetControlLabel.className  + ' SheetLabelSelected';
                                                  if(oSelectedSheetControl)
                                                    oSelectedSheetControl.value = NewSheetID;
                                              }
                                            }
                                        }
                        ";

                return script;
            }
        }

        private HtmlGenericControl SelectedSheetControl
        {
            get
            {
                string value = "";
                if (SelectedSheetControlRequest.IsValid() && SelectedSheetControlRequest.StringValue.Length > 0)
                {
                    value = SelectedSheetControlRequest.StringValue;
                }
                else
                    if (Sheets.Count > 0)
                        value = Sheets[0].ID;

                HtmlGenericControl hidden = new HtmlGenericControl("span");
                hidden.InnerHtml = "<input ";
                hidden.InnerHtml += "name=\"" + SelectedSheetControlID + "\" ";
                hidden.InnerHtml += "id=\"" + SelectedSheetControlID + "\" ";
                hidden.InnerHtml += "type=\"hidden\" ";
                hidden.InnerHtml += "value=\"" + value + "\" />";

                return hidden;
            }
        }
     
        public string SelectedSheetControlID
        {
            get
            {
                return ID + "_SelectedSheetControl";
            }
        }
    }

    public class HtmlSheet
    {
        private HtmlControl _Control;
        public HtmlControl Control
        {
            get { return _Control; }
            set { _Control = value; }
        }

        private string  _Label;
        public string  Label
        {
            get { return _Label; }
            set { _Label = value; }
        }

        private int _Index;
        public int Index
        {
            get { return _Index; }
            set { _Index = value; }
        }

        public string ID
        {
            get
            {
                string id = "";
                if (this.SheetsControlOwner != null)
                    id = SheetsControlOwner.ID + "_";
                id += "Sheet" + this.Index;

                return id;
            }
        }

        private SheetsControl _SheetsControlOwner;
        public SheetsControl SheetsControlOwner
        {
            get { return _SheetsControlOwner; }
        }


        public HtmlSheet(string label, HtmlControl control)
        {
            _Label = label;
            _Control = control;
        }

        public HtmlSheet()
        {
        }

        public HtmlGenericControl LabelControl()
        {
            HtmlGenericControl LabelControl_Container = new HtmlGenericControl("div");
            HtmlGenericControl LabelControl_Sx = new HtmlGenericControl("div");
            HtmlGenericControl LabelControl_Body = new HtmlGenericControl("div");
            HtmlGenericControl LabelControl_Dx = new HtmlGenericControl("div");

            LabelControl_Container.ID = "";
            if (this.SheetsControlOwner != null)
                LabelControl_Container.ID = SheetsControlOwner.ID + "_";
            LabelControl_Container.ID += "Sheet" + this.Index + "_Label";

            LabelControl_Container.Attributes["class"] = "SheetLabelContainer";
            LabelControl_Sx.Attributes["class"] = "SheetLabelSx";
            LabelControl_Dx.Attributes["class"] = "SheetLabelDx";
            LabelControl_Body.Attributes["class"] = "SheetLabelBody";

            LabelControl_Container.Controls.Add(LabelControl_Sx);
            LabelControl_Sx.Controls.Add(LabelControl_Dx);
            LabelControl_Dx.Controls.Add(LabelControl_Body);

            string href = "#";
            if (this.SheetsControlOwner != null)
            {
                href = SheetsControlOwner.ID + "_";
                href += "Sheet" + this.Index;
                href = "javascript:SelectSheet('" + href + "','" + SheetsControlOwner.SelectedSheetControlID + "','" + this.SheetsControlOwner.ID + "');";
            }
            LabelControl_Body.InnerHtml = "<a href=\"" + href + "\">";
            LabelControl_Body.InnerHtml += "<span>" + Label + "</span>";
            LabelControl_Body.InnerHtml += "</a>";

            return LabelControl_Container;
        }

        public void SetOwner(SheetsControl owner)
        {
            _SheetsControlOwner = owner;
        }
    }
}
