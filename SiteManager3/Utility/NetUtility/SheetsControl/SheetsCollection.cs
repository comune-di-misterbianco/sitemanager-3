using System;
using System.Data;
using System.Configuration;

/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>

namespace NetUtility
{
    public class SheetsCollection
    {
        private NetCollection coll;
        public int Count { get { return coll.Count; } }
        private SheetsControl Owner;

        public SheetsCollection(SheetsControl owner)
        {
            //
            // TODO: Add constructor logic here
            //
            Owner = owner;
            coll = new NetCollection();
        }

        public void Add(HtmlSheet sheet)
        {
            coll.Add(sheet.GetHashCode().ToString(), sheet);
            sheet.Index = this.IndexOf(sheet);
            sheet.SetOwner(Owner);
        }

        public void Remove(HtmlSheet sheet)
        {
            coll.Remove(sheet.GetHashCode().ToString());
        }
        public void Remove(int index)
        {
            coll.Remove(index);
        }

        public int IndexOf(HtmlSheet sheet)
        {
            for (int i = 0; i < coll.Count; i++)
            {
                if((string)coll[i].Key == sheet.GetHashCode().ToString())
                    return i;
            }
            return -1;
        }

        public void Clear( )
        {
            coll.Clear();
        }

        public HtmlSheet this[int i]
        {
            get
            {
                HtmlSheet sheet = (HtmlSheet)coll[coll.Keys[i]];
                return sheet;
            }
        }
        /*
        public string this[string str]
        {
            get
            {
                string val = (string)coll[str];
                return val;
            }
        }
        
        public bool Contains(string str)
        {
            return coll[str]!=null;
        }*/

    }
}