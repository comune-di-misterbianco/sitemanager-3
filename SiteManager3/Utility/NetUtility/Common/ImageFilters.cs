using System;
using System.Data;
using System.Drawing.Imaging;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace NetUtility
{
    public class ImageFilters
    {
        public int Width
        {
            get { return Image.Width; }
        }
        public int Height
        {
            get { return Image.Height; }
        }

        public int ThumbWidth
        {
            get { return _ThumbWidth; }
        }
        public int ThumbHeight
        {
            get { return _ThumbHeight; }
        }

        private int _ThumbWidth;
        private int _ThumbHeight;

        public string ThumbName
        {
            get 
            {
                if (_NameOverride == null)
                {
                    string _ThumbName = TargetPath.JustName;

                    if (NameOffset.Length > 0)
                        _ThumbName += "_" + NameOffset;

                    _ThumbName += "." + TargetPath.Extension;
                
                    return _ThumbName; 
                }
                return _NameOverride;
            }
        }

        private string _NameOverride;
        public string NameOverride
        {
            get
            {
                return _NameOverride;
            }
            set { _NameOverride = value; }
        }

        private string _NameOffset;
        public string NameOffset
        {
            get {
                if (_NameOffset == null)
                    _NameOffset = "tmb";
                return _NameOffset; }
            set { _NameOffset = value; }
        }

        private System.Drawing.Image Image;

        private FilePathInformation SourcePath;
        private FilePathInformation _TargetPath;
        public FilePathInformation TargetPath
        {
            get
            {
                return _TargetPath;
            }
            set
            {
                _TargetPath = value;
            }
        }

        public ImageFilters(string path)
        {
            SourcePath = new FilePathInformation(path);
            _TargetPath = SourcePath;
            Image = System.Drawing.Image.FromFile(path);             
        }

        public System.Drawing.Image ResizeOnWidth(int NewWidthSize)
        {
            int WX = NewWidthSize;
            int HX = Image.Height * WX / Image.Width;

            _ThumbWidth = WX;
            _ThumbHeight = HX;

            return Resize(WX,HX);
        }
        public System.Drawing.Image ResizeOnHeight(int NewHeigthSize)
        {
            int HX = NewHeigthSize;
            int WX = Image.Width * HX / Image.Height;
            
            _ThumbWidth = WX;
            _ThumbHeight = HX;

            return Resize(WX, HX);
        }
        public System.Drawing.Image Resize(int Width, int Heigth)
        {            
            System.Drawing.Image.GetThumbnailImageAbort dummyCallBack = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback);
            System.Drawing.Image thumbNailImg = Image.GetThumbnailImage(Width, Heigth, dummyCallBack, IntPtr.Zero);

            _ThumbWidth = Width;
            _ThumbHeight = Height;

            return thumbNailImg;
        }

        public void CreateThumbnail(int MaxSize)
        {
            System.Drawing.Image Thumbnail;
            if(Width>Height)
                Thumbnail = ResizeOnWidth(MaxSize);
            else
                Thumbnail = ResizeOnHeight(MaxSize);


            Thumbnail.Save(TargetPath.Path + ThumbName, Image.RawFormat);
        }
        public void CreateThumbnailWidth(int NewWidthSize)
        {
            System.Drawing.Image Thumbnail = ResizeOnWidth(NewWidthSize);
           
            Thumbnail.Save(TargetPath.Path + ThumbName, Image.RawFormat);
        }
        public void CreateThumbnailHeight(int NewHeigthSize)
        {
            System.Drawing.Image Thumbnail = ResizeOnHeight(NewHeigthSize);
           
            Thumbnail.Save(TargetPath.Path + ThumbName, Image.RawFormat);
        }
        public void CreateThumbnail(int Width, int Heigth)
        {
            System.Drawing.Image Thumbnail = Resize(Width, Heigth);
                      
            Thumbnail.Save(TargetPath.Path + ThumbName, Image.RawFormat);
        }

        public void Save()
        {           
            Image.Save(TargetPath.Path + ThumbName, Image.RawFormat);
        }
        public void SaveAs(string filename)
        {
            Image.Save(TargetPath.Path + filename, Image.RawFormat);
        }
        public bool ThumbnailCallback()
        {
            return false;
        }
   }
}