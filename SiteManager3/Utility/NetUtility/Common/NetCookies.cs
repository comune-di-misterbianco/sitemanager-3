using System;
using System.Web;
using NetCms.Connections;

namespace NetUtility
{
    public class NetCookies
    {
        private HttpResponse Response = HttpContext.Current.Response;
        private HttpRequest Request = HttpContext.Current.Request;

        public NetCookies()
        {

        }

        public string this[string key]
        {
            get
            {
                return this.Contains(key) ?
                    Request.Cookies[key].Value :
                    "";
            }
        }

        public bool Contains(string key)
        {
            return (Response.Cookies[key] != null);
        }
        public void Set(string key, string value)
        {
            Set(key, value, DateTime.Now.AddYears(30));
        }
        public void Set(string key, string value, DateTime expire)
        {
            Response.Cookies[key].Value = value;
            Response.Cookies[key].Expires = expire;
        }
        public void Remove(string key)
        {
            Response.Cookies[key].Expires = DateTime.MinValue;
            Response.Cookies[key].Value = "";
        }
    }
}