using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace NetUtility
{
    public class UriExtended
    {
        public string PageName
        {
            get
            { 
                if(_PageName==null)
                {
                string[] path = Uri.PathAndQuery.Split('/');
                string teoric = path[path.Length - 1];
                path = teoric.Split('?');
                teoric  = path[0];
                if (!teoric.Contains(".aspx"))
                    teoric = "default.aspx";
                _PageName= teoric.ToLower().Trim();
                }
                return _PageName;
            }
        }        
        private string _PageName;

        public string FullPath
        {
            get
            {
                if(_FullPath==null)
                {
                    _FullPath = Path;
                    while (_FullPath != null&&_FullPath.EndsWith("/")) 
                        _FullPath = _FullPath.Remove(_FullPath.Length-1);
                    _FullPath += "/"+PageName;
                }
                return _FullPath;
            }
        }
        private string _FullPath;

	    public string Path
        {
            get
            {
                if (_Path != null)
                {
                    _Path = Uri.PathAndQuery.ToLower();
                    _Path = _Path.Replace(PageName.ToLower(), "");
                }
                return _Path;
            }
        }
        private string _Path;

        public Uri Uri
        {
            get
            {
                return _uri;
            }
        }
        private Uri _uri;

        public UriExtended(Uri uri)
        {
            _uri = uri;
        }


    

   
   }
}