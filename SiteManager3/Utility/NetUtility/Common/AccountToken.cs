using System;
using System.Data;
using NetCms.Connections;

namespace NetUtility
{
    public class AccountToken
    {
        protected int _ID;
        public int ID
        {
            get
            {
                return _ID;
            }
        }

        protected Connection Conn;

        public AccountToken(Connection conn, int UserID)
        {
            Conn = conn;
            _ID = UserID;
        }
    }
}