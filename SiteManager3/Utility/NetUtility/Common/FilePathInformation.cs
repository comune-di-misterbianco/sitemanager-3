using System;
using System.Data;
using System.Drawing.Imaging;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace NetUtility
{
    public class FilePathInformation
    {        
        private string _Extension;
        public string Extension
        {
            get
            {
                if (_Extension == null)
                {
                    int start = FullName.LastIndexOf('.') + 1;
                    int end = (FullName.Length - FullName.LastIndexOf('.')) - 1;
                    _Extension = FullName.Substring(start, end);
                }
                return _Extension;
            }
        }

        private string _FullName;
        public string FullName
        {
            get
            {
                if (_FullName == null)
                {
                    int ExtractPos = FullPath.LastIndexOf("\\");
                    int start = ExtractPos + 1;
                    int end = (FullPath.Length - ExtractPos - 1);
                    _FullName = FullPath.Substring(start, end);                    
                }
                return _FullName;
            }
        }

        private string _JustName;
        public string JustName
        {
            get
            {
                if (_JustName == null)
                {
                    int start = 0;
                    int end = FullName.LastIndexOf('.');
                    _JustName = FullName.Substring(start, end);
                }
                return _JustName;
            }
        }
	       
        private string _Path;
        public string Path
        {
            get
            {
                if (_Path == null)
                {
                    int ExtractPos = FullPath.LastIndexOf("\\")+1;
                    _Path = FullPath.Substring(0, ExtractPos);
                }
                return _Path;
            }
        }

        private string _FullPath;
        public string FullPath
        {
            get
            {
                return _FullPath;
            }
        }

        public FilePathInformation(string path)
        {
            _FullPath = path;
        }
   }
}