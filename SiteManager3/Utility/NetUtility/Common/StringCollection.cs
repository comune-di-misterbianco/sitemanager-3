using System;
using System.Collections;

/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>


public class StringCollection : IEnumerable
{
    private NetCollection coll;
    public int Count { get { return coll.Count; } }
    public StringCollection()
    {
        //
        // TODO: Add constructor logic here
        //
        coll = new NetCollection();
    }
    public StringCollection(string firstValue)
    {
        //
        // TODO: Add constructor logic here
        //
        coll = new NetCollection();
    }
    public StringCollection(string[] Values)
    {
        //
        // TODO: Add constructor logic here
        //
        coll = new NetCollection();
        foreach (string s in Values)
            this.Add(s);
    }
    public void Add(string str, string key)
    {
        coll.Add(key, str);
    }
    public void Add(string str)
    {
        coll.Add(str, str);
    }

    public void Remove(string str)
    {
        coll.Remove(str);
    }

    public void Clear()
    {
        coll.Clear();
    }

    public string this[int i]
    {
        get
        {
            string str = (string)coll[coll.Keys[i]];
            return str;
        }
    }

    public string this[string str]
    {
        get
        {
            string val = (string)coll[str];
            return val;
        }
    }

    public bool Contains(string str)
    {
        return coll[str] != null;
    }

    #region Enumerator

    public IEnumerator GetEnumerator()
    {
        return new CollectionEnumerator(this);
    }

    private class CollectionEnumerator : IEnumerator
    {
        private int CurentPos = -1;
        private StringCollection Collection;
        public CollectionEnumerator(StringCollection coll)
        {
            Collection = coll;
        }
        public object Current
        {
            get
            {
                return Collection[CurentPos];
            }
        }
        public bool MoveNext()
        {
            if (CurentPos < Collection.Count - 1)
            {
                CurentPos++;
                return true;
            }
            else
                return false;
        }
        public void Reset()
        {
            CurentPos = -1;
        }
    }
    #endregion
}
