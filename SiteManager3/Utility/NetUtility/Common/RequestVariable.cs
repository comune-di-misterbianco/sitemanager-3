using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace NetUtility
{
    public class RequestVariable
    {
        private DateTime NullDate = new DateTime(0001, 1, 1);
        private NameValueCollection Items;
        private string RequestedVariable;
        public string OriginalValue
        {
            get
            {
                return RequestedVariable;
            }
        }
        public enum VariableType
        {
            Integer,
            String,
            Date
        }
        public enum RequestType
        {
            Form,
            QueryString,
            Form_QueryString,
            QueryString_Form        
        }
        private string _Key;
        public string Key
        {
            get { return _Key; }
        }
	

        private bool ValidContent;

        public RequestVariable(string key)
            :this(key,RequestType.Form)
        {
           
        }
        public RequestVariable(string key,HttpRequest Request)
        {
            _Key = key;
            if (Request != null && Request.Form != null && Request.Form[key] != null && key != null && key.Length > 0)
            {
                RequestedVariable = Request.Form[key];
                ValidContent = true;
            }
            else
                if (Request != null && Request.QueryString != null && Request.QueryString[key] != null && key != null && key.Length > 0)
                {
                    RequestedVariable = Request.QueryString[key];
                    ValidContent = true;
                }
                else
                    ValidContent = false;
        }
        public RequestVariable(string key, RequestType requestType)
        {
            _Key = key;
            #region RequestType.Form
            if (requestType == RequestType.Form)
            {
                NameValueCollection Coll = HttpContext.Current.Request.Form;
                if (Coll != null && Coll[key] != null && key != null && key.Length > 0)
                {
                    RequestedVariable = Coll[key];
                    ValidContent = true;
                }
                else
                    ValidContent = false;
            } 
            #endregion

            #region RequestType.QueryString
            if (requestType == RequestType.QueryString)
            {
                NameValueCollection Coll = HttpContext.Current.Request.QueryString;
                if (Coll != null && Coll[key] != null && key != null && key.Length > 0)
                {
                    RequestedVariable = Coll[key];
                    ValidContent = true;
                }
                else
                    ValidContent = false;
            }
            #endregion

            #region RequestType.Form_QueryString

            if (requestType == RequestType.Form_QueryString)
            {
                HttpRequest Request = HttpContext.Current.Request;
                if (Request != null && Request.Form != null && Request.Form[key] != null && key != null && key.Length > 0)
                {
                    RequestedVariable = Request.Form[key];
                    ValidContent = true;
                }
                else
                    if (Request != null && Request.QueryString != null && Request.QueryString[key] != null && key != null && key.Length > 0)
                    {
                        RequestedVariable = Request.QueryString[key];
                        ValidContent = true;
                    }
                    else
                        ValidContent = false;
            }

            #endregion

            #region RequestType.QueryString_Form

            if (requestType == RequestType.QueryString_Form)
            {
                HttpRequest Request = HttpContext.Current.Request;
                if (Request != null && Request.QueryString != null && Request.QueryString[key] != null && key != null && key.Length > 0)
                {
                    RequestedVariable = Request.QueryString[key];

                    ValidContent = true;
                }
                else
                    if (Request != null && Request.Form != null && Request.Form[key] != null && key != null && key.Length > 0)
                    {
                        RequestedVariable = Request.Form[key];
                        ValidContent = true;
                    }
                    else
                        ValidContent = false;
            }

            #endregion
        }           
        public RequestVariable(string key, NameValueCollection Coll)
        {
            _Key = key;
            if (Coll != null && Coll[key] != null && key != null && key.Length > 0)
            {
                RequestedVariable = Coll[key];
                ValidContent = true;
            }
            else
                ValidContent = false;
        }

        public bool IsPostBack
        {
            get
            {
                if (HttpContext.Current.Request.Form[this.Key] != null)
                    return true;
                return false;
            }
        }
        public bool IsValidInteger
        {
            get
            {
                return IsValid(VariableType.Integer);
            }
        }

        public bool IsValidString
        {
            get
            {
                return IsValid(VariableType.String) && this.StringValue.Length>0;
            }
        }
        public bool IsValid()
        {
            return IsValid(VariableType.String);
        }
        public bool IsValid(VariableType type)
        {
            switch (type)
            {
                case VariableType.Date:
                    GetAsDate();
                    return DateParsed;
                case VariableType.String:
                    GetAsString();
                    return StringParsed;
                case VariableType.Integer:
                    GetAsInt();
                    return IntParsed;
            }
            return false;
        }
        
        #region Int

        public int GetAsInt()
        {
            int value = 0;
            IntParsed = int.TryParse(OriginalValue, out value);
            return value;
        }
        private bool IntParsed;
        public int IntValue
        {
            get
            {
                if (_IntValue == 0)
                    _IntValue = GetAsInt();
                return _IntValue;
            }

        }
        private int _IntValue;
        #endregion

        #region String

        public string GetAsString()
        {
            StringParsed = OriginalValue != null;
            if (StringParsed)
                return OriginalValue.Trim().Replace("'", "''");
            else 
                return "";
        }
        private bool StringParsed;
        public string StringValue
        {
            get
            {
                if (_StringValue == null)
                    _StringValue = GetAsString();
                return _StringValue;
            }

        }
        private string _StringValue;

        #endregion

        #region DateTime

        private DateTime GetAsDate()
        {
            DateTime value = new DateTime(1, 1, 1);
            DateParsed = OriginalValue!=null && OriginalValue.Trim().Length > 0 && DateTime.TryParse(OriginalValue, out value);
            if (!DateParsed)
                value = new DateTime(1, 1, 1);
            return value;
        }
        private bool DateParsed;
        public DateTime DateTimeValue
        {
            get
            {
                if (_DateTimeValue == null || _DateTimeValue == NullDate)
                    _DateTimeValue = GetAsDate();
                return _DateTimeValue;
            }

        }
        private DateTime _DateTimeValue;

        #endregion

        public override string ToString()
        {
            if (IsValid())
                return StringValue;
            else return "";
        }
    }

    public class FormRequestVariable:RequestVariable
    {

        public FormRequestVariable(string key)
            : base(key, RequestType.Form)
        {

        }
       
    }
    public class QueryStringRequestVariable : RequestVariable
    {

        public QueryStringRequestVariable(string key)
            : base(key, RequestType.QueryString)
        {

        }

    }
}
