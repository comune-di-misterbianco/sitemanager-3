using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace NetUtility
{
    public class TreeUtility
    {
        public static string FormatNumber(string number, int lenght)
        {
            string value = number;

            while (value.Length < lenght)
                value = "0" + value;

            return value;
        }
        public static string FormatNumber(string number)
        {
            return FormatNumber(number, 6);
        }


        public static string CutWords(string str, int NumOfWords)
        {
            string[] Words = str.Split(' ');
            string filteredString = "";
            for (int i = 0; i < Words.Length && i <= 10; i++)
            {
                filteredString += Words[i] + " ";
            }
            return filteredString.Trim();
        }
    }

    


}