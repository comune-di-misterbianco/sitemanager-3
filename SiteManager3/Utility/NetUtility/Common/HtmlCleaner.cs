using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace NetUtility
{
    public class HtmlCleaner
    {
        private string HtmlStr;
        public HtmlCleaner(string htmlstring)
        {
            HtmlStr = htmlstring;
        }

        public HtmlCleaner()
            :this("")
        {
        }

        public string CleanHtml()
        {
            return CleanHtml(HtmlStr);
        }
        public string CleanHtml(string htmlstring)
        {
            return htmlstring;
        }

        public string TruncateText(string text, int length)
        {
            text = text.Replace("\"","");
            if (text == null || text.Length < length)
                return text;
            int iNextSpace = text.LastIndexOf(" ", length);
            return string.Format("{0}...", text.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }
    }
}