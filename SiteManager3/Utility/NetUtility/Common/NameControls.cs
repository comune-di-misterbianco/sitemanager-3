﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetUtility
{
    public class NameControls
    {
        public static bool CheckName(string input)
        {
            bool ok = true;
            string[] chars = { "'", "#", "@", "%", "=", "&", "/", "\\", "*", "?", ":", "|", "<", ">", "\"" };
            for (int i = 0; i < chars.Length && ok; i++)
                if (input.Contains(chars[i])) ok = false;

            return ok;
        }

    }
}
