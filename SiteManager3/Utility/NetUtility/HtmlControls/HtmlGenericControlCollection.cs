using System;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Collections;

/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>

namespace NetUtility
{
    public class HtmlGenericControlCollection : IEnumerable
    {
        private NetCollection coll;
        public int Count { get { return coll.Count; } }
        public HtmlGenericControlCollection()
        {
            coll = new NetCollection();
        }


        public void Add(HtmlGenericControl node,string key)
        {
            coll.Add(key, node);
        }

        public void Remove(string key)
        {
            coll.Remove(key);
        }
        public void Remove(int index)
        {
            coll.Remove(index);
        }

        public void Clear()
        {
            coll.Clear();
        }

        public HtmlGenericControl this[int i]
        {
            get
            {
                HtmlGenericControl str = (HtmlGenericControl)coll[coll.Keys[i]];
                return str;
            }
        }
        public HtmlGenericControl this[string key]
        {
            get
            {
                HtmlGenericControl val = (HtmlGenericControl)coll[key];
                return val;
            }
        }


        public bool Contains(string key)
        {
            return coll[key] != null;
        }
        
        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private HtmlGenericControlCollection Collection;
            public CollectionEnumerator(HtmlGenericControlCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}