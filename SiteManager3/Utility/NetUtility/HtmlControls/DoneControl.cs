using System;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace HtmlGenericControls
{
    public class DoneControl
    {
        public string CssClass
        {
            get
            {
                return _CssClass;
            }
            set
            {
                _CssClass = value;
            }
        }
        public string _CssClass;
        
        private string _Text;
        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }

        private string _Title;
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public HtmlGenericControl Control
        {
            get
            {
                return GetControl();
            }
        }

        private StringCollection _LinksHrefs;
        private StringCollection LinksHrefs
        {
            get { return _LinksHrefs; }
        }
	    
        private StringCollection _LinksLabels;
        private StringCollection LinksLabels
        {
            get { return _LinksLabels; }
        }

        public DoneControl(string title,string text)
        {
            _Text = text;
            _Title = title;
            _LinksHrefs = new StringCollection();
            _LinksLabels = new StringCollection();
        }
        public DoneControl(string title)
            :this(title,"")
        {
           
        }

        public void addLink(string label, string href)
        {
            this.LinksHrefs.Add(href);
            this.LinksLabels.Add(label);
        }

        public HtmlGenericControl GetControl()
        {
            Fieldset set = new Fieldset(this.Title);
            if (CssClass != null && CssClass != string.Empty)
                set.Class = CssClass;

            Div text = new Div();
            text.InnerHtml = this.Text;
            set.Controls.Add(text);

            if (LinksHrefs.Count > 0)
            {
                HtmlGenericControl links = new HtmlGenericControl(LinksHrefs.Count > 1 ? "ul" : "div");
                for (int i = 0; i < LinksHrefs.Count; i++)
                {
                    HtmlGenericControl shield = new HtmlGenericControl(LinksHrefs.Count > 1 ? "li" : "p");

                    HtmlGenericControl link = new HtmlGenericControl("a");
                    link.Attributes["href"] = this.LinksHrefs[i];
                    link.Attributes["title"] = this.LinksLabels[i];
                    link.InnerHtml = this.LinksLabels[i];

                    shield.Controls.Add(link);
                    links.Controls.Add(shield);
                }
                set.Controls.Add(links);
            }

            return set;
        }
    }

}