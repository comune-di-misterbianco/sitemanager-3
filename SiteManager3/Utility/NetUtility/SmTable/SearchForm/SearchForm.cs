using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for SearchForm
/// </summary>
namespace NetForms
{
    public class SearchForm
    {
        private NetFieldsCollection Fields;
        public string SubmitButtonLabel;
        public bool Link = false;

        public SearchForm()
        {
            Fields = new NetFieldsCollection();
            SubmitButtonLabel = "Cerca";
        }

        public void addField(NetField field)
        {
            Fields.Add(field);
        }

        public string getFilterString()
        {
            return getFilterString(HttpContext.Current.Request.Form);
        }
        public string getFilterString(NameValueCollection post)
        {
            string where = "";

            for (int i = 0; i < Fields.Count; i++)
            {
                string value = "";
                if (post.Get(Fields[i].getFieldName()) != null)
                    value = post.Get(Fields[i].getFieldName()).ToString();
                if (value.Length > 0)
                {
                    if (where.Length > 0) where += " AND ";
                    where += Fields[i].getFilter(value);
                }
            }

            if (where.Length > 0 && !Link)
                where = " WHERE" + where;
            if (where.Length > 0 && Link)
                where = " AND" + where;

            return where;
        }

        public Control getForm()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");


            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerText = "Ricerca";
            fieldset.Controls.Add(legend);

            div.Controls.Add(fieldset);

            for (int i = 0; i < Fields.Count; i++)
            {
                NetField field = Fields[i];
                fieldset.Controls.Add(field.getControl());
            }

            Button bt = new Button();
            bt.Text = SubmitButtonLabel;
            bt.ID = "SearchFormButton";
            bt.OnClientClick = "javascript: setSubmitSource('" + bt.ID + "')";
            HtmlGenericControl reset = new HtmlGenericControl("p");
            reset.InnerHtml = "<a href=\""+HttpContext.Current.Request.Url+"\">Azzera Ricerca</a>";

            fieldset.Controls.Add(bt);
            fieldset.Controls.Add(reset);

            return div;
        }
    }
}