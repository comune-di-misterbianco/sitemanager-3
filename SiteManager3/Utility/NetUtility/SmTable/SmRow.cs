using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Connections;

/// <summary>
/// Summary description for SmRow
/// </summary>

namespace NetTable
{
    public class SmRow
    {
        private SmTableColumnsCollection Columns;
        private string SqlQuery;
        private Connection Conn;

        public SmRow(string sqlQuery, Connection conn)
        {
            SqlQuery = sqlQuery;
            Conn = conn;
            Columns = new SmTableColumnsCollection();
        }

        public void addColumn(SmTableColumn col)
        {
            Columns.Add(col);
        }

        public HtmlGenericControl getDetailsView(string legendText)
        {
            return getDetailsView(legendText, "");
        }
        public HtmlGenericControl getDetailsView(string legendText,string infotext)
        {
            HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset(legendText);
            set.Attributes["class"] = "Details";

            HtmlGenericControls.Div info = new HtmlGenericControls.Div();
            info.Class = "DetailsText";
            info.InnerHtml = infotext;
            
            HtmlGenericControls.Div content = new HtmlGenericControls.Div();
            content.InnerHtml = BuildContent();

            set.Controls.Add(info);
            set.Controls.Add(content);

            return set;
        }
        public HtmlGenericControl getDetailsView()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "Details";

            div.InnerHtml = BuildContent();

            return div;

        }

        protected string BuildContent()
        {
            string html = "<table cellpadding=\"0\" cellspacing=\"0\">";
            DataTable table = Conn.SqlQuery(SqlQuery);
            if (table.Rows.Count>0)
            {
                for (int i = 0; i < Columns.Count; i++)
                {
                    SmTableColumn column = Columns[i];

                    string value = "";
                    if (table.Columns.Contains(column.FieldName))
                        value = table.Rows[0][column.FieldName].ToString();
                    value = column.filterValue(value);

                    html += FormatDetailRow(column.Caption, value, i%2==0);
                }
            }
            else
                html+= "<tr class=\"DetailsRow DetailsRowAlternate\"><td>Nessun Record</td></tr>";
            
            html += "</table>";

            return html;
        }

        protected string FormatDetailRow(string Label, string Content, bool alternate)
        {
            string format = "<tr class=\"DetailsRow DetailsRow{0}\"><td class=\"DetailsLabel\"><strong>{1}: </strong></td><td>{2}</td></tr>";
            return string.Format(format, alternate ? "Alternate" : "Normal", Label, Content);
        }
    }
}