using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Reflection;
using System.Xml;
using System.IO;

/// <summary>
/// Summary description for AppDataTable
/// </summary>

namespace NetTable
{
    public class SmTable
    {
        protected SmTableColumnsCollection _Columns;
        protected SmTableColumnsCollection Columns
        {
            get
            {
                return _Columns;
            }
        }
        
        protected string _SqlQuery;
        protected int CurentPage;

        private string _CssClass;
        public string CssClass
        {
            get { return _CssClass; }
            set { _CssClass = value; }
        }

        private string _TableID;
        public string TableID
        {
            get { return _TableID; }
            set { _TableID = value; }
        }

        private string _PaginationLink;
        public string PaginationLink
        {
            get 
            {
                if (_PaginationLink == null)
                    _PaginationLink = "javascript: setpage({0})";
                return _PaginationLink; 
            }
            set { _PaginationLink = value; }
        }
        
        private bool _AddPaginationControl;
        public bool AddPaginationControl
        {
            get { return _AddPaginationControl; }
            set { _AddPaginationControl = value; }
        }
	
        private string _OverTitle;
        public string OverTitle
        {
            get { return _OverTitle; }
            set { _OverTitle = value; }
        }

        private string _PaginationKey;
        public string PaginationKey
        {
            get 
            {
                if (_PaginationKey == null)
                {
                    _PaginationKey = "page";
                }
                return _PaginationKey; 
            }
            set { _PaginationKey = value; }
        }
        
        private string _DeleteFieldID;
        public string DeleteFieldID
        {
            get
            {
                if (_DeleteFieldID == null)
                {
                    _DeleteFieldID = "del";
                }
                return _DeleteFieldID;
            }
            set { _DeleteFieldID = value; }
        }
        
        private string _EnableFieldID;
        public string EnableFieldID
        {
            get
            {
                if (_EnableFieldID == null)
                {
                    _EnableFieldID = "enable";
                }
                return _EnableFieldID;
            }
            set { _EnableFieldID = value; }
        }

        public int RecordPerPagina
        {
            get
            {
                return _RecordPerPagina;
            }
            set
            {
                _RecordPerPagina = value;
            }
        }
        private int _RecordPerPagina;

        private string _NoRecordMsg;
        public string NoRecordMsg
        {
            get { return _NoRecordMsg; }
            set { _NoRecordMsg = value; }
        }

        private bool _Pagination;
        public bool Pagination
        {
            get { return _Pagination; }
            set { _Pagination = value; }
        }

        private void initVariables()
        {
            string page = HttpContext.Current.Request.Form.Get(PaginationKey);
            if (page != null)
            {
                int value;
                int.TryParse(page, out value);
                if (value > 1)
                    CurentPage = value;
            }
            else
            {
                if (HttpContext.Current.Request.QueryString.Get(PaginationKey) != null)
                {
                    int value = 0;
                    if (int.TryParse(HttpContext.Current.Request.QueryString.Get(PaginationKey), out value))
                        CurentPage = value;
                }
            }
        }

        private NetCms.Connections.Connection Conn;

        public SmTable(string paginationKey, string sqlQuery, NetCms.Connections.Connection conn)
        {
            _Columns = new SmTableColumnsCollection();
            PaginationKey = paginationKey;
            AddPaginationControl = true;
            _SqlQuery = sqlQuery;
            _NoRecordMsg = "Nessun record";
            RecordPerPagina = 25;
            initVariables();
            _Pagination = true;
            Conn = conn;
            _CssClass = "tab";
        }
        public SmTable(int curentpage, string sqlQuery, NetCms.Connections.Connection conn)
        {
            _Columns = new SmTableColumnsCollection();
            CurentPage = curentpage;
            _SqlQuery = sqlQuery;
            _NoRecordMsg = "Nessun record";
            RecordPerPagina = 25;
            _AddPaginationControl = true;
            _Pagination = true;
            Conn = conn;
            _CssClass = "tab";
        }
        public SmTable(string sqlQuery, NetCms.Connections.Connection conn)
            :this(1,sqlQuery, conn)
        {
            _AddPaginationControl = true;
            _Pagination = false;
            initVariables();
        }

        public bool deleteControl(string sql, string key)
        {
            if (key != null && key.Length > 0)
            {
                string del = key;
                Conn.Execute(sql.Replace("{0}", del));
                return true;
            }
            return false;
        }

        public void addColumn(SmTableColumn col)
        {
            _Columns.Add(col);
        }

        public virtual HtmlGenericControl getTable()
        {
            return getTable("");
        }
        public virtual HtmlGenericControl getTable(string search)
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            string Sql = _SqlQuery;
            if (search.Trim().Length > 0)
            {
                if (Sql.ToLower().Contains("where"))
                {
                    Sql += search.Replace("WHERE", "AND").Replace("where", "AND");
                }
                else
                    Sql += search;
            }
            int paginaCorrente = CurentPage;
            int PageCount = getPageCount(Sql);
            if (paginaCorrente > PageCount)
                paginaCorrente = 1;
            if (paginaCorrente < 1)
                paginaCorrente = 1;
            HtmlGenericControl span = new HtmlGenericControl("span");
            if(AddPaginationControl)
                span.InnerHtml = " <input type=\"hidden\" name=\"" + this.PaginationKey + "\" id=\"" + this.PaginationKey + "\" value=\"" + paginaCorrente + "\" />\n";
            span.InnerHtml += "<input type=\"hidden\" name=\"" + this.DeleteFieldID + "\" id=\"" + this.DeleteFieldID + "\" value=\"\" />";
            span.InnerHtml += "<input type=\"hidden\" name=\"" + this.EnableFieldID + "\" id=\"" + this.EnableFieldID + "\" value=\"\" />";
            div.Controls.Add(span);

            HtmlGenericControl table = new HtmlGenericControl("table");
            if (TableID != null && TableID != string.Empty)
                table.ID = this.TableID;
            div.Controls.Add(table);

            table.Attributes["class"] = CssClass;
            table.Attributes["border"] = "0";
            table.Attributes["cellspacing"] = "0";
            table.Attributes["cellpadding"] = "00";

            HtmlGenericControl tr = new HtmlGenericControl("tr");
            table.Controls.Add(tr);

            HtmlGenericControl th;
            if (OverTitle != null)
            {
                th = new HtmlGenericControl("th");
                th.Attributes["colspan"] = _Columns.Count.ToString();
                th.Attributes["class"] = "overtitle";
                th.InnerHtml = OverTitle;
                tr.Controls.Add(th);

                tr = new HtmlGenericControl("tr");
                table.Controls.Add(tr);
            }

            string last = "";
            th = new HtmlGenericControl("th");
            int actualSpan = 1;

            for (int i = 0; i < Columns.Count; i++)
            {
                if (Columns[i] != null)
                {
                    if (last != Columns[i].Caption)
                    {
                        th = new HtmlGenericControl("th");
                        th.InnerHtml = Columns[i].Caption;
                        th.Attributes["scope"] = "col";
                        tr.Controls.Add(th);
                        th.Attributes["class"] = Columns[i].ColumnCssClass;
                        actualSpan = 1;
                    }
                    else
                    {
                        th.Attributes["colspan"] = (++actualSpan).ToString();
                    }
                }
                last = Columns[i].Caption;
            }

            DataTable tab = Conn.SqlQuery(Sql);
            if (tab.Rows.Count > 0)
            {
                int offset;

                if (Pagination)
                    offset = (paginaCorrente - 1) * RecordPerPagina;
                else
                    offset = 0;

                bool alternate = false;
                for (int j = offset; j < tab.Rows.Count && (!Pagination || j < RecordPerPagina + offset); j++)
                {
                    tr = new HtmlGenericControl("tr");

                    if (alternate)
                        tr.Attributes["class"] = "alternate";
                    alternate = !alternate;

                    bool addRow = true;

                    for (int i = 0; i < Columns.Count; i++)
                    {
                        if (Columns[i] != null)
                        {
                            //if (Columns[i].SkipRows == null)
                            {
                                HtmlGenericControl td = _Columns[i].getField(tab.Rows[j]);
                                if (!Columns[i].SkipRows.Contains(j.ToString()) && td!=null)
                                {
                                    addRow = addRow && _Columns[i].ShowOwnerRow;
                                    _Columns[i].ShowOwnerRow = true;
                                    tr.Controls.Add(td);
                                    td.Attributes["class"] += " " + _Columns[i].ColumnCssClass;
                                }
                                else
                                    addRow = false;
                            }
                            /*else
                                addRow = false;*/
                        }
                    }
                    if (addRow)
                        table.Controls.Add(tr);
                    
                }
                #region Paginazione
                
                if (Pagination)
                {
                    tr = new HtmlGenericControl("tr");
                    HtmlGenericControl td_page = new HtmlGenericControl("td");

                    NetUtility.PageMaker pager = new NetUtility.PageMaker(paginaCorrente, NetUtility.PageMaker.VariabileType.Form);
                    pager.RPP = this.RecordPerPagina;
                    pager.Key = PaginationKey;
                    td_page = pager.PageNavigation(PaginationLink, tab.Rows.Count);

                    td_page.TagName = "td";
                    td_page.Attributes["colspan"] = this.Columns.Count.ToString();
                    td_page.Attributes["class"] += " FolderNavigationControls";

                    tr.Controls.Add(td_page);
                    table.Controls.Add(tr);
                }
                #endregion
            }
            else
            {
                tr = new HtmlGenericControl("tr");

                table.Controls.Add(tr);

                HtmlGenericControl td = new HtmlGenericControl("td");
                td.Attributes["colspan"] = _Columns.Count.ToString();
                td.InnerHtml = NoRecordMsg;
                tr.Controls.Add(td);

            }

            return div;
        }

        protected int getPageCount(string sql)
        {

            DataTable dt = Conn.SqlQuery(sql);

            float fl_pc = (float)(dt.Rows.Count - 1) / (float)RecordPerPagina;

            int pc = int.Parse(fl_pc.ToString().Split(',')[0].ToString());

            return pc + 1;
        }
    }
}