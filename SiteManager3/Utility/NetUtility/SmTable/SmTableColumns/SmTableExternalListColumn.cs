using System.Data;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for SmTableExternalColumn
/// </summary>

namespace NetTable
{
    public class SmTableExternalListColumn : SmTableColumn
    {
        public string SqlQuery;

        private NetCms.Connections.Connection _Conn;

        public SmTableExternalListColumn(string fieldname, string caption, string sqlQuery, NetCms.Connections.Connection conn)
            : base(fieldname, caption)
        {
            SqlQuery = sqlQuery;
            _Conn = conn;

        }

        public override HtmlGenericControl getField(DataRow row)
        {
            string value = row[this.FieldName].ToString();
            HtmlGenericControl td = new HtmlGenericControl("td");
            string retstr = "";
            DataTable table = _Conn.SqlQuery(SqlQuery.Replace("{0}", value));
            if (table.Rows.Count>0)
            {
                if (retstr != "") retstr += ", ";
                retstr += table.Rows[0][0].ToString();
            }

            td.InnerHtml = "&nbsp;" + retstr;
            return td;
        }

        public override string filterValue(string value)
        {
            string retstr = "";
            DataTable table = _Conn.SqlQuery(SqlQuery.Replace("{0}", value));
            if (table.Rows.Count > 0)
            {
                retstr = table.Rows[0][0].ToString();
            }

            return retstr;
        }
    }
}