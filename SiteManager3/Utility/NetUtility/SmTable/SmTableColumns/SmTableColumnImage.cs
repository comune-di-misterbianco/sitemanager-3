using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for AppDataColumn
/// </summary>

namespace NetTable
{
    public class SmTableColumnImage : SmTableColumn
    {
        private string src;

        public SmTableColumnImage(string fieldname, string caption, string path)
            : base(fieldname, caption)
        {
            src = path;
        }

        public override HtmlGenericControl getField(DataRow row)
        {
            string value = row[this.FieldName].ToString();
            
            HtmlGenericControl td = new HtmlGenericControl("td");
            td.Attributes["class"] = "imagecell";
            td.InnerHtml = "<img src=\"" +src+"/" +value+"\" >";

            return td;
        }

        public override string filterValue(string value)
        {           
            return value;
        }
    }
}