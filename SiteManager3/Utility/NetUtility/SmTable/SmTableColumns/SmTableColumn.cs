using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for AppDataColumn
/// </summary>

namespace NetTable
{
    public class SmTableColumn
    {
        public bool Search;
        private string _FieldName;
        public string FieldName { get { return _FieldName; } }
        protected string _Caption;
        public string Caption { get { return _Caption; } }

        private int _ColSpan;
        public int ColSpan
        {
            get { return _ColSpan; }
            set { _ColSpan = value; }
        }


        private string _ColumnCssClass = "";
        public string ColumnCssClass
        {
            get { return _ColumnCssClass; }
            set { _ColumnCssClass = value; }
        }
        public StringCollection SkipRows
        {
            get
            {
                if (_SkipRows == null) _SkipRows = new StringCollection();
                return _SkipRows;
            }
            set
            {
                _SkipRows = value;
            }
        }
        private StringCollection _SkipRows;

        private int _CutTextAt = 0;
        public int CutTextAt
        {
            get { return _CutTextAt; }
            set { _CutTextAt = value; }
        }

        private bool _AddDots = true;
        public bool AddDots
        {
            get { return _AddDots; }
            set { _AddDots = value; }
        }

        private bool _ShowOwnerRow = true;
        public bool ShowOwnerRow
        {
            get { return _ShowOwnerRow; }
            set { _ShowOwnerRow = value; }
        }

        public bool IsDate = false;

        public SmTableColumn(string fieldName, string caption)
        {
            _FieldName = fieldName;
            _Caption = caption;
            Search = false;
        }

        public virtual HtmlGenericControl getField(DataRow row)
        {
            string value = row[this.FieldName].ToString();
            HtmlGenericControl td = new HtmlGenericControl("td");
            if (ColSpan > 0)
                td.Attributes["colspan"] = ColSpan.ToString();
            if (IsDate && value.Length > 10) 
                value = value.Remove(10);
            else
                if (CutTextAt > 0 && value.Length > CutTextAt)
                {
                    value = value.Remove(CutTextAt);
                    if(AddDots)
                        value += "...";
                }
            td.InnerHtml = "&nbsp;" + value;

            return td;
        }

        public virtual string filterValue(string value)
        {
            if (IsDate && value.Length > 10) value = value.Remove(10);
            return value;
        }

        public virtual HtmlGenericControl SkippedRowControl
        {
            get 
            {
                    HtmlGenericControl newcontrol = new HtmlGenericControl("td");
                    newcontrol.InnerHtml = "&nbsp";
                    return newcontrol;

            }
            set
            {
            }
        }
    }
}