using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetUtility.LanguagesUtility;
/// <summary>
/// Summary description for SmTableExternalColumn
/// </summary>

namespace NetTable
{
    public class SmTableLangLabelColumn : SmTableColumn
    {
        private string _Title;
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _IconCssClass;
        public string IconCssClass
        {
            get 
            {
                if (_IconCssClass == null)
                {
                    _IconCssClass = "TreeColumn_DefaultIcon";
                }
                return _IconCssClass; 
            }
            set { _IconCssClass = value; }
        }

        private string _IndentBy;
        public string IndentBy
        {
            get { return _IndentBy; }
            set { _IndentBy = value; }
        }

        private int _IndentOffset =0;
        public int IndentOffset
        {
            get { return _IndentOffset; }
            set { _IndentOffset = value; }
        }

        private string CssClass;

        private string _Href;
        public string Href
        {
            get { return _Href; }
            set { _Href = value; }
        }

        private string _HrefValueField;
        public string HrefValueField
        {
            get 
            {
                if (_HrefValueField == null)
                {
                    _HrefValueField = this.FieldName;
                }
                return _HrefValueField; 
            }
            set { _HrefValueField = value; }
        }

        private bool _LoadDefaultValue;
        public bool LoadDefaultValue
        {
            get
            {
                return _LoadDefaultValue;
            }
            set
            {
                _LoadDefaultValue = value;
            }
        }

        private NetUtility.LanguagesUtility.LangDataSource LangData;

        public SmTableLangLabelColumn(string fieldName,string tableColumnCaption, string cssClass,NetUtility.LanguagesUtility.LangDataSource langData)
            : this(fieldName,tableColumnCaption, cssClass, langData, "")
        {
        }
        public SmTableLangLabelColumn(string fieldName, string tableColumnCaption, string cssClass, NetUtility.LanguagesUtility.LangDataSource langData, string href)
            : base(fieldName, tableColumnCaption)
        {
            Href = href;
            LangData = langData;
            CssClass = cssClass;
            _Title = string.Empty;
        }

        public override HtmlGenericControl getField(DataRow row)
        {
            int LabelRecordID = int.Parse(row[this.FieldName].ToString());
            string HrefValue = row[this.HrefValueField].ToString();

            LangLabel label = new LangLabel(LabelRecordID, LangData, this.LoadDefaultValue ? LangLabel.LabelPickModes.LoadDefault : LangLabel.LabelPickModes.OnlyCurrent);

            HtmlGenericControl td = new HtmlGenericControl("td");
            if(CssClass!=null && CssClass.Length>0)
                td.Attributes["class"] = CssClass;

            string CuttedText = label.ValueByCurrentLang; ;
            if (this.CutTextAt > 0 && CuttedText.Length >= CutTextAt)
                CuttedText = CuttedText.Remove(this.CutTextAt)+"...";

            string html = "";
            if (Href != null && Href.Length > 0)
            {
                html += "<a href=\"";
                html += string.Format(Href, HrefValue);
                html += "\" title=\"" + (Title.Length > 0 ? Title : label.ValueByCurrentLang) + "\"><span>";
                html += CuttedText;
                html += "</span></a>";
            }
            else
            {
                html += "<span>";
                html += CuttedText;
                html += "</span>";
            }
            if (this.IndentBy != null && this.IndentBy.Length > 0)
            {
                td.InnerHtml += BuildIndentLines(row);
                
            }
            td.InnerHtml += html;
            return td;
        }

        private string BuildIndentLines(DataRow row)
        {
            int Depth = int.Parse(row[this.IndentBy].ToString());
            int RowIndex = row.Table.Rows.IndexOf(row);

            int NextRowDepth = 0;

            bool HasNextRow = row.Table.Rows.Count > RowIndex + 1;

            if (HasNextRow)
                NextRowDepth = int.Parse(row.Table.Rows[RowIndex + 1][this.IndentBy].ToString());


            string lines = "";

            for (int i = 0; i < Depth - IndentOffset; i++)
            {
                string ClassName = " ";

                if (RowIndex == 0) ClassName = " TreeColumn_LinesVoid";
                else
                {
                    if (!HasNextRow) ClassName = " TreeColumn_LinesBottom";
                    else
                    {
                        if (NextRowDepth < Depth && i == 0) ClassName = " TreeColumn_LinesBottom";
                        else
                            if (NextRowDepth == Depth && i == 0) ClassName = " TreeColumn_LinesJoin";

                    }
                }
                lines = "<span class=\"TreeColumn_Lines" + ClassName + "\">&nbsp;</span>" + lines;
            }
            string icon = "";
            icon = "<span class=\"TreeColumn_Icon " + this.IconCssClass + "\">&nbsp;</span>";

            return  lines + icon;
        }
    }
}