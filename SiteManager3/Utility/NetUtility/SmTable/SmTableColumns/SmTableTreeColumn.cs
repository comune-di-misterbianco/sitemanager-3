using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Connections;

/// <summary>
/// Summary description for AppDataColumn
/// </summary>

namespace NetTable
{
    public class SmTableTreeColumn : SmTableColumn
    {

        private Connection Conn;

        public string SqlQuery;

        public SmTableTreeColumn(string fieldName, string caption, string sqlQuery, Connection conn)
            : base(fieldName, caption)
        {
            Conn = conn;
            SqlQuery = sqlQuery;
        }


        public override HtmlGenericControl getField(DataRow row)
        {
            string value = row[this.FieldName].ToString();
            HtmlGenericControl td = new HtmlGenericControl("td");

            DataTable reader = Conn.SqlQuery(SqlQuery.Replace("{0}", value));
            int count = 0;
            string name = "";
            if (reader.Rows.Count>0)
            {
                int.TryParse(reader.Rows[0][1].ToString(), out count);
                name = reader.Rows[0][0].ToString();
            }
            for (int i = 0; i < count; i++)
            {
                td.InnerHtml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            }

            td.InnerHtml += "&nbsp;" + name;

            return td;
        }

    }
}