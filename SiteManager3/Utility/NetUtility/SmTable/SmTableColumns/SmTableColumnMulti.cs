using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for AppDataColumn
/// </summary>

namespace NetTable
{
    public class SmTableColumnMulti : SmTableColumn
    {
        private string[] Fields;
        private string[] Separators;

        public SmTableColumnMulti(string[] fields,string[] separators, string caption)
            : base("", caption)
        {
            Separators = separators;
            Fields = fields;
            _Caption = caption;
        }

        public SmTableColumnMulti(string[] fields, string caption)
            :base("",caption)
        {
            Fields = fields;
        }

        public SmTableColumnMulti(string fields, string caption)
            : base("", caption)
        {
            Fields = fields.Split(','); ;
        }

        public override HtmlGenericControl getField(DataRow row)
        {
            string value = "";
            for (int i = 0; i < Fields.Length; i++)
            {
                string tmp;
                if (Fields[i].Contains("{") && Fields[i].Contains("}"))
                    tmp = Fields[i].Replace("{", "").Replace("}", "");
                else
                    tmp = row[Fields[i]].ToString();

                tmp = tmp.Trim();

                value += tmp + (Separators!=null && Separators.Length>i?Separators[i]: " ");
            }

            HtmlGenericControl td = new HtmlGenericControl("td");
            
            if (ColSpan > 0)
                td.Attributes["colspan"] = ColSpan.ToString();
            
            if (CutTextAt > 0 && value.Length > CutTextAt)
            {
                value = value.Remove(CutTextAt);
                if(AddDots)
                    value += "...";
            }
            td.InnerHtml = "&nbsp;" + value;

            return td;
        }

        public override string filterValue(string value)
        {           
            return value;
        }
    }
}