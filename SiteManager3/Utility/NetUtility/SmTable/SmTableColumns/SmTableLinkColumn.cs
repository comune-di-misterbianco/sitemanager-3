using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for SmTableExternalColumn
/// </summary>

namespace NetTable
{
    public class SmTableLinkColumn : SmTableColumn
    {
        private string Classe;
        private string Label;
        private string Href;
        private string Title;
        private string LabelField;

        public SmTableLinkColumn(string dataField,string labelField, string classe, string label,  string href)
            : this(dataField, labelField, classe, label, href, "")
        {
        }
        public SmTableLinkColumn(string dataField, string labelField, string classe, string label, string href, string title)
            : base(dataField, label)
        {
            LabelField = labelField;
            Classe = classe;
            Label = label;
            Href = href;
            Title = title;
        }

        public override HtmlGenericControl getField(DataRow row)
        {
            string value = row[this.LabelField].ToString();
            string href = row[this.FieldName].ToString();

            HtmlGenericControl td = new HtmlGenericControl("td");
            td.Attributes["class"] = Classe;

            string html = "";
            html += "<a href=\"";
            html += Href.Replace("{0}", href);
            html += "\" title=\"" + (Title.Length>0?Title:value) + "\"><span>";
            html += value;
            html += "</span></a>";

            td.InnerHtml = html;
            return td;
        }
    }
}