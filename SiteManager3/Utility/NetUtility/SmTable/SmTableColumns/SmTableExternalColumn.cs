using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for SmTableExternalColumn
/// </summary>

namespace NetTable
{
    public class SmTableExternalColumn : SmTableColumn
    {
        private string DisplayMember;
        private string JoinField;
        private NetCms.Connections.Connection Conn;
        private DataTable Table;

        public SmTableExternalColumn(string fieldName, string caption, NetCms.Connections.Connection conn, string displayMember, string displaySource, string joinfield)
            : base(fieldName, caption)
        {
            DisplayMember = displayMember;
            Table = conn.SqlQuery("SELECT " + DisplayMember + "," + joinfield + " FROM " + displaySource);
            JoinField = joinfield;
        }
        public SmTableExternalColumn(string fieldName, string caption, DataTable table, string displayMember, string joinfield)
            : base(fieldName, caption)
        {
            DisplayMember = displayMember;
            Table = table;
            JoinField = joinfield;
        }
        public override HtmlGenericControl getField(DataRow row)
        {
            string value = row[this.FieldName].ToString();
            HtmlGenericControl td = new HtmlGenericControl("td");
            string retstr = "";
            DataRow[] rows = Table.Select(JoinField + " = '" + value + "'");
            if (rows.Length > 0)
            {
                retstr += rows[0][DisplayMember].ToString();
            }
            td.InnerHtml = "&nbsp;" + retstr.Trim();
            return td;
        }

        public override string filterValue(string value)
        {
            string retstr = "";
            DataRow[] rows = Table.Select(JoinField + " = '" + value + "'");
            if (rows.Length > 0)
            {
                retstr = rows[0][DisplayMember.ToString()].ToString();
            }

            return retstr;
        }
    }
}