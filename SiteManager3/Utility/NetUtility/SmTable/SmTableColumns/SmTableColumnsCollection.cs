using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for SmColumnsCollection
/// </summary>

namespace NetTable
{
    public class SmTableColumnsCollection
    {
        private NetCollection coll;
        public int Count { get { return coll.Count; } }
        public SmTableColumnsCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new NetCollection();
        }

        public void Add(SmTableColumn col)
        {
            coll.Add(col.FieldName + col.GetHashCode().ToString(), col);
        }

        public SmTableColumn this[int i]
        {
            get
            {
                SmTableColumn field = (SmTableColumn)coll[coll.Keys[i]];
                return field;
            }
        }

        public SmTableColumn this[string key]
        {
            get
            {
                SmTableColumn field = (SmTableColumn)coll[key];
                return field;
            }
        }
    }
}