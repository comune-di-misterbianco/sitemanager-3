using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Connections;

/// <summary>
/// Summary description for AppDataColumn
/// </summary>

namespace NetTable
{
    public class SmTableTreeColumn2 : SmTableColumn
    {
        private string  _Icon;
        public string  Icon
        {
            set { _Icon = value; }
            get { return _Icon; }
        }

        private IconTypes _IconType;
        public IconTypes IconType
        {
            get { return _IconType; }
            set { _IconType = value; }
        }
	
        public enum IconTypes
        {
            None,
            DatabaseCssClass,
            DatabaseUrl,
            Url,
            CssClass
        }
	
        private Connection Conn;

        public string DepthField;
        public string IDField;
        public string NameField;

        public SmTableTreeColumn2(string caption, string idField, string nameField, string depthField, Connection conn)
            : base(idField, caption)
        {
            Conn = conn;

            DepthField = depthField;
            IDField = idField;
            NameField = nameField;
        }


        public override HtmlGenericControl getField(DataRow row)
        {
            int Depth = int.Parse(row[this.DepthField].ToString());
            string Name = row[this.NameField].ToString();
            string ID = row[this.IDField].ToString();            
            int RowIndex = row.Table.Rows.IndexOf(row);

            int NextRowDepth = 0;

            bool HasNextRow = row.Table.Rows.Count > RowIndex + 1;

            if(HasNextRow)
                NextRowDepth = int.Parse(row.Table.Rows[RowIndex + 1][this.DepthField].ToString());

            
            string lines = "";
            
            for (int i = 0; i < Depth; i++)
            {
                string ClassName = " " ;

                if (RowIndex == 0) ClassName = " TreeColumn_LinesVoid";
                else
                {
                    if (!HasNextRow) ClassName = " TreeColumn_LinesBottom";
                    else
                    {
                        if (NextRowDepth<Depth && i==0) ClassName = " TreeColumn_LinesBottom";
                        else
                            if (NextRowDepth == Depth && i == 0) ClassName = " TreeColumn_LinesJoin";

                    }
                }
                lines = "<span class=\"TreeColumn_Lines" + ClassName + "\">&nbsp;</span>" + lines;
            }

            string icon = "";

            if (this.IconType == IconTypes.CssClass)
                icon = "<span class=\"TreeColumn_Icon "+this.Icon+"\">&nbsp;</span>";

            if (this.IconType == IconTypes.Url)
                icon = "<span class=\"TreeColumn_Icon\" style=\"background-image:url(" + this.Icon + ")\">&nbsp;</span>";
            
            if (this.IconType == IconTypes.DatabaseCssClass)
                icon = "<span class=\"TreeColumn_Icon " + row[this.Icon] + "\">&nbsp;</span>";

            if (this.IconType == IconTypes.Url)
                icon = "<span class=\"TreeColumn_Icon\" style=\"background-image:url(" + row[this.Icon] + ")\">&nbsp;</span>";

            string label = "<span class=\"TreeColumn_Content\">&nbsp;" + Name + "</span>";

            HtmlGenericControl td = new HtmlGenericControl("td");
            td.InnerHtml += lines + icon + label;

            return td;
        }

    }
}