using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for SmTableExternalColumn
/// </summary>

namespace NetTable
{
    public class SmTableActionColumn : SmTableColumn
    {
        public string Classe;
        public string Label;
        public string Href;
        public string Title;
        public bool Thickbox;
        
        public SmTableActionColumn(string fieldName, string classe, string Caption, string href)
            : this(fieldName, classe, Caption, href, "",Caption)
        {
        }
        public SmTableActionColumn(string fieldName, string classe, string Caption, string href, string title)
            : this(fieldName, classe, Caption, href, "", Caption )            
        {
        }

        public SmTableActionColumn(string fieldName, string classe, string Caption, string href, string title,string label)
            : base(fieldName, Caption)
        {
            Classe = classe;
            Label = label;
            Href = href;
            Title = title;
        }
        public override HtmlGenericControl getField(DataRow row)
        {
            string value = row[this.FieldName].ToString();
            string DbLabel = row.Table.Columns.Contains(this.Label)?row[this.Label].ToString():Label;
            HtmlGenericControl td = new HtmlGenericControl("td");
            td.Attributes["class"] = Classe;

            string html = "";
            html += "<a ";
            if(Thickbox)
                html += "class=\"thickbox\" ";
            html += "href=\" ";
            html += Href.Replace("{0}", value);
            html += "\" title=\"" + (Title.Length>0?Title:Label) + "\"><span>";
            html += DbLabel;
            html += "</span></a>";

            td.InnerHtml = html;
            return td;
        }
    }
}