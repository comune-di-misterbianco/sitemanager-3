using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for SmTableExternalColumn
/// </summary>

namespace NetTable
{
    public class SmTableBoleanColumn : SmTableColumn
    {
        public Option Option1;
        public Option Option2;
        public string JoinField;
        public string LinkValueField;


        public struct Option
        {
            private string _Label;
            public string Label
            {
                get { return _Label; }
                set { _Label = value; }
            }

            private string _Link;
            public string Link
            {
                get { return _Link; }
                set { _Link = value; }
            }

            private string _CssClass;
            public string CssClass
            {
                get { return _CssClass; }
                set { _CssClass = value; }
            }
            public override string ToString()
            {
                return FormatOption();
            }

            private string FormatOption()
            {
                string value = "<td {2}><{0} {1}>{3}</{0}></td>";

                string tagname = "span";
                string cssClass = "";
                string link = "";
                string label = this.Label;

                if (this.CssClass != null && this.CssClass.Length > 0)
                    cssClass = "class=\"" + this.CssClass + "\"";

                if (this.Link != null && this.Link.Length > 0)
                {
                    link = "href\"" + this.Link + "\"";
                    tagname = "a";
                    label = "<span>" + label + "</span>";
                }

                return String.Format(value, tagname, link, cssClass, label);
            }
            public HtmlGenericControl GetControl()
            {
                return GetControl("");
            }
            public HtmlGenericControl GetControl(string LinkValue)
            {
                HtmlGenericControl td = new HtmlGenericControl("td");
                string value = "<{0} {1}>{2}</{0}>";

                string tagname = "span";
                string link = "";
                string label = "&nbsp;" + this.Label;

                if (this.CssClass != null && this.CssClass.Length > 0)
                    td.Attributes["class"] = this.CssClass;

                if (this.Link != null && this.Link.Length > 0)
                {
                    link = "href=\"";
                    if(LinkValue.Length>0)
                        link += String.Format(this.Link,LinkValue);
                    else
                        link += this.Link;
                    link += "\"";
                    tagname = "a";
                    label = "<span>" + label + "</span>";
                }

                td.InnerHtml = String.Format(value, tagname, link, label);
                return td; 
            }
        }

        public SmTableBoleanColumn(string fieldName, string caption, Option option_true, Option option_false,string linkValueField)
            : base(fieldName, caption)
        {
            Option1 = option_true;
            Option2 = option_false;
            this.LinkValueField = linkValueField;
        }

        public SmTableBoleanColumn(string fieldName, string caption, Option option_true, Option option_false)
            : this(fieldName, caption,option_true,option_false,"")
        {}

        public SmTableBoleanColumn(string fieldName, string caption, string option_true, string option_false)
            : base(fieldName, caption)
        {
            Option1 = new Option();
            Option1.Label = option_true;
            Option2 = new Option();
            Option2.Label = option_false;
        }
        public override HtmlGenericControl getField(DataRow row)
        {
            string state = row[this.FieldName].ToString();
            string value = "";
            if (this.LinkValueField != null &&
               this.LinkValueField.Length > 0 &&
               row[this.LinkValueField] != null)
                value = row[this.LinkValueField].ToString();
            HtmlGenericControl td;
            if (
                state == "1" ||
                state == "true" ||
                state == "True" ||
                state == "vero" ||
                state == "Vero"
            )
                td = Option1.GetControl(value);
            else
                td = Option2.GetControl(value);
            return td;
        }

        public override string filterValue(string value)
        {
            if (
                value == "1" ||
                value == "true" ||
                value == "True" ||
                value == "vero" ||
                value == "Vero"
            )
                value = Option1.Label;
            else
                value = Option2.Label;

            return value;
        }
    }
}