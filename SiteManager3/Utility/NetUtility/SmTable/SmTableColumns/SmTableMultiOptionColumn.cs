using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for SmTableExternalColumn
/// </summary>

namespace NetTable
{
    public class SmTableMultiOptionColumn : SmTableColumn
    {
        public string[] Options;


        public SmTableMultiOptionColumn(string fieldName, string caption, string options)
            : base(fieldName, caption)
        {
            Options = options.Split(',');
        }
        public SmTableMultiOptionColumn(string fieldName, string caption, string[] options)
            : base(fieldName, caption)
        {
            Options = options;
        }

        public override HtmlGenericControl getField(DataRow row)
        {
            HtmlGenericControl td = new HtmlGenericControl("td");
            td.InnerHtml = "";
            int value;
            int.TryParse(row[this.FieldName].ToString(),out value);
            if (value >=0 && value < Options.Length)
                td.InnerHtml += Options[value];

            if(td.InnerHtml.Length==0)
                td.InnerHtml = "&nbsp;";
            return td;
        }

        public override string filterValue(string val)
        {
            string filtered = "";
            int value;
            int.TryParse(val, out value);
            if (value >= 0 && value < Options.Length)
                filtered += Options[value];
            return filtered;
        }
    }
}