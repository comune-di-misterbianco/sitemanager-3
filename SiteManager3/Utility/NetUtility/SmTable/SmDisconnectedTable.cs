using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for AppDataTable
/// </summary>

namespace NetTable
{
    public class SmDisconnectedTable
    {
        protected SmTableColumnsCollection Columns;
        
        protected string _SqlQuery;
        protected int CurentPage;

        private string _OverTitle;
        public string OverTitle
        {
            get { return _OverTitle; }
            set { _OverTitle = value; }
        }

        private string _PaginationKey;
        public string PaginationKey
        {
            get 
            {
                if (_PaginationKey == null)
                {
                    _PaginationKey = "page";
                }
                return _PaginationKey; 
            }
            set { _PaginationKey = value; }
        }

        public string CssClass
        {
            get { return _CssClass; }
            set { _CssClass = value; }
        }
        private string _CssClass = "tab";

        private string _DeleteFieldID;
        public string DeleteFieldID
        {
            get
            {
                if (_DeleteFieldID == null)
                {
                    _DeleteFieldID = "del";
                }
                return _DeleteFieldID;
            }
            set { _DeleteFieldID = value; }
        }

        private string _EnableFieldID;
        public string EnableFieldID
        {
            get
            {
                if (_EnableFieldID == null)
                {
                    _EnableFieldID = "enable";
                }
                return _EnableFieldID;
            }
            set { _EnableFieldID = value; }
        }

        public int RecordPerPagina
        {
            get
            {
                return _RecordPerPagina;
            }
            set
            {
                _RecordPerPagina = value;
            }
        }
        private int _RecordPerPagina;

        private string _NoRecordMsg;
        public string NoRecordMsg
        {
            get { return _NoRecordMsg; }
            set { _NoRecordMsg = value; }
        }
        /*
        private DataTable _Table;
        public DataTable Table
        {
            get { return _Table; }
        }
        */
        private DataRow[] _Rows;
        public DataRow[] Rows
        {
            get 
            {
               return _Rows; 
            }
        }

        private bool _Pagination;
        public bool Pagination
        {
            get { return _Pagination; }
            set { _Pagination = value; }
        }

        public SmDisconnectedTable(int curentpage,DataRow[] rows)
        {
            Columns = new SmTableColumnsCollection();
            CurentPage = curentpage;
            _NoRecordMsg = "Nessun record";
            RecordPerPagina = 25;

            _Pagination = true;
            _Rows = rows;
        }
        public SmDisconnectedTable(int curentpage, DataTable table)
            : this(curentpage, table.Select())
        { }
        public SmDisconnectedTable(DataTable table)
            : this(1, table.Select())
        {
            _Pagination = false;
        }
        public SmDisconnectedTable(DataRow[] rows)
            : this(1, rows)
        {
            _Pagination = false;
        }

        
        public void addColumn(SmTableColumn col)
        {
            Columns.Add(col);
        }

        public virtual Control getTable()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            /*string Where = search;
            if (search.Trim().Length > 0)
            {
                if (Where.ToLower().Contains("where"))
                {
                    Where += search.Replace("WHERE", "").Replace("where", "");
                }
                else
                    Where += search;
            }
            string Order = order;
            if (Order.Trim().Length > 0)
            {
                if (Order.ToLower().Contains("order by"))
                {
                    Order += search.Replace("ORDER BY", "").Replace("order by", "");
                }
                else
                    Order += search;
            }*/

            int paginaCorrente = CurentPage;
            int PageCount = getPageCount(Rows.Length);
            if (paginaCorrente > PageCount)
                paginaCorrente = 1;
            if (paginaCorrente < 1)
                paginaCorrente = 1;
            HtmlGenericControl span = new HtmlGenericControl("span");
            span.InnerHtml = "<input type=\"hidden\" name=\"" + this.PaginationKey + "\" id=\"" + this.PaginationKey + "\" value=\"" + paginaCorrente + "\" />\n";
            span.InnerHtml += "<input type=\"hidden\" name=\"" + this.DeleteFieldID + "\" id=\"" + this.DeleteFieldID + "\" value=\"\" />";
            span.InnerHtml += "<input type=\"hidden\" name=\"" + this.EnableFieldID + "\" id=\"" + this.EnableFieldID + "\" value=\"\" />";
            div.Controls.Add(span);

            HtmlGenericControl table = new HtmlGenericControl("table");

            div.Controls.Add(table);

            table.Attributes["class"] = CssClass;
            table.Attributes["border"] = "0";
            table.Attributes["cellspacing"] = "0";
            table.Attributes["cellpadding"] = "00";

            HtmlGenericControl tr = new HtmlGenericControl("tr");
            table.Controls.Add(tr);

            if (OverTitle != null)
            {
                HtmlGenericControl th = new HtmlGenericControl("th");
                th.Attributes["colspan"] = Columns.Count.ToString();
                th.Attributes["class"] = "overtitle";
                th.InnerHtml = OverTitle;
                tr.Controls.Add(th);

                tr = new HtmlGenericControl("tr");
                table.Controls.Add(tr);
            }
            for (int i = 0; i < Columns.Count; i++)
            {
                if (Columns[i] != null)
                {
                    HtmlGenericControl th = new HtmlGenericControl("th");
                    th.InnerHtml = Columns[i].Caption;
                    th.Attributes["scope"] = "col";
                    tr.Controls.Add(th);
                }
            }

            if (Rows.Length > 0)
            {
                int offset;

                if (Pagination)
                    offset = (paginaCorrente - 1) * RecordPerPagina;
                else
                    offset = 0;

                bool alternate = false;
                for (int j = offset; j < Rows.Length && (!Pagination || j < RecordPerPagina + offset); j++)
                {
                        tr = new HtmlGenericControl("tr");

                        if (alternate)
                            tr.Attributes["class"] = "alternate";
                        alternate = !alternate;

                        bool addRow = true;

                        for (int i = 0; i < Columns.Count; i++)
                        {
                            if (Columns[i] != null)
                            {
                                if (Columns[i].SkipRows == null || !Columns[i].SkipRows.Contains(j.ToString()))
                                {
                                    Control td = Columns[i].getField(Rows[j]);
                                    addRow = addRow && Columns[i].ShowOwnerRow;
                                    Columns[i].ShowOwnerRow = true;
                                    tr.Controls.Add(td);
                                }
                                else
                                {/*
                                    Control td = Columns[i].SkippedRowControl;
                                    addRow = addRow && Columns[i].ShowOwnerRow;
                                    Columns[i].ShowOwnerRow = true;
                                    tr.Controls.Add(td);*/

                                    HtmlGenericControl td = Columns[i].SkippedRowControl;
                                    addRow = addRow && Columns[i].ShowOwnerRow;
                                    Columns[i].ShowOwnerRow = true;
                                    tr.Controls.Add(td);
                                }
                            }
                        }
                        if (addRow)
                            table.Controls.Add(tr);
                }
                #region Paginazione
                /*if (Pagination && PageCount > 1)
                {
                    tr = new HtmlGenericControl("tr");

                    HtmlGenericControl td_pag = new HtmlGenericControl("td");

                    td_pag.InnerHtml = "Pagina Corrente : " + paginaCorrente;

                    tr.Controls.Add(td_pag);

                    td_pag = new HtmlGenericControl("td");
                    td_pag.InnerHtml = "";
                    for (int i = 1; i <= PageCount; i++)
                    {
                        if (i != 1)
                            td_pag.InnerHtml += "-";
                        if (i != paginaCorrente)
                            td_pag.InnerHtml += "<a href=\"javascript: setpage(" + i + ")\">" + i + "</a>";
                        else
                            td_pag.InnerHtml += i;
                    }

                    int colspan = Columns.Count - 1;
                    td_pag.Attributes["colspan"] = colspan.ToString();

                    tr.Controls.Add(td_pag);

                    table.Controls.Add(tr);

                }*/
                tr = new HtmlGenericControl("tr");
                HtmlGenericControl td_page = new HtmlGenericControl("td");

                NetUtility.PageMaker pager = new NetUtility.PageMaker(paginaCorrente, NetUtility.PageMaker.VariabileType.Form);
                pager.RPP = this.RecordPerPagina;
                pager.Key = PaginationKey;
                td_page = pager.PageNavigation("javascript: setFormValue({0},'"+this.PaginationKey+"',1)", Rows.Length);

                td_page.TagName = "td";
                td_page.Attributes["colspan"] = this.Columns.Count.ToString();
                td_page.Attributes["class"] += " FolderNavigationControls";

                tr.Controls.Add(td_page);
                table.Controls.Add(tr);
                #endregion
            }
            else
            {
                tr = new HtmlGenericControl("tr");

                table.Controls.Add(tr);

                HtmlGenericControl td = new HtmlGenericControl("td");
                td.Attributes["colspan"] = Columns.Count.ToString();
                td.InnerHtml = NoRecordMsg;
                tr.Controls.Add(td);

            }

            return div;
        }

        protected int getPageCount(int recordcount)
        {
            float fl_pc = (float)(recordcount - 1) / (float)RecordPerPagina;

            int pc = int.Parse(fl_pc.ToString().Split(',')[0].ToString());

            return pc + 1;
        }
    }
}