using System;
using System.Security.Cryptography;


    /// <summary>
    /// Numeri pseudo-casuali e cryptographically-strong
    /// </summary>
    public class RandomNumbers
    {
        private RNGCryptoServiceProvider _rand = new RNGCryptoServiceProvider();
        private byte[] _rd4 = new byte[4], _rd8 = new byte[8];

        /// <summary>
        /// Genera un numero intero positivo casuale minore di max
        /// </summary>
        /// <param name="max">Limite superiore del numero casuale generato</param>
        /// <returns>Il numero casuale generato</returns>
        public int Next(int max)
        {
            if (max <= 0) 
            { 
                Exception ex = new ArgumentOutOfRangeException("Max");
                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Warning, ex);*/
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, ex.Message, "", "", "");
            }
            _rand.GetBytes(_rd4);
            int val = BitConverter.ToInt32(_rd4, 0) % max;
            if (val < 0) val = -val;
            return val;
        }

        /// <summary>
        /// Genera un numero intero positivo casuale compreso tra min e max
        /// </summary>
        /// <param name="min">Valore minimo possibile</param>
        /// <param name="max">Valore massimo possibile</param>
        /// <returns>Il numero casuale generato</returns>
        public int Next(int min, int max)
        {
            if (min > max) { 
                Exception ex = new ArgumentOutOfRangeException();
                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Warning, ex);*/
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, ex.Message, "", "", "");
            }
            return Next(max - min + 1) + min;
        }

        /// <summary>
        /// Genera un numero (double) compreso tra 0.0 e 1.1
        /// </summary>
        /// <returns>Il numero casuale generato</returns>
        public double NextDouble()
        {
            _rand.GetBytes(_rd8);
            return BitConverter.ToUInt64(_rd8, 0) / (double)UInt64.MaxValue;
        }

    }

