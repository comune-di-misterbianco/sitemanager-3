using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace NetForms
{
    public class NetCaptcha : NetField
    {
        public const string CaptchaApplicationIndexerKey = "CaptchaApplicationIndexer";
        public Dictionary<string, string> Keys
        {
            get
            {
                if (_Keys == null)
                {
                    var indexer = HttpContext.Current.Session[NetCaptcha.CaptchaApplicationIndexerKey];
                    if (indexer != null)
                    {
                        _Keys = (Dictionary<string, string>)indexer;
                        if (_Keys.Count > 50)
                        {
                            _Keys.Clear();
                            _Keys = null;
                        }
                    }
                }
                if (_Keys == null)
                {
                    HttpContext.Current.Session[NetCaptcha.CaptchaApplicationIndexerKey] = _Keys = new Dictionary<string, string>();
                }
                return _Keys;
            }
        }
        private Dictionary<string, string> _Keys;
        
        public G2Core.XhtmlControls.InputField KeyHiddenControl
        {
            get
            {
                if (_KeyHiddenCOntrol == null)
                {
                    _KeyHiddenCOntrol = new G2Core.XhtmlControls.InputField("k");
                    _KeyHiddenCOntrol.Value = Key.ToString();
                }
                return _KeyHiddenCOntrol;
            }
        }
        private G2Core.XhtmlControls.InputField _KeyHiddenCOntrol;

        public G2Core.Common.RequestVariable CaptchaRequestVariabile
        {
            get
            {
                if (_RequestVariabile == null)
                {
                    _RequestVariabile = new G2Core.Common.RequestVariable("k", G2Core.Common.RequestVariable.RequestType.Form);
                }
                return _RequestVariabile;
            }
        }
        private G2Core.Common.RequestVariable _RequestVariabile;

        public string Key
        {
            get
            {
                if (_Key == null)
                {
                    G2Core.Common.RequestVariable req = new G2Core.Common.RequestVariable("k", G2Core.Common.RequestVariable.RequestType.Form);
                    _Key = CaptchaRequestVariabile.IsValidInteger ? CaptchaRequestVariabile.IntValue.ToString() : this.GetHashCode().ToString();
                }
                return _Key;
            }
        }
        private string _Key;

        public string CurrentValue
        {
            get;
            private set;
        }

        private bool _DebugMode;
        public bool DebugMode
        {
            get { return _DebugMode; }
            set { _DebugMode = value; }
        }	

        private System.Web.UI.WebControls.Image image = new System.Web.UI.WebControls.Image();
        private bool _IsPostBack;
        private bool IsPostBack
        {
            get
            {
                return _IsPostBack;
            }
        }

        public NetCaptcha(string label, string fieldname, bool ispostback)
            : base(label, fieldname)
        {
            //IsPostBack = ispostback;
            _IsPostBack = this.RequestVariable.IsPostBack;
            _DebugMode = false;
            CurrentValue = this.Keys.ContainsKey(this.Key) ? this.Keys[this.Key] : null;
            if(Keys.ContainsKey(Key))
                Keys.Remove(Key.ToString());
            Keys.Add(Key.ToString(), generaPw(5));
        }

        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl field = new HtmlGenericControl("fieldset");
            field.Attributes["class"] = "captcha";
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerHtml = "Codice di controllo";
            field.Controls.Add(legend);

            HtmlGenericControl msg = new HtmlGenericControl("p");
            msg.InnerHtml = "Digita i caratteri visualizzati nell'immagine sottostante.";

            //string key = !IsPostBack ? genimgkey() : this.RequestVariable.StringValue;

            image.ImageUrl = "image.cap?k=" + Key.ToString();
            image.Attributes["alt"] = "Immagine di controllo antispam";

            HtmlGenericControl p = new HtmlGenericControl("p");
            p.Attributes["class"] = "p-captcha";
            p.Controls.Add(image);

            TextBox codice = new TextBox();
            codice.ID = this.FieldName;

            field.Controls.Add(msg);
            field.Controls.Add(p);
            field.Controls.Add(codice);/**/
            field.Controls.Add(KeyHiddenControl.Control);

            return field;
        }

        public override string getFieldName()
        {
            return _FieldName;
        }

        public override string validateValue(string value)
        {
            return base.validateValue(value);
        }

        public override string validateInput(string input)
        {
            string errors = "";
            if (this.Required && input.Length == 0)
            {
                errors += "<li>";
                errors += "Il campo <strong>" + Label.Replace(":","") + "</strong> � obbligatorio";
                errors += "</li>";
            }

            if (string.IsNullOrEmpty(CurrentValue) || string.IsNullOrEmpty(input) || CurrentValue.ToLower() != input.ToLower())
                errors += "<li>Il <strong>Codice di controllo</strong> digitato non corrisponde a quello riprodotto nell'immagine!</li>";

            return errors;
        }


        public override string getFilter(string value)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        
        private string generaPw(int maxlen)
        {
            string strNewPass = null;
            int whatsNext;
            int upper;
            int lower;

            Random rnd = new Random();

            for (int intCounter = 1; intCounter <= maxlen; intCounter++)
            {
                whatsNext = rnd.Next(2);

                if (whatsNext == 0)
                {
                    upper = 90;
                    lower = 65;
                }
                else
                {
                    upper = 57;
                    lower = 49;
                }

                int k = 0;

                do
                {
                    k = (int)((upper - lower + 1) * rnd.NextDouble() + lower);
                }
                while (k == 79 || k == 73);

                string strA = new string((Char)k, 1);

                strNewPass = strNewPass + strA;

            }

            return strNewPass;
        }

        private string genimgkey()
        {
      
            string key = generaPw(5);
            //HttpRuntime.Cache.Insert(this.FieldName, key);
            HttpRuntime.Cache.Insert("key", key);
            return key;
        }

        public bool CheckUserInput()
        {
            if (this.validateInput(this.RequestVariable.StringValue).Length == 0)
                return true;
            _IsPostBack = false;
            return false;
        }
    }
}
