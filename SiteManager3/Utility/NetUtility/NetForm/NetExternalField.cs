using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Class1
/// </summary>
/// 
namespace NetForms
{
    public class NetExternalField
    {
        public string TableName
        {
            get { return _TableName; }
        }
        public string FieldName
        {
            get { return _FieldName; }
        }
        private string _FieldName;
        private string _Value;
        public virtual string Value
        {
            get
            {
                return _Value;
            }
            set
            {
                _Value = value;
            }
        }
        private string _TableName;

        public NetExternalField(string table, string fieldname)
        {
            //
            // TODO: Add constructor logic here
            //
            _TableName = table;
            _FieldName = fieldname;
        }

        public NetExternalField(string table, string fieldname, string value)
            : this(table, fieldname)
        {
            //
            // TODO: Add constructor logic here
            //
            Value = value;
        }


    }
}