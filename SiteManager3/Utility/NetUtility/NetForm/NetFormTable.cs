using System;
using System.Data;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI.HtmlControls;
using NetCms.Diagnostics;
using System.Web.UI;

/// <summary>
/// Summary description for NetFormTable
/// </summary>
namespace NetForms
{
    public class NetFormTable : NetAbstractFormTable
    {
        protected NetFieldsCollection _Fields;
        public NetFieldsCollection Fields
        {
            get
            {
                return _Fields;
            }
        }

        private string _TableLabel;
        public override string TableLabel
        {
            get { return _TableLabel; }
            set { _TableLabel = value; }
        }

        private string _TableName;
        public override string TableName
        {
            get
            {
                return _TableName;
            }
        }

        private string _CssClass = "";
        public string CssClass 
        {
            get { return _CssClass; }
            set { _CssClass = value; }
        }

        private string _PrimaryKey;
        public string PrimaryKey
        {
            get
            {
                return _PrimaryKey;
            }
        }

        private int _ID = 0;
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }

        private bool BindingsLoaded;

        protected int _LastInsert;
        public override int LastInsert
        {
            get
            {
                return _LastInsert;
            }
        }

        protected NetFormTable[] _InnerJoinTables;
        public NetFormTable[] InnerJoinTables 
        {
            get
            {
                return _InnerJoinTables;
            }            
        }

        public NetFormTable(string tableName, string primaryKey)
            : this(tableName, primaryKey, null, 0)
        {
        }
        public NetFormTable(string tableName, string primaryKey, int id)
            : this(tableName, primaryKey, null, id)
        {
            _ID = id;
        }
        public NetFormTable(string tableName, string primaryKey, NetCms.Connections.Connection conn)
            : this(tableName, primaryKey, conn,0)
        {
        }
        public NetFormTable(string tableName, string primaryKey, NetCms.Connections.Connection conn, int id)
            :base(conn)
        {
            _ID = id;
            _TableName = tableName;
            _PrimaryKey = primaryKey;
            _Fields = new NetFieldsCollection();
        }

        public void SetInnerJoinTables(NetFormTable[] InnerJoinTables)
        {
            _InnerJoinTables = InnerJoinTables;
        }

        public virtual void bindFields()
        {
            if (ID != 0 && !BindingsLoaded)
            {
                string sql = "";
                sql += "SELECT * FROM " + TableName;
                sql += " WHERE " + PrimaryKey + " = " + ID;

                DataTable table = Conn.SqlQuery(sql);
                foreach (DataRow row in table.Rows)
                {
                    foreach (NetField field in Fields)
                    {
                        if (table.Columns.Contains(field.getFieldName()) && field.ValueIsEmpty())
                            field.Value = row[field.getFieldName()].ToString();
                    }
                }

                BindingsLoaded = true;
            }
        }
        public virtual void bindFields(DataRow row)
        {
            if (row != null && !BindingsLoaded)
            {
                for (int i = 0; i < Fields.Count; i++)
                    if (Fields[i].Value == null || Fields[i].Value.Length == 0)
                        Fields[i].Value = row[Fields[i].getFieldName()].ToString();
                BindingsLoaded = true;
            }
        }
        public virtual void bindFields(DataTable table)
        {
            if (table != null && !BindingsLoaded)
            {
                DataRow[] row = table.Select(this.PrimaryKey + " = " + ID);
                if(row.Length>0)
                    bindFields(row[0]);
                BindingsLoaded = true;
            }
        }
        public override void DataBind(NameValueCollection nameValueCollection)
        {
            if (!BindingsLoaded)
            {
                foreach (NetField field in this.Fields)
                {
                    if (nameValueCollection[field.FieldName] != null && field.Value == null)
                        field.Value = nameValueCollection[field.FieldName];
                }

                BindingsLoaded = true;
            }
        }
        public virtual void addField(NetField field)
        {
            Fields.Add(field);
        }
        public string getTableName()
        {
            return TableName;
        }

        public HtmlGenericControl getForm(params NetForm[] parentForm)
        {
            bindFields();
            
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "netformtable ";

            if (!string.IsNullOrEmpty(this.InfoText))
            div.Controls.Add(new LiteralControl("<h2 class=\"netform-title\"><i class=\"far fa-caret-square-right pr-1\"></i>" + this.InfoText+"</h2>"));
            
            if (!string.IsNullOrEmpty(CssClass))
                div.Attributes["class"] += CssClass;


            HtmlGenericControl innerContent = new HtmlGenericControl("div");
            innerContent.Attributes["class"] = "innerContent";

            innerContent.Controls.Add(this.PostbackCheckField.Control);
            for (int i = 0; i < Fields.Count; i++)
            {
                NetField field = Fields[i];
                if (field.RenderField)
                    innerContent.Controls.Add(field.getControl());
            }

            if (!IsPostBack && parentForm.Length==0)
                Diagnostics.TraceMessage(TraceLevel.Verbose, "Accesso alla tabella " + this.TableName, "", "","");

            div.Controls.Add(innerContent);

            return div;
        }
        public override HtmlGenericControl GetControl(params NetForm[] parentForm)
        {
            return getForm(parentForm);
        }
        public override HtmlGenericControl GetOverviewControl()
        {
            HtmlGenericControls.Fieldset control = new HtmlGenericControls.Fieldset(this.TableLabel);
            control.Class = "OverviewFieldset";
            
            HtmlGenericControl list = new HtmlGenericControl("ul");
            control.Controls.Add(list);

            foreach (NetField field in this.Fields)
                list.Controls.Add(field.GetOverviewControl());

            return control;
        }
        public override string ValidateInput(params NetForm[] parentForm)
        {
            return validateForm(parentForm);
        }
        public string validateForm(params NetForm[] parentForm)
        {
            return validateForm(HttpContext.Current.Request.Form, parentForm);
        }
        public string validateForm(NameValueCollection values, params NetForm[] parentForm)
        {
            string errors = "";
            string input = "";
            for (int i = 0; i < Fields.Count; i++)
            {
                NetField field = Fields[i];
                input = "";
                if (values.Get(field.getFieldName()) != null)
                    input = values.Get(field.getFieldName()).ToString();

                string resultFieldValidation;

                errors += (resultFieldValidation = field.validateInput(input));

                if (resultFieldValidation != String.Empty)
                {
                    if (field.PostBackValue != "")
                        field.FieldStringForLog = "L'utente ha inserito nel campo " + field.Label.Replace(":","") + " della tabella " + this.TableName + " il valore: " + field.PostBackValue;
                    else field.FieldStringForLog = "L'utente non ha valorizzato il campo " + field.Label.Replace(":", "") + " della tabella " + this.TableName;

                }

                if (field.FieldStringForLog != String.Empty && parentForm.Length != 0)
                    this.TableStringsForLog.Add(field.FieldStringForLog);

                if (parentForm.Length == 0 && field.FieldStringForLog != String.Empty)
                {
                    Diagnostics.TraceMessage(TraceLevel.Warning, field.FieldStringForLog, "", "","");
                }

            }
            if (errors.Length > 0)
                return errors;
            else
                return null;
        }
        public override bool Insert(NameValueCollection PostData)
        {
            insertQuery(PostData);
            return true;
        }
        public override bool Insert()
        {
            insertQuery(HttpContext.Current.Request.Form);
            return true;
        }
        
        public override bool Update(int RecordID)
        {
            this.ID = RecordID;
            updateQuery(HttpContext.Current.Request.Form);
            return true;
        }
        public override bool Update()
        {
            updateQuery(HttpContext.Current.Request.Form);            
            return true;
        }
        public override bool Update(NameValueCollection PostData)
        {
            updateQuery(PostData);
            return true;
        }
        public override bool Update(int RecordID, NameValueCollection PostData)
        {
            this.ID = RecordID;
            updateQuery(PostData);
            return true;
        }

        public string insertQuery()
        {
            return insertQuery(HttpContext.Current.Request.Form);
        }
        public virtual string insertQuery(NameValueCollection post)
        {
            string insertSQLQuery = "INSERT INTO {0} " + " ( {1} ) " + " VALUES( {2} ) ";

            string valid = validateForm(post);

            if (valid != null)
            {
                return "<ul>" + valid + "</ul>";
            }

            string values = "";
            string where = "";
            string fields = "";
            bool first = true;

            for (int i = 0; i < Fields.Count; i++)
            {
                NetField field = Fields[i];
                if (!field.JumpField)
                {
                    if (first) first = false;
                    else
                    {
                        values += ",";
                        fields += ",";
                        where += " AND ";
                    }
                    string value = "";
                    if (post.Get(field.getFieldName()) != null)
                    {
                        value = post.Get(field.getFieldName()).ToString();

                    }

                    value = field.validateValue(value);

                    values += value;
                    fields += field.getFieldName();
                    where += field.getFieldName() + " = " + value + "";
                }

            }
            for (int i = 0; i < ExternalFields.Count; i++)
            {
                string virgo = values.Length > 0 ? "," : "";
                values += virgo + ExternalFields[i].Value;
                fields += virgo + ExternalFields[i].FieldName;
            }

            string sql = "";

            sql += string.Format(insertSQLQuery, TableName, fields, values);

            if (Conn != null)
            {
                _LastInsert = Conn.ExecuteInsert(sql);                
            }

            #region CHILDREN TABLE for INNER JOIN
            if (InnerJoinTables != null && InnerJoinTables.Length > 0)
            {                
                foreach (var table in InnerJoinTables)
                {
                    string childrenvalues = "";
                    string childrenwhere = "";
                    string childrenfields = "";

                    first = true;

                    foreach (NetField field in table.Fields)
                    {
                        if (!field.JumpField)
                        {
                            if (first) first = false;
                            else
                            {
                                childrenvalues += ",";
                                childrenfields += ",";
                                childrenwhere += " AND ";
                            }
                            string value = "";
                            if (!string.IsNullOrEmpty(post.Get(field.getFieldName())))
                                value = post.Get(field.getFieldName()).ToString();
                            
                            /// Set external foreign key
                            /// 
                            else if (field.getFieldName().Equals(table.PrimaryKey))
                            {
                                for (int j = 0; j < this.ExternalFields.Count; j++)
                                {
                                    NetExternalField extfield = this.ExternalFields[j];
                                    if (extfield.FieldName.Equals(this.PrimaryKey))
                                    {
                                        value = extfield.Value;
                                        j = this.ExternalFields.Count - 1;
                                    }
                                }
                            }
                            value = field.validateValue(value);
                            childrenvalues += value;
                            childrenfields += field.getFieldName();
                            childrenwhere += field.getFieldName() + " = " + value + "";
                        }
                    }
                    for (int i = 0; i < table.ExternalFields.Count; i++)
                    {
                        string virgo = childrenvalues.Length > 0 ? "," : "";
                        childrenvalues += virgo + ExternalFields[i].Value;
                        childrenfields += virgo + ExternalFields[i].FieldName;
                    }

                    string childrensql = "";
                    childrensql += string.Format(insertSQLQuery, table.TableName, childrenfields, childrenvalues);
                    
                    if (Conn != null)
                        _LastInsert = Conn.ExecuteInsert(childrensql);
                }
            }
            #endregion

            return sql;
        }

        public string updateQuery()
        {
            return updateQuery(HttpContext.Current.Request.Form);
        }
       
        public virtual string updateQuery(NameValueCollection post)
        {
            string updateSQLQuery = "UPDATE {0} SET " + " {1} " + " WHERE {2} = {3}";

            if (ID == 0)
                return "ID del record non impostato";

            string valid = validateForm(post);

            if (valid != null)
                return "<ul>" + valid + "</ul>";

            string values = "";

            foreach (NetField field in this.Fields)
            {
                if (!field.JumpField)
                {
                    if (values.Length>0) values += ",";
                    
                    string value = "";

                    if (post.Get(field.FieldName) != null)
                        value = post.Get(field.FieldName).ToString();

                    value = field.validateValue(value);
                    values += field.FieldName + " = " + value;
                }
            }

            string sql = "";

            sql += string.Format(updateSQLQuery, TableName, values, PrimaryKey, ID.ToString());

            if(Conn!=null && values.Length>0)
                Conn.Execute(sql);

            #region CHILDREN TABLE for INNER JOIN
            if (InnerJoinTables != null && InnerJoinTables.Length > 0)
            {
                foreach (var table in InnerJoinTables)
                {
                    string childrenvalues = "";
                    foreach (NetField field in table.Fields)
                    {
                        if (!field.JumpField)
                        {
                            if (childrenvalues.Length > 0) childrenvalues += ",";
                            string value = "";
                            if (post.Get(field.FieldName) != null)
                                value = post.Get(field.FieldName).ToString();
                            value = field.validateValue(value);
                            childrenvalues += field.FieldName + " = " + value;
                        }
                    }
                    string childrensql = "";
                    childrensql += string.Format(updateSQLQuery, table.TableName, childrenvalues, table.PrimaryKey, ID.ToString());
                    if (Conn != null && childrenvalues.Length > 0)
                        Conn.Execute(childrensql);
                }                
            }
            #endregion

            return sql;
        }

        public void SetLabel(string ControlID, string NewLabel)
        {
            Fields[ControlID].Label = NewLabel;
        }

    }
}