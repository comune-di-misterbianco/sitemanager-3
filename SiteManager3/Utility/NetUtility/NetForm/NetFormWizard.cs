using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms.Custom;

/// <summary>
/// Summary description for NetFormAdvance
/// </summary>
namespace NetForms
{
    public class NetFormWizard
    {
        private NetAbstractFormTableCollection _Tables;
        public NetAbstractFormTableCollection Tables
        {
            get
            {
                return _Tables;
            }
        }
        
        private NetAbstractFormTableCollection _ExecuteOrder;
        public NetAbstractFormTableCollection ExecuteOrder
        {
            get
            {
                return _ExecuteOrder;
            }
            set
            {
                _ExecuteOrder = value;
            }
        }

        private NetAbstractFormTable _LastSelectedTable;
        private NetAbstractFormTable LastSelectedTable
        {
            get
            {
                if (_LastSelectedTable == null)
                {
                    int TbIndex;

                    if (int.TryParse(StepField.Value, out TbIndex))
                    {
                        if (TbIndex > 0&& TbIndex<=this.Tables.Count)
                            _LastSelectedTable = this.Tables[TbIndex-1];
                    }
                }

                return _LastSelectedTable;
            }
        }


        public enum LastOperationValues
        {
            NotYetExecute = 0,
            OK = 1,
            Error = 2
        }

        private LastOperationValues _LastOperation;
        public LastOperationValues LastOperation
        {
            get { return _LastOperation; }
        }

        protected NetFormLabels NLabels
        {
            get
            {
                return NetUtility.LabelsCacher.NetFormLabels;
            }
        }

        private bool _AddCapcha;
        public bool AddCapcha
        {
            get { return _AddCapcha; }
            set { _AddCapcha = value; }
        }

        private string _CaptchaLabel;
        public string CaptchaLabel
        {
            get { return _CaptchaLabel; }
            set { _CaptchaLabel = value; }
        }

        private NetCaptcha _CaptchaControl;
        private NetCaptcha CaptchaControl
        {
            get
            {
                if (_CaptchaControl == null)
                    _CaptchaControl = new NetCaptcha(this.CaptchaLabel, "captcha",true);
                return _CaptchaControl;
            }
        }

        private bool _AddDisclaimer;
        public bool AddDisclaimer
        {
            get { return _AddDisclaimer; }
            set { _AddDisclaimer = value; }
        }

        private DisclaimerControl _DisclaimerControl;
        public DisclaimerControl DisclaimerControl
        {
            get { if (_DisclaimerControl == null)_DisclaimerControl = new DisclaimerControl(); return _DisclaimerControl; }
        }
	
        private HtmlGenericControls.InputField _StepField;
        private HtmlGenericControls.InputField StepField
        {
            get
            {
                if (_StepField == null)
                {
                    _StepField = new HtmlGenericControls.InputField("CurentActiveStep");
                    if(_StepField.RequestVariable.IsValidInteger)
                        _StepField.Value = (_StepField.RequestVariable.IntValue).ToString();
                    else
                        _StepField.Value = "1";

                }
                return _StepField;
            }
        }

        private HtmlGenericControls.InputField _FormIDField;
        private HtmlGenericControls.InputField FormIDField
        {
            get
            {
                if (_FormIDField == null)
                {
                    
                    _FormIDField = new HtmlGenericControls.InputField("ActiveFormID");
                    if (_FormIDField.RequestVariable.IsValidString && _FormIDField.RequestVariable.StringValue != string.Empty)
                        _FormIDField.Value = _FormIDField.RequestVariable.StringValue;
                    else
                    {
                        _FormIDField.Value = "FormWizard_" + this.GetHashCode();
                        HttpContext.Current.Session["FormWizard_" + this.GetHashCode()] = true;
                    }

                }
                return _FormIDField;
            }
        }

        private int _CurrentStep=-1;
        public int CurrentStep
        {
            get 
            {
                if (_CurrentStep == -1)
                {
                    switch (LastButtonClick)
                    {
                        case ButtonTypes.Next:
                            _CurrentStep = int.Parse(this.StepField.Value);
                            if (LastPostbackValidationOk)
                                _CurrentStep++;
                            break;
                        case ButtonTypes.Refresh:
                            _CurrentStep = int.Parse(this.StepField.Value);
                            break;

                        case ButtonTypes.Back:
                            _CurrentStep = int.Parse(this.StepField.Value) - 1;
                            break;

                        case ButtonTypes.End:
                            _CurrentStep = int.Parse(this.StepField.Value);
                            if (LastPostbackValidationOk && (!this.AddDisclaimer || this.DisclaimerControl.CheckAcceptance()))
                                _CurrentStep++;
                            break;

                        default:
                            _CurrentStep = 1;
                            break;
                    }
                    StepField.Value = _CurrentStep.ToString();

                }
                return _CurrentStep; 
            }
        }
        private bool LastPostbackValidationOk
        {
            get
            {
                return LastPostbackValidation == string.Empty;
            }
        }

        private string _LastPostbackValidation;
        private string LastPostbackValidation
        {
            get
            {
                if (_LastPostbackValidation == null)
                if ((this.LastButtonClick == ButtonTypes.Next || this.LastButtonClick == ButtonTypes.End) && this.LastSelectedTable != null)
                    {
                        _LastPostbackValidation = string.Empty;
                        _LastPostbackValidation += this.LastSelectedTable.ValidateInput();

                        if (this.LastButtonClick == ButtonTypes.Next
                            && this.AddCapcha && this.CaptchaControl.RequestVariable.IsPostBack)
                            _LastPostbackValidation += this.CaptchaControl.validateInput(this.CaptchaControl.RequestVariable.StringValue);

                    }
                return _LastPostbackValidation;
            }
        }

        private HtmlGenericControl _EndWizardControl;
        public HtmlGenericControl EndWizardControl
        {
            get
            {
                if (_EndWizardControl == null)
                {
                    HtmlGenericControls.Fieldset fine = new HtmlGenericControls.Fieldset(this.NLabels[NetFormLabels.NetFormLabelsList.ProceduraCompletataConSuccesso], new HtmlGenericControl("div"));
                    fine.Content.InnerHtml = NLabels[NetFormLabels.NetFormLabelsList.MessaggioProceduraCompletata];
                    _EndWizardControl = fine;
                    fine.Class = "EndFieldset";
                }
                return _EndWizardControl;
            }
            set { _EndWizardControl = value; }
        }

        #region Buttons

        private Button _NextButton;
        private Button NextButton
        {
            get
            {
                if (_NextButton == null)
                {
                    _NextButton = new Button();
                    _NextButton.Text = this.NLabels[NetFormLabels.NetFormLabelsList.Avanti];
                    _NextButton.ID = "NextButton";
                    _NextButton.CssClass = "button";
                }
                return _NextButton;
            }
        }

        private Button _EndButton;
        private Button EndButton
        {
            get
            {
                if (_EndButton == null)
                {
                    _EndButton = new Button();
                    _EndButton.Text = this.NLabels[NetFormLabels.NetFormLabelsList.Completa];
                    _EndButton.ID = "EndButton";
                    _EndButton.CssClass = "button";
                }
                return _EndButton;
            }
        }

        private Button _BackButton;
        private Button BackButton
        {
            get
            {
                if (_BackButton == null)
                {
                    _BackButton = new Button();
                    _BackButton.Text = this.NLabels[NetFormLabels.NetFormLabelsList.Indietro];
                    _BackButton.ID = "BackButton";
                    _BackButton.CssClass = "button";
                }
                return _BackButton;
            }
        }

        private System.Web.SessionState.HttpSessionState Session
        {
            get
            {
                return
                HttpContext.Current.Session;
            }
        }

        #endregion

        public enum ButtonTypes
        {
            NotChecked,None,Next,Back,End,Refresh
        }
        public ButtonTypes _LastButtonClick;
        public ButtonTypes LastButtonClick
        {
            get
            {
                if (_LastButtonClick == ButtonTypes.NotChecked)
                {
                    _LastButtonClick = ButtonTypes.None;

                    if (_LastButtonClick == ButtonTypes.None)
                    {
                        NetUtility.RequestVariable NextButtonRequest = new NetUtility.RequestVariable(NextButton.ID);
                        if (NextButtonRequest.IsValidString && NextButton.Text == NextButtonRequest.StringValue)
                            _LastButtonClick = ButtonTypes.Next;
                    }
                    if (_LastButtonClick == ButtonTypes.None)
                    {
                        NetUtility.RequestVariable BackButtonRequest = new NetUtility.RequestVariable(BackButton.ID);
                        if (BackButtonRequest.IsValidString && BackButton.Text == BackButtonRequest.StringValue)
                            _LastButtonClick = ButtonTypes.Back;
                    }
                    if (_LastButtonClick == ButtonTypes.None)
                    {
                        NetUtility.RequestVariable EndButtonRequest = new NetUtility.RequestVariable(EndButton.ID);
                        if (EndButtonRequest.IsValidString && EndButton.Text == EndButtonRequest.StringValue)
                            _LastButtonClick = ButtonTypes.End;
                    }
                    if (_LastButtonClick == ButtonTypes.None)
                    {
                        NetUtility.RequestVariable RefreshButtonRequest = new NetUtility.RequestVariable("RefreshButton");
                        if (RefreshButtonRequest.IsValidInteger)
                            _LastButtonClick = ButtonTypes.Refresh;
                    }
                }
                return _LastButtonClick;
            }
        }

        private string _ErrorsFieldsetLabel;
        public string ErrorsFieldsetLabel
        {
            get {
                if (_ErrorsFieldsetLabel == null)
                    _ErrorsFieldsetLabel = "Sono stati riscontrati i seguenti errori:";
                return _ErrorsFieldsetLabel; 
            }
            set { _ErrorsFieldsetLabel = value; }
        }
	

        public NetFormWizard()
        {
            _Tables = _ExecuteOrder = new NetAbstractFormTableCollection();
            _CaptchaLabel = "Immagine di Sicurezza";
            CheckFormValidity();
        }
        public HtmlGenericControl GetInsertView()
        {
            return getForms(true);
        }
        public HtmlGenericControl GetUpdateView()
        {
            return getForms(false);
        }
        private HtmlGenericControl getForms(bool InsertView)
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            if ((this.LastButtonClick == ButtonTypes.Next || this.LastButtonClick == ButtonTypes.Refresh) && this.LastPostbackValidationOk && LastSelectedTable != null)
            {
                this.Session[FormIDField.Value + "_" + this.LastSelectedTable.TableName] = HttpContext.Current.Request.Form;
            }
            if (this.LastButtonClick != ButtonTypes.End)
                div.Controls.Add(ProgressControl());

            if (this.CurrentStep > 0 && this.LastButtonClick != ButtonTypes.End)
            {
                if ((this.LastButtonClick == ButtonTypes.Next || this.LastButtonClick == ButtonTypes.End) && !this.LastPostbackValidationOk)
                {
                    div.Controls.Add(ErrorsControl);
                }
                if ((this.LastButtonClick == ButtonTypes.Next || this.LastButtonClick == ButtonTypes.Refresh) && this.CurrentStep > this.Tables.Count && this.LastPostbackValidationOk)
                {
                    HtmlGenericControls.Fieldset riepilogo = new HtmlGenericControls.Fieldset(this.NLabels[NetFormLabels.NetFormLabelsList.CompletamentoProcedura],new HtmlGenericControls.Div());
                    riepilogo.Content.InnerHtml = this.NLabels[NetFormLabels.NetFormLabelsList.RiepilogoDatiInseriti];
                    riepilogo.Class = "OverviewFieldset";
                    div.Controls.Add(riepilogo);
                    div.Controls.Add(OverviewControl());                    
                }
                else
                {
                    NetAbstractFormTable table = Tables[this.CurrentStep - 1];

                    if (this.LastButtonClick != ButtonTypes.None &&
                        this.Session[FormIDField.Value + "_" + table.TableName] != null
                        )
                    {
                        NameValueCollection PostData = (NameValueCollection)this.Session[FormIDField.Value + "_" + table.TableName];
                        table.DataBind(PostData);
                    }
                    
                    div.Controls.Add(table.GetControl());
                }
            }

            if (this.LastButtonClick == ButtonTypes.End)
            {
                if (!this.AddDisclaimer || this.DisclaimerControl.CheckAcceptance())
                {
                    div.Controls.Add(this.EndWizardControl);
                    
                    if (InsertView)
                        this.Insert();
                    else
                        this.Update();
                }
                else
                {
                    HtmlGenericControls.Fieldset riepilogo = new HtmlGenericControls.Fieldset(this.NLabels[NetFormLabels.NetFormLabelsList.CompletamentoProcedura], new HtmlGenericControls.Div());
                    riepilogo.Content.InnerHtml = this.NLabels[NetFormLabels.NetFormLabelsList.RiepilogoDatiInseriti];
                    riepilogo.Class = "OverviewFieldset";
                    this._LastPostbackValidation += "<li>" + this.NLabels[NetFormLabels.NetFormLabelsList.NecessarioAccettareCondizione] + "</li>";
                    div.Controls.Add(ProgressControl());                    
                    div.Controls.Add(ErrorsControl);
                    div.Controls.Add(riepilogo);
                    div.Controls.Add(OverviewControl());

                    HtmlGenericControls.Par p = new HtmlGenericControls.Par();

                    p.Controls.Add(this.StepField.Control);
                    p.Controls.Add(this.FormIDField.Control);

                    div.Controls.Add(p);
                    p.Controls.Add(BackButton);
                    p.Controls.Add(EndButton);
                }
            }
            else
            {
                HtmlGenericControls.Par p = new HtmlGenericControls.Par();
                p.Class = "StepsButtons";
                div.Controls.Add(p);

                p.Controls.Add(this.StepField.Control);
                p.Controls.Add(this.FormIDField.Control);

                if (this.AddCapcha && this.CurrentStep == 1)
                    p.Controls.Add(this.CaptchaControl.getControl());

                if (this.CurrentStep > 1)
                    p.Controls.Add(BackButton);

                if (this.CurrentStep <= this.Tables.Count)
                    p.Controls.Add(NextButton);

                if (this.CurrentStep > this.Tables.Count)
                    p.Controls.Add(EndButton);
            }

            return div;
        }

        private HtmlGenericControl ErrorsControl
        {
            get
            {
                HtmlGenericControls.Div Div = new HtmlGenericControls.Div();
                HtmlGenericControls.Fieldset errori = new HtmlGenericControls.Fieldset(ErrorsFieldsetLabel);
                errori.Class = "ErrorsFieldset";

                HtmlGenericControls.Par p = new HtmlGenericControls.Par();
                p.InnerHtml = "<ul>" + this.LastPostbackValidation + "</ul>";
                errori.Controls.Add(p);

                Div.Controls.Add(errori);

                return Div;
            }
        }

        private HtmlGenericControl ProgressControl()
        {
            HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset(this.NLabels[NetFormLabels.NetFormLabelsList.StatoAvanzamentoProcedura]);
            set.Class = "ProgressFieldset";
            
            HtmlGenericControl info = new HtmlGenericControl("strong");
            info.InnerHtml = string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.TestoAvanzamentoProcedura], this.CurrentStep, Tables.Count+1);

            HtmlGenericControl list = new HtmlGenericControl("ol");
            HtmlGenericControl element;
            for (int i=0;i<this.Tables.Count;i++)
            {
                NetAbstractFormTable table = this.Tables[i];
                element = new HtmlGenericControl("li");
                if (i == this.CurrentStep-1)
                    element.InnerHtml = "<strong>" + table.TableLabel + "</strong>";
                else
                    element.InnerHtml = table.TableLabel;
                list.Controls.Add(element);
            }

            element = new HtmlGenericControl("li");
            if (Tables.Count == this.CurrentStep - 1)
                element.InnerHtml = "<strong>" + this.NLabels[NetFormLabels.NetFormLabelsList.CompletamentoProcedura] + "</strong>";
            else
                element.InnerHtml = this.NLabels[NetFormLabels.NetFormLabelsList.CompletamentoProcedura];
            list.Controls.Add(element);

            set.Controls.Add(info);
            set.Controls.Add(list);

            return set;
        }

        private HtmlGenericControl OverviewControl()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");

            foreach (NetAbstractFormTable table in this.Tables)
            {
                if (this.Session[FormIDField.Value + "_" + table.TableName] != null)
                {
                    NameValueCollection PostData = (NameValueCollection)this.Session[FormIDField.Value + "_" + table.TableName];
                    table.DataBind(PostData);
                }

                div.Controls.Add(table.GetOverviewControl());
            }

            if (this.AddDisclaimer && this.DisclaimerControl!=null)
            {
                div.Controls.Add(this.DisclaimerControl.GetControl);
            }

            return div;
        }

        private void CheckFormValidity()
        {
            if (this.FormIDField.RequestVariable.IsValidString && HttpContext.Current.Session[this.FormIDField.RequestVariable.StringValue] == null)
            {
                _LastButtonClick = ButtonTypes.None;
                _FormIDField = new HtmlGenericControls.InputField("ActiveFormID");
                _FormIDField.Value = "FormWizard_" + this.GetHashCode();
                HttpContext.Current.Session["FormWizard_" + this.GetHashCode()] = true;
            }
        }

        private void Update()
        {
            bool esito = true;
            string output = string.Empty;

            foreach (NetAbstractFormTable table in this.Tables)
            {
                if (esito)
                {
                    if (this.Session[FormIDField.Value + "_" + table.TableName] != null)
                    {
                        NameValueCollection PostData = (NameValueCollection)this.Session[FormIDField.Value + "_" + table.TableName];
                        esito &= table.Update(PostData);
                    }
                    else esito = false;
                }
            }
            if (esito)
            {
                HttpContext.Current.Session[this.FormIDField.RequestVariable.StringValue] = null;
                this._LastOperation = LastOperationValues.OK;
            }
            else
                this._LastOperation = LastOperationValues.Error;
        }
        private void Insert()
        {
            bool esito = true;
            string output = string.Empty;

            foreach (NetAbstractFormTable table in ExecuteOrder)
            {
                if (esito)
                {
                    if (this.Session[FormIDField.Value + "_" + table.TableName] != null)
                    {
                        NameValueCollection PostData = (NameValueCollection)this.Session[FormIDField.Value + "_" + table.TableName];

                        for (int j = 0; j < table.ExternalFields.Count; j++)
                        {
                            NetExternalField field = table.ExternalFields[j];

                            if (Tables[field.TableName] != null)
                                if (Tables[field.TableName].LastInsert != 0)
                                    field.Value = Tables[field.TableName].LastInsert.ToString();
                        }

                        esito &= table.Insert(PostData);
                    }
                    else esito = false;
                }
            }
            if (esito){
                HttpContext.Current.Session[this.FormIDField.RequestVariable.StringValue] = null;this._LastOperation = LastOperationValues.OK;
            }
            else
                this._LastOperation = LastOperationValues.Error;
            
        }
        
    }

    public class DisclaimerControl
    {
        private string  _Title;
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _Subtitle;
        public string Subtitle
        {
            get { return _Subtitle; }
            set { _Subtitle = value; }
        }

        private string _Infotext;
        public string Infotext
        {
            get { return _Infotext; }
            set { _Infotext = value; }
        }
        
        private string _ErrorText;
        public string ErrorText
        {
            get { return _ErrorText; }
            set { _ErrorText = value; }
        }

        private string _LabelAcceptControl;
        public string LabelAcceptControl
        {
            get { return _LabelAcceptControl; }
            set { _LabelAcceptControl = value; }
        }

        private string _LabelAccept;
        public string LabelAccept
        {
            get { return _LabelAccept; }
            set { _LabelAccept = value; }
        }

        private string _LabelDeny;
        public string LabelDeny
        {
            get { return _LabelDeny; }
            set { _LabelDeny = value; }
        }

        private string _AcceptControID;
        public string AcceptControID
        {
            get { return _AcceptControID; }
        }

        public DisclaimerControl()
        {
            _Infotext = "";
            _LabelAccept = "Si";
            _LabelDeny = "No";
            _Subtitle = "Informativa e consenso - DLG 196/03";
            _LabelAcceptControl = "Accetti le condizioni di utilizzo?";
            _Title = "Condizioni d'uso e privacy";
            _AcceptControID = "accettazione";
            _ErrorText = "E' necessario accettare le condizione d'uso.";
        }

        public bool CheckAcceptance()
        {
            NetUtility.RequestVariable request = new NetUtility.RequestVariable(this.AcceptControID);
            return request.IsValidInteger && request.IntValue == 1;
        }

        public HtmlGenericControl GetControl
        {
            get
            {
                HtmlGenericControls.Div content = new HtmlGenericControls.Div();
                HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset(this.Title,content);
                set.Class = "DisclaimerFieldset";

                TextBox infotextbox = new TextBox();
                infotextbox.Text = this.Infotext;
                infotextbox.Rows = 10;
                infotextbox.Columns = 70;
                infotextbox.TextMode = TextBoxMode.MultiLine;

                Label label = new Label();
                label.Text = this.Subtitle;
                label.AssociatedControlID = infotextbox.ID;

                content.Controls.Add(label);
                content.Controls.Add(infotextbox);

                HtmlGenericControl accettazione = new HtmlGenericControl("p");

                accettazione.InnerHtml = "<label>" + this.LabelAcceptControl + "</label>"
                + " <label for=\"" + this.AcceptControID + "1\">" + this.LabelAccept + "</label> <input id=\"" + this.AcceptControID + "1\" name=\"" + this.AcceptControID + "\" type=\"radio\" value=\"1\" />"
                + " <label for=\"" + this.AcceptControID + "2\">" + this.LabelDeny + "</label> <input id=\"" + this.AcceptControID + "2\" name=\"" + this.AcceptControID + "\" type=\"radio\" value=\"0\" checked=\"checked\" />";

                content.Controls.Add(accettazione);

                return set;
            }
        }
        public HtmlGenericControl GetControlNoFieldset
        {
            get
            {
                HtmlGenericControls.Div content = new HtmlGenericControls.Div();
                content.Class = "DisclaimerBlock";

                TextBox infotextbox = new TextBox();
                infotextbox.Text = this.Infotext;
                infotextbox.Rows = 10;
                infotextbox.Columns = 70;
                infotextbox.TextMode = TextBoxMode.MultiLine;

                Label label = new Label();
                label.Text = this.Subtitle;
                label.AssociatedControlID = infotextbox.ID;

                content.Controls.Add(label);
                content.Controls.Add(infotextbox);

                HtmlGenericControl accettazione = new HtmlGenericControl("p");

                accettazione.InnerHtml = "<label>" + this.LabelAcceptControl + "</label>"
                + " <label for=\"" + this.AcceptControID + "1\">" + this.LabelAccept + "</label> <input id=\"" + this.AcceptControID + "1\" name=\"" + this.AcceptControID + "\" type=\"radio\" value=\"1\" />"
                + " <label for=\"" + this.AcceptControID + "2\">" + this.LabelDeny + "</label> <input id=\"" + this.AcceptControID + "2\" name=\"" + this.AcceptControID + "\" type=\"radio\" value=\"0\" checked=\"checked\" />";

                content.Controls.Add(accettazione);

                return content;
            }
        }
    }
}