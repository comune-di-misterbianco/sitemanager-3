﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI.WebControls;

namespace NetForms
{
    public class NetWizard : Panel
    {
        public bool showProgressTitle=true;
        public bool showTotalSteps=true;
        public bool HideBackButton;
        public bool stopwizard;

        public bool UsedOnFrontend
        {
            get
            {
                return _UsedOnFrontend;
            }
            set
            {
                _UsedOnFrontend = value;
            }
        }
        private bool _UsedOnFrontend = false;

        System.Web.UI.LiteralControl steptitle;
        System.Web.UI.LiteralControl stepmessage;
        System.Web.UI.LiteralControl stepWarning;

        Label header;
        public string Header;

        private string _HeaderCssClass;
        public string HeaderCssClass
        {
            get
            {
                if (_HeaderCssClass == null)
                    _HeaderCssClass = "WzTitle";
                return _HeaderCssClass;
            }
            set { _HeaderCssClass = value; }
        }	


        private Panel _stepsContainer;
        public Panel stepsContainer
        {
            get
            {
                if (_stepsContainer == null)
                {
                    _stepsContainer = new Panel();
                    _stepsContainer.ID = "StepContainer";
                }
                return (_stepsContainer);
            }
            set
            {
                _stepsContainer = value;
            }
        }

        private NetWizardStepsCollection _NetWizardSteps;
        public NetWizardStepsCollection NetWizardSteps
        {
            get
            {
                if (_NetWizardSteps == null)
                    _NetWizardSteps = new NetWizardStepsCollection(this);
                return _NetWizardSteps;
            }
        }

        public bool CancelWizardRequest;

        private NetWizardStep _CompleteStep;
        public NetWizardStep CompleteStep
        {
            get
            {
                if (_CompleteStep == null)
                    _CompleteStep = new NetWizardStep();
                return (_CompleteStep);
            }
            set
            {
                _CompleteStep = value;
            }
        }

        #region Buttons
        public enum ButtonTypes
        {
            NotChecked, None, Next, Back, End
        }

        private ButtonTypes _LastButtonClick;
        public ButtonTypes LastButtonClick
        {
            get
            {
                if (_LastButtonClick == ButtonTypes.NotChecked)
                {
                    _LastButtonClick = ButtonTypes.None;
                    G2Core.Common.RequestVariable sb;
                    sb = new G2Core.Common.RequestVariable(NextButton.ID, G2Core.Common.RequestVariable.RequestType.Form);
                    if (sb.IsValid())
                    {
                        _LastButtonClick = ButtonTypes.Next;
                        return (_LastButtonClick);
                    }
                    sb = new G2Core.Common.RequestVariable(BackButton.ID, G2Core.Common.RequestVariable.RequestType.Form);
                    if (sb.IsValid())
                    {
                        _LastButtonClick = ButtonTypes.Back;
                        return (_LastButtonClick);
                    }
                    sb = new G2Core.Common.RequestVariable(EndButton.ID, G2Core.Common.RequestVariable.RequestType.Form);
                    if (sb.IsValid())
                    {
                        _LastButtonClick = ButtonTypes.End;
                        return (_LastButtonClick);
                    }
                }
                return _LastButtonClick;
            }
        }

        private Button _NextButton;
        private Button NextButton
        {
            get
            {
                if (_NextButton == null)
                {
                    _NextButton = new Button();
                    _NextButton.Text = "Avanti";
                    _NextButton.ID = "NextButton";
                    _NextButton.CssClass = UsedOnFrontend ? "btn btn-default next-button" : "button";                    
                }
                return _NextButton;
            }
        }

        private Button _BackButton;
        private Button BackButton
        {
            get
            {
                if (_BackButton == null)
                {
                    _BackButton = new Button();
                    _BackButton.Text = "Indietro";
                    _BackButton.ID = "BackButton";
                    _BackButton.CssClass = UsedOnFrontend ? "btn btn-default back-button" : "button";                   
                }
                return _BackButton;
            }
        }

        private Button _EndButton;
        private Button EndButton
        {
            get
            {
                if (_EndButton == null)
                {
                    _EndButton = new Button();
                    _EndButton.Text = "Completa";
                    _EndButton.ID = "EndButton";
                    _EndButton.CssClass = UsedOnFrontend ? "btn btn-default end-button" : "button";                    
                }
                return _EndButton;
            }
        }

        public string NextButtonLabel
        {
            get
            {
                return (NextButton.Text);
            }
            set
            {
                NextButton.Text = value;
            }
        }

        public string BackButtonLabel
        {
            get
            {
                return (BackButton.Text);
            }
            set
            {
                BackButton.Text = value;
            }
        }

        public string EndButtonLabel
        {
            get
            {
                return (EndButton.Text);
            }
            set
            {
                EndButton.Text = value;
            }
        }

        #endregion
              
        private HiddenField currentstepctr;

        private int _CurrentStep;
        public int CurrentStep
        {
            get
            {
                if (HttpContext.Current.Request.Form["currentstep"] != null)
                {
                    _CurrentStep = Convert.ToInt16(HttpContext.Current.Request.Form["currentstep"]);
                }
                return (_CurrentStep);
            }
            set
            {
                _CurrentStep = value;
            }
        }

        private int _NextStep;
        private int _NextStepSet=-1;
        public int NextStep
        {
            get
            {
                if (CancelWizardRequest)
                    _NextStep = CurrentStep;
                else if (_NextStepSet != -1)
                    _NextStep = _NextStepSet;
                else
                switch (LastButtonClick)
                {
                    case ButtonTypes.None:
                        _NextStep = CurrentStep;
                        break;
                    case ButtonTypes.Next:
                        _NextStep = CurrentStep + 1;
                        break;
                    case ButtonTypes.Back:
                        _NextStep = CurrentStep - 1;
                        break;
                    case ButtonTypes.End:
                        _NextStep = CurrentStep + 1;
                        break;
                    default:
                        break;
                }
                return (_NextStep);
            }
            set
            {
                _NextStepSet = value;
            }
        }

        public enum NetWizardRenderType
        {
            Frontend,
            Backoffice
        }


        /// <summary>
        /// NetWizard constructor with RenderType
        /// </summary>
        /// <param name="renderType">Backoffice Frontend Render type</param>
        public NetWizard(NetWizardRenderType renderType) 
            : this()
        {
            UsedOnFrontend = renderType == NetWizardRenderType.Frontend ? true : false;
        }

        public NetWizard()
        {
            currentstepctr = new HiddenField();
            currentstepctr.ID = "currentstep";
            #region header/title
            header = new Label();           
            header.CssClass = HeaderCssClass;
            this.Controls.Add(header);
            steptitle = new System.Web.UI.LiteralControl();            
            this.Controls.Add(steptitle);
            stepmessage = new System.Web.UI.LiteralControl();
            this.Controls.Add(stepmessage); 
            stepWarning = new System.Web.UI.LiteralControl();
            this.Controls.Add(stepWarning);                        
            #endregion

            this.Controls.Add(currentstepctr);
            this.Controls.Add(stepsContainer);
            this.Controls.Add(CompleteStep);
            this.Controls.Add(BackButton);
            this.Controls.Add(NextButton);
            this.Controls.Add(EndButton);            
        }
               

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (string.IsNullOrEmpty(CssClass))
                CssClass = "netWizard-default";

            _NextButton.Visible = false;
            _BackButton.Visible = false;
            _EndButton.Visible = false;

            int nextstep = NextStep;

            #region header/title
            header.Text = Header;
            if (nextstep < NetWizardSteps.Count)
            {
                if (showProgressTitle)
                {
                    steptitle.Text = @"
                            <span class=""steptitle"">
                                <span class=""steptitle-inner"">
                                    <span class=""steptitle-inner-inner"">
                                        <span class=""description-a"">Step</span>
                                            <span class=""current"">{0}</span>";
                    if(showTotalSteps) 
                            steptitle.Text+=@"                                                           
                                    <span class=""description-b""> di </span>
                                        <span class=""ntotal"">{1}</span>";
                steptitle.Text+=@"<span class=""description-c"">{2}</span>
                                    </span>
                                </span>
                             </span>";
                }
                else
                    /*steptitle.Text = @"
                                <span class=""steptitle"">
                                    <span class=""steptitle-inner"">
                                        <span class=""steptitle-inner-inner"">
                                            <span class=""description-c"">{2}</span>
                                        </span>
                                    </span>
                                </span>";*/
                    steptitle.Text = @"<span class=""description-title"">{2}</span>";

                stepmessage.Text = @"<span class=""stepmessage"">{0}</span>";
                //stepWarning.Text = @"<span class=""stepwarning"">{0}</span>";

            }
            #endregion

            for (int i = 0; i < NetWizardSteps.Count; i++)
            {
                NetWizardSteps[i].Visible = i == nextstep;
            }
            if(nextstep == NetWizardSteps.Count)
            {
                CompleteStep.Visible =true ;
                if (CompleteStep.Controls.Count == 0)
                    CompleteStep.Controls.Add(new System.Web.UI.LiteralControl("<span>Azione Completata</span>"));

            }
            if (nextstep >= 0 && nextstep < NetWizardSteps.Count)
            {
                NetWizardSteps[nextstep].Visible = true && !stopwizard;
                steptitle.Text = string.Format(steptitle.Text, nextstep+1, NetWizardSteps.Count, NetWizardSteps[nextstep].Title);
                stepmessage.Text = string.Format(stepmessage.Text,NetWizardSteps[nextstep].Message);
                if (NetWizardSteps[nextstep].Warning.Length>0)
                {
                    stepWarning.Text = @"<span class=""stepwarning"">{0}</span>";
                    stepWarning.Text = string.Format(stepWarning.Text, NetWizardSteps[nextstep].Warning);                    
                }
                if (NextStep > 0 && !HideBackButton)
                    _BackButton.Visible = true;
            }
            if (nextstep < NetWizardSteps.Count - 1)
            {
                _NextButton.Visible = true && !stopwizard;
            }
            else if (nextstep == NetWizardSteps.Count - 1)
                _EndButton.Visible = true;                
            currentstepctr.Value = nextstep.ToString();
        }
    }

    public class NetWizardStep : Panel
    {
        //public NetWizard ParentWizard;
        public string Title="";
        public string Message="";
        public string Warning="";   
        public NetWizardStep():base()
        {           

        }
        public void UseViewState(bool use)
        {
            this.EnableViewState = use;
        }

        /*public void clearviewstate()
        {
            this.ViewState.Clear();
        }
        public void disableviewstate()
        {
            this.EnableViewState = false;
        }

        public void enableviewstate()
        {
            this.EnableViewState = true;
        }
        */



    }

    public class NetWizardStepsCollection : IEnumerable
    {
        private NetWizard ParentWizard;
        private G2Collection coll;
        public int Count { get { return coll.Count; } }


        public NetWizardStepsCollection(NetWizard parentwizard)
        {
            coll = new G2Collection();
            ParentWizard = parentwizard;
        }


        public void Add(NetWizardStep step, string key)
        {
            coll.Add(key, step);
            //step.ParentWizard = this.ParentWizard;
            ParentWizard.stepsContainer.Controls.Add(step);
        }

        public void Add(NetWizardStep step)
        {
            coll.Add(step.GetHashCode().ToString(), step);
            //if (this.ParentWizard != null) step.ParentWizard = ParentWizard;
            ParentWizard.stepsContainer.Controls.Add(step);
        }

        public void Remove(string key)
        {
            ParentWizard.stepsContainer.Controls.Remove((NetWizardStep)coll[key]);
            coll.Remove(key);
        }

        public void Remove(NetWizardStep step)
        {
            coll.Remove(step.GetHashCode().ToString());
            ParentWizard.stepsContainer.Controls.Remove(step);
        }

        public void Clear()
        {
            coll.Clear();
            ParentWizard.stepsContainer.Controls.Clear();
        }

        public NetWizardStep this[int i]
        {
            get
            {
                NetWizardStep str = (NetWizardStep)coll[coll.Keys[i]];
                return str;
            }
        }

        public int IndexOf(string key)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (this.coll.Keys[i] == key)
                    return i;
            }
            return -1;
        }

        public NetWizardStep this[string key]
        {
            get
            {
                NetWizardStep val = (NetWizardStep)coll[key];
                return val;
            }
        }

        public bool Contains(string key)
        {
            return coll[key] != null;
        }

        public bool Contains(NetWizardStep step)
        {
            return coll[step.GetHashCode().ToString()] != null;
        }

        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private NetWizardStepsCollection Collection;
            public CollectionEnumerator(NetWizardStepsCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }   
}
