using System;
using System.Web;
using System.Xml;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetForms
{
    public class NetFormLabels : NetUtility.Labels
    {
        public const String ApplicationKey = "NetFormLabelsCacheObj";

        public NetFormLabels(string absoluteWebRoot)
            :base(absoluteWebRoot)
        {            
        }
        public NetFormLabels(XmlNode rootNode)
            : base(rootNode)
        {
        }

        public enum NetFormLabelsList
        {
            CampiConAsteriscoObbligatori, SubmitLabel,
            InserimentoEffettuatoConSuccesso, InserimentoFallito,
            AggiornamentoEffettuatoConSuccesso, AggiornamentoFallito,
            IlCampoEObbligatorio,IlCampoNonEValido,FormatoData,
            Mese,Anno,Ora,Minuti,Giorno,
            IlCampoEObbligatorioEDeveEssereDiQuattroCifre,
            IlCampoNonContieneUnaDataValida,
            Vecchia, VecchiaLasciareIlCampoVuoto,
            LasciareIlCampoVuotoPerMantentenereQuellaAttuale,
            Conferma, FormatoPasswordNonCorretto,
            LunghezzaMassimaCampo, LunghezzaMinimaCampo,
            VecchiaPasswordErrata, PasswordNonCoincidenti,
            PasswordNonValide,VecchiaPasswordNuovaPasswordCoincidenti,
            CampoEmailNonValido, CampoNumericoNonValido, CampoTelefonoNonValido,
            CampoFileNonValido, CampoValutaNonValido,
            Modifica,Elimina,Scheda,SchedaOpzioni,
            CompletamentoProcedura,
            OperazioneCompletata,
            CampoDataNonValido,
            DataSuccessivaAl,
            DataAntecedenteAl,
            Avanti,Indietro,Completa,
            DefaultInfoText,
            MessaggioProceduraCompletata,
            TestoAvanzamentoProcedura,
            StatoAvanzamentoProcedura,
            ProceduraCompletataConSuccesso,
            RiepilogoDatiInseriti,
            CampiNonCoincidenti,
            Confirm,NecessarioAccettareCondizione,DatiRipetuti
        }

        public string this[NetFormLabelsList label]
        {
            get
            {
                if (this.LabelsCache.Contains(label.ToString()))
                {
                    return this.FormatLabel(this.LabelsCache[label.ToString()]);
                }
                else
                {
                    XmlNodeList oNodeList = this.XmlLabelsRoot.SelectNodes("/labels/netform/"+label.ToString());
                    if (oNodeList != null && oNodeList.Count > 0)
                    {
                        string value = oNodeList[0].InnerXml;
                        LabelsCache.Add(value, label.ToString());
                        return this.FormatLabel(value);
                    }
                }
                return null;
            }
        }
    }

}