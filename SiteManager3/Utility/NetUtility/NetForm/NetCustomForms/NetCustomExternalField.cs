using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Class1
/// </summary>
/// 
namespace NetForms
{
    public class NetCustomExternalField:NetExternalField
    {
        private NetForms.Custom.NetCfExternalTable _RelatedCustomField;
        public NetForms.Custom.NetCfExternalTable RelatedCustomField
        {
            get
            {
                return _RelatedCustomField;
            }
        }
        
        public override string Value
        {
            get
            {
                return RelatedCustomField.PostbackValue;
            }
            set
            {
                RelatedCustomField.setValue(value);
            }
        }
        public NetCustomExternalField(NetForms.Custom.NetCfExternalTable relatedCustomField)
            :base(relatedCustomField.ReferencedTable.ToString(),relatedCustomField.ID.ToString())
        {
            _RelatedCustomField = relatedCustomField;
        }


    }
}