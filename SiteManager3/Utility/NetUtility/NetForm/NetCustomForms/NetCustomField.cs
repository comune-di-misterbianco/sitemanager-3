using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom
{
    public abstract class NetCustomField:NetField
    {
        public const string SqlSelectQuery = "SELECT * FROM CustomTables_Fields INNER JOIN CustomTables_FieldsLabels ON Field_FieldLabel = id_CustomField INNER JOIN CustomTables_FieldsTypes ON id_FieldsType = Type_CustomField INNER JOIN CustomTables_FieldsJoins ON Field_FieldJoin = id_CustomField  WHERE Lang_FieldLabel = [LANG] AND id_CustomField = {0}";

        protected NetCustomFieldValidationResult _LastValidation;
        public NetCustomFieldValidationResult LastValidation
        {
            get
            {
                return _LastValidation;
            }
        }

        private NetUtility.Languages.LanguageData _CurrentLang;
        protected NetUtility.Languages.LanguageData CurrentLang
        {
            get
            {
                if (_CurrentLang == null)
                {
                    _CurrentLang = new NetUtility.Languages.LanguageData(this.Conn);
                }
                return _CurrentLang;
            }
        }
        public override string FieldName
        {
            get
            {
                return this.ControlID;
            }
        }

        public string ControlID
        {
            get
            {
                return "CustomField" + ID;
            }
        }
                
        private string _Option1;
        public string Option1
        {
            get
            {
                if (_Option1 == null)
                    _Option1 = this.FieldRecord["Option1_CustomField"].ToString();
                    
                return _Option1;
            }
        }

        private string _Option2;
        public string Option2
        {
            get
            {
                if (_Option2 == null)
                    _Option2 = this.FieldRecord["Option2_CustomField"].ToString();

                return _Option2;
            }
        }

        private int _Type;
        public override int Type
        {
            get 
            {
                if (_Type == 0)
                    _Type = int.Parse(this.FieldRecord["Type_CustomField"].ToString());

                return _Type; 
            }
        }

        private string _TypeName;
        public string TypeName
        {
            get 
            {
                if (_TypeName == null && this.FieldRecord.Table.Columns.Contains("Nome_FieldsType"))
                    _TypeName = this.FieldRecord["Nome_FieldsType"].ToString();
                return _TypeName; 
            }
        }
            
        private int _AddToListView = -1;
        public bool AddToListView
        {
            get 
            {
                if (_AddToListView == -1)
                    _AddToListView = int.Parse(this.FieldRecord["AddToListView_CustomField"].ToString());
                return _AddToListView==1;
            }
        }

        private DataRow _FieldRecord;
        protected DataRow FieldRecord
        {
            get { return _FieldRecord; }
        }	

        private DataRow _ValueRecord;
        protected DataRow ValueRecord
        {
            get 
            {
                if (_ValueRecord == null && Conn!=null && ValueRecordID >0)
                {
                    DataTable tab = Conn.SqlQuery("SELECT * FROM CustomTables_FieldsValues WHERE Field_FieldValue = " + this.ID + " AND Record_FieldValue = " + ValueRecordID);
                    if (tab.Rows.Count > 0)
                        _ValueRecord = tab.Rows[0];
                }
                return _ValueRecord; 
            }
        }	

        protected int _ValueRecordID;
        public int ValueRecordID
        {
            set
            {
                _ValueRecordID = value;
            }
            get { return _ValueRecordID; }
        }

        public override string Value
        {
            get
            {
                return CurrentValue;
            }
            set
            {
                CurrentValue = value;
            }
        }

        protected string _CurrentValue;
        public string CurrentValue
        {
            get 
            {
                if (_CurrentValue == null && this.ValueRecord != null)
                {
                    _CurrentValue = ValueRecord["Value_FieldValue"].ToString();
                }
                return _CurrentValue; 
            }
            set
            {
                _CurrentValue = value;
            }
        }

        private string _ContainerTag;
        public string ContainerTag
        {
            get 
            {
                if (_ContainerTag == null)
                    _ContainerTag = "p";
                return _ContainerTag; 
            }
            set
            {
                _ContainerTag = value;
            }
        }

        private int _Required = -1;
        public override bool Required
        {
            get
            {
                if (_Required == -1)
                    _Required = int.Parse(this.FieldRecord["Required_CustomField"].ToString());

                return _Required==1;
            }
        }

        protected string _PostbackValue;
        public virtual string PostbackValue
        {
            get
            {
                if (_PostbackValue == null)
                {
                    if (RequestVariable.IsValidString)
                        _PostbackValue = RequestVariable.StringValue;
                    else
                        _PostbackValue = "";
                }
                return _PostbackValue;
            }
        }

        private NetCustomFieldsGroup _FieldsGroup;
        public NetCustomFieldsGroup FieldsGroup
        {
            get
            {
                if (_FieldsGroup == null)
                {
                    _FieldsGroup = new NetCustomFieldsGroup(int.Parse(this.FieldRecord["Group_FieldJoin"].ToString()), Conn);
                }
                return _FieldsGroup;
            }
            set
            {
                if (_FieldsGroup == null)
                    _FieldsGroup = value;
                else
                {
                   Exception ex = new Exception("FieldsGroup gi� impostato sull'istanza di un NetCustomField");
                   /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                   ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                   NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, "", "", "89");
                }
            }
        }

        public virtual string CurrentFilteredValue
        {
            get
            {               
                return CurrentValue;
            }
        }

        private System.Collections.Specialized.NameValueCollection _PostdataSource;
        public System.Collections.Specialized.NameValueCollection PostdataSource
        {
            get
            {
                if (_PostdataSource == null)
                {
                    _PostdataSource = ((NetCustomTable)this.FieldsGroup.Table).PostdataSource;
                }
                return _PostdataSource;
            }
        }

        public override NetUtility.RequestVariable RequestVariable
        {
            get
            {
                if (_RequestVariable == null)
                {
                     _RequestVariable = new NetUtility.RequestVariable("CustomField"+this.ID, PostdataSource);                    
                }
                return _RequestVariable;
            }
        }

        protected int _ID;
        public int ID
        {
            get 
            {
                if(_ID == 0 && this.FieldRecord!=null)
                    _ID = int.Parse(this.FieldRecord["id_CustomField"].ToString());

                return _ID; 
            }
        }

        public override string Label
        {
            get 
            {
                if (_Label == null)
                    _Label = this.FieldRecord["Label_FieldLabel"].ToString();
                return _Label; }
        }

        private NetCms.Connections.Connection _Conn;
        public NetCms.Connections.Connection Conn
        {
            get
            {
                return _Conn;
            }
            set
            {
                _Conn = value;
            }
        }

        public NetCustomField(DataRow fieldRecord)
        {
            _FieldRecord = fieldRecord;
        }
        public NetCustomField(DataRow fieldRecord, NetCms.Connections.Connection conn)
        {
            _Conn = conn;
            _FieldRecord = fieldRecord;
        }
        public NetCustomField(int id,NetCms.Connections.Connection conn)
        {
            _Conn = Conn;
            _FieldRecord = NetUtility.Languages.LanguagesUtility.LangRecordSelector(this.CurrentLang.ID, string.Format(SqlSelectQuery, this.ID), Conn);
            _ID = id;
        }

        public virtual HtmlGenericControl GetCellControl()
        {
            HtmlGenericControl td = new HtmlGenericControl("td");
            td.InnerHtml = this.CurrentValue;
            return td;
        }
        public virtual HtmlGenericControl GetDetailControl()
        {
            HtmlGenericControl ctr = new HtmlGenericControl("span");
            ctr.InnerHtml = "<strong>" + this.Label + ": </strong>" + this.CurrentValue;
            return ctr;
        }

        public abstract NetCustomFieldValidationResult ValidateInput();

        public abstract string GetSearchFilter(string value);
        public override string getFilter(string value)
        {
            return this.GetSearchFilter(value);
        }
        public override string validateInput(string input)
        {
            return this.ValidateInput().Message;
        }
        public virtual string ValueForSQL
        {
            get
            {
                return FormatValueForSQL(this.PostbackValue);
            }
        }

        public virtual string FormatValueForSQL(string value)
        {
            string output = value;
            output = output.Trim();
            output = output.Replace("'", "''");
            output = "'" + output + "'";
            return output;
        }

        public virtual bool Insert(int RecordID)
        {
            //Le 2 seguenti righe di codice servono a evitare che vengano inseriti i record vuoti per i campi dei gruppi non selezionati 
            //atraverso il sistema di filtraggio gruppi
            /*1)*/if (this.FieldsGroup.Table.GroupsFilterEnabled && (this.ValueForSQL == string.Empty || this.ValueForSQL == "''"))
            /*2)*/  return true;
            int lastInsert = this.Conn.ExecuteInsert(string.Format(this.InsertSql, RecordID));
            bool ok = lastInsert>0;
            if (ok)
            {
                this.ValueRecordID = lastInsert;
                this._ValueRecord = null;
            }
            return ok;
        }
        public virtual bool Delete(int RecordID)
        {
            return this.Conn.Execute(string.Format(this.DeleteSql, RecordID));            
        }
        public virtual bool Update(int RecordID)
        {
            if (this.ValueRecord!=null)
                return this.Conn.Execute(string.Format(this.UpdateSql, RecordID));
            else
                return this.Insert(RecordID);
        }

        public virtual string SelectSql
        {
            get
            {
                return "SELECT * CustomTables_FieldsValues WHERE Record_FieldValue = {0} AND Field_FieldValue = " + this.ID + "";
            }
        }
        public virtual string InsertSql
        {
            get
            {
                return "INSERT INTO CustomTables_FieldsValues (Record_FieldValue,Value_FieldValue,Field_FieldValue) VALUES ({0}," + this.ValueForSQL + "," + this.ID + ")";
            }
        }
        public virtual string DeleteSql
        {
            get
            {
                return "DELETE FROM CustomTables_FieldValues WHERE Record_FieldValue = {0} AND Field_FieldValue = " + this.ID;
            }
        }
        public virtual string UpdateSql
        {
            get 
            {
                string sql = "UPDATE CustomTables_FieldsValues SET";
                sql += " Value_FieldValue = " + this.ValueForSQL + "";
                sql += " WHERE Record_FieldValue = {0} AND Field_FieldValue = " + this.ID;

                return sql; 
            }
        }
        
        public static NetCustomField FieldFactory(DataRow fieldRecord)
        {
            switch (int.Parse(fieldRecord["Type_CustomField"].ToString()))
            {
                case NetCfTextbox.FieldTypeID:
                    return new NetCfTextbox(fieldRecord);

                case NetCfDate.FieldTypeID:
                    return new NetCfDate(fieldRecord);

                case NetCfDropDownList.FieldTypeID:
                    return new NetCfDropDownList(fieldRecord);
                    
                case NetCfDynamicDropDownList.FieldTypeID:
                    return new NetCfDynamicDropDownList(fieldRecord);

                case NetCfExternalTable.FieldTypeID:
                    return new NetCfExternalTable(fieldRecord);

                default:
                    return null;
            }
        }
    }
    public struct NetCustomFieldValidationResult
    {
        private ResultTypes _Result;
        public ResultTypes Result
        {
            get { return _Result; }
            set { _Result = value; }
        }

        public enum ResultTypes
        {
            NotYetExecuted = 0,
            Succeded = 1,
            FailValidation = 2
        }

        private string _Message;
        public string Message
        {
            get
            {                
                return _Message;
            }
            set { _Message = value; }
        }

    }
}