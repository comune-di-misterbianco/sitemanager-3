using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom
{
    public class NetCfDynamicDropDownList : NetCustomField
    {
        public const int FieldTypeID = 4;

        #region ProprietÓ

        public string CustomSqlQuery
        {
            get
            {
                return this.Option1;
            }
        }

        private DataTable _Options;
        public DataTable Options
        {
            get 
            {
                if (_Options == null)
                {
                    _Options = Conn.SqlQuery(CustomSqlQuery);
                }
                return _Options; 
            }
        }
        private string _CurrentFilteredValue;
        public override string CurrentFilteredValue
        {
            get
            {
                if (_CurrentFilteredValue == null)
                {
                    _CurrentFilteredValue = "";
                    if (this.CurrentValue != null && this.CurrentValue != string.Empty)
                    {
                        DataRow[] rows = Options.Select(this.Options.Columns[1].ColumnName+" = " + this.CurrentValue);
                        if (rows.Length > 0)
                            _CurrentFilteredValue = rows[0][0].ToString();
                    }
                }
                return _CurrentFilteredValue;
            }
        }

        #endregion

        #region Costruttori
        public NetCfDynamicDropDownList(DataRow fieldRecord)
            : base(fieldRecord) 
        {
            InitFieldData();
        }

        public NetCfDynamicDropDownList(int id, NetCms.Connections.Connection conn)
            : base(id, conn) 
        {
            InitFieldData();        
        }

        private void InitFieldData()
        {
           
        }

        #endregion

        #region Metodi
        public override HtmlGenericControl GetCellControl()
        {
            HtmlGenericControl td = new HtmlGenericControl("td");
            td.InnerHtml = "&nbsp;";
            if (CurrentFilteredValue != string.Empty)
                td.InnerHtml = CurrentFilteredValue;

            return td;
        }
        public override HtmlGenericControl GetDetailControl()
        {
            HtmlGenericControl ctr = new HtmlGenericControl("span");
            ctr.InnerHtml = "<strong>" + this.Label + ": </strong>" + this.CurrentFilteredValue;
            return ctr;
        }
        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl container = new HtmlGenericControl(this.ContainerTag);
            
            //Applico le classi css
            container.Attributes["class"] = "NetCfTextbox";
            if (this.CssClass != null && this.CssClass.Length > 0)
                container.Attributes["class"] += " " + this.CssClass;

            //costruisco la label
            Label label = new Label();
            label.Text = this.Label;
            if (this.Required)
                label.Text += "*";
            label.AssociatedControlID = this.ControlID;

            //costruisco il controllo
            DropDownList DropDownList = new DropDownList();
            DropDownList.ID = this.ControlID;
            if(!this.Required)
                DropDownList.Items.Add(new ListItem("",""));

            foreach (DataRow option in this.Options.Rows)
            {
                ListItem item = new ListItem(option[0].ToString(), option[1].ToString());
                DropDownList.Items.Add(item);
                if (this.CurrentValue != null && CurrentValue == item.Value)
                {
                    item.Selected = true;
                }
            }
           
            //Aggiungo i controlli al contenitore
            container.Controls.Add(label);
            container.Controls.Add(DropDownList);

            return container;
        }
        public override string GetSearchFilter(string value)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public override NetCustomFieldValidationResult ValidateInput()
        {
            string errors = "";
            if (Required && this.PostbackValue.Length == 0)
            {
                errors += "<li>";
                errors += String.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio], Label);
                errors += "</li>";
            }
           
            NetCustomFieldValidationResult result = new NetCustomFieldValidationResult();
            result.Message = errors;
            result.Result = errors.Length > 0 ? NetCustomFieldValidationResult.ResultTypes.FailValidation : NetCustomFieldValidationResult.ResultTypes.Succeded;
            _LastValidation = result;
            
            return result;
        }
        #endregion

    }

}