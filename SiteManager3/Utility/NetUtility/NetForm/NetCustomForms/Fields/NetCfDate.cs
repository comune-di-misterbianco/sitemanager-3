using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom
{
    public class NetCfDate : NetCustomField
    {
        public const int FieldTypeID = 2;

        #region ProprietÓ

        private int _MaxLength;
        public int MaxLength
        {
            get 
            { 

                return _MaxLength; 
            }
        }

        private string _MinDate;
        public string MinDate
        {
            get { return _MinDate; }
        }

        private string _MaxDate;
        public string MaxDate
        {
            get { return _MaxDate; }
        }
        /*
        private TextBox _Years;
        public TextBox Years
        {
            get 
            {
                if (_Years == null)
                {
                    _Years = new TextBox();
                    _Years.Text = DateTime.Now.Year.ToString();
                    _Years.ID = this.ControlID + "_Year";
                }
                return _Years; 
            }
        }

        private DropDownList _Months;
        public DropDownList Months
        {
            get
            {
                if (_Months == null)
                {
                    _Months = new DropDownList();
                    _Months.ID = this.ControlID + "_Month";
                    for (int i = 1; i <= 12; i++)
                        _Months.Items.Add(new ListItem(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i),NetUtility.TreeUtility.FormatNumber(i.ToString(),2)));
                }
                return _Months;
            }
        }

        private DropDownList _Days;
        public DropDownList Days
        {
            get
            {
                if (_Days == null)
                {
                    _Days = new DropDownList();
                    _Days.ID = this.ControlID + "_Day";
                    for (int i = 1; i <= 31; i++)
                        _Days.Items.Add(new ListItem(NetUtility.TreeUtility.FormatNumber(i.ToString(), 2), NetUtility.TreeUtility.FormatNumber(i.ToString(), 2)));
                }
                return _Days;
            }
        }
        
        public override string PostbackValue
        {
            get
            {
                if (_PostbackValue == null)
                {
                    _PostbackValue = string.Empty;
                    NetUtility.RequestVariable RequestYear = new NetUtility.RequestVariable(this.Years.ID);
                    NetUtility.RequestVariable RequestMonth = new NetUtility.RequestVariable(this.Months.ID);
                    NetUtility.RequestVariable RequestDay = new NetUtility.RequestVariable(this.Days.ID);

                    if (RequestDay.IsValidInteger && RequestMonth.IsValidInteger && RequestYear.IsValidInteger)
                    {
                        DateTime date;
                        string sdate = RequestDay.StringValue + "/" + RequestMonth.StringValue + "/" + RequestYear.StringValue;
                        if (DateTime.TryParse(sdate, out date))
                            _PostbackValue = sdate;
                    }
                }
                return _PostbackValue;
            }
        }*/

        public override string PostbackValue
        {
            get
            {
                if (_PostbackValue == null)
                {
                    _PostbackValue = base.PostbackValue;
                    
                        DateTime date;
                        if (DateTime.TryParse(_PostbackValue, out date))
                        {
                            _PostbackValue = NetUtility.TreeUtility.FormatNumber(date.Day.ToString(), 2) + "/" + 
                                             NetUtility.TreeUtility.FormatNumber(date.Month.ToString(), 2) + "/" + 
                                             NetUtility.TreeUtility.FormatNumber(date.Year.ToString(), 2);
                        }                    
                }
                return _PostbackValue;
            }
        }

        #endregion

        #region Costruttori
        public NetCfDate(DataRow fieldRecord)
            : base(fieldRecord) 
        {
            InitFieldData();
        }

        public NetCfDate(int id, NetCms.Connections.Connection conn)
            : base(id, conn) 
        {
            InitFieldData();        
        }

        private void InitFieldData()
        {
            this._MaxDate = this.FieldRecord["Option2_CustomField"].ToString();
            this._MinDate = this.FieldRecord["Option1_CustomField"].ToString();
        }

        #endregion

        #region Metodi
        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl container = new HtmlGenericControl(this.ContainerTag);
            
            //Applico le classi css
            container.Attributes["class"] = "NetCfTextbox";
            if (this.CssClass != null && this.CssClass.Length > 0)
                container.Attributes["class"] += " " + this.CssClass;

            //costruisco la label
            Label label = new Label();
            label.Text = this.Label;
            label.AssociatedControlID = this.ControlID;
            if (this.Required)
                label.Text += "*";
            label.Text += " (gg/mm/aaaa)";
            container.Controls.Add(label);

            TextBox textbox = new TextBox();
            if (this.CurrentValue != null)
                textbox.Text = this.CurrentValue;
            textbox.Columns = 10;
            textbox.Rows = 1;
            textbox.ID = this.ControlID;
            container.Controls.Add(textbox);

            /*
            //costruisco la label dei giorni
            label = new Label();
            label.Text = "Giorno";
            label.AssociatedControlID = this.Days.ID;
            container.Controls.Add(label);
            container.Controls.Add(this.Days);

            //costruisco la label dei giorni
            label = new Label();
            label.Text = "Mese";
            label.AssociatedControlID = this.Months.ID;
            container.Controls.Add(label);
            container.Controls.Add(this.Months);

            //costruisco la label anni
            label = new Label();
            label.Text = "Anno";
            label.AssociatedControlID = this.Years.ID;
            container.Controls.Add(label);
            container.Controls.Add(this.Years);
            */
            return container;
        }
        public override string GetSearchFilter(string value)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public override NetCustomFieldValidationResult ValidateInput()
        {
            string errors = "";
            
            if (this.PostbackValue.Length == 0)
            {
                if (Required)
                {
                    errors += "<li>";
                    errors += String.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio],Label);
                    errors += "</li>";
                }
            }
            else
            {
                DateTime date;
                DateTime mindate;
                DateTime maxdate;
                if (!DateTime.TryParse(PostbackValue, out date))
                {
                    errors += "<li>";
                    errors += String.Format(this.NLabels[NetFormLabels.NetFormLabelsList.CampoDataNonValido], Label);
                    errors += "</li>";
                }
                else
                {
                    if (DateTime.TryParse(this.MaxDate, out maxdate) && maxdate < date)
                    {
                        errors += "<li>";
                        errors += String.Format(this.NLabels[NetFormLabels.NetFormLabelsList.DataAntecedenteAl], Label, MaxDate);
                        errors += "</li>";
                    }

                    if (DateTime.TryParse(MinDate, out mindate) && mindate>date)
                    {
                        errors += "<li>";
                        errors += String.Format(this.NLabels[NetFormLabels.NetFormLabelsList.DataSuccessivaAl], Label,MinDate);
                        errors += "</li>";
                    }
                }
            }
            

            NetCustomFieldValidationResult result = new NetCustomFieldValidationResult();
            result.Message = errors;
            result.Result = errors.Length > 0 ? NetCustomFieldValidationResult.ResultTypes.FailValidation : NetCustomFieldValidationResult.ResultTypes.Succeded;
            _LastValidation = result;
            
            return result;
        }
        #endregion

    }

}