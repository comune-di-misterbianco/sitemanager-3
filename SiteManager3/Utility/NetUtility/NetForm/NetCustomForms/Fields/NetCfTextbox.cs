using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom
{
    public class NetCfTextbox : NetCustomField
    {
        public const int FieldTypeID = 1;

        #region ProprietÓ

        private int _MaxLength;
        public int MaxLength
        {
            get 
            { 

                return _MaxLength; 
            }
        }

        private int _Columns;
        public int Columns
        {
            get { return _Columns; }
        }

        private int _Rows;
        public int Rows
        {
            get { return _Rows; }
        }
	
        #endregion

        #region Costruttori
        public NetCfTextbox(DataRow fieldRecord)
            : base(fieldRecord) 
        {
            InitFieldData();
        }

        public NetCfTextbox(int id, NetCms.Connections.Connection conn)
            : base(id, conn) 
        {
            InitFieldData();        
        }

        private void InitFieldData()
        {
            this._MaxLength = 0;
            this._Columns= 0;
            this._Rows = 0;
            int.TryParse(this.FieldRecord["Length_CustomField"].ToString(), out _MaxLength);
            int.TryParse(this.FieldRecord["Option1_CustomField"].ToString(), out _Columns);
            int.TryParse(this.FieldRecord["Option2_CustomField"].ToString(), out _Rows);
        }

        #endregion

        #region Metodi

        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl container = new HtmlGenericControl(this.ContainerTag);
            
            //Applico le classi css
            container.Attributes["class"] = "NetCfTextbox";
            if (this.CssClass != null && this.CssClass.Length > 0)
                container.Attributes["class"] += " " + this.CssClass;

            //costruisco la label
            Label label = new Label();
            label.Text = this.Label;
            if (this.Required)
                label.Text += "*";
            label.AssociatedControlID = this.ControlID;

            //costruisco il controllo
            TextBox textbox = new TextBox();
            if(this.CurrentValue!=null)
                textbox.Text = this.CurrentValue;
            textbox.Columns = this.Columns;
            textbox.Rows = this.Rows;
            if (this.Rows > 1)
                textbox.TextMode = TextBoxMode.MultiLine;
            textbox.ID = this.ControlID;
            
            //Aggiungo i controlli al contenitore
            container.Controls.Add(label);
            container.Controls.Add(textbox);

            return container;
        }

        public override string GetSearchFilter(string value)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public override NetCustomFieldValidationResult ValidateInput()
        {
            string errors = "";
            if (Required && this.PostbackValue.Length == 0)
            {
                errors += "<li>";
                errors += String.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio], Label);
                errors += "</li>";
            }
            if (MaxLength > 0 && this.PostbackValue.Length > MaxLength)
            {
                errors += "<li>";
                errors += String.Format(this.NLabels[NetFormLabels.NetFormLabelsList.LunghezzaMassimaCampo], Label,MaxLength.ToString());
                errors += "</li>";
            }

            NetCustomFieldValidationResult result = new NetCustomFieldValidationResult();
            result.Message = errors;
            result.Result = errors.Length > 0 ? NetCustomFieldValidationResult.ResultTypes.FailValidation : NetCustomFieldValidationResult.ResultTypes.Succeded;
            _LastValidation = result;
            
            return result;
        }
        #endregion

    }

}