using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom
{
    public class NetCfExternalTable : NetCustomField
    {
        public const int FieldTypeID = 5;

        #region ProprietÓ

        private string _ReferencedTable;
        public string ReferencedTable
        {
            get 
            {
                if (_ReferencedTable == null)
                    _ReferencedTable = this.Option1;
                return _ReferencedTable; 
            }
        }
                
        #endregion

        #region Costruttori
        public NetCfExternalTable(DataRow fieldRecord)
            : base(fieldRecord) 
        {
        }

        public NetCfExternalTable(int id, NetCms.Connections.Connection conn)
            : base(id, conn) 
        {
        }

        #endregion

        #region Metodi
        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl container = new HtmlGenericControl("span");

            HiddenField field = new HiddenField();
            if (this.CurrentValue != null)
                field.Value = this.CurrentValue;
            field.ID = this.ControlID;
            container.Controls.Add(field);

            return container;
        }
        public override HtmlGenericControl GetOverviewControl()
        {
            return null;
        }
        public override string GetSearchFilter(string value)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public override NetCustomFieldValidationResult ValidateInput()
        {
            NetCustomFieldValidationResult result = new NetCustomFieldValidationResult();
            result.Message = string.Empty;
            result.Result = NetCustomFieldValidationResult.ResultTypes.Succeded;
            _LastValidation = result;
            
            return result;
        }
        public void setValue(string value)
        {
            this._PostbackValue = value;
        }
        #endregion

    }

}