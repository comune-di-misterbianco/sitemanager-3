using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom
{
    public class NetCustomTableCategoriesBuilder
    {
        protected int _DefaultLangID;
        public int DefaultLangID
        {
            get
            {
                if (_DefaultLangID == 0)
                {
                    DataTable tab = this.Conn.SqlQuery("SELECT * FROM Lang WHERE Default_Lang = 1");
                    if (tab.Rows.Count > 0)
                    {
                        _DefaultLangID = int.Parse(tab.Rows[0]["id_Lang"].ToString());
                    }
                }
                return _DefaultLangID;
            }
        }

        private NetUtility.Languages.LanguageData _CurrentLang;
        protected NetUtility.Languages.LanguageData CurrentLang
        {
            get
            {
                if (_CurrentLang == null)
                    _CurrentLang = new NetUtility.Languages.LanguageData(this.Conn);
                
                return _CurrentLang;
            }
        }

        private NetCms.Connections.Connection _Conn;
        private NetCms.Connections.Connection Conn
        {
            get { return _Conn; }
        }	

        private NetCustomTableCategoriesCollection _Categories;
	    public NetCustomTableCategoriesCollection Categories
	    {
	      get { return _Categories;}
	    }

        private HtmlGenericControl _Controls;
        public HtmlGenericControl Controls
        {
            get { return _Controls; }
        }

        private NetCustomTableCategory _SelectedCategory;
        public NetCustomTableCategory SelectedCategory
        {
            get 
            {
                if (_SelectedCategory == null && SelectedCategoryID != null)
                    _SelectedCategory = Categories.Find(SelectedCategoryID);
                    
                return _SelectedCategory; 
            }
        }

        private string _SelectedCategoryID;
        public string SelectedCategoryID
        {
            get
            {
                if (_SelectedCategoryID == null)
                {
                    if (OwnerTable != null && OwnerTable.ValueRecordID > 0)
                    {
                        DataTable table = this.Conn.SqlQuery("SELECT * FROM customtables_fieldsgroupscategoriesvalues WHERE Record_CategoryValue = " + OwnerTable.ValueRecordID);
                        if (table.Rows.Count > 0)
                            _SelectedCategoryID = table.Rows[0]["Category_CategoryValue"].ToString();
                    }
                }
                return _SelectedCategoryID;
            }
        }

        private NetCustomTable OwnerTable;

        private NetField _Field;
        public NetField Field
        {
            get { return _Field; }
        }

        private int _Table;
        public int Table
        {
            get { return _Table; }
        }

        private System.Collections.Specialized.NameValueCollection _Request;
        private System.Collections.Specialized.NameValueCollection Request
        {
            get
            {
                return _Request;
            }
        }

        public NetCustomTableCategoriesBuilder(NetCustomTable table, NetCms.Connections.Connection Conn,System.Collections.Specialized.NameValueCollection request)
        {
            OwnerTable = table;
            _Conn = Conn;
            _Request = request;
            _Table = table.ID;
            BuildCategories();
        }
        public NetCustomTableCategoriesBuilder(int table, NetCms.Connections.Connection Conn, System.Collections.Specialized.NameValueCollection request)
        {
            _Request = request;
            _Conn = Conn;
            _Table = table;
            BuildCategories();
        }
        
        private void BuildCategories()
        {
            _Controls = new HtmlGenericControl("div");
            _Categories = new NetCustomTableCategoriesCollection(null);
            _Field = new NetHiddenField("RefreshButton");
            _Field.CurrentRequest = this.Request;
            _Field.Value = _Field.RequestVariable.StringValue;
            _Field.Label = Level0Label;

            NetDropDownList Level0List = ListControl();
            Controls.Controls.AddAt(0,Field.getControl());
            Controls.Controls.AddAt(0, Level0List.getControl());         

        }

        
        public NetDropDownList ListControl()
        {
            NetDropDownList ddl = new NetDropDownList(Level0Label, "Livello0");
            ddl.OnClientClick = "javascript: setFormValueByDDL('" + ddl.FieldName + "','" + Field.FieldName + "',1)";
            ddl.AutoPostBack = true;
            ddl.Required = true;
            ddl.CurrentRequest = this.Request;
            ddl.Value = ddl.RequestVariable.StringValue;

            string sql = "SELECT * FROM customtables_fieldsgroupscategorylabels";
            sql += " INNER JOIN customtables_fieldsgroupscategories on category_fieldsgroupcategorylabel = id_fieldsgroupscategory ";
            sql += " WHERE Lang_FieldsGroupCategoryLabel = " + this.DefaultLangID + " AND table_fieldsgroupscategory = " + this.Table + "";
            sql += " AND depth_fieldsgroupscategory = 0";

            DataTable tab = Conn.SqlQuery(sql);

            if (SelectedCategoryID != null)ddl.Value = SelectedCategoryID;
            foreach (DataRow row in tab.Rows)
            {
                NetCustomTableCategory category = new NetCustomTableCategory(Conn, this.OwnerTable, row);
                Categories.Add(category);

                ListItem item = new ListItem(category.Label, category.ID.ToString());
                ddl.addItem(item);

                NetCustomTableCategory tempcategory = category.FindNode(Field.RequestVariable.StringValue);
                if(tempcategory != null)
                    _SelectedCategory = tempcategory;
                while (tempcategory != null)
                {
                    if (tempcategory.Childs.Count > 0 && tempcategory.Childs.HasJoinedFieldsGroups)
                    {
                        tempcategory.NetField.CurrentRequest = this.Request;
                        tempcategory.NetField.Value = tempcategory.NetField.RequestVariable.StringValue;
                        Controls.Controls.AddAt(0, tempcategory.NetField.getControl());
                    }
                    tempcategory = tempcategory.ParentCategory;
                }
            }

            return ddl;
        }


        private string _Level0Label;
        public string Level0Label
        {
            get
            {
                if (_Level0Label == null)
                {
                    DataTable table = Conn.SqlQuery("SElECT * FROM customtables_fieldsgroupscategorylevellabels");
                    DataRow[] rows = table.Select(" Lang_LevelLabel = " + this.CurrentLang.ID + " AND Table_LevelLabel = " + this.Table + " AND Depth_LevelLabel = 0");
                    if (rows.Length > 0)
                        _Level0Label = rows[0]["Label_LevelLabel"].ToString();
                    else
                        _Level0Label = string.Empty;
                }
                return _Level0Label;
            }
        }
    }
}