using System;
using System.Collections;

namespace NetForms.Custom
{
    public class NetCustomFieldsRecordCollection : IEnumerable
    {
        private NetCollection coll;
        public int Count { get { return coll.Count; } }
        public NetCustomFieldsRecordCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new NetCollection();
        }

        public void Add(NetCustomFieldsRecord Record, string key)
        {
            coll.Add(key, Record);
        }
        public void Add(NetCustomFieldsRecord Record)
        {
            coll.Add("Record" + Record.ID, Record);
        }

        public void Remove(int ID)
        {
            coll.Remove("Record" + ID);
        }

        public void Clear()
        {
            coll.Clear();
        }

        public NetCustomFieldsRecord this[int i]
        {
            get
            {
                NetCustomFieldsRecord str = (NetCustomFieldsRecord)coll[coll.Keys[i]];
                return str;
            }
        }

        public NetCustomFieldsRecord this[string ID]
        {
            get
            {
                NetCustomFieldsRecord val = (NetCustomFieldsRecord)coll["Record" + ID];
                return val;
            }
        }

        public bool Contains(int ID)
        {
            return coll["Record" + ID] != null;
        }

        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private NetCustomFieldsRecordCollection Collection;
            public CollectionEnumerator(NetCustomFieldsRecordCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}