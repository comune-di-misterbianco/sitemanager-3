using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom
{
    public class NetCustomTable : NetAbstractFormTable
    {
        private string _CssClass;
        public string CssClass
        {
            get 
            {
                if (_CssClass == null)
                    _CssClass = this.TableRecord["CssClass_CustomTable"].ToString();
                return _CssClass; 
            }
            set { _CssClass = value; }
        }

        //protected NetFormLabels NLabels
        //{
        //    get
        //    {
        //        return NetUtility.LabelsCacher.NetFormLabels;
        //    }
        //}

        public override string TableLabel
        {
            get { return this.Name; }
            set {}
        }

        public override string TableName 
        {
            get
            {
                return this.Name;
            }
        }        

        private DataRow _TableRecord;
        protected DataRow TableRecord
        {
            get 
            {
                if (_TableRecord == null && Conn != null && ID > 0)
                {
                    string sql = "SELECT * FROM CustomTables_Tables INNER JOIN CustomTables_TablesLabels ON id_CustomTable = Table_TableLabel WHERE Lang_TableLabel = [LANG] AND id_CustomTable = " + ID;
                    _TableRecord = NetUtility.Languages.LanguagesUtility.LangRecordSelector(this.CurrentLang.ID,sql, Conn);
                }
                return _TableRecord; 
            }
        }

        public override string InfoText
        {
            get
            {
                if (_InfoText == null)
                {
                    _InfoText = this.TableRecord["InfoText_TableLabel"].ToString();
                }
                return _InfoText;
            }
        }

        protected int _ValueRecordID;
        public int ValueRecordID
        {
            get
            {
                if (_ValueRecordID == 0 && this.LastInsert > 0)
                    return this.LastInsert; 
                return _ValueRecordID;
            }
            set
            {
                _ValueRecordID = value;
                foreach (NetCustomFieldsGroup group in this.Groups)
                    group.ValueRecordID = value;
            }
        }

        private bool _AddSubmitButton;
        public bool AddSubmitButton
        {
            get { return _AddSubmitButton; }
            set { _AddSubmitButton = value; }
        }

        private int _EnableGroupsFilter = -1;
        public bool GroupsFilterEnabled
        {
            get 
            {
                if (_EnableGroupsFilter == -1)
                {
                    _EnableGroupsFilter = int.Parse(this.TableRecord["EnableGroupsFillter_CustomTable"].ToString());

                }
                return _EnableGroupsFilter==1; 
            }
        }

        private int _AddHeader = -1;
        public bool AddHeader
        {
            get 
            {
                if (_AddHeader == -1)
                {
                    _AddHeader = int.Parse(this.TableRecord["AddHeader_CustomTable"].ToString());
                }
                return _AddHeader == 1; 
            }
        }

        private string _SubmitButtonLabel;
        public string SubmitButtonLabel
        {
            get 
            {
                if (_SubmitButtonLabel == null)
                    _SubmitButtonLabel = "Invia";
                return _SubmitButtonLabel; 
            }
            set 
            { 
                _SubmitButtonLabel = value;
                _SubmitButton = null;
            }
        }	

        private Button _SubmitButton;
        private Button SubmitButton
        {
            get
            {
                if (_SubmitButton == null)
                {
                    _SubmitButton = new Button();
                    _SubmitButton.ID = "CT_Submit_" + ID;
                    _SubmitButton.Text = this.SubmitButtonLabel;
                }
                return _SubmitButton;
            }
        }

        private int _LastInsert;
        public override int LastInsert
        {
            get { return _LastInsert; }
        }

        public enum DetailsOptions { NotSet, Enabled, Disabled };
        private DetailsOptions _DetailsOption = DetailsOptions.NotSet;
        public DetailsOptions DetailsOption
        {
            get
            {
                if (_DetailsOption == DetailsOptions.NotSet)
                {
                    switch (int.Parse(this.TableRecord["DetailsOption_CustomTable"].ToString()))
                    {
                        case 1:
                            _DetailsOption = DetailsOptions.Enabled;
                            break;
                        default:
                            _DetailsOption = DetailsOptions.Disabled;
                            break;
                    }
                }

                return _DetailsOption;
            }
        }

        public enum ModifyOptions { NotSet, Enabled, Disabled };
        private ModifyOptions _ModifyOption = ModifyOptions.NotSet;
        public ModifyOptions ModifyOption
        {
            get
            {
                if (_ModifyOption == ModifyOptions.NotSet)
                {
                    switch (int.Parse(this.TableRecord["ModifyOption_CustomTable"].ToString()))
                    {
                        case 1:
                            _ModifyOption = ModifyOptions.Enabled;
                            break;
                        default:
                            _ModifyOption = ModifyOptions.Disabled;
                            break;
                    }
                }

                return _ModifyOption;
            }
        }
        
        public enum DeleteOptions { NotSet, Enabled, Disabled };
        private DeleteOptions _DeleteOption = DeleteOptions.NotSet;
        public DeleteOptions DeleteOption
        {
            get
            {
                if (_DeleteOption == DeleteOptions.NotSet)
                {
                    switch (int.Parse(this.TableRecord["DeleteOption_CustomTable"].ToString()))
                    {
                        case 1:
                            _DeleteOption = DeleteOptions.Enabled;
                            break;
                        default:
                            _DeleteOption = DeleteOptions.Disabled;
                            break;
                    }
                }

                return _DeleteOption;
            }
        }

        private System.Collections.Specialized.NameValueCollection _PostdataSource;
        public System.Collections.Specialized.NameValueCollection PostdataSource
        {
            get
            {
                if (_PostdataSource == null)
                {
                    _PostdataSource = HttpContext.Current.Request.Form;
                }
                return _PostdataSource;
            }
            set
            {
                _PostdataSource = value;
            }
        }

        protected int _ID;
        public int ID
        {
            get 
            {
                if (_ID == 0 && this.TableRecord != null)
                    _ID = int.Parse(this.TableRecord["id_CustomTable"].ToString());

                return _ID; 
            }
        }

        private string _Name;
        public string Name
        {
            get 
            {
                if (_Name == null)
                    _Name = this.TableRecord["Label_TableLabel"].ToString();
                return _Name;
            }
        }

        private int _SelectedGroupFilter = -1;
        private int SelectedGroupFilter
        {
            get
            {
                if (Categories != null)
                    return _SelectedGroupFilter;
                else
                    return -1;
            }
        }


        private bool _ShowGroups = false;
        private bool ShowGroups
        {
            get
            {
                return _ShowGroups;
            }
        }

        private HtmlGenericControl _CategoriesControl;
        private HtmlGenericControl CategoriesControl
        {
            get
            {
                if (_CategoriesControl == null)
                    InitCategories();
                return _CategoriesControl;
            }
        }

        private void InitCategories()
        {           
            NetCustomTableCategoriesBuilder ctbuilder = new NetCustomTableCategoriesBuilder(this, Conn,PostdataSource);
            _Categories = ctbuilder.Field;
            _CategoriesControl = ctbuilder.Controls;
            if (ctbuilder.SelectedCategory != null)
            {
                _Categories.Label = ctbuilder.SelectedCategory.NetField.Label;
                if (ctbuilder.SelectedCategory.HasJoinedFieldsGroups)
                    if (_Categories.RequestVariable.IsPostBack)
                        _SelectedGroupFilter = _Categories.RequestVariable.IntValue;
                    else
                        _SelectedGroupFilter = int.Parse( ctbuilder.SelectedCategoryID);
            }
        }

        private NetField _Categories;
        private NetField Categories
        {
            get
            {
                if (_Categories == null)
                {
                    InitCategories();
                }
                return _Categories;
            }
        }

        private NetCustomFieldsGroupCollection _Groups;
        public NetCustomFieldsGroupCollection Groups
        {
            get 
            {
                if (_Groups == null)
                {
                    _Groups = new NetCustomFieldsGroupCollection();
                    DataTable tab = Conn.SqlQuery("SELECT * FROM CustomTables_FieldsGroups WHERE Table_FieldsGroup = " + ID);
                    foreach (DataRow row in tab.Rows)
                    {
                        DataRow DataWidthLang = NetUtility.Languages.LanguagesUtility.LangRecordSelector(this.CurrentLang.ID,string.Format(NetCustomFieldsGroup.SqlSelectQuery,row["id_FieldsGroup"].ToString()),Conn);
                        NetCustomFieldsGroup group = new NetCustomFieldsGroup(DataWidthLang);
                        group.Table = this;
                        if(this.Conn!=null)
                            group.Conn = this.Conn;
                        if (this.ValueRecordID > 0)
                            group.ValueRecordID = this.ValueRecordID;
                        Groups.Add(group);
                    }
                }
                return _Groups; 
            }
        }

        private NetCustomFieldsRecordCollection _Records;
        public NetCustomFieldsRecordCollection Records
        {
            get
            {
                if (_Records == null)
                {
                    _Records = new NetCustomFieldsRecordCollection();
                    DataTable tab = Conn.SqlQuery("SELECT * FROM CustomTables_Records WHERE Table_Record = " + ID);
                    foreach (DataRow row in tab.Rows)
                    {
                        NetCustomFieldsRecord record = new NetCustomFieldsRecord(row,this.Conn);
                        record.Table = this;
                        _Records.Add(record);
                    }
                }
                return _Records;
            }
        }

        public NetCustomTable(DataRow tableRecord, NetCms.Connections.Connection conn,int RecordID)
            : this(tableRecord,conn)
        {
            this._ValueRecordID = RecordID;
            this._TableRecord = tableRecord;
            InitExternalFields();
        }
        public NetCustomTable(int id, NetCms.Connections.Connection conn, int RecordID)
            : base(conn)
        {
            _ID = id;
            this._ValueRecordID = RecordID;
            InitExternalFields();
        }
        public NetCustomTable(DataRow tableRecord,NetCms.Connections.Connection conn):base(conn)
        {
            this._TableRecord = tableRecord;
            InitExternalFields();
        }
        public NetCustomTable(int id, NetCms.Connections.Connection conn):base(conn)
        {
            _ID = id;
            InitExternalFields();
        }
        private void InitExternalFields()
        {
            foreach (NetCustomFieldsGroup group in this.Groups)
                foreach (NetCustomField field in group.Fields)
                    if(field.Type == NetCfExternalTable.FieldTypeID)
                    {
                        this.ExternalFields.Add(new NetCustomExternalField((NetCfExternalTable)field));
                    }
        }

        public override void DataBind(System.Collections.Specialized.NameValueCollection nameValueCollection)
        {            
            foreach (NetCustomFieldsGroup group in this.Groups)
                foreach (NetCustomField field in group.Fields)
                {
                    if (nameValueCollection[field.ControlID] != null)
                        field.CurrentValue = nameValueCollection[field.ControlID];
                }
            PostdataSource = nameValueCollection;
            if(!Categories.RequestVariable.IsValidInteger)
            if(int.TryParse(nameValueCollection[Categories.FieldName], out this._SelectedGroupFilter))
                Categories.Value = this.SelectedGroupFilter.ToString();
        }

        public HtmlGenericControl GetInsertView()
        {
            return GetControl();
        }
        public override HtmlGenericControl GetControl(params NetForm[] parentForm)
        {
            HtmlGenericControl Control = new HtmlGenericControl("div");
            
            if (this.GroupsFilterEnabled)
            {
                HtmlGenericControl set = new HtmlGenericControl("fieldset");
                HtmlGenericControl legend = new HtmlGenericControl("legend");
                set.Controls.Add(legend);
                legend.InnerHtml = this.TableLabel;
                set.Controls.Add(CategoriesControl);

                Control.Controls.Add(set);
            }
            else
            {
                _ShowGroups = true;
            }

            NetCustomTablePostBackResult esito = CheckPostback(this.ValueRecordID <= 0);
                       
            Control.Attributes["class"] = "CustomTable";
            if (this.CssClass != null && this.CssClass.Length > 0)
                Control.Attributes["class"] += " " + this.CssClass;

            if (this.InfoText != null && this.InfoText.Length > 0)
            {
                if (this.AddHeader || esito.Result == NetCustomTablePostBackResult.ResultTypes.FailValidation)
                {
                    HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset(esito.Result == NetCustomTablePostBackResult.ResultTypes.FailValidation ? "Errori di compilazione dei campi" : this.Name);
                    Control.Controls.Add(set);

                    if (this.AddHeader)
                    {
                        HtmlGenericControl info = new HtmlGenericControl("div");
                        info.InnerHtml = this.InfoText;

                        set.Controls.Add(info);
                    }
                    else if (esito.Result != NetCustomTablePostBackResult.ResultTypes.NotYetExecuted)
                    {
                        HtmlGenericControl postbackMsg = new HtmlGenericControl("ul");
                        postbackMsg.InnerHtml = esito.Message;
                        set.Controls.Add(postbackMsg);

                    }
                }
            }

            foreach (NetCustomFieldsGroup group in this.Groups)
                if (!this.GroupsFilterEnabled || group.Categories.Contains(SelectedGroupFilter.ToString()))
                    Control.Controls.Add(group.GetControl());

            if (AddSubmitButton)
            {
                HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset(this.NLabels[NetFormLabels.NetFormLabelsList.CompletamentoProcedura]);
                Control.Controls.Add(set);
                set.Controls.Add(SubmitButton);
            }

            return Control;
        }
        public HtmlGenericControl GetUpdateView()
        {
            return GetControl();
            
        }
        public HtmlGenericControl GetDetailControl()
        {
            HtmlGenericControl detailControl = new HtmlGenericControl("div");
            if(this.Records.Contains(this.ValueRecordID))
                detailControl.Controls.Add(this.Records[this.ValueRecordID.ToString()].GetDetailControl());

            return detailControl;
        }
        public HtmlGenericControl GetListControl()
        {
            HtmlGenericControl tableControl = new HtmlGenericControl("table");

            tableControl.Attributes["cellpadding"] = "0";
            tableControl.Attributes["cellspacing"] = "0";
            tableControl.Attributes["border"] = "1";
            
            bool first = true;
            foreach (NetCustomFieldsRecord record in this.Records)
            {
                if (first)
                {
                    first = !first;
                    tableControl.Controls.Add(record.GetHeadRowControl());
                }
                tableControl.Controls.Add(record.GetTableRowControl());
            }

            return tableControl;
        }
        public override HtmlGenericControl GetOverviewControl()
        {
            HtmlGenericControls.Fieldset control = new HtmlGenericControls.Fieldset(this.TableLabel);
            control.Class = "OverviewFieldset";
            
            foreach (NetCustomFieldsGroup group in this.Groups)
                if (!this.GroupsFilterEnabled || group.Categories.Contains(this.SelectedGroupFilter.ToString()))
                {
                    HtmlGenericControl gtitle = new HtmlGenericControl("p");
                    control.Controls.Add(gtitle);
                    gtitle.InnerHtml = "<em>" + group.Label + "</em>";

                    HtmlGenericControl list = new HtmlGenericControl("ul");
                    control.Controls.Add(list);

                    if (!this.GroupsFilterEnabled)
                    {
                        DataTable tab = Conn.SqlQuery("SELECT * FROM customtables_fieldsgroupscategoriesvalues WHERE Record_CategoryValue = "+this.ValueRecordID);
                        if (tab.Rows.Count > 0)
                        {
                            int catid = int.Parse(tab.Rows[0]["Category_CategoryValue"].ToString());
                            NetCustomTableCategory cat = new NetCustomTableCategory(this.Conn,this, catid);
                            
                            while (cat != null)
                            {
                                HtmlGenericControl li = new HtmlGenericControl("li");
                                list.Controls.AddAt(0,li);
                                li.InnerHtml = "<strong>" + cat.NetField.Label + ": </strong>" + cat.Label;
                                cat = cat.ParentCategory;
                            }
                        }
                    }

                    foreach (NetField field in group.Fields)
                    {
                        HtmlGenericControl overview = field.GetOverviewControl();
                        if (overview != null)
                            list.Controls.Add(overview);
                    }
                }

            return control;
        }
        private NetCustomTablePostBackResult CheckPostback(bool DoInsert)
        {
            NetCustomTablePostBackResult esito = new NetCustomTablePostBackResult();
            NetUtility.RequestVariable SubmitRequest = new NetUtility.RequestVariable(this.SubmitButton.ID);
            if (SubmitRequest.IsValidString && SubmitRequest.StringValue == this.SubmitButtonLabel)
            {
                esito.Message = this.ValidateInput();
                if (esito.Message == string.Empty)
                {
                    if(DoInsert)
                        esito.Result = Insert() ? NetCustomTablePostBackResult.ResultTypes.Succeded : NetCustomTablePostBackResult.ResultTypes.FailSqlExecution;
                    else
                        esito.Result = Update(this.ValueRecordID) ? NetCustomTablePostBackResult.ResultTypes.Succeded : NetCustomTablePostBackResult.ResultTypes.FailSqlExecution;
                }
                else
                    esito.Result = NetCustomTablePostBackResult.ResultTypes.FailValidation;
            }
            else
                esito.Result = NetCustomTablePostBackResult.ResultTypes.NotYetExecuted;

            return esito;
        }

        public override string ValidateInput(params NetForm[] parentForm)
        {
            string validity = "";
            if (this.GroupsFilterEnabled && this.SelectedGroupFilter <= 0)
                validity = "<li>Il campo <strong>" + this.Categories.Label + "</strong> � obbligatorio</li>";
            else
            {
                foreach (NetCustomFieldsGroup group in this.Groups)
                    if (!this.GroupsFilterEnabled || group.Categories.Contains(SelectedGroupFilter.ToString()))
                        validity += group.ValidateInput();
            }
            return validity;
        }

        public override bool Insert()
        {
            this._LastInsert = this.Conn.ExecuteInsert(this.InsertSql);
            bool ok = this.LastInsert > 0;
            if (ok)
            {
                foreach (NetCustomFieldsGroup group in this.Groups)
                    ok &= group.Insert(this.ValueRecordID);
                    
            }
            if (ok)
            {
                if (this.GroupsFilterEnabled)
                {
                    NetUtility.RequestVariable OldCategoryRequest = new NetUtility.RequestVariable(this.Categories.FieldName, this.PostdataSource);
                    string sql = "INSERT INTO customtables_fieldsgroupscategoriesvalues (Record_CategoryValue,Category_CategoryValue) VALUES (" + this.LastInsert + "," + OldCategoryRequest.StringValue + ")";
                    this.Conn.Execute(sql);
                }
                /*
                foreach (NetExternalField field in this.ExternalFields)
                {
                    string sql = "INSERT INTO CustomTables_FieldsValues (Record_FieldValue,Value_FieldValue,Field_FieldValue) VALUES (" + this.LastInsert + "," + field.Value + "," + field.FieldName + ")";
                    this.Conn.Execute(sql);
                }*/
            }

            
            return ok;
        }
        public override bool Insert(System.Collections.Specialized.NameValueCollection PostData)
        {
            this.PostdataSource = PostData;
            return Insert();
        }
        public bool Delete(int RecordID)
        {
            bool ok = true;
            foreach (NetCustomFieldsGroup group in this.Groups)
                ok &= group.Delete(RecordID);
            return ok;       
        }
        public override bool Update()
        {
            return this.Update(this.ValueRecordID);
        }
        public override bool Update(int RecordID)
        {
            bool ok = true;
            foreach (NetCustomFieldsGroup group in this.Groups)
                ok &= group.Update(RecordID);
            return ok;
        }
        public override bool Update(System.Collections.Specialized.NameValueCollection PostData)
        {
            this.PostdataSource = PostData;
            return Update();
        }
        public override bool Update(int RecordID, System.Collections.Specialized.NameValueCollection PostData)
        {
            this.PostdataSource = PostData;
            return Update(RecordID);
        }
        public string InsertSql
        {
            get
            {
                return "INSERT INTO CustomTables_Records (Table_Record) Values(" + this.ID + ")";
            }
        }
    }
    public struct NetCustomTablePostBackResult
    {
        private NetFormLabels NLabels
        {
            get
            {
                return NetUtility.LabelsCacher.NetFormLabels;
            }
        }

        private ResultTypes _Result;
        public ResultTypes Result
        {
            get { return _Result; }
            set { _Result = value; }
        }

        public enum ResultTypes
        {
            NotYetExecuted = 0,
            Succeded = 1,
            FailValidation = 2,
            FailSqlExecution = 3 
        }

        private string _Message;
        public string Message
        {
            get 
            {
                if (Result == ResultTypes.Succeded)
                    return "<li>"+this.NLabels[NetFormLabels.NetFormLabelsList.OperazioneCompletata]+"</li>";
                return _Message; 
            }
            set { _Message = value; }
        }
	
    }

}