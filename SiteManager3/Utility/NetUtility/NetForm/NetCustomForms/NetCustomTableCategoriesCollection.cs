using System;
using System.Data;
using System.Collections;

namespace NetForms.Custom
{
    public class NetCustomTableCategoriesCollection : IEnumerable
    {
        private NetCollection coll;
        public int Count { get { return coll.Count; } }
        NetCustomTableCategory Owner;
        public NetCustomTableCategoriesCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            Owner = null;
            coll = new NetCollection();
        }
        public NetCustomTableCategoriesCollection(NetCustomTableCategory field)
        {
            //
            // TODO: Add constructor logic here
            //
            Owner = field;
            coll = new NetCollection();
        }
        
        public void Add(NetCustomTableCategory field, string key)
        {
            coll.Add(key, field);
            field.ParentCategory = Owner;
        }
        public void Add(NetCustomTableCategory field)
        {
            field.ParentCategory = Owner;
            coll.Add("Category" + field.ID, field);
        }

        public void Remove(int ID)
        {
            coll.Remove("Category" + ID);
        }

        public void Clear()
        {
            coll.Clear();
        }

        public NetCustomTableCategory this[int i]
        {
            get
            {
                NetCustomTableCategory str = (NetCustomTableCategory)coll[coll.Keys[i]];
                return str;
            }
        }

        public NetCustomTableCategory this[string ID]
        {
            get
            {
                NetCustomTableCategory val = (NetCustomTableCategory)coll["Field" + ID];
                return val;
            }
        }

        public bool Contains(int ID)
        {
            return coll["Category" + ID] != null;
        }

        public bool HasJoinedFieldsGroups
        {
            get
            {
                foreach (NetCustomTableCategory cat in this)
                    if (cat.HasJoinedFieldsGroups) return true;
                return false;
            }
        }

        public NetCustomTableCategory Find(string nodeid)
        {
            NetCustomTableCategory result = null;

            result = this["Category" + nodeid];

            if (result == null)
            {
                for (int i = 0; i < this.Count && result == null; i++)
                    result = this[i].FindNode(nodeid);
            }

            return result;
        }
        
        public int MaxDepth
        {
            get
            {
                int depth = 0;
                if (this.Count > 0)
                {
                    depth = this[0].Depth;
                    foreach (NetCustomTableCategory cat in this)
                        depth = cat.Childs.MaxDepth>depth?cat.Childs.MaxDepth:depth;
                }
                return depth;
            }
        }


        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private NetCustomTableCategoriesCollection Collection;
            public CollectionEnumerator(NetCustomTableCategoriesCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}