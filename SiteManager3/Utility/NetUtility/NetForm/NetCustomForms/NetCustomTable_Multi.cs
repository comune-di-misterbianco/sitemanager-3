using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom
{
    public class NetCustomTable_Multi : NetCustomTable
    {
        public NetCustomTable_Multi(string TableName,int id, NetCms.Connections.Connection conn, int RecordID)
            : base(id, conn, RecordID)
        {
        }

        public NetCustomTable_Multi(int id, NetCms.Connections.Connection conn)
            : base(id, conn)
        {
        }
    }
}