using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom
{
    public class NetCustomTableCategory
    {
        protected int _DefaultLangID;
        public int DefaultLangID
        {
            get
            {
                if (_DefaultLangID == 0)
                {
                    DataTable tab = this.Conn.SqlQuery("SELECT * FROM Lang WHERE Default_Lang = 1");
                    if (tab.Rows.Count > 0)
                    {
                        _DefaultLangID = int.Parse(tab.Rows[0]["id_Lang"].ToString());
                    }
                }
                return _DefaultLangID;
            }
        }

        private NetUtility.Languages.LanguageData _CurrentLang;
        protected NetUtility.Languages.LanguageData CurrentLang
        {
            get
            {
                if (_CurrentLang == null)
                {
                    _CurrentLang = new NetUtility.Languages.LanguageData(this.Conn);
                }
                return _CurrentLang;
            }
        }

        private string sqlQueryModel;
        private string SqlQueryModel
        {
            get
            {
                if (sqlQueryModel == null)
                {
                    sqlQueryModel = "SELECT * FROM customtables_fieldsgroupscategorylabels";
                    sqlQueryModel += " INNER JOIN customtables_fieldsgroupscategories on category_fieldsgroupcategorylabel = id_fieldsgroupscategory ";
                    sqlQueryModel += " WHERE Lang_FieldsGroupCategoryLabel = " + this.DefaultLangID + "";
                }
                return sqlQueryModel;
            }
        }

        private NetCms.Connections.Connection _Conn;
        private NetCms.Connections.Connection Conn
        {
            get { return _Conn; }
        }	

        private DataRow _Data;
        private DataRow Data
        {
            get { return _Data; }
        }

        private string _Label;
        public string Label
        {
            get { return _Label; }
        }

        private string _FieldLabel;
        public string FieldLabel
        {
            get 
            {
                if (_FieldLabel == null)
                {
                    _FieldLabel = "";
                    DataTable table = Conn.SqlQuery("SElECT * FROM customtables_fieldsgroupscategorylevellabels");
                    DataRow[] rows = table.Select(" Lang_LevelLabel = "+this.CurrentLang.ID+" AND Table_LevelLabel = "+this.Table+" AND Depth_LevelLabel = "+this.Depth+1);
                    if (rows.Length > 0)
                        _FieldLabel = rows[0]["Label_LevelLabel"].ToString();
                }
                return _FieldLabel; 
            }
        }
        
        private string _MyLevelLabel;
        public string MyLevelLabel
        {
            get
            {
                if (_MyLevelLabel == null)
                {
                    _MyLevelLabel = "";
                    DataTable table = Conn.SqlQuery("SElECT * FROM customtables_fieldsgroupscategorylevellabels");
                    DataRow[] rows = table.Select(" Lang_LevelLabel = " + this.CurrentLang.ID + " AND Table_LevelLabel = " + this.Table + " AND Depth_LevelLabel = " + this.Depth);
                    if (rows.Length > 0)
                        _MyLevelLabel = rows[0]["Label_LevelLabel"].ToString();
                }
                return _MyLevelLabel;
            }
        }	

        private NetCustomTableCategory _ParentCategory;
        public NetCustomTableCategory ParentCategory
        {
            get
            {
                if (_ParentCategory == null && ParentID>0)
                    _ParentCategory = new NetCustomTableCategory(Conn, this.OwnerTable, this.ParentID);
                return _ParentCategory;
            }
            set
            {
                if (_ParentCategory == null)
                    _ParentCategory = value;
                else
                {
                    Exception ex = new Exception("Propriet� parent gi� inizializzata");
                    /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                    ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message, "", "", "90");
                }
            }
        }

        private int _ParentID;
        public int ParentID
        {
            get
            {
                return _ParentID;
            }
        }

        private int _Depth;
        public int Depth
        {
            get
            {
                return _Depth;
            }
        }

        private int _Table;
        public int Table
        {
            get
            {
                return _Table;
            }
        }

        private int _JoinedFieldsGroups = -1;
        public bool HasJoinedFieldsGroups
        {
            get 
            {
                if (_JoinedFieldsGroups == -1)
                {
                    DataTable table = Conn.SqlQuery("SELECT * FROM customtables_fieldsgroupscategoriesjoins WHERE Category_GroupCategoryJoin = " + this.ID);
                    _JoinedFieldsGroups = table.Rows.Count;
                }
                return _JoinedFieldsGroups>0; 
            }
        }

        private NetCustomTable ownerTable;
        private NetCustomTable OwnerTable
        {
            get
            {
                return ownerTable;
            }
        }

        private NetCustomTableCategoriesCollection _Childs;
        public NetCustomTableCategoriesCollection Childs
        {
            get
            {
                if (_Childs == null)
                {
                        _Childs = new NetCustomTableCategoriesCollection(this);
                    DataTable tab = Conn.SqlQuery(this.SqlQueryModel + " AND parent_fieldsgroupscategory = " + this.ID);
                    if (tab.Rows.Count > 0)
                    {
                        _Childs = new NetCustomTableCategoriesCollection(this);
                        foreach (DataRow row in tab.Rows)
                            _Childs.Add(new NetCustomTableCategory(Conn, this.OwnerTable, row));
                    }
                }
                return _Childs;
            }
        }

        private int _ID;
        public int ID
        {
            get { return _ID; }
        }
	
        public NetCustomTableCategory(NetCms.Connections.Connection conn, NetCustomTable table, int id)
        {
            ownerTable = table;
            _Conn = conn;
            DataTable tab = Conn.SqlQuery(this.SqlQueryModel + " AND id_fieldsgroupscategory = " + id);
            _Data = tab.Rows[0];
            InitData();
        }
        public NetCustomTableCategory(NetCms.Connections.Connection conn, NetCustomTable table, DataRow row)
        {
            ownerTable = table;
            _Conn = conn;
            _Data = row;
            InitData();
        }
        public NetCustomTableCategory(NetCms.Connections.Connection conn, NetCustomTable table, int id, NetCustomTableCategory parent)
        {
            ownerTable = table;
            _Conn = conn;
            DataTable tab = Conn.SqlQuery(this.SqlQueryModel + " AND id_fieldsgroupscategory = " + id);
            _Data = tab.Rows[0];
            _ParentCategory = parent;
            InitData();
        }
        public NetCustomTableCategory(NetCms.Connections.Connection conn, NetCustomTable table, DataRow row, NetCustomTableCategory parent)
        {
            ownerTable = table;
            _Conn = conn;
            _ParentCategory = parent;
            _Data = row;
            InitData();
        }

        private void InitData()
        {
            _ID = int.Parse(this.Data["id_fieldsgroupscategory"].ToString());
            _ParentID = int.Parse(this.Data["parent_fieldsgroupscategory"].ToString());
            _Depth = int.Parse(this.Data["depth_fieldsgroupscategory"].ToString());
            _Label = this.Data["Label_fieldsgroupcategorylabel"].ToString();
            _Table = int.Parse(this.Data["table_fieldsgroupscategory"].ToString());
        }

        private NetField _NetField;
        public NetField NetField
        {
            get
            {
                if (_NetField == null)
                {
                    string selectedid = "";

                    if(OwnerTable!=null && OwnerTable.ValueRecordID>0)
                    {
                        DataTable table = this.Conn.SqlQuery("SELECT * FROM customtables_fieldsgroupscategoriesvalues WHERE Record_CategoryValue = " + OwnerTable.ValueRecordID);
                        if (table.Rows.Count > 0)
                            selectedid = table.Rows[0]["id_CategoryValue"].ToString();
                    }

                    NetDropDownList ddl = new NetDropDownList(this.FieldLabel, "Livello" + (this.Depth+1));

                    foreach (NetCustomTableCategory category in this.Childs)
                        if (category.HasJoinedFieldsGroups)
                        {
                            ListItem item = new ListItem(category.Label, category.ID.ToString());
                            ddl.addItem(item);
                            if(selectedid == category.ID.ToString()) item.Selected = true;
                        }

                    ddl.AutoPostBack = true;
                    ddl.OnClientClick = "javascript: setFormValueByDDL('" + ddl.FieldName + "','RefreshButton',1)";
                    ddl.Required = true;

                    _NetField = ddl;
                }
                return _NetField;
            }
        }

        public NetCustomTableCategory FindNode(string nodeid)
        {
            return this.ID.ToString() ==nodeid? this: this.Childs.Find(nodeid);
        }

        public NetCustomTableCategory FindParent(string nodeid)
        {
            NetCustomTableCategory result = null;

            if (this.ParentCategory != null)
                result = this.ParentCategory.ID.ToString() == nodeid ? this.ParentCategory : this.ParentCategory.FindParent(nodeid);

            return result;
        }
    }
}