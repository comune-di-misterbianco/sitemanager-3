using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom
{
    public class NetCustomFieldsGroup
    {
        public const string SqlSelectQuery = "SELECT * FROM CustomTables_FieldsGroups INNE JOIN CustomTables_FieldsGroupsLabels ON id_FieldsGroup = Group_FieldsGroupLabel WHERE Lang_FieldsGroupLabel = [LANG] AND id_FieldsGroup = {0}";
        
        private string _CssClass;
        public string CssClass
        {
            get 
            {
                if (_CssClass == null)
                    _CssClass = this.GroupRecord["CssClass_FieldsGroup"].ToString();
                return _CssClass; 
            }
            set { _CssClass = value; }
        }

        private NetUtility.Languages.LanguageData _CurrentLang;
        private NetUtility.Languages.LanguageData CurrentLang
        {
            get
            {
                if (_CurrentLang == null)
                {
                    _CurrentLang = new NetUtility.Languages.LanguageData(this.Conn);
                }
                return _CurrentLang;
            }
        }
	
        private DataRow _GroupRecord;
        protected DataRow GroupRecord
        {
            get 
            {
                if (_GroupRecord == null && Conn != null && _ID > 0)
                {
                    _GroupRecord = NetUtility.Languages.LanguagesUtility.LangRecordSelector(this.CurrentLang.ID, string.Format(SqlSelectQuery,this.ID), Conn);
                }
                return _GroupRecord; 
            }
        }	

        private string _InfoText;
        public string InfoText
        {
            get 
            {
                if (_InfoText == null)
                {
                    _InfoText = this.GroupRecord["InfoText_FieldsGroupLabel"].ToString();
                }
                return _InfoText; 
            }
        }

        protected int _ID;
        public int ID
        {
            get 
            {
                if(_ID == 0 )
                    if( this.GroupRecord!=null )
                    _ID = int.Parse(this.GroupRecord["id_FieldsGroup"].ToString());

                return _ID; 
            }
        }

        protected int _ValueRecordID;
        public int ValueRecordID
        {
            set
            {
                _ValueRecordID = value;
                foreach (NetCustomField field in this.Fields)
                    field.ValueRecordID = value;
            }
            get { return _ValueRecordID; }
        }

        private string _Label;
        public string Label
        {
            get 
            {
                if (_Label == null)
                    _Label = this.GroupRecord["Label_FieldsGroupLabel"].ToString();
                return _Label; }
        }

        /*private int _Category = -1;
        public int Category
        {
            get
            {
                if (_Category == -1)
                    _Category = int.Parse(this.GroupRecord["Category_FieldGroup"].ToString());
                return _Category;
            }
        }*/

        private StringCollection _Categories;
        public StringCollection Categories
        {
            get
            {
                if (_Categories == null)
                {
                    _Categories = new StringCollection();
                    DataTable table = Conn.SqlQuery("SELECT * FROM customtables_fieldsgroupscategoriesjoins WHERE Group_GroupCategoryJoin = "+this.ID);
                    foreach(DataRow row in table.Rows)
                        _Categories.Add(row["Category_GroupCategoryJoin"].ToString());
                }
                return _Categories;
            }
        }

        private NetCustomTable _Table;
        public NetCustomTable Table
        {
            get 
            {
                if (_Table == null)
                {
                    _Table = new NetCustomTable(int.Parse(this.GroupRecord["Table_FieldsGroup"].ToString()), Conn);
                }
                return _Table; 
            }
            set { _Table = value; }
        }

        private NetCustomFieldCollection _Fields;
        public NetCustomFieldCollection Fields
        {
            get 
            {
                if (_Fields == null)
                {
                    _Fields = new NetCustomFieldCollection();

                    string sql = "SELECT * FROM CustomTables_Fields";
                    sql += " INNER JOIN CustomTables_FieldsLabels ON Field_FieldLabel = id_CustomField ";
                    sql += " INNER JOIN CustomTables_FieldsJoins ON Field_FieldJoin = id_CustomField ";
                    sql += " INNER JOIN CustomTables_FieldsTypes ON id_FieldsType = Type_CustomField ";
                    sql += " WHERE Lang_FieldLabel = " + this.Table.DefaultLangID + "";
                    sql += " AND   Group_FieldJoin = " + ID+"";
                    sql += " ORDER BY Order_CustomField,id_CustomField";

                    DataTable tab = Conn.SqlQuery(sql);
                    foreach (DataRow row in tab.Rows)
                    {
                        DataRow LangFieldData = NetUtility.Languages.LanguagesUtility.LangRecordSelector(this.CurrentLang.ID, string.Format(NetCustomField.SqlSelectQuery, row["id_CustomField"]), Conn);
                        NetCustomField field = NetCustomField.FieldFactory(LangFieldData);
                        field.FieldsGroup = this;
                        if(this.Conn!=null)
                            field.Conn = this.Conn;
                        if (this.ValueRecordID > 0)
                            field.ValueRecordID = this.ValueRecordID;
                        Fields.Add(field);
                    }
                }
                return _Fields; 
            }
        }
	
        private NetCms.Connections.Connection _Conn;
        public NetCms.Connections.Connection Conn
        {
            get
            {
                return _Conn;
            }
            set
            {
                _Conn = value;
            }
        }

        public NetCustomFieldsGroup(DataRow groupRecord)
        {
            _GroupRecord = groupRecord;
        }
        public NetCustomFieldsGroup(int id, NetCms.Connections.Connection conn)
        {
            _Conn = conn;
            _ID = id;
        }

        public HtmlGenericControl GetControl()
        {
            HtmlGenericControl Control = new HtmlGenericControl("fieldset");
            Control.Attributes["class"] = "CustomTable_Fieldset " + this.CssClass;
            
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerHtml = this.Label;
            Control.Controls.Add(legend);

            if (this.InfoText != null && this.InfoText.Length > 0)
            {
                HtmlGenericControl info = new HtmlGenericControl("div");
                info.InnerHtml = this.InfoText;
                Control.Controls.Add(info);
            }

            foreach (NetCustomField field in this.Fields)
                Control.Controls.Add(field.getControl());            

            return Control;
        }
        
        public string ValidateInput()
        {
            string validity = "";
            foreach (NetCustomField field in this.Fields)
                validity += field.ValidateInput().Message;
            return validity;
        }

        public virtual bool Insert(int RecordID)
        {
            bool ok = true;
            foreach (NetCustomField field in this.Fields)
                ok &= field.Insert(RecordID);
            return ok;
        }
        public virtual bool Delete(int RecordID)
        {
            bool ok = true;
            foreach (NetCustomField field in this.Fields)
                ok &= field.Delete(RecordID);
            return ok;       
        }
        public virtual bool Update(int RecordID)
        {
            bool ok = true;
            foreach (NetCustomField field in this.Fields)
                ok &= field.Update(RecordID);
            return ok;
        }
    }

}