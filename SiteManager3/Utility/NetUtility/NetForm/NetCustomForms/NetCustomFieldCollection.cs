using System;
using System.Collections;

namespace NetForms.Custom
{
    public class NetCustomFieldCollection : IEnumerable
    {
        private NetCollection coll;
        public int Count { get { return coll.Count; } }
        public NetCustomFieldCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new NetCollection();
        }

        public void Add(NetCustomField field, string key)
        {
            coll.Add(key, field);
        }
        public void Add(NetCustomField field)
        {
            coll.Add("Field" + field.ID, field);
        }

        public void Remove(int ID)
        {
            coll.Remove("Field" + ID);
        }

        public void Clear()
        {
            coll.Clear();
        }

        public NetCustomField this[int i]
        {
            get
            {
                NetCustomField str = (NetCustomField)coll[coll.Keys[i]];
                return str;
            }
        }

        public NetCustomField this[string ID]
        {
            get
            {
                NetCustomField val = (NetCustomField)coll["Field" + ID];
                return val;
            }
        }

        public bool Contains(int ID)
        {
            return coll["Field" + ID] != null;
        }

        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private NetCustomFieldCollection Collection;
            public CollectionEnumerator(NetCustomFieldCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}