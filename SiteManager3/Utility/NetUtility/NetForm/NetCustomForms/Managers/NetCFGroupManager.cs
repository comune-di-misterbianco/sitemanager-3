using System;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NetTable;
using NetForms;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom.Manager
{
    public class NetCFGroupManager
    {
        private NetCms.Connections.Connection _Conn;
        private NetCms.Connections.Connection Conn
        {
            get
            {
                return _Conn;
            }
        }
        
        protected int _DefaultLangID;
        protected int DefaultLangID
        {
            get
            {
                if (_DefaultLangID == 0)
                {
                    DataTable tab = this.Conn.SqlQuery("SELECT * FROM Lang WHERE Default_Lang = 1");
                    if (tab.Rows.Count > 0)
                    {
                        _DefaultLangID = int.Parse(tab.Rows[0]["id_Lang"].ToString());
                    }
                }
                return _DefaultLangID;
            }
        }

        protected HtmlGenericControls.InputField _SelectedLangField;
        protected HtmlGenericControls.InputField SelectedLangField
        {
            get
            {
                if (_SelectedLangField == null)
                    _SelectedLangField = new HtmlGenericControls.InputField("SelectedLang");
                return _SelectedLangField;
            }
        }

        protected int _SelectedLangID;
        protected int SelectedLangID
        {
            get
            {
                if (_SelectedLangID == 0 && SelectedLangField.RequestVariable.IsValidInteger)
                    _SelectedLangID = SelectedLangField.RequestVariable.IntValue;
                if (_SelectedLangID == 0)
                {
                    _SelectedLangID = DefaultLangID;
                }
                return _SelectedLangID;
            }
        }

        private NetUtility.RequestVariable _TableRequest;
        public NetUtility.RequestVariable TableRequest
        {
            get 
            {
                if (_TableRequest == null)
                {
                    _TableRequest = new NetUtility.RequestVariable("tbid", NetUtility.RequestVariable.RequestType.QueryString);                    
                }
                return _TableRequest; 
            }
        }

        private NetUtility.RequestVariable _GroupRequest;
        public NetUtility.RequestVariable GroupRequest
        {
            get
            {
                if (_GroupRequest == null)
                {
                    _GroupRequest = new NetUtility.RequestVariable("grid", NetUtility.RequestVariable.RequestType.QueryString);
                }
                return _GroupRequest;
            }
        }

        private NetFormTable customGroupForm;
        private NetFormTable CustomGroupForm
        {
            get
            {
                if (customGroupForm == null)
                {
                    customGroupForm = new NetFormTable("CustomTables_FieldsGroups", "id_FieldsGroup", this.Conn);

                    if (!GroupRequest.IsValidInteger)
                    {
                        if (TableRequest.IsValidInteger)
                        {
                            NetHiddenField table = new NetHiddenField("Table_FieldsGroup");
                            table.Value = this.TableRequest.StringValue;
                            customGroupForm.addField(table);
                        }
                        else
                        {
                            NetDropDownList table = new NetDropDownList("Tabella", "Table_FieldsGroup");
                            table.Required = true;
                            table.setDataBind("SELECT Label_TableLabel,id_CustomTable FROM CustomTables_Tables INNER JOIN CustomTables_TablesLabels ON id_CustomTable = Table_TableLabels WHERE Lang_TableLabel = " + this.DefaultLangID + " ORDER BY Nome_CustomTable", this.Conn);
                            customGroupForm.addField(table);
                        }
                    }

                    NetTextBoxPlus cssclass = new NetTextBoxPlus("Classe CSS", "CssClass_FieldsGroup");
                    cssclass.Rows = 4;
                    cssclass.Size = 70;
                    customGroupForm.addField(cssclass);

                    string sql = @"SELECT * FROM customtables_fieldsgroupscategorylabels";
                    sql += @" INNER JOIN customtables_fieldsgroupscategories on category_fieldsgroupcategorylabel = id_fieldsgroupscategory ";
                    sql += @" WHERE Lang_FieldsGroupCategoryLabel = {0} AND table_fieldsgroupscategory = {1}";

                    DataTable Cats;
                    NetCustomFieldsGroup group = new NetCustomFieldsGroup(this.GroupRequest.IntValue, Conn);
                    if (!this.GroupRequest.IsValidInteger)
                        group.Table = new NetCustomTable(this.TableRequest.IntValue, Conn);
                    Cats = Conn.SqlQuery(string.Format(sql, this.DefaultLangID, group.Table.ID));

                }
                return customGroupForm;
            }
        }

        protected NetFormTable CustomGroupLabelsForm()
        {
            return CustomGroupLabelsForm(true);
        }
        protected NetFormTable CustomGroupLabelsForm(bool addLang)
        {
            NetFormTable table = new NetFormTable("CustomTables_FieldsGroupsLabels", "id_FieldsGroupLabel", this.Conn);

                NetTextBoxPlus nome = new NetTextBoxPlus("Label", "Label_FieldsGrouplabel");
                nome.Size = 70;
                nome.Required = true;
                table.addField(nome);

                NetTextBoxPlus info = new NetTextBoxPlus("Testo Informativo", "Infotext_FieldsGrouplabel");
                info.Rows = 4;
                info.Size = 70;
                table.addField(info);

                if (addLang)
                {
                    NetHiddenField lang = new NetHiddenField("Lang_FieldsGrouplabel");
                    lang.Value = this.SelectedLangID.ToString();
                    table.addField(lang);
                }

                return table;
        }

        public NetCFGroupManager(NetCms.Connections.Connection conn)
        {
            _Conn = conn;
        }

        public HtmlGenericControl DefaultView()
        {
            HtmlGenericControl Div = new HtmlGenericControl("div");

            if (!TableRequest.IsValidInteger)
               NetCms.Diagnostics.Diagnostics.Redirect("../default.aspx");

            HtmlGenericControls.InputField deleteField = new HtmlGenericControls.InputField("DeleteField");
            if (deleteField.RequestVariable.IsValidInteger)
                Conn.Execute("DELETE FROM CustomTables_FieldsGroups WHERE id_FieldsGroup = " + deleteField.RequestVariable.IntValue);
            Div.Controls.Add(deleteField.Control);


            NetTable.SmTable table = new SmTable("SELECT * FROM CustomTables_FieldsGroups INNER JOIN CustomTables_FieldsGroupsLabels ON id_FieldsGroup = Group_FieldsGroupLabel WHERE Lang_FieldsGroupLabel = " + this.DefaultLangID + " AND Table_FieldsGroup = " + this.TableRequest.IntValue, this.Conn);
            table.AddPaginationControl = true;
            table.RecordPerPagina = 20;

            SmTableColumn col = new SmTableColumn("Label_FieldsGroupLabel", "Label");
            table.addColumn(col);

            col = new SmTableColumn("CssClass_FieldsGroup", "Classe CSS");
            table.addColumn(col);

            col = new SmTableActionColumn("id_FieldsGroup", "action details", "Gestisci Campi", "../fields/default.aspx?grid={0}");
            table.addColumn(col);

           // col = new SmTableActionColumn("id_FieldsGroup", "action modify", "Aggiungi Campo", "../fields/fields_new.aspx?grid={0}");
           // table.addColumn(col);

            col = new SmTableActionColumn("id_FieldsGroup", "action modify", "Aggiungi Etichetta", "groups_add_labels.aspx?grid={0}");
            table.addColumn(col);

            col = new SmTableActionColumn("id_FieldsGroup", "action modify", "Modifica Etichette", "groups_mod_labels.aspx?grid={0}");
            table.addColumn(col);
            
            col = new SmTableActionColumn("id_FieldsGroup", "action modify", "Modifica ProprietÓ", "groups_mod.aspx?grid={0}");
            table.addColumn(col);
            
            col = new SmTableActionColumn("id_FieldsGroup", "action modify", "Modifica Categorie", "groups_categories.aspx?grid={0}&amp;tbid="+this.TableRequest.IntValue);
            table.addColumn(col);

            table.addColumn(new SmTableActionColumn("id_FieldsGroup", "action delete", "Elimina", "javascript: setFormValueConfirm({0},'" + deleteField.ID + "',1,'Sei Sicuro di eliminare questo gruppo?')"));

            Div.Controls.Add(table.getTable());

            return Div;
        }

        public HtmlGenericControl NewView()
        {         
            NetForm form = new NetForm();
            form.AddTable(this.CustomGroupForm);
            form.AddTable(this.CustomGroupLabelsForm());
            form.Tables[1].addExternalField(new NetExternalField("CustomTables_FieldsGroups", "Group_FieldsGroupLabel"));
            form.SubmitLabel = "Crea Gruppo di Campi";

            string errors = "";
            if (form.CheckSubmitSource)
            {
                errors = form.serializedInsert();
            }

            return form.getForms("Creazione Nuovo Gruppo di Campi", errors);
        }

        public HtmlGenericControl ModView()
        {
            if (!GroupRequest.IsValidInteger)
                return this.DefaultView();

            NetForm form = new NetForm();
            this.CustomGroupForm.ID = GroupRequest.IntValue;
            form.AddTable(this.CustomGroupForm);
            form.SubmitLabel = "Salva Gruppo di Campi";

            string errors = "";
            if (form.CheckSubmitSource)
            {
                errors = form.serializedUpdate();
            }

            return form.getForms("Modifica Gruppo di Campi", errors);
        }
        public HtmlGenericControl AddLabelsView()
        {
            if (!GroupRequest.IsValidInteger)
                return this.DefaultView();

            //Costruisco i contenitori
            HtmlGenericControls.Div container = new HtmlGenericControls.Div();
            HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset("Seleziona la ligua per modificare le etichette");
            container.Controls.Add(set);

            //Costruisco la lista con le possibili lingue
            DropDownList list = new DropDownList();
            list.ID = "AvailableLangs";
            list.Items.Add(new ListItem("", ""));
            list.AutoPostBack = true;
            set.Controls.Add(list);
            NetUtility.RequestVariable langRequest = new NetUtility.RequestVariable(list.ID);

            DataTable langsinstalled = this.Conn.SqlQuery("SELECT * FROM CustomTables_FieldsGroups INNER JOIN CustomTables_FieldsGroupsLabels ON id_FieldsGroup = Group_FieldsGroupLabel INNER JOIN Lang ON id_Lang = Lang_FieldsGroupLabel WHERE Group_FieldsGroupLabel = " + this.GroupRequest);
            string installed = ".";
            foreach (DataRow row in langsinstalled.Rows)
                installed += row["id_Lang"] + ".";

            DataTable langs = this.Conn.SqlQuery("SELECT * FROM Lang");
            foreach (DataRow row in langs.Rows)
            {
                if (!installed.Contains("." + row["id_Lang"] + "."))
                    list.Items.Add(new ListItem(row["Nome_Lang"].ToString(), row["id_Lang"].ToString()));
            }

            //Costruisco la NetForm
            NetForm form = new NetForm();
            form.SubmitLabel = "Aggiungi Etichetta del Gruppo di Campi";

            if (langRequest.IsValidInteger || form.CheckSubmitSource)
            {
                SelectedLangField.Value = langRequest.StringValue;
                
                list.Enabled = false;

                NetFormTable table = CustomGroupLabelsForm(true);
                if (form.CheckSubmitSource)
                    this._SelectedLangID = SelectedLangField.RequestVariable.IntValue;
                else
                    this._SelectedLangID = langRequest.IntValue;
                set.Controls.Add(SelectedLangField.Control);
                form.AddTable(table);

                NetHiddenField group = new NetHiddenField("Group_FieldsGrouplabel");
                group.Value = this.GroupRequest.StringValue;
                table.addField(group);

                string errors = "";
                if (form.CheckSubmitSource)
                {
                    errors = form.serializedInsert();
                }
                container.Controls.Add(form.getForms("Aggiungi Etichetta del Gruppo di Campi", errors));
            }

            return container;
        }

        public HtmlGenericControl ModLabelsView()
        {
            if (!GroupRequest.IsValidInteger)
                return this.DefaultView();

            //Costruisco i contenitori
            HtmlGenericControls.Div container = new HtmlGenericControls.Div();
            HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset("Seleziona la ligua per modificare le etichette");
            container.Controls.Add(set);

            //Costruisco la lista con le possibili lingue
            DropDownList list = new DropDownList();
            list.ID = "AvailableLangs";
            list.Items.Add(new ListItem("", ""));
            list.AutoPostBack = true;
            set.Controls.Add(list);
            NetUtility.RequestVariable langRequest = new NetUtility.RequestVariable(list.ID);
            
            DataTable langs = this.Conn.SqlQuery("SELECT * FROM CustomTables_FieldsGroups INNER JOIN CustomTables_FieldsGroupsLabels ON id_FieldsGroup = Group_FieldsGroupLabel INNER JOIN Lang ON id_Lang = Lang_FieldsGroupLabel WHERE Group_FieldsGroupLabel = " + this.GroupRequest);
            
            foreach (DataRow row in langs.Rows)
            {
                list.Items.Add(new ListItem(row["Nome_Lang"].ToString(), row["id_FieldsGroupLabel"].ToString()));
            }

            //Costruisco la NetForm
            NetForm form = new NetForm();
            form.SubmitLabel = "Salva Etichetta del Gruppo di Campi";

            if (langRequest.IsValidInteger || form.CheckSubmitSource)
            {
                SelectedLangField.Value = langRequest.StringValue;
                set.Controls.Add(SelectedLangField.Control);
                list.Enabled = false;

                NetFormTable table = CustomGroupLabelsForm(false);
                if (form.CheckSubmitSource)
                    table.ID = SelectedLangField.RequestVariable.IntValue;
                else
                    table.ID = langRequest.IntValue;
                form.AddTable(table);

                string errors = "";
                if (form.CheckSubmitSource)
                {
                    errors = form.serializedUpdate();
                }
                container.Controls.Add(form.getForms("Modifica Etichetta del Gruppo di Campi", errors));
            }

            return container;
        }

        public HtmlGenericControl OptionsView()
        {
            if (!this.GroupRequest.IsValidInteger)
                return this.DefaultView();
            HtmlGenericControls.Div control = new HtmlGenericControls.Div();

            HtmlGenericControls.Fieldset addOption = new HtmlGenericControls.Fieldset("Aggiungi Categoria");

            NetFormTable table = new NetFormTable("customtables_fieldsgroupscategoriesjoins", "id_GroupCategoryJoin", this.Conn);

            NetHiddenField group = new NetHiddenField("Group_GroupCategoryJoin");
            group.Value = this.GroupRequest.StringValue;
            table.addField(group);

            NetDropDownList Categories = new NetDropDownList("Categoria", "Category_GroupCategoryJoin");
            Categories.Required = true;
            table.addField(Categories);

            NetCustomTableCategoriesBuilder builder = new NetCustomTableCategoriesBuilder(this.TableRequest.IntValue, this.Conn, HttpContext.Current.Request.Form);
            foreach (NetCustomTableCategory category in builder.Categories)
            {
                NetCFGroupsCategoriesManager.AddCategoryToDDL(Categories, category);
            }

            NetForm form = new NetForm();
            form.AddTable(table);
            form.SubmitLabel = "Aggiungi Opzione";

            string errors = "";
            if (form.CheckSubmitSource)
            {
                errors = form.serializedInsert();
            }
            control.Controls.Add(form.getForms("Aggiungi Categoria", errors));


            HtmlGenericControls.Fieldset viewOption = new HtmlGenericControls.Fieldset("Elenco Categorie");

            string sql = "SELECT * FROM customtables_fieldsgroupscategoriesjoins";
            sql+=" INNER JOIN customtables_fieldsgroupscategories ON Category_GroupCategoryJoin = id_fieldsgroupscategory";
            sql+=" INNER JOIN customtables_fieldsgroupscategorylabels ON id_fieldsgroupscategory = Category_FieldsGroupCategoryLabel ";
            sql += " WHERE Lang_FieldsGroupCategoryLabel = " + this.DefaultLangID + " AND Group_GroupCategoryJoin = " + this.GroupRequest;

            SmTable tab = new SmTable(sql, this.Conn);
            NetUtility.RequestVariable del = new NetUtility.RequestVariable(tab.DeleteFieldID);
            if (del.IsValidInteger)
                Conn.Execute("DELETE FROM customtables_fieldsgroupscategoriesjoins WHERE id_GroupCategoryJoin = " + del);

            tab.addColumn(new SmTableColumn("Label_FieldsGroupCategoryLabel", "Label"));
            tab.addColumn(new SmTableActionColumn("id_GroupCategoryJoin", "action delete", "Elimina", "javascript: setFormValueConfirm({0},'" + tab.DeleteFieldID + "',1,'Sei sicuro di voler eleiminare questa opzione?')"));
            viewOption.Controls.Add(tab.getTable(" ORDER BY Lang_FieldsGroupCategoryLabel"));
            control.Controls.Add(viewOption);

            return control;
        }
    }
}