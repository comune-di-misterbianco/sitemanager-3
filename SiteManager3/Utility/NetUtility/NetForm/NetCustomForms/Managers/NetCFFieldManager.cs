using System;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NetTable;
using NetForms;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom.Manager
{
    public abstract class NetCfFieldManager
    {
        private NetCms.Connections.Connection _Conn;
        protected NetCms.Connections.Connection Conn
        {
            get
            {
                return _Conn;
            }
        }

        private NetUtility.RequestVariable _TableRequest;
        protected NetUtility.RequestVariable TableRequest
        {
            get 
            {
                if (_TableRequest == null)
                {
                    _TableRequest = new NetUtility.RequestVariable("tbid", NetUtility.RequestVariable.RequestType.QueryString);                    
                }
                return _TableRequest; 
            }
        }

        private NetUtility.RequestVariable _GroupRequest;
        protected NetUtility.RequestVariable GroupRequest
        {
            get
            {
                if (_GroupRequest == null)
                {
                    _GroupRequest = new NetUtility.RequestVariable("grid", NetUtility.RequestVariable.RequestType.QueryString);
                }
                return _GroupRequest;
            }
        }

        private NetUtility.RequestVariable _FieldRequest;
        protected NetUtility.RequestVariable FieldRequest
        {
            get
            {
                if (_FieldRequest == null)
                {
                    _FieldRequest = new NetUtility.RequestVariable("fid", NetUtility.RequestVariable.RequestType.QueryString);
                }
                return _FieldRequest;
            }
        }

        private NetUtility.RequestVariable _TypeRequest;
        protected NetUtility.RequestVariable TypeRequest
        {
            get
            {
                if (_TypeRequest == null)
                {
                    _TypeRequest = new NetUtility.RequestVariable("type", NetUtility.RequestVariable.RequestType.QueryString);
                }
                return _TypeRequest;
            }
        }

        protected int _DefaultLangID;
        protected int DefaultLangID
        {
            get
            {
                if (_DefaultLangID == 0)
                {
                    DataTable tab = this.Conn.SqlQuery("SELECT * FROM Lang WHERE Default_Lang = 1");
                    if (tab.Rows.Count > 0)
                    {
                        _DefaultLangID = int.Parse(tab.Rows[0]["id_Lang"].ToString());
                    }
                }
                return _DefaultLangID;
            }
        }

        protected HtmlGenericControls.InputField _SelectedLangField;
        protected HtmlGenericControls.InputField SelectedLangField
        {
            get
            {
                if (_SelectedLangField == null)
                    _SelectedLangField = new HtmlGenericControls.InputField("SelectedLang");
                return _SelectedLangField;
            }
        }

        protected int _SelectedLangID;
        protected int SelectedLangID
        {
            get
            {
                if (_SelectedLangID == 0 && SelectedLangField.RequestVariable.IsValidInteger)
                    _SelectedLangID = SelectedLangField.RequestVariable.IntValue;
                if (_SelectedLangID == 0)
                {
                    _SelectedLangID = DefaultLangID;
                }
                return _SelectedLangID;
            }
        }

        private NetCustomField _Field;
        public NetCustomField Field
        {
            get
            {
                if (_Field == null && FieldRequest.IsValidInteger)
                {
                    DataTable tab = Conn.SqlQuery("SELECT * FROM CustomTables_Fields INNER JOIN CustomTables_FieldsJoins ON Field_FieldJoin = id_CustomField INNER JOIN  CustomTables_FieldsLabels ON Field_FieldLabel = id_CustomField INNER JOIN CustomTables_FieldsTypes ON id_FieldsType = Type_CustomField WHERE Lang_FieldLabel = " + this.DefaultLangID + " AND id_CustomField = " + this.FieldRequest);
                    if (tab.Rows.Count > 0)
                    {
                        _Field = NetCustomField.FieldFactory(tab.Rows[0]);
                        _Field.Conn = this.Conn;
                    }
                }
                return _Field;
            }
        }

        private NetFormTable customFieldForm;
        protected NetFormTable CustomFieldForm
        {
            get
            {
                if (customFieldForm == null)
                {
                    customFieldForm = BuildFieldForm(!FieldRequest.IsValidInteger);
                }
                return customFieldForm;
            }
        }

        private NetFormTable customFieldJoinForm;
        protected NetFormTable CustomFieldJoinForm
        {
            get
            {
                if (customFieldJoinForm == null)
                {
                    customFieldJoinForm = new NetFormTable("CustomTables_FieldsJoins", "id_FieldJoin", this.Conn);

                    NetHiddenField fieldgroup = new NetHiddenField("Group_FieldJoin");
                    fieldgroup.Value = this.GroupRequest.StringValue;
                    customFieldJoinForm.addField(fieldgroup);

                    NetExternalField field = new NetExternalField("CustomTables_Fields", "Field_FieldJoin");
                    customFieldJoinForm.addExternalField(field);

                    return customFieldJoinForm;
                }
                return customFieldJoinForm;
            }
        }


        protected abstract NetFormTable BuildFieldForm(bool InsertView);
        protected NetFormTable CustomFieldsLabelsForm(bool addLang)
        {
            NetFormTable table = new NetFormTable("CustomTables_FieldsLabels", "id_FieldLabel", this.Conn);

            NetTextBoxPlus nome = new NetTextBoxPlus("Label", "Label_Fieldlabel");
            nome.Size = 70;
            nome.Required = true;
            table.addField(nome);

            if (addLang)
            {
                NetHiddenField lang = new NetHiddenField("Lang_Fieldlabel");
                lang.Value = this.SelectedLangID.ToString();
                table.addField(lang);
            }

            return table;

        }

        public NetCfFieldManager(NetCms.Connections.Connection conn)
        {
            _Conn = conn;
        }

        public HtmlGenericControl DefaultView()
        {
            HtmlGenericControl Div = new HtmlGenericControl("div");

            if (!this.GroupRequest.IsValidInteger)
               NetCms.Diagnostics.Diagnostics.Redirect("../default.aspx");

            HtmlGenericControls.InputField deleteField = new HtmlGenericControls.InputField("DeleteField");
            if (deleteField.RequestVariable.IsValidInteger)
                Conn.Execute("DELETE FROM CustomTables_Fields WHERE id_CustomField = "+deleteField.RequestVariable.IntValue);
            Div.Controls.Add(deleteField.Control);

            string sql = "SELECT * FROM CustomTables_Fields";
            sql += " INNER JOIN CustomTables_FieldsJoins ON Field_FieldJoin = id_CustomField ";
            sql += " INNER JOIN CustomTables_FieldsLabels ON id_CustomField = Field_FieldLabel ";
            sql += " INNER JOIN CustomTables_FieldsTypes ON Type_CustomField = id_FieldsType ";
            sql += " WHERE Lang_FieldLabel = " + this.DefaultLangID + " ";
            sql += " AND Group_FieldJoin = " + this.GroupRequest.IntValue;


            NetTable.SmTable table = new SmTable(sql, this.Conn);
            table.AddPaginationControl = true;
            table.RecordPerPagina = 20;

            SmTableColumn col = new SmTableColumn("Label_Fieldlabel", "Label");
            table.addColumn(col);

            col = new SmTableColumn("Nome_FieldsType", "Tipo");
            table.addColumn(col);

            table.addColumn(new SmTableActionColumn("id_CustomField", "action modify", "Scheda e Opzioni", "fields_details.aspx?fid={0}"));
            table.addColumn(new SmTableActionColumn("id_CustomField", "action delete", "Elimina", "javascript: setFormValueConfirm({0},'" + deleteField.ID + "',1,'Sei Sicuro di eliminare questo campo?')"));
            
            Div.Controls.Add(table.getTable());
            
            return Div;
        }

        public virtual HtmlGenericControl NewView()
        {
            if (!GroupRequest.IsValidInteger)
                return this.DefaultView();

            NetForm form = new NetForm();
            form.AddTable(this.CustomFieldForm);
            NetFormTable table = this.CustomFieldsLabelsForm(true);
            form.AddTable(table);
            form.AddTable(this.CustomFieldJoinForm);
            table.addExternalField(new NetExternalField("CustomTables_Fields", "Field_FieldLabel"));
            form.SubmitLabel = "Crea Campo";

            string errors = "";
            if (form.CheckSubmitSource)
            {
                errors = form.serializedInsert();
            }

            return form.getForms("Creazione Campo", errors);
        }

        public virtual HtmlGenericControl DetailsView()
        {
            HtmlGenericControls.Fieldset details = new HtmlGenericControls.Fieldset("Scheda Campo " + this.Field.Label, new HtmlGenericControl("div"));

            details.Content.InnerHtml = "Opzioni:<ul>";
            details.Content.InnerHtml += "<li><a href=\"fields_mod.aspx?fid=" + this.Field.ID + "\">Modifica Dati Campo</a></li>";
            details.Content.InnerHtml += "<li><a href=\"fields_add_labels.aspx?fid=" + this.Field.ID + "\">Aggiungi Etichette Campo</a></li>";
            details.Content.InnerHtml += "<li><a href=\"fields_mod_labels.aspx?fid=" + this.Field.ID + "\">Modifica Etichette Campo</a></li>";
            details.Content.InnerHtml += "</ul>";

            details.Content.InnerHtml += "Etichette:<ul>";

            DataTable langs = Conn.SqlQuery("SELECT * FROM Lang");
            DataTable langsoffield = Conn.SqlQuery("SELECT * FROM CustomTables_Fields INNER JOIN CustomTables_FieldsLabels ON id_CustomField = Field_FieldLabel WHERE id_CustomField = "+this.FieldRequest);
            foreach (DataRow lang in langs.Rows)
            {
                details.Content.InnerHtml += "<li><strong>" + lang["Nome_Lang"].ToString() + ":</strong>";
                DataRow[] labelRecord = langsoffield.Select("Lang_FieldLabel = " + lang["id_Lang"].ToString());
                if (labelRecord.Length > 0)
                     details.Content.InnerHtml += labelRecord[0]["Label_FieldLabel"].ToString();
                else
                     details.Content.InnerHtml += "Non Impostata";
                details.Content.InnerHtml += "</li>";

            }
            details.Content.InnerHtml += "</ul>";

            details.Content.InnerHtml += "Informazioni:<ul>";
            details.Content.InnerHtml += "<li><strong>Tipo:</strong>" + Field.TypeName + "</li>";
            details.Content.InnerHtml += "<li><strong>Obbligatorio:</strong>" + (Field.Required ? "Si" : "No") + "</li>";
            details.Content.InnerHtml += "<li><strong>Presente nella tabella di riepilogo:</strong>" + (Field.AddToListView ? "Si" : "No") + "</li>";
            details.Content.InnerHtml += "<li><strong>Classe CSS:</strong>" + (Field.CssClass) + "</li>";
            details.Content.InnerHtml += "<li><strong>Appartenente al Gruppo:</strong>" + (Field.FieldsGroup.Label) + "</li>";
            details.Content.InnerHtml += "<li><strong>Appartenente alla Tabella:</strong>" + (Field.FieldsGroup.Table.TableName) + "</li>";
            details.Content.InnerHtml += "</ul>";

            return details;
        }

        public virtual HtmlGenericControl OptionsView()
        {
            return new HtmlGenericControl("span");
        }
        public virtual HtmlGenericControl ModView()
        {
            if (!FieldRequest.IsValidInteger)
                return this.DefaultView();

            NetForm form = new NetForm();
            this.CustomFieldForm.ID = FieldRequest.IntValue;
            form.AddTable(this.CustomFieldForm);
            form.SubmitLabel = "Salva Campo";

            string errors = "";
            if (form.CheckSubmitSource)
            {
                errors = form.serializedUpdate();
            }

            return form.getForms("Modifica Campo", errors);
        }
        public virtual HtmlGenericControl AddLabelsView()
        {
            if (!FieldRequest.IsValidInteger)
                return this.DefaultView();

            HtmlGenericControls.Div container = new HtmlGenericControls.Div();
            HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset("Seleziona la ligua per aggiungere un etichetta");
            container.Controls.Add(set);

            //Costruisco la lista con le possibili lingue
            DropDownList list = new DropDownList();
            list.ID = "AvailableLangs";
            list.Items.Add(new ListItem("", ""));
            list.AutoPostBack = true;
            set.Controls.Add(list);
            NetUtility.RequestVariable langRequest = new NetUtility.RequestVariable(list.ID);

            DataTable langsinstalled = this.Conn.SqlQuery("SELECT * FROM CustomTables_Fields INNER JOIN CustomTables_FieldsLabels ON id_CustomField = Field_FieldLabel INNER JOIN Lang ON id_Lang = Lang_FieldLabel WHERE Field_FieldLabel = " + this.FieldRequest);
            string installed = ".";
            foreach (DataRow row in langsinstalled.Rows)
                installed += row["id_Lang"] + ".";

            DataTable langs = this.Conn.SqlQuery("SELECT * FROM Lang");
            foreach (DataRow row in langs.Rows)
            {
                if (!installed.Contains("." + row["id_Lang"] + "."))
                    list.Items.Add(new ListItem(row["Nome_Lang"].ToString(), row["id_Lang"].ToString()));
            }

            NetForm form = new NetForm();
            form.SubmitLabel = "Aggiungi Etichetta al Campo";

            if (langRequest.IsValidInteger || form.CheckSubmitSource)
            {
                SelectedLangField.Value = langRequest.StringValue;
                list.Enabled = false;

                NetFormTable table = this.CustomFieldsLabelsForm(true);
                if (form.CheckSubmitSource)
                    this._SelectedLangID = SelectedLangField.RequestVariable.IntValue;
                else
                    this._SelectedLangID = langRequest.IntValue;

                set.Controls.Add(SelectedLangField.Control);

                NetHiddenField group = new NetHiddenField("Field_Fieldlabel");
                group.Value = this.FieldRequest.StringValue;
                table.addField(group);

                form.AddTable(table);

                string errors = "";
                if (form.CheckSubmitSource)
                {
                    errors = form.serializedInsert();
                }
                container.Controls.Add(form.getForms("Aggiungi Etichetta al Campo", errors));
            }
            return container;
        }
        public virtual HtmlGenericControl ModLabelsView()
        {
            if (!FieldRequest.IsValidInteger)
                return this.DefaultView();
            
            HtmlGenericControls.Div container = new HtmlGenericControls.Div();
            HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset("Seleziona la ligua per aggiungere un etichetta");
            container.Controls.Add(set);

            //Costruisco la lista con le possibili lingue
            DropDownList list = new DropDownList();
            list.ID = "AvailableLangs";
            list.Items.Add(new ListItem("", ""));
            list.AutoPostBack = true;
            set.Controls.Add(list);
            NetUtility.RequestVariable langRequest = new NetUtility.RequestVariable(list.ID);

            DataTable langs = this.Conn.SqlQuery("SELECT * FROM CustomTables_Fields INNER JOIN CustomTables_FieldsLabels ON id_CustomField = Field_FieldLabel INNER JOIN Lang ON id_Lang = Lang_FieldLabel WHERE Field_FieldLabel = " + this.FieldRequest);

            foreach (DataRow row in langs.Rows)
            {
                list.Items.Add(new ListItem(row["Nome_Lang"].ToString(), row["id_FieldLabel"].ToString()));
            }

            NetForm form = new NetForm();
            form.SubmitLabel = "Salva Etichetta del Campo";

            if (langRequest.IsValidInteger || form.CheckSubmitSource)
            {
                SelectedLangField.Value = langRequest.StringValue;
                list.Enabled = false;

                NetFormTable table = this.CustomFieldsLabelsForm(false);
                if (form.CheckSubmitSource)
                    table.ID = SelectedLangField.RequestVariable.IntValue;
                else
                    table.ID = langRequest.IntValue;

                set.Controls.Add(SelectedLangField.Control);

                form.AddTable(table);

                string errors = "";
                if (form.CheckSubmitSource)
                {
                    errors = form.serializedUpdate();
                }
                container.Controls.Add(form.getForms("Modifica Etichetta del Campo", errors));
            }
            return container;
        }


        public static NetCfFieldManager FieldManagerFactory(NetCms.Connections.Connection conn)
        {
            NetUtility.RequestVariable request = new NetUtility.RequestVariable("type",NetUtility.RequestVariable.RequestType.QueryString);
            if (request.IsValidInteger)
                return FieldManagerFactory(request.IntValue, conn);

            request = new NetUtility.RequestVariable("fid", NetUtility.RequestVariable.RequestType.QueryString);
            if (request.IsValidInteger)
            {
                DataTable tab = conn.SqlQuery("SELECT * FROM CustomTables_Fields WHERE id_CustomField = "+request);
                if (tab.Rows.Count > 0)
                {
                return FieldManagerFactory(int.Parse(tab.Rows[0]["Type_CustomField"].ToString()), conn);

                }
            }
            return FieldManagerFactory(0, conn);
        }
        public static NetCfFieldManager FieldManagerFactory(int type, NetCms.Connections.Connection conn)
        {
            switch (type)
            {
                case NetCfTextbox.FieldTypeID:
                    return new NetCfTextboxFieldManager(conn);

                case NetCfDate.FieldTypeID:
                    return new NetCfDateFieldManager(conn);

                case NetCfDropDownList.FieldTypeID:
                    return new NetCfDropDownListFieldManager(conn);

                case NetCfDynamicDropDownList.FieldTypeID:
                    return new NetCfDynamicDropDownListFieldManager(conn);

                case NetCfExternalTable.FieldTypeID:
                    return new NetCfExternalTableFieldManager(conn);

                default:
                    return new NetCfGenericFieldManager(conn);
            }
        }

    }
}