using System;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using NetTable;
using NetForms;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom.Manager
{
    public class NetCfGenericFieldManager : NetCfFieldManager
    {
        protected override NetFormTable BuildFieldForm(bool InsertView)
        {           
            return null;
        }

        public NetCfGenericFieldManager(NetCms.Connections.Connection conn)
            :base(conn)
        {
        }

        public override HtmlGenericControl NewView()
        {
            HtmlGenericControl set = new HtmlGenericControl("fieldset");
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            HtmlGenericControl content = new HtmlGenericControl("div");

            legend.InnerHtml = "Seleziona Tipo Campo";

            DataTable fieldtypes = this.Conn.SqlQuery("SELECT * FROM CustomTables_FieldsTypes ORDER BY Nome_FieldsType");
            HtmlGenericControl list = new HtmlGenericControl("ul");
            foreach (DataRow row in fieldtypes.Rows)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                list.Controls.Add(li);
                li.InnerHtml = "<a href=\"fields_new.aspx?grid=" + this.GroupRequest + "&amp;type=" + row["id_FieldsType"] + "\">" + row["Nome_FieldsType"] + "</a>";
            }
            set.Controls.Add(legend);
            set.Controls.Add(content);
            content.Controls.Add(list);

            return set;
        }
    }
}