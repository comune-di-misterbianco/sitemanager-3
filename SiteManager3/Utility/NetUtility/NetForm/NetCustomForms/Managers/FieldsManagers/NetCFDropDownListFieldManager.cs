using System;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using NetTable;
using NetForms;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom.Manager
{
    public class NetCfDropDownListFieldManager : NetCfFieldManager
    {       
        protected override NetFormTable BuildFieldForm(bool InsertView)
        {
            NetFormTable table = new NetFormTable("CustomTables_Fields", "id_CustomField", this.Conn);
            
            if (InsertView)
            {
                NetHiddenField type = new NetHiddenField("Type_CustomField");
                type.Value = NetCfDropDownList.FieldTypeID.ToString();
                table.addField(type);
            }

            NetDropDownList addToListView = new NetDropDownList("Mostra nella tabella di riepilogo", "AddToListView_CustomField");
            addToListView.ClearList();
            addToListView.Required = true;
            addToListView.addItem("Si", "1");
            addToListView.addItem("No", "0");
            table.addField(addToListView);

            NetDropDownList required = new NetDropDownList("Obbligatorio", "Required_CustomField");
            required.ClearList();
            required.Required = true;
            required.addItem("Si", "1");
            required.addItem("No", "0");
            table.addField(required);

            return table;
        }

        public NetCfDropDownListFieldManager(NetCms.Connections.Connection conn)
            :base(conn)
        {
        }

        public override HtmlGenericControl DetailsView()
        {
            HtmlGenericControls.Fieldset details = new HtmlGenericControls.Fieldset("Scheda Campo "+this.Field.Label, new HtmlGenericControl("div"));

            details.Content.InnerHtml = "Opzioni:<ul>";
            details.Content.InnerHtml += "<li><a href=\"fields_mod.aspx?fid=" + this.Field.ID + "\">Modifica Dati Campo</a></li>";
            details.Content.InnerHtml += "<li><a href=\"fields_mod_options.aspx?fid=" + this.Field.ID + "\">Modifica Opzioni Campo</a></li>";
            details.Content.InnerHtml += "</ul>";

            details.Content.InnerHtml += "Informazioni:<ul>";
            details.Content.InnerHtml += "<li><strong>Tipo:</strong>" + Field.TypeName + "</li>";
            details.Content.InnerHtml += "<li><strong>Label:</strong>" + Field.Label + "</li>";
            details.Content.InnerHtml += "<li><strong>Obbligatorio:</strong>" + (Field.Required ? "Si" : "No") + "</li>";
            details.Content.InnerHtml += "<li><strong>Presente nella tabella di riepilogo:</strong>" + (Field.AddToListView ? "Si" : "No") + "</li>";
            details.Content.InnerHtml += "<li><strong>Classe CSS:</strong>" + (Field.CssClass) + "</li>";
            details.Content.InnerHtml += "<li><strong>Appartenente al Gruppo:</strong>" + (Field.FieldsGroup.Label) + "</li>";
            details.Content.InnerHtml += "<li><strong>Appartenente alla Tabella:</strong>" + (Field.FieldsGroup.Table.TableName) + "</li>";
            details.Content.InnerHtml += "</ul>";

            return details;
        }

        public override HtmlGenericControl OptionsView()
        {
            if (!FieldRequest.IsValidInteger)
                return this.DefaultView();
            HtmlGenericControls.Div control = new HtmlGenericControls.Div();

            HtmlGenericControls.Fieldset addOption = new HtmlGenericControls.Fieldset("Aggiungi Opzione");

            NetFormTable table = new NetFormTable("CustomTables_FieldsOptions", "id_FieldsOption",this.Conn);

            NetHiddenField field = new NetHiddenField("Field_FieldsOption");
            field.Value = this.FieldRequest.StringValue;
            table.addField(field);

            NetFormTable table1 = new NetFormTable("CustomTables_FieldsOptionsLabels", "id_FieldsOptionLabel", this.Conn);

            NetTextBoxPlus label = new NetTextBoxPlus("Label", "Label_FieldsOptionLabel");
            label.Required = true;
            table1.addField(label);

            NetDropDownList langsfield = new NetDropDownList("Lingua", "Lang_FieldsOptionLabel");
            langsfield.Required = true;
            table1.addField(langsfield);

            DataTable langs = this.Conn.SqlQuery("SELECT * FROM Lang");
            foreach (DataRow row in langs.Rows)
                langsfield.addItem(row["Nome_Lang"].ToString(), row["id_Lang"].ToString());

            table1.addExternalField(new NetExternalField("CustomTables_FieldsOptions", "Option_FieldsOptionLabel"));

            NetForm form = new NetForm();
            form.AddTable(table);
            form.AddTable(table1);
            form.SubmitLabel = "Aggiungi Opzione";

            string errors = "";
            if (form.CheckSubmitSource)
            {
                errors = form.serializedInsert();
            }
            control.Controls.Add(form.getForms("Aggiungi Opzione",errors));
            

            HtmlGenericControls.Fieldset viewOption = new HtmlGenericControls.Fieldset("Elenco Opzioni");

            SmTable tab = new SmTable("SELECT * FROM CustomTables_FieldsOptions INNER JOIN CustomTables_FieldsOptionsLabels ON id_FieldsOption = Option_FieldsOptionLabel INNER JOIN Lang ON id_Lang = Lang_FieldsOptionLabel WHERE Field_FieldsOption = " + this.FieldRequest.StringValue, this.Conn);
            NetUtility.RequestVariable del = new NetUtility.RequestVariable(tab.DeleteFieldID);
            if (del.IsValidInteger)
                Conn.Execute("DELETE FROM CustomTables_FieldsOptions WHERE id_FieldsOption = "+del);

            tab.addColumn(new SmTableColumn("Label_FieldsOptionLabel", "Label"));
            tab.addColumn(new SmTableColumn("Nome_Lang", "Lingua"));
            tab.addColumn(new SmTableActionColumn("id_FieldsOption","action delete","Elimina","javascript: setFormValueConfirm({0},'"+tab.DeleteFieldID+"',1,'Sei sicuro di voler eleiminare questa opzione?')"));
            viewOption.Controls.Add(tab.getTable(" ORDER BY Label_FieldsOptionLabel"));
            control.Controls.Add(viewOption);

            return control;
        }
    }
}