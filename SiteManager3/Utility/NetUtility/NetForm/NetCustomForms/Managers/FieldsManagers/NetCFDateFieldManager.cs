/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom.Manager
{
    public class NetCfDateFieldManager : NetCfFieldManager
    {
        protected override NetFormTable BuildFieldForm(bool InsertView)
        {
            NetFormTable table = new NetFormTable("CustomTables_Fields", "id_CustomField", this.Conn);

            NetDateField2 min = new NetDateField2("Data Minima", "Option1_CustomField");
            min.Size = 10;
            table.addField(min);

            NetDateField2 max = new NetDateField2("Data Massima", "Option2_CustomField");
            max.Size = 10;
            table.addField(max);

            
            if (InsertView)
            {
                NetHiddenField type = new NetHiddenField("Type_CustomField");
                type.Value = NetCfDate.FieldTypeID.ToString();
                table.addField(type);
            }

            NetDropDownList addToListView = new NetDropDownList("Mostra nella tabella di riepilogo", "AddToListView_CustomField");
            addToListView.ClearList();
            addToListView.Required = true;
            addToListView.addItem("Si", "1");
            addToListView.addItem("No", "0");
            table.addField(addToListView);

            NetDropDownList required = new NetDropDownList("Obbligatorio", "Required_CustomField");
            required.ClearList();
            required.Required = true;
            required.addItem("Si", "1");
            required.addItem("No", "0");
            table.addField(required);

            return table;
        }

        public NetCfDateFieldManager(NetCms.Connections.Connection conn)
            :base(conn)
        {
        }
    }
}