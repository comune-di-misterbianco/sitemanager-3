using System;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using NetTable;
using NetForms;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom.Manager
{
    public class NetCfExternalTableFieldManager : NetCfFieldManager
    {       
        protected override NetFormTable BuildFieldForm(bool InsertView)
        {
            NetFormTable table = new NetFormTable("CustomTables_Fields", "id_CustomField", this.Conn);

            NetTextBoxPlus sql = new NetTextBoxPlus("Nome Tabella di Riferimento", "Option1_CustomField");
            sql.Size = 90;
            sql.Rows = 10;
            sql.Required = true;
            table.addField(sql);

            if (InsertView)
            {
                NetHiddenField type = new NetHiddenField("Type_CustomField");
                type.Value = NetCfExternalTable.FieldTypeID.ToString();
                table.addField(type);
            }

            NetHiddenField addToListView = new NetHiddenField("AddToListView_CustomField");
            addToListView.Value = "0";
            table.addField(addToListView);

            NetHiddenField required = new NetHiddenField("Required_CustomField");
            required.Value = "0";
            table.addField(required);

            return table;
        }

        public NetCfExternalTableFieldManager(NetCms.Connections.Connection conn)
            :base(conn)
        {
        }

        public override HtmlGenericControl DetailsView()
        {
            HtmlGenericControls.Fieldset details = new HtmlGenericControls.Fieldset("Scheda Campo "+this.Field.Label, new HtmlGenericControl("div"));

            details.Content.InnerHtml = "Opzioni:<ul>";
            details.Content.InnerHtml += "<li><a href=\"fields_mod.aspx?fid=" + this.Field.ID + "\">Modifica Dati Campo</a></li>";
            details.Content.InnerHtml += "</ul>";

            details.Content.InnerHtml += "Informazioni:<ul>";
            details.Content.InnerHtml += "<li><strong>Tipo:</strong>" + Field.TypeName + "</li>";
            details.Content.InnerHtml += "<li><strong>Presente nella tabella di riepilogo:</strong>No</li>";
            details.Content.InnerHtml += "<li><strong>Tabella di Riferimento:</strong>" + (Field.Option1) + "</li>";
            details.Content.InnerHtml += "<li><strong>Appartenente al Gruppo:</strong>" + (Field.FieldsGroup.Label) + "</li>";
            details.Content.InnerHtml += "<li><strong>Appartenente alla Tabella:</strong>" + (Field.FieldsGroup.Table.TableName) + "</li>";
            details.Content.InnerHtml += "</ul>";

            return details;
        }

    }
}