using System;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using NetTable;
using NetForms;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom.Manager
{
    public class NetCfDynamicDropDownListFieldManager : NetCfFieldManager
    {       
        protected override NetFormTable BuildFieldForm(bool InsertView)
        {
            NetFormTable table = new NetFormTable("CustomTables_Fields", "id_CustomField", this.Conn);

            NetTextBoxPlus sql = new NetTextBoxPlus("Query SQL di selezione delle Opzioni", "Option1_CustomField");
            sql.Size = 90;
            sql.Rows = 10;
            sql.Required = true;
            table.addField(sql);

            if (InsertView)
            {
                NetHiddenField type = new NetHiddenField("Type_CustomField");
                type.Value = NetCfDynamicDropDownList.FieldTypeID.ToString();
                table.addField(type);
            }

            NetDropDownList addToListView = new NetDropDownList("Mostra nella tabella di riepilogo", "AddToListView_CustomField");
            addToListView.ClearList();
            addToListView.Required = true;
            addToListView.addItem("Si", "1");
            addToListView.addItem("No", "0");
            table.addField(addToListView);

            NetDropDownList required = new NetDropDownList("Obbligatorio", "Required_CustomField");
            required.ClearList();
            required.Required = true;
            required.addItem("Si", "1");
            required.addItem("No", "0");
            table.addField(required);

            return table;
        }

        public NetCfDynamicDropDownListFieldManager(NetCms.Connections.Connection conn)
            :base(conn)
        {
        }

        public override HtmlGenericControl DetailsView()
        {
            HtmlGenericControls.Fieldset details = new HtmlGenericControls.Fieldset("Scheda Campo "+this.Field.Label, new HtmlGenericControl("div"));

            details.Content.InnerHtml = "Opzioni:<ul>";
            details.Content.InnerHtml += "<li><a href=\"fields_mod.aspx?fid=" + this.Field.ID + "\">Modifica Dati Campo</a></li>";
            details.Content.InnerHtml += "</ul>";

            details.Content.InnerHtml += "Informazioni:<ul>";
            details.Content.InnerHtml += "<li><strong>Tipo:</strong>" + Field.TypeName + "</li>";
            details.Content.InnerHtml += "<li><strong>Label:</strong>" + Field.Label + "</li>";
            details.Content.InnerHtml += "<li><strong>Obbligatorio:</strong>" + (Field.Required ? "Si" : "No") + "</li>";
            details.Content.InnerHtml += "<li><strong>Presente nella tabella di riepilogo:</strong>" + (Field.AddToListView ? "Si" : "No") + "</li>";
            details.Content.InnerHtml += "<li><strong>Classe CSS:</strong>" + (Field.CssClass) + "</li>";
            details.Content.InnerHtml += "<li><strong>Query SQL di selezione delle Opzioni:</strong>" + (Field.Option1) + "</li>";
            details.Content.InnerHtml += "<li><strong>Appartenente al Gruppo:</strong>" + (Field.FieldsGroup.Label) + "</li>";
            details.Content.InnerHtml += "<li><strong>Appartenente alla Tabella:</strong>" + (Field.FieldsGroup.Table.TableName) + "</li>";
            details.Content.InnerHtml += "</ul>";

            return details;
        }

    }
}