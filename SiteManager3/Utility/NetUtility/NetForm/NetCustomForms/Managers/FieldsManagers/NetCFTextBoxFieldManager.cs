using System;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using NetTable;
using NetForms;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom.Manager
{
    public class NetCfTextboxFieldManager : NetCfFieldManager
    {
        protected override NetFormTable BuildFieldForm(bool InsertView)
        {
            NetFormTable table = new NetFormTable("CustomTables_Fields", "id_CustomField", this.Conn);

            NetTextBoxPlus len = new NetTextBoxPlus("Lunghezza Massima Testo ( 0 = Infinito )", "Length_CustomField",NetTextBoxPlus.ContentTypes.Numeric);
            len.Size = 10;
            len.Required = true;
            table.addField(len);

            NetTextBoxPlus cols = new NetTextBoxPlus("Numero Colonne Campo", "Option1_CustomField", NetTextBoxPlus.ContentTypes.Numeric);
            cols.Size = 10;
            cols.Required = true;
            table.addField(cols);

            NetTextBoxPlus rows = new NetTextBoxPlus("Numero Righe Campo", "Option2_CustomField", NetTextBoxPlus.ContentTypes.Numeric);
            rows.Size = 10;
            rows.Required = true;
            table.addField(rows);

            
            if (InsertView)
            {
                NetHiddenField type = new NetHiddenField("Type_CustomField");
                type.Value = NetCfTextbox.FieldTypeID.ToString();
                table.addField(type);
            }

            NetDropDownList addToListView = new NetDropDownList("Mostra nella tabella di riepilogo", "AddToListView_CustomField");
            addToListView.ClearList();
            addToListView.Required = true;
            addToListView.addItem("Si", "1");
            addToListView.addItem("No", "0");
            table.addField(addToListView);

            NetDropDownList required = new NetDropDownList("Obbligatorio", "Required_CustomField");
            required.ClearList();
            required.Required = true;
            required.addItem("Si", "1");
            required.addItem("No", "0");
            table.addField(required);

            return table;
        }

        public NetCfTextboxFieldManager(NetCms.Connections.Connection conn)
            :base(conn)
        {
        }
    }
}