using System;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NetTable;
using NetForms;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom.Manager
{
    public class NetCFTableManager
    {
        private NetCms.Connections.Connection _Conn;
        private NetCms.Connections.Connection Conn
        {
            get
            {
                return _Conn;
            }
        }

        protected int _DefaultLangID;
        protected int DefaultLangID
        {
            get
            {
                if (_DefaultLangID == 0)
                {
                    DataTable tab = this.Conn.SqlQuery("SELECT * FROM Lang WHERE Default_Lang = 1");
                    if (tab.Rows.Count > 0)
                    {
                        _DefaultLangID = int.Parse(tab.Rows[0]["id_Lang"].ToString());
                    }
                }
                return _DefaultLangID;
            }
        }

        protected HtmlGenericControls.InputField _SelectedLangField;
        protected HtmlGenericControls.InputField SelectedLangField
        {
            get
            {
                if(_SelectedLangField == null)
                    _SelectedLangField = new HtmlGenericControls.InputField("SelectedLang");
                return _SelectedLangField;
            }
        }

        protected int _SelectedLangID;
        protected int SelectedLangID
        {
            get
            {
                if (_SelectedLangID == 0 && SelectedLangField.RequestVariable.IsValidInteger)
                    _SelectedLangID = SelectedLangField.RequestVariable.IntValue;
                if (_SelectedLangID == 0)
                {
                    _SelectedLangID = DefaultLangID;
                }
                return _SelectedLangID;
            }
        }

        private NetFormTable customTableForm;
        private NetFormTable CustomTableForm
        {
            get
            {
                if (customTableForm == null)
                {
                    customTableForm = new NetFormTable("CustomTables_Tables", "id_CustomTable", this.Conn);

                    NetTextBoxPlus cssclass = new NetTextBoxPlus("Classe CSS", "CssClass_CustomTable");
                    cssclass.Rows = 4;
                    cssclass.Size = 70;
                    customTableForm.addField(cssclass);

                    NetDropDownList cat = new NetDropDownList("Associazione", "Category_CustomTable");
                    cat.ClearList();
                    cat.Required = true;
                    cat.setDataBind("SELECT Label_TableCategory,id_TableCategory FROM CustomTables_TablesCategories", Conn);
                    customTableForm.addField(cat);
                    
                    NetDropDownList header = new NetDropDownList("Aggiungi Fieldset con Nome Tabella e Testo Informativo?", "AddHeader_CustomTable");
                    header.ClearList();
                    header.Required = true;
                    header.addItem("Si", "1");
                    header.addItem("No", "0");
                    customTableForm.addField(header);

                    NetDropDownList details = new NetDropDownList("Opzione Scheda", "DetailsOption_CustomTable");
                    details.ClearList();
                    details.Required = true;
                    details.addItem("Abilitata", "1");
                    details.addItem("Disabilitata", "0");
                    customTableForm.addField(details);
                    
                    NetDropDownList filters = new NetDropDownList("Selezione Escusiva Dei Gruppi", "EnableGroupsFillter_CustomTable");
                    filters.ClearList();
                    filters.Required = true;
                    filters.addItem("Disabilitata", "0");
                    filters.addItem("Abilitata", "1");
                    customTableForm.addField(filters);

                    NetDropDownList modify = new NetDropDownList("Opzione Modifica", "ModifyOption_CustomTable");
                    modify.ClearList();
                    modify.Required = true;
                    modify.addItem("Abilitata", "1");
                    modify.addItem("Disabilitata", "0");
                    customTableForm.addField(modify);

                    NetDropDownList delete = new NetDropDownList("Opzione Delete", "DeleteOption_CustomTable");
                    delete.ClearList();
                    delete.Required = true;
                    delete.addItem("Abilitata", "1");
                    delete.addItem("Disabilitata", "0");
                    customTableForm.addField(delete);
                }
                return customTableForm;
            }
        }

        private NetFormTable CustomTableLabelForm()
        {
            return CustomTableLabelForm(true);
        }
        private NetFormTable CustomTableLabelForm(bool addLangField)
        {
            //get
            {
                NetFormTable customTableLabelForm;
                {
                    customTableLabelForm = new NetFormTable("CustomTables_TablesLabels", "id_TableLabel", this.Conn);

                    NetTextBoxPlus nome = new NetTextBoxPlus("Nome", "Label_Tablelabel");
                    nome.Size = 70;
                    nome.Required = true;
                    customTableLabelForm.addField(nome);

                    NetTextBoxPlus info = new NetTextBoxPlus("Testo Informativo", "Infotext_Tablelabel");
                    info.Rows = 4;
                    info.Size = 70;
                    customTableLabelForm.addField(info);
                    if (addLangField)
                    {
                        NetHiddenField lang = new NetHiddenField("Lang_Tablelabel");
                        lang.Value = this.SelectedLangID.ToString();
                        customTableLabelForm.addField(lang);
                    }
                    
                }
                return customTableLabelForm;
            }
        }

        public NetCFTableManager(NetCms.Connections.Connection conn)
        {
            _Conn = conn;
        }

        public HtmlGenericControl TablesView()
        {
            HtmlGenericControl Div = new HtmlGenericControl("div");

            HtmlGenericControls.InputField deleteField = new HtmlGenericControls.InputField("DeleteField");
            if (deleteField.RequestVariable.IsValidInteger)
                Conn.Execute("DELETE FROM CustomTables_Tables WHERE id_CustomTable = " + deleteField.RequestVariable.IntValue);
            Div.Controls.Add(deleteField.Control);

            NetTable.SmTable table = new SmTable("SELECT * FROM CustomTables_Tables INNER JOIN CustomTables_TablesLabels ON id_Customtable = Table_TableLabel INNER JOIN CustomTables_TablesCategories ON Category_CustomTable = id_TableCategory WHERE Lang_TableLabel = " + this.DefaultLangID + " ORDER BY Label_TableLabel", this.Conn);
            table.AddPaginationControl = true;
            table.RecordPerPagina = 20;

            SmTableColumn col = new SmTableColumn("Label_TableLabel", "Tabella");
            table.addColumn(col);

            col = new SmTableColumn("Label_TableCategory", "Associazione");
            table.addColumn(col);

            col = new SmTableColumn("CssClass_CustomTable", "Classe CSS");
            table.addColumn(col);

            col = new SmTableMultiOptionColumn("DetailsOption_CustomTable", "Opzione Scheda", "Disabilitata,Abilitata");
            table.addColumn(col);

            col = new SmTableMultiOptionColumn("ModifyOption_CustomTable", "Opzione Modifica", "Disabilitata,Abilitata");
            table.addColumn(col);

            col = new SmTableMultiOptionColumn("DeleteOption_CustomTable", "Opzione Elimina", "Disabilitata,Abilitata");
            table.addColumn(col);

            col = new SmTableActionColumn("id_CustomTable", "action details", "Gestione Categorie", "groupscategories/groups_categories.aspx?tbid={0}");
            table.addColumn(col);

            col = new SmTableActionColumn("id_CustomTable", "action details", "Gestione Tabella", "groups/default.aspx?tbid={0}");
            table.addColumn(col);

            col = new NetCFTableManager_CustomSmTableActionColumn("id_CustomTable", "action delete", "Elimina Tabella", "javascript: setFormValueConfirm({0},'" + deleteField.ID + "',1,'Sei Sicuro di eliminare questa tabella?')");
            table.addColumn(col);

            Div.Controls.Add(table.getTable());

            return Div;
        }

        public HtmlGenericControl NewTableView()
        {
            NetForm form = new NetForm();
            form.AddTable(CustomTableForm);
            form.AddTable(CustomTableLabelForm());
            form.Tables[1].addExternalField(new NetExternalField("CustomTables_Tables", "Table_TableLabel"));
            form.SubmitLabel = "Crea Tabella";

            string errors = "";
            if (form.CheckSubmitSource)
            {
                NetUtility.RequestVariable TableLabel = new NetUtility.RequestVariable("Label_Tablelabel");

                if (TableLabel.IsValidString)
                {
                    DataTable tab = Conn.SqlQuery("SELECT * FROM CustomTables_Tables INNER JOIN CustomTables_TablesLabels ON id_CustomTable = Table_TableLabel WHERE Lang_TableLabel = " + this.DefaultLangID + " AND Label_TableLabel = '" + TableLabel.StringValue + "'");
                    if (tab.Rows.Count == 0)
                    {
                        errors = form.serializedInsert();
                    }
                    else
                        errors = "<li>Il nome che hai scelto per la tabella non � valido in quanto esiste gi� una tabella con questo nome.</li>";
                }
            }

            return form.getForms("Creazione Nuova Tabella", errors);
        }

        public HtmlGenericControl ModTableView()
        {
            NetUtility.RequestVariable id = new NetUtility.RequestVariable("id", NetUtility.RequestVariable.RequestType.QueryString);
            if (!id.IsValidInteger)
                return this.TablesView();

            NetForm form = new NetForm();
            CustomTableForm.ID = id.IntValue;
            form.AddTable(CustomTableForm);
            //form.AddTable(CustomTableLabelForm);
            form.SubmitLabel = "Salva Tabella";

            string errors = "";
            if (form.CheckSubmitSource)
            {
                errors = form.serializedUpdate();
            }

            return form.getForms("Modifica Tabella", errors);
        }

        public HtmlGenericControl ModLabelsView()
        {
            NetUtility.RequestVariable id = new NetUtility.RequestVariable("id", NetUtility.RequestVariable.RequestType.QueryString);
            if (!id.IsValidInteger)
                return this.TablesView();

            HtmlGenericControls.Div container = new HtmlGenericControls.Div();
            HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset("Seleziona la ligua per modificare le etichette");


            DropDownList list = new DropDownList();
            list.ID = "AvailableLangs";
            list.AutoPostBack = true;
            DataTable langs = this.Conn.SqlQuery("SELECT * FROM CustomTables_Tables INNER JOIN CustomTables_TablesLabels ON id_Customtable = Table_TableLabel INNER JOIN Lang ON id_Lang = Lang_TableLabel WHERE Table_TableLabel = "+id);
            list.Items.Add(new ListItem("", ""));
            foreach (DataRow row in langs.Rows)
            {
                list.Items.Add(new ListItem(row["Nome_Lang"].ToString(), row["id_TableLabel"].ToString()));
            }
            set.Controls.Add(list);
            container.Controls.Add(set);
            NetUtility.RequestVariable langRequest = new NetUtility.RequestVariable(list.ID);
            NetForm form = new NetForm();
            form.SubmitLabel = "Salva Tabella";
            if (langRequest.IsValidInteger || form.CheckSubmitSource)
            {
                SelectedLangField.Value = langRequest.StringValue;
                set.Controls.Add(SelectedLangField.Control);
                list.Enabled = false;
                NetFormTable table = CustomTableLabelForm(false);
                if (form.CheckSubmitSource)
                    table.ID = SelectedLangField.RequestVariable.IntValue;
                else
                    table.ID = langRequest.IntValue;
                form.AddTable(table);

                string errors = "";
                if (form.CheckSubmitSource)
                {
                    errors = form.serializedUpdate();
                }
                container.Controls.Add(form.getForms("Modifica Tabella", errors));
            }
            return container;
        }

        public HtmlGenericControl AddLabelsView()
        {
            NetUtility.RequestVariable id = new NetUtility.RequestVariable("id", NetUtility.RequestVariable.RequestType.QueryString);
            if (!id.IsValidInteger)
                return this.TablesView();

            HtmlGenericControls.Div container = new HtmlGenericControls.Div();
            HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset("Seleziona la ligua per modificare le etichette");
            container.Controls.Add(set);
                        
            DropDownList list = new DropDownList();
            list.ID = "AvailableLangs";            
            list.Items.Add(new ListItem("", ""));
            list.AutoPostBack = true;
            set.Controls.Add(list);
            NetUtility.RequestVariable langRequest = new NetUtility.RequestVariable(list.ID);

            DataTable langsinstalled = this.Conn.SqlQuery("SELECT * FROM CustomTables_Tables INNER JOIN CustomTables_TablesLabels ON id_Customtable = Table_TableLabel INNER JOIN Lang ON id_Lang = Lang_TableLabel WHERE Table_tableLabel = "+id);
            string installed = ".";
            foreach (DataRow row in langsinstalled.Rows)
                installed += row["id_Lang"] + ".";            
            
            DataTable langs = this.Conn.SqlQuery("SELECT * FROM Lang");
            foreach (DataRow row in langs.Rows)
            {
                if(!installed.Contains("."+ row["id_Lang"]+"."))
                    list.Items.Add(new ListItem(row["Nome_Lang"].ToString(), row["id_Lang"].ToString()));
            }

            NetForm form = new NetForm();
            form.SubmitLabel = "Aggiungi Etichetta";

            if (langRequest.IsValidInteger || form.CheckSubmitSource)
            {
                SelectedLangField.Value = langRequest.StringValue;
                set.Controls.Add(SelectedLangField.Control);
                list.Enabled = false;

                if(form.CheckSubmitSource)
                    this._SelectedLangID = SelectedLangField.RequestVariable.IntValue;                    
                else
                    this._SelectedLangID = langRequest.IntValue;

                NetFormTable table = CustomTableLabelForm();

                NetHiddenField tabfield = new NetHiddenField("Table_Tablelabel");
                tabfield.Value = id.StringValue;
                table.addField(tabfield);

                form.AddTable(table);

                string errors = "";
                if (form.CheckSubmitSource)
                {
                    errors = form.serializedInsert();
                }
                container.Controls.Add(form.getForms("Aggiungi Etichette Tabella", errors));
            }
            return container;
        }

        public class NetCFTableManager_CustomSmTableActionColumn : NetTable.SmTableColumn
        {
            public string Classe;
            public string Label;
            public string Href;
            public string Title;

            public NetCFTableManager_CustomSmTableActionColumn(string fieldName, string classe, string Caption, string href)
                : this(fieldName, classe, Caption, href, "", Caption)
            {
            }
            public NetCFTableManager_CustomSmTableActionColumn(string fieldName, string classe, string Caption, string href, string title)
                : this(fieldName, classe, Caption, href, "", Caption)
            {
            }

            public NetCFTableManager_CustomSmTableActionColumn(string fieldName, string classe, string Caption, string href, string title, string label)
                : base(fieldName, Caption)
            {
                Classe = classe;
                Label = label;
                Href = href;
                Title = title;
            }
            public override HtmlGenericControl getField(DataRow row)
            {
                string value = row[this.FieldName].ToString();
                string DbLabel = row.Table.Columns.Contains(this.Label) ? row[this.Label].ToString() : Label;

                string html = "";
                html += "<a href=\"";
                html += Href.Replace("{0}", value);
                html += "\" title=\"" + (Title.Length > 0 ? Title : Label) + "\"><span>";
                html += DbLabel;
                html += "</span></a>";
                
                HtmlGenericControl td = new HtmlGenericControl("td");
                if (row["Static_CustomTable"].ToString() != "1")
                {
                    td.Attributes["class"] = Classe;
                    td.InnerHtml = html;
                }
                return td;
            }

        }
    }
}