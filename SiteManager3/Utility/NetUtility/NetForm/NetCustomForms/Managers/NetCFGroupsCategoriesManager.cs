using System;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NetTable;
using NetForms;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom.Manager
{
    public class NetCFGroupsCategoriesManager
    {
        private NetCms.Connections.Connection _Conn;
        private NetCms.Connections.Connection Conn
        {
            get
            {
                return _Conn;
            }
        }

        private NetUtility.RequestVariable _TableRequest;
        private NetUtility.RequestVariable TableRequest
        {
            get
            {
                if(_TableRequest==null)
                {
                    _TableRequest = new NetUtility.RequestVariable("tbid", NetUtility.RequestVariable.RequestType.QueryString);
                }
                return _TableRequest;
            }
        }   
        private const string GenericSelectQuerySQL = "SELECT * FROM CustomTables_Tables INNER JOIN customtables_fieldsgroupscategories ON Table_fieldsgroupscategory = id_CustomTable INNER JOIN customtables_fieldsgroupscategorylabels ON id_fieldsgroupscategory = Category_FieldsGroupCategoryLabel INNER JOIN Lang ON id_Lang = lang_FieldsGroupCategoryLabel";

        protected int _DefaultLangID;
        protected int DefaultLangID
        {
            get
            {
                if (_DefaultLangID == 0)
                {
                    DataTable tab = this.Conn.SqlQuery("SELECT * FROM Lang WHERE Default_Lang = 1");
                    if (tab.Rows.Count > 0)
                    {
                        _DefaultLangID = int.Parse(tab.Rows[0]["id_Lang"].ToString());
                    }
                }
                return _DefaultLangID;
            }
        }

        protected HtmlGenericControls.InputField _SelectedLangField;
        protected HtmlGenericControls.InputField SelectedLangField
        {
            get
            {
                if(_SelectedLangField == null)
                    _SelectedLangField = new HtmlGenericControls.InputField("SelectedLang");
                return _SelectedLangField;
            }
        }

        protected int _SelectedLangID;
        protected int SelectedLangID
        {
            get
            {
                if (_SelectedLangID == 0 && SelectedLangField.RequestVariable.IsValidInteger)
                    _SelectedLangID = SelectedLangField.RequestVariable.IntValue;
                if (_SelectedLangID == 0)
                {
                    _SelectedLangID = DefaultLangID;
                }
                return _SelectedLangID;
            }
        }

        private NetFormTable _CategoryForm;
        private NetFormTable CategoryForm
        {
            get
            {
                if (_CategoryForm == null)
                {
                    _CategoryForm = new NetFormTable("customtables_fieldsgroupscategories", "id_fieldsgroupscategory", this.Conn);

                    NetHiddenField tab = new NetHiddenField("table_fieldsgroupscategory");
                    tab.Value = this.TableRequest.StringValue;
                    _CategoryForm.addField(tab);

                    NetDropDownList cat = new NetDropDownList("Categoria Padre", "Parent_fieldsgroupscategory");
                    cat.ClearList();
                    cat.addItem("Nessuna", "0");

                    NetCustomTableCategoriesBuilder builder = new NetCustomTableCategoriesBuilder(this.TableRequest.IntValue, this.Conn,HttpContext.Current.Request.Form);
                    foreach (NetCustomTableCategory category in builder.Categories)
                    {
                        AddCategoryToDDL(cat, category);
                    }
                        _CategoryForm.addField(cat);

                }
                return _CategoryForm;
            }
        }

        public static void AddCategoryToDDL(NetDropDownList ddl, NetCustomTableCategory category)
        {
            string depth = " � ";
            for (int i = 0; i < category.Depth; i++) depth += "� ";
            ddl.addItem(depth+category.Label, category.ID.ToString());
            foreach (NetCustomTableCategory child in category.Childs)
            {
                AddCategoryToDDL(ddl, child);
            }
        }

        private NetFormTable CategoryLabelsForm()
        {
            return CategoryLabelsForm(true);
        }
        private NetFormTable CategoryLabelsForm(bool addLangField)
        {
            //get
            {
                NetFormTable categoryLabelsForm;
                {
                    categoryLabelsForm = new NetFormTable("customtables_fieldsgroupscategorylabels", "id_FieldsGroupCategoryLabel", this.Conn);

                    NetTextBoxPlus nome = new NetTextBoxPlus("Nome", "Label_FieldsGroupCategoryLabel");
                    nome.Size = 70;
                    nome.Required = true;
                    categoryLabelsForm.addField(nome);

                    if (addLangField)
                    {
                        NetHiddenField lang = new NetHiddenField("Lang_FieldsGroupCategoryLabel");
                        lang.Value = this.SelectedLangID.ToString();
                        categoryLabelsForm.addField(lang);
                    }

                }
                return categoryLabelsForm;
            }
        }

        public NetCFGroupsCategoriesManager(NetCms.Connections.Connection conn)
        {
            _Conn = conn;
        }

        public HtmlGenericControl TableView()
        {
            if (!TableRequest.IsValidInteger)
               NetCms.Diagnostics.Diagnostics.Redirect("../default.aspx");

            HtmlGenericControl Div = new HtmlGenericControl("div");
            HtmlGenericControls.InputField deleteField = new HtmlGenericControls.InputField("DeleteField");
            if (deleteField.RequestVariable.IsValidInteger)
                Conn.Execute("DELETE FROM customtables_fieldsgroupscategories WHERE id_fieldsgroupscategory = " + deleteField.RequestVariable.IntValue);
            Div.Controls.Add(deleteField.Control);

            NetTable.SmTable table = new SmTable(GenericSelectQuerySQL + " INNER JOIN customtables_TablesLabels ON id_customtable = Table_TableLabel WHERE lang_FieldsGroupCategoryLabel = " + this.DefaultLangID + " AND lang_TableLabel = " + this.DefaultLangID + "  AND table_TableLabel = " + this.TableRequest + " ORDER BY label_FieldsGroupCategoryLabel", this.Conn);
            table.AddPaginationControl = true;
            table.RecordPerPagina = 20;

            SmTableColumn col = new SmTableColumn("label_FieldsGroupCategoryLabel", "Categoria");
            table.addColumn(col);

            col = new SmTableActionColumn("id_fieldsgroupscategory", "action details", "Aggiungi Etichetta Categoria", "groups_categories_add_labels.aspx?ctid={0}&amp;tbid=" + this.TableRequest);
            table.addColumn(col);

            col = new SmTableActionColumn("id_fieldsgroupscategory", "action details", "Modifica Etichette Categoria", "groups_categories_mod_labels.aspx?ctid={0}&amp;tbid=" + this.TableRequest);
            table.addColumn(col);

            col = new SmTableActionColumn("id_fieldsgroupscategory", "action delete", "Elimina Categoria", "javascript: setFormValueConfirm({0},'" + deleteField.ID + "',1,'Sei Sicuro di eliminare questa tabella?')");
            table.addColumn(col);
            
            Div.Controls.Add(table.getTable());

            return Div;
        }
       
        public HtmlGenericControl NewView()
        {
            if (!TableRequest.IsValidInteger)
               NetCms.Diagnostics.Diagnostics.Redirect("../default.aspx");
            NetForm form = new NetForm();
            form.AddTable(CategoryForm);
            form.AddTable(CategoryLabelsForm());
            form.Tables[1].addExternalField(new NetExternalField("customtables_fieldsgroupscategories", "category_FieldsGroupCategoryLabel"));
            form.SubmitLabel = "Crea Categoria";

            string errors = "";
            if (form.CheckSubmitSource)
            {
                errors = form.serializedInsert();
                if (form.LastOperation == NetForm.LastOperationValues.OK)
                {
                    string sql = "SELECT * FROM customtables_fieldsgroupscategories WHERE id_fieldsgroupscategory = "+CategoryForm.LastInsert;
                    DataTable tab = Conn.SqlQuery(sql);
                    if (tab.Rows.Count > 0)
                    {
                        sql = "SELECT * FROM customtables_fieldsgroupscategories WHERE id_fieldsgroupscategory = " + tab.Rows[0]["parent_fieldsgroupscategory"];
                        tab = Conn.SqlQuery(sql);
                        if (tab.Rows.Count > 0)
                        {
                            sql = "UPDATE customtables_fieldsgroupscategories SET Depth_fieldsgroupscategory = " + (int.Parse(tab.Rows[0]["depth_fieldsgroupscategory"].ToString()) + 1) + " WHERE id_fieldsgroupscategory = " + CategoryForm.LastInsert;
                            Conn.Execute(sql);
                        }
                    }
                }
            }

            return form.getForms("Creazione Nuova Categoria", errors);
        }

        public HtmlGenericControl ModLabelsView()
        {
            if (!TableRequest.IsValidInteger)
               NetCms.Diagnostics.Diagnostics.Redirect("../default.aspx");
            NetUtility.RequestVariable id = new NetUtility.RequestVariable("ctid", NetUtility.RequestVariable.RequestType.QueryString);
            if (!id.IsValidInteger)
                return this.TableView();

            HtmlGenericControls.Div container = new HtmlGenericControls.Div();
            HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset("Seleziona la ligua per modificare le etichette");


            DropDownList list = new DropDownList();
            list.ID = "AvailableLangs";
            list.AutoPostBack = true;
            DataTable langs = this.Conn.SqlQuery(GenericSelectQuerySQL + " WHERE category_FieldsGroupCategoryLabel = " + id);
            list.Items.Add(new ListItem("", ""));
            foreach (DataRow row in langs.Rows)
            {
                list.Items.Add(new ListItem(row["Nome_Lang"].ToString(), row["id_FieldsGroupCategoryLabel"].ToString()));
            }
            set.Controls.Add(list);
            container.Controls.Add(set);
            NetUtility.RequestVariable langRequest = new NetUtility.RequestVariable(list.ID);
            NetForm form = new NetForm();
            form.SubmitLabel = "Salva Etichetta Categoria";
            if (langRequest.IsValidInteger || form.CheckSubmitSource)
            {
                SelectedLangField.Value = langRequest.StringValue;
                set.Controls.Add(SelectedLangField.Control);
                list.Enabled = false;
                NetFormTable table = this.CategoryLabelsForm(false);
                if (form.CheckSubmitSource)
                    table.ID = SelectedLangField.RequestVariable.IntValue;
                else
                    table.ID = langRequest.IntValue;
                form.AddTable(table);

                string errors = "";
                if (form.CheckSubmitSource)
                {
                    errors = form.serializedUpdate();
                    string sql = "SELECT * FROM customtables_fieldsgroupscategories WHERE id_fieldsgroupscategory = " + id;
                    DataTable tab = Conn.SqlQuery(sql);
                    if (tab.Rows.Count > 0)
                    {
                        sql = "SELECT * FROM customtables_fieldsgroupscategories WHERE id_fieldsgroupscategory = " + tab.Rows[0]["parent_fieldsgroupscategory"];
                        tab = Conn.SqlQuery(sql);
                        if (tab.Rows.Count > 0)
                        {
                            sql = "UPDATE customtables_fieldsgroupscategories SET Depth_fieldsgroupscategory = " + (int.Parse(tab.Rows[0]["depth_fieldsgroupscategory"].ToString()) + 1) + " WHERE id_fieldsgroupscategory = " + id;
                            Conn.Execute(sql);
                        }
                    }

                }
                container.Controls.Add(form.getForms("Modifica Tabella", errors));
            }
            return container;
        }
        public HtmlGenericControl AddLabelsView()
        {
            if (!TableRequest.IsValidInteger)
               NetCms.Diagnostics.Diagnostics.Redirect("../default.aspx");
            NetUtility.RequestVariable id = new NetUtility.RequestVariable("ctid", NetUtility.RequestVariable.RequestType.QueryString);
            if (!id.IsValidInteger)
                return this.TableView();

            HtmlGenericControls.Div container = new HtmlGenericControls.Div();
            HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset("Seleziona la ligua per modificare le etichette");
            container.Controls.Add(set);

            DropDownList list = new DropDownList();
            list.ID = "AvailableLangs";
            list.Items.Add(new ListItem("", ""));
            list.AutoPostBack = true;
            set.Controls.Add(list);
            NetUtility.RequestVariable langRequest = new NetUtility.RequestVariable(list.ID);

            DataTable langsinstalled =  this.Conn.SqlQuery(GenericSelectQuerySQL + " WHERE category_FieldsGroupCategoryLabel = " + id);
            string installed = ".";
            foreach (DataRow row in langsinstalled.Rows)
                installed += row["id_Lang"] + ".";

            DataTable langs = this.Conn.SqlQuery("SELECT * FROM Lang");
            foreach (DataRow row in langs.Rows)
            {
                if (!installed.Contains("." + row["id_Lang"] + "."))
                    list.Items.Add(new ListItem(row["Nome_Lang"].ToString(), row["id_Lang"].ToString()));
            }

            NetForm form = new NetForm();
            form.SubmitLabel = "Aggiungi Etichetta";

            if (langRequest.IsValidInteger || form.CheckSubmitSource)
            {
                SelectedLangField.Value = langRequest.StringValue;
                set.Controls.Add(SelectedLangField.Control);
                list.Enabled = false;

                if (form.CheckSubmitSource)
                    this._SelectedLangID = SelectedLangField.RequestVariable.IntValue;
                else
                    this._SelectedLangID = langRequest.IntValue;

                NetFormTable table = this.CategoryLabelsForm();

                NetHiddenField tabfield = new NetHiddenField("category_FieldsGroupCategoryLabel");
                tabfield.Value = id.StringValue;
                table.addField(tabfield);

                form.AddTable(table);

                string errors = "";
                if (form.CheckSubmitSource)
                {
                    errors = form.serializedInsert();
                }
                container.Controls.Add(form.getForms("Aggiungi Etichette alla Categoria", errors));
            }
            return container;
        }


        private NetFormTable _CategoryLevelForm;
        private NetFormTable CategoryLevelForm
        {
            get
            {
                if (_CategoryLevelForm == null)
                {
                    _CategoryLevelForm = new NetFormTable("customtables_fieldsgroupscategorylevellabels", "id_levellabel", this.Conn);

                    NetHiddenField tab = new NetHiddenField("table_levellabel");
                    tab.Value = this.TableRequest.StringValue;
                    _CategoryLevelForm.addField(tab);

                    NetDropDownList lang = new NetDropDownList("Lingua", "lang_levellabel");
                    lang.ClearList();
                    foreach(DataRow row in this.Conn.SqlQuery("SELECT * FROM lang").Rows)
                        lang.addItem(row["Nome_Lang"].ToString(), row["id_Lang"].ToString());
                    lang.Required = true;
                    _CategoryLevelForm.addField(lang);

                    NetTextBoxPlus depth = new NetTextBoxPlus("Livello", "depth_levellabel",NetTextBoxPlus.ContentTypes.Numeric);
                    _CategoryLevelForm.addField(depth);
                    depth.Required = true;


                    NetTextBoxPlus label = new NetTextBoxPlus("Label", "label_levellabel", NetTextBoxPlus.ContentTypes.NormalText);
                    _CategoryLevelForm.addField(label);
                    label.Required = true;
                }
                return _CategoryLevelForm;
            }
        }
        public HtmlGenericControl TableLevelsView()
        {
            if (!TableRequest.IsValidInteger)
               NetCms.Diagnostics.Diagnostics.Redirect("../default.aspx");

            HtmlGenericControl Div = new HtmlGenericControl("div");
            HtmlGenericControls.InputField deleteField = new HtmlGenericControls.InputField("DeleteField");
            if (deleteField.RequestVariable.IsValidInteger)
                Conn.Execute("DELETE FROM customtables_fieldsgroupscategorylevellabels WHERE id_levellabel = " + deleteField.RequestVariable.IntValue);
            Div.Controls.Add(deleteField.Control);

            NetTable.SmTable  table = new SmTable("SELECT * FROM customtables_fieldsgroupscategorylevellabels INNER JOIN lang on id_lang = lang_levellabel WHERE table_levellabel = " + this.TableRequest + " ORDER BY  depth_levellabel,lang_levellabel", this.Conn);
            table.AddPaginationControl = true;
            table.OverTitle = "Etichette delle form per livello";
            table.RecordPerPagina = 20;

            SmTableColumn col = new SmTableColumn("depth_levellabel", "Livello");
            table.addColumn(col);

            col = new SmTableColumn("nome_lang", "Lingua");
            table.addColumn(col);

            col = new SmTableColumn("label_levellabel", "Label");
            table.addColumn(col);

            col = new SmTableActionColumn("id_levellabel", "action details", "Modifica", "groups_categories_levels_mod.aspx?ctid={0}&amp;tbid=" + this.TableRequest);
            table.addColumn(col);
  /*
            col = new SmTableActionColumn("id_fieldsgroupscategory", "action details", "Modifica Etichette Categoria", "groups_categories_mod_labels.aspx?ctid={0}&amp;tbid=" + this.TableRequest);
            table.addColumn(col);
  */
            col = new SmTableActionColumn("id_levellabel", "action delete", "Elimina", "javascript: setFormValueConfirm({0},'" + deleteField.ID + "',1,'Sei Sicuro di eliminare questa tabella?')");
            table.addColumn(col);
          
            Div.Controls.Add(table.getTable());

            return Div;
        }
        public HtmlGenericControl AddLevelLabelsView()       
        {
            if (!TableRequest.IsValidInteger)
               NetCms.Diagnostics.Diagnostics.Redirect("../default.aspx");
            NetForm form = new NetForm();
            form.AddTable(CategoryLevelForm);

            form.SubmitLabel = "Crea Etichetta";

            string errors = "";
            if (form.CheckSubmitSource)
            {
                errors = form.serializedInsert();
                
            }

            return form.getForms("Creazione Nuova Etichetta", errors);
        }
        public HtmlGenericControl ModLevelLabelsView()
        {
            if (!TableRequest.IsValidInteger)
               NetCms.Diagnostics.Diagnostics.Redirect("../default.aspx");
            NetUtility.RequestVariable id = new NetUtility.RequestVariable("ctid", NetUtility.RequestVariable.RequestType.QueryString);
            if (!id.IsValidInteger)
                return this.TableLevelsView();

            NetForm form = new NetForm();
            form.AddTable(CategoryLevelForm);
            CategoryLevelForm.ID = id.IntValue;
            form.SubmitLabel = "Modifica Etichetta";

            string errors = "";
            if (form.CheckSubmitSource)
            {
                errors = form.serializedUpdate();

            }

            return form.getForms("Modifica Etichetta", errors);
        }
    }
}