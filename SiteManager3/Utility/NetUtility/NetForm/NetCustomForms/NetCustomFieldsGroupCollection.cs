using System;
using System.Collections;

namespace NetForms.Custom
{
    public class NetCustomFieldsGroupCollection : IEnumerable
    {
        private NetCollection coll;
        public int Count { get { return coll.Count; } }
        public NetCustomFieldsGroupCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new NetCollection();
        }

        public void Add(NetCustomFieldsGroup group, string key)
        {
            coll.Add(key, group);
        }
        public void Add(NetCustomFieldsGroup group)
        {
            coll.Add("Group" + group.ID, group);
        }

        public void Remove(int ID)
        {
            coll.Remove("Group" + ID);
        }

        public void Clear()
        {
            coll.Clear();
        }

        public NetCustomFieldsGroup this[int i]
        {
            get
            {
                NetCustomFieldsGroup str = (NetCustomFieldsGroup)coll[coll.Keys[i]];
                return str;
            }
        }

        public NetCustomFieldsGroup this[string ID]
        {
            get
            {
                NetCustomFieldsGroup val = (NetCustomFieldsGroup)coll["Group" + ID];
                return val;
            }
        }

        public bool Contains(int ID)
        {
            return coll["Group" + ID] != null;
        }

        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private NetCustomFieldsGroupCollection Collection;
            public CollectionEnumerator(NetCustomFieldsGroupCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}