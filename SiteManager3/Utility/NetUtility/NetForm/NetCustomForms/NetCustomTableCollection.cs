using System;
using System.Collections;

namespace NetForms.Custom
{
    public class NetCustomTableCollection : IEnumerable
    {
        private NetCollection coll;
        public int Count { get { return coll.Count; } }
        public NetCustomTableCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new NetCollection();
        }

        public void Add(NetCustomTable table, string key)
        {
            coll.Add(key, table);
        }
        public void Add(NetCustomTable table)
        {
            coll.Add("Table" + table.ID, table);
        }

        public void Remove(int ID)
        {
            coll.Remove("Table" + ID);
        }

        public void Clear()
        {
            coll.Clear();
        }

        public NetCustomTable this[int i]
        {
            get
            {
                NetCustomTable str = (NetCustomTable)coll[coll.Keys[i]];
                return str;
            }
        }

        public NetCustomTable this[string ID]
        {
            get
            {
                NetCustomTable val = (NetCustomTable)coll["Table" + ID];
                return val;
            }
        }

        public bool Contains(int ID)
        {
            return coll["Table" + ID] != null;
        }

        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private NetCustomTableCollection Collection;
            public CollectionEnumerator(NetCustomTableCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}