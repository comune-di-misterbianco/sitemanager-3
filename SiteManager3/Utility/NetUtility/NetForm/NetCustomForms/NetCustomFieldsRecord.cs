using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms.Custom
{
    public class NetCustomFieldsRecord
    {
        protected NetFormLabels NLabels
        {
            get
            {
                return NetUtility.LabelsCacher.NetFormLabels;
            }
        }

        private DataRow _RecordData;
        protected DataRow RecordData
        {
            get 
            {
                if (_RecordData == null && Conn != null && ID > 0)
                {
                    DataTable tab = Conn.SqlQuery("SELECT * FROM CustomTables_Records WHERE id_Record = " + ID);
                    if (tab.Rows.Count > 0)
                        _RecordData = tab.Rows[0];
                }
                return _RecordData; 
            }
        }

        protected int _DefaultLangID;
        public int DefaultLangID
        {
            get
            {
                if (_DefaultLangID == 0)
                {
                    DataTable tab = this.Conn.SqlQuery("SELECT * FROM Lang WHERE Default_Lang = 1");
                    if (tab.Rows.Count > 0)
                    {
                        _DefaultLangID = int.Parse(tab.Rows[0]["id_Lang"].ToString());
                    }
                }
                return _DefaultLangID;
            }
        }

        private StringCollection _AssociatedGroups;
        public StringCollection AssociatedGroups
        {
            get
            {
                if (_AssociatedGroups == null)
                {
                    string sql = "SELECT * FROM customtables_fieldsgroupscategoriesvalues";
                    sql += " INNER JOIN customtables_fieldsgroupscategories ON Category_CategoryValue = id_fieldsgroupscategory ";
                    sql += " INNER JOIN customtables_fieldsgroupscategoriesjoins ON id_fieldsgroupscategory = Category_GroupCategoryJoin ";
                    sql += " WHERE Record_CategoryValue = " + Table.ID;

                    DataTable tab = Conn.SqlQuery(sql);
                    foreach (DataRow row in tab.Rows)
                    {
                        NetCustomField field = NetCustomField.FieldFactory(row);
                        if (this.Conn != null)
                            field.Conn = this.Conn;

                        field.ValueRecordID = this.ID;
                        Fields.Add(field);
                    }
                }
                return _AssociatedGroups;
            }
        }

        protected int _ID;
        public int ID
        {
            get 
            {
                if (_ID == 0 && this.RecordData != null)
                    _ID = int.Parse(this.RecordData["id_Record"].ToString());

                return _ID; 
            }
        }

        private NetCustomTable _Table;
        public NetCustomTable Table
        {
            get { return _Table; }
            set { _Table = value; }
        }

        private NetCustomFieldCollection _Fields;
        public NetCustomFieldCollection Fields
        {
            get 
            {
                if (_Fields == null)
                {
                    _Fields = new NetCustomFieldCollection();
                    /*
                    string sql = "SELECT * FROM CustomTables_Fields";
                    sql+= "  INNER JOIN CustomTables_FieldsLabels ON id_CustomField = Field_FieldLabel ";
                    sql += " INNER JOIN CustomTables_FieldsTypes ON id_FieldsType = Type_CustomField ";
                    sql += " INNER JOIN CustomTables_FieldsJoins ON Field_FieldJoin = id_CustomField ";
                    sql += " INNER JOIN CustomTables_FieldsGroups ON Group_FieldJoin = id_FieldsGroup ";
                    sql += " INNER JOIN CustomTables_Tables ON Table_FieldsGroup = id_FieldsGroup ";
                    sql += " INNER JOIN CustomTables_Records ON Table_Record = id_CustomTable ";
                    sql += " WHERE id_Record = " + this.ID;
                    */

                    string sql = @"SELECT *
FROM
  customtables_fieldsgroups
  INNER JOIN customtables_records ON (customtables_fieldsgroups.Table_FieldsGroup = customtables_records.Table_Record)
  INNER JOIN customtables_fieldsjoins ON (customtables_fieldsgroups.id_FieldsGroup = customtables_fieldsjoins.Group_FieldJoin)
  INNER JOIN customtables_fields ON (customtables_fieldsjoins.Field_FieldJoin = customtables_fields.id_CustomField)
  INNER JOIN customtables_fieldsvalues ON (customtables_fields.id_CustomField = customtables_fieldsvalues.Field_FieldValue)
  INNER JOIN CustomTables_FieldsLabels ON id_CustomField = Field_FieldLabel
WHERE
  (Lang_FieldLabel = "+this.DefaultLangID+@") AND
  (id_record = record_fieldValue) AND
  (record_fieldValue = " + this.ID + @")";
                    DataTable tab = Conn.SqlQuery(sql);
                    foreach (DataRow row in tab.Rows)
                    {
                        NetCustomField field = NetCustomField.FieldFactory(row);
                        if(this.Conn!=null)
                            field.Conn = this.Conn;
                        
                        field.ValueRecordID = this.ID;
                        Fields.Add(field);
                    }
                }
                return _Fields; 
            }
        }
	
        private NetCms.Connections.Connection _Conn;
        public NetCms.Connections.Connection Conn
        {
            get
            {
                return _Conn;
            }
            set
            {
                _Conn = value;
            }
        }

        public NetCustomFieldsRecord(DataRow recordData, NetCms.Connections.Connection conn)
        {
            _RecordData = recordData;
            _Conn = conn;
        }
        public NetCustomFieldsRecord(int id, NetCms.Connections.Connection conn)
        {
            _Conn = conn;
            _ID = id;
        }

        public HtmlGenericControl GetTableRowControl()
        {
            HtmlGenericControl tr = new HtmlGenericControl("tr");
            HtmlGenericControl td = new HtmlGenericControl("td");

            foreach (NetCustomField field in this.Fields)
                if(field.AddToListView)
                    tr.Controls.Add(field.GetCellControl());

            if (this.Table.DetailsOption == NetCustomTable.DetailsOptions.Enabled)
            {
                td = new HtmlGenericControl("td");
                td.Attributes["class"] = "action details";
                td.InnerHtml = "<a title=\"" + NLabels[NetFormLabels.NetFormLabelsList.Scheda] + "\" href=\"details.aspx?id=" + this.Table.ID + "&amp;crid=" + this.ID + "\"><span>" + NLabels[NetFormLabels.NetFormLabelsList.Scheda] + "</span></a>";
                tr.Controls.Add(td);
            } 

            if (this.Table.ModifyOption == NetCustomTable.ModifyOptions.Enabled)
            {
                td = new HtmlGenericControl("td");
                td.Attributes["class"] = "action modify";
                td.InnerHtml = "<a title=\"" + NLabels[NetFormLabels.NetFormLabelsList.Modifica] + "\" href=\"modify.aspx?id=" + this.Table.ID + "&amp;crid=" + this.ID + "\"><span>" + NLabels[NetFormLabels.NetFormLabelsList.Modifica] + "</span></a>";
                tr.Controls.Add(td);
            } 
            
            if (this.Table.DeleteOption == NetCustomTable.DeleteOptions.Enabled)
            {
                td = new HtmlGenericControl("td");
                td.Attributes["class"] = "action delete";
                td.InnerHtml = "<a title=\"" + NLabels[NetFormLabels.NetFormLabelsList.Elimina] + "\" href=\"javascript: setFormValue();\"><span>" + NLabels[NetFormLabels.NetFormLabelsList.Elimina] + "</span></a>";
                tr.Controls.Add(td);
            }
            return tr;
        }
        public HtmlGenericControl GetDetailControl()
        {
            HtmlGenericControl ctr = new HtmlGenericControl("div");

            bool even = false;
            foreach (NetCustomField field in this.Fields)
            {
                HtmlGenericControl element = field.GetDetailControl();
                if (even)
                    element.Attributes["class"] += " alternate";
                even = !even;
                ctr.Controls.Add(element);
            }
           
            return ctr;
        }
        public HtmlGenericControl GetHeadRowControl()
        {
            HtmlGenericControl tr = new HtmlGenericControl("tr");

            HtmlGenericControl th = new HtmlGenericControl("th");
            foreach (NetCustomField field in this.Fields)
                if (field.AddToListView)
                    {
                        th = new HtmlGenericControl("th");
                        th.InnerHtml = field.Label;
                        tr.Controls.Add(th);
                    }

            if (this.Table.DetailsOption == NetCustomTable.DetailsOptions.Enabled)
            {
                th = new HtmlGenericControl("th");
                th.InnerHtml = NLabels[NetFormLabels.NetFormLabelsList.Scheda].ToString();
                tr.Controls.Add(th);
            }
            if (this.Table.ModifyOption == NetCustomTable.ModifyOptions.Enabled)
            {
                th = new HtmlGenericControl("th");
                th.InnerHtml = NLabels[NetFormLabels.NetFormLabelsList.Modifica].ToString();
                tr.Controls.Add(th);
            }
            if (this.Table.DeleteOption == NetCustomTable.DeleteOptions.Enabled)
            {
                th = new HtmlGenericControl("th");
                th.InnerHtml = NLabels[NetFormLabels.NetFormLabelsList.Elimina].ToString();
                tr.Controls.Add(th);
            }

            return tr;
        }
    }

}