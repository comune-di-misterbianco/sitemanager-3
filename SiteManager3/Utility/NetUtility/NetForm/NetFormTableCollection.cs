using System.Collections;
using System;

/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>

namespace NetForms
{
    public class NetAbstractFormTableCollection : IEnumerable
    {
        private NetCollection coll;
        public int Count { get { return coll.Count; } }
        public NetAbstractFormTableCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new NetCollection();
        }

        public void Add(NetAbstractFormTable table)
        {
            coll.Add(table.TableName, table);
        }

        public NetAbstractFormTable this[int i]
        {
            get
            {
                NetAbstractFormTable table = (NetAbstractFormTable)coll[coll.Keys[i]];
                return table;
            }
        }

        public NetAbstractFormTable this[string str]
        {
            get
            {
                NetAbstractFormTable table = (NetAbstractFormTable)coll[str];
                return table;
            }
        }

        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private NetAbstractFormTableCollection Collection;
            public CollectionEnumerator(NetAbstractFormTableCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}