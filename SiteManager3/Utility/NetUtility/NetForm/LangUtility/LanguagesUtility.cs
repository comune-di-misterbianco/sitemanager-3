using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetUtility.Languages
{
    public class LanguagesUtility
    {
       public static int GetParentLangByID(int id,NetCms.Connections.Connection Conn)
       {
           DataTable Langs = Conn.SqlQuery("SELECT * FROM Lang");

           int output = 0;

           DataRow[] rows = Langs.Select("id_Lang = "+id);
           if (rows.Length > 0 && int.TryParse(rows[0]["Parent_Lang"].ToString(),out output));

           return output;
       }

        public static DataRow LangRecordSelector(int LangID,string SQLQuery,NetCms.Connections.Connection Conn)
        {
            DataRow output = null;
            int SelectedCurrentLangID = LangID;

            do
            {
                DataTable tab = Conn.SqlQuery(SQLQuery.Replace("[LANG]",SelectedCurrentLangID.ToString()));
                if (tab.Rows.Count > 0)
                {
                    output = tab.Rows[0];
                    SelectedCurrentLangID = 0;
                }
                else
                    SelectedCurrentLangID = NetUtility.Languages.LanguagesUtility.GetParentLangByID(SelectedCurrentLangID, Conn);
            } while (output == null && SelectedCurrentLangID > 0);

            return output;
        }
    }

}