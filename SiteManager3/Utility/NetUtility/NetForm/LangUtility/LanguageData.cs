using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetUtility.Languages
{
    public class LanguageData
    {
        private DataRow _LangData;
        private DataRow LangData
        {
            get { return _LangData; }
        }

        private int _ParentID = -1;
        public int ParentID
        {
            get
            {

                if (_ParentID == -1)
                {
                    _ParentID = int.Parse(this.LangData["Parent_Lang"].ToString());
                }
                return _ParentID;
            }
        }

        private int _ID;
        public int ID
        {
            get
            {

                if (_ID == 0)
                    _ID = int.Parse(this.LangData["id_Lang"].ToString());
                return _ID;
            }
        }

        private string _CultureCode;
        public string CultureCode
        {
            get 
            {
                if (_CultureCode == null)
                {
                    _CultureCode = this.LangData["CultureCode_Lang"].ToString();
                }
                return _CultureCode; 
            }
        }

        private string _Nome;
        public string Nome
        {
            get
            {
                if (_Nome == null)
                {
                    _Nome = this.LangData["Nome_Lang"].ToString();
                }
                return _Nome;
            }
        }

        private int _IsDefault = -1;
        public bool IsDefault
        {
            get
            {

                if (_IsDefault == -1)
                {
                    _IsDefault = int.Parse(this.LangData["Parent_Lang"].ToString());
                }
                return _IsDefault == 1;
            }
        }

        private LanguageData _Parent;
        public LanguageData Parent
        {
            get
            {
                if (_Parent == null)
                {
                    _Parent = new LanguageData(this.ParentID, this.Conn);
                }
                return _Parent;
            }
        }

        private NetCms.Connections.Connection Conn;

        public LanguageData(NetCms.Connections.Connection conn)
        {
            Conn = conn;
            InitLangData("SELECT * FROM Lang WHERE CultureCode_Lang = '" + System.Globalization.CultureInfo.CurrentCulture.IetfLanguageTag + "'");
        }
        public LanguageData(int LangID, NetCms.Connections.Connection conn)
        {
            Conn = conn;
            InitLangData("SELECT * FROM Lang WHERE id_Lang = " + LangID);
        }
        public LanguageData(string CultureCode, NetCms.Connections.Connection conn)
        {
            Conn = conn;
            InitLangData("SELECT * FROM Lang WHERE CultureCode_Lang = '" + CultureCode + "'");
        }

        public void InitLangData(string sqlQuery)
        {
            DataTable Langs = Conn.SqlQuery(sqlQuery);
            if (Langs.Rows.Count > 0)
                _LangData = Langs.Rows[0];
        }
    }

}