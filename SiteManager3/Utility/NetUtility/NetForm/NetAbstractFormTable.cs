using System.Data;
using System.Collections;
using System.Collections.Specialized;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetFormTable
/// </summary>
namespace NetForms
{
    public abstract class NetAbstractFormTable
    {
        private HtmlGenericControls.InputField _PostbackCheckField;
        public HtmlGenericControls.InputField PostbackCheckField
        {
            get
            {
                if (_PostbackCheckField == null)
                {
                    _PostbackCheckField = new HtmlGenericControls.InputField("PostbackCheckField_" + this.TableName);
                    _PostbackCheckField.AutoLoadValueAfterPostback = true;
                    _PostbackCheckField.Value = "PostedBack";
                }
                return _PostbackCheckField;
            }
        }

        public bool IsPostBack
        {
            get
            {
                return PostbackCheckField.RequestVariable.IsPostBack;
            }
        }
        
        public ArrayList TableStringsForLog
        {
            get
            {
                if (_TableStringsForLog == null)
                    _TableStringsForLog = new ArrayList();
                return _TableStringsForLog;
            }
            set { _TableStringsForLog = value; }
        }
        private ArrayList _TableStringsForLog;
        
        protected NetFormLabels NLabels
        {
            get
            {
                return NetUtility.LabelsCacher.NetFormLabels;
            }
        }

        private NetUtility.Languages.LanguageData _CurrentLang;
        protected NetUtility.Languages.LanguageData CurrentLang
        {
            get 
            {
                if (_CurrentLang == null)
                {
                    _CurrentLang = new NetUtility.Languages.LanguageData(this.Conn);
                }
                return _CurrentLang; 
            }
        }
	

        protected int _DefaultLangID;
        public int DefaultLangID
        {
            get
            {
                if (_DefaultLangID == 0)
                {
                    DataTable tab = this.Conn.SqlQuery("SELECT * FROM Lang WHERE Default_Lang = 1");
                    if (tab.Rows.Count > 0)
                    {
                        _DefaultLangID = int.Parse(tab.Rows[0]["id_Lang"].ToString());
                    }
                }
                return _DefaultLangID;
            }
        }

        protected NetExternalFieldCollection _ExternalFields;
        public virtual NetExternalFieldCollection ExternalFields
        {
            get
            {
                return _ExternalFields;
            }
        }
        
        private NetCms.Connections.Connection _Conn;
        protected NetCms.Connections.Connection Conn
        {
            get
            {
                return _Conn;
            }
        }

        public abstract string TableName { get;}
        public abstract int LastInsert { get;}
        public abstract string TableLabel { get;set;}
        protected string _InfoText;
        public virtual string InfoText 
        {
            get
            {
                if (_InfoText == null)
                    _InfoText = this.NLabels[NetFormLabels.NetFormLabelsList.DefaultInfoText];
                return _InfoText;
            }
            set
            {
                _InfoText = value;
            }
        }

        public NetAbstractFormTable()
        {
            _ExternalFields = new NetExternalFieldCollection();
        }
        public NetAbstractFormTable(NetCms.Connections.Connection conn)
            :this()
        { _Conn = conn; }
        
        public virtual void addExternalField(NetExternalField field)
        {
            ExternalFields.Add(field);
        }
        public abstract void DataBind(System.Collections.Specialized.NameValueCollection nameValueCollection);

        public abstract HtmlGenericControl GetControl(params NetForm[] parentForm);
        public abstract HtmlGenericControl GetOverviewControl();

        public abstract bool Insert();
        public abstract bool Insert(NameValueCollection PostData);
        public abstract bool Update();
        public abstract bool Update(NameValueCollection PostData);
        public abstract bool Update(int RecordID);
        public abstract bool Update(int RecordID, NameValueCollection PostData);

        public abstract string ValidateInput(params NetForm[] parentForm);
    }
}