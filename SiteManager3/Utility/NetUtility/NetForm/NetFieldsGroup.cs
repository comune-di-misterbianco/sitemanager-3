using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetFieldsGroup
    {
        private string _CssClass;
        public string CssClass
        {
            get 
            {
                return _CssClass; 
            }
            set { _CssClass = value; }
        }	

        private string _InfoText;
        public string InfoText
        {
            get 
            {
                return _InfoText; 
            }
            set { _InfoText = value; }
        }

        private string _Label;
        public string Label
        {
            get
            {
                return _Label;
            }
            set { _Label = value; }
        }

        private NetAbstractFormTable _Table;
        public NetAbstractFormTable Table
        {
            get 
            {
                return _Table; 
            }
        }

        private NetFieldsCollection _Fields;
        public NetFieldsCollection Fields
        {
            get 
            {
                return _Fields; 
            }
        }

        public NetFieldsGroup(NetAbstractFormTable table)
        {
            _Fields = new NetFieldsCollection();
            _Table = table;
        }

        public HtmlGenericControl GetControl()
        {
            HtmlGenericControl Control = new HtmlGenericControl("fieldset");
            Control.Attributes["class"] = "CustomTable_Fieldset" + this.CssClass;
            
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            legend.InnerHtml = this.Label;
            Control.Controls.Add(legend);

            if (this.InfoText != null && this.InfoText.Length > 0)
            {
                HtmlGenericControl info = new HtmlGenericControl("div");
                info.InnerHtml = this.InfoText;
                Control.Controls.Add(info);
            }

            foreach (NetField field in this.Fields)
                Control.Controls.Add(field.getControl());            

            return Control;
        }
        
    }

}