using System;
using System.Collections;

namespace NetForms
{
    public class NetFieldsGroupCollection : IEnumerable
    {
        private NetCollection coll;
        public int Count { get { return coll.Count; } }
        public NetFieldsGroupCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new NetCollection();
        }

        public void Add(NetFieldsGroup group, string key)
        {
            coll.Add(key, group);
        }
        public void Add(NetFieldsGroup group)
        {
            coll.Add("Group" + this.Count, group);
        }

        public void Clear()
        {
            coll.Clear();
        }

        public NetFieldsGroup this[int i]
        {
            get
            {
                NetFieldsGroup str = (NetFieldsGroup)coll[coll.Keys[i]];
                return str;
            }
        }

        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private NetFieldsGroupCollection Collection;
            public CollectionEnumerator(NetFieldsGroupCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}