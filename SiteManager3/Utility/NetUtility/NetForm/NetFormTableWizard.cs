using System;
using System.Data;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI.HtmlControls;
using NetCms.Connections;
using NetCms.Diagnostics;

/// <summary>
/// Summary description for NetFormTable
/// </summary>
namespace NetForms
{
    public class NetFormTableWizard : NetAbstractFormTable
    {
        protected NetFieldsGroupCollection _Groups;
        public NetFieldsGroupCollection Groups
        {
            get
            {
                return _Groups;
            }
        }

        private bool _ShowTableInfo = true;
        public bool ShowTableInfo
        {
            get { return _ShowTableInfo; }
            set { _ShowTableInfo = value; }
        }	

        private string _TableLabel;
        public override string TableLabel
        {
            get { return _TableLabel; }
            set { _TableLabel = value; }
        }

        private string _TableName;
        public override string TableName
        {
            get
            {
                return _TableName;
            }
        }

        private string _PrimaryKey;
        public string PrimaryKey
        {
            get
            {
                return _PrimaryKey;
            }
        }

        private int _ID = 0;
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }

        private bool BindingsLoaded;

        protected int _LastInsert;
        public override int LastInsert
        {
            get
            {
                return _LastInsert;
            }
        }

        #region Costruttori

        public NetFormTableWizard(string tableName, string primaryKey)
            : this(tableName, primaryKey, null, 0)
        {
        }
        public NetFormTableWizard(string tableName, string primaryKey, int id)
            : this(tableName, primaryKey, null, id)
        {
            _ID = id;
        }
        public NetFormTableWizard(string tableName, string primaryKey, Connection conn)
            : this(tableName, primaryKey, conn, 0)
        {
        }
        public NetFormTableWizard(string tableName, string primaryKey, Connection conn, int id)
            : base(conn)
        {
            _ID = id;
            _TableName = tableName;
            _PrimaryKey = primaryKey;
            _Groups = new NetFieldsGroupCollection();
        } 

        #endregion

        public virtual void bindFields()
        {
            if (ID != 0 && !BindingsLoaded)
            {
                string sql = "";
                sql += "SELECT * FROM " + TableName;
                sql += " WHERE " + PrimaryKey + " = " + ID;

                DataTable table = Conn.SqlQuery(sql);
                foreach (DataRow row in table.Rows)
                {
                    foreach (NetFieldsGroup group in this.Groups)
                        foreach (NetField field in group.Fields)
                    {
                        if (table.Columns.Contains(field.getFieldName()))
                            field.Value = row[field.getFieldName()].ToString();
                    }
                }

                BindingsLoaded = true;
            }
        }
        public virtual void bindFields(DataRow row)
        {
            if (row != null && !BindingsLoaded)
            {
                foreach (NetFieldsGroup group in this.Groups)
                    foreach (NetField field in group.Fields)
                        field.Value = row[field.getFieldName()].ToString();
                BindingsLoaded = true;
            }
        }
        public virtual void bindFields(DataTable table)
        {
            if (table != null && !BindingsLoaded)
            {
                DataRow[] row = table.Select(this.PrimaryKey + " = " + ID);
                if(row.Length>0)
                    bindFields(row[0]);
                BindingsLoaded = true;
            }
        }
        public override void DataBind(NameValueCollection nameValueCollection)
        {
            if (!BindingsLoaded)
            {
                foreach (NetFieldsGroup group in this.Groups)
                    foreach (NetField field in group.Fields)
                {
                    if (nameValueCollection[field.FieldName] != null)
                        field.Value = nameValueCollection[field.FieldName];
                }
            }
        }
        public HtmlGenericControl getForm()
        {
            bindFields();
            HtmlGenericControl div = new HtmlGenericControl("div");

            HtmlGenericControls.Fieldset set = new HtmlGenericControls.Fieldset(this.TableLabel,new HtmlGenericControl("div"));
            set.Content.InnerHtml = this.InfoText;
            if(ShowTableInfo)
                div.Controls.Add(set);

            foreach (NetFieldsGroup group in this.Groups)
                div.Controls.Add(group.GetControl());

            if (!IsPostBack)
                Diagnostics.TraceMessage(TraceLevel.Verbose, "Accesso alla tabella " + this.TableName, "", "","");

            return div;
        }
        public override HtmlGenericControl GetControl(params NetForm[] parentForm)
        {
            return getForm();
        }
        public override HtmlGenericControl GetOverviewControl()
        {
            HtmlGenericControls.Fieldset control = new HtmlGenericControls.Fieldset(this.TableLabel);
            control.Class = "OverviewFieldset";
            foreach (NetFieldsGroup group in this.Groups)
            {
                HtmlGenericControl gtitle = new HtmlGenericControl("p");
                control.Controls.Add(gtitle);
                gtitle.InnerHtml = "<em>" + group.Label + "</em>";

                HtmlGenericControl list = new HtmlGenericControl("ul");
                control.Controls.Add(list);

                foreach (NetField field in group.Fields)
                    if(field.ShowInInOverview)
                        list.Controls.Add(field.GetOverviewControl());

            }

            return control;
        }
        public override string ValidateInput(params NetForm[] parentForm)
        {
            return validateForm(parentForm);
        }

        public string validateForm(params NetForm[] parentForm)
        {
            return validateForm(HttpContext.Current.Request.Form, parentForm);
        }
        public string validateForm(NameValueCollection values, params NetForm[] parentForm)
        {
            string errors = "";
            string input = "";
            foreach (NetFieldsGroup group in this.Groups)
                foreach (NetField field in group.Fields)
                {
                    field.CurrentRequest = values;
                    input = "";
                    if (values.Get(field.getFieldName()) != null)
                        input = values.Get(field.getFieldName()).ToString();


                    string resultFieldValidation;

                    errors += (resultFieldValidation = field.validateInput(input));

                    if (resultFieldValidation != String.Empty)
                    {
                        if (field.PostBackValue != "")
                            field.FieldStringForLog = "L'utente ha inserito nel campo " + field.Label.Replace(":", "") + " della tabella " + this.TableName + " il valore: " + field.PostBackValue;
                        else field.FieldStringForLog = "L'utente non ha valorizzato il campo " + field.Label.Replace(":", "") + " della tabella " + this.TableName;

                    }

                    if (field.FieldStringForLog != String.Empty && parentForm.Length != 0)
                        this.TableStringsForLog.Add(field.FieldStringForLog);

                    if (parentForm.Length == 0 && field.FieldStringForLog != String.Empty)
                    {
                        Diagnostics.TraceMessage(TraceLevel.Warning, field.FieldStringForLog, "", "","");
                    }

                }
            if (errors.Length > 0)
                return errors;
            else
                return null;
        }

        public override bool Insert()
        {
            insertQuery(HttpContext.Current.Request.Form);
            return true;
        }
        public override bool Insert(NameValueCollection PostData)
        {
            insertQuery(PostData);
            return true;
        }
        public override bool Update(int RecordID)
        {
            this.ID = RecordID;
            updateQuery(HttpContext.Current.Request.Form);
            return true;
        }
        public override bool Update()
        {
            updateQuery(HttpContext.Current.Request.Form);
            return true;
        }
        public override bool Update(NameValueCollection PostData)
        {
            updateQuery(PostData);
            return true;
        }
        public override bool Update(int RecordID, NameValueCollection PostData)
        {
            this.ID = RecordID;
            updateQuery(PostData);
            return true;
        }

        public string insertQuery()
        {
            return insertQuery(HttpContext.Current.Request.Form);
        }
        public virtual string insertQuery(NameValueCollection post)
        {
            string valid = validateForm(post);

            if (valid != null)
            {
                return "<ul>" + valid + "</ul>";
            }

            string values = "";
            string where = "";
            string fields = "";
            bool first = true;

            foreach (NetFieldsGroup group in this.Groups)
                foreach (NetField field in group.Fields)
            {
                if (!field.JumpField)
                {
                    if (first) first = false;
                    else
                    {
                        values += ",";
                        fields += ",";
                        where += " AND ";
                    }
                    string value = "";
                    if (post.Get(field.getFieldName()) != null)
                    {
                        value = post.Get(field.getFieldName()).ToString();

                    }

                    value = field.validateValue(value);

                    values += value;
                    fields += field.getFieldName();
                    where += field.getFieldName() + " = " + value + "";
                }

            }
            for (int i = 0; i < ExternalFields.Count; i++)
            {
                values += "," + ExternalFields[i].Value;
                fields += "," + ExternalFields[i].FieldName;
            }

            string sql = "";

            sql += "INSERT INTO " + TableName;
            sql += " (" + fields + ")";
            sql += " VALUES(" + values + ")";

            if (Conn != null)
            {
                _LastInsert = Conn.ExecuteInsert(sql);
            }

            return sql;
        }

        public string updateQuery()
        {
            return updateQuery(HttpContext.Current.Request.Form);
        }       
        public virtual string updateQuery(NameValueCollection post)
        {
            if (ID == 0)
            {
                return "ID del record non impostato";
            }

            string valid = validateForm(post);

            if (valid != null)
            {
                return "<ul>" + valid + "</ul>";
            }

            string values = "";

            foreach (NetFieldsGroup group in this.Groups)
                foreach (NetField field in group.Fields)
                {

                    if (!field.JumpField)
                    {

                        if (values.Length>0) values += ",";
                        string value = "";
                        if (post.Get(field.getFieldName()) != null)
                            value = post.Get(field.getFieldName()).ToString();

                        value = field.validateValue(value);

                        values += field.getFieldName() + " = " + value;

                    }
                }
            string sql = "";

            sql += "UPDATE " + TableName + " SET ";
            sql += " " + values + "";
            sql += " WHERE " + PrimaryKey + " = " + ID;

            if(Conn!=null)
                Conn.Execute(sql);

            return sql;
        }

    }
}