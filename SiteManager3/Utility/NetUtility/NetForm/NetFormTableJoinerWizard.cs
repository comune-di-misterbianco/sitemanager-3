using System;
using System.Data;
using System.Data.Common;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Connections;
using NetCms.Diagnostics;

/// <summary>
/// Summary description for NetFormTable
/// </summary>
namespace NetForms
{
    public class NetFormTableJoinerWizard : NetAbstractFormTable
    {
        public override NetExternalFieldCollection ExternalFields
        {
            get
            {
                return FrontendTable.ExternalFields;
            }
        }
        public override void addExternalField(NetExternalField field)
        {
            this.FrontendTable.addExternalField(field);
        }
        protected NetAbstractFormTableCollection _Tables;
        public NetAbstractFormTableCollection Tables
        {
            get
            {
                return _Tables;
            }
        }

        private NetAbstractFormTable _FrontendTable;
        public NetAbstractFormTable FrontendTable
        {
            get 
            { 
                if(_FrontendTable == null)
                    _FrontendTable = Tables[0];

                return _FrontendTable; 
            }
            set { _FrontendTable = value; }
        }
	

        private bool _ShowTableInfo = true;
        public bool ShowTableInfo
        {
            get { return _ShowTableInfo; }
            set { _ShowTableInfo = value; }
        }	

        private string _TableLabel;
        public override string TableLabel
        {
            get { return _TableLabel; }
            set { _TableLabel = value; }
        }

        private string _TableName;
        public override string TableName
        {
            get
            {
                return _TableName;
            }
        }

        public override int LastInsert
        {
            get
            {
                return FrontendTable.LastInsert;
            }
        }

        #region Costruttori

        public NetFormTableJoinerWizard(string name)
        {
            _TableName = name;
            _Tables = new NetAbstractFormTableCollection();
        } 

        #endregion

        private bool BindingsLoaded;
        public override void DataBind(NameValueCollection nameValueCollection)
        {
            if (!BindingsLoaded)
            {
                foreach (NetAbstractFormTable table in this.Tables)
                    table.DataBind(nameValueCollection);
                BindingsLoaded = true;
            }
        }
        public override HtmlGenericControl GetControl(params NetForm[] parentForm)
        { 
            HtmlGenericControl div = new HtmlGenericControl("div");

            foreach (NetAbstractFormTable table in this.Tables)
                div.Controls.Add(table.GetControl());

            if (!IsPostBack)
                Diagnostics.TraceMessage(TraceLevel.Verbose, "Accesso alla tabella " + this.TableName, "", "","");

            return div;
        }
        public override HtmlGenericControl GetOverviewControl()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");

            foreach (NetAbstractFormTable table in this.Tables)
                div.Controls.Add(table.GetOverviewControl());

            return div;
        }
        public override string ValidateInput(params NetForm[] parentForm)
        {
            return validateForm(parentForm);
        }

        public string validateForm(params NetForm[] parentForm)
        {
            return validateForm(HttpContext.Current.Request.Form, parentForm);
        }
        public string validateForm(NameValueCollection values, params NetForm[] parentForm)
        {
            string errors = "";

            foreach (NetAbstractFormTable table in this.Tables)
                errors += table.ValidateInput();
              
            if (errors.Length > 0)
                return errors;
            else
                return null;
        }

        public override bool Insert()
        {
            bool esito = true;

            foreach (NetAbstractFormTable table in this.Tables)
            {
                if (esito)
                {
                    for (int j = 0; j < table.ExternalFields.Count; j++)
                    {
                        NetExternalField field = table.ExternalFields[j];

                        if (Tables[field.TableName] != null)
                            if (Tables[field.TableName].LastInsert != 0)
                                field.Value = Tables[field.TableName].LastInsert.ToString();
                    }

                    esito &= table.Insert();
                    
                }
            }
            return esito;
        }
        public override bool Insert(NameValueCollection PostData)
        {
            bool esito = true;

            foreach (NetAbstractFormTable table in this.Tables)
            {
                if (esito)
                {
                    for (int j = 0; j < table.ExternalFields.Count; j++)
                    {
                        NetExternalField field = table.ExternalFields[j];

                        if (Tables[field.TableName] != null)
                            if (Tables[field.TableName].LastInsert != 0)
                                field.Value = Tables[field.TableName].LastInsert.ToString();
                    }

                    esito &= table.Insert(PostData);
                }
            }
            return esito;
        }
        public override bool Update(int RecordID)
        {
            throw new Exception("Non saprei come impermentare questa funzione");
        }
        public override bool Update()
        {
            foreach (NetAbstractFormTable table in this.Tables)
                table.Update();
            return true;
        }
        public override bool Update(NameValueCollection PostData)
        {
            foreach (NetAbstractFormTable table in this.Tables)
                table.Update(PostData);
            return true;
        }
        public override bool Update(int RecordID, NameValueCollection PostData)
        {
            throw new Exception("Non saprei come impermentare questa funzione");
        }
    }
}