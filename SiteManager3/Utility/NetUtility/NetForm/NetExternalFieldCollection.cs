using System.Collections;
using System;

/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>
/// 
namespace NetForms
{
    public class NetExternalFieldCollection : IEnumerable
    {
        private NetCollection coll;
        public int Count { get { return coll.Count; } }
        public NetExternalFieldCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new NetCollection();
        }

        public void Add(NetExternalField field)
        {
            coll.Add(field.TableName, field);
        }

        public NetExternalField this[int i]
        {
            get
            {
                NetExternalField field = (NetExternalField)coll[coll.Keys[i]];
                return field;
            }
        }

        public NetExternalField this[string str]
        {
            get
            {
                NetExternalField field = (NetExternalField)coll[str];
                return field;
            }
        }


        public int count()
        {
            return coll.Count;
        }

        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private NetExternalFieldCollection Collection;
            public CollectionEnumerator(NetExternalFieldCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}