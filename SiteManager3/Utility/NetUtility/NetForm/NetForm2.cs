using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms.Custom;

/// <summary>
/// Summary description for NetFormAdvance
/// </summary>
namespace NetForms
{
    public class NetForm2
    {
        private NetAbstractFormTableCollection _Tables;
        public NetAbstractFormTableCollection Tables
        {
            get
            {
                return _Tables;
            }
        }
        
        private NetAbstractFormTableCollection _ExecuteOrder;
        public NetAbstractFormTableCollection ExecuteOrder
        {
            get
            {
                return _ExecuteOrder;
            }
            set
            {
                _ExecuteOrder = value;
            }
        }
               
        public enum LastOperationValues
        {
            NotYetExecute = 0,
            OK = 1,
            Error = 2
        }

        private LastOperationValues _LastOperation;
        public LastOperationValues LastOperation
        {
            get { return _LastOperation; }
        }

        protected NetFormLabels NLabels
        {
            get
            {
                return NetUtility.LabelsCacher.NetFormLabels;
            }
        }

        private bool _AddCapcha;
        public bool AddCapcha
        {
            get { return _AddCapcha; }
            set { _AddCapcha = value; }
        }

        private string _CaptchaLabel;
        public string CaptchaLabel
        {
            get { return _CaptchaLabel; }
            set { _CaptchaLabel = value; }
        }

        private NetCaptcha _CaptchaControl;
        private NetCaptcha CaptchaControl
        {
            get
            {
                if (_CaptchaControl == null)
                    _CaptchaControl = new NetCaptcha(this.CaptchaLabel, "captcha",true);
                return _CaptchaControl;
            }
        }

        private bool _AddDisclaimer;
        public bool AddDisclaimer
        {
            get { return _AddDisclaimer; }
            set { _AddDisclaimer = value; }
        }

        private DisclaimerControl _DisclaimerControl;
        public DisclaimerControl DisclaimerControl
        {
            get { if (_DisclaimerControl == null)_DisclaimerControl = new DisclaimerControl(); return _DisclaimerControl; }
        }
	
        private HtmlGenericControls.InputField _FormIDField;
        private HtmlGenericControls.InputField FormIDField
        {
            get
            {
                if (_FormIDField == null)
                {
                    
                    _FormIDField = new HtmlGenericControls.InputField("ActiveFormID");
                    if (_FormIDField.RequestVariable.IsValidString && _FormIDField.RequestVariable.StringValue != string.Empty)
                        _FormIDField.Value = _FormIDField.RequestVariable.StringValue;
                    else
                    {
                        _FormIDField.Value = "FormWizard_" + this.GetHashCode();
                        HttpContext.Current.Session["FormWizard_" + this.GetHashCode()] = true;
                    }

                }
                return _FormIDField;
            }
        }

        private NetUtility.RequestVariable endButtonRequest;
        private NetUtility.RequestVariable EndButtonRequest
        {
            get
            {
                if(endButtonRequest == null)
                endButtonRequest  = new NetUtility.RequestVariable(EndButton.ID);
            return endButtonRequest;
            }
        }
        private bool IsPostbackByEndButton
        {
            get
            {
                return EndButtonRequest.IsValidString && EndButton.Text == EndButtonRequest.StringValue;
            }
        }
        private bool LastPostbackValidationOk
        {
            get
            {
                return LastPostbackValidation == string.Empty;
            }
        }

        private string _LastPostbackValidation;
        private string LastPostbackValidation
        {
            get
            {
                if (_LastPostbackValidation == null)
                {
                  
                    {
                        _LastPostbackValidation = string.Empty;
                        foreach (NetAbstractFormTable table in this.Tables)
                            _LastPostbackValidation += table.ValidateInput();

                        if (this.AddCapcha)
                            _LastPostbackValidation += this.CaptchaControl.validateInput(this.CaptchaControl.RequestVariable.StringValue);
                        if (!this.DisclaimerControl.CheckAcceptance())
                            _LastPostbackValidation += this._LastPostbackValidation += "<li>" + this.NLabels[NetFormLabels.NetFormLabelsList.NecessarioAccettareCondizione] + "</li>";
                    }
                }
                return _LastPostbackValidation;
            }
        }

        #region Buttons

        private Button _EndButton;
        private Button EndButton
        {
            get
            {
                if (_EndButton == null)
                {
                    _EndButton = new Button();
                    _EndButton.Text = this.NLabels[NetFormLabels.NetFormLabelsList.Completa];
                    _EndButton.ID = "EndButton";
                    _EndButton.CssClass = "button";
                }
                return _EndButton;
            }
        }

        private System.Web.SessionState.HttpSessionState Session
        {
            get
            {
                return
                HttpContext.Current.Session;
            }
        }

        #endregion
              
        private string _ErrorsFieldsetLabel;
        public string ErrorsFieldsetLabel
        {
            get {
                if (_ErrorsFieldsetLabel == null)
                    _ErrorsFieldsetLabel = "Sono stati riscontrati i seguenti errori:";
                return _ErrorsFieldsetLabel; 
            }
            set { _ErrorsFieldsetLabel = value; }
        }


        public NetForm2()
        {
            _Tables = _ExecuteOrder = new NetAbstractFormTableCollection();
            _CaptchaLabel = "Immagine di Sicurezza";
        }
        public HtmlGenericControl GetInsertView()
        {
            return getForms(true);
        }
        public HtmlGenericControl GetUpdateView()
        {
            return getForms(false);
        }
        private HtmlGenericControl getForms(bool InsertView)
        {
            HtmlGenericControl div = new HtmlGenericControl("div");

            if (this.IsPostbackByEndButton && !this.LastPostbackValidationOk)
                div.Controls.Add(ErrorsControl);

            if (!this.IsPostbackByEndButton || !this.LastPostbackValidationOk)
            {
                foreach (NetAbstractFormTable table in Tables)
                    div.Controls.Add(table.GetControl());
                if (InsertView && this.AddDisclaimer && this.DisclaimerControl != null)
                    div.Controls.Add(this.DisclaimerControl.GetControl);
                if (this.AddCapcha)
                    div.Controls.Add(this.CaptchaControl.getControl());
                HtmlGenericControl p = new HtmlGenericControl("p");

                div.Controls.Add(p);
                p.Controls.Add(this.FormIDField.Control);
                p.Controls.Add(EndButton);
            }
            if (this.IsPostbackByEndButton && this.LastPostbackValidationOk)
            {
                if (InsertView) this.Insert();
                else this.Update();
            
                HtmlGenericControls.Fieldset riepilogo = new HtmlGenericControls.Fieldset(this.NLabels[NetFormLabels.NetFormLabelsList.CompletamentoProcedura], new HtmlGenericControls.Div());
                riepilogo.Content.InnerHtml = this.NLabels[NetFormLabels.NetFormLabelsList.RiepilogoDatiInseriti];
                riepilogo.Class = "OverviewFieldset";
                div.Controls.Add(riepilogo);
                div.Controls.Add(OverviewControl());
            }

            return div;
        }

        private HtmlGenericControl ErrorsControl
        {
            get
            {
                HtmlGenericControls.Div Div = new HtmlGenericControls.Div();
                HtmlGenericControls.Fieldset errori = new HtmlGenericControls.Fieldset(ErrorsFieldsetLabel);
                errori.Class = "ErrorsFieldset";

                HtmlGenericControls.Par p = new HtmlGenericControls.Par();
                p.InnerHtml = "<ul>" + this.LastPostbackValidation + "</ul>";
                errori.Controls.Add(p);

                Div.Controls.Add(errori);

                return Div;
            }
        }

        private HtmlGenericControl OverviewControl()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");

            foreach (NetAbstractFormTable table in this.Tables)
            {
                table.DataBind(HttpContext.Current.Request.Form);
                div.Controls.Add(table.GetOverviewControl());
            }
            return div;
        }
        private void Update()
        {
            bool esito = true;
            string output = string.Empty;

            foreach (NetAbstractFormTable table in this.Tables)
            {
                if (esito)
                {
                    if (this.Session[FormIDField.Value + "_" + table.TableName] != null)
                    {
                        NameValueCollection PostData = (NameValueCollection)this.Session[FormIDField.Value + "_" + table.TableName];
                        esito &= table.Update(PostData);
                    }
                    else esito = false;
                }
            }
            if (esito)
            {
                HttpContext.Current.Session[this.FormIDField.RequestVariable.StringValue] = null;
                this._LastOperation = LastOperationValues.OK;
            }
            else
                this._LastOperation = LastOperationValues.Error;
        }
        private void Insert()
        {
            bool esito = true;
            string output = string.Empty;

            foreach (NetAbstractFormTable table in ExecuteOrder)
            {
                if (esito)
                {
                    NameValueCollection PostData = HttpContext.Current.Request.Form;

                    for (int j = 0; j < table.ExternalFields.Count; j++)
                    {
                        NetExternalField field = table.ExternalFields[j];

                        if (Tables[field.TableName] != null)
                            if (Tables[field.TableName].LastInsert != 0)
                                field.Value = Tables[field.TableName].LastInsert.ToString();
                    }

                    esito &= table.Insert(PostData);
                }
            }
            if (esito) this._LastOperation = LastOperationValues.OK;
            else       this._LastOperation = LastOperationValues.Error;
            
        }
        
    }
}