using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetForms.Custom;
using NetCms.Diagnostics;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for NetFormAdvance
/// </summary>
namespace NetForms
{
    public class NetForm
    {
        private Hashtable _FormStringsForLog;
        public Hashtable FormStringsForLog
        {
            get
            {
                if (_FormStringsForLog == null)
                    _FormStringsForLog = new Hashtable();
                return _FormStringsForLog;
            }
            set { _FormStringsForLog = value; }
        }
        
        private NetAbstractFormTableCollection _Tables;
        public NetAbstractFormTableCollection Tables
        {
            get
            {
                return _Tables;
            }
        }

        protected NetFormLabels NLabels
        {
            get
            {
                return NetUtility.LabelsCacher.NetFormLabels;
            }
        }
                
        public enum LastOperationValues
        {
            NotYetExecute = 0,
            OK = 1,
            Error = 2
        }

        private LastOperationValues _LastOperation;
        public LastOperationValues LastOperation
        {
            get { return _LastOperation; }
        }

        private string _LastValidationResult;
        public string LastValidationResult
        {
            get { return _LastValidationResult; }
        }

        private string  _TestoCampiObbligatori;
        public string  TestoCampiObbligatori
        {
            get {
                if (_TestoCampiObbligatori == null)
                    _TestoCampiObbligatori = NLabels[NetFormLabels.NetFormLabelsList.CampiConAsteriscoObbligatori];
                return _TestoCampiObbligatori; 
            }
            set { _TestoCampiObbligatori = value; }
        }
	
        #region Back Button

        private bool _AddBackButton;
        public bool AddBackButton
        {
            get { return _AddBackButton && BackHref != null; }
            set { _AddBackButton = value; }
        }

        private Button _BackButton;
        private Button BackButton
        {
            get
            {
                if (_BackButton == null)
                {
                    _BackButton = new Button();
                    _BackButton.Text = BackLabel;
                    _BackButton.ID = BackSourceID;
                    _BackButton.CssClass = "button";
                    _BackButton.OnClientClick = "javascript: setSubmitSource('" + BackSourceID + "')";
                }
                return _BackButton;
            }
        }

        private string _BackSourceID;
        public string BackSourceID
        {
            get { return _BackSourceID; }
            set
            {
                _BackSourceID = value;
                BackButton.ID = value;

                BackButton.OnClientClick = "javascript: setBackSource('" + value + "')";
            }
        }

        private string _BackHref;
        public string BackHref
        {
            get { return _BackHref; }
            set
            {
                _BackHref = value;
            }
        }

        private string _BackLabel;
        public string BackLabel
        {
            get
            {
                if (_BackLabel == null)
                    _BackLabel = "Torna Indietro";
                return _BackLabel;
            }
            set
            {
                _BackLabel = value;
                BackButton.Text = value;
            }
        } 
        #endregion

        #region SubmitButton

        private bool _AddSubmitButton;
        public bool AddSubmitButton
        {
            get { return _AddSubmitButton; }
            set { _AddSubmitButton = value; }
        }

        private Button _SubmitButton;
        private Button SubmitButton
        {
            get
            {
                if (_SubmitButton == null)
                {
                    _SubmitButton = new Button();
                    _SubmitButton.Text = SubmitLabel;
                    _SubmitButton.ID = SubmitSourceID;
                    _SubmitButton.CssClass = "button";
                    _SubmitButton.OnClientClick = "javascript: setSubmitSource('" + SubmitSourceID + "')";
                }
                return _SubmitButton;
            }
        }

        private string _SubmitSourceID;
        public string SubmitSourceID
        {
            get { return _SubmitSourceID; }
            set
            {
                _SubmitSourceID = value;
                SubmitButton.ID = value;

                SubmitButton.OnClientClick = "javascript: setSubmitSource('" + value + "')";
            }
        }

        private string _SubmitLabel;
        public string SubmitLabel
        {
            get
            {
                if (_SubmitLabel == null)
                    _SubmitLabel = NLabels[NetFormLabels.NetFormLabelsList.SubmitLabel];
                return _SubmitLabel;
            }
            set
            {
                _SubmitLabel = value;
                SubmitButton.Text = value;
            }
        } 

        #endregion

        private bool _AddDisclaimer;
        public bool AddDisclaimer
        {
            get { return _AddDisclaimer; }
            set { _AddDisclaimer = value; }
        }

        private DisclaimerControl _DisclaimerControl;
        public DisclaimerControl DisclaimerControl
        {
            get { if (_DisclaimerControl == null)_DisclaimerControl = new DisclaimerControl(); return _DisclaimerControl; }
        }
	
        public bool CheckSubmitSource
        {
            get
            {
                NetUtility.RequestVariable sb = new NetUtility.RequestVariable(SubmitSourceID, NetUtility.RequestVariable.RequestType.Form);
                if (sb.IsValid() && sb.StringValue == SubmitLabel)
                    return true;
                return false;
            }
        }

        private string _CssClass = "";
        public string CssClass
        {
            get { return _CssClass; }
            set { _CssClass = value; }
        }

        public NetForm()
        {
            _Tables = new NetAbstractFormTableCollection();
            _AddSubmitButton = true;
            _SubmitSourceID = "send";
            _BackSourceID = "back";

            _LastOperation = LastOperationValues.NotYetExecute;

            _ControlToInsertAfter = new List<KeyValuePair<string, WebControl>>();
        }

        public void AddTable(NetAbstractFormTable table)
        {
            Tables.Add(table);
        }

        /// <summary>
        /// Collezione inserita per consentire la visualizzazione di WebControl esterni, diversi dalle tabelle presenti nella NetForm, e alternati alle stesse.
        /// Il primo parametro stringa � l'id della tabella dopo la quale inserire il Web Control. Se pi� WebControl vanno visualizzati dopo una determinata tabella, dare a ciascun WebControl in id composto da "NomeDellaTabella" seguito da "#" e quindi un differenziatore. Es: FoldersLang#en.
        /// Il secondo parametro � il WebControl da visualizzare con id formattato secondo quanto spiegato sopra. I WebControl inseriti dovranno avere campi non obbligatori e non saranno elaborati automaticamente in alcun modo dalla form durante inserimento o modifica.
        /// Inserito per favorire la visualizzazione delle collapsable del supporto multilingua.
        /// </summary>
        public List<KeyValuePair<string, WebControl>> ControlToInsertAfter
        {
            get { return _ControlToInsertAfter; }
            set { _ControlToInsertAfter = value; }
        }
        private List<KeyValuePair<string, WebControl>> _ControlToInsertAfter;

        public HtmlGenericControl getForms()
        {
            if (AddBackButton)
                CheckBackButtonPress();

            HtmlGenericControl div = new HtmlGenericControl("div");
            if (!string.IsNullOrEmpty(CssClass))
                div.Attributes["class"] = CssClass;

             HtmlGenericControl campiobbligatori = new HtmlGenericControl("p");
             campiobbligatori.Attributes["class"] = "netform_fieldrequired";
             campiobbligatori.InnerHtml = "<strong>" + this.TestoCampiObbligatori + "</strong>";
            div.Controls.Add(campiobbligatori);
            
            bool isPostBack = false;
            for (int i = 0; i < Tables.Count; i++)
            {
                NetAbstractFormTable table = Tables[i];
                div.Controls.Add(table.GetControl(this));
                isPostBack = isPostBack | table.IsPostBack;

                //Controllo per la visualizzazione dei WebControl esterni subito dopo la tabella prescelta.
                if (ControlToInsertAfter.Count(x=>x.Key.Split('#').First().Equals(table.TableName))> 0)
                {
                    IEnumerable<KeyValuePair<string, WebControl>> ctrls = ControlToInsertAfter.Where(x => x.Key.Split('#').First().Equals(table.TableName));
                    foreach(KeyValuePair<string, WebControl> item in ctrls)
                        div.Controls.Add(item.Value);
                }
            }
            HtmlGenericControls.Par p = new HtmlGenericControls.Par();
            p.Attributes["class"] = "buttonscontainer";

            if (this.AddDisclaimer && this.DisclaimerControl != null)
                div.Controls.Add(this.DisclaimerControl.GetControlNoFieldset);

            div.Controls.Add(p);
            if(AddSubmitButton)
                p.Controls.Add(SubmitButton);
            if (AddBackButton)
                p.Controls.Add(BackButton);

            if (!isPostBack)
                Diagnostics.TraceMessage(TraceLevel.Verbose, "Accesso alla form", "", "","");
           
            return div;
        }
        public HtmlGenericControl getForms(string legendText)
        {
            return getForms(legendText, "");
        }
        public HtmlGenericControl getForms(string legendText, string errorsHtml)
        {
            HtmlGenericControl fieldset = new HtmlGenericControl("fieldset");
            HtmlGenericControl legend = new HtmlGenericControl("legend");
            HtmlGenericControl errors = new HtmlGenericControl("div");
            legend.InnerText = legendText;
            errors.InnerHtml = errorsHtml;
            if (errorsHtml != null && errorsHtml.Length > 0)
                errors.Attributes["class"] = "Form_Message";
            fieldset.Controls.Add(legend);
            fieldset.Controls.Add(errors);
            fieldset.Controls.Add(getForms());
            return fieldset;
        }
        public string serializedValidation()
        {
            return serializedValidation(HttpContext.Current.Request.Form);
        }

       
        public string serializedValidation(NameValueCollection post)
        {
            string valid, errors = "";

            if (this.AddDisclaimer && !this.DisclaimerControl.CheckAcceptance())
                errors += "<li>" + this.NLabels[NetFormLabels.NetFormLabelsList.NecessarioAccettareCondizione] + "</li>";

            for (int i = 0; i < Tables.Count; i++)
            {
                NetAbstractFormTable table = Tables[i];
                valid = table.ValidateInput(this);

                if (valid != null)
                {
                    errors += valid;
                    if (table.TableStringsForLog.Count != 0)
                        this.FormStringsForLog.Add(table.TableName, table.TableStringsForLog);
                }
            }
            _LastValidationResult = errors;
            if (errors.Length > 0)
            {
                ICollection tabelle = FormStringsForLog.Values;
                foreach (ArrayList tabella in tabelle)
                {
                    string[] log_campi = (string[])tabella.ToArray(typeof(string));
                    foreach (string log_campo in log_campi)
                    {
                        Diagnostics.TraceMessage(TraceLevel.Warning, log_campo, "", "","");
                    }
                }
                return errors;
            }
            else
                return null;
        }
        public string serializedInsert()
        {
            return serializedInsert(HttpContext.Current.Request.Form);
        }

        /// <summary>
        /// Esegue la INSERT del Record specifico nella tabella %NET%_docs_%app_xxx% in base ai Field appesi nel Form passato in post
        /// </summary>
        /// <param name="post">Form in cui sono appesi i Field usati per costruire la INSERT in SQL </param>
        /// <returns>Esito della INSERT come Boolean </returns>
        public string serializedInsert(NameValueCollection post)
        {
            string valid = serializedValidation(post);

            if (valid != null)
            {
                _LastOperation = LastOperationValues.Error;
                return "<div class=\"form-error\"><ul>" + valid + "</ul></div>";
            }

            bool esito = true;
            string output = string.Empty;
            
            foreach(NetAbstractFormTable table in this.Tables)
            {
                if (esito)
                {
                    for (int j = 0; j < table.ExternalFields.Count; j++)
                    {
                        NetExternalField field = table.ExternalFields[j];

                        if (Tables[field.TableName] != null)
                            if (Tables[field.TableName].LastInsert != 0)
                                field.Value = Tables[field.TableName].LastInsert.ToString();
                    }                   
                    esito &= table.Insert();
                }
            }


            if (esito)
            {
                _LastOperation = LastOperationValues.OK;
                output = NLabels[NetFormLabels.NetFormLabelsList.InserimentoEffettuatoConSuccesso];
                Diagnostics.TraceMessage(TraceLevel.Verbose, "Inserimento nella form avvenuto con successo", "", "","");
            }
            else
            {
                _LastOperation = LastOperationValues.Error;
                output = NLabels[NetFormLabels.NetFormLabelsList.InserimentoFallito];
                //Diagnostics.TraceMessage(TraceLevel.Error, "Errore durante l'inserimento nella form", "", "");
            }
            return output;
        }


        public string serializedUpdate()
        {
            return serializedUpdate(HttpContext.Current.Request.Form);
        }


        public string serializedUpdate(NameValueCollection post)
        {
            string valid = serializedValidation(post);

            if (valid != null)
            {
                _LastOperation = LastOperationValues.Error;
                return "<div class=\"form-error\"><ul>" + valid + "</ul></div>";

            }

            bool esito = true;
            string output = NLabels[NetFormLabels.NetFormLabelsList.AggiornamentoEffettuatoConSuccesso];

            foreach(NetAbstractFormTable table in this.Tables)
                esito &= table.Update();
            
            if (esito)
            {
                _LastOperation = LastOperationValues.OK;
                output = NLabels[NetFormLabels.NetFormLabelsList.AggiornamentoEffettuatoConSuccesso];
                Diagnostics.TraceMessage(TraceLevel.Verbose, "Aggiornamento della form avvenuto con successo", "", "","");
            }
            else
            {
                _LastOperation = LastOperationValues.Error;
                output = NLabels[NetFormLabels.NetFormLabelsList.AggiornamentoFallito];
                //Diagnostics.TraceMessage(TraceLevel.Error, "Errore durante l'aggiornamento della form", "", "");
            }

            return output;
        }

        private void CheckBackButtonPress()
        {
            if (AddBackButton)
            {
                string request = HttpContext.Current.Request.Form[BackSourceID];
                NetUtility.RequestVariable backrq = new NetUtility.RequestVariable(BackSourceID, NetUtility.RequestVariable.RequestType.Form);
                if (backrq.IsValid() && backrq.StringValue == BackLabel)
                   NetCms.Diagnostics.Diagnostics.Redirect(BackHref);
            }
        }
    }
}