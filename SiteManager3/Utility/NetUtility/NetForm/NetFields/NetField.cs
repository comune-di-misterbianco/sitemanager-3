using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public abstract class NetField
    {
        private System.Collections.Specialized.NameValueCollection _CurrentRequest;
        public System.Collections.Specialized.NameValueCollection CurrentRequest
        {
            get 
            {
                if (_CurrentRequest == null)
                    _CurrentRequest = HttpContext.Current.Request.Form;
                return _CurrentRequest; 
            }
            set { _CurrentRequest = value; }
        }	

        public virtual bool ShowInInOverview { get { return true; } }

        protected NetFormLabels NLabels
        {
            get
            {
                return NetUtility.LabelsCacher.NetFormLabels;
            }
        }

        private bool _JumpField = false;
        public virtual bool JumpField
        {
            get { return _JumpField; }
            set { _JumpField  = value; }
        }

        private string _CssClass;
        public string CssClass
        {
            get 
            {
                if (_CssClass == null)
                    _CssClass = "";
                return _CssClass; 
            }
            set { _CssClass = value; }
        }	

        private bool _RenderField = true;
        public virtual bool RenderField
        {
            get { return _RenderField; }
            set { _RenderField = value; }
        }

        private int _Type;
        public virtual int Type
        {
            set
            {
                _Type = value;
            }
            get { return _Type; }
        }

        public virtual string Value
        {
            set
            {
                _Value = value;
            }
            get { return _Value; }
        }
        protected string _Value = "";

        public virtual bool ValueIsEmpty()
        {
            return this.Value == null || this.Value.Length == 0;
        }

        private string _DefaultControlTag;
        public string DefaultControlTag
        {
            get 
            {
                if (_DefaultControlTag == null)
                    _DefaultControlTag = "p";
                return _DefaultControlTag; 
            }
            set
            {
                _DefaultControlTag = value;
            }
        }

        private bool _Required;
        public virtual bool Required
        {
            get { return _Required; }
            set { _Required = value; }
        }

        private string _PostBackValue;
        public string PostBackValue
        {
            get
            {
                if (_PostBackValue == null)
                {
                    if (RequestVariable.IsValid())
                    {
                        _PostBackValue = RequestVariable.StringValue;
                    }
                }
                return _PostBackValue;
            }
        }

        protected NetUtility.RequestVariable _RequestVariable;
        public virtual NetUtility.RequestVariable RequestVariable
        {
            get
            {
                if (_RequestVariable == null)
                {
                     _RequestVariable = new NetUtility.RequestVariable(FieldName,this.CurrentRequest);                    
                }
                return _RequestVariable;
            }
        }

        protected string _Label;
        protected string _FieldName;

        public virtual string Label
        {
            get { return _Label; }
            set { _Label = value; }
        }
        public virtual string FieldName
        {
            get { return _FieldName; }
        }

        public NetField()
        {
            Type = 0;
        }
        public NetField(string label, string fieldname)
        {
            //
            // TODO: Add constructor logic here
            //
            _Label = label;
            _FieldName = fieldname;
            Type = 0;
        }

        public abstract HtmlGenericControl getControl();

        public virtual String getFieldName()
        {
            return FieldName;
        }

        public abstract string validateInput(string input);

        public string FieldStringForLog
        {
            get
            {
                if (_FieldStringForLog == null)
                    _FieldStringForLog = "";
                return _FieldStringForLog;
            }
            set { _FieldStringForLog = value; }
        }
        private string _FieldStringForLog;

        public abstract string getFilter(string value);

        public virtual string validateValue(string value)
        {
            string output = value;
            output = output.Trim();
            output = output.Replace("'", "''");
            output = "'" + output + "'";
            return output;
        }

        public virtual HtmlGenericControl GetOverviewControl()
        {
            HtmlGenericControl fieldControl = new HtmlGenericControl("li");
            fieldControl.InnerHtml = "<strong>" + this.Label + ": </strong>" + this.Value;
            return fieldControl;
        }
    }

}