using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetTreeView : NetField
    {    

        public TreeNode Tree
        {
            get
            {
                return _Tree;
            }
        }
        private TreeNode _Tree;

        public bool AutoPostBack
        {
            get
            {
                return _AutoPostBack;
            }
            set
            {
                _AutoPostBack = value;
            }
        }
        private bool _AutoPostBack = false;

        public bool CheckAllows
        {
            get
            {
                return _CheckAllows;
            }
            set
            {
                _CheckAllows = value;
            }
        }
        private bool _CheckAllows = false;

        public bool IsStatic
        {
            get
            {
                return _IsStatic;
            }
            set
            {
                _IsStatic = value;
            }
        }
        private bool _IsStatic = false;

        private string _NotAllowedMsg;

        public string NotAllowedMsg
        {
            get {
                if (_NotAllowedMsg == null)
                    _NotAllowedMsg = "Attenzione non si possiedono i permessi per la cartella selezionata";
                return _NotAllowedMsg; 
            }
            set { _NotAllowedMsg = value; }
        }
	

        public string RootDist
        {
            get
            {
                return _RootDist;
            }
            set
            {
                _RootDist = value;
            }
        }
        private string _RootDist;

        private int idSeed = 0;

        public NetTreeView(string label, string fieldname)
            : base(label, fieldname)
        {
        }

        public NetTreeView(string label, string fieldname, TreeNode tree)
            : base(label, fieldname)
        {
            _Tree = tree;
        }

        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl par = new HtmlGenericControl(DefaultControlTag);
            if (this.CssClass != string.Empty)
                par.Attributes["class"] = this.CssClass;
            par.Attributes["style"] = "text-align: left;";
            HtmlInputHidden hidden;

            hidden = new HtmlInputHidden();
            hidden.ID = _FieldName;
            hidden.Value = Value;
            par.Controls.Add(hidden);
            par.Controls.Add(getTree());

            return par;
        }

        public override String getFieldName()
        {
            return _FieldName;
        }

        public override string validateInput(string input)
        {
            string errors = "";
            int val;
            int.TryParse(input, out val);
            if (Required && (input.Length == 0 ||val<1))
            {
                errors += "<li>";
                errors += "Il campo '" + Label + "' � obbligatorio";
                errors += "</li>";
            }

            return errors;
        }

        public override string getFilter(string value)
        {
            string filter = " ";
            if (value.Length > 0)
            {
                filter += _FieldName;

                filter += " = ";
                filter += value;
                return filter;
            }
            return filter;
        }

        private Control getTree()
        {
            idSeed = 0;
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "width:200px;text-align: left;";
            HtmlGenericControl script = new HtmlGenericControl("script");

            div.Controls.Add(script);
            script.Attributes["type"] = "text/javascript";
            string html = "\n";
            html += "<!--\n";

            html += "\nfunction setSelectedNode(value){\n";
            html += "   document.getElementById(\"" + _FieldName + "\").value = value; \n";
            if (AutoPostBack)
            {
                html += "   setSubmitSource(\"" + _FieldName + "\");\n";
                html += "   if (document.getElementById(\"page\")!=null){\n";
                html += "       setpage(0);}\n";
                html += "   document.getElementById(\"PageForm\").submit();\n";
            }
            html += "}\n";
            if (IsStatic)
                html += "	d_" + _FieldName + " = new dTree('d_" + _FieldName + "',true);\n";
            else
                html += "	d_" + _FieldName + " = new dTree('d_" + _FieldName + "');\n";
            html += getNodes(Tree, idSeed++, -1);

            html += "		document.write(d_" + _FieldName + ");\n";
            if (IsStatic)
                html += "		javascript: d_" + _FieldName + ".openAll();\n";
            html += "//-->\n";
            script.InnerHtml = html;

            return div;
        }

        private string getNodes(TreeNode node, int id, int padre)
        {
            string image = RootDist + "/App_images/dtree/folder.gif";
            string image_open = RootDist + "/App_images/dtree/folderopen.gif";
            string js_command = "javascript: setSelectedNode(\\\'" + node.Value + "\\\')";
            /*if (node.ImageUrl != "")
                image = node.ImageUrl;
            if (node.NavigateUrl != "")
                image_open = node.NavigateUrl;*/

            if (CheckAllows && !node.Checked)
            {
                image = RootDist + "/App_images/dtree/redfolder.gif";
                image_open = RootDist + "/App_images/dtree/redfolderopen.gif";
                js_command = "javascript: alert(\\\'"+NotAllowedMsg+"\\\')";
            }

            string html = "d_" + _FieldName + ".add(" + id + "," + padre + ",'" + node.Text.Replace("'","\\\'") + "','" + js_command + "','','','" + image + "','" + image_open + "'); \n";
            foreach (TreeNode childnode in node.ChildNodes)
            {
                html += getNodes(childnode, idSeed++, id);
            }
            return html;
        }

        public override string validateValue(string value)
        {
            string output = value;
            output = output.Trim();

            bool parseResult;
            int intvalue;
            parseResult = int.TryParse(output, out intvalue);
            if (parseResult)
            {
                output = output.Replace("'", "''");
                output = "" + output + "";
            }
            else
            {
                output = output.Replace("'", "''");
                output = "'" + output + "'";
            }

            return output;
        }
    }

}