using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.IO;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetSingleFileChoser : NetField
    {
        private bool _AddScripts;
        public bool AddScripts
        {
            get { return _AddScripts; }
            set { _AddScripts = value; }
        }
        
        protected string _FilePathOffset;
        public string FilePathOffset
        {
            get
            {
                if (_FilePathOffset == null)
                {
                    _FilePathOffset = "";
                }
                return _FilePathOffset;
            }
            set {_FilePathOffset= value;}
        }

        protected string _PreviewPath = "";
        public string PreviewPath
        {
            get { return _PreviewPath; }
            set { _PreviewPath = value; }
        }

        protected string _SelectedFilesQuery;
        public string SelectedFilesQuery
        {
            get { return _SelectedFilesQuery; }
            set { _SelectedFilesQuery = value; }
        }

        protected DropDownList _FileList;
        protected DropDownList FileList
        {
            get
            {
                if (_FileList == null)
                {
                    _FileList = new DropDownList();
                    _FileList.ID = FieldName;
                    _FileList.AutoPostBack = false;
                    _FileList.Attributes["onclick"] = "FileChoser_Preview('" + FileList.ID + "')";
                }
                return _FileList;
            }
        }

        private bool _Image;
        public bool Image
        {
            get
            {                
                return _Image;
            }
            set
            {
                _Image = value;
            }
        }

        public override string Value
        {
            set
            {
                _Value = value;
                foreach (ListItem item in FileList.Items)
                {
                    if (item.Value == value)
                    {
                        item.Selected = true;                        
                        FileList.SelectedIndex = FileList.Items.IndexOf(item);
                    }
                }
            }
            get { return _Value; }
        }

        protected NetCms.Connections.Connection Conn;

        public NetSingleFileChoser(string label, string fieldname, string folderPath, NetCms.Connections.Connection conn)
            : base(label, fieldname)
        {
            Conn = conn;
            Required = false;
            FolderPath = folderPath;
            _AddScripts = true;
        }

        protected string FolderPath;

        public virtual void DataBind()
        {
            if (Image)
                ImagesDataBind();
            else
            if (FolderPath != null)
            {
                DirectoryInfo Folder = new DirectoryInfo(FolderPath);
                ListItem item = new ListItem("");
                FileList.Items.Add(item);
                try
                {
                    FileInfo[] files = Folder.GetFiles();
                    for (int i = 0; i < files.Length; i++)
                    {
                        item = new ListItem(files[i].Name);
                        FileList.Items.Add(item);
                    }
                }
                catch (Exception ex)
                {
                    /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                    ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Directory non trovata: percorso non valido", "", "", "91");
                }
            }
        }
        protected virtual void ImagesDataBind()
        {
            if (FolderPath != null)
            {
                DirectoryInfo Folder = new DirectoryInfo(FolderPath);
                ListItem item = new ListItem("");
                FileList.Items.Add(item);
                try
                {
                    FileInfo[] files = Folder.GetFiles();
                    for (int i = 0; i < files.Length; i++)
                    {
                        if (files[i].Extension.Contains("jpg") ||
                            files[i].Extension.Contains(".png") ||
                            files[i].Extension.Contains(".gif"))
                        {
                            item = new ListItem(files[i].Name);
                            FileList.Items.Add(item);
                        }
                    }
                }
                catch (Exception ex)
                {
                    /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                    ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Directory non trovata: percorso non valido", "", "", "92");
                }
            }            
        }
        public virtual void addSourceItem(string label, string value)
        {
            FileList.Items.Add(new ListItem(label, value));
        }

        public override string validateInput(string input)
        {
            string errors = "";

            if (Required && input.Length == 0)
            {
                errors += "<li>";
                errors += "Il campo '" + _Label + "' � obbligatorio";
                errors += "</li>";
            }

            return errors;
        }
        public override string validateValue(string value)
        {
            string val = value;
            int i;
            if (!int.TryParse(val, out i))
                val = "'" + val + "'";

            return val;
        }
        public override string getFilter(string value)
        {
            string filter = " ";
            if (value.Length > 0)
            {
                filter += _FieldName;

                filter += " = ";
                filter += value;
                filter += " ";
            }
            return filter;
        }

        private HtmlGenericControl FileListControl()
        {
            HtmlGenericControl par = new HtmlGenericControl("p");

            Label lb = new Label();
            lb.AssociatedControlID = _FieldName;
            lb.Text = Label;
            if (Required)
                lb.Text += "*";

            lb.Style.Add("display", "block");
            par.Controls.Add(lb);

            par.Controls.Add(FileList);
            
            HtmlGenericControl span = new HtmlGenericControl("span");
            span.InnerHtml += "&nbsp;<input id=\"" + FieldName + "_Transfer" + "\" name=\"" + FieldName + "_ADD" + "\" type=\"button\" onclick=\"javascript: TransferDialog('" + FolderPath + "')\" value=\"Trasferisci\" />";
            par.Controls.Add(span);

            return par;
        }
        
        private HtmlGenericControl PreviewControl()
        {
            HtmlGenericControl iframe = new HtmlGenericControl("iframe");
            iframe.ID = FieldName + "_PreviewBox";
            iframe.Attributes["class"] = "FileChoserPreviewBox";
            return iframe;
        }

        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl p = new HtmlGenericControl("p");

            p.Controls.Add(FileListControl());
            p.Controls.Add(PreviewControl());

            if (AddScripts)
            {

                p.Controls.Add(Scripts);
            }
            return p;
        }

        public virtual HtmlGenericControl Scripts
        {
            get
            {
                HtmlGenericControl script = new HtmlGenericControl("script");
                script.Attributes["type"] = "text/javascript";
                script.InnerHtml = @"FileChoser_Preview('" + FieldName + "')";

                script.InnerHtml += @"

    function FileChoser_Preview(Source)
    {
        oPreview = document.getElementById('" + FieldName + @"_PreviewBox'); 
        var previewLoaded = false;
        if(Source)
        {
            oSource = document.getElementById(Source); 
               
            if(oSource != null && oSource.options.length > 0)
            {
                var index = oSource.options.selectedIndex
                if(index!=null && index<0)
                    index = 0;
                oOption = oSource.options[index];
                
                if(oOption != null)            
                {
                    var extsplit = oOption.text.split('.');
                    var ext = extsplit[extsplit.length-1];
                    if(ext!=null && ( ext == ""jpg"" || ext == ""gif"" || ext == ""png"" ))
                    {
                        oPreview.src = """ + PreviewPath + @"/""+oOption.text;
                        oPreview.style.display = ""block"";
                        previewLoaded = true;
                    }
                }
            } 
        }
        if(!previewLoaded)
            FileChoser_HidePreview();
    }

    function FileChoser_AddOption(target,text,value)
    {
        oTarget = document.getElementById(target);
        oOption = oSource.options[oTarget.options.length]; 
        oTarget.options[oTarget.options.length] = new Option(text,value);
    }

    function FileChoser_HidePreview()
    {
        oPreview.src = """";
        oPreview.style.display = ""none"";
    }
    
    function TransferDialog(folderpath)
    {   
	    window.open(""" + this.FilePathOffset + @"transfer.aspx?folder=""+folderpath+""&ctrid=" + FieldName + @""","""",""height=318,width=400,status=yes,toolbar=no,menubar=no,location=no"");//""1,0,0,250,200,0,0,0,0,1,0,100,150"");	    
    }
";
                return script;
            }

        }
    }

}