using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.IO;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class FileTransfer
    {
        private string FolderPath;

        private string _TargetControlID;
        public string TargetControlID
        {
            get
            {
                if (_TargetControlID == null)
                {
                    NetUtility.RequestVariable qs = new NetUtility.RequestVariable("ctrid", HttpContext.Current.Request.QueryString);
                    if (qs.IsValid())
                        _TargetControlID = qs.StringValue;
                }
                return _TargetControlID;
            }
        }

        public FileTransfer(string folderPath)
        {
            FolderPath = folderPath;
        }

        private void Trasferisci_Click(object sender, EventArgs e)
        {
            if (uploadform.HasFile)
            {
                //Uncomment this line to Save the uploaded file
                string SavePath = FolderPath;
                if (!SavePath.EndsWith("/"))
                    SavePath += "/";
                string FileName = uploadform.FileName;
                SavePath = SavePath + FileName;

                uploadform.SaveAs(SavePath);

                if (TargetControlID != null && 
                    TargetControlID.Length > 0 && 
                    HttpContext.Current.Request.Form["Lite"] != null &&
                    HttpContext.Current.Request.Form["Lite"] == "true")
                   NetCms.Diagnostics.Diagnostics.Redirect("./end.aspx?docname=" + FileName + "&ctrid=" + TargetControlID);

            }          
        }
        private FileUpload uploadform = new FileUpload();
        private HtmlGenericControl errors = new HtmlGenericControl("div");

        public HtmlGenericControl GetControl(bool IsPostBack)
        {
            HtmlGenericControl div = new HtmlGenericControl("div");

            if (HttpContext.Current.Request.Form["Lite"] != null && HttpContext.Current.Request.Form["Lite"] == "true")
            {
                HtmlGenericControl error = new HtmlGenericControl();
                div.Controls.Add(error);
                error.InnerHtml = "<strong>Errore nel trasferimento </strong><br />File non valido";
            }

            HtmlGenericControl fieldset = new HtmlGenericControl("div");
            div.Controls.Add(fieldset);
            HtmlGenericControl legend = new HtmlGenericControl("p");
            fieldset.Controls.Add(legend);
            legend.InnerHtml = "<strong>Trasferimento File</strong>";

            fieldset.Controls.Add(errors);

            HtmlGenericControl p = new HtmlGenericControl("p");
            fieldset.Controls.Add(p);

            Label label = new Label();
            p.Controls.Add(label);
            label.Style.Add("display", "block");
            label.Text = "File*";
            label.AssociatedControlID = "FileUpload";

            uploadform.ID = "FileUpload";
            p.Controls.Add(uploadform);

            p = new HtmlGenericControl("p");
            fieldset.Controls.Add(p);

            Button Trasferisci = new Button();
            Trasferisci.Text = "Trasferisci";
            Trasferisci.ID = "Trasferisci";
            Trasferisci.Click += Trasferisci_Click;
            p.Controls.Add(Trasferisci);

            HiddenField hidden = new HiddenField();
            hidden.ID = "Lite";
            hidden.Value = "true";
            div.Controls.Add(hidden);

            return div;
        }
    }

}