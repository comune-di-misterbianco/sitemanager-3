using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.IO;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetFileListBox : NetField
    {
        public bool Required = false;

        private string Path;
        public int Size
        {
            get { return _ListBox.Rows; }
            set { _ListBox.Rows = value; }
        }

        private ListBox _ListBox;

        public NetFileListBox(string label, string fieldname, string path)
            : base(label, fieldname)
        {
            _ListBox = new ListBox();
            Path = path;
        }


        public void addItem(string Label)
        {
            _ListBox.Items.Add(Label);
        }

        public void addItem(string Label, string Value)
        {
            ListItem item = new ListItem(Label, Value);
            _ListBox.Items.Add(item);
        }


        public override String getFieldName()
        {
            return _FieldName;
        }

        public override string validateInput(string input)
        {
            string errors = "";

            if (Required && input.Length == 0)
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio], this.Label);
                errors += "</li>";
            }

            return errors;
        }

        public override string getFilter(string value)
        {
            string filter = " ";
            if (value.Length > 0)
            {
                filter += _FieldName;

                filter += " = ";
                filter += value;
                filter += " ";
            }
            return filter;
        }

        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl par = getControlSimple();
            if (this.CssClass != string.Empty)
                par.Attributes["class"] = this.CssClass;
            bindControl();

            if (Value.Length > 0)
            {
                foreach (ListItem item in _ListBox.Items)
                {
                    if (item.Value == Value)
                    {
                        item.Selected = true;
                        _ListBox.SelectedIndex = _ListBox.Items.IndexOf(item);
                    }
                }
            }

            return par;
        }

        private HtmlGenericControl getControlSimple()
        {
            HtmlGenericControl par = new HtmlGenericControl(DefaultControlTag);

            Label lb = new Label();
            lb.AssociatedControlID = _FieldName;
            if (Required)
                lb.Text = _Label + "*";
            else
                lb.Text = _Label;
            lb.Attributes["class"] = "formlabel";
            par.Controls.Add(lb);

            _ListBox.ID = _FieldName;
            par.Controls.Add(_ListBox);

            return par;
        }

        private void bindControl()
        {
            DirectoryInfo dir = new DirectoryInfo(Path.Replace("\\", "/"));
            if (dir.Exists)
            {
                FileInfo[] files = dir.GetFiles();
                ListItem item;
                for (int i = 0; i < files.Length; i++)
                {
                    item = new ListItem(files[i].Name);

                    _ListBox.Items.Add(item);
                }
            }
        }
    }

}