using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Connections;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetDropDownList : NetField
    {
        private DataTable BindingSource;
        private string DataMember;
        private string DataSource;
        private string SqlQuery;
        private string DataValue;

        private bool _AutoPostBack;
        public bool AutoPostBack
        {
            get
            {
                return _AutoPostBack;
            }
            set
            {
                _AutoPostBack = value;
            }
        }

        private string _OnClientClick;
        public string OnClientClick
        {
            get {
                if (_OnClientClick == null)
                {
                    _OnClientClick = "setSubmitSource('" + FieldName + "')";
                }
                return _OnClientClick; 
            }
            set { _OnClientClick = value; }
        }
	
        private Connection Conn;

        private int bindings;

        private DropDownList DropDownList;

        public NetDropDownList(string label, string fieldname)
            : base(label, fieldname)
        {
            DropDownList = new DropDownList();
            bindings = 0;

            ListItem item = new ListItem("", "");
            DropDownList.Items.Add(item);
        }

        private HtmlGenericControl getControlSimple()
        {
            HtmlGenericControl par = new HtmlGenericControl(DefaultControlTag);
            par.Attributes["class"] = DefaultControlTag + "_" + _FieldName;

            Label lb = new Label();
            lb.AssociatedControlID = _FieldName;
            if (Required)
                lb.Text = _Label + "*";
            else
                lb.Text = _Label;

            lb.Attributes["class"] = "formlabel";

            par.Controls.Add(lb);
            DropDownList.AutoPostBack = AutoPostBack;
            if (AutoPostBack)
                DropDownList.Attributes["onchange"] =OnClientClick;
            DropDownList.ID = _FieldName;
            par.Controls.Add(DropDownList);

            return par;
        }
        public  void ClearList()
        {
            DropDownList.Items.Clear();
        }
        private void bindList()
        {
            string sql = "";
            DataTable dt;
            switch (bindings)
            {
                case 1:
                    sql = "SELECT * FROM " + DataSource;
                    dt = Conn.SqlQuery(sql);
                    break;
                case 2:
                    sql = SqlQuery;
                    dt = Conn.SqlQuery(sql);
                    break;
                case 3:
                    dt = BindingSource;
                    break;
                default:
                    sql = "SELECT * FROM " + DataSource;
                    dt = Conn.SqlQuery(sql);
                    break;

            }
            ListItem item;
            foreach (DataRow row in dt.Rows)
            {
                if (bindings == 2)
                    item = new ListItem(row[0].ToString(), row[1].ToString());
                else
                    item = new ListItem(row[DataMember].ToString(), row[DataValue].ToString());
                DropDownList.Items.Add(item);
            }

        }

        public void setDataBind(string dataMember, string dataValue, string dataSource, Connection conn)
        {
            DataMember = dataMember;
            DataValue = dataValue;
            DataSource = dataSource;
            Conn = conn;
            bindings = 1;
        }

        public void setDataBind(string sqlQuery, Connection conn)
        {
            SqlQuery = sqlQuery;
            Conn = conn;
            bindings = 2;
        }
        public void setDataBind(string dataMember, string dataValue, DataTable table)
        {
            DataMember = dataMember;
            DataValue = dataValue;
            BindingSource = table;
            bindings = 3;
        }

        public void addItem(string Label)
        {
            DropDownList.Items.Add(Label);
        }
        public void addItem(ListItem item)
        {
            DropDownList.Items.Add(item);
        }
        public void addItem(string Label, string Value)
        {
            ListItem item = new ListItem(Label, Value);
            DropDownList.Items.Add(item);
        }
        public void addItem(string Label, string Value,bool selected)
        {
            ListItem item = new ListItem(Label, Value);
            item.Selected = selected;
            DropDownList.Items.Add(item);
        }
        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl par = getControlSimple();

            if(this.CssClass!=string.Empty)
                par.Attributes["class"] = this.CssClass;

            if (bindings != 0)
                bindList();

            if (Value.Length > 0)
            {
                foreach (ListItem item in DropDownList.Items)
                {
                    if (item.Value == Value)
                    {
                        item.Selected = true;
                        DropDownList.SelectedIndex = DropDownList.Items.IndexOf(item);
                    }
                }
            }
            return par;
        }

        public override String getFieldName()
        {
            return _FieldName;
        }

        public override string validateInput(string input)
        {
            string errors = "";

            if (Required && input.Length == 0)
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio], this.Label);
                errors += "</li>";
            }

            return errors;
        }

        public override string getFilter(string value)
        {
            string filter = " ";
            if (value.Length > 0)
            {
                int val = 0;
                if (int.TryParse(value, out val))
                {
                    filter += _FieldName;

                    filter += " = ";
                    filter += value;
                    filter += " ";
                }
                else
                {
                    filter += _FieldName;

                    filter += " LIKE '";
                    filter += value;
                    filter += "' ";
                }
            }
            return filter;
        }

        public override HtmlGenericControl GetOverviewControl()
        {
            HtmlGenericControl fieldControl = new HtmlGenericControl("li");
            fieldControl.InnerHtml = "<strong>" + this.Label + ": </strong>" + DropDownList.Items.FindByValue(this.Value).Text;
            return fieldControl;
        }
    }

}