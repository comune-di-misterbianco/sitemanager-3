using System.Collections;
using System;

/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>
namespace NetForms
{
    public class NetFieldsCollection : IEnumerable
    {
        private NetCollection coll;
        public int Count { get { return coll.Count; } }
        public NetFieldsCollection()
        {
            //
            // TODO: Add constructor logic here
            //
            coll = new NetCollection();
        }

        public void Add(NetField field)
        {
            coll.Add(field.getFieldName(), field);
        }

        public NetField this[int i]
        {
            get
            {
                NetField field = (NetField)coll[coll.Keys[i]];
                return field;
            }
        }

        public NetField this[string key]
        {
            get
            {
                NetField field = (NetField)coll[key];
                return field;
            }
        }


        public int count()
        {
            return coll.Count;
        }
        #region Enumerator

        public IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private NetFieldsCollection Collection;
            public CollectionEnumerator(NetFieldsCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection[CurentPos];
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }
        #endregion
    }
}