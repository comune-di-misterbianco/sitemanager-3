using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetTextBoxPlus : NetField
    {
        private HtmlGenericControl _ControlsToAppend;
        private HtmlGenericControl ControlsToAppend
        {
            get
            {
                if (_ControlsToAppend == null)
                {
                    _ControlsToAppend = new HtmlGenericControl("div");
                }
                return _ControlsToAppend;
            }
        }

        private bool _CheckForDataExistance;
        public bool CheckForDataExistance
        {
            get { return _CheckForDataExistance; }
            set { _CheckForDataExistance = value; }
        }

        private NetFieldValueDBChecker _ValueDBChecker;
        public NetFieldValueDBChecker ValueDBChecker
        {
            get { return _ValueDBChecker; }
            set { _ValueDBChecker = value; }
        }	

        private bool _LoadValue = true;
        public bool LoadValue
        {
            get { return _LoadValue; }
            set { _LoadValue = value; }
        }

        private string _OnChange;
        public string OnChange
        {
            get { return _OnChange; }
            set { _OnChange = value;}
        }

        private bool _AddComfirmField;
        public bool AddComfirmField
        {
            get { return _AddComfirmField; }
            set { _AddComfirmField = value; }
        }	

        private string _OnKeyup;
        public string OnKeyup
        {
            get { return _OnKeyup; }
            set { _OnKeyup = value; }
        }

        private bool _ReadOnly = false;
        public bool ReadOnly
        {
            get { return _ReadOnly; }
            set { _ReadOnly = value; }
        }

        private int _Rows = 1;
        public int Rows
        {
            get { return _Rows; }
            set { _Rows = value; }
        }
        protected bool Multiline
        {
            get
            {
                return _Rows > 1;
            }
        }

        public ContentTypes ContentType
        {
            get
            {
                return _ContentType;
            }
            set
            {
                _ContentType = value;
            }
        }
        private ContentTypes _ContentType;

        public enum ContentTypes
        {
            NormalText = 0,
            eMail = 1,
            Phone = 2,
            Numeric = 3,
            File = 4,
            Folder = 5,
            Floating = 6,
            Valuta = 7
        }

        public override string Value
        {
            get
            {
                return base.Value;
            }
            set
            {
                if(LoadValue)
                    base.Value = value;
            }
        }

        public int MaxLenght = 255;
        public int Size = 60;

        public NetTextBoxPlus(string label, string fieldname)
            : this(label, fieldname,ContentTypes.NormalText)
        {
            _ValueDBChecker = new NetFieldValueDBChecker(this);
        }

        public NetTextBoxPlus(string label, string fieldname,ContentTypes type)
            : base(label, fieldname)
        {
            ContentType = type;
            _ValueDBChecker = new NetFieldValueDBChecker(this);
            Required = false;
        }

        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl par = new HtmlGenericControl(DefaultControlTag);
            if (this.CssClass != string.Empty)
                par.Attributes["class"] = this.CssClass;

            par.Controls.Add(this.BuildBox(this.FieldName,this.Label));
            if(this.AddComfirmField)
                par.Controls.Add(this.BuildBox(this.FieldName+"_Confirm",string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.Confirm], this.Label)));

            foreach (Control ctr in this.ControlsToAppend.Controls)
                par.Controls.Add(ctr);

            return par;
        }

        private HtmlGenericControl BuildBox(string id,string label)
        {
            HtmlGenericControl container = new HtmlGenericControl("div");
            container.Attributes["class"] = "field_container";

            Label lb = new Label();
            lb.AssociatedControlID = id;
            lb.Text = label + (Required ? "*" : "");
            //lb.Text += (MaxLenght == 0 ? "" : " (Lunghezza Massima: " + this.MaxLenght + " Caratteri)");
            lb.Attributes["class"] = "formlabel";
            container.Controls.Add(lb);

            TextBox textBox = new TextBox();
            textBox.ID = id;
            textBox.Attributes["title"] = "Lunghezza Massima: " + this.MaxLenght + " Caratteri";
            if (Size > 0)
            textBox.Columns = Size;

            if (ReadOnly)
                textBox.ReadOnly = true;


            if (OnChange != null)
                textBox.Attributes["onchange"] = OnChange;
            if (OnKeyup != null)
                textBox.Attributes["onkeyup"] = OnKeyup;
            if (Multiline)
            {
                textBox.TextMode = TextBoxMode.MultiLine;
                textBox.Rows = Rows;
            }
            textBox.Text = Value;

            container.Controls.Add(textBox);

            return container;
        }

        public override String getFieldName()
        {
            return _FieldName;
        }

        public override string validateInput(string input)
        {
            string errors = "";

            input = input.Trim();
            if (input.Length > 0)
            switch (ContentType)
            {
                case ContentTypes.eMail:
                {
                    #region eMail
		
                    Regex regex = new Regex(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$",
                               RegexOptions.IgnorePatternWhitespace);                    

                    if (!regex.IsMatch(input))
                    {
                        errors += "<li>";
                        errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.CampoEmailNonValido], this.Label);
                        errors += "</li>";
                    }
                    break;
	                #endregion
                }
                case ContentTypes.File:
                {
                    #region File
                    
                    //bool ok = true;
                    //string[] chars = { "'", "#", "@", "%", "=", "&", "/", "\\", "*", "?", ":", "|", "<", ">", "\"" };
                    //for (int i = 0; i < chars.Length && ok; i++)
                    //    if (input.Contains(chars[i])) ok = false;
                    bool ok = NetUtility.NameControls.CheckName(input);

                    if (!ok)
                    {
                        errors += "<li>";
                        errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.CampoFileNonValido], this.Label);
                        errors += "</li>";
                    }
                    break;
                    #endregion
                }
                case ContentTypes.Folder:
                {
                    #region Folder

                    bool ok = true;
                    // ".",
                    string[] chars = { "#", "@", "%", "=", "&", "/", "\\", "*", "?", ":", "|", "<", ">", "\"" };
                    for (int i = 0; i < chars.Length && ok; i++)
                        if (input.Contains(chars[i])) ok = false;
                    //bool ok = NetUtility.NameControls.CheckName(input);
                    if (ok) 
                    {
                        int c = 0;
                        for (int i = 0; i < 1 && ok; i++)
                            if (input.Contains("'")) c++;
                        if (c == input.Length) ok = false;
                    }


                    if (!ok)
                    {
                        errors += "<li>";
                        errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoNonEValido], this.Label);
                        errors += "</li>";
                    }
                    break;
                    #endregion
                }
                case ContentTypes.Phone:
                {
                    #region Phone
                    Regex regex = new Regex(@"^([0-9]{8,12})$",
                                               RegexOptions.IgnorePatternWhitespace);

                    if (!regex.IsMatch(input))
                    {
                        errors += "<li>";
                        errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.CampoTelefonoNonValido], this.Label);
                        errors += "</li>";
                    }
                    break;
                    #endregion
                }
                case ContentTypes.Numeric:
                {
                    #region Numeric

                    Regex regex = new Regex(@"^([0-9]*)$",
                    RegexOptions.IgnorePatternWhitespace);

                    if (!regex.IsMatch(input))
                    {
                        errors += "<li>";
                        errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.CampoNumericoNonValido], this.Label);
                        errors += "</li>";
                    }
                    break;
                    #endregion
                }
                case ContentTypes.Floating:
                {
                    #region Floating

                    Regex regex = new Regex(@"^\s*\d+((\.|,)\d+)?\s*$",
                    RegexOptions.IgnorePatternWhitespace);

                    if (!regex.IsMatch(input))
                    {
                        errors += "<li>";
                        errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.CampoNumericoNonValido], this.Label);
                        errors += "</li>";
                    }

                    break;
                    #endregion
                }
                case ContentTypes.Valuta:
                {
                    #region Floating

                    Regex regex = new Regex(@"^\s*\d+((\.|,)\d+)?\s*$",
                    RegexOptions.IgnorePatternWhitespace);

                    if (!regex.IsMatch(input))
                    {
                        errors += "<li>";
                        errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.CampoValutaNonValido], this.Label);
                        errors += "</li>";
                    }

                    break;
                    #endregion
                }
            }

            Regex regex1 = new Regex(@"^(?=.*[a-zA-Z0-9]+.*)", RegexOptions.IgnorePatternWhitespace);
            if (input.Length > 0 && !regex1.IsMatch(input))
            {
                string error = "<li>" + string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoNonEValido], this.Label) + "</li>";
                if (!errors.Contains(error)) errors += error;
            }

            if (Required && input.Length == 0)
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio], this.Label);
                errors += "</li>";
            }
            if (MaxLenght != 0 && input.Length > MaxLenght)
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.LunghezzaMassimaCampo], this.Label,MaxLenght.ToString());
                errors += "</li>";
            }
            if (this.CheckForDataExistance && ValueDBChecker.DataExist(input))
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.DatiRipetuti], this.Label);
                errors += "</li>";
            }
            if (this.AddComfirmField)
            {
                NetUtility.RequestVariable confirm = new NetUtility.RequestVariable(this.FieldName + "_Confirm",this.CurrentRequest);
                if (confirm.StringValue != this.RequestVariable.StringValue)
                {
                    errors += "<li>";
                    errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.CampiNonCoincidenti], this.Label, string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.Confirm], this.Label));
                    errors += "</li>";
                }
            }

            return errors;

        }

        public override string getFilter(string value)
        {
            string filter = " ";
            value = value.Replace("'", "''").Trim();
            if (value.Length > 0)
            {
                filter += _FieldName;
                
                if (ContentTypes.Numeric == ContentType || ContentTypes.Floating == ContentType)               
                {
                    if (ContentTypes.Floating == ContentType)
                    {
                        value = value.Replace(",", ".");
                    }
                    filter += " = ";
                    filter += value;
                    return filter;
                }
                else
                    if (ContentTypes.Phone == ContentType || ContentTypes.eMail == ContentType || ContentTypes.Valuta == ContentType)
                {
                    filter += " = '";
                    filter += value;
                    filter += "'";
                    return filter;
                }

                filter += " Like '%";
                filter += value;
                filter += "%'";
            }
            return filter;
        }

        public void AppendControl(HtmlGenericControl control)
        {
            this.ControlsToAppend.Controls.Add(control);
        }
        public override string validateValue(string value)
        {
            value = value.Trim();
            
            if (ContentTypes.Valuta == this.ContentType)
                return "'" + value.Replace(",", ".") + "'";
            
            if (ContentTypes.Floating == this.ContentType)
                return value.Replace(",", ".");

            if (ContentTypes.File == this.ContentType)
                return "'" + ClearFileSystemName(value) + "'";              
            
            return value = "'" + value.Replace("'", "''") + "'";
        }

        public static string ClearFileSystemName(string filename)
        {
            char[] chars = filename.ToLower().ToCharArray();
            string FileSystemName = "";

            for (int i = 0; i < chars.Length && i < 256; i++)
            {
                char carattere = chars[i] ;
                switch (carattere)
                {
                    case '�': carattere = 'a'; break;
                    case '�': carattere = 'e'; break;
                    case '�': carattere = 'e'; break;
                    case '�': carattere = 'i'; break;
                    case '�': carattere = 'o'; break;
                    case '�': carattere = 'u'; break;
                }
                FileSystemName += ((carattere >= 97 && carattere <= 122) || (carattere >= 48 && carattere <= 57)) ? carattere : '-';
            }
            while (FileSystemName.Contains("--")) FileSystemName = FileSystemName.Replace("--", "-");

            //Rimuovo i trattini alla fine del nome
            while (FileSystemName.EndsWith("-"))
                FileSystemName = FileSystemName.Remove(FileSystemName.Length - 1);

            //Rimuovo i trattini all inizio del nome
            while (FileSystemName.StartsWith("-"))
                FileSystemName = FileSystemName.Substring(1, FileSystemName.Length - 1);

            return FileSystemName;
        }
    }

    public class NetFieldValueDBChecker
    {
        private string _Sql;
        private string Sql 
        {
            get { return _Sql; }
        }

        private NetCms.Connections.Connection  _Conn;
        private NetCms.Connections.Connection Conn
        {
            get { return _Conn; }
        }

        private NetField _Field;
        private NetField Field
        {
            get { return _Field; }
        }

        public NetFieldValueDBChecker(NetField owner)
        {
            _Field = owner;
        }

        public void SetData(string sql, NetCms.Connections.Connection conn)
        {
            _Sql = sql;
            _Conn = conn;
        }

        public bool DataExist(string data)
        {
            if (Conn != null && Sql != null)
            {
                DataTable tab = Conn.SqlQuery(string.Format(Sql, Field.FieldName, data));
                return tab.Rows.Count > 0;
            }
            return false;
        }
    }
}