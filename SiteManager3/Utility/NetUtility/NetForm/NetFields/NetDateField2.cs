using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using NetCms.Connections;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetDateField2 : NetField
    {
        public enum SearchTypes { Equal = 0, Major = 1, Minor = 2};

        private SearchTypes _SearchType;

        public SearchTypes SearchType
        {
            get { return _SearchType; }
            set { _SearchType = value; }
        }
	
        private Connection Conn;
        public override string Value
        {
            set
            {
                //if (value.Length > 10)
                //    _Value = value.Remove(10);
                //else
                    _Value = value;
            }
            get { return _Value; }
        }

        private int _Size;
        public int Size
        {
            get { return _Size; }
            set { _Size = value; }
        }
	

        public NetDateField2(string label, string fieldname)
            : base(label, fieldname)
        {
            Required = false;
            _Size = 60;
        }
        public NetDateField2(string label, string fieldname, Connection conn)
            : base(label, fieldname)
        {
            Required = false;
            _Size = 60;
            Conn = conn;
        }

        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl par = new HtmlGenericControl(DefaultControlTag);
            par.Attributes["class"] = DefaultControlTag + "_" + _FieldName;

            Label lb = new Label();
            lb.AssociatedControlID = _FieldName;
            if (Required)
                lb.Text = _Label + " " + NLabels[NetFormLabels.NetFormLabelsList.FormatoData] + "*";
            else
                lb.Text = _Label + " " + NLabels[NetFormLabels.NetFormLabelsList.FormatoData];
            lb.Attributes["class"] = "formlabel";
            par.Controls.Add(lb);

            TextBox textBox = new TextBox();
            textBox.ID = _FieldName;
            textBox.Columns = Size;
            textBox.Text = Value;

            par.Controls.Add(textBox);

            return par;
        }

        public override String getFieldName()
        {
            return _FieldName;
        }

        public override string validateInput(string input)
        {
            string errors = "";

            DateTime date;
            if (Required)
            {
                if (input.Length == 0)
                {
                    errors += "<li>";
                    errors += string.Format(NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio],this.Label);
                    errors += "</li>";
                }
                else
                    if (!DateTime.TryParse(input, out date))
                    {
                        errors += "<li>";
                        errors += string.Format(NLabels[NetFormLabels.NetFormLabelsList.IlCampoNonEValido], this.Label);
                        errors += "</li>";
                    }
            }
            return errors;
        }

        public override string getFilter(string value)
        {
            value = value.Trim();
            string filter = " ";
            if (value.Length > 0)
            {
                filter += FieldName;
                filter += " " + (SearchType == SearchTypes.Equal ? "=" : (SearchType == SearchTypes.Major ? ">" : "<")) ;
                if (Conn != null)
                    filter += Conn.FilterDate(value);
                else
                    filter += " '" +value+"'";
                
                return filter;
            }
            return filter;
        }

        public override string validateValue(string value)
        {
            string output = value.Trim();
            output = output.Replace("'", "''");
            output = output.Trim();
            if (Conn != null)
                output = Conn.FilterDate(output).Length == 0 ? "NULL" : Conn.FilterDate(output);
            else
            {
                DateTime date;
                if (DateTime.TryParse(output, out date))
                    output = "'" + (date.Year + "-" + date.Month + "-" + date.Day + " " + date.Hour + "." + date.Minute + "." + date.Second) + "'";
                else
                    output = "'" + (output) + "'";
            }
            return output;
        }
    }

}