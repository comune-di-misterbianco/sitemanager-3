using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetHiddenField : NetField
    {
        public override bool ShowInInOverview{get{return false;}}

        private bool _ValidValue = false;
        public bool ValidValue
        {
            get { return _ValidValue; }
            set { _ValidValue = value; }
        }
	
        public override string Value
        {
            set
            {
                if (!init)
                {
                    init = !init;
                    _Value = value;
                    
                }
            }
            get { return _Value; }
        }
        private bool init = false;

        public NetHiddenField(string fieldname)
            : base("", fieldname)
        {
            Required = false;
        }
        public NetHiddenField(string fieldname,string label)
            : base(label, fieldname)
        {
            Required = false;
        }

        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl output = new HtmlGenericControl(DefaultControlTag);
            HtmlInputHidden hidden = new HtmlInputHidden();
            hidden.ID = FieldName;
            hidden.Value = Value;

            output.Controls.Add(hidden);
            return output;
        }

        public override String getFieldName()
        {
            return _FieldName;
        }

        public override string validateInput(string input)
        {
            return "";
        }
        
        public override string getFilter(string value)
        {
            string filter = " ";
            return filter;
        }

        public override string validateValue(string value)
        {
            if (init)
                return this.Value;

            if (!ValidValue)
                return base.validateValue(value);
            else
                return value;
        }
    }

}