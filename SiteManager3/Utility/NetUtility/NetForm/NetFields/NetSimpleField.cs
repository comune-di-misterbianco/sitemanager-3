using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetSimpleField
    {
        public string Value
        {
            set
            {
                _Value = value;
            }
            get { return _Value; }
        }
        public string _Value = "";

        private string _Label;
        private string _ID;

        public string Label
        {
            get { return _Label; }
        }
        public string ID
        {
            get { return _ID; }
        }

        private bool _LabelToBlock;

        public bool LabelToBlock
        {
            get { return _LabelToBlock; }
            set { _LabelToBlock = value; }
        }
        private int _Size;

        public int Size
        {
            get { return _Size; }
            set { _Size = value; }
        }

        private Control _CustomControl;

        public Control CustomControl
        {
            get {
                if (_CustomControl == null)
                {
                    TextBox textBox = new TextBox();
                    textBox.ID = ID;
                    textBox.Columns = Size;
                    textBox.Text = Value;

                    _CustomControl = textBox;
                }
                return _CustomControl; 
            }
            set { _CustomControl = value; }
        }
	

        public NetSimpleField(string label, string id)
        {
            //
            // TODO: Add constructor logic here
            //
            _Label = label;
            _ID = id;
            _LabelToBlock = true;
            _Size = 40;
        }
        public NetSimpleField(string label,Control customControl)
        {
            //
            // TODO: Add constructor logic here
            //
            _Label = label;
            _ID = customControl.ID;
            _LabelToBlock = true;
            _Size = 40;
            _CustomControl = customControl;
        }

        public HtmlGenericControl Control
        {
            get
            {
                HtmlGenericControl p = new HtmlGenericControl("p");

                Label lb = new Label();
                lb.AssociatedControlID = ID;
                lb.Text = Label;

                if(LabelToBlock)
                    lb.Style.Add("display", "block");
                p.Controls.Add(lb);

                p.Controls.Add(CustomControl);

                return p;
            }
        }

    }

}