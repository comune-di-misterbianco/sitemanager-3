using System;
using System.Globalization;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetCms.Connections;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetDateTimeField : NetField
    {        
        private Connection Conn;
        public override string Value
        {
            set
            {
                _Value = value;
            }
            get { return _Value; }
        }

        private bool _AddTime;
        public bool AddTime
        {
            get { return _AddTime; }
            set { _AddTime = value; }
        }


        public NetDateTimeField(string label, string fieldname, Connection conn,bool addTime)
            : base(label, fieldname)
        {
            AddTime = addTime;
            Required = false;
            Conn = conn;
        }
        public NetDateTimeField(string label, string fieldname, Connection conn)
            : this(label, fieldname,conn,false)
        {
        }

        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl par = new HtmlGenericControl(DefaultControlTag);

            if (this.CssClass != string.Empty)
                par.Attributes["class"] = this.CssClass;

            DateTime time;
            DateTime.TryParse(Value, out time);
            
            Label lb = new Label();
            lb.Text = Label;
            if (Required)
                 lb.Text += "*";
            lb.Text += ": ";
            lb.Attributes["class"] = "formlabel bold";
            par.Controls.Add(lb);

            #region Giorno
		
            lb = new Label();
            lb.AssociatedControlID = FieldName + "_Giorno";
            lb.Text = this.NLabels[NetFormLabels.NetFormLabelsList.Giorno];
            lb.Attributes["class"] = "dateformlabel";
            par.Controls.Add(lb);

            DropDownList giorni = new DropDownList();
            giorni.ID = FieldName + "_Giorno";
            for (int i = 1; i < 32; i++)
            {
                string val = i.ToString();
                if (val.Length == 1)
                    val = "0" + val;
                ListItem item = new ListItem(val,val);
                giorni.Items.Add(item);
                if (time.Day == i)
                {
                    item.Selected = true;
                    giorni.SelectedIndex = giorni.Items.IndexOf(item);
                }
            }

            par.Controls.Add(giorni);
 
	        #endregion

            #region Mesi

            //string[] Months = { "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre" };

            lb = new Label();
            lb.AssociatedControlID = FieldName + "_Mese";
            lb.Text = this.NLabels[NetFormLabels.NetFormLabelsList.Mese];
            lb.Attributes["class"] = "dateformlabel";
            par.Controls.Add(lb);

            DropDownList mesi = new DropDownList();
            mesi.ID = FieldName + "_Mese";
            for (int i = 1; i <= 12; i++)
            {
                string val = i.ToString();
                if (val.Length == 1)
                    val = "0" + val; 
                ListItem item = new ListItem(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i), val);
                mesi.Items.Add(item);
                if (time.Month == i)
                {
                    item.Selected = true;
                    mesi.SelectedIndex = mesi.Items.IndexOf(item);
                }
            }

            par.Controls.Add(mesi); 

            #endregion

            #region Anno

            lb = new Label();
            lb.AssociatedControlID = FieldName;
            lb.Text = this.NLabels[NetFormLabels.NetFormLabelsList.Anno];
            lb.Attributes["class"] = "dateformlabel";
            par.Controls.Add(lb);

            TextBox anno = new TextBox();
            anno.ID = FieldName;
            anno.Columns = 4;
            anno.Text = Value.Length>0?time.Year.ToString():DateTime.Now.Year.ToString();

            par.Controls.Add(anno); 

            #endregion

            if (AddTime)
            {
                #region Ora

                lb = new Label();
                lb.AssociatedControlID = FieldName + "_Ora";
                lb.Text = this.NLabels[NetFormLabels.NetFormLabelsList.Ora];
                lb.Attributes["class"] = "dateformlabel";
                par.Controls.Add(lb);

                DropDownList Ora = new DropDownList();
                Ora.ID = FieldName + "_Ora";
                for (int i = 0; i < 25; i++)
                {
                    string val = (i).ToString();
                    if (val.Length == 1)
                        val = "0" + val;
                    ListItem item = new ListItem(val, val);
                    Ora.Items.Add(item);
                    if (time.Hour == i)
                    {
                        item.Selected = true;
                        Ora.SelectedIndex = Ora.Items.IndexOf(item);
                    }
                }
                par.Controls.Add(Ora);

                #endregion

                #region Minuti

                lb = new Label();
                lb.AssociatedControlID = FieldName + "_Minuti";
                lb.Text = this.NLabels[NetFormLabels.NetFormLabelsList.Minuti];
                lb.Attributes["class"] = "dateformlabel";
                par.Controls.Add(lb);

                DropDownList Minuti = new DropDownList();
                Minuti.ID = FieldName + "_Minuti";
                for (int i = 0; i < 61; i++)
                {
                    string val = (i).ToString();
                    if (val.Length == 1)
                        val = "0" + val;
                    ListItem item = new ListItem(val, val);
                    Minuti.Items.Add(item);
                    if (time.Minute == i)
                    {
                        item.Selected = true;
                        Minuti.SelectedIndex = Minuti.Items.IndexOf(item);
                    }
                }
                par.Controls.Add(Minuti);

                #endregion
            }
            return par;
        }

        public override String getFieldName()
        {
            return _FieldName;
        }

        public override string validateInput(string input)
        {
            string errors = "";

            string[] date = GetDateTime();
            if (date[1].Length > 0 && Required)
                errors = date[1];
                    
            return errors;
        }

        public override string getFilter(string value)
        {
            value = value.Trim();
            string filter = " ";
            if (value.Length > 0)
            {
                filter += " = '";
                filter += value;
                filter += "'";
                return filter;
            }
            return filter;
        }

        public override string validateValue(string value)
        {
            string[] output = GetDateTime();
            if (Required && output[1].Length > 0)
                return  "";
            return "'"+output[0]+"'";
        }

        private string[] GetDateTime()
        {
            string errors = "";
            DateTime date;

            NetUtility.RequestVariable ora, minuti, anno, mese, giorno;

            ora = new NetUtility.RequestVariable(FieldName + "_Ora", NetUtility.RequestVariable.RequestType.Form);
            minuti = new NetUtility.RequestVariable(FieldName + "_Minuti", NetUtility.RequestVariable.RequestType.Form);
            anno = new NetUtility.RequestVariable(FieldName + "", NetUtility.RequestVariable.RequestType.Form);
            mese = new NetUtility.RequestVariable(FieldName + "_Mese", NetUtility.RequestVariable.RequestType.Form);
            giorno = new NetUtility.RequestVariable(FieldName + "_Giorno", NetUtility.RequestVariable.RequestType.Form);

            if (AddTime)
            {
                if (!ora.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                {
                    errors += "<li>";
                    errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio], this.NLabels[NetFormLabels.NetFormLabelsList.Ora]);
                    errors += "</li>";
                }

                if (!minuti.IsValid(NetUtility.RequestVariable.VariableType.Integer))
                {
                    errors += "<li>";
                    errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio], this.NLabels[NetFormLabels.NetFormLabelsList.Minuti]);
                    errors += "</li>";
                }
            }
            if (!giorno.IsValid(NetUtility.RequestVariable.VariableType.Integer))
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio], this.NLabels[NetFormLabels.NetFormLabelsList.Giorno]);
                errors += "</li>";
            }

            if (!mese.IsValid(NetUtility.RequestVariable.VariableType.Integer))
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio], this.NLabels[NetFormLabels.NetFormLabelsList.Mese]);
                errors += "</li>";
            }

            if (!anno.IsValid(NetUtility.RequestVariable.VariableType.Integer) || anno.StringValue.Length != 4)
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorioEDeveEssereDiQuattroCifre], this.NLabels[NetFormLabels.NetFormLabelsList.Anno]);
                errors += "</li>";
            }
            string datetime = "";
            if (errors.Length == 0)
            {
                datetime = anno.StringValue + "/" + mese.StringValue + "/" +giorno.StringValue ;
                if (AddTime)
                    datetime += " " + ora.StringValue + ":" + minuti.StringValue + ":" + "00";

                if (!DateTime.TryParse(datetime, out date))
                {
                    errors += "<li>";
                    errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoNonContieneUnaDataValida], Label);
                    errors += "</li>";
                }

                datetime = anno.StringValue + "" + mese.StringValue + "" + giorno.StringValue;
                if (AddTime)
                    datetime += "" + ora.StringValue + "" + minuti.StringValue + "" + "00";
            }
            string[] str = { datetime, errors };
            return str ;
        }
    }

}