using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetTextBox : NetField
    {
        public bool Required;

        public bool eMail;
        public bool Phone;
        public bool Numeric;
        public bool File;
        public bool Follder;

        public int MaxLenght = 255;
        public int Size = 60;

        public NetTextBox(string label, string fieldname)
            : base(label, fieldname)
        {
            //
            // TODO: Add constructor logic here
            //

            Required = false;
            eMail = false;
            Phone = false;
            Numeric = false;
        }

        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl par = new HtmlGenericControl(DefaultControlTag);
            if (this.CssClass != string.Empty)
                par.Attributes["class"] = this.CssClass;

            Label lb = new Label();
            lb.AssociatedControlID = _FieldName;
            if (Required)
                lb.Text = _Label + "*";
            else
                lb.Text = _Label;
            //lb.Text += (MaxLenght == 0 ? "" : " (Lunghezza Massima: " + this.MaxLenght + " Caratteri)");
        
            lb.Attributes["class"] = "formlabel";
            par.Controls.Add(lb);

            TextBox textBox = new TextBox();
            textBox.ID = _FieldName;
            textBox.Attributes["title"] = "Lunghezza Massima: " + this.MaxLenght + " Caratteri";
            textBox.Columns = Size;
            textBox.Text = Value;

            par.Controls.Add(textBox);

            return par;
        }

        public override String getFieldName()
        {
            return _FieldName;
        }

        public override string validateInput(string input)
        {
            string errors = "";

            if (Required && input.Length == 0)
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio], this.Label.Replace(":", ""));
                errors += "</li>";
            }

            if (eMail)
            {
                Regex regex = new Regex(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$",
                           RegexOptions.IgnorePatternWhitespace);

                if (!regex.IsMatch(input))
                {
                    errors += "<li>";
                    errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.CampoEmailNonValido], this.Label.Replace(":", ""));
                    errors += "</li>";
                }

            }

            if (Phone)
            {
                Regex regex = new Regex(@"^([0-9]{8,12})$",
                           RegexOptions.IgnorePatternWhitespace);

                if (!regex.IsMatch(input))
                {
                    errors += "<li>";
                    errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.CampoTelefonoNonValido], this.Label.Replace(":", ""));
                    errors += "</li>";
                }
            }

            if (Numeric)
            {

                Regex regex = new Regex(@"^([0-9]*)$",
                           RegexOptions.IgnorePatternWhitespace);

                if (!regex.IsMatch(input))
                {
                    errors += "<li>";
                    errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.CampoNumericoNonValido], this.Label.Replace(":", ""));
                    errors += "</li>";
                }
            }


            Regex regex1 = new Regex(@"^(?=.*[a-zA-Z0-9]+.*)", RegexOptions.IgnorePatternWhitespace);
            if (input.Length > 0 && !regex1.IsMatch(input))
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoNonEValido], this.Label);
                errors += "</li>";
            }

            if (MaxLenght != 0 && input.Length > MaxLenght)
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.LunghezzaMassimaCampo], this.Label.Replace(":", ""),MaxLenght.ToString());
                errors += "</li>";
            }

            return errors;

        }

        public override string getFilter(string value)
        {
            string filter = " ";
            value = value.Trim();
            if (value.Length > 0)
            {
                filter += _FieldName;

                if (Numeric)
                {
                    filter += " = ";
                    filter += value;
                    return filter;
                }

                if (eMail || Phone)
                {
                    filter += " = '";
                    filter += value;
                    filter += "'";
                    return filter;
                }

                filter += " Like '%";
                filter += value;
                filter += "%'";
            }
            return filter;
        }
    }
}