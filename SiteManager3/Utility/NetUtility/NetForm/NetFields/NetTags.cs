﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace NetForms
{
    public class NetTags : NetField
    {
        #region properties

        public int MaxLenght = 255;
        public int Size = 60;
        private string _PlaceholderText;
        public string PlaceholderText
        {
            get { return _PlaceholderText; }
            set { _PlaceholderText = value; }
        }
        private string _TagList;
        /// <summary>
        /// Elenco di tag già presenti nel sistema passati come stringa e formattati nel seguento modo: 'tag1', 'tag2' 
        /// </summary>
        public string TagList
        {
            get { return _TagList; }
            set { _TagList = value; }
        }

        private int maxtag = 5;
        public int MaxTag
        {
            get { return maxtag; }
            set { maxtag = value; }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="label">Etichetta</param>
        /// <param name="fieldname">Nome del campo</param>
        /// <param name="addedtaglist">Elenco dei Tag da visualizzare</param>
        /// <param name="avaibletags">Elenco dei Tag disponibili come suggest per l'utente.</param>
        public NetTags(string label, string fieldname, List<string> addedtaglist = null, List<string> avaibletags = null)
            : base(label, fieldname)
        {

        }

        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl control = new HtmlGenericControl("div");
            control.Attributes["class"] = "nettags";

            //string js = @"<script type=""text/javascript"" src=""/scripts/shared/bootstrap-tagsinput/src/bootstrap-tagsinput.js""></script>";
            //string css = @"<link href = ""/scripts/shared/bootstrap-tagsinput/src/bootstrap-tagsinput.css"" rel=""stylesheet""/>";

            string js = @"<script type=""text/javascript"" src=""/scripts/shared/tagify/dist/tagify.min.js""></script>";
            string css = @"<link href = ""/scripts/shared/tagify/dist/tagify.css"" rel=""stylesheet""/>";


            //  string whitelist = !string.IsNullOrEmpty(TagList) ? "whitelist : [" + TagList + @"]," : "" ;
            //delimiters: "",| "",  

            string strCode = @"
            <script type=""text/javascript"">
            $(document).ready(function() {
               $(window).keydown(function(event){
                if(event.keyCode == 13) {
                  event.preventDefault();
                  return false;
                }
              });
            
            var input_"+this._FieldName + @" = document.querySelector('input[name=" + this._FieldName + @"]'),
                tagify1 = new Tagify(input_" + this._FieldName + @", 
                {          
                    " + (!string.IsNullOrEmpty(TagList) ? "whitelist : [" + TagList + @"]," : "") + @"                    
                    maxTags: " + MaxTag+@"
                }
                )
            });
            </script>
            ";

            control.Controls.Add(new LiteralControl(js));
            control.Controls.Add(new LiteralControl(css));
            control.Controls.Add(new LiteralControl(strCode));

            HtmlGenericControl par = new HtmlGenericControl(DefaultControlTag);
            if (this.CssClass != string.Empty)
                par.Attributes["class"] = this.CssClass;

            Label lb = new Label();
            lb.AssociatedControlID = _FieldName;
            if (Required)
                lb.Text = _Label + "*";
            else
                lb.Text = _Label;
          

            lb.Attributes["class"] = "formlabel";
            par.Controls.Add(lb);

            TextBox textBox = new TextBox();
            textBox.ID = _FieldName;
           // textBox.Attributes["data-role"] = "tagsinput";
            textBox.Attributes["name"] = this._FieldName;
            textBox.Attributes["title"] = "Lunghezza Massima: " + this.MaxLenght + " Caratteri";
            textBox.Attributes["placeholder"] = PlaceholderText;// "Aggiungi qualche parola";
            textBox.Columns = Size;
            textBox.Text = Value;

            par.Controls.Add(textBox);

            control.Controls.Add(par);

            return control; 
        }

        public override string getFilter(string value)
        {
            string filter = " ";
            value = value.Trim();
            return filter;
        }

        public override string validateInput(string input)
        {
            string errors = "";

            if (Required && input.Length == 0)
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio], this.Label.Replace(":", ""));
                errors += "</li>";
            }

            return errors;
        }
    }
}
