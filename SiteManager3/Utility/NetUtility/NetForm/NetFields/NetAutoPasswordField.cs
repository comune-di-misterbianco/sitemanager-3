using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetAutoPasswordField : NetField
    {
        public override bool JumpField
        {
            get
            {
                return false;
            }
        }

        private CyperingType _CyperType;
        public CyperingType CyperType
        {
            get { return _CyperType; }
            set { _CyperType = value; }
        }

        public enum CyperingType
        {
            None,
            Default,
            MD5,
            SHA1
        }

        private string CyperFilter(string input)
        {
            string strCyper = null;
            switch (_CyperType)
            {
                case CyperingType.None:
                    strCyper = FormsAuthentication.HashPasswordForStoringInConfigFile(input, "MD5").ToString();
                    break;
                case CyperingType.Default:
                    strCyper = FormsAuthentication.HashPasswordForStoringInConfigFile(input, "MD5").ToString();
                    break;
                case CyperingType.MD5:
                    strCyper = FormsAuthentication.HashPasswordForStoringInConfigFile(input, "MD5").ToString();
                    break;
                case CyperingType.SHA1:
                    strCyper = FormsAuthentication.HashPasswordForStoringInConfigFile(input, "SHA1").ToString();
                    break;
            }
            return strCyper;
        }

        public NetAutoPasswordField(string fieldname)
            : base("", fieldname)
        {
            _CyperType = CyperingType.Default;
            this.Required = true;
            DefaultControlTag = "span";
        }

        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl par = new HtmlGenericControl(DefaultControlTag);

            HiddenField pwd;

            pwd = new HiddenField();
            pwd.ID = FieldName;
            par.Controls.Add(pwd);
            pwd.Value = BuildPassword(8);
            return par;
        }

        public override String getFieldName()
        {
            return FieldName;
        }

        public override string validateInput(string input)
        {
            return "";
        }

        public override string validateValue(string value)
        {
            string output = value.Trim();

            if (CyperType != CyperingType.None)
                output = this.CyperFilter(output);

            output = "'" + output + "'";

            return output;
        }

        public override string getFilter(string value)
        {
            string filter = " ";

            if (CyperType != CyperingType.None)
                filter = this.CyperFilter(value);

            filter = filter.Replace("'", "''");

            return filter;
        }

        private string BuildPassword(int len)
        {
            string strNewPass = null;
            int whatsNext;
            int upper;
            int lower;

            Random rnd = new Random();

            for (int intCounter = 1; intCounter <= len; intCounter++)
            {
                whatsNext = rnd.Next(2);

                if (whatsNext == 0)
                {
                    upper = 90;
                    lower = 65;
                }
                else
                {
                    upper = 57;
                    lower = 49;
                }

                int k = 0;

                do
                {
                    k = (int)((upper - lower + 1) * rnd.NextDouble() + lower);
                }
                while (k == 79 || k == 73);

                string strA = new string((Char)k, 1);

                strNewPass = strNewPass + strA;

            }

            return strNewPass;
        }
    }

}