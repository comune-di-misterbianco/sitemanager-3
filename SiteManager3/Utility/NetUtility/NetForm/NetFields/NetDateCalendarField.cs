using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using NetCms.Connections;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetDateCalendarField : NetField
    {
        public enum SearchTypes { Equal = 0, Major = 1, Minor = 2};

        private SearchTypes _SearchType;

        public SearchTypes SearchType
        {
            get { return _SearchType; }
            set { _SearchType = value; }
        }
	
        private Connection Conn;
        public override string Value
        {
            set
            {
                //if (value.Length > 10)
                //    _Value = value.Remove(10);
                //else
                    _Value = value;
            }
            get { return _Value; }
        }

        private int _Size;
        public int Size
        {
            get { return _Size; }
            set { _Size = value; }
        }

        string field_prefix = "";//"DC_";

        private string _Language = "it";
        /// <summary>
        /// Set DateTimePicker Localization
        /// </summary>
        public string Language
        {
            get { return _Language; }
            set { _Language = value; }

        }

        private bool _UseDateTimePicker = false;
        /// <summary>
        /// Enable/Disable Time Picker
        /// </summary>
        public bool UseDateTimePicker
        {
            get { return _UseDateTimePicker; }
            set { _UseDateTimePicker = value; }
        }

        private bool _ShowInline = false;
        /// <summary>
        /// Show DateTimePicker Inline
        /// </summary>                
        /// <remarks> Will display the picker inline without the need of a input field. This will also hide borders and shadows. </remarks>
        public bool ShowInline
        {
            get { return _ShowInline; }
            set { _ShowInline = value; }
        }

        private bool _ShowTimePickerSideBySide = false;
        /// <summary>
        /// Show TimePicker Side By Side
        /// </summary>
        /// <remarks> Shows the picker side by side when using the time and date together. </remarks>
        public bool ShowTimePickerSideBySide
        {
            get { return _ShowTimePickerSideBySide; }
            set { _ShowTimePickerSideBySide = value; }
        }

        private bool _ShowCalendarWeekNumber = false;
        /// <summary>
        /// Show Calendar Week Number
        /// </summary>
        public bool ShowCalendarWeekNumber
        {
            get { return _ShowCalendarWeekNumber; }
            set { _ShowCalendarWeekNumber = value; }
        }

        private bool _ShowClearButton = false;
        /// <summary>
        /// Show Clear Button
        /// </summary>
        /// <remarks>Show the "Clear" button in the icon toolbar. Clicking the "Clear" button will set the calendar to null.</remarks>   
        public bool ShowClearButton
        {
            get { return _ShowClearButton; }
            set { _ShowClearButton = value; }
        }

        private bool _ShowCloseButton = false;
        /// <summary>
        /// Show Close Button
        /// </summary>
        /// <remarks>Show the "Close" button in the icon toolbar. Clicking the "Close" button will hide datetimepicker.</remarks>   
        public bool ShowCloseButton
        {
            get { return _ShowCloseButton; }
            set { _ShowCloseButton = value; }
        }

        private bool _AutoClose;
        public bool AutoClose
        {
            get { return _AutoClose; }
            set { _AutoClose = value; }
        }

        private bool _ShowTodayButton = false;
        /// <summary>
        /// Show Today Button
        /// </summary>
        /// <remarks>Show the "Today" button in the icon toolbar. Clicking the "Today" button will set the calendar view and set the date to now</remarks>   
        public bool ShowTodayButton
        {
            get { return _ShowTodayButton; }
            set { _ShowTodayButton = value; }
        }

        /// <summary>
        /// Toolbar Placement Option
        /// </summary>
        public enum DTPToolBarPlacement
        {
            Default,
            Top,
            Bottom
        }

        private DTPToolBarPlacement _ToolbarPlacement = DTPToolBarPlacement.Default;
        /// <summary>
        /// Toolbar Placement
        /// </summary>
        /// <remarks>Changes the placement of the icon toolbar.</remarks>
        public DTPToolBarPlacement ToolbarPlacement
        {
            get { return _ToolbarPlacement; }
            set { _ToolbarPlacement = value; }
        }

        private DateTime _MaxDate = DateTime.MaxValue;
        /// <summary>
        /// Set Maximum date of DateTimePicker
        /// </summary>
        public DateTime MaxDate
        {
            get { return _MaxDate; }
            set { _MaxDate = value; }
        }

        private DateTime _MinDate = DateTime.MinValue;
        /// <summary>
        /// Min Date
        /// </summary>
        /// <remarks>Set Minimum date of DateTimePicker </remarks>
        public DateTime MinDate
        {
            get { return _MinDate; }
            set { _MinDate = value; }
        }

        private bool _Multidate = false;
        public bool Multidate
        {
            get {
                return _Multidate;
            }
            set {
                _Multidate = value;
            }
           
        }

        #region Costructor
        public NetDateCalendarField(string label, string fieldname)
            : base(label, fieldname)
        {
            Required = false;
            _Size = 60;
            DefaultControlTag = "div";
        }
        public NetDateCalendarField(string label, string fieldname, Connection conn)
            : base(label, fieldname)
        {
            Required = false;
            _Size = 60;
            Conn = conn;
            DefaultControlTag = "div";
        }

        public NetDateCalendarField(string label, string fieldname, Connection conn, bool useDateTimePicker)
            : base(label, fieldname)
        {
            Required = false;
            _Size = 60;
            Conn = conn;
            UseDateTimePicker = useDateTimePicker;
            DefaultControlTag = "div";
        }

        #endregion

       
        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl fieldControl = new HtmlGenericControl(DefaultControlTag);
            fieldControl.Attributes["class"] = DefaultControlTag + "_" + _FieldName + " " + "form-group";

            #region Scripts
            WebControl script = new WebControl(HtmlTextWriterTag.Script);
            script.Attributes["type"] = "text/javascript";

            string str_script = "";
            //int currentYear = DateTime.Now.Year;
            //int initYear = currentYear - 115;
            //int endYear = currentYear + 10;
            string parsedValue = string.Empty;
            if (!string.IsNullOrEmpty(Value))
            {
                DateTime currentValue = DateTime.Parse(Value);
                parsedValue = (UseDateTimePicker) ? currentValue.ToString("dd/MM/yyyy HH:mm"): currentValue.ToString("dd/MM/yyyy");
            }            

            string defaultValue = (!string.IsNullOrEmpty(Value) && Value != "01/01/0001 00:00:00" ? "defaultDate: ("+ (UseDateTimePicker ? "moment(\"" + parsedValue + "\",\"DD/MM/YYYY HH:mm\"" : "moment(\"" + parsedValue + "\",\"DD/MM/YYYY\"") + "))" : "");
            string multidateAllowed = (Multidate ? "allowMultidate: true, multidateSeparator:','" : "");           

            str_script = @"
                $(document).ready(function() {
                    

                    $(""#datetimepicker_" + this.FieldName + @""").datetimepicker({
                        locale:'" + Language + @"',                        
                        format: '" + (UseDateTimePicker ? "DD/MM/YYYY HH:mm" : "DD/MM/YYYY") + @"',                        
                        " + multidateAllowed + @"                        
                        " + defaultValue + @"
                    }) 
                });";
            #endregion

            script.Controls.Add(new LiteralControl(str_script));
            fieldControl.Controls.Add(script);

            string fieldLabel = _Label + " " + NLabels[NetFormLabels.NetFormLabelsList.FormatoData] + ((Required) ? "*" : "");
            fieldControl.Controls.Add(new LiteralControl(@"<label for=""" + this.FieldName + @""" > " + fieldLabel + @"</label>"));

            WebControl datetimepicker = new WebControl(HtmlTextWriterTag.Div);
            datetimepicker.CssClass = "input-group date";
            datetimepicker.ID = "datetimepicker_" + this.FieldName;
            datetimepicker.Attributes["data-target-input"] = "nearest";

            TextBox textBox = new TextBox();
            textBox.ID = FieldName;
            textBox.Columns = Size;
            //textBox.Text = Value;
            textBox.CssClass = "form-control datetimepicker-input";
            textBox.Attributes["data-target"] = "#datetimepicker_" + this.FieldName;
            datetimepicker.Controls.Add(textBox);

            WebControl inputGroup = new WebControl(HtmlTextWriterTag.Div);
            inputGroup.CssClass = "input-group-append";
            inputGroup.Attributes["data-toggle"] = "datetimepicker";
            inputGroup.Attributes["data-target"] = "#datetimepicker_" + this.FieldName;
            
            inputGroup.Controls.Add(new LiteralControl(@"<div class=""input-group-text""><i class=""fa fa-calendar""></i></div>"));
            

            datetimepicker.Controls.Add(inputGroup);

            fieldControl.Controls.Add(datetimepicker);

            return fieldControl;
        }

        public override String getFieldName()
        {
            return _FieldName;
        }

        public override string validateInput(string input)
        {
            string errors = "";

            DateTime date;
            if (Required)
            {
                if (input.Length == 0)
                {
                    errors += "<li>";
                    errors += string.Format(NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio],this.Label);
                    errors += "</li>";
                }
                else
                    if (!DateTime.TryParse(input, out date))
                    {
                        errors += "<li>";
                        errors += string.Format(NLabels[NetFormLabels.NetFormLabelsList.IlCampoNonEValido], this.Label);
                        errors += "</li>";
                    }
            }
            return errors;
        }

        public override string getFilter(string value)
        {
            value = value.Trim();
            string filter = " ";
            if (value.Length > 0)
            {
                filter += FieldName;
                filter += " " + (SearchType == SearchTypes.Equal ? "=" : (SearchType == SearchTypes.Major ? ">" : "<")) ;
                if (Conn != null)
                    filter += Conn.FilterDate(value);
                else
                    filter += " '" +value+"'";
                
                return filter;
            }
            return filter;
        }

        public override string validateValue(string value)
        {
            string output = value.Trim();
            output = output.Replace("'", "''");
            output = output.Trim();
            if (Conn != null)
                output = Conn.FilterDate(output).Length == 0 ? "NULL" : Conn.FilterDate(output);
            else
            {
                DateTime date;
                if (DateTime.TryParse(output, out date))
                    output = "'" + (date.Year + "-" + date.Month + "-" + date.Day + " " + date.Hour + "." + date.Minute + "." + date.Second) + "'";
                else
                    output = "'" + (output) + "'";
            }
            return output;
        }
    }

}