using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetPasswordField : NetField
    {
        private bool _Required;
        public bool Required
        {
            get { return _Required; }
            set { _Required = value; }
        }

        public int MaxLenght = 255;
        public int MinLenght = 8;
        public int Size = 60;


        private bool _OldPasswordCheck;
        public bool OldPasswordCheck
        {
            get { return _OldPasswordCheck; }
            set { _OldPasswordCheck = value; }
        }

        private string _OldPassword;
        public string OldPassword
        {
            get { return _OldPassword; }
            set { _OldPassword= value; }
        }

        private bool _HighSecurityField;
        public bool HighSecurityField
        {
            get { return _HighSecurityField; }
            set { _HighSecurityField = value; }
        }

        private bool _RememberPostbackValue;
        public bool RememberPostbackValue
        {
            get { return _RememberPostbackValue; }
            set { _RememberPostbackValue = value; }
        }
	
        public override bool JumpField
        {
            get
            {
                if (!Required)
                {
                    NetUtility.RequestVariable pwd = new NetUtility.RequestVariable(this.FieldName, HttpContext.Current.Request.Form);
                    NetUtility.RequestVariable pwd2 = new NetUtility.RequestVariable(this.FieldName + "_Confirm", HttpContext.Current.Request.Form);

                    return pwd.IsValid() && pwd2.IsValid() && pwd.StringValue == pwd2.StringValue && pwd.StringValue.Length == 0;
                }
                return false;
            }
        }

        private CyperingType _CyperType;
        public CyperingType CyperType
        {
            get { return _CyperType; }
            set { _CyperType = value; }
        }

        public enum CyperingType
        {
            None,
            Default,
            MD5,
            SHA1
        }

        private string CyperFilter(string input)
        {
            string strCyper = null;
            switch (_CyperType)
            {
                case CyperingType.None:
                    strCyper = FormsAuthentication.HashPasswordForStoringInConfigFile(input, "MD5").ToString();
                    break;
                case CyperingType.Default:
                    strCyper = FormsAuthentication.HashPasswordForStoringInConfigFile(input, "MD5").ToString();
                    break;
                case CyperingType.MD5:
                    strCyper = FormsAuthentication.HashPasswordForStoringInConfigFile(input, "MD5").ToString();
                    break;
                case CyperingType.SHA1:
                    strCyper = FormsAuthentication.HashPasswordForStoringInConfigFile(input, "SHA1").ToString();
                    break;
            }
            return strCyper;
        }

        public NetPasswordField(string label, string fieldname)
            : base(label, fieldname)
        {
            _CyperType = CyperingType.Default;
            this.Required = true;
        }

        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");

            HtmlGenericControl par = new HtmlGenericControl(DefaultControlTag);
            if (this.CssClass != string.Empty)
                par.Attributes["class"] = this.CssClass;

            Label lb;
            TextBox textBoxOld;

            if (OldPasswordCheck)
            {
                lb = new Label();
                lb.AssociatedControlID = FieldName;
                if (Required)
                    lb.Text = string.Format(NLabels[NetFormLabels.NetFormLabelsList.Vecchia],this.Label) + "*";
                else
                    lb.Text = string.Format(NLabels[NetFormLabels.NetFormLabelsList.VecchiaLasciareIlCampoVuoto], this.Label);
                lb.Attributes["class"] = "formlabel";
                par.Controls.Add(lb);

                textBoxOld = new TextBox();
                textBoxOld.ID = FieldName + "_Old";
                textBoxOld.Columns = Size;
                textBoxOld.TextMode = TextBoxMode.Password;

                par.Controls.Add(textBoxOld);
                div.Controls.Add(par);
            }

            par = new HtmlGenericControl(DefaultControlTag);
            
            TextBox textBoxPwd;

            lb = new Label();
            lb.AssociatedControlID = FieldName;
            if (Required)
                lb.Text = Label + "*";
            else
                lb.Text = Label + this.NLabels[NetFormLabels.NetFormLabelsList.LasciareIlCampoVuotoPerMantentenereQuellaAttuale];
            lb.Attributes["class"] = "formlabel";
            par.Controls.Add(lb);

            textBoxPwd = new TextBox();
            textBoxPwd.ID = FieldName;
            textBoxPwd.Columns = Size;
            textBoxPwd.TextMode = TextBoxMode.Password;

            if(this.CyperType != CyperingType.None)
                textBoxPwd.Text = Value;

            par.Controls.Add(textBoxPwd);
            div.Controls.Add(par);

            par = new HtmlGenericControl(DefaultControlTag);
            
            TextBox textBoxConfirm;

            lb = new Label();
            lb.AssociatedControlID = FieldName + "_Confirm";
            lb.Text = this.NLabels[NetFormLabels.NetFormLabelsList.Conferma] + " " + Label;           
            if (Required)
                lb.Text += "*";
            else
                lb.Text += " " + this.NLabels[NetFormLabels.NetFormLabelsList.LasciareIlCampoVuotoPerMantentenereQuellaAttuale];
            lb.Attributes["class"] = "formlabel";            
            par.Controls.Add(lb);

            textBoxConfirm = new TextBox();
            textBoxConfirm.ID = FieldName + "_Confirm";
            textBoxConfirm.Columns = Size;
            textBoxConfirm.TextMode = TextBoxMode.Password;

            if (this.CyperType != CyperingType.None)
                textBoxConfirm.Text = Value;

            par.Controls.Add(textBoxConfirm);
            div.Controls.Add(par);
            
            if (RememberPostbackValue && this.validateInput(this.RequestVariable.StringValue).Length == 0)
            {
                textBoxConfirm.Text = this.RequestVariable.StringValue;
                textBoxPwd.Text = this.RequestVariable.StringValue;
            }

            return div;
        }

        public override String getFieldName()
        {
            return FieldName;
        }

        public override string validateInput(string input)
        {
            string errors = "";
            input = input.Trim();

            NetUtility.RequestVariable old_pwd = new NetUtility.RequestVariable(this.FieldName + "_Old", HttpContext.Current.Request.Form);
            if (OldPasswordCheck && !old_pwd.IsValidString)
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio], "Vecchia password");
                errors += "</li>";
            }

            if (Required && input.Length == 0)
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio], this.Label);
                errors += "</li>";
            }

            if (HighSecurityField && input.Length > 0)
            {
                Regex regex = new Regex(@"(?=^.{6,255}$)((?=.*\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*",
                RegexOptions.IgnorePatternWhitespace); 
          

                if (!regex.IsMatch(input))
                {
                    errors += "<li>";
                    errors += string.Format("Il campo '"+ this.Label+"' non � valido, in quanto deve contenere almeno 3 di questi elementi: Lettera Maiuscola, Lettera Minuscola, Numero, Simbolo.");
                    errors += "</li>";
                }

            }

            if (MaxLenght != 0 && input.Length > MaxLenght)
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.LunghezzaMassimaCampo], this.Label, MaxLenght);
                errors += "</li>";
            }

            if (MinLenght > input.Length && (Required || input.Length>0))
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.LunghezzaMinimaCampo], this.Label,MinLenght);
                errors += "</li>";
            }

            NetUtility.RequestVariable pwd = new NetUtility.RequestVariable(this.FieldName, HttpContext.Current.Request.Form);
            NetUtility.RequestVariable pwd2 = new NetUtility.RequestVariable(this.FieldName+"_Confirm", HttpContext.Current.Request.Form);

            if (OldPasswordCheck && OldPassword != null)
            {
                bool ok=false;
                bool oldEqualNew = false;
                if (old_pwd.IsValid())
                {
                    if(CyperFilter(old_pwd.StringValue) == OldPassword)
                        ok=true;
                    if (pwd.IsValid() && CyperFilter(old_pwd.StringValue) == CyperFilter(pwd.StringValue))
                    {
                        ok = false;
                        oldEqualNew = true;
                    }
                } 
                if (!ok)
                {
                    errors += "<li>";
                    if (!oldEqualNew)
                    errors += NLabels[NetFormLabels.NetFormLabelsList.VecchiaPasswordErrata];
                    else
                        errors += NLabels[NetFormLabels.NetFormLabelsList.VecchiaPasswordNuovaPasswordCoincidenti];
                    errors += "</li>";
                }
            }
            if (!pwd.IsValid() || !pwd2.IsValid())
            {
                errors += "<li>";
                errors += NLabels[NetFormLabels.NetFormLabelsList.PasswordNonValide];
                errors += "</li>";
            }
            else
            {
                if (pwd.StringValue != pwd2.StringValue)
                {
                    errors += "<li>";
                    errors += string.Format(NLabels[NetFormLabels.NetFormLabelsList.PasswordNonCoincidenti],Label);
                    errors += "</li>";
                }
            }


            return errors;

        }

        public override string validateValue(string value)
        {
            string output = value.Trim();

            if (CyperType != CyperingType.None)
                output = this.CyperFilter(output);

            output = "'" + output + "'";

            return output;
        }

        public override string getFilter(string value)
        {
            string filter = " ";

            if (CyperType != CyperingType.None)
                filter = this.CyperFilter(value);

            filter = filter.Replace("'", "''");

            return filter;
        }
        
    }

}