using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Connections;
using NetUtility;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetUpDownListBox : NetField
    {
        private string _DataMember;
        private string _DataSource;
        private string _SqlQuery;
        private string _DataValue;

        private ListBox _ListBox;
        public ListBox ListBox
        {
            get 
            {
                if (_ListBox == null)
                {
                    _ListBox = new ListBox();

                    _ListBox.AutoPostBack = AutoPostBack;
                    if (AutoPostBack)
                        _ListBox.Attributes["onchange"] = "setSubmitSource('" + FieldName + "')";
                    _ListBox.ID = FieldName + "_ListBox";
                    _ListBox.CssClass = "NetUpDown_ListBox";
                }
                return _ListBox; 
            }
        }
	

        private bool _AutoPostBack = false;
        public bool AutoPostBack
        {
            get
            {
                return _AutoPostBack;
            }
            set
            {
                _AutoPostBack = value;
            }
        }

        private Connection Conn;

        private int bindings;

        public NetUpDownListBox(string label, string fieldname)
            : base(label, fieldname)
        {
            bindings = 0;

        }

        private string bindList(ListBox box)
        {
            string sql = "";
            if (bindings == 1) sql = "SELECT * FROM " + _DataSource;
            else sql = _SqlQuery;
            DataTable dt = Conn.SqlQuery(sql);
            ListItem item;
            string value = "";

            foreach (DataRow row in dt.Rows)
            {
                if (value.Length > 0)
                    value += ",";
                if (bindings == 1)
                {
                    item = new ListItem(row[_DataMember].ToString(), row[_DataValue].ToString());
                    value += row[_DataValue].ToString();
                }
                else
                {
                    item = new ListItem(row[0].ToString(), row[1].ToString());
                    value += row[1].ToString();
                }
                box.Items.Add(item);
            }

            box.Rows = dt.Rows.Count;
            return value;
        }

        public void setDataBind(string DataMember, string DataValue, string DataSource, Connection Conn)
        {
            _DataMember = DataMember;
            _DataValue = DataValue;
            _DataSource = DataSource;
            Conn = Conn;
            bindings = 1;
        }
        public void setDataBind(string SqlQuery, Connection conn)
        {
            _SqlQuery = SqlQuery;
            Conn = conn;
            bindings = 2;
        }

        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl table = new HtmlGenericControl("table");
            table.Attributes["class"] = "NetUpDown";
            table.Attributes["cellspacing"] = "0";
            table.Attributes["cellpadding"] = "0";
            HtmlGenericControl par = new HtmlGenericControl("td");
            HtmlGenericControl tr = new HtmlGenericControl("tr");
            table.Controls.Add(tr);
            tr.Controls.Add(par);
            par.Attributes["rowspan"] = "3";
            #region Label

            Label lb = new Label();
            lb.AssociatedControlID = FieldName+"_ListBox";
            if (Required)
                lb.Text = Label + "*";
            else
                lb.Text = Label;
            lb.Style.Add("display", "block");
            par.Controls.Add(lb); 

            #endregion

            #region ListBox

            par.Controls.Add(ListBox); 
            #endregion

            #region Databind del ListBox
            
            if (bindings != 0)
                _Value = bindList(ListBox);
            
            #endregion

            ListBox.Rows = ListBox.Items.Count;

            tr = new HtmlGenericControl("tr");
            table.Controls.Add(tr);

            HtmlGenericControl span = new HtmlGenericControl("td");            
            tr.Controls.Add(span);

            #region Campo Hidden per immagazzinare i valori

            span.InnerHtml = "<input ";
            span.InnerHtml += " type=\"hidden\"";
            //span.InnerHtml += " size=\"200\"";
            span.InnerHtml += " name=\"" + FieldName + "\"";
            span.InnerHtml += " id=\"" + FieldName + "\"";
            span.InnerHtml += " value=\"" + Value + "\"";
            span.InnerHtml += " /> ";

            #endregion

            #region Bottone Su

            span.InnerHtml += "<a ";
            span.InnerHtml += " class=\"NetUpDown_UpBt\"";
            span.InnerHtml += " href=\"javascript: " + FieldName + "_MoveUp()\"";
            span.InnerHtml += " ><span>Sposta Su</span></a> ";

            #endregion

            tr = new HtmlGenericControl("tr");
            table.Controls.Add(tr);

            span = new HtmlGenericControl("td");
            tr.Controls.Add(span);
            #region Bottone Gi�

            span.InnerHtml += "<a ";
            span.InnerHtml += " class=\"NetUpDown_DownBt\"";
            span.InnerHtml += " href=\"javascript: " + FieldName + "_MoveDown()\"";
            span.InnerHtml += " ><span>Sposta Gi�</span></a> ";


            #endregion

            tr = new HtmlGenericControl("tr");
            table.Controls.Add(tr);

            span = new HtmlGenericControl("td");
            tr.Controls.Add(span);
            #region Javascript 

            span.InnerHtml += "<script type=\"text/javascript\" >";
            span.InnerHtml += " function "+FieldName + "_MoveDown()";
            span.InnerHtml += @"{
                                    oList = document.getElementById('"+FieldName + @"_ListBox');
                                    var switched = false;
                                    for(i=0;i<oList.options.length && !switched ;i++)
                                    {            
                                        //alert(i+1 +"" - ""+oList.options.length);
                                        if(oList.options[i].selected && i+1<oList.options.length)
                                        {
                                            oOption = oList.options[i];
                                            oList.options[i] = new Option(oList.options[i+1].text, oList.options[i+1].value);											
                                            oList.options[i+1] = new Option(oOption.text, oOption.value);
                                            
                                            oList.options[i+1].selected = true;
                                            oList.optionSelected = i+1;

                                            switched = true;
                                        }
                                    }

                                    oValue = document.getElementById('" + FieldName + @"');
                                    str="""";
                                    for(i=0;i<oList.options.length ;i++)
                                    {            
                                        if(i!=0)
                                            str += ',';                                        
                                        str += oList.options[i].value;                                        
                                    }

                                    oValue.value = str;
}
";

            span.InnerHtml += "";
            span.InnerHtml += " function " + FieldName + "_MoveUp()";
            span.InnerHtml += @"{
                                    oList = document.getElementById('" + FieldName + @"_ListBox');
                                    var switched = false;
                                    for(i=0;i<oList.options.length && !switched ;i++)
                                    {            
                                        if(oList.options[i].selected && i>0)
                                        {
                                            oOption = oList.options[i];
                                            oList.options[i] = new Option(oList.options[i-1].text, oList.options[i-1].value);											
                                            oList.options[i-1] = new Option(oOption.text, oOption.value);
                                            
                                            oList.options[i-1].selected = true;
                                            oList.optionSelected = i-1;

                                            switched = true;
                                        }
                                    }
                                    oValue = document.getElementById('" + FieldName + @"');
                                    str="""";
                                    for(i=0;i<oList.options.length ;i++)
                                    {            
                                        if(i!=0)
                                            str += ',';                                        
                                        str += oList.options[i].value;                                        
                                    }
                                    oValue.value = str;
}
";
            span.InnerHtml += " </script>";
            
            #endregion


            return table;
        }
        
        public override string validateInput(string input)
        {
            return "";
        }

        public override string getFilter(string value)
        {
            string filter = " ";
            if (value.Length > 0)
            {
                filter += _FieldName;

                filter += " = ";
                filter += value;
                filter += " ";
            }
            return filter;
        }
    }

}