using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.IO;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for NetField
/// </summary>
namespace NetForms
{
    public class NetDropDownFileList : NetField
    {
        public string Path;
        private DropDownList DropDownList;

        public NetDropDownFileList(string label, string fieldname, string path)
            : base(label, fieldname)
        {
            Path = path;
            DropDownList = new DropDownList();
        }

        private HtmlGenericControl getControlSimple()
        {
            HtmlGenericControl par = new HtmlGenericControl(DefaultControlTag);
            if (this.CssClass != string.Empty)
                par.Attributes["class"] = this.CssClass;

            Label lb = new Label();
            lb.AssociatedControlID = FieldName;
            if (Required)
                lb.Text = Label + "*";
            else
                lb.Text = Label;
            lb.Attributes["class"] = "formlabel";
            par.Controls.Add(lb);

            DropDownList.ID = FieldName;
            par.Controls.Add(DropDownList);

            return par;
        }

        private void bindList()
        {

            DirectoryInfo dir = new DirectoryInfo(Path.Replace("\\", "/"));
            if (dir.Exists)
            {
                FileInfo[] files = dir.GetFiles();
                ListItem item;
                for (int i = 0; i < files.Length; i++)
                {
                    item = new ListItem(files[i].Name);
                    DropDownList.Items.Add(item);
                }
            }

        }

        public void addItem(string Label)
        {
            DropDownList.Items.Add(Label);
        }

        public void addItem(string Label, string Value)
        {
            ListItem item = new ListItem(Label, Value);
            DropDownList.Items.Add(item);
        }

        public override HtmlGenericControl getControl()
        {
            HtmlGenericControl par = getControlSimple();

            bindList();

            if (Value.Length > 0)
            {
                foreach (ListItem item in DropDownList.Items)
                {
                    if (item.Value == Value)
                    {
                        item.Selected = true;
                        DropDownList.SelectedIndex = DropDownList.Items.IndexOf(item);
                    }
                }
            }
            return par;
        }

        public override String getFieldName()
        {
            return _FieldName;
        }

        public override string validateInput(string input)
        {
            string errors = "";

            if (Required && input.Length == 0)
            {
                errors += "<li>";
                errors += string.Format(this.NLabels[NetFormLabels.NetFormLabelsList.IlCampoEObbligatorio], this.Label);
                errors += "</li>";
            }

            return errors;
        }

        public override string getFilter(string value)
        {
            string filter = " ";
            if (value.Length > 0)
            {
                filter += _FieldName;

                filter += " = ";
                filter += value;
                filter += " ";
            }
            return filter;
        }
    }

}