using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for NetField
/// </summary>

namespace NetForms
{
    public class NetCheckBox : NetField
    {
        public bool Checked;

        public NetCheckBox(string label, string fieldname)
            : base(label, fieldname)
        {
            Checked = false;
        }

        public NetCheckBox(string label, string fieldname, bool check)
            : base(label, fieldname)
        {
            Checked = check;
        }

        private bool _ShowLabelAfterInput = false;
        public bool ShowLabelAfterInput
        {
            get { return _ShowLabelAfterInput; }
            set { _ShowLabelAfterInput = value; }
        }


        // uscire il controllo Checkbox ed esporlo esternamente per consentirne la personalizzazione
        public CheckBox CheckBox
        {
            get
            {
                if (_CheckBox == null)
                {
                    _CheckBox = new CheckBox();
                    _CheckBox.ID = _FieldName;
                    _CheckBox.CssClass = "checkbox";                    
                }
                return _CheckBox;
            }
        }
        private CheckBox _CheckBox;

        private bool _AutoPostBack;
        public bool AutoPostBack
        {
            get
            {
                return _AutoPostBack;
            }
            set
            {
                _AutoPostBack = value;
            }
        }       

        public override HtmlGenericControl getControl()
        {
            if (AutoPostBack)
                CheckBox.AutoPostBack = true;

            var fieldControlName = this.GetType().Name;

            HtmlGenericControl par = new HtmlGenericControl(DefaultControlTag);
            par.Attributes["class"] = fieldControlName + " " + DefaultControlTag + "_" + _FieldName;

            Label lb = new Label();
            lb.AssociatedControlID = _FieldName;
            lb.Text = _Label;            
            lb.Attributes["class"] = "formlabel";
            
            //CheckBox checkBox = new CheckBox();

            if (Value.Length > 0)
            {
                if (validateValue(Value) == "1")
                    CheckBox.Checked = true;
                else
                    CheckBox.Checked = false;
            }
            //CheckBox.Checked = Checked;
            //checkBox.ID = _FieldName;

            if (!ShowLabelAfterInput)
                par.Controls.Add(lb);

            par.Controls.Add(CheckBox);

            if (ShowLabelAfterInput)
                par.Controls.Add(lb);

            return par;
        }

        public override String getFieldName()
        {
            return _FieldName;
        }

        public override string validateInput(string input)
        {
            return "";
        }

        public override string getFilter(string value)
        {
            return "";
        }

        public override string validateValue(string value)
        {
            value = value.Trim();
            string valid = "0";
            if (value == "on" ||
                value == "True" ||
                value == "true" ||
                value == "vero" ||
                value == "1")
                valid = "1";
            return valid;
        }

        
    }

}