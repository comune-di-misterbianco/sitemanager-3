using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for xNodeCollection
/// </summary>
namespace NetUtility.NetTreeViewControl
{
    public class NetTreeNodeCollection
    {
        private NetCollection coll;
        private NetTreeNode Owner;

        public int Count { get { return coll.Count; } }
        public NetTreeNodeCollection(NetTreeNode owner)
        {
            coll = new NetCollection();
            Owner = owner;
        }

        public void Add(NetTreeNode node)
        {
            coll.Add(node.ID, node);
            node.Parent = Owner;
            if (Owner != null)
            {
                node.Depth = Owner.Depth + 1;
                node.Style = Owner.Style;
            }
        }

        public String[] AllKeys
        {
            get
            {
                return coll.AllKeys;
            }
        }

        public NetTreeNode this[int i]
        {
            get
            {
                NetTreeNode value = (NetTreeNode)coll[coll.Keys[i]];
                return value;
            }
        }

        public NetTreeNode this[string key]
        {
            get
            {
                NetTreeNode field = (NetTreeNode)coll[key];
                return field;
            }
        }

        public NetTreeNode FindNode(string key)
        {
            NetTreeNode node = null;

            if (key == Owner.ID)
                node = Owner;

            if (node == null)
                node = this[key];

            int i = 0;
            while (node == null && i < this.Count)
                node = this[i++].ChildNodes.FindNode(key);

            return node;
        }
    }
}