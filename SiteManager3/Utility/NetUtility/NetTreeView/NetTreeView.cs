using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for xTreeView
/// </summary>
namespace NetUtility.NetTreeViewControl
{
    public class NetTreeView
    {
        public NetTreeViewType Type = 0;
        public NetTreeNode Root;
        public bool ShowCheckBoxes;

        private string ID;

        public NetTreeView()
            : this("xTreeViewSelectedNode")
        {
            Root = new NetTreeNode();
            ShowCheckBoxes = false;
        }
        public NetTreeView(string id)
        {
            ID = id;
            Root = new NetTreeNode();
            ShowCheckBoxes = false;
        }

        public HtmlGenericControl getControl()
        {
            HtmlGenericControl ul = new HtmlGenericControl("ul");
            ul.Attributes["class"] = "xTreeView";

            ul.Controls.Add(Root.getNode(ID, Type));

            HtmlInputHidden hidden = new HtmlInputHidden();
            hidden.ID = ID + "Value";
            ul.Controls.Add(hidden);

            hidden = new HtmlInputHidden();
            hidden.ID = ID;
            ul.Controls.Add(hidden);

            hidden = new HtmlInputHidden();
            hidden.ID = ID + "ID";
            ul.Controls.Add(hidden);


            hidden = new HtmlInputHidden();
            hidden.ID = ID + "Label";
            ul.Controls.Add(hidden);

            NetTreeNodeStyle style = new NetTreeNodeStyle();

            HtmlGenericControl script = new HtmlGenericControl("script");
            script.Attributes["type"] = "text/javascript";
            script.InnerHtml = "xTreeViewInitNodeState('" + style.Minus + "');";
            ul.Controls.Add(script);

            return ul;
        }
    }
    public enum NetTreeViewType
    {
        StaticSimple = 0,
        StaticCheckBoxes = 1,
        ElasticSimple = 2,
        ElasticCheckBoxes = 3
    }

}