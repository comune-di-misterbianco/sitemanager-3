using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for xNode
/// </summary>
namespace NetUtility.NetTreeViewControl
{
    public class NetTreeNode
    {
        public string ID;
        public string Tag;
        public string Value;
        public string Text;
        public bool Grant=true;

        public int Depth = 0;
        public string OnClientClick;

        public bool Checked;
        public NetTreeViewType Type = NetTreeViewType.ElasticSimple;
        public NetTreeNode Parent;

        public NetTreeNodeStyle Style;
        public bool HasChildNodes
        {
            get
            {
                if (ChildNodes != null && ChildNodes.Count > 0)
                    return true;
                return false;
            }
        }

        public NetTreeNodeCollection ChildNodes;

        public NetTreeNode()
            : this(new NetTreeNodeStyle())
        {

        }
        public NetTreeNode(NetTreeNodeStyle nodeStyle)
        {
            ChildNodes = new NetTreeNodeCollection(this);
            Checked = false;
            Style = nodeStyle;
        }

        public Control getNode(string ControlID)
        {
            return getNode(ControlID, false);
        }
        public Control getNode(string ControlID, bool HasNext)
        {
            HtmlGenericControl li = new HtmlGenericControl("li");
            if (!HasNext) li.Attributes["class"] = "nonext";
            li.InnerHtml = getLiBody(HasNext, ControlID);
            li.ID = ID;

            if (HasChildNodes)
            {
                HtmlGenericControl ul = new HtmlGenericControl("ul");
                if ((Type == NetTreeViewType.ElasticSimple || Type == NetTreeViewType.ElasticCheckBoxes) && Parent != null)
                    ul.Attributes["style"] = "display:none;";
                li.Controls.Add(ul);

                for (int i = 0; i < ChildNodes.Count; i++)
                {
                    bool next = true;
                    if (i + 1 >= ChildNodes.Count)
                        next = false;
                    ul.Controls.Add(ChildNodes[i].getNode(ControlID, Type, next));
                }
            }
            return li;
        }
        public Control getNode(string ControlID, NetTreeViewType type)
        {
            Type = type;
            return getNode(ControlID, false);
        }
        public Control getNode(string ControlID, NetTreeViewType type, bool HasNext)
        {
            Type = type;
            return getNode(ControlID, HasNext);
        }

        public string getCheckBox()
        {
            string check = "";
            if (Type == NetTreeViewType.ElasticCheckBoxes || Type == NetTreeViewType.StaticCheckBoxes)
            {
                check = "<input onclick=\"xTreeViewCheck('chk" + ID + "')\" id=\"chk" + ID + "\" name=\"chk" + ID + "\" class=\"checkbox\" type=\"checkbox\"";
                if (Checked)
                {
                    check += " checked=\"checked\"";
                    if (Parent != null && Parent.Checked)
                        check += " disabled=\"disabled\"";
                }
                check += " />";
            }
            return check;
        }

        public string getLiBody(bool HasNext, string ControlID)
        {
            string innerHtml = "";
            switch (Type)
            {
                case NetTreeViewType.StaticSimple:
                    innerHtml = getStaticSimple(HasNext, ControlID);
                    break;
                case NetTreeViewType.StaticCheckBoxes:
                    innerHtml = getStaticCheckBoxes(HasNext, ControlID);
                    break;
                case NetTreeViewType.ElasticSimple:
                    innerHtml = getElasticSimple(HasNext, ControlID);
                    break;
                case NetTreeViewType.ElasticCheckBoxes:
                    innerHtml = getElasticCheckBoxes(HasNext, ControlID);
                    break;
            }
            return innerHtml;
        }

        public string getStaticSimple(bool HasNext, string ControlID)
        {
            string img;
            string icon;
            string libody = Text;

            if (Grant)
                icon = "<img src=\"" + Style.Folder + "\" />";
             else
                    icon = "<img src=\"" + Style.FolderNegate + "\" />";
            if (Parent == null)
            {
                img = "<img src=\"" + Style.Empty + "\" />";
                if (Grant)
                    icon = "<img src=\"" + Style.Root + "\" />";
                else
                    icon = "<img src=\"" + Style.FolderNegate + "\" />";
            }
            else
                if (HasNext)
                {
                    img = "<img src=\"" + Style.Join + "\" />";
                    if (Grant)
                        icon = "<img src=\"" + Style.FolderOpen + "\" />";
                    else
                        icon = "<img src=\"" + Style.FolderNegate + "\" />";
                }
                else
                {
                    img = "<img src=\"" + Style.JoinEnd + "\" />";
                }

            if (Grant)
                libody = "<a id=\"" + ID + "_link\" href=\"javascript: xTreeViewSelectNode('" + Value + "','" + ID + "','" + Text + "','" + ControlID + "')\">" + libody + "</a>";
            else
                libody = "<a id=\"" + ID + "_link\" href=\"javascript: alert('Non possiedi i diritti su questo oggetto')\">" + libody + "</a>";
            
            return img + icon + libody;
        }

        public string getStaticCheckBoxes(bool HasNext, string ControlID)
        {
            string img;
            string icon;
            string libody = Text;

            if (Parent == null)
                img = "<img src=\"" + Style.Empty + "\" />";
            else
                if (HasNext)
                    img = "<img src=\"" + Style.Join + "\" />";
                else
                    img = "<img src=\"" + Style.JoinEnd + "\" />";
            icon = getCheckBox();
            return img + icon + libody;
        }

        public string getElasticCheckBoxes(bool HasNext, string ControlID)
        {
            string img;
            string icon;
            string libody = Text;

            if (Parent == null)
                img = "<img src=\"" + Style.Empty + "\" />";
            else
                if (HasNext)
                    if (HasChildNodes)
                    {
                        img = "<a href=\"javascript: xTreeViewOpenColapse('" + ID + "','" + Style.Plus + "','" + Style.Minus + "')\">";
                        img += "<img id=\"" + ID + "_OpenColapse\" src=\"" + Style.Plus + "\" />";
                        img += "</a>";
                    }

                    else
                    {
                        img = "<img id=\"" + ID + "_pl\" src=\"" + Style.Join + "\" />";
                    }
                else
                    if (HasChildNodes)
                    {
                        img = "<a href=\"javascript: xTreeViewOpenColapse('" + ID + "','" + Style.PlusEnd + "','" + Style.MinusEnd + "')\">";
                        img += "<img id=\"" + ID + "_OpenColapse\" src=\"" + Style.PlusEnd + "\" />";
                        img += "</a>";
                        icon = "<img src=\"" + Style.Folder + "\" />";
                    }

                    else
                        img = "<img src=\"" + Style.JoinEnd + "\" />";

            icon = getCheckBox();
            return img + icon + libody;
        }

        public string getElasticSimple(bool HasNext, string ControlID)
        {
            string img;
            string icon;
            string libody = Text;

            icon = "<img src=\"" + Style.Folder + "\" />";

            if (Parent == null)
            {
                img = "<img src=\"" + Style.Empty + "\" />";
                icon = "<img src=\"" + Style.Root + "\" />";

            }
            else
                if (HasNext)
                {
                    if (HasChildNodes)
                    {
                        img = "<a href=\"javascript: xTreeViewOpenColapse('" + ID + "','" + Style.Plus + "','" + Style.Minus + "')\">";
                        img += "<img id=\"" + ID + "_OpenColapse\" src=\"" + Style.Plus + "\" />";
                        img += "</a>";
                        icon = "<img src=\"" + Style.Folder + "\" />";
                    }

                    else
                    {
                        img = "<img id=\"" + ID + "_pl\" src=\"" + Style.Join + "\" />";
                        icon = "<img src=\"" + Style.Folder + "\" />";
                    }
                }
                else
                {
                    if (HasChildNodes)
                    {
                        img = "<a href=\"javascript: xTreeViewOpenColapse('" + ID + "','" + Style.PlusEnd + "','" + Style.MinusEnd + "')\">";
                        img += "<img id=\"" + ID + "_OpenColapse\" src=\"" + Style.PlusEnd + "\" />";
                        img += "</a>";
                        icon = "<img src=\"" + Style.Folder + "\" />";
                    }

                    else
                    {
                        img = "<img src=\"" + Style.JoinEnd + "\" />";
                        icon = "<img src=\"" + Style.Folder + "\" />";
                    }
                }

            libody = "<a id=\"" + ID + "_link\" href=\"javascript: xTreeViewSelectNode('" + Value + "','" + ID + "','" + Text + "','" + ControlID + "')\">" + libody + "</a>";

            return img + icon + libody;
        }

    }
}