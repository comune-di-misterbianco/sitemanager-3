using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for xNodeStyle
/// </summary>
namespace NetUtility.NetTreeViewControl
{
    public class NetTreeNodeStyle
    {
        public string Plus;
        public string PlusEnd;
        public string Minus;
        public string MinusEnd;
        public string Line;
        public string Empty;
        public string Join;
        public string JoinEnd;
        public string Root;

        public string Folder;
        public string FolderOpen;

        public string FolderNegate;

        private string _Offset = "";

        public NetTreeNodeStyle(string offset)
        {
            //
            // TODO: Add constructor logic here
            //
            _Offset = offset;
            Plus = _Offset + "xTreeView/plus.gif";
            PlusEnd = _Offset + "xTreeView/plusbottom.gif";
            Minus = _Offset + "xTreeView/minus.gif";
            MinusEnd = _Offset + "xTreeView/minusbottom.gif";
            Line = _Offset + "xTreeView/line.gif";
            Empty = _Offset + "xTreeView/empty.gif";
            Join = _Offset + "xTreeView/join.gif";
            JoinEnd = _Offset + "xTreeView/joinbottom.gif";
            Root = _Offset + "xTreeView/base.gif";

            Folder = _Offset + "xTreeView/folder.gif";

            FolderNegate = _Offset + "xTreeView/folderred.gif";
            FolderOpen = _Offset + "xTreeView/folderopen.gif";

        }
        public NetTreeNodeStyle()
            : this("")
        {
        }
    }
}