using System;
using System.Data;
using System.Collections;

/// <summary>
/// Summary description for NetFieldsCollection
/// </summary>
namespace NetUtility
{

    public class GenericCollection : IEnumerable
    {
        private NetCollection _BaseCollection;
        protected NetCollection BaseCollection
        {
            get
            {
                return _BaseCollection;
            }

        }
        public int Count { get { return BaseCollection.Count; } }
        public GenericCollection()
        {
            _BaseCollection = new NetCollection();
        }

        public void Add(object obj,string key)
        {
            BaseCollection.Add(key, obj);
        }

        public void Remove(string key)
        {
            BaseCollection.Remove(key);
        }
        public void Remove(int index)
        {
            BaseCollection.Remove(index);
        }
        public void Clear()
        {
            BaseCollection.Clear();
        }

        public object GetElement(int i)
        {
            return BaseCollection[BaseCollection.Keys[i]];
        }
        public object GetElement(string key)
        {
            return BaseCollection[key];
        }

        public bool Contains(string key)
        {
            return BaseCollection[key] != null;
        }
        #region Enumerator

        public virtual IEnumerator GetEnumerator()
        {
            return new CollectionEnumerator(this);
        }

        private class CollectionEnumerator : IEnumerator
        {
            private int CurentPos = -1;
            private GenericCollection Collection;
            public CollectionEnumerator(GenericCollection coll)
            {
                Collection = coll;
            }
            public object Current
            {
                get
                {
                    return Collection.GetElement(CurentPos);
                }
            }
            public bool MoveNext()
            {
                if (CurentPos < Collection.Count - 1)
                {
                    CurentPos++;
                    return true;
                }
                else
                    return false;
            }
            public void Reset()
            {
                CurentPos = -1;
            }
        }

        #endregion
    }
}