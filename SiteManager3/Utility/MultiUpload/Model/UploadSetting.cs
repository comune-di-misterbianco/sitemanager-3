﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetUpload.Model
{
    public class UploadSetting
    {
        /// <summary>
        /// Permette la definizione dei parametri necessari per l'upload multiplo di file da passare all'handler 
        /// </summary>
        /// <param name="destinationPath">Percorso relativo della cartella dove trasferire i file (esempio /repository/tmp/ )</param>
        /// <param name="fileSizeLimit">Dimensione massima per file espressa in byte (esempio 1)</param>
        /// <param name="allowedExtensions">Estensioni accettate (esempio) </param>
        public UploadSetting(string destinationPath, double fileSizeLimit, string allowedExtensions) 
        {
            DestinationPath = destinationPath;
            FileSizeLimit = fileSizeLimit;
            AllowedExtensions = allowedExtensions;
        }

        public string DestinationPath { get; protected set; }

        public double FileSizeLimit { get; protected set; }
        
        public string AllowedExtensions { get; protected set; }
    }
}
