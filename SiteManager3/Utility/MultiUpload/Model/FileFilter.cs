﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetUpload.Model
{
    public class FileFilter
    {
        public FileFilter(string title, string extensions) 
        {
            Title = title;
            Extensions = extensions;
        }

        public string Title { get; protected set; }
        public string Extensions { get; protected set; }
    }
}
