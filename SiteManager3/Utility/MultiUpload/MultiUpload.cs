﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetUpload.Model;

namespace NetUpload
{
    public class MultiUpload : WebControl
    {
        /// <summary>
        /// Wrapper del componente jquery PlUpload per il trasferimento multiplo di file
        /// </summary>
        /// <param name="id">Identificativo</param>
        /// <param name="scriptFolderPath">Percorso relativo della cartella contenente gli script del componente senza slash finale; es: /scripts/shared/plupload/js</param>                
        /// <param name="cmsFolderID">Id della cartella corrente</param>
        /// <param name="acceptedFiles">Lista di oggetti FileFilter conteneti le estensioni dei file accettati</param>
        /// <param name="maxFileSize">Dimensione massima per file in mb: esempio 10</param>
        /// <param name="uploadTmpFolder">Percorso relativo della cartella temporanea dove il componente salvi i file trasferiti</param>
        public MultiUpload(string id, string scriptFolderPath, int cmsFolderID, List<FileFilter> acceptedFiles, int maxFileSize, string uploadTmpFolder, CtrlRuntimes runtimes)
            : base(HtmlTextWriterTag.Div)
        {
            CtrlId = id; // rappresenta l'id che verrà stampato nel div                    
            ScriptFolderPath = scriptFolderPath;     
            CmsFolderID = cmsFolderID;         
            AcceptedFiles = acceptedFiles;
            MaxFileSize = maxFileSize;
            UploadTmpFolder = uploadTmpFolder;
            Runtime = runtimes;                            
        }        
      
        protected int CmsFolderID;

        protected string CtrlId;
        protected string ScriptFolderPath;

        private string _AcceptedExtensions;
        protected string AcceptedExtensions 
        {
            get 
            {
                if (_AcceptedExtensions == null)
                    _AcceptedExtensions = GetExtensionFiltersSerialized();
                return _AcceptedExtensions;
            }
        }

        protected CtrlRuntimes Runtime;               

        /// <summary>
        /// 
        /// </summary>
        protected List<FileFilter> AcceptedFiles
        {
            get;
            set;
        }

        /// <summary>
        /// Dimensione massima per file accettata espressa in byte
        /// </summary>
        protected double MaxFileSizeAllowed 
        {
            get {
                return ConvertMBinByte(MaxFileSize);
            }
        } 
       
        /// <summary>
        /// cartella temporanea dove il componente trasferisce il file
        /// </summary>
        protected string UploadTmpFolder;
        
        /// <summary>
        /// Dimensione massima in MB accettata per file
        /// </summary>
        public int MaxFileSize
        {
            get { return _maxFileSize; }
            protected set { _maxFileSize = value; }
        }
        private int _maxFileSize = 2;
        
        /// <summary>
        /// Dimensione del pacchetto in kb di default è settatto a 100KB
        /// </summary>
        public string ChunkSize
        {
            get { return _chunkSize; }
            set { _chunkSize = value; }
        }
        private string _chunkSize = "100kb";                  

        /// <summary>
        /// Se settato a true previene l'invio di file con nome e dimensione uguali. Di defautl è settato false.
        /// </summary>
        public bool PreventDuplicates
        {
            get { return preventDuplicates; }
            set { preventDuplicates = value; }
        }
        private bool preventDuplicates = false;

        /// <summary>
        /// Se settato a true abilita la modalità dragdrop se supportata dal browser web. Di default è è settato true.
        /// </summary>
        public bool Dragdrop
        {
            get { return _dragdrop; }
            set { _dragdrop = value; }
        }
        private bool _dragdrop = true;

        /// <summary>
        /// Se settato a false disabilita la selezione multipla dei file . Di default è è settato true.
        /// </summary>
        public bool MultiSelection
        {
            get { return _multiSelection; }
            set { _multiSelection = value; }
        }
        private bool _multiSelection = true;

        private Button _Save;
        protected Button Save
        {
            get {

                if (_Save == null)
                {
                    _Save = new Button();
                    _Save.Text = "Salva e completa";
                    _Save.CssClass = "btnHide";
                    _Save.ID = "save_" + this.CtrlId;
                    _Save.Click += SaveClickHandler;
                }
                return _Save;
            }
            set { _Save = value; }

        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            // Imposto i settagi di upload da passare all'handler           
           Model.UploadSetting uploadSetting = new Model.UploadSetting(
              UploadTmpFolder,
              MaxFileSizeAllowed,
              AcceptedExtensions
             );

            HttpContext.Current.Session["UploadSettings"] = uploadSetting;


            base.OnPreRender(e);

            this.ID = CtrlId;

            #region Script&Css
            string headCssImport = @"<link rel=""stylesheet"" type=""text/css"" href=""" + ScriptFolderPath + @"/jquery.plupload.queue/css/jquery.plupload.queue.css"" /><!-- jquery.plupload.css -->";
            headCssImport += @"<link rel=""stylesheet"" type=""text/css"" href=""" + ScriptFolderPath + @"/custom/fileupload.css"" /><!-- fileupload.css -->";

            base.Page.Header.Controls.Add(new LiteralControl(headCssImport));

            string headJsImport = @"<script type=""text/javascript"" src=""" + ScriptFolderPath + @"/plupload.full.min.js""></script>";
            headJsImport += @"<script type=""text/javascript"" src=""" + ScriptFolderPath + @"/jquery.plupload.queue/jquery.plupload.queue.js""></script>";

            // localizzazione
            // prevedere un sistema per il settaggio della lingua
            headJsImport += @"<script type=""text/javascript"" src=""" + ScriptFolderPath + @"/i18n/it.js""></script>";

            base.Page.Header.Controls.Add(new LiteralControl(headJsImport));

            this.Controls.Add(new LiteralControl(this.jsCode()));
            #endregion

            // box per esposizione msg errori
            #region Console Errori
            WebControl console = new WebControl(HtmlTextWriterTag.Div);
            console.ID = "console_" + this.CtrlId;
            console.CssClass = "consoleArea hide";

            WebControl console_ul = new WebControl(HtmlTextWriterTag.Ul);
            console.Controls.Add(console_ul);
            this.Controls.Add(console);
            #endregion

            WebControl uploadedfiles = new WebControl(HtmlTextWriterTag.Div);
            uploadedfiles.ID = "files-selected-container_" + this.CtrlId;
            uploadedfiles.CssClass = "files-selected-container hide";
            uploadedfiles.Controls.Add(new LiteralControl("<ul class=\"files-selected-list\"></ul>"));
            this.Controls.Add(uploadedfiles);

            #region DropArea
            WebControl DropArea = new WebControl(HtmlTextWriterTag.Div);
            DropArea.ID = "DropArea_" + this.CtrlId;
            DropArea.CssClass = "dropArea";

            WebControl plupload = new WebControl(HtmlTextWriterTag.Div);
            plupload.ID = "plupload_" + this.CtrlId;
            plupload.Controls.Add(new LiteralControl("<p>You browser doesn't have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.</p>"));
            DropArea.Controls.Add(plupload);

            WebControl uploadUI = new WebControl(HtmlTextWriterTag.Div);
            uploadUI.CssClass = "uploadui";
            uploadUI.Controls.Add(new LiteralControl("<h3 class=\"upload-instruction-msg\">Trascina qui tutti i file da trasferire</h3>"));
            uploadUI.Controls.Add(new LiteralControl("<p class=\"upload-instruction-msg2\">oppure</p>"));

            #region Buttons
            WebControl buttons = new WebControl(HtmlTextWriterTag.Div);
            buttons.CssClass = "btns";
            buttons.ID = "container_" + this.CtrlId;

            Button picker = new Button();
            picker.Text = "Seleziona i file";
            //picker.Attributes["type"] = "button";
            picker.ID = "pickfiles_" + this.CtrlId;
            buttons.Controls.Add(picker);

            //Button submit = new Button();
            //submit.Text = "Trasferisci";
            //submit.Attributes["type"] = "button";
            //submit.ID = "uploadfiles";
            //submit.Attributes["disabled"] = "disabled";
            //buttons.Controls.Add(submit);           

            uploadUI.Controls.Add(buttons);


            HiddenField fileUploaded = new HiddenField();
            fileUploaded.ID = "fileUploaded_" + this.CtrlId;
            DropArea.Controls.Add(fileUploaded);

            WebControl buttons_confirm = new WebControl(HtmlTextWriterTag.Div);
            buttons_confirm.CssClass = "btns_confirm";

            //Button save = new Button();
            //Save.Text = "Salva e completa";
            //Save.ID = "save_" + this.CtrlId;
            //Save.CssClass = "btnHide";
            //Save.Click += SaveClickHandler;
            buttons_confirm.Controls.Add(Save);

            uploadUI.Controls.Add(buttons_confirm);
            #endregion

            DropArea.Controls.Add(uploadUI);

            this.Controls.Add(DropArea);
            #endregion

            HiddenField currentFolder = new HiddenField();
            currentFolder.ID = "currentFolder_" + this.CtrlId;
            currentFolder.Value = this.CmsFolderID.ToString();
            this.Controls.Add(currentFolder);
        }

        //protected override void OnPreRender(EventArgs e)
        //{
        
        //}

        private EventHandler _saveClickHandler;
        public EventHandler SaveClickHandler
        {
            get { return _saveClickHandler; }
            set { _saveClickHandler = value; }

        }

        private double ConvertMBinByte(int value)
        { 
            int onemegabyte = 1048576;
            return value * onemegabyte;
        }

        private string GetFileFilter()
        {
           string strFilter = "";             

           foreach(FileFilter filter in AcceptedFiles)
           {
               strFilter += "{ title: '" + filter.Title + "', extensions: '" + filter.Extensions + "' }" + ((filter != AcceptedFiles.Last()) ? "," : "");
           }

           return strFilter;
        }

        private string GetExtensionFiltersSerialized()
        {
            string strExtensionsSerialized = "";

            foreach(FileFilter filter in AcceptedFiles)
            {
                strExtensionsSerialized += filter.Extensions + ",";
            }
            return strExtensionsSerialized;
        }

        protected string jsCode()
        {
            string script = @"
<script type=""text/javascript"">
$(document).ready(function () {
    var files_selected = new Array();
    var uploader = new plupload.Uploader({
        runtimes: '" + GetRuntimes(this.Runtime)+@"',
        browse_button: 'pickfiles_"+ this.CtrlId+@"', 
        container: document.getElementById('DropArea_" + this.CtrlId + @"'), // DOM element of plupload
        url: '/uploader/plupload_handler.ashx',
        max_file_size: '" + MaxFileSize+@"mb',
        //prevent_duplicates: "+PreventDuplicates.ToString().ToLower()+@",
        chunk_size: '"+ChunkSize+@"',
        filters: {            
            mime_types: [           
            "+GetFileFilter()+@"
            ]
        },
        dragdrop: " + Dragdrop.ToString().ToLower() + @",
        //multipart: false,
        drop_element: 'DropArea_" + this.CtrlId + @"',       
        multi_selection : " + MultiSelection.ToString().ToLower() + @",
        flash_swf_url: '" +ScriptFolderPath+@"/plupload.flash.swf', // Flash settings
        silverlight_xap_url: '"+ScriptFolderPath+@"/plupload.silverlight.xap', // Silverlight settings

        init: {
            PostInit: function () {
              
                $('#plupload_" + this.CtrlId + @"').empty();
               
                plupload.addI18n({
                    'File extension error.': 'Il file selezionato non è supportato.',
                    'File size error.': 'La dimensione del file supera i limiti consentiti.'
                });

                //$('#uploadfiles_" + this.CtrlId + @"').click(function () {
                //    uploader.start();
                //    return false;
                //});

            },

            BeforeUpload: function(up,file)
            {
                //var file = files[0];
                console.log(file);       
                up.settings.multipart_params = {
                   filenameRenamed: file.name
                };                                   
            },
            FilesAdded: function (up, files) {
                plupload.each(files, function (file)
                {
                    var results;
                    var exist = true;
                    var app = '';
                    var count = 0;
                    var ext = file.name.substr(file.name.lastIndexOf('.'));
                    var filename = file.name.substr(0,file.name.lastIndexOf('.'));

                    while(exist)
                    {
                        var params = {
                            ""folderId"": $('#currentFolder_" + this.CtrlId + @"').val(),
                            ""filename"": filename + app + ext
                        };

                        $.ajax({
                            url: '/api/UploadManager/checkfile/',
                            type: 'POST',
                            data: JSON.stringify(params),
                            contentType: 'application/json',
                            dataType: 'json',
                            async: false,
                            success: function(response){                       
                                results = response;
                            }
                        });
                        
                        if (!results.exist) 
                        {
                            exist = false;
                            file.name= filename + app + ext;
                        }
                        else{
                            count++;
                            app='_'+count;
                        }
                        
                    }
                    /*
                    var params = {
                        ""folderId"": $('#currentFolder_" + this.CtrlId + @"').val(),
                        ""filename"": file.name
                    };

                    $.ajax({
                        url: '/api/UploadManager/checkfile/',
                        type: 'POST',
                        data: JSON.stringify(params),
                        contentType: 'application/json',
                        dataType: 'json',
                        async: false,
                        success: function(response){                       
                            results = response;
                        }
                    });*/

                    if (app != '') {
                         var errorMsg = '<li> Il file ' + file.name.replace(app,'') + ' è stato rinominato in '+ file.name +'</li>';
                        if ($('#console_" + this.CtrlId + @"').hasClass('hide'))
                            $('#console_" + this.CtrlId + @"').removeClass('hide');
                        $('#console_" + this.CtrlId + @" ul').append(errorMsg);

                    }



                        if ($.inArray(file.name, files_selected) == -1) {
                            var item = '<li class=""file-selected"" id=""' + file.id + '"">'
                                     + '<div class=""file"">'
                                     + '    <div class=""file-container"">'
                                     + '        <div class=""status hide""></div>'
                                     + '        <div class=""remove hide""><a href=""#""><span>X</span></a></div>'
                                     + '        <div class=""document""><img class=""icon"" src=""/repository/plupload/document.png"" /></div>'
                                     + '        <div class=""filename""><div>' + file.name + ' [' + plupload.formatSize(file.size) +']</div></div>'                                     
                                     + '        <div class=""progressbar hide""><div></div></div>'
                                     + '    </div>'
                                     + '</div>'
                                     + '</li>';

                            if ($('#files-selected-container_" + this.CtrlId + @"').hasClass('hide'))
                                $('#files-selected-container_" + this.CtrlId + @"').removeClass('hide');

                            $('.files-selected-list').append(item);
                            files_selected.push(file.name);                       
                            setTimeout(up.start(), 100);
                        }
                        else {
                            // add msg error // documento già selezionato
                            up.removeFile(file);
                        }
                    /*}
                    else
                    {
                        var errorMsg = '<li>' + results.message + '</li>';
                        if ($('#console_" + this.CtrlId + @"').hasClass('hide'))
                            $('#console_" + this.CtrlId + @"').removeClass('hide');
                        $('#console_" + this.CtrlId + @" ul').append(errorMsg);
                    }*/
                });
                if (files_selected.length > 0) {
                    //$('#uploadfiles').prop('disabled', false);                    
                    if ($('#save_" + this.CtrlId + @"').hasClass('btnHide')) {
                        $('#save_" + this.CtrlId + @"').removeClass('btnHide').fadeIn(600);
                        $('#save_" + this.CtrlId + @"').addClass('btnGreen');
                    }
                    $('#fileUploaded_" + this.CtrlId + @"').val(files_selected);
                    
                }
            },
            UploadProgress: function (up, file) {

                if ($('#' + file.id + ' .progressbar').hasClass('hide'))
                $('#' + file.id + ' .progressbar').removeClass('hide').fadeIn(600);

                var percentLoadFile = file.percent;

                if (percentLoadFile < 100 && percentLoadFile >= 1) 
                    $('#' + file.id + ' .progressbar div').css('width', percentLoadFile + '%');                
                else                
                    $('#' + file.id + ' .progressbar').fadeOut(600);                
            },
            FileUploaded: function (up, file, response) {
                try{                    
                    if (response.status == '200') {                        
                        $('#' + file.id + ' .status').addClass('done');
                        $('#' + file.id + ' .status').css('display', 'block').append('<span>OK</span>');
                    }
                    else {                        
                        $('#' + file.id + ' .status').addClass('failed');
                        $('#' + file.id + ' .status').css('display', 'block').append('<span>Error!</span>');
                    }
                }
                catch (error) {
                    if ($('#console_" + this.CtrlId + @"').hasClass('hide'))
                        $('#console_" + this.CtrlId + @"').removeClass('hide');
                    $('#console_" + this.CtrlId + @" ul').append(""<li>""+errorMsg+""</li>"");
                    
                }
            },
            Error: function (up, err) {                
                var errorMsg = '<li>Attenzione: ' + err.message + ' (cod.err ' + err.code + ') </li>';
                if ($('#console_" + this.CtrlId + @"').hasClass('hide'))
                    $('#console_" + this.CtrlId + @"').removeClass('hide');
                $('#console_" + this.CtrlId + @" ul').append(errorMsg);
            }
        }
    });   

    uploader.init();   

    var dropArea = $('#DropArea_" + this.CtrlId + @"');
    dropArea.on('dragenter', function (event) {               
        dropArea.addClass('dropzone');
        event.stopPropagation();
        event.preventDefault();
        return false;
    });
    dropArea.on('dragleave', function (event) {        
        dropArea.removeClass('dropzone');
        event.stopPropagation();
        event.preventDefault();
        return false;
    });
    dropArea.on('drop', function (event) {
        dropArea.removeClass('dropzone');
        event.stopPropagation();
        event.preventDefault();
        return false;
    });
});
</script>
";
         return script;

        }

        public string GetRuntimes(CtrlRuntimes runtimes)
        {
            string strRuntime = "";
            switch (runtimes)
            {
                case CtrlRuntimes.All:
                    strRuntime = "html5,flash,silverlight,html4";
                    break;
                case CtrlRuntimes.Html5FlashSilverlight:
                    strRuntime = "html5,flash,silverlight";
                    break;
                case CtrlRuntimes.Html5Flash:
                    strRuntime = "html5,flash";
                    break;
                case CtrlRuntimes.Html5Silverlight:
                    strRuntime = "html5,silverlight";
                    break;
                case CtrlRuntimes.Html5:
                    strRuntime = "html5";
                    break;
                case CtrlRuntimes.Flash:
                    strRuntime = "flash";
                    break;
                case CtrlRuntimes.FlashSilverlight:
                    strRuntime = "flash,silverlight";
                    break;
                case CtrlRuntimes.Silverlight:
                    strRuntime = "silverlight";
                    break;
                case CtrlRuntimes.Html4:
                    strRuntime = "html4";
                    break;
                default:
                    strRuntime = "html5,flash,silverlight,html4";
                    break;
            }
            return strRuntime;

        }

        public enum CtrlRuntimes
        {
            All,
            Html5FlashSilverlight,
            Html5Flash,
            Html5Silverlight,
            Html5,
            Flash,
            FlashSilverlight,
            Silverlight,
            Html4
        }
    }
}
