﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using MigraDoc.DocumentObjectModel.Tables;
using Report.Components;

namespace Report.TemplateReader
{
    class TableReader : BlockReader
    {
        protected TableBlock table;

        protected override ReportBlock ReadBlockImpl(XPathNavigator n, TemplateReader reader)
        {
            if (n.Name == "table")
            {
                table = new TableBlock();

                XPathNodeIterator it = n.Select("preHeaderRows/*");
                table.PreHeaderRows = readRows(it);

                it = n.Select("columns/*");
                int index = 0;
                while (it.MoveNext())
                {
                    XPathNavigator colNavi = it.Current;
                    Column col = table.Columns.AddColumn();
                    col.Tag = colNavi.SelectSingleNode("binding").Value.Trim();
                    col.Comment = colNavi.SelectSingleNode("header").Value.Trim();

                    if (colNavi.SelectSingleNode("@width") != null)
                        table.FixedWidths[index++] = colNavi.SelectSingleNode("@width").ValueAsDouble;
                }
                table.DataSource = n.SelectSingleNode("data-source").Value.Trim();
                table.HeaderStyle = n.SelectSingleNode("columns/@headerStyle") != null ?
                    n.SelectSingleNode("columns/@headerStyle").Value.Trim() : null;

                it = n.Select("footerRows/*");
                table.FooterRows = readRows(it);

                return table;
            }
            else
                return null;
        }

        private Rows readRows(XPathNodeIterator rowIt)
        {
            Rows rows = new Rows();

            while (rowIt.MoveNext())
            {
                XPathNavigator rowNavi = rowIt.Current;
                Row r = new Row();
                int idx = 0;

                XPathNodeIterator cellIt = rowNavi.Select("cell");
                while (cellIt.MoveNext())
                {
                    XPathNavigator cellNavi = cellIt.Current;
                    Cell c = new Cell();

                    if (cellNavi.SelectSingleNode("@mergeRight") != null)
                    {
                        string merge = cellNavi.SelectSingleNode("@mergeRight").Value;
                        if (merge != "*")
                            c.MergeRight = int.Parse(merge);
                        else
                            c.MergeRight = -1;
                    }
                    if (cellNavi.SelectSingleNode("@mergeDown") != null)
                        c.MergeDown = cellNavi.SelectSingleNode("@mergeDown").ValueAsInt;

                    c.Tag = cellNavi.Value.Trim();
                    r.Cells.Add(c);
                    idx++;
                }

                rows.Add(r);
            }

            return rows;
        }
    }
}
