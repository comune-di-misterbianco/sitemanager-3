﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.DocumentObjectModel;
using Report.Components;
using System.Drawing;
using System.Data;

namespace Report.TemplateReader
{
    class GenericTableReader : BlockReader
    {
        protected GenericTableBlock table;

        protected override ReportBlock ReadBlockImpl(XPathNavigator n, TemplateReader reader)
        {
            if (n.Name == "gtable")
            {
                table = new GenericTableBlock();

                if (n.SelectSingleNode("@align") != null)
                    table.Table.Format.Alignment = GetAligment(n.SelectSingleNode("@align").Value);

                if (n.SelectSingleNode("data-source") != null)
                    table.DataSource = n.SelectSingleNode("data-source").Value;

                XPathNodeIterator it = n.Select("columns/*");
                int index = 0;
                while (it.MoveNext())
                {
                    XPathNavigator colNavi = it.Current;
                    Column col = table.Columns.AddColumn();

                    if (colNavi.SelectSingleNode("@align") != null)
                        col.Format.Alignment = GetAligment(colNavi.SelectSingleNode("@align").Value);

                    if (colNavi.SelectSingleNode("@width") != null)
                        if (colNavi.SelectSingleNode("@size") != null)
                            if (colNavi.SelectSingleNode("@size").Value.Trim() == "auto")
                                table.FixedWidths[index++] = colNavi.SelectSingleNode("@width").ValueAsDouble;
                            else
                                col.Width = colNavi.SelectSingleNode("@width").Value; 
                        else
                            table.FixedWidths[index++] = colNavi.SelectSingleNode("@width").ValueAsDouble;
                        
                }
                
                table.HeaderStyle = n.SelectSingleNode("columns/@headerStyle") != null ?
                    n.SelectSingleNode("columns/@headerStyle").Value.Trim() : null;

                it = n.Select("rows/*");
                ReadTableRows(it);

                return table;
            }
            else
                return null;
        }

        private void ReadTableRows(XPathNodeIterator rowIt)
        {
            Rows rows = new Rows();
            //int numberOfRows = -1;

            ////Se è stato indicato un data-source, questo attributo indica il numero di righe della tabella che 
            ////manterranno tutte la stessa formattazione di quella specificata nel template
            //if (rowIt.Current.SelectSingleNode("rows/@repeat") != null)
            //    numberOfRows = rowIt.Current.SelectSingleNode("rows/@repeat").ValueAsInt;

            while (rowIt.MoveNext())
            {
                XPathNavigator rowNavi = rowIt.Current;
                Row r = new Row();
                bool hasBordersVisibility = false;
                int idx = 0;

                if (rowNavi.SelectSingleNode("@borders") != null)
                {
                    r.Borders.Visible = rowNavi.SelectSingleNode("@borders") != null ?
                    rowNavi.SelectSingleNode("@borders").ValueAsBoolean : true;
                    hasBordersVisibility = true;
                }

                XPathNodeIterator cellIt = rowNavi.Select("cell");
                while (cellIt.MoveNext())
                {
                    XPathNavigator cellNavi = cellIt.Current;
                    Cell c = new Cell();

                    if (cellNavi.SelectSingleNode("@mergeRight") != null)
                    {
                        string merge = cellNavi.SelectSingleNode("@mergeRight").Value;
                        if (merge != "*")
                            c.MergeRight = int.Parse(merge);
                        else
                            c.MergeRight = -1;
                    }
                    if (cellNavi.SelectSingleNode("@mergeDown") != null)
                        c.MergeDown = cellNavi.SelectSingleNode("@mergeDown").ValueAsInt;

                    //setta la visibilità della cella se non è stata resa invisibile l'intera riga
                    if(!hasBordersVisibility)
                        c.Borders.Visible = cellNavi.SelectSingleNode("@borders") != null  ?
                        cellNavi.SelectSingleNode("@borders").ValueAsBoolean : true;

                    if (cellNavi.SelectSingleNode("@shading") != null)
                        if (cellNavi.SelectSingleNode("@color") != null)
                        {
                            switch (cellNavi.SelectSingleNode("@color").Value)
                            {
                                case "gray":
                                    c.Shading.Color = Colors.LightGray;
                                    break;
                                case "lgray":
                                    c.Shading.Color = Colors.WhiteSmoke;
                                    break;
                            }
                        }
                        else
                            c.Shading.Color = Colors.LightGray;

                    c.Tag = cellNavi.InnerXml.Trim();
                    r.Cells.Add(c);
                    idx++;
                }

                ////Se ho indicato un data-source, aggiungo un numero di righe pari al numero indicato nell'attributo "repeat"
                ////tutte con la stessa formattazione ed esco dal ciclo della lettura delle righe per evitare casi di errore
                //if (table.DataSource != null)
                //{
                //    for (; numberOfRows > 0; numberOfRows--)
                //        rows.Add(r.Clone());
                //    break;
                //}
                //else
                    rows.Add(r);
            }

            table.Rows = rows;
        }
    }
}
