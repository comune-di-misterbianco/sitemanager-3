﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.Components;
using System.Xml.XPath;

namespace Report.TemplateReader
{
    class IteratorReader : BlockReader
    {
        protected override ReportBlock ReadBlockImpl(XPathNavigator n, TemplateReader reader)
        {
            if (n.Name == "iterator")
            {
                IteratorBlock it = new IteratorBlock();
                it.DataSource = n.SelectSingleNode("@datasource").Value.Trim();

                reader.processSectionsIterator(n.SelectChildren(XPathNodeType.Element), it);
                return it;
            }
            else
                return null;
        }
    }
}
