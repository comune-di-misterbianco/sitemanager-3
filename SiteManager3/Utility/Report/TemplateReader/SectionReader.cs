﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using Report.Components;

namespace Report.TemplateReader
{
    class SectionReader : BlockReader
    {
        protected override ReportBlock ReadBlockImpl(XPathNavigator n, TemplateReader reader)
        {
            ReportBlock block = null;

            switch (n.Name)
            {
                case "section":
                    block = new SectionBlock();
                    break;
                case "head":
                    block = new HeadFootContentBlock(HeadFootContentBlock.Type.Header);
                    break;
                case "foot":
                    block = new HeadFootContentBlock(HeadFootContentBlock.Type.Footer);
                    break;
                case "content":
                    block = new HeadFootContentBlock(HeadFootContentBlock.Type.Content);
                    break;
                default:
                    return null;
            }
            reader.processSectionsIterator(n.SelectChildren(XPathNodeType.Element), block);

            return block;
        }
    }
}
