﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using System.IO;
using Report.Components;
using Report.Style;

namespace Report.TemplateReader
{
    public class TemplateReader
    {
        private List<BlockReader> readersStack;
          
        public TemplateReader()
        {
            readersStack = new List<BlockReader>();
            readersStack.Add(new SectionReader());
            readersStack.Add(new ParagraphReader());
            readersStack.Add(new ImageReader());
            readersStack.Add(new StreamImageReader());
            readersStack.Add(new TableReader());
            readersStack.Add(new DynamicTableReader());
            readersStack.Add(new IteratorReader());
            readersStack.Add(new GenericTableReader());
        }

        public List<BlockReader> ReadersStack
        {
            get
            {
                return readersStack;
            }
            set
            {
                readersStack = value;
            }
        }

        internal void processSectionsIterator(XPathNodeIterator nodeIterator, ReportBlock root)
        {
            while (nodeIterator.MoveNext())
            {
                ReportBlock block = null;
                IEnumerator<BlockReader> readerIterator = ReadersStack.GetEnumerator();

                while (block == null && readerIterator.MoveNext())
                {
                    block = readerIterator.Current.ReadBlock(nodeIterator.Current, this);
                }

                if (block != null)
                {
                    root.Children.Add(block);
                }
            }
        }

        private void processStylesIterator(XPathNodeIterator stylesIterator, Styles styles)
        {
            while (stylesIterator.MoveNext())
            {
                styles.LoadStyle(stylesIterator.Current);
            }
        }

        protected void loadReportTemplate(XPathDocument reportDocument, ReportObject report)
        {
            RootBlock root = new RootBlock();
            XPathNavigator tplNavi = reportDocument.CreateNavigator();

            XPathNodeIterator sectionsIterator = tplNavi.Select("/report/sections/*");
            processSectionsIterator(sectionsIterator, root);

            report.ContentRoot = root;
        }

        protected void loadReportStyles(XPathDocument reportDocument, ReportObject report)
        {
            Styles styles = new Styles();
            XPathNavigator tplNavi = reportDocument.CreateNavigator();

            XPathNodeIterator stylesIterator = tplNavi.Select("/report/styles/*");
            processStylesIterator(stylesIterator, styles);

            report.Styles = styles;
        }

        /// <summary>
        /// Carica il template del report
        /// </summary>
        /// <param name="reportPath">Percorso fisico del file di template del report</param>
        /// <returns></returns>
        public ReportObject LoadReport(string reportPath)
        {
            ReportObject report = new ReportObject();

            if (!File.Exists(reportPath))
            {
                throw new FileNotFoundException("Impossibile trovare il template del report");
            }

            report.ReportPath = new FileInfo(reportPath).DirectoryName;

            XPathDocument reportDocument = new XPathDocument(reportPath);

            XPathNavigator tplNavi = reportDocument.CreateNavigator();
            if (tplNavi.SelectSingleNode("/report/@orientation") != null)
                report.ReportOrientation = tplNavi.SelectSingleNode("/report/@orientation").Value;

            loadReportStyles(reportDocument, report);
            loadReportTemplate(reportDocument, report);
            
            return report;
        }
    }
}
