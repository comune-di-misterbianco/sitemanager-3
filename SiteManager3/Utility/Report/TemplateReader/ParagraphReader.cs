﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using Report.Components;

namespace Report.TemplateReader
{
    class ParagraphReader : BlockReader
    {
        protected override ReportBlock ReadBlockImpl(XPathNavigator n, TemplateReader reader)
        {
            ParagraphBlock block = null;

            if (n.Name == "paragraph")
            {
                block = new ParagraphBlock(n.InnerXml.Trim());

                //permettono di impostare spaziatura pre e post paragrafo
                if (n.SelectSingleNode("@spaceafter") != null)
                    block.SpaceAfter = n.SelectSingleNode("@spaceafter").Value.Trim();
                if (n.SelectSingleNode("@spacebefore") != null)
                    block.SpaceBefore = n.SelectSingleNode("@spacebefore").Value.Trim();

                //permette di impostare un separatore per il paragrafo
                if (n.SelectSingleNode("@border") != null)
                    block.Borders = n.SelectSingleNode("@border").Value; 
            }

            return block;
        }
    }
}
