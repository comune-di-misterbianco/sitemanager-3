﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using Report.Components;
using MigraDoc.DocumentObjectModel;

namespace Report.TemplateReader
{
    public abstract class BlockReader
    {
        protected abstract ReportBlock ReadBlockImpl(XPathNavigator n, TemplateReader reader);

        protected ParagraphAlignment GetAligment(string stringalign)
        {
            ParagraphAlignment pa = ParagraphAlignment.Left;

            switch (stringalign)
            {
                case "center":
                    pa = ParagraphAlignment.Center;
                    break;
                case "right":
                    pa = ParagraphAlignment.Right;
                    break;
                case "left":
                    pa = ParagraphAlignment.Left;
                    break;
                case "justify":
                    pa = ParagraphAlignment.Justify;
                    break;
            }

            return pa;
        }

        public ReportBlock ReadBlock(XPathNavigator n, TemplateReader reader)
        {
            ReportBlock b = ReadBlockImpl(n, reader);

            if (b is ParagraphBlock)
            {
                var lb = b as ParagraphBlock;
                XPathNavigator renderNode = n.SelectSingleNode("@render");
                if (renderNode != null)
                {
                    switch (renderNode.Value)
                    {
                        case "AllPlaceholderFilled":
                            lb.RenderMode = ParagraphBlock.RenderModes.AllPlaceholderFilled;
                            break;
                        case "AtLeastOnePlaceholderFilled":
                            lb.RenderMode = ParagraphBlock.RenderModes.AtLeastOnePlaceholderFilled;
                            break;
                        case "Always":
                            lb.RenderMode = ParagraphBlock.RenderModes.Always;
                            break;
                    }
                }
            }

            if (b != null)
            {
                // TODO: Aggiungere il supporto per più stili associati ad un unico blocco
                XPathNavigator styleNode = n.SelectSingleNode("@style");
                if (styleNode != null)
                {
                    b.StyleName = styleNode.Value;
                }
            }

            return b;
        }
    }
}
