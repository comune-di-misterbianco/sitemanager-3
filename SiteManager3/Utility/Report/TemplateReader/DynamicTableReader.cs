﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using MigraDoc.DocumentObjectModel.Tables;
using Report.Components;

namespace Report.TemplateReader
{
    class DynamicTableReader : BlockReader
    {
        protected DynamicTableBlock table;

        protected override ReportBlock ReadBlockImpl(XPathNavigator n, TemplateReader reader)
        {
            if (n.Name == "dynamicTable")
            {
                table = new DynamicTableBlock();

                if (n.SelectSingleNode("@align") != null)
                    table.Table.Format.Alignment = GetAligment(n.SelectSingleNode("@align").Value);

                XPathNodeIterator it = n.Select("preHeaderRows/*");
                table.PreHeaderRows = readRows(it);

                table.DataSource = n.SelectSingleNode("data-source").Value;
                table.HeaderStyle = n.SelectSingleNode("columns/@headerStyle") != null ?
                    n.SelectSingleNode("columns/@headerStyle").Value : null;

                it = n.Select("columns/*");
                while (it.MoveNext())
                {
                    XPathNavigator cur = it.Current;
                    table.FixedWidthDynamicColumns.Add(cur.SelectSingleNode("@tag").ToString(), 
                        cur.SelectSingleNode("@width").ValueAsDouble);
                }

                it = n.Select("footerRows/*");
                table.FooterRows = readRows(it);

                return table;
            }
            else
                return null;
        }

        private Rows readRows(XPathNodeIterator rowIt)
        {
            Rows rows = new Rows();

            while (rowIt.MoveNext())
            {
                XPathNavigator rowNavi = rowIt.Current;
                Row r = new Row();

                XPathNodeIterator cellIt = rowNavi.Select("cell");
                int idx = 0;
                while (cellIt.MoveNext())
                {
                    XPathNavigator cellNavi = cellIt.Current;
                    Cell c = new Cell();

                    if (cellNavi.SelectSingleNode("@mergeRight") != null)
                    {
                        string merge = cellNavi.SelectSingleNode("@mergeRight").Value;
                        if (merge != "*")
                            c.MergeRight = int.Parse(merge);
                        else
                            c.MergeRight = -1;
                    }
                    if (cellNavi.SelectSingleNode("@mergeDown") != null)
                        c.MergeDown = cellNavi.SelectSingleNode("@mergeDown").ValueAsInt;

                    c.Tag = cellNavi.Value.Trim();
                    r.Cells.Add(c);
                    idx++;
                }

                rows.Add(r);
            }

            return rows;
        }
    }
}
