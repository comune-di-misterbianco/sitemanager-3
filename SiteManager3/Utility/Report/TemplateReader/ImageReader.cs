﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using Report.Components;
using System.Text.RegularExpressions;

namespace Report.TemplateReader
{
    class ImageReader : BlockReader
    {
        protected override ReportBlock ReadBlockImpl(XPathNavigator n, TemplateReader reader)
        {
            ImageBlock block = null;

            if (n.Name == "image")
                block = new ImageBlock(Regex.Replace(n.Value, "[\\r\\n\\s]", ""));

            return block;
        }
    }
}
