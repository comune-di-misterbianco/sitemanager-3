﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using MigraDoc.DocumentObjectModel.Tables;
using Report.Components;

namespace Report.TemplateReader
{
    class StreamImageReader : BlockReader
    {
        protected override ReportBlock ReadBlockImpl(XPathNavigator n, TemplateReader reader)
        {
            if (n.Name == "streamImage")
            {
                StreamImageBlock table = new StreamImageBlock(n.SelectSingleNode("@imageName").Value);
                return table;
            }
            else
                return null;
        }
    }
}
