﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Report.Components
{
    class RootBlock : ReportBlock
    {
        private List<ReportBlock> children = new List<ReportBlock>();

        public override List<ReportBlock> Children
        {
            get
            {
                return children;
            }
            set
            {
                children = value;
            }
        }
    }
}
