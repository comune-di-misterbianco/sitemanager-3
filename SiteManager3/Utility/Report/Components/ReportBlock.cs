﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.Visitor;

namespace Report.Components
{
    public abstract class ReportBlock
    {
        private string styleName = null;

        public void Accept(ReportPdfVisitor v)
        {
            v.visit(this);
        }

        public abstract List<ReportBlock> Children
        {
            get;
            set;
        }

        public string StyleName 
        {
            get
            {
                return styleName;
            }
            set
            {
                styleName = value;
            }
        }

        public bool HasStyle
        {
            get
            {
                return styleName != null;
            }
        }

        
    }
}
