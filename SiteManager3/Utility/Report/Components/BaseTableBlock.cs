﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MigraDoc.DocumentObjectModel.Tables;
using Report.Visitor;

namespace Report.Components
{
    public abstract class BaseTableBlock : LeafReportBlock
    {
        protected Table table;
        protected string dataSource;
        protected string headerStyle = null;
        // private Dictionary<Column, double> minWidths = new Dictionary<Column, double>();
         
        /*
         * Mette i bordi visibili di default.
         */
        protected BaseTableBlock() 
        {
            FixedWidths = new Dictionary<int, double>();
            PreHeaderRows = new Rows();
            FooterRows = new Rows();
            reset();
        }

        public Rows PreHeaderRows { get; set; }
        public Rows FooterRows { get; set; }

        public Table Table
        {
            get
            {
                return table;
            }
        }

        public Dictionary<int, double> FixedWidths
        {
            get;
            set;
        }

        public Columns Columns
        {
            get
            {
                return table.Columns;
            }
            set
            {
                table.Columns = value;
            }
        }

        public string DataSource
        {
            get
            {
                return dataSource;
            }
            set
            {
                dataSource = value;
            }
        }

        public string HeaderStyle {
            get
            {
                return headerStyle;
            }
            set 
            {
                headerStyle = value;
            }
        }
    }
}
