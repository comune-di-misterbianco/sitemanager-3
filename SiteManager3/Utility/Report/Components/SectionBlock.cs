﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.Visitor;

namespace Report.Components
{
    public class SectionBlock : ReportBlock
    {
        private List<ReportBlock> children = new List<ReportBlock>();

        public SectionBlock()
        {
            children = new List<ReportBlock>();
        }

        public override List<ReportBlock> Children
        {
            get 
            {
                return children;
            }
            set 
            {
                children = value;
            }
        }
    }
}
