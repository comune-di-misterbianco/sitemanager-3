﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MigraDoc.DocumentObjectModel.Tables;
using Report.Visitor;

namespace Report.Components
{
    public class DynamicTableBlock : BaseTableBlock
    {
        public DynamicTableBlock()
            : base()
        {
            FixedWidthDynamicColumns = new Dictionary<string, double>();
        }

        public Dictionary<string, double> FixedWidthDynamicColumns { get; set; }

        public override void reset()
        {
            Columns c = new Columns();
            //if (table != null && Columns != null && Columns.Count > 0)
            //    c = Columns;

            table = new Table();
            table.Borders.Visible = true;
            table.Columns = c.Clone();
            FixedWidths = new Dictionary<int, double>();
        }
    }
}
