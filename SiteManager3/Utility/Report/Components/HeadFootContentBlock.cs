﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Report.Components
{
    class HeadFootContentBlock : ReportBlock
    {
        public enum Type
        {
            Header,
            Footer,
            Content
        };

        private Type subsectionType;
        private List<ReportBlock> children = new List<ReportBlock>();

        public HeadFootContentBlock(Type subsectionType)
        {
            this.subsectionType = subsectionType;
        }

        public Type Subsection
        {
            get
            {
                return subsectionType;
            }
            set
            {
                subsectionType = value;
            }
        }

        public override List<ReportBlock> Children
        {
            get
            {
                return children;
            }
            set
            {
                children = value;
            }
        }
    }
}
