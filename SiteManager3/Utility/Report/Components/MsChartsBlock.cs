﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MigraDoc.DocumentObjectModel.Tables;
using Report.Visitor;

namespace Report.Components
{
    public class StreamImageBlock : LeafReportBlock
    {
        public StreamImageBlock(string chartName)
        {
            this.ChartName = chartName;
        }

        public string ChartName { get; private set; }

        public override void reset()
        {
        }
    }
}
