﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Report.Components
{
    class ImageBlock : LeafReportBlock
    {
        private string imagePath;

        public ImageBlock(String imagePath)
        {
            this.imagePath = imagePath;
        }

        public string ImagePath { 
            get 
            {
                return imagePath;
            } 
            set 
            {
                imagePath = value;
            } 
        }

        public override void reset()
        {
        }
    }
}
