﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MigraDoc.DocumentObjectModel.Tables;
using Report.Visitor;

namespace Report.Components
{
    public class TableBlock : BaseTableBlock
    {
        public TableBlock() 
            : base() 
        {
        }

        public override void reset()
        {
            Columns c = new Columns();
            if (table != null && Columns != null && Columns.Count > 0)
                c = Columns;

            table = new Table();
            table.Borders.Visible = true;
            table.Columns = c.Clone();
        }
    }
}
