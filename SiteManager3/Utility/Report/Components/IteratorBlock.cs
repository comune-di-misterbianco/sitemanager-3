﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Report.Components
{
    class IteratorBlock : ReportBlock
    {
        public IteratorBlock()
        {
            Children = new List<ReportBlock>();
        }

        public override List<ReportBlock> Children
        {
            get;
            set;
        }

        public string DataSource { get; set; }
    }
}
