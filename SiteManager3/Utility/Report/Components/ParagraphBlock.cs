﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.Visitor;
using MigraDoc.DocumentObjectModel;

namespace Report.Components
{
    public class ParagraphBlock : LeafReportBlock
    {
        private Unit spaceAfter;
        private Unit spaceBefore;
        private string borders;

        public ParagraphBlock(string text)
        {
            this.Text = text;
            RenderMode = RenderModes.Always;
            spaceAfter = Unit.Empty;
            spaceBefore = Unit.Empty;
            borders = "none";
        }

        public string Text { get; private set; }

        public Unit SpaceAfter
        {
            get { return spaceAfter; }
            set
            {
                spaceAfter = value;
            }
        }

        public Unit SpaceBefore
        {
            get { return spaceBefore; }
            set
            {
                spaceBefore = value;
            }
        }

        public string Borders
        {
            get { return borders; }
            set
            {
                borders = value;
            }
        }

        public RenderModes RenderMode { get; set; } 
        public enum RenderModes { Always, AllPlaceholderFilled, AtLeastOnePlaceholderFilled, }

        public override void reset()
        {
        }
    }
}
