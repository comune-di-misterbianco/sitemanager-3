﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MigraDoc.DocumentObjectModel.Tables;
using Report.Visitor;

namespace Report.Components
{
    public class GenericTableBlock : BaseTableBlock
    {
        protected Rows rows;

        public GenericTableBlock() 
            : base() 
        {
            rows = new Rows();
        }

        public Rows Rows
        {
            get
            {
                return table.Rows;
            }
            set
            {
                table.Rows = value;
            }
        }


        public override void reset()
        {
            Columns c = new Columns();
            Rows r = new Rows();
            if (table != null && Columns != null && Columns.Count > 0)
                c = Columns;
            if (table != null && Rows != null && Rows.Count > 0)
                r = Rows;

            table = new Table();
            table.Borders.Visible = true;
            table.Columns = c.Clone();
            table.Rows = r.Clone();
        }
    }
}
