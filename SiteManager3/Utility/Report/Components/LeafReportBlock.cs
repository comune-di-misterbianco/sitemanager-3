﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.Visitor;

namespace Report.Components
{
    public abstract class LeafReportBlock : ReportBlock
    {
        public override List<ReportBlock> Children
        {
            get
            {
                return null;
            }
            set
            {
                throw new NotSupportedException("LeafReportBlock cannot have children blocks!");
            }
        }
        
        public abstract void reset();
    }
}
