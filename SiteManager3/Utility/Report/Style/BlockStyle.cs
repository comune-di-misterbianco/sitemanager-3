﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;

namespace Report.Style
{
    public class BlockStyle : ICloneable
    {
        private bool inheritable = true;

        private Font font = new Font();
        private ParagraphAlignment alignment;
        private Unit frameWidth;
        private Unit frameDistance;
        private Color frameColor;

        // Defaults per il posizionemento (segue il fluso della pagina)
        private double width = -1;
        private double height = -1;
        private double topLeftX = -1;
        private double topLeftY = -1;

        // Indicatori di modifica di una delle caratteristiche dello stile, utili nel layering
        private bool hasFont = false;
        private bool hasAlignment = false;
        private bool hasWidth = false;
        private bool hasHeight = false;
        private bool hasTopLeftX = false;
        private bool hasTopLeftY = false;
        private bool hasFrame = false;

        public BlockStyle()
        {
            // Defaults dei font
            font.Name = "Times New Roman";
            font.Size = 10;
            font.Bold = false;
            font.Italic = false;
            font.Underline = Underline.None;

            // Default nell'allineamento
            alignment = ParagraphAlignment.Left;
            frameWidth = Unit.Empty;
            frameDistance = Unit.Empty;
            //frameColor = Colors.LightGray;
        }

        public bool Inheritable
        {
            get { return inheritable; }
            set { inheritable = value; }
        }

        public bool HasTopLeftY
        {
            get { return hasTopLeftY; }
        }

        public bool HasTopLeftX
        {
            get { return hasTopLeftX; }
        }

        public bool HasWidth
        {
            get { return hasWidth; }
        }

        public bool HasHeight
        {
            get { return hasHeight; }
        }

        public bool HasFont
        {
            get { return hasFont; }
        }

        public bool HasAlignment
        {
            get { return hasAlignment; }
        }

        public ParagraphAlignment Alignment
        {
            get
            {
                return alignment;
            }
            set
            {
                hasAlignment = true;
                alignment = value;
            }
        }

        public Unit FrameWidth
        {
            get { return frameWidth; }
            set
            {
                hasFrame = true;
                frameWidth = value;
            }
        }

        public Unit FrameDistance
        {
            get { return frameDistance; }
            set
            {
                hasFrame = true;
                frameDistance = value;
            }
        }

        public Color FrameColor
        {
            get { return frameColor; }
            set
            {
                hasFrame = true;
                frameColor = value;
            }
        }

        public bool HasFrame
        {
            get { return hasFrame; }
            set { hasFrame = value; }
        }

        public Font Font
        {
            get
            {
                return font;
            }
            set
            {
                hasFont = true;
                font = value;
            }
        }

        public double Width
        {
            get
            {
                return width;
            }
            set
            {
                hasWidth = true;
                width = value;
            }
        }

        public double Height
        {
            get
            {
                return height;
            }
            set
            {
                hasHeight = true;
                height = value;
            }
        }

        public double TopLeftX
        {
            get
            {
                return topLeftX;
            }
            set
            {
                hasTopLeftX = true;
                topLeftX = value;
            }
        }

        public double TopLeftY
        {
            get
            {
                return topLeftY;
            }
            set
            {
                hasTopLeftY = true;
                topLeftY = value;
            }
        }

        internal void Merge(BlockStyle bs)
        {
            if (bs.HasAlignment)
                Alignment = bs.Alignment;
            if (bs.HasFont)
                Font = bs.Font;
            if (bs.HasHeight)
                Height = bs.Height;
            if (bs.HasWidth)
                Width = bs.Width;
            if (bs.HasTopLeftX)
                TopLeftX = bs.TopLeftX;
            if (bs.HasTopLeftY)
                TopLeftY = bs.TopLeftY;
            if (bs.HasFrame)
            {
                HasFrame = true;
                FrameWidth = bs.FrameWidth;
                FrameDistance = bs.FrameDistance;
                FrameColor = bs.FrameColor;
            }
        }

        public object Clone()
        {
            BlockStyle s = new BlockStyle();
            s.alignment = alignment;
            s.font = font;
            s.topLeftX = topLeftX;
            s.topLeftY = topLeftY;
            s.width = width;
            s.height = height;
            s.inheritable = inheritable;
            s.hasAlignment = hasAlignment;
            s.hasFont = hasFont;
            s.hasHeight = hasHeight;
            s.hasWidth = hasWidth;
            s.hasTopLeftX = hasTopLeftX;
            s.hasTopLeftY = hasTopLeftY;
            s.frameWidth = frameWidth;
            s.hasFrame = hasFrame;
            s.frameDistance = frameDistance;
            s.frameColor = frameColor;
            s.hasFrame = hasFrame;

            return s;
        }
    }
}
