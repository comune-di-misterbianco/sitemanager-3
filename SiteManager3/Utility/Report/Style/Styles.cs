﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using MigraDoc.DocumentObjectModel;

namespace Report.Style
{
    [Serializable]
    public class Styles : Dictionary<string, BlockStyle>
    {
        public Styles()
        {
            Add("_default", new BlockStyle());
        }

        internal void LoadStyle(XPathNavigator navigator)
        {
            string styleKey = navigator.SelectSingleNode("@name").Value;
            BlockStyle style = new BlockStyle();
            
            XPathNodeIterator it = navigator.Select("font/*");
            if (it.Count > 0)
                style.Font = new Font();

            while (it.MoveNext())
            {
                XPathNavigator fontNode = it.Current;
                switch (fontNode.Name)
                {
                    case "family":
                        style.Font.Name = fontNode.Value.Trim();
                        break;
                    case "size":
                        style.Font.Size = Unit.FromPoint(fontNode.ValueAsDouble);
                        break;
                    case "bold":
                        style.Font.Bold = fontNode.ValueAsBoolean;
                        break;
                    case "italic":
                        style.Font.Italic = fontNode.ValueAsBoolean;
                        break;
                    case "underline":
                        style.Font.Underline = fontNode.ValueAsBoolean ? Underline.Single : Underline.None;
                        break;
                    case "align":
                        switch (fontNode.Value.Trim())
                        {
                            case "center":
                                style.Alignment = ParagraphAlignment.Center;
                                break;
                            case "justify":
                                style.Alignment = ParagraphAlignment.Justify;
                                break;
                            case "left":
                                style.Alignment = ParagraphAlignment.Left;
                                break;
                            case "right":
                                style.Alignment = ParagraphAlignment.Right;
                                break;
                        }
                        break;
                }
            }

            it = navigator.Select("frame/*");
            while (it.MoveNext())
            {
                XPathNavigator bordersNode = it.Current;
                switch (bordersNode.Name)
                {
                    case "width":
                        style.FrameWidth = bordersNode.ValueAsDouble;
                        break;
                    case "distance":
                        style.FrameDistance = bordersNode.Value.Trim();
                        break;
                    case "color":
                        if(bordersNode.ValueAsBoolean == true) 
                            style.FrameColor = Colors.LightGray;
                        break;
                }
            }

            it = navigator.Select("bounds/*");
            while (it.MoveNext())
            {
                XPathNavigator boundsNode = it.Current;
                switch (boundsNode.Name)
                {
                    case "width":
                        style.Width = boundsNode.ValueAsDouble;
                        break;
                    case "height":
                        style.Height = boundsNode.ValueAsDouble;
                        break;
                    case "topLeftX":
                        style.TopLeftX = boundsNode.ValueAsDouble;
                        break;
                    case "topLeftY":
                        style.TopLeftY = boundsNode.ValueAsDouble;
                        break;
                }
            }

            XPathNavigator inheritableNode = navigator.SelectSingleNode("@inheritable");
            style.Inheritable = inheritableNode == null ? true : inheritableNode.ValueAsBoolean;

            this[styleKey] = style;
        }
    }
}
