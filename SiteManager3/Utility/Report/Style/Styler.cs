﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.Components;
using Report.Visitor;

namespace Report.Style
{
    /**
     * Questa classe si dovrebbe occupare di tracciare gli stili mentre si naviga il report.
     * 
     * Utilizzo i metodi Enter ed Exit in maniera da gestire gli stili durante la visita
     * dell'albero del report come se avessi una pila. L'unica differenza è che l'accesso in lettura
     * avviene effettuando il merge di tutti gli stili presenti appunto in pila.
     * 
     * Questo per far si che i blocchi interni ereditino certe caratteristiche di quelli
     * esterni quali ad esempio la configurazione dei font. Una proprietà dello stile
     * (inheritable) specifica se gli stili in questione non vanno ereditati dai figli del blocco.
     */
    public class Styler
    {
        private List<ReportBlock> stack = new List<ReportBlock>();
        private Styles styles;

        public Styles Styles
        {
            get
            {
                return styles;
            }
            set
            {
                styles = value;
            }
        }
        
        internal void Exit(ReportBlock block)
        {
            stack.RemoveAt(stack.Count - 1);
        }

        internal void Enter(ReportBlock block)
        {
            stack.Add(block);
        }

        public BlockStyle MergedStyle
        {
            get
            {
                BlockStyle merged = (BlockStyle) Styles["_default"].Clone();

                foreach (ReportBlock block in stack)
                {
                    if (block.HasStyle)
                    {
                        BlockStyle bs = Styles[block.StyleName];
                        if (bs.Inheritable || stack.IndexOf(block) == stack.Count - 1)
                        {
                            merged.Merge(bs);
                        }
                    }
                }

                return merged;
            }
        }
    }
}
