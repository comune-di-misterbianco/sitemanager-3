﻿using System;
using System.Collections.Generic;
using System.Text;
using MigraDoc.DocumentObjectModel.Tables;
using Report.Components;
using Report.Data;
using System.Data;
using System.Linq;
using MigraDoc.DocumentObjectModel;
using Report.Style;

namespace Report.Visitor
{
    public class GenericTableBlockHandler : BaseTableBlockHandler
    {
        protected override void HandleBlockImpl(ReportBlock block, ReportPdfVisitor v)
        {
            ReportPdfVisitor = v;
            GenericTableBlock tableBlock = block as GenericTableBlock;
            BlockStyle bs = v.Styler.MergedStyle;
            if (tableBlock != null)
            {
                Table table = tableBlock.Table;
                DataTable data;
                bool hasTable = false;

                //Se nel template non viene definita una data-source, viene fatto il databinding sfruttando gli eventuali placeholders
                if (tableBlock.DataSource == null)
                {
                    hasTable = true;
                    foreach (Row row in table.Rows)
                        foreach (Cell cell in row.Cells)
                        {
                            string content = fillPlaceholders(cell.Tag.ToString(), v.DataContext);
                            Paragraph p = cell.AddParagraph();
                            FormatParagraph(content, p);
                            p.Format.Font = bs.Font.Clone();
                        }
                }
                //Se viene definita una data-source ed è diversa da null, viene fatto il binding con i dati in essa contenuti
                else if ((data = ReportPdfVisitor.DataContext[tableBlock.DataSource] as DataTable) != null)
                {
                    hasTable = true;
                    foreach (Cell cell in table.Rows[0].Cells)
                        cell.Elements.Clear();

                    Row formattedRow = table.Rows[0].Clone();
                    table.Rows.Clear();
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        Row row = formattedRow.Clone();
                        table.Rows.Add(row);

                        for (int j = 0; j < row.Cells.Count; j++)
                        {
                            Cell cell = row.Cells[j];
                            Paragraph p = cell.AddParagraph();
                            FormatParagraph(data.Rows[i][j].ToString(), p);
                            p.Format.Font = bs.Font.Clone();
                        }
                    }
                }

                if(hasTable == true) 
                    AddDocumentObject(table, ReportPdfVisitor);
            }
        }
    }
}
