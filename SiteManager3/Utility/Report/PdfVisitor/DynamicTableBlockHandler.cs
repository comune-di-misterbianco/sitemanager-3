﻿using System;
using System.Collections.Generic;
using System.Text;
using MigraDoc.DocumentObjectModel.Tables;
using Report.Components;
using Report.Data;
using System.Data;
using System.Linq;
using MigraDoc.DocumentObjectModel;
using Report.Style;

namespace Report.Visitor
{
    public class DynamicTableBlockHandler : BaseTableBlockHandler
    {
        protected override void HandleBlockImpl(ReportBlock block, ReportPdfVisitor v)
        {
            ReportPdfVisitor = v;
            DynamicTableBlock tableBlock = block as DynamicTableBlock;
            if (tableBlock != null)
            {
                Table table = tableBlock.Table;
                DataTable data = ReportPdfVisitor.DataContext[tableBlock.DataSource] as DataTable;
                
                int index = 0;
                foreach (DataColumn col in data.Columns)
                {
                    Column migraccol = table.Columns.AddColumn();
                    migraccol.Tag = col.ColumnName;
                    migraccol.Comment = col.Caption;

                    if (tableBlock.FixedWidthDynamicColumns.ContainsKey(col.ColumnName))
                    {
                        tableBlock.FixedWidths.Add(index, tableBlock.FixedWidthDynamicColumns[col.ColumnName]);
                    }

                    index++;
                }

                MeasureColumns(tableBlock, data);

                BlockStyle cellStyle = ReportPdfVisitor.Styler.MergedStyle;
                BlockStyle headersStyle = (BlockStyle) cellStyle.Clone();
                if (tableBlock.HeaderStyle != null)
                    headersStyle.Merge(ReportPdfVisitor.Styler.Styles[tableBlock.HeaderStyle]);

                addStaticRows(tableBlock.PreHeaderRows, table);

                Row headerRow = new Row();
                foreach (Column col in tableBlock.Columns)
                {
                    Cell headerCell = new Cell();

                    Paragraph p = new Paragraph();
                    p.AddText(col.Comment);
                    p.Format.Font = headersStyle.Font.Clone();
                    p.Format.Alignment = headersStyle.Alignment;

                    headerCell.Add(p);
                    headerRow.Cells.Add(headerCell);
                }
                table.Rows.Add(headerRow);

                foreach (DataRow dataRow in data.Rows)
                {
                    Row tableRow = table.AddRow();
                    foreach (Column col in table.Columns)
                    {
                        Cell cell = new Cell();

                        Paragraph p = new Paragraph();
                        p.AddText(dataRow[col.Tag.ToString()].ToString());
                        p.Format.Font = cellStyle.Font.Clone();
                        p.Format.Alignment = cellStyle.Alignment;

                        cell.Add(p);
                        tableRow.Cells.Add(cell);
                    }
                }

                addStaticRows(tableBlock.FooterRows, table);

                AddDocumentObject(table, ReportPdfVisitor);
            }
        }
    }
}
