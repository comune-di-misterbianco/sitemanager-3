﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.Components;

namespace Report.Visitor
{
    class RootBlockHandler : BlockHandler
    {
        protected override void HandleBlockImpl(Components.ReportBlock block, ReportPdfVisitor v)
        {
            RootBlock b = block as RootBlock;
            if (b != null)
            {
                v.VisitorState = ReportPdfVisitor.ReportVisitorState.Root;

                foreach (ReportBlock child in b.Children)
                {
                    child.Accept(v);
                }

                v.VisitorState = ReportPdfVisitor.ReportVisitorState.Root;
            }
        }
    }
}
