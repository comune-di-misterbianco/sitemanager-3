﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.Components;
using MigraDoc.DocumentObjectModel;
using Report.Style;

namespace Report.Visitor
{
    public class TextBlockHandler : BlockHandler
    {
        protected override void HandleBlockImpl(ReportBlock block, ReportPdfVisitor v)
        {
            ParagraphBlock b = block as ParagraphBlock;
            if (b != null)
            {
                string content = fillPlaceholders(b.Text, v.DataContext);
                Paragraph p = new Paragraph();
                FormatParagraph(content, p);

                p.Format.SpaceAfter = b.SpaceAfter;
                p.Format.SpaceBefore = b.SpaceBefore;

                ConfigureBorders(p, b.Borders);

                // Diversamente da come fatto per gli stili di posizionamento quelli di testo
                // vengono invece applicati automaticamente; la correttezza del risultato
                // è garantita dall'utilizzo dello Styler.
                BlockStyle bs = v.Styler.MergedStyle;
                p.Format.Font = bs.Font.Clone();    // Se non clono c'è un'eccezione di MigraDoc
                p.Format.Alignment = bs.Alignment;
                
                if (bs.HasFrame)
                {
                    p.Format.Borders.Width = bs.FrameWidth;
                    p.Format.Borders.Distance = bs.FrameDistance;
                    p.Format.Shading.Color = bs.FrameColor;
                }
                AddDocumentObject(p, v);
            }
        }

        protected override bool RenderBlock(ReportBlock block, ReportPdfVisitor v)
        {
            if (block is ParagraphBlock)
            {
                var pb = block as ParagraphBlock;
                switch (pb.RenderMode)
                {
                    case ParagraphBlock.RenderModes.Always: return true;
                    case ParagraphBlock.RenderModes.AllPlaceholderFilled: return CheckIfAllPlaceHoldersAreFilled(pb.Text, v.DataContext);
                    case ParagraphBlock.RenderModes.AtLeastOnePlaceholderFilled: return CheckIfAtLeastOnePlaceHoldersIsFilled(pb.Text, v.DataContext);
                    default: return true;
                }
            }

            return true;
        }

        protected void ConfigureBorders(Paragraph p, string position)
        {
            Border b = new Border();
            b.Style = BorderStyle.Single;
            b.Visible = true;

            switch (position)
            {
                case "bottom":
                    p.Format.Borders.Bottom = b;
                    break;
                case "top":
                    p.Format.Borders.Top = b;
                    break;
                case "both":
                    p.Format.Borders.Bottom = b;
                    p.Format.Borders.Top = b.Clone();
                    break;
            }
        }
    }
}
