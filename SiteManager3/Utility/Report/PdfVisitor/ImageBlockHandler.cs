﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.Components;
using MigraDoc.DocumentObjectModel.Shapes;
using Report.Style;
using MigraDoc.DocumentObjectModel;

namespace Report.Visitor
{
    class ImageBlockHandler : BlockHandler
    {
        protected override void HandleBlockImpl(Components.ReportBlock block, ReportPdfVisitor v)
        {
            ImageBlock b = block as ImageBlock;
            if (b != null)
            {
                Image i = new Image(v.TemplatePath + "\\" + fillPlaceholders(b.ImagePath, v.DataContext));
                
                BlockStyle bs = v.Styler.MergedStyle;
                if (bs.HasTopLeftX)
                    i.Left = Unit.FromPoint(bs.TopLeftX);
                if (bs.HasTopLeftY)
                    i.Top = Unit.FromPoint(bs.TopLeftY);
                if (bs.HasWidth)
                {
                    if (bs.Width == -1)
                    {
                        i.Width = v.CurrentSectionWidth;
                    }
                    else
                        i.Width = Unit.FromPoint(bs.Width);
                }
                if (bs.HasHeight)
                    i.Height = Unit.FromPoint(bs.Height);

                AddDocumentObject(i, v);
            }
        }
    }
}
