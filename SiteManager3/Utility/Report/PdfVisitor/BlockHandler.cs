﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.Components;
using MigraDoc.DocumentObjectModel;
using Report.Data;
using System.Text.RegularExpressions;

namespace Report.Visitor
{
    // TODO: Unificare il foreach per la visita dei children nei blocchi composti?
    public abstract class BlockHandler
    {
        protected void AddDocumentObject(DocumentObject obj, ReportPdfVisitor v)
        { 
            switch (v.VisitorState)
            {
                case ReportPdfVisitor.ReportVisitorState.Header:
                    v.CurrentSection.Headers.Primary.Elements.Add(obj);
                    break;
                case ReportPdfVisitor.ReportVisitorState.Footer:
                    v.CurrentSection.Footers.Primary.Elements.Add(obj);
                    break;
                case ReportPdfVisitor.ReportVisitorState.Content:
                    v.CurrentSection.Elements.Add(obj);
                    break;
                default:
                    throw new NotSupportedException("Blocco non supportato nella posizione specificata");
            }
        }

        bool ParagraphContainsTextFormatTags(string text)
        {
            return text.Contains("<br />") || text.Contains("<t />");
        }

        public void FormatParagraph(string content, Paragraph p)
        {
            string[] contents;
            if (ParagraphContainsTextFormatTags(content))
            {
                contents = content.Split(new string[] { "/>" }, StringSplitOptions.None);

                foreach (string text in contents)
                {
                    if (text.Contains("<br"))
                    {
                        p.AddText(text.Replace("<br", ""));
                        p.AddLineBreak();
                    }
                    else if (text.Contains("<t"))
                    {
                        p.AddText(text.Replace("<t", ""));
                        p.AddSpace(5);
                    }
                    else
                        p.AddText(text);
                }
            }
            else
                p.AddText(content);
        }

        public string fillPlaceholders(string text, DataContext data)
        {
            Match m = Regex.Match(text, "\\{([\\w-]*)\\}");
            while (m.Success)
            {
                string replacement = m.Groups[1].Value;
                if (data.ContainsKey(replacement))
                    replacement = data[m.Groups[1].Value] != null ? data[m.Groups[1].Value].ToString() : "";

                text = Regex.Replace(text, "\\{" + m.Groups[1].Value + "\\}", replacement);
                m = Regex.Match(text, "\\{([\\w-]*)\\}");
            }

            return text;
        }

        public bool CheckIfAllPlaceHoldersAreFilled(string text, DataContext data)
        {
            //Creo un array con i placeholders contenuti nella string
            MatchCollection placeHoldersCollection = Regex.Matches(text, "\\{([\\w-]*)\\}");

            //Creo un lista con i valori dei placeholders dell'array placeHoldersCollection, rimuovendo i doppioni
            List<string> placeHoldersList = placeHoldersCollection.Cast<Match>().Select(x => x.Value.Substring(1, x.Value.Length-2)).Distinct().ToList();

            //Conto i placeholders nella lista placeHoldersList che non hanno una corrispondenza dentro al datacontext o che non hanno una stringa piena come valore associato
            var countPlaceHoldersNotFoundInDataContext = placeHoldersList.Where(x => !data.ContainsKey(x) || data[x] == null || data[x].ToString().Length == 0).Count();

            return countPlaceHoldersNotFoundInDataContext == 0;
        }
        public bool CheckIfAtLeastOnePlaceHoldersIsFilled(string text, DataContext data)
        {
            //Creo un array con i placeholders contenuti nella string
            MatchCollection placeHoldersCollection = Regex.Matches(text, "\\{([\\w-]*)\\}");

            //Creo un lista con i valori dei placeholders dell'array placeHoldersCollection, rimuovendo i doppioni
            List<string> placeHoldersList = placeHoldersCollection.Cast<Match>().Select(x => x.Value.Substring(1, x.Value.Length - 2)).Distinct().ToList();

            //Conto i placeholders nella lista placeHoldersList che non hanno una corrispondenza dentro al datacontext o che non hanno una stringa piena come valore associato
            var countPlaceHoldersFoundInDataContext = placeHoldersList.Where(x => data.ContainsKey(x) && data[x] != null && data[x].ToString().Length > 0).Count();

            return countPlaceHoldersFoundInDataContext > 0;
        }
        public void HandleBlock(ReportBlock block, ReportPdfVisitor v)
        {
            if(RenderBlock(block, v))
            {
                v.Styler.Enter(block);
                HandleBlockImpl(block, v);
                v.Styler.Exit(block);
            }
        }

        protected abstract void HandleBlockImpl(ReportBlock block, ReportPdfVisitor v);
        protected virtual bool RenderBlock(ReportBlock block, ReportPdfVisitor v)
        {
            return true;
        }
    }
}
