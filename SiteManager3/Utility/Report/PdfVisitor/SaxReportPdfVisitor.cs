﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using Report.Visitor;
using Report.Components;
using Report.Data;
using Report.Style;
using System.IO;

namespace Report.Visitor
{
    public class ReportPdfVisitor
    {
        public enum ReportVisitorState
        {
            Report,
            Root,
            Section,
            Header,
            Content,
            Footer
        };

        public const string ReportFilesToDeleteApplicationKey = "ReportFilesToDelete";
        public  List<string> FilesToDelete
        {
            get
            {
                if (_FilesToDelete == null && System.Web.HttpContext.Current != null)
                {
                    var app = System.Web.HttpContext.Current.Application;
                    if (app[ReportFilesToDeleteApplicationKey] != null)
                        _FilesToDelete = app[ReportFilesToDeleteApplicationKey] as List<string>;
                    else
                        app[ReportFilesToDeleteApplicationKey] = _FilesToDelete = new List<string>();
                }
                else if (_FilesToDelete == null)
                {
                    return new List<string>();
                }
                return _FilesToDelete;
            }
        }
        private  List<string> _FilesToDelete;

        private ReportVisitorState state = ReportVisitorState.Report;
        private Document document = new Document();
        private Section currentSection;
        private List<BlockHandler> handlers;
        private DataContext dataContext;
        private string templatePath;
        private Styler styler = new Styler();
        private string reportOrientation = "vertical";

        public string ReportOrientation
        {
            get
            {
                return reportOrientation;
            }
            set
            {
                reportOrientation = value;
            }
        }

        public ReportPdfVisitor()
        {
            handlers = new List<BlockHandler>();
            handlers.Add(new RootBlockHandler());
            handlers.Add(new SectionBlockHandler());
            handlers.Add(new HeadFootContentBlockHandler());
            handlers.Add(new TextBlockHandler());
            handlers.Add(new TableBlockHandler());
            handlers.Add(new ImageBlockHandler());
            handlers.Add(new StreamImageBlockHandler());
            handlers.Add(new DynamicTableBlockHandler());
            handlers.Add(new IteratorBlockHandler());
            handlers.Add(new GenericTableBlockHandler());
        }

        internal ReportVisitorState VisitorState
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }

        public DataContext DataContext
        {
            get
            {
                return dataContext;
            }
            set
            {
                dataContext = value;
            }
        }

        public List<BlockHandler> BlockHandlers
        {
            get
            {
                return handlers;
            }
            set
            {
                handlers = value;
            }
        }

        public Document Document
        {
            get
            {
                return document;
            }
            set
            {
                document = value;
            }
        }

        public Section CurrentSection
        {
            get
            {
                return currentSection;
            }
            set
            {
                currentSection = value;
            }
        }

        public Unit CurrentSectionWidth
        {
            get
            {
                Unit pageWidth;
                //Se la pagina ha orientamento orizzontale setta correttamente le dimensioni scambiando width ed height
                if (CurrentSection.PageSetup.Orientation == Orientation.Portrait) //verticale
                    pageWidth = CurrentSection.PageSetup.PageWidth.Value > 0 ?
                       CurrentSection.PageSetup.PageWidth : Document.DefaultPageSetup.PageWidth;
                else //orizzontale
                    pageWidth = CurrentSection.PageSetup.PageHeight.Value > 0 ?
                        CurrentSection.PageSetup.PageHeight : Document.DefaultPageSetup.PageHeight;

                Unit leftMargin = CurrentSection.PageSetup.LeftMargin.Value > 0 ?
                    CurrentSection.PageSetup.LeftMargin : Document.DefaultPageSetup.LeftMargin;

                Unit rightMargin = CurrentSection.PageSetup.RightMargin.Value > 0 ?
                    CurrentSection.PageSetup.RightMargin : Document.DefaultPageSetup.RightMargin;

                return pageWidth - leftMargin - rightMargin;
            }
        }

        public Unit CurrentSectionHeight
        {
            get
            {
                Unit pageHeight;
                //Se la pagina ha orientamento orizzontale setta correttamente le dimensioni scambiando width ed height
                if (CurrentSection.PageSetup.Orientation == Orientation.Portrait) //verticale
                    pageHeight = CurrentSection.PageSetup.PageHeight.Value > 0 ?
                        CurrentSection.PageSetup.PageHeight : Document.DefaultPageSetup.PageHeight;
                else //orizzontale
                    pageHeight = CurrentSection.PageSetup.PageWidth.Value > 0 ?
                       CurrentSection.PageSetup.PageWidth : Document.DefaultPageSetup.PageWidth;

                Unit topMargin = CurrentSection.PageSetup.TopMargin.Value > 0 ?
                    CurrentSection.PageSetup.TopMargin : Document.DefaultPageSetup.TopMargin;

                Unit bottomMargin = CurrentSection.PageSetup.BottomMargin.Value > 0 ?
                    CurrentSection.PageSetup.BottomMargin : Document.DefaultPageSetup.BottomMargin;

                return pageHeight - topMargin - bottomMargin;
            }
        }

        internal void createSection()
        {
            currentSection = document.AddSection();
            if (reportOrientation == "vertical")
                currentSection.PageSetup.Orientation = Orientation.Portrait;
            else
                currentSection.PageSetup.Orientation = Orientation.Landscape;
        }

        public void save(string filename)
        {
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(false,
                PdfFontEmbedding.Always);
            
            pdfRenderer.Document = Document;
            pdfRenderer.RenderDocument();

            pdfRenderer.PdfDocument.Save(filename);
            CleanTempFiles();
        }

        public void save(Stream s)
        {
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(false,
                PdfFontEmbedding.Always);

            pdfRenderer.Document = Document;
          
            pdfRenderer.RenderDocument();

            pdfRenderer.PdfDocument.Save(s);
            CleanTempFiles();
        }

        protected void CleanTempFiles()
        {
            foreach (string filename in this.FilesToDelete)
                try
                {
                    if (File.Exists(filename)) File.Delete(filename);
                }
                catch { }
        }

        internal void visit(ReportBlock block)
        {
            foreach (BlockHandler h in handlers)
            {
                h.HandleBlock(block, this);
            }
        }

        public string TemplatePath
        {
            get
            {
                return templatePath;
            }
            set
            {
                templatePath = value;
            }
        }

        public Styler Styler
        {
            get
            {
                return styler;
            }
            set
            {
                styler = value;
            }
        }
    }
}
