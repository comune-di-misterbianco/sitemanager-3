﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.Components;
using Report.Style;

namespace Report.Visitor
{
    public class SectionBlockHandler : BlockHandler
    {
        protected override void HandleBlockImpl(ReportBlock block, ReportPdfVisitor v)
        {
            SectionBlock b = block as SectionBlock;
            if (b != null)
            {
                v.createSection();
                v.VisitorState = ReportPdfVisitor.ReportVisitorState.Section;

                foreach (ReportBlock child in b.Children)
                {
                    child.Accept(v);
                }

                v.VisitorState = ReportPdfVisitor.ReportVisitorState.Report;
            }
        }
    }
}
