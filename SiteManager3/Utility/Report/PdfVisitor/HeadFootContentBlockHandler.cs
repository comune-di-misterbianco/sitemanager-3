﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.Components;
using MigraDoc.DocumentObjectModel;
using Report.Style;

namespace Report.Visitor
{
    class HeadFootContentBlockHandler : BlockHandler
    {
        protected override void HandleBlockImpl(ReportBlock block, ReportPdfVisitor v)
        {
            HeadFootContentBlock b = block as HeadFootContentBlock;
            BlockStyle bs = v.Styler.MergedStyle;
            if (b != null)
            {
                switch (b.Subsection)
                {
                    case HeadFootContentBlock.Type.Content:
                        v.VisitorState = ReportPdfVisitor.ReportVisitorState.Content;
                        break;
                    case HeadFootContentBlock.Type.Footer:
                        v.VisitorState = ReportPdfVisitor.ReportVisitorState.Footer;
                        if (bs.HasHeight)
                            v.CurrentSection.PageSetup.BottomMargin = bs.Height;
                        break;
                    case HeadFootContentBlock.Type.Header:
                        v.VisitorState = ReportPdfVisitor.ReportVisitorState.Header;
                        if (bs.HasHeight)
                            v.CurrentSection.PageSetup.TopMargin = bs.Height;
                        break;
                    default:
                        throw new NotSupportedException("Tipo di blocco non supportato: " + b.Subsection);
                }

                foreach (ReportBlock child in b.Children)
                {
                    child.Accept(v);
                }

                v.VisitorState = ReportPdfVisitor.ReportVisitorState.Section;
            }
        }
    }
}
