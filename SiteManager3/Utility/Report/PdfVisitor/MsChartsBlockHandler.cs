﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Report.Components;
using MigraDoc.DocumentObjectModel.Shapes;
using Report.Style;
using MigraDoc.DocumentObjectModel;
using System.Web.UI.DataVisualization.Charting;

namespace Report.Visitor
{
    class StreamImageBlockHandler : BlockHandler
    {
        protected override void HandleBlockImpl(Components.ReportBlock block, ReportPdfVisitor v)
        {
            StreamImageBlock b = block as StreamImageBlock;
            if (b != null)
            {
                var chart = v.DataContext[b.ChartName] as System.IO.Stream;

                System.Drawing.Image dotNetImage = System.Drawing.Image.FromStream(chart);

                var path = v.TemplatePath + "\\tempImage{0}.png";
                
                int index = 0;
                while (System.IO.File.Exists(string.Format(path, index))) index++;

                path = string.Format(path, index);
                v.FilesToDelete.Add(path);

                dotNetImage.Save(path, System.Drawing.Imaging.ImageFormat.Png);
                dotNetImage.Dispose();

                Image i = new Image(path);
                
                BlockStyle bs = v.Styler.MergedStyle;
                //double prop = 1;

                if (bs.HasTopLeftX) i.Left = Unit.FromPoint(bs.TopLeftX);
                if (bs.HasTopLeftY) i.Top = Unit.FromPoint(bs.TopLeftY);

                if (!bs.HasWidth || bs.Width == -1)
                {
                    //double w = v.CurrentSectionWidth.Point;
                    //prop = i.Width / w;
                    i.Width = Unit.FromPoint(v.CurrentSectionWidth.Point);
                }
                else
                    Unit.FromPoint(bs.Width);

                if (bs.HasHeight)
                    i.Height = Unit.FromPoint(bs.Height);
                //else
                //    i.Height = i.Height * prop;

                AddDocumentObject(i, v);


            }
        }
    }
}
