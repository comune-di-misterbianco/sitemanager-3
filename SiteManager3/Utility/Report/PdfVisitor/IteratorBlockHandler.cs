﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.Components;
using System.Data;
using Report.Data;

namespace Report.Visitor
{
    class IteratorBlockHandler : BlockHandler
    {
        protected override void HandleBlockImpl(ReportBlock block, ReportPdfVisitor v)
        {
            IteratorBlock itblk = block as IteratorBlock;
            if (itblk != null)
            {
                List<DataContext> iteratee = v.DataContext[itblk.DataSource] as List<DataContext>;
                if (iteratee != null && iteratee.Count > 0)
                {
                    DataContext originalContext = v.DataContext;

                    foreach (DataContext childContext in iteratee)
                    {
                        v.DataContext = childContext;
                        foreach (ReportBlock child in itblk.Children)
                        {
                            child.Accept(v);
                            if (child is LeafReportBlock)
                                ((LeafReportBlock)child).reset();
                        }
                    }

                    v.DataContext = originalContext;
                }
            }
        }
    }
}
