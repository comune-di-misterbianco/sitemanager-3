﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.Visitor;
using Report.Components;
using System.Data;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;

namespace Report.Visitor
{
    public abstract class BaseTableBlockHandler : BlockHandler
    {
        protected ReportPdfVisitor ReportPdfVisitor;

        protected void MeasureColumns(BaseTableBlock b, DataTable data)
        {
            double[] columnWidths = new double[b.Columns.Count];
            double colWidthSum = 0;
            int index = 0;
            double fixedColumns = 0;
            foreach (Column col in b.Columns)
            {
                if (b.FixedWidths.ContainsKey(index))
                    fixedColumns += b.FixedWidths[index];
                else
                {
                    TextMeasurement measure = new TextMeasurement(getColumnFont(col));

                    double rw = 0;
                    IEnumerable<DataRow> enumerable = data.Rows.Cast<DataRow>();
                    if (data.Rows.Count > 0)
                    {
                        rw = enumerable.
                            Select(r => MeasureString(r[col.Tag.ToString()].ToString(), measure)).
                            OrderBy(r => r).Last();
                    }

                    double hw = measure.MeasureString(col.Comment).Width;

                    columnWidths[index] = rw > hw ? rw : hw;
                    colWidthSum += columnWidths[index];
                }
                index++;
            }

            double colProp = (ReportPdfVisitor.CurrentSectionWidth - fixedColumns) / colWidthSum;
            index = 0;
            foreach (Column col in b.Columns)
            {
                if (b.FixedWidths.ContainsKey(index))
                    col.Width = b.FixedWidths[index];
                else
                    col.Width = Unit.FromPoint(columnWidths[index] * colProp);
                
                index++;
            }
        }

        protected Font getColumnFont(Column col)
        {
            Font font = null;

            if (ReportPdfVisitor.Document.Styles.Normal.Font.Size.Point > 0)
                font = ReportPdfVisitor.Document.Styles.Normal.Font;

            if (((MigraDoc.DocumentObjectModel.Style)ReportPdfVisitor.Document.Styles.LastObject).Font.Size.Point > 0)
                font = ((MigraDoc.DocumentObjectModel.Style)ReportPdfVisitor.Document.Styles.LastObject).Font;

            if (col.Format.Font.Size.Point > 0)
                font = col.Format.Font;

            return font;
        }

        protected double MeasureString(String text, TextMeasurement measure)
        {
            text = text.Split(new char[] { ' ', '\n', '-' }).OrderBy(r => r.Length).Last();
            return measure.MeasureString(text).Width;
        }

        protected void addStaticRows(Rows rows, Table table)
        {
            foreach (Row r in rows)
            {
                Row rowToAdd = r.Clone();
                int cellPos = 0;
                int rightCells = rowToAdd.Cells.Count - 1;
                foreach (Cell c in rowToAdd.Cells)
                {
                    if (c.MergeRight == -1)
                        c.MergeRight = int.MaxValue;

                    c.MergeRight = Math.Min(c.MergeRight, table.Columns.Count - 1 - cellPos - rightCells);
                    cellPos += c.MergeRight + 1;
                    rightCells -= c.MergeRight + 1;
                }

                foreach (Cell c in rowToAdd.Cells)
                {
                    c.AddParagraph(fillPlaceholders(c.Tag.ToString(), ReportPdfVisitor.DataContext));
                }

                table.Rows.Add(rowToAdd);
            }
        }
    }
}
