﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.Components;
using Report.Visitor;
using Report.TemplateReader;
using Report.Style;

namespace Report
{
    public class ReportObject
    {
        private ReportBlock root = new RootBlock();
        private Styles styles;
        private string reportPath = "";
        private string reportOrientation = "vertical";

        public string ReportPath
        {
            get
            {
                return reportPath;
            }
            set
            {
                reportPath = value;
            }
        }

        public string ReportOrientation
        {
            get
            {
                return reportOrientation;
            }
            set
            {
                reportOrientation = value;
            }
        }

        public ReportBlock ContentRoot
        {
            get
            {
                return root;
            }
            set
            {
                root = value;
            }
        }

        public Styles Styles
        {
            get
            {
                return styles;
            }
            set
            {
                styles = value;
            }
        }

        public void AppendToRoot(ReportBlock block)
        {
            root.Children.Add(block);
        }

        public void Accept(ReportPdfVisitor visitor)
        {
            visitor.TemplatePath = ReportPath;
            visitor.Styler.Styles = Styles;
            visitor.ReportOrientation = reportOrientation;
            root.Accept(visitor);
        }
    }
}
