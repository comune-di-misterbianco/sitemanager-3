﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Text.RegularExpressions;
//using System.Threading.Tasks;

//namespace SharedUtilities
//{
//    public class HtmlCleaner
//    {
//        /// <summary>
//        /// Remove styles and clean html syntax using html tidy.dll wrapper (CleanGoogleDocs and CleanWord2000 is active) 
//        /// </summary>
//        /// <param name="htmlText">Html code</param>
//        /// <returns>Clean html code</returns>
//        public static string Clean(string htmlText)
//        {
//            htmlText = Regex.Replace(htmlText, "style=\"[^\"]*\"", "", RegexOptions.IgnoreCase);
            
//            using (Document doc = Document.FromString(htmlText))
//            {
//                doc.OutputBodyOnly = AutoBool.Yes;
//                doc.Quiet = true;

//                doc.CleanGoogleDocs = true;
//                doc.CleanWord2000 = true;
//                doc.OutputXhtml = true;
//                doc.DropEmptyElements = true;
//                doc.DropFontTags = true;
//                doc.MergeSpans = TidyManaged.AutoBool.Yes;

//                doc.CleanAndRepair();
//                htmlText = doc.Save();
//            }

//            return htmlText;
//        }
//    }
//}
