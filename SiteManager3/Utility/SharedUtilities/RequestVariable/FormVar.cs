﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Collections.Specialized;

namespace NetService.Utility.Request
{
    public class FormVar<T> : RequestVariable<T>
    {
        protected override NameValueCollection Data
        {
            get
            {
                return _QueryStringData ?? (_QueryStringData = HttpContext.Current.Request.Form);
            }
        }
        private NameValueCollection _QueryStringData;

        public FormVar(string key)
            : base(key)
        {
        }
        public FormVar(string key, bool isEncrypted)
            : base(key, isEncrypted)
        {}
    }
}
