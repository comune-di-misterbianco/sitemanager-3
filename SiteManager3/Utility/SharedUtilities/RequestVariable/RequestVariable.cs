﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Collections.Specialized;

namespace NetService.Utility.Request
{
    public abstract class RequestVariable<T>
    {
        private string[] AllowedTypes = 
        { 
            typeof(int).FullName, 
            typeof(bool).FullName, 
            typeof(double).FullName, 
            typeof(string).FullName,
            typeof(float).FullName, 
            typeof(DateTime).FullName,
            typeof(long).FullName, 
            typeof(decimal).FullName 
        };

        protected abstract NameValueCollection Data
        {
            get;
        }

        public bool HasValidValue
        {
            get
            {
                return IsString ? !string.IsNullOrEmpty(Value as string) : !Value.Equals(GetMinValue());
            }
        }

        private bool IsString
        {
            get
            {
                return typeof(T).FullName == typeof(string).FullName;
            }
        }
        private bool IsBool
        {
            get
            {
                return typeof(T).FullName == typeof(bool).FullName;
            }
        }

        public bool IsEncrypted { get; private set; }
        public string Key { get; private set; }

        public RequestVariable(string key):this(key, false)
        {
        }
        public RequestVariable(string key, bool isEncrypted)
        {
            this.IsEncrypted = isEncrypted;
            this.Key = key;
            if (AllowedTypes.Contains(typeof(T).FullName))
            {
                this.Value = GetValue();
            }
            else
                throw new Exception("Unhandled type '" + typeof(T).FullName + "'");
        }

        public T Value
        {
            get;
            private set;
        }

        private T GetMinValue()
        {
            Type type = typeof(T);
            System.Reflection.FieldInfo fi = type.GetField("MinValue");
            return (T)fi.GetValue(this);
        }

        private T GetValue()
        {
            if (IsString)
            {
                return (T)((object)GetRawValue());
            } 
            else if (IsBool)
            {
                string value = GetRawValue();
                if (value.HasContent()) value = value.ToLower();
                return (T)((object)(value == "true" || value == "1" || value == "on"));
            }
            else
            {
                Type type = typeof(T);
                string queryStringValue = GetRawValue();

                System.Reflection.MethodInfo methodParse = type.GetMethod("Parse", new[] { typeof(string) });
                try
                {
                    return (T)methodParse.Invoke(null, new object[] { queryStringValue });
                }
                catch
                {
                    return GetMinValue();
                }
            }
        }

        private string GetRawValue()
        {
            string queryStringValue = Data[Key];
            if (queryStringValue.HasContent() && IsEncrypted)
            {
                Encryption.TryDeHex(queryStringValue, out queryStringValue);
            }
            return queryStringValue;
        }

        public static string Encrypt(string s)
        {
            return Encryption.Hex(s);
        }
    }
}
