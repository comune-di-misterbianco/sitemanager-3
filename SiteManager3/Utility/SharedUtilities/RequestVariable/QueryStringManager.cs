﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Collections.Specialized;

namespace NetService.Utility.QueryStringManager
{
    public class QueryStringManager 
    {
        protected NameValueCollection QueryStringData
        {
            get
            {
                return _QueryStringData ?? (_QueryStringData = HttpContext.Current.Request.QueryString);
            }
        }
        private NameValueCollection _QueryStringData;

        public bool HasData
        {
            get
            {
                return QueryStringData.Count > 0;
            }
        }

        public QueryStringManager()
        {
        }

        public string this[string key]
        {
            get
            {
                return QueryStringData[key];
            }
        }

        public bool Contains(string key)
        {
            return QueryStringData[key] != null;
        }

        public string[] Keys
        {
            get
            {
                return QueryStringData.Keys.Cast<string>().ToArray();
            }
        }
    }
}
