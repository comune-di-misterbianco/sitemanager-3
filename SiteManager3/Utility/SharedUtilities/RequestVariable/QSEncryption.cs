﻿using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using System.Web;

namespace NetService.Utility.Request
{
    public class Encryption
    {        
        public static bool TryDeHex(string hexstring, out string returnValue)
        {
            try
            {
                returnValue = DeHex(hexstring);
                return true;
            }
            catch { returnValue = ""; }
            return false;
        }

        public static bool TryHex(string sdata, out string returnValue)
        {
            try
            {
                returnValue = Hex(sdata);
                return true;
            }
            catch { returnValue = ""; }
            return false;
        }

        public static string DeHex(string hexstring)
        {
            string ret = String.Empty;
            StringBuilder sb = new StringBuilder(hexstring.Length / 2);
            for (int i = 0; i <= hexstring.Length - 1; i = i + 2)
            {
                sb.Append((char)int.Parse(hexstring.Substring(i, 2), NumberStyles.HexNumber));
            }
            return sb.ToString();
        }

        public static string Hex(string sData)
        {
            string temp = String.Empty; ;
            string newdata = String.Empty;
            StringBuilder sb = new StringBuilder(sData.Length * 2);
            for (int i = 0; i < sData.Length; i++)
            {
                if ((sData.Length - (i + 1)) > 0)
                {
                    temp = sData.Substring(i, 2);
                    if (temp == @"\n") newdata += "0A";
                    else if (temp == @"\b") newdata += "20";
                    else if (temp == @"\r") newdata += "0D";
                    else if (temp == @"\c") newdata += "2C";
                    else if (temp == @"\\") newdata += "5C";
                    else if (temp == @"\0") newdata += "00";
                    else if (temp == @"\t") newdata += "07";
                    else
                    {
                        sb.Append(String.Format("{0:X2}", (int)(sData.ToCharArray())[i]));
                        i--;
                    }
                }
                else
                {
                    sb.Append(String.Format("{0:X2}", (int)(sData.ToCharArray())[i]));
                }
                i++;
            }
            return sb.ToString();
        }
    }
}