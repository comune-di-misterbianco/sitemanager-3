﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Collections.Specialized;

namespace NetService.Utility.Request
{
    public class QSVar<T> : RequestVariable<T>
    {
        protected override NameValueCollection Data
        {
            get
            {
                return _QueryStringData ?? (_QueryStringData = HttpContext.Current.Request.QueryString);
            }
        }
        private NameValueCollection _QueryStringData;

        public QSVar(string key)
            : base(key)
        {
        }
        public QSVar(string key, bool isEncrypted)
            : base(key, isEncrypted)
        {}
    }
}
