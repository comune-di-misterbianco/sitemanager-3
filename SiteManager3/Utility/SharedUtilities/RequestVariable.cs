using System;
using System.Collections.Specialized;
using System.Web;

namespace NetService.Utility.Common
{
    /// <summary>
    /// Permette di leggere i valori di HTTP valori HTTP inviati da un client durante una richiesta Web. 
    /// tramite la classe System.Web.HttpRequest
    /// </summary>
    public class RequestVariable
    {
        private DateTime NullDate = new DateTime(0001, 1, 1);
        private TimeSpan NullTimeSpan = new TimeSpan(0, 0, 0);
        private string RequestedVariable;
        /// <summary>
        /// Restituisce il valore della variabile HTTP
        /// </summary>
        public string OriginalValue
        {
            get
            {
                return RequestedVariable;
            }
        }
        /// <summary>
        /// Lista del tipo di variabile
        /// </summary>
        public enum VariableType
        {
            Integer,
            String,
            Date,
            DateExact,
            Double,
            Decimal,
            TimeSpan
        }

        /// <summary>
        /// Tipo di collezione di variabili HTTP in cui cercare la variabile
        /// </summary>
        public enum RequestType
        {
            Form,
            QueryString,
            Form_QueryString,
            QueryString_Form        
        }
        private string _Key;
        /// <summary>
        /// Chiave con cui cercare la variabile
        /// </summary>
        public string Key
        {
            get { return _Key; }
        }
	

        private bool ValidContent;
        /// <summary>
        /// Inizializza un'istanza della classe.
        /// </summary>
        /// <param name="key">Chiave con cui cercare la variabile</param>
        public RequestVariable(string key)
            :this(key,RequestType.Form)
        {
           
        }

        /// <summary>
        /// Inizializza un'istanza della classe.
        /// </summary>
        /// <param name="key">Chiave con cui cercare la variabile</param>
        /// <param name="Request">Oggetto HttpRequest contenente la variabile </param>
        public RequestVariable(string key,HttpRequest Request)
        {
            _Key = key;
            if (Request != null && Request.Form != null && Request.Form[key] != null && key != null && key.Length > 0)
            {
                RequestedVariable = Request.Form[key];
                ValidContent = true;
            }
            else
                if (Request != null && Request.QueryString != null && Request.QueryString[key] != null && key != null && key.Length > 0)
                {
                    RequestedVariable = Request.QueryString[key];
                    ValidContent = true;
                }
                else
                    ValidContent = false;
        }
        /// <summary>
        /// Inizializza un'istanza della classe.
        /// </summary>
        /// <param name="key">Chiave con cui cercare la variabile</param>
        /// <param name="requestType">Tipo di collezione di variabili HTTP</param>
        public RequestVariable(string key, RequestType requestType)
        {
            _Key = key;
            #region RequestType.Form
            if (requestType == RequestType.Form)
            {
                NameValueCollection Coll = HttpContext.Current.Request.Form;
                if (Coll != null && Coll[key] != null && key != null && key.Length > 0)
                {
                    RequestedVariable = Coll[key];
                    ValidContent = true;
                }
                else
                    ValidContent = false;
            } 
            #endregion

            #region RequestType.QueryString
            if (requestType == RequestType.QueryString)
            {
                NameValueCollection Coll = HttpContext.Current.Request.QueryString;
                if (Coll != null && Coll[key] != null && key != null && key.Length > 0)
                {
                    RequestedVariable = Coll[key];
                    ValidContent = true;
                }
                else
                    ValidContent = false;
            }
            #endregion

            #region RequestType.Form_QueryString

            if (requestType == RequestType.Form_QueryString)
            {
                HttpRequest Request = HttpContext.Current.Request;
                if (Request != null && Request.Form != null && Request.Form[key] != null && key != null && key.Length > 0)
                {
                    RequestedVariable = Request.Form[key];
                    ValidContent = true;
                }
                else
                    if (Request != null && Request.QueryString != null && Request.QueryString[key] != null && key != null && key.Length > 0)
                    {
                        RequestedVariable = Request.QueryString[key];
                        ValidContent = true;
                    }
                    else
                        ValidContent = false;
            }

            #endregion

            #region RequestType.QueryString_Form

            if (requestType == RequestType.QueryString_Form)
            {
                HttpRequest Request = HttpContext.Current.Request;
                if (Request != null && Request.QueryString != null && Request.QueryString[key] != null && key != null && key.Length > 0)
                {
                    RequestedVariable = Request.QueryString[key];

                    ValidContent = true;
                }
                else
                    if (Request != null && Request.Form != null && Request.Form[key] != null && key != null && key.Length > 0)
                    {
                        RequestedVariable = Request.Form[key];
                        ValidContent = true;
                    }
                    else
                        ValidContent = false;
            }

            #endregion
        }  
        
        /// <summary>
        /// Inizializza un'istanza della classe.
        /// </summary>
        /// <param name="key">Chiave con cui cercare la variabile</param>
        /// <param name="Coll">collezione di variabili</param>
        public RequestVariable(string key, NameValueCollection Coll)
        {
            _Key = key;
            if (Coll != null && Coll[key] != null && key != null && key.Length > 0)
            {
                RequestedVariable = Coll[key];
                ValidContent = true;
            }
            else
                ValidContent = false;
        }

        /// <summary>
        /// Indica se � avvenuta una postback.
        /// </summary>
        public bool IsPostBack
        {
            get
            {
                if (HttpContext.Current.Request.Form[this.Key] != null)
                    return true;
                return false;
            }
        }

        /// <summary>
        /// Indica se il valore della variabile � un intero.
        /// </summary>
        public bool IsValidInteger
        {
            get
            {
                return IsValid(VariableType.Integer);
            }
        }

        /// <summary>
        /// Indica se il valore della variabile � una stringa.
        /// </summary>
        public bool IsValidString
        {
            get
            {
                return IsValid(VariableType.String);
            }
        }

        /// <summary>
        /// Indica se il valore della variabile � un Double.
        /// </summary>
        public bool IsValidDouble
        {
            get
            {
                return IsValid(VariableType.Double);
            }
        }

        /// <summary>
        /// Indica se il valore della variabile � un decimal.
        /// </summary>
        public bool IsValidDecimal
        {
            get
            {
                return IsValid(VariableType.Decimal);
            }
        }

        /// <summary>
        /// Indica se il valore della variabile � una stringa valida per inserimento su db.
        /// </summary>
        public bool IsValidDatabaseString
        {
            get
            {
                return IsValid(VariableType.String);
            }
        }

        /// <summary>
        /// Indica se il valore della variabile � valido come TimeSpan 
        /// </summary>
        public bool IsValidTimeSpan
        {
            get
            {
                return IsValid(VariableType.TimeSpan);
            }
        }

        /// <summary>
        /// Indica se il valore della variabile � valido come stringa.
        /// </summary>
        public bool IsValid()
        {
            return IsValid(VariableType.String);
        }

        /// <summary>
        /// Indica se il valore della variabile � del tipo specificato.
        /// </summary>
        /// <param name="type">Tipo di variabile con cui confrontare il valore della variabile</param>
        /// <returns>Indica se la variabile � del tipo indicato</returns>
        public bool IsValid(VariableType type, string format = null)
        {
            switch (type)
            {
                case VariableType.Date:
                    GetAsDate();
                    return DateParsed;
                case VariableType.DateExact:
                    GetAsDateExact(format);
                    return DateParsed;
                case VariableType.String:
                    GetAsString();
                    return StringParsed;
                case VariableType.Integer:
                    GetAsInt();
                    return IntParsed;
                case VariableType.Double:
                    GetAsDouble();
                    return DoubleParsed;
                case VariableType.Decimal:
                    GetAsDecimal();
                    return DecimalParsed;
                case VariableType.TimeSpan:
                    GetAsTimeSpan();
                    return TimeSpanParsed;
            }
            return false;
        }

        #region Int
        /// <summary>
        /// Restituisce il valore della variabile come tipo int.
        /// </summary>
        /// <returns></returns>
        public int GetAsInt()
        {
            int value = 0;
            IntParsed = int.TryParse(OriginalValue, out value);
            return value;
        }
        private bool IntParsed;

        /// <summary>
        /// Restituisce il valore della variabile come tipo int.
        /// </summary>
        /// <returns></returns>
        public int IntValue
        {
            get
            {
                if (_IntValue == 0)
                    _IntValue = GetAsInt();
                return _IntValue;
            }

        }
        private int _IntValue;
        #endregion

        #region Double
        /// <summary>
        /// Restituisce il valore della variabile come tipo double.
        /// </summary>
        /// <returns></returns>
        public double GetAsDouble()
        {
            double value = 0;
            DoubleParsed = double.TryParse(OriginalValue, out value);
            return value;
        }
        private bool DoubleParsed;

        /// <summary>
        /// Restituisce il valore della variabile come tipo double.
        /// </summary>
        /// <returns></returns>
        public double DoubleValue
        {
            get
            {
                if (_DoubleValue == 0)
                    _DoubleValue = GetAsDouble();
                return _DoubleValue;
            }

        }
        private double _DoubleValue;
        #endregion

        #region Decimal
        /// <summary>
        /// Restituisce il valore della variabile come tipo decimal.
        /// </summary>
        /// <returns></returns>
        public decimal GetAsDecimal()
        {
            decimal value = 0;
            DecimalParsed = decimal.TryParse(OriginalValue, out value);
            return value;
        }
        private bool DecimalParsed;

        /// <summary>
        /// Restituisce il valore della variabile come tipo double.
        /// </summary>
        /// <returns></returns>
        public decimal DecimalValue
        {
            get
            {
                if (_DecimalValue == 0)
                    _DecimalValue = GetAsDecimal();
                return _DecimalValue;
            }

        }
        private decimal _DecimalValue;
        #endregion

        #region String

        /// <summary>
        /// Restituisce il valore della variabile come tipo string.
        /// </summary>
        /// <returns></returns>
        public string GetAsString()
        {
            StringParsed = OriginalValue != null;
            if (StringParsed)
                return OriginalValue.Trim().Replace("'", "''");
            else 
                return "";
        }
        private bool StringParsed;

        /// <summary>
        ///  Restituisce il valore della variabile come tipo string trimmed senza effettuare replace di caratteri
        /// </summary>
        /// <returns>Se il valore recuperato � null torna "" altrimenti il valore informato string</returns>
        public string GetAsStringNonFiltered()
        {
            StringParsed = OriginalValue != null;
            if (StringParsed)
                return OriginalValue.Trim();
            else
                return "";
        }

        /// <summary>
        /// Restituisce il valore della variabile come tipo string.
        /// </summary>
        /// <returns></returns>
        public string StringValue
        {
            get
            {
                if (_StringValue == null)
                    _StringValue = GetAsString();
                return _StringValue;
            }

        }
        private string _StringValue;

        /// <summary>
        /// Restituisce il valore della variabile come tipo string trimmed e senza replace.
        /// </summary>
        /// <returns></returns>
        public string StringValueNoReplace
        {
            get
            {
                if (_StringValueNoReplace == null)
                    _StringValueNoReplace = GetAsStringNonFiltered();
                return _StringValueNoReplace;
            }

        }
        private string _StringValueNoReplace;

        #endregion

        #region DatabaseString

        /// <summary>
        /// Restituisce il valore della variabile come tipo string.
        /// </summary>
        /// <returns></returns>
        public string GetDatabaseAsString()
        {
            StringParsed = OriginalValue != null;
            if (StringParsed)
                return OriginalValue.Trim().Replace("'", "''");
            else
                return "";
        }
        private bool DatabaseStringParsed;
        /// <summary>
        /// Restituisce il valore della variabile come tipo string.
        /// </summary>
        /// <returns></returns>
        public string DatabaseStringValue
        {
            get
            {
                if (_DatabaseStringValue == null)
                    _DatabaseStringValue = GetDatabaseAsString();
                return _DatabaseStringValue;
            }

        }
        private string _DatabaseStringValue;

        #endregion

        #region TimeSpan
        /// <summary>
        /// Restituisce il valore della variabile come tipo TimeSpan
        /// </summary>
        /// <returns></returns>
        private TimeSpan GetAsTimeSpan()
        {
            TimeSpan value = new TimeSpan(1,1,1);
            TimeSpanParsed = OriginalValue!=null && OriginalValue.Trim().Length > 0 && TimeSpan.TryParse(OriginalValue, out value);
            if(!TimeSpanParsed)
                value = new TimeSpan(1,1,1);
            return value;
        }
        private bool TimeSpanParsed;

        /// <summary>
        /// Restituisce il valore della variabile come tipo TimeSpan
        /// </summary>
        public TimeSpan TimeSpanValue
        {
            get
            {
                if (_TimeSpanValue == null || _TimeSpanValue == NullTimeSpan)
                    _TimeSpanValue = GetAsTimeSpan();
                return _TimeSpanValue;
            }

        }
        private TimeSpan _TimeSpanValue;

        #endregion

        #region DateTime
        /// <summary>
        /// Restituisce il valore della variabile come tipo DateTime.
        /// </summary>
        /// <returns></returns>
        private DateTime GetAsDate()
        {
            DateTime value = new DateTime(1, 1, 1);
            DateParsed = OriginalValue!=null && OriginalValue.Trim().Length > 0 && DateTime.TryParse(OriginalValue, out value);
            if (!DateParsed)
                value = new DateTime(1, 1, 1);
            return value;
        }
       
        /// <summary>
        /// Restituisce il valore della variabile come tipo DateTime usando il TryParseExect
        /// </summary>
        /// <param name="dateFormat">Formato della data da testare</param>
        /// <returns>Restituisce il valore della data</returns>
        private DateTime GetAsDateExact(string dateFormat)
        {
            DateTime value = new DateTime(1, 1, 1);           
            DateParsed = OriginalValue != null && OriginalValue.Trim().Length > 0 && DateTime.TryParseExact(OriginalValue, dateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out value);
            if (!DateParsed)
               value = new DateTime(1, 1, 1);         
           
            return value;
        }


        private bool DateParsed;
        /// <summary>
        /// Restituisce il valore della variabile come tipo DateTime.
        /// </summary>
        /// <returns></returns>
        public DateTime DateTimeValue
        {
            get
            {
                if (_DateTimeValue == null || _DateTimeValue == NullDate)
                    _DateTimeValue = GetAsDate();
                return _DateTimeValue;
            }

        }
        private DateTime _DateTimeValue;

        #endregion



        /// <summary>
        /// Restituisce il valore della variabile come tipo string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (IsValid())
                return StringValue;
            else return "";
        }
    }
    /// <summary>
    /// Restituisce un oggetto di tipo G2Core.common.RequestVariable specifico per la collezione di variabili 
    /// HTTP Form.
    /// </summary>
    public class FormRequestVariable:RequestVariable
    {
        /// <summary>
        /// Inizializza una nuova istanza della classe.
        /// </summary>
        /// <param name="key">Chiave relativa alla variabile.</param>
        public FormRequestVariable(string key)
            : base(key, RequestType.Form)
        {

        }
       
    }

    /// <summary>
    /// Restituisce un oggetto di tipo G2Core.common.RequestVariable specifico per la collezione di variabili 
    /// HTTP QueryString.
    /// </summary>
    public class QueryStringRequestVariable : RequestVariable
    {
        /// <summary>
        /// Inizializza una nuova istanza della classe.
        /// </summary>
        /// <param name="key">Chiave relativa alla variabile.</param>
        public QueryStringRequestVariable(string key)
            : base(key, RequestType.QueryString)
        {

        }
 
    }
}
