﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Collections.Specialized;
using System.Collections;

namespace NetService.Utility.Context
{
    public class ContextApplicationProperty<T>:ContextPropertyBase<T>
    {
        public override bool  RepositoryExists
        {
	        get 
            {
                return HttpContext.Current != null && HttpContext.Current.Session != null;
            }
        }

        public ContextApplicationProperty(string key)
            : base(key)
        {
        }

        protected override object GetValueFromRepository()
        {
            if (!RepositoryExists)
                throw new InvalidOperationException("The HttpContext.Current.Items repository doesn't exists.");

            return HttpContext.Current.Application[Key];
        }

        protected override void SetValueOnRepository(object value)
        {
            if (!RepositoryExists)
                throw new InvalidOperationException("The HttpContext.Current.Items repository doesn't exists.");

            HttpContext.Current.Application[Key] = value;
        }

        protected override bool ValueExistsInRepository()
        {
            if (!RepositoryExists)
                throw new InvalidOperationException("The HttpContext.Current.Items repository doesn't exists.");

            return HttpContext.Current.Application[Key] != null;
        }
    }
}
