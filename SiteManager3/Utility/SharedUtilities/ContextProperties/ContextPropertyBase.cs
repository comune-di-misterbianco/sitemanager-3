﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Collections.Specialized;
using System.Collections;

namespace NetService.Utility.Context
{
    public abstract class ContextPropertyBase<T>
    {
        public bool Exists
        {
            get
            {
                return RepositoryExists && ValueExistsInRepository() && typeof(T).IsAssignableFrom(GetValueFromRepository().GetType());
            }
        }
        
        public abstract bool RepositoryExists
        {
            get;
        }

        public string Key { get; private set; }

        public ContextPropertyBase(string key)
        {
            this.Key = key;
        }

        public T Value
        {
            get
            {
                if (Exists)
                    return (T)GetValueFromRepository();
                else throw new InvalidOperationException("The given key was not found on the dictionary.");
            }
            set
            {
                if (RepositoryExists)
                    SetValueOnRepository(value);
                else throw new InvalidOperationException("The selected repository doesn't exists.");
            }
        }

        protected abstract void SetValueOnRepository(object value);
        protected abstract object GetValueFromRepository();
        protected abstract bool ValueExistsInRepository();
    }
}
