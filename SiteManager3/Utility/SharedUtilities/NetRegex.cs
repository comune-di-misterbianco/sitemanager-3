﻿using System.Text.RegularExpressions;

namespace SharedUtilities
{
    /// <summary>
    /// Classe utilizzabile come repository per espressioni regolari
    /// </summary>
    public static class NetRegex
    {
        /// <summary>
        /// Espressione regolare per controllo caratteri speciali
        /// </summary>
        public static Regex NotValidChars
        {
            get
            {
                return new Regex(@"^[_àèéìòù0-9a-zA-Z\'\,\.\- ]*$",
                     System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace);
            }
        }

        /// <summary>
        /// Espressione regolare per controllo codici meccanografici istituti
        /// </summary>
        public static Regex NotValidCodiceMeccanografico
        {
            get
            {
                //  \d indica tutti i numeri mentre \w equivale a [0-9A-Za-z]
                return new Regex(@"^([a-zA-Z]{4})+(\d{5}[\w{1}])*$",
                     System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace);
            }
        }

        /// <summary>
        /// Espressione regolare per controllo anni scolastici nella forma 2000/2001
        /// </summary>
        public static Regex ValidAnnoScolastico
        {
            get
            {
                return new Regex(@"^(\d{4}[/]\d{4})$",
                 System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace);
            }
        }


        /// <summary>
        /// Espressione regolare per controllo valore
        /// </summary>
        public static Regex ValidTesseraSanitariaNumber
        {
            get
            {
                return new Regex(@"^[0-9]{20}$",
                 System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace);
            }
        }

        /// <summary>
        /// Espressione regolare per controllo di un custom UserID
        /// Controlla lettere, numeri, trattino e undescore
        /// massima lunghezza di 15 caratteri di cui i primi 3 devono essere lettere.
        /// </summary>
        public static Regex ValidCustomUserID
        {
            get
            {
                return new Regex(@"^[a-z0-9_-]{3,15}$");

            }
        }

        /// <summary>
        /// Espressione Regolare per controllo Nomi e Cognomi
        /// </summary>
        public static Regex ValidNameLastName
        {
            get
            {
                return new Regex(@"[a-zA-Z ,.'-]+");
            }
        }

        /// <summary>
        /// Espressione Regolare per controllo Nomi e Cognomi Internazionali
        /// </summary>
        public static Regex ValidNameLastNameInternational
        {
            get
            {
                return new Regex(@"/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u");
            }
        }


       
        /// <summary>
        /// Espressione Regolare per controllare decimali positivi
        /// </summary>
        public static Regex ValidPositiveDecimalNumbers
        {
            get
            {
                return new Regex(@"/^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/");
            }
        }                
    }
}
