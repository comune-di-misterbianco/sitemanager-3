﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace SharedUtilities
{
    public static class EnumUtilities
    {
        /// <summary>
        /// Restituisce la stringa settata come descrizione per l'enumerativo mediante System.ComponenteModel.DescriptionAttribute, se non la trova
        /// restituisce il valore in formato stringa
        /// </summary>
        /// <param name="value">Valore dell'enumerativo</param>
        /// <returns>Descrizione in formato stringa</returns>
        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }


        /// <summary>
        /// Restituisce il valore del generico enumerativo a partire dalla sua descrizione, impostata con System.ComponenteModel.DescriptionAttribute
        /// </summary>
        /// <typeparam name="T">Generico enumerativo</typeparam>
        /// <param name="description">Stringa descrittiva</param>
        /// <returns>Valore Enumerativo</returns>
        public static T GetEnumValueFromDescription<T>(string description)
        {
            var type = typeof(T);
            if (!type.IsEnum)
                throw new ArgumentException();
            FieldInfo[] fields = type.GetFields();
            var field = fields
                            .SelectMany(f => f.GetCustomAttributes(
                                typeof(DescriptionAttribute), false), (
                                    f, a) => new { Field = f, Att = a })
                            .Where(a => ((DescriptionAttribute)a.Att)
                                .Description == description).SingleOrDefault();
            return field == null ? default(T) : (T)field.Field.GetRawConstantValue();
        }



        /// <summary>
        /// Restituisce il valore del generico enumerativo a partire da una stringa
        /// </summary>
        /// <typeparam name="T">Generico Enumerativo</typeparam>
        /// <param name="enumString">Stringa Descrittiva</param>
        /// <returns>Valore Enumerativo</returns>
        public static T ToEnum<T>(this string enumString)
        {
            return (T)Enum.Parse(typeof(T), enumString);
        }


        /// <summary>
        /// Restituisce il valore del generico enumerativo a partire dalla description del singolo valore enumerativo
        /// </summary>
        /// <typeparam name="T">Generico Enumerativo</typeparam>
        /// <param name="decriptionString">Description string</param>
        /// <returns>Valore Enumerativo</returns>
        public static T ToEnumFromDescription<T>(this string decriptionString)
        {
            var type = typeof(T);
            if (!type.IsEnum)
                throw new ArgumentException();
            FieldInfo[] fields = type.GetFields();
            var field = fields
                            .SelectMany(f => f.GetCustomAttributes(
                                typeof(DescriptionAttribute), false), (
                                    f, a) => new { Field = f, Att = a })
                            .Where(a => ((DescriptionAttribute)a.Att)
                                .Description == decriptionString).SingleOrDefault();
            return field == null ? default(T) : (T)field.Field.GetRawConstantValue();
        }


        /// <summary>
        /// Metodo Cher ritorna la description direttamnte dall'enumeratore
        /// </summary>
        /// <param name="value">Valore Enumerativo</param>
        /// <returns>Description del valore enumerativo</returns>
        public static string ToDescriptionString(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static TAttribute GetAttribute<TAttribute>(this Enum value) where TAttribute : Attribute
        {
            var type = value.GetType();
            var name = Enum.GetName(type, value);
            return type.GetField(name) // I prefer to get attributes this way
                .GetCustomAttributes(false)
                .OfType<TAttribute>()
                .SingleOrDefault();
        }
    }
}
