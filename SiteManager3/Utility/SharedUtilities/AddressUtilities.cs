﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace SharedUtilities
{
    public static class AddressUtilities
    {
        public static string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        //public static bool IsLocalIpAddress(string host)
        //{
        //    try
        //    { // get host IP addresses
        //        IPAddress[] hostIPs = Dns.GetHostAddresses(host);
        //        // get local IP addresses
        //        IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());

        //        // test if any host IP equals to any local IP or to localhost
        //        foreach (IPAddress hostIP in hostIPs)
        //        {
        //            // is localhost
        //            if (IPAddress.IsLoopback(hostIP)) return true;
        //            // is local address
        //            foreach (IPAddress localIP in localIPs)
        //            {
        //                if (hostIP.Equals(localIP)) return true;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
            
        //    }
        //    return false;
        //}

        public static bool IsLocalIpAddress(string ipAddress)
        {
            // http://en.wikipedia.org/wiki/Private_network
            // Private IP Addresses are: 
            //  24-bit block: 10.0.0.0 through 10.255.255.255
            //  20-bit block: 172.16.0.0 through 172.31.255.255
            //  16-bit block: 192.168.0.0 through 192.168.255.255
            //  Link-local addresses: 169.254.0.0 through 169.254.255.255 (http://en.wikipedia.org/wiki/Link-local_address)

            var ip = IPAddress.Parse(ipAddress);
            var octets = ip.GetAddressBytes();

            var is24BitBlock = octets[0] == 10;
            if (is24BitBlock) return true; // Return to prevent further processing

            var is20BitBlock = octets[0] == 172 && octets[1] >= 16 && octets[1] <= 31;
            if (is20BitBlock) return true; // Return to prevent further processing

            var is16BitBlock = octets[0] == 192 && octets[1] == 168;
            if (is16BitBlock) return true; // Return to prevent further processing

            var isLinkLocalAddress = octets[0] == 169 && octets[1] == 254;
            return isLinkLocalAddress;
        }
    }
}
