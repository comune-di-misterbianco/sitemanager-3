﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SharedUtilities
{
    public class WebControlToString
    {
        #region Control2Html
        public string ControltoHtml(WebControl temp)
        {
            if (temp == null)
                return "";
            else
            {
                TextWriter tw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(tw);
                //if (temp.Controls.Count > 0)
                //{
                // // temp.Render()
                //}
                temp.RenderControl(hw);
                return tw.ToString();
            }
        }


        public string ControltoHtml(Control temp)
        {
            if (temp == null)
                return "";
            else
            {
                TextWriter tw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(tw);
                temp.RenderControl(hw);
                return tw.ToString();
            }
        }
        #endregion
    }
}
