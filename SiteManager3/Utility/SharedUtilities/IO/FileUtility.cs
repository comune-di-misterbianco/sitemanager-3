﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace SharedUtilities.IO
{
    public static class FileUtility
    {
        public static int GetSizeInKB(string absoluteFilePath)
        {
            int size = -1;
            try
            {
                FileInfo fi = new FileInfo(absoluteFilePath);
                long sizeInBytes = fi.Length;

                if (sizeInBytes > 0)
                    size = (int)(sizeInBytes / 1024);
            }
            catch
            {
                
            }
            return size;
            
        }
    }
}
