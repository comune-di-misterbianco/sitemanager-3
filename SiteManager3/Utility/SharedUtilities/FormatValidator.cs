using System.Text.RegularExpressions;

namespace NetService.Utility.Common
{
    public class FormatValidator
    {
        public static bool ValidateEmail(string email)
        {
            //string ex = @"^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$";
            string ex = @"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$";
            Regex reg = new Regex(ex);
            return reg.IsMatch(email);
        }

        public static bool ValidateTextForDB(string text)
        {
            return true;
        }

        public static string FilterTextForDB(string text)
        {
            return text;
        }

        public static bool ValidateCodiceFiscale(string CodiceFiscale)
        {
            if (CodiceFiscale == null || CodiceFiscale.Length != 16)
                return false;

            //Questo array contiene 8 Regex rappresentanti le 8 possibili combinazioni di caratteri alfanumerici che pu� avere un codice fiscale, tenendo conto dei controlli di omocodia.
            //Se le validazione di almeno una di queste regex va a buon fine il codice fiscale � valido
           string[] regexOmocodia = {
                                    @"^[A-Za-z]{6}[0-9]{2}[A-Za-z][0-9]{2}[A-Za-z][0-9]{3}[A-Za-z]$",
                                    @"^[A-Za-z]{6}[0-9]{2}[A-Za-z][0-9]{2}[A-Za-z][0-9]{2}[LMNPQRSTUVlmnpqrstuv]{1}[A-Za-z]$",
                                    @"^[A-Za-z]{6}[0-9]{2}[A-Za-z][0-9]{2}[A-Za-z][0-9]{1}[LMNPQRSTUVlmnpqrstuv]{2}[A-Za-z]$",
                                    @"^[A-Za-z]{6}[0-9]{2}[A-Za-z][0-9]{2}[A-Za-z][LMNPQRSTUVlmnpqrstuv]{3}[A-Za-z]$",
                                    @"^[A-Za-z]{6}[0-9]{2}[A-Za-z][0-9]{1}[LMNPQRSTUVlmnpqrstuv]{1}[A-Za-z][LMNPQRSTUVlmnpqrstuv]{3}[A-Za-z]$",
                                    @"^[A-Za-z]{6}[0-9]{2}[A-Za-z][LMNPQRSTUVlmnpqrstuv]{2}[A-Za-z][LMNPQRSTUVlmnpqrstuv]{3}[A-Za-z]$",
                                    @"^[A-Za-z]{6}[0-9]{1}[LMNPQRSTUVlmnpqrstuv]{1}[A-Za-z][LMNPQRSTUVlmnpqrstuv]{2}[A-Za-z][LMNPQRSTUVlmnpqrstuv]{3}[A-Za-z]$",
                                    @"^[A-Za-z]{6}[LMNPQRSTUVlmnpqrstuv]{2}[A-Za-z][LMNPQRSTUVlmnpqrstuv]{2}[A-Za-z][LMNPQRSTUVlmnpqrstuv]{3}[A-Za-z]$",
                                    @"^[A-Za-z]{6}[0-9LMNPQRSTUV]{2}[ABCDEHLMPRST]{1}[0-9LMNPQRSTUV]{2}[A-Za-z]{1}[0-9LMNPQRSTUV]{3}[A-Za-z]{1}$"
                                    };
            foreach (string regStr in regexOmocodia)
            {
                Regex rgx = new Regex(regStr);
                Match m = rgx.Match(CodiceFiscale);
                if (m.Success) return true;
            }

            return false;
        }

        public static bool ValidatePartitaIVA(string PartitaIVA)
        {
            if (PartitaIVA == null || PartitaIVA.Length != 11)
                return false;

            string regex = @"^[0-9]{11}$";

            Regex rgx = new Regex(regex);
            Match m = rgx.Match(PartitaIVA);
            if (m.Success) return true;
            
            return false;
        }
    }
}