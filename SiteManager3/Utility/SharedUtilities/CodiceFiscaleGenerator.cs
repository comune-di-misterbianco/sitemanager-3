﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetService.Utilities
{
    public static class CodiceFiscaleUtilities
    {

        /// <summary>
        /// Caratteri accentati e relative traslitterazioni secondo la circolare n.1 del 22 Gennaio 2008
        /// </summary>
        public static IDictionary<string, string> CaratteriAccentati
        {
            get
            {
                if (_CaratteriAccentati == null)
                {
                    _CaratteriAccentati = new Dictionary<string, string>();
                    _CaratteriAccentati.Add("à", "A");
                    _CaratteriAccentati.Add("á", "A");
                    _CaratteriAccentati.Add("À", "A");
                    _CaratteriAccentati.Add("Á", "A");
                    _CaratteriAccentati.Add("È", "E");
                    _CaratteriAccentati.Add("É", "E");
                    _CaratteriAccentati.Add("è", "E");
                    _CaratteriAccentati.Add("é", "E");
                    _CaratteriAccentati.Add("ì", "I");
                    _CaratteriAccentati.Add("í", "I");
                    _CaratteriAccentati.Add("Ì", "I");
                    _CaratteriAccentati.Add("Í", "I");
                    _CaratteriAccentati.Add("ò", "O");
                    _CaratteriAccentati.Add("ó", "O");
                    _CaratteriAccentati.Add("Ò", "O");
                    _CaratteriAccentati.Add("Ó", "O");
                    _CaratteriAccentati.Add("ù", "U");
                    _CaratteriAccentati.Add("ú", "U");
                    _CaratteriAccentati.Add("Ù", "U");
                    _CaratteriAccentati.Add("Ú", "U");


                }
                return _CaratteriAccentati;
            }
        }
        private static IDictionary<string, string> _CaratteriAccentati;


        /// <summary>
        /// Caratteri diacritici e relative traslitterazioni secondo la circolare n.1 del 22 Gennaio 2008
        /// </summary>
        public static IDictionary<string, string> CaratteriDiacritici
        {
            get
            {
                if (_CaratteriDiacritici == null)
                {
                    _CaratteriDiacritici = new Dictionary<string, string>();
                    _CaratteriDiacritici.Add("Â", "A");
                    _CaratteriDiacritici.Add("â", "A");
                    _CaratteriDiacritici.Add("Ä", "AE");
                    _CaratteriDiacritici.Add("ä", "AE");
                    //_CaratteriDiacritici.Add("à", "A");
                    //_CaratteriDiacritici.Add("á", "A");
                    //_CaratteriDiacritici.Add("À", "A");
                    //_CaratteriDiacritici.Add("Á", "A");
                    _CaratteriDiacritici.Add("Æ", "AE");
                    _CaratteriDiacritici.Add("æ", "AE");
                    _CaratteriDiacritici.Add("ç", "C");
                    _CaratteriDiacritici.Add("Ç", "C");
                    //_CaratteriDiacritici.Add("È", "E");
                    //_CaratteriDiacritici.Add("É", "E");
                    _CaratteriDiacritici.Add("Ê", "E");
                    _CaratteriDiacritici.Add("Ë", "E");
                    _CaratteriDiacritici.Add("ë", "E");
                    _CaratteriDiacritici.Add("ê", "E");
                    //_CaratteriDiacritici.Add("è", "E");
                    //_CaratteriDiacritici.Add("é", "E");
                    //_CaratteriDiacritici.Add("ì", "I");
                    //_CaratteriDiacritici.Add("í", "I");
                    _CaratteriDiacritici.Add("î", "I");
                    _CaratteriDiacritici.Add("ï", "I");
                    //_CaratteriDiacritici.Add("Ì", "I");
                    //_CaratteriDiacritici.Add("Í", "I");
                    _CaratteriDiacritici.Add("Î", "I");
                    _CaratteriDiacritici.Add("Ï", "I");
                    //_CaratteriDiacritici.Add("ò", "O");
                    //_CaratteriDiacritici.Add("ó", "O");
                    _CaratteriDiacritici.Add("ô", "O");
                    _CaratteriDiacritici.Add("ö", "OE");
                    //_CaratteriDiacritici.Add("Ò", "O");
                    //_CaratteriDiacritici.Add("Ó", "O");
                    _CaratteriDiacritici.Add("Ô", "O");
                    _CaratteriDiacritici.Add("Ö", "OE");
                    _CaratteriDiacritici.Add("œ", "OE");
                    _CaratteriDiacritici.Add("Œ", "OE");
                    //_CaratteriDiacritici.Add("ù", "U");
                    //_CaratteriDiacritici.Add("ú", "U");
                    _CaratteriDiacritici.Add("û", "U");
                    _CaratteriDiacritici.Add("ü", "UE");
                    //_CaratteriDiacritici.Add("Ù", "U");
                    //_CaratteriDiacritici.Add("Ú", "U");
                    _CaratteriDiacritici.Add("Û", "U");
                    _CaratteriDiacritici.Add("Ü", "UE");
                    _CaratteriDiacritici.Add("Š", "S");
                    _CaratteriDiacritici.Add("š", "S");
                    _CaratteriDiacritici.Add("Ž", "Z");
                    _CaratteriDiacritici.Add("ž", "Z");
                    _CaratteriDiacritici.Add("ϐ", "SS");

                }
                return _CaratteriDiacritici;
            }
        }
        private static IDictionary<string, string> _CaratteriDiacritici;

        public static bool GetDatafromCodiceFiscale(string codicefiscale, out DateTime? birth, out string sesso, out string codCatastaleComune)
        {
            birth = null;
            sesso = "M";
            codCatastaleComune = "";
            string _day = "";
            string _month = "";
            string _year = "";
            string monthMap = "ABCDEHLMPRST";
            bool calculated = false;
            if (codicefiscale.Length == 16)
            {
                try
                {
                    _year = codicefiscale.Substring(6, 2);
                    if (DateTime.Now.Year >= Convert.ToInt16("20" + _year))
                        _year = "20" + _year;
                    else
                        _year = "19" + _year;

                    _month = (monthMap.IndexOf(codicefiscale.Substring(8, 1).ToUpper()) + 1).ToString();
                    _day = codicefiscale.Substring(9, 2);
                    if (Convert.ToInt16(_day) > 40)
                    {
                        _day = (Convert.ToInt16(_day) - 40).ToString();
                        sesso = "F";
                    }
                    birth = new DateTime(Convert.ToInt16(_year), Convert.ToInt16(_month), Convert.ToInt16(_day));
                    codCatastaleComune = codicefiscale.Substring(11, 4);
                    calculated = true;
                }
                catch (Exception ex)
                {

                    calculated = false;
                }

            }
            return calculated;
        }

        /// <summary>
        /// Effettua la traslitterazione dei caratteri diacritici presenti nel cognome o nel nome in allineamento alla circolare numero 34/E del 20/07/2011
        /// secondo i parametri stabiliti dalla circolare n.1 del 22 Gennaio 2008
        /// </summary>
        /// <param name="inputString">Cognome o Nome</param>
        /// <returns></returns>
        public static string TraslitterazioneCaratteriDiacritici(string inputString)
        {
            string returnString = "";

            int charIndex = 0;
            foreach (char c in inputString)
            {


                if (CaratteriDiacritici.ContainsKey(c.ToString()))
                    returnString += CaratteriDiacritici[c.ToString()];

                //Carattere accentato che non sia l'ultimo della stringa
                else if (CaratteriAccentati.ContainsKey(c.ToString()) && charIndex != (inputString.Length - 1))
                    returnString += CaratteriAccentati[c.ToString()];

                //Le vocali accentate che compaiono come ultima lettera del nome o del cognome, vengono traslitterate con le relative vocali senza accento, seguite dal segno apostrofo
                else if (CaratteriAccentati.ContainsKey(c.ToString()) && charIndex == (inputString.Length - 1))
                    returnString += CaratteriAccentati[c.ToString()] + "'";

                else
                    returnString += c;

                charIndex++;
            }

            return returnString;
        }
    }

    public class CodiceFiscaleGenerator
    {
        private string nome;
        private string cognome;
        private DateTime dataNascita;
        private bool isMale;
        private string codiceCatastaleComune;

        public string CodiceFiscale { get; private set; } 

        public string[] CodiciOmocodi { get; private set; }

        public CodiceFiscaleGenerator(string nome, string cognome, DateTime dataNascita, bool male, string codiceCatastaleComune)
        {
            //this.nome = nome;
            //this.cognome = cognome;
            this.nome = CodiceFiscaleUtilities.TraslitterazioneCaratteriDiacritici(nome);
            this.cognome = CodiceFiscaleUtilities.TraslitterazioneCaratteriDiacritici(cognome);
            this.dataNascita = dataNascita;
            this.isMale = male;
            this.codiceCatastaleComune = codiceCatastaleComune;

            this.CodiceFiscale = BuildCodiceFiscale();
            this.CodiciOmocodi = BuildCodiciOmocodi();
        }
        
        private bool FindString(string strVal, char cVal)
        {
            foreach (char c in strVal)
            {
                if (c == cVal)
                    return true;
            }
            return false;
        }

        private string ExtractChars(string val, bool bCognome)
        {
            string strVal = val;
            if (bCognome)
                strVal = val;
            string retValue = "";
            string strVoc = "AaEeIiOoUu";

            int numCon = 0;
            int numVoc = 0;

            for (int i = 0; i < strVal.Length; ++i)
            {
                if (!FindString(strVoc, strVal[i]) && strVal[i] != ' ' && strVal[i] != '\'')
                {
                    retValue += strVal[i];
                    ++numCon;
                }
                if (numCon >= 3)
                {
                    if (bCognome)
                    {
                        break;
                    }
                    else
                    {
                        for (int k = i + 1; k < strVal.Length; ++k)
                        {
                            if (!FindString(strVoc, strVal[k]) && strVal[k] != ' ' && strVal[i] != '\'')
                            {
                                retValue += strVal[k];
                                string strTemp = retValue;
                                retValue = strTemp.Substring(0, 1);
                                retValue += strTemp.Substring(strTemp.Length - 2, 2);
                                i = strVal.Length;
                                break;
                            }
                        }
                    }
                }
            }
            if (numCon < 3)
            {
                for (int i = 0; i < strVal.Length; ++i)
                {
                    if (FindString(strVoc, strVal[i]))
                    {
                        retValue += strVal[i];
                        ++numVoc;
                    }
                    if ((numCon + numVoc) >= 3)
                        break;
                }
            }
            if ((numCon + numVoc) < 3)
            {
                if ((numCon + numVoc) == 0)
                    retValue = "***";
                if ((numCon + numVoc) == 1)
                    retValue = "***";
                if ((numCon + numVoc) == 2)
                    retValue += 'X';
            }
            return retValue;
        }

        private string GetCharData(DateTime m_dtDataNasc, bool m_bSesso)
        {
            string retVal = m_dtDataNasc.Year.ToString();
            retVal = retVal.Substring(2, 2);
            switch (m_dtDataNasc.Month)
            {
                case 1:
                    retVal += 'A';
                    break;
                case 2:
                    retVal += 'B';
                    break;
                case 3:
                    retVal += 'C';
                    break;
                case 4:
                    retVal += 'D';
                    break;
                case 5:
                    retVal += 'E';
                    break;
                case 6:
                    retVal += 'H';
                    break;
                case 7:
                    retVal += 'L';
                    break;
                case 8:
                    retVal += 'M';
                    break;
                case 9:
                    retVal += 'P';
                    break;
                case 10:
                    retVal += 'R';
                    break;
                case 11:
                    retVal += 'S';
                    break;
                case 12:
                    retVal += 'T';
                    break;
            }
            if (m_bSesso)
            {
                string strTemp = m_dtDataNasc.Day.ToString();
                if (strTemp.Length <= 1)
                {
                    retVal += "0";
                    retVal += strTemp;
                }
                else
                    retVal += strTemp;
            }
            else
                retVal += (m_dtDataNasc.Day + 40).ToString();

            return retVal;
        }
        
        private char CodFiscControl(string strVal)
        {
            int pesi = 0;
            int[] arrPesi = {1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 1, 0, 5, 7, 9, 13, 15, 17, 19, 21,
		    2, 4, 18, 20, 11, 3, 6, 8, 12, 14, 16, 10, 22, 25, 24, 23};

            strVal = strVal.ToUpper();

            for (int i = 0; i < strVal.Length; ++i)
            {
                if (((i + 1) % 2) == 0)
                {
                    if (Convert.ToInt32(strVal[i]) >= Convert.ToInt32('0') && Convert.ToInt32(strVal[i]) <= Convert.ToInt32('9'))
                        pesi += Convert.ToInt32(strVal[i]) - Convert.ToInt32('0');
                    else if (Convert.ToInt32(strVal[i]) >= Convert.ToInt32('A') && Convert.ToInt32(strVal[i]) <= Convert.ToInt32('Z'))
                        pesi += Convert.ToInt32(strVal[i]) - Convert.ToInt32('A');
                }
                else
                {
                    if (Convert.ToInt32(strVal[i]) >= Convert.ToInt32('0') && Convert.ToInt32(strVal[i]) <= Convert.ToInt32('9'))
                        pesi += arrPesi[Convert.ToInt32(strVal[i]) - Convert.ToInt32('0')];
                    else if (Convert.ToInt32(strVal[i]) >= Convert.ToInt32('A') && Convert.ToInt32(strVal[i]) <= Convert.ToInt32('Z'))
                        pesi += arrPesi[Convert.ToInt32(strVal[i]) - Convert.ToInt32('A') + 10];
                }
            }
            return Convert.ToChar(65 + (pesi % 26));
        }

        public string BuildCodiceFiscale()
        {
            string retVal = ExtractChars(cognome, true) +
                            ExtractChars(nome, false) +
                            GetCharData(dataNascita, isMale) +
                            codiceCatastaleComune;

            retVal += CodFiscControl(retVal);

            return retVal.ToUpper();
        }

        public bool CheckCodiceFiscale(string codiceFiscale)
        {
            return codiceFiscale.ToUpper() == CodiceFiscale.ToUpper() || CodiciOmocodi.Any(x=>x.ToUpper() == codiceFiscale.ToUpper());
        }

        public string[] BuildCodiciOmocodi()
        {
            //0 = L | 1 = M | 2 = N | 3 = P | 4 = Q | 5 = R | 6 = S | 7 = T | 8 = U | 9 = V
            Dictionary<int, char> converter = new Dictionary<int, char>();
            converter.Add(0, 'L');
            converter.Add(1, 'M');
            converter.Add(2, 'N');
            converter.Add(3, 'P');
            converter.Add(4, 'Q');
            converter.Add(5, 'R');
            converter.Add(6, 'S');
            converter.Add(7, 'T');
            converter.Add(8, 'U');
            converter.Add(9, 'V');

            //B  R  G  F  R  C  8  3  P  0  1   C   3   5   1   I
            //X  X  X  X  X  X  _  _  X  _  _   X   _   _   _   X
            //0  1  2  3  4  5  6  7  8  9  10  11  12  13  14  15

            int[] numbersPosition = { 14, 13, 12, 10, 9, 7, 6 };

            string codiceTemp = this.BuildCodiceFiscale();

            List<string> result = new List<string>();

            foreach (int index in numbersPosition)
            {
                var chars = codiceTemp.ToCharArray();
                chars[index] = converter[int.Parse(chars[index].ToString())];
                codiceTemp = string.Concat(chars);
                result.Add(codiceTemp);
            }

            return result.ToArray();
        }
    }
}
