﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;

namespace NetService.Utility.Common
{
    public class InputField
    {
        private string _CssClass;
        public string CssClass
        {
            get
            {
                return _CssClass;
            }
            set
            {
                _CssClass = value;
            }
        }

        private string _ID;
        public string ID
        {
            get { return _ID; }
        }

        private string _Value;

        public string Value
        {
            get
            {
                if (AutoLoadValueAfterPostback && _Value == null && this.RequestVariable.IsValidString)
                {
                    _Value = this.RequestVariable.StringValue;
                }
                return _Value;
            }
            set { _Value = value; }
        }

        private bool _AutoLoadValueAfterPostback;
        public bool AutoLoadValueAfterPostback
        {
            get { return _AutoLoadValueAfterPostback; }
            set { _AutoLoadValueAfterPostback = value; }
        }


        private Common.RequestVariable _RequestVariable;
        public Common.RequestVariable RequestVariable
        {
            get
            {
                if (_RequestVariable == null)
                {
                    _RequestVariable = new Common.RequestVariable(this.ID, Common.RequestVariable.RequestType.Form_QueryString);
                }
                return _RequestVariable;
            }
        }


        public enum FieldTypes
        {
            Hidden,
            Text,
            Button,
            Submit
        }

        private FieldTypes _FieldType;
        public FieldTypes FieldType
        {
            get { return _FieldType; }
        }


        public InputField(string id)
            : this(id, FieldTypes.Hidden)
        {
        }

        public InputField(string id, FieldTypes fieldType)
        {
            _ID = id;
            _FieldType = fieldType;
            _AutoLoadValueAfterPostback = false;
        }

        public override string ToString()
        {
            string format = "<input type=\"{0}\" name=\"{1}\" id=\"{1}\" {2} {3}/>";

            string cssclass = "";
            if (CssClass != null && CssClass.Length > 0)
                cssclass = " class=\"" + CssClass + "\"";

            string value = "";
            if (this.Value != null && this.Value.Length > 0)
                value = " value=\"" + this.Value + "\"";

            return string.Format(format, this.FieldType.ToString().ToLower(), this.ID, cssclass, value);
        }

        public HtmlGenericControl Control
        {
            get
            {
                HtmlGenericControl span = new HtmlGenericControl("span");
                span.InnerHtml = this.ToString();
                return span;
            }
        }

        public string getSetFormValueJScript(string value)
        {
            return "javascript: setFormValue(" + value + ",'" + this.ID + "',1)";
        }
        public string getSetFormValueJScript(string value, bool submit)
        {
            return "javascript: setFormValue(" + value + ",'" + this.ID + "'," + (submit ? 1 : 0) + ")";
        }
        public string getSetFormValueConfirmJScript(string value, string message)
        {
            return "javascript: setFormValueConfirm(" + value + ",'" + this.ID + "',1,'" + message + "')";
        }
    }
}
