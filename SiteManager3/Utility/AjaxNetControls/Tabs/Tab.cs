﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;

namespace NetCms.AjaxNetControls
{
    /// <summary>
    /// Summary description for Tab
    /// </summary>
    public class Tab : WebControl
    {
        public Tab(string id, string label)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = id;
            this.Label.Text = label;
        }
        
        public Tab(string id, string label, WebControl control)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = id;
            this.Label.Text = label;
            this.Control = control;
        }

        public Tab(string id, string label, string callbackPaginationName)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = id;
            this.Label.Text = label;           
            this.CallbackPaginationName = callbackPaginationName;
        }

        private bool isDefault = false;
        public bool IsDefault 
        {
            get { return isDefault; }
            set { isDefault = value; }
        }        

        private bool useAjaxBinding;
        public bool UseAjaxBinding
        {
            get { return useAjaxBinding; }
            set { useAjaxBinding = value; }
        }

        private string ajaxBindingUrl;
        public string AjaxBindingUrl
        {
            get { return ajaxBindingUrl; }
            set { ajaxBindingUrl = value; }
        }

        private string ajaxBindingCountUrl;
        public string AjaxBindingCountUrl
        {
            get { return ajaxBindingCountUrl; }
            set { ajaxBindingCountUrl = value; }
        }

        private LiteralControl label;
        public LiteralControl Label
        {
            get
            {
                if (label == null)
                    label = new LiteralControl();
                return label;
            }
        }
        
        public WebControl Control
        {
            get;
            set;
        }

        private string searchLabelText;
        public string SearchLabelText
        {
            get { return searchLabelText; }
            set { searchLabelText = value; }
        }

        private Label searchLabel;
        public Label SearchLabel
        {
            get
            {
                if (searchLabel == null)
                {
                    searchLabel = new Label();
                    searchLabel.CssClass = this.ID + "_SearchLabel";
                    searchLabel.AssociatedControlID = this.ID + "_SearchText"; // serve per indicare al frameword che si tratta di una label associata ad una textbox

                    if (!string.IsNullOrEmpty(SearchLabelText))
                        searchLabel.Text = SearchLabelText;
                }
                return searchLabel;
            }
            set { searchLabel = value; }
        }

        private TextBox searchText;
        public TextBox SearchText
        {
            get
            {
                if (searchText == null)
                {
                    searchText = new TextBox();
                    searchText.ID = this.ID + "_SearchText";
                    searchText.Columns = 50;
                }
                return searchText;
            }
            set { searchText = value; }
        }

        private string searchButtonInnerText;
        public string SearchButtonInnerText
        {
            get { return searchButtonInnerText; }
            set { searchButtonInnerText = value; }
        }

        private HtmlButton searchButton;
        public HtmlButton SearchButton
        {
            get
            {
                if (searchButton == null)
                {
                    searchButton = new HtmlButton();
                    searchButton.Attributes["type"] = "button";
                    searchButton.ID = this.ID + "_SearchButton";
                    if (!string.IsNullOrEmpty(SearchButtonInnerText))
                        searchButton.InnerText = SearchButtonInnerText;
                    else
                        searchButton.InnerText = "Cerca";
                }
                return searchButton;
            }
            set { searchButton = value; }
        }

        private string searchResetButtonInnerText;
        public string SearchResetButtonInnerText
        {
            get { return searchResetButtonInnerText; }
            set { searchResetButtonInnerText = value; }
        }

        private HtmlButton searchResetButton;
        public HtmlButton SearchResetButton
        {
            get {
                if (searchResetButton == null)
                {
                    searchResetButton = new HtmlButton();
                    searchResetButton.Attributes["type"] = "button";
                    searchResetButton.ID = this.ID + "_ResetSearchButton";
                    if (!string.IsNullOrEmpty(SearchResetButtonInnerText))
                        searchResetButton.InnerText = SearchResetButtonInnerText;
                    else
                        searchResetButton.InnerText = "Azzera ricerca";
               }
                return searchResetButton;
            }
            set { searchResetButton = value; }
        }


        private WebControl SearchControl()
        {
            WebControl searchControl = new WebControl(HtmlTextWriterTag.Div);
            searchControl.CssClass = "SearchControl";

            // label
            searchControl.Controls.Add(SearchLabel);

            // textField
            searchControl.Controls.Add(SearchText);

            // buttons
            searchControl.Controls.Add(SearchButton);
            searchControl.Controls.Add(SearchResetButton);
            return searchControl;
        }

        private bool enableSearch;
        public bool EnableSearch
        {
            get { return enableSearch; }
            set { enableSearch = value; }
        }

        private WebControl AjaxControl()
        {
            WebControl ajaxControl = new WebControl(HtmlTextWriterTag.Div);
            ajaxControl.ID = this.ID + "_AjaxContent";

//            WebControl script = new WebControl(System.Web.UI.HtmlTextWriterTag.Script);
//            script.Attributes["type"] = "text/javascript";

//            string text = @"
//function " + this.CallbackPaginationName + @"(page_index, jq) {
//    var items_per_page = " + this.ItemPerPage + @";    
//    " + this.ID + @".search(page_index+1,items_per_page);
//    return false;
//}
//";
//            script.Controls.Add(new LiteralControl(text));
//            ajaxControl.Controls.Add(script);

            WebControl items = new WebControl(HtmlTextWriterTag.Ul);
            items.ID = this.ID + "_ItemsList";

            ajaxControl.Controls.Add(items);

            return ajaxControl;
        }

        private WebControl AjaxControl2()
        {
            WebControl ajaxControl = new WebControl(HtmlTextWriterTag.Div);
            ajaxControl.ID = this.ID + "_AjaxContent";

            WebControl script = new WebControl(System.Web.UI.HtmlTextWriterTag.Script);
            script.Attributes["type"] = "text/javascript";

            string text = @"
$(document).ready(function() {
    //search('','','');
    $('#" + this.ID + @"_SearchButton').click(function(){
        search($('#" + this.ID + @"_SearchText').val());
        countResults($('#" + this.ID + @"_SearchText').val());
    });
    $('#" + this.ID + @"_ResetSearchButton').click(function(){
        $('#" + this.ID + @"_SearchText').val('');
        search($('',1," + ItemPerPage + @").val());
        countResults($('#" + this.ID + @"_SearchText').val());
    }); 
});


function search(searchText,page,rpp)
{   
    if (page != """" && page != undefined)
    {
        //page = 0;
        if (rpp == """" || rpp == undefined)
        rpp = 6;
	    $.ajax({	    
	        url: '" + AjaxBindingUrl + @"',	
            type: 'GET',    
            data: {search:searchText,pageIndex:page,pageSize:rpp},
            contentType: 'application/json; charset=utf-8',
	        success: function(data) {
                Bind(data);			           
	          },
            error: function(jqXHR,textStatus,errorThrown)
            {
                alert(textStatus);
            }			
	    });
    }
}

function countResults(searchText)
{
    $.ajax({	    
	    url: '" + AjaxBindingCountUrl + @"',	
        type: 'GET',    
        data: {search:searchText},
        contentType: 'application/json; charset=utf-8',
	    success: function(data) {            
            var itemCount = data;
            $('#" + this.ID + @"_Pagination_NumEntry').val(itemCount);            
            initPagination();  // re-init pagination
	      },
        error: function(jqXHR,textStatus,errorThrown)
        {
            alert(textStatus);
        }			
	});
}

function Bind(data)
{
    var $itemslist = $('#" + this.ID + @"_ItemsList');    
    $itemslist.empty();	        
    $.each(data, function(index, jsonItem)
    {		                		                
        $itemslist.append(jsonItem);					
	});		
}

function pageSelectCallback(page_index, jq) {
    var items_per_page = " + Pagination.ItemPerPage + @";    
    search($('#" + this.ID + @"_SearchText').val(),page_index+1,items_per_page);
    return false;
}

function initPagination(){
  
    var numEntry = $('#" + this.ID + @"_Pagination_NumEntry').val();
  
    $('#" + Pagination.ID + Pagination.CtrlName + @"').empty();
    $('#" + Pagination.ID + Pagination.CtrlName + @"').pagination(numEntry,{
    items_per_page:" + ItemPerPage + @",
    callback:pageSelectCallback
    });         
}
           
";

            script.Controls.Add(new LiteralControl(text));
            ajaxControl.Controls.Add(script);

            WebControl items = new WebControl(HtmlTextWriterTag.Ul);
            items.ID = this.ID + "_ItemsList";

            ajaxControl.Controls.Add(items);

            return ajaxControl;
        }

        private bool usePagination;
        public bool UsePagination
        {
            get { return usePagination; }
            set { usePagination = value; }
        }

        private AjaxPagination pagination;
        public AjaxPagination Pagination
        {
            get
            {
                if (pagination == null)
                {
                    pagination = new AjaxPagination(this.ID + "_Pagination", ItemPerPage, MaxItemNum, CallbackPaginationName);                    
                }
                return pagination;
            }
        }

        private string callbackPaginationName;
        public string CallbackPaginationName
        {
            get { return callbackPaginationName; }
            set { callbackPaginationName = value; }
        }
       
        private int _ItemPerPage = 6;
        public int ItemPerPage 
        {
            get { return _ItemPerPage; }
            set { _ItemPerPage = value; }
        }

        private int _MaxItemNum;
        public int MaxItemNum 
        {
            get { return _MaxItemNum; }
            set { _MaxItemNum = value; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            
            //InitControl();

            if (EnableSearch)
                this.Controls.Add(SearchControl());

            if (UseAjaxBinding)
            {
                this.Controls.Add(AjaxControl());
                this.Controls.Add(Pagination);
            }
            else
            {
                if (Control != null)                
                    this.Controls.Add(Control);                
            }

        }

       
    }
}