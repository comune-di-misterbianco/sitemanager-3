﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace NetCms.AjaxNetControls
{
    /// <summary>
    /// Summary description for Tabs
    /// </summary>
    public class Tabs : WebControl
    {
        public Tabs(string id)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = id;
        }

        private IList<Tab> items;
        public IList<Tab> Items
        {
            get
            {
                if (items == null)
                    items = new List<Tab>();
                return items;
            }
        }

        private LiteralControl InitScriptControl()
        {
            // passare l'id del controllo
            LiteralControl txtControl = new LiteralControl();
            txtControl.Text = @"
            <script>
             $(function () {
                 $('#" + this.ID + @"').tabs(); 
             });
	        </script>
        ";
            return txtControl;
        }

        private WebControl GetLabels()
        {
            WebControl ul = new WebControl(HtmlTextWriterTag.Ul);
            WebControl li;
            WebControl a;
            foreach (Tab tab in Items)
            {
                li = new WebControl(HtmlTextWriterTag.Li);
                a = new WebControl(HtmlTextWriterTag.A);
                a.Attributes["href"] = "#" + tab.ID;
                a.CssClass = "lbl_" + tab.ID;
                a.Controls.Add(tab.Label);
                li.Controls.Add(a);
                ul.Controls.Add(li);
            }

            return ul;
        }

        private WebControl tabScripts() 
        {
            WebControl script = new WebControl(System.Web.UI.HtmlTextWriterTag.Script);
            script.Attributes["type"] = "text/javascript";

            string text = @"
function NetTab(id,ajaxBindingUrl,ajaxBindingCountUrl,paginationCtrlID,paginationCallbackName)
{
    this.ID = id;
    this.AjaxBindingUrl = ajaxBindingUrl;
    this.AjaxBindingCountUrl = ajaxBindingCountUrl;   
    this.PaginationCtrlID = paginationCtrlID;
    this.PaginationCallbackName = paginationCallbackName;    
}     

NetTab.prototype.search = function(page,rpp)
{   
        var searchText = $('#'+this.ID+'_SearchText').val();
        var $itemslist = $('#'+this.ID+'_ItemsList');    
        if (page != """" && page != undefined)
        {          
            if (rpp == """" || rpp == undefined)
            rpp = 6;
	        $.ajax({	    
	            url: this.AjaxBindingUrl,	
                type: 'GET',    
                data: {search:searchText,pageIndex:page,pageSize:rpp},
                contentType: 'application/json; charset=utf-8',
	            success: function(data){                     
                     $itemslist.empty();	        
                     $.each(data, function(index, jsonItem)
                     {		                		                
                        $itemslist.append(jsonItem);					
	                 });				           
	            },
                error: function(jqXHR,textStatus,errorThrown)
                {
                    alert(textStatus);
                }			
	        });
          }
}
NetTab.prototype.countResults = function()
{         
        var searchText = $('#'+this.ID+'_SearchText').val();               
        var PaginationCtrlID = this.PaginationCtrlID;

        $.ajax({	    
	        url: this.AjaxBindingCountUrl,	
            type: 'GET',    
            data: {search:searchText},
            contentType: 'application/json; charset=utf-8',
	        success: function(data) {            
                var itemCount = data;                                          
                // re-init pagination                
                $('#'+PaginationCtrlID+'').empty();
                $('#'+PaginationCtrlID+'').pagination(itemCount,{
                    items_per_page:3,
                    callback:this.PaginationCallbackName
                });  
	          },
            error: function(jqXHR,textStatus,errorThrown)
            {
                alert(textStatus);
            }			
	    });
}";

            script.Controls.Add(new LiteralControl(text));            
            return script;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // script intizializzazione componente
            this.Controls.Add(InitScriptControl());

            // aggiungo al controllo la lista di etichette (ul li)
            this.Controls.Add(GetLabels());

            this.Controls.Add(tabScripts());

            WebControl script = new WebControl(System.Web.UI.HtmlTextWriterTag.Script);
            script.Attributes["type"] = "text/javascript";

            string txtScripts = "$(document).ready(function() { ";
            
            // aggiungo al controllo i tab
            foreach (Tab tab in Items)
            {                
                // codice per init dell'obj javascript tab            
                txtScripts += @"
var " + tab.ID + @" = new NetTab('" + tab.ID + @"','" + tab.AjaxBindingUrl + @"','" + tab.AjaxBindingCountUrl + @"','" + tab.Pagination.ID + tab.Pagination.CtrlName + @"','" + tab.CallbackPaginationName+ @"');
$('#" + tab.ID + @"_SearchButton').click(function(){
    " + tab.ID + @".search(1," + tab.ItemPerPage + @");
    " + tab.ID + @".countResults();
});
$('#" + tab.ID + @"_ResetSearchButton').click(function(){
    $('#" + tab.ID + @"_SearchText').val('');
    " + tab.ID + @".search(1," + tab.ItemPerPage + @");
    " + tab.ID + @".countResults();
}); 
function " + tab.CallbackPaginationName + @"(page_index, jq) {
    var items_per_page = " + tab.ItemPerPage + @";    
    " + tab.ID + @".search(page_index+1,items_per_page);
    return false;
}         
";
                if (tab.IsDefault)
                {
                   txtScripts += tab.ID + @".search(1," + tab.ItemPerPage + @");
var numEntry_"+tab.ID+ @" = $('#" + tab.Pagination.ID + @"_NumEntry').val();
$('#" + tab.Pagination.ID + tab.Pagination.CtrlName + @"').pagination(numEntry_"+tab.ID+@",{
    items_per_page:" + tab.ItemPerPage + @",
    callback:" + tab.CallbackPaginationName + @"
});  
";
                }
                else
                {
                    txtScripts += @"$('.lbl_"+tab.ID+ @"').click(function(){ 
                            if ($('#"+tab.ID+@"_ItemsList').children().length == 0){                      
                                " + tab.ID + @".search(1," + tab.ItemPerPage + @");
                                var numEntry_" + tab.ID + @" = $('#" + tab.Pagination.ID + @"_NumEntry').val();
                                $('#" + tab.Pagination.ID + tab.Pagination.CtrlName + @"').pagination(numEntry_" + tab.ID + @",{
                                    items_per_page:" + tab.ItemPerPage + @",
                                    callback:" + tab.CallbackPaginationName + @"
                                });       
                            }                                             
                    });";
                }
                this.Controls.Add(tab);
            }

            txtScripts += "}); ";
            script.Controls.Add(new LiteralControl(txtScripts));
            this.Controls.Add(script);
        }
    }
}