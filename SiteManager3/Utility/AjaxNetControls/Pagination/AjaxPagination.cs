﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace NetCms.AjaxNetControls
{
    /// <summary>
    /// Summary description for AjaxPagination
    /// </summary>
    public class AjaxPagination : WebControl
    {
        public string CtrlName
        { 
            get{ 
                return "_pageControl";
            }
        }

        public AjaxPagination(string id, int itemPerPage, int maxRecord, string callbackPaginationName) : base( HtmlTextWriterTag.Div)
        {
            this.ID = id;
            this.ItemPerPage = itemPerPage;
            this.MaxRecord = maxRecord;
            this.CallbackPaginationName = callbackPaginationName;
        }

        private int itemPerPage;
        public int ItemPerPage
        {
            get { return itemPerPage; }
            set { itemPerPage = value; }
        }

        private int maxRecord;
        public int MaxRecord
        {
            get { return maxRecord; }
            set { maxRecord = value; }
        }

        private string callbackPaginationName;
        public string CallbackPaginationName
        {
            get { return callbackPaginationName; }
            set { callbackPaginationName = value; }
        }        

        private string prevLabel;
        public string PrevLabel
        {
            get { return prevLabel; }
            set { prevLabel = value; }
        }

        private string nextLabel;
        public string NextLabel
        {
            get { return nextLabel; }
            set { nextLabel = value; }
        }

        private int numPageShow;
        public int NumPageShow
        {
            get { return numPageShow; }
            set { numPageShow = value; }
        }

        private int numStartEndPoints;
        public int NumStartEndPoints
        {
            get { return numStartEndPoints; }
            set { numStartEndPoints = value; }
        }

        private HiddenField numEntry;
        public HiddenField NumEntry
        {
            get
            {
                if (numEntry == null)
                {
                    numEntry = new HiddenField();
                    numEntry.ID = this.ID + "_NumEntry";
                    numEntry.Value = this.MaxRecord.ToString();
                }
                return numEntry;
            }
            set { numEntry = value; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            this.Controls.Add(PaginationControl);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Controls.Add(NumEntry);
            
        }
   
        private WebControl paginationControl;
        private WebControl PaginationControl
        {
            get {
                if (paginationControl == null)
                {
                    paginationControl = new WebControl(HtmlTextWriterTag.Div);
                    paginationControl.ID = this.ID + CtrlName;
                }
                return paginationControl;
            }
        }

    }
}