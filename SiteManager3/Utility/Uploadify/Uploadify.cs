﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Uploadify
{
    public class Uploadify : WebControl
    {

        public Uploadify(string id,
            string fileType,
            string fileDesc,
            string uploadButtonText,
            string uploadFileHandlerPath,
            string deleteFileHandlerPath,
            string uploadFolder,
            string swfobjectFilePath,
            string uploadifyScriptsFolderPath,
            bool multi,
            bool auto)
            : base(HtmlTextWriterTag.Div)
        {

            this.ID = "ctrl_" + id; // rappresenta l'id che verrà stampato nel div
            CtrlId = id; // rappresenta l'id usato per identificare l'oggetto
            FileType = fileType;
            FileDesc = fileDesc;
            UploadButtonText = uploadButtonText;
            UploadFileHandlerPath = uploadFileHandlerPath;
            DeleteFileHandlerPath = deleteFileHandlerPath;
            UploadFolder = uploadFolder;
            SwfObjectFilePath = swfobjectFilePath;
            UploadifyScriptsFolderPath = uploadifyScriptsFolderPath;
            Multi = multi;
            Auto = auto;
            QueueSizeLimit = 999;
            UploadLimit = 499;

            //base.ID = "ctrl_" + CtrlId;
        }

        //public override string ID
        //{
        //    get
        //    {
        //        return "ctrl_" + CtrlId;
        //    }
        //}

        private string ctrlId;
        public string CtrlId
        {
            get { return ctrlId; }
            set { ctrlId = value; }
        }

        /// <summary>
        /// Percorso relativo dell'handler che effettua l'upload
        /// </summary>
        private string _UploadFileHandlerPath;
        public string UploadFileHandlerPath
        {
            get
            {
                return _UploadFileHandlerPath;
            }
            set
            {
                _UploadFileHandlerPath = value;
            }
        }

        /// <summary>
        /// Percorso relativo dell'handler che effettua l'eliminazione
        /// </summary>
        private string _DeleteFileHandlerPath;
        public string DeleteFileHandlerPath
        {
            get
            {
                return _DeleteFileHandlerPath;
            }
            set
            {
                _DeleteFileHandlerPath = value;
            }
        }

        /// <summary>
        /// The path to the folder you would like to save the files to. Do not end the path with a '/'.
        ///For absolute paths prefix the path with either '/' or 'http'. Note server security issues with
        ///trying to upload to remote destinations.
        /// 
        /// </summary>
        private string _UploadFolder;
        public string UploadFolder
        {
            get { return _UploadFolder; }
            set { _UploadFolder = value; }
        }

        /// <summary>
        /// Testo del bottone di Upload (è possibile aggiungere anche tag html???)
        /// </summary>
        private string _UploadButtonText = "Sfoglia";
        public string UploadButtonText
        {
            get { return _UploadButtonText; }
            set { _UploadButtonText = value; }
        }

        /// <summary>
        /// The path to the image you will be using for the browse button.
        /// </summary>
        private string _UploadButtonImg = "";
        public string UploadButtonImg
        {
            get
            {
                if (_UploadButtonImg.Length > 0)
                    _UploadButtonImg = "'buttonImg': '" + _UploadButtonImg + "',";
                return _UploadButtonImg;
            }
            set { _UploadButtonImg = value; }
        }

        /// <summary>
        /// The relative path to the backend script that will check if the file selected already resides
        ///on the server. No Default. 
        /// </summary>
        private string _checkScript;
        public string CheckScript
        {
            set
            {
                _checkScript = value;
            }
            get
            {
                if (_checkScript != null && _checkScript.Length > 0)
                {
                    _checkScript = "'checkScript':'" + _checkScript + "',";
                }
                return _checkScript;
            }
        }

        private ProgressDataType _progressData = ProgressDataType.percentage;
        public ProgressDataType ProgressData
        {
            get { return _progressData; }
            set { _progressData = value; }
        }


        /// <summary>
        /// Set to true if you want to allow multiple file uploads.
        /// </summary>
        private bool _multi;
        public bool Multi
        {
            get { return _multi; }
            set { _multi = value; }
        }
        /// <summary>
        /// Set to true if you would like the files to be uploaded when they are selected.
        /// </summary>
        private bool _auto;
        public bool Auto
        {
            get
            {
                return _auto;
            }
            set { _auto = value; }
        }

        /// <summary>
        /// Set the queue size limit.
        /// </summary>
        private int _queueSizeLimit = 10;
        public int QueueSizeLimit
        {
            get
            {
                return _queueSizeLimit;
            }
            set { _queueSizeLimit = value; }
        }

        private int _UploadLimit = 10;
        public int UploadLimit
        {
            get
            {
                return _UploadLimit;
            }
            set { _UploadLimit = value; }
        }

        /// <summary>
        /// A list of file extensions you would like to allow for upload. Format like '*.ext1;*. ext2;*.ext3'.
        /// FileDesc is required when using this option.
        /// </summary>
        private string _fileType;
        public string FileType
        {
            get
            {
                return _fileType.Replace("'", "");
            }
            set
            {
                _fileType = value;
            }
        }

        /// <summary>
        /// The text that will appear in the file type drop down at the bottom of the browse dialog box.
        /// </summary>
        private string _fileDesc;
        public string FileDesc
        {
            get { return _fileDesc.Replace("'", ""); }
            set { _fileDesc = value; }
        }

        private string _swfObjectFilePath;
        /// <summary>
        /// Percorso relativo del file swfobject.js
        /// </summary>
        public string SwfObjectFilePath
        {
            get { return _swfObjectFilePath; }
            set { _swfObjectFilePath = value; }
        }

        //private string _jqueryFilePath;
        ///// <summary>
        ///// Percorso relativo del file jquery.js
        ///// </summary>
        //public string JqueryFilePath 
        //{
        //    get { return _jqueryFilePath; }
        //    set { _jqueryFilePath = value; }
        //}

        private string _uploadifyScriptFolder;
        /// <summary>
        /// Percorso relativo della cartella contenente i file necessari al widget uploadify
        /// </summary>
        public string UploadifyScriptsFolderPath
        {
            get { return _uploadifyScriptFolder; }
            set { _uploadifyScriptFolder = value; }
        }

        private string _FileListPostBackValue;
        public string FileListPostBackValue
        {
            get { return _FileListPostBackValue; }
            set { _FileListPostBackValue = value; }
        }



        /// <summary>
        /// Imposta la dimensione massima accettata per file. La proprietà accetta unità B, KB, MB o GB 
        /// esempio: "100KB". Di default è settata a KB; per non inserire limiti settare la proprietà a zero.
        /// </summary>
        public string FileSizeLimit
        {
            get { return fileSizeLimit; }
            set { fileSizeLimit = value; }
        }
        private string fileSizeLimit = "0";

        //private string sizeLimitWidgetProperty
        //{
        //    get 
        //    {
        //        if (SizeLimit != 0)
        //            return "'sizeLimit':" + SizeLimit + ",";
        //        return "";
        //    }
        //}


        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            this.ID = "ctrl_" + CtrlId;

            if (Page.ClientScript.IsClientScriptBlockRegistered("UploadifyMainScript") == false)
            {
                Page.Header.Controls.Add(new LiteralControl(@"<link rel=""stylesheet"" type=""text/css"" href=""" + UploadifyScriptsFolderPath + @"uploadify.css""/>"));
                Page.Header.Controls.Add(new LiteralControl(@"<link rel=""stylesheet"" type=""text/css"" href=""" + UploadifyScriptsFolderPath + @"uploadify-custom.css""/>"));

                //Page.Header.Controls.Add(new LiteralControl(@"<script type=""text/javascript"" src=""" + SwfObjectFilePath + @"""></script>"));
                //Page.Header.Controls.Add(new LiteralControl(@"<script type=""text/javascript"" src=""" + UploadifyScriptsFolderPath + @"jquery.uploadify.v2.1.4.min.js""></script>"));
                Page.Header.Controls.Add(new LiteralControl(@"<script type=""text/javascript"" src=""" + UploadifyScriptsFolderPath + @"jquery.uploadify.js""></script>"));
            }

            // check size limit


            LiteralControl txtControl = new LiteralControl();
            txtControl.Text = @"
    <style type=""text/css"" media=""screen"">
        #fileQueue_" + this.CtrlId + @" {	        
	        height: 120px;
	        overflow: auto;
	        border: 1px solid #E5E5E5;
            background-color:#fff;
	        margin-bottom: 10px;
        }
        #listing_" + this.CtrlId + @" {
            border:1px solid #eee;
            background-color:#fff;            
		}
        #uploadify_" + this.CtrlId + @"Uploader {            
            margin-right:5px;
        }        
        #uploadify_" + this.CtrlId + @"Uploader
        {
	        float:left;
        }
	</style> 

    <script type=""text/javascript"">      
    var fileCount = 0;
    $(document).ready(function () {
        $(""#uploadify_" + this.CtrlId + @""").uploadify({            
            'swf': '" + UploadifyScriptsFolderPath + @"uploadify.swf',
            'uploader': '" + UploadFileHandlerPath + @"',             
            'buttonText': '" + UploadButtonText + @"', " + UploadButtonImg + @"" + CheckScript + @"
            'queueID': 'fileQueue_" + this.CtrlId + @"',            
            'queueSizeLimit': " + QueueSizeLimit + @",
            //'uploadLimit': " + UploadLimit + @",
            'auto': " + Auto.ToString().ToLower() + @",
            'multi': " + Multi.ToString().ToLower() + @",
            'progressData': '" + ProgressData + @"', 
            'fileTypeExts': '" + FileType + @"',
            'fileTypeDesc': '" + FileDesc + @"',  
            'fileSizeLimit': '" + FileSizeLimit + @"',   
            'method': 'post',                                       
            'formData': { 'folder':'" + UploadFolder + @"'}, 
            'onSelect': function (file){                                                           
                var elencofile = $('#file_list_" + this.CtrlId + @"').val();                  
                var filesArray = elencofile.split(';'); 
                if ($.inArray(file.name, filesArray) > -1)
                {
                    $(""#uploadify_" + this.CtrlId + @""").uploadify('cancel','*');
                }                           
             }, 
            'onUploadSuccess': function (file, data, response) {
                if ($('#filelist_" + this.CtrlId + @"').length == 0) 
                {
                    $('#listing_" + this.CtrlId + @"').append('<ul id=""filelist_" + this.CtrlId + @"""></ul>');
                }
                
                 var ext = file.type.replace('.', '');
                 var dim = file.size;

                 if (dim > 1024)
                    dim = Math.round((file.size/1024).toString()) + 'KB';
                 else
                    dim = dim.toString() + 'byte';

                 var item = '<li class=""file ' + ext + '"">'
                          + '  <p class=""fileinfo"">'
                          +'    <a href=""" + this.UploadFolder + @"/'+file.name+'"">'
                          +'        <span class=""name"">' + file.name + '</span>'
                          +'    </a>'
                          +'    <span class=""peso"">' + dim + '</span>'
                          +'    <span class=""fileDelete"">Elimina</span>'
                          +'  </p>'
                          +'</li>';                 
                 
                $('#filelist_" + this.CtrlId + @"').append(item);
                var elencofile = $('#file_list_" + this.CtrlId + @"').val();
                $('#file_list_" + this.CtrlId + @"').val(elencofile + file.name +';');               
               
                if (!this.settings.multi)               
                {
                    fileCount++;              
                    if (fileCount >= " + UploadLimit + @")
                    {                                     
                       $(""#uploadify_" + this.CtrlId + @""").uploadify('disable', true);
                    }
                }
                files.push(file.name);
            }           
        });        	
    });
    $(function () {       
	    $('.fileDelete').live('click', function () {                      
	        var fileName = $(this).parent().find('.name')[0].textContent;
            var multi =  " + Multi.ToString().ToLower() + @";	        
	        $.ajax({
	            type: 'POST',
	            url: '" + DeleteFileHandlerPath + @"',
	            cache: false,
	            data: { FileNameTOBeDeleted: '" + UploadFolder + @"/'+fileName+'' },
	            success: function (data, textStatus, jqXHR) {
	                if (data != undefined) 
                    {
	                    $('li.file').remove("":contains('"" + fileName + ""')"");	                    
	                    var fileListValue = $('#file_list_" + this.CtrlId + @"').val();
	                    fileListValue = fileListValue.replace(fileName+';', '');
	                    $('#file_list_" + this.CtrlId + @"').val(fileListValue);	                  
                        
                        if (!multi){
                            fileCount--;
                            $(""#uploadify_" + this.CtrlId + @""").uploadify('disable', false);                            
                        }
	                }
	            },
	            error: function (jqXHR, textStatus, errorThrown) {
	                alert(textStatus);
	            }
	        });
	    });
        if ($('#file_list_" + this.CtrlId + @"').val() != undefined) {

                var fileListValue = $('#file_list_" + this.CtrlId + @"').val();
                if (fileListValue != undefined) {                    
                    if ($('#filelist_" + this.CtrlId + @"').length == 0) {
                        $('#listing_" + this.CtrlId + @"').append('<ul id=""filelist_" + this.CtrlId + @"""></ul>');
                    }
                    var files = fileListValue.split("";"");
                    for (i = 0; i < files.length; i++) {
                        var filename = files[i];
                        if (filename.length > 0) {
                            var ext = filename.substr(filename.lastIndexOf(""."") + 1, 3);
                            var item = '<li class=""file ' + ext + '"">' +
                               ' <p class=""fileinfo"">' +
                               '   <a href=""" + this.UploadFolder + @"/' + filename + '"">' +
                               '      <span class=""name"">' + filename + '</span>' +
                               '   </a>' +
                               '   <span class=""fileDelete"">Elimina</span>' +
                               ' </p>' +
                               '</li>';
                            $('#filelist_" + this.CtrlId + @"').append(item);
                        }
                    } 
                }
            }
	});	
    </script>";

            // se auto = false devo stampare il bottone per il trasferimento manuale vedi esempio a seguire            
            string btn_upload = "";
            if (!Auto)
                btn_upload = "<div class=\"btn _upload\"><a href=\"javascript:$('#uploadify" + this.CtrlId + "').uploadify('upload','*')\">Trasferisci i file</a></div>";

            txtControl.Text += @"
<div class=""uploadify-control"">
        <p><br /><label>" + TurnFirstToUpper(this.CtrlId) + @"</label></p>
        <div class=""listAllegati"" id=""listing_" + this.CtrlId + @"""></div>       
        <div class=""fileQueue"" id=""fileQueue_" + this.CtrlId + @"""></div>
        <div class=""buttons"">
        <input type=""file"" name=""uploadify_" + this.CtrlId + @""" id=""uploadify_" + this.CtrlId + @""" />
        <input id=""file_list_" + this.CtrlId + @""" name=""file_list_" + this.CtrlId + @""" type=""hidden"" value=""" + FileListPostBackValue + @""" />
        
        <div class=""btn_del_all"">
            <a href=""javascript:jQuery('#uploadify_" + this.CtrlId + @"').uploadify('cancel','*')""><span>Annulla</span></a>
        </div>
        " + btn_upload + @"
        <div class=""clr""></div>
        </div>
</div>
";
            //<input id="""+ this.CtrlId + @""" name=""" + this.CtrlId + @""" type=""hidden"" value="""" />
            this.Controls.Add(txtControl);

            HiddenField ctrlValue = new HiddenField();
            ctrlValue.ID = this.CtrlId;
            ctrlValue.Value = "";

            this.Controls.Add(ctrlValue);
        }

        private string TurnFirstToUpper(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                string temp = input.Substring(0, 1);
                return temp.ToUpper() + input.Remove(0, 1);
            }
            return input;
        }

        public enum ProgressDataType
        {
            percentage,
            speed
        }
    }
}
