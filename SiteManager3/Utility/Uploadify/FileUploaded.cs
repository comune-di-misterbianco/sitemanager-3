﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Uploadify
{
    public class FileUploaded
    {                
        public FileUploaded() 
        {
        
        }       

        public FileUploaded(string filename)
        {
            FileName = filename;
        }
        
        private int _ID;
        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _Ext;
        public string Ext
        {
            get {
                if (_Ext == null)
                {
                    if (FileName != null)
                    {
                        _Ext = FileName.Substring(FileName.LastIndexOf('.') + 1, 3);
                    }
                }
                return _Ext; 
            }
            set { _Ext = value; }
        }

        private string _FileName;
        public string  FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }                       
    }
}
