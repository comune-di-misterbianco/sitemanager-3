﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using NetService.Utility.RecordsFinder;
using System.Web.UI;
using NetUtility.Report.DataTableConvert;


namespace NetUtility.Report.Exporter
{
    public enum ExportFormats { Csv, Pdf, Xml, Xls }
    public class ExporterGUI:WebControl
    {
        public List<ExportFormats> ExportFormats { get; private set; }
        public DataTable SourceTable { get; set; }
        public DataTable RecordsFinder { get; set; }
        public List<string> FieldsToRemoveName { get; set; }
        public Dictionary<string, object> PlaceHoldersValues { get; set; }
        public string Template { get; set; }
        public bool ClearTable { get; set; }
        public string BaseUrl { get; private set; }

        public ExporterGUI()
            :base(HtmlTextWriterTag.Div)
        {
            Template = "template-table.xml";
            ClearTable = true;
            BaseUrl = DataTableExporterUtility.BuildBaseUrl();
            ExportFormats = new List<ExportFormats>();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            DataTable clonedTable = DataTableExporterUtility.BuildClonedTable(SourceTable, FieldsToRemoveName, ClearTable);
            string exLink = DatatableConverterSessionEntry.AddNewEntry(clonedTable, PlaceHoldersValues, Template);
            this.CssClass = "export-buttons";

            var links = ExportFormats.Distinct().Select(x => BuildLink(x, exLink));
            string separator = "<span>&nbsp;|&nbsp;</span>";
            this.Controls.Add(new LiteralControl(string.Join(separator,links)));
        }

        private string BuildLink(ExportFormats format, string exLink)
        {
            string label = string.Empty;
            string extension = string.Empty;

            switch(format)
            {
                case Exporter.ExportFormats.Csv: 
                    label = "CSV";
                    extension = "csv";
                    break;
                case Exporter.ExportFormats.Xls:
                    label = "Excel";
                    extension = "xls";
                    break;
                case Exporter.ExportFormats.Pdf: 
                    label = "PDF";
                    extension = "pdf";
                    break;
                case Exporter.ExportFormats.Xml: 
                    label = "XML";
                    extension = "xml";
                    break;
            }

            return string.Format("<a class=\"export-link-{0}\" href=\"" + BaseUrl + exLink + ".{0}\"><span>Esporta come {1}</span></a>", extension, label);
        }
    }
}
