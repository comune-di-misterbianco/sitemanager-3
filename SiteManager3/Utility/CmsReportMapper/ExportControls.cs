using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetService.Utility.RecordsFinder;
using NetService.Utility.Common;
using System.Data;
using System.Collections;
using System.ComponentModel;
using NetUtility.Report.DataTableConvert;
using Report.Data;
using System.Web;
using NetUtility.Report.Exporter;

namespace NetService.Utility
{
    /// <summary>
    /// Questa classe permette di gestire la creazione di report nei seguenti formati XML, CSV, PDF ecc...
    /// Per utilizzarla � necessario implementare un codice analogo al seguente:
    /// ...
    /// ExportControls exportController = new ExportControls("ripartoExportData");
    /// exportController.TemplateFileName = "nome-del-mio-template.xml";
    /// exportController.BaseUrl = "mia-pagina.aspx";
    /// exportController.ExportRequestEvent += new ExportControls.AskExportEventHandler(ExportController_ExportRequestEvent);
    /// exportController.Add(ExportFormats.Pdf);
    /// exportController.Add(ExportFormats.Csv);
    /// ...
    /// void ExportController_ExportRequestEvent(object sender, EventArgs e)
    /// {
    ///      ExportControls exportController = sender as ExportControls;
    ///      exportController.DataTable = MiaDataTable;
    ///      exportController.DataTable = MiaDataTable;
    /// 
    ///      exportController.CustomPlaceholderValues.Add("mio-placeholder1", "mio valore");
    ///      exportController.CustomPlaceholderValues.Add("mio-placeholder2", "mio valore2");
    /// }
    /// </summary>
    public class ExportControls : WebControl
    {
        protected bool ExportTableBuilded { get; private set; }
        protected string CacheID { get; private set; }
        public DataTable DataTable { get; set; }

        public string TemplateFileName { get; set; }
        
        public string BaseUrl
        {
            get
            {
                if (_BaseUrl == null)
                {
                    string address = HttpContext.Current.Request.Url.ToString();
                    _BaseUrl = address;
                }
                return _BaseUrl;
            }
            set
            {
                _BaseUrl = value;
            }
        }
        private string _BaseUrl;

        public List<ExportFormats> ExportFormatsEnabled { get; private set; }
        public Dictionary<string, object> CustomPlaceholderValues { get; private set; }

        public Request.QSVar<int> ExportRequest
        {
            get
            {
                if (_ExportRequest == null)
                {
                    _ExportRequest = new Request.QSVar<int>("exportRequest" + this.ID);
                }
                return _ExportRequest;
            }
        }
        private Request.QSVar<int> _ExportRequest;

        public string ShowExportLinksLabel { get; set; }

        #region Costruttori

        public ExportControls(string cacheID)
            : base(HtmlTextWriterTag.Div)
        {
            this.CacheID = cacheID;
            ExportFormatsEnabled = new List<ExportFormats>();
            CustomPlaceholderValues = new Dictionary<string, object>();
            TemplateFileName = "template-table.xml";
            ShowExportLinksLabel = "Mostra Opzioni di Esportazione";
        }

        #endregion

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            var exports = new HtmlGenericControl("div");
            
            if (ExportRequest.HasValidValue && ExportRequest.Value == 1)
            {
                OnExportRequest(EventArgs.Empty);

                DataContext dati = new DataContext();

                foreach (var entry in CustomPlaceholderValues)
                    if (!dati.ContainsKey(entry.Key))
                        dati.Add(entry.Key, entry.Value);

                //HtmlGenericControl ExportLinks = DataTableExporterUtility.addExportLinks(SourceTable: DataTable,clearTable: false,
                //                                                                        PlaceHoldersValues: dati, template: this.TemplateFileName,
                //                                                                        printCsv: ExportFormatsEnabled.Contains(ExportFormats.Csv),
                //                                                                        printXml: ExportFormatsEnabled.Contains(ExportFormats.Xml),
                //                                                                        printPdf: ExportFormatsEnabled.Contains(ExportFormats.Pdf));

                var ExportLinks = new NetUtility.Report.Exporter.ExporterGUI()
                {
                    SourceTable = DataTable,
                    ClearTable = false,
                    PlaceHoldersValues = dati,
                    Template = this.TemplateFileName
                };
                ExportLinks.ExportFormats.AddRange(this.ExportFormatsEnabled);

                if (ExportLinks != null)
                    this.Controls.Add(ExportLinks);
            }
            else
            {
                string address = BaseUrl;
                address += address.Contains("?") ? "&" : "?";
                address += this.ExportRequest.Key + "=1";

                exports.Attributes.Add("class", "export-buttons");
                exports.InnerHtml = "<a href=\"" + address + "\"><span>" + ShowExportLinksLabel + "</span></a>";
                this.Controls.Add(exports);
            }
        }

        public delegate void AskExportEventHandler(object sender, EventArgs e);

        public event AskExportEventHandler ExportRequestEvent;

        protected virtual void OnExportRequest(EventArgs e)
        {
            if (ExportRequestEvent != null)
                ExportRequestEvent(this, e);
        }
    }

}

