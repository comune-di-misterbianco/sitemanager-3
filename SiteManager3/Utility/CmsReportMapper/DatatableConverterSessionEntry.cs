﻿using System;
using System.Text.RegularExpressions;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.Caching;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
//using www.serviciipeweb.ro.iafblog.ExportDLL;
using System.Security.Cryptography; // extension methods for data table ...

namespace NetUtility.Report.DataTableConvert
{
    public class DatatableConverterSessionEntry 
    {
        private const int MaxChachedItems = 3;

        public static string AddNewEntry(DataTable table, Dictionary<string, object> PlaceHoldersValues = null, string template = "template-table.xml")
        {
            DatabaseConverterSerssionElement item = new DatabaseConverterSerssionElement() { Table = table, PlaceHoldersValues = PlaceHoldersValues, Template = template };

            string fileName = new RandomNumbers().Next(int.MaxValue).ToString();
            fileName = "download-" + fileName;

            G2Core.Caching.Cache cache = new G2Core.Caching.Cache();

            var entries = cache.AllKeys.Select("key LIKE '%download-%'", "id");
            if (entries.Length >= MaxChachedItems)
            {
                var entryToRemove = entries[2];
                cache.Remove(entryToRemove["key"].ToString());
            }
            string scope = G2Core.Caching.Visibility.CachingVisibilityUtility.BuildVisibilityKey(G2Core.Caching.Visibility.CachingVisibility.User);
            cache.Insert(scope + fileName, item, 10);

            return "/export-table-data/" + fileName;
        }

        public static DatabaseConverterSerssionElement GetEntry(string key)
        {
            string scope = G2Core.Caching.Visibility.CachingVisibilityUtility.BuildVisibilityKey(G2Core.Caching.Visibility.CachingVisibility.User);
            G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
            return (DatabaseConverterSerssionElement)cache[scope + key];
        }


        /// <summary>
        /// Numeri pseudo-casuali e cryptographically-strong
        /// </summary>
        private class RandomNumbers
        {
            private RNGCryptoServiceProvider _rand = new RNGCryptoServiceProvider();
            private byte[] _rd4 = new byte[4], _rd8 = new byte[8];

            /// <summary>
            /// Genera un numero intero positivo casuale minore di max
            /// </summary>
            /// <param name="max">Limite superiore del numero casuale generato</param>
            /// <returns>Il numero casuale generato</returns>
            public int Next(int max)
            {
                if (max <= 0)
                {
                    Exception ex = new ArgumentOutOfRangeException("Max");
                }
                _rand.GetBytes(_rd4);
                int val = BitConverter.ToInt32(_rd4, 0) % max;
                if (val < 0) val = -val;
                return val;
            }

            /// <summary>
            /// Genera un numero intero positivo casuale compreso tra min e max
            /// </summary>
            /// <param name="min">Valore minimo possibile</param>
            /// <param name="max">Valore massimo possibile</param>
            /// <returns>Il numero casuale generato</returns>
            public int Next(int min, int max)
            {
                if (min > max)
                {
                    Exception ex = new ArgumentOutOfRangeException();
                }
                return Next(max - min + 1) + min;
            }

            /// <summary>
            /// Genera un numero (double) compreso tra 0.0 e 1.1
            /// </summary>
            /// <returns>Il numero casuale generato</returns>
            public double NextDouble()
            {
                _rand.GetBytes(_rd8);
                return BitConverter.ToUInt64(_rd8, 0) / (double)UInt64.MaxValue;
            }
        }
    }
    public class DatabaseConverterSerssionElement
    {
        public Dictionary<string, object> PlaceHoldersValues { get; set; }
        public string Template { get; set; }
        public DataTable Table { get; set; }   
    }
}
