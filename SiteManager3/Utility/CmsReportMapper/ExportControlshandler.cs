using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NetService.Utility.RecordsFinder;
using NetService.Utility.Common;
using System.Data;
using System.Collections;
using System.ComponentModel;
using NetUtility.Report.DataTableConvert;
using Report.Data;

namespace NetService.Utility
{
    public class ExportControlsHandler : WebControl
    {
        protected bool ExportTableBuilded { get; private set; }
        protected string CacheID { get; private set; }
        public DataTable DataTable { get; set; }

        public string TemplateFileName { get; set; }

        public enum ExportFormats { Csv, Pdf, Xml }

        public List<ExportFormats> ExportFormatsEnabled { get; private set; }
        public Dictionary<string, string> CustomPlaceholderValues { get; private set; }

        public InputField ExportRequest
        {
            get
            {
                if (_ExportRequest == null)
                {
                    _ExportRequest = new InputField("esportRequest" + this.ID);
                }
                return _ExportRequest;
            }
        }
        private InputField _ExportRequest;

        public string ShowExportLinksLabel { get; set; }

        #region Costruttori

        public ExportControlsHandler(string cacheID)
            : base(HtmlTextWriterTag.Div)
        {
            this.CacheID = cacheID;
            ExportFormatsEnabled = new List<ExportFormats>();
            CustomPlaceholderValues = new Dictionary<string, string>();
            TemplateFileName = "template-table.xml";
            ShowExportLinksLabel = "Mostra Opzioni di Esportazione";
        }

        #endregion

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            var exports = new HtmlGenericControl("div");

            this.Controls.Add(this.ExportRequest.Control);

            if (ExportRequest.RequestVariable.IsValidInteger && ExportRequest.RequestVariable.IntValue == 1)
            {
                OnExportRequest(EventArgs.Empty);

                DataContext dati = new DataContext();

                foreach (var entry in CustomPlaceholderValues)
                    if (!dati.ContainsKey(entry.Key))
                        dati.Add(entry.Key, entry.Value);

                HtmlGenericControl ExportLinks = DataTableExporterUtility.addExportLinks(SourceTable: DataTable,clearTable: false,
                                                                                        PlaceHoldersValues: dati, template: this.TemplateFileName,
                                                                                        printCsv: ExportFormatsEnabled.Contains(ExportFormats.Csv),
                                                                                        printXml: ExportFormatsEnabled.Contains(ExportFormats.Xml),
                                                                                        printPdf: ExportFormatsEnabled.Contains(ExportFormats.Pdf));
                if (ExportLinks != null)
                    this.Controls.Add(ExportLinks);
            }
            else
            {
                exports.Attributes.Add("class", "export-buttons");
                exports.InnerHtml = "<a href=\"javascript: setFormValue(1,'" + this.ExportRequest.ID + "',1)\"><span>" + ShowExportLinksLabel + "</span></a>";
                this.Controls.Add(exports);
            }
        }

        public delegate void AskExportEventHandler(object sender, EventArgs e);

        public event AskExportEventHandler ExportRequestEvent;

        protected virtual void OnExportRequest(EventArgs e)
        {
            if (ExportRequestEvent != null)
                ExportRequestEvent(this, e);
        }
    }

}

