﻿using System;
using System.Text.RegularExpressions;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.Caching;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using www.serviciipeweb.ro.iafblog.ExportDLL; // extension methods for data table ...
using Report.Data;
using Report;
using Report.Visitor;
using Report.TemplateReader;

namespace NetUtility.Report.DataTableConvert
{
    public class DatatableUrlHandledConverter 
    {

        private static string _AbsoluteRoot;
        public static string AbsoluteRoot
        {
            get
            {
                if (_AbsoluteRoot == null)
                {
                    _AbsoluteRoot = string.Empty;
                    string RootPathValue = System.Web.Configuration.WebConfigurationManager.AppSettings["RootPath"].ToLower();
                    if (RootPathValue.Length > 0)
                        _AbsoluteRoot += RootPathValue.ToLower();
                }
                return _AbsoluteRoot;
            }
        }

        public DatabaseConverterSerssionElement SessionEntry
        {
            get
            {
                if (_SessionEntry == null)
                {
                    _SessionEntry = DatatableConverterSessionEntry.GetEntry(this.PageName);
                }
                return _SessionEntry;
            }
        }
        private DatabaseConverterSerssionElement _SessionEntry;

        public string PageName
        {
            get
            {
                string sPath = System.Web.HttpContext.Current.Request.Url.LocalPath;
                System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
                string sRet = oInfo.Name.ToLower().Replace(Extension,"");
                return sRet.ToLower();
            }
        }

        public string Extension
        {
            get
            {
                string sPath = System.Web.HttpContext.Current.Request.Url.LocalPath;
                System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
                string sRet = oInfo.Extension;
                return sRet.ToLower();
            }
        }

        public string Path
        {
            get
            {
                return _Path;
            }
            private set
            {
                _Path = value;
            }
        }
        private string _Path;

        public bool IsValid
        {
            get
            {
                return SessionEntry != null;
            }
        }

        public DatatableUrlHandledConverter(string path)
        {
            this.Path = path;
        }

        public void ConvertAndSave()
        {
            switch (Extension)
            {
                case ".csv": SavedFileMimeType = "text/csv"; ConvertAndSaveCSV(); break;
                case ".xml": SavedFileMimeType = "text/csv"; ConvertAndSave(ExportToFormat.XML); break;
                case ".pdf": SavedFileMimeType = "application/pdf"; ConvertAndSavePDF(ExportToFormat.PDFtextSharpXML); break;
                case ".xls": SavedFileMimeType = "application/vnd.ms-excel"; ConvertAndSave(ExportToFormat.Excel2003XML); break;
                case ".xlsx": SavedFileMimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; ConvertAndSave(ExportToFormat.Excel2007); break;
                case ".docx": SavedFileMimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"; ConvertAndSave(ExportToFormat.Word2007); break;
            }
        }
        private void ConvertAndSaveCSV()
        {
            string userdirpath = HttpContext.Current.Server.MapPath( Path + "/");
            DirectoryInfo dir = new DirectoryInfo(userdirpath);
            if (!dir.Exists) dir.Create();

            SavedFilePath = userdirpath + "\\" + this.PageName + Extension;
            FileInfo file = new FileInfo(SavedFilePath);
            var stream = file.CreateText();
            ProduceCSV(SessionEntry.Table, stream, true);
            stream.Close();
        }
        private void ConvertAndSave(ExportToFormat format)
        {
            string userdirpath = HttpContext.Current.Server.MapPath(Path + "/");
            DirectoryInfo dir = new DirectoryInfo(userdirpath);
            if (!dir.Exists) dir.Create();

            //Il sottostante è quello in cui si trovano i file di template.
            //Viene calcolato appendendo '/template/Collection/' al path che viene passato nella proprietà 'CollectionRenderProperties.PathFiles'
            CollectionRenderProperties.PathFiles = HttpContext.Current.Server.MapPath(AbsoluteRoot + "/");
            SavedFilePath = userdirpath + "\\" + this.PageName + Extension;
            SessionEntry.Table.ExportTo(format, SavedFilePath);

        }
        private void ConvertAndSavePDF(ExportToFormat format)
        {
            string userdirpath = HttpContext.Current.Server.MapPath(Path + "/");
            DirectoryInfo dir = new DirectoryInfo(userdirpath);
            if (!dir.Exists) dir.Create();
            CollectionRenderProperties.PathFiles = userdirpath;

            /* Convertitore PDF */

            DataContext context = new DataContext();
            if (this.SessionEntry.PlaceHoldersValues != null)
                foreach(var entry in this.SessionEntry.PlaceHoldersValues)
                    context.Add(entry.Key, entry.Value);
            if(this.SessionEntry.Table!=null)
                context.Add(this.SessionEntry.Table.TableName, this.SessionEntry.Table);

            string template = string.IsNullOrEmpty(this.SessionEntry.Template) ? "template-table.xml" : this.SessionEntry.Template;

            ReportObject report = new TemplateReader().LoadReport(HttpContext.Current.Server.MapPath(AbsoluteRoot + "/templates/pdf/"+template));
            ReportPdfVisitor visitor = new ReportPdfVisitor();

            visitor.DataContext = context;
            report.Accept(visitor);

            SavedFilePath = userdirpath + "\\" + this.PageName + Extension;
            visitor.save(SavedFilePath);

            /* **************** */
        }

        public string SavedFileMimeType
        {
            get
            {
                return _SavedFileMimeType;
            }
            private set
            {
                _SavedFileMimeType = value;
            }
        }
        private string _SavedFileMimeType; 

        public string SavedFilePath
        {
            get
            {
                return _SavedFilePath;
            }
            private set
            {
                _SavedFilePath = value;
            }
        }
        private string _SavedFilePath;

        #region CSV Producer
        public static void ProduceCSV(DataTable dt, System.IO.StreamWriter file, bool WriteHeader)
        {
            if (WriteHeader)
            {
                string[] arr = new String[dt.Columns.Count];
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    arr[i] = dt.Columns[i].ColumnName;
                    arr[i] = GetWriteableValue(arr[i]);
                }

                file.WriteLine(string.Join(";", arr));
            }

            for (int j = 0; j < dt.Rows.Count; j++)
            {
                string[] dataArr = new String[dt.Columns.Count];
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    object o = dt.Rows[j][i];
                    dataArr[i] = GetWriteableValue(o);
                }
                file.WriteLine(string.Join(";", dataArr));
            }
        }

        public static string GetWriteableValue(object o)
        {
            if (o == null || o == Convert.DBNull)
                return "";
            else if (o.ToString().IndexOf(",") == -1)
                return o.ToString();
            else
                return "\"" + o.ToString().Replace("€ ","") + "\"";

        }
        #endregion
    }
}
