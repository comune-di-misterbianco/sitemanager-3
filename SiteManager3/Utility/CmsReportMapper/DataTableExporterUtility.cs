﻿using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Caching;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using NetService.Utility.RecordsFinder;
using System.Web.UI;


namespace NetUtility.Report.DataTableConvert
{
    public class DataTableExporterUtility
    {
        public static HtmlGenericControl addExportLinks(DataTable SourceTable = null, Finder recordsFinder = null, List<string> fieldsToRemoveName = null, Dictionary<string, object> PlaceHoldersValues = null, string template = "template-table.xml", bool printCsv = true, bool printPdf = true, bool printXml = false, bool printXls = false, bool clearTable = true)
        {           
            HtmlGenericControl exports = new HtmlGenericControl("div");
            exports.Attributes.Add("class", "export-buttons");
            exports.InnerHtml = addExportLinksAsString(SourceTable, recordsFinder, fieldsToRemoveName, PlaceHoldersValues, template, printCsv, printPdf, printXml, printXls, clearTable);

            return exports;
        }

        public static string addExportLinksAsString(DataTable SourceTable = null, Finder recordsFinder = null, List<string> fieldsToRemoveName = null, Dictionary<string, object> PlaceHoldersValues = null, string template = "template-table.xml", bool printCsv = true, bool printPdf = true, bool printXml = false, bool printXls = false, bool clearTable = true)
        {
            DataTable clonedTable = BuildClonedTable(SourceTable, fieldsToRemoveName, clearTable);
            string exLink = DatatableConverterSessionEntry.AddNewEntry(clonedTable, PlaceHoldersValues, template);
            string thisUrl = BuildBaseUrl();

            string exports = "";
            string separator = "<span>&nbsp;|&nbsp;</span>";

            if (printCsv)
            {
                if (!string.IsNullOrEmpty(exports)) exports += separator;
                exports += "<a class=\"export-link-csv\" href=\"" + thisUrl + exLink + ".csv\"><span>Esporta come CSV</span></a>";
            }
            if (printPdf)
            {
                if (!string.IsNullOrEmpty(exports)) exports += separator;
                exports += "<a class=\"export-link-pdf\" href=\"" + thisUrl + exLink + ".pdf\"><span>Esporta come PDF</span></a>";
            }
            if (printXml)
            {
                if (!string.IsNullOrEmpty(exports)) exports += separator;
                exports += "<a class=\"export-link-xml\" href=\"" + thisUrl + exLink + ".xml\"><span>Esporta come XML</span></a>";
            }
            if (printXls)
            {
                if (!string.IsNullOrEmpty(exports)) exports += separator;
                exports += "<a class=\"export-link-xls\" href=\"" + thisUrl + exLink + ".xls\"><span>Esporta come Excel</span></a>";
            }

            return exports;
        }

        public static string BuildBaseUrl()
        {
            string thisUrl = System.Web.HttpContext.Current.Request.Url.ToString().ToLower();

            string sPath = System.Web.HttpContext.Current.Request.Url.LocalPath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string pname = "/" + oInfo.Name.ToLower();
            thisUrl = thisUrl.Split(new string[] { pname }, StringSplitOptions.RemoveEmptyEntries)[0];
            return thisUrl;
        }

        public static DataTable BuildClonedTable(DataTable SourceTable, List<string> fieldsToRemoveName, bool clearTable)
        {
            DataTable clonedTable = null;
            if (SourceTable != null)
            {
                clonedTable = SourceTable.Copy();
                clonedTable.TableName = "DataTable";
                foreach (DataColumn col in SourceTable.Columns)
                {
                    if (clearTable && !(col.DataType.FullName == "System.String") && !(col.DataType.FullName == "System.DateTime"))
                        clonedTable.Columns.Remove(col.ColumnName);

                    if (fieldsToRemoveName != null && fieldsToRemoveName.Contains(col.ColumnName))
                        if (clonedTable.Columns.Contains(col.ColumnName))
                            clonedTable.Columns.Remove(col.ColumnName);
                }
            }
            return clonedTable;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <param name="columns">Chiave(Univoca) e Etichetta</param>
        /// <returns></returns>
        public static DataTable ConvertListToDatatableRecords(IEnumerable list, Dictionary<string, string> columns, Type ExternalType = null, string joinField = null, string methodName = "GetByID")
        {
            DataTable dt = new DataTable("DataTable");
            foreach (var col in columns)
            {
                DataColumn dcol = new DataColumn(col.Key);
                dcol.DataType = typeof(System.String);
                dcol.Caption = col.Value;
                dt.Columns.Add(dcol);
            }
            foreach (var record in list)
            {
                DataRow rowToAppend = dt.NewRow();

                foreach (var col in columns)
                {
                    string[] keys = col.Key.Split('.');
                    if (keys.Length == 1)
                    {
                        object value = GetValue(record, col.Key);
                        rowToAppend[col.Key] = value != null ? value : string.Empty;
                    }
                    else
                    {
                        
                        System.Reflection.MethodInfo met = ExternalType.GetMethod(methodName);
                        if (met != null)
                        {
                            object parameter = GetValue(record, keys[0]);
                            if (parameter is int)
                            {
                                object row = met.Invoke(null, new object[] { parameter });
                                if (row != null)
                                {
                                    System.Type ObjectType = row.GetType();
                                    System.Reflection.PropertyInfo prop = ObjectType.GetProperty(keys[1]);
                                    if (prop == null)
                                        throw new Exception("La classe " + ObjectType.FullName + " non contiene la proprietà " + keys[1] + "");

                                    object[] param = { };

                                    try
                                    {
                                        object value = prop.GetGetMethod().Invoke(row, param);
                                        rowToAppend[col.Key] = value != null ? value.ToString() : string.Empty;

                                    }
                                    catch { rowToAppend[col.Key] = string.Empty; }
                                }
                            }
                            else
                            {
                                if (parameter != null)
                                {
                                    System.Reflection.PropertyInfo prop = parameter.GetType().GetProperty(keys[1]);
                                    if (prop != null)
                                    {
                                        string displayVal = prop.GetValue(parameter, null).ToString();
                                        rowToAppend[col.Key] = displayVal != null ? displayVal : "";
                                    }
                                    else
                                    {
                                        rowToAppend[col.Key] = "";
                                    }
                                }
                                else
                                {
                                    rowToAppend[col.Key] = "";
                                }
                            }
                        }
                    }
                }

                dt.Rows.Add(rowToAppend);
            }

            return dt;
        }

        protected static object GetValue(object objectInstance, string Property)
        {
            System.Type ObjectType = objectInstance.GetType();
            System.Reflection.PropertyInfo prop = ObjectType.GetProperty(Property);
            if (prop == null)
                throw new Exception("La classe " + ObjectType.FullName + " non contiene la proprietà " + Property + "");

            object[] parameters = { };

            try
            {
                return prop.GetGetMethod().Invoke(objectInstance, parameters);
            }
            catch { return null; }
        }

    }
}
