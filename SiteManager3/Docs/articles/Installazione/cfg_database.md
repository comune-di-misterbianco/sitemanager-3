# Configurazione server MySQL e database

Nell'istanza server MySql dovranno essere creati i db cms_nomeportale, cms_logs,  e inoltre dovrà essere creato un account utente cmsdbuser.
Accedere a mysql con l'utente root ed eseguire i seguenti comandi:

```sql
CREATE DATABASE cms_nomeportale;
CREATE DATABASE cms_logs;

CREATE USER 'cmsdbuser'@'%' IDENTIFIED BY '<password-utente-mysql>';

GRANT SELECT, INSERT, UPDATE, EXECUTE, DELETE, CREATE, ALTER, DROP, INDEX, CREATE VIEW, SHOW VIEW, LOCK TABLES ON `cms\_nomeportale`.* TO 'cmsdbuser'@'%' WITH GRANT OPTION;
GRANT SELECT, INSERT, UPDATE, EXECUTE, DELETE, CREATE, ALTER, DROP, INDEX, CREATE VIEW, SHOW VIEW, LOCK TABLES ON `cms\_logs`.* TO 'cmsdbuser'@'%' WITH GRANT OPTION;

GRANT SELECT ON `mysql`.`proc` TO 'cmsdbuser'@'%'  

FLUSH PRIVILEGES;
```

Dove **<password-utente-mysql>** dovrà essere sostituita con la password che si desidera utilizzare per l'account utente che si sta creando. 

> [!TIP]
> Su server Linux prestare attenzione all'apice di traverso ( `) usato per fare l'escape del carattere underscore nei comandi GRANT.   
Editare il file my.cnf  (file di configurazione di mysql) e aggiungere la seguente riga:
```sql
lower_case_table_names=1
```
Salvare il file my.cnf e riavviare mysql.
