# Setup del Sitemanger

Aprire un browser e digitare sulla barra degli indirizzi, l'url o l'ip associato al 
sito web attivato nella configurazione di iis e aggiungere in coda /setup.

Il sistema mostrerà la schermata di Setup del CMS 3 che si articola in 3 step:

1. Cartella cms e file di configurazione;
2. Parametri di configurazione 
3. Esito e riepilo 

![Schermata di setup](~/images/setup/cms-setup.jpg)

Nel campo `percorso di installazione del cms` si specificherà il percorso assoluto della cartella `**cms_nomeportale**`.

Selezionare il tasto `Avanti` per continuare con l'installazione.

La seconda schermata è articolata in tre blocchi distinti:

- dati generali sito web;
- dati database MySQL;
- dati server smtp.

Nella sezione `dati generali sito web` immettere il nome del portale, l'e-mail da accociare al portale per le notifiche operative e l'indirizzo assoluto della cartella dove saranno salvati i log di sistema.

![Blocco dati generali](~/images/setup/cms-setup-1.jpg)

Nella sezione "dati database MySQL" inserire l'indirizzo fisico della macchina su cui risiede il database, il nome e l'utente attivati nel sezione ["**Configurazione server MySQL e database**"](cfg_database.md), la porta TCP. 
Selezionare il tasto `Verifica connessione` per testare la comunicazione col database server.

![Blocco dati Mysql](~/images/setup/cms-setup-2.jpg)

Se tutto va a buon fine il bottone `Avanti` a fine pagina risulterà attivo ed abilitato per poter passare al terzo ed ultimo step della fase di installazione, atrimenti verificare i dati immessi e controllare eventuali problemi di comunicazione con il database server.

L'ultima sezione andrà compilata con i parametri smtp necessari all'invio di email di sistema.

![Blocco settaggi Smtp](~/images/setup/cms-setup-3.jpg)

Selezionare il tasto **Avanti** per proseguire; il sistema mostrerà il riepilogo dei dati inseriti. 

Selezionare il tasto "**Installa**" e attendere l'esito dell'installazione del sistema.

Durante questa fase il sistema in backgroud eseguirà le seguenti attività;

- inizializzazione del database
- creazione delle tabelle e i record di base;
- creazione dell'account di amministrazione del sistema "Administrator" la cui password verrà generata automaticamente e salvata nel file temporaneo `/repository/admin.txt` che dovrà essere eliminato dopo l'installazione;
- configurazione dei percorsi operativi del cms
- configurazione dei parametri smtp per l'invio delle notifiche di sistema.

![Schermata di riepilogo](~/images/setup/cms-setup-installa.jpg)

A fine installazione il sistema esporrà una schermata di riepilogo con gli indirizzi di accesso al frontend e al backoffice.

![Schermata di setup](~/images/setup/cms-setup-4.jpg)

Collegarsi all'indirizzo dove è stato installato il cms ed attendere che si avvii l'istanza e venga generato il file di mapping `fluentConfig.bin` nella cartella `App_Data` (potrebbe occorrere qualche minuto).

Accedere al backoffice del cms aggiungendo al path "/cms" e collegarsi con l'utente Administrator e la password generata automaticamente.



