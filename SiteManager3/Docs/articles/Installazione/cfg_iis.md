# Configurazione server Web

## Installazione asp.net

Installare sul server web il Framework .Net 4.7.2 nella lingua corrispondente a quella del sistema operativo in uso.

- https://dotnet.microsoft.com/download/dotnet-framework/net472

## Creazione cartella, attivazione permessi e copia dei file.

Nello spazio disco, individuato per contenere il sistema cms, creare una cartella denominata `cms_nomeportale`.

Dalla proprietà dalla cartella accedere alla schermata di gestione dei permessi di sicurezza - quindi cercare l'utente di sistema 
IIS_IUSRS e aggiungerlo. 

![Impostazione permessi](~/images/webserver/permessi_cartella_cms.jpg)

Selezionare dal pannello di riepilogo delle autorizzazione i checkbox della colonna Consenti rispettivamente per **lettura**,  **scrittura** e **modifica** e mantenere inalterati gli altri.
Confermare e chiudere le varie finestre.

Scaricare l'archivio zip contenente il pacchetto d'istallazione del sistema cms Sitemanager 3.0 e scompattarlo nella cartella appena creata.

## Configurazione IIS

Dal menu avvio selezionare Esegui, quindi digitare inetmgr. Apparirà la console di gestione di Internet Information Services (IIS).

### Inserimento e configurazione application pool

Dalla console di IIS occorrerà creare un nuovo application pool cliccando su "Aggiungi pool di applicazioni"

Apparirà una finestra come in figura dove settarne il nome. 

![Settaggio app pool](~/images/webserver/iis_application_pool.jpg)

La versione del framework dovrà essere 4.0 e la modalità di gestione pipeline "integrata". 

Mettere la spunta sulla checkbox "Avvia pool di applicazioni immediatamente" e cliccare su OK . 

### Inserimento e attivazione sito web

Dal pannello di gestione di IIS cliccare col pulsante destro su "Siti" e selezionare `Aggiungi sito web`: 
il sistema mostrerà una schermata dove configurare i parametri del sito web, la cartella fisica, il binding e l'application pool da utilizzare.

Nel campo `Nome Sito` bisogna specificare il nome del sito che si vuole creare. 

Dal pulsante selezionare l'application pool creato precedentemente.

Nel percorso fisico andrà inserito il percorso della cartella `cms_nomeportale` inserita precedentemente.

Nella sezione Binding selezionare l'indirizzo IP da associare al sito web o lasciare "Tutti non assegnati" per far in modo che il sistema risponderà a tutti gli ip associati al server. Inserire nel campo "nome host" il nome dns che verrà associato al portale web oppure lasciarlo vuoto. 

Lasciare la spunta su `Avvia sito Web immediatamente`. Cliccare su OK. 

Il sito web appena creato comparirà adesso all'interno della cartella siti.

### Attivazione del Setup

Cliccare sul simbolo `+` accanto al nome del sito web.
Navigare l'albero fino ad individuare la cartella `Setup`: col pulsante destro sulla cartella setup. Si aprirà un menu, da questo selezionare la voce `Converti in applicazione`.
