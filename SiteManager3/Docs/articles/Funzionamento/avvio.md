# Avvio del cms

Il sistema cms nella fase di avvio esegue diverse attività di seguito illustrate:

1. Caricamento degli assembly e verifica delle applicazioni
2. Verifica dello stato del database e sincronizzazione schema
3. Caricamento labels per lingua 
4. Caricamento delle configurazioni di sistema
5. Caricamento dei template

## Caricamento degli assembly

In questa fase il sistema itera tutti gli assembly presenti nella cartella **bin** e indentifica le applicazioni presenti suddividendole per tipologia (CMS e Verticali). 
Inoltre colleziona gli url necessari al routing e analizzando lo stato (attiva, non installata, non attiva) in modo tale da renderle disponibili.
Infine collezione tutti i moduli di homepage attivi e distribuiti nelle varie applicazioni di cms e verticali ed eventualmente presenti nella cartella App_Code, in modo da renderli disponibili nel sistema cms.

## Verifica dello stato del database e sincronizzazione schema

Il sistema in questa fase avvia l'orm nhbiernate che esamina ogni oggetto mappato nei vari assembly caricati in memeori, con la relativa rappresentazione tabellare e ne esegue la sincronizzazione genenrando automaticamente le query di creazione e modifica (ALTER e UPDATE).
In caso di eventuali errori, il sistema blocca l'attività eseguendo un revert e registrando nel log di sistema presente nella cartella /logs e datato con l'orario dell'evento, indicando la problematica riscontrata. 

## Caricamento delle labels per lingua

Vengono eseminati i file in formato xml, contenti all'interno della cartella /configs/labels/it-IT (organizzata secondo lo standard internazionale - CultureInfo Code)  contententi le 
traduzioni per le lingue disponibili, al fine di convalidarne la sintassi e la disponibilità di tutte le entry richieste dalle applicazioni e del sistema core.

## Caricamento delle configurazione di sistema

Viene istanziato l'oggetto statico CmsConfig che carica le variabili di ambiente relativa alla configurazione del sistema, al template e alle lingue attive.

## Caricamento dei template

Viene analizzato il contenuto della cartella themes al fine di verificare la presenza di nuovi temi e file tpl da rendere disponibili nel sistema.