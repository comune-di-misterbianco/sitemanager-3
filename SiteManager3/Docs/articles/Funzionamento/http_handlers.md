# Http Handlers

Il sistema cms Sitemanger 3.0 per la gestione degli url (Routing) fa uso di diversi Http Handler che analizzano la richiesta web pervenuta e la smistano opportunamente alle parti di core per l'elaborazione e la restituzione dell'elemento richiesto (pagina web, file statici, applicazioni di cms e verticali).

> [!NOTE]
> Gli handler principali sono:
> - Network_Handler.cs 
> - NetWorkUrlParser.cs
> - FrontendHandler.cs 

In queste fasi viene verificata l'effettiva esistenza della risorsa con la restituzione dell'opportuno codice di errore http (404, 500, 403 ecc.).

Viene verificata inoltre anche se eventualmente protetta da credenziali, reindirizzando  automaticamente l'utente alla pagina di login.

