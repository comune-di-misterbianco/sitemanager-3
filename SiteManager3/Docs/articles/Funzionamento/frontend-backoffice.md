# Struttura del CMS

Il sistema cms si articola in un'unica entità applicativa, distinta in diversi blocchi operativi che possono essere riassunte in tre endpoint: 

- frontend (/);
- backoffice (/cms);
- api (/api)

Il frontend espone le pagine web e i contenuti applicativi gestiti nel backoffice, inseriti e elaborati da utenti opportunamente profilati.

Il backoffice permette la gestione del sistema, l'installazione e l'aggiornamento delle applicazione, la gestione delle anagrafiche e degli utenti, della gestione dei permessi.

Le api espongono i dati in modalita REST in formato json (default) e o xml in funzione alle impostazioni di sistema.

Attraverso l'uso e l'implementazione delle applicazioni verticali è possibile integrare nuove funzionalità estendendo le implementazioni core del cms Sitemanager.

> [!NOTE]
> Un esempio esplicativo è l'applicazione **Amministrazione Trasparente**, una suite di
> diverse applicazioni verticali che si occupano 
> di risolvere uno specifico problema gestendo e pubblicando dati richiesti dalla
> normativa nazionale.




