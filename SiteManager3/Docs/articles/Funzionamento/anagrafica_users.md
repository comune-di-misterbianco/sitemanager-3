# Utenti e anagrafica

Il sistema cms Sitemanager 3.0 integra al suo interno le funzionalità per la gestione di utenza multi anagrafiche.

All'operatore admministratore è consentito la creazione di nuove tipologie di utenza attraverso la funzionalità **Anagrafe Custom** dove includere campi customizzati in base alle esigenze.
Tali anagrafiche una volta attive nel sistema posso essere utilizzata direttamente dal pannello di gestione utenze e anche dalle applicazioni verticali per esigenze specifiche.

Attualmente il sistema supporta tre sistemi di autenzicazione:

- interna gestita con gli oggetti nativi e le tabelle del sitemanager;
- oracle single sign on, attraverso il filtro isapi Oracle SSO;
- active directory (LDAP personalizzata da Microsoft).


