# Diagnostica

In fase di boostrap il sistema verifica il funzionamento delle varie componenti software che lo compongono e inoltre monitora 
lo stato globale. 

Ad ogni richiesta viene verificato attraverso il modulo http CmsStatusModule lo stato di raggiungibilità del database, eventuali errori di sistema 
e l'eventuale messa in manutenzione del portale.

Il sistema inoltre, attraverso il componente core [Diagnostics](xref:NetCms.Diagnostics.Diagnostics) basato su Log4net, consente la registrazione degli eventi avvenuti durante l'attività applicativa:

sono previsti 6 livelli di tracciatura: Off, Verbose, Info, Warning, Error e Fatal.

Per impostare il livello di tracciatura del log su disco cercare nel file web.config, presente nella root del cms, la stringa `<add key="log4netVerbosityLevels" value="5"/>` all'interno della sezione appsetting del web.config e modificare il parametro value al valore corrispondente del livello desiderato

## Struttura di una entry nel log

Di seguito un esempio di entry di tipo errore registra nel file log:

```xml
<log4net:event logger="/.0becce5f-194c-412f-a534-5a6352222b36" timestamp="2019-09-25T11:14:56.3259673+02:00" level="ERROR" domain="/LM/W3SVC/2/ROOT-2-132138705953153638" logID="1909-2511-1456-325-0154733823" httpRequestID="0becce5f-194c-412f-a534-5a6352222b36" component="Frontend" smLevel="ERROR">
    <log4net:message>Riferimento a un oggetto non impostato su un'istanza di oggetto.</log4net:message>
    <log4net:properties>
        <log4net:data name="ServerName" value="localhost" />
        <log4net:data name="log4net:HostName" value="DESKTOP-5VEBKIQ" />
        <log4net:data name="ThreadLanguage" value="Italiano (Italia)" />
        <log4net:data name="UserAgent" value="Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0" />
        <log4net:data name="Form" value="" />
        <log4net:data name="QueryString" value="(null)" />
        <log4net:data name="Url" value="http://localhost/applicazioni/notizie/default.aspx" />
        <log4net:data name="log4net:Identity" value="" />
        <log4net:data name="log4net:UserName" value="DESKTOP-5VEBKIQ\Angelo" />
        <log4net:data name="UserHostName" value="127.0.0.1" />
    </log4net:properties>
<log4net:locationInfo class="NetCms.Exceptions.ExceptionsHandler" method="HandleException" file="\SiteManager3\Core\Configurations\ExceptionsHandler.cs" line="41" />
</log4net:event>
```
I file log, generati con cadenza giornaliera, sono generalemte contenuti nella cartella root del cms \logs e 
il nome del file presenta la seguente sintassi:

```xml
cms-<data>-<progressivo>.log
```
dove:
`<data>`	indica la data di riferimento del log il cui formato è `GG-MM-AAAA`
`<progressivo>` indica un numero incrementale nella stessa data

Il sistema di monitoring del portale registra un insieme di dati necessari per l'individuazione di eventuali attivittà critiche e informative, secondo uno specifico formato descritto di seguito:

| Parametro | Informazione       | Descrizione                           |
|-----------|--------------------|---------------------------------------|
| LogID     | Identificativo log | Rappresenta un identificativo univoco |
| datetime  | Data ora | Data ora dell'evento nel formato gg/mm/aaaa hh.mm.ss.ms |
| level |Livello |Indica il livello informativo. I valori assumibili sono: - Info - Verbose - Warning - Error - Fatal |
| URL  | Percorso pagina che ha generato l'entry  | Riporta l'indirizzo completo della pagina che ha generato l'entry |
| Querystring  | Parametri  | Riporta interamente i parametri inviati alla pagina. Tale valore potrebbe essere vuoto quando la querystring non contiene parametri.|
| File  | Percorso componente   |  Indica il percorso interno della classe  |
| Class  | Nome componente  |  Indica la classe  |
| Componente  | Componente  | Indica il componente |
| Message  | Messaggio  | Riporta il messaggio completo |
| StackTrace  | Messaggio dello stack |  Riporta interamente la descrizione dello stack trace. Tale messaggio viene registrato solo con i livelli Fatal ed Error |
| UserIP  | Indirizzo ip del client| Riporta l'indirizzo ip del client connesso |
| UserID  | Identificativo numerico| Riporta l'ID utente connesso rintracciabile nella tabella users. Tale valore può essere zero quando si verifica un errore non concatenato a azioni riconducibili a utenti, oppure il valore non è disponibile. |
| UserName | Identificativo testuale   | Riporta l'username dell'utente connesso. Tale valore può essere vuoto quando si verifica un errore non concatenato a azioni riconducibili a utenti.|

La lettura e l'analisi dei log di sistema risulta più agevole utilizzando il tool log4view scaricabile dall'indirizzo https://www.log4view.com/download-en (disponibile in versione trial 30 giorni e successivamente in licenza community edition). 

Inserire schermata del tools

## Log di IIS

L'application server IIS analizza e registra tutte le informazioni che transitano dai siti e dalle applicazione web ospitate. 
Tali informazioni vengono registrate di default su file log nel formato W3C Extended Log File Format, e normalmente sono contenuti sotto la cartella

```
c:\windows\system32\logfiles\W3SVC<identificativo-sito> 
```

dove  
`<identificativo-sito>` rappresenta il codice di identificazione del sito web.

Il codice identificativo del sito web è rintracciabile all'interno della console di gestione di IIS, espandendo
 il nodo `Siti` nella tabella visualizzata nel pannello centrale e in corrispondenza del sito web nella colonna `ID`.

![ID sito web](~/images/webserver/iis_identificativo_sito.jpg)
