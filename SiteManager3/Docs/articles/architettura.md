# Architettura

L'architettura del Cms Sitemanager è basata sul paradigma della Web.Form di Asp.Net,
 per la parte relativa alla gestione del ciclo di vita della pagine e dei controlli web quali ([WebControl](https://docs.microsoft.com/it-it/dotnet/api/system.web.ui.webcontrols.webcontrol?view=netframework-4.8) e [LiteralControl](https://docs.microsoft.com/it-it/dotnet/api/system.web.ui.literalcontrol?view=netframework-4.8)) e degli Http Handler
e per la parte di presentazione si basa sul template engine open source [Scriban](https://github.com/lunet-io/scriban).

La parte relativa alla gestione dati e connessione con il database, complessivamente, è gestita con il sistema ORM [Nhibernate](https://github.com/nhibernate/nhibernate-core) e per la parte di mappatura dei dati è gestita con il sistema [FluentNhibernate](https://github.com/FluentNHibernate/fluent-nhibernate).

![Schema architettura](~/images/CMS_Architettura.jpg)

Il sistema di diagnostica si basa su [Log4net](https://logging.apache.org/log4net/release/features.html) con una personalizzazione delle entry su file system al fine di essere gestite e monitorate in tempo reale attraverso il tool [log4View](https://www.log4view.com/download-en).

Il sistema offre anche il supporto all'esposizione dei dati attraverso webservice rest basate su architetture [Web Api](https://docs.microsoft.com/it-it/aspnet/web-api/overview/getting-started-with-aspnet-web-api/tutorial-your-first-web-api) fornite dal Framework .Net.