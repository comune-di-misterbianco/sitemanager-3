# Applicazione Verticale 

Per realizzare una applicazione verticale nel cms Sitemanager 3 è necessario aggiungere  un nuovo progetto C# `Class Library (.NET Framework)` in .Net Framework 4.7.2, fornendo come Project Name il nome dell'applicazione. Il progetto dovrà avere come piattaforma di destinazione Any CPU (Platform target).

## Reference minime

Queste sono le reference minime per un applicazione verticale:

### Riferimenti a libreria del framework.net  
  - System.Core
  - System
  - System.Web
  - System.Xml  
  

### Riferimenti a librerie del cms
- Configurations -> Espone gli oggetti e le proprietà relative alla configurazione del cms e dell'ambiente;
- FamFamIcons - Collezione di icone utilizzabili nel sistema ( `deprecate` si consiglia l'uso delle icone font awesome 5 attraverso le classi css https://fontawesome.com/how-to-use/on-the-web/referencing-icons/basic-use)
- Networks - Libreria che contiene le classi per la gestione dei sottoportali quando il sistema è configurato in `modalità multi site`;
- SharedUtilities - Libreria con oggetti di utilità generica;
- Frontend - Libreria core del cms;
- Structure - Libreria core del cms - contenente le funzionali di backoffice;
- Users - Libreria core con oggetti specializzati per le entità Utenti e Anagrafi Custom;
- Vertical - Libreria core per la gestione delle applicazioni verticali.
- GenericDao - Libreria di dialogo `generica` per le operazioni di lettura e scrittura sul il database;
- DbSessionProvider - Libreria di gestione e dialogo con l'ORM;
- ValidateFields - Libreria con oggetti specializzati al gestione delle form e dei campi;
- RecordFinder - Libreria con oggetti specializzati alla lettura e generazione delle query attraverso sistemi di wrapping basati su Nhibernate.  

### Riferimenti a librerie di terze parti (sorgente Nuget)
  - Nhibernate 3.2.0.4000
  - IesiCollections 1.0.1.0
  - FluentNhibernate 1.3.0.717
  - MySql.Data 6.9.12.0

Di seguito le classi necessarie all'implementazione dell'applicazione verticale con le funzionalità base:

- **Application.cs** -> Eredita da VerticalApplication e definisce l'applicazione.
- **Setup.cs** -> Eredita da VerticalApplicationSetup e definisce la procedura di istallazione.
- **MainPage.cs** -> Eredita da VerticalApplicationMainPage e definisce la pagina principale dell'applicazione da cui far ereditare le altre pagine.

Per le applicazioni dotate di frontend è necessario implementare le seguenti classi, che consentono l'override dell'interfacce grafiche:

- **FrontAdapter.cs**
- **InformationBox.cs**
- **Toolbar.cs**

E' sufficiente implementare le classi, nominandolo come indicato, ed implementare i metodi astratti.

## Implementazione delle classi basi

### Application.cs  
Il file eredita da VerticalApplication e definisce l'applicazione in se. Al suo interno è necessario effettuare l'override di alcune proprietà astratte in modo da specificare i dati dell'applicazione da creare.
Tutte le proprietà di cui è necessario fare l'override sono state commentate direttamente nel codice.

### Setup.cs  
Il file eredita da VerticalApplicationSetup e definisce la procedura di istallazione. 
Al suo interno è necessario effettuare l'override di alcune proprietà astratte.
Tutte le proprietà di cui è necessario fare l'override sono state commentate direttamente nel codice.

### Pagine  
Per creare una nuova pagine è necessario creare una classe nella libreria dell'applicazione che erediti da `ApplicationPageEx` definita nel namespace `NetCms.Vertical`. 

Inoltre questa classe deve avere l'attributo `DynamicUrl.PageControl`

```cs
[DynamicUrl.PageControl("/details.aspx")]
public class DetailsPage : ApplicationPageEx
```

Per ogni pagina è necessario ridefinire 3 proprietà:

- PageTitle: rappresenta il titolo della pagina.
- Icon: rapresenta l'icona per le applicazioni di cms, ma non viene usato per quelle di front.
- Control: deve restuire un WebControl con il contenuto della pagina.

Ogni pagina ha una proprietà Toolbar che rappresenta la toolbar dell'applicazione.
Per aggiungere un bottone alla toolbar è necessario appendere un oggetto `ToolbarButton` alla collezione `Buttons` dell'oggetto `Toolbar`.

```cs
this.Toolbar.Buttons.Add(new ToolbarButton("page.aspx", NetCms.GUI.Icons.Icons.Vcard_Add, "Vai Alla Mia Pagina "));
```

### Web.Config  
All'interno del web.config vanno messe le eventuali configurazioni dell'applicazione.
Per farlo è necessario aggiungere una section che abbia come nome il SystemName dell'applicazione stessa.

```xml
<configSections>
    <section name="MyApplication" type="System.Configuration.NameValueFileSectionHandler,System, Version=1.0.3300.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
</configSections>
<MyApplication>
    <add key="myProperty" value="myValue" />
</MyApplication>
```

Nell'esempio è stata impostata la proprietà `myProperty` al valore `myValue` per l'applicazione `MyApplication`.

Per accedere alle properità di configurazione è necessario aggiungere questa propietà alla classe Application:

```cs
public NetCms.Vertical.Configuration.Configuration Configuration
{
    get
    {
        return _Configuration ?? (_Configuration = new NetCms.Vertical.Configuration.Configuration(this.SystemName));
    }
}
private NetCms.Vertical.Configuration.Configuration _Configuration;
```

Dalla quale potrà essere richiamata in questo modo:

```cs
string myValue = application.Configuration["myProperty"];
```

### Gestione dei permessi

Per verificare i permessi di un utente è necessario aggiungere un oggetto GrantsManager, passando come parametri il system name dell'applicazione e il profilo dell'utente corrente:

```cs
NetCms.Users.GrantElement grantsManager = new NetCms.Users.GrantElement("myApplication",NetCms.Users.Profile.CurrentProfile);
```

Successivamente potrà essere usato questo oggetto per accedere ai permessi in questo modo:

```cs
grantsManager["myCriterianame"].Allowed
```

che restituirà `true` se l'utente è autorizzato.
