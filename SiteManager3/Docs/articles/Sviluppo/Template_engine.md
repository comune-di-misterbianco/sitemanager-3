# Template engine

Il sistema cms Sitemanager 3.0 fa uso di un template engine basato sulla piattaforma opensource Scriban https://github.com/lunet-io/scriban per la gestione delle pagine di frontend e dei moduli applicativi delle homepage.

Tale sistema basa il suo funzionamento sul parsing dei template in formato `liquid` molto simile al sistema Razor usato in ambiente MVC. 

Si rimanda alla documentazione di scriban per maggiori approfondimenti:  https://github.com/lunet-io/scriban/blob/master/doc/readme.md.

Nel cms si utilizza una prerogativa tipica di Scriban: i dati elaborati devono essere convertiti in dati riconducibili a tipi base (int, string, object, bool ecc.) e vanno passati al parser del template attraverso oggetti anonimi: 
```cs
var myAnonymousType = new { firstProperty = "First", 
    secondProperty = 2, 
    thirdProperty = true 
};
```
## Come funziona il template engine

Per utilizzare il template engine è necessario aggiungere una reference via nuget al pacchetto Scriban, versione 2.1.0.

Ipotizzando di voler esporre dei dati di tipo News restituiti da una ipotetica funzione `GetNews()` strutturati come `List<News>` dove `News` è un oggetto cosi costituito:

```cs
public class News
{
    public string Titolo { get; set; }
    public int Id { get;set; }
    public string Css_Class { get; set; }
}
```
Di seguito il codice che istanzia il template engine e renderizza nella variabile `result` il codice html che dovrà essere restituito al controllo web della pagina o del modulo in uso.  

```cs
List<News> news = NewsBusinessLogic.GetNews();

string tpl_path = NetCms.Configurations.Paths.AbsoluteRoot + "/percorso-del-file-tpl/";
tpl_path = System.Web.HttpContext.Current.Server.MapPath(tpl_path);
 
string result = string.Empty;
var template = Scriban.Template.Parse(System.IO.File.ReadAllText(tpl_path), tpl_path);
if (!template.HasErrors)
{
    var context = new Scriban.TemplateContext();
    context.PushCulture(System.Globalization.CultureInfo.CurrentCulture);

    var scriptObj = new Scriban.Runtime.ScriptObject();
    scriptObj.Add("items", news);
    context.PushGlobal(scriptObj);

    result = template.Render(context);
}
else
{
    foreach (var error in template.Messages)
        result += error.Message + System.Environment.NewLine;
}
```

## Esempio di template 

Di seguito è riportato un esempio di implementazione di un template tipo per la visualizzazione di dati inpaginati in formato html:

```html
<div class="news">
    {{if items.size > 0 }}
    <ul>
        {{ for item in items }}
        <li class="{{item.css_class}}">{{item.titolo}}</li>
        {{end}}
    </ul>
    {{end}}
</div>
```

I placeholder accettati dal sistema sono racchiusi all'interno di doppie parentesi graffe `{{placeholder}}` e verranno sostituiti a runtime dal parser del template engine. Inoltre all'interno delle {{}} è possibile inserire blocchi di codice che esegue ulteriori elaborazioni alle informazioni da gestire.

Per le funzionalità previste dal template engine Scriban si rimanda alla documentazione ufficiale [Docs](https://github.com/lunet-io/scriban/tree/master/doc).

> Nota   
> I parametri, i metodi e le variabili passati al template engine, per convenzione devono essere dichiarati in modalità minuscolo (lowercase) e gli eventuali camelcase vanno sostituiti con il simbolo underscore `_`. 