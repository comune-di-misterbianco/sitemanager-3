# Applicazioni Verticali: gestione delle pagine

Esempio di una pagina generica:

```cs
namespace ApplicazioneVerticaleTest.Pages
{
    [DynamicUrl.PageControl("/pagina-di-test.aspx")]
    public class Test : AppPageAbstract
    {
        public Test(StateBag viewState, bool isPostBack, Toolbar toolbar, NetCms.GUI.Info.InfoBox informationBox, NetCms.Networks.NetworkKey networkKey)
            : base(viewState, isPostBack, toolbar, informationBox, networkKey)
        {
             SetToolbar();
        }
        
         private void SetToolbar()
        {
            this.Toolbar.Buttons.Add(new ToolbarButton("default.aspx", NetCms.GUI.Icons.Icons.User, "Home" { CssClass = "go-home" });
            this.Toolbar.Buttons.Add(new ToolbarButton("/profilo.aspx", NetCms.GUI.Icons.Icons.User, "Mostra profilo") { CssClass = "show-profile"});
        }
    
        public override string PageTitle
        {
            get
            {
                return base.PageTitle + " Pagina di test";
            }
        }
        
        public override NetCms.GUI.Icons.Icons PageIcon
        {
            get
            {
                return NetCms.GUI.Icons.Icons.Application;
            }
        }
        
        public override System.Web.UI.WebControls.WebControl BuildControl()
        {
            WebControl control = new WebControl(HtmlTextWriterTag.Div);
            control.CssClass = "test-app"
             
            WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
            fieldset.ID = "id-test-app";
            fieldset.CssClass = "ctr-app";

            WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
            legend.Controls.Add(new LiteralControl("Pagina di test"));
            fieldset.Controls.Add(legend);                
            
            control.Controls.Add(fieldset);
            
            eturn control;
        }
    }
}
```