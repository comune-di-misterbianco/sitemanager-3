# Gestione delle form 
Nel sistema cms Sitemanager la gestione delle form è gestita con l'oggetto [VfManager](xref:NetService.Utility.ValidatedFields.VfManager) che estende il WebControl [Panel](https://docs.microsoft.com/it-it/dotnet/api/system.web.ui.webcontrols.panel) del Framework .net. e si occupa della costruzione e del rendering delle form html.

## Descrizione

Di seguito le attività  svolte dall'oggetto VfManager:

- Aspetto del form - template in modalita bootstrap 3 o 4 o xhtml 1.1;
- Validazione i dati ed esposizione degli errori;
- Costruzione del form dinamnicamente;
- Binding automatico dei campi con i relativi valori;
- Interazione con l'utente (Postback)

La proprietà [`Fields`](xref:NetService.Utility.ValidatedFields.VfManager.Fields) dell'oggetto VfManager collezione tutti i campi da esporre:
iterandoli e leggendo la proprietà PostBack e o PostBackValue e possibile recuperare il valore.

Il VfManager attraverso la proprietà UseBootstrap e BootstrapVer consente il rendering della form html utilizzando le specifiche del framework web Boostrap 3 o 4 in modalità responsive o in modalità xhtml.

La proprietà [`IsValid`](xref:NetService.Utility.ValidatedFields.VfManager.IsValid) fornisce lo stato di validazione del PostBack del form.

Implementando gli EventHandler dei bottoni Submit e BackSubmit e possibile definiere le azioni di interazione con il form.

Nel VfMananger il metodo `DataBindAdvanced` consente l'auto popolamento dei valori dei campi Field presenti nella lista `Fields` tramite dei meccanismi di reflection. 

## Implementazione della form

Per utilizzare il componente VfManager è necessario referenziare la libreria core del cms denomninata ValidateField.

Esempio d'implementazione

```cs
 public class UfficioForm : VfManager
    {
        public UfficioForm(string controlId, LabelsManager.Labels labels, Ufficio ufficio = null)
            : base(controlId) 
        {
            APLabels = alboPretorioLabels;
            CurrentUfficio = ufficio;

            this.Fields.Add(Nome);
            this.Fields.Add(Stato);

            if (ufficio != null)
                this.DataBindAdvanced(ufficio);
        }
    }
```
Esempio d'uso

```cs
public UfficioForm UfficioForm
{
    get
    {
        if (_UfficioForm == null)
        {
            _UfficioForm = new UfficioForm("UfficioForm", APLabels, CurrentUfficio.Instance);
            _UfficioForm.ShowMandatoryInfo = false;
            _UfficioForm.CssClass = "Axf";
            _UfficioForm.SubmitButton.Text = "Salva";                                    _UfficioForm.SubmitButton.Click += new EventHandler(SubmitButton_Click);
        }
        return _UfficioForm;
    }
}
private UfficioForm _UfficioForm;   
```  

Esempio di EventHandler associato al click sul bottone SubmitButton:

```cs
void SubmitButton_Click(object sender, EventArgs e)
{
    if (this.UfficioForm.IsValid == VfGeneric.ValidationStates.Valid)
    {
        Message esito = BusinessLogic.UfficioBL.ModificaUfficio(this.UfficioForm, CurrentUfficio.Instance);
        NetCms.GUI.PopupBox.AddSessionMessage(esito.Text, esito.MessageType, true);             
    }   
}
```