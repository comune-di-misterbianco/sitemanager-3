# Applicazioni
Il cms Sitemanager 3.0 è esteso attraverso moduli applicativi denominate applicazioni e che si suddividono in applicazioni di cms e applicazioni verticali.

Le applicazioni di cms si occupano della gestione dei contenuti ridefinendo le funzionalità base del sistema core, estendendole per costituire specifiche tipologie di documenti web con funzionalità operative e grafiche differenti. 

Nel sistema sono disponibili in modo predefinito le seguenti applicazioni suddivise in due gruppi:

- Applicazione base:
  - Homepage: gestisce la visualizzazione e la modifica delle homepage, attraverso i moduli applicativi e interfaccie UI semplici;
  - Files: gestisce funzionali di trattamento dei file su disco rigido;
  - Links: gestisce e collezione i collegamenti;
  - WebDocs: espone e gestisce la pagine web;
  - News: espone e gestisce contenuti web in modalità news (blog post);

- Applicazioni tipizzate:
  - Comunicati stampa
  - Rassegna stampa
  - Eventi
  - Bandi di gara
  - Gallerie immagini
  - Faq
  
Le applicazioni verticali assolvono a specifici compiti in cui è richiesta un iter procedurale particolare e non basta la semplice pubblicazione di contenuti. 

