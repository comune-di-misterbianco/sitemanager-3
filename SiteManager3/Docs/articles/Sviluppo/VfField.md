# I campi delle form (Field)

Rappresenta il campo da insereire nel form ed è definito in modo astratto in VfGeneric e viene tipizzato per assolvere a specifici compiti estendendo le funzionalità specifiche in relazione alle esigenze.

Alcune proprietà e meccanismi dell'oggetto Field:

 - `PostBackValue`: Ogni oggetto VF offre la possibilità di richiamare i dati inseriti dall’utente in maniera trasparente, richiamando, infatti, la proprietà PostBackValue verrà automaticamente effettuata un lettura dei valori di post back per ottenere l’informazione richiesta.
- `Validazione`: Tramite la proprietà isValid è possibile conoscere lo stato di validazione di ogni campo in cui sono stati inseriti i dati. Ogni campo possiede dei meccanismi di validazione propri per i tipi di dato che può contenere. 
- `Riempimento automatico di oggetti`: Ogni campo VF può essere associato ad una proprietà di un oggetto, in questo modo è possibile riempire automaticamente gli attributi di un oggetto con i dati contenuti all’interno di un form.
- `SearchParameter automatici`:  è possibile ottenere il corrispondente [SearchParameter](xref:NetService.Utility.RecordsFinder.SearchParameter) contente i dati immessi dall’utente. Questo meccanismo è molto utile nei form di ricerca per inserire dati parametrici all’interno di query che ne fanno uso.

Elenco dei campi disponibili:

[Consulta l'elenco](xref:NetService.Utility.ValidatedFields)

Esempio d'uso
```cs
private VfTextBox Nome
{
    get
    {
        if (_Nome == null)
        {
            _Nome = new VfTextBox(Model.Ufficio.Column.Nome.ToString(), APLabels[AlboPretorioLabels.AlboPretorioLabelsList.SoggettoEmittente], Model.Ufficio.Column.Nome.ToString(), InputTypes.Text);
            _Nome.Required = true;
            _Nome.TextBox.Rows = 40;
        }
        return _Nome;
    }
}
private VfTextBox _Nome;
```