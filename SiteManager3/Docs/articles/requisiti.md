# Requisiti

Il Cms Sitemanager 3.0, per il corretto funzionamento richiede un sistema server basato su architettura Microsoft e che soddisfi i seguenti requisiti 

Componente server | Requisiti 
--- | --- 
Web server | - Microsoft Windows Server 2008 R2, 2012, 2012 R2, 2016, 2019, Windows 10 - Internet Information Services dalla versione 7.0 - Microsoft .Net Framework 4.7.2 - Microsoft ASP.NET MVC 5
Database   | MySQL Server a 64 bit versione 5,7 e Maria DB dalla 10.3
Memoria    | 2 GB minima
Spazio disco | 500 MB minimo
Browser supportati | Firefox, Chrome, Edge con Javascrip abilitato.
Strumenti di sviluppo | Tutte le versioni di Visual Studio dalla version 2015

Il sistema può essere installato su un unico server contenete anche il database server, purchè le risorse allocate a MySql 
rispondano ai requisiti richiesti dal sistema database e dal carico di richieste.

E' consigliato comunque in relazione al carico massimo ipotizzato a cui il portale sarà soggetto in termini di richieste ora, 
suddividere il server web e il server database su sistemi separati.
