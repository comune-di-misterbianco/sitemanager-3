﻿using GenericDAO.DAO;
using NetCms.DBSessionProvider;
using NetService.Utility.RecordsFinder;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.Models.Token.UserTokens;

namespace Users.BusinessLogic
{
    public class UserTokenBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static UserToken SaveOrUpdate(UserToken usertoken, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<UserToken> dao = new CriteriaNhibernateDAO<UserToken>(innerSession);

            return dao.SaveOrUpdate(usertoken);
        }

        public static UserToken GetById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<UserToken> dao = new CriteriaNhibernateDAO<UserToken>(innerSession);

            return dao.GetById(id);
        }

        public static IList<UserToken> GetUserTokens(int userId, string loginProvider = null, string name = null, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<UserToken> dao = new CriteriaNhibernateDAO<UserToken>(innerSession);
            List<SearchParameter> searchParameters = new List<SearchParameter>();

            SearchParameter userIdSP = new SearchParameter(null, "UserId", userId, Finder.ComparisonCriteria.Equals, false);
            searchParameters.Add(userIdSP);

            SearchParameter loginProviderSP = null;
            if (!string.IsNullOrEmpty(loginProvider))
            {
                loginProviderSP = new SearchParameter(null, "LoginProvider", loginProvider, Finder.ComparisonCriteria.Like, false);
                searchParameters.Add(loginProviderSP);
            }
            SearchParameter nameSP = null;
            if (!string.IsNullOrEmpty(name))
            {
                nameSP = new SearchParameter(null, "Name", name, Finder.ComparisonCriteria.Like, false);
                searchParameters.Add(nameSP);
            }

            //return dao.GetByCriteria(   searchParameters.ToArray()  );
            return dao.FindByCriteria (searchParameters.ToArray());
        }

        public static void Delete(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<UserToken> dao = new CriteriaNhibernateDAO<UserToken>(innerSession);
            UserToken usertoken = dao.GetById(id);
            dao.Delete(usertoken);
        }
    }
}
