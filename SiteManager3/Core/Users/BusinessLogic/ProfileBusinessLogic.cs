﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NetCms.DBSessionProvider;
using GenericDAO.DAO;

namespace NetCms.Users
{
    public static class ProfileBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static Profile SaveOrUpdateProfile(Profile profile, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Profile> dao = new CriteriaNhibernateDAO<Profile>(innerSession);

            return dao.SaveOrUpdate(profile);
        }

        public static Profile GetById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Profile> dao = new CriteriaNhibernateDAO<Profile>(innerSession);

            return dao.GetById(id);
        }

        public static void DeleteProfile(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Profile> dao = new CriteriaNhibernateDAO<Profile>(innerSession);
            Profile profile = dao.GetById(id);
            dao.Delete(profile);
        }
    }
}
