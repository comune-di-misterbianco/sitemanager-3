﻿using NetCms.DBSessionProvider;
using NetCms.Users.CustomAnagrafe;
using NHibernate;
using System;
using System.Collections;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Security;

namespace NetCms.Users
{
    public static class ADBusinessLogic
    {
        public static bool AD_Is_Enabled()
        {
            bool status = false;

            // code 
            // verificare dal web.config se la modalità

            return status;
        }
                
        public static User IsLogged()
        {
            User currentADUser = null;


            return currentADUser;
        }

        public static User AddNewUser(string ADUserName)
        {
            string anagrafeName = "ActiveDirectory";
            string anagraficaType = "Anagrafica" + anagrafeName;

            User currentADUser = null;

            using (ISession sess = FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {                   
                    CustomAnagrafe.CustomAnagrafe ADAnagrafe = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(anagrafeName, sess);
                    

                    if (ADAnagrafe != null)
                    {
                        DirectoryEntry entry = new DirectoryEntry();
                        entry.Username = NetCms.Configurations.Generics.ActiveDirectoryUser;
                        entry.Password = NetCms.Configurations.Generics.ActiveDirectoryPw;
                        entry.Path = NetCms.Configurations.Generics.ActiveDirectoryPath;

                        SearchResult results = FindADUser(entry, ADUserName);
                        if (results != null)
                        {
                            string email = (results.Properties.Contains(DCPropertyNames.mail.ToString()) && !string.IsNullOrEmpty(results.Properties[DCPropertyNames.mail.ToString()][0].ToString()) ? results.Properties[DCPropertyNames.mail.ToString()][0].ToString() : "");
                            string username = (results.Properties.Contains(DCPropertyNames.sAMAccountName.ToString()) && !string.IsNullOrEmpty(results.Properties[DCPropertyNames.sAMAccountName.ToString()][0].ToString()) ? results.Properties[DCPropertyNames.sAMAccountName.ToString()][0].ToString() : "");
                            string cmsPassword = "AD_" + username + "#21";
                            cmsPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(cmsPassword, "MD5").ToString();

                            Hashtable anagrafica = new Hashtable();
                            anagrafica.Add("Email", email);
                            anagrafica.Add("Nome utente", username);
                            anagrafica.Add("Nome", (results.Properties.Contains(DCPropertyNames.givenName.ToString()) && !string.IsNullOrEmpty(results.Properties[DCPropertyNames.givenName.ToString()][0].ToString()) ? results.Properties[DCPropertyNames.givenName.ToString()][0] : ""));
                            anagrafica.Add("Cognome", (results.Properties.Contains(DCPropertyNames.sn.ToString()) && !string.IsNullOrEmpty(results.Properties[DCPropertyNames.sn.ToString()][0].ToString()) ? results.Properties[DCPropertyNames.sn.ToString()][0] : ""));                          
                            anagrafica.Add("Dipartimento", (results.Properties.Contains(DCPropertyNames.department.ToString()) && !string.IsNullOrEmpty(results.Properties[DCPropertyNames.department.ToString()][0].ToString()) ? results.Properties[DCPropertyNames.department.ToString()][0] : ""));
                            anagrafica.Add("Titolo", (results.Properties.Contains(DCPropertyNames.title.ToString()) && !string.IsNullOrEmpty(results.Properties[DCPropertyNames.title.ToString()][0].ToString()) ? results.Properties[DCPropertyNames.title.ToString()][0] : ""));
                            anagrafica.Add("Ufficio", (results.Properties.Contains(DCPropertyNames.physicalDeliveryOfficeName.ToString()) && !string.IsNullOrEmpty(results.Properties[DCPropertyNames.physicalDeliveryOfficeName.ToString()][0].ToString()) ? results.Properties[DCPropertyNames.physicalDeliveryOfficeName.ToString()][0] : ""));
                            anagrafica.Add("CN", (results.Properties.Contains(DCPropertyNames.cn.ToString()) && !string.IsNullOrEmpty(results.Properties[DCPropertyNames.cn.ToString()][0].ToString()) ? results.Properties[DCPropertyNames.cn.ToString()][0] : ""));
                            anagrafica.Add("Telefono", (results.Properties.Contains(DCPropertyNames.homePhone.ToString()) && !string.IsNullOrEmpty(results.Properties[DCPropertyNames.homePhone.ToString()][0].ToString()) ? results.Properties[DCPropertyNames.homePhone.ToString()][0] : ""));
                            anagrafica.Add("Cellulare", (results.Properties.Contains(DCPropertyNames.mobile.ToString()) && !string.IsNullOrEmpty(results.Properties[DCPropertyNames.mobile.ToString()][0].ToString()) ? results.Properties[DCPropertyNames.mobile.ToString()][0] : ""));

                            ICollection<Group> gruppo = new List<Group>();
                            gruppo.Add(ADAnagrafe.DefaultGroup);

                            currentADUser = UsersBusinessLogic.CreateUser(sess, username, cmsPassword, DateTime.Now, DateTime.Now, DateTime.Now, 1, anagrafica, anagraficaType, gruppo, true, new Random().Next(9999999).ToString());
                            tx.Commit();
                        }
                    }
                    else
                    {
                        // anagrafica ActiveDirectory non installata
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'anagrafica ActiveDirectory non è installata. Impossibile importare l'utente '"+ ADUserName + "' - nessun dato è stato inserito.");
                    }
                }
                catch (Exception ex)
                {
                    string error = "Errore tecnico durante l'inserimento dell'utente '"+ ADUserName + "' active directory del database del cms.";
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, error,"", "", ex);
                    tx.Rollback();
                }
            }

            return currentADUser;
        }     

        public static void NotifyAdmin()
        {

        }

        private static SearchResult FindADUser(DirectoryEntry entry,string username)
        {
            SearchResult result = null;

            try
            {
                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = "(SAMAccountName=" + username + ")";
                search.PropertiesToLoad.Add(DCPropertyNames.cn.ToString());                              // common name
                search.PropertiesToLoad.Add(DCPropertyNames.sn.ToString());                              // cognome
                search.PropertiesToLoad.Add(DCPropertyNames.givenName.ToString());                       // nome
                search.PropertiesToLoad.Add(DCPropertyNames.title.ToString());                           // titolo
                search.PropertiesToLoad.Add(DCPropertyNames.physicalDeliveryOfficeName.ToString());      // nome ufficio
                search.PropertiesToLoad.Add(DCPropertyNames.department.ToString());                      // dipartimento
                search.PropertiesToLoad.Add(DCPropertyNames.mail.ToString());                            // email
                search.PropertiesToLoad.Add(DCPropertyNames.homePhone.ToString());                       // telefono
                search.PropertiesToLoad.Add(DCPropertyNames.mobile.ToString());                          // cellulare
                search.PropertiesToLoad.Add(DCPropertyNames.facsimileTelephoneNumber.ToString());        // fax
                search.PropertiesToLoad.Add(DCPropertyNames.whenCreated.ToString());                     // data creazione account
                search.PropertiesToLoad.Add(DCPropertyNames.whenChanged.ToString());                     // data modifica  account
                search.PropertiesToLoad.Add(DCPropertyNames.distinguishedName.ToString());               // CN, OU, DC
                search.PropertiesToLoad.Add(DCPropertyNames.sAMAccountName.ToString());                  // username

                result = search.FindOne();

                if (result != null)
                    return result;                
            }
            catch (Exception ex)
            {
                string error = "Errore durante la ricerca dell'utente '"+ username + "' in active directory";
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, error, "", "", ex);
            }
            return result;
        }

        public enum DCPropertyNames
        {
            cn,
            sn,
            givenName,
            title,
            physicalDeliveryOfficeName,
            department,
            mail,
            homePhone,
            mobile,
            facsimileTelephoneNumber,
            whenCreated,
            whenChanged,
            distinguishedName,
            sAMAccountName,
        }
    }
}
