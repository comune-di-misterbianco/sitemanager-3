﻿using GenericDAO.DAO;
using NetCms.DBSessionProvider;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.Models;

namespace Users.BusinessLogic
{
    public static class UserTraceBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static void Save(TraceData data, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<TraceData> dao = new CriteriaNhibernateDAO<TraceData>(session);
            dao.SaveOrUpdate(data);
        }

        public static void TraceData(System.Web.HttpRequest req)
        {
            Users.Position pos = Users.BusinessLogic.UserTraceBusinessLogic.GetGeoLocationFromCookie(req);

            Users.Models.TraceData traceData = new Users.Models.TraceData();
            traceData.PublicIp = (req.Headers["X-Forwarded-For"] != null) ? req.Headers["X-Forwarded-For"] : req.ServerVariables["REMOTE_ADDR"];
            traceData.OS = req.UserAgent;
            traceData.Browser = req.UserAgent;
            traceData.Url = req.Url.AbsoluteUri;
            traceData.Referer = (req.UrlReferrer != null) ? req.UrlReferrer.ToString() : "";
            traceData.SessionID = System.Web.HttpContext.Current.Session.SessionID;
            traceData.Cookies = req.ServerVariables["HTTP_COOKIE"];
            traceData.DateOra = DateTime.Now;
            traceData.AcceptLanguage = req.ServerVariables["HTTP_ACCEPT_LANGUAGE"];

            traceData.Longitudine = pos.lon;
            traceData.Latitudine = pos.lat;
            traceData.PositionStatus = pos.PositionStatus;

            traceData.HypotheticUserType = "anonymous";

            Users.BusinessLogic.UserTraceBusinessLogic.Save(traceData);
        }

        public static Position GetGeoLocationFromCookie(System.Web.HttpRequest req)
        {
            Position pos = new Position();

            if (req.Cookies["posdata"] != null)
            {
                var posData = req.Cookies["posdata"].Value;
                // sanityze
                if (posData != null && posData.Length > 1)
                {
                    string[] coord = posData.Split(new char[] { '|' });
                    pos.lat = coord[0];
                    pos.lon = coord[1];
                    pos.PositionStatus = Users.Models.PositionStatusEnum.ok;
                }
                else
                {
                    pos.lat = null;
                    pos.lon = null;
                    pos.PositionStatus = (Users.Models.PositionStatusEnum)Enum.Parse(typeof(Users.Models.PositionStatusEnum), posData);
                }

                req.Cookies.Remove("posdata");
            }
            return pos;
        }       
           

        public static void SetUserType(string sessionID, string userType)
        {
            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    IList<TraceData> res = session
                                           .QueryOver<TraceData>()
                                           .Where(x => x.SessionID == sessionID)
                                           .List();

                    foreach (TraceData d in res)
                        d.HypotheticUserType = userType;

                    tx.Commit();

                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    string msg = "Errore durante l'aggiornamento dei dati di tracing dell'utente. " + ex.Message;                   
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, msg);
                }
            }
        }


    }
}
