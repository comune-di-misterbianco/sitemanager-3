﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NetCms.DBSessionProvider;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using NetService.Utility.ValidatedFields;
using NMG.Core;
using NMG.Core.Domain;
using NMG.Core.Generator;
using NHibernate.Criterion;
using System.Collections;
using System.Reflection;
using System.IO;
using System.Web;

namespace NetCms.Users.CustomAnagrafe
{
    public static class CustomAnagrafeBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static ISession GetCurrentMapSession()
        {
            return GetCurrentSession().GetSession(EntityMode.Map);
        }

        public static bool SaveOrUpdateCustomAnagrafe(CustomAnagrafe ca)
        {
            IGenericDAO<CustomAnagrafe> dao = new CriteriaNhibernateDAO<CustomAnagrafe>(GetCurrentSession());
            return (dao.SaveOrUpdate(ca) != null);
        }

        public static int CountCustomAnagrafe()
        {
            IGenericDAO<CustomAnagrafe> dao = new CriteriaNhibernateDAO<CustomAnagrafe>(GetCurrentSession());
            return dao.FindByCriteriaCount(null);
        }

        public static CustomAnagrafe GetCustomAnagrafeById(int customAnagrafeId, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<CustomAnagrafe> dao = new CriteriaNhibernateDAO<CustomAnagrafe>(session);
            return dao.GetById(customAnagrafeId);
        }

        public static CustomGroup GetCustomGroupById(int customGroupId, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<CustomGroup> dao = new CriteriaNhibernateDAO<CustomGroup>(session);
            return dao.GetById(customGroupId);
        }

        public static IList<CustomAnagrafe> FindAllCustomAnagrafe(ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            SearchParameter installedSP = new SearchParameter(null, "Installata", true, Finder.ComparisonCriteria.Equals, false);
            IGenericDAO<CustomAnagrafe> dao = new CriteriaNhibernateDAO<CustomAnagrafe>(session);
            return dao.FindByCriteria(new SearchParameter[] { installedSP });
        }

        public static IList<CustomAnagrafe> FindCustomAnagrafe(int pageStart, int pageSize, string sortBy, bool descending)
        {
            IGenericDAO<CustomAnagrafe> dao = new CriteriaNhibernateDAO<CustomAnagrafe>(GetCurrentSession());
            return dao.FindAll(pageStart, pageSize, sortBy, descending);
        }

        public static void DeleteCustomAnagrafe(CustomAnagrafe ca, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<CustomAnagrafe> dao = new CriteriaNhibernateDAO<CustomAnagrafe>(session);
            ca.DefaultGroup = null;
            ca.RegRequestNotifyGroup = null;
            dao.Delete(ca);
        }

        public static bool CustomAnagrafePresenteInInsert(string nome)
        {
            IGenericDAO<CustomAnagrafe> dao = new CriteriaNhibernateDAO<CustomAnagrafe>(GetCurrentSession());
            SearchParameter nomeSearchParameter = new SearchParameter(null, "Nome", nome, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { nomeSearchParameter }) != null;
        }

        //public static bool CustomAnagrafePresenteInUpdate(NetCms.Users.CustomAnagrafe.CustomAnagrafe anagrafe)
        //{
        //    IGenericDAO<NetCms.Users.CustomAnagrafe.CustomAnagrafe> dao = new CriteriaNhibernateDAO<NetCms.Users.CustomAnagrafe.CustomAnagrafe>(GetCurrentSession());
        //    SearchParameter idSearchParameter = new SearchParameter(null, "ID", anagrafe.ID, Finder.ComparisonCriteria.Not, false);
        //    SearchParameter nomeSearchParameter = new SearchParameter(null, "Nome", anagrafe.Nome, Finder.ComparisonCriteria.Equals, false);

        //    return dao.GetByCriteria(new SearchParameter[] { nomeSearchParameter, idSearchParameter }) != null;
        //}

        public static bool CreateAnagraficaCustom(VfManager customAnagrafeForm)
        {
            bool status = false;
            try
            {
                CustomAnagrafe anagrafe = new CustomAnagrafe();
                bool filled = customAnagrafeForm.FillObjectAdvanced(anagrafe, CustomAnagrafeBusinessLogic.GetCurrentSession());

                if (filled)
                {
                    IGenericDAO<CustomAnagrafe> dao = new CriteriaNhibernateDAO<CustomAnagrafe>(GetCurrentSession());
                    status = dao.SaveOrUpdate(anagrafe) != null;
                }
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante l'inserimento di un'anagrafica custom. " + ex.Message);
            }
            return status;
        }

        public static bool ModAnagraficaCustom(VfManager customAnagrafeForm, CustomAnagrafe anagrafica)
        {
            bool status = false;
            try
            {
                bool filled = customAnagrafeForm.FillObjectAdvanced(anagrafica, CustomAnagrafeBusinessLogic.GetCurrentSession());

                if (filled)
                {
                    IGenericDAO<CustomAnagrafe> dao = new CriteriaNhibernateDAO<CustomAnagrafe>(CustomAnagrafeBusinessLogic.GetCurrentSession());
                    IGenericDAO<CustomField> daoCustomField = new CriteriaNhibernateDAO<CustomField>(CustomAnagrafeBusinessLogic.GetCurrentSession());

                    //Cambio l'username associato all'anagrafica
                    //Tolgo il flag dal campo che non è più utilizzato come username
                    CustomField oldUsername = anagrafica.CustomFields.FirstOrDefault(x => x.IsUsername == true);
                    if (oldUsername != null)
                    {
                        oldUsername.IsUsername = false;
                        daoCustomField.SaveOrUpdate(oldUsername);
                    }
                   
                    if (!anagrafica.EmailAsUserName)
                    {
                        //Aggiungo il flag al nuovo campo che è stato selezionato nel form
                        int idNewUsername = int.Parse(customAnagrafeForm.Fields.Where(x => x.Key == "Username").First().PostBackValue);
                        CustomField newUsername = anagrafica.CustomFields.Where(x => x.ID == idNewUsername).First();
                        newUsername.IsUsername = true;
                        daoCustomField.SaveOrUpdate(newUsername);
                    }                                    

                    status = dao.SaveOrUpdate(anagrafica) != null;
                }
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la modifica dell'anagrafica custom con id " + anagrafica.ID + ". " + ex.Message);
            }
            return status;
        }

        public static int CountCustomFieldsByAnagrafe(CustomAnagrafe customAnagrafe)
        {
            IGenericDAO<CustomField> dao = new CriteriaNhibernateDAO<CustomField>(GetCurrentSession());
            SearchParameter anagrafeSp = new SearchParameter(null, "Anagrafe", customAnagrafe, Finder.ComparisonCriteria.Equals, false);
            SearchParameter nonCancellatoSp = new SearchParameter(null, "Cancellato", false, Finder.ComparisonCriteria.Equals, false);
            
            return dao.FindByCriteriaCount(new SearchParameter[] { anagrafeSp, nonCancellatoSp });
        }

        public static int CountFrontendCustomFieldsByAnagrafe(CustomAnagrafe customAnagrafe)
        {
            return customAnagrafe.FrontendFields.Where(x => x.Cancellato == false).Count();
        }

        public static IList<CustomField> FindCustomFieldsByAnagrafe(CustomAnagrafe customAnagrafe, int pageStart, int pageSize)
        {
            IGenericDAO<CustomField> dao = new CriteriaNhibernateDAO<CustomField>(GetCurrentSession());
            SearchParameter anagrafeSp = new SearchParameter(null, "Anagrafe", customAnagrafe, Finder.ComparisonCriteria.Equals, false);
            SearchParameter nonCancellatoSp = new SearchParameter(null, "Cancellato", false, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(pageStart,pageSize,"Name", false, new SearchParameter[] { anagrafeSp, nonCancellatoSp });
        }

        public static CustomField GetCustomFieldById(int id, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<CustomField> dao = new CriteriaNhibernateDAO<CustomField>(session);
            return dao.GetById(id);
        }

        public static CustomDropdownField GetCustomDropdownFieldById(int id, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<CustomDropdownField> dao = new CriteriaNhibernateDAO<CustomDropdownField>(session);
            return dao.GetById(id);
        }

        public static void DeleteCustomField(CustomField customField, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<CustomField> dao = new CriteriaNhibernateDAO<CustomField>(session);

            if (customField.Tipo == CustomField.TipoProprietà.Dropdown && customField.NewField != null)
            {
                IGenericDAO<CustomDropdownValue> dropValuesDao = new CriteriaNhibernateDAO<CustomDropdownValue>(session);

                CustomDropdownField cdfOld = customField as CustomDropdownField;
                CustomDropdownField cdfNew = cdfOld.NewField as CustomDropdownField;
                foreach (CustomDropdownValue oldValue in cdfOld.Values)
                {
                    CustomDropdownValue valueNew = new CustomDropdownValue();
                    valueNew.Label = oldValue.Label;
                    valueNew.Value = oldValue.Value;
                    valueNew.CustomField = cdfNew;
                    cdfNew.Values.Add(valueNew);

                    dropValuesDao.Delete(oldValue);
                    dropValuesDao.SaveOrUpdate(valueNew);
                }
            }
            dao.Delete(customField);
        }

        public static bool SaveOrUpdateCustomField(CustomField customField)
        {
            IGenericDAO<CustomField> dao = new CriteriaNhibernateDAO<CustomField>(GetCurrentSession());
            return (dao.SaveOrUpdate(customField) != null);
        }

        public static bool CreateCustomField(VfManager customFieldForm, CustomField field, CustomAnagrafe anagrafe)
        {
            bool status = false;
            try
            {
                bool filled = customFieldForm.FillObjectAdvanced(field, CustomAnagrafeBusinessLogic.GetCurrentSession());

                if (filled)
                {
                    field.Anagrafe = anagrafe;
                    anagrafe.CustomFields.Add(field);

                    IGenericDAO<CustomField> dao = new CriteriaNhibernateDAO<CustomField>(GetCurrentSession());
                    status = dao.SaveOrUpdate(field) != null;
                }
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante l'inserimento di un custom field. " + ex.Message);
            }
            return status;
        }

        public static bool ModCustomField(VfManager customFieldForm, int customFieldId, bool nomeNonModificato)
        {
            bool status = false;

            using (ISession sess = FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                CustomField field = GetCustomFieldById(customFieldId, sess);

                try
                {
                    IGenericDAO<CustomField> dao = new CriteriaNhibernateDAO<CustomField>(sess);

                    if (nomeNonModificato)
                    {
                        customFieldForm.FillObjectAdvanced(field, sess);
                        status = dao.SaveOrUpdate(field) != null;
                    }
                    else
                    {
                        // Creo un nuovo custom field
                        CustomField newCustomField = Activator.CreateInstance(field.GetType()) as CustomField;
                        field.NewField = newCustomField;

                        CustomAnagrafe anagrafe = field.Anagrafe;
                        bool cfInFront = anagrafe.FrontendFields.Contains(field);

                        //Aggiunta la condizione nel caso in cui l'anagrafica non usi la tipologia fieldset e il campo non sia ancora stato inserito in nessun raggruppamento
                        if (field.CustomGroup != null)
                        {
                            newCustomField.CustomGroup = GetCustomGroupById(field.CustomGroup.ID, sess);
                            newCustomField.PositionInGroup = field.PositionInGroup;
                        }

                        //Aggiorno il vecchio customfield settandolo come cancellato
                        MarkForDeleteCustomField(field, sess);

                        customFieldForm.FillObjectAdvanced(newCustomField, sess);

                        //Salvo un nuovo custom field modificato associandolo alla vecchia anagrafica
                        newCustomField.Anagrafe = anagrafe;
                        anagrafe.CustomFields.Add(newCustomField);
                        //Se il custom field era nella lista del frontend, aggiungo l'elemento nuovo nella lista del frontend (il vecchio elemento è già stato cancellato nel momento del MarkForDeleteCustomField)
                        if (cfInFront)
                        {
                            anagrafe.FrontendFields.Add(newCustomField);
                            IGenericDAO<CustomAnagrafe> daoAnagrafe = new CriteriaNhibernateDAO<CustomAnagrafe>(sess);
                            daoAnagrafe.SaveOrUpdate(anagrafe);
                        }
                        status = dao.SaveOrUpdate(newCustomField) != null;
                    }
                    tx.Commit();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la modifica del campo custom con id " + field.ID + " dell'anagrafica custom " + field.Anagrafe.Nome + ". " + ex.Message);
                    tx.Rollback();
                    status = false;
                }
            }
            return status;
        }

        public static CustomAnagrafe GetCustomAnagrafeByName(string nome, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<CustomAnagrafe> dao = new CriteriaNhibernateDAO<CustomAnagrafe>(session);
            SearchParameter nomeSearchParameter = new SearchParameter(null, "Nome", nome, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { nomeSearchParameter });
        }

        #region Metodi di generazione Anagrafiche su DB

        private static void GetAnagrafeBaseData(out PrimaryKey primaryKey, out List<Column> columns, out List<OneToOne> oneToOne, out List<HasMany> hasMany, out List<HasManyToMany> hasManyToMany)
        {
            var pkColumn = new Column { Name = "ID", IsPrimaryKey = true, PropertyType = "int", SqlType = "int(11)" };
            var emailColumn = new Column { Name = "Email", PropertyType = "string", SqlType = "varchar(255)", IsNullable = false };
            var oneToOneUsers = new OneToOne { Name = "User", ReferenceClass = "NetCms.Users.User", ReferenceProperty = "AnagraficaMapped" };
            var oneToManyAttachments = new HasMany { Reference = "Allegati", ReferenceClass = "NetCms.Users.AllegatoUtente", ReferenceColumn = "AnagraficaID" };

            primaryKey = new PrimaryKey { Columns = new List<Column> { pkColumn } };
            columns = new List<Column> { pkColumn, emailColumn };
            oneToOne = new List<OneToOne> { oneToOneUsers };
            hasMany = new List<HasMany> { oneToManyAttachments };
            hasManyToMany = new List<HasManyToMany> { };
        }

        private static ApplicationPreferences GetPreferences()
        {
            var preferences = new ApplicationPreferences
            {
                //FolderPath = NetCms.Configurations.Paths.AbsoluteConfigRoot + "\\applications-data\\",
                FolderPath = NetCms.Configurations.Paths.PhysicalRoot + "\\App_Data\\",
                TableName = "anagrafica",
                ClassName = "AnagraficaBase"
            };
            return preferences;
        }

        private static void GenerateMapping(ApplicationPreferences preferences, PrimaryKey primaryKey, List<Column> columns, List<OneToOne> oneToOne, List<HasMany> hasMany, List<HasManyToMany> hasManyToMany, List<JoinedSubClass> joinedSubClasses)
        {
            var generator = new SqlMappingGenerator(preferences, new NMG.Core.Domain.Table { PrimaryKey = primaryKey, Columns = columns, JoinedSubClasses = joinedSubClasses, OneToOneRelationships = oneToOne, HasManyRelationships = hasMany, HasManyToManyRelationships = hasManyToMany });

            generator.Generate();
        }

        private static void BuildJoinedSubClass(List<JoinedSubClass> joinedSubClasses, NetCms.Users.CustomAnagrafe.CustomAnagrafe customAnagrafe)
        {
            var joinedSubClass = new JoinedSubClass();
            string className = "Anagrafica" + customAnagrafe.Nome;
            joinedSubClass.ClassName = className;
            joinedSubClass.TableName = "anagrafica_" + customAnagrafe.DatabaseName;
            joinedSubClass.ColumnName = "id_" + joinedSubClass.TableName;

            var propertiesOfSubClass = new List<Column>();
            var oneToOneForSubClass = new List<OneToOne>();
            var hasManyForSubClass = new List<HasMany>();
            var hasManyToManyForSubClass = new List<HasManyToMany>();

            foreach (CustomField cf in customAnagrafe.CustomFields.Where(x => x.Cancellato == false))
            {
                Column property = null;
                HasMany hasMany = null;
                HasManyToMany manyToMany = null;
                switch (cf.EffettoSuMapping)
                {
                    case CustomField.MappingEffect.Property:
                        {
                            property = new Column { Name = cf.Name, IsNullable = !cf.Required, SqlType = cf.DBDataType, PropertyType = cf.SystemDataType };
                            break;
                        }
                    case CustomField.MappingEffect.ForeignKey:
                        {
                            property = new ForeignKeyColumn { Name = cf.Name, IsNullable = !cf.Required, IsForeignKey = true, ClassName = cf.SystemDataType, ForeignKeyString = cf.Anagrafe.DatabaseName + "_" + cf.Name.Replace(" ", "_") };
                            break;
                        }
                    case CustomField.MappingEffect.ManyToMany:
                        {
                            CustomManyToManyField field = cf as CustomManyToManyField;
                            manyToMany = new HasManyToMany { SetName = field.Name, TableName = joinedSubClass.TableName + "_" + field.TableName, KeyColumnName = "IDanagrafica_" + field.Anagrafe.DatabaseName, ManyToManyEntityName = field.EntityName, ManyToManyColumnName = field.IdColumnName };
                            break;
                        }
                    case CustomField.MappingEffect.OneToMany:
                        {
                            if (cf is CustomFileField)
                                break;

                            CustomOneToManyField field = cf as CustomOneToManyField;
                            hasMany = new HasMany { Reference = field.Name, ReferenceClass=field.SystemDataType, ReferenceColumn = "AnagraficaID" };
                            break;
                        }
                }

                if (property != null)
                    propertiesOfSubClass.Add(property);

                if (hasMany != null)
                    hasManyForSubClass.Add(hasMany);

                if (manyToMany != null)
                    hasManyToManyForSubClass.Add(manyToMany);

            }

            joinedSubClass.Columns = propertiesOfSubClass;
            joinedSubClass.OneToOneRelationships = oneToOneForSubClass;
            joinedSubClass.HasManyRelationships = hasManyForSubClass;
            joinedSubClass.HasManyToManyRelationships = hasManyToManyForSubClass;
            joinedSubClasses.Add(joinedSubClass);
        }

        public static void UpdateMappingConfiguration()
        {
            FluentSessionProvider.Instance.UpdateSessionFactory();
        }

        public static void BuildAnagrafeXmlMappingInsert(NetCms.Users.CustomAnagrafe.CustomAnagrafe caToAdd)
        {
            var preferences = GetPreferences();

            #region anagrafica base

            PrimaryKey primaryKey;
            List<Column> columns;
            List<OneToOne> oneToOne;
            List<HasMany> hasMany;
            List<HasManyToMany> hasManyToMany;
            GetAnagrafeBaseData(out primaryKey, out columns, out oneToOne, out hasMany, out hasManyToMany);

            #endregion

            #region anagrafiche specializzate

            var joinedSubClasses = new List<JoinedSubClass>();

            // ricostruisco la parte di mapping di quelle precedentemente installate
            foreach (NetCms.Users.CustomAnagrafe.CustomAnagrafe customAnagrafe in CustomAnagrafeBusinessLogic.FindAllCustomAnagrafe())
            {
                BuildJoinedSubClass(joinedSubClasses, customAnagrafe);
            }

            // aggiungo la parte per l'anagrafica che sto installando
            BuildJoinedSubClass(joinedSubClasses, caToAdd);

            #endregion

            GenerateMapping(preferences, primaryKey, columns, oneToOne, hasMany, hasManyToMany, joinedSubClasses);
        }

        public static void BuildAnagrafeXmlMappingRemove(NetCms.Users.CustomAnagrafe.CustomAnagrafe caToDel)
        {
            var preferences = GetPreferences();

            #region anagrafica base

            PrimaryKey primaryKey;
            List<Column> columns;
            List<OneToOne> oneToOne;
            List<HasMany> hasMany;
            List<HasManyToMany> hasManyToMany;
            GetAnagrafeBaseData(out primaryKey, out columns, out oneToOne, out hasMany, out hasManyToMany);

            #endregion

            #region anagrafiche specializzate

            var joinedSubClasses = new List<JoinedSubClass>();

            // ricostruisco la parte di mapping di quelle precedentemente installate tranne quella che sto eliminando
            foreach (NetCms.Users.CustomAnagrafe.CustomAnagrafe customAnagrafe in CustomAnagrafeBusinessLogic.FindAllCustomAnagrafe().Where(x => x.ID != caToDel.ID))
            {
                BuildJoinedSubClass(joinedSubClasses, customAnagrafe);
            }

            #endregion

            GenerateMapping(preferences, primaryKey, columns, oneToOne, hasMany, hasManyToMany, joinedSubClasses);

        }

        #region cancellazione manuale da database per mantenere la sincronizzazione file hbm e db

        public static void DropAnagrafica(NetCms.Users.CustomAnagrafe.CustomAnagrafe caToDel, NetCms.Connections.Connection connection)
        {
            connection.Execute("DROP TABLE anagrafica_" + caToDel.DatabaseName + ";");
        }

        public static void DropColonna(CustomField cfToDel, NetCms.Connections.Connection connection)
        {
            connection.Execute("ALTER TABLE anagrafica_" + cfToDel.Anagrafe.DatabaseName + " DROP COLUMN " + cfToDel.Name.Replace(" ", "_"));
        }

        public static void DropForeignKey(CustomField cfToDel, NetCms.Connections.Connection connection)
        {
            connection.Execute("ALTER TABLE anagrafica_" + cfToDel.Anagrafe.DatabaseName + " DROP FOREIGN KEY " + cfToDel.Anagrafe.DatabaseName + "_" + cfToDel.Name.Replace(" ", "_"));
        }

        public static void CopyDataColumn(CustomField cfToDel, CustomField cfNew, NetCms.Connections.Connection connection)
        {
            connection.Execute("UPDATE anagrafica_" + cfToDel.Anagrafe.DatabaseName + " SET " + cfNew.Name.Replace(" ", "_") + " = " + cfToDel.Name.Replace(" ", "_"));
        }

        public static void DropDeletedCustomField(NetCms.Connections.Connection connection)
        {
            using (ISession sess = FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    foreach (NetCms.Users.CustomAnagrafe.CustomAnagrafe customAnagrafe in CustomAnagrafeBusinessLogic.FindAllCustomAnagrafe(sess))
                    {
                        foreach (CustomField cf in customAnagrafe.CustomFields.Where(x => x.Cancellato == true))
                        {
                            if (cf.NewField != null)
                                CopyDataColumn(cf, cf.NewField, connection);
                            if (cf.EffettoSuMapping == CustomField.MappingEffect.ForeignKey)
                                DropForeignKey(cf, connection);
                            
                            if (cf.Tipo == CustomField.TipoProprietà.File)
                            {
                                // non ho colonne da cancellare nella tabella anagrafica specifica
                                // devo rimuovere gli allegati
                                DeleteAllegatiByAnagraficaType("Anagrafica" + customAnagrafe.Nome, sess); 
                            }
                            else
                            {
                                DropColonna(cf, connection);
                            } 
                            
                            DeleteCustomField(cf, sess);
                            if (customAnagrafe.FrontendFields.Contains(cf))
                                customAnagrafe.FrontendFields.Remove(cf);
                        }

                        customAnagrafe.CustomFields = customAnagrafe.CustomFields.Where(x => x.Cancellato == false).ToList();
                    }
                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
            }
        }

        public static void MarkForDeleteCustomField(CustomField cf, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = CustomAnagrafeBusinessLogic.GetCurrentSession();

            CustomAnagrafe anagrafe = cf.Anagrafe;
            
            IGenericDAO<CustomAnagrafe> daoAnagrafe = new CriteriaNhibernateDAO<CustomAnagrafe>(innerSession);

            if (anagrafe.FrontendFields.Contains(cf))
            {
                anagrafe.FrontendFields.Remove(cf);
            }

            IGenericDAO<CustomField> dao = new CriteriaNhibernateDAO<CustomField>(innerSession);
            cf.Cancellato = true;
            cf.CustomGroup = null;
            dao.SaveOrUpdate(cf);
        }

        public static void DeleteAnagrafiche(NetCms.Users.CustomAnagrafe.CustomAnagrafe ca, ISession session)
        {
            try
            {
                ISession mapSession = session.GetSession(EntityMode.Map);
                ICriteria criteria = mapSession.CreateCriteria("Anagrafica" + ca.Nome);
                IList<Hashtable> anagraficheHashTable = criteria.List<Hashtable>();

                foreach (Hashtable anagrafica in anagraficheHashTable)
                {
                    Hashtable userHashTable = (Hashtable)anagrafica["User"];

                    User user = UsersBusinessLogic.GetById(int.Parse(userHashTable["ID"].ToString()), session);
                    UsersBusinessLogic.DeleteUser(user, session);
                }
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la cancellazione delle anagrafiche associate alla custom anagrafe. " + ex.Message);
                throw ex;
            }
        }

        public static int CountAnagraficheByCustomAnagrafe(NetCms.Users.CustomAnagrafe.CustomAnagrafe ca, ISession session)
        {
            try
            {
                ISession mapSession = session.GetSession(EntityMode.Map);
                ICriteria criteria = mapSession.CreateCriteria("Anagrafica" + ca.Nome);
                ICriteria countCriteria = CriteriaTransformer.TransformToRowCount(criteria);
                return countCriteria.UniqueResult<int>();

            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la lettura delle anagrafiche associate alla custom anagrafe. " + ex.Message);
            }
            return 0;

        }


        #endregion


        #endregion

        #region Metodi gestione DropDown Custom

        public static IList<CustomDropdownValue> FindDropDownValues(int pageStart, int pageSize, int idCustomField)
        {
            IGenericDAO<NetCms.Users.CustomAnagrafe.CustomDropdownValue> dao = new CriteriaNhibernateDAO<NetCms.Users.CustomAnagrafe.CustomDropdownValue>(GetCurrentSession());
            SearchParameter idSP = new SearchParameter(null, "CustomField.ID", idCustomField, Finder.ComparisonCriteria.Equals, false);
            return dao.FindByCriteria(pageStart, pageSize, new SearchParameter[] { idSP });
        }

        public static int CountDropDownValues(int idCustomField)
        {
            IGenericDAO<NetCms.Users.CustomAnagrafe.CustomDropdownValue> dao = new CriteriaNhibernateDAO<NetCms.Users.CustomAnagrafe.CustomDropdownValue>(GetCurrentSession());
            SearchParameter idSP = new SearchParameter(null, "CustomField.ID", idCustomField, Finder.ComparisonCriteria.Equals, false);
            return dao.FindByCriteriaCount(new SearchParameter[] { idSP });
        }

        public static bool ModDropdownValue(VfManager DDForm, CustomDropdownValue value)
        {
            bool status = false;
            try
            {
                bool filled = DDForm.FillObjectAdvanced(value, CustomAnagrafeBusinessLogic.GetCurrentSession());

                if (filled)
                {
                    IGenericDAO<CustomDropdownValue> dao = new CriteriaNhibernateDAO<CustomDropdownValue>(CustomAnagrafeBusinessLogic.GetCurrentSession());
                    status = dao.SaveOrUpdate(value) != null;
                }
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la modifica del valore della dropdown. " + ex.Message);
            }
            return status;
        }

        public static bool CreateDropdownValue(VfManager DDForm, NetCms.Users.CustomAnagrafe.CustomDropdownField Dropdown)
        {
            bool status = false;
            try
            {
                CustomDropdownValue value = new CustomDropdownValue();
                bool filled = DDForm.FillObjectAdvanced(value, CustomAnagrafeBusinessLogic.GetCurrentSession());

                if (filled)
                {
                    value.CustomField = Dropdown;
                    Dropdown.Values.Add(value);

                    IGenericDAO<CustomDropdownValue> dao = new CriteriaNhibernateDAO<CustomDropdownValue>(GetCurrentSession());
                    status = dao.SaveOrUpdate(value) != null;
                }
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante l'inserimento di un Dropdown value. " + ex.Message);
            }
            return status;
        }

        public static void DeleteDropdownValue(NetCms.Users.CustomAnagrafe.CustomDropdownValue value)
        {
            IGenericDAO<NetCms.Users.CustomAnagrafe.CustomDropdownValue> dao = new CriteriaNhibernateDAO<NetCms.Users.CustomAnagrafe.CustomDropdownValue>(GetCurrentSession());
            value.CustomField.Values.Remove(value);
            value.CustomField = null;
            dao.Delete(value);
        }

        public static CustomDropdownValue GetDropDownValueById(int idDropDownValue)
        {
            IGenericDAO<NetCms.Users.CustomAnagrafe.CustomDropdownValue> dao = new CriteriaNhibernateDAO<NetCms.Users.CustomAnagrafe.CustomDropdownValue>(GetCurrentSession());
            
            return dao.GetById(idDropDownValue);
        }

        #endregion

        #region Metodi gestione CheckboxList Custom

        public static IList<CustomCheckboxListValue> FindCheckboxListValues(int pageStart, int pageSize, int idCustomField)
        {
            IGenericDAO<NetCms.Users.CustomAnagrafe.CustomCheckboxListValue> dao = new CriteriaNhibernateDAO<NetCms.Users.CustomAnagrafe.CustomCheckboxListValue>(GetCurrentSession());
            SearchParameter idSP = new SearchParameter(null, "CustomField.ID", idCustomField, Finder.ComparisonCriteria.Equals, false);
            return dao.FindByCriteria(pageStart, pageSize, new SearchParameter[] { idSP });
        }

        public static int CountCheckboxListValues(int idCustomField)
        {
            IGenericDAO<NetCms.Users.CustomAnagrafe.CustomCheckboxListValue> dao = new CriteriaNhibernateDAO<NetCms.Users.CustomAnagrafe.CustomCheckboxListValue>(GetCurrentSession());
            SearchParameter idSP = new SearchParameter(null, "CustomField.ID", idCustomField, Finder.ComparisonCriteria.Equals, false);
            return dao.FindByCriteriaCount(new SearchParameter[] { idSP });
        }

        public static bool ModCheckboxListValue(VfManager CBLForm, CustomCheckboxListValue value)
        {
            bool status = false;
            try
            {
                bool filled = CBLForm.FillObjectAdvanced(value, CustomAnagrafeBusinessLogic.GetCurrentSession());

                if (filled)
                {
                    IGenericDAO<CustomCheckboxListValue> dao = new CriteriaNhibernateDAO<CustomCheckboxListValue>(CustomAnagrafeBusinessLogic.GetCurrentSession());
                    status = dao.SaveOrUpdate(value) != null;
                }
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la modifica del valore della CheckboxList. " + ex.Message);
            }
            return status;
        }

        public static bool CreateCheckboxListValue(VfManager CBLForm, NetCms.Users.CustomAnagrafe.CustomCheckboxListField CheckboxList)
        {
            bool status = false;
            try
            {
                CustomCheckboxListValue value = new CustomCheckboxListValue();
                bool filled = CBLForm.FillObjectAdvanced(value, CustomAnagrafeBusinessLogic.GetCurrentSession());

                if (filled)
                {
                    value.CustomField = CheckboxList;
                    CheckboxList.Values.Add(value);

                    IGenericDAO<CustomCheckboxListValue> dao = new CriteriaNhibernateDAO<CustomCheckboxListValue>(GetCurrentSession());
                    status = dao.SaveOrUpdate(value) != null;
                }
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante l'inserimento di un CheckboxList value. " + ex.Message);
            }
            return status;
        }

        public static void DeleteCheckboxListValue(NetCms.Users.CustomAnagrafe.CustomCheckboxListValue value)
        {
            IGenericDAO<NetCms.Users.CustomAnagrafe.CustomCheckboxListValue> dao = new CriteriaNhibernateDAO<NetCms.Users.CustomAnagrafe.CustomCheckboxListValue>(GetCurrentSession());
            value.CustomField.Values.Remove(value);
            value.CustomField = null;
            dao.Delete(value);
        }

        #endregion

        #region Metodi gestione Comune Custom

        public static IList<Regione> FindRegioni()
        {
            IGenericDAO<Regione> dao = new CriteriaNhibernateDAO<Regione>(GetCurrentSession());
            return dao.FindAll();
        }

        public static IList<Provincia> FindProvinceByRegione(Regione rg)
        {
            IGenericDAO<Provincia> dao = new CriteriaNhibernateDAO<Provincia>(GetCurrentSession());
            SearchParameter IdSP = new SearchParameter(null, "Regione.ID", rg.ID, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[] { IdSP });
        }

        public static IList<Comune> FindComuniByProvincia(Provincia pv)
        {
            IGenericDAO<Comune> dao = new CriteriaNhibernateDAO<Comune>(GetCurrentSession());
            SearchParameter IdSP = new SearchParameter(null, "Provincia.ID", pv.ID, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[] { IdSP });
        }

        public static Comune GetComuneById(int idComune, ISession session = null)
        {
            if (session == null)
                session = CustomAnagrafeBusinessLogic.GetCurrentSession();

            IGenericDAO<Comune> dao = new CriteriaNhibernateDAO<Comune>(session);
            return dao.GetById(idComune);
        }

        public static Comune GetComuneByCodiceComune(string codiceComune, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = CustomAnagrafeBusinessLogic.GetCurrentSession();

            IGenericDAO<Comune> dao = new CriteriaNhibernateDAO<Comune>(innerSession);

            SearchParameter codiceComuneSP = new SearchParameter(null, "CodiceComune", codiceComune, Finder.ComparisonCriteria.Equals, false);
            return dao.GetByCriteria(new SearchParameter[] { codiceComuneSP });
        }

        public static Comune GetComuneByCodiceCatastale(string codiceCatastale, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = CustomAnagrafeBusinessLogic.GetCurrentSession();

            IGenericDAO<Comune> dao = new CriteriaNhibernateDAO<Comune>(innerSession);

            SearchParameter codiceCatastaleSP = new SearchParameter(null, "CodiceCatastale", codiceCatastale, Finder.ComparisonCriteria.Equals, false);
            return dao.GetByCriteria(new SearchParameter[] { codiceCatastaleSP });
        }

        public static Provincia GetProvinciaById(int idProv)
        {
            IGenericDAO<Provincia> dao = new CriteriaNhibernateDAO<Provincia>(GetCurrentSession());
            return dao.GetById(idProv);
        }

        public static Provincia GetProvinciaByCodiceProvincia(string codiceProvincia, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = CustomAnagrafeBusinessLogic.GetCurrentSession();

            IGenericDAO<Provincia> dao = new CriteriaNhibernateDAO<Provincia>(innerSession);

            SearchParameter codiceProvinciaSP = new SearchParameter(null, "CodiceProvincia", codiceProvincia, Finder.ComparisonCriteria.Equals, false);
            return dao.GetByCriteria(new SearchParameter[] { codiceProvinciaSP });
        }


        public static Provincia GetProvinciaByCodiceComune(string codiceComune, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = CustomAnagrafeBusinessLogic.GetCurrentSession();

            IGenericDAO<Provincia> dao = new CriteriaNhibernateDAO<Provincia>(innerSession);

            SearchParameter codiceComuneSP = new SearchParameter(null, "CodiceComune", codiceComune, Finder.ComparisonCriteria.Equals, false);
            SearchParameter comuneSP = new SearchParameter(null, "Comuni", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { codiceComuneSP });

            return dao.GetByCriteria(new SearchParameter[] { comuneSP });
        }

        public static Regione GetRegioneById(int idReg)
        {
            IGenericDAO<Regione> dao = new CriteriaNhibernateDAO<Regione>(GetCurrentSession());
            return dao.GetById(idReg);
        }

        public static Regione GetRegioneByNome(string nome, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = CustomAnagrafeBusinessLogic.GetCurrentSession();

            IGenericDAO<Regione> dao = new CriteriaNhibernateDAO<Regione>(innerSession);

            SearchParameter nomeRegioneSP = new SearchParameter(null, "Nome", nome, Finder.ComparisonCriteria.Equals, false);
            return dao.GetByCriteria(new SearchParameter[] { nomeRegioneSP });
        }

        public static IList<Provincia> FindProvinceByIdReg(int idReg)
        {
            IGenericDAO<Provincia> dao = new CriteriaNhibernateDAO<Provincia>(GetCurrentSession());
            SearchParameter IdSP = new SearchParameter(null, "Regione.ID", idReg, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[] { IdSP });
        }

        public static IList<Comune> FindComuniByIdProv(int idProv)
        {
            IGenericDAO<Comune> dao = new CriteriaNhibernateDAO<Comune>(GetCurrentSession());
            SearchParameter IdSP = new SearchParameter(null, "Provincia.ID", idProv, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[] { IdSP });
        }

        public static IList<Provincia> FindProvince()
        {
            IGenericDAO<Provincia> dao = new CriteriaNhibernateDAO<Provincia>(GetCurrentSession());

            return dao.FindAll("Nome",false);
        }

        public static IList<Comune> FindComuni()
        {
            IGenericDAO<Comune> dao = new CriteriaNhibernateDAO<Comune>(GetCurrentSession());

            return dao.FindAll("Nome", false);
        }

        #endregion

        #region Metodi gestione Custom Field su Frontend

        public static IEnumerable<CustomField> FindFrontCustomFieldsByAnagrafe(CustomAnagrafe ca)
        {
            return ca.CustomFields.Where(x => x.Cancellato == false).Where(x => !ca.FrontendFields.Any(m => x.Name==m.Name));
        }
        #endregion

        #region Metodi gestione CustomForeignKeyField

        public static object GetObjectByIdAndType(int id, Type tipo, ISession session = null)
        {
            if (session == null)
              session = GetCurrentSession();

            var daoType = typeof(GenericDAO.DAO.CriteriaNhibernateDAO<>);
            Type[] typeArgs = { tipo };
            var makeDao = daoType.MakeGenericType(typeArgs);
            var dao = Activator.CreateInstance(makeDao, session);
            MethodInfo mi = dao.GetType().GetMethod("GetById", new Type[] { typeof(int) });
            return mi.Invoke(dao, new object[] { id });
        }

        #endregion

        #region Metodi gestione Gruppi

        public static bool CreateCustomGroup(VfManager groupForm, CustomAnagrafe anagrafe)
        {
            try
            {
                CustomGroup group = new CustomGroup();
                group.CustomAnagrafe = anagrafe;

                groupForm.FillObjectAdvanced(group, CustomAnagrafeBusinessLogic.GetCurrentSession());
                
                IGenericDAO<CustomGroup> dao = new CriteriaNhibernateDAO<CustomGroup>(GetCurrentSession());
                dao.SaveOrUpdate(group);

                return true;
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la creazione di un gruppo per l'anagrafica custom " + anagrafe.Nome + ". " + ex.Message);

                return false;
            }
        }

        public static bool ModCustomGroup(VfManager customGroupForm, CustomGroup group)
        {
            try
            {
                customGroupForm.FillObjectAdvanced(group, GetCurrentSession());
                
                IGenericDAO<CustomGroup> dao = new CriteriaNhibernateDAO<CustomGroup>(GetCurrentSession());
                dao.SaveOrUpdate(group);
                return true;
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore durante la modifica del gruppo " + group.Nome + " per l'anagrafica custom " + group.CustomAnagrafe.Nome + ". " + ex.Message);

                return false;
            }
        }

        public static int CountCustomGroupsByAnagrafe(CustomAnagrafe customAnagrafe)
        {
            IGenericDAO<CustomGroup> dao = new CriteriaNhibernateDAO<CustomGroup>(GetCurrentSession());

            SearchParameter anagrafeSP = new SearchParameter(null, "CustomAnagrafe", customAnagrafe, Finder.ComparisonCriteria.Equals, false);
            return dao.FindByCriteriaCount(new SearchParameter[] { anagrafeSP });
        }

        public static int CountCustomFieldsByCustomGroup(CustomGroup customGroup)
        {
            IGenericDAO<CustomField> dao = new CriteriaNhibernateDAO<CustomField>(GetCurrentSession());

            SearchParameter nonCancellatoSp = new SearchParameter(null, "Cancellato", false, Finder.ComparisonCriteria.Equals, false);
            SearchParameter groupSP = new SearchParameter(null, "CustomGroup", customGroup, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteriaCount(new SearchParameter[] { groupSP, nonCancellatoSp });
        }

        public static bool SaveOrUpdateCustomGroup(CustomGroup customGroup)
        {
            IGenericDAO<CustomGroup> dao = new CriteriaNhibernateDAO<CustomGroup>(GetCurrentSession());
            return (dao.SaveOrUpdate(customGroup) != null);
        }

        public static IList<CustomField> FindCustomFieldNotYetAddedToGroups(CustomAnagrafe ca)
        {
            IGenericDAO<CustomField> dao = new CriteriaNhibernateDAO<CustomField>(GetCurrentSession());
            
            SearchParameter anagrafeSP = new SearchParameter(null, "Anagrafe", ca, Finder.ComparisonCriteria.Equals, false);
            SearchParameter groupSP = new SearchParameter(null, "CustomGroup", SearchParameter.NullValue, Finder.ComparisonCriteria.Null, false);
            SearchParameter nonCancellatoSp = new SearchParameter(null, "Cancellato", false, Finder.ComparisonCriteria.Equals, false);
            
            return dao.FindByCriteria(new SearchParameter[] { anagrafeSP, groupSP, nonCancellatoSp });
        }

        public static void DeleteCustomGroup(CustomGroup customGroup)
        {
            IGenericDAO<CustomGroup> dao = new CriteriaNhibernateDAO<CustomGroup>(GetCurrentSession());
            customGroup.CustomAnagrafe.CustomGroups.Remove(customGroup);
            customGroup.CustomAnagrafe = null;
            dao.Delete(customGroup); 
        }

        #endregion

        #region Metodi gestione Applicazioni

        public static void DeleteCustomAnagrafeApplication(CustomAnagrafeApplicazioni caapp)
        {
            caapp.Anagrafe.Applicazioni.Remove(caapp);
            caapp.Anagrafe = null;
            IGenericDAO<CustomAnagrafeApplicazioni> dao = new CriteriaNhibernateDAO<CustomAnagrafeApplicazioni>(GetCurrentSession());
            dao.Delete(caapp);
        }

        public static int CountAnagraficaApplicationsByAnagrafe(CustomAnagrafe customAnagrafe)
        {
            IGenericDAO<CustomAnagrafeApplicazioni> dao = new CriteriaNhibernateDAO<CustomAnagrafeApplicazioni>(GetCurrentSession());
            SearchParameter anagraficaSP = new SearchParameter(null, "Anagrafe", customAnagrafe, Finder.ComparisonCriteria.Equals, false);
            return dao.FindByCriteriaCount(new SearchParameter[] { anagraficaSP });
        }

        public static IList<CustomAnagrafeApplicazioni> FindAnagraficaApplicationsByAnagrafe(CustomAnagrafe customAnagrafe)
        {
            IGenericDAO<CustomAnagrafeApplicazioni> dao = new CriteriaNhibernateDAO<CustomAnagrafeApplicazioni>(GetCurrentSession());
            SearchParameter anagraficaSP = new SearchParameter(null, "Anagrafe", customAnagrafe, Finder.ComparisonCriteria.Equals, false);
            return dao.FindByCriteria(new SearchParameter[] { anagraficaSP });
        }

        public static bool SaveOrUpdateCustomAnagrafeApplication(CustomAnagrafeApplicazioni caapp)
        {
            IGenericDAO<CustomAnagrafeApplicazioni> dao = new CriteriaNhibernateDAO<CustomAnagrafeApplicazioni>(GetCurrentSession());
            return dao.SaveOrUpdate(caapp) != null;
        }

        public static CustomAnagrafeApplicazioni GetCustomAnagrafeApplication(CustomAnagrafe customAnagrafe, int idApp)
        {
            IGenericDAO<CustomAnagrafeApplicazioni> dao = new CriteriaNhibernateDAO<CustomAnagrafeApplicazioni>(GetCurrentSession());
            SearchParameter anagraficaSP = new SearchParameter(null, "Anagrafe", customAnagrafe, Finder.ComparisonCriteria.Equals, false);
            SearchParameter idAppSP = new SearchParameter(null, "IdApplicazione", idApp, Finder.ComparisonCriteria.Equals, false);
            return dao.GetByCriteria(new SearchParameter[] { anagraficaSP, idAppSP });
        }

        #endregion

        #region Metodi gestione Allegati

        public static IList<AllegatoUtente> FindAttachmentsByAnagrafe(Hashtable anagrafe, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            SearchParameter anagraficaSP = new SearchParameter(null, "AnagraficaMapped", anagrafe, Finder.ComparisonCriteria.Equals, false);
            IGenericDAO<AllegatoUtente> dao = new CriteriaNhibernateDAO<AllegatoUtente>(session);
            return dao.FindByCriteria(new SearchParameter[] { anagraficaSP });
        }

        public static AllegatoUtente GetAllegatoUtenteById(int idAllegato)
        {
            IGenericDAO<AllegatoUtente> dao = new CriteriaNhibernateDAO<AllegatoUtente>(GetCurrentSession());
            return dao.GetById(idAllegato);
        }

        public static void DeleteAllegatiByAnagraficaType(string anagraficaType, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<AllegatoUtente> dao = new CriteriaNhibernateDAO<AllegatoUtente>(session);

            foreach (AllegatoUtente allegato in FindAttachmentsByAnagraficaType(anagraficaType, session))
                dao.Delete(allegato);

        }

        public static IList<AllegatoUtente> FindAttachmentsByAnagraficaType(string anagraficaType, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IEnumerable<Hashtable> anagrafiche = UsersBusinessLogic.FindUsersByAnagraficaType(anagraficaType,session).Select(x=>x.AnagraficaMapped);

            IGenericDAO<AllegatoUtente> dao = new CriteriaNhibernateDAO<AllegatoUtente>(session);
            SearchParameter anagraficaSP = new SearchParameter(null, "AnagraficaMapped", anagrafiche, Finder.ComparisonCriteria.In, false);

            return dao.FindByCriteria(new SearchParameter[] { anagraficaSP });

        }

        #endregion

        public static Type GetTypeFromTypeName(string typeName)
        {
            foreach (Assembly currentassembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                Type t = currentassembly.GetType(typeName, false, true);
                if (t != null) { return t; }
            }

            return null;
        }

        /// <summary>
        /// Restituisce l'id dell'anagrafica corrispondente alla mail in input, utile in modelli dati che mappano l'id dell'anagrafica non associata direttamente all'oggetto
        /// </summary>
        /// <param name="mail"></param>
        /// <param name="mapSession"></param>
        /// <returns></returns>
        public static int GetAnagraficaIDByMail(string mail, ISession mapSession = null)
        {
            if (mapSession == null)
                mapSession = GetCurrentMapSession();


            ICriteria criteria = mapSession.CreateCriteria("AnagraficaBase");
            criteria.Add(Restrictions.Eq("Email", mail));
            criteria.SetProjection(Projections.Property("ID"));

            return criteria.UniqueResult<int>();

        }
    }
}
